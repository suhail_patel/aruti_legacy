﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalLeaveAssign
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalLeaveAssign))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlTraningYear = New System.Windows.Forms.Panel
        Me.LblEmployeeCount = New System.Windows.Forms.Label
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAppDate = New System.Windows.Forms.Panel
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpDate1 = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpDate2 = New System.Windows.Forms.DateTimePicker
        Me.pnlYear = New System.Windows.Forms.Panel
        Me.lblFromYear = New System.Windows.Forms.Label
        Me.nudToYear = New System.Windows.Forms.NumericUpDown
        Me.LblToYear = New System.Windows.Forms.Label
        Me.nudFromMonth = New System.Windows.Forms.NumericUpDown
        Me.LblToMonth = New System.Windows.Forms.Label
        Me.LblMonth = New System.Windows.Forms.Label
        Me.nudToMonth = New System.Windows.Forms.NumericUpDown
        Me.cboTocondition = New System.Windows.Forms.ComboBox
        Me.nudFromYear = New System.Windows.Forms.NumericUpDown
        Me.cboFromcondition = New System.Windows.Forms.ComboBox
        Me.objbtnSearchLeave1 = New eZee.Common.eZeeGradientButton
        Me.pnlFliter = New System.Windows.Forms.Panel
        Me.radConfirmationDate = New System.Windows.Forms.RadioButton
        Me.radProbationDate = New System.Windows.Forms.RadioButton
        Me.radExpYear = New System.Windows.Forms.RadioButton
        Me.radAppointedDate = New System.Windows.Forms.RadioButton
        Me.eline1 = New eZee.Common.eZeeLine
        Me.cboELCLeaveType = New System.Windows.Forms.ComboBox
        Me.LblELCLeaveType = New System.Windows.Forms.Label
        Me.rdDueDate = New System.Windows.Forms.RadioButton
        Me.LblDueDate = New System.Windows.Forms.Label
        Me.dtpDueDate = New System.Windows.Forms.DateTimePicker
        Me.rdNewEmployee = New System.Windows.Forms.RadioButton
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.lblGender = New System.Windows.Forms.Label
        Me.LblValue = New System.Windows.Forms.Label
        Me.LblFrom = New System.Windows.Forms.Label
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.TxtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgColhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhAppointdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJobTitle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAppliedLeave = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhApprovedLeave = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhIssuedLeave = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhRemaining_Bal = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.gbLeaveInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radShowAll = New System.Windows.Forms.RadioButton
        Me.radEmpWOAccrue = New System.Windows.Forms.RadioButton
        Me.lblAccrualDays = New System.Windows.Forms.Label
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.pnlAccrueSetting = New System.Windows.Forms.Panel
        Me.nudMaxNegativeDays = New System.Windows.Forms.NumericUpDown
        Me.lblMaxNegativeLimit = New System.Windows.Forms.Label
        Me.radDonotIssueAsonDate = New System.Windows.Forms.RadioButton
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.radDonotIssue = New System.Windows.Forms.RadioButton
        Me.radExceedBal = New System.Windows.Forms.RadioButton
        Me.chkNoAction = New System.Windows.Forms.CheckBox
        Me.radIssuebal = New System.Windows.Forms.RadioButton
        Me.LblEligibilityAfter = New System.Windows.Forms.Label
        Me.txtCfAmount = New eZee.TextBox.NumericTextBox
        Me.txtEligibilityAfter = New eZee.TextBox.NumericTextBox
        Me.LblCFAmount = New System.Windows.Forms.Label
        Me.lblAccrueSetting = New System.Windows.Forms.Label
        Me.objbtnSearchLeave2 = New eZee.Common.eZeeGradientButton
        Me.txtAccrualAmount = New eZee.TextBox.IntegerTextBox
        Me.lblStopDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpStopDate = New System.Windows.Forms.DateTimePicker
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.txtBatchNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblBatchNo = New System.Windows.Forms.Label
        Me.cboLeaveCode = New System.Windows.Forms.ComboBox
        Me.lblLeaveCode = New System.Windows.Forms.Label
        Me.LblMonthlyAccrue = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlTraningYear.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.pnlAppDate.SuspendLayout()
        Me.pnlYear.SuspendLayout()
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFliter.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbLeaveInfo.SuspendLayout()
        Me.pnlAccrueSetting.SuspendLayout()
        CType(Me.nudMaxNegativeDays, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTraningYear
        '
        Me.pnlTraningYear.Controls.Add(Me.LblEmployeeCount)
        Me.pnlTraningYear.Controls.Add(Me.gbFilterCriteria)
        Me.pnlTraningYear.Controls.Add(Me.LblValue)
        Me.pnlTraningYear.Controls.Add(Me.LblFrom)
        Me.pnlTraningYear.Controls.Add(Me.chkSelectAll)
        Me.pnlTraningYear.Controls.Add(Me.TxtSearch)
        Me.pnlTraningYear.Controls.Add(Me.dgEmployee)
        Me.pnlTraningYear.Controls.Add(Me.gbLeaveInfo)
        Me.pnlTraningYear.Controls.Add(Me.objFooter)
        Me.pnlTraningYear.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTraningYear.Location = New System.Drawing.Point(0, 0)
        Me.pnlTraningYear.Name = "pnlTraningYear"
        Me.pnlTraningYear.Size = New System.Drawing.Size(794, 648)
        Me.pnlTraningYear.TabIndex = 0
        '
        'LblEmployeeCount
        '
        Me.LblEmployeeCount.BackColor = System.Drawing.Color.Transparent
        Me.LblEmployeeCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmployeeCount.Location = New System.Drawing.Point(619, 138)
        Me.LblEmployeeCount.Name = "LblEmployeeCount"
        Me.LblEmployeeCount.Size = New System.Drawing.Size(157, 21)
        Me.LblEmployeeCount.TabIndex = 290
        Me.LblEmployeeCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.pnlAppDate)
        Me.gbFilterCriteria.Controls.Add(Me.pnlYear)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLeave1)
        Me.gbFilterCriteria.Controls.Add(Me.pnlFliter)
        Me.gbFilterCriteria.Controls.Add(Me.eline1)
        Me.gbFilterCriteria.Controls.Add(Me.cboELCLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.LblELCLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.rdDueDate)
        Me.gbFilterCriteria.Controls.Add(Me.LblDueDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDueDate)
        Me.gbFilterCriteria.Controls.Add(Me.rdNewEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboGender)
        Me.gbFilterCriteria.Controls.Add(Me.lblGender)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(2, 2)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(780, 130)
        Me.gbFilterCriteria.TabIndex = 14
        Me.gbFilterCriteria.TabStop = True
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAppDate
        '
        Me.pnlAppDate.Controls.Add(Me.lblToDate)
        Me.pnlAppDate.Controls.Add(Me.dtpDate1)
        Me.pnlAppDate.Controls.Add(Me.lblFromDate)
        Me.pnlAppDate.Controls.Add(Me.dtpDate2)
        Me.pnlAppDate.Location = New System.Drawing.Point(603, 74)
        Me.pnlAppDate.Name = "pnlAppDate"
        Me.pnlAppDate.Size = New System.Drawing.Size(171, 52)
        Me.pnlAppDate.TabIndex = 298
        '
        'lblToDate
        '
        Me.lblToDate.BackColor = System.Drawing.Color.Transparent
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(5, 32)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(69, 15)
        Me.lblToDate.TabIndex = 305
        Me.lblToDate.Text = "To Date"
        Me.lblToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate1
        '
        Me.dtpDate1.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate1.Location = New System.Drawing.Point(79, 2)
        Me.dtpDate1.Name = "dtpDate1"
        Me.dtpDate1.Size = New System.Drawing.Size(88, 21)
        Me.dtpDate1.TabIndex = 1
        '
        'lblFromDate
        '
        Me.lblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(5, 5)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(69, 15)
        Me.lblFromDate.TabIndex = 304
        Me.lblFromDate.Text = "From Date"
        Me.lblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate2
        '
        Me.dtpDate2.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate2.Location = New System.Drawing.Point(79, 29)
        Me.dtpDate2.Name = "dtpDate2"
        Me.dtpDate2.Size = New System.Drawing.Size(88, 21)
        Me.dtpDate2.TabIndex = 304
        '
        'pnlYear
        '
        Me.pnlYear.Controls.Add(Me.lblFromYear)
        Me.pnlYear.Controls.Add(Me.nudToYear)
        Me.pnlYear.Controls.Add(Me.LblToYear)
        Me.pnlYear.Controls.Add(Me.nudFromMonth)
        Me.pnlYear.Controls.Add(Me.LblToMonth)
        Me.pnlYear.Controls.Add(Me.LblMonth)
        Me.pnlYear.Controls.Add(Me.nudToMonth)
        Me.pnlYear.Controls.Add(Me.cboTocondition)
        Me.pnlYear.Controls.Add(Me.nudFromYear)
        Me.pnlYear.Controls.Add(Me.cboFromcondition)
        Me.pnlYear.Location = New System.Drawing.Point(265, 74)
        Me.pnlYear.Name = "pnlYear"
        Me.pnlYear.Size = New System.Drawing.Size(334, 52)
        Me.pnlYear.TabIndex = 303
        '
        'lblFromYear
        '
        Me.lblFromYear.BackColor = System.Drawing.Color.Transparent
        Me.lblFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromYear.Location = New System.Drawing.Point(3, 5)
        Me.lblFromYear.Name = "lblFromYear"
        Me.lblFromYear.Size = New System.Drawing.Size(57, 15)
        Me.lblFromYear.TabIndex = 279
        Me.lblFromYear.Text = "From Year"
        Me.lblFromYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudToYear
        '
        Me.nudToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToYear.Location = New System.Drawing.Point(67, 29)
        Me.nudToYear.Name = "nudToYear"
        Me.nudToYear.Size = New System.Drawing.Size(57, 21)
        Me.nudToYear.TabIndex = 292
        Me.nudToYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblToYear
        '
        Me.LblToYear.BackColor = System.Drawing.Color.Transparent
        Me.LblToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToYear.Location = New System.Drawing.Point(3, 32)
        Me.LblToYear.Name = "LblToYear"
        Me.LblToYear.Size = New System.Drawing.Size(57, 15)
        Me.LblToYear.TabIndex = 291
        Me.LblToYear.Text = "To Year"
        Me.LblToYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudFromMonth
        '
        Me.nudFromMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromMonth.Location = New System.Drawing.Point(203, 2)
        Me.nudFromMonth.Maximum = New Decimal(New Integer() {11, 0, 0, 0})
        Me.nudFromMonth.Name = "nudFromMonth"
        Me.nudFromMonth.Size = New System.Drawing.Size(57, 21)
        Me.nudFromMonth.TabIndex = 289
        Me.nudFromMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblToMonth
        '
        Me.LblToMonth.BackColor = System.Drawing.Color.Transparent
        Me.LblToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToMonth.Location = New System.Drawing.Point(129, 32)
        Me.LblToMonth.Name = "LblToMonth"
        Me.LblToMonth.Size = New System.Drawing.Size(68, 15)
        Me.LblToMonth.TabIndex = 293
        Me.LblToMonth.Text = "To Month"
        Me.LblToMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMonth
        '
        Me.LblMonth.BackColor = System.Drawing.Color.Transparent
        Me.LblMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMonth.Location = New System.Drawing.Point(129, 5)
        Me.LblMonth.Name = "LblMonth"
        Me.LblMonth.Size = New System.Drawing.Size(68, 15)
        Me.LblMonth.TabIndex = 288
        Me.LblMonth.Text = "From Month"
        Me.LblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudToMonth
        '
        Me.nudToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToMonth.Location = New System.Drawing.Point(203, 29)
        Me.nudToMonth.Maximum = New Decimal(New Integer() {11, 0, 0, 0})
        Me.nudToMonth.Name = "nudToMonth"
        Me.nudToMonth.Size = New System.Drawing.Size(57, 21)
        Me.nudToMonth.TabIndex = 294
        Me.nudToMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboTocondition
        '
        Me.cboTocondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTocondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTocondition.FormattingEnabled = True
        Me.cboTocondition.Location = New System.Drawing.Point(266, 29)
        Me.cboTocondition.Name = "cboTocondition"
        Me.cboTocondition.Size = New System.Drawing.Size(65, 21)
        Me.cboTocondition.TabIndex = 297
        '
        'nudFromYear
        '
        Me.nudFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromYear.Location = New System.Drawing.Point(67, 2)
        Me.nudFromYear.Name = "nudFromYear"
        Me.nudFromYear.Size = New System.Drawing.Size(57, 21)
        Me.nudFromYear.TabIndex = 281
        Me.nudFromYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboFromcondition
        '
        Me.cboFromcondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromcondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromcondition.FormattingEnabled = True
        Me.cboFromcondition.Location = New System.Drawing.Point(266, 2)
        Me.cboFromcondition.Name = "cboFromcondition"
        Me.cboFromcondition.Size = New System.Drawing.Size(65, 21)
        Me.cboFromcondition.TabIndex = 295
        '
        'objbtnSearchLeave1
        '
        Me.objbtnSearchLeave1.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave1.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave1.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeave1.BorderSelected = False
        Me.objbtnSearchLeave1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeave1.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeave1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeave1.Location = New System.Drawing.Point(733, 30)
        Me.objbtnSearchLeave1.Name = "objbtnSearchLeave1"
        Me.objbtnSearchLeave1.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeave1.TabIndex = 4
        '
        'pnlFliter
        '
        Me.pnlFliter.Controls.Add(Me.radConfirmationDate)
        Me.pnlFliter.Controls.Add(Me.radProbationDate)
        Me.pnlFliter.Controls.Add(Me.radExpYear)
        Me.pnlFliter.Controls.Add(Me.radAppointedDate)
        Me.pnlFliter.Location = New System.Drawing.Point(11, 74)
        Me.pnlFliter.Name = "pnlFliter"
        Me.pnlFliter.Size = New System.Drawing.Size(248, 52)
        Me.pnlFliter.TabIndex = 1
        '
        'radConfirmationDate
        '
        Me.radConfirmationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radConfirmationDate.Location = New System.Drawing.Point(132, 31)
        Me.radConfirmationDate.Name = "radConfirmationDate"
        Me.radConfirmationDate.Size = New System.Drawing.Size(112, 16)
        Me.radConfirmationDate.TabIndex = 305
        Me.radConfirmationDate.TabStop = True
        Me.radConfirmationDate.Text = "Confirmation Date"
        Me.radConfirmationDate.UseVisualStyleBackColor = True
        '
        'radProbationDate
        '
        Me.radProbationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radProbationDate.Location = New System.Drawing.Point(132, 4)
        Me.radProbationDate.Name = "radProbationDate"
        Me.radProbationDate.Size = New System.Drawing.Size(112, 16)
        Me.radProbationDate.TabIndex = 304
        Me.radProbationDate.TabStop = True
        Me.radProbationDate.Text = "Probation Date"
        Me.radProbationDate.UseVisualStyleBackColor = True
        '
        'radExpYear
        '
        Me.radExpYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExpYear.Location = New System.Drawing.Point(6, 4)
        Me.radExpYear.Name = "radExpYear"
        Me.radExpYear.Size = New System.Drawing.Size(122, 16)
        Me.radExpYear.TabIndex = 303
        Me.radExpYear.TabStop = True
        Me.radExpYear.Text = "Year of Experience"
        Me.radExpYear.UseVisualStyleBackColor = True
        '
        'radAppointedDate
        '
        Me.radAppointedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAppointedDate.Location = New System.Drawing.Point(6, 31)
        Me.radAppointedDate.Name = "radAppointedDate"
        Me.radAppointedDate.Size = New System.Drawing.Size(122, 16)
        Me.radAppointedDate.TabIndex = 1
        Me.radAppointedDate.TabStop = True
        Me.radAppointedDate.Text = "Appointment Date"
        Me.radAppointedDate.UseVisualStyleBackColor = True
        '
        'eline1
        '
        Me.eline1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.eline1.Location = New System.Drawing.Point(10, 55)
        Me.eline1.Name = "eline1"
        Me.eline1.Size = New System.Drawing.Size(741, 14)
        Me.eline1.TabIndex = 1
        Me.eline1.Text = "Filter Employee Using"
        '
        'cboELCLeaveType
        '
        Me.cboELCLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboELCLeaveType.DropDownWidth = 300
        Me.cboELCLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboELCLeaveType.FormattingEnabled = True
        Me.cboELCLeaveType.Location = New System.Drawing.Point(610, 30)
        Me.cboELCLeaveType.Name = "cboELCLeaveType"
        Me.cboELCLeaveType.Size = New System.Drawing.Size(117, 21)
        Me.cboELCLeaveType.TabIndex = 271
        '
        'LblELCLeaveType
        '
        Me.LblELCLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.LblELCLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblELCLeaveType.Location = New System.Drawing.Point(538, 33)
        Me.LblELCLeaveType.Name = "LblELCLeaveType"
        Me.LblELCLeaveType.Size = New System.Drawing.Size(66, 15)
        Me.LblELCLeaveType.TabIndex = 271
        Me.LblELCLeaveType.Text = "Leave Type"
        Me.LblELCLeaveType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rdDueDate
        '
        Me.rdDueDate.Checked = True
        Me.rdDueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdDueDate.Location = New System.Drawing.Point(166, 32)
        Me.rdDueDate.Name = "rdDueDate"
        Me.rdDueDate.Size = New System.Drawing.Size(72, 17)
        Me.rdDueDate.TabIndex = 300
        Me.rdDueDate.TabStop = True
        Me.rdDueDate.Text = "Due Date"
        Me.rdDueDate.UseVisualStyleBackColor = True
        '
        'LblDueDate
        '
        Me.LblDueDate.BackColor = System.Drawing.Color.Transparent
        Me.LblDueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDueDate.Location = New System.Drawing.Point(385, 33)
        Me.LblDueDate.Name = "LblDueDate"
        Me.LblDueDate.Size = New System.Drawing.Size(59, 15)
        Me.LblDueDate.TabIndex = 301
        Me.LblDueDate.Text = "Due Date"
        '
        'dtpDueDate
        '
        Me.dtpDueDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDueDate.Checked = False
        Me.dtpDueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDueDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDueDate.Location = New System.Drawing.Point(447, 30)
        Me.dtpDueDate.Name = "dtpDueDate"
        Me.dtpDueDate.Size = New System.Drawing.Size(85, 21)
        Me.dtpDueDate.TabIndex = 300
        '
        'rdNewEmployee
        '
        Me.rdNewEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdNewEmployee.Location = New System.Drawing.Point(244, 32)
        Me.rdNewEmployee.Name = "rdNewEmployee"
        Me.rdNewEmployee.Size = New System.Drawing.Size(135, 17)
        Me.rdNewEmployee.TabIndex = 299
        Me.rdNewEmployee.Text = "New Employee Accrue"
        Me.rdNewEmployee.UseVisualStyleBackColor = True
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(626, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(79, 16)
        Me.lnkAllocation.TabIndex = 284
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(753, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(730, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(65, 30)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(95, 21)
        Me.cboGender.TabIndex = 278
        '
        'lblGender
        '
        Me.lblGender.BackColor = System.Drawing.Color.Transparent
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(8, 33)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(51, 15)
        Me.lblGender.TabIndex = 277
        Me.lblGender.Text = "Gender"
        '
        'LblValue
        '
        Me.LblValue.BackColor = System.Drawing.Color.Transparent
        Me.LblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblValue.Location = New System.Drawing.Point(366, 138)
        Me.LblValue.Name = "LblValue"
        Me.LblValue.Size = New System.Drawing.Size(230, 21)
        Me.LblValue.TabIndex = 289
        Me.LblValue.Text = "#Value"
        Me.LblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblFrom
        '
        Me.LblFrom.BackColor = System.Drawing.Color.Transparent
        Me.LblFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFrom.Location = New System.Drawing.Point(301, 138)
        Me.LblFrom.Name = "LblFrom"
        Me.LblFrom.Size = New System.Drawing.Size(59, 21)
        Me.LblFrom.TabIndex = 288
        Me.LblFrom.Text = "From"
        Me.LblFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(11, 171)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 22
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'TxtSearch
        '
        Me.TxtSearch.Flags = 0
        Me.TxtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.TxtSearch.Location = New System.Drawing.Point(3, 138)
        Me.TxtSearch.Name = "TxtSearch"
        Me.TxtSearch.Size = New System.Drawing.Size(292, 21)
        Me.TxtSearch.TabIndex = 271
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objSelect, Me.dgColhEmpCode, Me.dgColhEmployee, Me.dgColhAppointdate, Me.dgcolhJobTitle, Me.dgcolhAppliedLeave, Me.dgcolhApprovedLeave, Me.dgcolhIssuedLeave, Me.dgcolhRemaining_Bal})
        Me.dgEmployee.Location = New System.Drawing.Point(3, 165)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.Size = New System.Drawing.Size(779, 221)
        Me.dgEmployee.TabIndex = 283
        '
        'objSelect
        '
        Me.objSelect.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objSelect.Frozen = True
        Me.objSelect.HeaderText = ""
        Me.objSelect.Name = "objSelect"
        Me.objSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objSelect.Width = 25
        '
        'dgColhEmpCode
        '
        Me.dgColhEmpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmpCode.HeaderText = "Code"
        Me.dgColhEmpCode.Name = "dgColhEmpCode"
        Me.dgColhEmpCode.ReadOnly = True
        Me.dgColhEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhEmpCode.Width = 90
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhEmployee.HeaderText = "Employee"
        Me.dgColhEmployee.MinimumWidth = 175
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        Me.dgColhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgColhAppointdate
        '
        Me.dgColhAppointdate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhAppointdate.HeaderText = "Appointment Date"
        Me.dgColhAppointdate.Name = "dgColhAppointdate"
        Me.dgColhAppointdate.ReadOnly = True
        Me.dgColhAppointdate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'dgcolhJobTitle
        '
        Me.dgcolhJobTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhJobTitle.HeaderText = "Job Title"
        Me.dgcolhJobTitle.Name = "dgcolhJobTitle"
        Me.dgcolhJobTitle.ReadOnly = True
        Me.dgcolhJobTitle.Width = 230
        '
        'dgcolhAppliedLeave
        '
        Me.dgcolhAppliedLeave.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhAppliedLeave.DecimalLength = 2
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F2"
        Me.dgcolhAppliedLeave.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhAppliedLeave.HeaderText = "Applied Leave Days"
        Me.dgcolhAppliedLeave.Name = "dgcolhAppliedLeave"
        Me.dgcolhAppliedLeave.ReadOnly = True
        Me.dgcolhAppliedLeave.Width = 125
        '
        'dgcolhApprovedLeave
        '
        Me.dgcolhApprovedLeave.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhApprovedLeave.DecimalLength = 2
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "F2"
        Me.dgcolhApprovedLeave.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhApprovedLeave.HeaderText = "Approved Leave Days"
        Me.dgcolhApprovedLeave.Name = "dgcolhApprovedLeave"
        Me.dgcolhApprovedLeave.ReadOnly = True
        Me.dgcolhApprovedLeave.Width = 125
        '
        'dgcolhIssuedLeave
        '
        Me.dgcolhIssuedLeave.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhIssuedLeave.DecimalLength = 2
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "F2"
        Me.dgcolhIssuedLeave.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhIssuedLeave.HeaderText = "Issued Leave Days"
        Me.dgcolhIssuedLeave.Name = "dgcolhIssuedLeave"
        Me.dgcolhIssuedLeave.ReadOnly = True
        Me.dgcolhIssuedLeave.Width = 125
        '
        'dgcolhRemaining_Bal
        '
        Me.dgcolhRemaining_Bal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhRemaining_Bal.DecimalLength = 2
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "F2"
        Me.dgcolhRemaining_Bal.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhRemaining_Bal.HeaderText = "Remaining Balance"
        Me.dgcolhRemaining_Bal.Name = "dgcolhRemaining_Bal"
        Me.dgcolhRemaining_Bal.ReadOnly = True
        Me.dgcolhRemaining_Bal.Width = 125
        '
        'gbLeaveInfo
        '
        Me.gbLeaveInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveInfo.Checked = False
        Me.gbLeaveInfo.CollapseAllExceptThis = False
        Me.gbLeaveInfo.CollapsedHoverImage = Nothing
        Me.gbLeaveInfo.CollapsedNormalImage = Nothing
        Me.gbLeaveInfo.CollapsedPressedImage = Nothing
        Me.gbLeaveInfo.CollapseOnLoad = False
        Me.gbLeaveInfo.Controls.Add(Me.radShowAll)
        Me.gbLeaveInfo.Controls.Add(Me.radEmpWOAccrue)
        Me.gbLeaveInfo.Controls.Add(Me.lblAccrualDays)
        Me.gbLeaveInfo.Controls.Add(Me.EZeeLine1)
        Me.gbLeaveInfo.Controls.Add(Me.pnlAccrueSetting)
        Me.gbLeaveInfo.Controls.Add(Me.lblAccrueSetting)
        Me.gbLeaveInfo.Controls.Add(Me.objbtnSearchLeave2)
        Me.gbLeaveInfo.Controls.Add(Me.txtAccrualAmount)
        Me.gbLeaveInfo.Controls.Add(Me.lblStopDate)
        Me.gbLeaveInfo.Controls.Add(Me.lblStartDate)
        Me.gbLeaveInfo.Controls.Add(Me.dtpStopDate)
        Me.gbLeaveInfo.Controls.Add(Me.dtpStartDate)
        Me.gbLeaveInfo.Controls.Add(Me.txtBatchNo)
        Me.gbLeaveInfo.Controls.Add(Me.lblBatchNo)
        Me.gbLeaveInfo.Controls.Add(Me.cboLeaveCode)
        Me.gbLeaveInfo.Controls.Add(Me.lblLeaveCode)
        Me.gbLeaveInfo.Controls.Add(Me.LblMonthlyAccrue)
        Me.gbLeaveInfo.ExpandedHoverImage = Nothing
        Me.gbLeaveInfo.ExpandedNormalImage = Nothing
        Me.gbLeaveInfo.ExpandedPressedImage = Nothing
        Me.gbLeaveInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveInfo.HeaderHeight = 25
        Me.gbLeaveInfo.HeaderMessage = ""
        Me.gbLeaveInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveInfo.HeightOnCollapse = 0
        Me.gbLeaveInfo.LeftTextSpace = 0
        Me.gbLeaveInfo.Location = New System.Drawing.Point(4, 391)
        Me.gbLeaveInfo.Name = "gbLeaveInfo"
        Me.gbLeaveInfo.OpenHeight = 300
        Me.gbLeaveInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveInfo.ShowBorder = True
        Me.gbLeaveInfo.ShowCheckBox = False
        Me.gbLeaveInfo.ShowCollapseButton = False
        Me.gbLeaveInfo.ShowDefaultBorderColor = True
        Me.gbLeaveInfo.ShowDownButton = False
        Me.gbLeaveInfo.ShowHeader = True
        Me.gbLeaveInfo.Size = New System.Drawing.Size(778, 202)
        Me.gbLeaveInfo.TabIndex = 15
        Me.gbLeaveInfo.Temp = 0
        Me.gbLeaveInfo.Text = "Leave Info"
        Me.gbLeaveInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radShowAll
        '
        Me.radShowAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radShowAll.Location = New System.Drawing.Point(592, 33)
        Me.radShowAll.Name = "radShowAll"
        Me.radShowAll.Size = New System.Drawing.Size(176, 18)
        Me.radShowAll.TabIndex = 307
        Me.radShowAll.Text = "Show All"
        Me.radShowAll.UseVisualStyleBackColor = True
        '
        'radEmpWOAccrue
        '
        Me.radEmpWOAccrue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEmpWOAccrue.Location = New System.Drawing.Point(410, 33)
        Me.radEmpWOAccrue.Name = "radEmpWOAccrue"
        Me.radEmpWOAccrue.Size = New System.Drawing.Size(173, 18)
        Me.radEmpWOAccrue.TabIndex = 306
        Me.radEmpWOAccrue.Text = "Employees without Accrue "
        Me.radEmpWOAccrue.UseVisualStyleBackColor = True
        '
        'lblAccrualDays
        '
        Me.lblAccrualDays.BackColor = System.Drawing.Color.Transparent
        Me.lblAccrualDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccrualDays.Location = New System.Drawing.Point(222, 59)
        Me.lblAccrualDays.Name = "lblAccrualDays"
        Me.lblAccrualDays.Size = New System.Drawing.Size(81, 17)
        Me.lblAccrualDays.TabIndex = 267
        Me.lblAccrualDays.Text = "Yearly Accrue"
        Me.lblAccrualDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(2, 81)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(755, 4)
        Me.EZeeLine1.TabIndex = 298
        Me.EZeeLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAccrueSetting
        '
        Me.pnlAccrueSetting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlAccrueSetting.Controls.Add(Me.nudMaxNegativeDays)
        Me.pnlAccrueSetting.Controls.Add(Me.lblMaxNegativeLimit)
        Me.pnlAccrueSetting.Controls.Add(Me.radDonotIssueAsonDate)
        Me.pnlAccrueSetting.Controls.Add(Me.EZeeStraightLine2)
        Me.pnlAccrueSetting.Controls.Add(Me.radDonotIssue)
        Me.pnlAccrueSetting.Controls.Add(Me.radExceedBal)
        Me.pnlAccrueSetting.Controls.Add(Me.chkNoAction)
        Me.pnlAccrueSetting.Controls.Add(Me.radIssuebal)
        Me.pnlAccrueSetting.Controls.Add(Me.LblEligibilityAfter)
        Me.pnlAccrueSetting.Controls.Add(Me.txtCfAmount)
        Me.pnlAccrueSetting.Controls.Add(Me.txtEligibilityAfter)
        Me.pnlAccrueSetting.Controls.Add(Me.LblCFAmount)
        Me.pnlAccrueSetting.Location = New System.Drawing.Point(110, 90)
        Me.pnlAccrueSetting.Name = "pnlAccrueSetting"
        Me.pnlAccrueSetting.Size = New System.Drawing.Size(661, 106)
        Me.pnlAccrueSetting.TabIndex = 297
        '
        'nudMaxNegativeDays
        '
        Me.nudMaxNegativeDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMaxNegativeDays.Location = New System.Drawing.Point(272, 31)
        Me.nudMaxNegativeDays.Maximum = New Decimal(New Integer() {0, 0, 0, 0})
        Me.nudMaxNegativeDays.Minimum = New Decimal(New Integer() {999, 0, 0, -2147483648})
        Me.nudMaxNegativeDays.Name = "nudMaxNegativeDays"
        Me.nudMaxNegativeDays.Size = New System.Drawing.Size(48, 21)
        Me.nudMaxNegativeDays.TabIndex = 314
        Me.nudMaxNegativeDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaxNegativeLimit
        '
        Me.lblMaxNegativeLimit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxNegativeLimit.Location = New System.Drawing.Point(206, 8)
        Me.lblMaxNegativeLimit.Name = "lblMaxNegativeLimit"
        Me.lblMaxNegativeLimit.Size = New System.Drawing.Size(122, 17)
        Me.lblMaxNegativeLimit.TabIndex = 313
        Me.lblMaxNegativeLimit.Text = "Set Max. Negative Limit"
        Me.lblMaxNegativeLimit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radDonotIssueAsonDate
        '
        Me.radDonotIssueAsonDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDonotIssueAsonDate.Location = New System.Drawing.Point(3, 81)
        Me.radDonotIssueAsonDate.Name = "radDonotIssueAsonDate"
        Me.radDonotIssueAsonDate.Size = New System.Drawing.Size(320, 17)
        Me.radDonotIssueAsonDate.TabIndex = 312
        Me.radDonotIssueAsonDate.Text = "Don't Issue on Exceeding Leave Balance As On Date"
        Me.radDonotIssueAsonDate.UseVisualStyleBackColor = True
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(325, 2)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(8, 101)
        Me.EZeeStraightLine2.TabIndex = 311
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'radDonotIssue
        '
        Me.radDonotIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDonotIssue.Location = New System.Drawing.Point(3, 57)
        Me.radDonotIssue.Name = "radDonotIssue"
        Me.radDonotIssue.Size = New System.Drawing.Size(320, 17)
        Me.radDonotIssue.TabIndex = 3
        Me.radDonotIssue.Text = "Don't Issue on Exceeding Leave Balance On Total Balance"
        Me.radDonotIssue.UseVisualStyleBackColor = True
        '
        'radExceedBal
        '
        Me.radExceedBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExceedBal.Location = New System.Drawing.Point(3, 33)
        Me.radExceedBal.Name = "radExceedBal"
        Me.radExceedBal.Size = New System.Drawing.Size(195, 17)
        Me.radExceedBal.TabIndex = 2
        Me.radExceedBal.Text = "Exceeding Leave Bal. with unpaid"
        Me.radExceedBal.UseVisualStyleBackColor = True
        '
        'chkNoAction
        '
        Me.chkNoAction.BackColor = System.Drawing.Color.Transparent
        Me.chkNoAction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNoAction.Location = New System.Drawing.Point(339, 34)
        Me.chkNoAction.Name = "chkNoAction"
        Me.chkNoAction.Size = New System.Drawing.Size(291, 17)
        Me.chkNoAction.TabIndex = 295
        Me.chkNoAction.Text = "Carry Forward Existing Balance to Next Leave Cycle"
        Me.chkNoAction.UseVisualStyleBackColor = False
        '
        'radIssuebal
        '
        Me.radIssuebal.Checked = True
        Me.radIssuebal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radIssuebal.Location = New System.Drawing.Point(3, 8)
        Me.radIssuebal.Name = "radIssuebal"
        Me.radIssuebal.Size = New System.Drawing.Size(195, 17)
        Me.radIssuebal.TabIndex = 1
        Me.radIssuebal.TabStop = True
        Me.radIssuebal.Text = "Issue Balance with Paid"
        Me.radIssuebal.UseVisualStyleBackColor = True
        '
        'LblEligibilityAfter
        '
        Me.LblEligibilityAfter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEligibilityAfter.Location = New System.Drawing.Point(339, 8)
        Me.LblEligibilityAfter.Name = "LblEligibilityAfter"
        Me.LblEligibilityAfter.Size = New System.Drawing.Size(225, 16)
        Me.LblEligibilityAfter.TabIndex = 293
        Me.LblEligibilityAfter.Text = "Eligibility After (In Days)"
        Me.LblEligibilityAfter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCfAmount
        '
        Me.txtCfAmount.AllowNegative = False
        Me.txtCfAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCfAmount.DigitsInGroup = 0
        Me.txtCfAmount.Flags = 65536
        Me.txtCfAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCfAmount.Location = New System.Drawing.Point(573, 55)
        Me.txtCfAmount.MaxDecimalPlaces = 2
        Me.txtCfAmount.MaxWholeDigits = 4
        Me.txtCfAmount.Name = "txtCfAmount"
        Me.txtCfAmount.Prefix = ""
        Me.txtCfAmount.RangeMax = 1.7976931348623157E+308
        Me.txtCfAmount.RangeMin = -1.7976931348623157E+308
        Me.txtCfAmount.Size = New System.Drawing.Size(82, 21)
        Me.txtCfAmount.TabIndex = 291
        Me.txtCfAmount.Text = "0"
        Me.txtCfAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtEligibilityAfter
        '
        Me.txtEligibilityAfter.AllowNegative = False
        Me.txtEligibilityAfter.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtEligibilityAfter.DigitsInGroup = 0
        Me.txtEligibilityAfter.Flags = 65536
        Me.txtEligibilityAfter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEligibilityAfter.Location = New System.Drawing.Point(573, 6)
        Me.txtEligibilityAfter.MaxDecimalPlaces = 0
        Me.txtEligibilityAfter.MaxWholeDigits = 3
        Me.txtEligibilityAfter.Name = "txtEligibilityAfter"
        Me.txtEligibilityAfter.Prefix = ""
        Me.txtEligibilityAfter.RangeMax = 1.7976931348623157E+308
        Me.txtEligibilityAfter.RangeMin = -1.7976931348623157E+308
        Me.txtEligibilityAfter.Size = New System.Drawing.Size(82, 21)
        Me.txtEligibilityAfter.TabIndex = 290
        Me.txtEligibilityAfter.Text = "0"
        Me.txtEligibilityAfter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblCFAmount
        '
        Me.LblCFAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCFAmount.Location = New System.Drawing.Point(339, 57)
        Me.LblCFAmount.Name = "LblCFAmount"
        Me.LblCFAmount.Size = New System.Drawing.Size(225, 16)
        Me.LblCFAmount.TabIndex = 292
        Me.LblCFAmount.Text = "Max Days to be C/F to Next Leave Cycle"
        Me.LblCFAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAccrueSetting
        '
        Me.lblAccrueSetting.BackColor = System.Drawing.Color.Transparent
        Me.lblAccrueSetting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccrueSetting.Location = New System.Drawing.Point(6, 93)
        Me.lblAccrueSetting.Name = "lblAccrueSetting"
        Me.lblAccrueSetting.Size = New System.Drawing.Size(97, 15)
        Me.lblAccrueSetting.TabIndex = 296
        Me.lblAccrueSetting.Text = "Accrue Settings"
        '
        'objbtnSearchLeave2
        '
        Me.objbtnSearchLeave2.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave2.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave2.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeave2.BorderSelected = False
        Me.objbtnSearchLeave2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeave2.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeave2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeave2.Location = New System.Drawing.Point(383, 30)
        Me.objbtnSearchLeave2.Name = "objbtnSearchLeave2"
        Me.objbtnSearchLeave2.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeave2.TabIndex = 271
        '
        'txtAccrualAmount
        '
        Me.txtAccrualAmount.AllowNegative = False
        Me.txtAccrualAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAccrualAmount.DigitsInGroup = 0
        Me.txtAccrualAmount.Flags = 65536
        Me.txtAccrualAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAccrualAmount.Location = New System.Drawing.Point(316, 57)
        Me.txtAccrualAmount.MaxDecimalPlaces = 2
        Me.txtAccrualAmount.MaxWholeDigits = 21
        Me.txtAccrualAmount.Name = "txtAccrualAmount"
        Me.txtAccrualAmount.Prefix = ""
        Me.txtAccrualAmount.RangeMax = 2147483647
        Me.txtAccrualAmount.RangeMin = -2147483648
        Me.txtAccrualAmount.Size = New System.Drawing.Size(61, 21)
        Me.txtAccrualAmount.TabIndex = 8
        Me.txtAccrualAmount.Text = "0"
        Me.txtAccrualAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblStopDate
        '
        Me.lblStopDate.BackColor = System.Drawing.Color.Transparent
        Me.lblStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStopDate.Location = New System.Drawing.Point(592, 60)
        Me.lblStopDate.Name = "lblStopDate"
        Me.lblStopDate.Size = New System.Drawing.Size(68, 15)
        Me.lblStopDate.TabIndex = 265
        Me.lblStopDate.Text = "Stop Date"
        Me.lblStopDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.BackColor = System.Drawing.Color.Transparent
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(410, 60)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(65, 15)
        Me.lblStartDate.TabIndex = 263
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStopDate
        '
        Me.dtpStopDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Checked = False
        Me.dtpStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStopDate.Location = New System.Drawing.Point(663, 57)
        Me.dtpStopDate.Name = "dtpStopDate"
        Me.dtpStopDate.ShowCheckBox = True
        Me.dtpStopDate.Size = New System.Drawing.Size(105, 21)
        Me.dtpStopDate.TabIndex = 7
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(478, 57)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(105, 21)
        Me.dtpStartDate.TabIndex = 6
        '
        'txtBatchNo
        '
        Me.txtBatchNo.Flags = 0
        Me.txtBatchNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBatchNo.Location = New System.Drawing.Point(94, 57)
        Me.txtBatchNo.Name = "txtBatchNo"
        Me.txtBatchNo.Size = New System.Drawing.Size(115, 21)
        Me.txtBatchNo.TabIndex = 5
        '
        'lblBatchNo
        '
        Me.lblBatchNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchNo.Location = New System.Drawing.Point(8, 59)
        Me.lblBatchNo.Name = "lblBatchNo"
        Me.lblBatchNo.Size = New System.Drawing.Size(80, 17)
        Me.lblBatchNo.TabIndex = 269
        Me.lblBatchNo.Text = "Batch No."
        Me.lblBatchNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLeaveCode
        '
        Me.cboLeaveCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveCode.DropDownWidth = 300
        Me.cboLeaveCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveCode.FormattingEnabled = True
        Me.cboLeaveCode.Location = New System.Drawing.Point(94, 30)
        Me.cboLeaveCode.Name = "cboLeaveCode"
        Me.cboLeaveCode.Size = New System.Drawing.Size(283, 21)
        Me.cboLeaveCode.TabIndex = 4
        '
        'lblLeaveCode
        '
        Me.lblLeaveCode.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveCode.Location = New System.Drawing.Point(8, 32)
        Me.lblLeaveCode.Name = "lblLeaveCode"
        Me.lblLeaveCode.Size = New System.Drawing.Size(80, 17)
        Me.lblLeaveCode.TabIndex = 230
        Me.lblLeaveCode.Text = "Leave Type"
        Me.lblLeaveCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMonthlyAccrue
        '
        Me.LblMonthlyAccrue.BackColor = System.Drawing.Color.Transparent
        Me.LblMonthlyAccrue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMonthlyAccrue.Location = New System.Drawing.Point(220, 59)
        Me.LblMonthlyAccrue.Name = "LblMonthlyAccrue"
        Me.LblMonthlyAccrue.Size = New System.Drawing.Size(84, 17)
        Me.LblMonthlyAccrue.TabIndex = 312
        Me.LblMonthlyAccrue.Text = "Monthly Accrue"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnAssign)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 597)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(794, 51)
        Me.objFooter.TabIndex = 20
        '
        'btnAssign
        '
        Me.btnAssign.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAssign.BackColor = System.Drawing.Color.White
        Me.btnAssign.BackgroundImage = CType(resources.GetObject("btnAssign.BackgroundImage"), System.Drawing.Image)
        Me.btnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAssign.BorderColor = System.Drawing.Color.Empty
        Me.btnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAssign.FlatAppearance.BorderSize = 0
        Me.btnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAssign.ForeColor = System.Drawing.Color.Black
        Me.btnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Location = New System.Drawing.Point(582, 9)
        Me.btnAssign.Name = "btnAssign"
        Me.btnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Size = New System.Drawing.Size(97, 30)
        Me.btnAssign.TabIndex = 9
        Me.btnAssign.Text = "A&ssign"
        Me.btnAssign.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(685, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 10
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.MinimumWidth = 175
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn3.HeaderText = "Appointment Date"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.Width = 125
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.HeaderText = "Job Title"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'frmGlobalLeaveAssign
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(794, 648)
        Me.Controls.Add(Me.pnlTraningYear)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalLeaveAssign"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Leave Global Assign"
        Me.pnlTraningYear.ResumeLayout(False)
        Me.pnlTraningYear.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.pnlAppDate.ResumeLayout(False)
        Me.pnlYear.ResumeLayout(False)
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFliter.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbLeaveInfo.ResumeLayout(False)
        Me.gbLeaveInfo.PerformLayout()
        Me.pnlAccrueSetting.ResumeLayout(False)
        Me.pnlAccrueSetting.PerformLayout()
        CType(Me.nudMaxNegativeDays, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTraningYear As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbLeaveInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboLeaveCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveCode As System.Windows.Forms.Label
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpStopDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStopDate As System.Windows.Forms.Label
    Friend WithEvents lblAccrualDays As System.Windows.Forms.Label
    Friend WithEvents txtBatchNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBatchNo As System.Windows.Forms.Label
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents radExceedBal As System.Windows.Forms.RadioButton
    Friend WithEvents radIssuebal As System.Windows.Forms.RadioButton
    Friend WithEvents txtAccrualAmount As eZee.TextBox.IntegerTextBox
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents nudFromYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblFromYear As System.Windows.Forms.Label
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents TxtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LblValue As System.Windows.Forms.Label
    Friend WithEvents LblFrom As System.Windows.Forms.Label
    Friend WithEvents nudFromMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblMonth As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nudToMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblToMonth As System.Windows.Forms.Label
    Friend WithEvents nudToYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblToYear As System.Windows.Forms.Label
    Friend WithEvents cboFromcondition As System.Windows.Forms.ComboBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboTocondition As System.Windows.Forms.ComboBox
    Friend WithEvents rdNewEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents dtpDueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblDueDate As System.Windows.Forms.Label
    Friend WithEvents rdDueDate As System.Windows.Forms.RadioButton
    Friend WithEvents cboELCLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents LblELCLeaveType As System.Windows.Forms.Label
    Friend WithEvents eline1 As eZee.Common.eZeeLine
    Friend WithEvents pnlFliter As System.Windows.Forms.Panel
    Friend WithEvents radExpYear As System.Windows.Forms.RadioButton
    Friend WithEvents radAppointedDate As System.Windows.Forms.RadioButton
    Friend WithEvents objbtnSearchLeave1 As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlYear As System.Windows.Forms.Panel
    Friend WithEvents pnlAppDate As System.Windows.Forms.Panel
    Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchLeave2 As eZee.Common.eZeeGradientButton
    Friend WithEvents chkNoAction As System.Windows.Forms.CheckBox
    Friend WithEvents txtEligibilityAfter As eZee.TextBox.NumericTextBox
    Friend WithEvents LblEligibilityAfter As System.Windows.Forms.Label
    Friend WithEvents txtCfAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents LblCFAmount As System.Windows.Forms.Label
    Friend WithEvents pnlAccrueSetting As System.Windows.Forms.Panel
    Friend WithEvents radDonotIssue As System.Windows.Forms.RadioButton
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents lblAccrueSetting As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents LblEmployeeCount As System.Windows.Forms.Label
    Friend WithEvents radProbationDate As System.Windows.Forms.RadioButton
    Friend WithEvents radConfirmationDate As System.Windows.Forms.RadioButton
    Friend WithEvents radShowAll As System.Windows.Forms.RadioButton
    Friend WithEvents radEmpWOAccrue As System.Windows.Forms.RadioButton
    Friend WithEvents LblMonthlyAccrue As System.Windows.Forms.Label
    Friend WithEvents radDonotIssueAsonDate As System.Windows.Forms.RadioButton
    Friend WithEvents objSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgColhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhAppointdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJobTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAppliedLeave As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhApprovedLeave As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhIssuedLeave As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhRemaining_Bal As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents nudMaxNegativeDays As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMaxNegativeLimit As System.Windows.Forms.Label
End Class
