﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalLvFreq_Assign
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalLvFreq_Assign))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.LblEmployeeCount = New System.Windows.Forms.Label
        Me.LblValue = New System.Windows.Forms.Label
        Me.LblFrom = New System.Windows.Forms.Label
        Me.gbFrequencyFactors = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.LblTenureMonths = New System.Windows.Forms.Label
        Me.nudTenureMonths = New System.Windows.Forms.NumericUpDown
        Me.chkLifeTime = New System.Windows.Forms.CheckBox
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.txtMaxamount = New eZee.TextBox.NumericTextBox
        Me.lblMaxAmount = New System.Windows.Forms.Label
        Me.txtDuration = New eZee.TextBox.NumericTextBox
        Me.lbltimes = New System.Windows.Forms.Label
        Me.lblDuration = New System.Windows.Forms.Label
        Me.nudDays = New System.Windows.Forms.NumericUpDown
        Me.txtEligibilityAfter = New eZee.TextBox.NumericTextBox
        Me.LblEligibilityAfter = New System.Windows.Forms.Label
        Me.objbtnSearchLeave = New eZee.Common.eZeeGradientButton
        Me.cboLeaveCode = New System.Windows.Forms.ComboBox
        Me.lblLeaveCode = New System.Windows.Forms.Label
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radExpYear = New System.Windows.Forms.RadioButton
        Me.radProbationDate = New System.Windows.Forms.RadioButton
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.radConfirmationDate = New System.Windows.Forms.RadioButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.radAppointedDate = New System.Windows.Forms.RadioButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.pnlDate = New System.Windows.Forms.Panel
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpDate1 = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpDate2 = New System.Windows.Forms.DateTimePicker
        Me.pnlYear = New System.Windows.Forms.Panel
        Me.lblFromYear = New System.Windows.Forms.Label
        Me.nudToYear = New System.Windows.Forms.NumericUpDown
        Me.LblToYear = New System.Windows.Forms.Label
        Me.nudFromMonth = New System.Windows.Forms.NumericUpDown
        Me.LblToMonth = New System.Windows.Forms.Label
        Me.LblMonth = New System.Windows.Forms.Label
        Me.nudToMonth = New System.Windows.Forms.NumericUpDown
        Me.cboTocondition = New System.Windows.Forms.ComboBox
        Me.nudFromYear = New System.Windows.Forms.NumericUpDown
        Me.cboFromcondition = New System.Windows.Forms.ComboBox
        Me.TxtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgColhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhAppointdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJobTitle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhConfirmationDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.radLifetime = New System.Windows.Forms.RadioButton
        Me.radFinELC = New System.Windows.Forms.RadioButton
        Me.pnlMain.SuspendLayout()
        Me.gbFrequencyFactors.SuspendLayout()
        CType(Me.nudTenureMonths, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDays, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDate.SuspendLayout()
        Me.pnlYear.SuspendLayout()
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.LblEmployeeCount)
        Me.pnlMain.Controls.Add(Me.LblValue)
        Me.pnlMain.Controls.Add(Me.LblFrom)
        Me.pnlMain.Controls.Add(Me.gbFrequencyFactors)
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Controls.Add(Me.TxtSearch)
        Me.pnlMain.Controls.Add(Me.chkSelectAll)
        Me.pnlMain.Controls.Add(Me.dgEmployee)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(697, 516)
        Me.pnlMain.TabIndex = 0
        '
        'LblEmployeeCount
        '
        Me.LblEmployeeCount.BackColor = System.Drawing.Color.Transparent
        Me.LblEmployeeCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmployeeCount.Location = New System.Drawing.Point(515, 217)
        Me.LblEmployeeCount.Name = "LblEmployeeCount"
        Me.LblEmployeeCount.Size = New System.Drawing.Size(175, 21)
        Me.LblEmployeeCount.TabIndex = 296
        Me.LblEmployeeCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblValue
        '
        Me.LblValue.BackColor = System.Drawing.Color.Transparent
        Me.LblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblValue.Location = New System.Drawing.Point(275, 217)
        Me.LblValue.Name = "LblValue"
        Me.LblValue.Size = New System.Drawing.Size(230, 21)
        Me.LblValue.TabIndex = 295
        Me.LblValue.Text = "#Value"
        Me.LblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblFrom
        '
        Me.LblFrom.BackColor = System.Drawing.Color.Transparent
        Me.LblFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFrom.Location = New System.Drawing.Point(221, 217)
        Me.LblFrom.Name = "LblFrom"
        Me.LblFrom.Size = New System.Drawing.Size(44, 21)
        Me.LblFrom.TabIndex = 294
        Me.LblFrom.Text = "From"
        Me.LblFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbFrequencyFactors
        '
        Me.gbFrequencyFactors.BorderColor = System.Drawing.Color.Black
        Me.gbFrequencyFactors.Checked = False
        Me.gbFrequencyFactors.CollapseAllExceptThis = False
        Me.gbFrequencyFactors.CollapsedHoverImage = Nothing
        Me.gbFrequencyFactors.CollapsedNormalImage = Nothing
        Me.gbFrequencyFactors.CollapsedPressedImage = Nothing
        Me.gbFrequencyFactors.CollapseOnLoad = False
        Me.gbFrequencyFactors.Controls.Add(Me.LblTenureMonths)
        Me.gbFrequencyFactors.Controls.Add(Me.nudTenureMonths)
        Me.gbFrequencyFactors.Controls.Add(Me.chkLifeTime)
        Me.gbFrequencyFactors.Controls.Add(Me.EZeeStraightLine2)
        Me.gbFrequencyFactors.Controls.Add(Me.txtMaxamount)
        Me.gbFrequencyFactors.Controls.Add(Me.lblMaxAmount)
        Me.gbFrequencyFactors.Controls.Add(Me.txtDuration)
        Me.gbFrequencyFactors.Controls.Add(Me.lbltimes)
        Me.gbFrequencyFactors.Controls.Add(Me.lblDuration)
        Me.gbFrequencyFactors.Controls.Add(Me.nudDays)
        Me.gbFrequencyFactors.Controls.Add(Me.txtEligibilityAfter)
        Me.gbFrequencyFactors.Controls.Add(Me.LblEligibilityAfter)
        Me.gbFrequencyFactors.Controls.Add(Me.objbtnSearchLeave)
        Me.gbFrequencyFactors.Controls.Add(Me.cboLeaveCode)
        Me.gbFrequencyFactors.Controls.Add(Me.lblLeaveCode)
        Me.gbFrequencyFactors.ExpandedHoverImage = Nothing
        Me.gbFrequencyFactors.ExpandedNormalImage = Nothing
        Me.gbFrequencyFactors.ExpandedPressedImage = Nothing
        Me.gbFrequencyFactors.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFrequencyFactors.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFrequencyFactors.HeaderHeight = 25
        Me.gbFrequencyFactors.HeaderMessage = ""
        Me.gbFrequencyFactors.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFrequencyFactors.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFrequencyFactors.HeightOnCollapse = 0
        Me.gbFrequencyFactors.LeftTextSpace = 0
        Me.gbFrequencyFactors.Location = New System.Drawing.Point(3, 124)
        Me.gbFrequencyFactors.Name = "gbFrequencyFactors"
        Me.gbFrequencyFactors.OpenHeight = 300
        Me.gbFrequencyFactors.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFrequencyFactors.ShowBorder = True
        Me.gbFrequencyFactors.ShowCheckBox = False
        Me.gbFrequencyFactors.ShowCollapseButton = False
        Me.gbFrequencyFactors.ShowDefaultBorderColor = True
        Me.gbFrequencyFactors.ShowDownButton = False
        Me.gbFrequencyFactors.ShowHeader = True
        Me.gbFrequencyFactors.Size = New System.Drawing.Size(692, 90)
        Me.gbFrequencyFactors.TabIndex = 22
        Me.gbFrequencyFactors.Temp = 0
        Me.gbFrequencyFactors.Text = "Leave Frequency"
        Me.gbFrequencyFactors.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblTenureMonths
        '
        Me.LblTenureMonths.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTenureMonths.Location = New System.Drawing.Point(537, 33)
        Me.LblTenureMonths.Name = "LblTenureMonths"
        Me.LblTenureMonths.Size = New System.Drawing.Size(88, 17)
        Me.LblTenureMonths.TabIndex = 315
        Me.LblTenureMonths.Text = "Tenure [Months]"
        Me.LblTenureMonths.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudTenureMonths
        '
        Me.nudTenureMonths.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudTenureMonths.Location = New System.Drawing.Point(627, 31)
        Me.nudTenureMonths.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nudTenureMonths.Name = "nudTenureMonths"
        Me.nudTenureMonths.Size = New System.Drawing.Size(56, 21)
        Me.nudTenureMonths.TabIndex = 314
        Me.nudTenureMonths.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkLifeTime
        '
        Me.chkLifeTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLifeTime.Location = New System.Drawing.Point(375, 33)
        Me.chkLifeTime.Name = "chkLifeTime"
        Me.chkLifeTime.Size = New System.Drawing.Size(158, 17)
        Me.chkLifeTime.TabIndex = 312
        Me.chkLifeTime.Text = "Life Time"
        Me.chkLifeTime.UseVisualStyleBackColor = True
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(365, 24)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(8, 66)
        Me.EZeeStraightLine2.TabIndex = 310
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'txtMaxamount
        '
        Me.txtMaxamount.AllowNegative = False
        Me.txtMaxamount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMaxamount.DigitsInGroup = 0
        Me.txtMaxamount.Flags = 65536
        Me.txtMaxamount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxamount.Location = New System.Drawing.Point(82, 60)
        Me.txtMaxamount.MaxDecimalPlaces = 0
        Me.txtMaxamount.MaxWholeDigits = 5
        Me.txtMaxamount.Name = "txtMaxamount"
        Me.txtMaxamount.Prefix = ""
        Me.txtMaxamount.RangeMax = 1.7976931348623157E+308
        Me.txtMaxamount.RangeMin = -1.7976931348623157E+308
        Me.txtMaxamount.Size = New System.Drawing.Size(51, 21)
        Me.txtMaxamount.TabIndex = 308
        Me.txtMaxamount.Text = "0"
        Me.txtMaxamount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaxAmount
        '
        Me.lblMaxAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxAmount.Location = New System.Drawing.Point(8, 57)
        Me.lblMaxAmount.Name = "lblMaxAmount"
        Me.lblMaxAmount.Size = New System.Drawing.Size(68, 26)
        Me.lblMaxAmount.TabIndex = 309
        Me.lblMaxAmount.Text = "Balance Max.Amount"
        Me.lblMaxAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDuration
        '
        Me.txtDuration.AllowNegative = False
        Me.txtDuration.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDuration.DigitsInGroup = 0
        Me.txtDuration.Flags = 65536
        Me.txtDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDuration.Location = New System.Drawing.Point(479, 60)
        Me.txtDuration.MaxDecimalPlaces = 0
        Me.txtDuration.MaxWholeDigits = 3
        Me.txtDuration.Name = "txtDuration"
        Me.txtDuration.Prefix = ""
        Me.txtDuration.RangeMax = 1.7976931348623157E+308
        Me.txtDuration.RangeMin = -1.7976931348623157E+308
        Me.txtDuration.Size = New System.Drawing.Size(53, 21)
        Me.txtDuration.TabIndex = 298
        Me.txtDuration.Text = "0"
        Me.txtDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbltimes
        '
        Me.lbltimes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltimes.Location = New System.Drawing.Point(537, 62)
        Me.lbltimes.Name = "lbltimes"
        Me.lbltimes.Size = New System.Drawing.Size(88, 17)
        Me.lbltimes.TabIndex = 303
        Me.lbltimes.Text = "Occurrence"
        Me.lbltimes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDuration
        '
        Me.lblDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDuration.Location = New System.Drawing.Point(375, 62)
        Me.lblDuration.Name = "lblDuration"
        Me.lblDuration.Size = New System.Drawing.Size(100, 17)
        Me.lblDuration.TabIndex = 299
        Me.lblDuration.Text = "Consecutive days"
        Me.lblDuration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudDays
        '
        Me.nudDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDays.Location = New System.Drawing.Point(627, 60)
        Me.nudDays.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nudDays.Name = "nudDays"
        Me.nudDays.Size = New System.Drawing.Size(56, 21)
        Me.nudDays.TabIndex = 302
        Me.nudDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtEligibilityAfter
        '
        Me.txtEligibilityAfter.AllowNegative = False
        Me.txtEligibilityAfter.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtEligibilityAfter.DigitsInGroup = 0
        Me.txtEligibilityAfter.Flags = 65536
        Me.txtEligibilityAfter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEligibilityAfter.Location = New System.Drawing.Point(272, 60)
        Me.txtEligibilityAfter.MaxDecimalPlaces = 0
        Me.txtEligibilityAfter.MaxWholeDigits = 3
        Me.txtEligibilityAfter.Name = "txtEligibilityAfter"
        Me.txtEligibilityAfter.Prefix = ""
        Me.txtEligibilityAfter.RangeMax = 1.7976931348623157E+308
        Me.txtEligibilityAfter.RangeMin = -1.7976931348623157E+308
        Me.txtEligibilityAfter.Size = New System.Drawing.Size(60, 21)
        Me.txtEligibilityAfter.TabIndex = 295
        Me.txtEligibilityAfter.Text = "0"
        Me.txtEligibilityAfter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblEligibilityAfter
        '
        Me.LblEligibilityAfter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEligibilityAfter.Location = New System.Drawing.Point(139, 62)
        Me.LblEligibilityAfter.Name = "LblEligibilityAfter"
        Me.LblEligibilityAfter.Size = New System.Drawing.Size(127, 17)
        Me.LblEligibilityAfter.TabIndex = 296
        Me.LblEligibilityAfter.Text = "Eligibility After (In Days)"
        Me.LblEligibilityAfter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchLeave
        '
        Me.objbtnSearchLeave.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLeave.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLeave.BorderSelected = False
        Me.objbtnSearchLeave.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLeave.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLeave.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLeave.Location = New System.Drawing.Point(338, 31)
        Me.objbtnSearchLeave.Name = "objbtnSearchLeave"
        Me.objbtnSearchLeave.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLeave.TabIndex = 274
        '
        'cboLeaveCode
        '
        Me.cboLeaveCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveCode.DropDownWidth = 300
        Me.cboLeaveCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveCode.FormattingEnabled = True
        Me.cboLeaveCode.Location = New System.Drawing.Point(82, 31)
        Me.cboLeaveCode.Name = "cboLeaveCode"
        Me.cboLeaveCode.Size = New System.Drawing.Size(250, 21)
        Me.cboLeaveCode.TabIndex = 272
        '
        'lblLeaveCode
        '
        Me.lblLeaveCode.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveCode.Location = New System.Drawing.Point(8, 33)
        Me.lblLeaveCode.Name = "lblLeaveCode"
        Me.lblLeaveCode.Size = New System.Drawing.Size(68, 17)
        Me.lblLeaveCode.TabIndex = 273
        Me.lblLeaveCode.Text = "Leave Type"
        Me.lblLeaveCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.radExpYear)
        Me.gbFilterCriteria.Controls.Add(Me.radProbationDate)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.radConfirmationDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.radAppointedDate)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.pnlDate)
        Me.gbFilterCriteria.Controls.Add(Me.pnlYear)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(3, 2)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(691, 119)
        Me.gbFilterCriteria.TabIndex = 286
        Me.gbFilterCriteria.TabStop = True
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radExpYear
        '
        Me.radExpYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExpYear.Location = New System.Drawing.Point(12, 34)
        Me.radExpYear.Name = "radExpYear"
        Me.radExpYear.Size = New System.Drawing.Size(117, 16)
        Me.radExpYear.TabIndex = 303
        Me.radExpYear.TabStop = True
        Me.radExpYear.Text = "Year of Experience"
        Me.radExpYear.UseVisualStyleBackColor = True
        '
        'radProbationDate
        '
        Me.radProbationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radProbationDate.Location = New System.Drawing.Point(279, 34)
        Me.radProbationDate.Name = "radProbationDate"
        Me.radProbationDate.Size = New System.Drawing.Size(117, 16)
        Me.radProbationDate.TabIndex = 309
        Me.radProbationDate.TabStop = True
        Me.radProbationDate.Text = "Probation Date"
        Me.radProbationDate.UseVisualStyleBackColor = True
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(560, 4)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(79, 17)
        Me.lnkAllocation.TabIndex = 307
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'radConfirmationDate
        '
        Me.radConfirmationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radConfirmationDate.Location = New System.Drawing.Point(420, 34)
        Me.radConfirmationDate.Name = "radConfirmationDate"
        Me.radConfirmationDate.Size = New System.Drawing.Size(117, 16)
        Me.radConfirmationDate.TabIndex = 310
        Me.radConfirmationDate.TabStop = True
        Me.radConfirmationDate.Text = "Confirmation Date"
        Me.radConfirmationDate.UseVisualStyleBackColor = True
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(665, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 306
        Me.objbtnReset.TabStop = False
        '
        'radAppointedDate
        '
        Me.radAppointedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAppointedDate.Location = New System.Drawing.Point(142, 34)
        Me.radAppointedDate.Name = "radAppointedDate"
        Me.radAppointedDate.Size = New System.Drawing.Size(117, 16)
        Me.radAppointedDate.TabIndex = 1
        Me.radAppointedDate.TabStop = True
        Me.radAppointedDate.Text = "Appointment Date"
        Me.radAppointedDate.UseVisualStyleBackColor = True
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(642, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 305
        Me.objbtnSearch.TabStop = False
        '
        'pnlDate
        '
        Me.pnlDate.Controls.Add(Me.lblToDate)
        Me.pnlDate.Controls.Add(Me.dtpDate1)
        Me.pnlDate.Controls.Add(Me.lblFromDate)
        Me.pnlDate.Controls.Add(Me.dtpDate2)
        Me.pnlDate.Location = New System.Drawing.Point(348, 58)
        Me.pnlDate.Name = "pnlDate"
        Me.pnlDate.Size = New System.Drawing.Size(324, 26)
        Me.pnlDate.TabIndex = 298
        '
        'lblToDate
        '
        Me.lblToDate.BackColor = System.Drawing.Color.Transparent
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(162, 5)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(58, 15)
        Me.lblToDate.TabIndex = 305
        Me.lblToDate.Text = "To Date"
        Me.lblToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate1
        '
        Me.dtpDate1.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate1.Location = New System.Drawing.Point(69, 2)
        Me.dtpDate1.Name = "dtpDate1"
        Me.dtpDate1.Size = New System.Drawing.Size(87, 21)
        Me.dtpDate1.TabIndex = 1
        '
        'lblFromDate
        '
        Me.lblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(6, 5)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(62, 15)
        Me.lblFromDate.TabIndex = 304
        Me.lblFromDate.Text = "From Date"
        Me.lblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate2
        '
        Me.dtpDate2.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate2.Location = New System.Drawing.Point(234, 3)
        Me.dtpDate2.Name = "dtpDate2"
        Me.dtpDate2.Size = New System.Drawing.Size(87, 21)
        Me.dtpDate2.TabIndex = 304
        '
        'pnlYear
        '
        Me.pnlYear.Controls.Add(Me.lblFromYear)
        Me.pnlYear.Controls.Add(Me.nudToYear)
        Me.pnlYear.Controls.Add(Me.LblToYear)
        Me.pnlYear.Controls.Add(Me.nudFromMonth)
        Me.pnlYear.Controls.Add(Me.LblToMonth)
        Me.pnlYear.Controls.Add(Me.LblMonth)
        Me.pnlYear.Controls.Add(Me.nudToMonth)
        Me.pnlYear.Controls.Add(Me.cboTocondition)
        Me.pnlYear.Controls.Add(Me.nudFromYear)
        Me.pnlYear.Controls.Add(Me.cboFromcondition)
        Me.pnlYear.Location = New System.Drawing.Point(7, 59)
        Me.pnlYear.Name = "pnlYear"
        Me.pnlYear.Size = New System.Drawing.Size(334, 52)
        Me.pnlYear.TabIndex = 303
        '
        'lblFromYear
        '
        Me.lblFromYear.BackColor = System.Drawing.Color.Transparent
        Me.lblFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromYear.Location = New System.Drawing.Point(3, 5)
        Me.lblFromYear.Name = "lblFromYear"
        Me.lblFromYear.Size = New System.Drawing.Size(57, 15)
        Me.lblFromYear.TabIndex = 279
        Me.lblFromYear.Text = "From Year"
        Me.lblFromYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudToYear
        '
        Me.nudToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToYear.Location = New System.Drawing.Point(67, 29)
        Me.nudToYear.Name = "nudToYear"
        Me.nudToYear.Size = New System.Drawing.Size(53, 21)
        Me.nudToYear.TabIndex = 292
        Me.nudToYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblToYear
        '
        Me.LblToYear.BackColor = System.Drawing.Color.Transparent
        Me.LblToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToYear.Location = New System.Drawing.Point(3, 32)
        Me.LblToYear.Name = "LblToYear"
        Me.LblToYear.Size = New System.Drawing.Size(57, 15)
        Me.LblToYear.TabIndex = 291
        Me.LblToYear.Text = "To Year"
        Me.LblToYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudFromMonth
        '
        Me.nudFromMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromMonth.Location = New System.Drawing.Point(203, 2)
        Me.nudFromMonth.Maximum = New Decimal(New Integer() {11, 0, 0, 0})
        Me.nudFromMonth.Name = "nudFromMonth"
        Me.nudFromMonth.Size = New System.Drawing.Size(57, 21)
        Me.nudFromMonth.TabIndex = 289
        Me.nudFromMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblToMonth
        '
        Me.LblToMonth.BackColor = System.Drawing.Color.Transparent
        Me.LblToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToMonth.Location = New System.Drawing.Point(129, 32)
        Me.LblToMonth.Name = "LblToMonth"
        Me.LblToMonth.Size = New System.Drawing.Size(68, 15)
        Me.LblToMonth.TabIndex = 293
        Me.LblToMonth.Text = "To Month"
        Me.LblToMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMonth
        '
        Me.LblMonth.BackColor = System.Drawing.Color.Transparent
        Me.LblMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMonth.Location = New System.Drawing.Point(129, 5)
        Me.LblMonth.Name = "LblMonth"
        Me.LblMonth.Size = New System.Drawing.Size(68, 15)
        Me.LblMonth.TabIndex = 288
        Me.LblMonth.Text = "From Month"
        Me.LblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudToMonth
        '
        Me.nudToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToMonth.Location = New System.Drawing.Point(203, 29)
        Me.nudToMonth.Maximum = New Decimal(New Integer() {11, 0, 0, 0})
        Me.nudToMonth.Name = "nudToMonth"
        Me.nudToMonth.Size = New System.Drawing.Size(57, 21)
        Me.nudToMonth.TabIndex = 294
        Me.nudToMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboTocondition
        '
        Me.cboTocondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTocondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTocondition.FormattingEnabled = True
        Me.cboTocondition.Location = New System.Drawing.Point(266, 29)
        Me.cboTocondition.Name = "cboTocondition"
        Me.cboTocondition.Size = New System.Drawing.Size(63, 21)
        Me.cboTocondition.TabIndex = 297
        '
        'nudFromYear
        '
        Me.nudFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromYear.Location = New System.Drawing.Point(67, 2)
        Me.nudFromYear.Name = "nudFromYear"
        Me.nudFromYear.Size = New System.Drawing.Size(53, 21)
        Me.nudFromYear.TabIndex = 281
        Me.nudFromYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboFromcondition
        '
        Me.cboFromcondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromcondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromcondition.FormattingEnabled = True
        Me.cboFromcondition.Location = New System.Drawing.Point(266, 2)
        Me.cboFromcondition.Name = "cboFromcondition"
        Me.cboFromcondition.Size = New System.Drawing.Size(63, 21)
        Me.cboFromcondition.TabIndex = 295
        '
        'TxtSearch
        '
        Me.TxtSearch.Flags = 0
        Me.TxtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.TxtSearch.Location = New System.Drawing.Point(4, 217)
        Me.TxtSearch.Name = "TxtSearch"
        Me.TxtSearch.Size = New System.Drawing.Size(203, 21)
        Me.TxtSearch.TabIndex = 272
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(10, 249)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 284
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objSelect, Me.dgColhEmpCode, Me.dgColhEmployee, Me.dgColhAppointdate, Me.dgcolhJobTitle, Me.dgcolhConfirmationDate})
        Me.dgEmployee.Location = New System.Drawing.Point(2, 242)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgEmployee.Size = New System.Drawing.Size(692, 225)
        Me.dgEmployee.TabIndex = 285
        '
        'objSelect
        '
        Me.objSelect.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objSelect.HeaderText = ""
        Me.objSelect.Name = "objSelect"
        Me.objSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objSelect.Width = 25
        '
        'dgColhEmpCode
        '
        Me.dgColhEmpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmpCode.HeaderText = "Code"
        Me.dgColhEmpCode.Name = "dgColhEmpCode"
        Me.dgColhEmpCode.ReadOnly = True
        Me.dgColhEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhEmpCode.Width = 90
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhEmployee.HeaderText = "Employee"
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        '
        'dgColhAppointdate
        '
        Me.dgColhAppointdate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhAppointdate.HeaderText = "Appointment Date"
        Me.dgColhAppointdate.Name = "dgColhAppointdate"
        Me.dgColhAppointdate.ReadOnly = True
        Me.dgColhAppointdate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'dgcolhJobTitle
        '
        Me.dgcolhJobTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhJobTitle.HeaderText = "Job Title"
        Me.dgcolhJobTitle.Name = "dgcolhJobTitle"
        Me.dgcolhJobTitle.ReadOnly = True
        Me.dgcolhJobTitle.Width = 230
        '
        'dgcolhConfirmationDate
        '
        Me.dgcolhConfirmationDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhConfirmationDate.HeaderText = "Confirmation Date"
        Me.dgcolhConfirmationDate.Name = "dgcolhConfirmationDate"
        Me.dgcolhConfirmationDate.ReadOnly = True
        Me.dgcolhConfirmationDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhConfirmationDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhConfirmationDate.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnAssign)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.radLifetime)
        Me.objFooter.Controls.Add(Me.radFinELC)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 465)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(697, 51)
        Me.objFooter.TabIndex = 21
        '
        'btnAssign
        '
        Me.btnAssign.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAssign.BackColor = System.Drawing.Color.White
        Me.btnAssign.BackgroundImage = CType(resources.GetObject("btnAssign.BackgroundImage"), System.Drawing.Image)
        Me.btnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAssign.BorderColor = System.Drawing.Color.Empty
        Me.btnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAssign.FlatAppearance.BorderSize = 0
        Me.btnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAssign.ForeColor = System.Drawing.Color.Black
        Me.btnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Location = New System.Drawing.Point(485, 9)
        Me.btnAssign.Name = "btnAssign"
        Me.btnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Size = New System.Drawing.Size(97, 30)
        Me.btnAssign.TabIndex = 9
        Me.btnAssign.Text = "A&ssign"
        Me.btnAssign.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(588, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 10
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'radLifetime
        '
        Me.radLifetime.Checked = True
        Me.radLifetime.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLifetime.Location = New System.Drawing.Point(128, 16)
        Me.radLifetime.Name = "radLifetime"
        Me.radLifetime.Size = New System.Drawing.Size(82, 17)
        Me.radLifetime.TabIndex = 23
        Me.radLifetime.TabStop = True
        Me.radLifetime.Text = "Life Time "
        Me.radLifetime.UseVisualStyleBackColor = True
        Me.radLifetime.Visible = False
        '
        'radFinELC
        '
        Me.radFinELC.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radFinELC.Location = New System.Drawing.Point(216, 16)
        Me.radFinELC.Name = "radFinELC"
        Me.radFinELC.Size = New System.Drawing.Size(74, 17)
        Me.radFinELC.TabIndex = 301
        Me.radFinELC.TabStop = True
        Me.radFinELC.Text = "#value"
        Me.radFinELC.UseVisualStyleBackColor = True
        Me.radFinELC.Visible = False
        '
        'frmGlobalLvFreq_Assign
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(697, 516)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalLvFreq_Assign"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Leave Frequency"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.gbFrequencyFactors.ResumeLayout(False)
        Me.gbFrequencyFactors.PerformLayout()
        CType(Me.nudTenureMonths, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDays, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDate.ResumeLayout(False)
        Me.pnlYear.ResumeLayout(False)
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents TxtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents gbFrequencyFactors As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchLeave As eZee.Common.eZeeGradientButton
    Friend WithEvents cboLeaveCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveCode As System.Windows.Forms.Label
    Friend WithEvents txtDuration As eZee.TextBox.NumericTextBox
    Friend WithEvents lblDuration As System.Windows.Forms.Label
    Friend WithEvents txtEligibilityAfter As eZee.TextBox.NumericTextBox
    Friend WithEvents LblEligibilityAfter As System.Windows.Forms.Label
    Friend WithEvents lbltimes As System.Windows.Forms.Label
    Friend WithEvents nudDays As System.Windows.Forms.NumericUpDown
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents txtMaxamount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMaxAmount As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents chkLifeTime As System.Windows.Forms.CheckBox
    Friend WithEvents radLifetime As System.Windows.Forms.RadioButton
    Friend WithEvents radFinELC As System.Windows.Forms.RadioButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents radExpYear As System.Windows.Forms.RadioButton
    Friend WithEvents radProbationDate As System.Windows.Forms.RadioButton
    Friend WithEvents radConfirmationDate As System.Windows.Forms.RadioButton
    Friend WithEvents radAppointedDate As System.Windows.Forms.RadioButton
    Friend WithEvents pnlDate As System.Windows.Forms.Panel
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnlYear As System.Windows.Forms.Panel
    Friend WithEvents lblFromYear As System.Windows.Forms.Label
    Friend WithEvents nudToYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblToYear As System.Windows.Forms.Label
    Friend WithEvents nudFromMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblToMonth As System.Windows.Forms.Label
    Friend WithEvents LblMonth As System.Windows.Forms.Label
    Friend WithEvents nudToMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents cboTocondition As System.Windows.Forms.ComboBox
    Friend WithEvents nudFromYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents cboFromcondition As System.Windows.Forms.ComboBox
    Friend WithEvents LblEmployeeCount As System.Windows.Forms.Label
    Friend WithEvents LblValue As System.Windows.Forms.Label
    Friend WithEvents LblFrom As System.Windows.Forms.Label
    Friend WithEvents objSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgColhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhAppointdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJobTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhConfirmationDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LblTenureMonths As System.Windows.Forms.Label
    Friend WithEvents nudTenureMonths As System.Windows.Forms.NumericUpDown
End Class
