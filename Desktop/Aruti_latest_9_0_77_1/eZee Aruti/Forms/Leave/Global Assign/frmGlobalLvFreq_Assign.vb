﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGlobalLvFreq_Assign

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmGlobalLvFreq_Assign"
    Private menAction As enAction = enAction.ADD_ONE
    Private objLeaveBalance As clsleavebalance_tran
    Private dsEmployee As DataSet
    Private mdtEmployee As DataTable = Nothing
    Private mstrAdvanceFilter As String = ""
    Private dvEmployee As DataView

    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.
    Private mdtFromDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    Private mdtToDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    'Pinkal (30-Jun-2016) -- End


#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim dtFill As DataTable = Nothing
        Try
            dsFill = Nothing
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.GetList("LeaveType", True)
            dtFill = New DataView(dsFill.Tables("LeaveType"), "isshortleave = 1", "", DataViewRowState.CurrentRows).ToTable
            Dim drRow As DataRow
            drRow = dtFill.NewRow
            drRow("leavetypeunkid") = 0
            drRow("leavename") = Language.getMessage(mstrModuleName, 1, "Select")
            dtFill.Rows.InsertAt(drRow, 0)
            With cboLeaveCode
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "leavename"
                .DataSource = dtFill
                .SelectedValue = 0
            End With


            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.

            Dim objMaster As New clsMasterData
            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsFill = objMaster.GetCondition(True)
            dsFill = objMaster.GetCondition(True, True, False, False, False)
            'Nilay (10-Nov-2016) -- End
            cboFromcondition.DisplayMember = "Name"
            cboFromcondition.ValueMember = "id"
            cboFromcondition.DataSource = dsFill.Tables(0)

            cboTocondition.DisplayMember = "Name"
            cboTocondition.ValueMember = "id"
            cboTocondition.DataSource = dsFill.Tables(0).Copy

            objMaster = Nothing
            'Pinkal (30-Jun-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub setColor()
        Try
            cboLeaveCode.BackColor = GUI.ColorComp
            txtMaxamount.BackColor = GUI.ColorComp
            txtDuration.BackColor = GUI.ColorOptional
            txtEligibilityAfter.BackColor = GUI.ColorOptional
            TxtSearch.BackColor = GUI.ColorOptional
            nudFromYear.BackColor = GUI.ColorOptional
            nudFromMonth.BackColor = GUI.ColorOptional
            nudToYear.BackColor = GUI.ColorOptional
            nudToMonth.BackColor = GUI.ColorOptional
            cboFromcondition.BackColor = GUI.ColorOptional
            cboFromcondition.BackColor = GUI.ColorOptional
            cboTocondition.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetEmployee()
        Try
            Dim strSearching As String = ""
            Dim objEmployee As New clsEmployee_Master



            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.

            Me.Cursor = Cursors.WaitCursor

            Dim dtDate1, dtDate2 As Date : Dim blnReinstatement As Boolean = False
            dtDate1 = Nothing : dtDate2 = Nothing
            If radAppointedDate.Checked = True Then
                dtDate1 = dtpDate1.Value.Date
                dtDate2 = dtpDate2.Value.Date
            Else
                dtDate1 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                dtDate2 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                blnReinstatement = True
            End If

            If radExpYear.Checked = True Then
                If nudFromYear.Value > 0 Or nudFromMonth.Value > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboFromcondition.Text & " '" & eZeeDate.convertDate(mdtToDate) & "' "
                End If

                If nudToYear.Value > 0 Or nudToMonth.Value > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboTocondition.Text & " '" & eZeeDate.convertDate(mdtFromDate) & "' "
                End If
            End If

            If radProbationDate.Checked Then
                strSearching &= "AND Convert(char(8),EPROB.probation_from_date,112) >= '" & eZeeDate.convertDate(dtpDate1.Value.Date) & "' AND Convert(char(8),EPROB.probation_to_date,112) <= '" & eZeeDate.convertDate(dtpDate2.Value.Date) & "' "
            End If

            If radConfirmationDate.Checked Then
                strSearching &= "AND Convert(char(8),ECNF.confirmation_date,112) >= '" & eZeeDate.convertDate(dtpDate1.Value.Date) & "' AND Convert(char(8),ECNF.confirmation_date,112) <= '" & eZeeDate.convertDate(dtpDate2.Value.Date) & "' "
            End If

            'Pinkal (30-Jun-2016) -- End


            'Pinkal (22-Oct-2018) -- Start
            'Bug - appointment date filter not working on leave Global Assign accrue and Caim & Request expense assignment screen[Support Issue Id # 0002823]
            If radAppointedDate.Checked = True Then
                strSearching &= "AND Convert(char(8),hremployee_master.appointeddate.112) BETWEEN '" & eZeeDate.convertDate(dtDate1) & "' AND '" & eZeeDate.convertDate(dtDate2) & "'"
            End If
            'Pinkal (22-Oct-2018) -- End


            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter
            End If

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Trim.Substring(3)
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'Dim iValue As String = objLeaveBalance.GetAssignedEmpIds(CInt(cboLeaveCode.SelectedValue))
            'If iValue.Trim.Length > 0 Then
            '    strSearching &= "AND hremployee_master.employeeunkid NOT IN(" & iValue & ") "
            'End If
            'dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                                 User._Object._Userunkid, _
            '                                 FinancialYear._Object._YearUnkid, _
            '                                 Company._Object._Companyunkid, _
            '                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                 ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                 ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                 "Employee", _
            '                                 ConfigParameter._Object._ShowFirstAppointmentDate, , , strSearching, True)
            'mdtEmployee = dsEmployee.Tables(0)

            'For Each dr As DataRow In mdtEmployee.Rows
            '    dr("appointeddate") = eZeeDate.convertDate(dr("appointeddate").ToString()).ToShortDateString
            '    'Pinkal (30-Jun-2016) -- Start
            '    'Enhancement - Working on SL changes on Leave Module.
            '    dr("confirmation_date") = eZeeDate.convertDate(dr("confirmation_date").ToString()).ToShortDateString
            '    'Pinkal (30-Jun-2016) -- End
            'Next
            'If mdtEmployee.Columns.Contains("ischecked") = False Then
            '    mdtEmployee.Columns.Add("ischecked", Type.GetType("System.Boolean"))
            '    mdtEmployee.Columns("ischecked").DefaultValue = False
            'End If

            Dim iValue As String = objLeaveBalance.GetAssignedEmpIds(CInt(cboLeaveCode.SelectedValue))

            Dim strColumnsNeeded As String = String.Empty
            strColumnsNeeded = CInt(clsEmployee_Master.EmpColEnum.Col_Code).ToString & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Employee_Name).ToString & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Appointed_Date).ToString & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Cofirmation_Date).ToString & "," & CInt(clsEmployee_Master.EmpColEnum.Col_Job).ToString


            'Pinkal (14-Jun-2018) -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmLeaveFrequencyList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            'dsEmployee = objEmployee.GetListForDynamicField(strColumnsNeeded, FinancialYear._Object._DatabaseName, _
            '                                 User._Object._Userunkid, _
            '                                 FinancialYear._Object._YearUnkid, _
            '                                 Company._Object._Companyunkid, _
            '                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                 ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                 ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                 "Employee", , , strSearching)

            dsEmployee = objEmployee.GetListForDynamicField(strColumnsNeeded, FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                             ConfigParameter._Object._IsIncludeInactiveEmp, _
                                         "Employee", , , strSearching, False, False, False, mblnAddApprovalCondition)

            'Pinkal (14-Jun-2018) -- End



            If iValue.Trim.Length > 0 Then
                mdtEmployee = New DataView(dsEmployee.Tables(0), "employeeunkid NOT IN (" & iValue & ")", "", DataViewRowState.CurrentRows).ToTable
            Else
            mdtEmployee = dsEmployee.Tables(0)
            End If

            For Each dr As DataRow In mdtEmployee.Rows
                dr.Item(IIf(Language.getMessage("clsEmployee_Master", 48, "Appointed Date") = "", "Appointed Date", Language.getMessage("clsEmployee_Master", 48, "Appointed Date")).ToString) = _
                eZeeDate.convertDate(dr.Item(IIf(Language.getMessage("clsEmployee_Master", 48, "Appointed Date") = "", "Appointed Date", Language.getMessage("clsEmployee_Master", 48, "Appointed Date")).ToString).ToString).ToShortDateString
                dr.Item(IIf(Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date") = "", "Cofirmation Date", Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date")).ToString) = _
                eZeeDate.convertDate(dr.Item(IIf(Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date") = "", "Cofirmation Date", Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date")).ToString).ToString).ToShortDateString
            Next

            Dim dCol As New DataColumn
            With dCol
                .ColumnName = "ischecked"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtEmployee.Columns.Add(dCol)
            'S.SANDEEP [15 NOV 2016] -- END

            Call SetDataSource()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployee", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub SetDataSource()
        Try
            dvEmployee = mdtEmployee.DefaultView
            dgEmployee.AutoGenerateColumns = False
            dgEmployee.DataSource = dvEmployee
            objSelect.DataPropertyName = "ischecked"

            'S.SANDEEP [15 NOV 2016] -- START
            'dgColhEmpCode.DataPropertyName = "employeecode"
            'dgColhEmployee.DataPropertyName = "name"
            'dgColhAppointdate.DataPropertyName = "appointeddate"
            'dgcolhJobTitle.DataPropertyName = "job_name"

            ''Pinkal (30-Jun-2016) -- Start
            ''Enhancement - Working on SL changes on Leave Module.
            'dgcolhConfirmationDate.DataPropertyName = "confirmation_date"
            'If radConfirmationDate.Checked Then
            '    dgcolhConfirmationDate.Visible = True
            '    dgcolhConfirmationDate.DisplayIndex = dgColhEmployee.DisplayIndex + 1
            '    dgColhAppointdate.Visible = False
            'Else
            '    dgcolhConfirmationDate.Visible = False
            '    dgColhAppointdate.Visible = True
            '    dgColhAppointdate.DisplayIndex = dgColhEmployee.DisplayIndex + 1
            'End If
            ''Pinkal (30-Jun-2016) -- End

            dgColhEmpCode.DataPropertyName = IIf(Language.getMessage("clsEmployee_Master", 42, "Code") = "", "Code", Language.getMessage("clsEmployee_Master", 42, "Code")).ToString
            dgColhEmployee.DataPropertyName = IIf(Language.getMessage("clsEmployee_Master", 46, "Employee Name") = "", "Employee Name", Language.getMessage("clsEmployee_Master", 46, "Employee Name")).ToString
            dgColhAppointdate.DataPropertyName = IIf(Language.getMessage("clsEmployee_Master", 48, "Appointed Date") = "", "Appointed Date", Language.getMessage("clsEmployee_Master", 48, "Appointed Date")).ToString
            dgcolhJobTitle.DataPropertyName = IIf(Language.getMessage("clsEmployee_Master", 118, "Job") = "", "Job", Language.getMessage("clsEmployee_Master", 118, "Job")).ToString
            dgcolhConfirmationDate.DataPropertyName = IIf(Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date") = "", "Cofirmation Date", Language.getMessage("clsEmployee_Master", 160, "Cofirmation Date")).ToString

            If radConfirmationDate.Checked Then
                dgcolhConfirmationDate.Visible = True
                dgcolhConfirmationDate.DisplayIndex = dgColhEmployee.DisplayIndex + 1
                dgColhAppointdate.Visible = False
            Else
                dgcolhConfirmationDate.Visible = False
                dgColhAppointdate.Visible = True
                dgColhAppointdate.DisplayIndex = dgColhEmployee.DisplayIndex + 1
            End If

            'S.SANDEEP [15 NOV 2016] -- END

            If dgEmployee.Rows.Count > 0 Then
                chkSelectAll.Enabled = True
            Else
                chkSelectAll.Enabled = False
                chkSelectAll.Checked = False
            End If


            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.

            LblEmployeeCount.Text = Language.getMessage(mstrModuleName, 12, "Employee Count : ") & dvEmployee.Count

            If radAppointedDate.Checked OrElse radConfirmationDate.Checked OrElse radProbationDate.Checked Then
                LblValue.Text = dtpDate1.Value.Date & " - " & dtpDate2.Value.Date
            Else
                LblValue.Text = mdtToDate.Date & " - " & mdtFromDate.Date
            End If

            'Pinkal (30-Jun-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataSource", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = dvEmployee.ToTable.Select("ischecked = true")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgEmployee.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgEmployee.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetValue(ByVal iEmpId As Integer, ByVal dsEmpList As DataSet)
        'Pinkal (13-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[ByVal dsEmpList As DataSet]
        Try
            Dim mIsCloseFy, mIsELC, mIsOpenELC As Boolean : mIsCloseFy = False : mIsELC = False : mIsOpenELC = False

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                mIsCloseFy = False : mIsELC = False : mIsOpenELC = False
            Else
                mIsCloseFy = False : mIsELC = True : mIsOpenELC = True
            End If

            Dim dsIssue As DataSet = objLeaveBalance.GetEmployeeBalanceData(CStr(CInt(cboLeaveCode.SelectedValue)), _
                                                                            CStr(iEmpId), _
                                                                            True, _
                                                                            FinancialYear._Object._YearUnkid, mIsOpenELC, mIsELC, mIsCloseFy)
            If dsIssue IsNot Nothing Then
                If dsIssue.Tables(0).Rows.Count > 0 Then
                    objLeaveBalance._LeaveBalanceunkid = CInt(dsIssue.Tables(0).Rows(0).Item("leavebalanceunkid"))
                    objLeaveBalance._IssueAmount = CDbl(dsIssue.Tables(0).Rows(0).Item("issue_amount"))
                Else
                    objLeaveBalance._IssueAmount = 0
                    objLeaveBalance._LeaveBalanceunkid = 0
                End If
            Else
                objLeaveBalance._IssueAmount = 0
                objLeaveBalance._LeaveBalanceunkid = 0
            End If

            Dim objLvIssue As New clsleaveissue_master

            If objLvIssue.GetEmployeeTotalIssue(iEmpId, CInt(cboLeaveCode.SelectedValue), True) > 0 Then
                objLeaveBalance._Remaining_Occurrence = objLeaveBalance._Remaining_Occurrence - CInt(nudDays.Value)
                If objLeaveBalance._Remaining_Occurrence <= 0 Then objLeaveBalance._Remaining_Occurrence = 0
            Else
                objLeaveBalance._Remaining_Occurrence = CInt(nudDays.Value)
            End If

            objLeaveBalance._IsCloseFy = False

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                objLeaveBalance._IsELC = False
                objLeaveBalance._IsOpenELC = False
            Else
                objLeaveBalance._IsELC = True
                objLeaveBalance._IsOpenELC = True
            End If

            objLeaveBalance._AccrueAmount = txtMaxamount.Decimal
            objLeaveBalance._AccrueSetting = enAccrueSetting.Issue_Balance
            objLeaveBalance._ActualAmount = txtMaxamount.Decimal
            objLeaveBalance._Balance = 0

            objLeaveBalance._Remaining_Balance = txtMaxamount.Decimal - objLvIssue.GetEmployeeTotalIssue(iEmpId, CInt(cboLeaveCode.SelectedValue), True)
            objLvIssue = Nothing
            objLeaveBalance._Batchunkid = 0
            objLeaveBalance._CFlvAmount = 0
            objLeaveBalance._Consicutivedays = CInt(txtDuration.Decimal)
            objLeaveBalance._Daily_Amount = 0
            objLeaveBalance._Days = 0
            objLeaveBalance._EligibilityAfter = CInt(txtEligibilityAfter.Decimal)
            objLeaveBalance._Employeeunkid = iEmpId
            objLeaveBalance._Enddate = FinancialYear._Object._Database_End_Date
            objLeaveBalance._Startdate = FinancialYear._Object._Database_Start_Date

            'Pinkal (13-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            'objLeaveBalance._Daily_Amount = CDec(txtMaxamount.Text.Trim) / DateDiff(DateInterval.Day, FinancialYear._Object._Database_Start_Date.Date, FinancialYear._Object._Database_End_Date.AddDays(1))
            'Dim intDiff As Integer = CInt(DateDiff(DateInterval.Day, objLeaveBalance._Startdate.Date, objLeaveBalance._Enddate.Date.AddDays(1)))
            'objLeaveBalance._AccrueAmount = objLeaveBalance._Daily_Amount * intDiff
            objLeaveBalance._Daily_Amount = 0
            'Pinkal (13-Oct-2018) -- End


            objLeaveBalance._Remaining_Balance = objLeaveBalance._AccrueAmount
            objLeaveBalance._AdjustmentAmt = 0
            objLeaveBalance._IsNoAction = False
            objLeaveBalance._Ispaid = True
            objLeaveBalance._IsshortLeave = True
            objLeaveBalance._Isvoid = False
            objLeaveBalance._LeaveBF = 0
            objLeaveBalance._Leavetypeunkid = CInt(cboLeaveCode.SelectedValue)
            objLeaveBalance._LoginEmployeeUnkid = 0
            objLeaveBalance._Occurance = CInt(nudDays.Value)
            If chkLifeTime.Checked = True Then
                objLeaveBalance._Optiondid = enLeaveFreq_Factors.LVF_LIFE_TIME
            Else
                objLeaveBalance._Optiondid = enLeaveFreq_Factors.LVF_FINANCIAL_YEAR
            End If
            objLeaveBalance._UptoLstYr_AccrueAmout = 0
            objLeaveBalance._UptoLstYr_IssueAmout = 0
            objLeaveBalance._Userunkid = User._Object._Userunkid
            objLeaveBalance._Voiddatetime = Nothing
            objLeaveBalance._Voiduserunkid = 0
            objLeaveBalance._Voidreason = ""
            objLeaveBalance._Yearunkid = FinancialYear._Object._YearUnkid

            'Pinkal (13-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.

            objLeaveBalance._OccurrenceTenure = CInt(nudTenureMonths.Value)

            If chkLifeTime.Checked Then

                Dim mdtStopdate As DateTime = Nothing
                If dsEmpList IsNot Nothing AndAlso dsEmpList.Tables(0).Rows.Count > 0 Then
                    Dim lst_Row As List(Of DataRow) = (From dr In dsEmpList.Tables(0).AsEnumerable() Where CInt(dr("employeeunkid")) = iEmpId Select dr).ToList()
                    If lst_Row.Count > 0 Then
                        Dim drRow As DataRow = lst_Row(0)
                        If drRow(Language.getMessage("clsEmployee_Master", 158, "EOC Date")).ToString().Trim() <> "" Then
                            mdtStopdate = eZeeDate.convertDate(drRow(Language.getMessage("clsEmployee_Master", 158, "EOC Date")).ToString().Trim()).Date
                        End If

                        If drRow(Language.getMessage("clsEmployee_Master", 115, "Leaving Date")).ToString() <> "" AndAlso mdtStopdate <> Nothing AndAlso eZeeDate.convertDate(drRow(Language.getMessage("clsEmployee_Master", 115, "Leaving Date")).ToString().Trim()).Date < mdtStopdate.Date Then
                            mdtStopdate = eZeeDate.convertDate(drRow(Language.getMessage("clsEmployee_Master", 115, "Leaving Date")).ToString().Trim()).Date
                        End If

                        If drRow(Language.getMessage("clsEmployee_Master", 116, "Retirement Date")).ToString().Trim() <> "" Then
                            If mdtStopdate <> Nothing AndAlso eZeeDate.convertDate(drRow(Language.getMessage("clsEmployee_Master", 116, "Retirement Date")).ToString().Trim()).Date < mdtStopdate.Date Then
                                mdtStopdate = eZeeDate.convertDate(drRow(Language.getMessage("clsEmployee_Master", 116, "Retirement Date")).ToString().Trim()).Date
                            Else
                                mdtStopdate = eZeeDate.convertDate(drRow(Language.getMessage("clsEmployee_Master", 116, "Retirement Date")).ToString().Trim()).Date
                            End If
                        Else
                            mdtStopdate = objLeaveBalance._Enddate.Date
                        End If
                    End If
                End If
                objLeaveBalance._Enddate = mdtStopdate.Date
            Else
                If objLeaveBalance._OccurrenceTenure > 0 Then
                    objLeaveBalance._Enddate = objLeaveBalance._Startdate.Date.AddMonths(objLeaveBalance._OccurrenceTenure).AddDays(-1)
                End If
            End If

            'Pinkal (13-Oct-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid_Data() As Boolean
        Try
            If CInt(cboLeaveCode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Leave Type is mandatory information. Please select Leave Type to continue."), enMsgBoxStyle.Information)
                cboLeaveCode.Focus()
                Return False
            End If

            If dgEmployee.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "There is no employee to assign leave frequency factors."), enMsgBoxStyle.Information)
                dgEmployee.Focus()
                Return False
            End If

            If mdtEmployee IsNot Nothing Then
                Dim dr() As DataRow = mdtEmployee.Select("ischecked=True")
                If dr.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employee is compulsory information.Please check atleast one employee."), enMsgBoxStyle.Information)
                    dgEmployee.Select()
                    Return False
                End If
            End If

            If txtMaxamount.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Leave Max Amount cannot be 0.Leave Max Amount is required information."), enMsgBoxStyle.Information)
                txtMaxamount.Focus()
                Return False
            End If

            If txtDuration.Decimal > 0 AndAlso txtDuration.Decimal > txtMaxamount.Decimal Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot set consecutive days more than leave max amount."), enMsgBoxStyle.Information)
                    txtDuration.Focus()
                    Return False
                End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid_Data", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Event "

    Private Sub frmGlobalLvFreq_Assign_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeaveBalance = New clsleavebalance_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                radFinELC.Text = Language.getMessage(mstrModuleName, 2, "Financial Year")
            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                radFinELC.Text = Language.getMessage(mstrModuleName, 3, "Employee Leave Cycle")
            End If
            Call FillCombo()

            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.
            setColor()
            LblValue.Text = dtpDate1.Value.Date & " - " & dtpDate2.Value.Date
            LblEmployeeCount.Text = Language.getMessage(mstrModuleName, 12, "Employee Count : ") & "0"
            'Pinkal (30-Jun-2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalLvFreq_Assign_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Try
            If IsValid_Data() = False Then Exit Sub


            'Pinkal (30-Nov-2013) -- Start
            'Enhancement : Oman Changes  ---- AS PER ANDREW COMMENTS FOR TRA

            If txtEligibilityAfter.Text.Trim = "" Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "You have not set leave eligibility days. ZERO will be set to all selected employee(s).") & vbCrLf & _
                                                      Language.getMessage(mstrModuleName, 11, "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub
            End If

            'Pinkal (30-Nov-2013) -- End

            Dim blnFlag As Boolean = False
            Dim dr() As DataRow = mdtEmployee.Select("ischecked=true")



            'Pinkal (13-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            Dim dsEmpList As DataSet = Nothing
            If chkLifeTime.Checked Then
                Dim strempids As String = String.Join(",", dr.ToList().AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())

                Dim objEmp As New clsEmployee_Master
                Dim StrCheck_Fields As String = clsEmployee_Master.EmpColEnum.Col_EOC_Date & "," & clsEmployee_Master.EmpColEnum.Col_Leaving_Date & "," & clsEmployee_Master.EmpColEnum.Col_Retirement_Date & "," & clsEmployee_Master.EmpColEnum.Col_Reinstatement_Date
                dsEmpList = objEmp.GetListForDynamicField(StrCheck_Fields, FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                                     , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
                                                                                                     , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, ConfigParameter._Object._UserAccessModeSetting _
                                                                                                     , True, False, "List", -1, False, "hremployee_master.employeeunkid in (" & strempids & ")", False, False, False, True, Nothing)

                objEmp = Nothing
            End If
            'Pinkal (13-Oct-2018) -- End

            For iRow As Integer = 0 To dr.Length - 1

                'Pinkal (13-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                'Call SetValue(CInt(dr(iRow).Item("employeeunkid")))
                Call SetValue(CInt(dr(iRow).Item("employeeunkid")), dsEmpList)
                'Pinkal (13-Oct-2018) -- End

 'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objLeaveBalance._FormName = mstrModuleName
                objLeaveBalance._LoginEmployeeUnkid = 0
                objLeaveBalance._ClientIP = getIP()
                objLeaveBalance._HostName = getHostName()
                objLeaveBalance._FromWeb = False
                objLeaveBalance._AuditUserId = User._Object._Userunkid
objLeaveBalance._CompanyUnkid = Company._Object._Companyunkid
                objLeaveBalance._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objLeaveBalance._LeaveBalanceunkid <= 0 Then
                    objLeaveBalance._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting

                    blnFlag = objLeaveBalance.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                    , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                    , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True)

                    If blnFlag = False Then
                        eZeeMsgBox.Show(objLeaveBalance._Message, enMsgBoxStyle.Information)
                        dgEmployee.Rows(mdtEmployee.Rows.IndexOf(dr(iRow))).DefaultCellStyle.ForeColor = Color.Red
                        Exit For
                    End If
                Else

                    objLeaveBalance._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting

                    blnFlag = objLeaveBalance.Update()
                    If blnFlag = False Then
                        eZeeMsgBox.Show(objLeaveBalance._Message, enMsgBoxStyle.Information)
                        dgEmployee.Rows(mdtEmployee.Rows.IndexOf(dr(iRow))).DefaultCellStyle.ForeColor = Color.Red
                        Exit For
                    End If
                End If
            Next
            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Leave frequency successfully assigned."), enMsgBoxStyle.Information)
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAssign_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchLeave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLeave.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboLeaveCode.ValueMember
                .DisplayMember = cboLeaveCode.DisplayMember
                .CodeMember = "leavetypecode"
                .DataSource = CType(cboLeaveCode.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboLeaveCode.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLeave_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboLeaveCode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Leave Type is mandatory information. Please select Leave Type to continue."), enMsgBoxStyle.Information)
                cboLeaveCode.Select()
                Exit Sub

                'Pinkal (30-Jun-2016) -- Start
                'Enhancement - Working on SL changes on Leave Module.

            ElseIf radAppointedDate.Checked = True Then
                If dtpDate2.Value.Date < dtpDate1.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, To date cannot ne less than From date."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            ElseIf radExpYear.Checked = True Then
                If (nudFromYear.Value > 0 Or nudFromMonth.Value > 0) And CInt(cboFromcondition.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "From Condition is compulsory information.Please Select From Condition."), enMsgBoxStyle.Information)
                    cboFromcondition.Select()
                    Exit Sub
                ElseIf (nudToYear.Value > 0 Or nudToMonth.Value > 0) And CInt(cboTocondition.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "To Condition is compulsory information.Please Select To Condition."), enMsgBoxStyle.Information)
                    cboTocondition.Select()
                Exit Sub
            End If
            End If

            'Pinkal (30-Jun-2016) -- End

            Call GetEmployee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboLeaveCode.SelectedValue = 0
            txtDuration.Decimal = 0
            txtEligibilityAfter.Decimal = 0
            txtMaxamount.Decimal = 0
            TxtSearch.Text = ""
            radFinELC.Checked = False
            radLifetime.Checked = False
            nudDays.Value = 0
            chkSelectAll.Checked = False
            If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()


            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.
            dtpDate1.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpDate2.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            nudFromYear.Value = 0
            nudFromMonth.Value = 0
            nudToYear.Value = 0
            nudToMonth.Value = 0
            cboFromcondition.SelectedValue = 0
            cboTocondition.SelectedValue = 0
            mstrAdvanceFilter = ""
            radExpYear.Checked = False
            radAppointedDate.Checked = False
            radConfirmationDate.Checked = False
            radProbationDate.Checked = False
            chkSelectAll.Checked = False
            pnlYear.Enabled = True
            pnlDate.Enabled = True
            LblValue.Text = dtpDate1.Value.Date & " - " & dtpDate2.Value.Date
            LblEmployeeCount.Text = Language.getMessage(mstrModuleName, 12, "Employee Count : ") & "0"
            'Pinkal (30-Jun-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Textbox Events"

    Private Sub TxtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtSearch.TextChanged
        Try
            If dvEmployee.Table.Rows.Count > 0 Then

                'Pinkal (13-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                ' dvEmployee.RowFilter = "employeecode like '%" & TxtSearch.Text.Trim & "%'  OR name like '%" & TxtSearch.Text.Trim & "%'"
                dvEmployee.RowFilter = "[" & Language.getMessage("clsEmployee_Master", 42, "Code") & "] like '%" & TxtSearch.Text.Trim & "%'  OR [" & Language.getMessage("clsEmployee_Master", 46, "Employee Name") & "] like '%" & TxtSearch.Text.Trim & "%'"
                'Pinkal (13-Oct-2018) -- End

                dgEmployee.Refresh()
            End If

            If dgEmployee.Rows.Count > 0 Then
                chkSelectAll.Enabled = True
            Else
                chkSelectAll.Enabled = False
                chkSelectAll.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "TxtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Checkbox Events"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try

            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            If mdtEmployee Is Nothing Then Exit Sub
            'Pinkal (01-Mar-2016) -- End

            If mdtEmployee.Rows.Count > 0 Then
                For Each dr As DataRowView In dvEmployee
                    RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr("ischecked") = chkSelectAll.Checked
                    AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub


    'Pinkal (13-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    Private Sub chkLifeTime_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLifeTime.CheckedChanged
        Try
            LblTenureMonths.Enabled = Not chkLifeTime.Checked
            nudTenureMonths.Enabled = Not chkLifeTime.Checked
            If chkLifeTime.Checked = True Then nudTenureMonths.Value = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkLifeTime_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (13-Oct-2018) -- End


#End Region

#Region "LinkButton Events"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            mstrAdvanceFilter = ""
            TxtSearch.Text = ""
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Datagrid Events "

    Private Sub dgEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick, dgEmployee.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objSelect.Index Then

                If dgEmployee.IsCurrentCellDirty Then
                    dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    mdtEmployee.AcceptChanges()
                End If

                SetCheckBoxValue()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "NumericUpdown Event"

    Private Sub nudFromYear_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudFromYear.ValueChanged, nudFromMonth.Validated, nudFromMonth.ValueChanged, nudFromMonth.Validated
        Try
            mdtFromDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(nudFromYear.Value) * -1).AddMonths(CInt(nudFromMonth.Value) * -1)
            nudToYear.Minimum = nudFromYear.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudFromYear_ValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub nudToYear_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudToYear.ValueChanged, nudToYear.Validated, nudToMonth.ValueChanged, nudToMonth.Validated
        Try
            mdtToDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(nudToYear.Value) * -1).AddMonths(CInt(nudToMonth.Value) * -1)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudToYear_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "RadioButton Event"

    Private Sub radExpYear_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radExpYear.CheckedChanged, radAppointedDate.CheckedChanged _
                                                                                                                                                                          , radProbationDate.CheckedChanged, radConfirmationDate.CheckedChanged
        Try

            If CType(sender, RadioButton).Name.ToUpper = "RADEXPYEAR" Then
                pnlDate.Enabled = False : pnlYear.Enabled = True
            ElseIf CType(sender, RadioButton).Name.ToUpper = "RADAPPOINTEDDATE" Then
                pnlDate.Enabled = True : pnlYear.Enabled = False
            ElseIf CType(sender, RadioButton).Name.ToUpper = "RADPROBATIONDATE" Then
                pnlDate.Enabled = True : pnlYear.Enabled = False
            ElseIf CType(sender, RadioButton).Name.ToUpper = "RADCONFIRMATIONDATE" Then
                pnlDate.Enabled = True : pnlYear.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radExpYear_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			Call SetLanguage()
			
			Me.gbFrequencyFactors.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFrequencyFactors.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnAssign.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAssign.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnAssign.Text = Language._Object.getCaption(Me.btnAssign.Name, Me.btnAssign.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.dgColhAppointdate.HeaderText = Language._Object.getCaption(Me.dgColhAppointdate.Name, Me.dgColhAppointdate.HeaderText)
			Me.dgcolhJobTitle.HeaderText = Language._Object.getCaption(Me.dgcolhJobTitle.Name, Me.dgcolhJobTitle.HeaderText)
			Me.gbFrequencyFactors.Text = Language._Object.getCaption(Me.gbFrequencyFactors.Name, Me.gbFrequencyFactors.Text)
			Me.lblLeaveCode.Text = Language._Object.getCaption(Me.lblLeaveCode.Name, Me.lblLeaveCode.Text)
			Me.lblDuration.Text = Language._Object.getCaption(Me.lblDuration.Name, Me.lblDuration.Text)
			Me.LblEligibilityAfter.Text = Language._Object.getCaption(Me.LblEligibilityAfter.Name, Me.LblEligibilityAfter.Text)
			Me.lbltimes.Text = Language._Object.getCaption(Me.lbltimes.Name, Me.lbltimes.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.lblMaxAmount.Text = Language._Object.getCaption(Me.lblMaxAmount.Name, Me.lblMaxAmount.Text)
			Me.chkLifeTime.Text = Language._Object.getCaption(Me.chkLifeTime.Name, Me.chkLifeTime.Text)
			Me.radLifetime.Text = Language._Object.getCaption(Me.radLifetime.Name, Me.radLifetime.Text)
			Me.radFinELC.Text = Language._Object.getCaption(Me.radFinELC.Name, Me.radFinELC.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.radExpYear.Text = Language._Object.getCaption(Me.radExpYear.Name, Me.radExpYear.Text)
			Me.radProbationDate.Text = Language._Object.getCaption(Me.radProbationDate.Name, Me.radProbationDate.Text)
			Me.radConfirmationDate.Text = Language._Object.getCaption(Me.radConfirmationDate.Name, Me.radConfirmationDate.Text)
			Me.radAppointedDate.Text = Language._Object.getCaption(Me.radAppointedDate.Name, Me.radAppointedDate.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.lblFromYear.Text = Language._Object.getCaption(Me.lblFromYear.Name, Me.lblFromYear.Text)
			Me.LblToYear.Text = Language._Object.getCaption(Me.LblToYear.Name, Me.LblToYear.Text)
			Me.LblToMonth.Text = Language._Object.getCaption(Me.LblToMonth.Name, Me.LblToMonth.Text)
			Me.LblMonth.Text = Language._Object.getCaption(Me.LblMonth.Name, Me.LblMonth.Text)
			Me.LblEmployeeCount.Text = Language._Object.getCaption(Me.LblEmployeeCount.Name, Me.LblEmployeeCount.Text)
			Me.LblValue.Text = Language._Object.getCaption(Me.LblValue.Name, Me.LblValue.Text)
			Me.LblFrom.Text = Language._Object.getCaption(Me.LblFrom.Name, Me.LblFrom.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Financial Year")
			Language.setMessage(mstrModuleName, 3, "Employee Leave Cycle")
			Language.setMessage(mstrModuleName, 4, "Leave Type is mandatory information. Please select Leave Type to continue.")
			Language.setMessage(mstrModuleName, 5, "There is no employee to assign leave frequency factors.")
			Language.setMessage(mstrModuleName, 6, "Employee is compulsory information.Please check atleast one employee.")
			Language.setMessage(mstrModuleName, 7, "Leave Max Amount cannot be 0.Leave Max Amount is required information.")
			Language.setMessage(mstrModuleName, 8, "Leave frequency successfully assigned.")
			Language.setMessage(mstrModuleName, 9, "Sorry, you cannot set consecutive days more than leave max amount.")
	                Language.setMessage(mstrModuleName, 10, "You have not set leave eligibility days. ZERO will be set to all selected employee(s).")
			Language.setMessage(mstrModuleName, 11, "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 12, "Employee Count :")
			Language.setMessage(mstrModuleName, 13, "Sorry, To date cannot ne less than From date.")
			Language.setMessage(mstrModuleName, 14, "From Condition is compulsory information.Please Select From Condition.")
			Language.setMessage(mstrModuleName, 15, "To Condition is compulsory information.Please Select To Condition.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class