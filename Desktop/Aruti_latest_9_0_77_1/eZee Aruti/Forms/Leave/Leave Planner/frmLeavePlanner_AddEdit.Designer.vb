﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmleaveplanner_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmleaveplanner_AddEdit))
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbLeavePlannerInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddLeaveType = New eZee.Common.eZeeGradientButton
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.lblStopDate = New System.Windows.Forms.Label
        Me.dtpStopDate = New System.Windows.Forms.DateTimePicker
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.lblGender = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.TxtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.LblEmployeeCount = New System.Windows.Forms.Label
        Me.LblValue = New System.Windows.Forms.Label
        Me.LblFrom = New System.Windows.Forms.Label
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.radExpYear = New System.Windows.Forms.RadioButton
        Me.radProbationDate = New System.Windows.Forms.RadioButton
        Me.radConfirmationDate = New System.Windows.Forms.RadioButton
        Me.radAppointedDate = New System.Windows.Forms.RadioButton
        Me.pnlDate = New System.Windows.Forms.Panel
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpDate1 = New System.Windows.Forms.DateTimePicker
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpDate2 = New System.Windows.Forms.DateTimePicker
        Me.pnlYear = New System.Windows.Forms.Panel
        Me.lblFromYear = New System.Windows.Forms.Label
        Me.nudToYear = New System.Windows.Forms.NumericUpDown
        Me.LblToYear = New System.Windows.Forms.Label
        Me.nudFromMonth = New System.Windows.Forms.NumericUpDown
        Me.LblToMonth = New System.Windows.Forms.Label
        Me.LblMonth = New System.Windows.Forms.Label
        Me.nudToMonth = New System.Windows.Forms.NumericUpDown
        Me.cboTocondition = New System.Windows.Forms.ComboBox
        Me.nudFromYear = New System.Windows.Forms.NumericUpDown
        Me.cboFromcondition = New System.Windows.Forms.ComboBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgColhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAppointedDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhConfirmationDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhProbationFromDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhProbationToDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJobTitle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbLeavePlannerInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlDate.SuspendLayout()
        Me.pnlYear.SuspendLayout()
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbLeavePlannerInfo
        '
        Me.gbLeavePlannerInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLeavePlannerInfo.Checked = False
        Me.gbLeavePlannerInfo.CollapseAllExceptThis = False
        Me.gbLeavePlannerInfo.CollapsedHoverImage = Nothing
        Me.gbLeavePlannerInfo.CollapsedNormalImage = Nothing
        Me.gbLeavePlannerInfo.CollapsedPressedImage = Nothing
        Me.gbLeavePlannerInfo.CollapseOnLoad = False
        Me.gbLeavePlannerInfo.Controls.Add(Me.objbtnAddLeaveType)
        Me.gbLeavePlannerInfo.Controls.Add(Me.lblRemarks)
        Me.gbLeavePlannerInfo.Controls.Add(Me.txtRemarks)
        Me.gbLeavePlannerInfo.Controls.Add(Me.lblStopDate)
        Me.gbLeavePlannerInfo.Controls.Add(Me.dtpStopDate)
        Me.gbLeavePlannerInfo.Controls.Add(Me.lblStartDate)
        Me.gbLeavePlannerInfo.Controls.Add(Me.dtpStartDate)
        Me.gbLeavePlannerInfo.Controls.Add(Me.cboLeaveType)
        Me.gbLeavePlannerInfo.Controls.Add(Me.lblLeaveType)
        Me.gbLeavePlannerInfo.ExpandedHoverImage = Nothing
        Me.gbLeavePlannerInfo.ExpandedNormalImage = Nothing
        Me.gbLeavePlannerInfo.ExpandedPressedImage = Nothing
        Me.gbLeavePlannerInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeavePlannerInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeavePlannerInfo.HeaderHeight = 25
        Me.gbLeavePlannerInfo.HeaderMessage = ""
        Me.gbLeavePlannerInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeavePlannerInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeavePlannerInfo.HeightOnCollapse = 0
        Me.gbLeavePlannerInfo.LeftTextSpace = 0
        Me.gbLeavePlannerInfo.Location = New System.Drawing.Point(12, 375)
        Me.gbLeavePlannerInfo.Name = "gbLeavePlannerInfo"
        Me.gbLeavePlannerInfo.OpenHeight = 300
        Me.gbLeavePlannerInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeavePlannerInfo.ShowBorder = True
        Me.gbLeavePlannerInfo.ShowCheckBox = False
        Me.gbLeavePlannerInfo.ShowCollapseButton = False
        Me.gbLeavePlannerInfo.ShowDefaultBorderColor = True
        Me.gbLeavePlannerInfo.ShowDownButton = False
        Me.gbLeavePlannerInfo.ShowHeader = True
        Me.gbLeavePlannerInfo.Size = New System.Drawing.Size(676, 139)
        Me.gbLeavePlannerInfo.TabIndex = 7
        Me.gbLeavePlannerInfo.Temp = 0
        Me.gbLeavePlannerInfo.Text = "Leave Information"
        Me.gbLeavePlannerInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddLeaveType
        '
        Me.objbtnAddLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddLeaveType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddLeaveType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddLeaveType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddLeaveType.BorderSelected = False
        Me.objbtnAddLeaveType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddLeaveType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddLeaveType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddLeaveType.Location = New System.Drawing.Point(383, 32)
        Me.objbtnAddLeaveType.Name = "objbtnAddLeaveType"
        Me.objbtnAddLeaveType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddLeaveType.TabIndex = 274
        '
        'lblRemarks
        '
        Me.lblRemarks.BackColor = System.Drawing.Color.Transparent
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(11, 86)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(80, 15)
        Me.lblRemarks.TabIndex = 272
        Me.lblRemarks.Text = "Remark"
        Me.lblRemarks.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemarks.Location = New System.Drawing.Point(97, 84)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.Size = New System.Drawing.Size(560, 47)
        Me.txtRemarks.TabIndex = 271
        '
        'lblStopDate
        '
        Me.lblStopDate.BackColor = System.Drawing.Color.Transparent
        Me.lblStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStopDate.Location = New System.Drawing.Point(468, 61)
        Me.lblStopDate.Name = "lblStopDate"
        Me.lblStopDate.Size = New System.Drawing.Size(82, 15)
        Me.lblStopDate.TabIndex = 267
        Me.lblStopDate.Text = "Stop Date"
        '
        'dtpStopDate
        '
        Me.dtpStopDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Checked = False
        Me.dtpStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStopDate.Location = New System.Drawing.Point(557, 57)
        Me.dtpStopDate.Name = "dtpStopDate"
        Me.dtpStopDate.Size = New System.Drawing.Size(100, 21)
        Me.dtpStopDate.TabIndex = 7
        '
        'lblStartDate
        '
        Me.lblStartDate.BackColor = System.Drawing.Color.Transparent
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(468, 35)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(82, 15)
        Me.lblStartDate.TabIndex = 266
        Me.lblStartDate.Text = "Start Date"
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(557, 32)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(100, 21)
        Me.dtpStartDate.TabIndex = 6
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(97, 32)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(280, 21)
        Me.cboLeaveType.TabIndex = 4
        '
        'lblLeaveType
        '
        Me.lblLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(11, 35)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(80, 15)
        Me.lblLeaveType.TabIndex = 234
        Me.lblLeaveType.Text = "Leave Type"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 520)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(700, 55)
        Me.objFooter.TabIndex = 8
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(488, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(591, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 11
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(20, 155)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 28
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(417, 87)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(88, 21)
        Me.cboGender.TabIndex = 276
        '
        'lblGender
        '
        Me.lblGender.BackColor = System.Drawing.Color.Transparent
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(353, 90)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(62, 15)
        Me.lblGender.TabIndex = 276
        Me.lblGender.Text = "Gender"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.chkSelectAll)
        Me.Panel2.Controls.Add(Me.TxtSearch)
        Me.Panel2.Controls.Add(Me.dgEmployee)
        Me.Panel2.Controls.Add(Me.LblEmployeeCount)
        Me.Panel2.Controls.Add(Me.LblValue)
        Me.Panel2.Controls.Add(Me.LblFrom)
        Me.Panel2.Controls.Add(Me.gbFilterCriteria)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(700, 575)
        Me.Panel2.TabIndex = 30
        '
        'TxtSearch
        '
        Me.TxtSearch.Flags = 0
        Me.TxtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.TxtSearch.Location = New System.Drawing.Point(12, 126)
        Me.TxtSearch.Name = "TxtSearch"
        Me.TxtSearch.Size = New System.Drawing.Size(204, 21)
        Me.TxtSearch.TabIndex = 295
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objSelect, Me.dgColhEmpCode, Me.dgColhEmployee, Me.dgcolhAppointedDate, Me.dgcolhConfirmationDate, Me.dgcolhProbationFromDate, Me.dgcolhProbationToDate, Me.dgcolhJobTitle, Me.objdgcolhEmployeeId})
        Me.dgEmployee.Location = New System.Drawing.Point(12, 148)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.Size = New System.Drawing.Size(676, 221)
        Me.dgEmployee.TabIndex = 294
        '
        'LblEmployeeCount
        '
        Me.LblEmployeeCount.BackColor = System.Drawing.Color.Transparent
        Me.LblEmployeeCount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmployeeCount.Location = New System.Drawing.Point(529, 126)
        Me.LblEmployeeCount.Name = "LblEmployeeCount"
        Me.LblEmployeeCount.Size = New System.Drawing.Size(157, 21)
        Me.LblEmployeeCount.TabIndex = 293
        Me.LblEmployeeCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblValue
        '
        Me.LblValue.BackColor = System.Drawing.Color.Transparent
        Me.LblValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblValue.Location = New System.Drawing.Point(284, 126)
        Me.LblValue.Name = "LblValue"
        Me.LblValue.Size = New System.Drawing.Size(230, 21)
        Me.LblValue.TabIndex = 292
        Me.LblValue.Text = "#Value"
        Me.LblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblFrom
        '
        Me.LblFrom.BackColor = System.Drawing.Color.Transparent
        Me.LblFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFrom.Location = New System.Drawing.Point(219, 126)
        Me.LblFrom.Name = "LblFrom"
        Me.LblFrom.Size = New System.Drawing.Size(59, 21)
        Me.LblFrom.TabIndex = 291
        Me.LblFrom.Text = "From"
        Me.LblFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboGender)
        Me.gbFilterCriteria.Controls.Add(Me.lblGender)
        Me.gbFilterCriteria.Controls.Add(Me.radExpYear)
        Me.gbFilterCriteria.Controls.Add(Me.radProbationDate)
        Me.gbFilterCriteria.Controls.Add(Me.radConfirmationDate)
        Me.gbFilterCriteria.Controls.Add(Me.radAppointedDate)
        Me.gbFilterCriteria.Controls.Add(Me.pnlDate)
        Me.gbFilterCriteria.Controls.Add(Me.pnlYear)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 3)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(676, 119)
        Me.gbFilterCriteria.TabIndex = 15
        Me.gbFilterCriteria.TabStop = True
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(544, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(79, 17)
        Me.lnkAllocation.TabIndex = 308
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(649, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(626, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'radExpYear
        '
        Me.radExpYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExpYear.Location = New System.Drawing.Point(12, 34)
        Me.radExpYear.Name = "radExpYear"
        Me.radExpYear.Size = New System.Drawing.Size(117, 16)
        Me.radExpYear.TabIndex = 303
        Me.radExpYear.TabStop = True
        Me.radExpYear.Text = "Year of Experience"
        Me.radExpYear.UseVisualStyleBackColor = True
        '
        'radProbationDate
        '
        Me.radProbationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radProbationDate.Location = New System.Drawing.Point(279, 34)
        Me.radProbationDate.Name = "radProbationDate"
        Me.radProbationDate.Size = New System.Drawing.Size(117, 16)
        Me.radProbationDate.TabIndex = 309
        Me.radProbationDate.TabStop = True
        Me.radProbationDate.Text = "Probation Date"
        Me.radProbationDate.UseVisualStyleBackColor = True
        '
        'radConfirmationDate
        '
        Me.radConfirmationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radConfirmationDate.Location = New System.Drawing.Point(420, 34)
        Me.radConfirmationDate.Name = "radConfirmationDate"
        Me.radConfirmationDate.Size = New System.Drawing.Size(117, 16)
        Me.radConfirmationDate.TabIndex = 310
        Me.radConfirmationDate.TabStop = True
        Me.radConfirmationDate.Text = "Confirmation Date"
        Me.radConfirmationDate.UseVisualStyleBackColor = True
        '
        'radAppointedDate
        '
        Me.radAppointedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAppointedDate.Location = New System.Drawing.Point(142, 34)
        Me.radAppointedDate.Name = "radAppointedDate"
        Me.radAppointedDate.Size = New System.Drawing.Size(117, 16)
        Me.radAppointedDate.TabIndex = 1
        Me.radAppointedDate.TabStop = True
        Me.radAppointedDate.Text = "Appointment Date"
        Me.radAppointedDate.UseVisualStyleBackColor = True
        '
        'pnlDate
        '
        Me.pnlDate.Controls.Add(Me.lblToDate)
        Me.pnlDate.Controls.Add(Me.dtpDate1)
        Me.pnlDate.Controls.Add(Me.lblFromDate)
        Me.pnlDate.Controls.Add(Me.dtpDate2)
        Me.pnlDate.Location = New System.Drawing.Point(348, 58)
        Me.pnlDate.Name = "pnlDate"
        Me.pnlDate.Size = New System.Drawing.Size(324, 26)
        Me.pnlDate.TabIndex = 298
        '
        'lblToDate
        '
        Me.lblToDate.BackColor = System.Drawing.Color.Transparent
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(162, 5)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(58, 15)
        Me.lblToDate.TabIndex = 305
        Me.lblToDate.Text = "To Date"
        Me.lblToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate1
        '
        Me.dtpDate1.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate1.Location = New System.Drawing.Point(69, 2)
        Me.dtpDate1.Name = "dtpDate1"
        Me.dtpDate1.Size = New System.Drawing.Size(87, 21)
        Me.dtpDate1.TabIndex = 1
        '
        'lblFromDate
        '
        Me.lblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(6, 5)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(62, 15)
        Me.lblFromDate.TabIndex = 304
        Me.lblFromDate.Text = "From Date"
        Me.lblFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate2
        '
        Me.dtpDate2.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate2.Location = New System.Drawing.Point(234, 3)
        Me.dtpDate2.Name = "dtpDate2"
        Me.dtpDate2.Size = New System.Drawing.Size(87, 21)
        Me.dtpDate2.TabIndex = 304
        '
        'pnlYear
        '
        Me.pnlYear.Controls.Add(Me.lblFromYear)
        Me.pnlYear.Controls.Add(Me.nudToYear)
        Me.pnlYear.Controls.Add(Me.LblToYear)
        Me.pnlYear.Controls.Add(Me.nudFromMonth)
        Me.pnlYear.Controls.Add(Me.LblToMonth)
        Me.pnlYear.Controls.Add(Me.LblMonth)
        Me.pnlYear.Controls.Add(Me.nudToMonth)
        Me.pnlYear.Controls.Add(Me.cboTocondition)
        Me.pnlYear.Controls.Add(Me.nudFromYear)
        Me.pnlYear.Controls.Add(Me.cboFromcondition)
        Me.pnlYear.Location = New System.Drawing.Point(7, 59)
        Me.pnlYear.Name = "pnlYear"
        Me.pnlYear.Size = New System.Drawing.Size(334, 52)
        Me.pnlYear.TabIndex = 303
        '
        'lblFromYear
        '
        Me.lblFromYear.BackColor = System.Drawing.Color.Transparent
        Me.lblFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromYear.Location = New System.Drawing.Point(3, 5)
        Me.lblFromYear.Name = "lblFromYear"
        Me.lblFromYear.Size = New System.Drawing.Size(57, 15)
        Me.lblFromYear.TabIndex = 279
        Me.lblFromYear.Text = "From Year"
        Me.lblFromYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudToYear
        '
        Me.nudToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToYear.Location = New System.Drawing.Point(67, 29)
        Me.nudToYear.Name = "nudToYear"
        Me.nudToYear.Size = New System.Drawing.Size(53, 21)
        Me.nudToYear.TabIndex = 292
        Me.nudToYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblToYear
        '
        Me.LblToYear.BackColor = System.Drawing.Color.Transparent
        Me.LblToYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToYear.Location = New System.Drawing.Point(3, 32)
        Me.LblToYear.Name = "LblToYear"
        Me.LblToYear.Size = New System.Drawing.Size(57, 15)
        Me.LblToYear.TabIndex = 291
        Me.LblToYear.Text = "To Year"
        Me.LblToYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudFromMonth
        '
        Me.nudFromMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromMonth.Location = New System.Drawing.Point(203, 2)
        Me.nudFromMonth.Maximum = New Decimal(New Integer() {11, 0, 0, 0})
        Me.nudFromMonth.Name = "nudFromMonth"
        Me.nudFromMonth.Size = New System.Drawing.Size(57, 21)
        Me.nudFromMonth.TabIndex = 289
        Me.nudFromMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblToMonth
        '
        Me.LblToMonth.BackColor = System.Drawing.Color.Transparent
        Me.LblToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToMonth.Location = New System.Drawing.Point(129, 32)
        Me.LblToMonth.Name = "LblToMonth"
        Me.LblToMonth.Size = New System.Drawing.Size(68, 15)
        Me.LblToMonth.TabIndex = 293
        Me.LblToMonth.Text = "To Month"
        Me.LblToMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblMonth
        '
        Me.LblMonth.BackColor = System.Drawing.Color.Transparent
        Me.LblMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMonth.Location = New System.Drawing.Point(129, 5)
        Me.LblMonth.Name = "LblMonth"
        Me.LblMonth.Size = New System.Drawing.Size(68, 15)
        Me.LblMonth.TabIndex = 288
        Me.LblMonth.Text = "From Month"
        Me.LblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudToMonth
        '
        Me.nudToMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudToMonth.Location = New System.Drawing.Point(203, 29)
        Me.nudToMonth.Maximum = New Decimal(New Integer() {11, 0, 0, 0})
        Me.nudToMonth.Name = "nudToMonth"
        Me.nudToMonth.Size = New System.Drawing.Size(57, 21)
        Me.nudToMonth.TabIndex = 294
        Me.nudToMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboTocondition
        '
        Me.cboTocondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTocondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTocondition.FormattingEnabled = True
        Me.cboTocondition.Location = New System.Drawing.Point(266, 29)
        Me.cboTocondition.Name = "cboTocondition"
        Me.cboTocondition.Size = New System.Drawing.Size(63, 21)
        Me.cboTocondition.TabIndex = 297
        '
        'nudFromYear
        '
        Me.nudFromYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudFromYear.Location = New System.Drawing.Point(67, 2)
        Me.nudFromYear.Name = "nudFromYear"
        Me.nudFromYear.Size = New System.Drawing.Size(53, 21)
        Me.nudFromYear.TabIndex = 281
        Me.nudFromYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboFromcondition
        '
        Me.cboFromcondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromcondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromcondition.FormattingEnabled = True
        Me.cboFromcondition.Location = New System.Drawing.Point(266, 2)
        Me.cboFromcondition.Name = "cboFromcondition"
        Me.cboFromcondition.Size = New System.Drawing.Size(63, 21)
        Me.cboFromcondition.TabIndex = 295
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.MinimumWidth = 175
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle5.Format = "d"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn3.HeaderText = "Appointment Date"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle6.Format = "d"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn4.HeaderText = "Confirmation Date"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        DataGridViewCellStyle7.Format = "d"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn5.HeaderText = "Probation From Date"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle8.Format = "d"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn6.HeaderText = "Probation To Date"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn7.HeaderText = "Job Title"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 230
        '
        'objSelect
        '
        Me.objSelect.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objSelect.Frozen = True
        Me.objSelect.HeaderText = ""
        Me.objSelect.Name = "objSelect"
        Me.objSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objSelect.Width = 25
        '
        'dgColhEmpCode
        '
        Me.dgColhEmpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmpCode.HeaderText = "Code"
        Me.dgColhEmpCode.Name = "dgColhEmpCode"
        Me.dgColhEmpCode.ReadOnly = True
        Me.dgColhEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgColhEmpCode.Width = 90
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhEmployee.HeaderText = "Employee"
        Me.dgColhEmployee.MinimumWidth = 175
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        Me.dgColhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAppointedDate
        '
        Me.dgcolhAppointedDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle1.NullValue = Nothing
        Me.dgcolhAppointedDate.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhAppointedDate.HeaderText = "Appointment Date"
        Me.dgcolhAppointedDate.Name = "dgcolhAppointedDate"
        Me.dgcolhAppointedDate.ReadOnly = True
        Me.dgcolhAppointedDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhAppointedDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhConfirmationDate
        '
        DataGridViewCellStyle2.NullValue = Nothing
        Me.dgcolhConfirmationDate.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhConfirmationDate.HeaderText = "Confirmation Date"
        Me.dgcolhConfirmationDate.Name = "dgcolhConfirmationDate"
        Me.dgcolhConfirmationDate.ReadOnly = True
        Me.dgcolhConfirmationDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhConfirmationDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhConfirmationDate.Visible = False
        '
        'dgcolhProbationFromDate
        '
        DataGridViewCellStyle3.NullValue = Nothing
        Me.dgcolhProbationFromDate.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhProbationFromDate.HeaderText = "Probation From Date"
        Me.dgcolhProbationFromDate.Name = "dgcolhProbationFromDate"
        Me.dgcolhProbationFromDate.ReadOnly = True
        Me.dgcolhProbationFromDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhProbationFromDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhProbationFromDate.Visible = False
        Me.dgcolhProbationFromDate.Width = 125
        '
        'dgcolhProbationToDate
        '
        DataGridViewCellStyle4.NullValue = Nothing
        Me.dgcolhProbationToDate.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhProbationToDate.HeaderText = "Probation To Date"
        Me.dgcolhProbationToDate.Name = "dgcolhProbationToDate"
        Me.dgcolhProbationToDate.ReadOnly = True
        Me.dgcolhProbationToDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhProbationToDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhProbationToDate.Visible = False
        Me.dgcolhProbationToDate.Width = 125
        '
        'dgcolhJobTitle
        '
        Me.dgcolhJobTitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgcolhJobTitle.HeaderText = "Job Title"
        Me.dgcolhJobTitle.Name = "dgcolhJobTitle"
        Me.dgcolhJobTitle.ReadOnly = True
        Me.dgcolhJobTitle.Width = 230
        '
        'objdgcolhEmployeeId
        '
        Me.objdgcolhEmployeeId.HeaderText = "EmployeeId"
        Me.objdgcolhEmployeeId.Name = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.ReadOnly = True
        Me.objdgcolhEmployeeId.Visible = False
        '
        'frmleaveplanner_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(700, 575)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbLeavePlannerInfo)
        Me.Controls.Add(Me.Panel2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmleaveplanner_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Leave Planner"
        Me.gbLeavePlannerInfo.ResumeLayout(False)
        Me.gbLeavePlannerInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlDate.ResumeLayout(False)
        Me.pnlYear.ResumeLayout(False)
        CType(Me.nudToYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudToMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudFromYear, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbLeavePlannerInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dtpStopDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLeaveType As System.Windows.Forms.Label
    Friend WithEvents lblStopDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents objbtnAddLeaveType As eZee.Common.eZeeGradientButton
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlDate As System.Windows.Forms.Panel
    Friend WithEvents lblToDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnlYear As System.Windows.Forms.Panel
    Friend WithEvents lblFromYear As System.Windows.Forms.Label
    Friend WithEvents nudToYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblToYear As System.Windows.Forms.Label
    Friend WithEvents nudFromMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblToMonth As System.Windows.Forms.Label
    Friend WithEvents LblMonth As System.Windows.Forms.Label
    Friend WithEvents nudToMonth As System.Windows.Forms.NumericUpDown
    Friend WithEvents cboTocondition As System.Windows.Forms.ComboBox
    Friend WithEvents nudFromYear As System.Windows.Forms.NumericUpDown
    Friend WithEvents cboFromcondition As System.Windows.Forms.ComboBox
    Friend WithEvents radExpYear As System.Windows.Forms.RadioButton
    Friend WithEvents radAppointedDate As System.Windows.Forms.RadioButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents radConfirmationDate As System.Windows.Forms.RadioButton
    Friend WithEvents radProbationDate As System.Windows.Forms.RadioButton
    Friend WithEvents LblEmployeeCount As System.Windows.Forms.Label
    Friend WithEvents LblValue As System.Windows.Forms.Label
    Friend WithEvents LblFrom As System.Windows.Forms.Label
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents TxtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgColhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAppointedDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhConfirmationDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhProbationFromDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhProbationToDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJobTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
