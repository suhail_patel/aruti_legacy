﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.ComponentModel
Imports System.IO
Imports System.Xml

'Last Message Index = 4

Public Class frmleaveplanner_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmleaveplanner_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objLeavePlanner As clsleaveplanner
    Private mintleaveplannerUnkid As Integer = -1
    Private dsEmployee As DataSet

    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.
    Private mstrAdvanceFilter As String = ""
    Private mdtFromDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    Private mdtToDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    Private dvEmployee As DataView = Nothing
    Dim dsPlannerFraction As DataSet = Nothing
    'Pinkal (30-Jun-2016) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintleaveplannerUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintleaveplannerUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboLeaveType.BackColor = GUI.ColorComp
            txtRemarks.BackColor = GUI.ColorOptional
            TxtSearch.BackColor = GUI.ColorOptional
            cboGender.BackColor = GUI.ColorOptional
            nudFromYear.BackColor = GUI.ColorOptional
            nudFromMonth.BackColor = GUI.ColorOptional
            nudToYear.BackColor = GUI.ColorOptional
            nudToMonth.BackColor = GUI.ColorOptional
            cboFromcondition.BackColor = GUI.ColorOptional
            cboFromcondition.BackColor = GUI.ColorOptional
            cboTocondition.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboLeaveType.SelectedValue = objLeavePlanner._Leavetypeunkid

            If mintleaveplannerUnkid > 0 Then
                dtpStartDate.Value = objLeavePlanner._Startdate.Date
                dtpStopDate.Value = objLeavePlanner._Stopdate.Date
            Else

                If FinancialYear._Object._Database_End_Date < ConfigParameter._Object._CurrentDateAndTime.Date.Date Then
                    dtpStartDate.Value = FinancialYear._Object._Database_End_Date
                    dtpStopDate.Value = FinancialYear._Object._Database_End_Date

                ElseIf FinancialYear._Object._Database_End_Date > ConfigParameter._Object._CurrentDateAndTime.Date.Date Then
                    dtpStartDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpStopDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date

                End If

            End If

            txtRemarks.Text = objLeavePlanner._Remarks



            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.

            '            Dim objEmployee As New clsEmployee_Master

            '            'S.SANDEEP [04 JUN 2015] -- START
            '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '            'objEmployee._Employeeunkid = objLeavePlanner._Employeeunkid
            '            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLeavePlanner._Employeeunkid
            '            'S.SANDEEP [04 JUN 2015] -- END

            '            If menAction = enAction.EDIT_ONE Then
            '                cboGender.Enabled = False
            '            End If

            '            cboGender.SelectedValue = objEmployee._Gender

            '            Select Case objLeavePlanner._Analysisrefid

            '                Case CInt(enAnalysisRef.Employee)
            '                    rabEmployee.Checked = True
            '                    If mintleaveplannerUnkid > 0 Then
            '                        rabSection.Enabled = False
            '                        rabDepartment.Enabled = False
            '                        rabJob.Enabled = False
            '                    End If

            'EmployeeCheck:      If lstList.Items.Count <= 0 Then Exit Sub
            '                    For i As Integer = 0 To lstList.Items.Count - 1
            '                        If CInt(lstList.Items(i).Tag) = objLeavePlanner._Employeeunkid Then
            '                            lstList.Items(i).Checked = True
            '                            Exit For
            '                        End If
            '                    Next


            '                Case CInt(enAnalysisRef.Section)
            '                    rabSection.Checked = True
            '                    If lstGroup.Items.Count <= 0 Then Exit Sub

            '                    For i As Integer = 0 To lstGroup.Items.Count - 1
            '                        If CInt(lstGroup.Items(i).Tag) = objEmployee._Sectionunkid Then
            '                            lstGroup.Items(i).Checked = True
            '                            Exit For
            '                        End If
            '                    Next

            '                    If mintleaveplannerUnkid > 0 Then
            '                        rabEmployee.Enabled = False
            '                        rabDepartment.Enabled = False
            '                        rabJob.Enabled = False
            '                    End If

            '                    GoTo EmployeeCheck

            '                Case CInt(enAnalysisRef.DeptInDeptGroup)
            '                    rabDepartment.Checked = True
            '                    If lstGroup.Items.Count <= 0 Then Exit Sub

            '                    For i As Integer = 0 To lstGroup.Items.Count - 1
            '                        If CInt(lstGroup.Items(i).Tag) = objEmployee._Departmentunkid Then
            '                            lstGroup.Items(i).Checked = True
            '                            Exit For
            '                        End If
            '                    Next

            '                    If mintleaveplannerUnkid > 0 Then
            '                        rabEmployee.Enabled = False
            '                        rabSection.Enabled = False
            '                        rabJob.Enabled = False
            '                    End If

            '                    GoTo EmployeeCheck

            '                Case CInt(enAnalysisRef.JobInJobGroup)
            '                    rabJob.Checked = True
            '                    If lstGroup.Items.Count <= 0 Then Exit Sub

            '                    For i As Integer = 0 To lstGroup.Items.Count - 1
            '                        If CInt(lstGroup.Items(i).Tag) = objEmployee._Jobunkid Then
            '                            lstGroup.Items(i).Checked = True
            '                            Exit For
            '                        End If
            '                    Next


            '                    If mintleaveplannerUnkid > 0 Then
            '                        rabEmployee.Enabled = False
            '                        rabSection.Enabled = False
            '                        rabDepartment.Enabled = False
            '                    End If

            '                    GoTo EmployeeCheck

            '            End Select

            '            If lstGroup.Items.Count = lstGroup.CheckedItems.Count Then
            '                objchkSelectallGroup.Checked = True
            '            End If


            '            If lstList.Items.Count = lstList.CheckedItems.Count Then
            '                objchkSelectallList.Checked = True
            '            End If

            If menAction = enAction.EDIT_ONE Then
                Dim objPlannerFraction As New clsplannerday_fraction
                dsPlannerFraction = objPlannerFraction.GetList("List", mintleaveplannerUnkid, True)
                FillList()
            End If


            'Pinkal (30-Jun-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub


    'Pinkal (27-Aug-2021) -- Start
    'Enhancement On Leave Planner -   Working on Leave Planner Enhancement suggested by sales team.
    'Private Sub SetValue()
    Private Sub SetValue(ByRef dsAllocationEmployee As DataSet, ByRef dtErrorEmp As DataTable)
        'Pinkal (27-Aug-2021) -- End
        Try
            objLeavePlanner._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
            objLeavePlanner._Startdate = dtpStartDate.Value.Date
            objLeavePlanner._Stopdate = dtpStopDate.Value.Date
            objLeavePlanner._Remarks = txtRemarks.Text.Trim
            objLeavePlanner._Userunkid = User._Object._Userunkid


            'Pinkal (27-Aug-2021) -- Start
            'Enhancement On Leave Planner -   Working on Leave Planner Enhancement suggested by sales team.

            Dim dtEmployee As DataTable = New DataView(dsEmployee.Tables(0), "ischecked = True", "", DataViewRowState.CurrentRows).ToTable()

            Dim dtAllocationEmployee As DataTable = Nothing
            If ConfigParameter._Object._MaxEmpPlannedLeave > 0 Then
                Dim mdtStartDate As DateTime = New DateTime(dtpStartDate.Value.Year, dtpStartDate.Value.Month, 1)
                Dim mdtEndDate As DateTime = New DateTime(dtpStartDate.Value.Year, dtpStartDate.Value.Month, DateTime.DaysInMonth(dtpStartDate.Value.Year, dtpStartDate.Value.Month))

                dtAllocationEmployee = objLeavePlanner.GetEmployeeFromAllocation(User._Object._Userunkid, Company._Object._Companyunkid _
                                                                                                                                             , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
                                                                                                                                             , mdtStartDate, mdtEndDate, dtEmployee, dtErrorEmp)
            Else
                dtAllocationEmployee = dtEmployee.Copy()
            End If

            'Dim dtTable As DataTable = New DataView(dsEmployee.Tables(0), "ischecked = True", "", DataViewRowState.CurrentRows).ToTable(True, "employeeunkid", "employeecode", "name")
            Dim dtTable As DataTable = New DataView(dtAllocationEmployee, "ischecked = True", "", DataViewRowState.CurrentRows).ToTable(True, "employeeunkid", "employeecode", "name")

            'Pinkal (27-Aug-2021) -- End

            Dim dsList As New DataSet
            dsList.Tables.Add(dtTable)
            Dim xmlStr As String = GetXML(dsList)
            Dim objPlannerFraction As New clsplannerday_fraction
            Dim dtEmployeeList As DataTable = objPlannerFraction.GetPlannerDayFraction(xmlStr, CInt(cboLeaveType.SelectedValue), dtpStartDate.Value.Date, dtpStopDate.Value.Date)

            If menAction = enAction.EDIT_ONE Then
                Dim emplist = dtEmployeeList.AsEnumerable
                Dim empPlannerList = dsPlannerFraction.Tables(0).AsEnumerable

                'RIGHT JOIN WITH PLANNER LIST TO DEFAULT GENERATE DATATABLE AND VOID NULL ROW FROM PLANNER LIST DATATABLE
                Dim rows = From r1 In empPlannerList Group Join r2 In emplist On r1.Field(Of Integer)("employeeunkid") Equals r2.Field(Of Integer)("employeeunkid") _
                                 And r1.Field(Of String)("leavedate") Equals r2.Field(Of String)("leavedate") Into cnt = Group _
                                 From dr In cnt.DefaultIfEmpty() Where dr Is Nothing OrElse dr Is DBNull.Value Select r1

                rows.ToList.ForEach(Function(x) objPlannerFraction.UpdateRow(x, "D"))
                dsPlannerFraction.AcceptChanges()

                'JOIN WITH DEFAULT GENERATE LIST TO PLANNER LIST DATATABLE AND DELETE COMMON ROW FROM DEFAULT GENERATED LIST. 
                Dim rows1 = From r1 In emplist Join r2 In empPlannerList On r1.Field(Of Integer)("employeeunkid") Equals r2.Field(Of Integer)("employeeunkid") _
                                   And r1.Field(Of String)("leavedate") Equals r2.Field(Of String)("leavedate") Where r1.Field(Of String)("AUD") <> "D" AndAlso r1.Field(Of String)("AUD") <> "" _
                                   Select r1


                rows1.ToList.ForEach(Function(x) objPlannerFraction.DeleteRow(x))
                dtEmployeeList.AcceptChanges()
                dtEmployeeList = dtEmployeeList.AsEnumerable.Union(rows.ToList().AsEnumerable()).CopyToDataTable

            End If

            objLeavePlanner._PlannerFraction = dtEmployeeList


            'Pinkal (27-Aug-2021) -- Start
            'Enhancement On Leave Planner -   Working on Leave Planner Enhancement suggested by sales team.
            If dtErrorEmp IsNot Nothing AndAlso dtErrorEmp.Rows.Count > 0 Then
                For Each dr As DataRow In dtErrorEmp.Rows
                    Dim xEmployeeId As Integer = CInt(dr("employeeunkid"))
                    Dim drRow = dgEmployee.Rows.Cast(Of DataGridViewRow).Where(Function(x) CInt(x.Cells(objdgcolhEmployeeId.Index).Value) = xEmployeeId).DefaultIfEmpty
                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                        drRow(0).DefaultCellStyle.ForeColor = Color.Red
                    End If
                Next
            End If

            dsAllocationEmployee.Tables.Add(dtAllocationEmployee)
            'Pinkal (27-Aug-2021) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim dtFill As DataTable = Nothing
        Try

            'FOR LEAVE TYPE
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.GetList("LeaveType", True)
            dtFill = New DataView(dsFill.Tables("LeaveType"), "ispaid=1", "", DataViewRowState.CurrentRows).ToTable

            Dim drRow As DataRow = Nothing
            drRow = dtFill.NewRow
            drRow("leavetypeunkid") = 0
            drRow("leavename") = "Select"
            dtFill.Rows.InsertAt(drRow, 0)

            cboLeaveType.DataSource = dtFill
            cboLeaveType.DisplayMember = "leavename"
            cboLeaveType.ValueMember = "leavetypeunkid"

            Dim objMaster As New clsMasterData
            dsFill = objMaster.getGenderList("Gender", True)
            cboGender.DisplayMember = "Name"
            cboGender.ValueMember = "id"
            cboGender.DataSource = dsFill.Tables(0)

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsFill = objMaster.GetCondition(True)
            dsFill = objMaster.GetCondition(True, True, False, False, False)
            'Nilay (10-Nov-2016) -- End
            cboFromcondition.DisplayMember = "Name"
            cboFromcondition.ValueMember = "id"
            cboFromcondition.DataSource = dsFill.Tables(0)

            cboTocondition.DisplayMember = "Name"
            cboTocondition.ValueMember = "id"
            cboTocondition.DataSource = dsFill.Tables(0).Copy


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            objbtnAddLeaveType.Enabled = User._Object.Privilege._AddLeaveType
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.

    Private Sub FillList()
        Dim strSearching As String = ""
        Dim objEmployee As New clsEmployee_Master
        Try

            Dim dtDate1, dtDate2 As Date : Dim blnReinstatement As Boolean = False
            dtDate1 = Nothing : dtDate2 = Nothing
            If radAppointedDate.Checked = True Then
                dtDate1 = dtpDate1.Value.Date
                dtDate2 = dtpDate2.Value.Date
            Else
                dtDate1 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                dtDate2 = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                blnReinstatement = True
            End If

            If CInt(cboGender.SelectedValue) > 0 Then
                strSearching &= "AND hremployee_master.gender = " & CInt(cboGender.SelectedValue) & " "
            End If

            If radExpYear.Checked = True Then
                If nudFromYear.Value > 0 Or nudFromMonth.Value > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboFromcondition.Text & " '" & eZeeDate.convertDate(mdtToDate) & "' "
                End If

                If nudToYear.Value > 0 Or nudToMonth.Value > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboTocondition.Text & " '" & eZeeDate.convertDate(mdtFromDate) & "' "
                End If
            End If

            If radProbationDate.Checked Then
                strSearching &= "AND Convert(char(8),EPROB.probation_from_date,112) >= '" & eZeeDate.convertDate(dtpDate1.Value.Date) & "' AND Convert(char(8),EPROB.probation_to_date,112) <= '" & eZeeDate.convertDate(dtpDate2.Value.Date) & "' "
            End If

            If radConfirmationDate.Checked Then
                strSearching &= "AND Convert(char(8),ECNF.confirmation_date,112) >= '" & eZeeDate.convertDate(dtpDate1.Value.Date) & "' AND Convert(char(8),ECNF.confirmation_date,112) <= '" & eZeeDate.convertDate(dtpDate2.Value.Date) & "' "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter
            End If

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Trim.Substring(3)
            End If


            If menAction = enAction.ADD_CONTINUE OrElse menAction = enAction.ADD_ONE Then

                dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               dtDate1, _
                                               dtDate2, _
                                               ConfigParameter._Object._UserAccessModeSetting, True, _
                                               ConfigParameter._Object._IsIncludeInactiveEmp, _
                                               "Employee", _
                                               ConfigParameter._Object._ShowFirstAppointmentDate, , , strSearching, blnReinstatement)
            Else
                dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           dtDate1, _
                                           dtDate2, _
                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                           ConfigParameter._Object._IsIncludeInactiveEmp, _
                                           "Employee", _
                                           ConfigParameter._Object._ShowFirstAppointmentDate, objLeavePlanner._Employeeunkid, , strSearching, blnReinstatement)
            End If




            'Pinkal (27-Aug-2021) -- Start
            'Enhancement On Leave Planner -   Working on Leave Planner Enhancement suggested by sales team.
            'If dsEmployee.Tables(0).Columns.Contains("ischecked") = False Then
            'dsEmployee.Tables(0).Columns.Add("ischecked", Type.GetType("System.Boolean"))
            'dsEmployee.Tables(0).Columns("ischecked").DefaultValue = False
            'End If
            If dsEmployee.Tables(0).Columns.Contains("ischecked") = False Then
                Dim dc As New DataColumn("ischecked")
                dc.DataType = Type.GetType("System.Boolean")
                dc.DefaultValue = False
                dsEmployee.Tables(0).Columns.Add(dc)
            End If
            'Pinkal (27-Aug-2021) -- End


            SetDataSource()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDataSource()
        Try
            dvEmployee = dsEmployee.Tables(0).DefaultView
            dgEmployee.AutoGenerateColumns = False
            dgEmployee.DataSource = dvEmployee
            objSelect.DataPropertyName = "ischecked"
            dgColhEmpCode.DataPropertyName = "employeecode"
            dgColhEmployee.DataPropertyName = "name"
            dgcolhJobTitle.DataPropertyName = "job_name"
            dgcolhAppointedDate.DataPropertyName = "appointeddate"
            dgcolhConfirmationDate.DataPropertyName = "confirmation_date"
            dgcolhProbationFromDate.DataPropertyName = "probation_from_date"
            dgcolhProbationToDate.DataPropertyName = "probation_to_date"


            'Pinkal (27-Aug-2021) -- Start
            'Enhancement On Leave Planner -   Working on Leave Planner Enhancement suggested by sales team.
            objdgcolhEmployeeId.DataPropertyName = "employeeunkid"
            'Pinkal (27-Aug-2021) -- End


            If radAppointedDate.Checked Then
                dgcolhConfirmationDate.Visible = False
                dgcolhProbationFromDate.Visible = False
                dgcolhProbationToDate.Visible = False
                dgcolhAppointedDate.Visible = True
                dgcolhAppointedDate.DisplayIndex = dgColhEmployee.DisplayIndex + 1

            ElseIf radConfirmationDate.Checked Then
                dgcolhAppointedDate.Visible = False
                dgcolhProbationFromDate.Visible = False
                dgcolhProbationToDate.Visible = False
                dgcolhConfirmationDate.Visible = True
                dgcolhConfirmationDate.DisplayIndex = dgColhEmployee.DisplayIndex + 1

            ElseIf radProbationDate.Checked Then
                dgcolhAppointedDate.Visible = False
                dgcolhConfirmationDate.Visible = False
                dgcolhProbationFromDate.Visible = True
                dgcolhProbationToDate.Visible = True
                dgcolhProbationFromDate.DisplayIndex = dgColhEmployee.DisplayIndex + 1
                dgcolhProbationToDate.DisplayIndex = dgcolhProbationFromDate.DisplayIndex + 1
            End If
            'Pinkal (07-Jul-2016) -- End


            LblEmployeeCount.Text = Language.getMessage(mstrModuleName, 7, "Employee Count : ") & dvEmployee.Count

            If radAppointedDate.Checked OrElse radConfirmationDate.Checked OrElse radProbationDate.Checked Then
                LblValue.Text = dtpDate1.Value.Date & " - " & dtpDate2.Value.Date
            Else
                LblValue.Text = mdtToDate.Date & " - " & mdtFromDate.Date
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataSource", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = dvEmployee.Table.Select("ischecked = true")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgEmployee.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgEmployee.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    'Private Sub FillList()
    '    Dim objEmployee As New clsEmployee_Master
    '    Try
    '        If lstList.CheckedItems.Count > 0 Then
    '            For i As Integer = 0 To lstList.CheckedItems.Count - 1
    '                Dim dsTemp As DataSet = Nothing


    '                If i = 0 Then
    '                    ' EVERY TIME INCLUDE ONLY ACTIVE EMPLOYEE SO DON"T GET CHANGED THE GETLIST PARAMETER

    '                    'S.SANDEEP [04 JUN 2015] -- START
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    'mdtEmployee = objEmployee.GetEmployeeList("Employee", , Not ConfigParameter._Object._IsIncludeInactiveEmp, CInt(lstList.CheckedItems(i).Tag), , , , , , , , , , , , , , , , , , True, False)

    '                    'Pinkal (06-Jan-2016) -- Start
    '                    'Enhancement - Working on Changes in SS for Leave Module.
    '                    'mdtEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
    '                    '                    User._Object._Userunkid, _
    '                    '                    FinancialYear._Object._YearUnkid, _
    '                    '                    Company._Object._Companyunkid, _
    '                    '                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                    '                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                    '                    ConfigParameter._Object._UserAccessModeSetting, _
    '                    '                    True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", False, CInt(lstList.CheckedItems(i).Tag), , , , , , , , , , , , , , , False)


    '                    mdtEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
    '                                        User._Object._Userunkid, _
    '                                        FinancialYear._Object._YearUnkid, _
    '                                        Company._Object._Companyunkid, _
    '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                        ConfigParameter._Object._UserAccessModeSetting, _
    '                                      True, True, "Employee", False, CInt(lstList.CheckedItems(i).Tag), , , , , , , , , , , , , , , False)
    '                    'Pinkal (06-Jan-2016) -- End


    '                    'S.SANDEEP [04 JUN 2015] -- END
    '                Else
    '                    ' EVERY TIME INCLUDE ONLY ACTIVE EMPLOYEE SO DON"T GET CHANGED THE GETLIST PARAMETER

    '                    'S.SANDEEP [04 JUN 2015] -- START
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    'dsTemp = objEmployee.GetEmployeeList("Employee", , Not ConfigParameter._Object._IsIncludeInactiveEmp, CInt(lstList.CheckedItems(i).Tag), , , , , , , , , , , , , , , , , , True, False)


    '                    'Pinkal (06-Jan-2016) -- Start
    '                    'Enhancement - Working on Changes in SS for Leave Module.

    '                    'dsTemp = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
    '                    '                    User._Object._Userunkid, _
    '                    '                    FinancialYear._Object._YearUnkid, _
    '                    '                    Company._Object._Companyunkid, _
    '                    '                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                    '                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                    '                    ConfigParameter._Object._UserAccessModeSetting, _
    '                    '                    True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", False, CInt(lstList.CheckedItems(i).Tag), , , , , , , , , , , , , , , False)

    '                    dsTemp = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
    '                                        User._Object._Userunkid, _
    '                                        FinancialYear._Object._YearUnkid, _
    '                                        Company._Object._Companyunkid, _
    '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                        ConfigParameter._Object._UserAccessModeSetting, _
    '                                    True, True, "Employee", False, CInt(lstList.CheckedItems(i).Tag), , , , , , , , , , , , , , , False)

    '                    'Pinkal (06-Jan-2016) -- End


    '                    'S.SANDEEP [04 JUN 2015] -- END

    '                End If


    '                If i > 0 Then
    '                    For Each drRow As DataRow In dsTemp.Tables("Employee").Rows
    '                        mdtEmployee.Tables(0).ImportRow(drRow)
    '                    Next
    '                End If

    '            Next
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub ClearSelection()
    '    Try
    '        If lstList.Items.Count > 0 Then
    '            For i As Integer = 0 To lstList.Items.Count - 1
    '                lstList.Items(i).Checked = False
    '            Next
    '        End If

    '        If lstGroup.Items.Count > 0 Then
    '            For i As Integer = 0 To lstGroup.Items.Count - 1
    '                lstGroup.Items(i).Checked = False
    '            Next
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ClearSelection", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub SetListOperation(ByVal blnOperation As Boolean)
    '    Try
    '        For Each Item As ListViewItem In lstList.Items
    '            RemoveHandler lstList.ItemChecked, AddressOf lstList_ItemChecked
    '            Item.Checked = blnOperation
    '            AddHandler lstList.ItemChecked, AddressOf lstList_ItemChecked
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetListOperation", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub SetGroupOperation(ByVal blnOperation As Boolean)
    '    Dim str As String = ""
    '    Try
    '        For Each Item As ListViewItem In lstGroup.Items
    '            RemoveHandler lstGroup.ItemChecked, AddressOf lstGroup_ItemChecked
    '            Item.Checked = blnOperation
    '            AddHandler lstGroup.ItemChecked, AddressOf lstGroup_ItemChecked
    '        Next
    '        GetEmployeeFromGroup()

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SetGroupOperation", mstrModuleName)
    '    End Try
    'End Sub

    'Pinkal (30-Jun-2016) -- End


    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.

    'Private Function GetData(ByVal dsFill As DataSet, ByVal mstrGroupId As String) As DataTable
    '    Dim dtFill As DataTable = Nothing
    '    Try

    '        Dim strSearching As String = ""

    '        If CInt(cboGender.SelectedValue) > 0 Then
    '            strSearching = " gender = " & CInt(cboGender.SelectedValue) & " AND "
    '        End If

    '        'FOR DEPARTMENT 
    '        If rabDepartment.Checked Then

    '            If mstrGroupId.Length > 0 Then

    '                If ConfigParameter._Object._IsIncludeInactiveEmp Then

    '                    If objLeavePlanner._Employeeunkid > 0 Then
    '                        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & "departmentunkid in (" & mstrGroupId & ") AND employeeunkid = " & objLeavePlanner._Employeeunkid, "", DataViewRowState.CurrentRows).ToTable
    '                    Else
    '                        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & "departmentunkid in (" & mstrGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
    '                    End If

    '                Else

    '                    If objLeavePlanner._Employeeunkid > 0 Then
    '                        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & "departmentunkid in (" & mstrGroupId & ") AND employeeunkid = " & objLeavePlanner._Employeeunkid & " AND isactive = 1", "", DataViewRowState.CurrentRows).ToTable
    '                    Else
    '                        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & "departmentunkid in (" & mstrGroupId & ") AND isactive = 1", "", DataViewRowState.CurrentRows).ToTable
    '                    End If

    '                End If

    '            End If

    '            'FOR SECTION 
    '        ElseIf rabSection.Checked Then

    '            If mstrGroupId.Length > 0 Then

    '                If ConfigParameter._Object._IsIncludeInactiveEmp Then
    '                    If objLeavePlanner._Employeeunkid > 0 Then
    '                        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & "sectionunkid in (" & mstrGroupId & ") AND employeeunkid = " & objLeavePlanner._Employeeunkid, "", DataViewRowState.CurrentRows).ToTable
    '                    Else
    '                        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & "sectionunkid in (" & mstrGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
    '                    End If

    '                Else

    '                    If objLeavePlanner._Employeeunkid > 0 Then
    '                        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & "sectionunkid in (" & mstrGroupId & ") AND employeeunkid = " & objLeavePlanner._Employeeunkid & " AND isactive = 1", "", DataViewRowState.CurrentRows).ToTable
    '                    Else
    '                        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & "sectionunkid in (" & mstrGroupId & ") AND isactive = 1 ", "", DataViewRowState.CurrentRows).ToTable
    '                    End If

    '                End If

    '            End If

    '            'FOR JOB 
    '        ElseIf rabJob.Checked Then

    '            If mstrGroupId.Length > 0 Then

    '                If ConfigParameter._Object._IsIncludeInactiveEmp Then

    '                    If objLeavePlanner._Employeeunkid > 0 Then
    '                        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & "jobunkid in (" & mstrGroupId & ") AND employeeunkid=" & objLeavePlanner._Employeeunkid, "", DataViewRowState.CurrentRows).ToTable
    '                    Else
    '                        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & "jobunkid in (" & mstrGroupId & ")", "", DataViewRowState.CurrentRows).ToTable
    '                    End If

    '                Else

    '                    If objLeavePlanner._Employeeunkid > 0 Then
    '                        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & "jobunkid in (" & mstrGroupId & ") AND employeeunkid=" & objLeavePlanner._Employeeunkid & " AND isactive = 1 ", "", DataViewRowState.CurrentRows).ToTable
    '                    Else
    '                        dtFill = New DataView(dsFill.Tables("Employee"), strSearching & "jobunkid in (" & mstrGroupId & ") AND isactive = 1 ", "", DataViewRowState.CurrentRows).ToTable
    '                    End If

    '                End If

    '            End If

    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetData", mstrModuleName)
    '    End Try
    '    Return dtFill
    'End Function

    'Private Sub GetEmployeeFromGroup()
    '    Dim strGroupId As String = String.Empty
    '    Try

    '        If dsEmployee Is Nothing Then
    '            Dim objEmployee As New clsEmployee_Master
    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'dsEmployee = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), objLeavePlanner._Employeeunkid)
    '            dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
    '                                         User._Object._Userunkid, _
    '                                         FinancialYear._Object._YearUnkid, _
    '                                         Company._Object._Companyunkid, _
    '                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                         ConfigParameter._Object._UserAccessModeSetting, True, _
    '                                         False, _
    '                                         "Employee", _
    '                                         ConfigParameter._Object._ShowFirstAppointmentDate, objLeavePlanner._Employeeunkid)
    '            'S.SANDEEP [04 JUN 2015] -- END
    '        End If

    '        Dim dtFill As DataTable = Nothing

    '        For i As Integer = 0 To lstGroup.CheckedItems.Count - 1
    '            strGroupId &= "'" & lstGroup.CheckedItems(i).Tag.ToString & "',"
    '        Next

    '        If strGroupId.Length > 0 Then
    '            strGroupId = strGroupId.Substring(0, strGroupId.Length - 1)
    '        End If

    '        dtFill = GetData(dsEmployee, strGroupId)

    '        RemoveHandler lstList.ItemChecked, AddressOf lstList_ItemChecked

    '        lstList.Items.Clear()
    '        If dtFill IsNot Nothing AndAlso dtFill.Rows.Count > 0 Then
    '            Dim lvItem As ListViewItem
    '            For i As Integer = 0 To dtFill.Rows.Count - 1
    '                lvItem = New ListViewItem
    '                lvItem.Text = dtFill.Rows(i)("name").ToString
    '                lvItem.Tag = CInt(dtFill.Rows(i)("employeeunkid"))
    '                lstList.Items.Add(lvItem)
    '            Next
    '        End If

    '        AddHandler lstList.ItemChecked, AddressOf lstList_ItemChecked

    '        If lstList.Items.Count > 0 Then
    '            objchkSelectallList.Enabled = True
    '        Else
    '            objchkSelectallList.Enabled = False
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetEmployeeFromGroup", mstrModuleName)
    '    End Try
    'End Sub

    'Pinkal (30-Jun-2016) -- End

#End Region

#Region " Form's Events "

    Private Sub frmleaveplanner_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeavePlanner = New clsleaveplanner
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            SetColor()
            FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objLeavePlanner._Leaveplannerunkid = mintleaveplannerUnkid
            End If
            'Pinkal (30-Jun-2016) -- Start
            LblValue.Text = dtpDate1.Value.Date & " - " & dtpDate2.Value.Date
            LblEmployeeCount.Text = Language.getMessage(mstrModuleName, 7, "Employee Count : ") & "0"
            GetValue()
            'Enhancement - Working on SL changes on Leave Module.
            'gbFilter.Focus()
            'If mintleaveplannerUnkid <= 0 Then
            '    rabEmployee.Checked = True
            '    rabEmployee.Focus()
            'End If
            'Pinkal (30-Jun-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmleaveplanner_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmleaveplanner_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyData = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmleaveplanner_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmleaveplanner_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmleaveplanner_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmleaveplanner_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLeavePlanner = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleaveplanner.SetMessages()
            objfrm._Other_ModuleNames = "clsleaveplanner"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "


    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try

            If radAppointedDate.Checked = True Then
                If dtpDate2.Value.Date < dtpDate1.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, To date cannot be less than From date."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            ElseIf radExpYear.Checked = True Then
                If (nudFromYear.Value > 0 Or nudFromMonth.Value > 0) And CInt(cboFromcondition.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "From Condition is compulsory information.Please Select From Condition."), enMsgBoxStyle.Information)
                    cboFromcondition.Select()
                    Exit Sub
                ElseIf (nudToYear.Value > 0 Or nudToMonth.Value > 0) And CInt(cboTocondition.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "To Condition is compulsory information.Please Select To Condition."), enMsgBoxStyle.Information)
                    cboTocondition.Select()
                    Exit Sub
                End If
            End If
            Me.Cursor = Cursors.WaitCursor
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            dtpDate1.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpDate2.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            cboLeaveType.SelectedValue = 0
            cboGender.SelectedValue = 0
            nudFromYear.Value = 0
            nudFromMonth.Value = 0
            nudToYear.Value = 0
            nudToMonth.Value = 0
            cboFromcondition.SelectedValue = 0
            cboTocondition.SelectedValue = 0
            mstrAdvanceFilter = ""
            If dsEmployee IsNot Nothing Then dsEmployee.Tables(0).Rows.Clear()
            radExpYear.Checked = False
            radAppointedDate.Checked = False
            radConfirmationDate.Checked = False
            radProbationDate.Checked = False
            chkSelectAll.Checked = False
            pnlYear.Enabled = True
            pnlDate.Enabled = True
            LblValue.Text = dtpDate1.Value.Date & " - " & dtpDate2.Value.Date
            LblEmployeeCount.Text = Language.getMessage(mstrModuleName, 7, "Employee Count : ") & "0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (30-Jun-2016) -- End


    Private Sub objbtnAddLeaveType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddLeaveType.Click
        Dim frm As New frmLeaveType_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objLeaveType As New clsleavetype_master
                dsList = objLeaveType.GetList("LVTYP", True)
                With cboLeaveType
                    .ValueMember = "leavetypeunkid"
                    .DisplayMember = "leavename"

                    Dim drRow As DataRow = dsList.Tables("LVTYP").NewRow
                    drRow("leavetypeunkid") = 0
                    drRow("leavename") = Language.getMessage(mstrModuleName, 3, "Select")
                    dsList.Tables("LVTYP").Rows.InsertAt(drRow, 0)

                    .DataSource = dsList.Tables("LVTYP")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objLeaveType = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddLeaveType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If dsEmployee IsNot Nothing Then
                Dim drRow() As DataRow = dsEmployee.Tables(0).Select("ischecked = True")
                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Check atleast One Employee from the List."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            If CInt(cboLeaveType.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type."), enMsgBoxStyle.Information)
                cboLeaveType.Select()
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            'Pinkal (30-Jun-2016) -- End


            'Pinkal (27-Aug-2021) -- Start
            'Enhancement On Leave Planner -   Working on Leave Planner Enhancement suggested by sales team.
            'SetValue()

            Dim drGridRow = dgEmployee.Rows.Cast(Of DataGridViewRow).Where(Function(x) x.DefaultCellStyle.ForeColor <> Color.Black).DefaultIfEmpty

            If drGridRow IsNot Nothing AndAlso drGridRow.Count > 0 Then
                For Each dr As DataGridViewRow In drGridRow
                    dr.DefaultCellStyle.ForeColor = Color.Black
                Next
            End If

            Dim xCount As Integer = dsEmployee.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischecked") = True).Count
            If ConfigParameter._Object._MaxEmpPlannedLeave > 0 AndAlso ConfigParameter._Object._MaxEmpPlannedLeave < xCount Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, you cannot add this plan now. Checked Employee(s) unit has reached the maximum leave plans allowed at a time."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            Dim dsAllocationEmployee As New DataSet
            Dim dtErrorEmp As DataTable = dsEmployee.Tables(0).Clone
            SetValue(dsAllocationEmployee, dtErrorEmp)

            'Pinkal (27-Aug-2021) -- End

            
'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objLeavePlanner._FormName = mstrModuleName
            objLeavePlanner._LoginEmployeeunkid = 0
            objLeavePlanner._ClientIP = getIP()
            objLeavePlanner._HostName = getHostName()
            objLeavePlanner._FromWeb = False
            objLeavePlanner._AuditUserId = User._Object._Userunkid
objLeavePlanner._CompanyUnkid = Company._Object._Companyunkid
            objLeavePlanner._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objLeavePlanner.Update(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            Else
                'Pinkal (27-Aug-2021) -- Start
                'Enhancement On Leave Planner -   Working on Leave Planner Enhancement suggested by sales team.
                'blnFlag = objLeavePlanner.Insert(dsEmployee, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                blnFlag = objLeavePlanner.Insert(dsAllocationEmployee, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Pinkal (27-Aug-2021) -- End
            End If


            If blnFlag = False And objLeavePlanner._Message <> "" Then
                eZeeMsgBox.Show(objLeavePlanner._Message, enMsgBoxStyle.Information)
                'Pinkal (27-Aug-2021) -- Start
                'Enhancement On Leave Planner -   Working on Leave Planner Enhancement suggested by sales team.
            Else
                If dtErrorEmp IsNot Nothing AndAlso dtErrorEmp.Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, some of the employee(s) cannot add this plan now. some of the employee(s) unit has reached the maximum leave plans allowed at a time for the selected dates and will be highlighted in red."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'Pinkal (27-Aug-2021) -- End

            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objLeavePlanner = Nothing
                    objLeavePlanner = New clsleaveplanner
                    GetValue()

                    'Pinkal (30-Jun-2016) -- Start
                    'Enhancement - Working on SL changes on Leave Module.
                    'ClearSelection()
                    'Pinkal (30-Jun-2016) -- End
                    chkSelectAll.Checked = False
                    radExpYear.Checked = False
                    radAppointedDate.Checked = False
                    radConfirmationDate.Checked = False
                    radProbationDate.Checked = False
                    'Pinkal (30-Jun-2016) -- Start
                    'Enhancement - Working on SL changes on Leave Module.
                    'rabEmployee.Select()
                    'rabEmployee.Checked = True
                    'Pinkal (30-Jun-2016) -- End
                Else
                    mintleaveplannerUnkid = objLeavePlanner._Leaveplannerunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

        'Pinkal (30-Jun-2016) -- Start
        'Enhancement - Working on SL changes on Leave Module.
        'lstGroup.Items.Clear()
        'lstList.Items.Clear()
        'dsEmployee.Dispose()
        'Pinkal (30-Jun-2016) -- End
        Me.Close()
    End Sub

#End Region

    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.

    '#Region "CheckBox's Event"

    '    Private Sub chkSelectallGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectallGroup.CheckedChanged
    '        Try
    '            If lstGroup.Items.Count = 0 Then Exit Sub
    '            SetGroupOperation(objchkSelectallGroup.Checked)

    '            If objchkSelectallGroup.Checked = False Then objchkSelectallList.Checked = False

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "chkSelectallGroup_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub chkSelectallList_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectallList.CheckedChanged
    '        Try
    '            If lstList.Items.Count = 0 Then Exit Sub
    '            SetListOperation(objchkSelectallList.Checked)
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "chkSelectallList_CheckedChanged", mstrModuleName)
    '        End Try
    '    End Sub

    '#End Region

    'Pinkal (30-Jun-2016) -- End

#Region "Radio Button's Event"

    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.


    'Private Sub rabEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        lstList.Items.Clear()
    '        lstGroup.Items.Clear()

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'If rabEmployee.Checked Then
    '        '    Dim objEmployee As New clsEmployee_Master



    '        '    Dim dtFill As DataTable = Nothing
    '        '    If objLeavePlanner Is Nothing Then Exit Sub

    '        '    If objLeavePlanner._Employeeunkid > 0 Then
    '        '        dtFill = New DataView(dsEmployee.Tables("Employee"), "employeeunkid=" & objLeavePlanner._Employeeunkid, "", DataViewRowState.CurrentRows).ToTable
    '        '    Else


    '        '    End If

    '        If rabEmployee.Checked Then
    '            Dim objEmployee As New clsEmployee_Master
    '            Dim strSearch As String = String.Empty
    '            Dim dtFill As DataTable = Nothing
    '            If objLeavePlanner._Employeeunkid > 0 Then
    '                strSearch = " hremployee_master.employeeunkid = '" & objLeavePlanner._Employeeunkid & "' "
    '            Else
    '                If CInt(cboGender.SelectedValue) > 0 Then
    '                    strSearch = " hremployee_master.gender = '" & CInt(cboGender.SelectedValue) & "' "
    '                End If
    '            End If

    '            dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
    '                                             User._Object._Userunkid, _
    '                                             FinancialYear._Object._YearUnkid, _
    '                                             Company._Object._Companyunkid, _
    '                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                             ConfigParameter._Object._UserAccessModeSetting, True, _
    '                                             False, _
    '                                             "Employee", _
    '                                             ConfigParameter._Object._ShowFirstAppointmentDate, , , strSearch)

    '            dtFill = dsEmployee.Tables(0)
    '            'S.SANDEEP [04 JUN 2015] -- END


    '            RemoveHandler lstList.ItemChecked, AddressOf lstList_ItemChecked

    '            If dtFill.Rows.Count > 0 Then
    '                Dim lvItem As ListViewItem
    '                lstList.Items.Clear()
    '                For i As Integer = 0 To dtFill.Rows.Count - 1
    '                    lvItem = New ListViewItem
    '                    lvItem.Text = dtFill.Rows(i)("name").ToString
    '                    lvItem.Tag = CInt(dtFill.Rows(i)("employeeunkid"))
    '                    lstList.Items.Add(lvItem)
    '                Next
    '            End If

    '            AddHandler lstList.ItemChecked, AddressOf lstList_ItemChecked

    '        End If

    '        If lstList.Items.Count > 0 Then
    '            objchkSelectallList.Enabled = True
    '        Else
    '            objchkSelectallList.Enabled = False
    '        End If

    '        objchkSelectallGroup.Enabled = False

    '        objchkSelectallGroup.Checked = False
    '        objchkSelectallList.Checked = False

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "rabEmployee_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub rabSection_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        lstList.Items.Clear()
    '        If rabSection.Checked Then
    '            Dim objSection As New clsSections
    '            Dim dsFill As DataSet = objSection.GetList("Section", True)
    '            Dim dtFill As DataTable = Nothing

    '            If objLeavePlanner._Employeeunkid > 0 Then
    '                Dim objemployee As New clsEmployee_Master

    '                'S.SANDEEP [04 JUN 2015] -- START
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                'objemployee._Employeeunkid = objLeavePlanner._Employeeunkid
    '                objemployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLeavePlanner._Employeeunkid
    '                'S.SANDEEP [04 JUN 2015] -- END

    '                dtFill = New DataView(dsFill.Tables("Section"), "sectionunkid=" & objemployee._Sectionunkid, "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtFill = dsFill.Tables("Section")
    '            End If

    '            RemoveHandler lstGroup.ItemChecked, AddressOf lstGroup_ItemChecked

    '            If dtFill.Rows.Count > 0 Then
    '                Dim lvItem As ListViewItem
    '                lstGroup.Items.Clear()
    '                For i As Integer = 0 To dtFill.Rows.Count - 1
    '                    lvItem = New ListViewItem
    '                    lvItem.Text = dtFill.Rows(i)("name").ToString
    '                    lvItem.Tag = CInt(dtFill.Rows(i)("sectionunkid"))
    '                    lstGroup.Items.Add(lvItem)
    '                Next
    '            End If

    '            AddHandler lstGroup.ItemChecked, AddressOf lstGroup_ItemChecked

    '        End If


    '        If lstGroup.Items.Count > 0 Then
    '            objchkSelectallGroup.Enabled = True
    '        Else
    '            objchkSelectallGroup.Enabled = False
    '        End If

    '        objchkSelectallGroup.Checked = False
    '        objchkSelectallList.Checked = False

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "rabSection_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub rabDepartment_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        lstList.Items.Clear()
    '        If rabDepartment.Checked Then
    '            Dim objDepartment As New clsDepartment
    '            Dim dsFill As DataSet = objDepartment.GetList("Department", True)
    '            Dim dtFill As DataTable = Nothing

    '            If objLeavePlanner._Employeeunkid > 0 Then
    '                Dim objEmployee As New clsEmployee_Master

    '                'S.SANDEEP [04 JUN 2015] -- START
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                'objEmployee._Employeeunkid = objLeavePlanner._Employeeunkid
    '                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLeavePlanner._Employeeunkid
    '                'S.SANDEEP [04 JUN 2015] -- END

    '                dtFill = New DataView(dsFill.Tables("Department"), "departmentunkid =" & objEmployee._Departmentunkid, "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtFill = dsFill.Tables("Department")
    '            End If
    '            RemoveHandler lstGroup.ItemChecked, AddressOf lstGroup_ItemChecked

    '            If dtFill.Rows.Count > 0 Then
    '                Dim lvItem As ListViewItem
    '                lstGroup.Items.Clear()
    '                For i As Integer = 0 To dtFill.Rows.Count - 1
    '                    lvItem = New ListViewItem
    '                    lvItem.Text = dtFill.Rows(i)("name").ToString
    '                    lvItem.Tag = CInt(dtFill.Rows(i)("departmentunkid"))
    '                    lstGroup.Items.Add(lvItem)
    '                Next
    '            End If

    '            AddHandler lstGroup.ItemChecked, AddressOf lstGroup_ItemChecked

    '        End If

    '        If lstGroup.Items.Count > 0 Then
    '            objchkSelectallGroup.Enabled = True
    '        Else
    '            objchkSelectallGroup.Enabled = False
    '        End If

    '        objchkSelectallGroup.Checked = False
    '        objchkSelectallList.Checked = False

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "rabDepartment_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub rabJob_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        lstList.Items.Clear()
    '        If rabJob.Checked Then
    '            Dim objJob As New clsJobs
    '            Dim dsFill As DataSet = objJob.GetList("Job", True)
    '            Dim dtFill As DataTable = Nothing
    '            If objLeavePlanner._Employeeunkid > 0 Then
    '                Dim objEmployee As New clsEmployee_Master

    '                'S.SANDEEP [04 JUN 2015] -- START
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                'objEmployee._Employeeunkid = objLeavePlanner._Employeeunkid
    '                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLeavePlanner._Employeeunkid
    '                'S.SANDEEP [04 JUN 2015] -- END

    '                dtFill = New DataView(dsFill.Tables("Job"), "jobunkid =" & objEmployee._Jobunkid, "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtFill = dsFill.Tables("Job")
    '            End If

    '            RemoveHandler lstGroup.ItemChecked, AddressOf lstGroup_ItemChecked

    '            If dtFill.Rows.Count > 0 Then
    '                Dim lvItem As ListViewItem
    '                lstGroup.Items.Clear()
    '                For i As Integer = 0 To dtFill.Rows.Count - 1

    '                    lvItem = New ListViewItem
    '                    lvItem.Text = dtFill.Rows(i)("JobName").ToString
    '                    lvItem.Tag = CInt(dtFill.Rows(i)("jobunkid"))
    '                    lstGroup.Items.Add(lvItem)

    '                Next
    '            End If

    '            AddHandler lstGroup.ItemChecked, AddressOf lstGroup_ItemChecked

    '        End If


    '        If lstGroup.Items.Count > 0 Then
    '            objchkSelectallGroup.Enabled = True
    '        Else
    '            objchkSelectallGroup.Enabled = False
    '        End If

    '        objchkSelectallGroup.Checked = False
    '        objchkSelectallList.Checked = False

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "rabJob_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub radExpYear_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radExpYear.CheckedChanged, radAppointedDate.CheckedChanged _
                                                                                                                                                                              , radProbationDate.CheckedChanged, radConfirmationDate.CheckedChanged
        Try

            If CType(sender, RadioButton).Name.ToUpper = "RADEXPYEAR" Then
                pnlDate.Enabled = False : pnlYear.Enabled = True
            ElseIf CType(sender, RadioButton).Name.ToUpper = "RADAPPOINTEDDATE" Then
                pnlDate.Enabled = True : pnlYear.Enabled = False
            ElseIf CType(sender, RadioButton).Name.ToUpper = "RADPROBATIONDATE" Then
                pnlDate.Enabled = True : pnlYear.Enabled = False
            ElseIf CType(sender, RadioButton).Name.ToUpper = "RADCONFIRMATIONDATE" Then
                pnlDate.Enabled = True : pnlYear.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radExpYear_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (30-Jun-2016) -- End


#End Region

    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.

    '#Region "ListView's Event"

    '    Private Sub lstGroup_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '        Try

    '            If e.Item.Tag Is Nothing Or e.Item.Tag.ToString() = "" Then Exit Sub


    '            RemoveHandler objchkSelectallGroup.CheckedChanged, AddressOf chkSelectallGroup_CheckedChanged
    '            If lstGroup.CheckedItems.Count <= 0 Then
    '                objchkSelectallGroup.CheckState = CheckState.Unchecked

    '            ElseIf lstGroup.CheckedItems.Count < lstGroup.Items.Count Then
    '                objchkSelectallGroup.CheckState = CheckState.Indeterminate

    '            ElseIf lstGroup.CheckedItems.Count = lstGroup.Items.Count Then
    '                objchkSelectallGroup.CheckState = CheckState.Checked

    '            End If
    '            AddHandler objchkSelectallGroup.CheckedChanged, AddressOf chkSelectallGroup_CheckedChanged

    '            If lstGroup.CheckedItems.Count = 0 Then lstList.Items.Clear() : objchkSelectallList.Enabled = False : Exit Sub
    '            GetEmployeeFromGroup()

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lstGroup_ItemChecked", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub lstList_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
    '        Try
    '            RemoveHandler objchkSelectallList.CheckedChanged, AddressOf chkSelectallList_CheckedChanged
    '            If lstList.CheckedItems.Count <= 0 Then
    '                objchkSelectallList.CheckState = CheckState.Unchecked

    '            ElseIf lstList.CheckedItems.Count < lstList.Items.Count Then
    '                objchkSelectallList.CheckState = CheckState.Indeterminate

    '            ElseIf lstList.CheckedItems.Count = lstList.Items.Count Then
    '                objchkSelectallList.CheckState = CheckState.Checked

    '            End If
    '            AddHandler objchkSelectallList.CheckedChanged, AddressOf chkSelectallList_CheckedChanged
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "lstList_ItemChecked", mstrModuleName)
    '        End Try
    '    End Sub

    '#End Region

    'Pinkal (30-Jun-2016) -- End

#Region "DatePicker's Event"

    Private Sub dtpStartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpStartDate.ValueChanged
        Try
            dtpStopDate.MinDate = dtpStartDate.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpStartDate_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Dropdown Event"

    Private Sub cboGender_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGender.SelectedIndexChanged
        Try

            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.

            'If rabEmployee.Checked Then
            '    rabEmployee_CheckedChanged(sender, e)
            'ElseIf rabSection.Checked Then
            '    rabSection_CheckedChanged(sender, e)
            'ElseIf rabDepartment.Checked Then
            '    rabDepartment_CheckedChanged(sender, e)
            'ElseIf rabJob.Checked Then
            '    rabJob_CheckedChanged(sender, e)
            'End If

            'Pinkal (30-Jun-2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGender_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (30-Jun-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.

#Region "LinkButton Events"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            mstrAdvanceFilter = ""
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "NumericUpdown Event"

    Private Sub nudFromYear_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudFromYear.ValueChanged, nudFromMonth.Validated, nudFromMonth.ValueChanged, nudFromMonth.Validated
        Try
            mdtFromDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(nudFromYear.Value) * -1).AddMonths(CInt(nudFromMonth.Value) * -1)
            nudToYear.Minimum = nudFromYear.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudFromYear_ValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub nudToYear_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudToYear.ValueChanged, nudToYear.Validated, nudToMonth.ValueChanged, nudToMonth.Validated
        Try
            mdtToDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(nudToYear.Value) * -1).AddMonths(CInt(nudToMonth.Value) * -1)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudToYear_ValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Private Sub TxtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtSearch.TextChanged
        Try
            If dvEmployee.Table.Rows.Count > 0 Then
                dvEmployee.RowFilter = "employeecode like '%" & TxtSearch.Text.Trim & "%'  OR name like '%" & TxtSearch.Text.Trim & "%'"
                dgEmployee.Refresh()
            End If

            If dgEmployee.Rows.Count > 0 Then
                chkSelectAll.Enabled = True
            Else
                chkSelectAll.Enabled = False
                chkSelectAll.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "TxtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox's Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try

            If dsEmployee.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRowView In dvEmployee
                    RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr("ischecked") = chkSelectAll.Checked
                    AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGrid Event"

    Private Sub dgEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick, dgEmployee.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objSelect.Index Then

                If dgEmployee.IsCurrentCellDirty Then
                    dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    dvEmployee.Table.AcceptChanges()
                End If
                SetCheckBoxValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

    'Pinkal (07-Jul-2016) -- Start
    'Enhancement - Working on SL changes on Leave Module.

    Private Sub dgEmployee_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgEmployee.CellPainting
        Try
            If e.RowIndex < 0 Then Exit Sub
            If dgEmployee.Columns(e.ColumnIndex).DataPropertyName = "appointeddate" OrElse dgEmployee.Columns(e.ColumnIndex).DataPropertyName = "confirmation_date" _
             OrElse dgEmployee.Columns(e.ColumnIndex).DataPropertyName = "probation_from_date" OrElse dgEmployee.Columns(e.ColumnIndex).DataPropertyName = "probation_to_date" Then
                If dgEmployee.Rows(e.RowIndex).Cells(e.ColumnIndex).Value IsNot Nothing AndAlso IsDBNull(dgEmployee.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False AndAlso dgEmployee.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString().Trim.Length > 0 AndAlso IsDate(dgEmployee.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                    dgEmployee.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = eZeeDate.convertDate(dgEmployee.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString()).ToShortDateString
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellPainting", mstrModuleName)
        End Try
    End Sub

    'Pinkal (07-Jul-2016) -- End


#End Region



    'Public Shared Function InsertCDATASections(ByVal ds As DataSet, ByVal cdataSections() As String) As String
    '    'Convert to XML with expanded general entities and CDATA sections
    '    'as appropriate
    '    Dim reader As XmlValidatingReader = Nothing
    '    Dim writer As XmlTextWriter = Nothing
    '    Dim sw As StringWriter = Nothing
    '    Array.Sort(cdataSections)
    '    Try
    '        reader = New XmlValidatingReader(ds.GetXml(), XmlNodeType.Document, Nothing)
    '        sw = New StringWriter()
    '        writer = New XmlTextWriter(sw)
    '        writer.Formatting = Formatting.Indented
    '        reader.ValidationType = ValidationType.None
    '        reader.EntityHandling = EntityHandling.ExpandCharEntities
    '        Dim currentElement As String = String.Empty
    '        While reader.Read()
    '            Select Case reader.NodeType
    '                Case XmlNodeType.Element
    '                    currentElement = reader.Name
    '                    writer.WriteStartElement(currentElement)
    '                    While reader.MoveToNextAttribute()
    '                        writer.WriteAttributeString(reader.Name, reader.Value)
    '                    End While
    '                    Exit While
    '                Case XmlNodeType.Text
    '                    If Array.BinarySearch(cdataSections, currentElement) < 0 Then
    '                        writer.WriteString(reader.Value)
    '                    Else
    '                        writer.WriteCData(reader.Value)
    '                    End If
    '                    Exit While
    '                Case XmlNodeType.EndElement
    '                    writer.WriteEndElement()
    '                    Exit While
    '                Case Else
    '                    Exit While
    '            End Select
    '        End While
    '    Catch exp As Exception
    '        Return exp.Message
    '    Finally
    '        reader.Close()
    '        writer.Close()
    '    End Try
    '    Return sw.ToString()
    'End Function





    'Pinkal (30-Jun-2016) -- End





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbLeavePlannerInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeavePlannerInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbLeavePlannerInfo.Text = Language._Object.getCaption(Me.gbLeavePlannerInfo.Name, Me.gbLeavePlannerInfo.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
            Me.lblStopDate.Text = Language._Object.getCaption(Me.lblStopDate.Name, Me.lblStopDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.lblFromYear.Text = Language._Object.getCaption(Me.lblFromYear.Name, Me.lblFromYear.Text)
            Me.LblToYear.Text = Language._Object.getCaption(Me.LblToYear.Name, Me.LblToYear.Text)
            Me.LblToMonth.Text = Language._Object.getCaption(Me.LblToMonth.Name, Me.LblToMonth.Text)
            Me.LblMonth.Text = Language._Object.getCaption(Me.LblMonth.Name, Me.LblMonth.Text)
            Me.radExpYear.Text = Language._Object.getCaption(Me.radExpYear.Name, Me.radExpYear.Text)
            Me.radAppointedDate.Text = Language._Object.getCaption(Me.radAppointedDate.Name, Me.radAppointedDate.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.radConfirmationDate.Text = Language._Object.getCaption(Me.radConfirmationDate.Name, Me.radConfirmationDate.Text)
            Me.radProbationDate.Text = Language._Object.getCaption(Me.radProbationDate.Name, Me.radProbationDate.Text)
            Me.LblEmployeeCount.Text = Language._Object.getCaption(Me.LblEmployeeCount.Name, Me.LblEmployeeCount.Text)
            Me.LblValue.Text = Language._Object.getCaption(Me.LblValue.Name, Me.LblValue.Text)
            Me.LblFrom.Text = Language._Object.getCaption(Me.LblFrom.Name, Me.LblFrom.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.dgColhEmpCode.HeaderText = Language._Object.getCaption(Me.dgColhEmpCode.Name, Me.dgColhEmpCode.HeaderText)
            Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
            Me.dgcolhAppointedDate.HeaderText = Language._Object.getCaption(Me.dgcolhAppointedDate.Name, Me.dgcolhAppointedDate.HeaderText)
            Me.dgcolhConfirmationDate.HeaderText = Language._Object.getCaption(Me.dgcolhConfirmationDate.Name, Me.dgcolhConfirmationDate.HeaderText)
            Me.dgcolhProbationFromDate.HeaderText = Language._Object.getCaption(Me.dgcolhProbationFromDate.Name, Me.dgcolhProbationFromDate.HeaderText)
            Me.dgcolhProbationToDate.HeaderText = Language._Object.getCaption(Me.dgcolhProbationToDate.Name, Me.dgcolhProbationToDate.HeaderText)
            Me.dgcolhJobTitle.HeaderText = Language._Object.getCaption(Me.dgcolhJobTitle.Name, Me.dgcolhJobTitle.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please Check atleast One Employee from the List.")
            Language.setMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type.")
            Language.setMessage(mstrModuleName, 3, "Select")
            Language.setMessage(mstrModuleName, 4, "From Condition is compulsory information.Please Select From Condition.")
            Language.setMessage(mstrModuleName, 5, "To Condition is compulsory information.Please Select To Condition.")
            Language.setMessage(mstrModuleName, 6, "Sorry, To date cannot be less than From date.")
            Language.setMessage(mstrModuleName, 7, "Employee Count :")
			Language.setMessage(mstrModuleName, 22, "Sorry, you cannot add this plan now. Checked Employee(s) unit has reached the maximum leave plans allowed at a time.")
			Language.setMessage(mstrModuleName, 23, "Sorry, some of the employee(s) cannot add this plan now. some of the employee(s) unit has reached the maximum leave plans allowed at a time for the selected dates and will be highlighted in red.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class


