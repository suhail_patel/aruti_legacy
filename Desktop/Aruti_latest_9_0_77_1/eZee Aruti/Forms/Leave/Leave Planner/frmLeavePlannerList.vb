﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 5

Public Class frmleaveplannerList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmleaveplannerList"
    Private objLeavePlanner As clsleaveplanner
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 
#End Region

#Region " Private Methods "

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Dim strSearching As String = ""
        Try

            If User._Object.Privilege._AllowToViewLeavePlannerList = True Then

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'dsList = objLeavePlanner.GetList("LeavePlanner", True, "", "", "", mstrAdvanceFilter)

                'If CInt(cboEmployee.SelectedValue) > 0 Then
                '    strSearching = "AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                'End If

                'If CInt(cboLeaveType.SelectedValue) > 0 Then
                '    strSearching &= "AND leavetypeunkid = " & CInt(cboLeaveType.SelectedValue) & " "
                'End If

                'If dtpStartDate.Checked Then
                '    strSearching &= "AND startdate >= '" & eZeeDate.convertDate(dtpStartDate.Value) & "'" & " "
                'End If

                'If dtpEndDate.Checked Then
                '    strSearching &= "AND stopdate <= '" & eZeeDate.convertDate(dtpEndDate.Value) & "'" & " "
                'End If

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    strSearching = "AND hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboLeaveType.SelectedValue) > 0 Then
                    strSearching &= "AND lvleaveplanner.leavetypeunkid = " & CInt(cboLeaveType.SelectedValue) & " "
                End If

                If dtpStartDate.Checked Then
                    strSearching &= "AND startdate >= '" & eZeeDate.convertDate(dtpStartDate.Value) & "'" & " "
                End If

                If dtpEndDate.Checked Then
                    strSearching &= "AND stopdate <= '" & eZeeDate.convertDate(dtpEndDate.Value) & "'" & " "
                End If


                If mstrAdvanceFilter.Trim.Length > 0 Then
                    strSearching &= "AND " & mstrAdvanceFilter.Trim
                End If


                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                End If

                dsList = objLeavePlanner.GetList("LeavePlanner", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                  , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                  , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                  , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, True, strSearching)

                'If strSearching.Length > 0 Then
                '    strSearching = strSearching.Substring(3)
                '    dtList = New DataView(dsList.Tables("LeavePlanner"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtList = dsList.Tables("LeavePlanner")
                'End If

                'Pinkal (24-Aug-2015) -- End


                lvLeavePlanner.Items.Clear()
                Dim lvArray As New List(Of ListViewItem)
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    lvItem = New ListViewItem
                    lvItem.Tag = dtRow("leaveplannerunkid").ToString
                    lvItem.Text = dtRow("EName").ToString
                    lvItem.SubItems.Add(dtRow("leavename").ToString)
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow("startdate").ToString).ToShortDateString)
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow("stopdate").ToString).ToShortDateString)

                    'Pinkal (30-Jun-2016) -- Start
                    'Enhancement - Working on SL changes on Leave Module.
                    lvItem.SubItems.Add(Math.Round(CDec(dtRow("Days")), 2).ToString())
                    'Pinkal (30-Jun-2016) -- End

                    lvItem.SubItems.Add(dtRow("statusname").ToString)
                    lvItem.SubItems.Add(dtRow("status").ToString)
                    lvArray.Add(lvItem)
                Next
                'Pinkal (30-Jun-2016) -- Start
                'Enhancement - Working on SL changes on Leave Module.
                lvLeavePlanner.Items.AddRange(lvArray.ToArray)
                lvLeavePlanner.GroupingColumn = colhEmployee
                lvLeavePlanner.DisplayGroups(True)

                'If lvLeavePlanner.Groups.Count > 4 Then
                '    colhStopdate.Width = 125 - 20
                'Else
                '    colhStopdate.Width = 125
                'End If

                If lvLeavePlanner.Groups.Count > 4 Then
                    colhDays.Width = 100 - 20
                Else
                    colhDays.Width = 100
                End If

                'Pinkal (30-Jun-2016) -- End


            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally

            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.
            objbtnSearch.ShowResult(CStr(lvLeavePlanner.Items.Count))
            'Pinkal (30-Jun-2016) -- End

        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            'FOR EMPLOYEE
            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If

            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsFill.Tables("Employee")

            'FOR LEAVE TYPE
            dsFill = Nothing
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.getListForCombo("LeaveType", True)
            cboLeaveType.ValueMember = "leavetypeunkid"
            cboLeaveType.DisplayMember = "name"
            cboLeaveType.DataSource = dsFill.Tables("LeaveType")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddPlanLeave
            btnEdit.Enabled = User._Object.Privilege._EditPlanLeave
            btnDelete.Enabled = User._Object.Privilege._DeletePlanLeave


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in leave module.
            btnImport.Enabled = User._Object.Privilege._AllowToImportLeavePlanner
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region

#Region " Form's Events "

    Private Sub frmleaveplannerList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLeavePlanner = New clsleaveplanner
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)


            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            ' Pinkal (1-Nov-2010) -- START
            Call SetVisibility()
            FillCombo()
            dtpStartDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpStartDate.MaxDate = FinancialYear._Object._Database_End_Date

            dtpEndDate.MinDate = FinancialYear._Object._Database_Start_Date
            dtpEndDate.MaxDate = FinancialYear._Object._Database_End_Date
            dtpEndDate.Checked = False
            dtpStartDate.Checked = False
            ' Pinkal (1-Nov-2010) -- END

            'FillList()
            If lvLeavePlanner.Items.Count > 0 Then lvLeavePlanner.Items(0).Selected = True
            lvLeavePlanner.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmleaveplannerList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmleaveplannerList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmleaveplannerList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmleaveplannerList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvLeavePlanner.Focused = True Then
                btnDelete_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveTypeList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmleaveplannerList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Close()
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsleaveplanner.SetMessages()
            objfrm._Other_ModuleNames = "clsleaveplanner"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmLeavePlanner As New frmleaveplanner_AddEdit
            objfrmLeavePlanner.displayDialog(-1, enAction.ADD_CONTINUE)
            Me.SuspendLayout()
            FillList()
            Me.ResumeLayout()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvLeavePlanner.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvLeavePlanner.Select()
            Exit Sub
        End If
        Dim objfrmLeavePlanner_AddEdit As New frmleaveplanner_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLeavePlanner.SelectedItems(0).Index

            ' Pinkal (1-Nov-2010) -- START

            ' If CInt(lvLeavePlanner.SelectedItems(0).SubItems(colhStatus.Index).Text) <> 6 Then
            If objfrmLeavePlanner_AddEdit.displayDialog(CInt(lvLeavePlanner.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Me.SuspendLayout()
                Call FillList()
                Me.ResumeLayout()
            End If
            'ElseIf CInt(lvLeavePlanner.SelectedItems(0).SubItems(colhStatus.Index).Text) = 6 Then
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "You cannot edit this Employee Leave. Reason: This Employee Leave is already Cancel."), enMsgBoxStyle.Information)
            'lvLeavePlanner.Select()
            'Exit Sub
            'End If

            ' Pinkal (1-Nov-2010) -- END
            objfrmLeavePlanner_AddEdit = Nothing

            lvLeavePlanner.Items(intSelectedIndex).Selected = True
            lvLeavePlanner.EnsureVisible(intSelectedIndex)
            lvLeavePlanner.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmLeavePlanner_AddEdit IsNot Nothing Then objfrmLeavePlanner_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvLeavePlanner.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvLeavePlanner.Select()
            Exit Sub
        End If
        'If objLeaveTypeMaster.isUsed(CInt(lvLeavePlanner.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Employee Leave. Reason: This Employee Leave is in use."), enMsgBoxStyle.Information) '?2
        '    lvLeavePlanner.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvLeavePlanner.SelectedItems(0).Index

            ' Pinkal (1-Nov-2010) -- START

            'If CInt(lvLeavePlanner.SelectedItems(0).SubItems(colhStatus.Index).Text) <> 6 Then
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Employee Leave?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                'Sandeep [ 16 Oct 2010 ] -- Start
                'objLeavePlanner._Voidreason = "VOID REASON"
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.LEAVE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objLeavePlanner._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                'Sandeep [ 16 Oct 2010 ] -- End 
                objLeavePlanner._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objLeavePlanner._Voiduserunkid = User._Object._Userunkid

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objLeavePlanner._FormName = mstrModuleName
                objLeavePlanner._LoginEmployeeunkid = 0
                objLeavePlanner._ClientIP = getIP()
                objLeavePlanner._HostName = getHostName()
                objLeavePlanner._FromWeb = False
                objLeavePlanner._AuditUserId = User._Object._Userunkid
objLeavePlanner._CompanyUnkid = Company._Object._Companyunkid
                objLeavePlanner._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objLeavePlanner.Delete(CInt(lvLeavePlanner.SelectedItems(0).Tag))
                lvLeavePlanner.SelectedItems(0).Remove()
            End If
            '     ElseIf CInt(lvLeavePlanner.SelectedItems(0).SubItems(colhStatus.Index).Text) = 6 Then
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You cannot delete this Employee Leave. Reason: This Employee Leave is already Cancel."), enMsgBoxStyle.Information)
            '  lvLeavePlanner.Select()
            '  Exit Sub
            '  End If

            ' Pinkal (1-Nov-2010) -- END

            If lvLeavePlanner.Items.Count <= 0 Then
                Exit Try
            End If

            If lvLeavePlanner.Items.Count = intSelectedIndex Then
                intSelectedIndex = lvLeavePlanner.Items.Count - 1
                lvLeavePlanner.Items(intSelectedIndex).Selected = True
                lvLeavePlanner.EnsureVisible(intSelectedIndex)
            ElseIf lvLeavePlanner.Items.Count <> 0 Then
                lvLeavePlanner.Items(intSelectedIndex).Selected = True
                lvLeavePlanner.EnsureVisible(intSelectedIndex)
            End If
            lvLeavePlanner.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.
            objbtnSearch.ShowResult(CStr(lvLeavePlanner.Items.Count))
            'Pinkal (30-Jun-2016) -- End
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedIndex = 0
            cboLeaveType.SelectedIndex = 0

            'Pinkal (03-Jan-2011) -- Start

            If FinancialYear._Object._Database_End_Date < ConfigParameter._Object._CurrentDateAndTime.Date.Date Then
                dtpStartDate.Value = FinancialYear._Object._Database_End_Date
                dtpEndDate.Value = FinancialYear._Object._Database_End_Date

            ElseIf FinancialYear._Object._Database_End_Date > ConfigParameter._Object._CurrentDateAndTime.Date.Date Then
                dtpStartDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpEndDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date

            End If

            dtpStartDate.Checked = False
            dtpEndDate.Checked = False

            'Pinkal (03-Jan-2011) -- End

            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End 

            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch

            'Pinkal (06-May-2014) -- Start
            'Enhancement : TRA Changes
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            'Pinkal (06-May-2014) -- End

            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    'Pinkal (19-Jan-2017) -- Start
    'Enhancement - Implementing Imporation feature of Leave Planner.

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Try
            Dim objFrm As New frmImportLeavePlanner
            objFrm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (19-Jan-2017) -- End


#End Region






    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnImport.GradientBackColor = GUI._ButttonBackColor
            Me.btnImport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhLeaveType.Text = Language._Object.getCaption(CStr(Me.colhLeaveType.Tag), Me.colhLeaveType.Text)
            Me.colhStartdate.Text = Language._Object.getCaption(CStr(Me.colhStartdate.Tag), Me.colhStartdate.Text)
            Me.colhStopdate.Text = Language._Object.getCaption(CStr(Me.colhStopdate.Tag), Me.colhStopdate.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.colhDays.Text = Language._Object.getCaption(CStr(Me.colhDays.Tag), Me.colhDays.Text)
            Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Employee from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Employee Leave?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

