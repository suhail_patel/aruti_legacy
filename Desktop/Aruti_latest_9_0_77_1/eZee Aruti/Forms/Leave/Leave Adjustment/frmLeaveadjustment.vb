﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmLeaveAdjustment

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmLeaveAdjustment"
    Private mstrAdvanceFilter As String = ""
    Private dsFill As New DataSet
    Private dvEmployee As DataView

    'Pinkal (02-Sep-2015) -- Start
    'Enhancement - WORKED ON LEAVE ADJUSTMENT SCREEN FOR ADJUSTMENT CONSOLIDATION AS PER RUTTA'S REQUIREMENT.
    Dim objLeaveAdjustment As clsleaveadjustment_Tran
    'Pinkal (02-Sep-2015) -- End

#End Region

#Region "Forms Event"

    Private Sub frmLeaveAdjustment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objLeaveAdjustment = New clsleaveadjustment_Tran
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call FillCombo()
            dgColhAppointmentDt.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgColhLeaveBF.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgColhAccruetodate.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgColhIssueLeave.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhRemaining_Bal.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhIssuetodate.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhEncashment.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhBalanceAmt.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhBalanceAsonDate.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            rdAssignGlobally.Checked = True
            cboLeaveType.Select()

            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            chkBalanceAmount_CheckedChanged(chkBalanceAmount, New EventArgs())
            chkBalanceAmount_CheckedChanged(chkLeaveBFAdjustment, New EventArgs())
            'Pinkal (08-Oct-2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLeaveAdjustment_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsleavebalance_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsleavebalance_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            dsFill = Nothing
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.getListForCombo("LeaveType", True)
            With cboLeaveType
                .ValueMember = "leavetypeunkid"
                .DisplayMember = "name"
                .DataSource = dsFill.Tables(0)
                .SelectedValue = 0
            End With

            Dim objMaster As New clsMasterData
            dsFill = objMaster.getGenderList("List", True)
            With cboGender
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsFill.Tables(0)
                .SelectedValue = 0
            End With




            dsFill = Nothing
            dsFill = objMaster.GetCondition(True, True, True, True, False)
            With cboLeaveBFCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsFill.Tables(0)
                .SelectedValue = 0
            End With


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            With cboLeaveIssueAmtCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsFill.Tables(0).Copy()
                .SelectedValue = 0
            End With
            'Pinkal (08-Oct-2018) -- End


            objLeaveType = Nothing
            objMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillData()
        Dim objLeavebalance As New clsleavebalance_tran
        Dim strSearch As String = ""
        Try
            If CInt(cboGender.SelectedValue) > 0 Then
                strSearch = "AND hremployee_master.gender = " & CInt(cboGender.SelectedValue) & " "
                End If

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                If strSearch.Trim.Length > 0 Then strSearch &= " AND "
                strSearch &= mstrAdvanceFilter
            End If

            If CInt(cboLeaveBFCondition.SelectedValue) > 0 Then
                If strSearch.Trim.Length > 0 Then strSearch &= " AND "
                strSearch &= "lvleavebalance_tran.leavebf " & cboLeaveBFCondition.Text & nudLeaveBF.Value
            End If

            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If CInt(cboLeaveIssueAmtCondition.SelectedValue) > 0 Then
                If strSearch.Trim.Length > 0 Then strSearch &= " AND "
                strSearch &= "lvleavebalance_tran.issue_amount " & cboLeaveIssueAmtCondition.Text & nudLeaveIssueAmt.Value
            End If
            'Pinkal (08-Oct-2018) -- End


            dsFill = objLeavebalance.GetEmployeeLeaveDataForAdjustment(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                                        , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, True _
                                                                                                        , ConfigParameter._Object._LeaveBalanceSetting, CInt(cboLeaveType.SelectedValue), dtpDate.Value.Date, True, True, strSearch)



            If dsFill IsNot Nothing AndAlso dsFill.Tables(0).Rows.Count > 0 Then

                If dsFill.Tables(0).Columns.Contains("IssueAsonDate") = False Then
                    dsFill.Tables(0).Columns.Add("IssueAsonDate", Type.GetType("System.Decimal"))
                    dsFill.Tables(0).Columns("IssueAsonDate").DefaultValue = 0
                End If

                If dsFill.Tables(0).Columns.Contains("Balancetodate") = False Then
                    dsFill.Tables(0).Columns.Add("Balancetodate", Type.GetType("System.Decimal"))
                    dsFill.Tables(0).Columns("Balancetodate").DefaultValue = 0
                End If

                Dim objLeaveIssue As New clsleaveissue_Tran

                For Each drRow As DataRow In dsFill.Tables(0).Rows
                    Dim objClaimMst As New clsclaim_request_master
                    Dim dsBalanceList As DataSet = Nothing
                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then

                    drRow("IssueAsonDate") = objLeaveIssue.GetEmployeeIssueDaysCount(CInt(drRow("employeeunkid")), CInt(cboLeaveType.SelectedValue), FinancialYear._Object._YearUnkid, dtpDate.Value.Date)
                        drRow("Encashment") = objClaimMst.GetLeaveEncashmentForEmployee(FinancialYear._Object._YearUnkid, CInt(drRow("employeeunkid")), CInt(cboLeaveType.SelectedValue), 0).ToString("#0.00")
                        objLeavebalance._DBStartdate = FinancialYear._Object._Database_Start_Date.Date
                        objLeavebalance._DBEnddate = FinancialYear._Object._Database_End_Date.Date

                        'Pinkal (18-Nov-2016) -- Start
                        'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                        'dsBalanceList = objLeavebalance.GetLeaveBalanceInfo(dtpDate.Value.Date, cboLeaveType.SelectedValue.ToString(), drRow("employeeunkid").ToString(), Nothing, Nothing, False, False, ConfigParameter._Object._LeaveBalanceSetting, -1, FinancialYear._Object._YearUnkid)
                        dsBalanceList = objLeavebalance.GetLeaveBalanceInfo(dtpDate.Value.Date, cboLeaveType.SelectedValue.ToString(), drRow("employeeunkid").ToString(), ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, Nothing, Nothing, False, False, ConfigParameter._Object._LeaveBalanceSetting, -1, FinancialYear._Object._YearUnkid)
                        'Pinkal (18-Nov-2016) -- End


                    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        drRow("IssueAsonDate") = CDec(objLeaveIssue.GetEmployeeIssueDaysCount(CInt(drRow("employeeunkid")), CInt(cboLeaveType.SelectedValue), FinancialYear._Object._YearUnkid, dtpDate.Value.Date, CDate(drRow("startdate")), CDate(drRow("enddate")), FinancialYear._Object._Database_Start_Date.Date))
                        drRow("Encashment") = CDec(objClaimMst.GetLeaveEncashmentForEmployee(FinancialYear._Object._YearUnkid, CInt(drRow("employeeunkid")), CInt(cboLeaveType.SelectedValue), 0, CDate(drRow("startdate")), CDate(drRow("enddate")), FinancialYear._Object._Database_Start_Date.Date)).ToString("#0.00")

                        'Pinkal (18-Nov-2016) -- Start
                        'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                        'dsBalanceList = objLeavebalance.GetLeaveBalanceInfo(dtpDate.Value.Date, cboLeaveType.SelectedValue.ToString(), drRow("employeeunkid").ToString(), Nothing, Nothing, True, True, ConfigParameter._Object._LeaveBalanceSetting, -1, FinancialYear._Object._YearUnkid)
                        dsBalanceList = objLeavebalance.GetLeaveBalanceInfo(dtpDate.Value.Date, cboLeaveType.SelectedValue.ToString(), drRow("employeeunkid").ToString(), ConfigParameter._Object._LeaveAccrueTenureSetting, ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth, Nothing, Nothing, True, True, ConfigParameter._Object._LeaveBalanceSetting, -1, FinancialYear._Object._YearUnkid)
                        'Pinkal (18-Nov-2016) -- End

                    End If

                    If dsBalanceList.Tables(0).Rows.Count > 0 Then
                        drRow("Balancetodate") = CDec(CDec(dsBalanceList.Tables(0).Rows(0)("Accrue_amount")) + CDec(dsBalanceList.Tables(0).Rows(0)("LeaveBF")) - CDec(dsBalanceList.Tables(0).Rows(0)("Issue_amount"))).ToString("#0.00")
                    End If

                Next
                dsFill.Tables(0).AcceptChanges()

            End If


            dvEmployee = dsFill.Tables(0).DefaultView
            dgEmployee.AutoGenerateColumns = False
            dgColhEmployee.DataPropertyName = "Employee"
            dgColhEmployee.Tag = "employeeunkid"
            dgColhJobtitle.DataPropertyName = "Job"
            dgColhAppointmentDt.DataPropertyName = "appointeddate"
            dgColhLeaveBF.DataPropertyName = "leavebf"
            dgColhAccruetodate.DataPropertyName = "AccrueAsOnDate"
            dgcolhIssuetodate.DataPropertyName = "IssueAsonDate"
            dgColhIssueLeave.DataPropertyName = "Issue_amount"
            dgcolhRemaining_Bal.DataPropertyName = "Remaining_Bal"
            dgcolhEncashment.DataPropertyName = "Encashment"
            dgcolhBalanceAmt.DataPropertyName = "Balance_amt"
            dgcolhBalanceAsonDate.DataPropertyName = "Balancetodate"
            dgEmployee.Tag = "leavebalanceunkid"
            objSelect.DataPropertyName = "ischeck"


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            dgcolhLeaveBFAdjustment.DataPropertyName = "Leavebf_Adjustment"
            'Pinkal (08-Oct-2018) -- End


            dgEmployee.DataSource = dsFill.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillData", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button Event"
            
    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboLeaveType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Type is compulsory information.Please Select Leave Type."), enMsgBoxStyle.Information)
                cboLeaveType.Select()
                Exit Sub
            ElseIf nudLeaveBF.Value <> 0 AndAlso CInt(cboLeaveBFCondition.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Condition filtering is mandatory information when Leave BF is not equal to 0."), enMsgBoxStyle.Information)
                cboLeaveBFCondition.Select()
                Exit Sub


                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
            ElseIf nudLeaveIssueAmt.Value <> 0 AndAlso CInt(cboLeaveIssueAmtCondition.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Condition filtering is mandatory information when Leave Issued is not equal to 0."), enMsgBoxStyle.Information)
                cboLeaveIssueAmtCondition.Select()
                Exit Sub
                'Pinkal (08-Oct-2018) -- End

            End If
            Call FillData()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboGender.SelectedValue = 0
            cboLeaveType.SelectedValue = 0
            dgEmployee.DataSource = Nothing
            txtAdjustmentAmount.Decimal = 0
            TxtSearch.Text = ""
            txtRemark.Text = ""
            dtpDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            
            'Pinkal (02-Sep-2015) -- Start
            'Enhancement - WORKED ON LEAVE ADJUSTMENT SCREEN FOR ADJUSTMENT CONSOLIDATION AS PER RUTTA'S REQUIREMENT.
            rdAssignGlobally.Checked = True
            'Pinkal (02-Sep-2015) -- End
            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            mstrAdvanceFilter = ""
            'Pinkal (24-Aug-2015) -- End

            'Pinkal (29-Oct-2015) -- Start
            'Enhancement - WORKING VFT LEAVE ADJUSTMENT CHANGES.
            nudLeaveBF.Value = 0
            cboLeaveBFCondition.SelectedValue = 0
            'Pinkal (29-Oct-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnsearchLeaveType_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnsearchLeaveType.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboLeaveType.DataSource, DataTable)
            With cboLeaveType
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
            End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnsearchLeaveType_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim drRow As DataRow() = Nothing
            If CInt(cboLeaveType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Leave Type is compulsory information.Please Select Leave Type."), enMsgBoxStyle.Information)
                cboLeaveType.Select()
                Exit Sub

                'Pinkal (02-Sep-2015) -- Start
                'Enhancement - WORKED ON LEAVE ADJUSTMENT SCREEN FOR ADJUSTMENT CONSOLIDATION AS PER RUTTA'S REQUIREMENT.
                'ElseIf txtAdjustmentAmount.Text.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Balance Amount is compulsory information.Please Enter Balance Amount."), enMsgBoxStyle.Information)
                '    txtAdjustmentAmount.Select()
                '    Exit Sub
                'ElseIf txtAdjustmentAmount.Decimal = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Balance Amount is compulsory information.Please Enter Balance Amount."), enMsgBoxStyle.Information)
                '    txtAdjustmentAmount.Select()
                '    Exit Sub
                'Pinkal (02-Sep-2015) -- End

            ElseIf txtRemark.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Remark is compulsory information.Please Select Remark."), enMsgBoxStyle.Information)
                txtRemark.Select()
                Exit Sub
            ElseIf dsFill IsNot Nothing Then
                drRow = dvEmployee.ToTable.Select("ischeck = true")
                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information.Please check atleast one employee."), enMsgBoxStyle.Information)
                    Exit Sub
                Else

                    'Pinkal (08-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                    If chkBalanceAmount.Checked Then
                    Dim drBalanceAmt As DataRow() = dvEmployee.ToTable.Select("ischeck = true AND (balance_amt is NOT NULL AND balance_amt <> 0)")
                    If drBalanceAmt.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Balance amount is compulsory information.Please enter balance amount."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

                    If chkLeaveBFAdjustment.Checked Then
                        Dim drLeaveBFAdjAmt As DataRow() = dvEmployee.ToTable.Select("ischeck = true AND (leavebf_adjustment is NOT NULL AND leavebf_adjustment <> 0)")
                        If drLeaveBFAdjAmt.Length <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Leave BF Adjustment is compulsory information.Please enter Leave BF Adjustment amount."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'Pinkal (08-Oct-2018) -- End

                End If
            End If

            If drRow.Length > 0 Then
                Dim mblnFlag As Boolean = False
                For Each dr As DataRow In drRow

                    'Pinkal (02-Sep-2015) -- Start
                    'Enhancement - WORKED ON LEAVE ADJUSTMENT SCREEN FOR ADJUSTMENT CONSOLIDATION AS PER RUTTA'S REQUIREMENT.
                    'objbalance._LeaveBalanceunkid = CInt(dr("leavebalanceunkid"))
                    'objbalance._AdjustmentAmt = CDec(txtAdjustmentAmoun-t.Decimal)
                    'objbalance._AccrueAmount += txtAdjustmentAmount.Decimal
                    'objbalance._UptoLstYr_AccrueAmout += txtAdjustmentAmount.Decimal
                    'objbalance._Remaining_Balance = objbalance._AccrueAmount - objbalance._IssueAmount
                    'objbalance._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
                    'If objbalance.Update() = False Then
                    '    mblnFlag = False
                    '    eZeeMsgBox.Show(objbalance._Message, enMsgBoxStyle.Information)
                    '    Exit Sub
                    'End If

                    objLeaveAdjustment._Leavebalanceunkid = CInt(dr("leavebalanceunkid"))
                    objLeaveAdjustment._Employeeunkid = CInt(dr("employeeunkid"))
                    objLeaveAdjustment._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
                    objLeaveAdjustment._Adjustment_Amt = CDec(dr("Balance_amt"))
                    objLeaveAdjustment._Remarks = txtRemark.Text.Trim
                    objLeaveAdjustment._Userunkid = User._Object._Userunkid
                    objLeaveAdjustment._Isopenelc = CBool(dr("Isopenelc"))
                    objLeaveAdjustment._Iselc = CBool(dr("Iselc"))
                    objLeaveAdjustment._Isclose_Fy = CBool(dr("Isclose_Fy"))
                    objLeaveAdjustment._Isvoid = False

                    'Pinkal (08-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                    objLeaveAdjustment._LeaveBf_Adjustment = CDec(dr("leavebf_adjustment"))
                    'Pinkal (08-Oct-2018) -- End

'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objLeaveAdjustment._FormName = mstrModuleName
                    objLeaveAdjustment._LoginEmployeeunkid = 0
                    objLeaveAdjustment._ClientIP = getIP()
                    objLeaveAdjustment._HostName = getHostName()
                    objLeaveAdjustment._FromWeb = False
                    objLeaveAdjustment._AuditUserId = User._Object._Userunkid
objLeaveAdjustment._CompanyUnkid = Company._Object._Companyunkid
                    objLeaveAdjustment._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    If objLeaveAdjustment.Insert() = False Then
                        mblnFlag = False
                        eZeeMsgBox.Show(objLeaveAdjustment._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'Pinkal (02-Sep-2015) -- End


                    mblnFlag = True
                Next

                If mblnFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Adjustment done successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    chkSelectAll.Checked = False
                    cboGender.SelectedValue = 0
                    cboLeaveType.SelectedValue = 0
                    dtpDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    txtAdjustmentAmount.Decimal = 0

                    'Pinkal (08-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                    txtLeaveBFAdjustment.Decimal = 0
                    'Pinkal (08-Oct-2018) -- End

                    txtRemark.Text = ""
                    dgEmployee.DataSource = Nothing
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = dvEmployee.ToTable.Select("ischeck = true")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgEmployee.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgEmployee.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
                End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

    'Pinkal (02-Sep-2015) -- Start
    'Enhancement - WORKED ON LEAVE ADJUSTMENT SCREEN FOR ADJUSTMENT CONSOLIDATION AS PER RUTTA'S REQUIREMENT.

    Private Sub btnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSet.Click
        Try
            If dgEmployee.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "There is no data to set balance amount."), enMsgBoxStyle.Information)
                Exit Sub
            Else
                Dim dtTable As DataTable = CType(dgEmployee.DataSource, DataTable)
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    For Each dr As DataRow In dtTable.Rows

                        'Pinkal (08-Oct-2018) -- Start
                        'Enhancement - Leave Enhancement for NMB.
                        If chkBalanceAmount.Checked Then
                        dr("Balance_amt") = txtAdjustmentAmount.Decimal
                        End If
                        If chkLeaveBFAdjustment.Checked Then
                            dr("leavebf_adjustment") = txtLeaveBFAdjustment.Decimal
                        End If
                        'Pinkal (08-Oct-2018) -- End
                    Next
                    dtTable.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSet_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (02-Sep-2015) -- End

#End Region

#Region "GridView Event"

    Private Sub dgEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick, dgEmployee.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objSelect.Index Then

                If dgEmployee.IsCurrentCellDirty Then
                    dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    End If
                dsFill.Tables(0).AcceptChanges()
                SetCheckBoxValue()
                        End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkButon Events"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
		Try
            mstrAdvanceFilter = ""
            TxtSearch.Text = ""
            Dim frm As New frmAdvanceSearch
            'Pinkal (18-Aug-2015) -- Start
            'Enhancement - WORKING ON LEAVE ENCASHMENT DEDUCTION IN LEAVE MODULE.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            'Pinkal (18-Aug-2015) -- End
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
		End Try
	End Sub
			

#End Region

#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
		Try
            If dsFill.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRowView In dvEmployee
                    RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr("ischeck") = chkSelectAll.Checked
                    AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()
            End If
			
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
		End Try
	End Sub
			
    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private Sub chkBalanceAmount_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBalanceAmount.CheckedChanged, chkLeaveBFAdjustment.CheckedChanged
        Try
            If CType(sender, CheckBox).Name.ToUpper() = chkBalanceAmount.Name.ToUpper() Then
                If CType(sender, CheckBox).Checked = False Then
                    txtAdjustmentAmount.Text = "0"
                End If
                lblAdjustmentAmount.Enabled = CType(sender, CheckBox).Checked
                txtAdjustmentAmount.Enabled = CType(sender, CheckBox).Checked
                dgcolhBalanceAmt.ReadOnly = Not CType(sender, CheckBox).Checked
            ElseIf CType(sender, CheckBox).Name.ToUpper() = chkLeaveBFAdjustment.Name.ToUpper() Then
                If CType(sender, CheckBox).Checked = False Then
                    txtLeaveBFAdjustment.Text = "0"
                End If
                LblLeaveBFAdjustment.Enabled = CType(sender, CheckBox).Checked
                txtLeaveBFAdjustment.Enabled = CType(sender, CheckBox).Checked
                dgcolhLeaveBFAdjustment.ReadOnly = Not CType(sender, CheckBox).Checked
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkBalanceAmount_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Pinkal (08-Oct-2018) -- End


#End Region

#Region "Textbox Event"

    Private Sub TxtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtSearch.TextChanged
		Try
            If dvEmployee.Table.Rows.Count > 0 Then
                dvEmployee.RowFilter = "employeecode like '%" & TxtSearch.Text.Trim & "%'  OR EmpName like '%" & TxtSearch.Text.Trim & "%'"
                dgEmployee.Refresh()
            End If

            If dgEmployee.Rows.Count > 0 Then
                chkSelectAll.Enabled = True
            Else
                chkSelectAll.Enabled = False
                chkSelectAll.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
		End Try
	End Sub

#End Region
    

    'Pinkal (02-Sep-2015) -- Start
    'Enhancement - WORKED ON LEAVE ADJUSTMENT SCREEN FOR ADJUSTMENT CONSOLIDATION AS PER RUTTA'S REQUIREMENT.

#Region "RadioButton Events"

    Private Sub rdAssignGlobally_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdAssignGlobally.CheckedChanged, rdAssignIndividually.CheckedChanged
        Try
            If CType(sender, RadioButton).Name.ToUpper = "RDASSIGNGLOBALLY" Then
                txtAdjustmentAmount.ReadOnly = False
                dgcolhBalanceAmt.ReadOnly = True
                btnSet.Enabled = True
            ElseIf CType(sender, RadioButton).Name.ToUpper = "RDASSIGNINDIVIDUALLY" Then

                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                chkBalanceAmount.Checked = False
                txtAdjustmentAmount.ReadOnly = True
                dgcolhBalanceAmt.ReadOnly = False
                txtAdjustmentAmount.Decimal = 0


                chkLeaveBFAdjustment.Checked = False
                txtLeaveBFAdjustment.ReadOnly = True
                dgcolhLeaveBFAdjustment.ReadOnly = False
                txtLeaveBFAdjustment.Decimal = 0
                'Pinkal (08-Oct-2018) -- End
                
                btnSet.Enabled = False

                Dim dtTable As DataTable = CType(dgEmployee.DataSource, DataTable)
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    For Each dr As DataRow In dtTable.Rows
                        dr("Balance_amt") = txtAdjustmentAmount.Decimal

                        'Pinkal (08-Oct-2018) -- Start
                        'Enhancement - Leave Enhancement for NMB.
                        dr("leavebf_adjustment") = txtLeaveBFAdjustment.Decimal
                        'Pinkal (08-Oct-2018) -- End
                    Next
                    dtTable.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rdAssignGlobally_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (02-Sep-2015) -- End
    


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbLeaveAdjustment.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLeaveAdjustment.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSet.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSet.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.Label2.Text = Language._Object.getCaption(Me.Label2.Name, Me.Label2.Text)
			Me.Label3.Text = Language._Object.getCaption(Me.Label3.Name, Me.Label3.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.Name, Me.lblLeaveType.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblAdjustmentAmount.Text = Language._Object.getCaption(Me.lblAdjustmentAmount.Name, Me.lblAdjustmentAmount.Text)
			Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
			Me.gbLeaveAdjustment.Text = Language._Object.getCaption(Me.gbLeaveAdjustment.Name, Me.gbLeaveAdjustment.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.rdAssignIndividually.Text = Language._Object.getCaption(Me.rdAssignIndividually.Name, Me.rdAssignIndividually.Text)
			Me.rdAssignGlobally.Text = Language._Object.getCaption(Me.rdAssignGlobally.Name, Me.rdAssignGlobally.Text)
			Me.btnSet.Text = Language._Object.getCaption(Me.btnSet.Name, Me.btnSet.Text)
			Me.LblLeaveBF.Text = Language._Object.getCaption(Me.LblLeaveBF.Name, Me.LblLeaveBF.Text)
			Me.LblLeaveIssueAmt.Text = Language._Object.getCaption(Me.LblLeaveIssueAmt.Name, Me.LblLeaveIssueAmt.Text)
			Me.LblLeaveBFAdjustment.Text = Language._Object.getCaption(Me.LblLeaveBFAdjustment.Name, Me.LblLeaveBFAdjustment.Text)
			Me.chkLeaveBFAdjustment.Text = Language._Object.getCaption(Me.chkLeaveBFAdjustment.Name, Me.chkLeaveBFAdjustment.Text)
			Me.chkBalanceAmount.Text = Language._Object.getCaption(Me.chkBalanceAmount.Name, Me.chkBalanceAmount.Text)
			Me.dgColhEmployee.HeaderText = Language._Object.getCaption(Me.dgColhEmployee.Name, Me.dgColhEmployee.HeaderText)
			Me.dgColhAppointmentDt.HeaderText = Language._Object.getCaption(Me.dgColhAppointmentDt.Name, Me.dgColhAppointmentDt.HeaderText)
			Me.dgColhJobtitle.HeaderText = Language._Object.getCaption(Me.dgColhJobtitle.Name, Me.dgColhJobtitle.HeaderText)
			Me.dgColhLeaveBF.HeaderText = Language._Object.getCaption(Me.dgColhLeaveBF.Name, Me.dgColhLeaveBF.HeaderText)
			Me.dgColhAccruetodate.HeaderText = Language._Object.getCaption(Me.dgColhAccruetodate.Name, Me.dgColhAccruetodate.HeaderText)
			Me.dgcolhIssuetodate.HeaderText = Language._Object.getCaption(Me.dgcolhIssuetodate.Name, Me.dgcolhIssuetodate.HeaderText)
			Me.dgcolhBalanceAsonDate.HeaderText = Language._Object.getCaption(Me.dgcolhBalanceAsonDate.Name, Me.dgcolhBalanceAsonDate.HeaderText)
			Me.dgcolhEncashment.HeaderText = Language._Object.getCaption(Me.dgcolhEncashment.Name, Me.dgcolhEncashment.HeaderText)
			Me.dgcolhBalanceAmt.HeaderText = Language._Object.getCaption(Me.dgcolhBalanceAmt.Name, Me.dgcolhBalanceAmt.HeaderText)
			Me.dgcolhLeaveBFAdjustment.HeaderText = Language._Object.getCaption(Me.dgcolhLeaveBFAdjustment.Name, Me.dgcolhLeaveBFAdjustment.HeaderText)
			Me.dgcolhRemaining_Bal.HeaderText = Language._Object.getCaption(Me.dgcolhRemaining_Bal.Name, Me.dgcolhRemaining_Bal.HeaderText)
			Me.dgColhIssueLeave.HeaderText = Language._Object.getCaption(Me.dgColhIssueLeave.Name, Me.dgColhIssueLeave.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Leave Type is compulsory information.Please Select Leave Type.")
			Language.setMessage(mstrModuleName, 2, "Balance amount is compulsory information.Please enter balance amount.")
			Language.setMessage(mstrModuleName, 3, "Remark is compulsory information.Please Select Remark.")
			Language.setMessage(mstrModuleName, 4, "Employee is compulsory information.Please check atleast one employee.")
			Language.setMessage(mstrModuleName, 5, "Adjustment done successfully.")
			Language.setMessage(mstrModuleName, 6, "There is no data to set balance amount.")
			Language.setMessage(mstrModuleName, 7, "Condition filtering is mandatory information when Leave BF is not equal to 0.")
			Language.setMessage(mstrModuleName, 8, "Condition filtering is mandatory information when Leave Issued is not equal to 0.")
			Language.setMessage(mstrModuleName, 9, "Leave BF Adjustment is compulsory information.Please enter Leave BF Adjustment amount.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class