﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLeaveAdjustment
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLeaveAdjustment))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblLeaveType = New System.Windows.Forms.Label
        Me.cboLeaveType = New System.Windows.Forms.ComboBox
        Me.objbtnsearchLeaveType = New eZee.Common.eZeeGradientButton
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboLeaveIssueAmtCondition = New System.Windows.Forms.ComboBox
        Me.nudLeaveIssueAmt = New System.Windows.Forms.NumericUpDown
        Me.LblLeaveIssueAmt = New System.Windows.Forms.Label
        Me.cboLeaveBFCondition = New System.Windows.Forms.ComboBox
        Me.nudLeaveBF = New System.Windows.Forms.NumericUpDown
        Me.LblLeaveBF = New System.Windows.Forms.Label
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.lblGender = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgColhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhAppointmentDt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhJobtitle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhLeaveBF = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhAccruetodate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIssuetodate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBalanceAsonDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEncashment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBalanceAmt = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhLeaveBFAdjustment = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhRemaining_Bal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhIssueLeave = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TxtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.txtAdjustmentAmount = New eZee.TextBox.NumericTextBox
        Me.lblAdjustmentAmount = New System.Windows.Forms.Label
        Me.gbLeaveAdjustment = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkLeaveBFAdjustment = New System.Windows.Forms.CheckBox
        Me.chkBalanceAmount = New System.Windows.Forms.CheckBox
        Me.txtLeaveBFAdjustment = New eZee.TextBox.NumericTextBox
        Me.LblLeaveBFAdjustment = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.btnSet = New eZee.Common.eZeeLightButton(Me.components)
        Me.rdAssignIndividually = New System.Windows.Forms.RadioButton
        Me.rdAssignGlobally = New System.Windows.Forms.RadioButton
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TableLayoutPanel2.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.nudLeaveIssueAmt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLeaveBF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbLeaveAdjustment.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.77049!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.22951!))
        Me.TableLayoutPanel2.Controls.Add(Me.Label1, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label2, 1, 1)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(200, 100)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(150, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 56)
        Me.Label1.TabIndex = 292
        Me.Label1.Text = "0"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(150, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 20)
        Me.Label2.TabIndex = 291
        Me.Label2.Text = "0"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(129, 1)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 1)
        Me.Label3.TabIndex = 289
        Me.Label3.Text = "Amount"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpDate
        '
        Me.dtpDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Checked = False
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(264, 30)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(93, 21)
        Me.dtpDate.TabIndex = 5
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(149, 33)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(109, 15)
        Me.lblDate.TabIndex = 228
        Me.lblDate.Text = "Date"
        '
        'lblLeaveType
        '
        Me.lblLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeaveType.Location = New System.Drawing.Point(8, 59)
        Me.lblLeaveType.Name = "lblLeaveType"
        Me.lblLeaveType.Size = New System.Drawing.Size(71, 15)
        Me.lblLeaveType.TabIndex = 246
        Me.lblLeaveType.Text = "Leave Type"
        '
        'cboLeaveType
        '
        Me.cboLeaveType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveType.DropDownWidth = 400
        Me.cboLeaveType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveType.FormattingEnabled = True
        Me.cboLeaveType.Location = New System.Drawing.Point(82, 56)
        Me.cboLeaveType.Name = "cboLeaveType"
        Me.cboLeaveType.Size = New System.Drawing.Size(173, 21)
        Me.cboLeaveType.TabIndex = 2
        '
        'objbtnsearchLeaveType
        '
        Me.objbtnsearchLeaveType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnsearchLeaveType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnsearchLeaveType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnsearchLeaveType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnsearchLeaveType.BorderSelected = False
        Me.objbtnsearchLeaveType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnsearchLeaveType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnsearchLeaveType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnsearchLeaveType.Location = New System.Drawing.Point(261, 56)
        Me.objbtnsearchLeaveType.Name = "objbtnsearchLeaveType"
        Me.objbtnsearchLeaveType.Size = New System.Drawing.Size(19, 20)
        Me.objbtnsearchLeaveType.TabIndex = 273
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(9, 400)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(66, 15)
        Me.lblRemark.TabIndex = 274
        Me.lblRemark.Text = "Remark"
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(83, 397)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(634, 42)
        Me.txtRemark.TabIndex = 10
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboLeaveIssueAmtCondition)
        Me.gbFilterCriteria.Controls.Add(Me.nudLeaveIssueAmt)
        Me.gbFilterCriteria.Controls.Add(Me.LblLeaveIssueAmt)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeaveBFCondition)
        Me.gbFilterCriteria.Controls.Add(Me.nudLeaveBF)
        Me.gbFilterCriteria.Controls.Add(Me.LblLeaveBF)
        Me.gbFilterCriteria.Controls.Add(Me.cboGender)
        Me.gbFilterCriteria.Controls.Add(Me.lblGender)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnsearchLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.cboLeaveType)
        Me.gbFilterCriteria.Controls.Add(Me.lblLeaveType)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(1, 1)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(285, 135)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLeaveIssueAmtCondition
        '
        Me.cboLeaveIssueAmtCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveIssueAmtCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveIssueAmtCondition.FormattingEnabled = True
        Me.cboLeaveIssueAmtCondition.Location = New System.Drawing.Point(190, 108)
        Me.cboLeaveIssueAmtCondition.Name = "cboLeaveIssueAmtCondition"
        Me.cboLeaveIssueAmtCondition.Size = New System.Drawing.Size(65, 21)
        Me.cboLeaveIssueAmtCondition.TabIndex = 307
        '
        'nudLeaveIssueAmt
        '
        Me.nudLeaveIssueAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudLeaveIssueAmt.Location = New System.Drawing.Point(82, 108)
        Me.nudLeaveIssueAmt.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nudLeaveIssueAmt.Minimum = New Decimal(New Integer() {999, 0, 0, -2147483648})
        Me.nudLeaveIssueAmt.Name = "nudLeaveIssueAmt"
        Me.nudLeaveIssueAmt.Size = New System.Drawing.Size(104, 21)
        Me.nudLeaveIssueAmt.TabIndex = 306
        Me.nudLeaveIssueAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblLeaveIssueAmt
        '
        Me.LblLeaveIssueAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLeaveIssueAmt.Location = New System.Drawing.Point(8, 111)
        Me.LblLeaveIssueAmt.Name = "LblLeaveIssueAmt"
        Me.LblLeaveIssueAmt.Size = New System.Drawing.Size(71, 15)
        Me.LblLeaveIssueAmt.TabIndex = 305
        Me.LblLeaveIssueAmt.Text = "Leave Issued"
        '
        'cboLeaveBFCondition
        '
        Me.cboLeaveBFCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeaveBFCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeaveBFCondition.FormattingEnabled = True
        Me.cboLeaveBFCondition.Location = New System.Drawing.Point(190, 82)
        Me.cboLeaveBFCondition.Name = "cboLeaveBFCondition"
        Me.cboLeaveBFCondition.Size = New System.Drawing.Size(65, 21)
        Me.cboLeaveBFCondition.TabIndex = 303
        '
        'nudLeaveBF
        '
        Me.nudLeaveBF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudLeaveBF.Location = New System.Drawing.Point(82, 82)
        Me.nudLeaveBF.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me.nudLeaveBF.Minimum = New Decimal(New Integer() {999, 0, 0, -2147483648})
        Me.nudLeaveBF.Name = "nudLeaveBF"
        Me.nudLeaveBF.Size = New System.Drawing.Size(104, 21)
        Me.nudLeaveBF.TabIndex = 302
        Me.nudLeaveBF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblLeaveBF
        '
        Me.LblLeaveBF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLeaveBF.Location = New System.Drawing.Point(8, 85)
        Me.LblLeaveBF.Name = "LblLeaveBF"
        Me.LblLeaveBF.Size = New System.Drawing.Size(71, 15)
        Me.LblLeaveBF.TabIndex = 301
        Me.LblLeaveBF.Text = "Leave BF"
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(82, 30)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(174, 21)
        Me.cboGender.TabIndex = 1
        '
        'lblGender
        '
        Me.lblGender.BackColor = System.Drawing.Color.Transparent
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(8, 33)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(71, 15)
        Me.lblGender.TabIndex = 299
        Me.lblGender.Text = "Gender"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(258, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 297
        Me.objbtnReset.TabStop = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(157, 5)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(72, 16)
        Me.lnkAllocation.TabIndex = 296
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(232, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 295
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 444)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(729, 50)
        Me.objFooter.TabIndex = 276
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(517, 9)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 11
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(620, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 12
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.AllowUserToResizeRows = False
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objSelect, Me.dgColhEmployee, Me.dgColhAppointmentDt, Me.dgColhJobtitle, Me.dgColhLeaveBF, Me.dgColhAccruetodate, Me.dgcolhIssuetodate, Me.dgcolhBalanceAsonDate, Me.dgcolhEncashment, Me.dgcolhBalanceAmt, Me.dgcolhLeaveBFAdjustment, Me.dgcolhRemaining_Bal, Me.dgColhIssueLeave})
        Me.dgEmployee.Location = New System.Drawing.Point(2, 162)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.RowHeadersWidth = 5
        Me.dgEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgEmployee.Size = New System.Drawing.Size(726, 229)
        Me.dgEmployee.TabIndex = 9
        '
        'objSelect
        '
        Me.objSelect.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objSelect.Frozen = True
        Me.objSelect.HeaderText = ""
        Me.objSelect.Name = "objSelect"
        Me.objSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objSelect.Width = 25
        '
        'dgColhEmployee
        '
        Me.dgColhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhEmployee.Frozen = True
        Me.dgColhEmployee.HeaderText = "Employee"
        Me.dgColhEmployee.MinimumWidth = 175
        Me.dgColhEmployee.Name = "dgColhEmployee"
        Me.dgColhEmployee.ReadOnly = True
        Me.dgColhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgColhEmployee.Width = 250
        '
        'dgColhAppointmentDt
        '
        Me.dgColhAppointmentDt.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhAppointmentDt.HeaderText = "Appointment Date"
        Me.dgColhAppointmentDt.Name = "dgColhAppointmentDt"
        Me.dgColhAppointmentDt.ReadOnly = True
        Me.dgColhAppointmentDt.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgColhAppointmentDt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgColhJobtitle
        '
        Me.dgColhJobtitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhJobtitle.HeaderText = "Job Title"
        Me.dgColhJobtitle.Name = "dgColhJobtitle"
        Me.dgColhJobtitle.ReadOnly = True
        Me.dgColhJobtitle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgColhJobtitle.Width = 230
        '
        'dgColhLeaveBF
        '
        Me.dgColhLeaveBF.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgColhLeaveBF.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgColhLeaveBF.HeaderText = "Leave B/F"
        Me.dgColhLeaveBF.Name = "dgColhLeaveBF"
        Me.dgColhLeaveBF.ReadOnly = True
        Me.dgColhLeaveBF.Width = 80
        '
        'dgColhAccruetodate
        '
        Me.dgColhAccruetodate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgColhAccruetodate.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgColhAccruetodate.HeaderText = "Accrue to Date"
        Me.dgColhAccruetodate.Name = "dgColhAccruetodate"
        Me.dgColhAccruetodate.ReadOnly = True
        '
        'dgcolhIssuetodate
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhIssuetodate.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhIssuetodate.HeaderText = "Issue to Date"
        Me.dgcolhIssuetodate.Name = "dgcolhIssuetodate"
        Me.dgcolhIssuetodate.ReadOnly = True
        '
        'dgcolhBalanceAsonDate
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhBalanceAsonDate.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhBalanceAsonDate.HeaderText = "Balance to Date"
        Me.dgcolhBalanceAsonDate.Name = "dgcolhBalanceAsonDate"
        Me.dgcolhBalanceAsonDate.ReadOnly = True
        '
        'dgcolhEncashment
        '
        Me.dgcolhEncashment.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhEncashment.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgcolhEncashment.HeaderText = "Encashment"
        Me.dgcolhEncashment.Name = "dgcolhEncashment"
        Me.dgcolhEncashment.ReadOnly = True
        '
        'dgcolhBalanceAmt
        '
        Me.dgcolhBalanceAmt.DecimalLength = 2
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "F2"
        Me.dgcolhBalanceAmt.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgcolhBalanceAmt.HeaderText = "Balance Amount"
        Me.dgcolhBalanceAmt.Name = "dgcolhBalanceAmt"
        Me.dgcolhBalanceAmt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhBalanceAmt.Width = 120
        '
        'dgcolhLeaveBFAdjustment
        '
        Me.dgcolhLeaveBFAdjustment.DecimalLength = 2
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "F2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.dgcolhLeaveBFAdjustment.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgcolhLeaveBFAdjustment.HeaderText = "Leave BF Adjustment"
        Me.dgcolhLeaveBFAdjustment.Name = "dgcolhLeaveBFAdjustment"
        Me.dgcolhLeaveBFAdjustment.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhLeaveBFAdjustment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhLeaveBFAdjustment.Width = 130
        '
        'dgcolhRemaining_Bal
        '
        Me.dgcolhRemaining_Bal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhRemaining_Bal.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgcolhRemaining_Bal.HeaderText = "Total Remaining Balance"
        Me.dgcolhRemaining_Bal.Name = "dgcolhRemaining_Bal"
        Me.dgcolhRemaining_Bal.ReadOnly = True
        Me.dgcolhRemaining_Bal.Width = 150
        '
        'dgColhIssueLeave
        '
        Me.dgColhIssueLeave.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgColhIssueLeave.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgColhIssueLeave.HeaderText = "Issue Leave"
        Me.dgColhIssueLeave.Name = "dgColhIssueLeave"
        Me.dgColhIssueLeave.ReadOnly = True
        Me.dgColhIssueLeave.Visible = False
        '
        'TxtSearch
        '
        Me.TxtSearch.Flags = 0
        Me.TxtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.TxtSearch.Location = New System.Drawing.Point(1, 139)
        Me.TxtSearch.Name = "TxtSearch"
        Me.TxtSearch.Size = New System.Drawing.Size(726, 21)
        Me.TxtSearch.TabIndex = 8
        '
        'txtAdjustmentAmount
        '
        Me.txtAdjustmentAmount.AllowNegative = True
        Me.txtAdjustmentAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtAdjustmentAmount.DigitsInGroup = 0
        Me.txtAdjustmentAmount.Flags = 0
        Me.txtAdjustmentAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAdjustmentAmount.Location = New System.Drawing.Point(264, 56)
        Me.txtAdjustmentAmount.MaxDecimalPlaces = 2
        Me.txtAdjustmentAmount.MaxWholeDigits = 6
        Me.txtAdjustmentAmount.Name = "txtAdjustmentAmount"
        Me.txtAdjustmentAmount.Prefix = ""
        Me.txtAdjustmentAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAdjustmentAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAdjustmentAmount.Size = New System.Drawing.Size(93, 21)
        Me.txtAdjustmentAmount.TabIndex = 6
        Me.txtAdjustmentAmount.Text = "0.00"
        Me.txtAdjustmentAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAdjustmentAmount
        '
        Me.lblAdjustmentAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdjustmentAmount.Location = New System.Drawing.Point(149, 59)
        Me.lblAdjustmentAmount.Name = "lblAdjustmentAmount"
        Me.lblAdjustmentAmount.Size = New System.Drawing.Size(109, 15)
        Me.lblAdjustmentAmount.TabIndex = 293
        Me.lblAdjustmentAmount.Text = "Balance Amount"
        '
        'gbLeaveAdjustment
        '
        Me.gbLeaveAdjustment.BorderColor = System.Drawing.Color.Black
        Me.gbLeaveAdjustment.Checked = False
        Me.gbLeaveAdjustment.CollapseAllExceptThis = False
        Me.gbLeaveAdjustment.CollapsedHoverImage = Nothing
        Me.gbLeaveAdjustment.CollapsedNormalImage = Nothing
        Me.gbLeaveAdjustment.CollapsedPressedImage = Nothing
        Me.gbLeaveAdjustment.CollapseOnLoad = False
        Me.gbLeaveAdjustment.Controls.Add(Me.chkLeaveBFAdjustment)
        Me.gbLeaveAdjustment.Controls.Add(Me.chkBalanceAmount)
        Me.gbLeaveAdjustment.Controls.Add(Me.txtLeaveBFAdjustment)
        Me.gbLeaveAdjustment.Controls.Add(Me.LblLeaveBFAdjustment)
        Me.gbLeaveAdjustment.Controls.Add(Me.objLine1)
        Me.gbLeaveAdjustment.Controls.Add(Me.btnSet)
        Me.gbLeaveAdjustment.Controls.Add(Me.rdAssignIndividually)
        Me.gbLeaveAdjustment.Controls.Add(Me.rdAssignGlobally)
        Me.gbLeaveAdjustment.Controls.Add(Me.lblAdjustmentAmount)
        Me.gbLeaveAdjustment.Controls.Add(Me.lblDate)
        Me.gbLeaveAdjustment.Controls.Add(Me.txtAdjustmentAmount)
        Me.gbLeaveAdjustment.Controls.Add(Me.dtpDate)
        Me.gbLeaveAdjustment.ExpandedHoverImage = Nothing
        Me.gbLeaveAdjustment.ExpandedNormalImage = Nothing
        Me.gbLeaveAdjustment.ExpandedPressedImage = Nothing
        Me.gbLeaveAdjustment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeaveAdjustment.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeaveAdjustment.HeaderHeight = 25
        Me.gbLeaveAdjustment.HeaderMessage = ""
        Me.gbLeaveAdjustment.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeaveAdjustment.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeaveAdjustment.HeightOnCollapse = 0
        Me.gbLeaveAdjustment.LeftTextSpace = 0
        Me.gbLeaveAdjustment.Location = New System.Drawing.Point(289, 1)
        Me.gbLeaveAdjustment.Name = "gbLeaveAdjustment"
        Me.gbLeaveAdjustment.OpenHeight = 300
        Me.gbLeaveAdjustment.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeaveAdjustment.ShowBorder = True
        Me.gbLeaveAdjustment.ShowCheckBox = False
        Me.gbLeaveAdjustment.ShowCollapseButton = False
        Me.gbLeaveAdjustment.ShowDefaultBorderColor = True
        Me.gbLeaveAdjustment.ShowDownButton = False
        Me.gbLeaveAdjustment.ShowHeader = True
        Me.gbLeaveAdjustment.Size = New System.Drawing.Size(437, 135)
        Me.gbLeaveAdjustment.TabIndex = 302
        Me.gbLeaveAdjustment.Temp = 0
        Me.gbLeaveAdjustment.Text = "Leave Adjustment"
        Me.gbLeaveAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkLeaveBFAdjustment
        '
        Me.chkLeaveBFAdjustment.AutoSize = True
        Me.chkLeaveBFAdjustment.Location = New System.Drawing.Point(133, 85)
        Me.chkLeaveBFAdjustment.Name = "chkLeaveBFAdjustment"
        Me.chkLeaveBFAdjustment.Size = New System.Drawing.Size(15, 14)
        Me.chkLeaveBFAdjustment.TabIndex = 318
        Me.chkLeaveBFAdjustment.UseVisualStyleBackColor = True
        '
        'chkBalanceAmount
        '
        Me.chkBalanceAmount.AutoSize = True
        Me.chkBalanceAmount.Location = New System.Drawing.Point(133, 59)
        Me.chkBalanceAmount.Name = "chkBalanceAmount"
        Me.chkBalanceAmount.Size = New System.Drawing.Size(15, 14)
        Me.chkBalanceAmount.TabIndex = 317
        Me.chkBalanceAmount.UseVisualStyleBackColor = True
        '
        'txtLeaveBFAdjustment
        '
        Me.txtLeaveBFAdjustment.AllowNegative = True
        Me.txtLeaveBFAdjustment.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtLeaveBFAdjustment.DigitsInGroup = 0
        Me.txtLeaveBFAdjustment.Flags = 0
        Me.txtLeaveBFAdjustment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLeaveBFAdjustment.Location = New System.Drawing.Point(264, 82)
        Me.txtLeaveBFAdjustment.MaxDecimalPlaces = 2
        Me.txtLeaveBFAdjustment.MaxWholeDigits = 6
        Me.txtLeaveBFAdjustment.Name = "txtLeaveBFAdjustment"
        Me.txtLeaveBFAdjustment.Prefix = ""
        Me.txtLeaveBFAdjustment.RangeMax = 1.7976931348623157E+308
        Me.txtLeaveBFAdjustment.RangeMin = -1.7976931348623157E+308
        Me.txtLeaveBFAdjustment.Size = New System.Drawing.Size(93, 21)
        Me.txtLeaveBFAdjustment.TabIndex = 316
        Me.txtLeaveBFAdjustment.Text = "0.00"
        Me.txtLeaveBFAdjustment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblLeaveBFAdjustment
        '
        Me.LblLeaveBFAdjustment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLeaveBFAdjustment.Location = New System.Drawing.Point(149, 85)
        Me.LblLeaveBFAdjustment.Name = "LblLeaveBFAdjustment"
        Me.LblLeaveBFAdjustment.Size = New System.Drawing.Size(109, 15)
        Me.LblLeaveBFAdjustment.TabIndex = 308
        Me.LblLeaveBFAdjustment.Text = "Leave BF Adjustment"
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(127, 24)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(5, 110)
        Me.objLine1.TabIndex = 314
        '
        'btnSet
        '
        Me.btnSet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSet.BackColor = System.Drawing.Color.White
        Me.btnSet.BackgroundImage = CType(resources.GetObject("btnSet.BackgroundImage"), System.Drawing.Image)
        Me.btnSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSet.BorderColor = System.Drawing.Color.Empty
        Me.btnSet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSet.FlatAppearance.BorderSize = 0
        Me.btnSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSet.ForeColor = System.Drawing.Color.Black
        Me.btnSet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSet.GradientForeColor = System.Drawing.Color.Black
        Me.btnSet.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSet.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSet.Location = New System.Drawing.Point(361, 58)
        Me.btnSet.Name = "btnSet"
        Me.btnSet.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSet.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSet.Size = New System.Drawing.Size(72, 44)
        Me.btnSet.TabIndex = 7
        Me.btnSet.Text = "Set"
        Me.btnSet.UseVisualStyleBackColor = True
        '
        'rdAssignIndividually
        '
        Me.rdAssignIndividually.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdAssignIndividually.Location = New System.Drawing.Point(5, 58)
        Me.rdAssignIndividually.Name = "rdAssignIndividually"
        Me.rdAssignIndividually.Size = New System.Drawing.Size(117, 17)
        Me.rdAssignIndividually.TabIndex = 4
        Me.rdAssignIndividually.Text = "Assign Individually"
        Me.rdAssignIndividually.UseVisualStyleBackColor = True
        '
        'rdAssignGlobally
        '
        Me.rdAssignGlobally.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdAssignGlobally.Location = New System.Drawing.Point(5, 32)
        Me.rdAssignGlobally.Name = "rdAssignGlobally"
        Me.rdAssignGlobally.Size = New System.Drawing.Size(123, 17)
        Me.rdAssignGlobally.TabIndex = 3
        Me.rdAssignGlobally.Text = "Assign Globally"
        Me.rdAssignGlobally.UseVisualStyleBackColor = True
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(10, 168)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 311
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.MinimumWidth = 175
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 175
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.MinimumWidth = 175
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn3.HeaderText = "Appointment Date"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 230
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle10
        Me.DataGridViewTextBoxColumn4.HeaderText = "Job Title"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 230
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle11
        Me.DataGridViewTextBoxColumn5.HeaderText = "Accrue to Date"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle12
        Me.DataGridViewTextBoxColumn6.HeaderText = "Issue Leave"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle13
        Me.DataGridViewTextBoxColumn7.HeaderText = "Remaining Balance"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 120
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle14
        Me.DataGridViewTextBoxColumn8.HeaderText = "Issue Leave"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Visible = False
        Me.DataGridViewTextBoxColumn8.Width = 150
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn9.DefaultCellStyle = DataGridViewCellStyle15
        Me.DataGridViewTextBoxColumn9.HeaderText = "Issue Leave"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Visible = False
        Me.DataGridViewTextBoxColumn9.Width = 150
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn10.DefaultCellStyle = DataGridViewCellStyle16
        Me.DataGridViewTextBoxColumn10.HeaderText = "Issue Leave"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'frmLeaveAdjustment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(729, 494)
        Me.Controls.Add(Me.gbLeaveAdjustment)
        Me.Controls.Add(Me.chkSelectAll)
        Me.Controls.Add(Me.TxtSearch)
        Me.Controls.Add(Me.dgEmployee)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.lblRemark)
        Me.Controls.Add(Me.txtRemark)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLeaveAdjustment"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Leave Adjustment"
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.nudLeaveIssueAmt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLeaveBF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbLeaveAdjustment.ResumeLayout(False)
        Me.gbLeaveAdjustment.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblLeaveType As System.Windows.Forms.Label
    Friend WithEvents cboLeaveType As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnsearchLeaveType As eZee.Common.eZeeGradientButton
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents TxtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAdjustmentAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAdjustmentAmount As System.Windows.Forms.Label
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents gbLeaveAdjustment As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rdAssignIndividually As System.Windows.Forms.RadioButton
    Friend WithEvents rdAssignGlobally As System.Windows.Forms.RadioButton
    Friend WithEvents btnSet As eZee.Common.eZeeLightButton
    Friend WithEvents LblLeaveBF As System.Windows.Forms.Label
    Friend WithEvents nudLeaveBF As System.Windows.Forms.NumericUpDown
    Friend WithEvents cboLeaveBFCondition As System.Windows.Forms.ComboBox
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents cboLeaveIssueAmtCondition As System.Windows.Forms.ComboBox
    Friend WithEvents nudLeaveIssueAmt As System.Windows.Forms.NumericUpDown
    Friend WithEvents LblLeaveIssueAmt As System.Windows.Forms.Label
    Friend WithEvents txtLeaveBFAdjustment As eZee.TextBox.NumericTextBox
    Friend WithEvents LblLeaveBFAdjustment As System.Windows.Forms.Label
    Friend WithEvents chkLeaveBFAdjustment As System.Windows.Forms.CheckBox
    Friend WithEvents chkBalanceAmount As System.Windows.Forms.CheckBox
    Friend WithEvents objSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgColhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhAppointmentDt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhJobtitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhLeaveBF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhAccruetodate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIssuetodate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBalanceAsonDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEncashment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBalanceAmt As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhLeaveBFAdjustment As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhRemaining_Bal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhIssueLeave As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
