﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmAssignHolidaysList


#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmAssignHolidaysList"
    Private objEmployeeholiday As clsemployee_holiday
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 
#End Region

#Region "Form's Event"

    Private Sub frmAssignHolidaysList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmployeeholiday = New clsemployee_holiday
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            dtpApplyDate.MinDate = FinancialYear._Object._Database_Start_Date.Date

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            'dtpApplyDate.MaxDate = FinancialYear._Object._Database_End_Date.Date
            'Pinkal (21-Jul-2014) -- End
            FillCombo()
            'fillList()
            cboEmployee.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignHolidaysList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignHolidaysList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvAssignHolidays.Focused Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignHolidaysList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub lvAssignHolidays_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lvAssignHolidays.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAssignHolidays_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignHolidaysList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objEmployeeholiday = Nothing
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()


            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 


#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmAssignHolidays
            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvAssignHolidays.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee Holiday from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvAssignHolidays.Select()
            Exit Sub
        End If
        Dim objfrmAssignHolidays As New frmAssignHolidays
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAssignHolidays.SelectedItems(0).Index
            If objfrmAssignHolidays.displayDialog(CInt(lvAssignHolidays.SelectedItems(0).Tag), enAction.EDIT_ONE, CInt(lvAssignHolidays.SelectedItems(0).SubItems.Item(colhEmployeeName.Index).Tag)) Then
                fillList()
            End If
            objfrmAssignHolidays = Nothing

            lvAssignHolidays.Items(intSelectedIndex).Selected = True
            lvAssignHolidays.EnsureVisible(intSelectedIndex)
            lvAssignHolidays.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmAssignHolidays IsNot Nothing Then objfrmAssignHolidays.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAssignHolidays.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Check Employee Holiday from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvAssignHolidays.Select()
            Exit Sub
        End If
        'If objEmployeeholiday.isUsed(CInt(lvAssignHolidays.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Employee Holiday. Reason: This Employee Holiday is in use."), enMsgBoxStyle.Information) '?2
        '    lvAssignHolidays.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAssignHolidays.CheckedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Employee Holiday?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'Pinkal (13-Jan-2015) -- Start
                'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 

                Dim mblnIsPeriodClose As Boolean = False
                Dim mblnIsPaymentDone As Boolean = False

                For i As Integer = 0 To lvAssignHolidays.CheckedItems.Count - 1
                    If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                        Dim objHoliday As New clsholiday_master
                        objHoliday._Holidayunkid = CInt(lvAssignHolidays.CheckedItems(0).SubItems(objColhHolidayId.Index).Text)
                        Dim objMaster As New clsMasterData
                        Dim intPeriodId As Integer = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, objHoliday._Holidaydate.Date)
                        Dim objPeriod As New clscommom_period_Tran
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objPeriod._Periodunkid = intPeriodId
                        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodId
                        'Sohail (21 Aug 2015) -- End
                        If objPeriod._Statusid = 2 Then  'Close
                            mblnIsPeriodClose = True
                            Continue For
                        ElseIf objPeriod._Statusid = 1 Then  'Open

                            Dim mdtTnAStartdate As Date = Nothing
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'If objPeriod._Periodunkid > 0 Then
                            'mdtTnAStartdate = objPeriod.GetTnA_StartDate(objPeriod._Periodunkid)
                            If objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) > 0 Then
                                mdtTnAStartdate = objPeriod.GetTnA_StartDate(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName))
                                'Sohail (21 Aug 2015) -- End
                            End If

                            Dim objPaymentTran As New clsPayment_tran
                            If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(lvAssignHolidays.CheckedItems(0).SubItems(objColhEmployeeID.Index).Text), mdtTnAStartdate, intPeriodId, objHoliday._Holidaydate.Date) > 0 Then
                                mblnIsPaymentDone = True
                                Continue For
                            End If
                        End If
                    End If

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objEmployeeholiday._FormName = mstrModuleName
                    objEmployeeholiday._LoginEmployeeunkid = 0
                    objEmployeeholiday._ClientIP = getIP()
                    objEmployeeholiday._HostName = getHostName()
                    objEmployeeholiday._FromWeb = False
                    objEmployeeholiday._AuditUserId = User._Object._Userunkid
objEmployeeholiday._CompanyUnkid = Company._Object._Companyunkid
                    objEmployeeholiday._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    objEmployeeholiday.Delete(CInt(lvAssignHolidays.CheckedItems(0).Tag))
                    lvAssignHolidays.CheckedItems(0).Remove()
                Next

                If mblnIsPeriodClose Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't delete some holiday(s) for some employee(s).Reason: Holiday dates are falling in a period which is already closed."), CType(enMsgBoxStyle.OkOnly + enMsgBoxStyle.Information, enMsgBoxStyle))
                ElseIf mblnIsPaymentDone Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Payroll payment for some employee(s) are already done for this tenure.you can't delete some holiday(s) to some employee(s)."), CType(enMsgBoxStyle.OkOnly + enMsgBoxStyle.Information, enMsgBoxStyle))
                End If

                'Pinkal (13-Jan-2015) -- End

                fillList()

                If lvAssignHolidays.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvAssignHolidays.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvAssignHolidays.Items.Count - 1
                    lvAssignHolidays.Items(intSelectedIndex).Selected = True
                    lvAssignHolidays.EnsureVisible(intSelectedIndex)
                ElseIf lvAssignHolidays.Items.Count <> 0 Then
                    lvAssignHolidays.Items(intSelectedIndex).Selected = True
                    lvAssignHolidays.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvAssignHolidays.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try

            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is Compulsory information.Please Select Employee to perform further operation."))
                Exit Sub
            End If
            fillList()
            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedIndex = 0

            'Pinkal (03-Jan-2011) -- Start

            If FinancialYear._Object._Database_End_Date.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpApplyDate.Value = FinancialYear._Object._Database_End_Date.Date
            ElseIf FinancialYear._Object._Database_End_Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpApplyDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            End If

            'Pinkal (03-Jan-2011) -- End

            dtpApplyDate.Checked = False
            txtHolidayName.Text = ""
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End 

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            lvAssignHolidays.Items.Clear()
            'Pinkal (12-Oct-2011) -- End


            'Pinkal (07-APR-2012) -- Start
            'Enhancement : TRA Changes
            ' fillList()
            'Pinkal (07-APR-2012) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()

        Dim dsFill As DataSet = Nothing
        Try

            'FOR Employee
            Dim objEmployee As New clsEmployee_Master


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : CHECK FOR ACTIVE EMPLOYEE
            'dsFill = objEmployee.GetEmployeeList("Employee", True, True)

            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'dsFill = objEmployee.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If
            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Anjan (17 Apr 2012)-End 


            'Pinkal (24-Jun-2011) -- End

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsFill.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsEmployeeHolidayList As DataSet = Nothing
        Dim dtEmployeeHolidayList As DataTable = Nothing
        Dim strSearch As String = String.Empty
        Try


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'dsEmployeeHolidayList = objEmployeeholiday.GetList("Holiday List", CInt(cboEmployee.SelectedValue), , , , , mstrAdvanceFilter)

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch = "AND lvemployee_holiday.employeeunkid=" & CInt(cboEmployee.SelectedValue) & " "
            End If

            If dtpApplyDate.Checked Then
                strSearch &= "AND lvemployee_holiday.holidaydate='" & eZeeDate.convertDate(dtpApplyDate.Value) & "' "
            End If
            If txtHolidayName.Text.Length > 0 Then
                strSearch &= "AND holidayname like '%" & txtHolidayName.Text.Trim & "%' "
            End If

            dsEmployeeHolidayList = objEmployeeholiday.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                                            , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                            , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                                            , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, -1, Nothing, "", strSearch)


            'If strSearch.Length > 0 Then
            '    strSearch = strSearch.Substring(3)
            '    dtEmployeeHolidayList = New DataView(dsEmployeeHolidayList.Tables("Holiday List"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtEmployeeHolidayList = dsEmployeeHolidayList.Tables("Holiday List")
            'End If

            'Pinkal (24-Aug-2015) -- End

            Dim lvItem As ListViewItem
            lvAssignHolidays.Items.Clear()

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'For Each drRow As DataRow In dtEmployeeHolidayList.Rows
            For Each drRow As DataRow In dsEmployeeHolidayList.Tables(0).Rows
                'Pinkal (24-Aug-2015) -- End
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.Tag = drRow("holidaytranunkid")
                lvItem.SubItems.Add(drRow("holidayname").ToString)
                lvItem.SubItems.Add(drRow("name").ToString)
                lvItem.SubItems.Item(colhEmployeeName.Index).Tag = CInt(drRow("employeeunkid"))
                lvItem.SubItems.Add(eZeeDate.convertDate(drRow("holidaydate").ToString).ToShortDateString)


                'Pinkal (13-Jan-2015) -- Start
                'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
                lvItem.SubItems.Add(drRow("employeeunkid").ToString)
                lvItem.SubItems.Add(drRow("holidayunkid").ToString)
                'Pinkal (13-Jan-2015) -- End


                lvAssignHolidays.Items.Add(lvItem)
            Next
            lvAssignHolidays.GridLines = False
            lvAssignHolidays.GroupingColumn = colhEmployeeName
            lvAssignHolidays.DisplayGroups(True)

            If lvAssignHolidays.Items.Count > 16 Then
                colhDate.Width = 115 - 18
            Else
                colhDate.Width = 115
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsEmployeeHolidayList.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddEmployeeHolidays
            btnEdit.Enabled = User._Object.Privilege._EditEmployeeHolidays
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeHolidays
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region




    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Anjan (21 Nov 2011)-End 
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhEmployeeName.Text = Language._Object.getCaption(CStr(Me.colhEmployeeName.Tag), Me.colhEmployeeName.Text)
            Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
            Me.colhcheck.Text = Language._Object.getCaption(CStr(Me.colhcheck.Tag), Me.colhcheck.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Employee Holiday from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Please Check Employee Holiday from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Employee Holiday?")
            Language.setMessage(mstrModuleName, 4, "Employee is Compulsory information.Please Select Employee to perform further operation.")
            Language.setMessage(mstrModuleName, 5, "You can't delete some holiday(s) for some employee(s).Reason: Holiday dates are falling in a period which is already closed.")
            Language.setMessage(mstrModuleName, 6, "Payroll payment for some employee(s) are already done for this tenure.you can't delete some holiday(s) to some employee(s).")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class