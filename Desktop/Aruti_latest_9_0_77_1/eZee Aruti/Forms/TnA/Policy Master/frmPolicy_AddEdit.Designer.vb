﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPolicy_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPolicy_AddEdit))
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.EZeeHeading1 = New eZee.Common.eZeeHeading
        Me.lnkCopyToAll = New System.Windows.Forms.LinkLabel
        Me.dgPolicy = New System.Windows.Forms.DataGridView
        Me.dgcolhDay = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhbreaktime = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhCntBreakTimeAfter = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhteatime = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhCntTeaTimeAfter = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhWeekend = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhBaseHrs = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgColhOT1 = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhOT2 = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhOT3 = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhOT4 = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhEarlycoming = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhLatecoming = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhEarlygoing = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhLateGoing = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkOTPenalty = New System.Windows.Forms.LinkLabel
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.lblcode = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.EZeeHeading1.SuspendLayout()
        CType(Me.dgPolicy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.EZeeHeading1)
        Me.pnlMain.Controls.Add(Me.dgPolicy)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.gbInfo)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(512, 403)
        Me.pnlMain.TabIndex = 0
        '
        'EZeeHeading1
        '
        Me.EZeeHeading1.BorderColor = System.Drawing.Color.Black
        Me.EZeeHeading1.Controls.Add(Me.lnkCopyToAll)
        Me.EZeeHeading1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeading1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeHeading1.Location = New System.Drawing.Point(12, 99)
        Me.EZeeHeading1.Name = "EZeeHeading1"
        Me.EZeeHeading1.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        Me.EZeeHeading1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeHeading1.ShowDefaultBorderColor = True
        Me.EZeeHeading1.Size = New System.Drawing.Size(488, 25)
        Me.EZeeHeading1.TabIndex = 106
        '
        'lnkCopyToAll
        '
        Me.lnkCopyToAll.BackColor = System.Drawing.Color.Transparent
        Me.lnkCopyToAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkCopyToAll.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkCopyToAll.Location = New System.Drawing.Point(390, 3)
        Me.lnkCopyToAll.Name = "lnkCopyToAll"
        Me.lnkCopyToAll.Size = New System.Drawing.Size(91, 19)
        Me.lnkCopyToAll.TabIndex = 107
        Me.lnkCopyToAll.TabStop = True
        Me.lnkCopyToAll.Text = "Copy To All"
        Me.lnkCopyToAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgPolicy
        '
        Me.dgPolicy.AllowUserToAddRows = False
        Me.dgPolicy.AllowUserToDeleteRows = False
        Me.dgPolicy.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgPolicy.ColumnHeadersHeight = 33
        Me.dgPolicy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgPolicy.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhDay, Me.dgcolhbreaktime, Me.dgcolhCntBreakTimeAfter, Me.dgcolhteatime, Me.dgcolhCntTeaTimeAfter, Me.dgcolhWeekend, Me.dgcolhBaseHrs, Me.dgColhOT1, Me.dgcolhOT2, Me.dgcolhOT3, Me.dgcolhOT4, Me.dgcolhEarlycoming, Me.dgcolhLatecoming, Me.dgcolhEarlygoing, Me.dgcolhLateGoing})
        Me.dgPolicy.Location = New System.Drawing.Point(12, 124)
        Me.dgPolicy.Name = "dgPolicy"
        Me.dgPolicy.RowHeadersVisible = False
        Me.dgPolicy.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgPolicy.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgPolicy.Size = New System.Drawing.Size(488, 215)
        Me.dgPolicy.TabIndex = 105
        '
        'dgcolhDay
        '
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgcolhDay.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhDay.Frozen = True
        Me.dgcolhDay.HeaderText = "Day"
        Me.dgcolhDay.Name = "dgcolhDay"
        Me.dgcolhDay.ReadOnly = True
        Me.dgcolhDay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhDay.Width = 125
        '
        'dgcolhbreaktime
        '
        Me.dgcolhbreaktime.AllowNegative = False
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhbreaktime.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhbreaktime.HeaderText = "Breaktime (Mins.)"
        Me.dgcolhbreaktime.Name = "dgcolhbreaktime"
        Me.dgcolhbreaktime.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhbreaktime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCntBreakTimeAfter
        '
        Me.dgcolhCntBreakTimeAfter.AllowNegative = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "F0"
        Me.dgcolhCntBreakTimeAfter.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhCntBreakTimeAfter.HeaderText = "Count Breaktime After (Mins.)"
        Me.dgcolhCntBreakTimeAfter.Name = "dgcolhCntBreakTimeAfter"
        Me.dgcolhCntBreakTimeAfter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhteatime
        '
        Me.dgcolhteatime.AllowNegative = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhteatime.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhteatime.HeaderText = "Teatime (Mins.)"
        Me.dgcolhteatime.Name = "dgcolhteatime"
        Me.dgcolhteatime.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhteatime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCntTeaTimeAfter
        '
        Me.dgcolhCntTeaTimeAfter.AllowNegative = False
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhCntTeaTimeAfter.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgcolhCntTeaTimeAfter.HeaderText = "Count Teatime After (Mins.)"
        Me.dgcolhCntTeaTimeAfter.Name = "dgcolhCntTeaTimeAfter"
        Me.dgcolhCntTeaTimeAfter.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhCntTeaTimeAfter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhWeekend
        '
        Me.dgcolhWeekend.HeaderText = "Weekend"
        Me.dgcolhWeekend.Name = "dgcolhWeekend"
        Me.dgcolhWeekend.Visible = False
        Me.dgcolhWeekend.Width = 60
        '
        'dgcolhBaseHrs
        '
        Me.dgcolhBaseHrs.AllowNegative = False
        Me.dgcolhBaseHrs.DecimalLength = 2
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "F2"
        Me.dgcolhBaseHrs.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgcolhBaseHrs.HeaderText = "Base Hours"
        Me.dgcolhBaseHrs.Name = "dgcolhBaseHrs"
        Me.dgcolhBaseHrs.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhBaseHrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhBaseHrs.Visible = False
        '
        'dgColhOT1
        '
        Me.dgColhOT1.AllowNegative = False
        Me.dgColhOT1.DecimalLength = 2
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "F2"
        Me.dgColhOT1.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgColhOT1.HeaderText = "O.T 1 (Hours)"
        Me.dgColhOT1.Name = "dgColhOT1"
        Me.dgColhOT1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhOT2
        '
        Me.dgcolhOT2.AllowNegative = False
        Me.dgcolhOT2.DecimalLength = 2
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "F2"
        Me.dgcolhOT2.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgcolhOT2.HeaderText = "O.T 2 (Hours)"
        Me.dgcolhOT2.Name = "dgcolhOT2"
        Me.dgcolhOT2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhOT3
        '
        Me.dgcolhOT3.AllowNegative = False
        Me.dgcolhOT3.DecimalLength = 2
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "F2"
        Me.dgcolhOT3.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgcolhOT3.HeaderText = "O.T 3 (Hours)"
        Me.dgcolhOT3.Name = "dgcolhOT3"
        Me.dgcolhOT3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhOT4
        '
        Me.dgcolhOT4.AllowNegative = False
        Me.dgcolhOT4.DecimalLength = 2
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "F2"
        Me.dgcolhOT4.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgcolhOT4.HeaderText = "O.T 4 (Hours)"
        Me.dgcolhOT4.Name = "dgcolhOT4"
        Me.dgcolhOT4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEarlycoming
        '
        Me.dgcolhEarlycoming.AllowNegative = False
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "F0"
        Me.dgcolhEarlycoming.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgcolhEarlycoming.HeaderText = "Early Coming Grace (In Mins)"
        Me.dgcolhEarlycoming.Name = "dgcolhEarlycoming"
        Me.dgcolhEarlycoming.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEarlycoming.Width = 160
        '
        'dgcolhLatecoming
        '
        Me.dgcolhLatecoming.AllowNegative = False
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "F0"
        Me.dgcolhLatecoming.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgcolhLatecoming.HeaderText = "Late Coming Grace (In Mins)"
        Me.dgcolhLatecoming.Name = "dgcolhLatecoming"
        Me.dgcolhLatecoming.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhLatecoming.Width = 150
        '
        'dgcolhEarlygoing
        '
        Me.dgcolhEarlygoing.AllowNegative = False
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "F0"
        Me.dgcolhEarlygoing.DefaultCellStyle = DataGridViewCellStyle13
        Me.dgcolhEarlygoing.HeaderText = "Early Going Grace (In Mins)"
        Me.dgcolhEarlygoing.Name = "dgcolhEarlygoing"
        Me.dgcolhEarlygoing.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEarlygoing.Width = 150
        '
        'dgcolhLateGoing
        '
        Me.dgcolhLateGoing.AllowNegative = False
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.Format = "F0"
        Me.dgcolhLateGoing.DefaultCellStyle = DataGridViewCellStyle14
        Me.dgcolhLateGoing.HeaderText = "Late Going Grace (In Mins)"
        Me.dgcolhLateGoing.Name = "dgcolhLateGoing"
        Me.dgcolhLateGoing.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhLateGoing.Width = 150
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objFooter.Location = New System.Drawing.Point(0, 348)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(512, 55)
        Me.objFooter.TabIndex = 104
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(317, 12)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 23
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(413, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 24
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbInfo
        '
        Me.gbInfo.BackColor = System.Drawing.Color.Transparent
        Me.gbInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInfo.Checked = False
        Me.gbInfo.CollapseAllExceptThis = False
        Me.gbInfo.CollapsedHoverImage = Nothing
        Me.gbInfo.CollapsedNormalImage = Nothing
        Me.gbInfo.CollapsedPressedImage = Nothing
        Me.gbInfo.CollapseOnLoad = False
        Me.gbInfo.Controls.Add(Me.lnkOTPenalty)
        Me.gbInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbInfo.Controls.Add(Me.lblcode)
        Me.gbInfo.Controls.Add(Me.lblName)
        Me.gbInfo.Controls.Add(Me.txtName)
        Me.gbInfo.Controls.Add(Me.txtCode)
        Me.gbInfo.ExpandedHoverImage = Nothing
        Me.gbInfo.ExpandedNormalImage = Nothing
        Me.gbInfo.ExpandedPressedImage = Nothing
        Me.gbInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInfo.HeaderHeight = 25
        Me.gbInfo.HeaderMessage = ""
        Me.gbInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInfo.HeightOnCollapse = 0
        Me.gbInfo.LeftTextSpace = 0
        Me.gbInfo.Location = New System.Drawing.Point(12, 7)
        Me.gbInfo.Name = "gbInfo"
        Me.gbInfo.OpenHeight = 300
        Me.gbInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.gbInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInfo.ShowBorder = True
        Me.gbInfo.ShowCheckBox = False
        Me.gbInfo.ShowCollapseButton = False
        Me.gbInfo.ShowDefaultBorderColor = True
        Me.gbInfo.ShowDownButton = False
        Me.gbInfo.ShowHeader = True
        Me.gbInfo.Size = New System.Drawing.Size(488, 88)
        Me.gbInfo.TabIndex = 102
        Me.gbInfo.Temp = 0
        Me.gbInfo.Text = "Policy Information"
        Me.gbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkOTPenalty
        '
        Me.lnkOTPenalty.BackColor = System.Drawing.Color.Transparent
        Me.lnkOTPenalty.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkOTPenalty.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkOTPenalty.Location = New System.Drawing.Point(343, 3)
        Me.lnkOTPenalty.Name = "lnkOTPenalty"
        Me.lnkOTPenalty.Size = New System.Drawing.Size(140, 19)
        Me.lnkOTPenalty.TabIndex = 109
        Me.lnkOTPenalty.TabStop = True
        Me.lnkOTPenalty.Text = "Add/Edit OT Penalty"
        Me.lnkOTPenalty.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkOTPenalty.Visible = False
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(460, 59)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 4
        '
        'lblcode
        '
        Me.lblcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblcode.Location = New System.Drawing.Point(8, 36)
        Me.lblcode.Name = "lblcode"
        Me.lblcode.Size = New System.Drawing.Size(80, 13)
        Me.lblcode.TabIndex = 0
        Me.lblcode.Text = "Code"
        Me.lblcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 61)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(80, 13)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(94, 59)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(360, 21)
        Me.txtName.TabIndex = 3
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(94, 32)
        Me.txtCode.MaxLength = 5
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(78, 21)
        Me.txtCode.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle15
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Day"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 125
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle16
        Me.DataGridViewTextBoxColumn2.HeaderText = "Working Hrs"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle17.Format = "N2"
        DataGridViewCellStyle17.NullValue = Nothing
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle17
        Me.DataGridViewTextBoxColumn3.HeaderText = "Mark as Half Day From Hrs"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 160
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle18.Format = "N2"
        DataGridViewCellStyle18.NullValue = Nothing
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle18
        Me.DataGridViewTextBoxColumn4.HeaderText = "Mark as Half Day To Hrs"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 160
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Night From Hrs"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Night To Hrs"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'frmPolicy_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(512, 403)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPolicy_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add Edit Policy"
        Me.pnlMain.ResumeLayout(False)
        Me.EZeeHeading1.ResumeLayout(False)
        CType(Me.dgPolicy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbInfo.ResumeLayout(False)
        Me.gbInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents lblcode As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgPolicy As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EZeeHeading1 As eZee.Common.eZeeHeading
    Friend WithEvents lnkCopyToAll As System.Windows.Forms.LinkLabel
    Friend WithEvents dgcolhDay As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhbreaktime As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhCntBreakTimeAfter As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhteatime As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhCntTeaTimeAfter As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhWeekend As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhBaseHrs As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgColhOT1 As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhOT2 As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhOT3 As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhOT4 As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhEarlycoming As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhLatecoming As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhEarlygoing As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhLateGoing As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents lnkOTPenalty As System.Windows.Forms.LinkLabel
End Class
