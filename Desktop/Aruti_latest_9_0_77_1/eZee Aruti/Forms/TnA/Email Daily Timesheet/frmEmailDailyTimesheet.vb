﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmEmailDailyTimesheet

#Region " Private Varaibles "

    Private ReadOnly mstrModuleName As String = "frmEmailDailyTimesheet"
    Private mstrAdvanceFilter As String = ""
    Private mdtEmployeeList As DataTable = Nothing
    Private dvEmployeeList As DataView = Nothing
    Private StrCheck_Fields As String = String.Empty
    Private StrCheck_Emails As String = String.Empty
    Private mdtFromDate As Date = Nothing
    Private mdtToDate As Date = Nothing
    Private dsEmailsData As New DataSet
    Private mblnCancel As Boolean = True

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef strEmailAddress As String, ByRef dtFromDate As Date, ByRef dtToDate As Date, ByRef dsetEmails As DataSet) As Boolean
        Try
            StrCheck_Emails = strEmailAddress
            mdtFromDate = dtFromDate
            mdtToDate = dtToDate
            dsetEmails = dsEmailsData

            Me.ShowDialog()

            strEmailAddress = StrCheck_Emails
            dtFromDate = mdtFromDate
            dtToDate = mdtToDate
            dsetEmails = dsEmailsData

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Event(s) "

    Private Sub frmEmailDailyTimesheet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Email
            Call FillEmployeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmailDailyTimesheet_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillEmployeeList()
        Dim objEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            dsList = objEmp.GetListForDynamicField(StrCheck_Fields, _
                                                   FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, False, _
                                                   False, "List", , , mstrAdvanceFilter, _
                                                   ConfigParameter._Object._ShowFirstAppointmentDate, _
                                                   False, True, False)
            mdtEmployeeList = dsList.Tables("List").Copy
            dvEmployeeList = mdtEmployeeList.DefaultView()
            dgvAEmployee.AutoGenerateColumns = False

            objdgcolhECheck.DataPropertyName = "ischeck"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgcolhEcode.DataPropertyName = CStr(IIf(Language.getMessage("clsEmployee_Master", 42, "Code") = "", "Code", Language.getMessage("clsEmployee_Master", 42, "Code")))
            dgcolhEmail.DataPropertyName = CStr(IIf(Language.getMessage("clsEmployee_Master", 52, "Email") = "", "Email", Language.getMessage("clsEmployee_Master", 52, "Email")))
            dgcolhEName.DataPropertyName = CStr(IIf(Language.getMessage("clsEmployee_Master", 46, "Employee Name") = "", "Employee Name", Language.getMessage("clsEmployee_Master", 46, "Employee Name")))
            dgvAEmployee.DataSource = dvEmployeeList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeList", mstrModuleName)
        Finally
            objEmp = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            mdtEmployeeList.AcceptChanges()
            If mdtEmployeeList.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please check atleast one employee in oder to send timesheet report."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetValue()
        Try
            StrCheck_Emails = String.Join(",", mdtEmployeeList.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of String)(dgcolhEmail.DataPropertyName) <> "").Select(Function(x) x.Field(Of String)(dgcolhEmail.DataPropertyName)).ToArray())
            Dim objLetterFld As New clsLetterFields
            Dim strEmpIds As String = String.Empty
            strEmpIds = String.Join(",", mdtEmployeeList.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of String)(dgcolhEmail.DataPropertyName) <> "").Select(Function(x) x.Field(Of Integer)(objdgcolhEmpId.DataPropertyName).ToString()).ToArray())
            dsEmailsData = objLetterFld.GetEmployeeData(strEmpIds, enImg_Email_RefId.Employee_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, "List")
            objLetterFld = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                               FinancialYear._Object._YearUnkid, _
                                               FinancialYear._Object._DatabaseName, _
                                               FinancialYear._Object._Database_Start_Date, _
                                               "List", True)
            With cboTnAPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                Call FillEmployeeList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = "[" & dgcolhEcode.DataPropertyName & "]" & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            "[" & dgcolhEName.DataPropertyName & "]" & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            "[" & dgcolhEmail.DataPropertyName & "]" & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            dvEmployeeList.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick, dgvAEmployee.CellContentDoubleClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dvEmployeeList.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dvEmployeeList.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
            For Each dr As DataRowView In dvEmployeeList
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvAEmployee.Refresh()
            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAdvanceFilter = "" : Call FillEmployeeList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEmailClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmailClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEmailClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEmailOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmailOk.Click
        Try
            If IsValidData() = False Then Exit Sub
            If mdtEmployeeList.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of String)(dgcolhEmail.DataPropertyName) = "").Count > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, There are some checked employee(s), who does not have email and will not be included in sending list. Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
            End If
            Call SetValue()
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEmailOk_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboTnAPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTnAPeriod.SelectedIndexChanged
        Try
            If CInt(cboTnAPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboTnAPeriod.SelectedValue)
                mdtFromDate = objPeriod._TnA_StartDate
                mdtToDate = objPeriod._TnA_EndDate
                objlblDates.Text = objPeriod._TnA_StartDate.ToShortDateString & " To " & objPeriod._TnA_EndDate.ToShortDateString
            Else
                objlblDates.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTnAPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.btnEmailClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmailClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmailOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmailOk.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnEmailClose.Text = Language._Object.getCaption(Me.btnEmailClose.Name, Me.btnEmailClose.Text)
            Me.btnEmailOk.Text = Language._Object.getCaption(Me.btnEmailOk.Name, Me.btnEmailOk.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.dgcolhEmail.HeaderText = Language._Object.getCaption(Me.dgcolhEmail.Name, Me.dgcolhEmail.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsEmployee_Master", 42, "Code")
            Language.setMessage("clsEmployee_Master", 46, "Employee Name")
            Language.setMessage("clsEmployee_Master", 52, "Email")
            Language.setMessage(mstrModuleName, 1, "Sorry, Please check atleast one employee in oder to send timesheet report.")
            Language.setMessage(mstrModuleName, 2, "Sorry, There are some checked employee(s), who does not have email and will not be included in sending list. Do you wish to continue?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class