﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 2

Public Class frmEmployeeAbsents

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployeeAbsents"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objabsent As clsemployee_absent
    Private mdtPeriodStartDate As DateTime
    Private mdtPeriodEndDate As DateTime
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetValue()
        Try
            objabsent._Periodunkid = CInt(cboPayrollPeriod.SelectedValue)
            objabsent._StartDate = mdtPeriodStartDate.Date

            'Pinkal (29-Jan-2019) -- Start
            'Bug [Support ID 0003438] - Appt. date issue in Leave GA Not Working Again.
            If mdtPeriodEndDate.Date > eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date Then
                objabsent._EndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date
            Else
            objabsent._EndDate = mdtPeriodEndDate.Date
            End If
            'Pinkal (29-Jan-2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsFill As DataSet = Nothing
        Dim dtFill As DataTable = Nothing
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsFill = objPeriod.GetList("Period", enModuleReference.Payroll, True)
            dsFill = objPeriod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True)
            'Sohail (21 Aug 2015) -- End
            dtFill = New DataView(dsFill.Tables("Period"), "statusid=1", "", DataViewRowState.CurrentRows).ToTable

            Dim drRow As DataRow = dtFill.NewRow
            drRow("periodunkid") = 0
            drRow("period_name") = "Select"
            dtFill.Rows.InsertAt(drRow, 0)

            cboPayrollPeriod.ValueMember = "periodunkid"
            cboPayrollPeriod.DisplayMember = "period_name"
            cboPayrollPeriod.DataSource = dtFill

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayYear_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeAbsents_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objabsent = New clsemployee_absent
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()

            'Pinkal (15-Sep-2015) -- Start
            'Enhancement - WORKING ON ABSET PROCESS ENHANCEMENT.
            btnUnProcessedEmployees.Enabled = False
            'Pinkal (15-Sep-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeAbsents_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeAbsents_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeAbsents_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeAbsents_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objabsent = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_absent.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_absent"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInsert.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboPayrollPeriod.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please select Period."), enMsgBoxStyle.Information)
                cboPayrollPeriod.Select()
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            'Pinkal (07-OCT-2014) -- Start
            'Enhancement - VOLTAMP CHANGES - Putting Setting For TNA IF CONSIDER LEAVE OR ATTENDANCE IF EMPLOYEE APPLIED FOR LEAVE ON SAME DAY 

            Me.SuspendLayout()
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

            SetValue()
            objbgwProcessEmpAbsent.RunWorkerAsync()
            
            'blnFlag = objabsent.InsertEmployeeAbsent(CInt(cboPayrollPeriod.SelectedValue))
            'If blnFlag = False And objabsent._Message <> "" Then
            '    Me.Cursor = Cursors.Default
            '    eZeeMsgBox.Show(objabsent._Message, enMsgBoxStyle.Information)
            'End If

            'If blnFlag Then
            '    Me.Cursor = Cursors.Default
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee Absent Process is successfully done."), enMsgBoxStyle.Information)
            'End If

            'Pinkal (07-OCT-2014) -- End

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            DisplayError.Show("-1", ex.Message, "btnInsert_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Pinkal (15-Sep-2015) -- Start
    'Enhancement - WORKING ON ABSET PROCESS ENHANCEMENT.

    Private Sub btnUnProcessedEmployees_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnProcessedEmployees.Click
        Try
            Dim dtTable As DataTable = objabsent.GetEmployeeAbsentProcessList(False, mdtPeriodStartDate.Date, mdtPeriodEndDate.Date)
            Dim objFrmEmployee As New frmUnprocessedEmployee
            If objFrmEmployee.displayDialog(CInt(cboPayrollPeriod.SelectedValue), dtTable, cboPayrollPeriod.Text, mdtPeriodStartDate, mdtPeriodEndDate) Then
                cboPayrollPeriod_SelectedIndexChanged(New Object(), New EventArgs())
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnUnProcessedEmployees_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (15-Sep-2015) -- End

#End Region

#Region "Dropdown's Event"

    Private Sub cboPayrollPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPayrollPeriod.SelectedIndexChanged
        Try
            If CInt(cboPayrollPeriod.SelectedValue) <= 0 Then
                objlblPeriod.Text = ""
                'Pinkal (15-Sep-2015) -- Start
                'Enhancement - WORKING ON ABSET PROCESS ENHANCEMENT.
                btnUnProcessedEmployees.Enabled = False
                'Pinkal (15-Sep-2015) -- End
                Exit Sub
            End If

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPayrollPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayrollPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End


            'Pinkal (03-Jan-2014) -- Start
            'Enhancement : Oman Changes

            'mdtPeriodStartDate = objPeriod._Start_Date
            'mdtPeriodEndDate = objPeriod._End_Date

            'objlblPeriod.Text = objPeriod._Start_Date.ToShortDateString & " To " & objPeriod._End_Date.ToShortDateString

            mdtPeriodStartDate = objPeriod._TnA_StartDate
            mdtPeriodEndDate = objPeriod._TnA_EndDate

            objlblPeriod.Text = objPeriod._TnA_StartDate.ToShortDateString & " To " & objPeriod._TnA_EndDate.ToShortDateString

            'Pinkal (03-Jan-2014) -- End

            'Pinkal (15-Sep-2015) -- Start
            'Enhancement - WORKING ON ABSET PROCESS ENHANCEMENT.
            Dim dtTable As DataTable = objabsent.GetEmployeeAbsentProcessList(False, objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date)
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                btnUnProcessedEmployees.Enabled = True
            Else
                btnUnProcessedEmployees.Enabled = False
            End If
            'Pinkal (15-Sep-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayrollPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    'Pinkal (07-OCT-2014) -- Start
    'Enhancement - VOLTAMP CHANGES - Putting Setting For TNA IF CONSIDER LEAVE OR ATTENDANCE IF EMPLOYEE APPLIED FOR LEAVE ON SAME DAY

#Region "BackGroundWorker Events"

    Private Sub objbgwProcessEmpAbsent_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgwProcessEmpAbsent.DoWork
        Try
            Me.ControlBox = False
            Me.Enabled = False


            'Pinkal (11-AUG-2017) -- Start
            'Enhancement - Working On B5 Plus Company TnA Enhancements.

            'objabsent.InsertEmployeeAbsent(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                               ConfigParameter._Object._ShowFirstAppointmentDate, _
            '                               ConfigParameter._Object._PolicyManagementTNA, _
            '                               CInt(cboPayrollPeriod.SelectedValue), ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._FirstCheckInLastCheckOut, False, -1, "", "", objbgwProcessEmpAbsent)



            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objabsent._FormName = mstrModuleName
            objabsent._LoginEmployeeUnkid = 0
            objabsent._ClientIP = getIP()
            objabsent._HostName = getHostName()
            objabsent._FromWeb = False
            objabsent._AuditUserId = User._Object._Userunkid
objabsent._CompanyUnkid = Company._Object._Companyunkid
            objabsent._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            objabsent.InsertEmployeeAbsent(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                           ConfigParameter._Object._ShowFirstAppointmentDate, _
                                           ConfigParameter._Object._PolicyManagementTNA, _
                                                             CInt(cboPayrollPeriod.SelectedValue), ConfigParameter._Object._DonotAttendanceinSeconds, _
                                                             ConfigParameter._Object._FirstCheckInLastCheckOut, ConfigParameter._Object._IsHolidayConsiderOnWeekend, _
                                                             ConfigParameter._Object._IsDayOffConsiderOnWeekend, ConfigParameter._Object._IsHolidayConsiderOnDayoff, _
                                                             False, -1, "", "", objbgwProcessEmpAbsent)

            'Pinkal (11-AUG-2017) -- End

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "objbgwProcessEmpAbsent_DoWork", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgwProcessEmpAbsent_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgwProcessEmpAbsent.ProgressChanged
        Try
            objlblProgress.Text = "[ " & e.ProgressPercentage.ToString & " / " & objabsent._TotalEmployeeCount & " ]"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwProcessEmpAbsent_ProgressChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgwProcessEmpAbsent_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgwProcessEmpAbsent.RunWorkerCompleted
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee Absent Process is successfully done."), enMsgBoxStyle.Information)
            End If
            Me.ControlBox = True
            Me.Enabled = True
            'Pinkal (15-Sep-2015) -- Start
            'Enhancement - WORKING ON ABSET PROCESS ENHANCEMENT.
            objlblProgress.Text = ""
            cboPayrollPeriod.SelectedValue = 0
            'Pinkal (15-Sep-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwProcessEmpAbsent_RunWorkerCompleted", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

    'Pinkal (07-OCT-2014) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbEmployeeAbsent.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmployeeAbsent.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnInsert.GradientBackColor = GUI._ButttonBackColor 
			Me.btnInsert.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnInsert.Text = Language._Object.getCaption(Me.btnInsert.Name, Me.btnInsert.Text)
			Me.gbEmployeeAbsent.Text = Language._Object.getCaption(Me.gbEmployeeAbsent.Name, Me.gbEmployeeAbsent.Text)
			Me.lblPayrollPeriod.Text = Language._Object.getCaption(Me.lblPayrollPeriod.Name, Me.lblPayrollPeriod.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is compulsory information.Please select Period.")
			Language.setMessage(mstrModuleName, 2, "Employee Absent Process is successfully done.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region

    'Language & UI Settings
	'</Language>

  

End Class

