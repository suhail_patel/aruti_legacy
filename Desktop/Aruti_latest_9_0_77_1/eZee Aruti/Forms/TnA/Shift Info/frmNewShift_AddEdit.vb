﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization


Public Class frmNewShift_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmNewShift_AddEdit"
    Private mblnCancel As Boolean = True
    Private objShiftMaster As New clsNewshift_master
    Private objShifttran As New clsshift_tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintShiftMasterUnkid As Integer = -1
    Dim wkMins As Double
    Dim mdtDays As DataTable = Nothing
    Dim Separator As String = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator
    Dim mintTotalmins As Double
    Dim mblnError As Boolean = False
    Dim mstrErrorMsg As String = ""


    'Pinkal (18-Jul-2017) -- Start
    'Enhancement - TnA Enhancement for B5 Plus Company .
    Private mintTotalWeekHrs As Integer = 0
    'Pinkal (18-Jul-2017) -- End


#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintShiftMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintShiftMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmShift_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objShiftMaster = New clsNewshift_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            setColor()
            FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objShiftMaster._Shiftunkid = mintShiftMasterUnkid
                objShifttran.GetShiftTran(objShiftMaster._Shiftunkid)
                mdtDays = objShifttran._dtShiftday
                GetValue()

                'Pinkal (12-Oct-2017) -- Start
                'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
            Else
                rdCountOTOnWorkedHrs.Checked = True
            End If
            'Pinkal (12-Oct-2017) --End


            GenerateShiftDays()
            txtCode.Focus()



            'Pinkal (01-Apr-2020) -- Start
            'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
            If ArtLic._Object.HotelName.Trim().ToUpper() <> "NMB PLC" Then
                dgShiftDay.Columns(dgcolhOTHoliday.Index).Visible = False
            End If
            'Pinkal (01-Apr-2020) -- End


            If ConfigParameter._Object._PolicyManagementTNA Then
                dgShiftDay.Columns(dgcolhOverTimeAfter.Index).Visible = False
                dgShiftDay.Columns(dgcolhShortimeBefore.Index).Visible = False
                dgShiftDay.Columns(dgcolhHalffrmhrs.Index).Visible = False
                dgShiftDay.Columns(dgcolhhalftohrs.Index).Visible = False
                dgShiftDay.Columns(dgcolhnightfrmhrs.Index).Visible = False
                dgShiftDay.Columns(dgcolhnighttohrs.Index).Visible = False
                dgShiftDay.Columns(dgcolhBreaktime.Index).Visible = False

                'Pinkal (03-Aug-2018) -- Start
                'Enhancement - Changes For B5 [Ref #289]
                dgShiftDay.Columns(dgcolhOTGraceAfterShiftEndTime.Index).Visible = False
                'Pinkal (03-Aug-2018) -- End

            End If

            'Pinkal (30-Dec-2014) -- Start
            'Enhancement - CHANGES IN DAILY TIMESHEET REPORT FOR OCEAN LINK
            'dgShiftDay.Columns(dgcolhDayStartTime.Index).Visible = ConfigParameter._Object._FirstCheckInLastCheckOut
            'Pinkal (30-Dec-2014) -- End
            'Pinkal (20-Dec-2013) -- End


            'Pinkal (03-AUG-2015) -- Start
            'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.
            chkIsOpenShift_CheckedChanged(sender, e)
            'Pinkal (03-AUG-2015) -- End


            'Pinkal (12-Oct-2017) -- Start
            'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
            If ConfigParameter._Object._PolicyManagementTNA Then
                rdCountOTOnWorkedHrs.Checked = True
                rdCountOTOnWorkedHrs.Enabled = False
                rdCountOTAfterShiftEndTime.Enabled = False
            End If
            'Pinkal (12-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmShift_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmShift_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmShift_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmShift_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                'btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmShift_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmShift_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objShiftMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If



            'Pinkal (03-Jul-2013) -- Start
            'Enhancement : TRA Changes

            'clsshift_master.SetMessages()
            'objfrm._Other_ModuleNames = "clsshift_master"

            clsNewshift_master.SetMessages()
            objfrm._Other_ModuleNames = "clsNewshift_master"

            'Pinkal (03-Jul-2013) -- End


            objfrm.displayDialog(Me)


        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods"

    Private Sub setColor()
        Try
            cboShiftType.BackColor = GUI.ColorComp
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objCommonMaster As New clsCommon_Master
            Dim dsShifttype As DataSet = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "Shift Type")
            cboShiftType.ValueMember = "masterunkid"
            cboShiftType.DisplayMember = "name"
            cboShiftType.DataSource = dsShifttype.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objShiftMaster._Shiftcode
            txtName.Text = objShiftMaster._Shiftname
            cboShiftType.SelectedValue = objShiftMaster._Shifttypeunkid

            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .
            'Dim calcMinute As Double = objShiftMaster._TotalWeekHrs / 60
            'Dim calMinute As Double = calcMinute Mod 60
            'Dim calHour As Double = calcMinute / 60
            'TxtTotalWeekHours.Tag = objShiftMaster._TotalWeekHrs
            'TxtTotalWeekHours.Text = CStr(CDec(Int(calHour) + (calMinute / 100)))
            'Pinkal (18-Jul-2017) -- End

            chkIsOpenShift.Checked = CBool(objShiftMaster._IsOpenShift)
            If menAction = enAction.EDIT_ONE Then
                chkIsOpenShift.Enabled = False
                ChkSetAttOnDeviceStatus.Enabled = False
            End If

            nudMaxhours.Value = CInt(objShiftMaster._MaxHrs / 60)
            ChkSetAttOnDeviceStatus.Checked = objShiftMaster._AttOnDeviceInOutStatus

            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .
            chkNormalHrsWeekend.Checked = objShiftMaster._IsNormalHrsForWeekend
            chkNormalHrsHoliday.Checked = objShiftMaster._IsNormalHrsForHoliday
            chkNormalHrsDayOff.Checked = objShiftMaster._IsNormalHrsForDayOff
            'Pinkal (18-Jul-2017) -- End


            'Pinkal (07-Sep-2017) -- Start
            'Enhancement - Working on B5 Plus Enhancement.
            chkCountShiftTimingOnly.Checked = objShiftMaster._CountOnlyShiftTime
            'Pinkal (07-Sep-2017) -- End


            'Pinkal (12-Oct-2017) -- Start
            'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
            rdCountOTOnWorkedHrs.Checked = objShiftMaster._CountOTonTotalWorkedHrs
            rdCountOTAfterShiftEndTime.Checked = Not objShiftMaster._CountOTonTotalWorkedHrs
            'Pinkal (12-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Dim inthrsec As Integer = 0
        Dim intminsec As Integer = 0
        Try
            objShiftMaster._Shiftcode = txtCode.Text
            objShiftMaster._Shiftname = txtName.Text
            objShiftMaster._Shifttypeunkid = CInt(cboShiftType.SelectedValue)

            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .
            'objShiftMaster._TotalWeekHrs = CInt(TxtTotalWeekHours.Tag)
            objShiftMaster._TotalWeekHrs = mintTotalWeekHrs
            'Pinkal (18-Jul-2017) -- End

            objShiftMaster._IsOpenShift = chkIsOpenShift.Checked
            objShiftMaster._MaxHrs = CInt(nudMaxhours.Value) * 60
            objShiftMaster._AttOnDeviceInOutStatus = ChkSetAttOnDeviceStatus.Checked

            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .
            objShiftMaster._IsNormalHrsForWeekend = chkNormalHrsWeekend.Checked
            objShiftMaster._IsNormalHrsForHoliday = chkNormalHrsHoliday.Checked
            objShiftMaster._IsNormalHrsForDayOff = chkNormalHrsDayOff.Checked
            'Pinkal (18-Jul-2017) -- End


            'Pinkal (07-Sep-2017) -- Start
            'Enhancement - Working on B5 Plus Enhancement.
            objShiftMaster._CountOnlyShiftTime = chkCountShiftTimingOnly.Checked
            'Pinkal (07-Sep-2017) -- End


            'Pinkal (12-Oct-2017) -- Start
            'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
            objShiftMaster._CountOTonTotalWorkedHrs = rdCountOTOnWorkedHrs.Checked
            'Pinkal (12-Oct-2017) -- End


            'Pinkal (03-Aug-2018) -- Start
            'Enhancement - Changes For B5 [Ref #289]
            If rdCountOTOnWorkedHrs.Checked Then
                If mdtDays IsNot Nothing AndAlso mdtDays.Rows.Count > 0 Then
                    Dim drRow() As DataRow = mdtDays.Select("countotmins_aftersftendtime > 0")
                    If drRow.Length > 0 Then
                        For Each dr As DataRow In drRow
                            dr("countotmins_aftersftendtime") = 0
                            dr("countotmins_aftersftendtimeinsec") = 0
                            dr("AUD") = "U"
                            dr.AcceptChanges()
                        Next
                    End If
                End If
            End If
            'Pinkal (03-Aug-2018) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GenerateShiftDays()
        Try

            Dim days() As String = CultureInfo.CurrentCulture.DateTimeFormat.DayNames

            If mdtDays Is Nothing Then
                mdtDays = New DataTable("ShiftDays")
                mdtDays.Columns.Add("shifttranunkid", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("shiftunkid", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("dayId", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("days", Type.GetType("System.String"))
                mdtDays.Columns.Add("isweekend", Type.GetType("System.Boolean"))
                mdtDays.Columns.Add("starttime", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("endtime", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("breaktime", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("workinghrs", Type.GetType("System.Decimal"))
                mdtDays.Columns.Add("calcovertimeafter", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("calcshorttimebefore", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("halffromhrs", Type.GetType("System.Decimal"))
                mdtDays.Columns.Add("halftohrs", Type.GetType("System.Decimal"))
                mdtDays.Columns.Add("nightfromhrs", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("nighttohrs", Type.GetType("System.DateTime"))
                mdtDays.Columns.Add("breaktimeinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("workinghrsinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("calcovertimeafterinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("calcshorttimebeforeinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("halffromhrsinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("halftohrsinsec", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("AUD", Type.GetType("System.String"))
                mdtDays.Columns.Add("daystart_time", Type.GetType("System.DateTime"))


                'Pinkal (03-Aug-2018) -- Start
                'Enhancement - Changes For B5 [Ref #289]
                mdtDays.Columns.Add("countotmins_aftersftendtime", Type.GetType("System.Int32"))
                mdtDays.Columns.Add("countotmins_aftersftendtimeinsec", Type.GetType("System.Int32"))
                'Pinkal (03-Aug-2018) -- End


                'Pinkal (01-Apr-2020) -- Start
                'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
                mdtDays.Columns.Add("isotholiday", Type.GetType("System.Boolean"))
                'Pinkal (01-Apr-2020) -- End



GenerateDays:
                For i As Integer = 0 To 6
                    Dim dr As DataRow = mdtDays.NewRow
                    dr("shifttranunkid") = -1
                    dr("shiftunkid") = mintShiftMasterUnkid
                    dr("dayId") = i
                    dr("Days") = days(i).ToString()
                    dr("isweekend") = False
                    If chkIsOpenShift.Checked Then
                        dr("starttime") = DBNull.Value
                        dr("endtime") = DBNull.Value
                    Else
                        dr("starttime") = ConfigParameter._Object._CurrentDateAndTime.Date.ToShortDateString & " " & "08:00 AM"   'AS PER MR.RUTTA'S COMMENT
                        dr("endtime") = ConfigParameter._Object._CurrentDateAndTime.Date.ToShortDateString & " " & "05:00 PM"    'AS PER MR.RUTTA'S COMMENT
                    End If
                    dr("breaktime") = 0
                    If chkIsOpenShift.Checked = False Then
                        dr("workinghrs") = CalculateTime(True, CInt(DateDiff(DateInterval.Second, CDate(dr("starttime")), CDate(dr("endtime")))))
                    End If
                    dr("calcovertimeafter") = 0
                    dr("calcshorttimebefore") = 0
                    dr("halffromhrs") = 0.0
                    dr("halftohrs") = 0.0
                    dr("nightfromhrs") = ConfigParameter._Object._CurrentDateAndTime.Date
                    dr("nighttohrs") = ConfigParameter._Object._CurrentDateAndTime.Date
                    dr("breaktimeinsec") = 0
                    If chkIsOpenShift.Checked = False Then
                        dr("workinghrsinsec") = DateDiff(DateInterval.Second, CDate(dr("starttime")), CDate(dr("endtime")))
                    End If
                    dr("calcovertimeafterinsec") = 0
                    dr("calcshorttimebeforeinsec") = 0
                    dr("halffromhrsinsec") = 0.0
                    dr("halftohrsinsec") = 0.0

                    If chkIsOpenShift.Checked = False Then
                        dr("daystart_time") = CDate(dr("starttime")).AddHours(-5)
                    End If


                    'Pinkal (03-Aug-2018) -- Start
                    'Enhancement - Changes For B5 [Ref #289]
                    dr("countotmins_aftersftendtime") = 0
                    dr("countotmins_aftersftendtimeinsec") = 0
                    'Pinkal (03-Aug-2018) -- End


                    'Pinkal (01-Apr-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
                    dr("isotholiday") = False
                    'Pinkal (01-Apr-2020) -- End


                    dr("AUD") = "A"
                    mdtDays.Rows.Add(dr)
                Next

            Else

                If mdtDays.Rows.Count > 0 Then

                    'Pinkal (18-Jul-2017) -- Start
                    'Enhancement - TnA Enhancement for B5 Plus Company .
                    'For i As Integer = 0 To mdtDays.Rows.Count - 1
                    For i As Integer = 0 To 6
                        'Pinkal (18-Jul-2017) -- End
                        mdtDays.Rows(i)("Days") = days(i).ToString()

                        If chkIsOpenShift.Checked = False Then
                            mdtDays.Rows(i)("starttime") = CDate(mdtDays.Rows(i)("starttime"))
                            mdtDays.Rows(i)("endtime") = CDate(mdtDays.Rows(i)("endtime"))
                            mdtDays.Rows(i)("daystart_time") = CDate(mdtDays.Rows(i)("daystart_time"))
                            Dim mdtStartTime As DateTime = Nothing
                            Dim mdtEndTime As DateTime = Nothing
                            Dim mdtDayStartTime As DateTime = Nothing

                            mdtStartTime = CDate(mdtDays.Rows(i)("starttime"))
                            mdtEndTime = CDate(mdtDays.Rows(i)("endtime"))
                            mdtDayStartTime = CDate(mdtDays.Rows(i)("daystart_time"))

                            If CInt(mdtDays.Rows(i)("workinghrsinsec")) <> CInt(DateDiff(DateInterval.Second, mdtStartTime, mdtEndTime)) Then
                                mdtDays.Rows(i)("workinghrsinsec") = DateDiff(DateInterval.Second, mdtStartTime, mdtEndTime) - CInt(mdtDays.Rows(i)("breaktimeinsec"))
                                mdtDays.Rows(i)("workinghrs") = CalculateTime(True, CInt(mdtDays.Rows(i)("workinghrsinsec")))
                                mdtDays.Rows(i)("AUD") = "U"
                            ElseIf CDate(mdtDays.Rows(i)("daystart_time")) <> mdtDayStartTime Then
                                mdtDays.Rows(i)("AUD") = "U"
                            End If
                        Else
                            mdtDays.Rows(i)("starttime") = DBNull.Value
                            mdtDays.Rows(i)("endtime") = DBNull.Value
                            mdtDays.Rows(i)("daystart_time") = DBNull.Value
                        End If

                        mdtDays.Rows(i)("nightfromhrs") = CDate(mdtDays.Rows(i)("nightfromhrs")).ToShortTimeString
                        mdtDays.Rows(i)("nighttohrs") = CDate(mdtDays.Rows(i)("nighttohrs")).ToShortTimeString
                        mdtDays.Rows(i)("halffromhrs") = CalculateTime(True, CInt(mdtDays.Rows(i)("halffromhrsinsec")))
                        mdtDays.Rows(i)("halftohrs") = CalculateTime(True, CInt(mdtDays.Rows(i)("halftohrsinsec")))
                    Next

                Else
                    GoTo GenerateDays
                End If

            End If

            dgShiftDay.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None
            dgShiftDay.AutoGenerateColumns = False
            dgcolhDay.DataPropertyName = "days"
            dgcolhWeekend.DataPropertyName = "isweekend"
            dgcolhStarttime.DataPropertyName = "starttime"
            dgcolhStarttime.CellTemplate.Style.Format = "hh:mm tt"

            dgcolhEndtime.DataPropertyName = "endtime"
            dgcolhEndtime.CellTemplate.Style.Format = "hh:mm tt"

            dgcolhBreaktime.DataPropertyName = "breaktime"
            dgcolhWorkinghrs.DataPropertyName = "workinghrs"
            dgcolhOverTimeAfter.DataPropertyName = "calcovertimeafter"
            dgcolhShortimeBefore.DataPropertyName = "calcshorttimebefore"

            dgcolhHalffrmhrs.DataPropertyName = "halffromhrs"
            dgcolhhalftohrs.DataPropertyName = "halftohrs"

            dgcolhnightfrmhrs.DataPropertyName = "nightfromhrs"
            dgcolhnightfrmhrs.CellTemplate.Style.Format = "hh:mm tt"

            dgcolhnighttohrs.DataPropertyName = "nighttohrs"
            dgcolhnighttohrs.CellTemplate.Style.Format = "hh:mm tt"
            dgcolhDayStartTime.DataPropertyName = "daystart_time"
            dgcolhDayStartTime.CellTemplate.Style.Format = "hh:mm tt"


            'Pinkal (03-Aug-2018) -- Start
            'Enhancement - Changes For B5 [Ref #289]
            dgcolhOTGraceAfterShiftEndTime.DataPropertyName = "countotmins_aftersftendtime"
            'Pinkal (03-Aug-2018) -- End


            'Pinkal (01-Apr-2020) -- Start
            'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
            dgcolhOTHoliday.DataPropertyName = "isotholiday"
            'Pinkal (01-Apr-2020) -- End



            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .
            GetTotalWeekHours()
            'Pinkal (18-Jul-2017) -- End

            dgShiftDay.DataSource = mdtDays

            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .
            SetLastRowFormatting()
            'Pinkal (18-Jul-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenerateShiftDays", mstrModuleName)
        End Try
    End Sub

    Private Sub GetTotalWeekHours()
        Try

            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .

            'Dim dtTable As DataTable = CType(dgShiftDay.DataSource, DataTable)
            'Dim mdblHours As Long = 0
            'For i As Integer = 0 To dtTable.Rows.Count - 1

            '    'Pinkal (09-Jun-2015) -- Start
            '    'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
            '    If chkIsOpenShift.Checked Then
            '        If CBool(dtTable.Rows(i)("isweekend")) = False Then
            '            mdblHours += CLng(CDec(dtTable.Rows(i)("workinghrsinsec")) / 60)
            '        End If
            '    Else
            '        If CBool(dtTable.Rows(i)("isweekend")) = False AndAlso CDate(dtTable.Rows(i)("starttime")) < CDate(dtTable.Rows(i)("endtime")) Then
            '            mdblHours += DateDiff(DateInterval.Minute, CDate(dtTable.Rows(i)("starttime")), CDate(dtTable.Rows(i)("endtime")))
            '            mdblHours -= CInt(dtTable.Rows(i)("breaktime"))
            '        End If
            '    End If
            '    'Pinkal (09-Jun-2015) -- End

            'Next

            'Dim calMinute As Double = mdblHours Mod 60
            'Dim calHour As Double = mdblHours / 60

            'TxtTotalWeekHours.Text = CStr(CDec(Int(calHour) + (calMinute / 100)))
            'TxtTotalWeekHours.Tag = mdblHours * 60

            If mdtDays IsNot Nothing AndAlso mdtDays.Rows.Count > 0 Then

                Dim mdblHours As Long = 0
                For i As Integer = 0 To 6
                    If chkIsOpenShift.Checked Then
                        If CBool(mdtDays.Rows(i)("isweekend")) = False Then
                            mdblHours += CLng(CDec(mdtDays.Rows(i)("workinghrsinsec")) / 60)
                        End If
                    Else
                        If CBool(mdtDays.Rows(i)("isweekend")) = False AndAlso CDate(mdtDays.Rows(i)("starttime")) < CDate(mdtDays.Rows(i)("endtime")) Then
                            mdblHours += DateDiff(DateInterval.Minute, CDate(mdtDays.Rows(i)("starttime")), CDate(mdtDays.Rows(i)("endtime")))
                            mdblHours -= CInt(mdtDays.Rows(i)("breaktime"))
                        End If
                    End If
                Next
                Dim calMinute As Double = mdblHours Mod 60
                Dim calHour As Double = mdblHours / 60

                Dim dRow() As DataRow = mdtDays.Select("shifttranunkid = -999")
                If dRow.Length <= 0 Then
                    Dim drRow As DataRow = mdtDays.NewRow()
                    drRow("shifttranunkid") = -999
                    drRow("shiftunkid") = mintShiftMasterUnkid
                    drRow("isweekend") = False
                    drRow("Days") = Language.getMessage(mstrModuleName, 13, "Total Week Hours (Excluding Weekend Hrs)")
                    drRow("workinghrs") = CStr(CDec(Int(calHour) + (calMinute / 100)))
                    'Hemant (07 June 2019) -- Start
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    drRow("AUD") = ""
                    'Hemant (07 June 2019) -- End


                    'Pinkal (01-Apr-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
                    drRow("isotholiday") = False
                    'Pinkal (01-Apr-2020) -- End

                    mdtDays.Rows.InsertAt(drRow, mdtDays.Rows.Count)
                Else
                    mdtDays.Rows(mdtDays.Rows.Count - 1)("workinghrs") = CStr(CDec(Int(calHour) + (calMinute / 100)))
                    RemoveHandler dgShiftDay.CellValueChanged, AddressOf dgShiftDay_CellValueChanged
                    SetLastRowFormatting()
                    AddHandler dgShiftDay.CellValueChanged, AddressOf dgShiftDay_CellValueChanged
                End If

                mintTotalWeekHrs = CInt(mdblHours) * 60
            End If

            'Pinkal (18-Jul-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetTotalWeekHours", mstrModuleName)
        End Try
    End Sub


    'Pinkal (18-Jul-2017) -- Start
    'Enhancement - TnA Enhancement for B5 Plus Company .

    Private Sub SetLastRowFormatting()
        Try
            Dim drRow() As DataRow = mdtDays.Select("shifttranunkid = -999")
            If drRow.Length > 0 Then
                dgShiftDay.Rows(mdtDays.Rows.Count - 1).ReadOnly = True
                dgShiftDay.Rows(mdtDays.Rows.Count - 1).DefaultCellStyle.ForeColor = Color.Blue
                dgShiftDay.Rows(mdtDays.Rows.Count - 1).DefaultCellStyle.SelectionBackColor = Color.Transparent
                dgShiftDay.Rows(mdtDays.Rows.Count - 1).DefaultCellStyle.SelectionForeColor = Color.Transparent
                dgShiftDay.Rows(mdtDays.Rows.Count - 1).DefaultCellStyle.Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Bold)
                dgShiftDay.Rows(mdtDays.Rows.Count - 1).Cells(dgcolhWeekend.Index).Value = DBNull.Value
                dgShiftDay.Rows(mdtDays.Rows.Count - 1).Cells(dgcolhWeekend.Index) = New DataGridViewTextBoxCell()

                'Pinkal (01-Apr-2020) -- Start
                'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
                dgShiftDay.Rows(mdtDays.Rows.Count - 1).Cells(dgcolhOTHoliday.Index).Value = DBNull.Value
                dgShiftDay.Rows(mdtDays.Rows.Count - 1).Cells(dgcolhOTHoliday.Index) = New DataGridViewTextBoxCell()
                'Pinkal (01-Apr-2020) -- End

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLastRowFormatting", mstrModuleName)
        End Try
    End Sub

    'Pinkal (18-Jul-2017) -- End


#End Region

#Region " Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Shift Code cannot be blank. Shift Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf CInt(cboShiftType.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Shift Type is compulsory information.Please Select Shift Type."), enMsgBoxStyle.Information)
                cboShiftType.Focus()
                Exit Sub
            ElseIf Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Shift Name cannot be blank. Shift Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub


                'Pinkal (18-Jul-2017) -- Start
                'Enhancement - TnA Enhancement for B5 Plus Company .

                'ElseIf CDec(TxtTotalWeekHours.Text) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Shift Hour is compulsory information.Please define Shift hour."), enMsgBoxStyle.Information)
                '    txtName.Focus()
                '    Exit Sub


                'Pinkal (04-Dec-2019) -- Start
                'Enhancement NMB - Working On Close Year Enhancement for NMB.
                '[Annor set day off shift for 2 clients (Papaye & Natural) in which he configured all shift day as weekend but system didn't allow him to save due to 0 working hours for week]

                'ElseIf mintTotalWeekHrs <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Shift Hour is compulsory information.Please define Shift hour."), enMsgBoxStyle.Information)
                '    txtName.Focus()
                '    Exit Sub

                'Pinkal (04-Dec-2019) -- End

                'Pinkal (18-Jul-2017) -- End

            End If

            If mblnError Then
                eZeeMsgBox.Show(mstrErrorMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objShiftMaster._FormName = mstrModuleName
            objShiftMaster._LoginEmployeeunkid = 0
            objShiftMaster._ClientIP = getIP()
            objShiftMaster._HostName = getHostName()
            objShiftMaster._FromWeb = False
            objShiftMaster._AuditUserId = User._Object._Userunkid
objShiftMaster._CompanyUnkid = Company._Object._Companyunkid
            objShiftMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objShiftMaster.Update(mdtDays)
            Else
                blnFlag = objShiftMaster.Insert(mdtDays)
            End If

            If blnFlag = False And objShiftMaster._Message <> "" Then
                eZeeMsgBox.Show(objShiftMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objShiftMaster = Nothing
                    objShiftMaster = New clsNewshift_master
                    objShifttran = New clsshift_tran
                    mdtDays = Nothing
                    Call GetValue()
                    GenerateShiftDays()
                    txtCode.Focus()
                Else
                    mintShiftMasterUnkid = objShiftMaster._Shiftunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            Call objFrm.displayDialog(txtName.Text, objShiftMaster._Shiftname1, objShiftMaster._Shiftname2)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddShiftType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddShiftType.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.SHIFT_TYPE, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "Shift")
                With cboShiftType
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Shift")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "DataGrid Event"

    Private Sub dgShiftDay_CellValidated(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgShiftDay.CellValidated
        Try
            Dim inthrsec As Integer = 0
            Dim intminsec As Integer = 0

            If dgShiftDay.CurrentCell Is Nothing Then Exit Sub


            'Pinkal (11-AUG-2017) -- Start
            'Enhancement - Working On B5 Plus Company TnA Enhancements.
            If IsDBNull(dgShiftDay.CurrentCell.Value) Then Exit Sub
            'Pinkal (11-AUG-2017) -- End


            'Pinkal (09-Jun-2015) -- Start
            'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.

            If dgShiftDay.CurrentCell.ColumnIndex = dgcolhHalffrmhrs.Index Or dgShiftDay.CurrentCell.ColumnIndex = dgcolhhalftohrs.Index Or dgShiftDay.CurrentCell.ColumnIndex = dgcolhWorkinghrs.Index Then

                'Pinkal (09-Jun-2015) -- End

                If dgShiftDay.CurrentCell.Value.ToString() = "" Then
                    dgShiftDay.CurrentCell.Value = "0"

                ElseIf dgShiftDay.CurrentCell.Value.ToString().Trim.Contains(".") Then

                    If dgShiftDay.CurrentCell.Value.ToString().Trim.Contains(".60") Then
calFromHr:              dgShiftDay.CurrentCell.Value = CDec(CDec(dgShiftDay.CurrentCell.Value.ToString().Trim) + 0.4).ToString("#0.00")

                    ElseIf dgShiftDay.CurrentCell.Value.ToString() <> "" And dgShiftDay.CurrentCell.Value.ToString() <> "." Then

                        If dgShiftDay.CurrentCell.Value.ToString.Trim.IndexOf(".") = 0 And dgShiftDay.CurrentCell.Value.ToString().Trim.Contains(".60") Then
                            GoTo calFromHr
                        ElseIf dgShiftDay.CurrentCell.Value.ToString().Substring(dgShiftDay.CurrentCell.Value.ToString().Trim.IndexOf("."), dgShiftDay.CurrentCell.Value.ToString().Trim.Length - dgShiftDay.CurrentCell.Value.ToString().Trim.IndexOf(".")) = "." Then
                            dgShiftDay.CurrentCell.Value = dgShiftDay.CurrentCell.Value.ToString() & "00"
                        ElseIf CDec(dgShiftDay.CurrentCell.Value.ToString().Substring(dgShiftDay.CurrentCell.Value.ToString().Trim.IndexOf("."), dgShiftDay.CurrentCell.Value.ToString().Trim.Length - dgShiftDay.CurrentCell.Value.ToString().Trim.IndexOf("."))) > 0.6 Then
                            GoTo calFromHr
                        Else
                            dgShiftDay.CurrentCell.Value = CDec(dgShiftDay.CurrentCell.Value.ToString()).ToString("#0.00")
                        End If

                    End If

                End If

                inthrsec = CInt(CDec(dgShiftDay.CurrentCell.Value).ToString("#0.00").Substring(0, CDec(dgShiftDay.CurrentCell.Value).ToString("#0.00").IndexOf(Separator))) * 3600
                intminsec = CInt(CDec(dgShiftDay.CurrentCell.Value).ToString("#0.00").Substring(CDec(dgShiftDay.CurrentCell.Value).ToString("#0.00").IndexOf(Separator) + 1, CDec(dgShiftDay.CurrentCell.Value).ToString("#0.00").Length - CDec(dgShiftDay.CurrentCell.Value).ToString("#0.00").IndexOf(Separator) - 1)) * 60

                If dgShiftDay.CurrentCell.ColumnIndex = dgcolhHalffrmhrs.Index Then
                    mdtDays.Rows(e.RowIndex)("halffromhrsinsec") = inthrsec + intminsec

                ElseIf dgShiftDay.CurrentCell.ColumnIndex = dgcolhhalftohrs.Index Then
                    mdtDays.Rows(e.RowIndex)("halftohrsinsec") = inthrsec + intminsec

                    'Pinkal (09-Jun-2015) -- Start
                    'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
                ElseIf chkIsOpenShift.Checked AndAlso dgShiftDay.CurrentCell.ColumnIndex = dgcolhWorkinghrs.Index Then
                    mdtDays.Rows(e.RowIndex)("workinghrsinsec") = inthrsec + intminsec
                    'Pinkal (09-Jun-2015) -- End


                End If

                If mintShiftMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("shifttranunkid")) > 0 Then
                    mdtDays.Rows(e.RowIndex)("AUD") = "U"
                Else
                    mdtDays.Rows(e.RowIndex)("AUD") = "A"
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgShiftDay_CellValidated", mstrModuleName)
        End Try
    End Sub

    Private Sub dgShiftDay_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgShiftDay.CellValueChanged
        Try

            If dgShiftDay.CurrentCell Is Nothing Then Exit Sub


            'Pinkal (11-AUG-2017) -- Start
            'Enhancement - Working On B5 Plus Company TnA Enhancements.
            If IsDBNull(dgShiftDay.CurrentCell.Value) Then Exit Sub
            'Pinkal (11-AUG-2017) -- End

            If dgShiftDay.CurrentCell.ColumnIndex = dgcolhStarttime.Index Or dgShiftDay.CurrentCell.ColumnIndex = dgcolhEndtime.Index Or dgShiftDay.CurrentCell.ColumnIndex = dgcolhBreaktime.Index Then


                'Pinkal (09-Jun-2015) -- Start
                'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.

                If (IsDBNull(dgShiftDay.CurrentRow.Cells(dgcolhStarttime.Index).Value) OrElse CDate(dgShiftDay.CurrentRow.Cells(dgcolhStarttime.Index).Value) = Nothing) OrElse (IsDBNull(dgShiftDay.CurrentRow.Cells(dgcolhEndtime.Index).Value) OrElse CDate(dgShiftDay.CurrentRow.Cells(dgcolhEndtime.Index).Value) = Nothing) Then Exit Sub

                'Pinkal (09-Jun-2015) -- End

                Dim mdtStartTime As DateTime = CDate(ConfigParameter._Object._CurrentDateAndTime.Date & " " & CDate(dgShiftDay.CurrentRow.Cells(dgcolhStarttime.Index).Value).ToString("hh:mm tt"))
                Dim mdtEndTime As DateTime = CDate(ConfigParameter._Object._CurrentDateAndTime.Date & " " & CDate(dgShiftDay.CurrentRow.Cells(dgcolhEndtime.Index).Value).ToString("hh:mm tt"))

                If mdtEndTime > mdtStartTime Then
                    wkMins = DateDiff(DateInterval.Minute, mdtStartTime, mdtEndTime)
                Else
                    mdtEndTime = mdtEndTime.AddDays(1)
                    wkMins = DateDiff(DateInterval.Minute, mdtStartTime, mdtEndTime)
                End If


                dgShiftDay.CurrentRow.Cells(dgcolhStarttime.Index).Value = mdtStartTime
                dgShiftDay.CurrentRow.Cells(dgcolhEndtime.Index).Value = mdtEndTime


                'Pinkal (18-Jul-2017) -- Start
                'Enhancement - TnA Enhancement for B5 Plus Company .

                'Pinkal (28-Feb-2018) -- Start
                'Internal Bug - REF ID 11 In Edit Shift If I Change In Day Start Time And Click On Save Button There Is No Error Occure But Change That We Are Done In Data Grid That Will Not Save.
                If dgShiftDay.CurrentRow.Cells(dgcolhDayStartTime.Index).Value Is Nothing OrElse IsDBNull(dgShiftDay.CurrentRow.Cells(dgcolhDayStartTime.Index).Value) Then
                dgShiftDay.CurrentRow.Cells(dgcolhDayStartTime.Index).Value = mdtStartTime.AddHours(-5)
                End If
                'Pinkal (28-Feb-2018) -- End


                'Pinkal (18-Jul-2017) -- End


                If wkMins < Val(dgShiftDay.CurrentRow.Cells(dgcolhBreaktime.Index).Value) Then
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhBreaktime.Index).ErrorText = Language.getMessage(mstrModuleName, 5, "Invalid Break Time. Break Time cannot Greater than working hours.")
                    dgShiftDay.CurrentRow.Cells(dgcolhBreaktime.Index).Selected = True
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhBreaktime.Index).ErrorText
                    mblnError = True
                    Exit Sub
                Else
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhBreaktime.Index).ErrorText = ""
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhBreaktime.Index).ErrorText
                    mblnError = False
                    wkMins = wkMins - Val(dgShiftDay.CurrentRow.Cells(dgcolhBreaktime.Index).Value)
                End If

                Dim calMinute As Double = wkMins Mod 60
                Dim calHour As Double = wkMins / 60

                dgShiftDay.CurrentRow.Cells(dgcolhWorkinghrs.Index).Value = CStr(CDec(Int(calHour) + (calMinute / 100)))

                mdtDays.Rows(e.RowIndex)("workinghrsinsec") = wkMins * 60
                mdtDays.Rows(e.RowIndex)("breaktimeinsec") = Val(dgShiftDay.CurrentRow.Cells(dgcolhBreaktime.Index).Value) * 60

                If mintShiftMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("shifttranunkid")) > 0 Then
                    mdtDays.Rows(e.RowIndex)("AUD") = "U"
                Else
                    mdtDays.Rows(e.RowIndex)("AUD") = "A"
                End If

            ElseIf dgShiftDay.CurrentCell.ColumnIndex = dgcolhHalffrmhrs.Index Or dgShiftDay.CurrentCell.ColumnIndex = dgcolhhalftohrs.Index Then


                'Pinkal (11-AUG-2017) -- Start
                'Enhancement - Working On B5 Plus Company TnA Enhancements.
                If IsDBNull(dgShiftDay.CurrentRow.Cells(dgcolhHalffrmhrs.Index).Value) OrElse IsDBNull(dgShiftDay.CurrentRow.Cells(dgcolhhalftohrs.Index).Value) Then Exit Sub
                'Pinkal (11-AUG-2017) -- End

                If CDec(dgShiftDay.CurrentRow.Cells(dgcolhWorkinghrs.Index).Value) <= CDec(dgShiftDay.CurrentRow.Cells(dgcolhHalffrmhrs.Index).Value) AndAlso CDec(dgShiftDay.CurrentRow.Cells(dgcolhHalffrmhrs.Index).Value) > 0 Then
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhHalffrmhrs.Index).ErrorText = Language.getMessage(mstrModuleName, 6, "Starting Half hour cannot be greater than equal to shift hours.")
                    dgShiftDay.CurrentRow.Cells(dgcolhHalffrmhrs.Index).Selected = True
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhHalffrmhrs.Index).ErrorText
                    mblnError = True
                    Exit Sub
                Else
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhHalffrmhrs.Index).ErrorText = ""
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhHalffrmhrs.Index).ErrorText
                    mblnError = False
                End If

                If CDec(dgShiftDay.CurrentRow.Cells(dgcolhWorkinghrs.Index).Value) <= CDec(dgShiftDay.CurrentRow.Cells(dgcolhhalftohrs.Index).Value) AndAlso CDec(dgShiftDay.CurrentRow.Cells(dgcolhhalftohrs.Index).Value) > 0 Then
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhhalftohrs.Index).ErrorText = Language.getMessage(mstrModuleName, 7, "End Half hour cannot be greater than equal to shift hours.")
                    dgShiftDay.CurrentRow.Cells(dgcolhhalftohrs.Index).Selected = True
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhhalftohrs.Index).ErrorText
                    mblnError = True
                    Exit Sub
                Else
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhhalftohrs.Index).ErrorText = ""
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhhalftohrs.Index).ErrorText
                    mblnError = False
                End If


            ElseIf dgShiftDay.CurrentCell.ColumnIndex = dgcolhOverTimeAfter.Index Then

                If CInt(dgShiftDay.CurrentRow.Cells(dgcolhOverTimeAfter.Index).Value) > (CInt(mdtDays.Rows(e.RowIndex)("workinghrsinsec")) / 60) Then
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhOverTimeAfter.Index).ErrorText = Language.getMessage(mstrModuleName, 8, "Over Time Minutes cannot be greater than shift hours.")
                    dgShiftDay.CurrentRow.Cells(dgcolhOverTimeAfter.Index).Selected = True
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhOverTimeAfter.Index).ErrorText
                    mblnError = True
                    Exit Sub
                Else
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhOverTimeAfter.Index).ErrorText = ""
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhOverTimeAfter.Index).ErrorText
                    mblnError = False
                End If

                mdtDays.Rows(e.RowIndex)("calcovertimeafterinsec") = CInt(dgShiftDay.CurrentRow.Cells(dgcolhOverTimeAfter.Index).Value) * 60

                If mintShiftMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("shifttranunkid")) > 0 Then
                    mdtDays.Rows(e.RowIndex)("AUD") = "U"
                Else
                    mdtDays.Rows(e.RowIndex)("AUD") = "A"
                End If


            ElseIf dgShiftDay.CurrentCell.ColumnIndex = dgcolhShortimeBefore.Index Then

                If CInt(dgShiftDay.CurrentRow.Cells(dgcolhShortimeBefore.Index).Value) > (CInt(mdtDays.Rows(e.RowIndex)("workinghrsinsec")) / 60) Then
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhShortimeBefore.Index).ErrorText = Language.getMessage(mstrModuleName, 9, "Short time Minutes cannot be greater than shift hours.")
                    dgShiftDay.CurrentRow.Cells(dgcolhShortimeBefore.Index).Selected = True
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhShortimeBefore.Index).ErrorText
                    mblnError = True
                    Exit Sub
                Else
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhShortimeBefore.Index).ErrorText = ""
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhShortimeBefore.Index).ErrorText
                    mblnError = False
                End If

                mdtDays.Rows(e.RowIndex)("calcshorttimebeforeinsec") = CInt(dgShiftDay.CurrentRow.Cells(dgcolhShortimeBefore.Index).Value) * 60

                If mintShiftMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("shifttranunkid")) > 0 Then
                    mdtDays.Rows(e.RowIndex)("AUD") = "U"
                Else
                    mdtDays.Rows(e.RowIndex)("AUD") = "A"
                End If


            ElseIf dgShiftDay.CurrentCell.ColumnIndex = dgcolhDayStartTime.Index Then


                'Pinkal (04-Dec-2019) -- Start
                'Enhancement NMB - Working On Close Year Enhancement for NMB.
                '[Annor set day off shift for 2 clients (Papaye & Natural) in which he configured all shift day as weekend but system didn't allow him to save due to 0 working hours for week]
                If CBool(dgShiftDay.Rows(e.RowIndex).Cells(dgcolhWeekend.Index).Value) = True Then
                    If mintShiftMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("shifttranunkid")) > 0 Then
                        mdtDays.Rows(e.RowIndex)("AUD") = "U"
                    Else
                        mdtDays.Rows(e.RowIndex)("AUD") = "A"
                    End If
                    Exit Sub
                End If
                'Pinkal (04-Dec-2019) -- End

                Dim mdtStartTime As DateTime = CDate(ConfigParameter._Object._CurrentDateAndTime.Date & " " & CDate(dgShiftDay.CurrentRow.Cells(dgcolhStarttime.Index).Value).ToString("hh:mm tt"))
                Dim mdtDayStartTime As DateTime = CDate(ConfigParameter._Object._CurrentDateAndTime.Date & " " & CDate(dgShiftDay.CurrentRow.Cells(dgcolhDayStartTime.Index).Value).ToString("hh:mm tt"))

                If DateDiff(DateInterval.Minute, mdtDayStartTime, mdtStartTime) > 300 Then
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhDayStartTime.Index).ErrorText = Language.getMessage(mstrModuleName, 10, "Difference of Day Start time and Shift start time cannot be greater than 5.")
                    dgShiftDay.CurrentRow.Cells(dgcolhDayStartTime.Index).Selected = True
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhDayStartTime.Index).ErrorText
                    mblnError = True
                    Exit Sub
                ElseIf DateDiff(DateInterval.Minute, mdtDayStartTime, mdtStartTime) < 120 Then
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhDayStartTime.Index).ErrorText = Language.getMessage(mstrModuleName, 11, "Difference of Day Start time and Shift start time cannot be less than 2.")
                    dgShiftDay.CurrentRow.Cells(dgcolhDayStartTime.Index).Selected = True
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhDayStartTime.Index).ErrorText
                    mblnError = True
                    Exit Sub
                Else
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhDayStartTime.Index).ErrorText = ""
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhDayStartTime.Index).ErrorText
                    mblnError = False
                End If


                If mintShiftMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("shifttranunkid")) > 0 Then
                    mdtDays.Rows(e.RowIndex)("AUD") = "U"
                Else
                    mdtDays.Rows(e.RowIndex)("AUD") = "A"
                End If

            ElseIf dgShiftDay.CurrentCell.ColumnIndex = dgcolhWorkinghrs.Index Then

                If CDec(dgShiftDay.CurrentCell.Value) > 24 Then
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhWorkinghrs.Index).ErrorText = Language.getMessage(mstrModuleName, 12, "Sorry, entered hours cannot be greater than 24 hrs per day.")
                    dgShiftDay.CurrentRow.Cells(dgcolhWorkinghrs.Index).Selected = True
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhWorkinghrs.Index).ErrorText
                    mblnError = True
                    Exit Sub
                Else
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhWorkinghrs.Index).ErrorText = ""
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhWorkinghrs.Index).ErrorText
                    mblnError = False
                End If


                Dim inthrsec As Integer = CInt(CDec(dgShiftDay.CurrentCell.Value).ToString("#0.00").Substring(0, CDec(dgShiftDay.CurrentCell.Value).ToString("#0.00").IndexOf(Separator))) * 3600
                Dim intminsec As Integer = CInt(CDec(dgShiftDay.CurrentCell.Value).ToString("#0.00").Substring(CDec(dgShiftDay.CurrentCell.Value).ToString("#0.00").IndexOf(Separator) + 1, CDec(dgShiftDay.CurrentCell.Value).ToString("#0.00").Length - CDec(dgShiftDay.CurrentCell.Value).ToString("#0.00").IndexOf(Separator) - 1)) * 60

                mdtDays.Rows(e.RowIndex)("workinghrsinsec") = inthrsec + intminsec

                If mintShiftMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("shifttranunkid")) > 0 Then
                    mdtDays.Rows(e.RowIndex)("AUD") = "U"
                Else
                    mdtDays.Rows(e.RowIndex)("AUD") = "A"
                End If


                'Pinkal (03-Aug-2018) -- Start
                'Enhancement - Changes For B5 [Ref #289]

            ElseIf dgShiftDay.CurrentCell.ColumnIndex = dgcolhOTGraceAfterShiftEndTime.Index Then

                If CInt(dgShiftDay.CurrentRow.Cells(dgcolhOTGraceAfterShiftEndTime.Index).Value) > (CInt(mdtDays.Rows(e.RowIndex)("workinghrsinsec")) / 60) Then
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhOTGraceAfterShiftEndTime.Index).ErrorText = Language.getMessage(mstrModuleName, 15, "Over Time grace minutes cannot be greater than shift hours.")
                    dgShiftDay.CurrentRow.Cells(dgcolhOTGraceAfterShiftEndTime.Index).Selected = True
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhOTGraceAfterShiftEndTime.Index).ErrorText
                    mblnError = True
                    Exit Sub
                ElseIf CInt(dgShiftDay.CurrentRow.Cells(dgcolhOTGraceAfterShiftEndTime.Index).Value) > 0 AndAlso CInt(dgShiftDay.CurrentRow.Cells(dgcolhOTGraceAfterShiftEndTime.Index).Value) < (CInt(mdtDays.Rows(e.RowIndex)("calcovertimeafterinsec")) / 60) Then
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhOTGraceAfterShiftEndTime.Index).ErrorText = Language.getMessage(mstrModuleName, 16, "Over Time grace minutes cannot be less than") & " " & dgShiftDay.Columns(dgcolhOverTimeAfter.Index).HeaderText & "."
                    dgShiftDay.CurrentRow.Cells(dgcolhOTGraceAfterShiftEndTime.Index).Selected = True
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhOTGraceAfterShiftEndTime.Index).ErrorText
                    mblnError = True
                    Exit Sub
                Else
                    dgShiftDay.Rows(e.RowIndex).Cells(dgcolhOTGraceAfterShiftEndTime.Index).ErrorText = ""
                    mstrErrorMsg = dgShiftDay.Rows(e.RowIndex).Cells(dgcolhOTGraceAfterShiftEndTime.Index).ErrorText
                    mblnError = False
                End If

                mdtDays.Rows(e.RowIndex)("countotmins_aftersftendtimeinsec") = CInt(dgShiftDay.CurrentRow.Cells(dgcolhOTGraceAfterShiftEndTime.Index).Value) * 60

                If mintShiftMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("shifttranunkid")) > 0 Then
                    mdtDays.Rows(e.RowIndex)("AUD") = "U"
                Else
                    mdtDays.Rows(e.RowIndex)("AUD") = "A"
                End If

                'Pinkal (03-Aug-2018) -- End

                'Pinkal (29-Apr-2020) -- Start
                'Bug  -  Problem Solved  when User set night from hrs and night to hrs but not set in shift master
            ElseIf dgShiftDay.CurrentCell.ColumnIndex = dgcolhnightfrmhrs.Index OrElse dgShiftDay.CurrentCell.ColumnIndex = dgcolhnighttohrs.Index Then
                Dim mdtNightFromDate As DateTime = CDate(mdtDays.Rows(e.RowIndex)("nightfromhrs"))
                Dim mdtNightToDate As DateTime = CDate(mdtDays.Rows(e.RowIndex)("nighttohrs"))
                If mdtNightFromDate = Nothing Then
                    mdtDays.Rows(e.RowIndex)("nightfromhrs") = ConfigParameter._Object._CurrentDateAndTime.Date
                End If

                If mdtNightToDate = Nothing Then
                    mdtDays.Rows(e.RowIndex)("nighttohrs") = ConfigParameter._Object._CurrentDateAndTime.Date
                End If

                If mintShiftMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("shifttranunkid")) > 0 Then
                    mdtDays.Rows(e.RowIndex)("AUD") = "U"
                Else
                    mdtDays.Rows(e.RowIndex)("AUD") = "A"
                End If
                'Pinkal (29-Apr-2020) -- End

            End If

            GetTotalWeekHours()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgShiftDay_CellValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgShiftDay_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgShiftDay.CellContentClick, dgShiftDay.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = dgcolhWeekend.Index Then

                If dgShiftDay.IsCurrentCellDirty Then
                    dgShiftDay.CommitEdit(DataGridViewDataErrorContexts.Commit)

                    If mintShiftMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("shifttranunkid")) > 0 Then
                        mdtDays.Rows(e.RowIndex)("AUD") = "U"
                    Else
                        mdtDays.Rows(e.RowIndex)("AUD") = "A"
                    End If

                    mdtDays.AcceptChanges()

                    GetTotalWeekHours()

                End If

                'Pinkal (01-Apr-2020) -- Start
                'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
            ElseIf e.ColumnIndex = dgcolhOTHoliday.Index Then

                If dgShiftDay.IsCurrentCellDirty Then
                    dgShiftDay.CommitEdit(DataGridViewDataErrorContexts.Commit)

                    If mintShiftMasterUnkid > 0 AndAlso CInt(mdtDays.Rows(e.RowIndex)("shifttranunkid")) > 0 Then
                        mdtDays.Rows(e.RowIndex)("AUD") = "U"
                    Else
                        mdtDays.Rows(e.RowIndex)("AUD") = "A"
                    End If

                    mdtDays.AcceptChanges()

                    GetTotalWeekHours()
                End If

                'Pinkal (01-Apr-2020) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgShiftDay_CellContentClick", mstrModuleName)
        End Try

    End Sub

    Private Sub dgShiftDay_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgShiftDay.DataError

    End Sub

#End Region

#Region "LinkButton Event"

    Private Sub lnkCopyToAll_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopyToAll.LinkClicked
        Try

            mdtDays.AcceptChanges()


            'Pinkal (04-Dec-2019) -- Start
            'Enhancement NMB - Working On Close Year Enhancement for NMB.
            '[Annor set day off shift for 2 clients (Papaye & Natural) in which he configured all shift day as weekend but system didn't allow him to save due to 0 working hours for week]

            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .


            Dim drRow() As DataRow = mdtDays.Select("isweekend = false", "dayid asc")

            If drRow.Length > 0 Then

                'Pinkal (18-Jul-2017) -- Start
                'Enhancement - TnA Enhancement for B5 Plus Company .
                'For i As Integer = 0 To mdtDays.Rows.Count - 1
                For i As Integer = 0 To mdtDays.Rows.Count - 2
                    'Pinkal (18-Jul-2017) -- End
                    If CBool(mdtDays.Rows(i)("isweekend")) = True Then Continue For

                    dgShiftDay.Rows(i).Selected = True

                    If dgcolhStarttime.Visible AndAlso chkIsOpenShift.Checked = False Then
                        mdtDays.Rows(i)("starttime") = drRow(0)("starttime")
                        dgShiftDay.CurrentCell = dgShiftDay.Rows(i).Cells(dgcolhStarttime.Index)
                        dgShiftDay_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhStarttime.Index, i))
                    End If

                    If dgcolhEndtime.Visible AndAlso chkIsOpenShift.Checked = False Then
                        mdtDays.Rows(i)("endtime") = drRow(0)("endtime")
                        dgShiftDay.CurrentCell = dgShiftDay.Rows(i).Cells(dgcolhEndtime.Index)
                        dgShiftDay_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhEndtime.Index, i))
                    End If

                    If dgcolhBreaktime.Visible Then
                        mdtDays.Rows(i)("breaktime") = drRow(0)("breaktime")
                        dgShiftDay.CurrentCell = dgShiftDay.Rows(i).Cells(dgcolhBreaktime.Index)
                        dgShiftDay_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhBreaktime.Index, i))
                        If dgShiftDay.Rows(i).Cells(dgcolhBreaktime.Index).ErrorText <> "" Then
                            eZeeMsgBox.Show(dgShiftDay.Rows(i).Cells(dgcolhBreaktime.Index).ErrorText)
                            dgShiftDay.CurrentCell.Selected = True
                            Exit For
                        End If
                    End If

                    If dgcolhWorkinghrs.Visible Then
                        mdtDays.Rows(i)("workinghrs") = drRow(0)("workinghrs")
                        If chkIsOpenShift.Checked Then mdtDays.Rows(i)("AUD") = "U"
                    End If

                    If ConfigParameter._Object._PolicyManagementTNA = False Then
                        If dgcolhOverTimeAfter.Visible Then
                        mdtDays.Rows(i)("calcovertimeafter") = drRow(0)("calcovertimeafter")
                        dgShiftDay.CurrentCell = dgShiftDay.Rows(i).Cells(dgcolhOverTimeAfter.Index)
                        dgShiftDay_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhOverTimeAfter.Index, i))
                        If dgShiftDay.Rows(i).Cells(dgcolhOverTimeAfter.Index).ErrorText <> "" Then
                            eZeeMsgBox.Show(dgShiftDay.Rows(i).Cells(dgcolhOverTimeAfter.Index).ErrorText)
                            dgShiftDay.CurrentCell.Selected = True
                            Exit For
                        End If
                        End If

                        If dgcolhShortimeBefore.Visible Then
                        mdtDays.Rows(i)("calcshorttimebefore") = drRow(0)("calcshorttimebefore")
                        dgShiftDay.CurrentCell = dgShiftDay.Rows(i).Cells(dgcolhShortimeBefore.Index)
                        dgShiftDay_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhShortimeBefore.Index, i))
                        If dgShiftDay.Rows(i).Cells(dgcolhShortimeBefore.Index).ErrorText <> "" Then
                            eZeeMsgBox.Show(dgShiftDay.Rows(i).Cells(dgcolhShortimeBefore.Index).ErrorText)
                            dgShiftDay.CurrentCell.Selected = True
                            Exit For
                        End If
                        End If

                        If dgcolhHalffrmhrs.Visible Then
                        mdtDays.Rows(i)("halffromhrs") = drRow(0)("halffromhrs")
                        mdtDays.Rows(i)("halffromhrsinsec") = drRow(0)("halffromhrsinsec")
                        dgShiftDay.CurrentCell = dgShiftDay.Rows(i).Cells(dgcolhHalffrmhrs.Index)
                        dgShiftDay_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhHalffrmhrs.Index, i))
                        If dgShiftDay.Rows(i).Cells(dgcolhHalffrmhrs.Index).ErrorText <> "" Then
                            eZeeMsgBox.Show(dgShiftDay.Rows(i).Cells(dgcolhHalffrmhrs.Index).ErrorText)
                            dgShiftDay.CurrentCell.Selected = True
                            Exit For
                        End If
                        End If

                        If dgcolhhalftohrs.Visible Then
                        mdtDays.Rows(i)("halftohrs") = drRow(0)("halftohrs")
                        mdtDays.Rows(i)("halftohrsinsec") = drRow(0)("halftohrsinsec")
                        dgShiftDay.CurrentCell = dgShiftDay.Rows(i).Cells(dgcolhhalftohrs.Index)
                        dgShiftDay_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhhalftohrs.Index, i))
                        If dgShiftDay.Rows(i).Cells(dgcolhhalftohrs.Index).ErrorText <> "" Then
                            eZeeMsgBox.Show(dgShiftDay.Rows(i).Cells(dgcolhhalftohrs.Index).ErrorText)
                            dgShiftDay.CurrentCell.Selected = True
                            Exit For
                        End If
                        End If

                        If dgcolhnightfrmhrs.Visible Then
                        mdtDays.Rows(i)("nightfromhrs") = drRow(0)("nightfromhrs")
                        dgShiftDay.CurrentCell = dgShiftDay.Rows(i).Cells(dgcolhnightfrmhrs.Index)
                        dgShiftDay_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhnightfrmhrs.Index, i))
                        If dgShiftDay.Rows(i).Cells(dgcolhnightfrmhrs.Index).ErrorText <> "" Then
                            eZeeMsgBox.Show(dgShiftDay.Rows(i).Cells(dgcolhnightfrmhrs.Index).ErrorText)
                            dgShiftDay.CurrentCell.Selected = True
                            Exit For
                        End If
                        End If

                        If dgcolhnighttohrs.Visible Then
                        mdtDays.Rows(i)("nighttohrs") = drRow(0)("nighttohrs")
                        dgShiftDay.CurrentCell = dgShiftDay.Rows(i).Cells(dgcolhnighttohrs.Index)
                        dgShiftDay_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhnighttohrs.Index, i))
                        If dgShiftDay.Rows(i).Cells(dgcolhnighttohrs.Index).ErrorText <> "" Then
                            eZeeMsgBox.Show(dgShiftDay.Rows(i).Cells(dgcolhnighttohrs.Index).ErrorText)
                            dgShiftDay.CurrentCell.Selected = True
                            Exit For
                        End If
                        End If

                        If dgcolhOTGraceAfterShiftEndTime.Visible Then
                        mdtDays.Rows(i)("countotmins_aftersftendtime") = drRow(0)("countotmins_aftersftendtime")
                        mdtDays.Rows(i)("countotmins_aftersftendtimeinsec") = drRow(0)("countotmins_aftersftendtimeinsec")
                        dgShiftDay.CurrentCell = dgShiftDay.Rows(i).Cells(dgcolhOTGraceAfterShiftEndTime.Index)
                        dgShiftDay_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhOTGraceAfterShiftEndTime.Index, i))
                        If dgShiftDay.Rows(i).Cells(dgcolhOTGraceAfterShiftEndTime.Index).ErrorText <> "" Then
                            eZeeMsgBox.Show(dgShiftDay.Rows(i).Cells(dgcolhOTGraceAfterShiftEndTime.Index).ErrorText)
                            dgShiftDay.CurrentCell.Selected = True
                            Exit For
                        End If
                        End If

                    End If

                    mdtDays.Rows(i)("breaktimeinsec") = drRow(0)("breaktimeinsec")
                    mdtDays.Rows(i)("workinghrsinsec") = drRow(0)("workinghrsinsec")
                    mdtDays.Rows(i)("calcovertimeafterinsec") = drRow(0)("calcovertimeafterinsec")
                    mdtDays.Rows(i)("calcshorttimebeforeinsec") = drRow(0)("calcshorttimebeforeinsec")

                    If dgcolhDayStartTime.Visible Then
                        mdtDays.Rows(i)("daystart_time") = drRow(0)("daystart_time")
                        dgShiftDay.CurrentCell = dgShiftDay.Rows(i).Cells(dgcolhDayStartTime.Index)
                        dgShiftDay_CellValueChanged(sender, New DataGridViewCellEventArgs(dgcolhDayStartTime.Index, i))
                        If dgShiftDay.Rows(i).Cells(dgcolhDayStartTime.Index).ErrorText <> "" Then
                            eZeeMsgBox.Show(dgShiftDay.Rows(i).Cells(dgcolhDayStartTime.Index).ErrorText)
                            dgShiftDay.CurrentCell.Selected = True
                            Exit For
                        End If
                    End If
                Next
                mdtDays.AcceptChanges()
                GetTotalWeekHours()
            End If
            dgShiftDay.FirstDisplayedScrollingColumnIndex = dgcolhWeekend.Index

            'Pinkal (04-Dec-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCopyToAll_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Checkbox Event"

    Private Sub chkIsOpenShift_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIsOpenShift.CheckedChanged
        Try
            If chkIsOpenShift.Checked Then
                dgcolhStarttime.Visible = False
                dgcolhEndtime.Visible = False
                dgcolhDayStartTime.Visible = False
                dgcolhWorkinghrs.ReadOnly = False
                dgcolhDay.Frozen = False
                If ConfigParameter._Object._PolicyManagementTNA Then
                    dgcolhDay.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                End If
                If menAction = enAction.ADD_ONE OrElse menAction = enAction.ADD_CONTINUE Then
                    GenerateShiftDays()
                End If

                'Pinkal (18-Jul-2017) -- Start
                'Enhancement - TnA Enhancement for B5 Plus Company .
                'lblMaxHours.Visible = True
                'nudMaxhours.Visible = True
                lblMaxHours.Enabled = True
                nudMaxhours.Enabled = True
                'Pinkal (11-AUG-2017) -- Start
                'Enhancement - Working On B5 Plus Company TnA Enhancements.
                'nudMaxhours.Value = 0
                'Pinkal (11-AUG-2017) -- End
                'Pinkal (18-Jul-2017) -- End



                'Pinkal (06-May-2016) -- Start
                'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
                If menAction = enAction.ADD_ONE OrElse menAction = enAction.ADD_CONTINUE Then
                    ChkSetAttOnDeviceStatus.Enabled = True
                Else
                    ChkSetAttOnDeviceStatus.Enabled = False
                End If
                'Pinkal (06-May-2016) -- End


                'Pinkal (12-Oct-2017) -- Start
                'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
                rdCountOTOnWorkedHrs.Checked = True
                rdCountOTOnWorkedHrs.Enabled = False
                rdCountOTAfterShiftEndTime.Enabled = False
                'Pinkal (12-Oct-2017) -- End

            Else

                'Pinkal (18-Jul-2017) -- Start
                'Enhancement - TnA Enhancement for B5 Plus Company .
                'lblMaxHours.Visible = False
                'nudMaxhours.Visible = False
                lblMaxHours.Enabled = False
                nudMaxhours.Enabled = False
                nudMaxhours.Value = 0
                'Pinkal (18-Jul-2017) -- End

                'Pinkal (06-May-2016) -- Start
                'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
                ChkSetAttOnDeviceStatus.Checked = False
                ChkSetAttOnDeviceStatus.Enabled = False
                'Pinkal (06-May-2016) -- End


                'Pinkal (18-Jul-2017) -- Start
                'Enhancement - TnA Enhancement for B5 Plus Company .
                dgcolhStarttime.Visible = True
                dgcolhEndtime.Visible = True
                dgcolhDayStartTime.Visible = True
                dgcolhWorkinghrs.ReadOnly = True
                dgcolhDay.Frozen = True
                dgcolhDay.Width = 275
                If menAction = enAction.ADD_ONE OrElse menAction = enAction.ADD_CONTINUE Then
                    mdtDays = Nothing
                    GenerateShiftDays()
                End If
                'Pinkal (18-Jul-2017) -- End

                'Pinkal (12-Oct-2017) -- Start
                'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
                rdCountOTOnWorkedHrs.Enabled = True
                rdCountOTAfterShiftEndTime.Enabled = True
                'Pinkal (12-Oct-2017) -- End

            End If
            'Pinkal (03-AUG-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkIsOpenShift_CheckedChanged", mstrModuleName)
        End Try
    End Sub


#End Region

    'Pinkal (12-Oct-2017) -- Start
    'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).

#Region "Radio Button Events"

    Private Sub rdCountOTOnWorkedHrs_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdCountOTOnWorkedHrs.CheckedChanged _
                                                                                                                                                                                               , rdCountOTAfterShiftEndTime.CheckedChanged
        Try

            'Pinkal (28-Feb-2018) -- Start
            'Internal Bug - REF ID 12 In Add/Edit Shift If I Change In Day Start Time And Try To Change Overtime Setting It Throw An Error.

            RemoveHandler dgShiftDay.CellValueChanged, AddressOf dgShiftDay_CellValueChanged

            If CType(sender, RadioButton).Name.ToUpper() = rdCountOTOnWorkedHrs.Name.ToUpper() Then
                dgcolhOverTimeAfter.HeaderText = rdCountOTOnWorkedHrs.Text
                dgcolhOverTimeAfter.Width = 270

                'Pinkal (03-Aug-2018) -- Start
                'Enhancement - Changes For B5 [Ref #289]
                If mblnError Then
                    eZeeMsgBox.Show(mstrErrorMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    RemoveHandler rdCountOTOnWorkedHrs.CheckedChanged, AddressOf rdCountOTOnWorkedHrs_CheckedChanged
                    rdCountOTAfterShiftEndTime.Checked = True
                    AddHandler rdCountOTOnWorkedHrs.CheckedChanged, AddressOf rdCountOTOnWorkedHrs_CheckedChanged
                    Exit Sub
                Else
                    dgcolhOTGraceAfterShiftEndTime.Visible = False
                End If
                'Pinkal (03-Aug-2018) -- End

            ElseIf CType(sender, RadioButton).Name.ToUpper() = rdCountOTAfterShiftEndTime.Name.ToUpper() Then
                dgcolhOverTimeAfter.HeaderText = rdCountOTAfterShiftEndTime.Text
                dgcolhOverTimeAfter.Width = 250

                'Pinkal (03-Aug-2018) -- Start
                'Enhancement - Changes For B5 [Ref #289]
                dgcolhOTGraceAfterShiftEndTime.Visible = True
                'Pinkal (03-Aug-2018) -- End

            End If

            dgcolhOverTimeAfter.HeaderText &= " " & Language.getMessage(mstrModuleName, 14, "After (Mins)")

            AddHandler dgShiftDay.CellValueChanged, AddressOf dgShiftDay_CellValueChanged

            'Pinkal (28-Feb-2018) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "rdCountOTOnWorkedHrs_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (12-Oct-2017) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
		
            Call SetLanguage()

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbShiftSettings.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbShiftSettings.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.lblMaxHours.Text = Language._Object.getCaption(Me.lblMaxHours.Name, Me.lblMaxHours.Text)
            Me.chkIsOpenShift.Text = Language._Object.getCaption(Me.chkIsOpenShift.Name, Me.chkIsOpenShift.Text)
            Me.lblShiftType.Text = Language._Object.getCaption(Me.lblShiftType.Name, Me.lblShiftType.Text)
            Me.lblcode.Text = Language._Object.getCaption(Me.lblcode.Name, Me.lblcode.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.EZeeHeading1.Text = Language._Object.getCaption(Me.EZeeHeading1.Name, Me.EZeeHeading1.Text)
            Me.lnkCopyToAll.Text = Language._Object.getCaption(Me.lnkCopyToAll.Name, Me.lnkCopyToAll.Text)
            Me.ChkSetAttOnDeviceStatus.Text = Language._Object.getCaption(Me.ChkSetAttOnDeviceStatus.Name, Me.ChkSetAttOnDeviceStatus.Text)
            Me.gbShiftSettings.Text = Language._Object.getCaption(Me.gbShiftSettings.Name, Me.gbShiftSettings.Text)
            Me.chkNormalHrsHoliday.Text = Language._Object.getCaption(Me.chkNormalHrsHoliday.Name, Me.chkNormalHrsHoliday.Text)
            Me.chkNormalHrsDayOff.Text = Language._Object.getCaption(Me.chkNormalHrsDayOff.Name, Me.chkNormalHrsDayOff.Text)
            Me.chkNormalHrsWeekend.Text = Language._Object.getCaption(Me.chkNormalHrsWeekend.Name, Me.chkNormalHrsWeekend.Text)
            Me.gbRevAgreedScore.Text = Language._Object.getCaption(Me.gbRevAgreedScore.Name, Me.gbRevAgreedScore.Text)
			Me.chkCountShiftTimingOnly.Text = Language._Object.getCaption(Me.chkCountShiftTimingOnly.Name, Me.chkCountShiftTimingOnly.Text)
			Me.rdCountOTAfterShiftEndTime.Text = Language._Object.getCaption(Me.rdCountOTAfterShiftEndTime.Name, Me.rdCountOTAfterShiftEndTime.Text)
			Me.rdCountOTOnWorkedHrs.Text = Language._Object.getCaption(Me.rdCountOTOnWorkedHrs.Name, Me.rdCountOTOnWorkedHrs.Text)
			Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.dgcolhDay.HeaderText = Language._Object.getCaption(Me.dgcolhDay.Name, Me.dgcolhDay.HeaderText)
            Me.dgcolhWeekend.HeaderText = Language._Object.getCaption(Me.dgcolhWeekend.Name, Me.dgcolhWeekend.HeaderText)
            Me.dgcolhStarttime.HeaderText = Language._Object.getCaption(Me.dgcolhStarttime.Name, Me.dgcolhStarttime.HeaderText)
            Me.dgcolhEndtime.HeaderText = Language._Object.getCaption(Me.dgcolhEndtime.Name, Me.dgcolhEndtime.HeaderText)
            Me.dgcolhBreaktime.HeaderText = Language._Object.getCaption(Me.dgcolhBreaktime.Name, Me.dgcolhBreaktime.HeaderText)
            Me.dgcolhWorkinghrs.HeaderText = Language._Object.getCaption(Me.dgcolhWorkinghrs.Name, Me.dgcolhWorkinghrs.HeaderText)
            Me.dgcolhOverTimeAfter.HeaderText = Language._Object.getCaption(Me.dgcolhOverTimeAfter.Name, Me.dgcolhOverTimeAfter.HeaderText)
			Me.dgcolhOTGraceAfterShiftEndTime.HeaderText = Language._Object.getCaption(Me.dgcolhOTGraceAfterShiftEndTime.Name, Me.dgcolhOTGraceAfterShiftEndTime.HeaderText)
            Me.dgcolhShortimeBefore.HeaderText = Language._Object.getCaption(Me.dgcolhShortimeBefore.Name, Me.dgcolhShortimeBefore.HeaderText)
            Me.dgcolhHalffrmhrs.HeaderText = Language._Object.getCaption(Me.dgcolhHalffrmhrs.Name, Me.dgcolhHalffrmhrs.HeaderText)
            Me.dgcolhhalftohrs.HeaderText = Language._Object.getCaption(Me.dgcolhhalftohrs.Name, Me.dgcolhhalftohrs.HeaderText)
            Me.dgcolhnightfrmhrs.HeaderText = Language._Object.getCaption(Me.dgcolhnightfrmhrs.Name, Me.dgcolhnightfrmhrs.HeaderText)
            Me.dgcolhnighttohrs.HeaderText = Language._Object.getCaption(Me.dgcolhnighttohrs.Name, Me.dgcolhnighttohrs.HeaderText)
            Me.dgcolhDayStartTime.HeaderText = Language._Object.getCaption(Me.dgcolhDayStartTime.Name, Me.dgcolhDayStartTime.HeaderText)

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Shift Code cannot be blank. Shift Code is required information.")
            Language.setMessage(mstrModuleName, 2, "Shift Type is compulsory information.Please Select Shift Type.")
            Language.setMessage(mstrModuleName, 3, "Shift Name cannot be blank. Shift Name is required information.")
            Language.setMessage(mstrModuleName, 4, "Shift Hour is compulsory information.Please define Shift hour.")
            Language.setMessage(mstrModuleName, 5, "Invalid Break Time. Break Time cannot Greater than working hours.")
            Language.setMessage(mstrModuleName, 6, "Starting Half hour cannot be greater than equal to shift hours.")
            Language.setMessage(mstrModuleName, 7, "End Half hour cannot be greater than equal to shift hours.")
            Language.setMessage(mstrModuleName, 8, "Over Time Minutes cannot be greater than shift hours.")
            Language.setMessage(mstrModuleName, 9, "Short time Minutes cannot be greater than shift hours.")
            Language.setMessage(mstrModuleName, 10, "Difference of Day Start time and Shift start time cannot be greater than 5.")
            Language.setMessage(mstrModuleName, 11, "Difference of Day Start time and Shift start time cannot be less than 2.")
            Language.setMessage(mstrModuleName, 12, "Sorry, entered hours cannot be greater than 24 hrs per day.")
            Language.setMessage(mstrModuleName, 13, "Total Week Hours (Excluding Weekend Hrs)")
            Language.setMessage(mstrModuleName, 14, "After (Mins)")
			Language.setMessage(mstrModuleName, 15, "Over Time grace minutes cannot be greater than shift hours.")
			Language.setMessage(mstrModuleName, 16, "Over Time grace minutes cannot be less than")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class