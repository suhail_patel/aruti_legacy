﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewShift_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNewShift_AddEdit))
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddShiftType = New eZee.Common.eZeeGradientButton
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.cboShiftType = New System.Windows.Forms.ComboBox
        Me.lblShiftType = New System.Windows.Forms.Label
        Me.lblcode = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.ChkSetAttOnDeviceStatus = New System.Windows.Forms.CheckBox
        Me.nudMaxhours = New System.Windows.Forms.NumericUpDown
        Me.lblMaxHours = New System.Windows.Forms.Label
        Me.chkIsOpenShift = New System.Windows.Forms.CheckBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgShiftDay = New System.Windows.Forms.DataGridView
        Me.EZeeHeading1 = New eZee.Common.eZeeHeading
        Me.lnkCopyToAll = New System.Windows.Forms.LinkLabel
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbShiftSettings = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.rdCountOTAfterShiftEndTime = New System.Windows.Forms.RadioButton
        Me.rdCountOTOnWorkedHrs = New System.Windows.Forms.RadioButton
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.chkCountShiftTimingOnly = New System.Windows.Forms.CheckBox
        Me.gbRevAgreedScore = New System.Windows.Forms.GroupBox
        Me.chkNormalHrsHoliday = New System.Windows.Forms.CheckBox
        Me.chkNormalHrsWeekend = New System.Windows.Forms.CheckBox
        Me.chkNormalHrsDayOff = New System.Windows.Forms.CheckBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDay = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhWeekend = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhOTHoliday = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhStarttime = New eZee.Common.DataGridViewMaskTextBoxColumn
        Me.dgcolhEndtime = New eZee.Common.DataGridViewMaskTextBoxColumn
        Me.dgcolhBreaktime = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhWorkinghrs = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhOverTimeAfter = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhOTGraceAfterShiftEndTime = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhShortimeBefore = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhHalffrmhrs = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhhalftohrs = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhnightfrmhrs = New eZee.Common.DataGridViewMaskTextBoxColumn
        Me.dgcolhnighttohrs = New eZee.Common.DataGridViewMaskTextBoxColumn
        Me.dgcolhDayStartTime = New eZee.Common.DataGridViewMaskTextBoxColumn
        Me.gbInfo.SuspendLayout()
        CType(Me.nudMaxhours, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        CType(Me.dgShiftDay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeHeading1.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.gbShiftSettings.SuspendLayout()
        Me.gbRevAgreedScore.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbInfo
        '
        Me.gbInfo.BackColor = System.Drawing.Color.Transparent
        Me.gbInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInfo.Checked = False
        Me.gbInfo.CollapseAllExceptThis = False
        Me.gbInfo.CollapsedHoverImage = Nothing
        Me.gbInfo.CollapsedNormalImage = Nothing
        Me.gbInfo.CollapsedPressedImage = Nothing
        Me.gbInfo.CollapseOnLoad = False
        Me.gbInfo.Controls.Add(Me.objbtnAddShiftType)
        Me.gbInfo.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbInfo.Controls.Add(Me.cboShiftType)
        Me.gbInfo.Controls.Add(Me.lblShiftType)
        Me.gbInfo.Controls.Add(Me.lblcode)
        Me.gbInfo.Controls.Add(Me.lblName)
        Me.gbInfo.Controls.Add(Me.txtName)
        Me.gbInfo.Controls.Add(Me.txtCode)
        Me.gbInfo.ExpandedHoverImage = Nothing
        Me.gbInfo.ExpandedNormalImage = Nothing
        Me.gbInfo.ExpandedPressedImage = Nothing
        Me.gbInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInfo.HeaderHeight = 25
        Me.gbInfo.HeaderMessage = ""
        Me.gbInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInfo.HeightOnCollapse = 0
        Me.gbInfo.LeftTextSpace = 0
        Me.gbInfo.Location = New System.Drawing.Point(1, 2)
        Me.gbInfo.Name = "gbInfo"
        Me.gbInfo.OpenHeight = 300
        Me.gbInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.gbInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInfo.ShowBorder = True
        Me.gbInfo.ShowCheckBox = False
        Me.gbInfo.ShowCollapseButton = False
        Me.gbInfo.ShowDefaultBorderColor = True
        Me.gbInfo.ShowDownButton = False
        Me.gbInfo.ShowHeader = True
        Me.gbInfo.Size = New System.Drawing.Size(563, 84)
        Me.gbInfo.TabIndex = 0
        Me.gbInfo.Temp = 0
        Me.gbInfo.Text = "Shift Information"
        Me.gbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddShiftType
        '
        Me.objbtnAddShiftType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddShiftType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddShiftType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddShiftType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddShiftType.BorderSelected = False
        Me.objbtnAddShiftType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddShiftType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddShiftType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddShiftType.Location = New System.Drawing.Point(531, 30)
        Me.objbtnAddShiftType.Name = "objbtnAddShiftType"
        Me.objbtnAddShiftType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddShiftType.TabIndex = 4
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(531, 57)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 7
        '
        'cboShiftType
        '
        Me.cboShiftType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShiftType.DropDownWidth = 300
        Me.cboShiftType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShiftType.FormattingEnabled = True
        Me.cboShiftType.Location = New System.Drawing.Point(344, 30)
        Me.cboShiftType.Name = "cboShiftType"
        Me.cboShiftType.Size = New System.Drawing.Size(181, 21)
        Me.cboShiftType.TabIndex = 3
        '
        'lblShiftType
        '
        Me.lblShiftType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShiftType.Location = New System.Drawing.Point(272, 34)
        Me.lblShiftType.Name = "lblShiftType"
        Me.lblShiftType.Size = New System.Drawing.Size(66, 13)
        Me.lblShiftType.TabIndex = 2
        Me.lblShiftType.Text = "Shift Type"
        Me.lblShiftType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblcode
        '
        Me.lblcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblcode.Location = New System.Drawing.Point(8, 34)
        Me.lblcode.Name = "lblcode"
        Me.lblcode.Size = New System.Drawing.Size(67, 13)
        Me.lblcode.TabIndex = 0
        Me.lblcode.Text = "Code"
        Me.lblcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 61)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(67, 13)
        Me.lblName.TabIndex = 5
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(84, 57)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(441, 21)
        Me.txtName.TabIndex = 6
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(84, 30)
        Me.txtCode.MaxLength = 255
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(161, 21)
        Me.txtCode.TabIndex = 1
        '
        'ChkSetAttOnDeviceStatus
        '
        Me.ChkSetAttOnDeviceStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChkSetAttOnDeviceStatus.Location = New System.Drawing.Point(290, 31)
        Me.ChkSetAttOnDeviceStatus.Name = "ChkSetAttOnDeviceStatus"
        Me.ChkSetAttOnDeviceStatus.Size = New System.Drawing.Size(267, 17)
        Me.ChkSetAttOnDeviceStatus.TabIndex = 3
        Me.ChkSetAttOnDeviceStatus.Text = "Set Attendance on device In/Out Status" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.ChkSetAttOnDeviceStatus.UseVisualStyleBackColor = True
        '
        'nudMaxhours
        '
        Me.nudMaxhours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMaxhours.Location = New System.Drawing.Point(203, 29)
        Me.nudMaxhours.Maximum = New Decimal(New Integer() {170, 0, 0, 0})
        Me.nudMaxhours.Name = "nudMaxhours"
        Me.nudMaxhours.Size = New System.Drawing.Size(63, 21)
        Me.nudMaxhours.TabIndex = 2
        Me.nudMaxhours.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaxHours
        '
        Me.lblMaxHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblMaxHours.Location = New System.Drawing.Point(110, 31)
        Me.lblMaxHours.Name = "lblMaxHours"
        Me.lblMaxHours.Size = New System.Drawing.Size(72, 17)
        Me.lblMaxHours.TabIndex = 1
        Me.lblMaxHours.Text = "Max. Hours"
        Me.lblMaxHours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIsOpenShift
        '
        Me.chkIsOpenShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsOpenShift.Location = New System.Drawing.Point(11, 31)
        Me.chkIsOpenShift.Name = "chkIsOpenShift"
        Me.chkIsOpenShift.Size = New System.Drawing.Size(93, 17)
        Me.chkIsOpenShift.TabIndex = 0
        Me.chkIsOpenShift.Text = "Open Shift"
        Me.chkIsOpenShift.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objFooter.Location = New System.Drawing.Point(0, 468)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(567, 55)
        Me.objFooter.TabIndex = 5
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(368, 11)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(464, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgShiftDay
        '
        Me.dgShiftDay.AllowUserToAddRows = False
        Me.dgShiftDay.AllowUserToDeleteRows = False
        Me.dgShiftDay.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgShiftDay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgShiftDay.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhDay, Me.dgcolhWeekend, Me.dgcolhOTHoliday, Me.dgcolhStarttime, Me.dgcolhEndtime, Me.dgcolhBreaktime, Me.dgcolhWorkinghrs, Me.dgcolhOverTimeAfter, Me.dgcolhOTGraceAfterShiftEndTime, Me.dgcolhShortimeBefore, Me.dgcolhHalffrmhrs, Me.dgcolhhalftohrs, Me.dgcolhnightfrmhrs, Me.dgcolhnighttohrs, Me.dgcolhDayStartTime})
        Me.dgShiftDay.Location = New System.Drawing.Point(2, 119)
        Me.dgShiftDay.Name = "dgShiftDay"
        Me.dgShiftDay.RowHeadersVisible = False
        Me.dgShiftDay.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgShiftDay.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgShiftDay.Size = New System.Drawing.Size(563, 218)
        Me.dgShiftDay.TabIndex = 2
        '
        'EZeeHeading1
        '
        Me.EZeeHeading1.BorderColor = System.Drawing.Color.Black
        Me.EZeeHeading1.Controls.Add(Me.lnkCopyToAll)
        Me.EZeeHeading1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeading1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeHeading1.Location = New System.Drawing.Point(2, 91)
        Me.EZeeHeading1.Name = "EZeeHeading1"
        Me.EZeeHeading1.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        Me.EZeeHeading1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeHeading1.ShowDefaultBorderColor = True
        Me.EZeeHeading1.Size = New System.Drawing.Size(563, 25)
        Me.EZeeHeading1.TabIndex = 1
        '
        'lnkCopyToAll
        '
        Me.lnkCopyToAll.BackColor = System.Drawing.Color.Transparent
        Me.lnkCopyToAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkCopyToAll.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkCopyToAll.Location = New System.Drawing.Point(469, 3)
        Me.lnkCopyToAll.Name = "lnkCopyToAll"
        Me.lnkCopyToAll.Size = New System.Drawing.Size(91, 19)
        Me.lnkCopyToAll.TabIndex = 0
        Me.lnkCopyToAll.TabStop = True
        Me.lnkCopyToAll.Text = "Copy To All"
        Me.lnkCopyToAll.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbShiftSettings)
        Me.pnlMain.Controls.Add(Me.EZeeHeading1)
        Me.pnlMain.Controls.Add(Me.dgShiftDay)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.gbInfo)
        Me.pnlMain.Controls.Add(Me.gbRevAgreedScore)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(567, 523)
        Me.pnlMain.TabIndex = 0
        '
        'gbShiftSettings
        '
        Me.gbShiftSettings.BorderColor = System.Drawing.Color.Black
        Me.gbShiftSettings.Checked = False
        Me.gbShiftSettings.CollapseAllExceptThis = False
        Me.gbShiftSettings.CollapsedHoverImage = Nothing
        Me.gbShiftSettings.CollapsedNormalImage = Nothing
        Me.gbShiftSettings.CollapsedPressedImage = Nothing
        Me.gbShiftSettings.CollapseOnLoad = False
        Me.gbShiftSettings.Controls.Add(Me.rdCountOTAfterShiftEndTime)
        Me.gbShiftSettings.Controls.Add(Me.rdCountOTOnWorkedHrs)
        Me.gbShiftSettings.Controls.Add(Me.EZeeLine1)
        Me.gbShiftSettings.Controls.Add(Me.chkCountShiftTimingOnly)
        Me.gbShiftSettings.Controls.Add(Me.lblMaxHours)
        Me.gbShiftSettings.Controls.Add(Me.nudMaxhours)
        Me.gbShiftSettings.Controls.Add(Me.ChkSetAttOnDeviceStatus)
        Me.gbShiftSettings.Controls.Add(Me.chkIsOpenShift)
        Me.gbShiftSettings.ExpandedHoverImage = Nothing
        Me.gbShiftSettings.ExpandedNormalImage = Nothing
        Me.gbShiftSettings.ExpandedPressedImage = Nothing
        Me.gbShiftSettings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbShiftSettings.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbShiftSettings.HeaderHeight = 25
        Me.gbShiftSettings.HeaderMessage = ""
        Me.gbShiftSettings.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbShiftSettings.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbShiftSettings.HeightOnCollapse = 0
        Me.gbShiftSettings.LeftTextSpace = 0
        Me.gbShiftSettings.Location = New System.Drawing.Point(3, 341)
        Me.gbShiftSettings.Name = "gbShiftSettings"
        Me.gbShiftSettings.OpenHeight = 300
        Me.gbShiftSettings.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbShiftSettings.ShowBorder = True
        Me.gbShiftSettings.ShowCheckBox = False
        Me.gbShiftSettings.ShowCollapseButton = False
        Me.gbShiftSettings.ShowDefaultBorderColor = True
        Me.gbShiftSettings.ShowDownButton = False
        Me.gbShiftSettings.ShowHeader = True
        Me.gbShiftSettings.Size = New System.Drawing.Size(562, 124)
        Me.gbShiftSettings.TabIndex = 3
        Me.gbShiftSettings.Temp = 0
        Me.gbShiftSettings.Text = "Shift Settings"
        Me.gbShiftSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rdCountOTAfterShiftEndTime
        '
        Me.rdCountOTAfterShiftEndTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdCountOTAfterShiftEndTime.Location = New System.Drawing.Point(258, 98)
        Me.rdCountOTAfterShiftEndTime.Name = "rdCountOTAfterShiftEndTime"
        Me.rdCountOTAfterShiftEndTime.Size = New System.Drawing.Size(226, 17)
        Me.rdCountOTAfterShiftEndTime.TabIndex = 6
        Me.rdCountOTAfterShiftEndTime.Text = "Count Overtime After Shift End Time"
        Me.rdCountOTAfterShiftEndTime.UseVisualStyleBackColor = True
        '
        'rdCountOTOnWorkedHrs
        '
        Me.rdCountOTOnWorkedHrs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdCountOTOnWorkedHrs.Location = New System.Drawing.Point(19, 98)
        Me.rdCountOTOnWorkedHrs.Name = "rdCountOTOnWorkedHrs"
        Me.rdCountOTOnWorkedHrs.Size = New System.Drawing.Size(226, 17)
        Me.rdCountOTOnWorkedHrs.TabIndex = 7
        Me.rdCountOTOnWorkedHrs.Text = "Count Overtime On Total Worked Hrs "
        Me.rdCountOTOnWorkedHrs.UseVisualStyleBackColor = True
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(7, 79)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(516, 15)
        Me.EZeeLine1.TabIndex = 5
        Me.EZeeLine1.Text = "Overtime Settings"
        '
        'chkCountShiftTimingOnly
        '
        Me.chkCountShiftTimingOnly.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCountShiftTimingOnly.Location = New System.Drawing.Point(11, 55)
        Me.chkCountShiftTimingOnly.Name = "chkCountShiftTimingOnly"
        Me.chkCountShiftTimingOnly.Size = New System.Drawing.Size(539, 17)
        Me.chkCountShiftTimingOnly.TabIndex = 4
        Me.chkCountShiftTimingOnly.Text = "Count Shift Timings only and do not count Early or Late Arrival / Departure."
        Me.chkCountShiftTimingOnly.UseVisualStyleBackColor = True
        '
        'gbRevAgreedScore
        '
        Me.gbRevAgreedScore.Controls.Add(Me.chkNormalHrsHoliday)
        Me.gbRevAgreedScore.Controls.Add(Me.chkNormalHrsWeekend)
        Me.gbRevAgreedScore.Controls.Add(Me.chkNormalHrsDayOff)
        Me.gbRevAgreedScore.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.gbRevAgreedScore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRevAgreedScore.Location = New System.Drawing.Point(13, 499)
        Me.gbRevAgreedScore.Name = "gbRevAgreedScore"
        Me.gbRevAgreedScore.Size = New System.Drawing.Size(540, 40)
        Me.gbRevAgreedScore.TabIndex = 4
        Me.gbRevAgreedScore.TabStop = False
        Me.gbRevAgreedScore.Text = "Consider Working Hrs as Normal Hrs "
        Me.gbRevAgreedScore.Visible = False
        '
        'chkNormalHrsHoliday
        '
        Me.chkNormalHrsHoliday.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNormalHrsHoliday.Location = New System.Drawing.Point(208, 18)
        Me.chkNormalHrsHoliday.Name = "chkNormalHrsHoliday"
        Me.chkNormalHrsHoliday.Size = New System.Drawing.Size(119, 17)
        Me.chkNormalHrsHoliday.TabIndex = 47
        Me.chkNormalHrsHoliday.Text = "On Holiday "
        Me.chkNormalHrsHoliday.UseVisualStyleBackColor = True
        '
        'chkNormalHrsWeekend
        '
        Me.chkNormalHrsWeekend.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNormalHrsWeekend.Location = New System.Drawing.Point(51, 18)
        Me.chkNormalHrsWeekend.Name = "chkNormalHrsWeekend"
        Me.chkNormalHrsWeekend.Size = New System.Drawing.Size(119, 17)
        Me.chkNormalHrsWeekend.TabIndex = 45
        Me.chkNormalHrsWeekend.Text = "On Weekend"
        Me.chkNormalHrsWeekend.UseVisualStyleBackColor = True
        '
        'chkNormalHrsDayOff
        '
        Me.chkNormalHrsDayOff.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNormalHrsDayOff.Location = New System.Drawing.Point(365, 18)
        Me.chkNormalHrsDayOff.Name = "chkNormalHrsDayOff"
        Me.chkNormalHrsDayOff.Size = New System.Drawing.Size(119, 17)
        Me.chkNormalHrsDayOff.TabIndex = 0
        Me.chkNormalHrsDayOff.Text = "On Dayoff"
        Me.chkNormalHrsDayOff.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle13
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Day"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 125
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Working Hrs"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle14.Format = "N2"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle14
        Me.DataGridViewTextBoxColumn3.HeaderText = "Mark as Half Day From Hrs"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 160
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle15.Format = "N2"
        DataGridViewCellStyle15.NullValue = Nothing
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle15
        Me.DataGridViewTextBoxColumn4.HeaderText = "Mark as Half Day To Hrs"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 160
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Night From Hrs"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Night To Hrs"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        '
        'dgcolhDay
        '
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgcolhDay.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhDay.Frozen = True
        Me.dgcolhDay.HeaderText = "Day"
        Me.dgcolhDay.Name = "dgcolhDay"
        Me.dgcolhDay.ReadOnly = True
        Me.dgcolhDay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhDay.Width = 275
        '
        'dgcolhWeekend
        '
        Me.dgcolhWeekend.HeaderText = "Weekend"
        Me.dgcolhWeekend.Name = "dgcolhWeekend"
        Me.dgcolhWeekend.Width = 60
        '
        'dgcolhOTHoliday
        '
        Me.dgcolhOTHoliday.HeaderText = "OT Holiday"
        Me.dgcolhOTHoliday.Name = "dgcolhOTHoliday"
        Me.dgcolhOTHoliday.Width = 70
        '
        'dgcolhStarttime
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.NullValue = Nothing
        Me.dgcolhStarttime.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhStarttime.HeaderText = "Start Time"
        Me.dgcolhStarttime.Mask = ""
        Me.dgcolhStarttime.Name = "dgcolhStarttime"
        Me.dgcolhStarttime.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhStarttime.Width = 80
        '
        'dgcolhEndtime
        '
        DataGridViewCellStyle3.NullValue = Nothing
        Me.dgcolhEndtime.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhEndtime.HeaderText = "End Time"
        Me.dgcolhEndtime.Mask = ""
        Me.dgcolhEndtime.Name = "dgcolhEndtime"
        Me.dgcolhEndtime.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhEndtime.Width = 80
        '
        'dgcolhBreaktime
        '
        Me.dgcolhBreaktime.AllowNegative = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "F0"
        Me.dgcolhBreaktime.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhBreaktime.HeaderText = "Break Time (Mins)"
        Me.dgcolhBreaktime.Name = "dgcolhBreaktime"
        Me.dgcolhBreaktime.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhBreaktime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhWorkinghrs
        '
        Me.dgcolhWorkinghrs.AllowNegative = False
        Me.dgcolhWorkinghrs.DecimalLength = 2
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "F2"
        Me.dgcolhWorkinghrs.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgcolhWorkinghrs.HeaderText = "Working / Base Hrs"
        Me.dgcolhWorkinghrs.Name = "dgcolhWorkinghrs"
        Me.dgcolhWorkinghrs.ReadOnly = True
        Me.dgcolhWorkinghrs.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhWorkinghrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhWorkinghrs.Width = 125
        '
        'dgcolhOverTimeAfter
        '
        Me.dgcolhOverTimeAfter.AllowNegative = False
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N0"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.dgcolhOverTimeAfter.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgcolhOverTimeAfter.HeaderText = "Count OverTime After (Mins)"
        Me.dgcolhOverTimeAfter.Name = "dgcolhOverTimeAfter"
        Me.dgcolhOverTimeAfter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOverTimeAfter.Width = 175
        '
        'dgcolhOTGraceAfterShiftEndTime
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhOTGraceAfterShiftEndTime.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgcolhOTGraceAfterShiftEndTime.HeaderText = "OT Grace (Mins) after Shift End Time"
        Me.dgcolhOTGraceAfterShiftEndTime.Name = "dgcolhOTGraceAfterShiftEndTime"
        Me.dgcolhOTGraceAfterShiftEndTime.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhOTGraceAfterShiftEndTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOTGraceAfterShiftEndTime.Width = 195
        '
        'dgcolhShortimeBefore
        '
        Me.dgcolhShortimeBefore.AllowNegative = False
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N0"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.dgcolhShortimeBefore.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgcolhShortimeBefore.HeaderText = "Count ShortTime Before (Mins)"
        Me.dgcolhShortimeBefore.Name = "dgcolhShortimeBefore"
        Me.dgcolhShortimeBefore.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhShortimeBefore.Width = 175
        '
        'dgcolhHalffrmhrs
        '
        Me.dgcolhHalffrmhrs.AllowNegative = False
        Me.dgcolhHalffrmhrs.DecimalLength = 2
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        Me.dgcolhHalffrmhrs.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgcolhHalffrmhrs.HeaderText = "Mark as Half Day From Hrs"
        Me.dgcolhHalffrmhrs.Name = "dgcolhHalffrmhrs"
        Me.dgcolhHalffrmhrs.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhHalffrmhrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhHalffrmhrs.Width = 160
        '
        'dgcolhhalftohrs
        '
        Me.dgcolhhalftohrs.AllowNegative = False
        Me.dgcolhhalftohrs.DecimalLength = 2
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        Me.dgcolhhalftohrs.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgcolhhalftohrs.HeaderText = "Mark as Half Day To Hrs"
        Me.dgcolhhalftohrs.Name = "dgcolhhalftohrs"
        Me.dgcolhhalftohrs.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhhalftohrs.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhhalftohrs.Width = 160
        '
        'dgcolhnightfrmhrs
        '
        DataGridViewCellStyle11.NullValue = Nothing
        Me.dgcolhnightfrmhrs.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgcolhnightfrmhrs.HeaderText = "Night From Hrs"
        Me.dgcolhnightfrmhrs.Mask = ""
        Me.dgcolhnightfrmhrs.Name = "dgcolhnightfrmhrs"
        Me.dgcolhnightfrmhrs.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhnightfrmhrs.Width = 120
        '
        'dgcolhnighttohrs
        '
        DataGridViewCellStyle12.NullValue = Nothing
        Me.dgcolhnighttohrs.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgcolhnighttohrs.HeaderText = "Night To Hrs"
        Me.dgcolhnighttohrs.Mask = ""
        Me.dgcolhnighttohrs.Name = "dgcolhnighttohrs"
        Me.dgcolhnighttohrs.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhnighttohrs.Width = 120
        '
        'dgcolhDayStartTime
        '
        Me.dgcolhDayStartTime.HeaderText = "Day Start Time"
        Me.dgcolhDayStartTime.Mask = ""
        Me.dgcolhDayStartTime.Name = "dgcolhDayStartTime"
        Me.dgcolhDayStartTime.Width = 125
        '
        'frmNewShift_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(567, 523)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNewShift_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add Edit Shift"
        Me.gbInfo.ResumeLayout(False)
        Me.gbInfo.PerformLayout()
        CType(Me.nudMaxhours, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgShiftDay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeHeading1.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.gbShiftSettings.ResumeLayout(False)
        Me.gbRevAgreedScore.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents nudMaxhours As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMaxHours As System.Windows.Forms.Label
    Friend WithEvents chkIsOpenShift As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnAddShiftType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents cboShiftType As System.Windows.Forms.ComboBox
    Friend WithEvents lblShiftType As System.Windows.Forms.Label
    Friend WithEvents lblcode As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgShiftDay As System.Windows.Forms.DataGridView
    Friend WithEvents EZeeHeading1 As eZee.Common.eZeeHeading
    Friend WithEvents lnkCopyToAll As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents ChkSetAttOnDeviceStatus As System.Windows.Forms.CheckBox
    Friend WithEvents gbShiftSettings As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkNormalHrsHoliday As System.Windows.Forms.CheckBox
    Friend WithEvents chkNormalHrsDayOff As System.Windows.Forms.CheckBox
    Friend WithEvents chkNormalHrsWeekend As System.Windows.Forms.CheckBox
    Friend WithEvents gbRevAgreedScore As System.Windows.Forms.GroupBox
    Friend WithEvents chkCountShiftTimingOnly As System.Windows.Forms.CheckBox
    Friend WithEvents rdCountOTAfterShiftEndTime As System.Windows.Forms.RadioButton
    Friend WithEvents rdCountOTOnWorkedHrs As System.Windows.Forms.RadioButton
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents dgcolhDay As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhWeekend As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhOTHoliday As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhStarttime As eZee.Common.DataGridViewMaskTextBoxColumn
    Friend WithEvents dgcolhEndtime As eZee.Common.DataGridViewMaskTextBoxColumn
    Friend WithEvents dgcolhBreaktime As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhWorkinghrs As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhOverTimeAfter As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhOTGraceAfterShiftEndTime As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhShortimeBefore As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhHalffrmhrs As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhhalftohrs As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhnightfrmhrs As eZee.Common.DataGridViewMaskTextBoxColumn
    Friend WithEvents dgcolhnighttohrs As eZee.Common.DataGridViewMaskTextBoxColumn
    Friend WithEvents dgcolhDayStartTime As eZee.Common.DataGridViewMaskTextBoxColumn

End Class
