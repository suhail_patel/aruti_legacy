﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOT_Penalty
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOT_Penalty))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.LblPolicyValue = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgOTPenalty = New System.Windows.Forms.DataGridView
        Me.objdgcolhADD = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhUptoMins = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhOT1 = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhOT2 = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhOT3 = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhOT4 = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.objdgcolhPenaltyunkid = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.objdgcolhAUD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.gbInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        CType(Me.dgOTPenalty, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbInfo)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.dgOTPenalty)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(619, 348)
        Me.pnlMain.TabIndex = 0
        '
        'gbInfo
        '
        Me.gbInfo.BackColor = System.Drawing.Color.Transparent
        Me.gbInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInfo.Checked = False
        Me.gbInfo.CollapseAllExceptThis = False
        Me.gbInfo.CollapsedHoverImage = Nothing
        Me.gbInfo.CollapsedNormalImage = Nothing
        Me.gbInfo.CollapsedPressedImage = Nothing
        Me.gbInfo.CollapseOnLoad = False
        Me.gbInfo.Controls.Add(Me.LblPolicyValue)
        Me.gbInfo.Controls.Add(Me.lblName)
        Me.gbInfo.ExpandedHoverImage = Nothing
        Me.gbInfo.ExpandedNormalImage = Nothing
        Me.gbInfo.ExpandedPressedImage = Nothing
        Me.gbInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInfo.HeaderHeight = 25
        Me.gbInfo.HeaderMessage = ""
        Me.gbInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInfo.HeightOnCollapse = 0
        Me.gbInfo.LeftTextSpace = 0
        Me.gbInfo.Location = New System.Drawing.Point(13, 5)
        Me.gbInfo.Name = "gbInfo"
        Me.gbInfo.OpenHeight = 300
        Me.gbInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.gbInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInfo.ShowBorder = True
        Me.gbInfo.ShowCheckBox = False
        Me.gbInfo.ShowCollapseButton = False
        Me.gbInfo.ShowDefaultBorderColor = True
        Me.gbInfo.ShowDownButton = False
        Me.gbInfo.ShowHeader = True
        Me.gbInfo.Size = New System.Drawing.Size(594, 59)
        Me.gbInfo.TabIndex = 111
        Me.gbInfo.Temp = 0
        Me.gbInfo.Text = "Policy Information"
        Me.gbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblPolicyValue
        '
        Me.LblPolicyValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.LblPolicyValue.Location = New System.Drawing.Point(95, 33)
        Me.LblPolicyValue.Name = "LblPolicyValue"
        Me.LblPolicyValue.Size = New System.Drawing.Size(480, 14)
        Me.LblPolicyValue.TabIndex = 110
        Me.LblPolicyValue.Text = "#PolicyName"
        Me.LblPolicyValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblName.Location = New System.Drawing.Point(7, 33)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(69, 14)
        Me.lblName.TabIndex = 106
        Me.lblName.Text = "Policy Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objFooter.Location = New System.Drawing.Point(0, 293)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(619, 55)
        Me.objFooter.TabIndex = 109
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(424, 12)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 23
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(520, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 24
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgOTPenalty
        '
        Me.dgOTPenalty.AllowUserToAddRows = False
        Me.dgOTPenalty.AllowUserToDeleteRows = False
        Me.dgOTPenalty.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgOTPenalty.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgOTPenalty.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhADD, Me.objdgcolhDelete, Me.dgcolhUptoMins, Me.dgcolhOT1, Me.dgcolhOT2, Me.dgcolhOT3, Me.dgcolhOT4, Me.objdgcolhPenaltyunkid, Me.objdgcolhAUD, Me.objdgcolhGUID})
        Me.dgOTPenalty.Location = New System.Drawing.Point(13, 69)
        Me.dgOTPenalty.Name = "dgOTPenalty"
        Me.dgOTPenalty.RowHeadersVisible = False
        Me.dgOTPenalty.RowHeadersWidth = 20
        Me.dgOTPenalty.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgOTPenalty.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgOTPenalty.Size = New System.Drawing.Size(594, 215)
        Me.dgOTPenalty.TabIndex = 108
        '
        'objdgcolhADD
        '
        Me.objdgcolhADD.Frozen = True
        Me.objdgcolhADD.HeaderText = ""
        Me.objdgcolhADD.Image = Global.Aruti.Main.My.Resources.Resources.add_16
        Me.objdgcolhADD.Name = "objdgcolhADD"
        Me.objdgcolhADD.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.Width = 25
        '
        'dgcolhUptoMins
        '
        Me.dgcolhUptoMins.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.Format = "N0"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.dgcolhUptoMins.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhUptoMins.HeaderText = "Up To Mins"
        Me.dgcolhUptoMins.Name = "dgcolhUptoMins"
        Me.dgcolhUptoMins.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhUptoMins.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhUptoMins.Width = 68
        '
        'dgcolhOT1
        '
        Me.dgcolhOT1.AllowNegative = False
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.dgcolhOT1.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhOT1.HeaderText = "OT1 Deduction Mins"
        Me.dgcolhOT1.Name = "dgcolhOT1"
        Me.dgcolhOT1.ReadOnly = True
        Me.dgcolhOT1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOT1.Width = 115
        '
        'dgcolhOT2
        '
        Me.dgcolhOT2.AllowNegative = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.dgcolhOT2.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhOT2.HeaderText = "OT2 Deduction Mins"
        Me.dgcolhOT2.Name = "dgcolhOT2"
        Me.dgcolhOT2.ReadOnly = True
        Me.dgcolhOT2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOT2.Width = 115
        '
        'dgcolhOT3
        '
        Me.dgcolhOT3.AllowNegative = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.dgcolhOT3.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhOT3.HeaderText = "OT3 Deduction Mins"
        Me.dgcolhOT3.Name = "dgcolhOT3"
        Me.dgcolhOT3.ReadOnly = True
        Me.dgcolhOT3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOT3.Width = 115
        '
        'dgcolhOT4
        '
        Me.dgcolhOT4.AllowNegative = False
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N0"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.dgcolhOT4.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgcolhOT4.HeaderText = "OT4 Deduction Mins"
        Me.dgcolhOT4.Name = "dgcolhOT4"
        Me.dgcolhOT4.ReadOnly = True
        Me.dgcolhOT4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhOT4.Width = 115
        '
        'objdgcolhPenaltyunkid
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "F0"
        Me.objdgcolhPenaltyunkid.DefaultCellStyle = DataGridViewCellStyle6
        Me.objdgcolhPenaltyunkid.HeaderText = "Penaltyunkid"
        Me.objdgcolhPenaltyunkid.Name = "objdgcolhPenaltyunkid"
        Me.objdgcolhPenaltyunkid.Visible = False
        '
        'objdgcolhAUD
        '
        Me.objdgcolhAUD.HeaderText = "AUD"
        Me.objdgcolhAUD.Name = "objdgcolhAUD"
        Me.objdgcolhAUD.Visible = False
        '
        'objdgcolhGUID
        '
        Me.objdgcolhGUID.HeaderText = "GUID"
        Me.objdgcolhGUID.Name = "objdgcolhGUID"
        Me.objdgcolhGUID.ReadOnly = True
        Me.objdgcolhGUID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhGUID.Visible = False
        '
        'frmOT_Penalty
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(619, 348)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOT_Penalty"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "OT Penalty"
        Me.pnlMain.ResumeLayout(False)
        Me.gbInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgOTPenalty, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents dgOTPenalty As System.Windows.Forms.DataGridView
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents LblPolicyValue As System.Windows.Forms.Label
    Friend WithEvents gbInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objdgcolhADD As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhUptoMins As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhOT1 As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhOT2 As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhOT3 As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhOT4 As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents objdgcolhPenaltyunkid As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents objdgcolhAUD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
