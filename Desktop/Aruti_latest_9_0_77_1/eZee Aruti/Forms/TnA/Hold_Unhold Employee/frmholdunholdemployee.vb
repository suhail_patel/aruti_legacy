﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index=2

Public Class frmholdunholdemployee

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmholdunholdemployee"
    Private mblnCancel As Boolean = True
    Private objTimesheet As clslogin_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintholdUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal mdtLogindate As DateTime, ByVal intEmployeeid As Integer, ByVal intShiftid As Integer) As Boolean
        Try
            mintholdUnkid = intUnkId
            menAction = eAction
            GetEmployeedata(intEmployeeid, intShiftid)
            dtpFromDate.MinDate = mdtLogindate
            dtpFromDate.MaxDate = mdtLogindate
            Me.ShowDialog()
            intUnkId = mintholdUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Form's Event"

    Private Sub frmholdunholdemployee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objTimesheet = New clslogin_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call othersettings()
            'Anjan (02 Sep 2011)-End 


            objTimesheet._Holdunkid = mintholdUnkid
            dtpFromDate.Select()

            'Pinkal (17-Nov-2010) -- Start

            chkPostTimesheet.Visible = False

            'Pinkal (17-Nov-2010) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmholdunholdemployee_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmholdunholdemployee_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmholdunholdemployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmholdunholdemployee_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objTimesheet = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clslogin_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clslogin_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region "Button's Event"

    Private Sub btnhold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnhold.Click
        Dim blnFlag As Boolean = False
        Try
            SetValue()
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Are you sure you want to Hold Employee?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objTimesheet._FormName = mstrModuleName 
objTimesheet._LoginEmployeeUnkid = 0 
objTimesheet._ClientIP = getIP() 
objTimesheet._HostName = getHostName() 
objTimesheet._FromWeb = False
objTimesheet._AuditUserId = mintAuditUserId
objTimesheet._AuditDate = mdtAuditDate
'S.SANDEEP [28-May-2018] -- END

                blnFlag = objTimesheet.InsertHoldEmployee()

                If blnFlag = False And objTimesheet._Message <> "" Then
                    eZeeMsgBox.Show(objTimesheet._Message, enMsgBoxStyle.Information)
                End If
                If blnFlag Then
                    mblnCancel = False
                    If menAction = enAction.ADD_CONTINUE Then
                        objTimesheet = Nothing
                        objTimesheet = New clslogin_Tran
                        Call Getvalue()
                        dtpFromDate.Select()
                    Else
                        mintholdUnkid = objTimesheet._Holdunkid
                        Me.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnhold_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnUnhold_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnhold.Click
        Dim blnFlag As Boolean = False
        Try
            If objTimesheet._Holdunkid > 0 Then
                SetValue()
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to UnHold Employee?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'blnFlag = objTimesheet.UnHoldEmployee()

'S.SANDEEP [28-May-2018] -- START
'ISSUE/ENHANCEMENT : {Audit Trails} 
objTimesheet._FormName = mstrModuleName 
objTimesheet._LoginEmployeeUnkid = 0 
objTimesheet._ClientIP = getIP() 
objTimesheet._HostName = getHostName() 
objTimesheet._FromWeb = False
objTimesheet._AuditUserId = mintAuditUserId
objTimesheet._AuditDate = mdtAuditDate
'S.SANDEEP [28-May-2018] -- END

                    blnFlag = objTimesheet.UnHoldEmployee(ConfigParameter._Object._DonotAttendanceinSeconds)
                    'S.SANDEEP [04 JUN 2015] -- END
                    If blnFlag = False And objTimesheet._Message <> "" Then
                        eZeeMsgBox.Show(objTimesheet._Message, enMsgBoxStyle.Information)
                    End If
                    If blnFlag Then
                        mblnCancel = False
                        If menAction = enAction.ADD_CONTINUE Then
                            objTimesheet = Nothing
                            objTimesheet = New clslogin_Tran
                            Call Getvalue()
                            dtpFromDate.Select()
                        Else
                            mintholdUnkid = objTimesheet._Holdunkid
                            Me.Close()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnUnhold_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub Getvalue()
        Try
            dtpFromDate.Value = objTimesheet._Fromdate
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Getvalue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objTimesheet._Employeeunkid = CInt(objEmployee.Tag)
            objTimesheet._Fromdate = dtpFromDate.Value.Date
            objTimesheet._Todate = dtpFromDate.Value.Date
            If chkPostTimesheet.Visible Then
                objTimesheet._IsPost = chkPostTimesheet.Checked
            Else
                objTimesheet._IsPost = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetEmployeedata(ByVal intEmployeeid As Integer, ByVal intShiftid As Integer)
        Try
            'FOR EMPLOYEE
            Dim objEmployeeMaster As New clsEmployee_Master
            objEmployeeMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmployeeid
            objEmployee.Tag = intEmployeeid

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployeeMaster.GetData()
            'S.SANDEEP [04 JUN 2015] -- END

            objEmployee.Text = objEmployeeMaster._Firstname + " " + objEmployeeMaster._Surname

            'FOR SHIFT

            'Pinkal (03-Jul-2013) -- Start
            'Enhancement : TRA Changes
            'Dim objShiftMaster As New clsshift_master
            Dim objShiftMaster As New clsNewshift_master
            'Pinkal (03-Jul-2013) -- End

            objShiftMaster._Shiftunkid = intShiftid
            objShift.Tag = intShiftid
            objShiftMaster.GetData()
            objShift.Text = objShiftMaster._Shiftname

            'FOR DEPARTMENT
            Dim objDepartmentMaster As New clsDepartment

            'Pinkal (29-Aug-2012) -- Start
            'Enhancement : TRA Changes
            'objDepartmentMaster._Departmentunkid = intDeparmentid
            'objDepartment.Tag = intDeparmentid
            objDepartmentMaster._Departmentunkid = objEmployeeMaster._Departmentunkid
            objDepartment.Tag = objEmployeeMaster._Departmentunkid

            'objDepartmentMaster.GetData()

            'Pinkal (29-Aug-2012) -- End
            objDepartment.Text = objDepartmentMaster._Name
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeedata", mstrModuleName)
        End Try
    End Sub

#End Region


   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnhold.GradientBackColor = GUI._ButttonBackColor 
			Me.btnhold.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnUnhold.GradientBackColor = GUI._ButttonBackColor 
			Me.btnUnhold.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnhold.Text = Language._Object.getCaption(Me.btnhold.Name, Me.btnhold.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
			Me.elDateInfo.Text = Language._Object.getCaption(Me.elDateInfo.Name, Me.elDateInfo.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.elAssignInfo.Text = Language._Object.getCaption(Me.elAssignInfo.Name, Me.elAssignInfo.Text)
			Me.lblshift.Text = Language._Object.getCaption(Me.lblshift.Name, Me.lblshift.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.btnUnhold.Text = Language._Object.getCaption(Me.btnUnhold.Name, Me.btnUnhold.Text)
			Me.chkPostTimesheet.Text = Language._Object.getCaption(Me.chkPostTimesheet.Name, Me.chkPostTimesheet.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Are you sure you want to Hold Employee?")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to UnHold Employee?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class