﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditTimesheet
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditTimesheet))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlTimesheet = New System.Windows.Forms.Panel
        Me.lblTotalHours = New System.Windows.Forms.Label
        Me.objTotalHours = New System.Windows.Forms.Label
        Me.objBreak = New System.Windows.Forms.Label
        Me.lblBreak = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgvTimeSheet = New System.Windows.Forms.DataGridView
        Me.dgcolhLoginDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStarttime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEndtime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhWorkingHours = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhLoginunkid = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.objcolhInTime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objColhOuttime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbEmployeeInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.LblEmpCodeValue = New System.Windows.Forms.Label
        Me.LblEmpCode = New System.Windows.Forms.Label
        Me.LblPolicyValue = New System.Windows.Forms.Label
        Me.LblPolicy = New System.Windows.Forms.Label
        Me.LblShiftValue = New System.Windows.Forms.Label
        Me.LblShift = New System.Windows.Forms.Label
        Me.LblEmpValue = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objTeaBreak = New System.Windows.Forms.Label
        Me.LblTeaBreak = New System.Windows.Forms.Label
        Me.pnlTimesheet.SuspendLayout()
        Me.objFooter.SuspendLayout()
        CType(Me.dgvTimeSheet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEmployeeInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTimesheet
        '
        Me.pnlTimesheet.Controls.Add(Me.objTeaBreak)
        Me.pnlTimesheet.Controls.Add(Me.LblTeaBreak)
        Me.pnlTimesheet.Controls.Add(Me.lblTotalHours)
        Me.pnlTimesheet.Controls.Add(Me.objTotalHours)
        Me.pnlTimesheet.Controls.Add(Me.objBreak)
        Me.pnlTimesheet.Controls.Add(Me.lblBreak)
        Me.pnlTimesheet.Controls.Add(Me.objFooter)
        Me.pnlTimesheet.Controls.Add(Me.dgvTimeSheet)
        Me.pnlTimesheet.Controls.Add(Me.gbEmployeeInfo)
        Me.pnlTimesheet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTimesheet.Location = New System.Drawing.Point(0, 0)
        Me.pnlTimesheet.Name = "pnlTimesheet"
        Me.pnlTimesheet.Size = New System.Drawing.Size(532, 441)
        Me.pnlTimesheet.TabIndex = 0
        '
        'lblTotalHours
        '
        Me.lblTotalHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblTotalHours.Location = New System.Drawing.Point(411, 368)
        Me.lblTotalHours.Name = "lblTotalHours"
        Me.lblTotalHours.Size = New System.Drawing.Size(49, 13)
        Me.lblTotalHours.TabIndex = 8
        Me.lblTotalHours.Text = "Hours"
        Me.lblTotalHours.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotalHours
        '
        Me.objTotalHours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.objTotalHours.Location = New System.Drawing.Point(468, 368)
        Me.objTotalHours.Name = "objTotalHours"
        Me.objTotalHours.Size = New System.Drawing.Size(38, 13)
        Me.objTotalHours.TabIndex = 9
        Me.objTotalHours.Text = "0"
        Me.objTotalHours.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objBreak
        '
        Me.objBreak.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.objBreak.Location = New System.Drawing.Point(343, 368)
        Me.objBreak.Name = "objBreak"
        Me.objBreak.Size = New System.Drawing.Size(64, 13)
        Me.objBreak.TabIndex = 7
        Me.objBreak.Text = "0"
        Me.objBreak.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBreak
        '
        Me.lblBreak.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.lblBreak.Location = New System.Drawing.Point(289, 368)
        Me.lblBreak.Name = "lblBreak"
        Me.lblBreak.Size = New System.Drawing.Size(46, 13)
        Me.lblBreak.TabIndex = 6
        Me.lblBreak.Text = "Break"
        Me.lblBreak.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 391)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(532, 50)
        Me.objFooter.TabIndex = 3
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(431, 8)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 12
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(335, 8)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'dgvTimeSheet
        '
        Me.dgvTimeSheet.AllowUserToAddRows = False
        Me.dgvTimeSheet.AllowUserToDeleteRows = False
        Me.dgvTimeSheet.AllowUserToResizeColumns = False
        Me.dgvTimeSheet.AllowUserToResizeRows = False
        Me.dgvTimeSheet.BackgroundColor = System.Drawing.Color.White
        Me.dgvTimeSheet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTimeSheet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhLoginDate, Me.dgcolhStarttime, Me.dgcolhEndtime, Me.dgcolhWorkingHours, Me.objcolhLoginunkid, Me.objcolhInTime, Me.objColhOuttime})
        Me.dgvTimeSheet.Location = New System.Drawing.Point(11, 149)
        Me.dgvTimeSheet.Name = "dgvTimeSheet"
        Me.dgvTimeSheet.RowHeadersVisible = False
        Me.dgvTimeSheet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvTimeSheet.Size = New System.Drawing.Size(509, 207)
        Me.dgvTimeSheet.TabIndex = 7
        '
        'dgcolhLoginDate
        '
        Me.dgcolhLoginDate.HeaderText = "Login Date"
        Me.dgcolhLoginDate.Name = "dgcolhLoginDate"
        Me.dgcolhLoginDate.ReadOnly = True
        Me.dgcolhLoginDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhLoginDate.Width = 110
        '
        'dgcolhStarttime
        '
        Me.dgcolhStarttime.HeaderText = "Check In"
        Me.dgcolhStarttime.Name = "dgcolhStarttime"
        Me.dgcolhStarttime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhStarttime.Width = 130
        '
        'dgcolhEndtime
        '
        Me.dgcolhEndtime.HeaderText = "Check Out"
        Me.dgcolhEndtime.Name = "dgcolhEndtime"
        Me.dgcolhEndtime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEndtime.Width = 130
        '
        'dgcolhWorkingHours
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhWorkingHours.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhWorkingHours.HeaderText = "Hours"
        Me.dgcolhWorkingHours.Name = "dgcolhWorkingHours"
        Me.dgcolhWorkingHours.ReadOnly = True
        Me.dgcolhWorkingHours.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhWorkingHours.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhWorkingHours.Width = 130
        '
        'objcolhLoginunkid
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "F0"
        Me.objcolhLoginunkid.DefaultCellStyle = DataGridViewCellStyle2
        Me.objcolhLoginunkid.HeaderText = "Loginunkid"
        Me.objcolhLoginunkid.Name = "objcolhLoginunkid"
        Me.objcolhLoginunkid.ReadOnly = True
        Me.objcolhLoginunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhLoginunkid.Visible = False
        '
        'objcolhInTime
        '
        Me.objcolhInTime.HeaderText = "InTime"
        Me.objcolhInTime.Name = "objcolhInTime"
        Me.objcolhInTime.ReadOnly = True
        Me.objcolhInTime.Visible = False
        '
        'objColhOuttime
        '
        Me.objColhOuttime.HeaderText = "Out Time"
        Me.objColhOuttime.Name = "objColhOuttime"
        Me.objColhOuttime.ReadOnly = True
        Me.objColhOuttime.Visible = False
        '
        'gbEmployeeInfo
        '
        Me.gbEmployeeInfo.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.Checked = False
        Me.gbEmployeeInfo.CollapseAllExceptThis = False
        Me.gbEmployeeInfo.CollapsedHoverImage = Nothing
        Me.gbEmployeeInfo.CollapsedNormalImage = Nothing
        Me.gbEmployeeInfo.CollapsedPressedImage = Nothing
        Me.gbEmployeeInfo.CollapseOnLoad = False
        Me.gbEmployeeInfo.Controls.Add(Me.LblEmpCodeValue)
        Me.gbEmployeeInfo.Controls.Add(Me.LblEmpCode)
        Me.gbEmployeeInfo.Controls.Add(Me.LblPolicyValue)
        Me.gbEmployeeInfo.Controls.Add(Me.LblPolicy)
        Me.gbEmployeeInfo.Controls.Add(Me.LblShiftValue)
        Me.gbEmployeeInfo.Controls.Add(Me.LblShift)
        Me.gbEmployeeInfo.Controls.Add(Me.LblEmpValue)
        Me.gbEmployeeInfo.Controls.Add(Me.lblEmployee)
        Me.gbEmployeeInfo.ExpandedHoverImage = Nothing
        Me.gbEmployeeInfo.ExpandedNormalImage = Nothing
        Me.gbEmployeeInfo.ExpandedPressedImage = Nothing
        Me.gbEmployeeInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeInfo.HeaderHeight = 25
        Me.gbEmployeeInfo.HeaderMessage = ""
        Me.gbEmployeeInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeInfo.HeightOnCollapse = 0
        Me.gbEmployeeInfo.LeftTextSpace = 0
        Me.gbEmployeeInfo.Location = New System.Drawing.Point(11, 8)
        Me.gbEmployeeInfo.Name = "gbEmployeeInfo"
        Me.gbEmployeeInfo.OpenHeight = 300
        Me.gbEmployeeInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeInfo.ShowBorder = True
        Me.gbEmployeeInfo.ShowCheckBox = False
        Me.gbEmployeeInfo.ShowCollapseButton = False
        Me.gbEmployeeInfo.ShowDefaultBorderColor = True
        Me.gbEmployeeInfo.ShowDownButton = False
        Me.gbEmployeeInfo.ShowHeader = True
        Me.gbEmployeeInfo.Size = New System.Drawing.Size(509, 132)
        Me.gbEmployeeInfo.TabIndex = 1
        Me.gbEmployeeInfo.Temp = 0
        Me.gbEmployeeInfo.Text = "Employee Information"
        Me.gbEmployeeInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblEmpCodeValue
        '
        Me.LblEmpCodeValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmpCodeValue.Location = New System.Drawing.Point(99, 32)
        Me.LblEmpCodeValue.Name = "LblEmpCodeValue"
        Me.LblEmpCodeValue.Size = New System.Drawing.Size(401, 17)
        Me.LblEmpCodeValue.TabIndex = 116
        Me.LblEmpCodeValue.Text = "#Employee"
        Me.LblEmpCodeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblEmpCode
        '
        Me.LblEmpCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmpCode.Location = New System.Drawing.Point(7, 32)
        Me.LblEmpCode.Name = "LblEmpCode"
        Me.LblEmpCode.Size = New System.Drawing.Size(84, 17)
        Me.LblEmpCode.TabIndex = 115
        Me.LblEmpCode.Text = "Employee Code"
        Me.LblEmpCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblPolicyValue
        '
        Me.LblPolicyValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPolicyValue.Location = New System.Drawing.Point(99, 106)
        Me.LblPolicyValue.Name = "LblPolicyValue"
        Me.LblPolicyValue.Size = New System.Drawing.Size(401, 17)
        Me.LblPolicyValue.TabIndex = 113
        Me.LblPolicyValue.Text = "#Policy"
        Me.LblPolicyValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblPolicy
        '
        Me.LblPolicy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPolicy.Location = New System.Drawing.Point(7, 106)
        Me.LblPolicy.Name = "LblPolicy"
        Me.LblPolicy.Size = New System.Drawing.Size(84, 17)
        Me.LblPolicy.TabIndex = 112
        Me.LblPolicy.Text = "Policy"
        Me.LblPolicy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblShiftValue
        '
        Me.LblShiftValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblShiftValue.Location = New System.Drawing.Point(99, 82)
        Me.LblShiftValue.Name = "LblShiftValue"
        Me.LblShiftValue.Size = New System.Drawing.Size(401, 17)
        Me.LblShiftValue.TabIndex = 111
        Me.LblShiftValue.Text = "#Shift"
        Me.LblShiftValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblShift
        '
        Me.LblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblShift.Location = New System.Drawing.Point(7, 82)
        Me.LblShift.Name = "LblShift"
        Me.LblShift.Size = New System.Drawing.Size(84, 17)
        Me.LblShift.TabIndex = 110
        Me.LblShift.Text = "Shift"
        Me.LblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblEmpValue
        '
        Me.LblEmpValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmpValue.Location = New System.Drawing.Point(99, 56)
        Me.LblEmpValue.Name = "LblEmpValue"
        Me.LblEmpValue.Size = New System.Drawing.Size(401, 17)
        Me.LblEmpValue.TabIndex = 109
        Me.LblEmpValue.Text = "#Employee"
        Me.LblEmpValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(7, 56)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(84, 17)
        Me.lblEmployee.TabIndex = 108
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Check In"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 200
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Break In"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Visible = False
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn3.HeaderText = "Break Out"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn4.HeaderText = "Check Out"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn4.Width = 155
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "Hours"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'objTeaBreak
        '
        Me.objTeaBreak.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.objTeaBreak.Location = New System.Drawing.Point(219, 368)
        Me.objTeaBreak.Name = "objTeaBreak"
        Me.objTeaBreak.Size = New System.Drawing.Size(64, 13)
        Me.objTeaBreak.TabIndex = 13
        Me.objTeaBreak.Text = "0"
        Me.objTeaBreak.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblTeaBreak
        '
        Me.LblTeaBreak.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(162, Byte))
        Me.LblTeaBreak.Location = New System.Drawing.Point(145, 368)
        Me.LblTeaBreak.Name = "LblTeaBreak"
        Me.LblTeaBreak.Size = New System.Drawing.Size(69, 13)
        Me.LblTeaBreak.TabIndex = 12
        Me.LblTeaBreak.Text = "Tea Break"
        Me.LblTeaBreak.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmEditTimesheet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(532, 441)
        Me.Controls.Add(Me.pnlTimesheet)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditTimesheet"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Edit Timesheet"
        Me.pnlTimesheet.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        CType(Me.dgvTimeSheet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEmployeeInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTimesheet As System.Windows.Forms.Panel
    Friend WithEvents gbEmployeeInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents lblTotalHours As System.Windows.Forms.Label
    Friend WithEvents objTotalHours As System.Windows.Forms.Label
    Friend WithEvents objBreak As System.Windows.Forms.Label
    Friend WithEvents lblBreak As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvTimeSheet As System.Windows.Forms.DataGridView
    Friend WithEvents LblEmpValue As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents LblPolicyValue As System.Windows.Forms.Label
    Friend WithEvents LblPolicy As System.Windows.Forms.Label
    Friend WithEvents LblShiftValue As System.Windows.Forms.Label
    Friend WithEvents LblShift As System.Windows.Forms.Label
    Friend WithEvents LblEmpCodeValue As System.Windows.Forms.Label
    Friend WithEvents LblEmpCode As System.Windows.Forms.Label
    Friend WithEvents dgcolhLoginDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStarttime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEndtime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhWorkingHours As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhLoginunkid As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents objcolhInTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objColhOuttime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objTeaBreak As System.Windows.Forms.Label
    Friend WithEvents LblTeaBreak As System.Windows.Forms.Label
End Class
