﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmTimesheetDetail

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmTimesheetDetail"
    Private mblnCancel As Boolean = True
    Private objTimeSheet As clslogin_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintTimesheetUnkid As Integer = -1
    Private mstrAdvanceFilter As String = ""
    Private objlogintran As clslogin_Tran
    Private mintMouseX As Integer = 0
    Private mintMouseY As Integer = 0

#End Region

#Region " Display Dialog "

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), -1, "", False, mstrAdvanceFilter)

            'If mstrAdvanceFilter.ToString().Trim.Length > 0 Then
            '    mstrAdvanceFilter = "hremployee_master." & mstrAdvanceFilter
            'End If

            Dim objEmployee As New clsEmployee_Master

            
            Dim dsList As DataSet = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             False, _
                                             "Employee", _
                                             ConfigParameter._Object._ShowFirstAppointmentDate, , , mstrAdvanceFilter)
            'S.SANDEEP [04 JUN 2015] -- END


            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("employeeunkid") = 0
            drRow("name") = Language.getMessage(mstrModuleName, 1, "Select")
            dsList.Tables(0).Rows.InsertAt(drRow, 0)

            cboEmployee.DisplayMember = "name"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = dsList.Tables(0)
            cboEmployee.SelectedValue = 0


            'Pinkal (15-Nov-2013) -- Start
            'Enhancement : Oman Changes

            Dim objShift As New clsNewshift_master
            dsList = objShift.getListForCombo("Shift", True)
            cboShift.DisplayMember = "name"
            cboShift.ValueMember = "shiftunkid"
            cboShift.DataSource = dsList.Tables(0)
            cboShift.SelectedValue = 0

            Dim objPolicy As New clspolicy_master
            dsList = objPolicy.getListForCombo("Shift", True)
            CboPolicy.DisplayMember = "name"
            CboPolicy.ValueMember = "policyunkid"
            CboPolicy.DataSource = dsList.Tables(0)
            CboPolicy.SelectedValue = 0

            'Pinkal (15-Nov-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Try
            dgTimeSheet.AutoGenerateColumns = False

            'Pinkal (15-Nov-2013) -- Start
            'Enhancement : Oman Changes
            'Dim dtEmployeeData As DataTable = objlogintran.GetEmployeeLoginData(CInt(cboEmployee.SelectedValue), dtpFromDate.Value.Date, dtpToDate.Value.Date)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dtEmployeeData As DataTable = objlogintran.GetEmployeeLoginData(CInt(cboEmployee.SelectedValue), dtpFromDate.Value.Date, dtpToDate.Value.Date, CInt(cboShift.SelectedValue), CInt(CboPolicy.SelectedValue))
            Dim dtEmployeeData As DataTable = objlogintran.GetEmployeeLoginData(CInt(cboEmployee.SelectedValue), dtpFromDate.Value.Date, dtpToDate.Value.Date, CInt(cboShift.SelectedValue), CInt(CboPolicy.SelectedValue), ConfigParameter._Object._PolicyManagementTNA)
            'S.SANDEEP [04 JUN 2015] -- END
            'Pinkal (15-Nov-2013) -- End
            dgcolhDate.DataPropertyName = "logindate"
            dgcolhShift.DataPropertyName = "Shift"
            dgcolhPolicy.DataPropertyName = "Policy"
            dgColhIntime.DataPropertyName = "checkintime"
            dgColhoutTime.DataPropertyName = "checkouttime"
            dgcolhWorkhrs.DataPropertyName = "WorkingHours"
            dgcolhBreakhrs.DataPropertyName = "BreakHour"
            dgcolhBaseHrs.DataPropertyName = "BaseHrs"

            If ConfigParameter._Object._PolicyManagementTNA Then
            dgcolhOT1.DataPropertyName = "OT1"
            Else
                dgcolhTotalOverTime.DataPropertyName = "OT1"
                dgcolhTotalShortHrs.DataPropertyName = "TotalShortHours"
            End If

            dgcolhOT2.DataPropertyName = "OT2"
            dgColhOT3.DataPropertyName = "OT3"
            dgColhOT4.DataPropertyName = "OT4"
            dgcolhInException.DataPropertyName = "InException"
            dgcolhOutException.DataPropertyName = "OutException"
            objcolhLoginunkId.DataPropertyName = "loginunkid"
            objcolhEmployeeID.DataPropertyName = "employeeunkid"
            objcolhShiftId.DataPropertyName = "shiftunkid"
            objcolhPolicyId.DataPropertyName = "policyunkid"
            dgTimeSheet.DataSource = dtEmployeeData
            SetFormat()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetFormat()
        Try
            Dim dtEmployeeData As DataTable = CType(dgTimeSheet.DataSource, DataTable)

            For i As Integer = 0 To dtEmployeeData.Rows.Count - 1

                If Not IsDBNull(dtEmployeeData.Rows(i)("logindate")) Then
                    'dtEmployeeData.Rows(i)("logindate") = CDate(dtEmployeeData.Rows(i)("logindate")).ToString("dd/MMM/yyyy") & " , " & WeekdayName(Weekday(CDate(dtEmployeeData.Rows(i)("logindate"))), True).ToString()
                    dtEmployeeData.Rows(i)("logindate") = CDate(dtEmployeeData.Rows(i)("logindate")).ToShortDateString & " , " & WeekdayName(Weekday(CDate(dtEmployeeData.Rows(i)("logindate"))), True, FirstDayOfWeek.Sunday).ToString()
                End If

                If Not IsDBNull(dtEmployeeData.Rows(i)("checkintime")) Then
                    'dtEmployeeData.Rows(i)("checkintime") ==CDate(dtEmployeeData.Rows(i)("checkintime")).ToString("hh:mm tt")
                    dtEmployeeData.Rows(i)("checkintime") = CDate(dtEmployeeData.Rows(i)("checkintime")).ToShortDateString & "  " & CDate(dtEmployeeData.Rows(i)("checkintime")).ToString("hh:mm tt")
                End If

                If Not IsDBNull(dtEmployeeData.Rows(i)("checkouttime")) Then
                    'dtEmployeeData.Rows(i)("checkouttime") = CDate(dtEmployeeData.Rows(i)("checkouttime")).ToString("hh:mm tt")
                    dtEmployeeData.Rows(i)("checkouttime") = CDate(dtEmployeeData.Rows(i)("checkouttime")).ToShortDateString & " " & CDate(dtEmployeeData.Rows(i)("checkouttime")).ToString("hh:mm tt")
                End If


                'If Not IsDBNull(dtEmployeeData.Rows(i)("BaseHrs")) Then
                '    Dim objPolicyTran As New clsPolicy_tran
                '    objPolicyTran.GetPolicyTran(CInt(dtEmployeeData.Rows(i)("policyunkid")), GetWeekDayNumber(WeekdayName(Weekday(CDate(dtEmployeeData.Rows(i)("logindate")).Date)).ToString()))
                '    Dim mdtTable As DataTable = objPolicyTran._DataList
                '    If objPolicyTran._DataList.Rows.Count > 0 Then
                '        dtEmployeeData.Rows(i)("BaseHrs") = CDec(CalculateTime(True, CInt(objPolicyTran._DataList.Rows(0)("basehoursinsec")))).ToString("#0.00")
                '    Else
                '        dtEmployeeData.Rows(i)("BaseHrs") = ""
                '    End If
                'End If

                If Not IsDBNull(dtEmployeeData.Rows(i)("BaseHrs")) Then
                    Dim objShiftTran As New clsshift_tran
                    objShiftTran.GetShiftTran(CInt(dtEmployeeData.Rows(i)("shiftunkid")))
                    Dim mdtTable As DataTable = objShiftTran._dtShiftday

                    'Pinkal (17-Sep-2014) -- Start
                    'Enhancement - VOLTAMP WEEKDAY NAME ISSUE WHEN FIRST DAY OF WEEK IS NOT SUNDAY.
                    'Dim drRow As DataRow() = mdtTable.Select("dayid =  " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dtEmployeeData.Rows(i)("logindate")).Date)).ToString()))
                    Dim drRow As DataRow() = mdtTable.Select("dayid =  " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dtEmployeeData.Rows(i)("logindate")).Date), False, FirstDayOfWeek.Sunday).ToString()))
                    'Pinkal (17-Sep-2014) -- End
                    If drRow.Length > 0 Then

                        If ConfigParameter._Object._PolicyManagementTNA Then
                            Dim objPolicyTran As New clsPolicy_tran
                            objPolicyTran.GetPolicyTran(CInt(dtEmployeeData.Rows(i)("policyunkid")))
                            Dim mdtPolicy As DataTable = objPolicyTran._DataList
                            'Pinkal (17-Sep-2014) -- Start
                            'Enhancement - VOLTAMP WEEKDAY NAME ISSUE WHEN FIRST DAY OF WEEK IS NOT SUNDAY.
                            'Dim drPolicy As DataRow() = mdtPolicy.Select("dayid =  " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dtEmployeeData.Rows(i)("logindate")).Date)).ToString()))
                            Dim drPolicy As DataRow() = mdtPolicy.Select("dayid =  " & GetWeekDayNumber(WeekdayName(Weekday(CDate(dtEmployeeData.Rows(i)("logindate")).Date), False, FirstDayOfWeek.Sunday).ToString()))
                            'Pinkal (17-Sep-2014) -- End
                            If drPolicy.Length > 0 Then
                                Dim mintActualBaseHr As Integer = CInt(drRow(0)("workinghrsinsec")) - CInt(drPolicy(0)("breaktimeinsec"))
                                dtEmployeeData.Rows(i)("BaseHrs") = CDec(CalculateTime(True, mintActualBaseHr)).ToString("#0.00")
                            Else
                                dtEmployeeData.Rows(i)("BaseHrs") = CDec(CalculateTime(True, CInt(drRow(0)("workinghrsinsec")))).ToString("#0.00")
                            End If
                        Else
                        dtEmployeeData.Rows(i)("BaseHrs") = CDec(CalculateTime(True, CInt(drRow(0)("workinghrsinsec")))).ToString("#0.00")
                        End If

                    Else
                        dtEmployeeData.Rows(i)("BaseHrs") = ""
                    End If

                End If


                If Not IsDBNull(dtEmployeeData.Rows(i)("WorkingHours")) Then
                    dtEmployeeData.Rows(i)("WorkingHours") = CDec(dtEmployeeData.Rows(i)("WorkingHours")).ToString("#0.00")
                End If

                'If Not IsDBNull(dtEmployeeData.Rows(i)("TotalHrs")) Then
                '    dtEmployeeData.Rows(i)("TotalHrs") = CDec(dtEmployeeData.Rows(i)("TotalHrs")).ToString("#0.00")
                'End If


                'Pinkal (15-Oct-2013) -- Start
                'Enhancement : TRA Changes

                'If Not IsDBNull(dtEmployeeData.Rows(i)("BreakHour")) Then
                'dtEmployeeData.Rows(i)("BreakHour") = CDec(dtEmployeeData.Rows(i)("BreakHour")).ToString("#0.00")
                'End If

                If Not IsDBNull(dtEmployeeData.Rows(i)("BreakHour")) Then
                    dtEmployeeData.Rows(i)("BreakHour") = CDec(CalculateTime(True, CInt(dtEmployeeData.Rows(i)("BreakHr")))).ToString("#0.00")
                End If
                'Pinkal (15-Oct-2013) -- End


                If ConfigParameter._Object._PolicyManagementTNA = False Then
                    If Not IsDBNull(dtEmployeeData.Rows(i)("TotalShortHours")) Then
                        dtEmployeeData.Rows(i)("TotalShortHours") = CDec(dtEmployeeData.Rows(i)("TotalShortHours")).ToString("#0.00")
                    End If
                End If


                If Not IsDBNull(dtEmployeeData.Rows(i)("OT1")) Then
                    dtEmployeeData.Rows(i)("OT1") = CDec(dtEmployeeData.Rows(i)("OT1")).ToString("#0.00")
                End If

                If Not IsDBNull(dtEmployeeData.Rows(i)("OT2")) Then
                    dtEmployeeData.Rows(i)("OT2") = CDec(dtEmployeeData.Rows(i)("OT2")).ToString("#0.00")
                End If

                If Not IsDBNull(dtEmployeeData.Rows(i)("OT3")) Then
                    dtEmployeeData.Rows(i)("OT3") = CDec(dtEmployeeData.Rows(i)("OT3")).ToString("#0.00")
                End If

                If Not IsDBNull(dtEmployeeData.Rows(i)("OT4")) Then
                    dtEmployeeData.Rows(i)("OT4") = CDec(dtEmployeeData.Rows(i)("OT4")).ToString("#0.00")
                End If

                If IsDBNull(dtEmployeeData.Rows(i)("elrcoming_grace")) = False AndAlso CDec(dtEmployeeData.Rows(i)("elrcoming_grace")) > 0 Then
                    dtEmployeeData.Rows(i)("InException") = "B " & CDec(dtEmployeeData.Rows(i)("elrcoming_grace")).ToString("#0.00")
                End If

                If IsDBNull(dtEmployeeData.Rows(i)("ltcoming_grace")) = False AndAlso CDec(dtEmployeeData.Rows(i)("ltcoming_grace")) > 0 Then
                    dtEmployeeData.Rows(i)("InException") = "L " & CDec(dtEmployeeData.Rows(i)("ltcoming_grace")).ToString("#0.00")
                End If

                If IsDBNull(dtEmployeeData.Rows(i)("elrgoing_grace")) = False AndAlso CDec(dtEmployeeData.Rows(i)("elrgoing_grace")) > 0 Then
                    dtEmployeeData.Rows(i)("OutException") = "B " & CDec(dtEmployeeData.Rows(i)("elrgoing_grace")).ToString("#0.00")
                End If

                If IsDBNull(dtEmployeeData.Rows(i)("ltgoing_grace")) = False AndAlso CDec(dtEmployeeData.Rows(i)("ltgoing_grace")) > 0 Then
                    dtEmployeeData.Rows(i)("OutException") = "L " & CDec(dtEmployeeData.Rows(i)("ltgoing_grace")).ToString("#0.00")
                End If

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFormat", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisiblity()
        Try
            mnuRoundingoff.Visible = ConfigParameter._Object._PolicyManagementTNA

            'Pinkal (30-Oct-2013) -- Start
            'Enhancement : Oman Changes 
            dgcolhPolicy.Visible = ConfigParameter._Object._PolicyManagementTNA
            dgcolhBaseHrs.Visible = ConfigParameter._Object._PolicyManagementTNA
            dgcolhOT1.Visible = ConfigParameter._Object._PolicyManagementTNA
            dgcolhOT2.Visible = ConfigParameter._Object._PolicyManagementTNA
            dgColhOT3.Visible = ConfigParameter._Object._PolicyManagementTNA
            dgColhOT4.Visible = ConfigParameter._Object._PolicyManagementTNA
            dgcolhTotalOverTime.Visible = Not ConfigParameter._Object._PolicyManagementTNA
            dgcolhTotalShortHrs.Visible = Not ConfigParameter._Object._PolicyManagementTNA
            'Pinkal (30-Oct-2013) -- End 

            'Pinkal (15-Nov-2013) -- Start
            'Enhancement : Oman Changes
            LblPolicy.Visible = ConfigParameter._Object._PolicyManagementTNA
            CboPolicy.Visible = ConfigParameter._Object._PolicyManagementTNA
            objbtnSearchPolicy.Visible = ConfigParameter._Object._PolicyManagementTNA
            'Pinkal (15-Nov-2013) -- End


            'Pinkal (30-Dec-2013) -- Start
            'Enhancement : Oman Changes
            mnuEditTimeSheet.Enabled = User._Object.Privilege._AllowToEditTimesheetCard
            mnuRoundingoff.Enabled = User._Object.Privilege._AllowToRoundoffTimings
            'Pinkal (30-Dec-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisiblity", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Private Sub frmTimesheetDetail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objlogintran = New clslogin_Tran
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
            SetVisiblity()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheetDetail_Load", mstrModuleName)
        End Try
    End Sub


    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clslogin_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clslogin_Tran"
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


#End Region

#Region " Button's Event(s) "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try

            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (15-Nov-2013) -- Start
    'Enhancement : Oman Changes

    Private Sub objbtnSearchShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchShift.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try

            dtList = CType(cboShift.DataSource, DataTable)
            With cboShift
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchShift_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchPolicy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPolicy.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try

            dtList = CType(CboPolicy.DataSource, DataTable)
            With CboPolicy
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPolicy_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (15-Nov-2013) -- End

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employee is compulsory information.Please select Employee."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboEmployee.Select()
                Exit Sub
            End If
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            mstrAdvanceFilter = ""
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " LinkButton's Event(s) "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            'S.SANDEEP [04 JUN 2015] -- END
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                FillCombo()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboEmployee_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedValueChanged
        Try
            dgTimeSheet.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ContextMenu Event"

    Private Sub mnuEditTimeSheet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEditTimeSheet.Click
        Try
            If dgTimeSheet.SelectedCells.Count < 0 Then Exit Sub
            Dim intRowIndex As Integer = dgTimeSheet.SelectedCells(0).RowIndex

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ConfigParameter._Object._IsArutiDemo Then
                Dim objMstdata As New clsMasterData
                Dim objPeriod As New clscommom_period_Tran

                'Pinkal (20-Jan-2014) -- Start
                'Enhancement : Oman Changes
                'Dim intPeriod As Integer = objMstdata.getCurrentPeriodID(enModuleReference.Payroll, CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value))
                Dim intPeriod As Integer = objMstdata.getCurrentTnAPeriodID(enModuleReference.Payroll, CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value))
                'Pinkal (20-Jan-2014) -- End


                If intPeriod > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = intPeriod
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriod
                    'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "you can't edit this timesheet entry.Reason:Period is already over."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

                    'Pinkal (29-Jul-2016) -- Start
                    'Enhancement - IMPLEMENTING TnA Valiadation For Timesheet Entry.

                    If ConfigParameter._Object._AllowToChangeAttendanceAfterPayment Then

                        Dim objPaymentTran As New clsPayment_tran
                        Dim intPeriodId As Integer = objMstdata.getCurrentTnAPeriodID(enModuleReference.Payroll, CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value).Date)

                        Dim mdtTnAStartdate As Date = Nothing
                        If intPeriodId > 0 Then
                            Dim objTnAPeriod As New clscommom_period_Tran
                            mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                        End If

                        If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboEmployee.SelectedValue), mdtTnAStartdate, intPeriodId, CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value).Date) > 0 Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to edit timesheet data ?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If
                        End If

                    Else

                        Dim objLeaveTran As New clsTnALeaveTran

                    Dim mdtTnADate As DateTime = Nothing
                    If objPeriod._TnA_EndDate.Date < CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value).Date Then
                        mdtTnADate = CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value).Date
                    Else
                        mdtTnADate = objPeriod._TnA_EndDate.Date
                    End If

                    If objLeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName), CInt(cboEmployee.SelectedValue).ToString(), mdtTnADate.Date, enModuleReference.TnA) Then
                            'Sohail (19 Apr 2019) -- Start
                            'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "you can't edit this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "you can't edit this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 9, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                Dim objFrmProcess As New frmProcessPayroll
                                objFrmProcess.ShowDialog()
                            End If
                            'Sohail (19 Apr 2019) -- End
                    Exit Sub
                End If

                End If

            End If

                'Pinkal (29-Jul-2016) -- End

            End If


            Dim objFrm As New frmEditTimesheet
            If objFrm.displayDialog(CInt(dgTimeSheet.Rows(intRowIndex).Cells(objcolhLoginunkId.Index).Value), CInt(dgTimeSheet.Rows(intRowIndex).Cells(objcolhEmployeeID.Index).Value), _
                                          CInt(dgTimeSheet.Rows(intRowIndex).Cells(objcolhShiftId.Index).Value), CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value), _
                                          CInt(dgTimeSheet.Rows(intRowIndex).Cells(objcolhPolicyId.Index).Value), mnuEditTimeSheet.Text, False) Then
                objbtnSearch_Click(New Object(), New EventArgs())
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEditTimeSheet_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuRoundingoff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRoundingoff.Click
        Try
            If dgTimeSheet.SelectedCells.Count < 0 Then Exit Sub
            Dim intRowIndex As Integer = dgTimeSheet.SelectedCells(0).RowIndex

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ConfigParameter._Object._IsArutiDemo Then
                Dim objMstdata As New clsMasterData
                Dim objPeriod As New clscommom_period_Tran

                'Pinkal (20-Jan-2014) -- Start
                'Enhancement : Oman Changes
                'Dim intPeriod As Integer = objMstdata.getCurrentPeriodID(enModuleReference.Payroll, CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value))
                Dim intPeriod As Integer = objMstdata.getCurrentTnAPeriodID(enModuleReference.Payroll, CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value))
                'Pinkal (20-Jan-2014) -- End

                If intPeriod > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = intPeriod
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriod
                    'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "you can't round off this timesheet entry.Reason:Period is already over."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If


                    'Pinkal (29-Jul-2016) -- Start
                    'Enhancement - IMPLEMENTING TnA Valiadation For Timesheet Entry.

                    If ConfigParameter._Object._AllowToChangeAttendanceAfterPayment Then

                        Dim objPaymentTran As New clsPayment_tran
                        Dim intPeriodId As Integer = objMstdata.getCurrentTnAPeriodID(enModuleReference.Payroll, CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value).Date)

                        Dim mdtTnAStartdate As Date = Nothing
                        If intPeriodId > 0 Then
                            Dim objTnAPeriod As New clscommom_period_Tran
                            mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                        End If

                        If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboEmployee.SelectedValue), mdtTnAStartdate, intPeriodId, CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value).Date) > 0 Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to roundoff timesheet data ?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If
                        End If

                    Else

                        Dim objLeaveTran As New clsTnALeaveTran
                    Dim mdtTnADate As DateTime = Nothing
                    If objPeriod._TnA_EndDate.Date < CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value).Date Then
                        mdtTnADate = CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value).Date
                    Else
                        mdtTnADate = objPeriod._TnA_EndDate.Date
                    End If

                    If objLeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName), CInt(cboEmployee.SelectedValue).ToString(), mdtTnADate.Date, enModuleReference.TnA) Then
                            'Sohail (19 Apr 2019) -- Start
                            'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "you can't round off this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "you can't round off this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 9, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                Dim objFrmProcess As New frmProcessPayroll
                                objFrmProcess.ShowDialog()
                            End If
                            'Sohail (19 Apr 2019) -- End
                    Exit Sub
                End If

                    End If

                    'Pinkal (29-Jul-2016) -- End

                End If

            End If

            Dim objFrm As New frmEditTimesheet
            If objFrm.displayDialog(CInt(dgTimeSheet.Rows(intRowIndex).Cells(objcolhLoginunkId.Index).Value), CInt(dgTimeSheet.Rows(intRowIndex).Cells(objcolhEmployeeID.Index).Value), _
                                          CInt(dgTimeSheet.Rows(intRowIndex).Cells(objcolhShiftId.Index).Value), CDate(dgTimeSheet.Rows(intRowIndex).Cells(dgcolhDate.Index).Value), _
                                          CInt(dgTimeSheet.Rows(intRowIndex).Cells(objcolhPolicyId.Index).Value), mnuRoundingoff.Text, True) Then
                objbtnSearch_Click(New Object(), New EventArgs())
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRoundingoff_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGridview Event"

    Private Sub dgTimeSheet_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgTimeSheet.MouseDown
        Try
            mintMouseX = e.X
            mintMouseY = e.Y
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgTimeSheet_MouseDown", mstrModuleName)
        End Try
    End Sub

    Private Sub dgTimeSheet_CellMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgTimeSheet.CellMouseDown
        Try
            If e.Button = Windows.Forms.MouseButtons.Right AndAlso e.RowIndex > -1 Then
                dgTimeSheet.ClearSelection()
                dgTimeSheet.Rows(e.RowIndex).Cells(e.ColumnIndex).Selected = True
                cmnuTimesheet.Show(dgTimeSheet, New Drawing.Point(mintMouseX, mintMouseY))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgTimeSheet_CellMouseDown", mstrModuleName)
        End Try
    End Sub


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbShiftinfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbShiftinfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbShiftinfo.Text = Language._Object.getCaption(Me.gbShiftinfo.Name, Me.gbShiftinfo.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.lblFromdate.Text = Language._Object.getCaption(Me.lblFromdate.Name, Me.lblFromdate.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.Name, Me.LblToDate.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.mnuEditTimeSheet.Text = Language._Object.getCaption(Me.mnuEditTimeSheet.Name, Me.mnuEditTimeSheet.Text)
			Me.mnuRoundingoff.Text = Language._Object.getCaption(Me.mnuRoundingoff.Name, Me.mnuRoundingoff.Text)
			Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
			Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
			Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
			Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
			Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
			Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
			Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
			Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
			Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
			Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
			Me.LblPolicy.Text = Language._Object.getCaption(Me.LblPolicy.Name, Me.LblPolicy.Text)
			Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.Name, Me.LblShift.Text)
			Me.dgcolhDate.HeaderText = Language._Object.getCaption(Me.dgcolhDate.Name, Me.dgcolhDate.HeaderText)
			Me.dgcolhShift.HeaderText = Language._Object.getCaption(Me.dgcolhShift.Name, Me.dgcolhShift.HeaderText)
			Me.dgcolhPolicy.HeaderText = Language._Object.getCaption(Me.dgcolhPolicy.Name, Me.dgcolhPolicy.HeaderText)
			Me.dgColhIntime.HeaderText = Language._Object.getCaption(Me.dgColhIntime.Name, Me.dgColhIntime.HeaderText)
			Me.dgColhoutTime.HeaderText = Language._Object.getCaption(Me.dgColhoutTime.Name, Me.dgColhoutTime.HeaderText)
			Me.dgcolhWorkhrs.HeaderText = Language._Object.getCaption(Me.dgcolhWorkhrs.Name, Me.dgcolhWorkhrs.HeaderText)
			Me.dgcolhBreakhrs.HeaderText = Language._Object.getCaption(Me.dgcolhBreakhrs.Name, Me.dgcolhBreakhrs.HeaderText)
			Me.dgcolhBaseHrs.HeaderText = Language._Object.getCaption(Me.dgcolhBaseHrs.Name, Me.dgcolhBaseHrs.HeaderText)
			Me.dgcolhOT1.HeaderText = Language._Object.getCaption(Me.dgcolhOT1.Name, Me.dgcolhOT1.HeaderText)
			Me.dgcolhOT2.HeaderText = Language._Object.getCaption(Me.dgcolhOT2.Name, Me.dgcolhOT2.HeaderText)
			Me.dgColhOT3.HeaderText = Language._Object.getCaption(Me.dgColhOT3.Name, Me.dgColhOT3.HeaderText)
			Me.dgColhOT4.HeaderText = Language._Object.getCaption(Me.dgColhOT4.Name, Me.dgColhOT4.HeaderText)
			Me.dgcolhTotalOverTime.HeaderText = Language._Object.getCaption(Me.dgcolhTotalOverTime.Name, Me.dgcolhTotalOverTime.HeaderText)
			Me.dgcolhTotalShortHrs.HeaderText = Language._Object.getCaption(Me.dgcolhTotalShortHrs.Name, Me.dgcolhTotalShortHrs.HeaderText)
			Me.dgcolhInException.HeaderText = Language._Object.getCaption(Me.dgcolhInException.Name, Me.dgcolhInException.HeaderText)
			Me.dgcolhOutException.HeaderText = Language._Object.getCaption(Me.dgcolhOutException.Name, Me.dgcolhOutException.HeaderText)
			Me.dgcolhRemarks.HeaderText = Language._Object.getCaption(Me.dgcolhRemarks.Name, Me.dgcolhRemarks.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "you can't edit this timesheet entry.Reason:Period is already over.")
			Language.setMessage(mstrModuleName, 3, "you can't edit this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period.")
			Language.setMessage(mstrModuleName, 4, "you can't round off this timesheet entry.Reason:Period is already over.")
			Language.setMessage(mstrModuleName, 5, "you can't round off this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period.")
			Language.setMessage(mstrModuleName, 6, "Employee is compulsory information.Please select Employee.")
			Language.setMessage(mstrModuleName, 7, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to edit timesheet data ?")
			Language.setMessage(mstrModuleName, 8, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to roundoff timesheet data ?")
			Language.setMessage(mstrModuleName, 9, "Do you want to void Payroll?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class