﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 6

Public Class frmTimesheet

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmTimesheet"
    Private mblnCancel As Boolean = True
    Private objTimeSheet As clslogin_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintTimesheetUnkid As Integer = -1
    Private mintHoldunkid As Integer = -1


    'Pinkal (29-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = ""
    'Pinkal (29-Aug-2012) -- End


#End Region

#Region "Form's Event"

    Private Sub frmTimesheet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objTimeSheet = New clslogin_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Pinkal (25-Oct-2012) -- Start
            'Enhancement : TRA Changes

            'dtpdate.MinDate = FinancialYear._Object._Database_Start_Date
            'dtpdate.MaxDate = FinancialYear._Object._Database_End_Date

            'Pinkal (25-Oct-2012) -- End


            'Pinkal (25-Feb-2015) -- Start
            'Enhancement - CHANGING FOR VOLTAMP NET BF ISSUE WHEN CLOSED PERIOD.
            If ConfigParameter._Object._PolicyManagementTNA Then
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Dim dsList As DataSet = objPeriod.GetList("List", enModuleReference.Payroll, True, enStatusType.Open)
                Dim dsList As DataSet = objPeriod.GetList("List", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Open)
                'Sohail (21 Aug 2015) -- End
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = CInt(dsList.Tables(0).Rows(0)("periodunkid"))
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dsList.Tables(0).Rows(0)("periodunkid"))
                    'Sohail (21 Aug 2015) -- End
                    dtpdate.MinDate = objPeriod._TnA_StartDate.Date
                    'dtpdate.MaxDate = objPeriod._TnA_EndDate.Date
                End If
            Else
                dtpdate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                dtpdate.MaxDate = FinancialYear._Object._Database_End_Date.Date
            End If
            ''Pinkal (25-Feb-2015) -- End

            Call SetVisibility()

            'Pinkal (25-Oct-2012) -- Start
            'Enhancement : TRA Changes
            'FillCombo()
            FillEmployee()
            cboEmployee_SelectedIndexChanged(sender, e)
            'Pinkal (25-Oct-2012) -- End

            dtpdate_ValueChanged(sender, e)
            dtpdate.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheet_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTimesheet_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTimesheet_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTimesheet_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objTimeSheet = Nothing
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboshift_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboshift.SelectedIndexChanged
        Try
            'START FOR WORK HOUR PER DAY AS PER SHIFT WORK HOUR


            'Pinkal (03-Jul-2013) -- Start
            'Enhancement : TRA Changes

            'Dim objShift As New clsshift_master
            Dim objShift As New clsNewshift_master

            'Pinkal (03-Jul-2013) -- End



            'Pinkal (17-Sep-2014) -- Start
            'Enhancement - VOLTAMP WEEKDAY NAME ISSUE WHEN FIRST DAY OF WEEK IS NOT SUNDAY.
            'ObjHoursDay.Text = CStr(CalculateTime(True, objShift.GetShiftWorkingHours(CInt(cboshift.SelectedValue), GetWeekDayNumber(WeekdayName(CInt(Weekday(dtpdate.Value.Date).ToString()))))))
            ObjHoursDay.Text = CStr(CalculateTime(True, objShift.GetShiftWorkingHours(CInt(cboshift.SelectedValue), GetWeekDayNumber(WeekdayName(CInt(Weekday(dtpdate.Value.Date).ToString()), False, FirstDayOfWeek.Sunday)))))
            'Pinkal (17-Sep-2014) -- End



            'END FOR WORK HOUR PER DAY AS PER SHIFT WORK HOUR


            'Pinkal (25-Oct-2012) -- Start
            'Enhancement : TRA Changes
            'FillEmployee()
            'Pinkal (25-Oct-2012) -- End


            'mintHoldunkid = 0

            'Pinkal (21-Oct-2016) -- Start
            'Enhancement - Working on Budget Employee Timesheet as Per Mr.Requirement.
            'CheckForHoldEmployee()
            'Pinkal (21-Oct-2016) -- End

            fillGrid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboshift_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try

            If CInt(cboEmployee.SelectedValue) > 0 Then

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

                If Not IsDBNull(objEmployee._Appointeddate) Then

                    If objEmployee._Appointeddate.Date > dtpdate.Value.Date Then
                        dtpdate.MinDate = objEmployee._Appointeddate.Date

                    ElseIf objEmployee._Appointeddate.Date < dtpdate.Value.Date Then

                        If objEmployee._Appointeddate.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                            dtpdate.MinDate = objEmployee._Appointeddate.Date
                        End If

                    End If

                End If


                If Not objEmployee._Termination_To_Date = Nothing Then

                    If objEmployee._Termination_To_Date.Date > dtpdate.Value.Date Then
                        dtpdate.MaxDate = objEmployee._Termination_To_Date.Date

                    ElseIf objEmployee._Termination_To_Date.Date < dtpdate.Value.Date Then

                        If objEmployee._Termination_To_Date.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                            dtpdate.MaxDate = objEmployee._Termination_To_Date.Date
                        End If


                    End If


                End If
                objTimeSheet._Logindate = dtpdate.Value.Date
                objTimeSheet._Employeeunkid = CInt(cboEmployee.SelectedValue)

                'Pinkal (18-Nov-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
                'CheckForHoldEmployee()
                'Pinkal (18-Nov-2016) -- End
                fillGrid()
                objWorkedDays.Text = objTimeSheet.GetEmployeeDayWorked(CInt(cboEmployee.SelectedValue), dtpdate.Value.Month).ToString
                objHoliday.Text = objTimeSheet.GetEmployeeHoliday(CInt(cboEmployee.SelectedValue), dtpdate.Value.Month).ToString
                objEmpLeave.Text = objTimeSheet.GetEmployeeLeave(CInt(cboEmployee.SelectedValue), dtpdate.Value.Month).ToString
                objWorkingDays.Text = CStr(CInt(DateTime.DaysInMonth(dtpdate.Value.Year, dtpdate.Value.Month)) - GetWeekendDaysCount() - CInt(objHoliday.Text))

            End If


            Dim objEmpShift As New clsEmployee_Shift_Tran
            Dim dtTable As DataTable = objEmpShift.GetEmployeeShift(dtpdate.Value.Date, CInt(cboEmployee.SelectedValue))
            Dim drRow As DataRow = dtTable.NewRow()
            drRow("shiftunkid") = 0
            drRow("shiftname") = Language.getMessage(mstrModuleName, 10, "Select")
            dtTable.Rows.InsertAt(drRow, 0)

            cboshift.ValueMember = "shiftunkid"
            cboshift.DisplayMember = "shiftname"
            cboshift.DataSource = dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Try
            If Validation() Then
                Dim objfrmtimesheet_AddEdit As New frmTimesheet_AddEdit
                objfrmtimesheet_AddEdit.lblCheckout.Visible = False
                objfrmtimesheet_AddEdit.lblCheckouttime.Visible = False
                objfrmtimesheet_AddEdit.dtpCheckouttime.Visible = False

                If objfrmtimesheet_AddEdit.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue), CInt(cboshift.SelectedValue), dtpdate.Value, Not btnLogin.Enabled) Then
                    objTimeSheet._Logindate = dtpdate.Value.Date
                    objTimeSheet._Employeeunkid = CInt(cboEmployee.SelectedValue)
                    objTimeSheet._Shiftunkid = CInt(cboshift.SelectedValue)
                    btnLogin.Enabled = objfrmtimesheet_AddEdit.mblnIsLogin
                    fillGrid()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnLogin_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() Then
                If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ConfigParameter._Object._IsArutiDemo Then
                    If CheckForPeriod() = False Then
                        Exit Sub
                    End If
                End If

                Dim objfrmtimesheet_AddEdit As New frmTimesheet_AddEdit
                If objfrmtimesheet_AddEdit.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue), CInt(cboshift.SelectedValue), dtpdate.Value) Then
                    objTimeSheet._Logindate = dtpdate.Value.Date
                    objTimeSheet._Employeeunkid = CInt(cboEmployee.SelectedValue)
                    objTimeSheet._Shiftunkid = CInt(cboshift.SelectedValue)
                    objWorkedDays.Text = objTimeSheet.GetEmployeeDayWorked(CInt(cboEmployee.SelectedValue), dtpdate.Value.Month).ToString
                    fillGrid()
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If dgvTimeSheet.SelectedRows.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Time Record from the list to perform further operation."), enMsgBoxStyle.Information)
                Exit Sub
            ElseIf Validation() Then

                If CheckForPeriod() Then

                    If dgvTimeSheet.SelectedRows.Count > 0 Then
                        Dim objfrmtimesheet_AddEdit As New frmTimesheet_AddEdit
                        objfrmtimesheet_AddEdit.btnLogout.Visible = True
                        If objfrmtimesheet_AddEdit.displayDialog(CInt(dgvTimeSheet.SelectedRows(0).Cells(objcolhLoginunkid.Index).Value), enAction.EDIT_ONE, CInt(cboEmployee.SelectedValue), CInt(cboshift.SelectedValue), CDate(dgvTimeSheet.SelectedRows(0).Cells(colhLogindate.Index).Value), Not btnLogin.Enabled) Then
                            btnLogin.Enabled = Not objfrmtimesheet_AddEdit.mblnIsLogin
                            fillGrid()
                        End If
                    End If

                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnVoid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        Try
            If dgvTimeSheet.SelectedRows.Count > 0 Then


                'Pinkal (20-Sep-2013) -- Start
                'Enhancement : TRA Changes

                If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ConfigParameter._Object._IsArutiDemo Then
                    Dim objMstdata As New clsMasterData
                    Dim objPeriod As New clscommom_period_Tran

                    'Pinkal (20-Jan-2014) -- Start
                    'Enhancement : Oman Changes
                    'Dim intPeriod As Integer = objMstdata.getCurrentPeriodID(enModuleReference.Payroll, dtpdate.Value.Date)
                    Dim intPeriod As Integer = objMstdata.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpdate.Value.Date)
                    'Pinkal (20-Jan-2014) -- End

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = intPeriod
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriod
                    'Sohail (21 Aug 2015) -- End
                    If objPeriod._Statusid = enStatusType.Close Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "you can't delete this timesheet entry.Reason:Period is already over."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Dim objLeaveTran As New clsTnALeaveTran


                    'Pinkal (03-Jan-2014) -- Start
                    'Enhancement : Oman Changes

                    'If objLeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid, CInt(cboEmployee.SelectedValue).ToString(), objPeriod._End_Date.Date) Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "you can't delete this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    '    Exit Sub
                    'End If

                    Dim mdtTnADate As DateTime = Nothing
                    If objPeriod._TnA_EndDate.Date < dtpdate.Value.Date Then
                        mdtTnADate = dtpdate.Value.Date
                    Else
                        mdtTnADate = objPeriod._TnA_EndDate.Date
                    End If

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objLeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid, CInt(cboEmployee.SelectedValue).ToString(), mdtTnADate.Date, enModuleReference.TnA) Then
                    If objLeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid(FinancialYear._Object._DatabaseName), CInt(cboEmployee.SelectedValue).ToString(), mdtTnADate.Date, enModuleReference.TnA) Then
                        'Sohail (21 Aug 2015) -- End
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "you can't delete this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "you can't delete this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 17, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                        Exit Sub
                    End If

                    'Pinkal (03-Jan-2014) -- End

                End If

                'Pinkal (29-Jul-2016) -- Start
                'Enhancement - IMPLEMENTING TnA Valiadation For Timesheet Entry.

                If ConfigParameter._Object._AllowToChangeAttendanceAfterPayment Then

                    If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                        Dim objPaymentTran As New clsPayment_tran
                        Dim objPeriod As New clsMasterData
                        Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, CDate(dgvTimeSheet.SelectedRows(0).Cells(colhLogindate.Index).Value).Date)

                        Dim mdtTnAStartdate As Date = Nothing
                        If intPeriodId > 0 Then
                            Dim objTnAPeriod As New clscommom_period_Tran
                            mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                        End If

                        If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboEmployee.SelectedValue), mdtTnAStartdate, intPeriodId, CDate(dgvTimeSheet.SelectedRows(0).Cells(colhLogindate.Index).Value).Date) > 0 Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to delete timesheet data ?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If
                        End If
                    End If

                End If

                'Pinkal (29-Jul-2016) -- End


                If dgvTimeSheet.SelectedRows(0).Index = 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "This is the main record of day.Are you sure you want to delete selected Time Information?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        objTimeSheet._Loginunkid = CInt(dgvTimeSheet.SelectedRows(0).Cells(objcolhLoginunkid.Index).Value)
                        Dim frm As New frmReasonSelection
                        Dim mstrVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.TNA, mstrVoidReason)
                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        Else
                            objTimeSheet._Voidreason = mstrVoidReason
                        End If
                        frm = Nothing
                        objTimeSheet._Voiduserunkid = User._Object._Userunkid
                        objTimeSheet._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime



                        'Pinkal (11-AUG-2017) -- Start
                        'Enhancement - Working On B5 Plus Company TnA Enhancements.


                        'objTimeSheet.Void(CInt(dgvTimeSheet.SelectedRows(0).Cells(objcolhLoginunkid.Index).Value), FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                        '                , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                        '                , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                        '                , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                        '                , ConfigParameter._Object._PolicyManagementTNA _
                        '                , ConfigParameter._Object._FirstCheckInLastCheckOut, ConfigParameter._Object._DonotAttendanceinSeconds, False, -1, Nothing, "", "")

'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objTimeSheet._FormName = mstrModuleName
                        objTimeSheet._LoginEmployeeUnkid = 0
                        objTimeSheet._ClientIP = getIP()
                        objTimeSheet._HostName = getHostName()
                        objTimeSheet._FromWeb = False
                        objTimeSheet._AuditUserId = User._Object._Userunkid
objTimeSheet._CompanyUnkid = Company._Object._Companyunkid
                        objTimeSheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        objTimeSheet.Void(CInt(dgvTimeSheet.SelectedRows(0).Cells(objcolhLoginunkid.Index).Value), FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                        , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                        , ConfigParameter._Object._PolicyManagementTNA _
                                                 , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                                 , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                                 , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                                 , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                                 , ConfigParameter._Object._DonotAttendanceinSeconds, False, -1, Nothing, "", "")


                        'Pinkal (11-AUG-2017) -- End


                        objWorkedDays.Text = objTimeSheet.GetEmployeeDayWorked(CInt(cboEmployee.SelectedValue), dtpdate.Value.Month).ToString
                        fillGrid()
                    End If
                ElseIf eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to delete selected Time Information?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    objTimeSheet._Loginunkid = CInt(dgvTimeSheet.SelectedRows(0).Cells(objcolhLoginunkid.Index).Value)
                    Dim frm As New frmReasonSelection
                    Dim mstrVoidReason As String = String.Empty
                    frm.displayDialog(enVoidCategoryType.TNA, mstrVoidReason)
                    If mstrVoidReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objTimeSheet._Voidreason = mstrVoidReason
                    End If
                    frm = Nothing
                    objTimeSheet._Voiduserunkid = User._Object._Userunkid
                    objTimeSheet._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime



                    'Pinkal (11-AUG-2017) -- Start
                    'Enhancement - Working On B5 Plus Company TnA Enhancements.

                    'objTimeSheet.Void(CInt(dgvTimeSheet.SelectedRows(0).Cells(objcolhLoginunkid.Index).Value), FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                    '                                   , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                    '                                   , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                    '                                   , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA, _
                    '                       ConfigParameter._Object._FirstCheckInLastCheckOut, ConfigParameter._Object._DonotAttendanceinSeconds, False, -1, Nothing, "", "")

'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objTimeSheet._FormName = mstrModuleName
                    objTimeSheet._LoginEmployeeUnkid = 0
                    objTimeSheet._ClientIP = getIP()
                    objTimeSheet._HostName = getHostName()
                    objTimeSheet._FromWeb = False
                    objTimeSheet._AuditUserId = User._Object._Userunkid
objTimeSheet._CompanyUnkid = Company._Object._Companyunkid
                    objTimeSheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    objTimeSheet.Void(CInt(dgvTimeSheet.SelectedRows(0).Cells(objcolhLoginunkid.Index).Value), FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                      , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                      , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                              , True, ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PolicyManagementTNA _
                                              , ConfigParameter._Object._FirstCheckInLastCheckOut _
                                              , ConfigParameter._Object._DonotAttendanceinSeconds _
                                              , ConfigParameter._Object._IsHolidayConsiderOnWeekend _
                                              , ConfigParameter._Object._IsDayOffConsiderOnWeekend _
                                              , ConfigParameter._Object._IsHolidayConsiderOnDayoff _
                                              , False, -1, Nothing, "", "")

                    'Pinkal (11-AUG-2017) -- End


                    objWorkedDays.Text = objTimeSheet.GetEmployeeDayWorked(CInt(cboEmployee.SelectedValue), dtpdate.Value.Month).ToString
                    fillGrid()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


    'Pinkal (03-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try

            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (03-Nov-2012) -- End


#End Region

#Region "Link Button's Event"

    'Pinkal (01-Sep-2017) -- Start
    'Enhancement - Solved bug for Heron Portico Giving Error when Employee Absent Process did.

    'Private Sub lnkHoldemployee_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkHoldemployee.LinkClicked
    '    Try
    '        If Validation() Then

    '            'Pinkal (03-Jan-2011) -- Start

    '            If CheckForPeriod() Then

    '                Dim objfrmholdunholdemployee As New frmholdunholdemployee
    '                objfrmholdunholdemployee.btnhold.Visible = True
    '                objfrmholdunholdemployee.btnUnhold.Visible = False
    '                objfrmholdunholdemployee.chkPostTimesheet.Visible = False
    '                objfrmholdunholdemployee.Text = "Hold Employee"


    '                'Pinkal (29-Aug-2012) -- Start
    '                'Enhancement : TRA Changes
    '                'If objfrmholdunholdemployee.displayDialog(mintHoldunkid, enAction.ADD_ONE, dtpdate.Value.Date, CInt(cboEmployee.SelectedValue), CInt(cboshift.SelectedValue), CInt(cboDepartment.SelectedValue)) Then
    '                If objfrmholdunholdemployee.displayDialog(mintHoldunkid, enAction.ADD_ONE, dtpdate.Value.Date, CInt(cboEmployee.SelectedValue), CInt(cboshift.SelectedValue)) Then
    '                    'Pinkal (29-Aug-2012) -- End

    '                    If dgvTimeSheet.SelectedRows.Count > 0 Then

    '                        If CInt(dgvTimeSheet.SelectedCells(objcolhLoginunkid.Index).Value) > 0 Then
    '                            objTimeSheet._Loginunkid = CInt(dgvTimeSheet.SelectedCells(objcolhLoginunkid.Index).Value)
    '                            objTimeSheet._Holdunkid = mintHoldunkid

    '                            'STRAT FOR UPDATE HOLDID IN ALREADY EXIST EMPLOYEE LOGIN DATA IN LOGIN TRAN TABLE

    '                            'S.SANDEEP [04 JUN 2015] -- START
    '                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                            'objTimeSheet.UpdateHoldEmployee()
    '                            objTimeSheet.UpdateHoldEmployee(ConfigParameter._Object._DonotAttendanceinSeconds)
    '                            'S.SANDEEP [04 JUN 2015] -- END


    '                            'END FOR UPDATE HOLDID IN ALREADY EXIST EMPLOYEE LOGIN DATA IN LOGIN TRAN TABLE
    '                        End If

    '                    End If

    '                    If mintHoldunkid > 0 Then
    '                        lnkHoldemployee.Enabled = False
    '                        lnkUnhold.Enabled = True
    '                        objStatus.Text = "On Hold"
    '                    Else
    '                        lnkHoldemployee.Enabled = True
    '                        lnkUnhold.Enabled = False
    '                        objStatus.Text = "UnHold"
    '                    End If

    '                End If


    '            End If

    '            'Pinkal (03-Jan-2011) -- End

    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lnkHoldemployee_LinkClicked", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub lnkUnhold_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkUnhold.LinkClicked
    '    Try
    '        If Validation() Then

    '            'Pinkal (03-Jan-2011) -- Start

    '            If CheckForPeriod() Then

    '                Dim objfrmholdunholdemployee As New frmholdunholdemployee
    '                objfrmholdunholdemployee.btnhold.Visible = False
    '                objfrmholdunholdemployee.btnUnhold.Visible = True
    '                objfrmholdunholdemployee.chkPostTimesheet.Visible = True
    '                objfrmholdunholdemployee.Text = "Unhold Employee"


    '                'Pinkal (29-Aug-2012) -- Start
    '                'Enhancement : TRA Changes
    '                'objfrmholdunholdemployee.displayDialog(mintHoldunkid, enAction.ADD_ONE, dtpdate.Value.Date, CInt(cboEmployee.SelectedValue), CInt(cboshift.SelectedValue), CInt(cboDepartment.SelectedValue))
    '                objfrmholdunholdemployee.displayDialog(mintHoldunkid, enAction.ADD_ONE, dtpdate.Value.Date, CInt(cboEmployee.SelectedValue), CInt(cboshift.SelectedValue))
    '                'Pinkal (29-Aug-2012) -- End

    '                If mintHoldunkid > 0 Then
    '                    lnkHoldemployee.Enabled = False
    '                    lnkUnhold.Enabled = True
    '                    objStatus.Text = "On Hold"
    '                Else
    '                    lnkHoldemployee.Enabled = True
    '                    lnkUnhold.Enabled = False
    '                    objStatus.Text = "UnHold"
    '                End If

    '            End If

    '            'Pinkal (03-Jan-2011) -- End

    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lnkUnhold_LinkClicked", mstrModuleName)
    '    End Try
    'End Sub

    'Pinkal (01-Sep-2017) -- End


    'Pinkal (29-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                FillEmployee()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Pinkal (29-Aug-2012) -- End


#End Region

#Region "Private Methods"


    'Pinkal (25-Oct-2012) -- Start
    'Enhancement : TRA Changes

    'Private Sub FillCombo()

    '    Try
    '        'FOR SHIFT 
    '        Dim dsShift As DataSet = Nothing
    '        Dim objshiftmaster As New clsshift_master
    '        dsShift = objshiftmaster.getListForCombo("Shift", True)
    '        cboshift.ValueMember = "shiftunkid"
    '        cboshift.DisplayMember = "name"
    '        cboshift.DataSource = dsShift.Tables(0)



    '        'Pinkal (29-Aug-2012) -- Start
    '        'Enhancement : TRA Changes

    '        'FOR DEPARTMENT
    '        'Dim dsDepartment As DataSet = Nothing
    '        'Dim objDepartment As New clsDepartment
    '        'dsDepartment = objDepartment.getComboList("Department", True)
    '        'cboDepartment.ValueMember = "departmentunkid"
    '        'cboDepartment.DisplayMember = "name"
    '        'cboDepartment.DataSource = dsDepartment.Tables(0)

    '        'Pinkal (29-Aug-2012) -- End

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
    '    End Try
    'End Sub


    'Pinkal (25-Oct-2012) -- End

    Private Sub FillEmployee()
        Try
            'FOR EMPLOYEE
            Dim dsEmployee As DataSet = Nothing
            Dim dtEmployee As DataTable = Nothing
            Dim strSearch As String = String.Empty
            Dim objEmployee As New clsEmployee_Master
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsEmployee = objEmployee.GetList("Employee")

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsEmployee = objEmployee.GetList("Employee", , , dtpdate.Value.Date, dtpdate.Value.Date)

            'S.SANDEEP [15 NOV 2016] -- START
            'ENHANCEMENT : QUERY OPTIMIZATION
            'dsEmployee = objEmployee.GetList(FinancialYear._Object._DatabaseName, _
            '                                 User._Object._Userunkid, _
            '                                 FinancialYear._Object._YearUnkid, _
            '                                 Company._Object._Companyunkid, _
            '                                 dtpdate.Value.Date, _
            '                                 dtpdate.Value.Date, _
            '                                 ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                 False, _
            '                                 "Employee", _
            '                                 ConfigParameter._Object._ShowFirstAppointmentDate)
            ''S.SANDEEP [04 JUN 2015] -- END

            ''Sohail (06 Jan 2012) -- End

            'If CInt(cboshift.SelectedValue) > 0 Then
            '    strSearch = "AND shiftunkid=" & CInt(cboshift.SelectedValue) & " "
            'End If


            If CInt(cboshift.SelectedValue) > 0 Then
                'Pinkal (27-Apr-2019) -- Start
                'Enhancement - Audit Trail changes.
                'strSearch = "AND shiftunkid=" & CInt(cboshift.SelectedValue) & " "
                strSearch = " shiftunkid=" & CInt(cboshift.SelectedValue) & " "
                'Pinkal (27-Apr-2019) -- End
            End If

            If mstrAdvanceFilter.Length > 0 Then

                'Pinkal (27-Apr-2019) -- Start
                'Enhancement - Audit Trail changes.
                'strSearch &= "AND " & mstrAdvanceFilter
                strSearch &= " " & mstrAdvanceFilter
                'Pinkal (27-Apr-2019) -- End
            End If

            dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             dtpdate.Value.Date, _
                                             dtpdate.Value.Date, _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._IsIncludeInactiveEmp, _
                                             "Employee", True, , , , , , , , , , , , , , , strSearch)
            'S.SANDEEP [15 NOV 2016] -- END





            'Pinkal (29-Aug-2012) -- Start
            'Enhancement : TRA Changes

            'If CInt(cboDepartment.SelectedValue) > 0 Then
            '    strSearch &= "AND departmentunkid=" & CInt(cboDepartment.SelectedValue) & " "
            'End If


            'Pinkal (29-Aug-2012) -- End


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : CHECK FOR ACTIVE EMPLOYEE

            'If strSearch.Length > 0 Then
            '    strSearch = strSearch.Substring(3)
            '    dtEmployee = New DataView(dsEmployee.Tables("Employee"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtEmployee = dsEmployee.Tables("Employee")
            'End If

            'S.SANDEEP [15 NOV 2016] -- START
            'ENHANCEMENT : QUERY OPTIMIZATION
            'If ConfigParameter._Object._IsIncludeInactiveEmp Then
            '    If strSearch.Length > 0 Then
            '        strSearch = strSearch.Substring(3)
            '        dtEmployee = New DataView(dsEmployee.Tables("Employee"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            '    Else
            '        dtEmployee = dsEmployee.Tables("Employee")
            '    End If

            'Else
            '    If strSearch.Length > 0 Then
            '        strSearch = strSearch.Substring(3)
            '        dtEmployee = New DataView(dsEmployee.Tables("Employee"), strSearch & " AND isactive = 1 ", "", DataViewRowState.CurrentRows).ToTable
            '    Else
            '        dtEmployee = New DataView(dsEmployee.Tables("Employee"), " isactive = 1 ", "", DataViewRowState.CurrentRows).ToTable
            '    End If
            'End If

            dtEmployee = dsEmployee.Tables("Employee")
            'S.SANDEEP [15 NOV 2016] -- END



            'Pinkal (24-Jun-2011) -- End

            'S.SANDEEP [15 NOV 2016] -- START
            'Dim drRow As DataRow = dtEmployee.NewRow
            'drRow("employeeunkid") = 0
            'drRow("name") = "Select"
            'dtEmployee.Rows.InsertAt(drRow, 0)

            cboEmployee.ValueMember = "employeeunkid"


            'ENHANCEMENT : QUERY OPTIMIZATION
            'cboEmployee.DisplayMember = "name"
            cboEmployee.DisplayMember = "employeename"
            'S.SANDEEP [15 NOV 2016] -- END

            cboEmployee.DataSource = dtEmployee


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboshift.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Shift is compulsory information.Please Select Shift."), enMsgBoxStyle.Information)
                cboshift.Select()
                Return False


                'Pinkal (29-Aug-2012) -- Start
                'Enhancement : TRA Changes

                'ElseIf CInt(cboDepartment.SelectedValue) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Department is compulsory information.Please Select Department."), enMsgBoxStyle.Information)
                '    cboDepartment.Focus()
                '    Return False

                'Pinkal (29-Aug-2012) -- End

            ElseIf CInt(cboEmployee.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub fillGrid()
        Dim dsTimeSheet As New DataSet
        Try
            dgvTimeSheet.Rows.Clear()

            'Pinkal (29-Aug-2012) -- Start
            'Enhancement : TRA Changes

            'If CInt(cboshift.SelectedValue) > 0 And CInt(cboDepartment.SelectedValue) > 0 And CInt(cboEmployee.SelectedValue) > 0 Then
            If CInt(cboshift.SelectedValue) > 0 And CInt(cboEmployee.SelectedValue) > 0 Then
                dsTimeSheet = objTimeSheet.GetList("TimeInfo", True)
                For Each dtrow As DataRow In dsTimeSheet.Tables("TimeInfo").Rows
                    Dim dgvRow As New DataGridViewRow
                    dgvRow.CreateCells(dgvTimeSheet)
                    dgvRow.Cells(objcolhLoginunkid.Index).Value = dtrow.Item("loginunkid")
                    If Not dtrow.Item("holdunkid") Is DBNull.Value Then
                        dgvRow.Cells(objcolhHoldunkid.Index).Value = dtrow.Item("holdunkid")
                        mintHoldunkid = CInt(dgvRow.Cells(objcolhHoldunkid.Index).Value)
                    End If
                    If Not dtrow.Item("checkintime") Is DBNull.Value Then
                        dgvRow.Cells(colhCheckIntime.Index).Value = (CDate(dtrow.Item("checkintime"))).ToShortTimeString
                    End If
                    If Not dtrow.Item("checkouttime") Is DBNull.Value Then
                        dgvRow.Cells(colhCheckOut.Index).Value = (CDate(dtrow.Item("checkouttime"))).ToShortTimeString
                    End If
                    dgvRow.Cells(colhLogindate.Index).Value = CDate(dtrow.Item("logindate"))
                    dgvRow.Cells(colhWorkingHours.Index).Value = CalculateTime(True, CInt(dtrow.Item("workhour"))).ToString("#0.00")
                    dgvTimeSheet.Rows.Add(dgvRow)
                Next
                If Not dsTimeSheet Is Nothing And dsTimeSheet.Tables(0).Rows.Count > 0 Then
                    objTotalHours.Text = CalculateTime(True, CInt(dsTimeSheet.Tables(0).Compute("sum(workhour)", "isvoid = 0"))).ToString("#0.00")
                    objBreak.Text = CStr(CalculateTime(False, CInt(dsTimeSheet.Tables(0).Compute("sum(breakhr)", "isvoid = 0")))) & " Mins"

                    'Pinkal (28-Jan-2014) -- Start
                    'Enhancement : Oman Changes
                    objTeaBreak.Text = CStr(CalculateTime(False, CInt(dsTimeSheet.Tables(0).Compute("sum(teahr)", "isvoid = 0")))) & " Mins"
                    'Pinkal (28-Jan-2014) -- End

                Else
                    objTotalHours.Text = "0"
                    objBreak.Text = "0 Mins"

                    'Pinkal (28-Jan-2014) -- Start
                    'Enhancement : Oman Changes
                    objTeaBreak.Text = "0 Mins"
                    'Pinkal (28-Jan-2014) -- End

                End If


                'Pinkal (28-Jan-2014) -- Start
                'Enhancement : Oman Changes

            Else
                objTotalHours.Text = "0"
                objBreak.Text = "0 Mins"
                objTeaBreak.Text = "0 Mins"

                'Pinkal (28-Jan-2014) -- End

            End If

            'Pinkal (29-Aug-2012) -- End

            If mintHoldunkid > 0 Then
                lnkHoldemployee.Enabled = False
                lnkUnhold.Enabled = True
                objStatus.Text = Language.getMessage(mstrModuleName, 8, "On Hold")
            Else
                lnkHoldemployee.Enabled = True
                lnkUnhold.Enabled = False
                objStatus.Text = Language.getMessage(mstrModuleName, 9, "UnHold")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "fillGrid", mstrModuleName)
        Finally
            dsTimeSheet.Dispose()
        End Try
    End Sub

    Private Sub CheckForHoldEmployee()
        Try
            If CInt(cboshift.SelectedValue) > 0 Then
                mintHoldunkid = objTimeSheet.CheckforHoldEmployee(CInt(cboEmployee.SelectedValue), dtpdate.Value.Date)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckForHoldEmployee", mstrModuleName)
        End Try
    End Sub

    Private Function GetWeekendDaysCount() As Integer
        Dim Count As Integer = 0
        Try
            If CInt(cboshift.SelectedValue) <= 0 Then Return 0
            Dim mdtDate As New DateTime(dtpdate.Value.Year, dtpdate.Value.Month, 1)


            'Pinkal (25-Oct-2012) -- Start
            'Enhancement : TRA Changes
            'Dim objShift As New clsshift_master

            Dim objShift As New clsNewshift_master
            Dim objShiftTran As New clsshift_tran

            objShift._Shiftunkid = CInt(cboshift.SelectedValue)
            objShiftTran.GetShiftTran(objShift._Shiftunkid)
            For i As Integer = 1 To Date.DaysInMonth(dtpdate.Value.Year, dtpdate.Value.Month)

                Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtDate.AddDays(i - 1).DayOfWeek.ToString) & " AND isweekend = 1")

                'If objShift._Shiftdays.Contains(mdtDate.AddDays(i - 1).DayOfWeek.ToString) = False Then
                '    Count += 1
                'End If

                If drRow.Length > 0 Then Count += 1

                'Pinkal (25-Oct-2012) -- End

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetWeekendDaysCount", mstrModuleName)
        End Try
        Return Count
    End Function

    Private Function CheckForPeriod() As Boolean
        Try
            Dim dsList As DataSet = Nothing
            Dim objperiod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objperiod.GetList("Period", enModuleReference.Payroll, True, enStatusType.Close)
            dsList = objperiod.GetList("Period", enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, enStatusType.Close)
            'Sohail (21 Aug 2015) -- End

            If dsList.Tables(0).Rows.Count > 0 Then

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If eZeeDate.convertDate(dsList.Tables(0).Rows(i)("start_date").ToString) <= dtpdate.Value.Date _
                    And eZeeDate.convertDate(dsList.Tables(0).Rows(i)("tna_enddate").ToString) >= dtpdate.Value.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select correct date. Reason:This date period was already closed."), enMsgBoxStyle.Information)
                        dtpdate.Select()
                        Return False
                    End If

                Next

            End If

            'Pinkal (21-Oct-2013) -- Start
            'Enhancement : Oman Changes

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ConfigParameter._Object._IsArutiDemo Then
                Dim objMstdata As New clsMasterData


                'Pinkal (20-Jan-2014) -- Start
                'Enhancement : Oman Changes
                'Dim intPeriod As Integer = objMstdata.getCurrentPeriodID(enModuleReference.Payroll, dtpdate.Value.Date)
                Dim intPeriod As Integer = objMstdata.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpdate.Value.Date)
                'Pinkal (20-Jan-2014) -- End

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objperiod._Periodunkid = intPeriod
                objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriod
                'Sohail (21 Aug 2015) -- End
                If objperiod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "you can't add/edit this timesheet entry.Reason:Period is already over."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Function
                End If

                'Pinkal (29-Jul-2016) -- Start
                'Enhancement - IMPLEMENTING TnA Valiadation For Timesheet Entry.

                If ConfigParameter._Object._AllowToChangeAttendanceAfterPayment Then

                    Dim objPaymentTran As New clsPayment_tran
                    Dim intPeriodId As Integer = objMstdata.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpdate.Value.Date)

                    Dim mdtTnAStartdate As Date = Nothing
                    If intPeriodId > 0 Then
                        Dim objTnAPeriod As New clscommom_period_Tran
                        mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                    End If

                    If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboEmployee.SelectedValue), mdtTnAStartdate, intPeriodId, dtpdate.Value.Date) > 0 Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to add/edit timesheet data ?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Function
                        End If
                    End If

                Else

                    Dim objLeaveTran As New clsTnALeaveTran
                    Dim mdtTnADate As DateTime = Nothing
                    If objperiod._TnA_EndDate.Date < dtpdate.Value.Date Then
                        mdtTnADate = dtpdate.Value.Date
                    Else
                        mdtTnADate = objperiod._TnA_EndDate.Date
                    End If

                    If objLeaveTran.IsPayrollProcessDone(objperiod._Periodunkid(FinancialYear._Object._DatabaseName), CInt(cboEmployee.SelectedValue).ToString(), mdtTnADate.Date, enModuleReference.TnA) Then
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "you can't add/edit this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "you can't add/edit this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 17, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                        Return False
                    End If
                End If

                'Pinkal (29-Jul-2016) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckForPeriod", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Private Sub SetVisibility()

        Try
            btnAdd.Enabled = User._Object.Privilege._AddTimeSheetInformation
            btnEdit.Enabled = User._Object.Privilege._EditTimeSheetInformation
            btnVoid.Enabled = User._Object.Privilege._DeleteTimeSheetInformation
            btnLogin.Enabled = User._Object.Privilege._AllowLoginTimesheet
            lnkHoldemployee.Enabled = User._Object.Privilege._AllowHoldEmployee
            lnkUnhold.Enabled = User._Object.Privilege._AllowUnHoldEmployee
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region

#Region "Datepicker's Event"

    Private Sub dtpdate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpdate.ValueChanged
        Try

            'Pinkal (03-Jan-2011) --Start

            cboEmployee_SelectedIndexChanged(sender, e)

            'Pinkal (03-Jan-2011) --End

            objDayInMonth.Text = DateTime.DaysInMonth(dtpdate.Value.Year, dtpdate.Value.Month).ToString()
            objWorkedDays.Text = objTimeSheet.GetEmployeeDayWorked(CInt(cboEmployee.SelectedValue), dtpdate.Value.Month).ToString
            objHoliday.Text = objTimeSheet.GetEmployeeHoliday(CInt(cboEmployee.SelectedValue), dtpdate.Value.Month).ToString
            objEmpLeave.Text = objTimeSheet.GetEmployeeLeave(CInt(cboEmployee.SelectedValue), dtpdate.Value.Month).ToString
            objWorkingDays.Text = CStr(CInt(DateTime.DaysInMonth(dtpdate.Value.Year, dtpdate.Value.Month)) - GetWeekendDaysCount() - CInt(objHoliday.Text))
            objTimeSheet._Logindate = dtpdate.Value.Date
            objTimeSheet._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objTimeSheet._Shiftunkid = CInt(cboshift.SelectedValue)
            'mintHoldunkid = 0
            CheckForHoldEmployee()
            fillGrid()
            If dtpdate.Value.Date = ConfigParameter._Object._CurrentDateAndTime.Date Then
                btnLogin.Enabled = True
                btnLogin.Enabled = User._Object.Privilege._AllowLoginTimesheet
            Else
                btnLogin.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpdate_ValueChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub dtpdate_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpdate.LostFocus
        Try
            '  Call FillEmployee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpdate_LostFocus", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Jan 2012) -- End

#End Region




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbShiftinfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbShiftinfo.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnVoid.GradientBackColor = GUI._ButttonBackColor
            Me.btnVoid.GradientForeColor = GUI._ButttonFontColor

            Me.btnLogin.GradientBackColor = GUI._ButttonBackColor
            Me.btnLogin.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbShiftinfo.Text = Language._Object.getCaption(Me.gbShiftinfo.Name, Me.gbShiftinfo.Text)
            Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title", Me.EZeeHeader1.Title)
            Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message", Me.EZeeHeader1.Message)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.gbWorkDetail.Text = Language._Object.getCaption(Me.gbWorkDetail.Name, Me.gbWorkDetail.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblHoursDay.Text = Language._Object.getCaption(Me.lblHoursDay.Name, Me.lblHoursDay.Text)
            Me.lblDayWorked.Text = Language._Object.getCaption(Me.lblDayWorked.Name, Me.lblDayWorked.Text)
            Me.lblWorkingDays.Text = Language._Object.getCaption(Me.lblWorkingDays.Name, Me.lblWorkingDays.Text)
            Me.lblDayInMonth.Text = Language._Object.getCaption(Me.lblDayInMonth.Name, Me.lblDayInMonth.Text)
            Me.lblHoliday.Text = Language._Object.getCaption(Me.lblHoliday.Name, Me.lblHoliday.Text)
            Me.lblleave.Text = Language._Object.getCaption(Me.lblleave.Name, Me.lblleave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnVoid.Text = Language._Object.getCaption(Me.btnVoid.Name, Me.btnVoid.Text)
            Me.lblTotalHours.Text = Language._Object.getCaption(Me.lblTotalHours.Name, Me.lblTotalHours.Text)
            Me.lblBreak.Text = Language._Object.getCaption(Me.lblBreak.Name, Me.lblBreak.Text)
            Me.lbldate.Text = Language._Object.getCaption(Me.lbldate.Name, Me.lbldate.Text)
            Me.btnLogin.Text = Language._Object.getCaption(Me.btnLogin.Name, Me.btnLogin.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lnkHoldemployee.Text = Language._Object.getCaption(Me.lnkHoldemployee.Name, Me.lnkHoldemployee.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.lnkUnhold.Text = Language._Object.getCaption(Me.lnkUnhold.Name, Me.lnkUnhold.Text)
            Me.colhCheckIntime.HeaderText = Language._Object.getCaption(Me.colhCheckIntime.Name, Me.colhCheckIntime.HeaderText)
            Me.colhLogindate.HeaderText = Language._Object.getCaption(Me.colhLogindate.Name, Me.colhLogindate.HeaderText)
            Me.colhCheckOut.HeaderText = Language._Object.getCaption(Me.colhCheckOut.Name, Me.colhCheckOut.HeaderText)
            Me.colhWorkingHours.HeaderText = Language._Object.getCaption(Me.colhWorkingHours.Name, Me.colhWorkingHours.HeaderText)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.LblTeaBreak.Text = Language._Object.getCaption(Me.LblTeaBreak.Name, Me.LblTeaBreak.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Shift is compulsory information.Please Select Shift.")
            Language.setMessage(mstrModuleName, 3, "Employee is compulsory information.Please Select Employee.")
            Language.setMessage(mstrModuleName, 4, "Please select Time Record from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 5, "This is the main record of day.Are you sure you want to delete selected Time Information?")
            Language.setMessage(mstrModuleName, 6, "Are you sure you want to delete selected Time Information?")
            Language.setMessage(mstrModuleName, 7, "Please select correct date. Reason:This date period was already closed.")
            Language.setMessage(mstrModuleName, 8, "On Hold")
            Language.setMessage(mstrModuleName, 9, "UnHold")
            Language.setMessage(mstrModuleName, 10, "Select")
            Language.setMessage(mstrModuleName, 11, "you can't delete this timesheet entry.Reason:Period is already over.")
            Language.setMessage(mstrModuleName, 12, "you can't delete this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period.")
            Language.setMessage(mstrModuleName, 13, "you can't add/edit this timesheet entry.Reason:Period is already over.")
            Language.setMessage(mstrModuleName, 14, "you can't add/edit this timesheet entry.Reason:Payroll Process already done for this employee for last date of current open period.")
            Language.setMessage(mstrModuleName, 15, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to delete timesheet data ?")
            Language.setMessage(mstrModuleName, 16, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to add/edit timesheet data ?")
			Language.setMessage(mstrModuleName, 17, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class