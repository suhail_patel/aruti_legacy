﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTimesheet_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTimesheet_AddEdit))
        Me.pnlTime = New System.Windows.Forms.Panel
        Me.gbRemark = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnLogout = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbInOutinfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblCheckIntime = New System.Windows.Forms.Label
        Me.lblCheckouttime = New System.Windows.Forms.Label
        Me.dtpCheckouttime = New System.Windows.Forms.DateTimePicker
        Me.dtpCheckIntime = New System.Windows.Forms.DateTimePicker
        Me.lblcheckin = New eZee.Common.eZeeLine
        Me.lblCheckout = New eZee.Common.eZeeLine
        Me.pnlTime.SuspendLayout()
        Me.gbRemark.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbInOutinfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTime
        '
        Me.pnlTime.Controls.Add(Me.gbRemark)
        Me.pnlTime.Controls.Add(Me.objFooter)
        Me.pnlTime.Controls.Add(Me.gbInOutinfo)
        Me.pnlTime.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTime.Location = New System.Drawing.Point(0, 0)
        Me.pnlTime.Name = "pnlTime"
        Me.pnlTime.Size = New System.Drawing.Size(386, 315)
        Me.pnlTime.TabIndex = 0
        '
        'gbRemark
        '
        Me.gbRemark.BorderColor = System.Drawing.Color.Black
        Me.gbRemark.Checked = False
        Me.gbRemark.CollapseAllExceptThis = False
        Me.gbRemark.CollapsedHoverImage = Nothing
        Me.gbRemark.CollapsedNormalImage = Nothing
        Me.gbRemark.CollapsedPressedImage = Nothing
        Me.gbRemark.CollapseOnLoad = False
        Me.gbRemark.Controls.Add(Me.txtRemark)
        Me.gbRemark.ExpandedHoverImage = Nothing
        Me.gbRemark.ExpandedNormalImage = Nothing
        Me.gbRemark.ExpandedPressedImage = Nothing
        Me.gbRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRemark.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRemark.HeaderHeight = 25
        Me.gbRemark.HeaderMessage = ""
        Me.gbRemark.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbRemark.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRemark.HeightOnCollapse = 0
        Me.gbRemark.LeftTextSpace = 0
        Me.gbRemark.Location = New System.Drawing.Point(12, 159)
        Me.gbRemark.Name = "gbRemark"
        Me.gbRemark.OpenHeight = 300
        Me.gbRemark.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRemark.ShowBorder = True
        Me.gbRemark.ShowCheckBox = False
        Me.gbRemark.ShowCollapseButton = False
        Me.gbRemark.ShowDefaultBorderColor = True
        Me.gbRemark.ShowDownButton = False
        Me.gbRemark.ShowHeader = True
        Me.gbRemark.Size = New System.Drawing.Size(361, 93)
        Me.gbRemark.TabIndex = 3
        Me.gbRemark.Temp = 0
        Me.gbRemark.Text = "Remark"
        Me.gbRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(8, 31)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(344, 55)
        Me.txtRemark.TabIndex = 4
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnLogout)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 260)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(386, 55)
        Me.objFooter.TabIndex = 3
        '
        'btnLogout
        '
        Me.btnLogout.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLogout.BackColor = System.Drawing.Color.White
        Me.btnLogout.BackgroundImage = CType(resources.GetObject("btnLogout.BackgroundImage"), System.Drawing.Image)
        Me.btnLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnLogout.BorderColor = System.Drawing.Color.Empty
        Me.btnLogout.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnLogout.FlatAppearance.BorderSize = 0
        Me.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogout.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogout.ForeColor = System.Drawing.Color.Black
        Me.btnLogout.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnLogout.GradientForeColor = System.Drawing.Color.Black
        Me.btnLogout.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLogout.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnLogout.Location = New System.Drawing.Point(12, 12)
        Me.btnLogout.Name = "btnLogout"
        Me.btnLogout.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLogout.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnLogout.Size = New System.Drawing.Size(143, 30)
        Me.btnLogout.TabIndex = 5
        Me.btnLogout.Text = "Save &With Logout"
        Me.btnLogout.UseVisualStyleBackColor = False
        Me.btnLogout.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(188, 12)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(284, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbInOutinfo
        '
        Me.gbInOutinfo.BorderColor = System.Drawing.Color.Black
        Me.gbInOutinfo.Checked = False
        Me.gbInOutinfo.CollapseAllExceptThis = False
        Me.gbInOutinfo.CollapsedHoverImage = Nothing
        Me.gbInOutinfo.CollapsedNormalImage = Nothing
        Me.gbInOutinfo.CollapsedPressedImage = Nothing
        Me.gbInOutinfo.CollapseOnLoad = False
        Me.gbInOutinfo.Controls.Add(Me.lblCheckIntime)
        Me.gbInOutinfo.Controls.Add(Me.lblCheckouttime)
        Me.gbInOutinfo.Controls.Add(Me.dtpCheckouttime)
        Me.gbInOutinfo.Controls.Add(Me.dtpCheckIntime)
        Me.gbInOutinfo.Controls.Add(Me.lblcheckin)
        Me.gbInOutinfo.Controls.Add(Me.lblCheckout)
        Me.gbInOutinfo.ExpandedHoverImage = Nothing
        Me.gbInOutinfo.ExpandedNormalImage = Nothing
        Me.gbInOutinfo.ExpandedPressedImage = Nothing
        Me.gbInOutinfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInOutinfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInOutinfo.HeaderHeight = 25
        Me.gbInOutinfo.HeaderMessage = ""
        Me.gbInOutinfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbInOutinfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInOutinfo.HeightOnCollapse = 0
        Me.gbInOutinfo.LeftTextSpace = 0
        Me.gbInOutinfo.Location = New System.Drawing.Point(12, 11)
        Me.gbInOutinfo.Name = "gbInOutinfo"
        Me.gbInOutinfo.OpenHeight = 300
        Me.gbInOutinfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInOutinfo.ShowBorder = True
        Me.gbInOutinfo.ShowCheckBox = False
        Me.gbInOutinfo.ShowCollapseButton = False
        Me.gbInOutinfo.ShowDefaultBorderColor = True
        Me.gbInOutinfo.ShowDownButton = False
        Me.gbInOutinfo.ShowHeader = True
        Me.gbInOutinfo.Size = New System.Drawing.Size(361, 142)
        Me.gbInOutinfo.TabIndex = 0
        Me.gbInOutinfo.Temp = 0
        Me.gbInOutinfo.Text = "In/Out Time Information"
        Me.gbInOutinfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCheckIntime
        '
        Me.lblCheckIntime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblCheckIntime.Location = New System.Drawing.Point(48, 53)
        Me.lblCheckIntime.Name = "lblCheckIntime"
        Me.lblCheckIntime.Size = New System.Drawing.Size(56, 19)
        Me.lblCheckIntime.TabIndex = 5
        Me.lblCheckIntime.Text = "Time"
        Me.lblCheckIntime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCheckouttime
        '
        Me.lblCheckouttime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.lblCheckouttime.Location = New System.Drawing.Point(50, 110)
        Me.lblCheckouttime.Name = "lblCheckouttime"
        Me.lblCheckouttime.Size = New System.Drawing.Size(56, 19)
        Me.lblCheckouttime.TabIndex = 17
        Me.lblCheckouttime.Text = "Time"
        Me.lblCheckouttime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpCheckouttime
        '
        Me.dtpCheckouttime.CustomFormat = "dd/MM/yyyy hh:mm tt"
        Me.dtpCheckouttime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.dtpCheckouttime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpCheckouttime.Location = New System.Drawing.Point(112, 110)
        Me.dtpCheckouttime.Name = "dtpCheckouttime"
        Me.dtpCheckouttime.ShowUpDown = True
        Me.dtpCheckouttime.Size = New System.Drawing.Size(143, 21)
        Me.dtpCheckouttime.TabIndex = 2
        '
        'dtpCheckIntime
        '
        Me.dtpCheckIntime.CustomFormat = "dd/MM/yyyy hh:mm tt"
        Me.dtpCheckIntime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.dtpCheckIntime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpCheckIntime.Location = New System.Drawing.Point(110, 53)
        Me.dtpCheckIntime.Name = "dtpCheckIntime"
        Me.dtpCheckIntime.ShowUpDown = True
        Me.dtpCheckIntime.Size = New System.Drawing.Size(143, 21)
        Me.dtpCheckIntime.TabIndex = 1
        '
        'lblcheckin
        '
        Me.lblcheckin.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lblcheckin.Location = New System.Drawing.Point(19, 31)
        Me.lblcheckin.Name = "lblcheckin"
        Me.lblcheckin.Size = New System.Drawing.Size(329, 23)
        Me.lblcheckin.TabIndex = 1
        Me.lblcheckin.Text = "Check In detail"
        '
        'lblCheckout
        '
        Me.lblCheckout.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lblCheckout.Location = New System.Drawing.Point(19, 85)
        Me.lblCheckout.Name = "lblCheckout"
        Me.lblCheckout.Size = New System.Drawing.Size(329, 23)
        Me.lblCheckout.TabIndex = 4
        Me.lblCheckout.Text = "Check Out detail"
        '
        'frmTimesheet_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(386, 315)
        Me.Controls.Add(Me.pnlTime)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTimesheet_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Time"
        Me.pnlTime.ResumeLayout(False)
        Me.gbRemark.ResumeLayout(False)
        Me.gbRemark.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.gbInOutinfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTime As System.Windows.Forms.Panel
    Friend WithEvents gbInOutinfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCheckIntime As System.Windows.Forms.Label
    Friend WithEvents dtpCheckIntime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCheckout As eZee.Common.eZeeLine
    Friend WithEvents lblcheckin As eZee.Common.eZeeLine
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents lblCheckouttime As System.Windows.Forms.Label
    Friend WithEvents dtpCheckouttime As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbRemark As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents btnLogout As eZee.Common.eZeeLightButton
End Class
