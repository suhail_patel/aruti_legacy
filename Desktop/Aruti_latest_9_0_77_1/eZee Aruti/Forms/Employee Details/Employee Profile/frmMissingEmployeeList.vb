﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMissingEmployeeList

#Region " Private Varaibles "

    Private objEmployee As clsEmployee_Master
    Private ReadOnly mstrModuleName As String = "frmMissingEmployeeList"
    Private dvEmplst As DataView = Nothing
    Private mdtEmployee As DataTable = Nothing
    Private mblnIncludeInactiveEmployee As Boolean = True

#End Region

#Region " Private Function "

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Try
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges 
            If User._Object.Privilege._AllowToViewMissingEmployee = False Then Exit Sub
            'Varsha Rana (17-Oct-2017) -- End

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, True, mblnIncludeInactiveEmployee, "List", , , , , , , , , , , , , , , , , , False)

            mdtEmployee = dsList.Tables("List").Copy
            Dim dCol As New DataColumn
            With dCol
                .ColumnName = "ischeck"
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
            End With
            mdtEmployee.Columns.Add(dCol)

            dvEmplst = mdtEmployee.DefaultView
            dgvAEmployee.AutoGenerateColumns = False

            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "employeename"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgvAEmployee.DataSource = dvEmplst

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub


    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges
    Private Sub SetVisibility()

        Try

            btnOperations.Enabled = User._Object.Privilege._AllowToViewMissingEmployee

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
    'Varsha Rana (17-Oct-2017) -- End

#End Region

#Region " Form's Event(s) "

    Private Sub frmMissingEmployeeList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmployee = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMissingEmployeeList_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmMissingEmployeeList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmployee = New clsEmployee_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

            objSpc1.Panel2Collapsed = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMissingEmployeeList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Master.SetMessages()
            clsEmployee_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Master"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            dgvAEmployee.DataSource = Nothing
            objbtnReset.ShowResult(dgvAEmployee.RowCount.ToString)
            objSpc1.Panel2Collapsed = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
            objbtnSearch.ShowResult(dgvAEmployee.RowCount.ToString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If dvEmplst Is Nothing Then Exit Sub 'S.SANDEEP [26-SEP-2017] -- START -- END
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            dvEmplst.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGridview Event(s) "

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick, dgvAEmployee.CellContentDoubleClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dvEmplst.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dvEmplst.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub


#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
            For Each dr As DataRowView In dvEmplst
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvAEmployee.Refresh()
            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkShowActiveEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowActiveEmployee.CheckedChanged
        Try
            If chkShowActiveEmployee.Checked = True Then
                mblnIncludeInactiveEmployee = False
            Else
                mblnIncludeInactiveEmployee = True
            End If
            Call objbtnSearch_Click(New Object(), New EventArgs())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowActiveEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Menu Operations "

    Private Sub mnuExportAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportAll.Click
        Try
            If dgvAEmployee.RowCount > 0 Then
                objSpc1.Panel2Collapsed = True
                Dim dtExTab As DataTable = Nothing
                Dim ds = From dr In mdtEmployee Where (dr.Field(Of Boolean)("ischeck") = True) Select dr
                If ds.Count > 0 Then
                    dtExTab = ds.CopyToDataTable()
                    If dtExTab.Columns.Contains("employeeunkid") Then dtExTab.Columns.Remove("employeeunkid")
                    If dtExTab.Columns.Contains("isapproved") Then dtExTab.Columns.Remove("isapproved")
                    If dtExTab.Columns.Contains("EmpCodeName") Then dtExTab.Columns.Remove("EmpCodeName")
                    If dtExTab.Columns.Contains("ischeck") Then dtExTab.Columns.Remove("ischeck")
                End If

                Dim StrFilter As String = String.Empty
                StrFilter &= "," & Language.getMessage(mstrModuleName, 1, "Employee As On Date") & " : " & gfrmMDI.dtpEmployeeAsOnDate.Value.Date.ToShortDateString
                StrFilter &= "," & Language.getMessage(mstrModuleName, 2, "Total Employee") & " : " & dgvAEmployee.RowCount.ToString
                If chkShowActiveEmployee.Checked = True Then
                    StrFilter &= "," & chkShowActiveEmployee.Text
                Else
                    StrFilter &= "," & Language.getMessage(mstrModuleName, 3, "All Employee(s)")
                End If
                If StrFilter.Trim.Length > 0 Then StrFilter = Mid(StrFilter, 2)
                Dim objEmpListing As New ArutiReports.clsEmployee_Listing(User._Object._Languageunkid, Company._Object._Companyunkid)
                If objEmpListing.ListEmployee(StrFilter, mdtEmployee, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, Me.Text) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee List exported successfully to the given path."), enMsgBoxStyle.Information)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportAll_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuExportCheckedEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportCheckedEmployee.Click
        Try
            If dgvAEmployee.RowCount > 0 Then
                objSpc1.Panel2Collapsed = True
                If mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count > 0 Then
                    Dim dtExTab As DataTable = Nothing
                    Dim ds = From dr In mdtEmployee Where (dr.Field(Of Boolean)("ischeck") = True) Select dr
                    If ds.Count > 0 Then
                        dtExTab = ds.CopyToDataTable()

                        If dtExTab.Columns.Contains("employeeunkid") Then dtExTab.Columns.Remove("employeeunkid")
                        If dtExTab.Columns.Contains("isapproved") Then dtExTab.Columns.Remove("isapproved")
                        If dtExTab.Columns.Contains("EmpCodeName") Then dtExTab.Columns.Remove("EmpCodeName")
                        If dtExTab.Columns.Contains("ischeck") Then dtExTab.Columns.Remove("ischeck")

                        Dim StrFilter As String = String.Empty
                        StrFilter &= "," & Language.getMessage(mstrModuleName, 1, "Employee As On Date") & " : " & gfrmMDI.dtpEmployeeAsOnDate.Value.Date.ToShortDateString
                        StrFilter &= "," & Language.getMessage(mstrModuleName, 2, "Total Employee") & " : " & dgvAEmployee.RowCount.ToString
                        If chkShowActiveEmployee.Checked = True Then
                            StrFilter &= "," & chkShowActiveEmployee.Text
                        Else
                            StrFilter &= "," & Language.getMessage(mstrModuleName, 3, "All Employee(s)")
                        End If
                        If StrFilter.Trim.Length > 0 Then StrFilter = Mid(StrFilter, 2)
                        Dim objEmpListing As New ArutiReports.clsEmployee_Listing(User._Object._Languageunkid, Company._Object._Companyunkid)
                        If objEmpListing.ListEmployee(StrFilter, dtExTab, enPrintAction.None, enExportAction.ExcelExtra, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, Me.Text) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee List exported successfully to the given path."), enMsgBoxStyle.Information)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportCheckedEmployee_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuShowMovementDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuShowMovementDetails.Click
        Try
            If dgvAEmployee.RowCount > 0 Then
                objSpc1.Panel2Collapsed = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuShowMovementDetails_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub lnkGenerateDetails_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkGenerateDetails.LinkClicked
        Try
            If dgvAEmployee.RowCount > 0 Then
                If mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count > 0 Then
                    Dim strCheckedIds = String.Join(",", mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) CStr(y.Field(Of Integer)("employeeunkid"))).ToArray)
                    Dim objEmpListing As New ArutiReports.clsEmployee_Listing(User._Object._Languageunkid, Company._Object._Companyunkid)
                    objEmpListing.DisplayEmployeeMovements(strCheckedIds, chkShowTransfer.Checked, chkShowCategorization.Checked, chkShowCostCenters.Checked, chkShowDates.Checked, chkShowRehire.Checked, chkShowWorkPermit.Checked, enPrintAction.Preview, enExportAction.None, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, lblMovements.Text, ConfigParameter._Object._IsDisplayLogo, ConfigParameter._Object._ShowLogoRightSide)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkGenerateDetails_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbMissingEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMissingEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbMissingEmployeeInfo.Text = Language._Object.getCaption(Me.gbMissingEmployeeInfo.Name, Me.gbMissingEmployeeInfo.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lnkNote.Text = Language._Object.getCaption(Me.lnkNote.Name, Me.lnkNote.Text)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.chkShowActiveEmployee.Text = Language._Object.getCaption(Me.chkShowActiveEmployee.Name, Me.chkShowActiveEmployee.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuExportAll.Text = Language._Object.getCaption(Me.mnuExportAll.Name, Me.mnuExportAll.Text)
            Me.mnuExportCheckedEmployee.Text = Language._Object.getCaption(Me.mnuExportCheckedEmployee.Name, Me.mnuExportCheckedEmployee.Text)
            Me.mnuShowMovementDetails.Text = Language._Object.getCaption(Me.mnuShowMovementDetails.Name, Me.mnuShowMovementDetails.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee As On Date")
            Language.setMessage(mstrModuleName, 2, "Total Employee")
            Language.setMessage(mstrModuleName, 3, "All Employee(s)")
            Language.setMessage(mstrModuleName, 4, "Employee List exported successfully to the given path.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class