﻿Option Strict On

#Region "Import"
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmMembershipGlobalVoid

#Region "Private Variable"
    Private ReadOnly mstrModuleName As String = "frmMembershipGlobalVoid"
    Private mstrAdvanceFilter As String = ""
    Private dsEmployee As DataSet = Nothing
    Private dtView As DataView
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
#End Region

#Region "Form's Event"

    Private Sub frmMembershipGlobalVoid_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()
            Call SetVisiblity(False)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMembershipGlobalVoid_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            'Call SetMessages()

            'clsmembership_master.SetMessages()
            'objfrm._Other_ModuleNames = "clsmembership_master"
            objfrm.displayDialog(Me)

            'Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Method(S)"

    Private Sub FillCombo()
        Dim objCommon As New clsCommon_Master
        Dim dsList As DataSet
        Try
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, True, "List")
            With cboMembershipCate
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployee()
        Try
            If dsEmployee IsNot Nothing AndAlso dsEmployee.Tables(0).Rows.Count > 0 Then
                dtView = dsEmployee.Tables(0).DefaultView
                objcolhChecked.DataPropertyName = "Ischecked"
                colhEmployeeCode.DataPropertyName = "employeecode"
                colhEmployeeName.DataPropertyName = "employeename"
                objcolhEmployeeunkid.DataPropertyName = "employeeunkid"
                'objImgStatus.DataPropertyName = "image"
                dgvEmployee.AutoGenerateColumns = False
                dgvEmployee.DataSource = dtView
            Else
                dgvEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            mstrAdvanceFilter = ""
            cboMembershipCate.SelectedValue = 0
            cboMembership.SelectedValue = 0
            dsEmployee = Nothing
            dtView = Nothing
            dgvEmployee.DataSource = New List(Of String)
            objChkAllCheked.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisiblity(ByVal bln As Boolean)
        Try
            'objcolhStatus.Visible = bln
            'If bln = False Then
            '    colhEmployeeName.Width = CInt(CInt(colhEmployeeName.Width) + CInt(objcolhStatus.Width))
            'Else
            '    colhEmployeeName.Width = CInt(CInt(colhEmployeeName.Width) - CInt(objcolhStatus.Width))
            'End If

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            'btnVoid.Enabled = Not bln

            If User._Object.Privilege._AllowToGlobalVoidEmployeeMembership = True Then
                btnVoid.Enabled = Not bln
            Else
                btnVoid.Enabled = bln
            End If

            'Varsha Rana (17-Oct-2017) -- End
            btnFilter.Enabled = bln
            objbtnSearch.Enabled = Not bln
            objbtnReset.Enabled = Not bln
            lnkAllocation.Enabled = Not bln
            objChkAllCheked.Enabled = Not bln
            objcolhChecked.ReadOnly = bln
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisiblity", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidationData() As Boolean
        Try
            If dsEmployee Is Nothing OrElse dsEmployee.Tables(0).Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Check Employee(s) from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                Return False
            End If

            If dsEmployee.Tables(0).Select("IsChecked = '" & True & "'").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Check Employee(s) from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return True
    End Function
#End Region

#Region "Button's Event"

    Private Sub btnVoid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        Dim dtTran As DataTable = Nothing
        Dim objMembershipTran As clsMembershipTran
        Dim dtRow As DataRow
        Try
            If dsEmployee Is Nothing Then Exit Sub
            dsEmployee.Tables(0).AcceptChanges()
            If IsValidationData() = False Then Exit Sub

            objMembershipTran = New clsMembershipTran
            dtTran = objMembershipTran._DataList.Clone
            Call SetVisiblity(True)

            objcolhChecked.DataPropertyName = "Ischecked"
            colhEmployeeCode.DataPropertyName = "employeecode"
            colhEmployeeName.DataPropertyName = "employeename"
            objcolhEmployeeunkid.DataPropertyName = "employeeunkid"
            objcolhStatus.DataPropertyName = "message"
            objImgStatus.DataPropertyName = "image"
            dgvEmployee.AutoGenerateColumns = False
            dtView = New DataView(dsEmployee.Tables(0), "IsChecked = '" & True & "'", "", DataViewRowState.CurrentRows)
            dgvEmployee.DataSource = dtView
            Application.DoEvents()

            dgvEmployee.DataSource = dtView

            For Each dRow As DataRow In dsEmployee.Tables(0).Select("IsChecked = '" & True & "'")
                Try
                    dgvEmployee.FirstDisplayedScrollingRowIndex = dsEmployee.Tables(0).Rows.IndexOf(dRow) - 15
                    Application.DoEvents()
                Catch ex As Exception
                    Application.DoEvents()
                End Try

                dtRow = dtTran.NewRow
                dtRow.Item("membershiptranunkid") = dRow.Item("membershiptranunkid")
                dtRow.Item("emptrnheadid") = dRow.Item("emptranheadunkid")
                dtRow.Item("cotrnheadid") = dRow.Item("cotranheadunkid")
                dtRow.Item("membershiptranunkid") = dRow.Item("membershiptranunkid")
                dtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
                dtRow.Item("isdeleted") = True
                dtRow.Item("AUD") = "D"
                dtTran.Rows.Add(dtRow)
                objMembershipTran._DataList = dtTran
                Try

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objMembershipTran._FormName = mstrModuleName
                    objMembershipTran._LoginEmployeeunkid = 0
                    objMembershipTran._ClientIP = getIP()
                    objMembershipTran._HostName = getHostName()
                    objMembershipTran._FromWeb = False
                    objMembershipTran._AuditUserId = User._Object._Userunkid
objMembershipTran._CompanyUnkid = Company._Object._Companyunkid
                    objMembershipTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    If objMembershipTran.InsertUpdateDelete_MembershipTran(FinancialYear._Object._DatabaseName, _
                                                                       User._Object._Userunkid, _
                                                                       FinancialYear._Object._YearUnkid, _
                                                                       Company._Object._Companyunkid, _
                                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                       ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                       ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                       User._Object.Privilege._AllowToApproveEarningDeduction, _
                                                                       ConfigParameter._Object._CurrentDateAndTime, Nothing) = False Then

                        dRow.Item("image") = imgError
                        dRow.Item("objStatus") = 2
                        dRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Fail") & " " & objMembershipTran._ErrorMessage.ToString
                    Else
                        dRow.Item("image") = imgAccept
                        dRow.Item("objStatus") = 1
                        dRow.Item("message") = Language.getMessage(mstrModuleName, 8, "Success")
                    End If
                Catch ex As Exception
                    dRow.Item("image") = imgError
                    dRow.Item("objStatus") = 2
                    dRow.Item("message") = Language.getMessage(mstrModuleName, 7, "Fail") & " " & objMembershipTran._ErrorMessage.ToString
                End Try
                dtTran = dtTran.Clone
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnVoid_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMembership.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboMembership.ValueMember
                .DisplayMember = cboMembership.DisplayMember
                .DataSource = CType(cboMembership.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboMembership.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMembership_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchMembershipCate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchMembershipCate.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboMembershipCate.ValueMember
                .DisplayMember = cboMembershipCate.DisplayMember
                .DataSource = CType(cboMembershipCate.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboMembershipCate.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMembershipCate_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboMembershipCate.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Membership Category from the comobox to perform further operation on it."), enMsgBoxStyle.Information) '?1
                cboMembershipCate.Focus()
                Exit Sub
            End If

            If CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Membership from the combobox to perform further operation on it."), enMsgBoxStyle.Information) '?1
                cboMembership.Focus()
                Exit Sub
            End If
            dsEmployee = (New clsMembershipTran).GetEmployee_By_Membership(FinancialYear._Object._DatabaseName, _
                                                                           User._Object._Userunkid, _
                                                                           FinancialYear._Object._YearUnkid, _
                                                                           Company._Object._Companyunkid, _
                                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                           ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                           ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                           CInt(cboMembership.SelectedValue), "List", , mstrAdvanceFilter)

            dsEmployee.Tables(0).Columns.Add("Ischecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            dsEmployee.Tables(0).Columns.Add("objStatus", System.Type.GetType("System.String"))
            dsEmployee.Tables(0).Columns.Add("image", System.Type.GetType("System.Object")) '.DefaultValue = New Drawing.Bitmap(1, 1).Clone
            dsEmployee.Tables(0).Columns.Add("message", System.Type.GetType("System.String"))
            Call FillEmployee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Controls Event(S)"

    Private Sub cboMembershipCate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMembershipCate.SelectedIndexChanged
        Dim objMembership As New clsmembership_master
        Dim dsList As DataSet
        Try
            dsList = objMembership.getListForCombo("List", True, CInt(cboMembershipCate.SelectedValue))
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMembershipCate_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmployee_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmployee.TextChanged
        Try
            If dtView Is Nothing Then Exit Sub
            Dim StrSearch As String = ""
            If txtSearchEmployee.Text.Trim.Length > 0 Then
                StrSearch = " AND employeecode LIKE '%" & txtSearchEmployee.Text.Trim & "%' OR employeename LIKE '%" & txtSearchEmployee.Text.Trim & "%' "
            End If

            If btnVoid.Enabled = False Then
                StrSearch &= " AND IsChecked = '" & True & "'"
            End If

            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(4)
            End If

            dtView.RowFilter = StrSearch
            dgvEmployee.DataSource = dtView
            RemoveHandler objChkAllCheked.CheckedChanged, AddressOf objChkAllCheked_CheckedChanged
            If dtView.ToTable().Select("Ischecked = '" & True & "' ").Length = dgvEmployee.Rows.Count Then
                objChkAllCheked.CheckState = CheckState.Checked
            ElseIf dtView.ToTable().Select("Ischecked = '" & True & "' ").Length = 0 Then
                objChkAllCheked.CheckState = CheckState.Unchecked
            Else
                objChkAllCheked.CheckState = CheckState.Indeterminate
            End If
            AddHandler objChkAllCheked.CheckedChanged, AddressOf objChkAllCheked_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmployee_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub objChkAllCheked_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objChkAllCheked.CheckedChanged
        Try
            If dgvEmployee.Rows.Count <= 0 Then Exit Sub
            RemoveHandler dgvEmployee.CellContentClick, AddressOf dgvEmployee_CellContentClick
            For Each dgRow As DataGridViewRow In dgvEmployee.Rows
                dgRow.Cells(objcolhChecked.Index).Value = objChkAllCheked.Checked
            Next
            AddHandler dgvEmployee.CellContentClick, AddressOf dgvEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAllCheked_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvEmployee_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmployee.CellContentClick
        Try
            If Me.dgvEmployee.IsCurrentCellDirty Then
                Me.dgvEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If e.ColumnIndex = objcolhChecked.Index Then
                RemoveHandler objChkAllCheked.CheckedChanged, AddressOf objChkAllCheked_CheckedChanged
                If dtView.ToTable().Select("Ischecked = '" & True & "' ").Length = dgvEmployee.Rows.Count Then
                    objChkAllCheked.CheckState = CheckState.Checked
                ElseIf dtView.ToTable().Select("Ischecked = '" & True & "' ").Length = 0 Then
                    objChkAllCheked.CheckState = CheckState.Unchecked
                Else
                    objChkAllCheked.CheckState = CheckState.Indeterminate
                End If
                AddHandler objChkAllCheked.CheckedChanged, AddressOf objChkAllCheked_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Menu Event(S)"
    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dtView.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dtView.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    For Each dCol As DataColumn In dsEmployee.Tables(0).Columns
                        Select Case dCol.ColumnName.ToUpper
                            Case "EMPLOYEECODE", "EMPLOYEENAME", "MESSAGE"

                            Case Else
                                If dtTable.Columns.Contains(dCol.ColumnName) Then
                                    dtTable.Columns.Remove(dCol.ColumnName)
                                End If
                        End Select
                    Next

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Qalification Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dtView.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dtView.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dtView.RowFilter = "IsChecked = '" & True & "'"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnVoid.GradientBackColor = GUI._ButttonBackColor
            Me.btnVoid.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblMembershipCategory.Text = Language._Object.getCaption(Me.lblMembershipCategory.Name, Me.lblMembershipCategory.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnVoid.Text = Language._Object.getCaption(Me.btnVoid.Name, Me.btnVoid.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.colhEmployeeCode.HeaderText = Language._Object.getCaption(Me.colhEmployeeCode.Name, Me.colhEmployeeCode.HeaderText)
            Me.colhEmployeeName.HeaderText = Language._Object.getCaption(Me.colhEmployeeName.Name, Me.colhEmployeeName.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Membership Category from the comobox to perform further operation on it.")
            Language.setMessage(mstrModuleName, 2, "Please select Membership from the combobox to perform further operation on it.")
            Language.setMessage(mstrModuleName, 3, "Please Check Employee(s) from the list to perform further operation on it.")
            Language.setMessage(mstrModuleName, 7, "Fail")
            Language.setMessage(mstrModuleName, 8, "Success")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class