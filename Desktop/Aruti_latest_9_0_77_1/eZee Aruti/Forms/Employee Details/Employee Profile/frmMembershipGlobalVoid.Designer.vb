﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMembershipGlobalVoid
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMembershipGlobalVoid))
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.cboMembershipCate = New System.Windows.Forms.ComboBox
        Me.lblMembershipCategory = New System.Windows.Forms.Label
        Me.objbtnSearchMembershipCate = New eZee.Common.eZeeGradientButton
        Me.objfrmFooter = New eZee.Common.eZeeFooter
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnVoid = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlEmployee = New System.Windows.Forms.Panel
        Me.objChkAllCheked = New System.Windows.Forms.CheckBox
        Me.dgvEmployee = New System.Windows.Forms.DataGridView
        Me.txtSearchEmployee = New System.Windows.Forms.TextBox
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchMembership = New eZee.Common.eZeeGradientButton
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblMembership = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhChecked = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objImgStatus = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployeeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmployeeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objfrmFooter.SuspendLayout()
        Me.cmsFilter.SuspendLayout()
        Me.pnlEmployee.SuspendLayout()
        CType(Me.dgvEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(15, 70)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(302, 16)
        Me.objelLine1.TabIndex = 8
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembershipCate
        '
        Me.cboMembershipCate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembershipCate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembershipCate.FormattingEnabled = True
        Me.cboMembershipCate.Location = New System.Drawing.Point(156, 12)
        Me.cboMembershipCate.Name = "cboMembershipCate"
        Me.cboMembershipCate.Size = New System.Drawing.Size(286, 21)
        Me.cboMembershipCate.TabIndex = 12
        '
        'lblMembershipCategory
        '
        Me.lblMembershipCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembershipCategory.Location = New System.Drawing.Point(12, 13)
        Me.lblMembershipCategory.Name = "lblMembershipCategory"
        Me.lblMembershipCategory.Size = New System.Drawing.Size(138, 18)
        Me.lblMembershipCategory.TabIndex = 11
        Me.lblMembershipCategory.Text = "Membership Category"
        Me.lblMembershipCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchMembershipCate
        '
        Me.objbtnSearchMembershipCate.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMembershipCate.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembershipCate.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembershipCate.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMembershipCate.BorderSelected = False
        Me.objbtnSearchMembershipCate.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMembershipCate.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMembershipCate.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMembershipCate.Location = New System.Drawing.Point(448, 12)
        Me.objbtnSearchMembershipCate.Name = "objbtnSearchMembershipCate"
        Me.objbtnSearchMembershipCate.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMembershipCate.TabIndex = 13
        '
        'objfrmFooter
        '
        Me.objfrmFooter.BorderColor = System.Drawing.Color.Silver
        Me.objfrmFooter.Controls.Add(Me.btnFilter)
        Me.objfrmFooter.Controls.Add(Me.btnClose)
        Me.objfrmFooter.Controls.Add(Me.btnVoid)
        Me.objfrmFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objfrmFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objfrmFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objfrmFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objfrmFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objfrmFooter.Location = New System.Drawing.Point(0, 440)
        Me.objfrmFooter.Name = "objfrmFooter"
        Me.objfrmFooter.Size = New System.Drawing.Size(481, 55)
        Me.objfrmFooter.TabIndex = 14
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(5, 12)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(107, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 23
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 92)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(375, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 85
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnVoid
        '
        Me.btnVoid.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVoid.BackColor = System.Drawing.Color.White
        Me.btnVoid.BackgroundImage = CType(resources.GetObject("btnVoid.BackgroundImage"), System.Drawing.Image)
        Me.btnVoid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVoid.BorderColor = System.Drawing.Color.Empty
        Me.btnVoid.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnVoid.FlatAppearance.BorderSize = 0
        Me.btnVoid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVoid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoid.ForeColor = System.Drawing.Color.Black
        Me.btnVoid.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVoid.GradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoid.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Location = New System.Drawing.Point(276, 12)
        Me.btnVoid.Name = "btnVoid"
        Me.btnVoid.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoid.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Size = New System.Drawing.Size(94, 30)
        Me.btnVoid.TabIndex = 84
        Me.btnVoid.Text = "&Void"
        Me.btnVoid.UseVisualStyleBackColor = True
        '
        'pnlEmployee
        '
        Me.pnlEmployee.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnlEmployee.Controls.Add(Me.objChkAllCheked)
        Me.pnlEmployee.Controls.Add(Me.dgvEmployee)
        Me.pnlEmployee.Controls.Add(Me.txtSearchEmployee)
        Me.pnlEmployee.Location = New System.Drawing.Point(0, 96)
        Me.pnlEmployee.Name = "pnlEmployee"
        Me.pnlEmployee.Size = New System.Drawing.Size(480, 342)
        Me.pnlEmployee.TabIndex = 15
        '
        'objChkAllCheked
        '
        Me.objChkAllCheked.AutoSize = True
        Me.objChkAllCheked.Location = New System.Drawing.Point(11, 33)
        Me.objChkAllCheked.Name = "objChkAllCheked"
        Me.objChkAllCheked.Size = New System.Drawing.Size(15, 14)
        Me.objChkAllCheked.TabIndex = 2
        Me.objChkAllCheked.UseVisualStyleBackColor = True
        '
        'dgvEmployee
        '
        Me.dgvEmployee.AllowUserToAddRows = False
        Me.dgvEmployee.AllowUserToDeleteRows = False
        Me.dgvEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhChecked, Me.objImgStatus, Me.colhEmployeeCode, Me.colhEmployeeName, Me.objcolhStatus, Me.objcolhEmployeeunkid})
        Me.dgvEmployee.Location = New System.Drawing.Point(4, 27)
        Me.dgvEmployee.Name = "dgvEmployee"
        Me.dgvEmployee.RowHeadersVisible = False
        Me.dgvEmployee.Size = New System.Drawing.Size(471, 312)
        Me.dgvEmployee.TabIndex = 1
        '
        'txtSearchEmployee
        '
        Me.txtSearchEmployee.Location = New System.Drawing.Point(5, 3)
        Me.txtSearchEmployee.Name = "txtSearchEmployee"
        Me.txtSearchEmployee.Size = New System.Drawing.Size(471, 21)
        Me.txtSearchEmployee.TabIndex = 0
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(323, 70)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(99, 16)
        Me.lnkAllocation.TabIndex = 104
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'objbtnReset
        '
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(448, 66)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 106
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(421, 66)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 105
        Me.objbtnSearch.TabStop = False
        '
        'objbtnSearchMembership
        '
        Me.objbtnSearchMembership.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMembership.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMembership.BorderSelected = False
        Me.objbtnSearchMembership.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMembership.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMembership.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMembership.Location = New System.Drawing.Point(448, 39)
        Me.objbtnSearchMembership.Name = "objbtnSearchMembership"
        Me.objbtnSearchMembership.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMembership.TabIndex = 109
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(156, 39)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(286, 21)
        Me.cboMembership.TabIndex = 108
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(12, 40)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(138, 18)
        Me.lblMembership.TabIndex = 107
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 110
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "objcolhEmployeeunkid"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'objcolhChecked
        '
        Me.objcolhChecked.HeaderText = ""
        Me.objcolhChecked.Name = "objcolhChecked"
        Me.objcolhChecked.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhChecked.Width = 25
        '
        'objImgStatus
        '
        Me.objImgStatus.HeaderText = ""
        Me.objImgStatus.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.objImgStatus.Name = "objImgStatus"
        Me.objImgStatus.ReadOnly = True
        Me.objImgStatus.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objImgStatus.Width = 30
        '
        'colhEmployeeCode
        '
        Me.colhEmployeeCode.HeaderText = "Employee Code"
        Me.colhEmployeeCode.Name = "colhEmployeeCode"
        Me.colhEmployeeCode.ReadOnly = True
        Me.colhEmployeeCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhEmployeeCode.Width = 90
        '
        'colhEmployeeName
        '
        Me.colhEmployeeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployeeName.HeaderText = "Employee Name"
        Me.colhEmployeeName.Name = "colhEmployeeName"
        Me.colhEmployeeName.ReadOnly = True
        Me.colhEmployeeName.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colhEmployeeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objcolhStatus
        '
        Me.objcolhStatus.HeaderText = "Status"
        Me.objcolhStatus.Name = "objcolhStatus"
        Me.objcolhStatus.ReadOnly = True
        Me.objcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhStatus.Width = 110
        '
        'objcolhEmployeeunkid
        '
        Me.objcolhEmployeeunkid.HeaderText = "objcolhEmployeeunkid"
        Me.objcolhEmployeeunkid.Name = "objcolhEmployeeunkid"
        Me.objcolhEmployeeunkid.ReadOnly = True
        Me.objcolhEmployeeunkid.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhEmployeeunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhEmployeeunkid.Visible = False
        '
        'frmMembershipGlobalVoid
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(481, 495)
        Me.Controls.Add(Me.objbtnSearchMembership)
        Me.Controls.Add(Me.cboMembership)
        Me.Controls.Add(Me.lblMembership)
        Me.Controls.Add(Me.objbtnReset)
        Me.Controls.Add(Me.objbtnSearch)
        Me.Controls.Add(Me.lnkAllocation)
        Me.Controls.Add(Me.pnlEmployee)
        Me.Controls.Add(Me.objfrmFooter)
        Me.Controls.Add(Me.objbtnSearchMembershipCate)
        Me.Controls.Add(Me.cboMembershipCate)
        Me.Controls.Add(Me.lblMembershipCategory)
        Me.Controls.Add(Me.objelLine1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMembershipGlobalVoid"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Global Void Membership"
        Me.objfrmFooter.ResumeLayout(False)
        Me.cmsFilter.ResumeLayout(False)
        Me.pnlEmployee.ResumeLayout(False)
        Me.pnlEmployee.PerformLayout()
        CType(Me.dgvEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents cboMembershipCate As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembershipCategory As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchMembershipCate As eZee.Common.eZeeGradientButton
    Friend WithEvents objfrmFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnVoid As eZee.Common.eZeeLightButton
    Friend WithEvents pnlEmployee As System.Windows.Forms.Panel
    Friend WithEvents txtSearchEmployee As System.Windows.Forms.TextBox
    Friend WithEvents dgvEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objChkAllCheked As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchMembership As eZee.Common.eZeeGradientButton
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhChecked As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objImgStatus As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployeeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmployeeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
