﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeSkills_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeSkills_AddEdit))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbSkillRegister = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddSkill = New eZee.Common.eZeeGradientButton
        Me.objbtnAddCategory = New eZee.Common.eZeeGradientButton
        Me.lnSkillInfo = New eZee.Common.eZeeLine
        Me.lnEmployeeName = New eZee.Common.eZeeLine
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboSkillCategory = New System.Windows.Forms.ComboBox
        Me.lblSkillcategory = New System.Windows.Forms.Label
        Me.txtSkillDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lblSkillDescription = New System.Windows.Forms.Label
        Me.cboSkill = New System.Windows.Forms.ComboBox
        Me.lblSkill = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSaveInfo = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1.SuspendLayout()
        Me.gbSkillRegister.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.eZeeHeader)
        Me.Panel1.Controls.Add(Me.gbSkillRegister)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(539, 327)
        Me.Panel1.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(539, 60)
        Me.eZeeHeader.TabIndex = 27
        Me.eZeeHeader.Title = "Employee Skills"
        '
        'gbSkillRegister
        '
        Me.gbSkillRegister.BorderColor = System.Drawing.Color.Black
        Me.gbSkillRegister.Checked = False
        Me.gbSkillRegister.CollapseAllExceptThis = False
        Me.gbSkillRegister.CollapsedHoverImage = Nothing
        Me.gbSkillRegister.CollapsedNormalImage = Nothing
        Me.gbSkillRegister.CollapsedPressedImage = Nothing
        Me.gbSkillRegister.CollapseOnLoad = False
        Me.gbSkillRegister.Controls.Add(Me.objbtnAddSkill)
        Me.gbSkillRegister.Controls.Add(Me.objbtnAddCategory)
        Me.gbSkillRegister.Controls.Add(Me.lnSkillInfo)
        Me.gbSkillRegister.Controls.Add(Me.lnEmployeeName)
        Me.gbSkillRegister.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbSkillRegister.Controls.Add(Me.cboEmployee)
        Me.gbSkillRegister.Controls.Add(Me.lblEmployee)
        Me.gbSkillRegister.Controls.Add(Me.cboSkillCategory)
        Me.gbSkillRegister.Controls.Add(Me.lblSkillcategory)
        Me.gbSkillRegister.Controls.Add(Me.txtSkillDescription)
        Me.gbSkillRegister.Controls.Add(Me.lblSkillDescription)
        Me.gbSkillRegister.Controls.Add(Me.cboSkill)
        Me.gbSkillRegister.Controls.Add(Me.lblSkill)
        Me.gbSkillRegister.ExpandedHoverImage = Nothing
        Me.gbSkillRegister.ExpandedNormalImage = Nothing
        Me.gbSkillRegister.ExpandedPressedImage = Nothing
        Me.gbSkillRegister.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSkillRegister.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSkillRegister.HeaderHeight = 25
        Me.gbSkillRegister.HeaderMessage = ""
        Me.gbSkillRegister.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSkillRegister.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSkillRegister.HeightOnCollapse = 0
        Me.gbSkillRegister.LeftTextSpace = 0
        Me.gbSkillRegister.Location = New System.Drawing.Point(12, 66)
        Me.gbSkillRegister.Name = "gbSkillRegister"
        Me.gbSkillRegister.OpenHeight = 208
        Me.gbSkillRegister.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSkillRegister.ShowBorder = True
        Me.gbSkillRegister.ShowCheckBox = False
        Me.gbSkillRegister.ShowCollapseButton = False
        Me.gbSkillRegister.ShowDefaultBorderColor = True
        Me.gbSkillRegister.ShowDownButton = False
        Me.gbSkillRegister.ShowHeader = True
        Me.gbSkillRegister.Size = New System.Drawing.Size(516, 200)
        Me.gbSkillRegister.TabIndex = 0
        Me.gbSkillRegister.Temp = 0
        Me.gbSkillRegister.Text = "Skill Register"
        Me.gbSkillRegister.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddSkill
        '
        Me.objbtnAddSkill.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddSkill.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddSkill.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddSkill.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddSkill.BorderSelected = False
        Me.objbtnAddSkill.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddSkill.Image = CType(resources.GetObject("objbtnAddSkill.Image"), System.Drawing.Image)
        Me.objbtnAddSkill.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddSkill.Location = New System.Drawing.Point(484, 100)
        Me.objbtnAddSkill.Name = "objbtnAddSkill"
        Me.objbtnAddSkill.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddSkill.TabIndex = 10
        '
        'objbtnAddCategory
        '
        Me.objbtnAddCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCategory.BorderSelected = False
        Me.objbtnAddCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCategory.Image = CType(resources.GetObject("objbtnAddCategory.Image"), System.Drawing.Image)
        Me.objbtnAddCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCategory.Location = New System.Drawing.Point(257, 100)
        Me.objbtnAddCategory.Name = "objbtnAddCategory"
        Me.objbtnAddCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCategory.TabIndex = 7
        '
        'lnSkillInfo
        '
        Me.lnSkillInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnSkillInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnSkillInfo.Location = New System.Drawing.Point(8, 80)
        Me.lnSkillInfo.Name = "lnSkillInfo"
        Me.lnSkillInfo.Size = New System.Drawing.Size(197, 17)
        Me.lnSkillInfo.TabIndex = 4
        Me.lnSkillInfo.Text = "Skill Info"
        '
        'lnEmployeeName
        '
        Me.lnEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmployeeName.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmployeeName.Location = New System.Drawing.Point(8, 36)
        Me.lnEmployeeName.Name = "lnEmployeeName"
        Me.lnEmployeeName.Size = New System.Drawing.Size(197, 17)
        Me.lnEmployeeName.TabIndex = 0
        Me.lnEmployeeName.Text = "Employee Info"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(484, 56)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 3
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(118, 56)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(360, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(22, 59)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(90, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        '
        'cboSkillCategory
        '
        Me.cboSkillCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSkillCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSkillCategory.FormattingEnabled = True
        Me.cboSkillCategory.Location = New System.Drawing.Point(118, 100)
        Me.cboSkillCategory.Name = "cboSkillCategory"
        Me.cboSkillCategory.Size = New System.Drawing.Size(133, 21)
        Me.cboSkillCategory.TabIndex = 6
        '
        'lblSkillcategory
        '
        Me.lblSkillcategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSkillcategory.Location = New System.Drawing.Point(22, 103)
        Me.lblSkillcategory.Name = "lblSkillcategory"
        Me.lblSkillcategory.Size = New System.Drawing.Size(90, 15)
        Me.lblSkillcategory.TabIndex = 5
        Me.lblSkillcategory.Text = "Skill Category"
        Me.lblSkillcategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSkillDescription
        '
        Me.txtSkillDescription.Flags = 0
        Me.txtSkillDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSkillDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSkillDescription.Location = New System.Drawing.Point(118, 127)
        Me.txtSkillDescription.Multiline = True
        Me.txtSkillDescription.Name = "txtSkillDescription"
        Me.txtSkillDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSkillDescription.Size = New System.Drawing.Size(360, 64)
        Me.txtSkillDescription.TabIndex = 12
        '
        'lblSkillDescription
        '
        Me.lblSkillDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSkillDescription.Location = New System.Drawing.Point(22, 127)
        Me.lblSkillDescription.Name = "lblSkillDescription"
        Me.lblSkillDescription.Size = New System.Drawing.Size(90, 15)
        Me.lblSkillDescription.TabIndex = 11
        Me.lblSkillDescription.Text = "Description"
        Me.lblSkillDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSkill
        '
        Me.cboSkill.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSkill.FormattingEnabled = True
        Me.cboSkill.Location = New System.Drawing.Point(345, 100)
        Me.cboSkill.Name = "cboSkill"
        Me.cboSkill.Size = New System.Drawing.Size(133, 21)
        Me.cboSkill.TabIndex = 9
        '
        'lblSkill
        '
        Me.lblSkill.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSkill.Location = New System.Drawing.Point(284, 103)
        Me.lblSkill.Name = "lblSkill"
        Me.lblSkill.Size = New System.Drawing.Size(55, 15)
        Me.lblSkill.TabIndex = 8
        Me.lblSkill.Text = "Skill"
        Me.lblSkill.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSaveInfo)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 272)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(539, 55)
        Me.objFooter.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(434, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 27
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSaveInfo
        '
        Me.btnSaveInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveInfo.BackColor = System.Drawing.Color.White
        Me.btnSaveInfo.BackgroundImage = CType(resources.GetObject("btnSaveInfo.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveInfo.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveInfo.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveInfo.FlatAppearance.BorderSize = 0
        Me.btnSaveInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveInfo.ForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveInfo.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveInfo.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.Location = New System.Drawing.Point(335, 13)
        Me.btnSaveInfo.Name = "btnSaveInfo"
        Me.btnSaveInfo.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveInfo.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.Size = New System.Drawing.Size(93, 30)
        Me.btnSaveInfo.TabIndex = 26
        Me.btnSaveInfo.Text = "&Save"
        Me.btnSaveInfo.UseVisualStyleBackColor = True
        '
        'frmEmployeeSkills_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(539, 327)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeSkills_AddEdit"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Skills"
        Me.Panel1.ResumeLayout(False)
        Me.gbSkillRegister.ResumeLayout(False)
        Me.gbSkillRegister.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbSkillRegister As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboSkillCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblSkillcategory As System.Windows.Forms.Label
    Friend WithEvents txtSkillDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSkillDescription As System.Windows.Forms.Label
    Friend WithEvents cboSkill As System.Windows.Forms.ComboBox
    Friend WithEvents lblSkill As System.Windows.Forms.Label
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSaveInfo As eZee.Common.eZeeLightButton
    Friend WithEvents lnSkillInfo As eZee.Common.eZeeLine
    Friend WithEvents lnEmployeeName As eZee.Common.eZeeLine
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objbtnAddSkill As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddCategory As eZee.Common.eZeeGradientButton
End Class
