﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAddressImportWizard

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmAdressImportWizard"
    Private mds_ImportData As DataSet
    Private mdt_ImportData As New DataTable
    Dim dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)

    'Gajanan [27-May-2019] -- Start
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [27-May-2019] -- End

#End Region

#Region " Display Dialog "

    Public Sub displayDialog()
        Try
            Me.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAdressImportWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Fill_Combo()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAdressImportWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportImgs_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportImgs.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportImgs.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportImgs_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportImgs_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportImgs.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportImgs.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                    If cboSelectType.SelectedIndex <= -1 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Address Type cannot be blank. Please set Address Type to continue"), enMsgBoxStyle.Information)
                        e.Cancel = True
                        Exit Sub
                    End If


                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    Call SetVisiblity()
                    Call SetDataCombo()
                    'cboEmployeeCode.SelectedIndex = 0
                    'cboAddress1.SelectedIndex = 1
                    'cboAddress2.SelectedIndex = 2
                    'cboPostCountry.SelectedIndex = 3
                    'cboState.SelectedIndex = 4
                    'cboPostTown.SelectedIndex = 5
                    'cboPostCode.SelectedIndex = 6
                    'cboProvRegion.SelectedIndex = 7
                    'cboRoadStreet.SelectedIndex = 8
                    'cboEState.SelectedIndex = 9
                    'cboProvRegion1.SelectedIndex = 10
                    'cboRoadStreet1.SelectedIndex = 11
                    'cboChiefdom.SelectedIndex = 12
                    'cboVillage.SelectedIndex = 13
                    'cboTown1.SelectedIndex = 14
                    'cboTelNo.SelectedIndex = 16
                    'cboPlotNo.SelectedIndex = 17

                    'If cboSelectType.SelectedIndex <> 2 Then
                    '    cboMobile.SelectedIndex = 15
                    '    cboAlternativeNo.SelectedIndex = 18
                    '    cboEmail.SelectedIndex = 19
                    '    cboFax.SelectedIndex = 20
                    'End If
                Case eZeeWizImportImgs.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        'For Each ctrl As Control In gbFieldMapping.Controls
                        '    If TypeOf ctrl Is ComboBox Then
                        '        If ctrl.Visible = True Then
                        '            If CType(ctrl, ComboBox).Text = "" Then
                        '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please the select proper field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        '                e.Cancel = True
                        '                Exit Sub
                        '            End If
                        '        End If
                        '    End If
                        'Next
                        If cboEmployeeCode.Text = "" Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If
                Case eZeeWizImportImgs.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportImgs_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            'For Each ctrl As Control In gbFieldMapping.Controls
            '    If TypeOf ctrl Is ComboBox Then
            '        Call ClearCombo(CType(ctrl, ComboBox))
            '    End If
            'Next

            'For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
            '    cboEmployeeCode.Items.Add(dtColumns.ColumnName)
            '    cboAddress1.Items.Add(dtColumns.ColumnName)
            '    cboAddress2.Items.Add(dtColumns.ColumnName)
            '    cboPostCountry.Items.Add(dtColumns.ColumnName)
            '    cboState.Items.Add(dtColumns.ColumnName)
            '    cboPostTown.Items.Add(dtColumns.ColumnName)
            '    cboPostCode.Items.Add(dtColumns.ColumnName)
            '    cboProvRegion.Items.Add(dtColumns.ColumnName)
            '    cboRoadStreet.Items.Add(dtColumns.ColumnName)
            '    cboEState.Items.Add(dtColumns.ColumnName)
            '    cboProvRegion1.Items.Add(dtColumns.ColumnName)
            '    cboRoadStreet1.Items.Add(dtColumns.ColumnName)
            '    cboChiefdom.Items.Add(dtColumns.ColumnName)
            '    cboVillage.Items.Add(dtColumns.ColumnName)
            '    cboTown1.Items.Add(dtColumns.ColumnName)
            '    cboMobile.Items.Add(dtColumns.ColumnName)
            '    cboTelNo.Items.Add(dtColumns.ColumnName)
            '    cboPlotNo.Items.Add(dtColumns.ColumnName)
            '    cboAlternativeNo.Items.Add(dtColumns.ColumnName)
            '    cboEmail.Items.Add(dtColumns.ColumnName)
            '    cboFax.Items.Add(dtColumns.ColumnName)
            'Next
            Dim lstCombos As List(Of Control) = Nothing
            lstCombos = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            lstCombos.AddRange(From p In pnlEmergencyAddress.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p))
            lstCombos.AddRange(From p In pnlOtherAddress.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p))
            For Each ctrl As Control In lstCombos
                    Call ClearCombo(CType(ctrl, ComboBox))
            Next
            Dim columnNames() As String = mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)().Select(Function(x) x.ColumnName).ToArray()
            For Each ctrl As Control In lstCombos
                CType(ctrl, ComboBox).Items.AddRange(columnNames)
            Next
            'S.SANDEEP [14-JUN-2018] -- END

            'Gajanan (24 Nov 2018) -- Start
            'Enhancement : Import template column headers should read from language set for 
            'users (custom 1) and columns' date formats for importation should be clearly known 
            'UAT No:TC017 NMB
            If mds_ImportData IsNot Nothing Then

                Select Case cboSelectType.SelectedIndex
                    Case 0, 1, 2
                        For Each dc As DataColumn In mds_ImportData.Tables(0).Columns

                            If dc.ColumnName = Language.getMessage(mstrModuleName, 12, "Address1") Then
                                dc.Caption = "Address1"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 13, "Address2") Then
                                dc.Caption = "Address2"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 14, "PostCountry") Then
                                dc.Caption = "PostCountry"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 15, "State") Then
                                dc.Caption = "State"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 16, "PostTown") Then
                                dc.Caption = "PostTown"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 22, "PostCode") Then
                                dc.Caption = "PostCode"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 23, "ProvRegion") Then
                                dc.Caption = "ProvRegion"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 24, "RoadStreet") Then
                                dc.Caption = "RoadStreet"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 25, "EState") Then
                                dc.Caption = "EState"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 26, "RoadStreet1") Then
                                dc.Caption = "RoadStreet1"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 27, "ProvRegion1") Then
                                dc.Caption = "ProvRegion1"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 28, "Chiefdom") Then
                                dc.Caption = "Chiefdom"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 29, "Village") Then
                                dc.Caption = "Village"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 30, "Town1") Then
                                dc.Caption = "Town1"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 31, "Mobile") Then
                                dc.Caption = "Mobile"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 32, "TelNo") Then
                                dc.Caption = "TelNo"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 33, "PlotNo") Then
                                dc.Caption = "PlotNo"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 34, "AlternativeNo") Then
                                dc.Caption = "AlternativeNo"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 35, "Email") Then
                                dc.Caption = "Email"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 36, "Fax") Then
                                dc.Caption = "Fax"
                            End If

                        Next
                        mds_ImportData.AcceptChanges()

                    Case 3, 4, 5
                        For Each dc As DataColumn In mds_ImportData.Tables(0).Columns

                            If dc.ColumnName = Language.getMessage(mstrModuleName, 37, "EmergencyFirstname") Then
                                dc.Caption = "EmergencyFirstname"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 38, "EmergencyLastname") Then
                                dc.Caption = "EmergencyLastname"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 39, "EmergencyAddress") Then
                                dc.Caption = "EmergencyAddress"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 40, "EmergencyCountry") Then
                                dc.Caption = "EmergencyCountry"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 56, "EmergencyState") Then
                                dc.Caption = "EmergencyState"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 57, "EmergencyTown") Then
                                dc.Caption = "EmergencyTown"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 43, "EmergencyPostCode") Then
                                dc.Caption = "EmergencyPostCode"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 44, "EmergencyProvRegion") Then
                                dc.Caption = "EmergencyProvRegion"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 45, "EmergencyRoadStreet") Then
                                dc.Caption = "EmergencyRoadStreet"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 46, "EmergencyEstate") Then
                                dc.Caption = "EmergencyEstate"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 47, "EmergencyPlotNumber") Then
                                dc.Caption = "EmergencyPlotNumber"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 48, "EmergencyMobile") Then
                                dc.Caption = "EmergencyMobile"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 58, "EmergencyAltNo") Then
                                dc.Caption = "EmergencyAltNo"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 60, "EmergencyTelNo") Then
                                dc.Caption = "EmergencyTelNo"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 51, "EmergencyFaxNo") Then
                                dc.Caption = "EmergencyFaxNo"
                            ElseIf dc.ColumnName = Language.getMessage(mstrModuleName, 52, "EmergencyEmail") Then
                                dc.Caption = "EmergencyEmail"
                            End If
                        Next
                        mds_ImportData.AcceptChanges()
                End Select

            End If
            'Gajanan (24 Nov 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        ezWait.Active = True
        Try



            mdt_ImportData.Columns.Add("ecode", GetType(String)).DefaultValue = ""

            'Gajanan [27-May-2019] -- Start              
            mdt_ImportData.Columns.Add("Firstname", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("Surname", GetType(String)).DefaultValue = ""
            'Gajanan [27-May-2019] -- End


            mdt_ImportData.Columns.Add("address1", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("address2", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("postcountry", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("state", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("posttown", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("postcode", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("provregion", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("roadstreet", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("estate", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("roadstreet1", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("provregion1", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("chiefdom", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("village", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("town1", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("mobile", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("telno", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("plotno", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("alternativeno", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("email", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("fax", GetType(String)).DefaultValue = ""

            'Gajanan [27-May-2019] -- Start    

            If cboSelectType.SelectedValue = 1 OrElse _
                      cboSelectType.SelectedValue = 2 OrElse _
                      cboSelectType.SelectedValue = 3 Then

                mdt_ImportData.Columns("address1").ExtendedProperties.Add("col", "address1")
                mdt_ImportData.Columns("address2").ExtendedProperties.Add("col", "address2")
                mdt_ImportData.Columns("postcountry").ExtendedProperties.Add("col", "postcountry")
                mdt_ImportData.Columns("state").ExtendedProperties.Add("col", "state")
                mdt_ImportData.Columns("posttown").ExtendedProperties.Add("col", "posttown")
                mdt_ImportData.Columns("postcode").ExtendedProperties.Add("col", "postcode")
                mdt_ImportData.Columns("provregion").ExtendedProperties.Add("col", "provregion")
                mdt_ImportData.Columns("roadstreet").ExtendedProperties.Add("col", "roadstreet")
                mdt_ImportData.Columns("estate").ExtendedProperties.Add("col", "estate")
                mdt_ImportData.Columns("roadstreet1").ExtendedProperties.Add("col", "roadstreet1")
                mdt_ImportData.Columns("provregion1").ExtendedProperties.Add("col", "provregion1")
                mdt_ImportData.Columns("chiefdom").ExtendedProperties.Add("col", "chiefdom")
                mdt_ImportData.Columns("village").ExtendedProperties.Add("col", "village")
                mdt_ImportData.Columns("town1").ExtendedProperties.Add("col", "town1")
                mdt_ImportData.Columns("mobile").ExtendedProperties.Add("col", "mobile")
                mdt_ImportData.Columns("telno").ExtendedProperties.Add("col", "telno")
                mdt_ImportData.Columns("plotno").ExtendedProperties.Add("col", "plotno")
                mdt_ImportData.Columns("alternativeno").ExtendedProperties.Add("col", "alternativeno")
                mdt_ImportData.Columns("email").ExtendedProperties.Add("col", "email")
                mdt_ImportData.Columns("fax").ExtendedProperties.Add("col", "fax")
            End If
            'Gajanan [27-May-2019] -- End


            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            mdt_ImportData.Columns.Add("EmergencyFirstname", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyLastname", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyAddress", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyCountry", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyState", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyTown", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyPostCode", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyProvRegion", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyRoadStreet", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyEstate", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyPlotNumber", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyMobile", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyAltNo", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyTelNo", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyFaxNo", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("EmergencyEmail", GetType(String)).DefaultValue = ""


            'Gajanan [27-May-2019] -- Start  
            If cboSelectType.SelectedValue = 4 OrElse _
                           cboSelectType.SelectedValue = 5 OrElse _
                           cboSelectType.SelectedValue = 6 Then
                mdt_ImportData.Columns("EmergencyFirstname").ExtendedProperties.Add("col", "EmergencyFirstname")
                mdt_ImportData.Columns("EmergencyLastname").ExtendedProperties.Add("col", "EmergencyLastname")
                mdt_ImportData.Columns("EmergencyAddress").ExtendedProperties.Add("col", "EmergencyAddress")
                mdt_ImportData.Columns("EmergencyCountry").ExtendedProperties.Add("col", "EmergencyCountry")
                mdt_ImportData.Columns("EmergencyState").ExtendedProperties.Add("col", "EmergencyState")
                mdt_ImportData.Columns("EmergencyTown").ExtendedProperties.Add("col", "EmergencyTown")
                mdt_ImportData.Columns("EmergencyPostCode").ExtendedProperties.Add("col", "EmergencyPostCode")
                mdt_ImportData.Columns("EmergencyProvRegion").ExtendedProperties.Add("col", "EmergencyProvRegion")
                mdt_ImportData.Columns("EmergencyRoadStreet").ExtendedProperties.Add("col", "EmergencyRoadStreet")
                mdt_ImportData.Columns("EmergencyEstate").ExtendedProperties.Add("col", "EmergencyEstate")
                mdt_ImportData.Columns("EmergencyPlotNumber").ExtendedProperties.Add("col", "EmergencyPlotNumber")
                mdt_ImportData.Columns("EmergencyMobile").ExtendedProperties.Add("col", "EmergencyMobile")
                mdt_ImportData.Columns("EmergencyAltNo").ExtendedProperties.Add("col", "EmergencyAltNo")
                mdt_ImportData.Columns("EmergencyTelNo").ExtendedProperties.Add("col", "EmergencyTelNo")
                mdt_ImportData.Columns("EmergencyFaxNo").ExtendedProperties.Add("col", "EmergencyFaxNo")
                mdt_ImportData.Columns("EmergencyEmail").ExtendedProperties.Add("col", "EmergencyEmail")
            End If
            'Gajanan [27-May-2019] -- End

            'S.SANDEEP [14-JUN-2018] -- END

            mdt_ImportData.Columns.Add("image", GetType(Object))
            mdt_ImportData.Columns.Add("Message", GetType(String))
            mdt_ImportData.Columns.Add("Status", GetType(String))
            mdt_ImportData.Columns.Add("objStatus", GetType(String))

            'Gajanan [27-May-2019] -- Start              
            mdt_ImportData.Columns.Add("EmployeeId", System.Type.GetType("System.String"))
            'Gajanan [27-May-2019] -- End


            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData.NewRow
                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim

                If cboAddress1.Text <> "" Then drNewRow.Item("address1") = dtRow.Item(cboAddress1.Text).ToString.Trim
                If cboAddress2.Text <> "" Then drNewRow.Item("address2") = dtRow.Item(cboAddress2.Text).ToString.Trim
                If cboPostCountry.Text <> "" Then drNewRow.Item("postcountry") = dtRow.Item(cboPostCountry.Text).ToString.Trim
                If cboState.Text <> "" Then drNewRow.Item("state") = dtRow.Item(cboState.Text).ToString.Trim
                If cboPostTown.Text <> "" Then drNewRow.Item("posttown") = dtRow.Item(cboPostTown.Text).ToString.Trim
                If cboPostCode.Text <> "" Then drNewRow.Item("postcode") = dtRow.Item(cboPostCode.Text).ToString.Trim
                If cboProvRegion.Text <> "" Then drNewRow.Item("provregion") = dtRow.Item(cboProvRegion.Text).ToString.Trim
                If cboRoadStreet.Text <> "" Then drNewRow.Item("roadstreet") = dtRow.Item(cboRoadStreet.Text).ToString.Trim
                If cboEState.Text <> "" Then drNewRow.Item("estate") = dtRow.Item(cboEState.Text).ToString.Trim
                If cboRoadStreet1.Text <> "" Then drNewRow.Item("roadstreet1") = dtRow.Item(cboRoadStreet1.Text).ToString.Trim
                If cboProvRegion1.Text <> "" Then drNewRow.Item("provregion1") = dtRow.Item(cboProvRegion1.Text).ToString.Trim
                If cboChiefdom.Text <> "" Then drNewRow.Item("chiefdom") = dtRow.Item(cboChiefdom.Text).ToString.Trim
                If cboVillage.Text <> "" Then drNewRow.Item("village") = dtRow.Item(cboVillage.Text).ToString.Trim
                If cboTown1.Text <> "" Then drNewRow.Item("town1") = dtRow.Item(cboTown1.Text).ToString.Trim
                If cboMobile.Text <> "" Then drNewRow.Item("mobile") = dtRow.Item(cboMobile.Text).ToString.Trim
                If cboTelNo.Text <> "" Then drNewRow.Item("telno") = dtRow.Item(cboTelNo.Text).ToString.Trim
                If cboPlotNo.Text <> "" Then drNewRow.Item("plotno") = dtRow.Item(cboPlotNo.Text).ToString.Trim
                If cboAlternativeNo.Text <> "" Then drNewRow.Item("alternativeno") = dtRow.Item(cboAlternativeNo.Text).ToString.Trim
                If cboEmail.Text <> "" Then drNewRow.Item("email") = dtRow.Item(cboEmail.Text).ToString.Trim
                If cboFax.Text <> "" Then drNewRow.Item("fax") = dtRow.Item(cboFax.Text).ToString.Trim

                'S.SANDEEP [14-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {NMB}
                If cboEmergencyFirstname.Text <> "" Then drNewRow.Item("EmergencyFirstname") = dtRow.Item(cboEmergencyFirstname.Text).ToString.Trim
                If cboEmergencyLastname.Text <> "" Then drNewRow.Item("EmergencyLastname") = dtRow.Item(cboEmergencyLastname.Text).ToString.Trim
                If cboEmergencyAddress.Text <> "" Then drNewRow.Item("EmergencyAddress") = dtRow.Item(cboEmergencyAddress.Text).ToString.Trim
                If cboEmergencyCountry.Text <> "" Then drNewRow.Item("EmergencyCountry") = dtRow.Item(cboEmergencyCountry.Text).ToString.Trim
                If cboEmergencyState.Text <> "" Then drNewRow.Item("EmergencyState") = dtRow.Item(cboEmergencyState.Text).ToString.Trim
                If cboEmergencyTown.Text <> "" Then drNewRow.Item("EmergencyTown") = dtRow.Item(cboEmergencyTown.Text).ToString.Trim
                If cboEmergencyPostCode.Text <> "" Then drNewRow.Item("EmergencyPostCode") = dtRow.Item(cboEmergencyPostCode.Text).ToString.Trim
                If cboEmergencyProvRegion.Text <> "" Then drNewRow.Item("EmergencyProvRegion") = dtRow.Item(cboEmergencyProvRegion.Text).ToString.Trim
                If cboEmergencyRoadStreet.Text <> "" Then drNewRow.Item("EmergencyRoadStreet") = dtRow.Item(cboEmergencyRoadStreet.Text).ToString.Trim
                If cboEmergencyEstate.Text <> "" Then drNewRow.Item("EmergencyEstate") = dtRow.Item(cboEmergencyEstate.Text).ToString.Trim
                If cboEmergencyPlotNumber.Text <> "" Then drNewRow.Item("EmergencyPlotNumber") = dtRow.Item(cboEmergencyPlotNumber.Text).ToString.Trim
                If cboEmergencyMobile.Text <> "" Then drNewRow.Item("EmergencyMobile") = dtRow.Item(cboEmergencyMobile.Text).ToString.Trim
                If cboEmergencyAltNo.Text <> "" Then drNewRow.Item("EmergencyAltNo") = dtRow.Item(cboEmergencyAltNo.Text).ToString.Trim
                If cboEmergencyTelNo.Text <> "" Then drNewRow.Item("EmergencyTelNo") = dtRow.Item(cboEmergencyTelNo.Text).ToString.Trim
                If cboEmergencyFaxNo.Text <> "" Then drNewRow.Item("EmergencyFaxNo") = dtRow.Item(cboEmergencyFaxNo.Text).ToString.Trim
                If cboEmergencyEmail.Text <> "" Then drNewRow.Item("EmergencyEmail") = dtRow.Item(cboEmergencyEmail.Text).ToString.Trim
                'S.SANDEEP [14-JUN-2018] -- END


                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next


            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ecode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data_Employee()

            ezWait.Active = False
            eZeeWizImportImgs.BackEnabled = False
            eZeeWizImportImgs.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Images(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data_Employee()
        Try
            btnFilter.Enabled = False
            If mdt_ImportData.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim intEmpId As Integer = 0 : Dim intPostCountry As Integer = 0
            Dim intState As Integer = 0 : Dim intPostTown As Integer = 0
            Dim intPostCode As Integer = 0 : Dim intProvRegion1 As Integer = 0
            Dim intProvStreet1 As Integer = 0 : Dim intChiefdom As Integer = 0
            Dim intVillage As Integer = 0 : Dim intTown1 As Integer = 0

            'Gajanan [27-May-2019] -- Start
            Dim EmpList As New List(Of String)
            Dim AddressType As clsEmployeeAddress_approval_tran.enAddressType = 0
            Dim EmeAddressType As clsEmployee_emergency_address_approval.enEmeAddressType = 0
            'Gajanan [27-May-2019] -- End

            Dim objEMaster As New clsEmployee_Master

            For Each dtRow As DataRow In mdt_ImportData.Rows

                'Gajanan [27-May-2019] -- Start              
                Dim isEmployeeApprove As Boolean = False
                'Gajanan [27-May-2019] -- End


                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                intEmpId = 0 : intPostCountry = 0 : intState = 0 : intPostTown = 0 : intPostCode = 0
                intProvRegion1 = 0 : intProvStreet1 = 0 : intChiefdom = 0 : intVillage = 0 : intTown1 = 0
                Select Case cboSelectType.SelectedIndex
                    Case 0, 1, 2
                If dtRow.Item("address1").ToString.Trim.Length <= 0 AndAlso dtRow.Item("address2").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("postcountry").ToString.Trim.Length <= 0 AndAlso dtRow.Item("state").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("posttown").ToString.Trim.Length <= 0 AndAlso dtRow.Item("postcode").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("provregion").ToString.Trim.Length <= 0 AndAlso dtRow.Item("roadstreet").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("estate").ToString.Trim.Length <= 0 AndAlso dtRow.Item("roadstreet1").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("provregion1").ToString.Trim.Length <= 0 AndAlso dtRow.Item("chiefdom").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("village").ToString.Trim.Length <= 0 AndAlso dtRow.Item("town1").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("mobile").ToString.Trim.Length <= 0 AndAlso dtRow.Item("telno").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("plotno").ToString.Trim.Length <= 0 AndAlso dtRow.Item("alternativeno").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("email").ToString.Trim.Length <= 0 AndAlso dtRow.Item("fax").ToString.Trim.Length <= 0 Then

                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 21, "Employee data Not Found.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If


                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    intEmpId = objEMaster.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If intEmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 5, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                            'Gajanan [27-May-2019] -- Start   
                            objEMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpId
                            dtRow.Item("Surname") = objEMaster._Surname
                            dtRow.Item("Firstname") = objEMaster._Firstname
                            'Gajanan [27-May-2019] -- End

                        End If

                        'Gajanan [27-May-2019] -- Start  

                        'isEmployeeApprove = objEMaster.IsEmployeeApproved(dtRow.Item("ECode").ToString.Trim)
                        isEmployeeApprove = objEMaster._Isapproved
                        If isEmployeeApprove Then
                            dtRow.Item("EmployeeId") = intEmpId
                End If
                        'Gajanan [27-May-2019] -- End

                '------------------------------ CHECKING IF Post Country.
                If dtRow.Item("postcountry").ToString.Trim.Length > 0 Then
                    intPostCountry = (New clsMasterData).GetCountryUnkId(dtRow.Item("postcountry").ToString)
                    If intPostCountry <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Post Country Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If
                '------------------------------ CHECKING IF State.
                If dtRow.Item("state").ToString.Trim.Length > 0 Then
                    'Dim dsState As DataSet = (New clsstate_master).GetList("List", True, , intPostCountry)
                    'If dsState.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dtRow.Item("state").ToString).Count > 0 Then
                    '    intState = dsState.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dtRow.Item("state").ToString).Select(Function(x) x.Field(Of Integer)("stateunkid")).First
                    'End If

                    'If intState <= 0 AndAlso intPostCountry > 0 Then
                    '    Dim objState As New clsstate_master
                    '    objState._Countryunkid = intPostCountry
                    '    objState._Code = dtRow.Item("state").ToString
                    '    objState._Name = dtRow.Item("state").ToString
                    '    objState._Name1 = dtRow.Item("state").ToString
                    '    objState._Name2 = dtRow.Item("state").ToString
                    '    objState._Isactive = True
                    '    If objState.Insert() = False Then
                    '        dtRow.Item("image") = imgError
                    '        dtRow.Item("message") = objState._Message
                    '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    '        dtRow.Item("objStatus") = 2
                    '        objError.Text = CStr(Val(objError.Text) + 1)
                    '        Continue For
                    '    Else
                    '        intState = objState._Stateunkid
                    '    End If
                    'ElseIf intState <= 0 Then
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = Language.getMessage(mstrModuleName, 8, "State Not Found.")
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    '    Continue For
                    'End If
                    Dim dRow As DataRow = dtRow
                    Dim dsState As DataSet = (New clsstate_master).GetList("List", True, , intPostCountry)
                    If dsState.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dRow.Item("state").ToString).Count > 0 Then
                        intState = dsState.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dRow.Item("state").ToString).Select(Function(x) x.Field(Of Integer)("stateunkid")).First
                    End If
                    If intState <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 8, "State Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                End If
                '------------------------------ CHECKING IF Post Town.
                If dtRow.Item("posttown").ToString.Trim.Length > 0 Then
                    'Dim dsCity As DataSet = (New clscity_master).GetList("List", True, , intState)
                    'If dsCity.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dtRow.Item("posttown").ToString).Count > 0 Then
                    '    intPostTown = dsCity.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dtRow.Item("posttown").ToString) _
                    '                                            .Select(Function(x) x.Field(Of Integer)("cityunkid")).First()
                    'End If
                    'If intPostTown <= 0 AndAlso intState > 0 AndAlso intPostCountry > 0 Then
                    '    Dim objCity As New clscity_master
                    '    objCity._Countryunkid = intPostCountry
                    '    objCity._Stateunkid = intState
                    '    objCity._Code = dtRow.Item("posttown").ToString
                    '    objCity._Name = dtRow.Item("posttown").ToString
                    '    objCity._Name1 = dtRow.Item("posttown").ToString
                    '    objCity._Name2 = dtRow.Item("posttown").ToString
                    '    objCity._Isactive = True
                    '    If objCity.Insert() = False Then
                    '        dtRow.Item("image") = imgError
                    '        dtRow.Item("message") = objCity._Message
                    '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    '        dtRow.Item("objStatus") = 2
                    '        objError.Text = CStr(Val(objError.Text) + 1)
                    '        Continue For
                    '    Else
                    '        intPostTown = objCity._Cityunkid
                    '    End If
                    'ElseIf intPostTown <= 0 Then
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = Language.getMessage(mstrModuleName, 9, "City Not Found.")
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    '    Continue For
                    'End If
                    Dim dRow As DataRow = dtRow
                    Dim dsCity As DataSet = (New clscity_master).GetList("List", True, , intState)
                    If dsCity.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dRow.Item("posttown").ToString).Count > 0 Then
                        intPostTown = dsCity.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dRow.Item("posttown").ToString) _
                                                                .Select(Function(x) x.Field(Of Integer)("cityunkid")).First()
                    End If
                    If intPostTown <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 9, "City Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If
                '------------------------------ CHECKING IF Post Code.
                If dtRow.Item("postcode").ToString.Trim.Length > 0 Then
                    'Dim dsPostCode As DataSet = (New clszipcode_master).GetList("List", True, , intPostTown)
                    'If dsPostCode.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("zipcode_no") = dtRow.Item("postcode").ToString).Count > 0 Then
                    '    intPostCode = dsPostCode.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("zipcode_no") = dtRow.Item("postcode").ToString) _
                    '                                                .Select(Function(x) x.Field(Of Integer)("zipcodeunkid")).First
                    'End If

                    'If intPostCode <= 0 AndAlso intPostCountry > 0 AndAlso intState > 0 AndAlso intPostTown > 0 Then
                    '    Dim objPostCode As New clszipcode_master
                    '    objPostCode._Countryunkid = intPostCountry
                    '    objPostCode._Stateunkid = intState
                    '    objPostCode._Cityunkid = intPostTown
                    '    objPostCode._Zipcode_Code = dtRow.Item("postcode").ToString
                    '    objPostCode._Zipcode_No = dtRow.Item("postcode").ToString
                    '    objPostCode._Isactive = True
                    '    If objPostCode.Insert() = False Then
                    '        dtRow.Item("image") = imgError
                    '        dtRow.Item("message") = objPostCode._Message
                    '        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    '        dtRow.Item("objStatus") = 2
                    '        objError.Text = CStr(Val(objError.Text) + 1)
                    '        Continue For
                    '    Else
                    '        intPostCode = objPostCode._Zipcodeunkid
                    '    End If
                    'ElseIf intPostCode <= 0 Then
                    '    dtRow.Item("image") = imgError
                    '    dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Post Code Not Found.")
                    '    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    '    dtRow.Item("objStatus") = 2
                    '    objError.Text = CStr(Val(objError.Text) + 1)
                    '    Continue For
                    'End If

                    Dim dsPostCode As DataSet = (New clszipcode_master).GetList("List", True, , intPostTown)
                    Dim dRow As DataRow = dtRow
                    If dsPostCode.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("zipcode_no") = dRow.Item("postcode").ToString).Count > 0 Then
                        intPostCode = dsPostCode.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("zipcode_no") = dRow.Item("postcode").ToString) _
                                                                    .Select(Function(x) x.Field(Of Integer)("zipcodeunkid")).First
                    End If

                    If intPostCode <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Post Code Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If
                '------------------------------ CHECKING IF Prov Region1.
                If dtRow.Item("provregion1").ToString.Trim.Length > 0 Then
                    intProvRegion1 = (New clsCommon_Master).GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.PROV_REGION_1, dtRow.Item("provregion1").ToString.Trim)
                    If intProvRegion1 <= 0 Then
                        Dim objCMaster As New clsCommon_Master
                        objCMaster._Code = dtRow.Item("provregion1").ToString.Trim
                        objCMaster._Name = dtRow.Item("provregion1").ToString.Trim
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.PROV_REGION_1
                        objCMaster._Isactive = True

                        With objCMaster
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        If objCMaster.Insert() = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Else
                            intProvRegion1 = objCMaster._Masterunkid
                        End If
                    End If
                End If
                '------------------------------ CHECKING IF Road/street1.
                If dtRow.Item("roadstreet1").ToString.Trim.Length > 0 Then
                    intProvStreet1 = (New clsCommon_Master).GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.ROAD_STREET_1, dtRow.Item("roadstreet1").ToString.Trim)
                    If intProvStreet1 <= 0 Then
                        Dim objCMaster As New clsCommon_Master
                        objCMaster._Code = dtRow.Item("roadstreet1").ToString.Trim
                        objCMaster._Name = dtRow.Item("roadstreet1").ToString.Trim
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.ROAD_STREET_1
                        objCMaster._Isactive = True

                        With objCMaster
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        If objCMaster.Insert() = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Else
                            intProvStreet1 = objCMaster._Masterunkid
                        End If
                    End If
                End If
                '------------------------------ CHECKING IF Chiefdom.
                If dtRow.Item("chiefdom").ToString.Trim.Length > 0 Then
                    intChiefdom = (New clsCommon_Master).GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.CHIEFDOM, dtRow.Item("chiefdom").ToString.Trim)
                    If intChiefdom <= 0 Then
                        Dim objCMaster As New clsCommon_Master
                        objCMaster._Code = dtRow.Item("chiefdom").ToString.Trim
                        objCMaster._Name = dtRow.Item("chiefdom").ToString.Trim
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.CHIEFDOM
                        objCMaster._Isactive = True

                        With objCMaster
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        If objCMaster.Insert() = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Else
                            intChiefdom = objCMaster._Masterunkid
                        End If
                    End If
                End If
                '------------------------------ CHECKING IF Village.
                If dtRow.Item("village").ToString.Trim.Length > 0 Then
                    intVillage = (New clsCommon_Master).GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.VILLAGE, dtRow.Item("village").ToString.Trim)
                    If intVillage <= 0 Then
                        Dim objCMaster As New clsCommon_Master
                        objCMaster._Code = dtRow.Item("village").ToString.Trim
                        objCMaster._Name = dtRow.Item("village").ToString.Trim
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.VILLAGE
                        objCMaster._Isactive = True

                        With objCMaster
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        If objCMaster.Insert() = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Else
                            intVillage = objCMaster._Masterunkid
                        End If
                    End If
                End If
                '------------------------------ CHECKING IF Town1.
                If dtRow.Item("town1").ToString.Trim.Length > 0 Then
                    intTown1 = (New clsCommon_Master).GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.TOWN_1, dtRow.Item("town1").ToString.Trim)
                    If intTown1 <= 0 Then
                        Dim objCMaster As New clsCommon_Master
                        objCMaster._Code = dtRow.Item("town1").ToString.Trim
                        objCMaster._Name = dtRow.Item("town1").ToString.Trim
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.TOWN_1
                        objCMaster._Isactive = True

                        With objCMaster
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        If objCMaster.Insert() = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Else
                            intTown1 = objCMaster._Masterunkid
                        End If
                    End If
                End If
                    Case 3, 4, 5
                        If dtRow.Item("EmergencyFirstname").ToString.Trim.Length <= 0 AndAlso dtRow.Item("EmergencyLastname").ToString.Trim.Length <= 0 AndAlso _
                           dtRow.Item("EmergencyAddress").ToString.Trim.Length <= 0 AndAlso dtRow.Item("EmergencyCountry").ToString.Trim.Length <= 0 AndAlso _
                           dtRow.Item("EmergencyState").ToString.Trim.Length <= 0 AndAlso dtRow.Item("EmergencyTown").ToString.Trim.Length <= 0 AndAlso _
                           dtRow.Item("EmergencyPostCode").ToString.Trim.Length <= 0 AndAlso dtRow.Item("EmergencyProvRegion").ToString.Trim.Length <= 0 AndAlso _
                           dtRow.Item("EmergencyRoadStreet").ToString.Trim.Length <= 0 AndAlso dtRow.Item("EmergencyEstate").ToString.Trim.Length <= 0 AndAlso _
                           dtRow.Item("EmergencyPlotNumber").ToString.Trim.Length <= 0 AndAlso dtRow.Item("EmergencyMobile").ToString.Trim.Length <= 0 AndAlso _
                           dtRow.Item("EmergencyAltNo").ToString.Trim.Length <= 0 AndAlso dtRow.Item("EmergencyTelNo").ToString.Trim.Length <= 0 AndAlso _
                           dtRow.Item("EmergencyFaxNo").ToString.Trim.Length <= 0 AndAlso dtRow.Item("EmergencyEmail").ToString.Trim.Length <= 0 Then

                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 21, "Employee data Not Found.")
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        End If


                        '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                        If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                            intEmpId = objEMaster.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                            If intEmpId <= 0 Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 5, "Employee Not Found.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If

                            'Gajanan [27-May-2019] -- Start   
                            objEMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpId
                            dtRow.Item("Surname") = objEMaster._Surname
                            dtRow.Item("Firstname") = objEMaster._Firstname
                            'Gajanan [27-May-2019] -- End


                        End If

                        'Gajanan [27-May-2019] -- Start  

                        isEmployeeApprove = objEMaster._Isapproved
                        If isEmployeeApprove Then
                            dtRow.Item("EmployeeId") = intEmpId
                        End If
                        'Gajanan [27-May-2019] -- End

                        '------------------------------ CHECKING IF Post Country.
                        If dtRow.Item("EmergencyCountry").ToString.Trim.Length > 0 Then
                            intPostCountry = (New clsMasterData).GetCountryUnkId(dtRow.Item("EmergencyCountry").ToString)
                            If intPostCountry <= 0 Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Post Country Not Found.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        End If
                        '------------------------------ CHECKING IF State.
                        If dtRow.Item("EmergencyState").ToString.Trim.Length > 0 Then
                            Dim dRow As DataRow = dtRow
                            Dim dsState As DataSet = (New clsstate_master).GetList("List", True, , intPostCountry)
                            If dsState.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dRow.Item("EmergencyState").ToString).Count > 0 Then
                                intState = dsState.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dRow.Item("EmergencyState").ToString).Select(Function(x) x.Field(Of Integer)("stateunkid")).First
                            End If
                            If intState <= 0 Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 8, "State Not Found.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If

                        End If
                        '------------------------------ CHECKING IF Post Town.
                        If dtRow.Item("EmergencyTown").ToString.Trim.Length > 0 Then
                            Dim dRow As DataRow = dtRow
                            Dim dsCity As DataSet = (New clscity_master).GetList("List", True, , intState)
                            If dsCity.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dRow.Item("EmergencyTown").ToString).Count > 0 Then
                                intPostTown = dsCity.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dRow.Item("EmergencyTown").ToString) _
                                                                        .Select(Function(x) x.Field(Of Integer)("cityunkid")).First()
                            End If
                            If intPostTown <= 0 Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 9, "City Not Found.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        End If
                        '------------------------------ CHECKING IF Post Code.
                        If dtRow.Item("EmergencyPostCode").ToString.Trim.Length > 0 Then
                            Dim dsPostCode As DataSet = (New clszipcode_master).GetList("List", True, , intPostTown)
                            Dim dRow As DataRow = dtRow
                            If dsPostCode.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("zipcode_no") = dRow.Item("EmergencyPostCode").ToString).Count > 0 Then
                                intPostCode = dsPostCode.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("zipcode_no") = dRow.Item("EmergencyPostCode").ToString) _
                                                                            .Select(Function(x) x.Field(Of Integer)("zipcodeunkid")).First
                            End If

                            If intPostCode <= 0 Then
                                dtRow.Item("image") = imgError
                                dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Post Code Not Found.")
                                dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                                dtRow.Item("objStatus") = 2
                                objError.Text = CStr(Val(objError.Text) + 1)
                                Continue For
                            End If
                        End If

                End Select
                '------------------------------ CHECKING Check Row Data.
                


                objEMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpId
                If cboSelectType.SelectedIndex = 0 Then
                    If cboAddress1.Text <> "" Then objEMaster._Present_Address1 = dtRow.Item("address1").ToString.Trim
                    If cboAddress2.Text <> "" Then objEMaster._Present_Address2 = dtRow.Item("address2").ToString.Trim
                    If cboPostCountry.Text <> "" Then objEMaster._Present_Countryunkid = intPostCountry
                    If cboState.Text <> "" Then objEMaster._Present_Stateunkid = intState
                    If cboPostTown.Text <> "" Then objEMaster._Present_Post_Townunkid = intPostTown
                    If cboPostCode.Text <> "" Then objEMaster._Present_Postcodeunkid = intPostCode
                    If cboProvRegion.Text <> "" Then objEMaster._Present_Provicnce = dtRow.Item("provregion").ToString.Trim
                    If cboRoadStreet.Text <> "" Then objEMaster._Present_Road = dtRow.Item("roadstreet").ToString.Trim
                    If cboEState.Text <> "" Then objEMaster._Present_Estate = dtRow.Item("estate").ToString.Trim
                    If cboProvRegion1.Text <> "" Then objEMaster._Present_Provinceunkid = intProvRegion1
                    If cboRoadStreet1.Text <> "" Then objEMaster._Present_Roadunkid = intProvStreet1
                    If cboChiefdom.Text <> "" Then objEMaster._Present_Chiefdomunkid = intChiefdom
                    If cboVillage.Text <> "" Then objEMaster._Present_Villageunkid = intVillage
                    If cboTown1.Text <> "" Then objEMaster._Present_Town1unkid = intTown1
                    If cboMobile.Text <> "" Then objEMaster._Present_Mobile = dtRow.Item("mobile").ToString.Trim
                    If cboTelNo.Text <> "" Then objEMaster._Present_Tel_No = dtRow.Item("telno").ToString.Trim
                    If cboPlotNo.Text <> "" Then objEMaster._Present_Plotno = dtRow.Item("plotno").ToString.Trim
                    If cboAlternativeNo.Text <> "" Then objEMaster._Present_Alternateno = dtRow.Item("alternativeno").ToString.Trim
                    If cboEmail.Text <> "" Then objEMaster._Present_Email = dtRow.Item("email").ToString.Trim
                    If cboFax.Text <> "" Then objEMaster._Present_Fax = dtRow.Item("fax").ToString.Trim
                ElseIf cboSelectType.SelectedIndex = 1 Then
                    If cboAddress1.Text <> "" Then objEMaster._Domicile_Address1 = dtRow.Item("address1").ToString.Trim
                    If cboAddress2.Text <> "" Then objEMaster._Domicile_Address2 = dtRow.Item("address2").ToString.Trim
                    If cboPostCountry.Text <> "" Then objEMaster._Domicile_Countryunkid = intPostCountry
                    If cboState.Text <> "" Then objEMaster._Domicile_Stateunkid = intState
                    If cboPostTown.Text <> "" Then objEMaster._Domicile_Post_Townunkid = intPostTown
                    If cboPostCode.Text <> "" Then objEMaster._Domicile_Postcodeunkid = intPostCode
                    If cboProvRegion.Text <> "" Then objEMaster._Domicile_Provicnce = dtRow.Item("provregion").ToString.Trim
                    If cboRoadStreet.Text <> "" Then objEMaster._Domicile_Road = dtRow.Item("roadstreet").ToString.Trim
                    If cboEState.Text <> "" Then objEMaster._Domicile_Estate = dtRow.Item("estate").ToString.Trim
                    If cboProvRegion1.Text <> "" Then objEMaster._domicile_provinceunkid = intProvRegion1
                    If cboRoadStreet1.Text <> "" Then objEMaster._domicile_roadunkid = intProvStreet1
                    If cboChiefdom.Text <> "" Then objEMaster._Domicile_Chiefdomunkid = intChiefdom
                    If cboVillage.Text <> "" Then objEMaster._Domicile_Villageunkid = intVillage
                    If cboTown1.Text <> "" Then objEMaster._Domicile_Town1unkid = intTown1
                    If cboMobile.Text <> "" Then objEMaster._Domicile_Mobile = dtRow.Item("mobile").ToString.Trim
                    If cboTelNo.Text <> "" Then objEMaster._Domicile_Tel_No = dtRow.Item("telno").ToString.Trim
                    If cboPlotNo.Text <> "" Then objEMaster._Domicile_Plotno = dtRow.Item("plotno").ToString.Trim
                    If cboAlternativeNo.Text <> "" Then objEMaster._Domicile_Alternateno = dtRow.Item("alternativeno").ToString.Trim
                    If cboEmail.Text <> "" Then objEMaster._Domicile_Email = dtRow.Item("email").ToString.Trim
                    If cboFax.Text <> "" Then objEMaster._Domicile_Fax = dtRow.Item("fax").ToString.Trim
                ElseIf cboSelectType.SelectedIndex = 2 Then
                    If cboAddress1.Text <> "" Then objEMaster._Recruitment_Address1 = dtRow.Item("address1").ToString.Trim
                    If cboAddress2.Text <> "" Then objEMaster._Recruitment_Address2 = dtRow.Item("address2").ToString.Trim
                    If cboPostCountry.Text <> "" Then objEMaster._Recruitment_Countryunkid = intPostCountry
                    If cboState.Text <> "" Then objEMaster._Recruitment_Stateunkid = intState
                    If cboPostTown.Text <> "" Then objEMaster._Recruitment_Post_Townunkid = intPostTown
                    If cboPostCode.Text <> "" Then objEMaster._Recruitment_Postcodeunkid = intPostCode
                    If cboProvRegion.Text <> "" Then objEMaster._Recruitment_Province = dtRow.Item("provregion").ToString.Trim
                    If cboRoadStreet.Text <> "" Then objEMaster._Recruitment_Road = dtRow.Item("roadstreet").ToString.Trim
                    If cboEState.Text <> "" Then objEMaster._Recruitment_Estate = dtRow.Item("estate").ToString.Trim
                    If cboProvRegion1.Text <> "" Then objEMaster._Recruitment_Provinceunkid = intProvRegion1
                    If cboRoadStreet1.Text <> "" Then objEMaster._Recruitment_Roadunkid = intProvStreet1
                    If cboChiefdom.Text <> "" Then objEMaster._Recruitment_Chiefdomunkid = intChiefdom
                    If cboVillage.Text <> "" Then objEMaster._Recruitment_Villageunkid = intVillage
                    If cboTown1.Text <> "" Then objEMaster._Recruitment_Town1unkid = intTown1
                    If cboTelNo.Text <> "" Then objEMaster._Recruitment_Tel_No = dtRow.Item("telno").ToString.Trim
                    If cboPlotNo.Text <> "" Then objEMaster._Recruitment_Plotno = dtRow.Item("plotno").ToString.Trim
                    'S.SANDEEP [14-JUN-2018] -- START
                    'ISSUE/ENHANCEMENT : {NMB}
                ElseIf cboSelectType.SelectedIndex = 3 Then 'Emergency Address1
                    If cboEmergencyFirstname.Text <> "" Then objEMaster._Emer_Con_Firstname = dtRow.Item("EmergencyFirstname").ToString.Trim
                    If cboEmergencyLastname.Text <> "" Then objEMaster._Emer_Con_Lastname = dtRow.Item("EmergencyLastname").ToString.Trim
                    If cboEmergencyAddress.Text <> "" Then objEMaster._Emer_Con_Address = dtRow.Item("EmergencyAddress").ToString.Trim
                    If cboEmergencyCountry.Text <> "" Then objEMaster._Emer_Con_Countryunkid = intPostCountry
                    If cboEmergencyState.Text <> "" Then objEMaster._Emer_Con_State = intState
                    If cboEmergencyTown.Text <> "" Then objEMaster._Emer_Con_Post_Townunkid = intPostTown
                    If cboEmergencyPostCode.Text <> "" Then objEMaster._Emer_Con_Postcodeunkid = intPostCode
                    If cboEmergencyProvRegion.Text <> "" Then objEMaster._Emer_Con_Provicnce = dtRow.Item("EmergencyProvRegion").ToString.Trim
                    If cboEmergencyRoadStreet.Text <> "" Then objEMaster._Emer_Con_Road = dtRow.Item("EmergencyRoadStreet").ToString.Trim
                    If cboEmergencyEstate.Text <> "" Then objEMaster._Emer_Con_Estate = dtRow.Item("EmergencyEstate").ToString.Trim
                    If cboEmergencyPlotNumber.Text <> "" Then objEMaster._Emer_Con_Plotno = dtRow.Item("EmergencyPlotNumber").ToString.Trim
                    If cboEmergencyMobile.Text <> "" Then objEMaster._Emer_Con_Mobile = dtRow.Item("EmergencyMobile").ToString.Trim
                    If cboEmergencyAltNo.Text <> "" Then objEMaster._Emer_Con_Alternateno = dtRow.Item("EmergencyAltNo").ToString.Trim
                    If cboEmergencyTelNo.Text <> "" Then objEMaster._Emer_Con_Tel_No = dtRow.Item("EmergencyTelNo").ToString.Trim
                    If cboEmergencyFaxNo.Text <> "" Then objEMaster._Emer_Con_Fax = dtRow.Item("EmergencyFaxNo").ToString.Trim
                    If cboEmergencyEmail.Text <> "" Then objEMaster._Emer_Con_Email = dtRow.Item("EmergencyEmail").ToString.Trim
                ElseIf cboSelectType.SelectedIndex = 4 Then 'Emergency Address2
                    If cboEmergencyFirstname.Text <> "" Then objEMaster._Emer_Con_Firstname2 = dtRow.Item("EmergencyFirstname").ToString.Trim
                    If cboEmergencyLastname.Text <> "" Then objEMaster._Emer_Con_Lastname2 = dtRow.Item("EmergencyLastname").ToString.Trim
                    If cboEmergencyAddress.Text <> "" Then objEMaster._Emer_Con_Address2 = dtRow.Item("EmergencyAddress").ToString.Trim
                    If cboEmergencyCountry.Text <> "" Then objEMaster._Emer_Con_Countryunkid2 = intPostCountry
                    If cboEmergencyState.Text <> "" Then objEMaster._Emer_Con_State2 = intState
                    If cboEmergencyTown.Text <> "" Then objEMaster._Emer_Con_Post_Townunkid2 = intPostTown
                    If cboEmergencyPostCode.Text <> "" Then objEMaster._Emer_Con_Postcodeunkid2 = intPostCode
                    If cboEmergencyProvRegion.Text <> "" Then objEMaster._Emer_Con_Provicnce2 = dtRow.Item("EmergencyProvRegion").ToString.Trim
                    If cboEmergencyRoadStreet.Text <> "" Then objEMaster._Emer_Con_Road2 = dtRow.Item("EmergencyRoadStreet").ToString.Trim
                    If cboEmergencyEstate.Text <> "" Then objEMaster._Emer_Con_Estate2 = dtRow.Item("EmergencyEstate").ToString.Trim
                    If cboEmergencyPlotNumber.Text <> "" Then objEMaster._Emer_Con_Plotno2 = dtRow.Item("EmergencyPlotNumber").ToString.Trim
                    If cboEmergencyMobile.Text <> "" Then objEMaster._Emer_Con_Mobile2 = dtRow.Item("EmergencyMobile").ToString.Trim
                    If cboEmergencyAltNo.Text <> "" Then objEMaster._Emer_Con_Alternateno2 = dtRow.Item("EmergencyAltNo").ToString.Trim
                    If cboEmergencyTelNo.Text <> "" Then objEMaster._Emer_Con_Tel_No2 = dtRow.Item("EmergencyTelNo").ToString.Trim
                    If cboEmergencyFaxNo.Text <> "" Then objEMaster._Emer_Con_Fax2 = dtRow.Item("EmergencyFaxNo").ToString.Trim
                    If cboEmergencyEmail.Text <> "" Then objEMaster._Emer_Con_Email2 = dtRow.Item("EmergencyEmail").ToString.Trim
                ElseIf cboSelectType.SelectedIndex = 5 Then 'Emergency Address3
                    If cboEmergencyFirstname.Text <> "" Then objEMaster._Emer_Con_Firstname3 = dtRow.Item("EmergencyFirstname").ToString.Trim
                    If cboEmergencyLastname.Text <> "" Then objEMaster._Emer_Con_Lastname3 = dtRow.Item("EmergencyLastname").ToString.Trim
                    If cboEmergencyAddress.Text <> "" Then objEMaster._Emer_Con_Address3 = dtRow.Item("EmergencyAddress").ToString.Trim
                    If cboEmergencyCountry.Text <> "" Then objEMaster._Emer_Con_Countryunkid3 = intPostCountry
                    If cboEmergencyState.Text <> "" Then objEMaster._Emer_Con_State3 = intState
                    If cboEmergencyTown.Text <> "" Then objEMaster._Emer_Con_Post_Townunkid3 = intPostTown
                    If cboEmergencyPostCode.Text <> "" Then objEMaster._Emer_Con_Postcodeunkid3 = intPostCode
                    If cboEmergencyProvRegion.Text <> "" Then objEMaster._Emer_Con_Provicnce3 = dtRow.Item("EmergencyProvRegion").ToString.Trim
                    If cboEmergencyRoadStreet.Text <> "" Then objEMaster._Emer_Con_Road3 = dtRow.Item("EmergencyRoadStreet").ToString.Trim
                    If cboEmergencyEstate.Text <> "" Then objEMaster._Emer_Con_Estate3 = dtRow.Item("EmergencyEstate").ToString.Trim
                    If cboEmergencyPlotNumber.Text <> "" Then objEMaster._Emer_Con_Plotno3 = dtRow.Item("EmergencyPlotNumber").ToString.Trim
                    If cboEmergencyMobile.Text <> "" Then objEMaster._Emer_Con_Mobile3 = dtRow.Item("EmergencyMobile").ToString.Trim
                    If cboEmergencyAltNo.Text <> "" Then objEMaster._Emer_Con_Alternateno3 = dtRow.Item("EmergencyAltNo").ToString.Trim
                    If cboEmergencyTelNo.Text <> "" Then objEMaster._Emer_Con_Tel_No3 = dtRow.Item("EmergencyTelNo").ToString.Trim
                    If cboEmergencyFaxNo.Text <> "" Then objEMaster._Emer_Con_Fax3 = dtRow.Item("EmergencyFaxNo").ToString.Trim
                    If cboEmergencyEmail.Text <> "" Then objEMaster._Emer_Con_Email3 = dtRow.Item("EmergencyEmail").ToString.Trim
                    'S.SANDEEP [14-JUN-2018] -- END
                End If

                With objEMaster
                    ._FormName = mstrModuleName
                    ._LoginEmployeeUnkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                'If objEMaster.Update(FinancialYear._Object._DatabaseName, _
                '                      FinancialYear._Object._YearUnkid, _
                '                      Company._Object._Companyunkid, _
                '                      ConfigParameter._Object._CurrentDateAndTime.Date, _
                '                      CStr(ConfigParameter._Object._IsArutiDemo), _
                '                      Company._Object._Total_Active_Employee_ForAllCompany, _
                '                      ConfigParameter._Object._NoOfEmployees, _
                '                      User._Object._Userunkid, _
                '                      ConfigParameter._Object._DonotAttendanceinSeconds, _
                '                      ConfigParameter._Object._UserAccessModeSetting, _
                '                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, _
                '                      ConfigParameter._Object._IsIncludeInactiveEmp, _
                '                      User._Object.Privilege._AllowToApproveEarningDeduction, _
                '                      ConfigParameter._Object._CurrentDateAndTime, , , , , , , , , , , , , getHostName(), getIP, User._Object._Username, enLogin_Mode.DESKTOP) = True Then


                Dim mDicApprFlow As New Dictionary(Of Integer, Integer)
                Call objEMaster.GetApprovalSetting(Company._Object._Companyunkid, mDicApprFlow)




                If cboSelectType.SelectedValue = 1 OrElse _
                   cboSelectType.SelectedValue = 2 OrElse _
                   cboSelectType.SelectedValue = 3 Then

                    If cboSelectType.SelectedValue = 1 Then
                        AddressType = clsEmployeeAddress_approval_tran.enAddressType.PRESENT
                    ElseIf cboSelectType.SelectedValue = 2 Then
                        AddressType = clsEmployeeAddress_approval_tran.enAddressType.DOMICILE
                    ElseIf cboSelectType.SelectedValue = 3 Then
                        AddressType = clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT
                    End If

                ElseIf cboSelectType.SelectedValue = 4 OrElse _
                       cboSelectType.SelectedValue = 5 OrElse _
                       cboSelectType.SelectedValue = 6 Then

                    If cboSelectType.SelectedValue = 4 Then
                        EmeAddressType = clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1

                    ElseIf cboSelectType.SelectedValue = 5 Then
                        EmeAddressType = clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2

                    ElseIf cboSelectType.SelectedValue = 6 Then
                        EmeAddressType = clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3
                    End If

                End If


                If objEMaster.Update(FinancialYear._Object._DatabaseName, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     ConfigParameter._Object._CurrentDateAndTime.Date, _
                                     CStr(ConfigParameter._Object._IsArutiDemo), _
                                     Company._Object._Total_Active_Employee_ForAllCompany, _
                                     ConfigParameter._Object._NoOfEmployees, _
                                     User._Object._Userunkid, _
                                     ConfigParameter._Object._DonotAttendanceinSeconds, _
                                     ConfigParameter._Object._UserAccessModeSetting, _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, _
                                     ConfigParameter._Object._IsIncludeInactiveEmp, _
                                     User._Object.Privilege._AllowToApproveEarningDeduction, _
                                     ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, _
                                     ConfigParameter._Object._UserMustchangePwdOnNextLogon, , , , , , , , , , , , , _
                                     getHostName(), getIP, User._Object._Username, enLogin_Mode.DESKTOP, , , , True, AddressType, False, EmeAddressType) = True Then

                    'Pinkal (18-Aug-2018) -- End

                    'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{getHostName(), getIP, User._Object._Username, enLogin_Mode.DESKTOP})} -- END
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Success")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)


                    'Gajanan [27-May-2019] -- Start              
                    If EmpList.Contains(intEmpId) = False Then
                        EmpList.Add(intEmpId)
                    End If
                    'Gajanan [27-May-2019] -- End

                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objEMaster._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                End If

            Next


            'Gajanan [27-May-2019] -- Start              
            objApprovalData = New clsEmployeeDataApproval
            If mdt_ImportData.Select("status <> '" & Language.getMessage(mstrModuleName, 7, "Fail") & "'").Count > 0 Then
                If IsNothing(EmpList) = False AndAlso EmpList.Count > 0 Then
                    Dim EmpListCsv As String = String.Join(",", EmpList.ToArray())
                    mdt_ImportData.DefaultView.RowFilter = "status <> '" & Language.getMessage(mstrModuleName, 7, "Fail") & "'"

                    If cboSelectType.SelectedValue = 1 OrElse _
                       cboSelectType.SelectedValue = 2 OrElse _
                       cboSelectType.SelectedValue = 3 Then
                        objApprovalData.ImportDataSendNotification(1, FinancialYear._Object._DatabaseName, _
                                                               ConfigParameter._Object._UserAccessModeSetting, _
                                                               Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                               CInt(enUserPriviledge.AllowToApproveRejectAddress), _
                                                               enScreenName.frmAddressList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                               User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                               User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                               User._Object._EmployeeUnkid, getIP(), getHostName(), False, User._Object._Userunkid, _
                                                               ConfigParameter._Object._CurrentDateAndTime, mdt_ImportData.DefaultView.ToTable(), , EmpListCsv, False, AddressType, Nothing)

                    ElseIf cboSelectType.SelectedValue = 4 OrElse _
                           cboSelectType.SelectedValue = 5 OrElse _
                           cboSelectType.SelectedValue = 6 Then

                        objApprovalData.ImportDataSendNotification(1, FinancialYear._Object._DatabaseName, _
                                                               ConfigParameter._Object._UserAccessModeSetting, _
                                                               Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                               CInt(enUserPriviledge.AllowToApproveRejectEmergencyAddress), _
                                                               enScreenName.frmEmergencyAddressList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                               User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                               User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                               User._Object._EmployeeUnkid, getIP(), getHostName(), False, User._Object._Userunkid, _
                                                               ConfigParameter._Object._CurrentDateAndTime, mdt_ImportData.DefaultView.ToTable(), , EmpListCsv, False, EmeAddressType, Nothing)
                    End If


                End If
            End If
            'Gajanan [27-May-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data_Employee", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

    Private Sub Fill_Combo()
        Try




            'Gajanan [27-May-2019] -- Start              




            'cboSelectType.Items.Add(Language.getMessage(mstrModuleName, 17, "Present Address"))
            'cboSelectType.Items.Add(Language.getMessage(mstrModuleName, 18, "Domicile Address"))
            'cboSelectType.Items.Add(Language.getMessage(mstrModuleName, 19, "Recruitment Address"))
            ''S.SANDEEP [14-JUN-2018] -- START
            ''ISSUE/ENHANCEMENT : {NMB}
            'cboSelectType.Items.Add(Language.getMessage(mstrModuleName, 53, "Emergency Address1"))
            'cboSelectType.Items.Add(Language.getMessage(mstrModuleName, 54, "Emergency Address2"))
            'cboSelectType.Items.Add(Language.getMessage(mstrModuleName, 55, "Emergency Address3"))
            ''S.SANDEEP [14-JUN-2018] -- END

            Dim dtAddress As New DataTable

            dtAddress.Columns.Add("AddreessType")
            dtAddress.Columns.Add("AddressTypeid")

            dtAddress.Rows.Add(Language.getMessage(mstrModuleName, 17, "Present Address"), 1)
            dtAddress.Rows.Add(Language.getMessage(mstrModuleName, 18, "Domicile Address"), 2)
            dtAddress.Rows.Add(Language.getMessage(mstrModuleName, 19, "Recruitment Address"), 3)

            dtAddress.Rows.Add(Language.getMessage(mstrModuleName, 53, "Emergency Address1"), 4)
            dtAddress.Rows.Add(Language.getMessage(mstrModuleName, 54, "Emergency Address2"), 5)
            dtAddress.Rows.Add(Language.getMessage(mstrModuleName, 55, "Emergency Address3"), 6)

            cboSelectType.DataSource = dtAddress
            cboSelectType.DisplayMember = "AddreessType"
            cboSelectType.ValueMember = "AddressTypeid"
            'Gajanan [27-May-2019] -- End



            cboSelectType.SelectedIndex = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisiblity()
        Try
            cboEmail.Visible = Not cboSelectType.SelectedIndex = 2
            lblEmail.Visible = Not cboSelectType.SelectedIndex = 2
            cboFax.Visible = Not cboSelectType.SelectedIndex = 2
            lblFax.Visible = Not cboSelectType.SelectedIndex = 2
            cboAlternativeNo.Visible = Not cboSelectType.SelectedIndex = 2
            lblAlternativeNo.Visible = Not cboSelectType.SelectedIndex = 2
            cboMobile.Visible = Not cboSelectType.SelectedIndex = 2
            lblMobile.Visible = Not cboSelectType.SelectedIndex = 2
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisiblity", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)
            If cmb.Text <> "" Then
                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString
                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        If cr.Name <> cmb.Name Then
                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Image Processing Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub cboSelectType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSelectType.SelectedIndexChanged
        Try
            If cboSelectType.SelectedIndex <= 2 Then
                objspc1.Panel1Collapsed = False
                objspc1.Panel2Collapsed = True
            Else
                objspc1.Panel1Collapsed = True
                objspc1.Panel2Collapsed = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSelectType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END

#End Region

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
#Region " Link Event(s) "

    Private Sub lnkAllocationFormat_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocationFormat.LinkClicked
        Try
            Dim dtExTable As New DataTable("DataList")
            dtExTable.Columns.Add("EmployeeCode", System.Type.GetType("System.String")).DefaultValue = ""
            Select Case cboSelectType.SelectedIndex
                Case 0, 1, 2
                    'Gajanan (24 Nov 2018) -- Start
                    'Enhancement : Import template column headers should read from language set for 
                    'users (custom 1) and columns' date formats for importation should be clearly known 
                    'UAT No:TC017 NMB

                    'dtExTable.Columns.Add("Address1", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("Address2", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("PostCountry", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("State", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("PostTown", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("PostCode", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("ProvRegion", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("RoadStreet", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EState", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("RoadStreet1", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("ProvRegion1", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("Chiefdom", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("Village", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("Town1", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("Mobile", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("TelNo", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("PlotNo", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("AlternativeNo", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("Email", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("Fax", System.Type.GetType("System.String")).DefaultValue = ""

                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 12, "Address1"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 13, "Address2"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 14, "PostCountry"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 15, "State"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 16, "PostTown"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 22, "PostCode"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 23, "ProvRegion"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 24, "RoadStreet"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 25, "EState"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 26, "RoadStreet1"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 27, "ProvRegion1"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 28, "Chiefdom"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 29, "Village"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 30, "Town1"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 31, "Mobile"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 32, "TelNo"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 33, "PlotNo"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 34, "AlternativeNo"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 35, "Email"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 36, "Fax"), System.Type.GetType("System.String")).DefaultValue = ""

                    'Gajanan (24 Nov 2018) -- End
                Case 3, 4, 5
                    'Gajanan (24 Nov 2018) -- Start
                    'Enhancement : Import template column headers should read from language set for 
                    'users (custom 1) and columns' date formats for importation should be clearly known 
                    'UAT No:TC017 NMB

                    'dtExTable.Columns.Add("EmergencyFirstname", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyLastname", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyAddress", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyCountry", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyState", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyTown", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyPostCode", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyProvRegion", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyRoadStreet", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyEstate", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyPlotNumber", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyMobile", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyAltNo", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyTelNo", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyFaxNo", System.Type.GetType("System.String")).DefaultValue = ""
                    'dtExTable.Columns.Add("EmergencyEmail", System.Type.GetType("System.String")).DefaultValue = ""


                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 37, "EmergencyFirstname"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 38, "EmergencyLastname"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 39, "EmergencyAddress"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 40, "EmergencyCountry"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 56, "EmergencyState"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 57, "EmergencyTown"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 43, "EmergencyPostCode"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 44, "EmergencyProvRegion"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 45, "EmergencyRoadStreet"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 46, "EmergencyEstate"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 47, "EmergencyPlotNumber"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 48, "EmergencyMobile"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 58, "EmergencyAltNo"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 60, "EmergencyTelNo"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 51, "EmergencyFaxNo"), System.Type.GetType("System.String")).DefaultValue = ""
                    dtExTable.Columns.Add(Language.getMessage(mstrModuleName, 52, "EmergencyEmail"), System.Type.GetType("System.String")).DefaultValue = ""
                    'Gajanan (24 Nov 2018) -- End

                  
            End Select

            If cboSelectType.SelectedIndex = 2 Then
                dtExTable.Columns.Remove("Mobile")
                dtExTable.Columns.Remove("AlternativeNo")
                dtExTable.Columns.Remove("Email")
                dtExTable.Columns.Remove("Fax")
            End If

            Dim dsList As New DataSet
            dsList.Tables.Add(dtExTable.Copy)
            Dim dlgSaveFile As New SaveFileDialog
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"

            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                OpenXML_Export(dlgSaveFile.FileName, dsList)
            End If
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 59, "Template Exported Successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocationFormat_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next

            Dim objParentCtrl As Control = Nothing
            If objspc1.Panel1Collapsed = False Then
                objParentCtrl = pnlOtherAddress
            ElseIf objspc1.Panel2Collapsed = False Then
                objParentCtrl = pnlEmergencyAddress
            End If

            lstCombos = (From p In objParentCtrl.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) And (p).Enabled = True Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB

                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                'Gajanan (24 Nov 2018) -- End


                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In objParentCtrl.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region
    'S.SANDEEP [14-JUN-2018] -- END



#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try

            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.eZeeWizImportImgs.CancelText = Language._Object.getCaption(Me.eZeeWizImportImgs.Name & "_CancelText", Me.eZeeWizImportImgs.CancelText)
            Me.eZeeWizImportImgs.NextText = Language._Object.getCaption(Me.eZeeWizImportImgs.Name & "_NextText", Me.eZeeWizImportImgs.NextText)
            Me.eZeeWizImportImgs.BackText = Language._Object.getCaption(Me.eZeeWizImportImgs.Name & "_BackText", Me.eZeeWizImportImgs.BackText)
            Me.eZeeWizImportImgs.FinishText = Language._Object.getCaption(Me.eZeeWizImportImgs.Name & "_FinishText", Me.eZeeWizImportImgs.FinishText)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblSelectType.Text = Language._Object.getCaption(Me.lblSelectType.Name, Me.lblSelectType.Text)
            Me.lblVillage.Text = Language._Object.getCaption(Me.lblVillage.Name, Me.lblVillage.Text)
            Me.lblProvince1.Text = Language._Object.getCaption(Me.lblProvince1.Name, Me.lblProvince1.Text)
            Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
            Me.lblProvRegion.Text = Language._Object.getCaption(Me.lblProvRegion.Name, Me.lblProvRegion.Text)
            Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
            Me.lblTelNo.Text = Language._Object.getCaption(Me.lblTelNo.Name, Me.lblTelNo.Text)
            Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
            Me.lblTown1.Text = Language._Object.getCaption(Me.lblTown1.Name, Me.lblTown1.Text)
            Me.lblRoadStreet1.Text = Language._Object.getCaption(Me.lblRoadStreet1.Name, Me.lblRoadStreet1.Text)
            Me.lblAddress2.Text = Language._Object.getCaption(Me.lblAddress2.Name, Me.lblAddress2.Text)
            Me.lblPostTown.Text = Language._Object.getCaption(Me.lblPostTown.Name, Me.lblPostTown.Text)
            Me.lblRoad.Text = Language._Object.getCaption(Me.lblRoad.Name, Me.lblRoad.Text)
            Me.lblFax.Text = Language._Object.getCaption(Me.lblFax.Name, Me.lblFax.Text)
            Me.lblPloteNo.Text = Language._Object.getCaption(Me.lblPloteNo.Name, Me.lblPloteNo.Text)
            Me.lblChiefdom.Text = Language._Object.getCaption(Me.lblChiefdom.Name, Me.lblChiefdom.Text)
            Me.lblEstate.Text = Language._Object.getCaption(Me.lblEstate.Name, Me.lblEstate.Text)
            Me.lblAlternativeNo.Text = Language._Object.getCaption(Me.lblAlternativeNo.Name, Me.lblAlternativeNo.Text)
            Me.lblMobile.Text = Language._Object.getCaption(Me.lblMobile.Name, Me.lblMobile.Text)
            Me.lblPostCountry.Text = Language._Object.getCaption(Me.lblPostCountry.Name, Me.lblPostCountry.Text)
            Me.lblPostcode.Text = Language._Object.getCaption(Me.lblPostcode.Name, Me.lblPostcode.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.lblNote.Text = Language._Object.getCaption(Me.lblNote.Name, Me.lblNote.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)
			Me.lblEmgFirstName.Text = Language._Object.getCaption(Me.lblEmgFirstName.Name, Me.lblEmgFirstName.Text)
			Me.lblEmgLastName.Text = Language._Object.getCaption(Me.lblEmgLastName.Name, Me.lblEmgLastName.Text)
			Me.lblEmgAddress.Text = Language._Object.getCaption(Me.lblEmgAddress.Name, Me.lblEmgAddress.Text)
			Me.lblEmgPostCountry.Text = Language._Object.getCaption(Me.lblEmgPostCountry.Name, Me.lblEmgPostCountry.Text)
			Me.lblEmgPostTown.Text = Language._Object.getCaption(Me.lblEmgPostTown.Name, Me.lblEmgPostTown.Text)
			Me.lblEmgState.Text = Language._Object.getCaption(Me.lblEmgState.Name, Me.lblEmgState.Text)
			Me.lblEmgProvince.Text = Language._Object.getCaption(Me.lblEmgProvince.Name, Me.lblEmgProvince.Text)
			Me.lblEmgPostCode.Text = Language._Object.getCaption(Me.lblEmgPostCode.Name, Me.lblEmgPostCode.Text)
			Me.lblEmgRoad.Text = Language._Object.getCaption(Me.lblEmgRoad.Name, Me.lblEmgRoad.Text)
			Me.lblEmgMobile.Text = Language._Object.getCaption(Me.lblEmgMobile.Name, Me.lblEmgMobile.Text)
			Me.lblEmgPlotNo.Text = Language._Object.getCaption(Me.lblEmgPlotNo.Name, Me.lblEmgPlotNo.Text)
			Me.lblEmgEstate.Text = Language._Object.getCaption(Me.lblEmgEstate.Name, Me.lblEmgEstate.Text)
			Me.lblEmgTelNo.Text = Language._Object.getCaption(Me.lblEmgTelNo.Name, Me.lblEmgTelNo.Text)
			Me.lblEmgAltNo.Text = Language._Object.getCaption(Me.lblEmgAltNo.Name, Me.lblEmgAltNo.Text)
			Me.lblEmgFax.Text = Language._Object.getCaption(Me.lblEmgFax.Name, Me.lblEmgFax.Text)
			Me.lblEmgEmail.Text = Language._Object.getCaption(Me.lblEmgEmail.Name, Me.lblEmgEmail.Text)
			Me.lnkAllocationFormat.Text = Language._Object.getCaption(Me.lnkAllocationFormat.Name, Me.lnkAllocationFormat.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Address Type cannot be blank. Please set Address Type to continue")
            Language.setMessage(mstrModuleName, 3, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 4, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Images(s).")
            Language.setMessage(mstrModuleName, 5, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 6, "Post Country Not Found.")
            Language.setMessage(mstrModuleName, 7, "Fail")
            Language.setMessage(mstrModuleName, 8, "State Not Found.")
            Language.setMessage(mstrModuleName, 9, "City Not Found.")
            Language.setMessage(mstrModuleName, 10, "Success")
            Language.setMessage(mstrModuleName, 11, "Post Code Not Found.")
			Language.setMessage(mstrModuleName, 12, "Address1")
			Language.setMessage(mstrModuleName, 13, "Address2")
			Language.setMessage(mstrModuleName, 14, "PostCountry")
			Language.setMessage(mstrModuleName, 15, "State")
			Language.setMessage(mstrModuleName, 16, "PostTown")
            Language.setMessage(mstrModuleName, 17, "Present Address")
            Language.setMessage(mstrModuleName, 18, "Domicile Address")
            Language.setMessage(mstrModuleName, 19, "Recruitment Address")
            Language.setMessage(mstrModuleName, 20, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 21, "Employee data Not Found.")
			Language.setMessage(mstrModuleName, 22, "PostCode")
			Language.setMessage(mstrModuleName, 23, "ProvRegion")
			Language.setMessage(mstrModuleName, 24, "RoadStreet")
			Language.setMessage(mstrModuleName, 25, "EState")
			Language.setMessage(mstrModuleName, 26, "RoadStreet1")
			Language.setMessage(mstrModuleName, 27, "ProvRegion1")
			Language.setMessage(mstrModuleName, 28, "Chiefdom")
			Language.setMessage(mstrModuleName, 29, "Village")
			Language.setMessage(mstrModuleName, 30, "Town1")
			Language.setMessage(mstrModuleName, 31, "Mobile")
			Language.setMessage(mstrModuleName, 32, "TelNo")
			Language.setMessage(mstrModuleName, 33, "PlotNo")
			Language.setMessage(mstrModuleName, 34, "AlternativeNo")
			Language.setMessage(mstrModuleName, 35, "Email")
			Language.setMessage(mstrModuleName, 36, "Fax")
			Language.setMessage(mstrModuleName, 37, "EmergencyFirstname")
			Language.setMessage(mstrModuleName, 38, "EmergencyLastname")
			Language.setMessage(mstrModuleName, 39, "EmergencyAddress")
			Language.setMessage(mstrModuleName, 40, "EmergencyCountry")
			Language.setMessage(mstrModuleName, 41, " Fields From File.")
			Language.setMessage(mstrModuleName, 42, "Please select different Field.")
			Language.setMessage(mstrModuleName, 43, "EmergencyPostCode")
			Language.setMessage(mstrModuleName, 44, "EmergencyProvRegion")
			Language.setMessage(mstrModuleName, 45, "EmergencyRoadStreet")
			Language.setMessage(mstrModuleName, 46, "EmergencyEstate")
			Language.setMessage(mstrModuleName, 47, "EmergencyPlotNumber")
			Language.setMessage(mstrModuleName, 48, "EmergencyMobile")
			Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
			Language.setMessage(mstrModuleName, 50, "is already Mapped with")
			Language.setMessage(mstrModuleName, 51, "EmergencyFaxNo")
			Language.setMessage(mstrModuleName, 52, "EmergencyEmail")
			Language.setMessage(mstrModuleName, 53, "Emergency Address1")
			Language.setMessage(mstrModuleName, 54, "Emergency Address2")
			Language.setMessage(mstrModuleName, 55, "Emergency Address3")
			Language.setMessage(mstrModuleName, 56, "EmergencyState")
			Language.setMessage(mstrModuleName, 57, "EmergencyTown")
			Language.setMessage(mstrModuleName, 58, "EmergencyAltNo")
			Language.setMessage(mstrModuleName, 59, "Template Exported Successfully.")
			Language.setMessage(mstrModuleName, 60, "EmergencyTelNo")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class