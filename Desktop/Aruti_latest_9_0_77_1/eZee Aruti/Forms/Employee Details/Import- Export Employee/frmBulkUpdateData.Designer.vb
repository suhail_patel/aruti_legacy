﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBulkUpdateDataWizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBulkUpdateDataWizard))
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.WizUpdateEmployee = New eZee.Common.eZeeWizard
        Me.wizPageFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbDatatoUpdate = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocationFormat = New System.Windows.Forms.LinkLabel
        Me.chkEmployeeCode = New System.Windows.Forms.CheckBox
        Me.chkEmploymentType = New System.Windows.Forms.CheckBox
        Me.chkPassword = New System.Windows.Forms.CheckBox
        Me.chkSurname = New System.Windows.Forms.CheckBox
        Me.chkFirstname = New System.Windows.Forms.CheckBox
        Me.chkPayPoint = New System.Windows.Forms.CheckBox
        Me.chkPayType = New System.Windows.Forms.CheckBox
        Me.chkTransctionHead = New System.Windows.Forms.CheckBox
        Me.chkBirthdate = New System.Windows.Forms.CheckBox
        Me.chkGender = New System.Windows.Forms.CheckBox
        Me.elOthers = New eZee.Common.eZeeLine
        Me.chkDisplayName = New System.Windows.Forms.CheckBox
        Me.chkEmail = New System.Windows.Forms.CheckBox
        Me.lblTitle = New System.Windows.Forms.Label
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.wizPageData = New eZee.Common.eZeeWizardPage(Me.components)
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.wizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFiledMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.cboEmploymentType = New System.Windows.Forms.ComboBox
        Me.lblEmploymentType = New System.Windows.Forms.Label
        Me.objlblSign30 = New System.Windows.Forms.Label
        Me.cboPassword = New System.Windows.Forms.ComboBox
        Me.lblPassword = New System.Windows.Forms.Label
        Me.objlblSign29 = New System.Windows.Forms.Label
        Me.cboSurname = New System.Windows.Forms.ComboBox
        Me.lblSurname = New System.Windows.Forms.Label
        Me.objlblSign28 = New System.Windows.Forms.Label
        Me.cboFirstname = New System.Windows.Forms.ComboBox
        Me.lblFirstname = New System.Windows.Forms.Label
        Me.objlblSign27 = New System.Windows.Forms.Label
        Me.lblNewEmployeeCode = New System.Windows.Forms.Label
        Me.cboNewEmployeeCode = New System.Windows.Forms.ComboBox
        Me.objlblSign26 = New System.Windows.Forms.Label
        Me.cboPayPoint = New System.Windows.Forms.ComboBox
        Me.lblPayPoint = New System.Windows.Forms.Label
        Me.objlblSign25 = New System.Windows.Forms.Label
        Me.cboPayType = New System.Windows.Forms.ComboBox
        Me.lblPayType = New System.Windows.Forms.Label
        Me.objlblSign24 = New System.Windows.Forms.Label
        Me.cboTransactionHead = New System.Windows.Forms.ComboBox
        Me.lblTransactionHead = New System.Windows.Forms.Label
        Me.objlblSign23 = New System.Windows.Forms.Label
        Me.cboBirthdate = New System.Windows.Forms.ComboBox
        Me.lblBirthdate = New System.Windows.Forms.Label
        Me.objlblSign10 = New System.Windows.Forms.Label
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.lblGender = New System.Windows.Forms.Label
        Me.objlblSign9 = New System.Windows.Forms.Label
        Me.cboDisplayname = New System.Windows.Forms.ComboBox
        Me.lblDisplayname = New System.Windows.Forms.Label
        Me.objlblSign3 = New System.Windows.Forms.Label
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblCaption = New System.Windows.Forms.Label
        Me.lblEmail = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.cboEmail = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objlblSign2 = New System.Windows.Forms.Label
        Me.chkMiddlename = New System.Windows.Forms.CheckBox
        Me.chkFirstappointmentDate = New System.Windows.Forms.CheckBox
        Me.cboMiddlename = New System.Windows.Forms.ComboBox
        Me.lblMiddlename = New System.Windows.Forms.Label
        Me.objlblSign31 = New System.Windows.Forms.Label
        Me.cboAppointmentDate = New System.Windows.Forms.ComboBox
        Me.lblAppointmentDate = New System.Windows.Forms.Label
        Me.objlblSign32 = New System.Windows.Forms.Label
        Me.cmsFilter.SuspendLayout()
        Me.WizUpdateEmployee.SuspendLayout()
        Me.wizPageFile.SuspendLayout()
        Me.gbDatatoUpdate.SuspendLayout()
        Me.wizPageData.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInfo.SuspendLayout()
        Me.wizPageMapping.SuspendLayout()
        Me.gbFiledMapping.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'WizUpdateEmployee
        '
        Me.WizUpdateEmployee.Controls.Add(Me.wizPageMapping)
        Me.WizUpdateEmployee.Controls.Add(Me.wizPageFile)
        Me.WizUpdateEmployee.Controls.Add(Me.wizPageData)
        Me.WizUpdateEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WizUpdateEmployee.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.WizUpdateEmployee.Location = New System.Drawing.Point(0, 0)
        Me.WizUpdateEmployee.Name = "WizUpdateEmployee"
        Me.WizUpdateEmployee.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wizPageFile, Me.wizPageMapping, Me.wizPageData})
        Me.WizUpdateEmployee.SaveEnabled = True
        Me.WizUpdateEmployee.SaveText = "Save && Finish"
        Me.WizUpdateEmployee.SaveVisible = False
        Me.WizUpdateEmployee.SetSaveIndexBeforeFinishIndex = False
        Me.WizUpdateEmployee.Size = New System.Drawing.Size(708, 431)
        Me.WizUpdateEmployee.TabIndex = 2
        Me.WizUpdateEmployee.WelcomeImage = Nothing
        '
        'wizPageFile
        '
        Me.wizPageFile.Controls.Add(Me.gbDatatoUpdate)
        Me.wizPageFile.Controls.Add(Me.lblTitle)
        Me.wizPageFile.Controls.Add(Me.btnOpenFile)
        Me.wizPageFile.Controls.Add(Me.txtFilePath)
        Me.wizPageFile.Controls.Add(Me.lblSelectfile)
        Me.wizPageFile.Location = New System.Drawing.Point(0, 0)
        Me.wizPageFile.Name = "wizPageFile"
        Me.wizPageFile.Size = New System.Drawing.Size(708, 383)
        Me.wizPageFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageFile.TabIndex = 7
        '
        'gbDatatoUpdate
        '
        Me.gbDatatoUpdate.BorderColor = System.Drawing.Color.Black
        Me.gbDatatoUpdate.Checked = False
        Me.gbDatatoUpdate.CollapseAllExceptThis = False
        Me.gbDatatoUpdate.CollapsedHoverImage = Nothing
        Me.gbDatatoUpdate.CollapsedNormalImage = Nothing
        Me.gbDatatoUpdate.CollapsedPressedImage = Nothing
        Me.gbDatatoUpdate.CollapseOnLoad = False
        Me.gbDatatoUpdate.Controls.Add(Me.chkFirstappointmentDate)
        Me.gbDatatoUpdate.Controls.Add(Me.chkMiddlename)
        Me.gbDatatoUpdate.Controls.Add(Me.lnkAllocationFormat)
        Me.gbDatatoUpdate.Controls.Add(Me.chkEmployeeCode)
        Me.gbDatatoUpdate.Controls.Add(Me.chkEmploymentType)
        Me.gbDatatoUpdate.Controls.Add(Me.chkPassword)
        Me.gbDatatoUpdate.Controls.Add(Me.chkSurname)
        Me.gbDatatoUpdate.Controls.Add(Me.chkFirstname)
        Me.gbDatatoUpdate.Controls.Add(Me.chkPayPoint)
        Me.gbDatatoUpdate.Controls.Add(Me.chkPayType)
        Me.gbDatatoUpdate.Controls.Add(Me.chkTransctionHead)
        Me.gbDatatoUpdate.Controls.Add(Me.chkBirthdate)
        Me.gbDatatoUpdate.Controls.Add(Me.chkGender)
        Me.gbDatatoUpdate.Controls.Add(Me.elOthers)
        Me.gbDatatoUpdate.Controls.Add(Me.chkDisplayName)
        Me.gbDatatoUpdate.Controls.Add(Me.chkEmail)
        Me.gbDatatoUpdate.ExpandedHoverImage = Nothing
        Me.gbDatatoUpdate.ExpandedNormalImage = Nothing
        Me.gbDatatoUpdate.ExpandedPressedImage = Nothing
        Me.gbDatatoUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDatatoUpdate.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDatatoUpdate.HeaderHeight = 25
        Me.gbDatatoUpdate.HeaderMessage = ""
        Me.gbDatatoUpdate.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbDatatoUpdate.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDatatoUpdate.HeightOnCollapse = 0
        Me.gbDatatoUpdate.LeftTextSpace = 0
        Me.gbDatatoUpdate.Location = New System.Drawing.Point(177, 137)
        Me.gbDatatoUpdate.Name = "gbDatatoUpdate"
        Me.gbDatatoUpdate.OpenHeight = 300
        Me.gbDatatoUpdate.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDatatoUpdate.ShowBorder = True
        Me.gbDatatoUpdate.ShowCheckBox = False
        Me.gbDatatoUpdate.ShowCollapseButton = False
        Me.gbDatatoUpdate.ShowDefaultBorderColor = True
        Me.gbDatatoUpdate.ShowDownButton = False
        Me.gbDatatoUpdate.ShowHeader = True
        Me.gbDatatoUpdate.Size = New System.Drawing.Size(519, 236)
        Me.gbDatatoUpdate.TabIndex = 23
        Me.gbDatatoUpdate.Temp = 0
        Me.gbDatatoUpdate.Text = "Update Information"
        Me.gbDatatoUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocationFormat
        '
        Me.lnkAllocationFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocationFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocationFormat.Location = New System.Drawing.Point(383, 4)
        Me.lnkAllocationFormat.Name = "lnkAllocationFormat"
        Me.lnkAllocationFormat.Size = New System.Drawing.Size(132, 17)
        Me.lnkAllocationFormat.TabIndex = 38
        Me.lnkAllocationFormat.TabStop = True
        Me.lnkAllocationFormat.Text = "Get Update Format"
        Me.lnkAllocationFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkEmployeeCode
        '
        Me.chkEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmployeeCode.Location = New System.Drawing.Point(45, 68)
        Me.chkEmployeeCode.Name = "chkEmployeeCode"
        Me.chkEmployeeCode.Size = New System.Drawing.Size(155, 17)
        Me.chkEmployeeCode.TabIndex = 37
        Me.chkEmployeeCode.Text = "Employee Code"
        Me.chkEmployeeCode.UseVisualStyleBackColor = True
        '
        'chkEmploymentType
        '
        Me.chkEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmploymentType.Location = New System.Drawing.Point(45, 183)
        Me.chkEmploymentType.Name = "chkEmploymentType"
        Me.chkEmploymentType.Size = New System.Drawing.Size(155, 17)
        Me.chkEmploymentType.TabIndex = 36
        Me.chkEmploymentType.Text = "Employment Type"
        Me.chkEmploymentType.UseVisualStyleBackColor = True
        '
        'chkPassword
        '
        Me.chkPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPassword.Location = New System.Drawing.Point(45, 160)
        Me.chkPassword.Name = "chkPassword"
        Me.chkPassword.Size = New System.Drawing.Size(155, 17)
        Me.chkPassword.TabIndex = 35
        Me.chkPassword.Text = "Password"
        Me.chkPassword.UseVisualStyleBackColor = True
        '
        'chkSurname
        '
        Me.chkSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSurname.Location = New System.Drawing.Point(45, 137)
        Me.chkSurname.Name = "chkSurname"
        Me.chkSurname.Size = New System.Drawing.Size(155, 17)
        Me.chkSurname.TabIndex = 34
        Me.chkSurname.Text = "Surname"
        Me.chkSurname.UseVisualStyleBackColor = True
        '
        'chkFirstname
        '
        Me.chkFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFirstname.Location = New System.Drawing.Point(45, 91)
        Me.chkFirstname.Name = "chkFirstname"
        Me.chkFirstname.Size = New System.Drawing.Size(155, 17)
        Me.chkFirstname.TabIndex = 33
        Me.chkFirstname.Text = "Firstname"
        Me.chkFirstname.UseVisualStyleBackColor = True
        '
        'chkPayPoint
        '
        Me.chkPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPayPoint.Location = New System.Drawing.Point(251, 137)
        Me.chkPayPoint.Name = "chkPayPoint"
        Me.chkPayPoint.Size = New System.Drawing.Size(155, 17)
        Me.chkPayPoint.TabIndex = 31
        Me.chkPayPoint.Text = "Pay Point"
        Me.chkPayPoint.UseVisualStyleBackColor = True
        '
        'chkPayType
        '
        Me.chkPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPayType.Location = New System.Drawing.Point(251, 160)
        Me.chkPayType.Name = "chkPayType"
        Me.chkPayType.Size = New System.Drawing.Size(155, 17)
        Me.chkPayType.TabIndex = 29
        Me.chkPayType.Text = "Pay Type"
        Me.chkPayType.UseVisualStyleBackColor = True
        '
        'chkTransctionHead
        '
        Me.chkTransctionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTransctionHead.Location = New System.Drawing.Point(251, 183)
        Me.chkTransctionHead.Name = "chkTransctionHead"
        Me.chkTransctionHead.Size = New System.Drawing.Size(155, 17)
        Me.chkTransctionHead.TabIndex = 27
        Me.chkTransctionHead.Text = "Transaction Head"
        Me.chkTransctionHead.UseVisualStyleBackColor = True
        '
        'chkBirthdate
        '
        Me.chkBirthdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBirthdate.Location = New System.Drawing.Point(251, 114)
        Me.chkBirthdate.Name = "chkBirthdate"
        Me.chkBirthdate.Size = New System.Drawing.Size(155, 17)
        Me.chkBirthdate.TabIndex = 14
        Me.chkBirthdate.Text = "Birthdate"
        Me.chkBirthdate.UseVisualStyleBackColor = True
        '
        'chkGender
        '
        Me.chkGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkGender.Location = New System.Drawing.Point(45, 206)
        Me.chkGender.Name = "chkGender"
        Me.chkGender.Size = New System.Drawing.Size(155, 17)
        Me.chkGender.TabIndex = 11
        Me.chkGender.Text = "Gender"
        Me.chkGender.UseVisualStyleBackColor = True
        '
        'elOthers
        '
        Me.elOthers.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elOthers.Location = New System.Drawing.Point(14, 35)
        Me.elOthers.Name = "elOthers"
        Me.elOthers.Size = New System.Drawing.Size(414, 17)
        Me.elOthers.TabIndex = 7
        Me.elOthers.Text = "Other Data"
        Me.elOthers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkDisplayName
        '
        Me.chkDisplayName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDisplayName.Location = New System.Drawing.Point(251, 91)
        Me.chkDisplayName.Name = "chkDisplayName"
        Me.chkDisplayName.Size = New System.Drawing.Size(155, 17)
        Me.chkDisplayName.TabIndex = 3
        Me.chkDisplayName.Text = "Displayname"
        Me.chkDisplayName.UseVisualStyleBackColor = True
        '
        'chkEmail
        '
        Me.chkEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmail.Location = New System.Drawing.Point(251, 68)
        Me.chkEmail.Name = "chkEmail"
        Me.chkEmail.Size = New System.Drawing.Size(155, 17)
        Me.chkEmail.TabIndex = 2
        Me.chkEmail.Text = "Email"
        Me.chkEmail.UseVisualStyleBackColor = True
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(173, 22)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(432, 23)
        Me.lblTitle.TabIndex = 22
        Me.lblTitle.Text = "Employee Detail Update Wizard"
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(668, 110)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(28, 20)
        Me.btnOpenFile.TabIndex = 21
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(177, 110)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(485, 21)
        Me.txtFilePath.TabIndex = 20
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(177, 90)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(126, 17)
        Me.lblSelectfile.TabIndex = 19
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wizPageData
        '
        Me.wizPageData.Controls.Add(Me.dgData)
        Me.wizPageData.Controls.Add(Me.btnFilter)
        Me.wizPageData.Controls.Add(Me.pnlInfo)
        Me.wizPageData.Location = New System.Drawing.Point(0, 0)
        Me.wizPageData.Name = "wizPageData"
        Me.wizPageData.Size = New System.Drawing.Size(708, 366)
        Me.wizPageData.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizPageData.TabIndex = 9
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.colhStatus, Me.colhMessage, Me.objdgcolhEmployeeId, Me.objcolhstatus})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(684, 257)
        Me.dgData.TabIndex = 20
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 30
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 175
        '
        'objdgcolhEmployeeId
        '
        Me.objdgcolhEmployeeId.HeaderText = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.Name = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.ReadOnly = True
        Me.objdgcolhEmployeeId.Visible = False
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 331)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(107, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 23
        Me.btnFilter.Text = "Filter"
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(684, 51)
        Me.pnlInfo.TabIndex = 3
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(562, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(448, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(562, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(494, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(607, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(448, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(607, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(494, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wizPageMapping
        '
        Me.wizPageMapping.Controls.Add(Me.gbFiledMapping)
        Me.wizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.wizPageMapping.Name = "wizPageMapping"
        Me.wizPageMapping.Size = New System.Drawing.Size(708, 383)
        Me.wizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageMapping.TabIndex = 8
        '
        'gbFiledMapping
        '
        Me.gbFiledMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFiledMapping.Checked = False
        Me.gbFiledMapping.CollapseAllExceptThis = False
        Me.gbFiledMapping.CollapsedHoverImage = Nothing
        Me.gbFiledMapping.CollapsedNormalImage = Nothing
        Me.gbFiledMapping.CollapsedPressedImage = Nothing
        Me.gbFiledMapping.CollapseOnLoad = False
        Me.gbFiledMapping.Controls.Add(Me.cboAppointmentDate)
        Me.gbFiledMapping.Controls.Add(Me.lblAppointmentDate)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign32)
        Me.gbFiledMapping.Controls.Add(Me.cboMiddlename)
        Me.gbFiledMapping.Controls.Add(Me.lblMiddlename)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign31)
        Me.gbFiledMapping.Controls.Add(Me.lnkAutoMap)
        Me.gbFiledMapping.Controls.Add(Me.cboEmploymentType)
        Me.gbFiledMapping.Controls.Add(Me.lblEmploymentType)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign30)
        Me.gbFiledMapping.Controls.Add(Me.cboPassword)
        Me.gbFiledMapping.Controls.Add(Me.lblPassword)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign29)
        Me.gbFiledMapping.Controls.Add(Me.cboSurname)
        Me.gbFiledMapping.Controls.Add(Me.lblSurname)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign28)
        Me.gbFiledMapping.Controls.Add(Me.cboFirstname)
        Me.gbFiledMapping.Controls.Add(Me.lblFirstname)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign27)
        Me.gbFiledMapping.Controls.Add(Me.lblNewEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.cboNewEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign26)
        Me.gbFiledMapping.Controls.Add(Me.cboPayPoint)
        Me.gbFiledMapping.Controls.Add(Me.lblPayPoint)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign25)
        Me.gbFiledMapping.Controls.Add(Me.cboPayType)
        Me.gbFiledMapping.Controls.Add(Me.lblPayType)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign24)
        Me.gbFiledMapping.Controls.Add(Me.cboTransactionHead)
        Me.gbFiledMapping.Controls.Add(Me.lblTransactionHead)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign23)
        Me.gbFiledMapping.Controls.Add(Me.cboBirthdate)
        Me.gbFiledMapping.Controls.Add(Me.lblBirthdate)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign10)
        Me.gbFiledMapping.Controls.Add(Me.cboGender)
        Me.gbFiledMapping.Controls.Add(Me.lblGender)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign9)
        Me.gbFiledMapping.Controls.Add(Me.cboDisplayname)
        Me.gbFiledMapping.Controls.Add(Me.lblDisplayname)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign3)
        Me.gbFiledMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.lblCaption)
        Me.gbFiledMapping.Controls.Add(Me.lblEmail)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign1)
        Me.gbFiledMapping.Controls.Add(Me.cboEmail)
        Me.gbFiledMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign2)
        Me.gbFiledMapping.ExpandedHoverImage = Nothing
        Me.gbFiledMapping.ExpandedNormalImage = Nothing
        Me.gbFiledMapping.ExpandedPressedImage = Nothing
        Me.gbFiledMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFiledMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFiledMapping.HeaderHeight = 25
        Me.gbFiledMapping.HeaderMessage = ""
        Me.gbFiledMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFiledMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFiledMapping.HeightOnCollapse = 0
        Me.gbFiledMapping.LeftTextSpace = 0
        Me.gbFiledMapping.Location = New System.Drawing.Point(164, 0)
        Me.gbFiledMapping.Name = "gbFiledMapping"
        Me.gbFiledMapping.OpenHeight = 300
        Me.gbFiledMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFiledMapping.ShowBorder = True
        Me.gbFiledMapping.ShowCheckBox = False
        Me.gbFiledMapping.ShowCollapseButton = False
        Me.gbFiledMapping.ShowDefaultBorderColor = True
        Me.gbFiledMapping.ShowDownButton = False
        Me.gbFiledMapping.ShowHeader = True
        Me.gbFiledMapping.Size = New System.Drawing.Size(544, 365)
        Me.gbFiledMapping.TabIndex = 0
        Me.gbFiledMapping.Temp = 0
        Me.gbFiledMapping.Text = "Field Mapping"
        Me.gbFiledMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAutoMap.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAutoMap.Location = New System.Drawing.Point(397, 336)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(135, 20)
        Me.lnkAutoMap.TabIndex = 99
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "Auto Map"
        Me.lnkAutoMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboEmploymentType
        '
        Me.cboEmploymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmploymentType.Enabled = False
        Me.cboEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmploymentType.FormattingEnabled = True
        Me.cboEmploymentType.Location = New System.Drawing.Point(156, 198)
        Me.cboEmploymentType.Name = "cboEmploymentType"
        Me.cboEmploymentType.Size = New System.Drawing.Size(115, 21)
        Me.cboEmploymentType.TabIndex = 97
        '
        'lblEmploymentType
        '
        Me.lblEmploymentType.Enabled = False
        Me.lblEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmploymentType.Location = New System.Drawing.Point(35, 200)
        Me.lblEmploymentType.Name = "lblEmploymentType"
        Me.lblEmploymentType.Size = New System.Drawing.Size(115, 17)
        Me.lblEmploymentType.TabIndex = 96
        Me.lblEmploymentType.Text = "Employment Type"
        Me.lblEmploymentType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign30
        '
        Me.objlblSign30.Enabled = False
        Me.objlblSign30.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign30.ForeColor = System.Drawing.Color.Red
        Me.objlblSign30.Location = New System.Drawing.Point(11, 200)
        Me.objlblSign30.Name = "objlblSign30"
        Me.objlblSign30.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign30.TabIndex = 95
        Me.objlblSign30.Text = "*"
        Me.objlblSign30.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboPassword
        '
        Me.cboPassword.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPassword.Enabled = False
        Me.cboPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPassword.FormattingEnabled = True
        Me.cboPassword.Location = New System.Drawing.Point(156, 171)
        Me.cboPassword.Name = "cboPassword"
        Me.cboPassword.Size = New System.Drawing.Size(115, 21)
        Me.cboPassword.TabIndex = 94
        '
        'lblPassword
        '
        Me.lblPassword.Enabled = False
        Me.lblPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPassword.Location = New System.Drawing.Point(35, 173)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(115, 17)
        Me.lblPassword.TabIndex = 93
        Me.lblPassword.Text = "Password"
        Me.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign29
        '
        Me.objlblSign29.Enabled = False
        Me.objlblSign29.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign29.ForeColor = System.Drawing.Color.Red
        Me.objlblSign29.Location = New System.Drawing.Point(11, 173)
        Me.objlblSign29.Name = "objlblSign29"
        Me.objlblSign29.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign29.TabIndex = 92
        Me.objlblSign29.Text = "*"
        Me.objlblSign29.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboSurname
        '
        Me.cboSurname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSurname.Enabled = False
        Me.cboSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSurname.FormattingEnabled = True
        Me.cboSurname.Location = New System.Drawing.Point(156, 144)
        Me.cboSurname.Name = "cboSurname"
        Me.cboSurname.Size = New System.Drawing.Size(115, 21)
        Me.cboSurname.TabIndex = 91
        '
        'lblSurname
        '
        Me.lblSurname.Enabled = False
        Me.lblSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSurname.Location = New System.Drawing.Point(35, 146)
        Me.lblSurname.Name = "lblSurname"
        Me.lblSurname.Size = New System.Drawing.Size(115, 17)
        Me.lblSurname.TabIndex = 90
        Me.lblSurname.Text = "Surname"
        Me.lblSurname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign28
        '
        Me.objlblSign28.Enabled = False
        Me.objlblSign28.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign28.ForeColor = System.Drawing.Color.Red
        Me.objlblSign28.Location = New System.Drawing.Point(11, 146)
        Me.objlblSign28.Name = "objlblSign28"
        Me.objlblSign28.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign28.TabIndex = 89
        Me.objlblSign28.Text = "*"
        Me.objlblSign28.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboFirstname
        '
        Me.cboFirstname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFirstname.Enabled = False
        Me.cboFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFirstname.FormattingEnabled = True
        Me.cboFirstname.Location = New System.Drawing.Point(156, 90)
        Me.cboFirstname.Name = "cboFirstname"
        Me.cboFirstname.Size = New System.Drawing.Size(115, 21)
        Me.cboFirstname.TabIndex = 88
        '
        'lblFirstname
        '
        Me.lblFirstname.Enabled = False
        Me.lblFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstname.Location = New System.Drawing.Point(35, 92)
        Me.lblFirstname.Name = "lblFirstname"
        Me.lblFirstname.Size = New System.Drawing.Size(115, 17)
        Me.lblFirstname.TabIndex = 87
        Me.lblFirstname.Text = "Firstname"
        Me.lblFirstname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign27
        '
        Me.objlblSign27.Enabled = False
        Me.objlblSign27.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign27.ForeColor = System.Drawing.Color.Red
        Me.objlblSign27.Location = New System.Drawing.Point(11, 92)
        Me.objlblSign27.Name = "objlblSign27"
        Me.objlblSign27.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign27.TabIndex = 86
        Me.objlblSign27.Text = "*"
        Me.objlblSign27.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblNewEmployeeCode
        '
        Me.lblNewEmployeeCode.Enabled = False
        Me.lblNewEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewEmployeeCode.Location = New System.Drawing.Point(35, 65)
        Me.lblNewEmployeeCode.Name = "lblNewEmployeeCode"
        Me.lblNewEmployeeCode.Size = New System.Drawing.Size(115, 17)
        Me.lblNewEmployeeCode.TabIndex = 84
        Me.lblNewEmployeeCode.Text = "New Employee Code"
        Me.lblNewEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNewEmployeeCode
        '
        Me.cboNewEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNewEmployeeCode.Enabled = False
        Me.cboNewEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNewEmployeeCode.FormattingEnabled = True
        Me.cboNewEmployeeCode.Location = New System.Drawing.Point(156, 63)
        Me.cboNewEmployeeCode.Name = "cboNewEmployeeCode"
        Me.cboNewEmployeeCode.Size = New System.Drawing.Size(115, 21)
        Me.cboNewEmployeeCode.TabIndex = 85
        '
        'objlblSign26
        '
        Me.objlblSign26.Enabled = False
        Me.objlblSign26.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign26.ForeColor = System.Drawing.Color.Red
        Me.objlblSign26.Location = New System.Drawing.Point(11, 65)
        Me.objlblSign26.Name = "objlblSign26"
        Me.objlblSign26.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign26.TabIndex = 83
        Me.objlblSign26.Text = "*"
        Me.objlblSign26.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboPayPoint
        '
        Me.cboPayPoint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPoint.Enabled = False
        Me.cboPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPoint.FormattingEnabled = True
        Me.cboPayPoint.Location = New System.Drawing.Point(416, 198)
        Me.cboPayPoint.Name = "cboPayPoint"
        Me.cboPayPoint.Size = New System.Drawing.Size(115, 21)
        Me.cboPayPoint.TabIndex = 81
        '
        'lblPayPoint
        '
        Me.lblPayPoint.Enabled = False
        Me.lblPayPoint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPoint.Location = New System.Drawing.Point(295, 200)
        Me.lblPayPoint.Name = "lblPayPoint"
        Me.lblPayPoint.Size = New System.Drawing.Size(115, 17)
        Me.lblPayPoint.TabIndex = 80
        Me.lblPayPoint.Text = "Pay Point"
        Me.lblPayPoint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign25
        '
        Me.objlblSign25.Enabled = False
        Me.objlblSign25.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign25.ForeColor = System.Drawing.Color.Red
        Me.objlblSign25.Location = New System.Drawing.Point(277, 200)
        Me.objlblSign25.Name = "objlblSign25"
        Me.objlblSign25.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign25.TabIndex = 79
        Me.objlblSign25.Text = "*"
        Me.objlblSign25.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboPayType
        '
        Me.cboPayType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayType.Enabled = False
        Me.cboPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayType.FormattingEnabled = True
        Me.cboPayType.Location = New System.Drawing.Point(416, 171)
        Me.cboPayType.Name = "cboPayType"
        Me.cboPayType.Size = New System.Drawing.Size(115, 21)
        Me.cboPayType.TabIndex = 77
        '
        'lblPayType
        '
        Me.lblPayType.Enabled = False
        Me.lblPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayType.Location = New System.Drawing.Point(295, 173)
        Me.lblPayType.Name = "lblPayType"
        Me.lblPayType.Size = New System.Drawing.Size(115, 17)
        Me.lblPayType.TabIndex = 76
        Me.lblPayType.Text = "Pay Type"
        Me.lblPayType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign24
        '
        Me.objlblSign24.Enabled = False
        Me.objlblSign24.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign24.ForeColor = System.Drawing.Color.Red
        Me.objlblSign24.Location = New System.Drawing.Point(277, 173)
        Me.objlblSign24.Name = "objlblSign24"
        Me.objlblSign24.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign24.TabIndex = 75
        Me.objlblSign24.Text = "*"
        Me.objlblSign24.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboTransactionHead
        '
        Me.cboTransactionHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransactionHead.Enabled = False
        Me.cboTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransactionHead.FormattingEnabled = True
        Me.cboTransactionHead.Location = New System.Drawing.Point(416, 144)
        Me.cboTransactionHead.Name = "cboTransactionHead"
        Me.cboTransactionHead.Size = New System.Drawing.Size(115, 21)
        Me.cboTransactionHead.TabIndex = 73
        '
        'lblTransactionHead
        '
        Me.lblTransactionHead.Enabled = False
        Me.lblTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionHead.Location = New System.Drawing.Point(295, 146)
        Me.lblTransactionHead.Name = "lblTransactionHead"
        Me.lblTransactionHead.Size = New System.Drawing.Size(115, 17)
        Me.lblTransactionHead.TabIndex = 72
        Me.lblTransactionHead.Text = "Transaction Head"
        Me.lblTransactionHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign23
        '
        Me.objlblSign23.Enabled = False
        Me.objlblSign23.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign23.ForeColor = System.Drawing.Color.Red
        Me.objlblSign23.Location = New System.Drawing.Point(277, 146)
        Me.objlblSign23.Name = "objlblSign23"
        Me.objlblSign23.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign23.TabIndex = 71
        Me.objlblSign23.Text = "*"
        Me.objlblSign23.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboBirthdate
        '
        Me.cboBirthdate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBirthdate.Enabled = False
        Me.cboBirthdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBirthdate.FormattingEnabled = True
        Me.cboBirthdate.Location = New System.Drawing.Point(416, 117)
        Me.cboBirthdate.Name = "cboBirthdate"
        Me.cboBirthdate.Size = New System.Drawing.Size(115, 21)
        Me.cboBirthdate.TabIndex = 31
        '
        'lblBirthdate
        '
        Me.lblBirthdate.Enabled = False
        Me.lblBirthdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBirthdate.Location = New System.Drawing.Point(295, 119)
        Me.lblBirthdate.Name = "lblBirthdate"
        Me.lblBirthdate.Size = New System.Drawing.Size(115, 17)
        Me.lblBirthdate.TabIndex = 30
        Me.lblBirthdate.Text = "Birthdate"
        Me.lblBirthdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign10
        '
        Me.objlblSign10.Enabled = False
        Me.objlblSign10.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign10.ForeColor = System.Drawing.Color.Red
        Me.objlblSign10.Location = New System.Drawing.Point(277, 119)
        Me.objlblSign10.Name = "objlblSign10"
        Me.objlblSign10.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign10.TabIndex = 29
        Me.objlblSign10.Text = "*"
        Me.objlblSign10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.Enabled = False
        Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(156, 225)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(115, 21)
        Me.cboGender.TabIndex = 28
        '
        'lblGender
        '
        Me.lblGender.Enabled = False
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(35, 227)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(115, 17)
        Me.lblGender.TabIndex = 27
        Me.lblGender.Text = "Gender"
        Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign9
        '
        Me.objlblSign9.Enabled = False
        Me.objlblSign9.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign9.ForeColor = System.Drawing.Color.Red
        Me.objlblSign9.Location = New System.Drawing.Point(11, 227)
        Me.objlblSign9.Name = "objlblSign9"
        Me.objlblSign9.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign9.TabIndex = 26
        Me.objlblSign9.Text = "*"
        Me.objlblSign9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboDisplayname
        '
        Me.cboDisplayname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisplayname.Enabled = False
        Me.cboDisplayname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDisplayname.FormattingEnabled = True
        Me.cboDisplayname.Location = New System.Drawing.Point(416, 63)
        Me.cboDisplayname.Name = "cboDisplayname"
        Me.cboDisplayname.Size = New System.Drawing.Size(115, 21)
        Me.cboDisplayname.TabIndex = 9
        '
        'lblDisplayname
        '
        Me.lblDisplayname.Enabled = False
        Me.lblDisplayname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisplayname.Location = New System.Drawing.Point(295, 65)
        Me.lblDisplayname.Name = "lblDisplayname"
        Me.lblDisplayname.Size = New System.Drawing.Size(115, 17)
        Me.lblDisplayname.TabIndex = 8
        Me.lblDisplayname.Text = "Displayname"
        Me.lblDisplayname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign3
        '
        Me.objlblSign3.Enabled = False
        Me.objlblSign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign3.ForeColor = System.Drawing.Color.Red
        Me.objlblSign3.Location = New System.Drawing.Point(277, 65)
        Me.objlblSign3.Name = "objlblSign3"
        Me.objlblSign3.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign3.TabIndex = 7
        Me.objlblSign3.Text = "*"
        Me.objlblSign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(156, 36)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(375, 21)
        Me.cboEmployeeCode.TabIndex = 3
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(307, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(234, 19)
        Me.lblCaption.TabIndex = 0
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEmail
        '
        Me.lblEmail.Enabled = False
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(295, 92)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(115, 17)
        Me.lblEmail.TabIndex = 5
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(11, 38)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign1.TabIndex = 1
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboEmail
        '
        Me.cboEmail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmail.Enabled = False
        Me.cboEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmail.FormattingEnabled = True
        Me.cboEmail.Location = New System.Drawing.Point(416, 90)
        Me.cboEmail.Name = "cboEmail"
        Me.cboEmail.Size = New System.Drawing.Size(115, 21)
        Me.cboEmail.TabIndex = 6
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(37, 38)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(115, 17)
        Me.lblEmployeeCode.TabIndex = 2
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign2
        '
        Me.objlblSign2.Enabled = False
        Me.objlblSign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign2.ForeColor = System.Drawing.Color.Red
        Me.objlblSign2.Location = New System.Drawing.Point(277, 92)
        Me.objlblSign2.Name = "objlblSign2"
        Me.objlblSign2.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign2.TabIndex = 4
        Me.objlblSign2.Text = "*"
        Me.objlblSign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'chkMiddlename
        '
        Me.chkMiddlename.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMiddlename.Location = New System.Drawing.Point(45, 114)
        Me.chkMiddlename.Name = "chkMiddlename"
        Me.chkMiddlename.Size = New System.Drawing.Size(155, 17)
        Me.chkMiddlename.TabIndex = 40
        Me.chkMiddlename.Text = "Middle Name"
        Me.chkMiddlename.UseVisualStyleBackColor = True
        '
        'chkFirstappointmentDate
        '
        Me.chkFirstappointmentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFirstappointmentDate.Location = New System.Drawing.Point(251, 206)
        Me.chkFirstappointmentDate.Name = "chkFirstappointmentDate"
        Me.chkFirstappointmentDate.Size = New System.Drawing.Size(155, 17)
        Me.chkFirstappointmentDate.TabIndex = 41
        Me.chkFirstappointmentDate.Text = "First Appointment Date"
        Me.chkFirstappointmentDate.UseVisualStyleBackColor = True
        '
        'cboMiddlename
        '
        Me.cboMiddlename.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMiddlename.Enabled = False
        Me.cboMiddlename.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMiddlename.FormattingEnabled = True
        Me.cboMiddlename.Location = New System.Drawing.Point(156, 117)
        Me.cboMiddlename.Name = "cboMiddlename"
        Me.cboMiddlename.Size = New System.Drawing.Size(115, 21)
        Me.cboMiddlename.TabIndex = 103
        '
        'lblMiddlename
        '
        Me.lblMiddlename.Enabled = False
        Me.lblMiddlename.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMiddlename.Location = New System.Drawing.Point(35, 119)
        Me.lblMiddlename.Name = "lblMiddlename"
        Me.lblMiddlename.Size = New System.Drawing.Size(115, 17)
        Me.lblMiddlename.TabIndex = 102
        Me.lblMiddlename.Text = "Middle Name"
        Me.lblMiddlename.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign31
        '
        Me.objlblSign31.Enabled = False
        Me.objlblSign31.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign31.ForeColor = System.Drawing.Color.Red
        Me.objlblSign31.Location = New System.Drawing.Point(11, 119)
        Me.objlblSign31.Name = "objlblSign31"
        Me.objlblSign31.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign31.TabIndex = 101
        Me.objlblSign31.Text = "*"
        Me.objlblSign31.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboAppointmentDate
        '
        Me.cboAppointmentDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAppointmentDate.Enabled = False
        Me.cboAppointmentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAppointmentDate.FormattingEnabled = True
        Me.cboAppointmentDate.Location = New System.Drawing.Point(417, 225)
        Me.cboAppointmentDate.Name = "cboAppointmentDate"
        Me.cboAppointmentDate.Size = New System.Drawing.Size(115, 21)
        Me.cboAppointmentDate.TabIndex = 106
        '
        'lblAppointmentDate
        '
        Me.lblAppointmentDate.Enabled = False
        Me.lblAppointmentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointmentDate.Location = New System.Drawing.Point(296, 227)
        Me.lblAppointmentDate.Name = "lblAppointmentDate"
        Me.lblAppointmentDate.Size = New System.Drawing.Size(115, 28)
        Me.lblAppointmentDate.TabIndex = 105
        Me.lblAppointmentDate.Text = "First Appointment Date"
        Me.lblAppointmentDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign32
        '
        Me.objlblSign32.Enabled = False
        Me.objlblSign32.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign32.ForeColor = System.Drawing.Color.Red
        Me.objlblSign32.Location = New System.Drawing.Point(278, 227)
        Me.objlblSign32.Name = "objlblSign32"
        Me.objlblSign32.Size = New System.Drawing.Size(12, 17)
        Me.objlblSign32.TabIndex = 104
        Me.objlblSign32.Text = "*"
        Me.objlblSign32.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'frmBulkUpdateDataWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(708, 431)
        Me.Controls.Add(Me.WizUpdateEmployee)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBulkUpdateDataWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Bulk Update Data Wizard"
        Me.cmsFilter.ResumeLayout(False)
        Me.WizUpdateEmployee.ResumeLayout(False)
        Me.wizPageFile.ResumeLayout(False)
        Me.wizPageFile.PerformLayout()
        Me.gbDatatoUpdate.ResumeLayout(False)
        Me.wizPageData.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInfo.ResumeLayout(False)
        Me.wizPageMapping.ResumeLayout(False)
        Me.gbFiledMapping.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WizUpdateEmployee As eZee.Common.eZeeWizard
    Friend WithEvents wizPageFile As eZee.Common.eZeeWizardPage
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents wizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents gbFiledMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents wizPageData As eZee.Common.eZeeWizardPage
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents gbDatatoUpdate As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkDisplayName As System.Windows.Forms.CheckBox
    Friend WithEvents chkEmail As System.Windows.Forms.CheckBox
    Friend WithEvents elOthers As eZee.Common.eZeeLine
    Friend WithEvents cboEmail As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents objlblSign2 As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents cboDisplayname As System.Windows.Forms.ComboBox
    Friend WithEvents lblDisplayname As System.Windows.Forms.Label
    Friend WithEvents objlblSign3 As System.Windows.Forms.Label
    Friend WithEvents chkBirthdate As System.Windows.Forms.CheckBox
    Friend WithEvents chkGender As System.Windows.Forms.CheckBox
    Friend WithEvents cboBirthdate As System.Windows.Forms.ComboBox
    Friend WithEvents lblBirthdate As System.Windows.Forms.Label
    Friend WithEvents objlblSign10 As System.Windows.Forms.Label
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents objlblSign9 As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents chkTransctionHead As System.Windows.Forms.CheckBox
    Friend WithEvents cboTransactionHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransactionHead As System.Windows.Forms.Label
    Friend WithEvents objlblSign23 As System.Windows.Forms.Label
    Friend WithEvents chkPayType As System.Windows.Forms.CheckBox
    Friend WithEvents cboPayType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayType As System.Windows.Forms.Label
    Friend WithEvents objlblSign24 As System.Windows.Forms.Label
    Friend WithEvents chkPayPoint As System.Windows.Forms.CheckBox
    Friend WithEvents cboPayPoint As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPoint As System.Windows.Forms.Label
    Friend WithEvents objlblSign25 As System.Windows.Forms.Label
    Friend WithEvents chkFirstname As System.Windows.Forms.CheckBox
    Friend WithEvents chkEmployeeCode As System.Windows.Forms.CheckBox
    Friend WithEvents chkEmploymentType As System.Windows.Forms.CheckBox
    Friend WithEvents chkPassword As System.Windows.Forms.CheckBox
    Friend WithEvents chkSurname As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAllocationFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents cboEmploymentType As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmploymentType As System.Windows.Forms.Label
    Friend WithEvents objlblSign30 As System.Windows.Forms.Label
    Friend WithEvents cboPassword As System.Windows.Forms.ComboBox
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents objlblSign29 As System.Windows.Forms.Label
    Friend WithEvents cboSurname As System.Windows.Forms.ComboBox
    Friend WithEvents lblSurname As System.Windows.Forms.Label
    Friend WithEvents objlblSign28 As System.Windows.Forms.Label
    Friend WithEvents cboFirstname As System.Windows.Forms.ComboBox
    Friend WithEvents lblFirstname As System.Windows.Forms.Label
    Friend WithEvents objlblSign27 As System.Windows.Forms.Label
    Friend WithEvents lblNewEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents cboNewEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign26 As System.Windows.Forms.Label
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
    Friend WithEvents chkMiddlename As System.Windows.Forms.CheckBox
    Friend WithEvents chkFirstappointmentDate As System.Windows.Forms.CheckBox
    Friend WithEvents cboMiddlename As System.Windows.Forms.ComboBox
    Friend WithEvents lblMiddlename As System.Windows.Forms.Label
    Friend WithEvents objlblSign31 As System.Windows.Forms.Label
    Friend WithEvents cboAppointmentDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblAppointmentDate As System.Windows.Forms.Label
    Friend WithEvents objlblSign32 As System.Windows.Forms.Label
End Class
