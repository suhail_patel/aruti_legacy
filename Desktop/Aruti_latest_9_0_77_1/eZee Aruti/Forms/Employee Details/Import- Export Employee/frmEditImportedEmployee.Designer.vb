﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditImportedEmployee
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditImportedEmployee))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchPayType = New eZee.Common.eZeeGradientButton
        Me.cboPayType = New System.Windows.Forms.ComboBox
        Me.lblPayType = New System.Windows.Forms.Label
        Me.cboEmployeeStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lnkSet = New System.Windows.Forms.LinkLabel
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.radApplytoChecked = New System.Windows.Forms.RadioButton
        Me.radApplytoAll = New System.Windows.Forms.RadioButton
        Me.objbtnSearchCostCenter = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchGrade = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchGradeGrp = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchGradeLevel = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmplType = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchJob = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchUnits = New eZee.Common.eZeeGradientButton
        Me.lblUnits = New System.Windows.Forms.Label
        Me.cboUnits = New System.Windows.Forms.ComboBox
        Me.cboTeams = New System.Windows.Forms.ComboBox
        Me.lblTeam = New System.Windows.Forms.Label
        Me.objbtnSearchTeam = New eZee.Common.eZeeGradientButton
        Me.EZeeStraightLine4 = New eZee.Common.eZeeStraightLine
        Me.cboSectionGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchSecGroup = New eZee.Common.eZeeGradientButton
        Me.lblSectionGroup = New System.Windows.Forms.Label
        Me.cboSections = New System.Windows.Forms.ComboBox
        Me.lblUnitGroup = New System.Windows.Forms.Label
        Me.objbtnSearchSection = New eZee.Common.eZeeGradientButton
        Me.lblSection = New System.Windows.Forms.Label
        Me.cboUnitGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchUnitGrp = New eZee.Common.eZeeGradientButton
        Me.EZeeStraightLine3 = New eZee.Common.eZeeStraightLine
        Me.lblBranch = New System.Windows.Forms.Label
        Me.objbtnSearchDeptGrp = New eZee.Common.eZeeGradientButton
        Me.lblDepartmentGroup = New System.Windows.Forms.Label
        Me.cboDepartmentGrp = New System.Windows.Forms.ComboBox
        Me.objbtnSearchBranch = New eZee.Common.eZeeGradientButton
        Me.cboStation = New System.Windows.Forms.ComboBox
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.objbtnSearchDepartment = New eZee.Common.eZeeGradientButton
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.pnlEmployeeData = New System.Windows.Forms.Panel
        Me.dgvImportInfo = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEmployeeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFirstName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBranch = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDeptGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDept = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSectionGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSection = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUnitGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUnit = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTeam = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhBranchId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhDeptGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSecGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSectionId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhUnitGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhUnitId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTeamId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmpType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhShift = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradeGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGrade = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradelevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTranHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCostCenter = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSalaray = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhPayType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPayType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmploymentId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhShiftId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhDepartmentId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhJobId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGradeGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGradeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGradeLevelId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTranHeadId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCostCenterId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.objbtnAddSalary = New eZee.Common.eZeeGradientButton
        Me.txtSalary = New eZee.TextBox.NumericTextBox
        Me.lblSalary = New System.Windows.Forms.Label
        Me.cboCostcenter = New System.Windows.Forms.ComboBox
        Me.lblCostcenter = New System.Windows.Forms.Label
        Me.cboTranhead = New System.Windows.Forms.ComboBox
        Me.lblTranHead = New System.Windows.Forms.Label
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.lblGrade = New System.Windows.Forms.Label
        Me.lblGradeGrp = New System.Windows.Forms.Label
        Me.cboGradeGrp = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.objLine2 = New eZee.Common.eZeeStraightLine
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.cboEmpType = New System.Windows.Forms.ComboBox
        Me.lblEmpType = New System.Windows.Forms.Label
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.objbtnSearchShift = New eZee.Common.eZeeGradientButton
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.lblShift = New System.Windows.Forms.Label
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnUpdate = New eZee.Common.eZeeLightButton(Me.components)
        Me.ofdlgOpen = New System.Windows.Forms.OpenFileDialog
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.gbMandatoryInfo.SuspendLayout()
        Me.pnlEmployeeData.SuspendLayout()
        CType(Me.dgvImportInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbMandatoryInfo)
        Me.pnlMainInfo.Controls.Add(Me.objefFormFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(833, 586)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchPayType)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPayType)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPayType)
        Me.gbMandatoryInfo.Controls.Add(Me.cboEmployeeStatus)
        Me.gbMandatoryInfo.Controls.Add(Me.lblStatus)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSet)
        Me.gbMandatoryInfo.Controls.Add(Me.EZeeStraightLine2)
        Me.gbMandatoryInfo.Controls.Add(Me.radApplytoChecked)
        Me.gbMandatoryInfo.Controls.Add(Me.radApplytoAll)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchCostCenter)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchGrade)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchGradeGrp)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchGradeLevel)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchEmplType)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchJob)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchUnits)
        Me.gbMandatoryInfo.Controls.Add(Me.lblUnits)
        Me.gbMandatoryInfo.Controls.Add(Me.cboUnits)
        Me.gbMandatoryInfo.Controls.Add(Me.cboTeams)
        Me.gbMandatoryInfo.Controls.Add(Me.lblTeam)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchTeam)
        Me.gbMandatoryInfo.Controls.Add(Me.EZeeStraightLine4)
        Me.gbMandatoryInfo.Controls.Add(Me.cboSectionGroup)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchSecGroup)
        Me.gbMandatoryInfo.Controls.Add(Me.lblSectionGroup)
        Me.gbMandatoryInfo.Controls.Add(Me.cboSections)
        Me.gbMandatoryInfo.Controls.Add(Me.lblUnitGroup)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchSection)
        Me.gbMandatoryInfo.Controls.Add(Me.lblSection)
        Me.gbMandatoryInfo.Controls.Add(Me.cboUnitGroup)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchUnitGrp)
        Me.gbMandatoryInfo.Controls.Add(Me.EZeeStraightLine3)
        Me.gbMandatoryInfo.Controls.Add(Me.lblBranch)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchDeptGrp)
        Me.gbMandatoryInfo.Controls.Add(Me.lblDepartmentGroup)
        Me.gbMandatoryInfo.Controls.Add(Me.cboDepartmentGrp)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchBranch)
        Me.gbMandatoryInfo.Controls.Add(Me.cboStation)
        Me.gbMandatoryInfo.Controls.Add(Me.cboDepartment)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnSearchDepartment)
        Me.gbMandatoryInfo.Controls.Add(Me.lblDepartment)
        Me.gbMandatoryInfo.Controls.Add(Me.pnlEmployeeData)
        Me.gbMandatoryInfo.Controls.Add(Me.EZeeStraightLine1)
        Me.gbMandatoryInfo.Controls.Add(Me.objbtnAddSalary)
        Me.gbMandatoryInfo.Controls.Add(Me.txtSalary)
        Me.gbMandatoryInfo.Controls.Add(Me.lblSalary)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCostcenter)
        Me.gbMandatoryInfo.Controls.Add(Me.lblCostcenter)
        Me.gbMandatoryInfo.Controls.Add(Me.cboTranhead)
        Me.gbMandatoryInfo.Controls.Add(Me.lblTranHead)
        Me.gbMandatoryInfo.Controls.Add(Me.cboGradeLevel)
        Me.gbMandatoryInfo.Controls.Add(Me.lblGradeLevel)
        Me.gbMandatoryInfo.Controls.Add(Me.cboGrade)
        Me.gbMandatoryInfo.Controls.Add(Me.lblGrade)
        Me.gbMandatoryInfo.Controls.Add(Me.lblGradeGrp)
        Me.gbMandatoryInfo.Controls.Add(Me.cboGradeGrp)
        Me.gbMandatoryInfo.Controls.Add(Me.lblJob)
        Me.gbMandatoryInfo.Controls.Add(Me.cboJob)
        Me.gbMandatoryInfo.Controls.Add(Me.objLine2)
        Me.gbMandatoryInfo.Controls.Add(Me.objLine1)
        Me.gbMandatoryInfo.Controls.Add(Me.cboEmpType)
        Me.gbMandatoryInfo.Controls.Add(Me.lblEmpType)
        Me.gbMandatoryInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(833, 531)
        Me.gbMandatoryInfo.TabIndex = 2
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchPayType
        '
        Me.objbtnSearchPayType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPayType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPayType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPayType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPayType.BorderSelected = False
        Me.objbtnSearchPayType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPayType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPayType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPayType.Location = New System.Drawing.Point(243, 213)
        Me.objbtnSearchPayType.Name = "objbtnSearchPayType"
        Me.objbtnSearchPayType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPayType.TabIndex = 339
        '
        'cboPayType
        '
        Me.cboPayType.DropDownHeight = 200
        Me.cboPayType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayType.DropDownWidth = 300
        Me.cboPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayType.FormattingEnabled = True
        Me.cboPayType.IntegralHeight = False
        Me.cboPayType.Location = New System.Drawing.Point(97, 211)
        Me.cboPayType.Name = "cboPayType"
        Me.cboPayType.Size = New System.Drawing.Size(140, 21)
        Me.cboPayType.TabIndex = 337
        '
        'lblPayType
        '
        Me.lblPayType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayType.Location = New System.Drawing.Point(8, 213)
        Me.lblPayType.Name = "lblPayType"
        Me.lblPayType.Size = New System.Drawing.Size(83, 17)
        Me.lblPayType.TabIndex = 338
        Me.lblPayType.Text = "Pay Type"
        Me.lblPayType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployeeStatus
        '
        Me.cboEmployeeStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployeeStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployeeStatus.DropDownHeight = 200
        Me.cboEmployeeStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeStatus.FormattingEnabled = True
        Me.cboEmployeeStatus.IntegralHeight = False
        Me.cboEmployeeStatus.Location = New System.Drawing.Point(657, 184)
        Me.cboEmployeeStatus.Name = "cboEmployeeStatus"
        Me.cboEmployeeStatus.Size = New System.Drawing.Size(140, 21)
        Me.cboEmployeeStatus.TabIndex = 335
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(563, 186)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(88, 17)
        Me.lblStatus.TabIndex = 334
        Me.lblStatus.Text = "Status"
        '
        'lnkSet
        '
        Me.lnkSet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkSet.BackColor = System.Drawing.Color.Transparent
        Me.lnkSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSet.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSet.Location = New System.Drawing.Point(753, 4)
        Me.lnkSet.Name = "lnkSet"
        Me.lnkSet.Size = New System.Drawing.Size(77, 15)
        Me.lnkSet.TabIndex = 55
        Me.lnkSet.TabStop = True
        Me.lnkSet.Text = "[ SET ]"
        Me.lnkSet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(742, 4)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(5, 17)
        Me.EZeeStraightLine2.TabIndex = 54
        '
        'radApplytoChecked
        '
        Me.radApplytoChecked.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radApplytoChecked.BackColor = System.Drawing.Color.Transparent
        Me.radApplytoChecked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoChecked.Location = New System.Drawing.Point(607, 4)
        Me.radApplytoChecked.Name = "radApplytoChecked"
        Me.radApplytoChecked.Size = New System.Drawing.Size(129, 17)
        Me.radApplytoChecked.TabIndex = 15
        Me.radApplytoChecked.TabStop = True
        Me.radApplytoChecked.Text = "Apply To Checked"
        Me.radApplytoChecked.UseVisualStyleBackColor = False
        '
        'radApplytoAll
        '
        Me.radApplytoAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radApplytoAll.BackColor = System.Drawing.Color.Transparent
        Me.radApplytoAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radApplytoAll.Location = New System.Drawing.Point(499, 4)
        Me.radApplytoAll.Name = "radApplytoAll"
        Me.radApplytoAll.Size = New System.Drawing.Size(102, 17)
        Me.radApplytoAll.TabIndex = 14
        Me.radApplytoAll.TabStop = True
        Me.radApplytoAll.Text = "Apply to All"
        Me.radApplytoAll.UseVisualStyleBackColor = False
        '
        'objbtnSearchCostCenter
        '
        Me.objbtnSearchCostCenter.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCostCenter.BorderSelected = False
        Me.objbtnSearchCostCenter.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCostCenter.Location = New System.Drawing.Point(803, 88)
        Me.objbtnSearchCostCenter.Name = "objbtnSearchCostCenter"
        Me.objbtnSearchCostCenter.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCostCenter.TabIndex = 333
        '
        'objbtnSearchGrade
        '
        Me.objbtnSearchGrade.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGrade.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGrade.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGrade.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGrade.BorderSelected = False
        Me.objbtnSearchGrade.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGrade.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGrade.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGrade.Location = New System.Drawing.Point(523, 61)
        Me.objbtnSearchGrade.Name = "objbtnSearchGrade"
        Me.objbtnSearchGrade.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGrade.TabIndex = 329
        '
        'objbtnSearchGradeGrp
        '
        Me.objbtnSearchGradeGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGradeGrp.BorderSelected = False
        Me.objbtnSearchGradeGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGradeGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGradeGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGradeGrp.Location = New System.Drawing.Point(523, 34)
        Me.objbtnSearchGradeGrp.Name = "objbtnSearchGradeGrp"
        Me.objbtnSearchGradeGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGradeGrp.TabIndex = 328
        '
        'objbtnSearchGradeLevel
        '
        Me.objbtnSearchGradeLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGradeLevel.BorderSelected = False
        Me.objbtnSearchGradeLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGradeLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGradeLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGradeLevel.Location = New System.Drawing.Point(523, 88)
        Me.objbtnSearchGradeLevel.Name = "objbtnSearchGradeLevel"
        Me.objbtnSearchGradeLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGradeLevel.TabIndex = 330
        '
        'objbtnSearchEmplType
        '
        Me.objbtnSearchEmplType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmplType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmplType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmplType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmplType.BorderSelected = False
        Me.objbtnSearchEmplType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmplType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmplType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmplType.Location = New System.Drawing.Point(243, 34)
        Me.objbtnSearchEmplType.Name = "objbtnSearchEmplType"
        Me.objbtnSearchEmplType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmplType.TabIndex = 325
        '
        'objbtnSearchJob
        '
        Me.objbtnSearchJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJob.BorderSelected = False
        Me.objbtnSearchJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJob.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJob.Location = New System.Drawing.Point(243, 63)
        Me.objbtnSearchJob.Name = "objbtnSearchJob"
        Me.objbtnSearchJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJob.TabIndex = 327
        '
        'objbtnSearchUnits
        '
        Me.objbtnSearchUnits.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUnits.BorderSelected = False
        Me.objbtnSearchUnits.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUnits.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUnits.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUnits.Location = New System.Drawing.Point(803, 130)
        Me.objbtnSearchUnits.Name = "objbtnSearchUnits"
        Me.objbtnSearchUnits.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUnits.TabIndex = 324
        '
        'lblUnits
        '
        Me.lblUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnits.Location = New System.Drawing.Point(563, 132)
        Me.lblUnits.Name = "lblUnits"
        Me.lblUnits.Size = New System.Drawing.Size(88, 17)
        Me.lblUnits.TabIndex = 321
        Me.lblUnits.Text = "Units"
        Me.lblUnits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnits
        '
        Me.cboUnits.DropDownHeight = 200
        Me.cboUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnits.DropDownWidth = 300
        Me.cboUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnits.FormattingEnabled = True
        Me.cboUnits.IntegralHeight = False
        Me.cboUnits.Location = New System.Drawing.Point(657, 130)
        Me.cboUnits.Name = "cboUnits"
        Me.cboUnits.Size = New System.Drawing.Size(140, 21)
        Me.cboUnits.TabIndex = 319
        '
        'cboTeams
        '
        Me.cboTeams.DropDownHeight = 200
        Me.cboTeams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTeams.DropDownWidth = 300
        Me.cboTeams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeams.FormattingEnabled = True
        Me.cboTeams.IntegralHeight = False
        Me.cboTeams.Location = New System.Drawing.Point(657, 157)
        Me.cboTeams.Name = "cboTeams"
        Me.cboTeams.Size = New System.Drawing.Size(140, 21)
        Me.cboTeams.TabIndex = 320
        '
        'lblTeam
        '
        Me.lblTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.Location = New System.Drawing.Point(563, 159)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(88, 17)
        Me.lblTeam.TabIndex = 322
        Me.lblTeam.Text = "Team"
        Me.lblTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchTeam
        '
        Me.objbtnSearchTeam.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTeam.BorderSelected = False
        Me.objbtnSearchTeam.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTeam.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTeam.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTeam.Location = New System.Drawing.Point(803, 157)
        Me.objbtnSearchTeam.Name = "objbtnSearchTeam"
        Me.objbtnSearchTeam.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTeam.TabIndex = 323
        '
        'EZeeStraightLine4
        '
        Me.EZeeStraightLine4.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeStraightLine4.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine4.Location = New System.Drawing.Point(550, 130)
        Me.EZeeStraightLine4.Name = "EZeeStraightLine4"
        Me.EZeeStraightLine4.Size = New System.Drawing.Size(7, 100)
        Me.EZeeStraightLine4.TabIndex = 318
        '
        'cboSectionGroup
        '
        Me.cboSectionGroup.DropDownHeight = 200
        Me.cboSectionGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSectionGroup.DropDownWidth = 300
        Me.cboSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGroup.FormattingEnabled = True
        Me.cboSectionGroup.IntegralHeight = False
        Me.cboSectionGroup.Location = New System.Drawing.Point(377, 130)
        Me.cboSectionGroup.Name = "cboSectionGroup"
        Me.cboSectionGroup.Size = New System.Drawing.Size(140, 21)
        Me.cboSectionGroup.TabIndex = 309
        '
        'objbtnSearchSecGroup
        '
        Me.objbtnSearchSecGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSecGroup.BorderSelected = False
        Me.objbtnSearchSecGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSecGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSecGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSecGroup.Location = New System.Drawing.Point(523, 130)
        Me.objbtnSearchSecGroup.Name = "objbtnSearchSecGroup"
        Me.objbtnSearchSecGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSecGroup.TabIndex = 313
        '
        'lblSectionGroup
        '
        Me.lblSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSectionGroup.Location = New System.Drawing.Point(283, 132)
        Me.lblSectionGroup.Name = "lblSectionGroup"
        Me.lblSectionGroup.Size = New System.Drawing.Size(88, 17)
        Me.lblSectionGroup.TabIndex = 312
        Me.lblSectionGroup.Text = "Sec. Group"
        Me.lblSectionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSections
        '
        Me.cboSections.DropDownHeight = 200
        Me.cboSections.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSections.DropDownWidth = 300
        Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSections.FormattingEnabled = True
        Me.cboSections.IntegralHeight = False
        Me.cboSections.Location = New System.Drawing.Point(377, 157)
        Me.cboSections.Name = "cboSections"
        Me.cboSections.Size = New System.Drawing.Size(140, 21)
        Me.cboSections.TabIndex = 310
        '
        'lblUnitGroup
        '
        Me.lblUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitGroup.Location = New System.Drawing.Point(283, 186)
        Me.lblUnitGroup.Name = "lblUnitGroup"
        Me.lblUnitGroup.Size = New System.Drawing.Size(88, 17)
        Me.lblUnitGroup.TabIndex = 315
        Me.lblUnitGroup.Text = "Unit Group"
        Me.lblUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchSection
        '
        Me.objbtnSearchSection.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSection.BorderSelected = False
        Me.objbtnSearchSection.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSection.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSection.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSection.Location = New System.Drawing.Point(523, 157)
        Me.objbtnSearchSection.Name = "objbtnSearchSection"
        Me.objbtnSearchSection.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSection.TabIndex = 316
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(283, 159)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(88, 17)
        Me.lblSection.TabIndex = 314
        Me.lblSection.Text = "Sections"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnitGroup
        '
        Me.cboUnitGroup.DropDownHeight = 200
        Me.cboUnitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnitGroup.DropDownWidth = 300
        Me.cboUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGroup.FormattingEnabled = True
        Me.cboUnitGroup.IntegralHeight = False
        Me.cboUnitGroup.Location = New System.Drawing.Point(377, 184)
        Me.cboUnitGroup.Name = "cboUnitGroup"
        Me.cboUnitGroup.Size = New System.Drawing.Size(140, 21)
        Me.cboUnitGroup.TabIndex = 311
        '
        'objbtnSearchUnitGrp
        '
        Me.objbtnSearchUnitGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUnitGrp.BorderSelected = False
        Me.objbtnSearchUnitGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUnitGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUnitGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUnitGrp.Location = New System.Drawing.Point(523, 184)
        Me.objbtnSearchUnitGrp.Name = "objbtnSearchUnitGrp"
        Me.objbtnSearchUnitGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUnitGrp.TabIndex = 317
        '
        'EZeeStraightLine3
        '
        Me.EZeeStraightLine3.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeStraightLine3.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine3.Location = New System.Drawing.Point(270, 130)
        Me.EZeeStraightLine3.Name = "EZeeStraightLine3"
        Me.EZeeStraightLine3.Size = New System.Drawing.Size(7, 100)
        Me.EZeeStraightLine3.TabIndex = 308
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 132)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(83, 17)
        Me.lblBranch.TabIndex = 302
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchDeptGrp
        '
        Me.objbtnSearchDeptGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDeptGrp.BorderSelected = False
        Me.objbtnSearchDeptGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDeptGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDeptGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDeptGrp.Location = New System.Drawing.Point(243, 157)
        Me.objbtnSearchDeptGrp.Name = "objbtnSearchDeptGrp"
        Me.objbtnSearchDeptGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDeptGrp.TabIndex = 305
        '
        'lblDepartmentGroup
        '
        Me.lblDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartmentGroup.Location = New System.Drawing.Point(8, 159)
        Me.lblDepartmentGroup.Name = "lblDepartmentGroup"
        Me.lblDepartmentGroup.Size = New System.Drawing.Size(83, 17)
        Me.lblDepartmentGroup.TabIndex = 304
        Me.lblDepartmentGroup.Text = "Dept. Group"
        Me.lblDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDepartmentGrp
        '
        Me.cboDepartmentGrp.DropDownHeight = 200
        Me.cboDepartmentGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartmentGrp.DropDownWidth = 300
        Me.cboDepartmentGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartmentGrp.FormattingEnabled = True
        Me.cboDepartmentGrp.IntegralHeight = False
        Me.cboDepartmentGrp.Location = New System.Drawing.Point(97, 157)
        Me.cboDepartmentGrp.Name = "cboDepartmentGrp"
        Me.cboDepartmentGrp.Size = New System.Drawing.Size(140, 21)
        Me.cboDepartmentGrp.TabIndex = 300
        '
        'objbtnSearchBranch
        '
        Me.objbtnSearchBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBranch.BorderSelected = False
        Me.objbtnSearchBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBranch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBranch.Location = New System.Drawing.Point(243, 130)
        Me.objbtnSearchBranch.Name = "objbtnSearchBranch"
        Me.objbtnSearchBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBranch.TabIndex = 303
        '
        'cboStation
        '
        Me.cboStation.DropDownHeight = 200
        Me.cboStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStation.DropDownWidth = 300
        Me.cboStation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStation.FormattingEnabled = True
        Me.cboStation.IntegralHeight = False
        Me.cboStation.Location = New System.Drawing.Point(97, 130)
        Me.cboStation.Name = "cboStation"
        Me.cboStation.Size = New System.Drawing.Size(140, 21)
        Me.cboStation.TabIndex = 299
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownHeight = 200
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.DropDownWidth = 300
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.IntegralHeight = False
        Me.cboDepartment.Location = New System.Drawing.Point(97, 184)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(140, 21)
        Me.cboDepartment.TabIndex = 301
        '
        'objbtnSearchDepartment
        '
        Me.objbtnSearchDepartment.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDepartment.BorderSelected = False
        Me.objbtnSearchDepartment.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDepartment.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDepartment.Location = New System.Drawing.Point(243, 184)
        Me.objbtnSearchDepartment.Name = "objbtnSearchDepartment"
        Me.objbtnSearchDepartment.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDepartment.TabIndex = 307
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 186)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(83, 17)
        Me.lblDepartment.TabIndex = 306
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEmployeeData
        '
        Me.pnlEmployeeData.Controls.Add(Me.dgvImportInfo)
        Me.pnlEmployeeData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployeeData.Location = New System.Drawing.Point(11, 238)
        Me.pnlEmployeeData.Name = "pnlEmployeeData"
        Me.pnlEmployeeData.Size = New System.Drawing.Size(813, 287)
        Me.pnlEmployeeData.TabIndex = 52
        '
        'dgvImportInfo
        '
        Me.dgvImportInfo.AllowUserToAddRows = False
        Me.dgvImportInfo.AllowUserToDeleteRows = False
        Me.dgvImportInfo.AllowUserToResizeColumns = False
        Me.dgvImportInfo.AllowUserToResizeRows = False
        Me.dgvImportInfo.BackgroundColor = System.Drawing.Color.White
        Me.dgvImportInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvImportInfo.ColumnHeadersHeight = 22
        Me.dgvImportInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvImportInfo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhEmployeeCode, Me.dgcolhFirstName, Me.dgcolhBranch, Me.dgcolhDeptGroup, Me.dgcolhDept, Me.dgcolhSectionGroup, Me.dgcolhSection, Me.dgcolhUnitGroup, Me.dgcolhUnit, Me.dgcolhTeam, Me.objdgcolhBranchId, Me.objdgcolhDeptGrpId, Me.objdgcolhSecGrpId, Me.objdgcolhSectionId, Me.objdgcolhUnitGrpId, Me.objdgcolhUnitId, Me.objdgcolhTeamId, Me.dgcolhEmpType, Me.dgcolhShift, Me.dgcolhJob, Me.dgcolhGradeGrp, Me.dgcolhGrade, Me.dgcolhGradelevel, Me.dgcolhTranHead, Me.dgcolhCostCenter, Me.dgcolhSalaray, Me.dgcolhPayType, Me.objdgcolhPayType, Me.objdgcolhEmploymentId, Me.objdgcolhShiftId, Me.objdgcolhDepartmentId, Me.objdgcolhJobId, Me.objdgcolhGradeGrpId, Me.objdgcolhGradeId, Me.objdgcolhGradeLevelId, Me.objdgcolhTranHeadId, Me.objdgcolhCostCenterId})
        Me.dgvImportInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvImportInfo.Location = New System.Drawing.Point(0, 0)
        Me.dgvImportInfo.MultiSelect = False
        Me.dgvImportInfo.Name = "dgvImportInfo"
        Me.dgvImportInfo.RowHeadersVisible = False
        Me.dgvImportInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvImportInfo.Size = New System.Drawing.Size(813, 287)
        Me.dgvImportInfo.TabIndex = 5
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhEmployeeCode
        '
        Me.dgcolhEmployeeCode.HeaderText = "Code"
        Me.dgcolhEmployeeCode.Name = "dgcolhEmployeeCode"
        Me.dgcolhEmployeeCode.ReadOnly = True
        Me.dgcolhEmployeeCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmployeeCode.Width = 80
        '
        'dgcolhFirstName
        '
        Me.dgcolhFirstName.HeaderText = "Name"
        Me.dgcolhFirstName.Name = "dgcolhFirstName"
        Me.dgcolhFirstName.ReadOnly = True
        Me.dgcolhFirstName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhFirstName.Width = 150
        '
        'dgcolhBranch
        '
        Me.dgcolhBranch.HeaderText = "Branch"
        Me.dgcolhBranch.Name = "dgcolhBranch"
        Me.dgcolhBranch.ReadOnly = True
        '
        'dgcolhDeptGroup
        '
        Me.dgcolhDeptGroup.HeaderText = "Department Group"
        Me.dgcolhDeptGroup.Name = "dgcolhDeptGroup"
        Me.dgcolhDeptGroup.ReadOnly = True
        '
        'dgcolhDept
        '
        Me.dgcolhDept.HeaderText = "Department"
        Me.dgcolhDept.Name = "dgcolhDept"
        Me.dgcolhDept.ReadOnly = True
        Me.dgcolhDept.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSectionGroup
        '
        Me.dgcolhSectionGroup.HeaderText = "Section Group"
        Me.dgcolhSectionGroup.Name = "dgcolhSectionGroup"
        Me.dgcolhSectionGroup.ReadOnly = True
        '
        'dgcolhSection
        '
        Me.dgcolhSection.HeaderText = "Section"
        Me.dgcolhSection.Name = "dgcolhSection"
        Me.dgcolhSection.ReadOnly = True
        '
        'dgcolhUnitGroup
        '
        Me.dgcolhUnitGroup.HeaderText = "Unit Group"
        Me.dgcolhUnitGroup.Name = "dgcolhUnitGroup"
        Me.dgcolhUnitGroup.ReadOnly = True
        '
        'dgcolhUnit
        '
        Me.dgcolhUnit.HeaderText = "Unit"
        Me.dgcolhUnit.Name = "dgcolhUnit"
        Me.dgcolhUnit.ReadOnly = True
        '
        'dgcolhTeam
        '
        Me.dgcolhTeam.HeaderText = "Team"
        Me.dgcolhTeam.Name = "dgcolhTeam"
        Me.dgcolhTeam.ReadOnly = True
        '
        'objdgcolhBranchId
        '
        Me.objdgcolhBranchId.HeaderText = "objdgcolhBranchId"
        Me.objdgcolhBranchId.Name = "objdgcolhBranchId"
        Me.objdgcolhBranchId.Visible = False
        '
        'objdgcolhDeptGrpId
        '
        Me.objdgcolhDeptGrpId.HeaderText = "objdgcolhDeptGrpId"
        Me.objdgcolhDeptGrpId.Name = "objdgcolhDeptGrpId"
        Me.objdgcolhDeptGrpId.Visible = False
        '
        'objdgcolhSecGrpId
        '
        Me.objdgcolhSecGrpId.HeaderText = "objdgcolhSecGrpId"
        Me.objdgcolhSecGrpId.Name = "objdgcolhSecGrpId"
        Me.objdgcolhSecGrpId.Visible = False
        '
        'objdgcolhSectionId
        '
        Me.objdgcolhSectionId.HeaderText = "objdgcolhSectionId"
        Me.objdgcolhSectionId.Name = "objdgcolhSectionId"
        Me.objdgcolhSectionId.Visible = False
        '
        'objdgcolhUnitGrpId
        '
        Me.objdgcolhUnitGrpId.HeaderText = "objdgcolhUnitGrpId"
        Me.objdgcolhUnitGrpId.Name = "objdgcolhUnitGrpId"
        Me.objdgcolhUnitGrpId.Visible = False
        '
        'objdgcolhUnitId
        '
        Me.objdgcolhUnitId.HeaderText = "objdgcolhUnitId"
        Me.objdgcolhUnitId.Name = "objdgcolhUnitId"
        Me.objdgcolhUnitId.Visible = False
        '
        'objdgcolhTeamId
        '
        Me.objdgcolhTeamId.HeaderText = "objdgcolhTeamId"
        Me.objdgcolhTeamId.Name = "objdgcolhTeamId"
        Me.objdgcolhTeamId.Visible = False
        '
        'dgcolhEmpType
        '
        Me.dgcolhEmpType.HeaderText = "Employement Type"
        Me.dgcolhEmpType.Name = "dgcolhEmpType"
        Me.dgcolhEmpType.ReadOnly = True
        Me.dgcolhEmpType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmpType.Width = 130
        '
        'dgcolhShift
        '
        Me.dgcolhShift.HeaderText = "Shift"
        Me.dgcolhShift.Name = "dgcolhShift"
        Me.dgcolhShift.ReadOnly = True
        Me.dgcolhShift.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhJob
        '
        Me.dgcolhJob.HeaderText = "Job"
        Me.dgcolhJob.Name = "dgcolhJob"
        Me.dgcolhJob.ReadOnly = True
        Me.dgcolhJob.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGradeGrp
        '
        Me.dgcolhGradeGrp.HeaderText = "Grade Group"
        Me.dgcolhGradeGrp.Name = "dgcolhGradeGrp"
        Me.dgcolhGradeGrp.ReadOnly = True
        Me.dgcolhGradeGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGrade
        '
        Me.dgcolhGrade.HeaderText = "Grade"
        Me.dgcolhGrade.Name = "dgcolhGrade"
        Me.dgcolhGrade.ReadOnly = True
        Me.dgcolhGrade.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGradelevel
        '
        Me.dgcolhGradelevel.HeaderText = "Grade Level"
        Me.dgcolhGradelevel.Name = "dgcolhGradelevel"
        Me.dgcolhGradelevel.ReadOnly = True
        Me.dgcolhGradelevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhTranHead
        '
        Me.dgcolhTranHead.HeaderText = "Tran. Head"
        Me.dgcolhTranHead.Name = "dgcolhTranHead"
        Me.dgcolhTranHead.ReadOnly = True
        Me.dgcolhTranHead.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCostCenter
        '
        Me.dgcolhCostCenter.HeaderText = "Cost Center"
        Me.dgcolhCostCenter.Name = "dgcolhCostCenter"
        Me.dgcolhCostCenter.ReadOnly = True
        Me.dgcolhCostCenter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSalaray
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F0"
        Me.dgcolhSalaray.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhSalaray.HeaderText = "Salary"
        Me.dgcolhSalaray.Name = "dgcolhSalaray"
        Me.dgcolhSalaray.ReadOnly = True
        Me.dgcolhSalaray.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhSalaray.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhPayType
        '
        Me.dgcolhPayType.HeaderText = "Pay Type"
        Me.dgcolhPayType.Name = "dgcolhPayType"
        Me.dgcolhPayType.ReadOnly = True
        '
        'objdgcolhPayType
        '
        Me.objdgcolhPayType.HeaderText = "objdgcolhPayType"
        Me.objdgcolhPayType.Name = "objdgcolhPayType"
        Me.objdgcolhPayType.ReadOnly = True
        Me.objdgcolhPayType.Visible = False
        '
        'objdgcolhEmploymentId
        '
        Me.objdgcolhEmploymentId.HeaderText = "objdgcolhEmploymentId"
        Me.objdgcolhEmploymentId.Name = "objdgcolhEmploymentId"
        Me.objdgcolhEmploymentId.Visible = False
        '
        'objdgcolhShiftId
        '
        Me.objdgcolhShiftId.HeaderText = "objdgcolhShiftId"
        Me.objdgcolhShiftId.Name = "objdgcolhShiftId"
        Me.objdgcolhShiftId.Visible = False
        '
        'objdgcolhDepartmentId
        '
        Me.objdgcolhDepartmentId.HeaderText = "objdgcolhDepartmentId"
        Me.objdgcolhDepartmentId.Name = "objdgcolhDepartmentId"
        Me.objdgcolhDepartmentId.Visible = False
        '
        'objdgcolhJobId
        '
        Me.objdgcolhJobId.HeaderText = "objdgcolhJobId"
        Me.objdgcolhJobId.Name = "objdgcolhJobId"
        Me.objdgcolhJobId.Visible = False
        '
        'objdgcolhGradeGrpId
        '
        Me.objdgcolhGradeGrpId.HeaderText = "objdgcolhGradeGrpId"
        Me.objdgcolhGradeGrpId.Name = "objdgcolhGradeGrpId"
        Me.objdgcolhGradeGrpId.Visible = False
        '
        'objdgcolhGradeId
        '
        Me.objdgcolhGradeId.HeaderText = "objdgcolhGradeId"
        Me.objdgcolhGradeId.Name = "objdgcolhGradeId"
        Me.objdgcolhGradeId.Visible = False
        '
        'objdgcolhGradeLevelId
        '
        Me.objdgcolhGradeLevelId.HeaderText = "objdgcolhGradeLevelId"
        Me.objdgcolhGradeLevelId.Name = "objdgcolhGradeLevelId"
        Me.objdgcolhGradeLevelId.Visible = False
        '
        'objdgcolhTranHeadId
        '
        Me.objdgcolhTranHeadId.HeaderText = "objdgcolhTranHeadId"
        Me.objdgcolhTranHeadId.Name = "objdgcolhTranHeadId"
        Me.objdgcolhTranHeadId.Visible = False
        '
        'objdgcolhCostCenterId
        '
        Me.objdgcolhCostCenterId.HeaderText = "objdgcolhCostCenterId"
        Me.objdgcolhCostCenterId.Name = "objdgcolhCostCenterId"
        Me.objdgcolhCostCenterId.Visible = False
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(12, 110)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(812, 14)
        Me.EZeeStraightLine1.TabIndex = 51
        '
        'objbtnAddSalary
        '
        Me.objbtnAddSalary.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddSalary.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddSalary.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddSalary.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddSalary.BorderSelected = False
        Me.objbtnAddSalary.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddSalary.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddSalary.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddSalary.Location = New System.Drawing.Point(800, 34)
        Me.objbtnAddSalary.Name = "objbtnAddSalary"
        Me.objbtnAddSalary.Size = New System.Drawing.Size(24, 21)
        Me.objbtnAddSalary.TabIndex = 46
        '
        'txtSalary
        '
        Me.txtSalary.AllowNegative = True
        Me.txtSalary.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtSalary.DigitsInGroup = 0
        Me.txtSalary.Flags = 0
        Me.txtSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalary.Location = New System.Drawing.Point(657, 34)
        Me.txtSalary.MaxDecimalPlaces = 6
        Me.txtSalary.MaxWholeDigits = 21
        Me.txtSalary.Name = "txtSalary"
        Me.txtSalary.Prefix = ""
        Me.txtSalary.RangeMax = 1.7976931348623157E+308
        Me.txtSalary.RangeMin = -1.7976931348623157E+308
        Me.txtSalary.Size = New System.Drawing.Size(140, 21)
        Me.txtSalary.TabIndex = 34
        Me.txtSalary.Text = "0.00"
        Me.txtSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblSalary
        '
        Me.lblSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalary.Location = New System.Drawing.Point(563, 36)
        Me.lblSalary.Name = "lblSalary"
        Me.lblSalary.Size = New System.Drawing.Size(88, 17)
        Me.lblSalary.TabIndex = 33
        Me.lblSalary.Text = "Salary"
        Me.lblSalary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCostcenter
        '
        Me.cboCostcenter.DropDownHeight = 200
        Me.cboCostcenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostcenter.FormattingEnabled = True
        Me.cboCostcenter.IntegralHeight = False
        Me.cboCostcenter.Location = New System.Drawing.Point(657, 88)
        Me.cboCostcenter.Name = "cboCostcenter"
        Me.cboCostcenter.Size = New System.Drawing.Size(140, 21)
        Me.cboCostcenter.TabIndex = 31
        '
        'lblCostcenter
        '
        Me.lblCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostcenter.Location = New System.Drawing.Point(563, 90)
        Me.lblCostcenter.Name = "lblCostcenter"
        Me.lblCostcenter.Size = New System.Drawing.Size(88, 17)
        Me.lblCostcenter.TabIndex = 30
        Me.lblCostcenter.Text = "Cost Center"
        Me.lblCostcenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTranhead
        '
        Me.cboTranhead.DropDownHeight = 200
        Me.cboTranhead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTranhead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranhead.FormattingEnabled = True
        Me.cboTranhead.IntegralHeight = False
        Me.cboTranhead.Location = New System.Drawing.Point(657, 61)
        Me.cboTranhead.Name = "cboTranhead"
        Me.cboTranhead.Size = New System.Drawing.Size(140, 21)
        Me.cboTranhead.TabIndex = 29
        '
        'lblTranHead
        '
        Me.lblTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranHead.Location = New System.Drawing.Point(563, 63)
        Me.lblTranHead.Name = "lblTranHead"
        Me.lblTranHead.Size = New System.Drawing.Size(88, 17)
        Me.lblTranHead.TabIndex = 28
        Me.lblTranHead.Text = "Tran.  Head"
        Me.lblTranHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownHeight = 200
        Me.cboGradeLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.IntegralHeight = False
        Me.cboGradeLevel.Location = New System.Drawing.Point(377, 88)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(140, 21)
        Me.cboGradeLevel.TabIndex = 27
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(283, 90)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(88, 17)
        Me.lblGradeLevel.TabIndex = 26
        Me.lblGradeLevel.Text = "Grade Level"
        Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrade
        '
        Me.cboGrade.DropDownHeight = 200
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.IntegralHeight = False
        Me.cboGrade.Location = New System.Drawing.Point(377, 61)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(140, 21)
        Me.cboGrade.TabIndex = 25
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(283, 63)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(88, 17)
        Me.lblGrade.TabIndex = 24
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGradeGrp
        '
        Me.lblGradeGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGrp.Location = New System.Drawing.Point(283, 36)
        Me.lblGradeGrp.Name = "lblGradeGrp"
        Me.lblGradeGrp.Size = New System.Drawing.Size(88, 17)
        Me.lblGradeGrp.TabIndex = 23
        Me.lblGradeGrp.Text = "Grade Group"
        Me.lblGradeGrp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGradeGrp
        '
        Me.cboGradeGrp.DropDownHeight = 200
        Me.cboGradeGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGrp.FormattingEnabled = True
        Me.cboGradeGrp.IntegralHeight = False
        Me.cboGradeGrp.Location = New System.Drawing.Point(377, 34)
        Me.cboGradeGrp.Name = "cboGradeGrp"
        Me.cboGradeGrp.Size = New System.Drawing.Size(140, 21)
        Me.cboGradeGrp.TabIndex = 22
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(8, 65)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(83, 17)
        Me.lblJob.TabIndex = 21
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJob
        '
        Me.cboJob.DropDownHeight = 200
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.IntegralHeight = False
        Me.cboJob.Location = New System.Drawing.Point(97, 63)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(140, 21)
        Me.cboJob.TabIndex = 20
        '
        'objLine2
        '
        Me.objLine2.BackColor = System.Drawing.Color.Transparent
        Me.objLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine2.Location = New System.Drawing.Point(550, 36)
        Me.objLine2.Name = "objLine2"
        Me.objLine2.Size = New System.Drawing.Size(7, 73)
        Me.objLine2.TabIndex = 17
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(270, 36)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(7, 73)
        Me.objLine1.TabIndex = 16
        '
        'cboEmpType
        '
        Me.cboEmpType.DropDownHeight = 200
        Me.cboEmpType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpType.FormattingEnabled = True
        Me.cboEmpType.IntegralHeight = False
        Me.cboEmpType.Location = New System.Drawing.Point(97, 34)
        Me.cboEmpType.Name = "cboEmpType"
        Me.cboEmpType.Size = New System.Drawing.Size(140, 21)
        Me.cboEmpType.TabIndex = 8
        '
        'lblEmpType
        '
        Me.lblEmpType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpType.Location = New System.Drawing.Point(8, 36)
        Me.lblEmpType.Name = "lblEmpType"
        Me.lblEmpType.Size = New System.Drawing.Size(83, 17)
        Me.lblEmpType.TabIndex = 7
        Me.lblEmpType.Text = "Emp. Type"
        Me.lblEmpType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.objbtnSearchShift)
        Me.objefFormFooter.Controls.Add(Me.cboShift)
        Me.objefFormFooter.Controls.Add(Me.lblShift)
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnUpdate)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 531)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(833, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'objbtnSearchShift
        '
        Me.objbtnSearchShift.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchShift.BorderSelected = False
        Me.objbtnSearchShift.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchShift.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchShift.Location = New System.Drawing.Point(253, 13)
        Me.objbtnSearchShift.Name = "objbtnSearchShift"
        Me.objbtnSearchShift.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchShift.TabIndex = 329
        Me.objbtnSearchShift.Visible = False
        '
        'cboShift
        '
        Me.cboShift.DropDownHeight = 200
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.IntegralHeight = False
        Me.cboShift.Location = New System.Drawing.Point(107, 13)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(140, 21)
        Me.cboShift.TabIndex = 328
        Me.cboShift.Visible = False
        '
        'lblShift
        '
        Me.lblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.Location = New System.Drawing.Point(18, 15)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(83, 17)
        Me.lblShift.TabIndex = 327
        Me.lblShift.Text = "Shift"
        Me.lblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblShift.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(724, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnUpdate
        '
        Me.btnUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdate.BackColor = System.Drawing.Color.White
        Me.btnUpdate.BackgroundImage = CType(resources.GetObject("btnUpdate.BackgroundImage"), System.Drawing.Image)
        Me.btnUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUpdate.BorderColor = System.Drawing.Color.Empty
        Me.btnUpdate.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnUpdate.FlatAppearance.BorderSize = 0
        Me.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdate.ForeColor = System.Drawing.Color.Black
        Me.btnUpdate.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUpdate.GradientForeColor = System.Drawing.Color.Black
        Me.btnUpdate.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUpdate.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUpdate.Location = New System.Drawing.Point(621, 13)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUpdate.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUpdate.Size = New System.Drawing.Size(97, 30)
        Me.btnUpdate.TabIndex = 0
        Me.btnUpdate.Text = "&Update"
        Me.btnUpdate.UseVisualStyleBackColor = False
        '
        'ofdlgOpen
        '
        Me.ofdlgOpen.FileName = "OpenFileDialog1"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Trans. Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 130
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn3.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Head Type"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 135
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Type Of"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Visible = False
        Me.DataGridViewTextBoxColumn5.Width = 135
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Shift"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 130
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Department"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Job"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Grade Group"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Grade"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Grade Level"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Tran. Head"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Cost Center"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Cost Center"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "objdgcolhEmploymentId"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "objdgcolhShiftId"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "objdgcolhDepartmentId"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "objdgcolhJobId"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "objdgcolhGradeGrpId"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "objdgcolhGradeId"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.Visible = False
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "objdgcolhGradeLevelId"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.Visible = False
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "objdgcolhTranHeadId"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.Visible = False
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "objdgcolhCostCenterId"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.Visible = False
        '
        'frmEditImportedEmployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(833, 586)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditImportedEmployee"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Edit Imported Employee(s)"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.gbMandatoryInfo.PerformLayout()
        Me.pnlEmployeeData.ResumeLayout(False)
        CType(Me.dgvImportInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnUpdate As eZee.Common.eZeeLightButton
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents radApplytoChecked As System.Windows.Forms.RadioButton
    Friend WithEvents radApplytoAll As System.Windows.Forms.RadioButton
    Friend WithEvents cboEmpType As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmpType As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents ofdlgOpen As System.Windows.Forms.OpenFileDialog
    Friend WithEvents dgvImportInfo As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboCostcenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblCostcenter As System.Windows.Forms.Label
    Friend WithEvents cboTranhead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTranHead As System.Windows.Forms.Label
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents lblGradeGrp As System.Windows.Forms.Label
    Friend WithEvents cboGradeGrp As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents txtSalary As eZee.TextBox.NumericTextBox
    Friend WithEvents lblSalary As System.Windows.Forms.Label
    Friend WithEvents objbtnAddSalary As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlEmployeeData As System.Windows.Forms.Panel
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkSet As System.Windows.Forms.LinkLabel
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents EZeeStraightLine3 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchDeptGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDepartmentGroup As System.Windows.Forms.Label
    Friend WithEvents cboDepartmentGrp As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboStation As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchDepartment As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine4 As eZee.Common.eZeeStraightLine
    Friend WithEvents cboSectionGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchSecGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents lblSectionGroup As System.Windows.Forms.Label
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchSection As eZee.Common.eZeeGradientButton
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents cboUnitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchUnitGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchCostCenter As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchGrade As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchGradeGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchGradeLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmplType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchJob As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchUnits As eZee.Common.eZeeGradientButton
    Friend WithEvents lblUnits As System.Windows.Forms.Label
    Friend WithEvents cboUnits As System.Windows.Forms.ComboBox
    Friend WithEvents cboTeams As System.Windows.Forms.ComboBox
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTeam As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployeeStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchShift As eZee.Common.eZeeGradientButton
    Friend WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents cboPayType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayType As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchPayType As eZee.Common.eZeeGradientButton
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEmployeeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFirstName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBranch As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDeptGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDept As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSectionGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSection As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUnitGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUnit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTeam As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhBranchId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhDeptGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSecGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSectionId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhUnitGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhUnitId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTeamId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmpType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShift As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGradeGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGrade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGradelevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTranHead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCostCenter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSalaray As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhPayType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPayType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmploymentId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhShiftId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhDepartmentId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhJobId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGradeGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGradeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGradeLevelId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTranHeadId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCostCenterId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
