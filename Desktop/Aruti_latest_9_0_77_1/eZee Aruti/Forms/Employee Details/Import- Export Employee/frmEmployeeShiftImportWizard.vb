﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeShiftImportWizard

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeShiftImportWizard"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Dim dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Dim mdicEmpAdded As New Dictionary(Of String, Integer)

#End Region

#Region " From's Events "

    Private Sub frmEmployeeShiftImportWizard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeShiftImportWizard_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub WizImportEmployeeShift_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles WizImportEmployeeShift.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case WizImportEmployeeShift.Pages.IndexOf(wizPageData)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportEmployeeShift_AfterSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub WizImportEmployeeShift_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles WizImportEmployeeShift.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case WizImportEmployeeShift.Pages.IndexOf(wizPageFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        'Dim iExcelData As New ExcelData
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    Call SetDataCombo()

                Case WizImportEmployeeShift.Pages.IndexOf(wizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFiledMapping.Controls
                            If TypeOf ctrl Is ComboBox AndAlso ctrl.Enabled = True Then
                                If CType(ctrl, ComboBox).Text = "" Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    e.Cancel = True
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Case WizImportEmployeeShift.Pages.IndexOf(wizPageData)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "WizImportEmployeeShift_BeforeSwitchPages", mstrModuleName)
        Finally

        End Try
    End Sub

#End Region

#Region " Private Functions & Procedures "

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        Dim objEmployee As New clsEmployee_Master
        Try
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("effectivedate", System.Type.GetType("System.DateTime"))
            mdt_ImportData_Others.Columns.Add("employeecode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("employee", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("shift", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData_Others.NewRow
                drNewRow.Item("effectivedate") = dtRow.Item(cboEffectiveDate.Text).ToString.Trim
                drNewRow.Item("employeecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim
                drNewRow.Item("employee") = dtRow.Item(cboEmployeeName.Text).ToString.Trim
                drNewRow.Item("shift") = dtRow.Item(cboShift.Text).ToString.Trim
                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("message") = ""
                drNewRow.Item("status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhEffectiveDate.DataPropertyName = "effectivedate"
                colhEmployee.DataPropertyName = "employee"
                colhShift.DataPropertyName = "shift"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dvGriddata.Sort = "employee"
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            WizImportEmployeeShift.BackEnabled = False
            WizImportEmployeeShift.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFiledMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                For Each ctrl As Control In gbFiledMapping.Controls
                    If TypeOf ctrl Is ComboBox Then
                        CType(ctrl, ComboBox).Items.Add(dtColumns.ColumnName)

                        'Gajanan (24 Nov 2018) -- Start
                        'Enhancement : Import template column headers should read from language set for 
                        'users (custom 1) and columns' date formats for importation should be clearly known 
                        'UAT No:TC017 NMB
                        If dtColumns.ColumnName = Language.getMessage(mstrModuleName, 15, "EmployeeCode") Then
                            dtColumns.Caption = "EmployeeCode"
                        ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 16, "EffectiveDate") Then
                            dtColumns.Caption = "EffectiveDate"
                        ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 17, "EmployeeName") Then
                            dtColumns.Caption = "EmployeeName"
                        ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 18, "Shift") Then
                            dtColumns.Caption = "Shift"
                        End If
                        'Gajanan (24 Nov 2018) -- End
                    End If
                Next
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow

                If .Item(cboEffectiveDate.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Effective Date cannot be blank. Please set Effective Date in order to import."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Empolyee Code cannot be blank. Please set Employee Code in order to import."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboEmployeeName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employee Name cannot be blank. Please set Employee Name in order to import."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboShift.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Shift cannot be blank. Please set Shift in order to import."), enMsgBoxStyle.Information)
                    Return False
                End If

            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            btnFilter.Enabled = False

            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If
            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                Dim objEmpShift As New clsEmployee_Shift_Tran
                Dim objEmployee As New clsEmployee_Master
                Dim objShift As New clsNewshift_master
                Dim xEmployeeID As Integer = -1
                Dim xShiftID As Integer = -1
                Dim xEmpShiftTranID As Integer = -1


                '---------- CHECK IF EMPLOYEE EXISTS ------ START
                xEmployeeID = objEmployee.GetEmployeeUnkid("", CStr(dtRow.Item("employeecode")))
                If xEmployeeID <= 0 Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 9, "Invalid Employee Code.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                '---------- CHECK IF EMPLOYEE EXISTS ------ END


                '---------- CHECK IF SHIFT EXISTS ------ START
                xShiftID = objShift.GetShiftUnkId(dtRow.Item("shift").ToString())
                If xShiftID <= 0 Then
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Invalid Shift Name.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                '---------- CHECK IF SHIFT EXISTS ------ END



                '---------- CHECK  & INSERT IF EMPLOYEE SHIFT EXIST ------ START
                If objEmpShift.isExist(CDate(dtRow.Item("effectivedate")).Date, xEmployeeID, Nothing).ToString.Trim.Length <= 0 Then

                    Dim iCurrShiftId As Integer = objEmpShift.GetEmployee_Current_ShiftId(CDate(dtRow.Item("effectivedate")).Date, xEmployeeID)

                    If iCurrShiftId = xShiftID Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Sorry, This shift cannot be assigned to this employee.Reason : Same shift is already assigned to this employee for previous or future date.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    objEmpShift._effectivedate = CDate(dtRow.Item("effectivedate")).Date
                    objEmpShift._EmployeeUnkid = xEmployeeID
                    objEmpShift._ShiftUnkid = xShiftID
                    objEmpShift._Userunkid = User._Object._Userunkid
                    objEmpShift._Isvoid = False

                    With objEmpShift
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With

                    If objEmpShift.ImportInsertEmployeeShift() = False Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = objEmpShift._Message
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 8, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    Else
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                    End If
                Else
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 13, "This Employee's Shift already exists for this date. Please define new Employee's Shift.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 12, "Warning")
                    dtRow.Item("objStatus") = 0
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                End If
                '---------- CHECK  & INSERT IF EMPLOYEE SHIFT EXISTS ------ END

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)
            If cmb.Text <> "" Then
                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString
                For Each cr As Control In gbFiledMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        If cr.Name <> cmb.Name Then
                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            If dvGriddata IsNot Nothing Then
                dvGriddata.RowFilter = "objStatus = 2"

                Dim savDialog As New SaveFileDialog
                Dim dtTable As DataTable = dvGriddata.ToTable
                If dtTable.Rows.Count > 0 Then
                    savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                    If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                        dtTable.Columns.Remove("image")
                        dtTable.Columns.Remove("objstatus")
                        If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Employee Shift Wizard") = True Then
                            Process.Start(savDialog.FileName)
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            If dvGriddata IsNot Nothing Then dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub lnkAllocationFormat_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocationFormat.LinkClicked
        Try
            Dim dtExTable As New DataTable("DataList")
            With dtExTable

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB

                '.Columns.Add("EmployeeCode", System.Type.GetType("System.String"))
                '.Columns.Add("EffectiveDate", System.Type.GetType("System.String"))
                '.Columns.Add("EmployeeName", System.Type.GetType("System.String"))
                '.Columns.Add("Shift", System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 15, "EmployeeCode"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 16, "EffectiveDate"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 17, "EmployeeName"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 18, "Shift"), System.Type.GetType("System.String"))

                'Gajanan (24 Nov 2018) -- End

            End With
            Dim dsList As New DataSet
            dsList.Tables.Add(dtExTable.Copy)
            Dim dlgSaveFile As New SaveFileDialog
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                OpenXML_Export(dlgSaveFile.FileName, dsList)
            End If
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 67, "Template Exported Successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocationFormat_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB
                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                'Gajanan (24 Nov 2018) -- End

                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFiledMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"
            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If
            objFileOpen = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
			
            Call SetLanguage()

            Me.gbFiledMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFiledMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.WizImportEmployeeShift.CancelText = Language._Object.getCaption(Me.WizImportEmployeeShift.Name & "_CancelText", Me.WizImportEmployeeShift.CancelText)
            Me.WizImportEmployeeShift.NextText = Language._Object.getCaption(Me.WizImportEmployeeShift.Name & "_NextText", Me.WizImportEmployeeShift.NextText)
            Me.WizImportEmployeeShift.BackText = Language._Object.getCaption(Me.WizImportEmployeeShift.Name & "_BackText", Me.WizImportEmployeeShift.BackText)
            Me.WizImportEmployeeShift.FinishText = Language._Object.getCaption(Me.WizImportEmployeeShift.Name & "_FinishText", Me.WizImportEmployeeShift.FinishText)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.gbFiledMapping.Text = Language._Object.getCaption(Me.gbFiledMapping.Name, Me.gbFiledMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblFirstname.Text = Language._Object.getCaption(Me.lblFirstname.Name, Me.lblFirstname.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
            Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.Name, Me.LblShift.Text)
            Me.colhEffectiveDate.HeaderText = Language._Object.getCaption(Me.colhEffectiveDate.Name, Me.colhEffectiveDate.HeaderText)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhShift.HeaderText = Language._Object.getCaption(Me.colhShift.Name, Me.colhShift.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.lblNote.Text = Language._Object.getCaption(Me.lblNote.Name, Me.lblNote.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
			Me.lnkAllocationFormat.Text = Language._Object.getCaption(Me.lnkAllocationFormat.Name, Me.lnkAllocationFormat.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 3, "Effective Date cannot be blank. Please set Effective Date in order to import.")
            Language.setMessage(mstrModuleName, 4, "Empolyee Code cannot be blank. Please set Employee Code in order to import.")
            Language.setMessage(mstrModuleName, 5, "Employee Name cannot be blank. Please set Employee Name in order to import.")
            Language.setMessage(mstrModuleName, 6, "Shift cannot be blank. Please set Shift in order to import.")
            Language.setMessage(mstrModuleName, 7, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 8, "Fail")
            Language.setMessage(mstrModuleName, 9, "Invalid Employee Code.")
            Language.setMessage(mstrModuleName, 10, "Invalid Shift Name.")
            Language.setMessage(mstrModuleName, 11, "Success")
            Language.setMessage(mstrModuleName, 12, "Warning")
            Language.setMessage(mstrModuleName, 13, "This Employee's Shift already exists for this date. Please define new Employee's Shift.")
            Language.setMessage(mstrModuleName, 14, "Sorry, This shift cannot be assigned to this employee.Reason : Same shift is already assigned to this employee for previous or future date.")
			Language.setMessage(mstrModuleName, 15, "EmployeeCode")
			Language.setMessage(mstrModuleName, 16, "EffectiveDate")
			Language.setMessage(mstrModuleName, 17, "EmployeeName")
			Language.setMessage(mstrModuleName, 18, "Shift")
			Language.setMessage(mstrModuleName, 41, " Fields From File.")
			Language.setMessage(mstrModuleName, 42, "Please select different Field.")
			Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
			Language.setMessage(mstrModuleName, 50, "is already Mapped with")
			Language.setMessage(mstrModuleName, 67, "Template Exported Successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class