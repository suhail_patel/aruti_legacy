﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmBirthInformationImportWizard

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmBirthInformationImportWizard"
    Private mds_ImportData As DataSet
    Private mdt_ImportData As New DataTable
    Dim dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    'Gajanan [27-May-2019] -- Start
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [27-May-2019] -- End
#End Region

#Region " Display Dialog "

    Public Sub displayDialog()
        Try
            Me.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAdressImportWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAdressImportWizard_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizImportImgs_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizImportImgs.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizImportImgs.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportImgs_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizImportImgs_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizImportImgs.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizImportImgs.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)

                        'Gajanan (24 Nov 2018) -- Start
                        'Enhancement : Import template column headers should read from language set for 
                        'users (custom 1) and columns' date formats for importation should be clearly known 
                        'UAT No:TC017 NMB
                        'Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)s
                        'Gajanan (24 Nov 2018) -- End
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    Call SetDataCombo()
                    'cboEmployeeCode.SelectedIndex = 0
                    'cboAddress1.SelectedIndex = 1
                    'cboAddress2.SelectedIndex = 2
                    'cboPostCountry.SelectedIndex = 3
                    'cboState.SelectedIndex = 4
                    'cboPostTown.SelectedIndex = 5
                    'cboPostCode.SelectedIndex = 6
                    'cboProvRegion.SelectedIndex = 7
                    'cboRoadStreet.SelectedIndex = 8
                    'cboEState.SelectedIndex = 9
                    'cboProvRegion1.SelectedIndex = 10
                    'cboRoadStreet1.SelectedIndex = 11
                    'cboChiefdom.SelectedIndex = 12
                    'cboVillage.SelectedIndex = 13
                    'cboTown1.SelectedIndex = 14
                    'cboTelNo.SelectedIndex = 16
                    'cboPlotNo.SelectedIndex = 17
                Case eZeeWizImportImgs.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        'For Each ctrl As Control In gbFieldMapping.Controls
                        '    If TypeOf ctrl Is ComboBox Then
                        '        If ctrl.Visible = True Then
                        '            If CType(ctrl, ComboBox).Text = "" Then
                        '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please the select proper field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        '                e.Cancel = True
                        '                Exit Sub
                        '            End If
                        '        End If
                        '    End If
                        'Next
                        If cboEmployeeCode.Text = "" Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            e.Cancel = True
                            Exit Sub
                        End If
                    End If
                Case eZeeWizImportImgs.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizImportImgs_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                cboCountry.Items.Add(dtColumns.ColumnName)
                cboState.Items.Add(dtColumns.ColumnName)
                cboCity.Items.Add(dtColumns.ColumnName)
                cboWard.Items.Add(dtColumns.ColumnName)
                cboCertNo.Items.Add(dtColumns.ColumnName)
                cboVillage.Items.Add(dtColumns.ColumnName)
                cboTown1.Items.Add(dtColumns.ColumnName)
                cboChiefdom.Items.Add(dtColumns.ColumnName)
                cboVillage1.Items.Add(dtColumns.ColumnName)

                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB
                If dtColumns.ColumnName = Language.getMessage(mstrModuleName, 11, "EmployeeCode") Then
                    dtColumns.Caption = "EmployeeCode"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 12, "Country") Then
                    dtColumns.Caption = "Country"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 13, "State") Then
                    dtColumns.Caption = "State"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 14, "City") Then
                    dtColumns.Caption = "City"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 15, "Ward") Then
                    dtColumns.Caption = "Ward"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 16, "CertNo") Then
                    dtColumns.Caption = "CertNo"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 17, "Village") Then
                    dtColumns.Caption = "Village"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 18, "Town1") Then
                    dtColumns.Caption = "Town1"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 19, "Chiefdom") Then
                    dtColumns.Caption = "Chiefdom"
                ElseIf dtColumns.ColumnName = Language.getMessage(mstrModuleName, 20, "Village1") Then
                    dtColumns.Caption = "Village1"
                End If
                'Gajanan (24 Nov 2018) -- End

            Next
            mds_ImportData.AcceptChanges()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        ezWait.Active = True
        Try

            mdt_ImportData.Columns.Add("ecode", GetType(String)).DefaultValue = ""
            'Gajanan [27-May-2019] -- Start              
            mdt_ImportData.Columns.Add("Firstname", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("Surname", GetType(String)).DefaultValue = ""
            'Gajanan [27-May-2019] -- End
            mdt_ImportData.Columns.Add("country", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("state", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("city", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("ward", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("certno", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("village", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("town1", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("chiefdom", GetType(String)).DefaultValue = ""
            mdt_ImportData.Columns.Add("village1", GetType(String)).DefaultValue = ""


            'Gajanan [27-May-2019] -- Start              

            mdt_ImportData.Columns("country").ExtendedProperties.Add("col", "country")
            mdt_ImportData.Columns("state").ExtendedProperties.Add("col", "state")
            mdt_ImportData.Columns("city").ExtendedProperties.Add("col", "city")
            mdt_ImportData.Columns("ward").ExtendedProperties.Add("col", "ward")
            mdt_ImportData.Columns("certno").ExtendedProperties.Add("col", "certno")
            mdt_ImportData.Columns("village").ExtendedProperties.Add("col", "village")
            mdt_ImportData.Columns("town1").ExtendedProperties.Add("col", "town1")
            mdt_ImportData.Columns("chiefdom").ExtendedProperties.Add("col", "chiefdom")
            mdt_ImportData.Columns("village1").ExtendedProperties.Add("col", "village1")
            'Gajanan [27-May-2019] -- End


            mdt_ImportData.Columns.Add("image", GetType(Object))
            mdt_ImportData.Columns.Add("Message", GetType(String))
            mdt_ImportData.Columns.Add("Status", GetType(String))
            mdt_ImportData.Columns.Add("objStatus", GetType(String))

            'Gajanan [27-May-2019] -- Start              
            mdt_ImportData.Columns.Add("EmployeeId", System.Type.GetType("System.String"))
            'Gajanan [27-May-2019] -- End

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboEmployeeCode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData.NewRow
                drNewRow.Item("ecode") = dtRow.Item(cboEmployeeCode.Text).ToString.Trim

                If cboCountry.Text <> "" Then drNewRow.Item("country") = dtRow.Item(cboCountry.Text).ToString.Trim
                If cboState.Text <> "" Then drNewRow.Item("state") = dtRow.Item(cboState.Text).ToString.Trim
                If cboCity.Text <> "" Then drNewRow.Item("city") = dtRow.Item(cboCity.Text).ToString.Trim
                If cboWard.Text <> "" Then drNewRow.Item("ward") = dtRow.Item(cboWard.Text).ToString.Trim
                If cboCertNo.Text <> "" Then drNewRow.Item("certno") = dtRow.Item(cboCertNo.Text).ToString.Trim
                If cboVillage.Text <> "" Then drNewRow.Item("village") = dtRow.Item(cboVillage.Text).ToString.Trim
                If cboTown1.Text <> "" Then drNewRow.Item("town1") = dtRow.Item(cboTown1.Text).ToString.Trim
                If cboChiefdom.Text <> "" Then drNewRow.Item("chiefdom") = dtRow.Item(cboChiefdom.Text).ToString.Trim
                If cboVillage1.Text <> "" Then drNewRow.Item("village1") = dtRow.Item(cboVillage1.Text).ToString.Trim


                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next


            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ecode"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data_Employee()

            ezWait.Active = False
            eZeeWizImportImgs.BackEnabled = False
            eZeeWizImportImgs.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboEmployeeCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Images(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data_Employee()
        Try
            btnFilter.Enabled = False
            If mdt_ImportData.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim intEmpId As Integer = 0 : Dim intCountry As Integer = 0
            Dim intState As Integer = 0 : Dim intCity As Integer = 0
            Dim intTown1 As Integer = 0 : Dim intChiefdom As Integer = 0
            Dim intVillage1 As Integer = 0

            'Gajanan [27-May-2019] -- Start
            Dim EmpList As New List(Of String)
            'Gajanan [27-May-2019] -- End

            Dim objEMaster As New clsEmployee_Master

            For Each dtRow As DataRow In mdt_ImportData.Rows
                'Gajanan [27-May-2019] -- Start              
                Dim isEmployeeApprove As Boolean = False
                'Gajanan [27-May-2019] -- End

                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                intEmpId = 0 : intCountry = 0 : intState = 0 : intCity = 0
                intTown1 = 0 : intChiefdom = 0 : intVillage1 = 0
                '------------------------------ CHECKING Check Row Data.
                If dtRow.Item("country").ToString.Trim.Length <= 0 AndAlso dtRow.Item("state").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("city").ToString.Trim.Length <= 0 AndAlso dtRow.Item("ward").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("certno").ToString.Trim.Length <= 0 AndAlso dtRow.Item("village").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("town1").ToString.Trim.Length <= 0 AndAlso dtRow.Item("chiefdom").ToString.Trim.Length <= 0 AndAlso _
                   dtRow.Item("village1").ToString.Trim.Length <= 0 Then

                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = Language.getMessage(mstrModuleName, 21, "Birth data Not Found.")
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If


                '------------------------------ CHECKING IF EMPLOYEE PRESENT.
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    intEmpId = objEMaster.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If intEmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 5, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If

                    'Gajanan [27-May-2019] -- Start   
                    objEMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpId
                    dtRow.Item("Surname") = objEMaster._Surname
                    dtRow.Item("Firstname") = objEMaster._Firstname
                    'Gajanan [27-May-2019] -- End
                End If


                'Gajanan [27-May-2019] -- Start      
                isEmployeeApprove = objEMaster._Isapproved
                If isEmployeeApprove Then
                    dtRow.Item("EmployeeId") = intEmpId
                End If

                'Gajanan [27-May-2019] -- End

                '------------------------------ CHECKING IF Post Country.
                If dtRow.Item("country").ToString.Trim.Length > 0 Then
                    intCountry = (New clsMasterData).GetCountryUnkId(dtRow.Item("country").ToString)
                    If intCountry <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 6, "Country Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If
                '------------------------------ CHECKING IF State.
                If dtRow.Item("state").ToString.Trim.Length > 0 Then
                    Dim dsState As DataSet = (New clsstate_master).GetList("List", True, , intCountry)
                    Dim dRow As DataRow = dtRow
                    If dsState.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dRow.Item("state").ToString).Count > 0 Then
                        intState = dsState.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dRow.Item("state").ToString).Select(Function(x) x.Field(Of Integer)("stateunkid")).First
                    End If

                    If intState <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 8, "State Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If
                '------------------------------ CHECKING IF Post Town.
                If dtRow.Item("city").ToString.Trim.Length > 0 Then
                    Dim dsCity As DataSet = (New clscity_master).GetList("List", True, , intState)
                    Dim dRow As DataRow = dtRow
                    If dsCity.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dRow.Item("city").ToString).Count > 0 Then
                        intCity = dsCity.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("name") = dRow.Item("city").ToString) _
                                                                .Select(Function(x) x.Field(Of Integer)("cityunkid")).First()
                    End If
                    If intCity <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 9, "City Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If
                '------------------------------ CHECKING IF Town1.
                If dtRow.Item("town1").ToString.Trim.Length > 0 Then
                    intTown1 = (New clsCommon_Master).GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.TOWN_1, dtRow.Item("town1").ToString.Trim)
                    If intTown1 <= 0 Then
                        Dim objCMaster As New clsCommon_Master
                        objCMaster._Code = dtRow.Item("town1").ToString.Trim
                        objCMaster._Name = dtRow.Item("town1").ToString.Trim
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.TOWN_1
                        objCMaster._Isactive = True

                        With objCMaster
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        If objCMaster.Insert() = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Else
                            intTown1 = objCMaster._Masterunkid
                        End If
                    End If
                End If
                '------------------------------ CHECKING IF Chiefdom.
                If dtRow.Item("chiefdom").ToString.Trim.Length > 0 Then
                    intChiefdom = (New clsCommon_Master).GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.CHIEFDOM, dtRow.Item("chiefdom").ToString.Trim)
                    If intChiefdom <= 0 Then
                        Dim objCMaster As New clsCommon_Master
                        objCMaster._Code = dtRow.Item("chiefdom").ToString.Trim
                        objCMaster._Name = dtRow.Item("chiefdom").ToString.Trim
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.CHIEFDOM
                        objCMaster._Isactive = True

                        With objCMaster
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        If objCMaster.Insert() = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Else
                            intChiefdom = objCMaster._Masterunkid
                        End If
                    End If
                End If
                '------------------------------ CHECKING IF Village.
                If dtRow.Item("village1").ToString.Trim.Length > 0 Then
                    intVillage1 = (New clsCommon_Master).GetCommonMasterUnkId(clsCommon_Master.enCommonMaster.VILLAGE, dtRow.Item("village1").ToString.Trim)
                    If intVillage1 <= 0 Then
                        Dim objCMaster As New clsCommon_Master
                        objCMaster._Code = dtRow.Item("village1").ToString.Trim
                        objCMaster._Name = dtRow.Item("village1").ToString.Trim
                        objCMaster._Mastertype = clsCommon_Master.enCommonMaster.VILLAGE
                        objCMaster._Isactive = True

                        With objCMaster
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With

                        If objCMaster.Insert() = False Then
                            dtRow.Item("image") = imgError
                            dtRow.Item("message") = objCMaster._Message
                            dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                            dtRow.Item("objStatus") = 2
                            objError.Text = CStr(Val(objError.Text) + 1)
                            Continue For
                        Else
                            intVillage1 = objCMaster._Masterunkid
                        End If
                    End If
                End If



                objEMaster._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = intEmpId
                If cboCountry.Text <> "" Then objEMaster._Birthcountryunkid = intCountry
                If cboState.Text <> "" Then objEMaster._Birthstateunkid = intState
                If cboCity.Text <> "" Then objEMaster._Birthcityunkid = intCity
                If cboWard.Text <> "" Then objEMaster._Birth_Ward = dtRow.Item("ward").ToString.Trim
                If cboCertNo.Text <> "" Then objEMaster._Birthcertificateno = dtRow.Item("certno").ToString.Trim
                If cboVillage.Text <> "" Then objEMaster._Birth_Village = dtRow.Item("village").ToString.Trim
                If cboTown1.Text <> "" Then objEMaster._Birthtownunkid = intTown1
                If cboChiefdom.Text <> "" Then objEMaster._Birthchiefdomunkid = intChiefdom
                If cboVillage1.Text <> "" Then objEMaster._Birthvillageunkid = intVillage1

                With objEMaster
                    ._FormName = mstrModuleName
                    ._LoginEmployeeUnkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                'If objEMaster.Update(FinancialYear._Object._DatabaseName, _
                '                     FinancialYear._Object._YearUnkid, _
                '                     Company._Object._Companyunkid, _
                '                     ConfigParameter._Object._CurrentDateAndTime.Date, _
                '                     CStr(ConfigParameter._Object._IsArutiDemo), _
                '                     Company._Object._Total_Active_Employee_ForAllCompany, _
                '                     ConfigParameter._Object._NoOfEmployees, _
                '                     User._Object._Userunkid, _
                '                     ConfigParameter._Object._DonotAttendanceinSeconds, _
                '                     ConfigParameter._Object._UserAccessModeSetting, _
                '                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, _
                '                     ConfigParameter._Object._IsIncludeInactiveEmp, _
                '                     User._Object.Privilege._AllowToApproveEarningDeduction, _
                '                     ConfigParameter._Object._CurrentDateAndTime, , , , , , , , , , , , , getHostName(), getIP, User._Object._Username, enLogin_Mode.DESKTOP) = True Then

                If objEMaster.Update(FinancialYear._Object._DatabaseName, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     ConfigParameter._Object._CurrentDateAndTime.Date, _
                                     CStr(ConfigParameter._Object._IsArutiDemo), _
                                     Company._Object._Total_Active_Employee_ForAllCompany, _
                                     ConfigParameter._Object._NoOfEmployees, _
                                     User._Object._Userunkid, _
                                     ConfigParameter._Object._DonotAttendanceinSeconds, _
                                     ConfigParameter._Object._UserAccessModeSetting, _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, _
                                     ConfigParameter._Object._IsIncludeInactiveEmp, _
                                     User._Object.Privilege._AllowToApproveEarningDeduction, _
                                     ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, _
                                     ConfigParameter._Object._UserMustchangePwdOnNextLogon, , , , , , , , , , , , , getHostName(), getIP, _
                                     User._Object._Username, enLogin_Mode.DESKTOP, , , mstrForm_Name, True, , True) = True Then


                    'Pinkal (18-Aug-2018) -- End

                    'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{getHostName(), getIP, User._Object._Username, enLogin_Mode.DESKTOP})} -- END
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Success")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)

                    'Gajanan [27-May-2019] -- Start              
                    If EmpList.Contains(intEmpId) = False Then
                        EmpList.Add(intEmpId)
                    End If
                    'Gajanan [27-May-2019] -- End
                Else
                    dtRow.Item("image") = imgError
                    dtRow.Item("message") = objEMaster._Message
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 7, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                End If

            Next

            'Gajanan [27-May-2019] -- Start              
            objApprovalData = New clsEmployeeDataApproval
            If mdt_ImportData.Select("status <> '" & Language.getMessage(mstrModuleName, 7, "Fail") & "'").Count > 0 Then
                If IsNothing(EmpList) = False AndAlso EmpList.Count > 0 Then
                    Dim EmpListCsv As String = String.Join(",", EmpList.ToArray())
                    mdt_ImportData.DefaultView.RowFilter = "status <> '" & Language.getMessage(mstrModuleName, 7, "Fail") & "'"
                    objApprovalData.ImportDataSendNotification(1, FinancialYear._Object._DatabaseName, _
                                                               ConfigParameter._Object._UserAccessModeSetting, _
                                                               Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                               CInt(enUserPriviledge.AllowToApproveRejectPersonalInfo), _
                                                               enScreenName.frmBirthinfo, ConfigParameter._Object._EmployeeAsOnDate, _
                                                               User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                               User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, _
                                                               User._Object._EmployeeUnkid, getIP(), getHostName(), False, User._Object._Userunkid, _
                                                               ConfigParameter._Object._CurrentDateAndTime, mdt_ImportData.DefaultView.ToTable(), , EmpListCsv, False, , Nothing)
                End If
            End If
            'Gajanan [27-May-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data_Employee", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)
            If cmb.Text <> "" Then
                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString
                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then
                        If cr.Name <> cmb.Name Then
                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")
                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Image Processing Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Private Sub lnkAllocationFormat_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocationFormat.LinkClicked
        Try
            Dim dtExTable As New DataTable("DataList")
            With dtExTable
                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB
                '.Columns.Add("EmployeeCode", System.Type.GetType("System.String"))
                '.Columns.Add("Country", System.Type.GetType("System.String"))
                '.Columns.Add("State", System.Type.GetType("System.String"))
                '.Columns.Add("City", System.Type.GetType("System.String"))
                '.Columns.Add("Ward", System.Type.GetType("System.String"))
                '.Columns.Add("CertNo", System.Type.GetType("System.String"))
                '.Columns.Add("Village", System.Type.GetType("System.String"))
                '.Columns.Add("Town1", System.Type.GetType("System.String"))
                '.Columns.Add("Chiefdom", System.Type.GetType("System.String"))
                '.Columns.Add("Village1", System.Type.GetType("System.String"))

                .Columns.Add(Language.getMessage(mstrModuleName, 11, "EmployeeCode"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 12, "Country"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 13, "State"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 14, "City"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 15, "Ward"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 16, "CertNo"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 17, "Village"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 18, "Town1"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 19, "Chiefdom"), System.Type.GetType("System.String"))
                .Columns.Add(Language.getMessage(mstrModuleName, 22, "Village1"), System.Type.GetType("System.String"))
                'Gajanan (24 Nov 2018) -- End

            End With            
            Dim dsList As New DataSet
            dsList.Tables.Add(dtExTable.Copy)
            Dim dlgSaveFile As New SaveFileDialog
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                OpenXML_Export(dlgSaveFile.FileName, dsList)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 67, "Template Exported Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocationFormat_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAutoMap_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAutoMap.LinkClicked
        Try
            Dim lstCombos As List(Of Control) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox) Select (p)).ToList
            For Each ctrl In lstCombos
                Dim strName As String = CType(ctrl, ComboBox).Name.Substring(3)
                'Gajanan (24 Nov 2018) -- Start
                'Enhancement : Import template column headers should read from language set for 
                'users (custom 1) and columns' date formats for importation should be clearly known 
                'UAT No:TC017 NMB

                'Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.ColumnName.ToUpper.Contains(strName.ToUpper) = True) Select (p)).FirstOrDefault
                Dim col As DataColumn = (From p In mds_ImportData.Tables(0).Columns.Cast(Of DataColumn)() Where (p.Caption.ToUpper = strName.ToUpper) Select (p)).FirstOrDefault
                'Gajanan (24 Nov 2018) -- End

                If col IsNot Nothing Then
                    Dim strCtrlName As String = ctrl.Name
                    Dim SelCombos As List(Of ComboBox) = (From p In gbFieldMapping.Controls.Cast(Of Control)() Where (TypeOf p Is ComboBox AndAlso CType(p, ComboBox).DataSource Is Nothing AndAlso CType(p, ComboBox).Name <> strCtrlName AndAlso CType(p, ComboBox).SelectedIndex = col.Ordinal) Select (CType(p, ComboBox))).ToList
                    If SelCombos.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Sorry! This Column") & " " & ctrl.Name.Substring(3) & " " & Language.getMessage(mstrModuleName, 50, "is already Mapped with ") & SelCombos(0).Name.Substring(3) & Language.getMessage(mstrModuleName, 41, " Fields From File.") & vbCrLf & Language.getMessage(mstrModuleName, 42, "Please select different Field."), enMsgBoxStyle.Information)
                        CType(ctrl, ComboBox).SelectedIndex = -1
                    Else
                        CType(ctrl, ComboBox).SelectedIndex = col.Ordinal
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAutoMap_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try

            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.eZeeWizImportImgs.CancelText = Language._Object.getCaption(Me.eZeeWizImportImgs.Name & "_CancelText", Me.eZeeWizImportImgs.CancelText)
            Me.eZeeWizImportImgs.NextText = Language._Object.getCaption(Me.eZeeWizImportImgs.Name & "_NextText", Me.eZeeWizImportImgs.NextText)
            Me.eZeeWizImportImgs.BackText = Language._Object.getCaption(Me.eZeeWizImportImgs.Name & "_BackText", Me.eZeeWizImportImgs.BackText)
            Me.eZeeWizImportImgs.FinishText = Language._Object.getCaption(Me.eZeeWizImportImgs.Name & "_FinishText", Me.eZeeWizImportImgs.FinishText)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblVillage.Text = Language._Object.getCaption(Me.lblVillage.Name, Me.lblVillage.Text)
            Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
            Me.lblTown1.Text = Language._Object.getCaption(Me.lblTown1.Name, Me.lblTown1.Text)
            Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
            Me.lblChiefdom.Text = Language._Object.getCaption(Me.lblChiefdom.Name, Me.lblChiefdom.Text)
            Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
            Me.lblWard.Text = Language._Object.getCaption(Me.lblWard.Name, Me.lblWard.Text)
            Me.lblVillage1.Text = Language._Object.getCaption(Me.lblVillage1.Name, Me.lblVillage1.Text)
            Me.lblCertNo.Text = Language._Object.getCaption(Me.lblCertNo.Name, Me.lblCertNo.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.lblNote.Text = Language._Object.getCaption(Me.lblNote.Name, Me.lblNote.Text)
			Me.lnkAllocationFormat.Text = Language._Object.getCaption(Me.lnkAllocationFormat.Name, Me.lnkAllocationFormat.Text)
			Me.lnkAutoMap.Text = Language._Object.getCaption(Me.lnkAutoMap.Name, Me.lnkAutoMap.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 3, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 4, "Employee Code cannot be blank. Please set Employee Code in order to import Employee Images(s).")
            Language.setMessage(mstrModuleName, 5, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 6, "Country Not Found.")
            Language.setMessage(mstrModuleName, 7, "Fail")
            Language.setMessage(mstrModuleName, 8, "State Not Found.")
            Language.setMessage(mstrModuleName, 9, "City Not Found.")
            Language.setMessage(mstrModuleName, 10, "Success")
			Language.setMessage(mstrModuleName, 11, "EmployeeCode")
			Language.setMessage(mstrModuleName, 12, "Country")
			Language.setMessage(mstrModuleName, 13, "State")
			Language.setMessage(mstrModuleName, 14, "City")
			Language.setMessage(mstrModuleName, 15, "Ward")
			Language.setMessage(mstrModuleName, 16, "CertNo")
			Language.setMessage(mstrModuleName, 17, "Village")
			Language.setMessage(mstrModuleName, 18, "Town1")
			Language.setMessage(mstrModuleName, 19, "Chiefdom")
			Language.setMessage(mstrModuleName, 20, "Village1")
			Language.setMessage(mstrModuleName, 21, "Birth data Not Found.")
			Language.setMessage(mstrModuleName, 22, "Village1")
			Language.setMessage(mstrModuleName, 41, " Fields From File.")
			Language.setMessage(mstrModuleName, 42, "Please select different Field.")
			Language.setMessage(mstrModuleName, 49, "Sorry! This Column")
			Language.setMessage(mstrModuleName, 50, "is already Mapped with")
			Language.setMessage(mstrModuleName, 67, "Template Exported Successfully.")
            Language.setMessage(mstrModuleName, 20, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 21, "Birth data Not Found.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class