﻿Option Strict On

'Last Message Index = 21

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmImportEmployee

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImportExportHeads"
    Private mblnCancel As Boolean = True
    Private dsList As New DataSet
    Private mdtTable As New DataTable
    Private objIExcel As ExcelData

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Try
            Dim objEmpType As New clsCommon_Master
            dsCombos = objEmpType.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EmpType")
            cboEmpType.ValueMember = "masterunkid"
            cboEmpType.DisplayMember = "name"
            cboEmpType.DataSource = dsCombos.Tables("EmpType")

            dsCombos = Nothing

            'Pinkal (03-Jul-2013) -- Start
            'Enhancement : TRA Changes

            'Dim objShift As New clsshift_master
            Dim objShift As New clsNewshift_master
            'Pinkal (03-Jul-2013) -- End

            dsCombos = objShift.getListForCombo("Shift", True)
            cboShift.ValueMember = "shiftunkid"
            cboShift.DisplayMember = "name"
            cboShift.DataSource = dsCombos.Tables("Shift")

            dsCombos = Nothing
            Dim objDept As New clsDepartment
            dsCombos = objDept.getComboList("Dept", True)
            cboDepartment.ValueMember = "departmentunkid"
            cboDepartment.DisplayMember = "name"
            cboDepartment.DataSource = dsCombos.Tables("Dept")

            dsCombos = Nothing
            Dim objjob As New clsJobs
            dsCombos = objjob.getComboList("Job", True)
            cboJob.ValueMember = "jobunkid"
            cboJob.DisplayMember = "name"
            cboJob.DataSource = dsCombos.Tables("Job")

            dsCombos = Nothing
            Dim objGradegrp As New clsGradeGroup
            dsCombos = objGradegrp.getComboList("GradeGrp", True)
            cboGradeGrp.ValueMember = "gradegroupunkid"
            cboGradeGrp.DisplayMember = "name"
            cboGradeGrp.DataSource = dsCombos.Tables("GradeGrp")

          
            dsCombos = Nothing
            Dim objTranHead As New clsTransactionHead
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("TranHead", True, 0, 0, enTypeOf.Salary)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, 0, 0, enTypeOf.Salary)
            'Sohail (21 Aug 2015) -- End
            cboTranhead.ValueMember = "tranheadunkid"
            cboTranhead.DisplayMember = "name"
            cboTranhead.DataSource = dsCombos.Tables("TranHead")


            dsCombos = Nothing
            Dim objCostCenter As New clscostcenter_master
            dsCombos = objCostCenter.getComboList("CostCenter", True)
            cboCostcenter.ValueMember = "costcenterunkid"
            cboCostcenter.DisplayMember = "costcentername"
            cboCostcenter.DataSource = dsCombos.Tables("CostCenter")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGirdView()
        Try
            dgvImportInfo.AutoGenerateColumns = False


            objdgcolhCheck.DataPropertyName = "IsChecked"
            dgcolhEmployee.DataPropertyName = "employeecode"
            dgcolhFirstName.DataPropertyName = "firstname"
            dgcolhOthername.DataPropertyName = "othername"
            dgcolhSurname.DataPropertyName = "surname"
            dgcolhEmpType.DataPropertyName = "employmenttypename"
            dgcolhShift.DataPropertyName = "shiftname"
            dgcolhDept.DataPropertyName = "deptname"
            dgcolhJob.DataPropertyName = "jobname"
            dgcolhGradeGrp.DataPropertyName = "gradegrpname"
            dgcolhGrade.DataPropertyName = "gradename"
            dgcolhGradelevel.DataPropertyName = "gradelevelname"
            dgcolhTranHead.DataPropertyName = "tranheadname"
            dgcolhCostCenter.DataPropertyName = "costcentername"
            dgcolhSalaray.DataPropertyName = "scale"
            dgcolhappointeddate.DataPropertyName = "appointeddate"

            dgvImportInfo.DataSource = mdtTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGirdView", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmImportEmployee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objIExcel = New ExcelData
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportEmployee_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Buttons "

    Private Sub objbtnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSet.Click

        If radApplytoAll.Checked = False And radApplySelected.Checked = False And radApplytoChecked.Checked = False Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please atleast one action to set the value."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        If CInt(cboEmpType.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employeement Type is compulsory Information. Please Select Employeement Type."), enMsgBoxStyle.Information)
            cboEmpType.Select()
            Exit Sub

        ElseIf CInt(cboShift.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Shift is compulsory Information. Please Select Shift."), enMsgBoxStyle.Information)
            cboShift.Select()
            Exit Sub

        ElseIf CInt(cboDepartment.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Deparment is compulsory Information. Please Select Deparment."), enMsgBoxStyle.Information)
            cboDepartment.Select()
            Exit Sub

        ElseIf CInt(cboJob.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Job is compulsory Information. Please Select Job."), enMsgBoxStyle.Information)
            cboJob.Select()
            Exit Sub

        ElseIf CInt(cboGradeGrp.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Grade Group is compulsory Information. Please Select Grade Group."), enMsgBoxStyle.Information)
            cboGradeGrp.Select()
            Exit Sub

        ElseIf CInt(cboGrade.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Grade is compulsory Information. Please Select Grade."), enMsgBoxStyle.Information)
            cboGrade.Select()
            Exit Sub

        ElseIf CInt(cboGradeLevel.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Grade Level is compulsory Information. Please Select Grade Level."), enMsgBoxStyle.Information)
            cboGradeLevel.Select()
            Exit Sub

        ElseIf txtSalary.Text = "" Or CDec(txtSalary.Text.Trim) = 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Salary cannot be blank. Salary is required information."), enMsgBoxStyle.Information)
            cboGradeLevel.Select()
            Exit Sub

        ElseIf CInt(cboTranhead.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Transaction Head is compulsory Information. Please Select Transaction Head."), enMsgBoxStyle.Information)
            cboTranhead.Select()
            Exit Sub

        ElseIf CInt(cboCostcenter.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Cost Center is compulsory Information. Please Select Cost Center."), enMsgBoxStyle.Information)
            cboCostcenter.Select()
            Exit Sub

        End If

        If mdtTable.Rows.Count = 0 Then Exit Sub

        Try
            If radApplytoAll.Checked = True Then

                For Each dRow As DataRow In mdtTable.Rows
                    dRow.Item("employmenttypeunkid") = CInt(cboEmpType.SelectedValue)
                    dRow.Item("shiftunkid") = CInt(cboShift.SelectedValue)
                    dRow.Item("departmentunkid") = CInt(cboDepartment.SelectedValue)
                    dRow.Item("jobunkid") = CInt(cboJob.SelectedValue)
                    dRow.Item("gradegroupunkid") = CInt(cboGradeGrp.SelectedValue)
                    dRow.Item("gradeunkid") = CInt(cboGrade.SelectedValue)
                    dRow.Item("gradelevelunkid") = CInt(cboGradeLevel.SelectedValue)
                    dRow.Item("tranhedunkid") = CInt(cboTranhead.SelectedValue)
                    dRow.Item("costcenterunkid") = CInt(cboCostcenter.SelectedValue)
                    dRow.Item("scale") = CDec(txtSalary.Text.Trim)
                    dRow.Item("appointeddate") = dtAppointdate.Value.Date
                    dRow.Item("IsChange") = True

                    dRow.Item("employmenttypename") = cboEmpType.Text
                    dRow.Item("shiftname") = cboShift.Text
                    dRow.Item("deptname") = cboDepartment.Text
                    dRow.Item("jobname") = cboJob.Text
                    dRow.Item("gradegrpname") = cboGradeGrp.Text
                    dRow.Item("gradename") = cboGrade.Text
                    dRow.Item("gradelevelname") = cboGradeLevel.Text
                    dRow.Item("tranheadname") = cboTranhead.Text
                    dRow.Item("costcentername") = cboCostcenter.Text

                    mdtTable.AcceptChanges()

                Next

            ElseIf radApplySelected.Checked = True Then

                Dim intSelectedIndex As Integer
                If dgvImportInfo.SelectedRows.Count > 0 Then

                    intSelectedIndex = dgvImportInfo.SelectedRows(0).Index

                    mdtTable.Rows(intSelectedIndex).Item("employmenttypeunkid") = CInt(cboEmpType.SelectedValue)
                    mdtTable.Rows(intSelectedIndex).Item("shiftunkid") = CInt(cboShift.SelectedValue)
                    mdtTable.Rows(intSelectedIndex).Item("departmentunkid") = CInt(cboDepartment.SelectedValue)
                    mdtTable.Rows(intSelectedIndex).Item("jobunkid") = CInt(cboJob.SelectedValue)
                    mdtTable.Rows(intSelectedIndex).Item("gradegroupunkid") = CInt(cboGradeGrp.SelectedValue)
                    mdtTable.Rows(intSelectedIndex).Item("gradeunkid") = CInt(cboGrade.SelectedValue)
                    mdtTable.Rows(intSelectedIndex).Item("gradelevelunkid") = CInt(cboGradeLevel.SelectedValue)
                    mdtTable.Rows(intSelectedIndex).Item("tranhedunkid") = CInt(cboTranhead.SelectedValue)
                    mdtTable.Rows(intSelectedIndex).Item("costcenterunkid") = CInt(cboCostcenter.SelectedValue)
                    mdtTable.Rows(intSelectedIndex).Item("scale") = CDec(txtSalary.Text.Trim)
                    mdtTable.Rows(intSelectedIndex).Item("appointeddate") = dtAppointdate.Value.Date
                    mdtTable.Rows(intSelectedIndex).Item("IsChange") = True


                    mdtTable.Rows(intSelectedIndex).Item("employmenttypename") = cboEmpType.Text
                    mdtTable.Rows(intSelectedIndex).Item("shiftname") = cboShift.Text
                    mdtTable.Rows(intSelectedIndex).Item("deptname") = cboDepartment.Text
                    mdtTable.Rows(intSelectedIndex).Item("jobname") = cboJob.Text
                    mdtTable.Rows(intSelectedIndex).Item("gradegrpname") = cboGradeGrp.Text
                    mdtTable.Rows(intSelectedIndex).Item("gradename") = cboGrade.Text
                    mdtTable.Rows(intSelectedIndex).Item("gradelevelname") = cboGradeLevel.Text
                    mdtTable.Rows(intSelectedIndex).Item("tranheadname") = cboTranhead.Text
                    mdtTable.Rows(intSelectedIndex).Item("costcentername") = cboCostcenter.Text

                End If


            ElseIf radApplytoChecked.Checked = True Then

                Dim dtTemp() As DataRow = mdtTable.Select("IsChecked = True And IsChange = False")
                'If dtTemp.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select atleast one item to assign."), enMsgBoxStyle.Information)
                '    Exit Sub
                'End If

                For i As Integer = 0 To dtTemp.Length - 1

                    dtTemp(i).Item("employmenttypeunkid") = CInt(cboEmpType.SelectedValue)
                    dtTemp(i).Item("shiftunkid") = CInt(cboShift.SelectedValue)
                    dtTemp(i).Item("departmentunkid") = CInt(cboDepartment.SelectedValue)
                    dtTemp(i).Item("jobunkid") = CInt(cboJob.SelectedValue)
                    dtTemp(i).Item("gradegroupunkid") = CInt(cboGradeGrp.SelectedValue)
                    dtTemp(i).Item("gradeunkid") = CInt(cboGrade.SelectedValue)
                    dtTemp(i).Item("gradelevelunkid") = CInt(cboGradeLevel.SelectedValue)
                    dtTemp(i).Item("tranhedunkid") = CInt(cboTranhead.SelectedValue)
                    dtTemp(i).Item("costcenterunkid") = CInt(cboCostcenter.SelectedValue)
                    dtTemp(i).Item("scale") = CDec(txtSalary.Text.Trim)
                    dtTemp(i).Item("appointeddate") = dtAppointdate.Value.Date
                    dtTemp(i).Item("IsChange") = True

                    dtTemp(i).Item("employmenttypename") = cboEmpType.Text
                    dtTemp(i).Item("shiftname") = cboShift.Text
                    dtTemp(i).Item("deptname") = cboDepartment.Text
                    dtTemp(i).Item("jobname") = cboJob.Text
                    dtTemp(i).Item("gradegrpname") = cboGradeGrp.Text
                    dtTemp(i).Item("gradename") = cboGrade.Text
                    dtTemp(i).Item("gradelevelname") = cboGradeLevel.Text
                    dtTemp(i).Item("tranheadname") = cboTranhead.Text
                    dtTemp(i).Item("costcentername") = cboCostcenter.Text

                Next

            End If
            Call FillGirdView()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSet_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOpenFile.Click
        Dim ofdlgOpen As New OpenFileDialog
        Dim ObjFile As FileInfo
        Try

            'Pinkal (24-Jan-2011) -- Start

            If ConfigParameter._Object._ExportDataPath = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Please set Export Data Path from Aruti configuration -> Options -> Path."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            'Pinkal (24-Jan-2011) -- End


            ofdlgOpen.InitialDirectory = ConfigParameter._Object._ExportDataPath

            ofdlgOpen.Filter = "XML files (*.xml)|*.xml|Excel files(*.xls)|*.xls"
            If ofdlgOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New FileInfo(ofdlgOpen.FileName)
                txtFilePath.Text = ofdlgOpen.FileName
                Select Case ofdlgOpen.FilterIndex
                    Case 1
                        dsList.Tables.Clear()
                        dsList.ReadXml(txtFilePath.Text)
                    Case 2
                        dsList = objIExcel.Import(txtFilePath.Text)
                End Select
                Dim frm As New frmEmployeeMapping
                If frm.displayDialog(dsList) = False Then
                    mblnCancel = False
                    Exit Sub
                End If
                mdtTable = frm._DataTable
                Call FillGirdView()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim blnFlag As Boolean = False
        Dim dtTemp() As DataRow = Nothing

        'Sohail (13 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objPeriod As New clscommom_period_Tran
        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        'Dim dsEmp = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
        Dim dsEmp = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
        'Sohail (21 Aug 2015) -- End
        If dsEmp.Tables("Period").Rows.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please first Create atleast One Period."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        'Sohail (13 Jan 2012) -- End

        dtTemp = mdtTable.Select("employmenttypeunkid = -1")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please set the Employment Type in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        dtTemp = mdtTable.Select("shiftunkid = -1")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please set the Shift  in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        dtTemp = mdtTable.Select("departmentunkid =  -1")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please set the Department in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        dtTemp = mdtTable.Select("jobunkid = -1")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please set the Job in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        dtTemp = mdtTable.Select("gradegroupunkid =  -1")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please set the Grade Group in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        dtTemp = mdtTable.Select("gradeunkid =  -1")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please set the Grade in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        dtTemp = mdtTable.Select("gradelevelunkid = -1")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Please set the Grade Level in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        dtTemp = mdtTable.Select("scale <=  0 ")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Please enter the Salary in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        dtTemp = mdtTable.Select("tranhedunkid = -1")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Please set the Transaction Head in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        dtTemp = mdtTable.Select("costcenterunkid = -1")
        If dtTemp.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Please set the Cost Center in order to Import file."), enMsgBoxStyle.Information)
            Exit Sub
        End If


        Try
            Dim objemployee As New clsEmployee_Master

            For i As Integer = 0 To mdtTable.Rows.Count - 1

                objemployee._Employeecode = mdtTable.Rows(i)("employeecode").ToString
                objemployee._Titalunkid = CInt(mdtTable.Rows(i)("titleunkid").ToString)
                objemployee._Firstname = mdtTable.Rows(i)("firstname").ToString
                objemployee._Surname = mdtTable.Rows(i)("surname").ToString
                objemployee._Othername = mdtTable.Rows(i)("othername").ToString

                If Not IsDBNull(mdtTable.Rows(i)("appointeddate")) Then
                    objemployee._Appointeddate = CDate(mdtTable.Rows(i)("appointeddate"))
                End If

                objemployee._Gender = CInt(mdtTable.Rows(i)("gender").ToString)
                objemployee._Employmenttypeunkid = CInt(mdtTable.Rows(i)("employmenttypeunkid").ToString)
                objemployee._Paytypeunkid = CInt(mdtTable.Rows(i)("paytypeunkid").ToString)
                objemployee._Paypointunkid = CInt(mdtTable.Rows(i)("paypointunkid").ToString)
                objemployee._Loginname = mdtTable.Rows(i)("loginname").ToString
                objemployee._Password = mdtTable.Rows(i)("password").ToString
                objemployee._Email = mdtTable.Rows(i)("email").ToString
                objemployee._Displayname = mdtTable.Rows(i)("displayname").ToString
                objemployee._Shiftunkid = CInt(mdtTable.Rows(i)("shiftunkid").ToString)

                If Not IsDBNull(mdtTable.Rows(i)("birthdate")) Then
                    objemployee._Birthdate = CDate(mdtTable.Rows(i)("birthdate"))
                End If

                objemployee._Birth_Ward = mdtTable.Rows(i)("birth_ward").ToString
                objemployee._Birthcertificateno = mdtTable.Rows(i)("birthcertificateno").ToString
                objemployee._Birthstateunkid = CInt(mdtTable.Rows(i)("birthstateunkid").ToString)
                objemployee._Birthcountryunkid = CInt(mdtTable.Rows(i)("birthcountryunkid").ToString)
                objemployee._Birthcityunkid = CInt(mdtTable.Rows(i)("birthcityunkid").ToString)
                objemployee._Birth_Village = mdtTable.Rows(i)("birth_village").ToString
                objemployee._Work_Permit_No = mdtTable.Rows(i)("work_permit_no").ToString
                objemployee._Workcountryunkid = CInt(mdtTable.Rows(i)("workcountryunkid").ToString)
                objemployee._Work_Permit_Issue_Place = mdtTable.Rows(i)("work_permit_issue_place").ToString

                If Not IsDBNull(mdtTable.Rows(i)("work_permit_issue_date")) Then
                    objemployee._Work_Permit_Issue_Date = CDate(mdtTable.Rows(i)("work_permit_issue_date"))
                End If

                If Not IsDBNull(mdtTable.Rows(i)("work_permit_expiry_date")) Then
                    objemployee._Work_Permit_Expiry_Date = CDate(mdtTable.Rows(i)("work_permit_expiry_date"))
                End If

                objemployee._Complexionunkid = CInt(mdtTable.Rows(i)("complexionunkid").ToString)
                objemployee._Bloodgroupunkid = CInt(mdtTable.Rows(i)("bloodgroupunkid").ToString)
                objemployee._Eyecolorunkid = CInt(mdtTable.Rows(i)("eyecolorunkid").ToString)
                objemployee._Nationalityunkid = CInt(mdtTable.Rows(i)("nationalityunkid").ToString)
                objemployee._Ethincityunkid = CInt(mdtTable.Rows(i)("ethnicityunkid").ToString)
                objemployee._Religionunkid = CInt(mdtTable.Rows(i)("religionunkid").ToString)
                objemployee._Hairunkid = CInt(mdtTable.Rows(i)("hairunkid").ToString)
                objemployee._Language1unkid = CInt(mdtTable.Rows(i)("language1unkid").ToString)
                objemployee._Language2unkid = CInt(mdtTable.Rows(i)("language2unkid").ToString)
                objemployee._Language3unkid = CInt(mdtTable.Rows(i)("language3unkid").ToString)
                objemployee._Language4unkid = CInt(mdtTable.Rows(i)("language4unkid").ToString)
                objemployee._Extra_Tel_No = mdtTable.Rows(i)("extra_tel_no").ToString
                objemployee._Height = CDec(mdtTable.Rows(i)("height").ToString)
                objemployee._Weight = CDec(mdtTable.Rows(i)("Weight").ToString)
                objemployee._Maritalstatusunkid = CInt(mdtTable.Rows(i)("maritalstatusunkid").ToString)

                If Not IsDBNull(mdtTable.Rows(i)("anniversary_date")) Then
                    objemployee._Anniversary_Date = CDate(mdtTable.Rows(i)("anniversary_date"))
                End If

                objemployee._Sports_Hobbies = mdtTable.Rows(i)("sports_hobbies").ToString
                objemployee._Present_Address1 = mdtTable.Rows(i)("present_address1").ToString
                objemployee._Present_Address2 = mdtTable.Rows(i)("present_address2").ToString
                objemployee._Present_Countryunkid = CInt(mdtTable.Rows(i)("present_countryunkid").ToString)
                objemployee._Present_Postcodeunkid = CInt(mdtTable.Rows(i)("present_postcodeunkid").ToString)
                objemployee._Present_Stateunkid = CInt(mdtTable.Rows(i)("present_stateunkid").ToString)
                objemployee._Present_Provicnce = mdtTable.Rows(i)("present_provicnce").ToString
                objemployee._Present_Post_Townunkid = CInt(mdtTable.Rows(i)("present_post_townunkid").ToString)
                objemployee._Present_Road = mdtTable.Rows(i)("present_road").ToString
                objemployee._Present_Estate = mdtTable.Rows(i)("present_estate").ToString
                objemployee._Present_Plotno = mdtTable.Rows(i)("present_plotNo").ToString
                objemployee._Present_Mobile = mdtTable.Rows(i)("present_mobile").ToString
                objemployee._Present_Alternateno = mdtTable.Rows(i)("present_alternateno").ToString
                objemployee._Present_Tel_No = mdtTable.Rows(i)("present_tel_no").ToString
                objemployee._Present_Fax = mdtTable.Rows(i)("present_fax").ToString
                objemployee._Present_Email = mdtTable.Rows(i)("present_email").ToString
                objemployee._Domicile_Address1 = mdtTable.Rows(i)("domicile_address1").ToString
                objemployee._Domicile_Address2 = mdtTable.Rows(i)("domicile_address2").ToString
                objemployee._Domicile_Countryunkid = CInt(mdtTable.Rows(i)("domicile_countryunkid").ToString)
                objemployee._Domicile_Postcodeunkid = CInt(mdtTable.Rows(i)("domicile_postcodeunkid").ToString)
                objemployee._Domicile_Stateunkid = CInt(mdtTable.Rows(i)("domicile_stateunkid").ToString)
                objemployee._Domicile_Provicnce = mdtTable.Rows(i)("domicile_provicnce").ToString
                objemployee._Domicile_Post_Townunkid = CInt(mdtTable.Rows(i)("domicile_post_townunkid").ToString)
                objemployee._Domicile_Road = mdtTable.Rows(i)("domicile_road").ToString
                objemployee._Domicile_Estate = mdtTable.Rows(i)("domicile_estate").ToString
                objemployee._Domicile_Plotno = mdtTable.Rows(i)("domicile_plotNo").ToString
                objemployee._Domicile_Mobile = mdtTable.Rows(i)("domicile_mobile").ToString
                objemployee._Domicile_Alternateno = mdtTable.Rows(i)("domicile_alternateno").ToString
                objemployee._Domicile_Tel_No = mdtTable.Rows(i)("domicile_tel_no").ToString
                objemployee._Domicile_Fax = mdtTable.Rows(i)("domicile_fax").ToString
                objemployee._Domicile_Email = mdtTable.Rows(i)("domicile_email").ToString
                objemployee._Emer_Con_Firstname = mdtTable.Rows(i)("emer_con_firstname").ToString
                objemployee._Emer_Con_Lastname = mdtTable.Rows(i)("emer_con_lastname").ToString
                objemployee._Emer_Con_Address = mdtTable.Rows(i)("emer_con_address").ToString
                objemployee._Emer_Con_Countryunkid = CInt(mdtTable.Rows(i)("emer_con_countryunkid").ToString)
                objemployee._Emer_Con_Postcodeunkid = CInt(mdtTable.Rows(i)("emer_con_postcodeunkid").ToString)
                objemployee._Emer_Con_State = CInt(mdtTable.Rows(i)("emer_con_state").ToString)
                objemployee._Emer_Con_Provicnce = mdtTable.Rows(i)("emer_con_provicnce").ToString
                objemployee._Emer_Con_Post_Townunkid = CInt(mdtTable.Rows(i)("emer_con_post_townunkid").ToString)
                objemployee._Emer_Con_Road = mdtTable.Rows(i)("emer_con_road").ToString
                objemployee._Emer_Con_Estate = mdtTable.Rows(i)("emer_con_estate").ToString
                objemployee._Emer_Con_Plotno = mdtTable.Rows(i)("emer_con_plotNo").ToString
                objemployee._Emer_Con_Mobile = mdtTable.Rows(i)("emer_con_mobile").ToString
                objemployee._Emer_Con_Alternateno = mdtTable.Rows(i)("emer_con_alternateno").ToString
                objemployee._Emer_Con_Tel_No = mdtTable.Rows(i)("emer_con_tel_no").ToString
                objemployee._Emer_Con_Fax = mdtTable.Rows(i)("emer_con_fax").ToString
                objemployee._Emer_Con_Email = mdtTable.Rows(i)("emer_con_email").ToString
                objemployee._Stationunkid = CInt(mdtTable.Rows(i)("stationunkid").ToString)
                objemployee._Deptgroupunkid = CInt(mdtTable.Rows(i)("deptgroupunkid").ToString)
                objemployee._Departmentunkid = CInt(mdtTable.Rows(i)("departmentunkid").ToString)
                objemployee._Sectionunkid = CInt(mdtTable.Rows(i)("sectionunkid").ToString)
                objemployee._Unitunkid = CInt(mdtTable.Rows(i)("unitunkid").ToString)
                objemployee._Jobgroupunkid = CInt(mdtTable.Rows(i)("jobgroupunkid").ToString)
                objemployee._Jobunkid = CInt(mdtTable.Rows(i)("jobunkid").ToString)
                objemployee._Gradegroupunkid = CInt(mdtTable.Rows(i)("gradegroupunkid").ToString)
                objemployee._Gradeunkid = CInt(mdtTable.Rows(i)("gradeunkid").ToString)
                objemployee._Gradelevelunkid = CInt(mdtTable.Rows(i)("gradelevelunkid").ToString)
                objemployee._Scale = CDec(mdtTable.Rows(i)("scale").ToString)
                objemployee._Accessunkid = CInt(mdtTable.Rows(i)("accessunkid").ToString)
                objemployee._Classgroupunkid = CInt(mdtTable.Rows(i)("classgroupunkid").ToString)
                objemployee._Classunkid = CInt(mdtTable.Rows(i)("classunkid").ToString)
                objemployee._Serviceunkid = CInt(mdtTable.Rows(i)("serviceunkid").ToString)
                objemployee._Costcenterunkid = CInt(mdtTable.Rows(i)("costcenterunkid").ToString)
                objemployee._Tranhedunkid = CInt(mdtTable.Rows(i)("tranhedunkid").ToString)
                objemployee._Actionreasonunkid = CInt(mdtTable.Rows(i)("actionreasonunkid").ToString)

                If mdtTable.Rows(i)("suspended_from_date").ToString.Trim <> "" Then
                    objemployee._Suspende_From_Date = CDate(mdtTable.Rows(i)("suspended_from_date"))
                End If

                If mdtTable.Rows(i)("suspended_to_date").ToString.Trim <> "" Then
                    objemployee._Suspende_To_Date = CDate(mdtTable.Rows(i)("suspended_to_date"))
                End If

                If mdtTable.Rows(i)("probation_from_date").ToString.Trim <> "" Then
                    objemployee._Probation_From_Date = CDate(mdtTable.Rows(i)("probation_from_date"))
                End If

                If mdtTable.Rows(i)("probation_to_date").ToString.Trim <> "" Then
                    objemployee._Probation_To_Date = CDate(mdtTable.Rows(i)("probation_to_date"))
                End If

                If mdtTable.Rows(i)("termination_from_date").ToString.Trim <> "" Then
                    objemployee._Termination_From_Date = CDate(mdtTable.Rows(i)("termination_from_date"))
                End If

                If mdtTable.Rows(i)("termination_to_date").ToString.Trim <> "" Then
                    objemployee._Termination_To_Date = CDate(mdtTable.Rows(i)("termination_to_date"))
                End If

                objemployee._Remark = mdtTable.Rows(i)("remark").ToString
                objemployee._Isactive = CBool(mdtTable.Rows(i)("isactive").ToString)

                'blnFlag = objemployee.Insert()
                If blnFlag = False And objemployee._Message <> "" Then
                    eZeeMsgBox.Show(objemployee._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Next
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddSalary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSalary.Click
        Dim frm As New frmWagetable_AddEdit
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSalary_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGridview Event "
    'Anjan (29 Jan 2011)-Start
    'Issue : this made is been commented temporarily.
    'Private Sub dgvImportInfo_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvImportInfo.CellContentClick
    '    Try
    '        If e.RowIndex <= -1 Then Exit Sub

    '        If CBool(mdtTable.Rows(e.RowIndex)("IsChecked")) = True Then

    '            mdtTable.Rows(e.RowIndex)("IsChecked") = False
    '            mdtTable.Rows(e.RowIndex).Item("employmenttypeunkid") = -1
    '            mdtTable.Rows(e.RowIndex).Item("shiftunkid") = -1
    '            mdtTable.Rows(e.RowIndex).Item("departmentunkid") = -1
    '            mdtTable.Rows(e.RowIndex).Item("jobunkid") = -1
    '            mdtTable.Rows(e.RowIndex).Item("gradegroupunkid") = -1
    '            mdtTable.Rows(e.RowIndex).Item("gradeunkid") = -1
    '            mdtTable.Rows(e.RowIndex).Item("gradelevelunkid") = -1
    '            mdtTable.Rows(e.RowIndex).Item("tranhedunkid") = -1
    '            mdtTable.Rows(e.RowIndex).Item("costcenterunkid") = -1
    '            mdtTable.Rows(e.RowIndex).Item("scale") = 0.0
    '            mdtTable.Rows(e.RowIndex).Item("appointeddate") = DBNull.Value
    '            mdtTable.Rows(e.RowIndex).Item("IsChange") = False

    '            mdtTable.Rows(e.RowIndex).Item("employmenttypename") = ""
    '            mdtTable.Rows(e.RowIndex).Item("shiftname") = ""
    '            mdtTable.Rows(e.RowIndex).Item("deptname") = ""
    '            mdtTable.Rows(e.RowIndex).Item("jobname") = ""
    '            mdtTable.Rows(e.RowIndex).Item("gradegrpname") = ""
    '            mdtTable.Rows(e.RowIndex).Item("gradename") = ""
    '            mdtTable.Rows(e.RowIndex).Item("gradelevelname") = ""
    '            mdtTable.Rows(e.RowIndex).Item("tranheadname") = ""
    '            mdtTable.Rows(e.RowIndex).Item("costcentername") = ""

    '            dgvImportInfo.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value = mdtTable.Rows(e.RowIndex)("IsChecked")

    '            dgvImportInfo.RefreshEdit()
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub
    'Anjan (29 Jan 2011)-End
#End Region

#Region "Combobox Event"

    Private Sub cboGradeGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeGrp.SelectedIndexChanged
        Try
            Dim dsCombos As DataSet = Nothing
            Dim objGrade As New clsGrade
            dsCombos = objGrade.getComboList("Grade", True, CInt(cboGradeGrp.SelectedValue))
            cboGrade.ValueMember = "gradeunkid"
            cboGrade.DisplayMember = "name"
            cboGrade.DataSource = dsCombos.Tables("Grade")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Try
            Dim dsCombos As DataSet = Nothing
            Dim objGradeLevel As New clsGradeLevel
            dsCombos = objGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
            cboGradeLevel.ValueMember = "gradelevelunkid"
            cboGradeLevel.DisplayMember = "name"
            cboGradeLevel.DataSource = dsCombos.Tables("GradeLevel")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGradeLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeLevel.SelectedIndexChanged
        Dim objWagesTran As New clsWagesTran
        Dim decScale As Decimal = 0 'Sohail (11 May 2011)
        Dim objWages As New clsWagesTran
        Try
            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'objWagesTran.GetSalary(CInt(cboGradeGrp.SelectedValue), CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), decScale)
            objWagesTran.GetSalary(CInt(cboGradeGrp.SelectedValue), CInt(cboGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), decScale)
            'Sohail (27 Apr 2016) -- End
            txtSalary.Text = CStr(decScale)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFileInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFileInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnImport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnImport.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnSet.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnSet.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
			Me.gbFileInfo.Text = Language._Object.getCaption(Me.gbFileInfo.Name, Me.gbFileInfo.Text)
			Me.lblFileName.Text = Language._Object.getCaption(Me.lblFileName.Name, Me.lblFileName.Text)
			Me.elMandatoryInfo.Text = Language._Object.getCaption(Me.elMandatoryInfo.Name, Me.elMandatoryInfo.Text)
			Me.radApplytoChecked.Text = Language._Object.getCaption(Me.radApplytoChecked.Name, Me.radApplytoChecked.Text)
			Me.radApplytoAll.Text = Language._Object.getCaption(Me.radApplytoAll.Name, Me.radApplytoAll.Text)
			Me.lblDept.Text = Language._Object.getCaption(Me.lblDept.Name, Me.lblDept.Text)
			Me.radApplySelected.Text = Language._Object.getCaption(Me.radApplySelected.Name, Me.radApplySelected.Text)
			Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
			Me.lblEmpType.Text = Language._Object.getCaption(Me.lblEmpType.Name, Me.lblEmpType.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
			Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
			Me.lblCostcenter.Text = Language._Object.getCaption(Me.lblCostcenter.Name, Me.lblCostcenter.Text)
			Me.lblTranHead.Text = Language._Object.getCaption(Me.lblTranHead.Name, Me.lblTranHead.Text)
			Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
			Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
			Me.lblGradeGrp.Text = Language._Object.getCaption(Me.lblGradeGrp.Name, Me.lblGradeGrp.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.lblSalary.Text = Language._Object.getCaption(Me.lblSalary.Name, Me.lblSalary.Text)
			Me.lblAppointdate.Text = Language._Object.getCaption(Me.lblAppointdate.Name, Me.lblAppointdate.Text)
			Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
			Me.dgcolhFirstName.HeaderText = Language._Object.getCaption(Me.dgcolhFirstName.Name, Me.dgcolhFirstName.HeaderText)
			Me.dgcolhOthername.HeaderText = Language._Object.getCaption(Me.dgcolhOthername.Name, Me.dgcolhOthername.HeaderText)
			Me.dgcolhSurname.HeaderText = Language._Object.getCaption(Me.dgcolhSurname.Name, Me.dgcolhSurname.HeaderText)
			Me.dgcolhappointeddate.HeaderText = Language._Object.getCaption(Me.dgcolhappointeddate.Name, Me.dgcolhappointeddate.HeaderText)
			Me.dgcolhEmpType.HeaderText = Language._Object.getCaption(Me.dgcolhEmpType.Name, Me.dgcolhEmpType.HeaderText)
			Me.dgcolhShift.HeaderText = Language._Object.getCaption(Me.dgcolhShift.Name, Me.dgcolhShift.HeaderText)
			Me.dgcolhDept.HeaderText = Language._Object.getCaption(Me.dgcolhDept.Name, Me.dgcolhDept.HeaderText)
			Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
			Me.dgcolhGradeGrp.HeaderText = Language._Object.getCaption(Me.dgcolhGradeGrp.Name, Me.dgcolhGradeGrp.HeaderText)
			Me.dgcolhGrade.HeaderText = Language._Object.getCaption(Me.dgcolhGrade.Name, Me.dgcolhGrade.HeaderText)
			Me.dgcolhGradelevel.HeaderText = Language._Object.getCaption(Me.dgcolhGradelevel.Name, Me.dgcolhGradelevel.HeaderText)
			Me.dgcolhTranHead.HeaderText = Language._Object.getCaption(Me.dgcolhTranHead.Name, Me.dgcolhTranHead.HeaderText)
			Me.dgcolhCostCenter.HeaderText = Language._Object.getCaption(Me.dgcolhCostCenter.Name, Me.dgcolhCostCenter.HeaderText)
			Me.dgcolhSalaray.HeaderText = Language._Object.getCaption(Me.dgcolhSalaray.Name, Me.dgcolhSalaray.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please atleast one action to set the value.")
			Language.setMessage(mstrModuleName, 2, "Employeement Type is compulsory Information. Please Select Employeement Type.")
			Language.setMessage(mstrModuleName, 3, "Shift is compulsory Information. Please Select Shift.")
			Language.setMessage(mstrModuleName, 4, "Deparment is compulsory Information. Please Select Deparment.")
			Language.setMessage(mstrModuleName, 5, "Job is compulsory Information. Please Select Job.")
			Language.setMessage(mstrModuleName, 6, "Grade Group is compulsory Information. Please Select Grade Group.")
			Language.setMessage(mstrModuleName, 7, "Grade is compulsory Information. Please Select Grade.")
			Language.setMessage(mstrModuleName, 8, "Grade Level is compulsory Information. Please Select Grade Level.")
			Language.setMessage(mstrModuleName, 9, "Salary cannot be blank. Salary is required information.")
			Language.setMessage(mstrModuleName, 10, "Transaction Head is compulsory Information. Please Select Transaction Head.")
			Language.setMessage(mstrModuleName, 11, "Cost Center is compulsory Information. Please Select Cost Center.")
			Language.setMessage(mstrModuleName, 13, "Please set the Employment Type in order to Import file.")
			Language.setMessage(mstrModuleName, 14, "Please set the Shift  in order to Import file.")
			Language.setMessage(mstrModuleName, 15, "Please set the Department in order to Import file.")
			Language.setMessage(mstrModuleName, 16, "Please set the Job in order to Import file.")
			Language.setMessage(mstrModuleName, 17, "Please set the Grade Group in order to Import file.")
			Language.setMessage(mstrModuleName, 18, "Please set the Grade in order to Import file.")
			Language.setMessage(mstrModuleName, 19, "Please set the Grade Level in order to Import file.")
			Language.setMessage(mstrModuleName, 20, "Please enter the Salary in order to Import file.")
			Language.setMessage(mstrModuleName, 21, "Please set the Transaction Head in order to Import file.")
			Language.setMessage(mstrModuleName, 22, "Please set Export Data Path from Aruti configuration -> Options -> Path.")
			Language.setMessage(mstrModuleName, 23, "Please first Create atleast One Period.")
			Language.setMessage(mstrModuleName, 24, "Please set the Cost Center in order to Import file.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class