﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddressImportWizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddressImportWizard))
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.eZeeWizImportImgs = New eZee.Common.eZeeWizard
        Me.WizPageSelectFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblNote = New System.Windows.Forms.Label
        Me.cboSelectType = New System.Windows.Forms.ComboBox
        Me.lblSelectType = New System.Windows.Forms.Label
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.WizPageImporting = New eZee.Common.eZeeWizardPage(Me.components)
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.WizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboFax = New System.Windows.Forms.ComboBox
        Me.cboEmail = New System.Windows.Forms.ComboBox
        Me.cboAlternativeNo = New System.Windows.Forms.ComboBox
        Me.cboPlotNo = New System.Windows.Forms.ComboBox
        Me.cboTelNo = New System.Windows.Forms.ComboBox
        Me.cboMobile = New System.Windows.Forms.ComboBox
        Me.cboTown1 = New System.Windows.Forms.ComboBox
        Me.cboVillage = New System.Windows.Forms.ComboBox
        Me.cboChiefdom = New System.Windows.Forms.ComboBox
        Me.cboProvRegion1 = New System.Windows.Forms.ComboBox
        Me.cboRoadStreet1 = New System.Windows.Forms.ComboBox
        Me.cboEState = New System.Windows.Forms.ComboBox
        Me.cboRoadStreet = New System.Windows.Forms.ComboBox
        Me.cboProvRegion = New System.Windows.Forms.ComboBox
        Me.cboPostCode = New System.Windows.Forms.ComboBox
        Me.cboPostTown = New System.Windows.Forms.ComboBox
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.cboPostCountry = New System.Windows.Forms.ComboBox
        Me.cboAddress1 = New System.Windows.Forms.ComboBox
        Me.cboAddress2 = New System.Windows.Forms.ComboBox
        Me.lblChiefdom = New System.Windows.Forms.Label
        Me.lblEstate = New System.Windows.Forms.Label
        Me.lblAlternativeNo = New System.Windows.Forms.Label
        Me.lblMobile = New System.Windows.Forms.Label
        Me.lblPostCountry = New System.Windows.Forms.Label
        Me.lblPostcode = New System.Windows.Forms.Label
        Me.lblTown1 = New System.Windows.Forms.Label
        Me.lblRoadStreet1 = New System.Windows.Forms.Label
        Me.lblAddress2 = New System.Windows.Forms.Label
        Me.lblPostTown = New System.Windows.Forms.Label
        Me.lblRoad = New System.Windows.Forms.Label
        Me.lblFax = New System.Windows.Forms.Label
        Me.lblPloteNo = New System.Windows.Forms.Label
        Me.lblVillage = New System.Windows.Forms.Label
        Me.lblProvince1 = New System.Windows.Forms.Label
        Me.lblState = New System.Windows.Forms.Label
        Me.lblProvRegion = New System.Windows.Forms.Label
        Me.lblEmail = New System.Windows.Forms.Label
        Me.lblTelNo = New System.Windows.Forms.Label
        Me.lblAddress = New System.Windows.Forms.Label
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.objlblSign6 = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.pnlOtherAddress = New System.Windows.Forms.Panel
        Me.objspc1 = New System.Windows.Forms.SplitContainer
        Me.pnlEmergencyAddress = New System.Windows.Forms.Panel
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.lblEmgFirstName = New System.Windows.Forms.Label
        Me.lblEmgLastName = New System.Windows.Forms.Label
        Me.lblEmgAddress = New System.Windows.Forms.Label
        Me.cboEmergencyFirstname = New System.Windows.Forms.ComboBox
        Me.cboEmergencyLastname = New System.Windows.Forms.ComboBox
        Me.cboEmergencyAddress = New System.Windows.Forms.ComboBox
        Me.cboEmergencyCountry = New System.Windows.Forms.ComboBox
        Me.lblEmgPostCountry = New System.Windows.Forms.Label
        Me.cboEmergencyState = New System.Windows.Forms.ComboBox
        Me.lblEmgPostTown = New System.Windows.Forms.Label
        Me.lblEmgState = New System.Windows.Forms.Label
        Me.cboEmergencyTown = New System.Windows.Forms.ComboBox
        Me.lblEmgProvince = New System.Windows.Forms.Label
        Me.cboEmergencyPostCode = New System.Windows.Forms.ComboBox
        Me.lblEmgPostCode = New System.Windows.Forms.Label
        Me.cboEmergencyProvRegion = New System.Windows.Forms.ComboBox
        Me.lblEmgRoad = New System.Windows.Forms.Label
        Me.lblEmgMobile = New System.Windows.Forms.Label
        Me.lblEmgPlotNo = New System.Windows.Forms.Label
        Me.lblEmgEstate = New System.Windows.Forms.Label
        Me.lblEmgTelNo = New System.Windows.Forms.Label
        Me.lblEmgAltNo = New System.Windows.Forms.Label
        Me.lblEmgFax = New System.Windows.Forms.Label
        Me.lblEmgEmail = New System.Windows.Forms.Label
        Me.cboEmergencyRoadStreet = New System.Windows.Forms.ComboBox
        Me.cboEmergencyEstate = New System.Windows.Forms.ComboBox
        Me.cboEmergencyPlotNumber = New System.Windows.Forms.ComboBox
        Me.cboEmergencyTelNo = New System.Windows.Forms.ComboBox
        Me.cboEmergencyAltNo = New System.Windows.Forms.ComboBox
        Me.cboEmergencyMobile = New System.Windows.Forms.ComboBox
        Me.cboEmergencyEmail = New System.Windows.Forms.ComboBox
        Me.cboEmergencyFaxNo = New System.Windows.Forms.ComboBox
        Me.lnkAllocationFormat = New System.Windows.Forms.LinkLabel
        Me.cmsFilter.SuspendLayout()
        Me.eZeeWizImportImgs.SuspendLayout()
        Me.WizPageSelectFile.SuspendLayout()
        Me.WizPageImporting.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInfo.SuspendLayout()
        Me.WizPageMapping.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.pnlOtherAddress.SuspendLayout()
        Me.objspc1.Panel1.SuspendLayout()
        Me.objspc1.Panel2.SuspendLayout()
        Me.objspc1.SuspendLayout()
        Me.pnlEmergencyAddress.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'eZeeWizImportImgs
        '
        Me.eZeeWizImportImgs.Controls.Add(Me.WizPageSelectFile)
        Me.eZeeWizImportImgs.Controls.Add(Me.WizPageMapping)
        Me.eZeeWizImportImgs.Controls.Add(Me.WizPageImporting)
        Me.eZeeWizImportImgs.Dock = System.Windows.Forms.DockStyle.Fill
        Me.eZeeWizImportImgs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeWizImportImgs.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.eZeeWizImportImgs.Location = New System.Drawing.Point(0, 0)
        Me.eZeeWizImportImgs.Name = "eZeeWizImportImgs"
        Me.eZeeWizImportImgs.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.WizPageSelectFile, Me.WizPageMapping, Me.WizPageImporting})
        Me.eZeeWizImportImgs.SaveEnabled = True
        Me.eZeeWizImportImgs.SaveText = "Save && Finish"
        Me.eZeeWizImportImgs.SaveVisible = False
        Me.eZeeWizImportImgs.SetSaveIndexBeforeFinishIndex = False
        Me.eZeeWizImportImgs.Size = New System.Drawing.Size(717, 429)
        Me.eZeeWizImportImgs.TabIndex = 0
        Me.eZeeWizImportImgs.WelcomeImage = Nothing
        '
        'WizPageSelectFile
        '
        Me.WizPageSelectFile.Controls.Add(Me.lnkAllocationFormat)
        Me.WizPageSelectFile.Controls.Add(Me.lblNote)
        Me.WizPageSelectFile.Controls.Add(Me.cboSelectType)
        Me.WizPageSelectFile.Controls.Add(Me.lblSelectType)
        Me.WizPageSelectFile.Controls.Add(Me.lblMessage)
        Me.WizPageSelectFile.Controls.Add(Me.lblTitle)
        Me.WizPageSelectFile.Controls.Add(Me.btnOpenFile)
        Me.WizPageSelectFile.Controls.Add(Me.txtFilePath)
        Me.WizPageSelectFile.Controls.Add(Me.lblSelectfile)
        Me.WizPageSelectFile.Location = New System.Drawing.Point(0, 0)
        Me.WizPageSelectFile.Name = "WizPageSelectFile"
        Me.WizPageSelectFile.Size = New System.Drawing.Size(717, 381)
        Me.WizPageSelectFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageSelectFile.TabIndex = 7
        '
        'lblNote
        '
        Me.lblNote.BackColor = System.Drawing.Color.Transparent
        Me.lblNote.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNote.ForeColor = System.Drawing.Color.Red
        Me.lblNote.Location = New System.Drawing.Point(182, 236)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(523, 48)
        Me.lblNote.TabIndex = 20
        Me.lblNote.Text = resources.GetString("lblNote.Text")
        '
        'cboSelectType
        '
        Me.cboSelectType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSelectType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSelectType.FormattingEnabled = True
        Me.cboSelectType.Location = New System.Drawing.Point(185, 121)
        Me.cboSelectType.Name = "cboSelectType"
        Me.cboSelectType.Size = New System.Drawing.Size(520, 21)
        Me.cboSelectType.TabIndex = 100
        '
        'lblSelectType
        '
        Me.lblSelectType.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectType.Location = New System.Drawing.Point(182, 101)
        Me.lblSelectType.Name = "lblSelectType"
        Me.lblSelectType.Size = New System.Drawing.Size(523, 17)
        Me.lblSelectType.TabIndex = 19
        Me.lblSelectType.Text = "Select Type"
        Me.lblSelectType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(182, 46)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(523, 20)
        Me.lblMessage.TabIndex = 18
        Me.lblMessage.Text = "This wizard will import 'Address' to database from shared folder."
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(181, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(524, 36)
        Me.lblTitle.TabIndex = 17
        Me.lblTitle.Text = "Address Import Wizard"
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(678, 165)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(27, 21)
        Me.btnOpenFile.TabIndex = 16
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(185, 165)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(487, 21)
        Me.txtFilePath.TabIndex = 15
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(182, 145)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(523, 17)
        Me.lblSelectfile.TabIndex = 14
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'WizPageImporting
        '
        Me.WizPageImporting.Controls.Add(Me.dgData)
        Me.WizPageImporting.Controls.Add(Me.btnFilter)
        Me.WizPageImporting.Controls.Add(Me.pnlInfo)
        Me.WizPageImporting.Location = New System.Drawing.Point(0, 0)
        Me.WizPageImporting.Name = "WizPageImporting"
        Me.WizPageImporting.Size = New System.Drawing.Size(717, 381)
        Me.WizPageImporting.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.WizPageImporting.TabIndex = 9
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeight = 22
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.colhStatus, Me.colhMessage, Me.objcolhstatus})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(702, 270)
        Me.dgData.TabIndex = 19
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 25
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        Me.colhStatus.Width = 70
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 175
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 345)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(95, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 21
        Me.btnFilter.Text = "Filter"
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(702, 51)
        Me.pnlInfo.TabIndex = 2
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(580, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(466, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(580, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(512, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(625, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(466, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(625, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(512, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'WizPageMapping
        '
        Me.WizPageMapping.Controls.Add(Me.gbFieldMapping)
        Me.WizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.WizPageMapping.Name = "WizPageMapping"
        Me.WizPageMapping.Size = New System.Drawing.Size(717, 381)
        Me.WizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageMapping.TabIndex = 8
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.lnkAutoMap)
        Me.gbFieldMapping.Controls.Add(Me.objspc1)
        Me.gbFieldMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFieldMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign1)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign6)
        Me.gbFieldMapping.Controls.Add(Me.lblCaption)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(164, 0)
        Me.gbFieldMapping.Margin = New System.Windows.Forms.Padding(0)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(552, 381)
        Me.gbFieldMapping.TabIndex = 0
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFax
        '
        Me.cboFax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFax.FormattingEnabled = True
        Me.cboFax.Location = New System.Drawing.Point(373, 248)
        Me.cboFax.Name = "cboFax"
        Me.cboFax.Size = New System.Drawing.Size(169, 21)
        Me.cboFax.TabIndex = 20
        '
        'cboEmail
        '
        Me.cboEmail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmail.FormattingEnabled = True
        Me.cboEmail.Location = New System.Drawing.Point(373, 221)
        Me.cboEmail.Name = "cboEmail"
        Me.cboEmail.Size = New System.Drawing.Size(169, 21)
        Me.cboEmail.TabIndex = 19
        '
        'cboAlternativeNo
        '
        Me.cboAlternativeNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAlternativeNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAlternativeNo.FormattingEnabled = True
        Me.cboAlternativeNo.Location = New System.Drawing.Point(373, 194)
        Me.cboAlternativeNo.Name = "cboAlternativeNo"
        Me.cboAlternativeNo.Size = New System.Drawing.Size(169, 21)
        Me.cboAlternativeNo.TabIndex = 18
        '
        'cboPlotNo
        '
        Me.cboPlotNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPlotNo.FormattingEnabled = True
        Me.cboPlotNo.Location = New System.Drawing.Point(372, 167)
        Me.cboPlotNo.Name = "cboPlotNo"
        Me.cboPlotNo.Size = New System.Drawing.Size(169, 21)
        Me.cboPlotNo.TabIndex = 17
        '
        'cboTelNo
        '
        Me.cboTelNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTelNo.FormattingEnabled = True
        Me.cboTelNo.Location = New System.Drawing.Point(373, 140)
        Me.cboTelNo.Name = "cboTelNo"
        Me.cboTelNo.Size = New System.Drawing.Size(169, 21)
        Me.cboTelNo.TabIndex = 16
        '
        'cboMobile
        '
        Me.cboMobile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMobile.FormattingEnabled = True
        Me.cboMobile.Location = New System.Drawing.Point(373, 113)
        Me.cboMobile.Name = "cboMobile"
        Me.cboMobile.Size = New System.Drawing.Size(169, 21)
        Me.cboMobile.TabIndex = 15
        '
        'cboTown1
        '
        Me.cboTown1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTown1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTown1.FormattingEnabled = True
        Me.cboTown1.Location = New System.Drawing.Point(373, 86)
        Me.cboTown1.Name = "cboTown1"
        Me.cboTown1.Size = New System.Drawing.Size(169, 21)
        Me.cboTown1.TabIndex = 14
        '
        'cboVillage
        '
        Me.cboVillage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVillage.FormattingEnabled = True
        Me.cboVillage.Location = New System.Drawing.Point(373, 59)
        Me.cboVillage.Name = "cboVillage"
        Me.cboVillage.Size = New System.Drawing.Size(169, 21)
        Me.cboVillage.TabIndex = 13
        '
        'cboChiefdom
        '
        Me.cboChiefdom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChiefdom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChiefdom.FormattingEnabled = True
        Me.cboChiefdom.Location = New System.Drawing.Point(373, 32)
        Me.cboChiefdom.Name = "cboChiefdom"
        Me.cboChiefdom.Size = New System.Drawing.Size(169, 21)
        Me.cboChiefdom.TabIndex = 12
        '
        'cboProvRegion1
        '
        Me.cboProvRegion1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProvRegion1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProvRegion1.FormattingEnabled = True
        Me.cboProvRegion1.Location = New System.Drawing.Point(373, 5)
        Me.cboProvRegion1.Name = "cboProvRegion1"
        Me.cboProvRegion1.Size = New System.Drawing.Size(169, 21)
        Me.cboProvRegion1.TabIndex = 11
        '
        'cboRoadStreet1
        '
        Me.cboRoadStreet1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRoadStreet1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRoadStreet1.FormattingEnabled = True
        Me.cboRoadStreet1.Location = New System.Drawing.Point(116, 248)
        Me.cboRoadStreet1.Name = "cboRoadStreet1"
        Me.cboRoadStreet1.Size = New System.Drawing.Size(169, 21)
        Me.cboRoadStreet1.TabIndex = 10
        '
        'cboEState
        '
        Me.cboEState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEState.FormattingEnabled = True
        Me.cboEState.Location = New System.Drawing.Point(116, 221)
        Me.cboEState.Name = "cboEState"
        Me.cboEState.Size = New System.Drawing.Size(169, 21)
        Me.cboEState.TabIndex = 9
        '
        'cboRoadStreet
        '
        Me.cboRoadStreet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRoadStreet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRoadStreet.FormattingEnabled = True
        Me.cboRoadStreet.Location = New System.Drawing.Point(116, 194)
        Me.cboRoadStreet.Name = "cboRoadStreet"
        Me.cboRoadStreet.Size = New System.Drawing.Size(169, 21)
        Me.cboRoadStreet.TabIndex = 8
        '
        'cboProvRegion
        '
        Me.cboProvRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProvRegion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProvRegion.FormattingEnabled = True
        Me.cboProvRegion.Location = New System.Drawing.Point(116, 167)
        Me.cboProvRegion.Name = "cboProvRegion"
        Me.cboProvRegion.Size = New System.Drawing.Size(169, 21)
        Me.cboProvRegion.TabIndex = 7
        '
        'cboPostCode
        '
        Me.cboPostCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPostCode.FormattingEnabled = True
        Me.cboPostCode.Location = New System.Drawing.Point(116, 140)
        Me.cboPostCode.Name = "cboPostCode"
        Me.cboPostCode.Size = New System.Drawing.Size(169, 21)
        Me.cboPostCode.TabIndex = 6
        '
        'cboPostTown
        '
        Me.cboPostTown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPostTown.FormattingEnabled = True
        Me.cboPostTown.Location = New System.Drawing.Point(116, 113)
        Me.cboPostTown.Name = "cboPostTown"
        Me.cboPostTown.Size = New System.Drawing.Size(168, 21)
        Me.cboPostTown.TabIndex = 5
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(116, 86)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(169, 21)
        Me.cboState.TabIndex = 4
        '
        'cboPostCountry
        '
        Me.cboPostCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPostCountry.FormattingEnabled = True
        Me.cboPostCountry.Location = New System.Drawing.Point(116, 59)
        Me.cboPostCountry.Name = "cboPostCountry"
        Me.cboPostCountry.Size = New System.Drawing.Size(169, 21)
        Me.cboPostCountry.TabIndex = 3
        '
        'cboAddress1
        '
        Me.cboAddress1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAddress1.FormattingEnabled = True
        Me.cboAddress1.Location = New System.Drawing.Point(116, 5)
        Me.cboAddress1.Name = "cboAddress1"
        Me.cboAddress1.Size = New System.Drawing.Size(169, 21)
        Me.cboAddress1.TabIndex = 1
        '
        'cboAddress2
        '
        Me.cboAddress2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAddress2.FormattingEnabled = True
        Me.cboAddress2.Location = New System.Drawing.Point(116, 32)
        Me.cboAddress2.Name = "cboAddress2"
        Me.cboAddress2.Size = New System.Drawing.Size(169, 21)
        Me.cboAddress2.TabIndex = 2
        '
        'lblChiefdom
        '
        Me.lblChiefdom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChiefdom.Location = New System.Drawing.Point(290, 35)
        Me.lblChiefdom.Name = "lblChiefdom"
        Me.lblChiefdom.Size = New System.Drawing.Size(77, 15)
        Me.lblChiefdom.TabIndex = 33
        Me.lblChiefdom.Text = "Chiefdom"
        Me.lblChiefdom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEstate
        '
        Me.lblEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstate.Location = New System.Drawing.Point(24, 224)
        Me.lblEstate.Name = "lblEstate"
        Me.lblEstate.Size = New System.Drawing.Size(86, 15)
        Me.lblEstate.TabIndex = 30
        Me.lblEstate.Text = "Estate"
        Me.lblEstate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAlternativeNo
        '
        Me.lblAlternativeNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAlternativeNo.Location = New System.Drawing.Point(290, 197)
        Me.lblAlternativeNo.Name = "lblAlternativeNo"
        Me.lblAlternativeNo.Size = New System.Drawing.Size(77, 15)
        Me.lblAlternativeNo.TabIndex = 39
        Me.lblAlternativeNo.Text = "Alternative No"
        Me.lblAlternativeNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMobile
        '
        Me.lblMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMobile.Location = New System.Drawing.Point(290, 116)
        Me.lblMobile.Name = "lblMobile"
        Me.lblMobile.Size = New System.Drawing.Size(77, 15)
        Me.lblMobile.TabIndex = 36
        Me.lblMobile.Text = "Mobile"
        Me.lblMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostCountry
        '
        Me.lblPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostCountry.Location = New System.Drawing.Point(24, 62)
        Me.lblPostCountry.Name = "lblPostCountry"
        Me.lblPostCountry.Size = New System.Drawing.Size(86, 15)
        Me.lblPostCountry.TabIndex = 24
        Me.lblPostCountry.Text = "Post Country"
        Me.lblPostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostcode
        '
        Me.lblPostcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostcode.Location = New System.Drawing.Point(24, 143)
        Me.lblPostcode.Name = "lblPostcode"
        Me.lblPostcode.Size = New System.Drawing.Size(86, 15)
        Me.lblPostcode.TabIndex = 27
        Me.lblPostcode.Text = "Post Code"
        Me.lblPostcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTown1
        '
        Me.lblTown1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTown1.Location = New System.Drawing.Point(290, 89)
        Me.lblTown1.Name = "lblTown1"
        Me.lblTown1.Size = New System.Drawing.Size(77, 15)
        Me.lblTown1.TabIndex = 35
        Me.lblTown1.Text = "Town1"
        Me.lblTown1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRoadStreet1
        '
        Me.lblRoadStreet1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRoadStreet1.Location = New System.Drawing.Point(24, 251)
        Me.lblRoadStreet1.Name = "lblRoadStreet1"
        Me.lblRoadStreet1.Size = New System.Drawing.Size(86, 15)
        Me.lblRoadStreet1.TabIndex = 31
        Me.lblRoadStreet1.Text = "Road/Street1"
        Me.lblRoadStreet1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAddress2
        '
        Me.lblAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress2.Location = New System.Drawing.Point(24, 35)
        Me.lblAddress2.Name = "lblAddress2"
        Me.lblAddress2.Size = New System.Drawing.Size(86, 15)
        Me.lblAddress2.TabIndex = 23
        Me.lblAddress2.Text = "Address2"
        Me.lblAddress2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostTown
        '
        Me.lblPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostTown.Location = New System.Drawing.Point(24, 116)
        Me.lblPostTown.Name = "lblPostTown"
        Me.lblPostTown.Size = New System.Drawing.Size(86, 15)
        Me.lblPostTown.TabIndex = 26
        Me.lblPostTown.Text = "Post Town"
        Me.lblPostTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRoad
        '
        Me.lblRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRoad.Location = New System.Drawing.Point(24, 197)
        Me.lblRoad.Name = "lblRoad"
        Me.lblRoad.Size = New System.Drawing.Size(86, 15)
        Me.lblRoad.TabIndex = 29
        Me.lblRoad.Text = "Road/Street"
        Me.lblRoad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFax
        '
        Me.lblFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFax.Location = New System.Drawing.Point(290, 251)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(77, 15)
        Me.lblFax.TabIndex = 41
        Me.lblFax.Text = "Fax"
        Me.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPloteNo
        '
        Me.lblPloteNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPloteNo.Location = New System.Drawing.Point(290, 170)
        Me.lblPloteNo.Name = "lblPloteNo"
        Me.lblPloteNo.Size = New System.Drawing.Size(77, 15)
        Me.lblPloteNo.TabIndex = 38
        Me.lblPloteNo.Text = "Plot No"
        Me.lblPloteNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVillage
        '
        Me.lblVillage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVillage.Location = New System.Drawing.Point(290, 62)
        Me.lblVillage.Name = "lblVillage"
        Me.lblVillage.Size = New System.Drawing.Size(77, 15)
        Me.lblVillage.TabIndex = 34
        Me.lblVillage.Text = "Village"
        Me.lblVillage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblProvince1
        '
        Me.lblProvince1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProvince1.Location = New System.Drawing.Point(290, 8)
        Me.lblProvince1.Name = "lblProvince1"
        Me.lblProvince1.Size = New System.Drawing.Size(77, 15)
        Me.lblProvince1.TabIndex = 32
        Me.lblProvince1.Text = "Prov/Region1"
        Me.lblProvince1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(24, 89)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(86, 15)
        Me.lblState.TabIndex = 25
        Me.lblState.Text = "State"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblProvRegion
        '
        Me.lblProvRegion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProvRegion.Location = New System.Drawing.Point(24, 170)
        Me.lblProvRegion.Name = "lblProvRegion"
        Me.lblProvRegion.Size = New System.Drawing.Size(86, 15)
        Me.lblProvRegion.TabIndex = 28
        Me.lblProvRegion.Text = "Prov/Region"
        Me.lblProvRegion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(290, 224)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(77, 15)
        Me.lblEmail.TabIndex = 40
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTelNo
        '
        Me.lblTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTelNo.Location = New System.Drawing.Point(290, 143)
        Me.lblTelNo.Name = "lblTelNo"
        Me.lblTelNo.Size = New System.Drawing.Size(77, 15)
        Me.lblTelNo.TabIndex = 37
        Me.lblTelNo.Text = "Tel. No"
        Me.lblTelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAddress
        '
        Me.lblAddress.BackColor = System.Drawing.Color.Transparent
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(24, 8)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(86, 15)
        Me.lblAddress.TabIndex = 22
        Me.lblAddress.Text = "Address1"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(117, 32)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(305, 21)
        Me.cboEmployeeCode.TabIndex = 0
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(25, 35)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(86, 15)
        Me.lblEmployeeCode.TabIndex = 21
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(9, 34)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign1.TabIndex = 42
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign6
        '
        Me.objlblSign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign6.ForeColor = System.Drawing.Color.Red
        Me.objlblSign6.Location = New System.Drawing.Point(9, 37)
        Me.objlblSign6.Name = "objlblSign6"
        Me.objlblSign6.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign6.TabIndex = 100
        Me.objlblSign6.Text = "*"
        Me.objlblSign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(291, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(258, 19)
        Me.lblCaption.TabIndex = 43
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pnlOtherAddress
        '
        Me.pnlOtherAddress.Controls.Add(Me.lblAddress)
        Me.pnlOtherAddress.Controls.Add(Me.lblTelNo)
        Me.pnlOtherAddress.Controls.Add(Me.cboFax)
        Me.pnlOtherAddress.Controls.Add(Me.lblEmail)
        Me.pnlOtherAddress.Controls.Add(Me.cboEmail)
        Me.pnlOtherAddress.Controls.Add(Me.lblProvRegion)
        Me.pnlOtherAddress.Controls.Add(Me.cboAlternativeNo)
        Me.pnlOtherAddress.Controls.Add(Me.lblState)
        Me.pnlOtherAddress.Controls.Add(Me.cboPlotNo)
        Me.pnlOtherAddress.Controls.Add(Me.lblProvince1)
        Me.pnlOtherAddress.Controls.Add(Me.cboTelNo)
        Me.pnlOtherAddress.Controls.Add(Me.lblVillage)
        Me.pnlOtherAddress.Controls.Add(Me.cboMobile)
        Me.pnlOtherAddress.Controls.Add(Me.lblPloteNo)
        Me.pnlOtherAddress.Controls.Add(Me.cboTown1)
        Me.pnlOtherAddress.Controls.Add(Me.lblFax)
        Me.pnlOtherAddress.Controls.Add(Me.cboVillage)
        Me.pnlOtherAddress.Controls.Add(Me.lblRoad)
        Me.pnlOtherAddress.Controls.Add(Me.cboChiefdom)
        Me.pnlOtherAddress.Controls.Add(Me.lblPostTown)
        Me.pnlOtherAddress.Controls.Add(Me.cboProvRegion1)
        Me.pnlOtherAddress.Controls.Add(Me.lblAddress2)
        Me.pnlOtherAddress.Controls.Add(Me.cboRoadStreet1)
        Me.pnlOtherAddress.Controls.Add(Me.lblRoadStreet1)
        Me.pnlOtherAddress.Controls.Add(Me.cboEState)
        Me.pnlOtherAddress.Controls.Add(Me.lblTown1)
        Me.pnlOtherAddress.Controls.Add(Me.cboRoadStreet)
        Me.pnlOtherAddress.Controls.Add(Me.lblPostcode)
        Me.pnlOtherAddress.Controls.Add(Me.cboProvRegion)
        Me.pnlOtherAddress.Controls.Add(Me.lblPostCountry)
        Me.pnlOtherAddress.Controls.Add(Me.cboPostCode)
        Me.pnlOtherAddress.Controls.Add(Me.lblMobile)
        Me.pnlOtherAddress.Controls.Add(Me.cboPostTown)
        Me.pnlOtherAddress.Controls.Add(Me.lblAlternativeNo)
        Me.pnlOtherAddress.Controls.Add(Me.cboState)
        Me.pnlOtherAddress.Controls.Add(Me.lblEstate)
        Me.pnlOtherAddress.Controls.Add(Me.cboPostCountry)
        Me.pnlOtherAddress.Controls.Add(Me.lblChiefdom)
        Me.pnlOtherAddress.Controls.Add(Me.cboAddress1)
        Me.pnlOtherAddress.Controls.Add(Me.cboAddress2)
        Me.pnlOtherAddress.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlOtherAddress.Location = New System.Drawing.Point(0, 0)
        Me.pnlOtherAddress.Name = "pnlOtherAddress"
        Me.pnlOtherAddress.Size = New System.Drawing.Size(550, 153)
        Me.pnlOtherAddress.TabIndex = 103
        '
        'objspc1
        '
        Me.objspc1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objspc1.Location = New System.Drawing.Point(1, 53)
        Me.objspc1.Name = "objspc1"
        Me.objspc1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objspc1.Panel1
        '
        Me.objspc1.Panel1.Controls.Add(Me.pnlOtherAddress)
        Me.objspc1.Panel1Collapsed = True
        '
        'objspc1.Panel2
        '
        Me.objspc1.Panel2.Controls.Add(Me.pnlEmergencyAddress)
        Me.objspc1.Size = New System.Drawing.Size(550, 325)
        Me.objspc1.SplitterDistance = 153
        Me.objspc1.SplitterWidth = 1
        Me.objspc1.TabIndex = 104
        '
        'pnlEmergencyAddress
        '
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyEmail)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyFaxNo)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyTelNo)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyAltNo)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyMobile)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyPlotNumber)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyEstate)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyRoadStreet)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgEmail)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgFax)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgTelNo)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgAltNo)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgMobile)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgPlotNo)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgEstate)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgRoad)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyProvRegion)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgProvince)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyPostCode)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgPostCode)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyCountry)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgPostCountry)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyState)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgPostTown)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgState)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyTown)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyAddress)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyLastname)
        Me.pnlEmergencyAddress.Controls.Add(Me.cboEmergencyFirstname)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgFirstName)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgLastName)
        Me.pnlEmergencyAddress.Controls.Add(Me.lblEmgAddress)
        Me.pnlEmergencyAddress.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEmergencyAddress.Location = New System.Drawing.Point(0, 0)
        Me.pnlEmergencyAddress.Name = "pnlEmergencyAddress"
        Me.pnlEmergencyAddress.Size = New System.Drawing.Size(550, 325)
        Me.pnlEmergencyAddress.TabIndex = 0
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAutoMap.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAutoMap.Location = New System.Drawing.Point(428, 32)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(113, 20)
        Me.lnkAutoMap.TabIndex = 105
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "Auto Map"
        Me.lnkAutoMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEmgFirstName
        '
        Me.lblEmgFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgFirstName.Location = New System.Drawing.Point(24, 9)
        Me.lblEmgFirstName.Name = "lblEmgFirstName"
        Me.lblEmgFirstName.Size = New System.Drawing.Size(86, 15)
        Me.lblEmgFirstName.TabIndex = 86
        Me.lblEmgFirstName.Text = "First Name"
        Me.lblEmgFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgLastName
        '
        Me.lblEmgLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgLastName.Location = New System.Drawing.Point(24, 36)
        Me.lblEmgLastName.Name = "lblEmgLastName"
        Me.lblEmgLastName.Size = New System.Drawing.Size(86, 15)
        Me.lblEmgLastName.TabIndex = 88
        Me.lblEmgLastName.Text = "Last Name"
        Me.lblEmgLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgAddress
        '
        Me.lblEmgAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgAddress.Location = New System.Drawing.Point(24, 63)
        Me.lblEmgAddress.Name = "lblEmgAddress"
        Me.lblEmgAddress.Size = New System.Drawing.Size(86, 15)
        Me.lblEmgAddress.TabIndex = 90
        Me.lblEmgAddress.Text = "Address"
        Me.lblEmgAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmergencyFirstname
        '
        Me.cboEmergencyFirstname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyFirstname.FormattingEnabled = True
        Me.cboEmergencyFirstname.Location = New System.Drawing.Point(116, 6)
        Me.cboEmergencyFirstname.Name = "cboEmergencyFirstname"
        Me.cboEmergencyFirstname.Size = New System.Drawing.Size(168, 21)
        Me.cboEmergencyFirstname.TabIndex = 91
        '
        'cboEmergencyLastname
        '
        Me.cboEmergencyLastname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyLastname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyLastname.FormattingEnabled = True
        Me.cboEmergencyLastname.Location = New System.Drawing.Point(116, 33)
        Me.cboEmergencyLastname.Name = "cboEmergencyLastname"
        Me.cboEmergencyLastname.Size = New System.Drawing.Size(168, 21)
        Me.cboEmergencyLastname.TabIndex = 92
        '
        'cboEmergencyAddress
        '
        Me.cboEmergencyAddress.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyAddress.FormattingEnabled = True
        Me.cboEmergencyAddress.Location = New System.Drawing.Point(116, 60)
        Me.cboEmergencyAddress.Name = "cboEmergencyAddress"
        Me.cboEmergencyAddress.Size = New System.Drawing.Size(168, 21)
        Me.cboEmergencyAddress.TabIndex = 93
        '
        'cboEmergencyCountry
        '
        Me.cboEmergencyCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyCountry.FormattingEnabled = True
        Me.cboEmergencyCountry.Location = New System.Drawing.Point(116, 87)
        Me.cboEmergencyCountry.Name = "cboEmergencyCountry"
        Me.cboEmergencyCountry.Size = New System.Drawing.Size(168, 21)
        Me.cboEmergencyCountry.TabIndex = 95
        '
        'lblEmgPostCountry
        '
        Me.lblEmgPostCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPostCountry.Location = New System.Drawing.Point(24, 90)
        Me.lblEmgPostCountry.Name = "lblEmgPostCountry"
        Me.lblEmgPostCountry.Size = New System.Drawing.Size(86, 15)
        Me.lblEmgPostCountry.TabIndex = 94
        Me.lblEmgPostCountry.Text = "Post Country"
        Me.lblEmgPostCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmergencyState
        '
        Me.cboEmergencyState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyState.FormattingEnabled = True
        Me.cboEmergencyState.Location = New System.Drawing.Point(116, 114)
        Me.cboEmergencyState.Name = "cboEmergencyState"
        Me.cboEmergencyState.Size = New System.Drawing.Size(168, 21)
        Me.cboEmergencyState.TabIndex = 97
        '
        'lblEmgPostTown
        '
        Me.lblEmgPostTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPostTown.Location = New System.Drawing.Point(24, 144)
        Me.lblEmgPostTown.Name = "lblEmgPostTown"
        Me.lblEmgPostTown.Size = New System.Drawing.Size(86, 15)
        Me.lblEmgPostTown.TabIndex = 98
        Me.lblEmgPostTown.Text = "Post Town"
        Me.lblEmgPostTown.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgState
        '
        Me.lblEmgState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgState.Location = New System.Drawing.Point(24, 117)
        Me.lblEmgState.Name = "lblEmgState"
        Me.lblEmgState.Size = New System.Drawing.Size(86, 15)
        Me.lblEmgState.TabIndex = 96
        Me.lblEmgState.Text = "State"
        Me.lblEmgState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmergencyTown
        '
        Me.cboEmergencyTown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyTown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyTown.FormattingEnabled = True
        Me.cboEmergencyTown.Location = New System.Drawing.Point(116, 141)
        Me.cboEmergencyTown.Name = "cboEmergencyTown"
        Me.cboEmergencyTown.Size = New System.Drawing.Size(168, 21)
        Me.cboEmergencyTown.TabIndex = 99
        '
        'lblEmgProvince
        '
        Me.lblEmgProvince.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgProvince.Location = New System.Drawing.Point(24, 198)
        Me.lblEmgProvince.Name = "lblEmgProvince"
        Me.lblEmgProvince.Size = New System.Drawing.Size(86, 15)
        Me.lblEmgProvince.TabIndex = 102
        Me.lblEmgProvince.Text = "Prov/Region"
        Me.lblEmgProvince.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmergencyPostCode
        '
        Me.cboEmergencyPostCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyPostCode.FormattingEnabled = True
        Me.cboEmergencyPostCode.Location = New System.Drawing.Point(116, 168)
        Me.cboEmergencyPostCode.Name = "cboEmergencyPostCode"
        Me.cboEmergencyPostCode.Size = New System.Drawing.Size(168, 21)
        Me.cboEmergencyPostCode.TabIndex = 101
        '
        'lblEmgPostCode
        '
        Me.lblEmgPostCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPostCode.Location = New System.Drawing.Point(24, 171)
        Me.lblEmgPostCode.Name = "lblEmgPostCode"
        Me.lblEmgPostCode.Size = New System.Drawing.Size(86, 15)
        Me.lblEmgPostCode.TabIndex = 100
        Me.lblEmgPostCode.Text = "Post Code"
        Me.lblEmgPostCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmergencyProvRegion
        '
        Me.cboEmergencyProvRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyProvRegion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyProvRegion.FormattingEnabled = True
        Me.cboEmergencyProvRegion.Location = New System.Drawing.Point(116, 195)
        Me.cboEmergencyProvRegion.Name = "cboEmergencyProvRegion"
        Me.cboEmergencyProvRegion.Size = New System.Drawing.Size(168, 21)
        Me.cboEmergencyProvRegion.TabIndex = 103
        '
        'lblEmgRoad
        '
        Me.lblEmgRoad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgRoad.Location = New System.Drawing.Point(290, 9)
        Me.lblEmgRoad.Name = "lblEmgRoad"
        Me.lblEmgRoad.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgRoad.TabIndex = 104
        Me.lblEmgRoad.Text = "Road/Street"
        Me.lblEmgRoad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgMobile
        '
        Me.lblEmgMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgMobile.Location = New System.Drawing.Point(290, 90)
        Me.lblEmgMobile.Name = "lblEmgMobile"
        Me.lblEmgMobile.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgMobile.TabIndex = 108
        Me.lblEmgMobile.Text = "Mobile"
        Me.lblEmgMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgPlotNo
        '
        Me.lblEmgPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgPlotNo.Location = New System.Drawing.Point(290, 63)
        Me.lblEmgPlotNo.Name = "lblEmgPlotNo"
        Me.lblEmgPlotNo.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgPlotNo.TabIndex = 107
        Me.lblEmgPlotNo.Text = "Plot No"
        Me.lblEmgPlotNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgEstate
        '
        Me.lblEmgEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgEstate.Location = New System.Drawing.Point(290, 36)
        Me.lblEmgEstate.Name = "lblEmgEstate"
        Me.lblEmgEstate.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgEstate.TabIndex = 106
        Me.lblEmgEstate.Text = "Estate"
        Me.lblEmgEstate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgTelNo
        '
        Me.lblEmgTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgTelNo.Location = New System.Drawing.Point(290, 144)
        Me.lblEmgTelNo.Name = "lblEmgTelNo"
        Me.lblEmgTelNo.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgTelNo.TabIndex = 111
        Me.lblEmgTelNo.Text = "Tel. No"
        Me.lblEmgTelNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgAltNo
        '
        Me.lblEmgAltNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgAltNo.Location = New System.Drawing.Point(290, 117)
        Me.lblEmgAltNo.Name = "lblEmgAltNo"
        Me.lblEmgAltNo.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgAltNo.TabIndex = 110
        Me.lblEmgAltNo.Text = "Alt. No"
        Me.lblEmgAltNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgFax
        '
        Me.lblEmgFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgFax.Location = New System.Drawing.Point(290, 171)
        Me.lblEmgFax.Name = "lblEmgFax"
        Me.lblEmgFax.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgFax.TabIndex = 112
        Me.lblEmgFax.Text = "Fax"
        Me.lblEmgFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmgEmail
        '
        Me.lblEmgEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmgEmail.Location = New System.Drawing.Point(290, 198)
        Me.lblEmgEmail.Name = "lblEmgEmail"
        Me.lblEmgEmail.Size = New System.Drawing.Size(77, 15)
        Me.lblEmgEmail.TabIndex = 114
        Me.lblEmgEmail.Text = "Email"
        Me.lblEmgEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmergencyRoadStreet
        '
        Me.cboEmergencyRoadStreet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyRoadStreet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyRoadStreet.FormattingEnabled = True
        Me.cboEmergencyRoadStreet.Location = New System.Drawing.Point(373, 6)
        Me.cboEmergencyRoadStreet.Name = "cboEmergencyRoadStreet"
        Me.cboEmergencyRoadStreet.Size = New System.Drawing.Size(169, 21)
        Me.cboEmergencyRoadStreet.TabIndex = 115
        '
        'cboEmergencyEstate
        '
        Me.cboEmergencyEstate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyEstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyEstate.FormattingEnabled = True
        Me.cboEmergencyEstate.Location = New System.Drawing.Point(373, 33)
        Me.cboEmergencyEstate.Name = "cboEmergencyEstate"
        Me.cboEmergencyEstate.Size = New System.Drawing.Size(169, 21)
        Me.cboEmergencyEstate.TabIndex = 116
        '
        'cboEmergencyPlotNumber
        '
        Me.cboEmergencyPlotNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyPlotNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyPlotNumber.FormattingEnabled = True
        Me.cboEmergencyPlotNumber.Location = New System.Drawing.Point(373, 60)
        Me.cboEmergencyPlotNumber.Name = "cboEmergencyPlotNumber"
        Me.cboEmergencyPlotNumber.Size = New System.Drawing.Size(169, 21)
        Me.cboEmergencyPlotNumber.TabIndex = 117
        '
        'cboEmergencyTelNo
        '
        Me.cboEmergencyTelNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyTelNo.FormattingEnabled = True
        Me.cboEmergencyTelNo.Location = New System.Drawing.Point(373, 141)
        Me.cboEmergencyTelNo.Name = "cboEmergencyTelNo"
        Me.cboEmergencyTelNo.Size = New System.Drawing.Size(169, 21)
        Me.cboEmergencyTelNo.TabIndex = 120
        '
        'cboEmergencyAltNo
        '
        Me.cboEmergencyAltNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyAltNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyAltNo.FormattingEnabled = True
        Me.cboEmergencyAltNo.Location = New System.Drawing.Point(373, 114)
        Me.cboEmergencyAltNo.Name = "cboEmergencyAltNo"
        Me.cboEmergencyAltNo.Size = New System.Drawing.Size(169, 21)
        Me.cboEmergencyAltNo.TabIndex = 119
        '
        'cboEmergencyMobile
        '
        Me.cboEmergencyMobile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyMobile.FormattingEnabled = True
        Me.cboEmergencyMobile.Location = New System.Drawing.Point(373, 87)
        Me.cboEmergencyMobile.Name = "cboEmergencyMobile"
        Me.cboEmergencyMobile.Size = New System.Drawing.Size(169, 21)
        Me.cboEmergencyMobile.TabIndex = 118
        '
        'cboEmergencyEmail
        '
        Me.cboEmergencyEmail.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyEmail.FormattingEnabled = True
        Me.cboEmergencyEmail.Location = New System.Drawing.Point(373, 195)
        Me.cboEmergencyEmail.Name = "cboEmergencyEmail"
        Me.cboEmergencyEmail.Size = New System.Drawing.Size(169, 21)
        Me.cboEmergencyEmail.TabIndex = 122
        '
        'cboEmergencyFaxNo
        '
        Me.cboEmergencyFaxNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmergencyFaxNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmergencyFaxNo.FormattingEnabled = True
        Me.cboEmergencyFaxNo.Location = New System.Drawing.Point(373, 168)
        Me.cboEmergencyFaxNo.Name = "cboEmergencyFaxNo"
        Me.cboEmergencyFaxNo.Size = New System.Drawing.Size(169, 21)
        Me.cboEmergencyFaxNo.TabIndex = 121
        '
        'lnkAllocationFormat
        '
        Me.lnkAllocationFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocationFormat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocationFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocationFormat.Location = New System.Drawing.Point(573, 341)
        Me.lnkAllocationFormat.Name = "lnkAllocationFormat"
        Me.lnkAllocationFormat.Size = New System.Drawing.Size(132, 22)
        Me.lnkAllocationFormat.TabIndex = 101
        Me.lnkAllocationFormat.TabStop = True
        Me.lnkAllocationFormat.Text = "Get Import Format"
        Me.lnkAllocationFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmAddressImportWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(717, 429)
        Me.Controls.Add(Me.eZeeWizImportImgs)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAddressImportWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Address Import Wizard"
        Me.cmsFilter.ResumeLayout(False)
        Me.eZeeWizImportImgs.ResumeLayout(False)
        Me.WizPageSelectFile.ResumeLayout(False)
        Me.WizPageSelectFile.PerformLayout()
        Me.WizPageImporting.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInfo.ResumeLayout(False)
        Me.WizPageMapping.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.pnlOtherAddress.ResumeLayout(False)
        Me.objspc1.Panel1.ResumeLayout(False)
        Me.objspc1.Panel2.ResumeLayout(False)
        Me.objspc1.ResumeLayout(False)
        Me.pnlEmergencyAddress.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents eZeeWizImportImgs As eZee.Common.eZeeWizard
    Friend WithEvents WizPageSelectFile As eZee.Common.eZeeWizardPage
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents WizPageImporting As eZee.Common.eZeeWizardPage
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents WizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents objlblSign6 As System.Windows.Forms.Label
    Friend WithEvents cboSelectType As System.Windows.Forms.ComboBox
    Friend WithEvents lblSelectType As System.Windows.Forms.Label
    Friend WithEvents lblVillage As System.Windows.Forms.Label
    Friend WithEvents lblProvince1 As System.Windows.Forms.Label
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents lblProvRegion As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents lblTelNo As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents lblTown1 As System.Windows.Forms.Label
    Friend WithEvents lblRoadStreet1 As System.Windows.Forms.Label
    Friend WithEvents lblAddress2 As System.Windows.Forms.Label
    Friend WithEvents lblPostTown As System.Windows.Forms.Label
    Friend WithEvents lblRoad As System.Windows.Forms.Label
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents lblPloteNo As System.Windows.Forms.Label
    Friend WithEvents lblChiefdom As System.Windows.Forms.Label
    Friend WithEvents lblEstate As System.Windows.Forms.Label
    Friend WithEvents lblAlternativeNo As System.Windows.Forms.Label
    Friend WithEvents lblMobile As System.Windows.Forms.Label
    Friend WithEvents lblPostCountry As System.Windows.Forms.Label
    Friend WithEvents lblPostcode As System.Windows.Forms.Label
    Friend WithEvents cboRoadStreet1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboEState As System.Windows.Forms.ComboBox
    Friend WithEvents cboRoadStreet As System.Windows.Forms.ComboBox
    Friend WithEvents cboProvRegion As System.Windows.Forms.ComboBox
    Friend WithEvents cboPostCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboPostTown As System.Windows.Forms.ComboBox
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents cboPostCountry As System.Windows.Forms.ComboBox
    Friend WithEvents cboAddress1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboAddress2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboProvRegion1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboFax As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmail As System.Windows.Forms.ComboBox
    Friend WithEvents cboAlternativeNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboPlotNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboTelNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboMobile As System.Windows.Forms.ComboBox
    Friend WithEvents cboTown1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboVillage As System.Windows.Forms.ComboBox
    Friend WithEvents cboChiefdom As System.Windows.Forms.ComboBox
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblNote As System.Windows.Forms.Label
    Friend WithEvents objspc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents pnlOtherAddress As System.Windows.Forms.Panel
    Friend WithEvents pnlEmergencyAddress As System.Windows.Forms.Panel
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
    Friend WithEvents cboEmergencyAddress As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmergencyLastname As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmergencyFirstname As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgFirstName As System.Windows.Forms.Label
    Friend WithEvents lblEmgLastName As System.Windows.Forms.Label
    Friend WithEvents lblEmgAddress As System.Windows.Forms.Label
    Friend WithEvents cboEmergencyCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgPostCountry As System.Windows.Forms.Label
    Friend WithEvents cboEmergencyState As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgPostTown As System.Windows.Forms.Label
    Friend WithEvents lblEmgState As System.Windows.Forms.Label
    Friend WithEvents cboEmergencyTown As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmergencyProvRegion As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgProvince As System.Windows.Forms.Label
    Friend WithEvents cboEmergencyPostCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgPostCode As System.Windows.Forms.Label
    Friend WithEvents lblEmgRoad As System.Windows.Forms.Label
    Friend WithEvents lblEmgMobile As System.Windows.Forms.Label
    Friend WithEvents lblEmgPlotNo As System.Windows.Forms.Label
    Friend WithEvents lblEmgEstate As System.Windows.Forms.Label
    Friend WithEvents lblEmgTelNo As System.Windows.Forms.Label
    Friend WithEvents lblEmgAltNo As System.Windows.Forms.Label
    Friend WithEvents lblEmgFax As System.Windows.Forms.Label
    Friend WithEvents cboEmergencyEmail As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmergencyFaxNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmergencyTelNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmergencyAltNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmergencyMobile As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmergencyPlotNumber As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmergencyEstate As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmergencyRoadStreet As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmgEmail As System.Windows.Forms.Label
    Friend WithEvents lnkAllocationFormat As System.Windows.Forms.LinkLabel
End Class
