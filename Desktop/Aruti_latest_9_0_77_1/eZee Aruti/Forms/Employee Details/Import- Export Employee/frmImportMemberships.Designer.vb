﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportMemberships
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportMemberships))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.WizImportMembership = New eZee.Common.eZeeWizard
        Me.wizPageData = New eZee.Common.eZeeWizardPage(Me.components)
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMembershipCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMembershipName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMembershipNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.wizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFiledMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.chkOverwrite = New System.Windows.Forms.CheckBox
        Me.chkOverwritePrevEDSlabHeads = New System.Windows.Forms.CheckBox
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.objlblSign7 = New System.Windows.Forms.Label
        Me.lblEffPeriod = New System.Windows.Forms.Label
        Me.cboEffectivePeriod = New System.Windows.Forms.ComboBox
        Me.lblExpiryDate = New System.Windows.Forms.Label
        Me.cboExpiryDate = New System.Windows.Forms.ComboBox
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.cboStartDate = New System.Windows.Forms.ComboBox
        Me.lblIssueDate = New System.Windows.Forms.Label
        Me.cboIssueDate = New System.Windows.Forms.ComboBox
        Me.objlblSign6 = New System.Windows.Forms.Label
        Me.lblMembershipNo = New System.Windows.Forms.Label
        Me.cboMembershipNo = New System.Windows.Forms.ComboBox
        Me.objlblSign4 = New System.Windows.Forms.Label
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboMembershipName = New System.Windows.Forms.ComboBox
        Me.objlblSign3 = New System.Windows.Forms.Label
        Me.lblMembershipCategory = New System.Windows.Forms.Label
        Me.cboMembershipCategory = New System.Windows.Forms.ComboBox
        Me.objlblSign2 = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.cboName = New System.Windows.Forms.ComboBox
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.wizPageFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lnkAllocationFormat = New System.Windows.Forms.LinkLabel
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.WizImportMembership.SuspendLayout()
        Me.wizPageData.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsFilter.SuspendLayout()
        Me.pnlInfo.SuspendLayout()
        Me.wizPageMapping.SuspendLayout()
        Me.gbFiledMapping.SuspendLayout()
        Me.wizPageFile.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(729, 405)
        Me.pnlMainInfo.TabIndex = 0
        '
        'WizImportMembership
        '
        Me.WizImportMembership.Controls.Add(Me.wizPageMapping)
        Me.WizImportMembership.Controls.Add(Me.wizPageFile)
        Me.WizImportMembership.Controls.Add(Me.wizPageData)
        Me.WizImportMembership.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WizImportMembership.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.WizImportMembership.Location = New System.Drawing.Point(0, 0)
        Me.WizImportMembership.Name = "WizImportMembership"
        Me.WizImportMembership.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wizPageFile, Me.wizPageMapping, Me.wizPageData})
        Me.WizImportMembership.SaveEnabled = True
        Me.WizImportMembership.SaveText = "Save && Finish"
        Me.WizImportMembership.SaveVisible = False
        Me.WizImportMembership.SetSaveIndexBeforeFinishIndex = False
        Me.WizImportMembership.Size = New System.Drawing.Size(729, 405)
        Me.WizImportMembership.TabIndex = 1
        Me.WizImportMembership.WelcomeImage = Nothing
        '
        'wizPageData
        '
        Me.wizPageData.Controls.Add(Me.dgData)
        Me.wizPageData.Controls.Add(Me.btnFilter)
        Me.wizPageData.Controls.Add(Me.pnlInfo)
        Me.wizPageData.Location = New System.Drawing.Point(0, 0)
        Me.wizPageData.Name = "wizPageData"
        Me.wizPageData.Size = New System.Drawing.Size(729, 357)
        Me.wizPageData.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizPageData.TabIndex = 9
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.colhMembershipCategory, Me.colhMembershipName, Me.colhMembershipNo, Me.colhStatus, Me.colhMessage, Me.objdgcolhEmployeeId, Me.objcolhstatus})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(705, 247)
        Me.dgData.TabIndex = 20
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 30
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhMembershipCategory
        '
        Me.colhMembershipCategory.HeaderText = "Membership Category"
        Me.colhMembershipCategory.Name = "colhMembershipCategory"
        Me.colhMembershipCategory.ReadOnly = True
        '
        'colhMembershipName
        '
        Me.colhMembershipName.HeaderText = "Membership Name"
        Me.colhMembershipName.Name = "colhMembershipName"
        Me.colhMembershipName.ReadOnly = True
        '
        'colhMembershipNo
        '
        Me.colhMembershipNo.HeaderText = "Membership No"
        Me.colhMembershipNo.Name = "colhMembershipNo"
        Me.colhMembershipNo.ReadOnly = True
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 175
        '
        'objdgcolhEmployeeId
        '
        Me.objdgcolhEmployeeId.HeaderText = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.Name = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.ReadOnly = True
        Me.objdgcolhEmployeeId.Visible = False
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 322)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(107, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 23
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(705, 51)
        Me.pnlInfo.TabIndex = 3
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(583, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(469, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(583, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(515, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(628, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(469, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(628, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(515, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'wizPageMapping
        '
        Me.wizPageMapping.Controls.Add(Me.gbFiledMapping)
        Me.wizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.wizPageMapping.Name = "wizPageMapping"
        Me.wizPageMapping.Size = New System.Drawing.Size(729, 357)
        Me.wizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageMapping.TabIndex = 8
        '
        'gbFiledMapping
        '
        Me.gbFiledMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFiledMapping.Checked = False
        Me.gbFiledMapping.CollapseAllExceptThis = False
        Me.gbFiledMapping.CollapsedHoverImage = Nothing
        Me.gbFiledMapping.CollapsedNormalImage = Nothing
        Me.gbFiledMapping.CollapsedPressedImage = Nothing
        Me.gbFiledMapping.CollapseOnLoad = False
        Me.gbFiledMapping.Controls.Add(Me.lnkAutoMap)
        Me.gbFiledMapping.Controls.Add(Me.chkOverwrite)
        Me.gbFiledMapping.Controls.Add(Me.chkOverwritePrevEDSlabHeads)
        Me.gbFiledMapping.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign7)
        Me.gbFiledMapping.Controls.Add(Me.lblEffPeriod)
        Me.gbFiledMapping.Controls.Add(Me.cboEffectivePeriod)
        Me.gbFiledMapping.Controls.Add(Me.lblExpiryDate)
        Me.gbFiledMapping.Controls.Add(Me.cboExpiryDate)
        Me.gbFiledMapping.Controls.Add(Me.lblStartDate)
        Me.gbFiledMapping.Controls.Add(Me.cboStartDate)
        Me.gbFiledMapping.Controls.Add(Me.lblIssueDate)
        Me.gbFiledMapping.Controls.Add(Me.cboIssueDate)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign6)
        Me.gbFiledMapping.Controls.Add(Me.lblMembershipNo)
        Me.gbFiledMapping.Controls.Add(Me.cboMembershipNo)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign4)
        Me.gbFiledMapping.Controls.Add(Me.lblMembership)
        Me.gbFiledMapping.Controls.Add(Me.cboMembershipName)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign3)
        Me.gbFiledMapping.Controls.Add(Me.lblMembershipCategory)
        Me.gbFiledMapping.Controls.Add(Me.cboMembershipCategory)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign2)
        Me.gbFiledMapping.Controls.Add(Me.lblName)
        Me.gbFiledMapping.Controls.Add(Me.cboName)
        Me.gbFiledMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign1)
        Me.gbFiledMapping.Controls.Add(Me.lblCaption)
        Me.gbFiledMapping.ExpandedHoverImage = Nothing
        Me.gbFiledMapping.ExpandedNormalImage = Nothing
        Me.gbFiledMapping.ExpandedPressedImage = Nothing
        Me.gbFiledMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFiledMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFiledMapping.HeaderHeight = 25
        Me.gbFiledMapping.HeaderMessage = ""
        Me.gbFiledMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFiledMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFiledMapping.HeightOnCollapse = 0
        Me.gbFiledMapping.LeftTextSpace = 0
        Me.gbFiledMapping.Location = New System.Drawing.Point(165, 0)
        Me.gbFiledMapping.Name = "gbFiledMapping"
        Me.gbFiledMapping.OpenHeight = 300
        Me.gbFiledMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFiledMapping.ShowBorder = True
        Me.gbFiledMapping.ShowCheckBox = False
        Me.gbFiledMapping.ShowCollapseButton = False
        Me.gbFiledMapping.ShowDefaultBorderColor = True
        Me.gbFiledMapping.ShowDownButton = False
        Me.gbFiledMapping.ShowHeader = True
        Me.gbFiledMapping.Size = New System.Drawing.Size(564, 357)
        Me.gbFiledMapping.TabIndex = 0
        Me.gbFiledMapping.Temp = 0
        Me.gbFiledMapping.Text = "Field Mapping"
        Me.gbFiledMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAutoMap.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAutoMap.Location = New System.Drawing.Point(401, 327)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(113, 20)
        Me.lnkAutoMap.TabIndex = 110
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "Auto Map"
        Me.lnkAutoMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkOverwrite
        '
        Me.chkOverwrite.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwrite.Location = New System.Drawing.Point(140, 330)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.Size = New System.Drawing.Size(198, 17)
        Me.chkOverwrite.TabIndex = 90
        Me.chkOverwrite.Text = "Overwrite Heads"
        Me.chkOverwrite.UseVisualStyleBackColor = True
        '
        'chkOverwritePrevEDSlabHeads
        '
        Me.chkOverwritePrevEDSlabHeads.Enabled = False
        Me.chkOverwritePrevEDSlabHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwritePrevEDSlabHeads.Location = New System.Drawing.Point(140, 307)
        Me.chkOverwritePrevEDSlabHeads.Name = "chkOverwritePrevEDSlabHeads"
        Me.chkOverwritePrevEDSlabHeads.Size = New System.Drawing.Size(198, 17)
        Me.chkOverwritePrevEDSlabHeads.TabIndex = 89
        Me.chkOverwritePrevEDSlabHeads.Text = "Overwrite Previous ED Slab heads"
        Me.chkOverwritePrevEDSlabHeads.UseVisualStyleBackColor = True
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(140, 284)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(198, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 88
        Me.chkCopyPreviousEDSlab.Text = "Copy Previous ED Slab"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'objlblSign7
        '
        Me.objlblSign7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign7.ForeColor = System.Drawing.Color.Red
        Me.objlblSign7.Location = New System.Drawing.Point(114, 178)
        Me.objlblSign7.Name = "objlblSign7"
        Me.objlblSign7.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign7.TabIndex = 91
        Me.objlblSign7.Text = "*"
        Me.objlblSign7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblEffPeriod
        '
        Me.lblEffPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffPeriod.Location = New System.Drawing.Point(137, 178)
        Me.lblEffPeriod.Name = "lblEffPeriod"
        Me.lblEffPeriod.Size = New System.Drawing.Size(119, 17)
        Me.lblEffPeriod.TabIndex = 92
        Me.lblEffPeriod.Text = "Effective Period"
        Me.lblEffPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEffectivePeriod
        '
        Me.cboEffectivePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEffectivePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEffectivePeriod.FormattingEnabled = True
        Me.cboEffectivePeriod.Location = New System.Drawing.Point(262, 176)
        Me.cboEffectivePeriod.Name = "cboEffectivePeriod"
        Me.cboEffectivePeriod.Size = New System.Drawing.Size(198, 21)
        Me.cboEffectivePeriod.TabIndex = 84
        '
        'lblExpiryDate
        '
        Me.lblExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpiryDate.Location = New System.Drawing.Point(137, 259)
        Me.lblExpiryDate.Name = "lblExpiryDate"
        Me.lblExpiryDate.Size = New System.Drawing.Size(119, 17)
        Me.lblExpiryDate.TabIndex = 88
        Me.lblExpiryDate.Text = "Expiry Date"
        Me.lblExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExpiryDate
        '
        Me.cboExpiryDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpiryDate.FormattingEnabled = True
        Me.cboExpiryDate.Location = New System.Drawing.Point(262, 257)
        Me.cboExpiryDate.Name = "cboExpiryDate"
        Me.cboExpiryDate.Size = New System.Drawing.Size(198, 21)
        Me.cboExpiryDate.TabIndex = 87
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(137, 232)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(119, 17)
        Me.lblStartDate.TabIndex = 86
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStartDate
        '
        Me.cboStartDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStartDate.FormattingEnabled = True
        Me.cboStartDate.Location = New System.Drawing.Point(262, 230)
        Me.cboStartDate.Name = "cboStartDate"
        Me.cboStartDate.Size = New System.Drawing.Size(198, 21)
        Me.cboStartDate.TabIndex = 86
        '
        'lblIssueDate
        '
        Me.lblIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIssueDate.Location = New System.Drawing.Point(137, 205)
        Me.lblIssueDate.Name = "lblIssueDate"
        Me.lblIssueDate.Size = New System.Drawing.Size(119, 17)
        Me.lblIssueDate.TabIndex = 84
        Me.lblIssueDate.Text = "Issue Date"
        Me.lblIssueDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIssueDate
        '
        Me.cboIssueDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIssueDate.FormattingEnabled = True
        Me.cboIssueDate.Location = New System.Drawing.Point(262, 203)
        Me.cboIssueDate.Name = "cboIssueDate"
        Me.cboIssueDate.Size = New System.Drawing.Size(198, 21)
        Me.cboIssueDate.TabIndex = 85
        '
        'objlblSign6
        '
        Me.objlblSign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign6.ForeColor = System.Drawing.Color.Red
        Me.objlblSign6.Location = New System.Drawing.Point(114, 151)
        Me.objlblSign6.Name = "objlblSign6"
        Me.objlblSign6.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign6.TabIndex = 81
        Me.objlblSign6.Text = "*"
        Me.objlblSign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblMembershipNo
        '
        Me.lblMembershipNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembershipNo.Location = New System.Drawing.Point(137, 151)
        Me.lblMembershipNo.Name = "lblMembershipNo"
        Me.lblMembershipNo.Size = New System.Drawing.Size(119, 17)
        Me.lblMembershipNo.TabIndex = 82
        Me.lblMembershipNo.Text = "Membership No."
        Me.lblMembershipNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembershipNo
        '
        Me.cboMembershipNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembershipNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembershipNo.FormattingEnabled = True
        Me.cboMembershipNo.Location = New System.Drawing.Point(262, 149)
        Me.cboMembershipNo.Name = "cboMembershipNo"
        Me.cboMembershipNo.Size = New System.Drawing.Size(198, 21)
        Me.cboMembershipNo.TabIndex = 83
        '
        'objlblSign4
        '
        Me.objlblSign4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign4.ForeColor = System.Drawing.Color.Red
        Me.objlblSign4.Location = New System.Drawing.Point(114, 124)
        Me.objlblSign4.Name = "objlblSign4"
        Me.objlblSign4.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign4.TabIndex = 78
        Me.objlblSign4.Text = "*"
        Me.objlblSign4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(137, 124)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(119, 17)
        Me.lblMembership.TabIndex = 79
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembershipName
        '
        Me.cboMembershipName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembershipName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembershipName.FormattingEnabled = True
        Me.cboMembershipName.Location = New System.Drawing.Point(262, 122)
        Me.cboMembershipName.Name = "cboMembershipName"
        Me.cboMembershipName.Size = New System.Drawing.Size(198, 21)
        Me.cboMembershipName.TabIndex = 80
        '
        'objlblSign3
        '
        Me.objlblSign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign3.ForeColor = System.Drawing.Color.Red
        Me.objlblSign3.Location = New System.Drawing.Point(114, 97)
        Me.objlblSign3.Name = "objlblSign3"
        Me.objlblSign3.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign3.TabIndex = 75
        Me.objlblSign3.Text = "*"
        Me.objlblSign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblMembershipCategory
        '
        Me.lblMembershipCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembershipCategory.Location = New System.Drawing.Point(137, 97)
        Me.lblMembershipCategory.Name = "lblMembershipCategory"
        Me.lblMembershipCategory.Size = New System.Drawing.Size(119, 17)
        Me.lblMembershipCategory.TabIndex = 76
        Me.lblMembershipCategory.Text = "Membership Category"
        Me.lblMembershipCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembershipCategory
        '
        Me.cboMembershipCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembershipCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembershipCategory.FormattingEnabled = True
        Me.cboMembershipCategory.Location = New System.Drawing.Point(262, 95)
        Me.cboMembershipCategory.Name = "cboMembershipCategory"
        Me.cboMembershipCategory.Size = New System.Drawing.Size(198, 21)
        Me.cboMembershipCategory.TabIndex = 77
        '
        'objlblSign2
        '
        Me.objlblSign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign2.ForeColor = System.Drawing.Color.Red
        Me.objlblSign2.Location = New System.Drawing.Point(114, 70)
        Me.objlblSign2.Name = "objlblSign2"
        Me.objlblSign2.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign2.TabIndex = 72
        Me.objlblSign2.Text = "*"
        Me.objlblSign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(137, 70)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(119, 17)
        Me.lblName.TabIndex = 73
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboName
        '
        Me.cboName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboName.FormattingEnabled = True
        Me.cboName.Location = New System.Drawing.Point(262, 68)
        Me.cboName.Name = "cboName"
        Me.cboName.Size = New System.Drawing.Size(198, 21)
        Me.cboName.TabIndex = 74
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(262, 41)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(198, 21)
        Me.cboEmployeeCode.TabIndex = 71
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(137, 43)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(119, 17)
        Me.lblEmployeeCode.TabIndex = 70
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(114, 43)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign1.TabIndex = 69
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(327, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(234, 19)
        Me.lblCaption.TabIndex = 68
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'wizPageFile
        '
        Me.wizPageFile.Controls.Add(Me.lnkAllocationFormat)
        Me.wizPageFile.Controls.Add(Me.btnOpenFile)
        Me.wizPageFile.Controls.Add(Me.txtFilePath)
        Me.wizPageFile.Controls.Add(Me.lblSelectfile)
        Me.wizPageFile.Controls.Add(Me.lblTitle)
        Me.wizPageFile.Location = New System.Drawing.Point(0, 0)
        Me.wizPageFile.Name = "wizPageFile"
        Me.wizPageFile.Size = New System.Drawing.Size(729, 357)
        Me.wizPageFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageFile.TabIndex = 7
        '
        'lnkAllocationFormat
        '
        Me.lnkAllocationFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocationFormat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocationFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocationFormat.Location = New System.Drawing.Point(585, 314)
        Me.lnkAllocationFormat.Name = "lnkAllocationFormat"
        Me.lnkAllocationFormat.Size = New System.Drawing.Size(132, 22)
        Me.lnkAllocationFormat.TabIndex = 103
        Me.lnkAllocationFormat.TabStop = True
        Me.lnkAllocationFormat.Text = "Get Import Format"
        Me.lnkAllocationFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lnkAllocationFormat.Visible = False
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(689, 164)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(28, 21)
        Me.btnOpenFile.TabIndex = 21
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(176, 164)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(507, 21)
        Me.txtFilePath.TabIndex = 20
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(177, 141)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(143, 17)
        Me.lblSelectfile.TabIndex = 19
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(172, 21)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(412, 23)
        Me.lblTitle.TabIndex = 18
        Me.lblTitle.Text = "Employee Membership Import Wizard"
        '
        'frmImportMemberships
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(729, 405)
        Me.Controls.Add(Me.WizImportMembership)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportMemberships"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Employee Memberships"
        Me.WizImportMembership.ResumeLayout(False)
        Me.wizPageData.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsFilter.ResumeLayout(False)
        Me.pnlInfo.ResumeLayout(False)
        Me.wizPageMapping.ResumeLayout(False)
        Me.gbFiledMapping.ResumeLayout(False)
        Me.wizPageFile.ResumeLayout(False)
        Me.wizPageFile.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents WizImportMembership As eZee.Common.eZeeWizard
    Friend WithEvents wizPageData As eZee.Common.eZeeWizardPage
    Friend WithEvents wizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents wizPageFile As eZee.Common.eZeeWizardPage
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents gbFiledMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents objlblSign2 As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents cboName As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign6 As System.Windows.Forms.Label
    Friend WithEvents lblMembershipNo As System.Windows.Forms.Label
    Friend WithEvents cboMembershipNo As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign4 As System.Windows.Forms.Label
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembershipName As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign3 As System.Windows.Forms.Label
    Friend WithEvents lblMembershipCategory As System.Windows.Forms.Label
    Friend WithEvents cboMembershipCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpiryDate As System.Windows.Forms.Label
    Friend WithEvents cboExpiryDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents cboStartDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblIssueDate As System.Windows.Forms.Label
    Friend WithEvents cboIssueDate As System.Windows.Forms.ComboBox
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objlblSign7 As System.Windows.Forms.Label
    Friend WithEvents lblEffPeriod As System.Windows.Forms.Label
    Friend WithEvents cboEffectivePeriod As System.Windows.Forms.ComboBox
    Friend WithEvents chkOverwrite As System.Windows.Forms.CheckBox
    Friend WithEvents chkOverwritePrevEDSlabHeads As System.Windows.Forms.CheckBox
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
    Friend WithEvents lnkAllocationFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMembershipCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMembershipName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMembershipNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
