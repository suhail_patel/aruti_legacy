﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportIdentities
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportIdentities))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.WizImportIdentity = New eZee.Common.eZeeWizard
        Me.wizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFiledMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.LblDLClass = New System.Windows.Forms.Label
        Me.cboDLClass = New System.Windows.Forms.ComboBox
        Me.LblPlaceofIssue = New System.Windows.Forms.Label
        Me.cboPlaceofIssue = New System.Windows.Forms.ComboBox
        Me.LblSerialNo = New System.Windows.Forms.Label
        Me.lblExpiryDate = New System.Windows.Forms.Label
        Me.cboExpiryDate = New System.Windows.Forms.ComboBox
        Me.lblIssueDate = New System.Windows.Forms.Label
        Me.cboIssueDate = New System.Windows.Forms.ComboBox
        Me.cboSerialNo = New System.Windows.Forms.ComboBox
        Me.LblCountry = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.objlblSign6 = New System.Windows.Forms.Label
        Me.lblIsDefault = New System.Windows.Forms.Label
        Me.cboIsDefault = New System.Windows.Forms.ComboBox
        Me.objlblSign3 = New System.Windows.Forms.Label
        Me.lblIdentityNo = New System.Windows.Forms.Label
        Me.cboIdentityNo = New System.Windows.Forms.ComboBox
        Me.objlblSign2 = New System.Windows.Forms.Label
        Me.lblIdentityType = New System.Windows.Forms.Label
        Me.cboIdentityType = New System.Windows.Forms.ComboBox
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.wizPageFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lnkAllocationFormat = New System.Windows.Forms.LinkLabel
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.wizPageData = New eZee.Common.eZeeWizardPage(Me.components)
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIdentityType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhIdentityNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.WizImportIdentity.SuspendLayout()
        Me.wizPageMapping.SuspendLayout()
        Me.gbFiledMapping.SuspendLayout()
        Me.wizPageFile.SuspendLayout()
        Me.wizPageData.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsFilter.SuspendLayout()
        Me.pnlInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(709, 405)
        Me.pnlMainInfo.TabIndex = 0
        '
        'WizImportIdentity
        '
        Me.WizImportIdentity.Controls.Add(Me.wizPageFile)
        Me.WizImportIdentity.Controls.Add(Me.wizPageMapping)
        Me.WizImportIdentity.Controls.Add(Me.wizPageData)
        Me.WizImportIdentity.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WizImportIdentity.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.WizImportIdentity.Location = New System.Drawing.Point(0, 0)
        Me.WizImportIdentity.Name = "WizImportIdentity"
        Me.WizImportIdentity.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.wizPageFile, Me.wizPageMapping, Me.wizPageData})
        Me.WizImportIdentity.SaveEnabled = True
        Me.WizImportIdentity.SaveText = "Save && Finish"
        Me.WizImportIdentity.SaveVisible = False
        Me.WizImportIdentity.SetSaveIndexBeforeFinishIndex = False
        Me.WizImportIdentity.Size = New System.Drawing.Size(709, 405)
        Me.WizImportIdentity.TabIndex = 1
        Me.WizImportIdentity.WelcomeImage = Nothing
        '
        'wizPageMapping
        '
        Me.wizPageMapping.Controls.Add(Me.gbFiledMapping)
        Me.wizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.wizPageMapping.Name = "wizPageMapping"
        Me.wizPageMapping.Size = New System.Drawing.Size(709, 357)
        Me.wizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageMapping.TabIndex = 8
        '
        'gbFiledMapping
        '
        Me.gbFiledMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFiledMapping.Checked = False
        Me.gbFiledMapping.CollapseAllExceptThis = False
        Me.gbFiledMapping.CollapsedHoverImage = Nothing
        Me.gbFiledMapping.CollapsedNormalImage = Nothing
        Me.gbFiledMapping.CollapsedPressedImage = Nothing
        Me.gbFiledMapping.CollapseOnLoad = False
        Me.gbFiledMapping.Controls.Add(Me.lnkAutoMap)
        Me.gbFiledMapping.Controls.Add(Me.LblDLClass)
        Me.gbFiledMapping.Controls.Add(Me.cboDLClass)
        Me.gbFiledMapping.Controls.Add(Me.LblPlaceofIssue)
        Me.gbFiledMapping.Controls.Add(Me.cboPlaceofIssue)
        Me.gbFiledMapping.Controls.Add(Me.LblSerialNo)
        Me.gbFiledMapping.Controls.Add(Me.lblExpiryDate)
        Me.gbFiledMapping.Controls.Add(Me.cboExpiryDate)
        Me.gbFiledMapping.Controls.Add(Me.lblIssueDate)
        Me.gbFiledMapping.Controls.Add(Me.cboIssueDate)
        Me.gbFiledMapping.Controls.Add(Me.cboSerialNo)
        Me.gbFiledMapping.Controls.Add(Me.LblCountry)
        Me.gbFiledMapping.Controls.Add(Me.cboCountry)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign6)
        Me.gbFiledMapping.Controls.Add(Me.lblIsDefault)
        Me.gbFiledMapping.Controls.Add(Me.cboIsDefault)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign3)
        Me.gbFiledMapping.Controls.Add(Me.lblIdentityNo)
        Me.gbFiledMapping.Controls.Add(Me.cboIdentityNo)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign2)
        Me.gbFiledMapping.Controls.Add(Me.lblIdentityType)
        Me.gbFiledMapping.Controls.Add(Me.cboIdentityType)
        Me.gbFiledMapping.Controls.Add(Me.cboEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFiledMapping.Controls.Add(Me.objlblSign1)
        Me.gbFiledMapping.Controls.Add(Me.lblCaption)
        Me.gbFiledMapping.ExpandedHoverImage = Nothing
        Me.gbFiledMapping.ExpandedNormalImage = Nothing
        Me.gbFiledMapping.ExpandedPressedImage = Nothing
        Me.gbFiledMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFiledMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFiledMapping.HeaderHeight = 25
        Me.gbFiledMapping.HeaderMessage = ""
        Me.gbFiledMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFiledMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFiledMapping.HeightOnCollapse = 0
        Me.gbFiledMapping.LeftTextSpace = 0
        Me.gbFiledMapping.Location = New System.Drawing.Point(165, 0)
        Me.gbFiledMapping.Name = "gbFiledMapping"
        Me.gbFiledMapping.OpenHeight = 300
        Me.gbFiledMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFiledMapping.ShowBorder = True
        Me.gbFiledMapping.ShowCheckBox = False
        Me.gbFiledMapping.ShowCollapseButton = False
        Me.gbFiledMapping.ShowDefaultBorderColor = True
        Me.gbFiledMapping.ShowDownButton = False
        Me.gbFiledMapping.ShowHeader = True
        Me.gbFiledMapping.Size = New System.Drawing.Size(544, 357)
        Me.gbFiledMapping.TabIndex = 0
        Me.gbFiledMapping.Temp = 0
        Me.gbFiledMapping.Text = "Field Mapping"
        Me.gbFiledMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAutoMap.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAutoMap.Location = New System.Drawing.Point(390, 325)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(113, 20)
        Me.lnkAutoMap.TabIndex = 111
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "Auto Map"
        Me.lnkAutoMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'LblDLClass
        '
        Me.LblDLClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDLClass.Location = New System.Drawing.Point(126, 233)
        Me.LblDLClass.Name = "LblDLClass"
        Me.LblDLClass.Size = New System.Drawing.Size(119, 17)
        Me.LblDLClass.TabIndex = 98
        Me.LblDLClass.Text = "DL Class"
        Me.LblDLClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDLClass
        '
        Me.cboDLClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDLClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDLClass.FormattingEnabled = True
        Me.cboDLClass.Location = New System.Drawing.Point(251, 231)
        Me.cboDLClass.Name = "cboDLClass"
        Me.cboDLClass.Size = New System.Drawing.Size(198, 21)
        Me.cboDLClass.TabIndex = 99
        '
        'LblPlaceofIssue
        '
        Me.LblPlaceofIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPlaceofIssue.Location = New System.Drawing.Point(126, 206)
        Me.LblPlaceofIssue.Name = "LblPlaceofIssue"
        Me.LblPlaceofIssue.Size = New System.Drawing.Size(119, 17)
        Me.LblPlaceofIssue.TabIndex = 96
        Me.LblPlaceofIssue.Text = "Place of Issue"
        Me.LblPlaceofIssue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPlaceofIssue
        '
        Me.cboPlaceofIssue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPlaceofIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPlaceofIssue.FormattingEnabled = True
        Me.cboPlaceofIssue.Location = New System.Drawing.Point(251, 204)
        Me.cboPlaceofIssue.Name = "cboPlaceofIssue"
        Me.cboPlaceofIssue.Size = New System.Drawing.Size(198, 21)
        Me.cboPlaceofIssue.TabIndex = 97
        '
        'LblSerialNo
        '
        Me.LblSerialNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSerialNo.Location = New System.Drawing.Point(126, 179)
        Me.LblSerialNo.Name = "LblSerialNo"
        Me.LblSerialNo.Size = New System.Drawing.Size(119, 17)
        Me.LblSerialNo.TabIndex = 88
        Me.LblSerialNo.Text = "Serial No"
        Me.LblSerialNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblExpiryDate
        '
        Me.lblExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpiryDate.Location = New System.Drawing.Point(126, 286)
        Me.lblExpiryDate.Name = "lblExpiryDate"
        Me.lblExpiryDate.Size = New System.Drawing.Size(119, 17)
        Me.lblExpiryDate.TabIndex = 94
        Me.lblExpiryDate.Text = "Expiry Date"
        Me.lblExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExpiryDate
        '
        Me.cboExpiryDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExpiryDate.FormattingEnabled = True
        Me.cboExpiryDate.Location = New System.Drawing.Point(251, 284)
        Me.cboExpiryDate.Name = "cboExpiryDate"
        Me.cboExpiryDate.Size = New System.Drawing.Size(198, 21)
        Me.cboExpiryDate.TabIndex = 95
        '
        'lblIssueDate
        '
        Me.lblIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIssueDate.Location = New System.Drawing.Point(126, 259)
        Me.lblIssueDate.Name = "lblIssueDate"
        Me.lblIssueDate.Size = New System.Drawing.Size(119, 17)
        Me.lblIssueDate.TabIndex = 90
        Me.lblIssueDate.Text = "Issue Date"
        Me.lblIssueDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIssueDate
        '
        Me.cboIssueDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIssueDate.FormattingEnabled = True
        Me.cboIssueDate.Location = New System.Drawing.Point(251, 257)
        Me.cboIssueDate.Name = "cboIssueDate"
        Me.cboIssueDate.Size = New System.Drawing.Size(198, 21)
        Me.cboIssueDate.TabIndex = 91
        '
        'cboSerialNo
        '
        Me.cboSerialNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSerialNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSerialNo.FormattingEnabled = True
        Me.cboSerialNo.Location = New System.Drawing.Point(251, 177)
        Me.cboSerialNo.Name = "cboSerialNo"
        Me.cboSerialNo.Size = New System.Drawing.Size(198, 21)
        Me.cboSerialNo.TabIndex = 89
        '
        'LblCountry
        '
        Me.LblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCountry.Location = New System.Drawing.Point(126, 152)
        Me.LblCountry.Name = "LblCountry"
        Me.LblCountry.Size = New System.Drawing.Size(119, 17)
        Me.LblCountry.TabIndex = 86
        Me.LblCountry.Text = "Country"
        Me.LblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(251, 150)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(198, 21)
        Me.cboCountry.TabIndex = 87
        '
        'objlblSign6
        '
        Me.objlblSign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign6.ForeColor = System.Drawing.Color.Red
        Me.objlblSign6.Location = New System.Drawing.Point(103, 125)
        Me.objlblSign6.Name = "objlblSign6"
        Me.objlblSign6.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign6.TabIndex = 81
        Me.objlblSign6.Text = "*"
        Me.objlblSign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblIsDefault
        '
        Me.lblIsDefault.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIsDefault.Location = New System.Drawing.Point(126, 125)
        Me.lblIsDefault.Name = "lblIsDefault"
        Me.lblIsDefault.Size = New System.Drawing.Size(119, 17)
        Me.lblIsDefault.TabIndex = 82
        Me.lblIsDefault.Text = "Is Default"
        Me.lblIsDefault.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIsDefault
        '
        Me.cboIsDefault.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIsDefault.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIsDefault.FormattingEnabled = True
        Me.cboIsDefault.Location = New System.Drawing.Point(251, 123)
        Me.cboIsDefault.Name = "cboIsDefault"
        Me.cboIsDefault.Size = New System.Drawing.Size(198, 21)
        Me.cboIsDefault.TabIndex = 83
        '
        'objlblSign3
        '
        Me.objlblSign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign3.ForeColor = System.Drawing.Color.Red
        Me.objlblSign3.Location = New System.Drawing.Point(103, 97)
        Me.objlblSign3.Name = "objlblSign3"
        Me.objlblSign3.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign3.TabIndex = 75
        Me.objlblSign3.Text = "*"
        Me.objlblSign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblIdentityNo
        '
        Me.lblIdentityNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdentityNo.Location = New System.Drawing.Point(126, 97)
        Me.lblIdentityNo.Name = "lblIdentityNo"
        Me.lblIdentityNo.Size = New System.Drawing.Size(119, 17)
        Me.lblIdentityNo.TabIndex = 76
        Me.lblIdentityNo.Text = "Identity No"
        Me.lblIdentityNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIdentityNo
        '
        Me.cboIdentityNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIdentityNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIdentityNo.FormattingEnabled = True
        Me.cboIdentityNo.Location = New System.Drawing.Point(251, 95)
        Me.cboIdentityNo.Name = "cboIdentityNo"
        Me.cboIdentityNo.Size = New System.Drawing.Size(198, 21)
        Me.cboIdentityNo.TabIndex = 77
        '
        'objlblSign2
        '
        Me.objlblSign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign2.ForeColor = System.Drawing.Color.Red
        Me.objlblSign2.Location = New System.Drawing.Point(103, 70)
        Me.objlblSign2.Name = "objlblSign2"
        Me.objlblSign2.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign2.TabIndex = 72
        Me.objlblSign2.Text = "*"
        Me.objlblSign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblIdentityType
        '
        Me.lblIdentityType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdentityType.Location = New System.Drawing.Point(126, 70)
        Me.lblIdentityType.Name = "lblIdentityType"
        Me.lblIdentityType.Size = New System.Drawing.Size(119, 17)
        Me.lblIdentityType.TabIndex = 73
        Me.lblIdentityType.Text = "Identity Type"
        Me.lblIdentityType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIdentityType
        '
        Me.cboIdentityType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIdentityType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIdentityType.FormattingEnabled = True
        Me.cboIdentityType.Location = New System.Drawing.Point(251, 68)
        Me.cboIdentityType.Name = "cboIdentityType"
        Me.cboIdentityType.Size = New System.Drawing.Size(198, 21)
        Me.cboIdentityType.TabIndex = 74
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(251, 41)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(198, 21)
        Me.cboEmployeeCode.TabIndex = 71
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(126, 43)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(119, 17)
        Me.lblEmployeeCode.TabIndex = 70
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(103, 43)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(14, 17)
        Me.objlblSign1.TabIndex = 69
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(307, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(234, 19)
        Me.lblCaption.TabIndex = 68
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'wizPageFile
        '
        Me.wizPageFile.Controls.Add(Me.lnkAllocationFormat)
        Me.wizPageFile.Controls.Add(Me.btnOpenFile)
        Me.wizPageFile.Controls.Add(Me.txtFilePath)
        Me.wizPageFile.Controls.Add(Me.lblSelectfile)
        Me.wizPageFile.Controls.Add(Me.lblTitle)
        Me.wizPageFile.Location = New System.Drawing.Point(0, 0)
        Me.wizPageFile.Name = "wizPageFile"
        Me.wizPageFile.Size = New System.Drawing.Size(709, 357)
        Me.wizPageFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.wizPageFile.TabIndex = 7
        '
        'lnkAllocationFormat
        '
        Me.lnkAllocationFormat.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocationFormat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocationFormat.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocationFormat.Location = New System.Drawing.Point(565, 320)
        Me.lnkAllocationFormat.Name = "lnkAllocationFormat"
        Me.lnkAllocationFormat.Size = New System.Drawing.Size(132, 22)
        Me.lnkAllocationFormat.TabIndex = 104
        Me.lnkAllocationFormat.TabStop = True
        Me.lnkAllocationFormat.Text = "Get Import Format"
        Me.lnkAllocationFormat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(670, 164)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(27, 20)
        Me.btnOpenFile.TabIndex = 21
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(176, 164)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(488, 21)
        Me.txtFilePath.TabIndex = 20
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(177, 141)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(143, 17)
        Me.lblSelectfile.TabIndex = 19
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(172, 21)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(412, 23)
        Me.lblTitle.TabIndex = 18
        Me.lblTitle.Text = "Employee Identity Import Wizard"
        '
        'wizPageData
        '
        Me.wizPageData.Controls.Add(Me.dgData)
        Me.wizPageData.Controls.Add(Me.btnFilter)
        Me.wizPageData.Controls.Add(Me.pnlInfo)
        Me.wizPageData.Location = New System.Drawing.Point(0, 0)
        Me.wizPageData.Name = "wizPageData"
        Me.wizPageData.Size = New System.Drawing.Size(709, 357)
        Me.wizPageData.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.wizPageData.TabIndex = 9
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.colhIdentityType, Me.colhIdentityNo, Me.colhStatus, Me.colhMessage, Me.objdgcolhEmployeeId, Me.objcolhstatus})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(685, 247)
        Me.dgData.TabIndex = 20
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 322)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(107, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 23
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(685, 51)
        Me.pnlInfo.TabIndex = 3
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(563, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(449, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(563, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(495, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(608, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(449, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(608, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(495, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 30
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhIdentityType
        '
        Me.colhIdentityType.HeaderText = "Identity Type"
        Me.colhIdentityType.Name = "colhIdentityType"
        Me.colhIdentityType.ReadOnly = True
        '
        'colhIdentityNo
        '
        Me.colhIdentityNo.HeaderText = "Identity No."
        Me.colhIdentityNo.Name = "colhIdentityNo"
        Me.colhIdentityNo.ReadOnly = True
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 175
        '
        'objdgcolhEmployeeId
        '
        Me.objdgcolhEmployeeId.HeaderText = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.Name = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.ReadOnly = True
        Me.objdgcolhEmployeeId.Visible = False
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'frmImportIdentities
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(709, 405)
        Me.Controls.Add(Me.WizImportIdentity)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportIdentities"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Employee Identities"
        Me.WizImportIdentity.ResumeLayout(False)
        Me.wizPageMapping.ResumeLayout(False)
        Me.gbFiledMapping.ResumeLayout(False)
        Me.wizPageFile.ResumeLayout(False)
        Me.wizPageFile.PerformLayout()
        Me.wizPageData.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsFilter.ResumeLayout(False)
        Me.pnlInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents WizImportIdentity As eZee.Common.eZeeWizard
    Friend WithEvents wizPageData As eZee.Common.eZeeWizardPage
    Friend WithEvents wizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents wizPageFile As eZee.Common.eZeeWizardPage
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents gbFiledMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents objlblSign2 As System.Windows.Forms.Label
    Friend WithEvents lblIdentityType As System.Windows.Forms.Label
    Friend WithEvents cboIdentityType As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign6 As System.Windows.Forms.Label
    Friend WithEvents lblIsDefault As System.Windows.Forms.Label
    Friend WithEvents cboIsDefault As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign3 As System.Windows.Forms.Label
    Friend WithEvents lblIdentityNo As System.Windows.Forms.Label
    Friend WithEvents cboIdentityNo As System.Windows.Forms.ComboBox
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LblSerialNo As System.Windows.Forms.Label
    Friend WithEvents cboSerialNo As System.Windows.Forms.ComboBox
    Friend WithEvents LblCountry As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpiryDate As System.Windows.Forms.Label
    Friend WithEvents cboExpiryDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblIssueDate As System.Windows.Forms.Label
    Friend WithEvents cboIssueDate As System.Windows.Forms.ComboBox
    Friend WithEvents LblDLClass As System.Windows.Forms.Label
    Friend WithEvents cboDLClass As System.Windows.Forms.ComboBox
    Friend WithEvents LblPlaceofIssue As System.Windows.Forms.Label
    Friend WithEvents cboPlaceofIssue As System.Windows.Forms.ComboBox
    Friend WithEvents lnkAllocationFormat As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIdentityType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhIdentityNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
