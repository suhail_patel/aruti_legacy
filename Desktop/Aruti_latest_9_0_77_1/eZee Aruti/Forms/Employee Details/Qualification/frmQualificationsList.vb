﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmQualificationsList

#Region " Private Varaibles "
    Private objQualification As clsEmp_Qualification_Tran
    Private ReadOnly mstrModuleName As String = "frmQualificationsList"
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    Private mintTransactionId As Integer = 0
    Private objAQualificationTran As clsEmp_Qualification_Approval_Tran

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim QualificationApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmQualificationsList)))
    Private intParentRowIndex As Integer = -1
    'Gajanan [17-DEC-2018] -- End

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim isEmployeeApprove As Boolean = False
    'Gajanan [22-Feb-2019] -- End


    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End
#End Region

    'S.SANDEEP [ 14 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Property "

    Dim mintEmployeeUnkid As Integer = -1

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

#End Region
    'S.SANDEEP [ 14 AUG 2012 ] -- END

#Region " Private Functions "
    Private Sub FillCombo()
        Dim objCommon As New clsCommon_Master
        Dim objEmp As New clsEmployee_Master
        Dim objQualify As New clsqualification_master
        Dim objInstitute As New clsinstitute_master
        Dim dsList As New DataSet
        Try
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "QGrp")
            With cboCertificateGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("QGrp")
            End With

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsList = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, )
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , , , , False)
            'End If


            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmQualificationsList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            'dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                          mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, _
                                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


            'S.SANDEEP [20-JUN-2018] -- End


            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 08 OCT 2012 ] -- END


            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
            End With

            dsList = objQualify.GetComboList("Qualify", True)
            With cboCertificates
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Qualify")
            End With

            dsList = objInstitute.getListForCombo(False, "List", True)
            With cboInstitute
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            If User._Object.Privilege._AllowToViewEmpQualificationList = True Then                'Pinkal (02-Jul-2012) -- Start

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'dsList = objQualification.GetList("QualifyList")


                ''Pinkal (25-APR-2012) -- Start
                ''Enhancement : TRA Changes

                'If txtOtherQualificationGrp.Text.Trim.Trim.Length > 0 Then
                '    StrSearching &= "AND other_qualificationgrp LIKE '%" & txtOtherQualificationGrp.Text.Trim & "%' "
                'End If

                'If txtOtherQualification.Text.Trim.Trim.Length > 0 Then
                '    StrSearching &= "AND other_qualification LIKE '%" & txtOtherQualification.Text.Trim & "%' "
                'End If

                'If CInt(cboCertificateGroup.SelectedValue) > 0 Then
                '    StrSearching &= "AND QGrpId = " & CInt(cboCertificateGroup.SelectedValue) & " "
                'End If

                'If CInt(cboCertificates.SelectedValue) > 0 Then
                '    StrSearching &= "AND QualifyId = " & CInt(cboCertificates.SelectedValue) & " "
                'End If


                ''Pinkal (25-APR-2012) -- End


                'If CInt(cboEmployee.SelectedValue) > 0 Then
                '    StrSearching &= "AND EmpId = " & CInt(cboEmployee.SelectedValue) & " "
                'End If

                ''If txtAward.Text.Trim <> "" Then
                ''    StrSearching &= "AND Award LIKE '%" & txtAward.Text & "%'" & " "
                ''End If

                'If CInt(cboInstitute.SelectedValue) > 0 Then
                '    StrSearching &= "AND InstituteId = " & CInt(cboInstitute.SelectedValue)
                'End If


                'If txtAwardNo.Text.Trim <> "" Then
                '    StrSearching &= "AND RefNo LIKE '%" & txtAwardNo.Text & "%'" & " "
                'End If

                'If dtpQualificationFrom.Checked = True And dtpQualificationTo.Checked = True Then
                '    StrSearching &= "AND StDate >= '" & eZeeDate.convertDate(dtpQualificationFrom.Value) & "' AND EnDate <= '" & eZeeDate.convertDate(dtpQualificationTo.Value) & "'" & " "
                'End If

                ''Anjan (21 Nov 2011)-Start
                ''ENHANCEMENT : TRA COMMENTS
                'If mstrAdvanceFilter.Length > 0 Then
                '    StrSearching &= "AND " & mstrAdvanceFilter
                'End If
                ''Anjan (21 Nov 2011)-End 

                'If StrSearching.Length > 0 Then
                '    StrSearching = StrSearching.Substring(3)
                '    dtTable = New DataView(dsList.Tables("QualifyList"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = New DataView(dsList.Tables("QualifyList"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                'End If

                'S.SANDEEP [23 JUL 2016] -- START
                'CCK- OTHER QUALIFICATION CHANGE
                If txtOtherInstitute.Text.Trim.Length > 0 Then
                    StrSearching &= "AND ISNULL(hremp_qualification_tran.other_institute,'') LIKE '%" & txtOtherInstitute.Text.Trim & "%' "
                End If
                'S.SANDEEP [23 JUL 2016] -- END

                If txtOtherQualificationGrp.Text.Trim.Trim.Length > 0 Then
                    StrSearching &= "AND ISNULL(hremp_qualification_tran.other_qualificationgrp,'') LIKE '%" & txtOtherQualificationGrp.Text.Trim & "%' "
                End If

                If txtOtherQualification.Text.Trim.Trim.Length > 0 Then
                    StrSearching &= "AND ISNULL(hremp_qualification_tran.other_qualification,'') LIKE '%" & txtOtherQualification.Text.Trim & "%' "
                End If

                If CInt(cboCertificateGroup.SelectedValue) > 0 Then
                    StrSearching &= "AND ISNULL(cfcommon_master.masterunkid,0) = " & CInt(cboCertificateGroup.SelectedValue) & " "
                End If

                If CInt(cboCertificates.SelectedValue) > 0 Then
                    StrSearching &= "AND ISNULL(hrqualification_master.qualificationunkid,0) = " & CInt(cboCertificates.SelectedValue) & " "
                End If

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboInstitute.SelectedValue) > 0 Then
                    StrSearching &= "AND hrinstitute_master.instituteunkid = " & CInt(cboInstitute.SelectedValue)
                End If


                If txtAwardNo.Text.Trim <> "" Then
                    StrSearching &= "AND ISNULL(hremp_qualification_tran.reference_no, '') LIKE '%" & txtAwardNo.Text & "%'" & " "
                End If

                If dtpQualificationFrom.Checked = True And dtpQualificationTo.Checked = True Then
                    StrSearching &= "AND CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112) >= '" & eZeeDate.convertDate(dtpQualificationFrom.Value) & "' AND CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112) <= '" & eZeeDate.convertDate(dtpQualificationTo.Value) & "'" & " "
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If


                'Pinkal (28-Dec-2015) -- Start
                'Enhancement - Working on Changes in SS for Employee Master.

                'dsList = objQualification.GetList(FinancialYear._Object._DatabaseName, _
                '                                   User._Object._Userunkid, _
                '                                   FinancialYear._Object._YearUnkid, _
                '                                   Company._Object._Companyunkid, _
                '                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                '                                   ConfigParameter._Object._IsIncludeInactiveEmp, "QualifyList", , StrSearching)

                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .
                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmQualificationsList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If

                'dsList = objQualification.GetList(FinancialYear._Object._DatabaseName, _
                '                                  User._Object._Userunkid, _
                '                                  FinancialYear._Object._YearUnkid, _
                '                                  Company._Object._Companyunkid, _
                '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                  ConfigParameter._Object._UserAccessModeSetting, True, _
                '                                ConfigParameter._Object._IsIncludeInactiveEmp, "QualifyList", , StrSearching, True)

                dsList = objQualification.GetList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                               ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                                               ConfigParameter._Object._IsIncludeInactiveEmp, "QualifyList", True, _
                                                               StrSearching, True, mblnAddApprovalCondition)


                'S.SANDEEP [20-JUN-2018] -- End


                'Pinkal (28-Dec-2015) -- End


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim dcol As New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "tranguid"
                    .DefaultValue = ""
                End With
                dsList.Tables(0).Columns.Add(dcol)





                If QualificationApprovalFlowVal Is Nothing AndAlso mintTransactionId <= 0 Then
                    Dim dsPending As New DataSet


                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                    'S.SANDEEP |31-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [Employee Bio Data UAT]
                    'StrSearching &= StrSearching.Replace("hremp_qualification_tran", "hremp_qualification_approval_tran")
                    StrSearching = StrSearching.Replace("hremp_qualification_tran", "hremp_qualification_approval_tran")
                    'S.SANDEEP |31-MAY-2019| -- END

                    'Gajanan [17-April-2019] -- End


                    dsPending = objAQualificationTran.GetList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                              ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", , StrSearching, _
                                              True, mblnAddApprovalCondition)

                    If dsPending.Tables(0).Rows.Count > 0 Then
                        For Each row As DataRow In dsPending.Tables(0).Rows
                            dsList.Tables(0).ImportRow(row)
                        Next
                    End If
                End If
                'Gajanan [17-DEC-2018] -- End

                dtTable = New DataView(dsList.Tables("QualifyList"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                lvQualification.BeginUpdate()
                'S.SANDEEP [04 JUN 2015] -- END

                lvQualification.Items.Clear()

                For Each dtRow As DataRow In dtTable.Rows


                    'Pinkal (25-APR-2012) -- Start
                    'Enhancement : TRA Changes

                    If chkOtherQualification.Checked = False AndAlso CInt(dtRow.Item("QualifyId")) <= 0 Then
                        Continue For
                    ElseIf chkOtherQualification.Checked = True AndAlso CInt(dtRow.Item("QualifyId")) > 0 Then
                        Continue For
                    End If

                    'Pinkal (25-APR-2012) -- End

                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("EmpName").ToString
                    lvItem.SubItems(colhEmployee.Index).Tag = dtRow.Item("EmpId")

                    lvItem.SubItems.Add(dtRow.Item("QGrp").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Qualify").ToString)

                    If IsDBNull(dtRow.Item("StDate")) Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("StDate").ToString).ToShortDateString)
                    End If
                    If IsDBNull(dtRow.Item("EnDate")) Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("EnDate").ToString).ToShortDateString)
                    End If
                    lvItem.SubItems.Add(dtRow.Item("Institute").ToString)
                    lvItem.SubItems.Add(dtRow.Item("RefNo").ToString)

                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    lvItem.SubItems.Add(dtRow.Item("tranguid").ToString)
                    lvItem.SubItems(objdgcolhtranguid.Index).Tag = dtRow.Item("operationtypeid").ToString()
                    lvItem.SubItems.Add(dtRow.Item("EmpId").ToString)
                    If dtRow.Item("tranguid").ToString.Length > 0 Then
                        lvItem.BackColor = Color.PowderBlue
                        lvItem.ForeColor = Color.Black
                        lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                        'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.



'lblPendingData.Visible = True

                        objtblPanel.Visible = False
'Gajanan [17-DEC-2018] -- End
                        btnApprovalinfo.Visible = False
                    End If
                    lvItem.SubItems.Add(dtRow("OperationType").ToString())
                    'Gajanan [17-DEC-2018] -- End


                    lvItem.Tag = dtRow.Item("QulifyTranId")

                    lvQualification.Items.Add(lvItem)

                    lvItem = Nothing
                Next

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                colhOperationType.DisplayIndex = 0
                'Gajanan [17-DEC-2018] -- End

                colhQualifyGroup.Width += colhEmployee.Width
                colhEmployee.Width = 0

                If lvQualification.Items.Count > 5 Then
                    colhQualifyGroup.Width = 270 - 20
                Else
                    colhQualifyGroup.Width = 270
                End If

                lvQualification.GroupingColumn = colhEmployee
                lvQualification.DisplayGroups(True)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                lvQualification.EndUpdate()
                'S.SANDEEP [04 JUN 2015] -- END

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AddEmployeeQualification
            btnEdit.Enabled = User._Object.Privilege._EditEmployeeExperience
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeExperience

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            mnuExportQualification.Enabled = User._Object.Privilege._AllowtoExportEmpQualification
            mnuImportQualification.Enabled = User._Object.Privilege._AllowtoImportEmpQualification
            mnuExportOtherQualification.Enabled = User._Object.Privilege._AllowtoExportOtherQualification
            mnuScanAttach.Enabled = User._Object.Privilege._AllowToScanAttachDocument
            'Anjan (25 Oct 2012)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmQualificationsList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objQualification = New clsEmp_Qualification_Tran
        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        objAQualificationTran = New clsEmp_Qualification_Approval_Tran
        'Gajanan [17-DEC-2018] -- End
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()

            Call FillCombo()

            ' Call FillList()

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'lblPendingData.Visible = False
            objtblPanel.Visible = False
            btnApprovalinfo.Visible = False

            'Gajanan [17-DEC-2018] -- End

            Call SetVisibility()

            If lvQualification.Items.Count > 0 Then lvQualification.Items(0).Selected = True
            lvQualification.Select()

            'S.SANDEEP [ 04 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call chkOtherQualification_CheckedChanged(sender, e)
            'S.SANDEEP [ 04 MAY 2012 ] -- END


            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
                Call FillList()
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmQualificationsList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmQualificationsList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvQualification.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmQualificationsList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmQualificationsList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objQualification = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmp_Qualification_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsEmp_Qualification_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvQualification.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvQualification.Select()
            Exit Sub
        End If

        Try

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim itmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid= '" & CInt(lvQualification.SelectedItems(0).Tag) & "'")
            If itmp.Length > 0 Then
                isEmployeeApprove = CBool(itmp(0).Item("isapproved").ToString())
            Else
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'isEmployeeApprove = True
                isEmployeeApprove = False
                'Gajanan [17-April-2019] -- End
            End If
            'Gajanan [22-Feb-2019] -- End




            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If QualificationApprovalFlowVal Is Nothing Then
            If QualificationApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If objApprovalData.IsApproverPresent(enScreenName.frmQualificationsList, FinancialYear._Object._DatabaseName, _
                                                      ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                      FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                                                      User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(lvQualification.SelectedItems(0).SubItems(colhEmployee.Index).Tag), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)
                    lvQualification.SelectedItems(0).Selected = False
                    Exit Sub
                End If

                Dim item As ListViewItem = Nothing
                item = lvQualification.Items.Cast(Of ListViewItem).Where(Function(x) CInt(x.Tag) = CInt(lvQualification.SelectedItems(0).Tag) And x.SubItems(objdgcolhtranguid.Index).Text.Trim.Length > 0).FirstOrDefault()
                If item IsNot Nothing Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process."), enMsgBoxStyle.Information)
                    lvQualification.SelectedItems(0).Selected = False
                    Exit Sub
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvQualification.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objQualification._Isvoid = True
                objQualification._Voiduserunkid = User._Object._Userunkid

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objQualification._Voidreason = "Testing"
                'objQualification._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                Dim frm As New frmReasonSelection
                'Anjan (02 Sep 2011)-Start
                'Issue : Including Language Settings.
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                'Anjan (02 Sep 2011)-End 
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.EMPLOYEE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objQualification._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objQualification._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End 

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objQualification._FormName = mstrModuleName
                objQualification._Loginemployeeunkid = 0
                objQualification._ClientIP = getIP()
                objQualification._HostName = getHostName()
                objQualification._FromWeb = False
                objQualification._AuditUserId = User._Object._Userunkid
objQualification._CompanyUnkid = Company._Object._Companyunkid
                objQualification._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'objQualification.Delete(CInt(lvQualification.SelectedItems(0).Tag))

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If QualificationApprovalFlowVal Is Nothing Then
                If QualificationApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                    'Gajanan [22-Feb-2019] -- End
                    objAQualificationTran._Isvoid = True
                    objAQualificationTran._Audituserunkid = User._Object._Userunkid
                    objAQualificationTran._Isweb = False
                    objAQualificationTran._Ip = getIP()
                    objAQualificationTran._Host = getHostName()
                    objAQualificationTran._Form_Name = mstrModuleName
                    If objAQualificationTran.Delete(CInt(lvQualification.SelectedItems(0).Tag), mstrVoidReason, Company._Object._Companyunkid, Nothing) = False Then
                        If objAQualificationTran._Message <> "" Then
                            eZeeMsgBox.Show(objAQualificationTran._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub

                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Else

                        objApprovalData = New clsEmployeeDataApproval

                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                                 Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                                 CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                                                                 enScreenName.frmQualificationsList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                                 User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                                 User._Object._Username, clsEmployeeDataApproval.enOperationType.DELETED, , lvQualification.SelectedItems(0).SubItems(objdgcolhempid.Index).Text, , , _
                                                                 " qualificationtranunkid = " & CInt(lvQualification.SelectedItems(0).Tag), Nothing, , , _
                                                                 " qualificationtranunkid = " & CInt(lvQualification.SelectedItems(0).Tag), Nothing)
                        'Gajanan [17-April-2019] -- End

                    End If
                Else
                    If objQualification.Delete(CInt(lvQualification.SelectedItems(0).Tag)) = False Then
                        If objQualification._Message <> "" Then
                            eZeeMsgBox.Show(objQualification._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If
                End If


                'Sandeep [ 16 Oct 2010 ] -- End 
                'Gajanan [17-DEC-2018] -- End
                objQualification.Delete(CInt(lvQualification.SelectedItems(0).Tag))
                lvQualification.SelectedItems(0).Remove()

                If lvQualification.Items.Count <= 0 Then
                    Exit Try
                End If

            End If
            lvQualification.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvQualification.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvQualification.Select()
            Exit Sub
        End If
        Dim frm As New frmQualifications
        Try

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If QualificationApprovalFlowVal Is Nothing Then
                Dim item As ListViewItem = Nothing
                item = lvQualification.Items.Cast(Of ListViewItem).Where(Function(x) CInt(x.Tag) = CInt(lvQualification.SelectedItems(0).Tag) And x.SubItems(objdgcolhtranguid.Index).Text.Trim.Length > 0).FirstOrDefault()
                If item IsNot Nothing Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process."), enMsgBoxStyle.Information)
                    lvQualification.SelectedItems(0).Selected = False
                    Exit Sub
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvQualification.SelectedItems(0).Index

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If frm.displayDialog(CInt(lvQualification.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
            If frm.displayDialog(CInt(lvQualification.SelectedItems(0).Tag), enAction.EDIT_ONE, mintEmployeeUnkid) Then
                'S.SANDEEP [ 14 AUG 2012 ] -- END

                'Anjan (17 May 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                'Call FillList()
                lvQualification.Groups.Clear()
                lvQualification.Items.Clear()
                'Anjan (17 May 2012)-End 

                'S.SANDEEP [ 14 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If mintEmployeeUnkid > 0 Then
                    Call FillList()
                End If
                'S.SANDEEP [ 14 AUG 2012 ] -- END

            End If
            frm = Nothing


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmQualifications
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
            If frm.displayDialog(-1, enAction.ADD_CONTINUE, mintEmployeeUnkid) Then
                'S.SANDEEP [ 14 AUG 2012 ] -- END

                'Anjan (17 May 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                'Call FillList()
                lvQualification.Groups.Clear()
                lvQualification.Items.Clear()
                'Anjan (17 May 2012)-End 

                'S.SANDEEP [ 14 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If mintEmployeeUnkid > 0 Then
                    Call FillList()
                End If
                'S.SANDEEP [ 14 AUG 2012 ] -- END

            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCertificateGroup.SelectedValue = 0

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'cboEmployee.SelectedValue = 0
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            Else
                cboEmployee.SelectedValue = 0
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            cboCertificates.SelectedValue = 0
            cboInstitute.SelectedValue = 0
            txtAwardNo.Text = ""
            dtpQualificationFrom.Checked = False
            dtpQualificationTo.Checked = False
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End 


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            txtOtherQualificationGrp.Text = ""
            txtOtherQualification.Text = ""
            'Pinkal (25-APR-2012) -- End


            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 
            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES


    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuScanAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanAttach.Click
        Dim frm As New frmScanOrAttachmentInfo
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP |26-APR-2019| -- START
            'frm.displayDialog(Language.getMessage(mstrModuleName, 4, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.ADD_ONE, "", mstrModuleName) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
            frm.displayDialog(Language.getMessage(mstrModuleName, 4, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.ADD_ONE, "", mstrModuleName, True) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
            'S.SANDEEP |26-APR-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuScanAttach_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'If lvQualification.SelectedItems.Count <= 0 Then
    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one employee qualification."), enMsgBoxStyle.Information)
    '    lvQualification.Select()
    '    Exit Sub
    'End If
    'Dim dtTable As DataTable
    'Dim dsList As New DataSet
    'Try
    '    Dim ObjFrm As New frmScanOrAttachmentInfo
    '    dsList = objQualification.GetList("Qualification")
    '    dtTable = New DataView(dsList.Tables(0), "EmpId = " & CInt(lvQualification.SelectedItems(0).SubItems(colhEmployee.Index).Tag), "", DataViewRowState.CurrentRows).ToTable
    '    dsList.Tables.Remove("Qualification")
    '    dsList.Tables.Add(dtTable)
    '    ObjFrm._Dataset = dsList
    '    ObjFrm._LableCaption = Language.getMessage(mstrModuleName, 2, "Select Qualification")
    '    ObjFrm._RefId = enScanAttactRefId.EMPLOYEE_QUALIFICATION
    '    ObjFrm._EmployeeUnkid = CInt(lvQualification.SelectedItems(0).SubItems(colhEmployee.Index).Tag)
    '    ObjFrm._IsComboVisible = True
    '    ObjFrm._EmployeeName = lvQualification.SelectedItems(0).SubItems(colhEmployee.Index).Text

    '    ObjFrm.ShowDialog()

    'Catch ex As Exception
    '    DisplayError.Show("-1", ex.Message, "mnuScanAttach_Click", mstrModuleName)
    'End Try

    'S.SANDEEP [ 18 FEB 2012 ] -- END







    Private Sub mnuImportQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportQualification.Click
        Dim frm As New frmImportQualificationWizard
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportQualification_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuExportQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportQualification.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx|XML files (*.xml)|*.xml"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'strFilePath = ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                'S.SANDEEP [12-Jan-2018] -- END
                strFilePath &= ObjFile.Extension


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objQualification._FormName = mstrModuleName
                objQualification._Loginemployeeunkid = 0
                objQualification._ClientIP = getIP()
                objQualification._HostName = getHostName()
                objQualification._FromWeb = False
                objQualification._AuditUserId = User._Object._Userunkid
objQualification._CompanyUnkid = Company._Object._Companyunkid
                objQualification._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                dsList = objQualification.GetQualificationData_Export()

                Select Case dlgSaveFile.FilterIndex
                    Case 1   'XLS
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'IExcel.Export(strFilePath, dsList)
                        OpenXML_Export(strFilePath, dsList)
                        'S.SANDEEP [12-Jan-2018] -- END
                    Case 2   'XML
                        dsList.WriteXml(strFilePath)
                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "File exported successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportQualification_Click", mstrModuleName)
        Finally
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList.Dispose() : path = String.Empty : strFilePath = String.Empty : dlgSaveFile = Nothing : ObjFile = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    'S.SANDEEP [ 26 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuExportOtherQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportOtherQualification.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx|XML files (*.xml)|*.xml"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'strFilePath = ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                'S.SANDEEP [12-Jan-2018] -- END
                strFilePath &= ObjFile.Extension

                dsList = objQualification.GetQualificationData_Export(True)

                Select Case dlgSaveFile.FilterIndex
                    Case 1   'XLS
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'IExcel.Export(strFilePath, dsList)
                        OpenXML_Export(strFilePath, dsList)
                        'S.SANDEEP [12-Jan-2018] -- END
                    Case 2   'XML
                        dsList.WriteXml(strFilePath)
                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "File exported successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportQualification_Click", mstrModuleName)
        Finally
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList.Dispose() : path = String.Empty : strFilePath = String.Empty : dlgSaveFile = Nothing : ObjFile = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 26 APRIL 2012 ] -- END


    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    Private Sub btnApprovalinfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprovalinfo.Click
        Try
            Dim empid As Integer = 0
            If lvQualification.SelectedItems.Count > 0 Then
                empid = CInt(lvQualification.SelectedItems(0).SubItems(objdgcolhempid.Index).Text)
            End If
            Dim frm As New frmViewEmployeeDataApproval
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            '" ( " & CInt(enUserPriviledge.) & _
            '       ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences) & _
            '       ", " & CInt() & _
            '       ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences) & " ) "



            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'frm.displayDialog(User._Object._Userunkid, 0, enUserPriviledge.AllowToApproveRejectEmployeeQualifications, enScreenName.frmQualificationsList, CType(lvQualification.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Tag, clsEmployeeDataApproval.enOperationType), "EM.employeeunkid = " & empid, False)
            frm.displayDialog(User._Object._Userunkid, 0, enUserPriviledge.AllowToApproveRejectEmployeeQualifications, enScreenName.frmQualificationsList, clsEmployeeDataApproval.enOperationType.NONE, "EM.employeeunkid = " & empid, False)
            'Gajanan [17-DEC-2018] -- End


            If frm IsNot Nothing Then frm.Dispose()
            Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprovalinfo_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan (21 Nov 2011)-End 
#End Region

    'Pinkal (25-APR-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub chkOtherQualification_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOtherQualification.CheckedChanged
        Try
            txtOtherQualificationGrp.Text = ""
            txtOtherQualification.Text = ""
            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            txtOtherInstitute.Text = ""
            'S.SANDEEP [23 JUL 2016] -- END
            If chkOtherQualification.Checked Then
                txtOtherQualificationGrp.Enabled = True
                txtOtherQualification.Enabled = True
                'S.SANDEEP [23 JUL 2016] -- START
                'CCK- OTHER QUALIFICATION CHANGE
                txtOtherInstitute.Enabled = True
                'S.SANDEEP [23 JUL 2016] -- END
            Else
                txtOtherQualificationGrp.Enabled = False
                txtOtherQualification.Enabled = False
                'S.SANDEEP [23 JUL 2016] -- START
                'CCK- OTHER QUALIFICATION CHANGE
                txtOtherInstitute.Enabled = False
                'S.SANDEEP [23 JUL 2016] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkOtherQualification_CheckedChanged", mstrModuleName)
        End Try
    End Sub


    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
#Region " Listview Events "

    Private Sub lvQualification_ItemSelectionChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvQualification.ItemSelectionChanged
        Try
            If lvQualification.SelectedItems.Count > 0 Then
                If intParentRowIndex <> -1 Then
                    lvQualification.Items(intParentRowIndex).BackColor = Color.White
                    intParentRowIndex = -1
                End If

                If lvQualification.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Text.Length > 0 Then

                    If lvQualification.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Tag IsNot Nothing Then
                        If CInt(lvQualification.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Tag) = clsEmployeeDataApproval.enOperationType.EDITED Then
                            Dim item As ListViewItem = lvQualification.Items.Cast(Of ListViewItem).Where(Function(x) CInt(x.Tag) = CInt(lvQualification.SelectedItems(0).Tag) And x.SubItems(objdgcolhtranguid.Index).Text = "").FirstOrDefault()
                            If item IsNot Nothing Then intParentRowIndex = item.Index
                            lvQualification.Items(intParentRowIndex).BackColor = Color.LightCoral
                        End If
                    End If

                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                    btnOperation.Visible = False
                    btnApprovalinfo.Visible = True
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'lblPendingData.Visible = True
objtblPanel.Visible = True

'Gajanan [17-DEC-2018] -- End
                Else
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                    btnOperation.Visible = True
                    btnApprovalinfo.Visible = False
                    'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'lblPendingData.Visible = True
objtblPanel.Visible = True

'Gajanan [17-DEC-2018] -- End
                End If
            Else
                btnEdit.Enabled = True
                btnDelete.Enabled = True
                btnOperation.Visible = True
                btnApprovalinfo.Visible = False
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.


                    'lblPendingData.Visible = True
objtblPanel.Visible = True

'Gajanan [17-DEC-2018] -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvSkillList_ItemSelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'Gajanan [17-DEC-2018] -- End

    'Pinkal (25-APR-2012) -- End





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

			Me.btnApprovalinfo.GradientBackColor = GUI._ButttonBackColor 
			Me.btnApprovalinfo.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblQualification.Text = Language._Object.getCaption(Me.lblQualification.Name, Me.lblQualification.Text)
            Me.lblQualificationGroup.Text = Language._Object.getCaption(Me.lblQualificationGroup.Name, Me.lblQualificationGroup.Text)
            Me.lblAwardDate.Text = Language._Object.getCaption(Me.lblAwardDate.Name, Me.lblAwardDate.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhQualifyGroup.Text = Language._Object.getCaption(CStr(Me.colhQualifyGroup.Tag), Me.colhQualifyGroup.Text)
            Me.colhQualification.Text = Language._Object.getCaption(CStr(Me.colhQualification.Tag), Me.colhQualification.Text)
            Me.colhAwardDate.Text = Language._Object.getCaption(CStr(Me.colhAwardDate.Tag), Me.colhAwardDate.Text)
            Me.lblReferenceNo.Text = Language._Object.getCaption(Me.lblReferenceNo.Name, Me.lblReferenceNo.Text)
            Me.colhInstitute.Text = Language._Object.getCaption(CStr(Me.colhInstitute.Tag), Me.colhInstitute.Text)
            Me.colhRefNo.Text = Language._Object.getCaption(CStr(Me.colhRefNo.Tag), Me.colhRefNo.Text)
            Me.lblInstitute.Text = Language._Object.getCaption(Me.lblInstitute.Name, Me.lblInstitute.Text)
            Me.colhAwardToDate.Text = Language._Object.getCaption(CStr(Me.colhAwardToDate.Tag), Me.colhAwardToDate.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuScanAttach.Text = Language._Object.getCaption(Me.mnuScanAttach.Name, Me.mnuScanAttach.Text)
            Me.mnuImportQualification.Text = Language._Object.getCaption(Me.mnuImportQualification.Name, Me.mnuImportQualification.Text)
            Me.mnuExportQualification.Text = Language._Object.getCaption(Me.mnuExportQualification.Name, Me.mnuExportQualification.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.lblOtherQualificationGrp.Text = Language._Object.getCaption(Me.lblOtherQualificationGrp.Name, Me.lblOtherQualificationGrp.Text)
            Me.lblOtherQualification.Text = Language._Object.getCaption(Me.lblOtherQualification.Name, Me.lblOtherQualification.Text)
            Me.mnuExportOtherQualification.Text = Language._Object.getCaption(Me.mnuExportOtherQualification.Name, Me.mnuExportOtherQualification.Text)
            Me.chkOtherQualification.Text = Language._Object.getCaption(Me.chkOtherQualification.Name, Me.chkOtherQualification.Text)
            Me.lblOtherInstitute.Text = Language._Object.getCaption(Me.lblOtherInstitute.Name, Me.lblOtherInstitute.Text)
			Me.btnApprovalinfo.Text = Language._Object.getCaption(Me.btnApprovalinfo.Name, Me.btnApprovalinfo.Text)
			Me.colhOperationType.Text = Language._Object.getCaption(CStr(Me.colhOperationType.Tag), Me.colhOperationType.Text)
			Me.lblParentData.Text = Language._Object.getCaption(Me.lblParentData.Name, Me.lblParentData.Text)
	    Me.lblPendingData.Text = Language._Object.getCaption(Me.lblPendingData.Name, Me.lblPendingData.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction ?")
            Language.setMessage(mstrModuleName, 3, "File exported successfully.")
            Language.setMessage(mstrModuleName, 4, "Select Employee")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process.")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class