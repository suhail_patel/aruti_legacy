﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQualificationsList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmQualificationsList))
        Me.pnlQualificationList = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtOtherInstitute = New System.Windows.Forms.TextBox
        Me.lblOtherInstitute = New System.Windows.Forms.Label
        Me.chkOtherQualification = New System.Windows.Forms.CheckBox
        Me.txtOtherQualification = New eZee.TextBox.AlphanumericTextBox
        Me.txtOtherQualificationGrp = New eZee.TextBox.AlphanumericTextBox
        Me.lblOtherQualification = New System.Windows.Forms.Label
        Me.lblOtherQualificationGrp = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.lblInstitute = New System.Windows.Forms.Label
        Me.cboInstitute = New System.Windows.Forms.ComboBox
        Me.lblReferenceNo = New System.Windows.Forms.Label
        Me.txtAwardNo = New eZee.TextBox.AlphanumericTextBox
        Me.dtpQualificationTo = New System.Windows.Forms.DateTimePicker
        Me.lblTo = New System.Windows.Forms.Label
        Me.lblAwardDate = New System.Windows.Forms.Label
        Me.dtpQualificationFrom = New System.Windows.Forms.DateTimePicker
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.lblQualification = New System.Windows.Forms.Label
        Me.cboCertificates = New System.Windows.Forms.ComboBox
        Me.cboCertificateGroup = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblQualificationGroup = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lvQualification = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhQualifyGroup = New System.Windows.Forms.ColumnHeader
        Me.colhQualification = New System.Windows.Forms.ColumnHeader
        Me.colhAwardDate = New System.Windows.Forms.ColumnHeader
        Me.colhAwardToDate = New System.Windows.Forms.ColumnHeader
        Me.colhInstitute = New System.Windows.Forms.ColumnHeader
        Me.colhRefNo = New System.Windows.Forms.ColumnHeader
        Me.objdgcolhtranguid = New System.Windows.Forms.ColumnHeader
        Me.objdgcolhempid = New System.Windows.Forms.ColumnHeader
        Me.colhOperationType = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnApprovalinfo = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuScanAttach = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuImportQualification = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportQualification = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportOtherQualification = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objtblPanel = New System.Windows.Forms.TableLayoutPanel
        Me.lblParentData = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.lblPendingData = New System.Windows.Forms.Label
        Me.pnlQualificationList.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperations.SuspendLayout()
        Me.objtblPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlQualificationList
        '
        Me.pnlQualificationList.Controls.Add(Me.gbFilterCriteria)
        Me.pnlQualificationList.Controls.Add(Me.lvQualification)
        Me.pnlQualificationList.Controls.Add(Me.objFooter)
        Me.pnlQualificationList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlQualificationList.Location = New System.Drawing.Point(0, 0)
        Me.pnlQualificationList.Name = "pnlQualificationList"
        Me.pnlQualificationList.Size = New System.Drawing.Size(842, 492)
        Me.pnlQualificationList.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.txtOtherInstitute)
        Me.gbFilterCriteria.Controls.Add(Me.lblOtherInstitute)
        Me.gbFilterCriteria.Controls.Add(Me.chkOtherQualification)
        Me.gbFilterCriteria.Controls.Add(Me.txtOtherQualification)
        Me.gbFilterCriteria.Controls.Add(Me.txtOtherQualificationGrp)
        Me.gbFilterCriteria.Controls.Add(Me.lblOtherQualification)
        Me.gbFilterCriteria.Controls.Add(Me.lblOtherQualificationGrp)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.lblInstitute)
        Me.gbFilterCriteria.Controls.Add(Me.cboInstitute)
        Me.gbFilterCriteria.Controls.Add(Me.lblReferenceNo)
        Me.gbFilterCriteria.Controls.Add(Me.txtAwardNo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpQualificationTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblAwardDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpQualificationFrom)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeStraightLine2)
        Me.gbFilterCriteria.Controls.Add(Me.lblQualification)
        Me.gbFilterCriteria.Controls.Add(Me.cboCertificates)
        Me.gbFilterCriteria.Controls.Add(Me.cboCertificateGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblQualificationGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(8, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(825, 116)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherInstitute
        '
        Me.txtOtherInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherInstitute.Location = New System.Drawing.Point(549, 87)
        Me.txtOtherInstitute.Name = "txtOtherInstitute"
        Me.txtOtherInstitute.Size = New System.Drawing.Size(270, 21)
        Me.txtOtherInstitute.TabIndex = 228
        '
        'lblOtherInstitute
        '
        Me.lblOtherInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherInstitute.Location = New System.Drawing.Point(480, 88)
        Me.lblOtherInstitute.Name = "lblOtherInstitute"
        Me.lblOtherInstitute.Size = New System.Drawing.Size(70, 16)
        Me.lblOtherInstitute.TabIndex = 227
        Me.lblOtherInstitute.Text = "Oth. Inst."
        Me.lblOtherInstitute.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkOtherQualification
        '
        Me.chkOtherQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOtherQualification.Location = New System.Drawing.Point(11, 89)
        Me.chkOtherQualification.Name = "chkOtherQualification"
        Me.chkOtherQualification.Size = New System.Drawing.Size(222, 17)
        Me.chkOtherQualification.TabIndex = 225
        Me.chkOtherQualification.Text = "Other Qualification / Short Course"
        Me.chkOtherQualification.UseVisualStyleBackColor = True
        '
        'txtOtherQualification
        '
        Me.txtOtherQualification.Flags = 0
        Me.txtOtherQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherQualification.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherQualification.Location = New System.Drawing.Point(549, 60)
        Me.txtOtherQualification.Name = "txtOtherQualification"
        Me.txtOtherQualification.Size = New System.Drawing.Size(106, 21)
        Me.txtOtherQualification.TabIndex = 2
        '
        'txtOtherQualificationGrp
        '
        Me.txtOtherQualificationGrp.Flags = 0
        Me.txtOtherQualificationGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherQualificationGrp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherQualificationGrp.Location = New System.Drawing.Point(357, 86)
        Me.txtOtherQualificationGrp.Name = "txtOtherQualificationGrp"
        Me.txtOtherQualificationGrp.Size = New System.Drawing.Size(121, 21)
        Me.txtOtherQualificationGrp.TabIndex = 1
        '
        'lblOtherQualification
        '
        Me.lblOtherQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherQualification.Location = New System.Drawing.Point(480, 62)
        Me.lblOtherQualification.Name = "lblOtherQualification"
        Me.lblOtherQualification.Size = New System.Drawing.Size(64, 16)
        Me.lblOtherQualification.TabIndex = 211
        Me.lblOtherQualification.Text = "Oth. Qualif."
        Me.lblOtherQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOtherQualificationGrp
        '
        Me.lblOtherQualificationGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherQualificationGrp.Location = New System.Drawing.Point(247, 88)
        Me.lblOtherQualificationGrp.Name = "lblOtherQualificationGrp"
        Me.lblOtherQualificationGrp.Size = New System.Drawing.Size(97, 16)
        Me.lblOtherQualificationGrp.TabIndex = 223
        Me.lblOtherQualificationGrp.Text = "Oth. Qualif. Group"
        Me.lblOtherQualificationGrp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(798, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 137
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(775, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 136
        Me.objbtnSearch.TabStop = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(682, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(81, 13)
        Me.lnkAllocation.TabIndex = 142
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'lblInstitute
        '
        Me.lblInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstitute.Location = New System.Drawing.Point(8, 63)
        Me.lblInstitute.Name = "lblInstitute"
        Me.lblInstitute.Size = New System.Drawing.Size(64, 15)
        Me.lblInstitute.TabIndex = 140
        Me.lblInstitute.Text = "Institute"
        '
        'cboInstitute
        '
        Me.cboInstitute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInstitute.FormattingEnabled = True
        Me.cboInstitute.Location = New System.Drawing.Point(78, 60)
        Me.cboInstitute.Name = "cboInstitute"
        Me.cboInstitute.Size = New System.Drawing.Size(128, 21)
        Me.cboInstitute.TabIndex = 139
        '
        'lblReferenceNo
        '
        Me.lblReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReferenceNo.Location = New System.Drawing.Point(656, 62)
        Me.lblReferenceNo.Name = "lblReferenceNo"
        Me.lblReferenceNo.Size = New System.Drawing.Size(44, 16)
        Me.lblReferenceNo.TabIndex = 14
        Me.lblReferenceNo.Text = "Ref.No"
        Me.lblReferenceNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAwardNo
        '
        Me.txtAwardNo.Flags = 0
        Me.txtAwardNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAwardNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAwardNo.Location = New System.Drawing.Point(712, 60)
        Me.txtAwardNo.Name = "txtAwardNo"
        Me.txtAwardNo.Size = New System.Drawing.Size(107, 21)
        Me.txtAwardNo.TabIndex = 15
        '
        'dtpQualificationTo
        '
        Me.dtpQualificationTo.Checked = False
        Me.dtpQualificationTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpQualificationTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpQualificationTo.Location = New System.Drawing.Point(712, 33)
        Me.dtpQualificationTo.Name = "dtpQualificationTo"
        Me.dtpQualificationTo.ShowCheckBox = True
        Me.dtpQualificationTo.Size = New System.Drawing.Size(107, 21)
        Me.dtpQualificationTo.TabIndex = 11
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(655, 32)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(56, 27)
        Me.lblTo.TabIndex = 10
        Me.lblTo.Text = "Award Dt."
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAwardDate
        '
        Me.lblAwardDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAwardDate.Location = New System.Drawing.Point(480, 35)
        Me.lblAwardDate.Name = "lblAwardDate"
        Me.lblAwardDate.Size = New System.Drawing.Size(67, 16)
        Me.lblAwardDate.TabIndex = 8
        Me.lblAwardDate.Text = "Start Date"
        Me.lblAwardDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpQualificationFrom
        '
        Me.dtpQualificationFrom.Checked = False
        Me.dtpQualificationFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpQualificationFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpQualificationFrom.Location = New System.Drawing.Point(549, 35)
        Me.dtpQualificationFrom.Name = "dtpQualificationFrom"
        Me.dtpQualificationFrom.ShowCheckBox = True
        Me.dtpQualificationFrom.Size = New System.Drawing.Size(106, 21)
        Me.dtpQualificationFrom.TabIndex = 9
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(239, 25)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(8, 89)
        Me.EZeeStraightLine2.TabIndex = 3
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'lblQualification
        '
        Me.lblQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualification.Location = New System.Drawing.Point(247, 62)
        Me.lblQualification.Name = "lblQualification"
        Me.lblQualification.Size = New System.Drawing.Size(108, 16)
        Me.lblQualification.TabIndex = 6
        Me.lblQualification.Text = "Qualification/Award"
        Me.lblQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCertificates
        '
        Me.cboCertificates.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCertificates.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCertificates.FormattingEnabled = True
        Me.cboCertificates.Location = New System.Drawing.Point(357, 60)
        Me.cboCertificates.Name = "cboCertificates"
        Me.cboCertificates.Size = New System.Drawing.Size(121, 21)
        Me.cboCertificates.TabIndex = 7
        '
        'cboCertificateGroup
        '
        Me.cboCertificateGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCertificateGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCertificateGroup.FormattingEnabled = True
        Me.cboCertificateGroup.Location = New System.Drawing.Point(357, 33)
        Me.cboCertificateGroup.Name = "cboCertificateGroup"
        Me.cboCertificateGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboCertificateGroup.TabIndex = 5
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(64, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        '
        'lblQualificationGroup
        '
        Me.lblQualificationGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualificationGroup.Location = New System.Drawing.Point(247, 35)
        Me.lblQualificationGroup.Name = "lblQualificationGroup"
        Me.lblQualificationGroup.Size = New System.Drawing.Size(108, 16)
        Me.lblQualificationGroup.TabIndex = 4
        Me.lblQualificationGroup.Text = "Quali.Award Group"
        Me.lblQualificationGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(212, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(78, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(128, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lvQualification
        '
        Me.lvQualification.BackColorOnChecked = True
        Me.lvQualification.ColumnHeaders = Nothing
        Me.lvQualification.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhQualifyGroup, Me.colhQualification, Me.colhAwardDate, Me.colhAwardToDate, Me.colhInstitute, Me.colhRefNo, Me.objdgcolhtranguid, Me.objdgcolhempid, Me.colhOperationType})
        Me.lvQualification.CompulsoryColumns = ""
        Me.lvQualification.FullRowSelect = True
        Me.lvQualification.GridLines = True
        Me.lvQualification.GroupingColumn = Nothing
        Me.lvQualification.HideSelection = False
        Me.lvQualification.Location = New System.Drawing.Point(9, 186)
        Me.lvQualification.MinColumnWidth = 50
        Me.lvQualification.MultiSelect = False
        Me.lvQualification.Name = "lvQualification"
        Me.lvQualification.OptionalColumns = ""
        Me.lvQualification.ShowMoreItem = False
        Me.lvQualification.ShowSaveItem = False
        Me.lvQualification.ShowSelectAll = True
        Me.lvQualification.ShowSizeAllColumnsToFit = True
        Me.lvQualification.Size = New System.Drawing.Size(824, 245)
        Me.lvQualification.Sortable = True
        Me.lvQualification.TabIndex = 2
        Me.lvQualification.UseCompatibleStateImageBehavior = False
        Me.lvQualification.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 140
        '
        'colhQualifyGroup
        '
        Me.colhQualifyGroup.Tag = "colhQualifyGroup"
        Me.colhQualifyGroup.Text = "Quali/Award Group"
        Me.colhQualifyGroup.Width = 130
        '
        'colhQualification
        '
        Me.colhQualification.Tag = "colhQualification"
        Me.colhQualification.Text = "Qualification/Award"
        Me.colhQualification.Width = 130
        '
        'colhAwardDate
        '
        Me.colhAwardDate.Tag = "colhAwardDate"
        Me.colhAwardDate.Text = "Start Date"
        Me.colhAwardDate.Width = 90
        '
        'colhAwardToDate
        '
        Me.colhAwardToDate.Tag = "colhAwardToDate"
        Me.colhAwardToDate.Text = "Award Date"
        Me.colhAwardToDate.Width = 85
        '
        'colhInstitute
        '
        Me.colhInstitute.Tag = "colhInstitute"
        Me.colhInstitute.Text = "Institute"
        Me.colhInstitute.Width = 120
        '
        'colhRefNo
        '
        Me.colhRefNo.Tag = "colhRefNo"
        Me.colhRefNo.Text = "Ref. No"
        Me.colhRefNo.Width = 120
        '
        'objdgcolhtranguid
        '
        Me.objdgcolhtranguid.Width = 0
        '
        'objdgcolhempid
        '
        Me.objdgcolhempid.Width = 0
        '
        'colhOperationType
        '
        Me.colhOperationType.Tag = "colhOperationType"
        Me.colhOperationType.Text = "Operation Type"
        Me.colhOperationType.Width = 100
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objtblPanel)
        Me.objFooter.Controls.Add(Me.btnApprovalinfo)
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 437)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(842, 55)
        Me.objFooter.TabIndex = 0
        '
        'btnApprovalinfo
        '
        Me.btnApprovalinfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprovalinfo.BackColor = System.Drawing.Color.White
        Me.btnApprovalinfo.BackgroundImage = CType(resources.GetObject("btnApprovalinfo.BackgroundImage"), System.Drawing.Image)
        Me.btnApprovalinfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprovalinfo.BorderColor = System.Drawing.Color.Empty
        Me.btnApprovalinfo.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprovalinfo.FlatAppearance.BorderSize = 0
        Me.btnApprovalinfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprovalinfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprovalinfo.ForeColor = System.Drawing.Color.Black
        Me.btnApprovalinfo.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprovalinfo.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprovalinfo.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprovalinfo.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprovalinfo.Location = New System.Drawing.Point(8, 13)
        Me.btnApprovalinfo.Name = "btnApprovalinfo"
        Me.btnApprovalinfo.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprovalinfo.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprovalinfo.Size = New System.Drawing.Size(90, 30)
        Me.btnApprovalinfo.TabIndex = 187
        Me.btnApprovalinfo.Text = "&View Detail"
        Me.btnApprovalinfo.UseVisualStyleBackColor = True
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(12, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(106, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperations
        Me.btnOperation.TabIndex = 123
        Me.btnOperation.Text = "Operations"
        '
        'cmnuOperations
        '
        Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuScanAttach, Me.objSep1, Me.mnuImportQualification, Me.mnuExportQualification, Me.mnuExportOtherQualification})
        Me.cmnuOperations.Name = "cmnuOperations"
        Me.cmnuOperations.Size = New System.Drawing.Size(212, 98)
        '
        'mnuScanAttach
        '
        Me.mnuScanAttach.Name = "mnuScanAttach"
        Me.mnuScanAttach.Size = New System.Drawing.Size(211, 22)
        Me.mnuScanAttach.Text = "Scan/Attach"
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(208, 6)
        '
        'mnuImportQualification
        '
        Me.mnuImportQualification.Name = "mnuImportQualification"
        Me.mnuImportQualification.Size = New System.Drawing.Size(211, 22)
        Me.mnuImportQualification.Text = "&Import Qualification"
        '
        'mnuExportQualification
        '
        Me.mnuExportQualification.Name = "mnuExportQualification"
        Me.mnuExportQualification.Size = New System.Drawing.Size(211, 22)
        Me.mnuExportQualification.Text = "E&xport Qualification"
        '
        'mnuExportOtherQualification
        '
        Me.mnuExportOtherQualification.Name = "mnuExportOtherQualification"
        Me.mnuExportOtherQualification.Size = New System.Drawing.Size(211, 22)
        Me.mnuExportOtherQualification.Tag = "mnuExportOtherQualification"
        Me.mnuExportOtherQualification.Text = "Export &Other Qualification"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(641, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 76
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(545, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 75
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(449, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 74
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(737, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 73
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(842, 58)
        Me.eZeeHeader.TabIndex = 1
        Me.eZeeHeader.Title = "Qualification List"
        '
        'objtblPanel
        '
        Me.objtblPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.objtblPanel.ColumnCount = 2
        Me.objtblPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtblPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 178.0!))
        Me.objtblPanel.Controls.Add(Me.lblParentData, 1, 1)
        Me.objtblPanel.Controls.Add(Me.Panel2, 0, 0)
        Me.objtblPanel.Controls.Add(Me.Panel3, 0, 1)
        Me.objtblPanel.Controls.Add(Me.lblPendingData, 1, 0)
        Me.objtblPanel.Location = New System.Drawing.Point(247, 6)
        Me.objtblPanel.Margin = New System.Windows.Forms.Padding(0)
        Me.objtblPanel.Name = "objtblPanel"
        Me.objtblPanel.RowCount = 2
        Me.objtblPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtblPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtblPanel.Size = New System.Drawing.Size(199, 43)
        Me.objtblPanel.TabIndex = 189
        '
        'lblParentData
        '
        Me.lblParentData.BackColor = System.Drawing.Color.Transparent
        Me.lblParentData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblParentData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblParentData.ForeColor = System.Drawing.Color.Black
        Me.lblParentData.Location = New System.Drawing.Point(25, 22)
        Me.lblParentData.Name = "lblParentData"
        Me.lblParentData.Size = New System.Drawing.Size(172, 20)
        Me.lblParentData.TabIndex = 183
        Me.lblParentData.Text = "Parent Detail"
        Me.lblParentData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.PowderBlue
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(4, 4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(14, 14)
        Me.Panel2.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.LightCoral
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(4, 25)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(14, 14)
        Me.Panel3.TabIndex = 1
        '
        'lblPendingData
        '
        Me.lblPendingData.BackColor = System.Drawing.Color.Transparent
        Me.lblPendingData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblPendingData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPendingData.ForeColor = System.Drawing.Color.Black
        Me.lblPendingData.Location = New System.Drawing.Point(25, 1)
        Me.lblPendingData.Name = "lblPendingData"
        Me.lblPendingData.Size = New System.Drawing.Size(172, 20)
        Me.lblPendingData.TabIndex = 182
        Me.lblPendingData.Text = "Pending Approval"
        Me.lblPendingData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmQualificationsList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(842, 492)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.pnlQualificationList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmQualificationsList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Qualification List"
        Me.pnlQualificationList.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperations.ResumeLayout(False)
        Me.objtblPanel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlQualificationList As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents dtpQualificationFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboCertificates As System.Windows.Forms.ComboBox
    Friend WithEvents cboCertificateGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblQualification As System.Windows.Forms.Label
    Friend WithEvents lblQualificationGroup As System.Windows.Forms.Label
    Friend WithEvents lblAwardDate As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents dtpQualificationTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvQualification As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhQualifyGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhQualification As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAwardDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtAwardNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblReferenceNo As System.Windows.Forms.Label
    Friend WithEvents colhInstitute As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRefNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblInstitute As System.Windows.Forms.Label
    Friend WithEvents cboInstitute As System.Windows.Forms.ComboBox
    Friend WithEvents colhAwardToDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuScanAttach As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuImportQualification As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportQualification As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents lblOtherQualificationGrp As System.Windows.Forms.Label
    Friend WithEvents txtOtherQualificationGrp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOtherQualification As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblOtherQualification As System.Windows.Forms.Label
    Friend WithEvents mnuExportOtherQualification As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkOtherQualification As System.Windows.Forms.CheckBox
    Friend WithEvents txtOtherInstitute As System.Windows.Forms.TextBox
    Friend WithEvents lblOtherInstitute As System.Windows.Forms.Label
    Friend WithEvents objdgcolhtranguid As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnApprovalinfo As eZee.Common.eZeeLightButton
    Friend WithEvents objdgcolhempid As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhOperationType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objtblPanel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblParentData As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblPendingData As System.Windows.Forms.Label
End Class
