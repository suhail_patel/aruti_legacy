﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Loan_Advance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.elLoanAdvance = New eZee.Common.eZeeLine
        Me.lvLoanAdvance = New eZee.Common.eZeeListView(Me.components)
        Me.colhVocNo = New System.Windows.Forms.ColumnHeader
        Me.colhEmpCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhLoanCode = New System.Windows.Forms.ColumnHeader
        Me.colhLoanScheme = New System.Windows.Forms.ColumnHeader
        Me.colhLoan_Advance = New System.Windows.Forms.ColumnHeader
        Me.colhAmount = New System.Windows.Forms.ColumnHeader
        Me.colhInstallments = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhStatus = New System.Windows.Forms.ColumnHeader
        Me.colhIsLoan = New System.Windows.Forms.ColumnHeader
        Me.objcolhStatusUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhemployeeunkid = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'elLoanAdvance
        '
        Me.elLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elLoanAdvance.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elLoanAdvance.Location = New System.Drawing.Point(3, 9)
        Me.elLoanAdvance.Name = "elLoanAdvance"
        Me.elLoanAdvance.Size = New System.Drawing.Size(575, 17)
        Me.elLoanAdvance.TabIndex = 319
        Me.elLoanAdvance.Text = "Loan/Advances"
        Me.elLoanAdvance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvLoanAdvance
        '
        Me.lvLoanAdvance.BackColorOnChecked = True
        Me.lvLoanAdvance.ColumnHeaders = Nothing
        Me.lvLoanAdvance.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhVocNo, Me.colhEmpCode, Me.colhEmployee, Me.colhLoanCode, Me.colhLoanScheme, Me.colhLoan_Advance, Me.colhAmount, Me.colhInstallments, Me.colhPeriod, Me.colhStatus, Me.colhIsLoan, Me.objcolhStatusUnkid, Me.objcolhemployeeunkid})
        Me.lvLoanAdvance.CompulsoryColumns = ""
        Me.lvLoanAdvance.FullRowSelect = True
        Me.lvLoanAdvance.GridLines = True
        Me.lvLoanAdvance.GroupingColumn = Nothing
        Me.lvLoanAdvance.HideSelection = False
        Me.lvLoanAdvance.Location = New System.Drawing.Point(6, 32)
        Me.lvLoanAdvance.MinColumnWidth = 50
        Me.lvLoanAdvance.MultiSelect = False
        Me.lvLoanAdvance.Name = "lvLoanAdvance"
        Me.lvLoanAdvance.OptionalColumns = ""
        Me.lvLoanAdvance.ShowMoreItem = False
        Me.lvLoanAdvance.ShowSaveItem = False
        Me.lvLoanAdvance.ShowSelectAll = True
        Me.lvLoanAdvance.ShowSizeAllColumnsToFit = True
        Me.lvLoanAdvance.Size = New System.Drawing.Size(572, 374)
        Me.lvLoanAdvance.Sortable = True
        Me.lvLoanAdvance.TabIndex = 320
        Me.lvLoanAdvance.UseCompatibleStateImageBehavior = False
        Me.lvLoanAdvance.View = System.Windows.Forms.View.Details
        '
        'colhVocNo
        '
        Me.colhVocNo.Tag = "colhVocNo"
        Me.colhVocNo.Text = "Voc #"
        Me.colhVocNo.Width = 100
        '
        'colhEmpCode
        '
        Me.colhEmpCode.Tag = "colhEmpCode"
        Me.colhEmpCode.Text = "Employee Code"
        Me.colhEmpCode.Width = 0
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 0
        '
        'colhLoanCode
        '
        Me.colhLoanCode.Tag = "colhLoanCode"
        Me.colhLoanCode.Text = "Loan Code"
        Me.colhLoanCode.Width = 90
        '
        'colhLoanScheme
        '
        Me.colhLoanScheme.Tag = "colhLoanScheme"
        Me.colhLoanScheme.Text = "Loan Scheme"
        Me.colhLoanScheme.Width = 182
        '
        'colhLoan_Advance
        '
        Me.colhLoan_Advance.Tag = "colhLoan_Advance"
        Me.colhLoan_Advance.Text = "Loan / Advance"
        Me.colhLoan_Advance.Width = 0
        '
        'colhAmount
        '
        Me.colhAmount.Tag = "colhAmount"
        Me.colhAmount.Text = "Amount"
        Me.colhAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhAmount.Width = 0
        '
        'colhInstallments
        '
        Me.colhInstallments.Tag = "colhInstallments"
        Me.colhInstallments.Text = "No. Of Installments"
        Me.colhInstallments.Width = 0
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Assigned Period"
        Me.colhPeriod.Width = 95
        '
        'colhStatus
        '
        Me.colhStatus.Tag = "colhStatus"
        Me.colhStatus.Text = "Status"
        Me.colhStatus.Width = 100
        '
        'colhIsLoan
        '
        Me.colhIsLoan.Tag = "colhIsLoan"
        Me.colhIsLoan.Text = ""
        Me.colhIsLoan.Width = 0
        '
        'objcolhStatusUnkid
        '
        Me.objcolhStatusUnkid.Tag = "objcolhStatusUnkid"
        Me.objcolhStatusUnkid.Text = ""
        Me.objcolhStatusUnkid.Width = 0
        '
        'objcolhemployeeunkid
        '
        Me.objcolhemployeeunkid.Tag = "objcolhemployeeunkid"
        Me.objcolhemployeeunkid.Text = ""
        Me.objcolhemployeeunkid.Width = 0
        '
        'objfrmDiary_Loan_Advance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lvLoanAdvance)
        Me.Controls.Add(Me.elLoanAdvance)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_Loan_Advance"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elLoanAdvance As eZee.Common.eZeeLine
    Friend WithEvents lvLoanAdvance As eZee.Common.eZeeListView
    Friend WithEvents colhVocNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmpCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoanCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoanScheme As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoan_Advance As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInstallments As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIsLoan As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhStatusUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhemployeeunkid As System.Windows.Forms.ColumnHeader
End Class
