﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDairy_Shift_Policy

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDairy_Shift_Policy_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End

            dgvPolicy.Enabled = ConfigParameter._Object._PolicyManagementTNA
            Call Fill_Data()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objfrmDairy_Shift_Policy_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Fill_Data()
        Try
            Dim objShiftTran As New clsEmployee_Shift_Tran
            Dim mdtShiftTran As DataTable
            objShiftTran._EmployeeUnkid = mintEmployeeId
            mdtShiftTran = objShiftTran._SDataTable.Copy
            Dim dView As DataView = mdtShiftTran.DefaultView
            dView.Sort = "effectivedate DESC "
            mdtShiftTran = dView.ToTable
            dgvAssignedShift.AutoGenerateColumns = False
            dgcolhEffective_Date.DataPropertyName = "effdate"
            dgcolhShiftType.DataPropertyName = "shifttype"
            dgcolhShiftName.DataPropertyName = "shiftname"
            dgvAssignedShift.DataSource = mdtShiftTran
            objShiftTran = Nothing

            If ConfigParameter._Object._PolicyManagementTNA = True Then
                Dim mdtPolicyTran As DataTable
                Dim objPolicyTran As New clsemployee_policy_tran
                mdtPolicyTran = objPolicyTran.GetList(mintEmployeeId)
                Dim dView1 As DataView = mdtPolicyTran.DefaultView
                dView1.Sort = "effectivedate DESC "
                mdtPolicyTran = dView1.ToTable
                dgvPolicy.AutoGenerateColumns = False
                dgcolhPEffective_Date.DataPropertyName = "effdate"
                dgcolhPolicy.DataPropertyName = "policyname"
                dgvPolicy.DataSource = mdtPolicyTran
                objPolicyTran = Nothing
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)

            Me.elShift.Text = Language._Object.getCaption(Me.elShift.Name, Me.elShift.Text)
            Me.elPolicy.Text = Language._Object.getCaption(Me.elPolicy.Name, Me.elPolicy.Text)
            Me.dgcolhEffective_Date.HeaderText = Language._Object.getCaption(Me.dgcolhEffective_Date.Name, Me.dgcolhEffective_Date.HeaderText)
            Me.dgcolhShiftType.HeaderText = Language._Object.getCaption(Me.dgcolhShiftType.Name, Me.dgcolhShiftType.HeaderText)
            Me.dgcolhShiftName.HeaderText = Language._Object.getCaption(Me.dgcolhShiftName.Name, Me.dgcolhShiftName.HeaderText)
            Me.dgcolhPEffective_Date.HeaderText = Language._Object.getCaption(Me.dgcolhPEffective_Date.Name, Me.dgcolhPEffective_Date.HeaderText)
            Me.dgcolhPolicy.HeaderText = Language._Object.getCaption(Me.dgcolhPolicy.Name, Me.dgcolhPolicy.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class