﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeDiary
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeDiary))
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objSpcDiary = New System.Windows.Forms.SplitContainer
        Me.pnlData = New System.Windows.Forms.Panel
        Me.flpDiaryOperations = New System.Windows.Forms.FlowLayoutPanel
        Me.btnDetails = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnShift_Policy = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDates = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddress = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDependants = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOccasion = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExperience = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReferences = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCompanyAssets = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnBenefits = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnLeave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnHolidays = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnTrainingEnrollment = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnPerformance = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnLoan_Advances = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDiscipline = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEmployeeMovement = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEmployeeMembership = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEmployeeBanks = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.pnlMainInfo.SuspendLayout()
        Me.objSpcDiary.Panel1.SuspendLayout()
        Me.objSpcDiary.Panel2.SuspendLayout()
        Me.objSpcDiary.SuspendLayout()
        Me.flpDiaryOperations.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(652, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(96, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objSpcDiary)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(760, 420)
        Me.pnlMainInfo.TabIndex = 3
        '
        'objSpcDiary
        '
        Me.objSpcDiary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objSpcDiary.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objSpcDiary.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objSpcDiary.IsSplitterFixed = True
        Me.objSpcDiary.Location = New System.Drawing.Point(0, 0)
        Me.objSpcDiary.Name = "objSpcDiary"
        '
        'objSpcDiary.Panel1
        '
        Me.objSpcDiary.Panel1.Controls.Add(Me.flpDiaryOperations)
        '
        'objSpcDiary.Panel2
        '
        Me.objSpcDiary.Panel2.Controls.Add(Me.pnlData)
        Me.objSpcDiary.Size = New System.Drawing.Size(760, 420)
        Me.objSpcDiary.SplitterDistance = 175
        Me.objSpcDiary.SplitterWidth = 2
        Me.objSpcDiary.TabIndex = 0
        '
        'pnlData
        '
        Me.pnlData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlData.Location = New System.Drawing.Point(0, 0)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(581, 418)
        Me.pnlData.TabIndex = 0
        '
        'flpDiaryOperations
        '
        Me.flpDiaryOperations.AutoScroll = True
        Me.flpDiaryOperations.Controls.Add(Me.btnDetails)
        Me.flpDiaryOperations.Controls.Add(Me.btnShift_Policy)
        Me.flpDiaryOperations.Controls.Add(Me.btnDates)
        Me.flpDiaryOperations.Controls.Add(Me.btnAddress)
        Me.flpDiaryOperations.Controls.Add(Me.btnDependants)
        Me.flpDiaryOperations.Controls.Add(Me.btnOccasion)
        Me.flpDiaryOperations.Controls.Add(Me.btnExperience)
        Me.flpDiaryOperations.Controls.Add(Me.btnReferences)
        Me.flpDiaryOperations.Controls.Add(Me.btnCompanyAssets)
        Me.flpDiaryOperations.Controls.Add(Me.btnBenefits)
        Me.flpDiaryOperations.Controls.Add(Me.btnLeave)
        Me.flpDiaryOperations.Controls.Add(Me.btnHolidays)
        Me.flpDiaryOperations.Controls.Add(Me.btnTrainingEnrollment)
        Me.flpDiaryOperations.Controls.Add(Me.btnPerformance)
        Me.flpDiaryOperations.Controls.Add(Me.btnLoan_Advances)
        Me.flpDiaryOperations.Controls.Add(Me.btnDiscipline)
        Me.flpDiaryOperations.Controls.Add(Me.btnEmployeeMovement)
        Me.flpDiaryOperations.Controls.Add(Me.btnEmployeeMembership)
        Me.flpDiaryOperations.Controls.Add(Me.btnEmployeeBanks)
        Me.flpDiaryOperations.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flpDiaryOperations.Location = New System.Drawing.Point(0, 0)
        Me.flpDiaryOperations.Name = "flpDiaryOperations"
        Me.flpDiaryOperations.Size = New System.Drawing.Size(173, 418)
        Me.flpDiaryOperations.TabIndex = 0
        '
        'btnDetails
        '
        Me.btnDetails.BackColor = System.Drawing.Color.White
        Me.btnDetails.BackgroundImage = CType(resources.GetObject("btnDetails.BackgroundImage"), System.Drawing.Image)
        Me.btnDetails.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDetails.BorderColor = System.Drawing.Color.Empty
        Me.btnDetails.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnDetails.FlatAppearance.BorderSize = 0
        Me.btnDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDetails.ForeColor = System.Drawing.Color.Black
        Me.btnDetails.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDetails.GradientForeColor = System.Drawing.Color.Black
        Me.btnDetails.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDetails.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDetails.Location = New System.Drawing.Point(3, 3)
        Me.btnDetails.Name = "btnDetails"
        Me.btnDetails.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDetails.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDetails.Size = New System.Drawing.Size(149, 40)
        Me.btnDetails.TabIndex = 6
        Me.btnDetails.Text = "Details"
        Me.btnDetails.UseVisualStyleBackColor = False
        '
        'btnShift_Policy
        '
        Me.btnShift_Policy.BackColor = System.Drawing.Color.White
        Me.btnShift_Policy.BackgroundImage = CType(resources.GetObject("btnShift_Policy.BackgroundImage"), System.Drawing.Image)
        Me.btnShift_Policy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnShift_Policy.BorderColor = System.Drawing.Color.Empty
        Me.btnShift_Policy.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnShift_Policy.FlatAppearance.BorderSize = 0
        Me.btnShift_Policy.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnShift_Policy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShift_Policy.ForeColor = System.Drawing.Color.Black
        Me.btnShift_Policy.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnShift_Policy.GradientForeColor = System.Drawing.Color.Black
        Me.btnShift_Policy.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnShift_Policy.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnShift_Policy.Location = New System.Drawing.Point(3, 49)
        Me.btnShift_Policy.Name = "btnShift_Policy"
        Me.btnShift_Policy.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnShift_Policy.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnShift_Policy.Size = New System.Drawing.Size(149, 40)
        Me.btnShift_Policy.TabIndex = 21
        Me.btnShift_Policy.Text = "Shift/Policy Details"
        Me.btnShift_Policy.UseVisualStyleBackColor = False
        '
        'btnDates
        '
        Me.btnDates.BackColor = System.Drawing.Color.White
        Me.btnDates.BackgroundImage = CType(resources.GetObject("btnDates.BackgroundImage"), System.Drawing.Image)
        Me.btnDates.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDates.BorderColor = System.Drawing.Color.Empty
        Me.btnDates.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnDates.FlatAppearance.BorderSize = 0
        Me.btnDates.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDates.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDates.ForeColor = System.Drawing.Color.Black
        Me.btnDates.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDates.GradientForeColor = System.Drawing.Color.Black
        Me.btnDates.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDates.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDates.Location = New System.Drawing.Point(3, 95)
        Me.btnDates.Name = "btnDates"
        Me.btnDates.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDates.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDates.Size = New System.Drawing.Size(149, 40)
        Me.btnDates.TabIndex = 7
        Me.btnDates.Text = "Dates"
        Me.btnDates.UseVisualStyleBackColor = True
        '
        'btnAddress
        '
        Me.btnAddress.BackColor = System.Drawing.Color.White
        Me.btnAddress.BackgroundImage = CType(resources.GetObject("btnAddress.BackgroundImage"), System.Drawing.Image)
        Me.btnAddress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddress.BorderColor = System.Drawing.Color.Empty
        Me.btnAddress.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnAddress.FlatAppearance.BorderSize = 0
        Me.btnAddress.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddress.ForeColor = System.Drawing.Color.Black
        Me.btnAddress.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddress.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddress.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddress.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddress.Location = New System.Drawing.Point(3, 141)
        Me.btnAddress.Name = "btnAddress"
        Me.btnAddress.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddress.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddress.Size = New System.Drawing.Size(149, 40)
        Me.btnAddress.TabIndex = 10
        Me.btnAddress.Text = "Address"
        Me.btnAddress.UseVisualStyleBackColor = True
        '
        'btnDependants
        '
        Me.btnDependants.BackColor = System.Drawing.Color.White
        Me.btnDependants.BackgroundImage = CType(resources.GetObject("btnDependants.BackgroundImage"), System.Drawing.Image)
        Me.btnDependants.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDependants.BorderColor = System.Drawing.Color.Empty
        Me.btnDependants.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnDependants.FlatAppearance.BorderSize = 0
        Me.btnDependants.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDependants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDependants.ForeColor = System.Drawing.Color.Black
        Me.btnDependants.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDependants.GradientForeColor = System.Drawing.Color.Black
        Me.btnDependants.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDependants.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDependants.Location = New System.Drawing.Point(3, 187)
        Me.btnDependants.Name = "btnDependants"
        Me.btnDependants.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDependants.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDependants.Size = New System.Drawing.Size(149, 40)
        Me.btnDependants.TabIndex = 11
        Me.btnDependants.Text = "Dependants"
        Me.btnDependants.UseVisualStyleBackColor = True
        '
        'btnOccasion
        '
        Me.btnOccasion.BackColor = System.Drawing.Color.White
        Me.btnOccasion.BackgroundImage = CType(resources.GetObject("btnOccasion.BackgroundImage"), System.Drawing.Image)
        Me.btnOccasion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOccasion.BorderColor = System.Drawing.Color.Empty
        Me.btnOccasion.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnOccasion.FlatAppearance.BorderSize = 0
        Me.btnOccasion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOccasion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOccasion.ForeColor = System.Drawing.Color.Black
        Me.btnOccasion.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOccasion.GradientForeColor = System.Drawing.Color.Black
        Me.btnOccasion.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOccasion.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOccasion.Location = New System.Drawing.Point(3, 233)
        Me.btnOccasion.Name = "btnOccasion"
        Me.btnOccasion.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOccasion.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOccasion.Size = New System.Drawing.Size(149, 40)
        Me.btnOccasion.TabIndex = 8
        Me.btnOccasion.Text = "Qualifications"
        Me.btnOccasion.UseVisualStyleBackColor = True
        '
        'btnExperience
        '
        Me.btnExperience.BackColor = System.Drawing.Color.White
        Me.btnExperience.BackgroundImage = CType(resources.GetObject("btnExperience.BackgroundImage"), System.Drawing.Image)
        Me.btnExperience.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExperience.BorderColor = System.Drawing.Color.Empty
        Me.btnExperience.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnExperience.FlatAppearance.BorderSize = 0
        Me.btnExperience.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExperience.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExperience.ForeColor = System.Drawing.Color.Black
        Me.btnExperience.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExperience.GradientForeColor = System.Drawing.Color.Black
        Me.btnExperience.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExperience.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExperience.Location = New System.Drawing.Point(3, 279)
        Me.btnExperience.Name = "btnExperience"
        Me.btnExperience.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExperience.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExperience.Size = New System.Drawing.Size(149, 40)
        Me.btnExperience.TabIndex = 13
        Me.btnExperience.Text = "Experiences"
        Me.btnExperience.UseVisualStyleBackColor = True
        '
        'btnReferences
        '
        Me.btnReferences.BackColor = System.Drawing.Color.White
        Me.btnReferences.BackgroundImage = CType(resources.GetObject("btnReferences.BackgroundImage"), System.Drawing.Image)
        Me.btnReferences.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReferences.BorderColor = System.Drawing.Color.Empty
        Me.btnReferences.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnReferences.FlatAppearance.BorderSize = 0
        Me.btnReferences.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReferences.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReferences.ForeColor = System.Drawing.Color.Black
        Me.btnReferences.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReferences.GradientForeColor = System.Drawing.Color.Black
        Me.btnReferences.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReferences.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReferences.Location = New System.Drawing.Point(3, 325)
        Me.btnReferences.Name = "btnReferences"
        Me.btnReferences.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReferences.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReferences.Size = New System.Drawing.Size(149, 40)
        Me.btnReferences.TabIndex = 14
        Me.btnReferences.Text = "References"
        Me.btnReferences.UseVisualStyleBackColor = True
        '
        'btnCompanyAssets
        '
        Me.btnCompanyAssets.BackColor = System.Drawing.Color.White
        Me.btnCompanyAssets.BackgroundImage = CType(resources.GetObject("btnCompanyAssets.BackgroundImage"), System.Drawing.Image)
        Me.btnCompanyAssets.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCompanyAssets.BorderColor = System.Drawing.Color.Empty
        Me.btnCompanyAssets.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnCompanyAssets.FlatAppearance.BorderSize = 0
        Me.btnCompanyAssets.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCompanyAssets.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCompanyAssets.ForeColor = System.Drawing.Color.Black
        Me.btnCompanyAssets.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCompanyAssets.GradientForeColor = System.Drawing.Color.Black
        Me.btnCompanyAssets.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCompanyAssets.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCompanyAssets.Location = New System.Drawing.Point(3, 371)
        Me.btnCompanyAssets.Name = "btnCompanyAssets"
        Me.btnCompanyAssets.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCompanyAssets.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCompanyAssets.Size = New System.Drawing.Size(149, 40)
        Me.btnCompanyAssets.TabIndex = 15
        Me.btnCompanyAssets.Text = "Company Assets"
        Me.btnCompanyAssets.UseVisualStyleBackColor = True
        '
        'btnBenefits
        '
        Me.btnBenefits.BackColor = System.Drawing.Color.White
        Me.btnBenefits.BackgroundImage = CType(resources.GetObject("btnBenefits.BackgroundImage"), System.Drawing.Image)
        Me.btnBenefits.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBenefits.BorderColor = System.Drawing.Color.Empty
        Me.btnBenefits.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnBenefits.FlatAppearance.BorderSize = 0
        Me.btnBenefits.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBenefits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBenefits.ForeColor = System.Drawing.Color.Black
        Me.btnBenefits.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnBenefits.GradientForeColor = System.Drawing.Color.Black
        Me.btnBenefits.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBenefits.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnBenefits.Location = New System.Drawing.Point(3, 417)
        Me.btnBenefits.Name = "btnBenefits"
        Me.btnBenefits.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBenefits.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnBenefits.Size = New System.Drawing.Size(149, 40)
        Me.btnBenefits.TabIndex = 16
        Me.btnBenefits.Text = "Benefits"
        Me.btnBenefits.UseVisualStyleBackColor = True
        '
        'btnLeave
        '
        Me.btnLeave.BackColor = System.Drawing.Color.White
        Me.btnLeave.BackgroundImage = CType(resources.GetObject("btnLeave.BackgroundImage"), System.Drawing.Image)
        Me.btnLeave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnLeave.BorderColor = System.Drawing.Color.Empty
        Me.btnLeave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnLeave.FlatAppearance.BorderSize = 0
        Me.btnLeave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLeave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLeave.ForeColor = System.Drawing.Color.Black
        Me.btnLeave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnLeave.GradientForeColor = System.Drawing.Color.Black
        Me.btnLeave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLeave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnLeave.Location = New System.Drawing.Point(3, 463)
        Me.btnLeave.Name = "btnLeave"
        Me.btnLeave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLeave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnLeave.Size = New System.Drawing.Size(149, 40)
        Me.btnLeave.TabIndex = 5
        Me.btnLeave.Text = "Leaves"
        Me.btnLeave.UseVisualStyleBackColor = True
        '
        'btnHolidays
        '
        Me.btnHolidays.BackColor = System.Drawing.Color.White
        Me.btnHolidays.BackgroundImage = CType(resources.GetObject("btnHolidays.BackgroundImage"), System.Drawing.Image)
        Me.btnHolidays.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnHolidays.BorderColor = System.Drawing.Color.Empty
        Me.btnHolidays.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnHolidays.FlatAppearance.BorderSize = 0
        Me.btnHolidays.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnHolidays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHolidays.ForeColor = System.Drawing.Color.Black
        Me.btnHolidays.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnHolidays.GradientForeColor = System.Drawing.Color.Black
        Me.btnHolidays.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnHolidays.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnHolidays.Location = New System.Drawing.Point(3, 509)
        Me.btnHolidays.Name = "btnHolidays"
        Me.btnHolidays.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnHolidays.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnHolidays.Size = New System.Drawing.Size(149, 40)
        Me.btnHolidays.TabIndex = 2
        Me.btnHolidays.Text = "Holidays"
        Me.btnHolidays.UseVisualStyleBackColor = True
        '
        'btnTrainingEnrollment
        '
        Me.btnTrainingEnrollment.BackColor = System.Drawing.Color.White
        Me.btnTrainingEnrollment.BackgroundImage = CType(resources.GetObject("btnTrainingEnrollment.BackgroundImage"), System.Drawing.Image)
        Me.btnTrainingEnrollment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnTrainingEnrollment.BorderColor = System.Drawing.Color.Empty
        Me.btnTrainingEnrollment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnTrainingEnrollment.FlatAppearance.BorderSize = 0
        Me.btnTrainingEnrollment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTrainingEnrollment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTrainingEnrollment.ForeColor = System.Drawing.Color.Black
        Me.btnTrainingEnrollment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnTrainingEnrollment.GradientForeColor = System.Drawing.Color.Black
        Me.btnTrainingEnrollment.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTrainingEnrollment.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnTrainingEnrollment.Location = New System.Drawing.Point(3, 555)
        Me.btnTrainingEnrollment.Name = "btnTrainingEnrollment"
        Me.btnTrainingEnrollment.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTrainingEnrollment.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnTrainingEnrollment.Size = New System.Drawing.Size(149, 40)
        Me.btnTrainingEnrollment.TabIndex = 3
        Me.btnTrainingEnrollment.Text = "Training Enrollment"
        Me.btnTrainingEnrollment.UseVisualStyleBackColor = True
        '
        'btnPerformance
        '
        Me.btnPerformance.BackColor = System.Drawing.Color.White
        Me.btnPerformance.BackgroundImage = CType(resources.GetObject("btnPerformance.BackgroundImage"), System.Drawing.Image)
        Me.btnPerformance.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPerformance.BorderColor = System.Drawing.Color.Empty
        Me.btnPerformance.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnPerformance.FlatAppearance.BorderSize = 0
        Me.btnPerformance.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPerformance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPerformance.ForeColor = System.Drawing.Color.Black
        Me.btnPerformance.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPerformance.GradientForeColor = System.Drawing.Color.Black
        Me.btnPerformance.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPerformance.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPerformance.Location = New System.Drawing.Point(3, 601)
        Me.btnPerformance.Name = "btnPerformance"
        Me.btnPerformance.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPerformance.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPerformance.Size = New System.Drawing.Size(149, 40)
        Me.btnPerformance.TabIndex = 17
        Me.btnPerformance.Text = "Performance and Evaluation"
        Me.btnPerformance.UseVisualStyleBackColor = True
        Me.btnPerformance.Visible = False
        '
        'btnLoan_Advances
        '
        Me.btnLoan_Advances.BackColor = System.Drawing.Color.White
        Me.btnLoan_Advances.BackgroundImage = CType(resources.GetObject("btnLoan_Advances.BackgroundImage"), System.Drawing.Image)
        Me.btnLoan_Advances.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnLoan_Advances.BorderColor = System.Drawing.Color.Empty
        Me.btnLoan_Advances.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnLoan_Advances.FlatAppearance.BorderSize = 0
        Me.btnLoan_Advances.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoan_Advances.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLoan_Advances.ForeColor = System.Drawing.Color.Black
        Me.btnLoan_Advances.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnLoan_Advances.GradientForeColor = System.Drawing.Color.Black
        Me.btnLoan_Advances.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLoan_Advances.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnLoan_Advances.Location = New System.Drawing.Point(3, 647)
        Me.btnLoan_Advances.Name = "btnLoan_Advances"
        Me.btnLoan_Advances.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLoan_Advances.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnLoan_Advances.Size = New System.Drawing.Size(149, 40)
        Me.btnLoan_Advances.TabIndex = 18
        Me.btnLoan_Advances.Text = "Loan/Advances"
        Me.btnLoan_Advances.UseVisualStyleBackColor = True
        '
        'btnDiscipline
        '
        Me.btnDiscipline.BackColor = System.Drawing.Color.White
        Me.btnDiscipline.BackgroundImage = CType(resources.GetObject("btnDiscipline.BackgroundImage"), System.Drawing.Image)
        Me.btnDiscipline.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDiscipline.BorderColor = System.Drawing.Color.Empty
        Me.btnDiscipline.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnDiscipline.FlatAppearance.BorderSize = 0
        Me.btnDiscipline.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDiscipline.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDiscipline.ForeColor = System.Drawing.Color.Black
        Me.btnDiscipline.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDiscipline.GradientForeColor = System.Drawing.Color.Black
        Me.btnDiscipline.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDiscipline.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDiscipline.Location = New System.Drawing.Point(3, 693)
        Me.btnDiscipline.Name = "btnDiscipline"
        Me.btnDiscipline.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDiscipline.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDiscipline.Size = New System.Drawing.Size(149, 40)
        Me.btnDiscipline.TabIndex = 4
        Me.btnDiscipline.Text = "Discipline"
        Me.btnDiscipline.UseVisualStyleBackColor = True
        '
        'btnEmployeeMovement
        '
        Me.btnEmployeeMovement.BackColor = System.Drawing.Color.White
        Me.btnEmployeeMovement.BackgroundImage = CType(resources.GetObject("btnEmployeeMovement.BackgroundImage"), System.Drawing.Image)
        Me.btnEmployeeMovement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmployeeMovement.BorderColor = System.Drawing.Color.Empty
        Me.btnEmployeeMovement.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnEmployeeMovement.FlatAppearance.BorderSize = 0
        Me.btnEmployeeMovement.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmployeeMovement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmployeeMovement.ForeColor = System.Drawing.Color.Black
        Me.btnEmployeeMovement.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmployeeMovement.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmployeeMovement.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmployeeMovement.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmployeeMovement.Location = New System.Drawing.Point(3, 739)
        Me.btnEmployeeMovement.Name = "btnEmployeeMovement"
        Me.btnEmployeeMovement.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmployeeMovement.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmployeeMovement.Size = New System.Drawing.Size(149, 40)
        Me.btnEmployeeMovement.TabIndex = 9
        Me.btnEmployeeMovement.Text = "Employee Movement"
        Me.btnEmployeeMovement.UseVisualStyleBackColor = True
        '
        'btnEmployeeMembership
        '
        Me.btnEmployeeMembership.BackColor = System.Drawing.Color.White
        Me.btnEmployeeMembership.BackgroundImage = CType(resources.GetObject("btnEmployeeMembership.BackgroundImage"), System.Drawing.Image)
        Me.btnEmployeeMembership.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmployeeMembership.BorderColor = System.Drawing.Color.Empty
        Me.btnEmployeeMembership.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnEmployeeMembership.FlatAppearance.BorderSize = 0
        Me.btnEmployeeMembership.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmployeeMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmployeeMembership.ForeColor = System.Drawing.Color.Black
        Me.btnEmployeeMembership.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmployeeMembership.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmployeeMembership.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmployeeMembership.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmployeeMembership.Location = New System.Drawing.Point(3, 785)
        Me.btnEmployeeMembership.Name = "btnEmployeeMembership"
        Me.btnEmployeeMembership.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmployeeMembership.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmployeeMembership.Size = New System.Drawing.Size(149, 40)
        Me.btnEmployeeMembership.TabIndex = 19
        Me.btnEmployeeMembership.Text = "Membership/Identities"
        Me.btnEmployeeMembership.UseVisualStyleBackColor = True
        '
        'btnEmployeeBanks
        '
        Me.btnEmployeeBanks.BackColor = System.Drawing.Color.White
        Me.btnEmployeeBanks.BackgroundImage = CType(resources.GetObject("btnEmployeeBanks.BackgroundImage"), System.Drawing.Image)
        Me.btnEmployeeBanks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmployeeBanks.BorderColor = System.Drawing.Color.Empty
        Me.btnEmployeeBanks.ButtonType = eZee.Common.eZeeLightButton.enButtonType.RadioButton
        Me.btnEmployeeBanks.FlatAppearance.BorderSize = 0
        Me.btnEmployeeBanks.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmployeeBanks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmployeeBanks.ForeColor = System.Drawing.Color.Black
        Me.btnEmployeeBanks.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmployeeBanks.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmployeeBanks.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmployeeBanks.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmployeeBanks.Location = New System.Drawing.Point(3, 831)
        Me.btnEmployeeBanks.Name = "btnEmployeeBanks"
        Me.btnEmployeeBanks.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmployeeBanks.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmployeeBanks.Size = New System.Drawing.Size(149, 40)
        Me.btnEmployeeBanks.TabIndex = 20
        Me.btnEmployeeBanks.Text = "Employee Banks"
        Me.btnEmployeeBanks.UseVisualStyleBackColor = True
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 420)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(760, 50)
        Me.EZeeFooter1.TabIndex = 4
        '
        'frmEmployeeDiary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(760, 470)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeDiary"
        Me.RightToLeftLayout = True
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Diary"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objSpcDiary.Panel1.ResumeLayout(False)
        Me.objSpcDiary.Panel2.ResumeLayout(False)
        Me.objSpcDiary.ResumeLayout(False)
        Me.flpDiaryOperations.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objSpcDiary As System.Windows.Forms.SplitContainer
    Friend WithEvents flpDiaryOperations As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents btnDetails As eZee.Common.eZeeLightButton
    Friend WithEvents btnLeave As eZee.Common.eZeeLightButton
    Friend WithEvents btnOccasion As eZee.Common.eZeeLightButton
    Friend WithEvents btnDates As eZee.Common.eZeeLightButton
    Friend WithEvents btnHolidays As eZee.Common.eZeeLightButton
    Friend WithEvents btnDiscipline As eZee.Common.eZeeLightButton
    Friend WithEvents btnTrainingEnrollment As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeMovement As eZee.Common.eZeeLightButton
    Friend WithEvents btnAddress As eZee.Common.eZeeLightButton
    Friend WithEvents btnDependants As eZee.Common.eZeeLightButton
    Friend WithEvents btnExperience As eZee.Common.eZeeLightButton
    Friend WithEvents btnReferences As eZee.Common.eZeeLightButton
    Friend WithEvents btnCompanyAssets As eZee.Common.eZeeLightButton
    Friend WithEvents btnBenefits As eZee.Common.eZeeLightButton
    Friend WithEvents btnPerformance As eZee.Common.eZeeLightButton
    Friend WithEvents btnLoan_Advances As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeMembership As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmployeeBanks As eZee.Common.eZeeLightButton
    Friend WithEvents btnShift_Policy As eZee.Common.eZeeLightButton
End Class
