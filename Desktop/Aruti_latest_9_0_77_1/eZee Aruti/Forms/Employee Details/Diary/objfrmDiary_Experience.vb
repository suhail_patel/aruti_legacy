﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Experience

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDiary_Experience_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            lvJobHistory.GridLines = False
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_Experience_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Dim objJobExperience As New clsJobExperience_tran
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objJobExperience.GetList("JobHistory")

            'If CInt(mintEmployeeId) > 0 Then
            '    StrSearching &= "AND EmpId = " & CInt(mintEmployeeId) & " "
            'End If

            'If StrSearching.Length > 0 Then
            '    StrSearching = StrSearching.Substring(3)
            '    dtTable = New DataView(dsList.Tables("JobHistory"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsList.Tables("JobHistory"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            'End If

            If CInt(mintEmployeeId) > 0 Then
                StrSearching &= "AND hremployee_master.employeeunkid = " & CInt(mintEmployeeId) & " "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            dsList = objJobExperience.GetList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                              ConfigParameter._Object._IsIncludeInactiveEmp, "JobHistory", , StrSearching)

            dtTable = New DataView(dsList.Tables("JobHistory"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            'S.SANDEEP [04 JUN 2015] -- END

            lvJobHistory.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("EmpName").ToString
                lvItem.SubItems.Add(dtRow.Item("Company").ToString)
                lvItem.SubItems.Add(dtRow.Item("Jobs").ToString)
                If dtRow.Item("StartDate").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("StartDate").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If
                If dtRow.Item("EndDate").ToString.Trim.Length > 0 Then
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("EndDate").ToString).ToShortDateString)
                Else
                    lvItem.SubItems.Add("")
                End If
                lvItem.SubItems.Add(dtRow.Item("Supervisor").ToString)
                lvItem.SubItems.Add(dtRow.Item("Remark").ToString)

                lvItem.Tag = dtRow.Item("ExpId")

                lvJobHistory.Items.Add(lvItem)

                lvItem = Nothing
            Next

            If lvJobHistory.Items.Count > 5 Then
                colhRemark.Width = 100 - 20
            Else
                colhRemark.Width = 100
            End If

            lvJobHistory.GroupingColumn = colhEmployee
            lvJobHistory.DisplayGroups(True)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elEmployeeExperience.Text = Language._Object.getCaption(Me.elEmployeeExperience.Name, Me.elEmployeeExperience.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhCompany.Text = Language._Object.getCaption(CStr(Me.colhCompany.Tag), Me.colhCompany.Text)
			Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
			Me.colhStartDate.Text = Language._Object.getCaption(CStr(Me.colhStartDate.Tag), Me.colhStartDate.Text)
			Me.colhEndDate.Text = Language._Object.getCaption(CStr(Me.colhEndDate.Tag), Me.colhEndDate.Text)
			Me.colhSupervisor.Text = Language._Object.getCaption(CStr(Me.colhSupervisor.Tag), Me.colhSupervisor.Text)
			Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class