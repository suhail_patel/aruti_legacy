﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Holiday
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.elPreviousHolidays = New eZee.Common.eZeeLine
        Me.lvOldHoliday = New System.Windows.Forms.ListView
        Me.colhPreviousHoliday = New System.Windows.Forms.ColumnHeader
        Me.colhPreviousdates = New System.Windows.Forms.ColumnHeader
        Me.elUpcomingHoliday = New eZee.Common.eZeeLine
        Me.lvNewHoliday = New System.Windows.Forms.ListView
        Me.colhUpcomingHolidays = New System.Windows.Forms.ColumnHeader
        Me.colhUpcomingdate = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'elPreviousHolidays
        '
        Me.elPreviousHolidays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elPreviousHolidays.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elPreviousHolidays.Location = New System.Drawing.Point(2, 6)
        Me.elPreviousHolidays.Name = "elPreviousHolidays"
        Me.elPreviousHolidays.Size = New System.Drawing.Size(575, 17)
        Me.elPreviousHolidays.TabIndex = 309
        Me.elPreviousHolidays.Text = "Pervious Holidays"
        Me.elPreviousHolidays.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvOldHoliday
        '
        Me.lvOldHoliday.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhPreviousHoliday, Me.colhPreviousdates})
        Me.lvOldHoliday.FullRowSelect = True
        Me.lvOldHoliday.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvOldHoliday.Location = New System.Drawing.Point(5, 27)
        Me.lvOldHoliday.MultiSelect = False
        Me.lvOldHoliday.Name = "lvOldHoliday"
        Me.lvOldHoliday.Size = New System.Drawing.Size(572, 176)
        Me.lvOldHoliday.TabIndex = 310
        Me.lvOldHoliday.UseCompatibleStateImageBehavior = False
        Me.lvOldHoliday.View = System.Windows.Forms.View.Details
        '
        'colhPreviousHoliday
        '
        Me.colhPreviousHoliday.Tag = "colhPreviousHoliday"
        Me.colhPreviousHoliday.Text = "Holidays"
        Me.colhPreviousHoliday.Width = 450
        '
        'colhPreviousdates
        '
        Me.colhPreviousdates.Tag = "colhPreviousdates"
        Me.colhPreviousdates.Text = "Date"
        Me.colhPreviousdates.Width = 118
        '
        'elUpcomingHoliday
        '
        Me.elUpcomingHoliday.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elUpcomingHoliday.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elUpcomingHoliday.Location = New System.Drawing.Point(2, 206)
        Me.elUpcomingHoliday.Name = "elUpcomingHoliday"
        Me.elUpcomingHoliday.Size = New System.Drawing.Size(575, 17)
        Me.elUpcomingHoliday.TabIndex = 311
        Me.elUpcomingHoliday.Text = "Upcoming Holidays"
        Me.elUpcomingHoliday.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvNewHoliday
        '
        Me.lvNewHoliday.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhUpcomingHolidays, Me.colhUpcomingdate})
        Me.lvNewHoliday.FullRowSelect = True
        Me.lvNewHoliday.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvNewHoliday.Location = New System.Drawing.Point(5, 226)
        Me.lvNewHoliday.MultiSelect = False
        Me.lvNewHoliday.Name = "lvNewHoliday"
        Me.lvNewHoliday.Size = New System.Drawing.Size(572, 180)
        Me.lvNewHoliday.TabIndex = 312
        Me.lvNewHoliday.UseCompatibleStateImageBehavior = False
        Me.lvNewHoliday.View = System.Windows.Forms.View.Details
        '
        'colhUpcomingHolidays
        '
        Me.colhUpcomingHolidays.Tag = "colhUpcomingHolidays"
        Me.colhUpcomingHolidays.Text = "Holidays"
        Me.colhUpcomingHolidays.Width = 450
        '
        'colhUpcomingdate
        '
        Me.colhUpcomingdate.Tag = "colhUpcomingdate"
        Me.colhUpcomingdate.Text = "Date"
        Me.colhUpcomingdate.Width = 118
        '
        'objfrmDiary_Holiday
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lvNewHoliday)
        Me.Controls.Add(Me.elUpcomingHoliday)
        Me.Controls.Add(Me.lvOldHoliday)
        Me.Controls.Add(Me.elPreviousHolidays)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_Holiday"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elPreviousHolidays As eZee.Common.eZeeLine
    Friend WithEvents lvOldHoliday As System.Windows.Forms.ListView
    Friend WithEvents elUpcomingHoliday As eZee.Common.eZeeLine
    Friend WithEvents lvNewHoliday As System.Windows.Forms.ListView
    Friend WithEvents colhPreviousHoliday As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhPreviousdates As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUpcomingHolidays As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUpcomingdate As System.Windows.Forms.ColumnHeader
End Class
