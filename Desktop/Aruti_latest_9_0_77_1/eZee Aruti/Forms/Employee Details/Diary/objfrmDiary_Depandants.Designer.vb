﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Depandants
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.elEmployeeDepandents = New eZee.Common.eZeeLine
        Me.lvDepandants_Benefice = New eZee.Common.eZeeListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhDBName = New System.Windows.Forms.ColumnHeader
        Me.colhRelation = New System.Windows.Forms.ColumnHeader
        Me.colhCountry = New System.Windows.Forms.ColumnHeader
        Me.colhIdNo = New System.Windows.Forms.ColumnHeader
        Me.colhGender = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'elEmployeeDepandents
        '
        Me.elEmployeeDepandents.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elEmployeeDepandents.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elEmployeeDepandents.Location = New System.Drawing.Point(3, 9)
        Me.elEmployeeDepandents.Name = "elEmployeeDepandents"
        Me.elEmployeeDepandents.Size = New System.Drawing.Size(575, 17)
        Me.elEmployeeDepandents.TabIndex = 313
        Me.elEmployeeDepandents.Text = "Employee Dependants"
        Me.elEmployeeDepandents.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lvDepandants_Benefice
        '
        Me.lvDepandants_Benefice.BackColorOnChecked = True
        Me.lvDepandants_Benefice.ColumnHeaders = Nothing
        Me.lvDepandants_Benefice.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhDBName, Me.colhRelation, Me.colhCountry, Me.colhIdNo, Me.colhGender})
        Me.lvDepandants_Benefice.CompulsoryColumns = ""
        Me.lvDepandants_Benefice.FullRowSelect = True
        Me.lvDepandants_Benefice.GridLines = True
        Me.lvDepandants_Benefice.GroupingColumn = Nothing
        Me.lvDepandants_Benefice.HideSelection = False
        Me.lvDepandants_Benefice.Location = New System.Drawing.Point(6, 29)
        Me.lvDepandants_Benefice.MinColumnWidth = 50
        Me.lvDepandants_Benefice.MultiSelect = False
        Me.lvDepandants_Benefice.Name = "lvDepandants_Benefice"
        Me.lvDepandants_Benefice.OptionalColumns = ""
        Me.lvDepandants_Benefice.ShowMoreItem = False
        Me.lvDepandants_Benefice.ShowSaveItem = False
        Me.lvDepandants_Benefice.ShowSelectAll = True
        Me.lvDepandants_Benefice.ShowSizeAllColumnsToFit = True
        Me.lvDepandants_Benefice.Size = New System.Drawing.Size(572, 377)
        Me.lvDepandants_Benefice.Sortable = True
        Me.lvDepandants_Benefice.TabIndex = 314
        Me.lvDepandants_Benefice.UseCompatibleStateImageBehavior = False
        Me.lvDepandants_Benefice.View = System.Windows.Forms.View.Details
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 0
        '
        'colhDBName
        '
        Me.colhDBName.Tag = "colhDBName"
        Me.colhDBName.Text = "Name"
        Me.colhDBName.Width = 160
        '
        'colhRelation
        '
        Me.colhRelation.Tag = "colhRelation"
        Me.colhRelation.Text = "Relation"
        Me.colhRelation.Width = 115
        '
        'colhCountry
        '
        Me.colhCountry.DisplayIndex = 4
        Me.colhCountry.Tag = "colhCountry"
        Me.colhCountry.Text = "Birth Date"
        Me.colhCountry.Width = 100
        '
        'colhIdNo
        '
        Me.colhIdNo.DisplayIndex = 5
        Me.colhIdNo.Tag = "colhIdNo"
        Me.colhIdNo.Text = "Identify No."
        Me.colhIdNo.Width = 120
        '
        'colhGender
        '
        Me.colhGender.DisplayIndex = 3
        Me.colhGender.Tag = "colhGender"
        Me.colhGender.Text = "Gender"
        Me.colhGender.Width = 65
        '
        'objfrmDiary_Depandants
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.lvDepandants_Benefice)
        Me.Controls.Add(Me.elEmployeeDepandents)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmDiary_Depandants"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents elEmployeeDepandents As eZee.Common.eZeeLine
    Friend WithEvents lvDepandants_Benefice As eZee.Common.eZeeListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDBName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRelation As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCountry As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIdNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGender As System.Windows.Forms.ColumnHeader
End Class
