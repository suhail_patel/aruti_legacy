﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmDiary_Perf_Eval
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbPerfomanceEvaluation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkShow = New System.Windows.Forms.LinkLabel
        Me.cboGroup = New System.Windows.Forms.ComboBox
        Me.lblGroup = New System.Windows.Forms.Label
        Me.tabcAssessments = New System.Windows.Forms.TabControl
        Me.tabpAssessmentResults = New System.Windows.Forms.TabPage
        Me.lvAssessments = New eZee.Common.eZeeListView(Me.components)
        Me.tabpAssessmentRemarks = New System.Windows.Forms.TabPage
        Me.tabcRemarks = New eZee.Common.eZeeTabControlEx
        Me.tabpSelf = New System.Windows.Forms.TabPage
        Me.lvImprovement = New eZee.Common.eZeeListView(Me.components)
        Me.colhI_Improvement = New System.Windows.Forms.ColumnHeader
        Me.colhI_ActivityReq = New System.Windows.Forms.ColumnHeader
        Me.colhI_Support = New System.Windows.Forms.ColumnHeader
        Me.colhI_Course = New System.Windows.Forms.ColumnHeader
        Me.colhI_Timeframe = New System.Windows.Forms.ColumnHeader
        Me.colhI_OtherTraning = New System.Windows.Forms.ColumnHeader
        Me.tabpAppriasers = New System.Windows.Forms.TabPage
        Me.lvPersonalDevelop = New eZee.Common.eZeeListView(Me.components)
        Me.colhP_Development = New System.Windows.Forms.ColumnHeader
        Me.colhP_ActivityReq = New System.Windows.Forms.ColumnHeader
        Me.colhP_Support = New System.Windows.Forms.ColumnHeader
        Me.colhP_Course = New System.Windows.Forms.ColumnHeader
        Me.colhP_Timeframe = New System.Windows.Forms.ColumnHeader
        Me.colhP_OtherTraning = New System.Windows.Forms.ColumnHeader
        Me.objcolhAppraiser = New System.Windows.Forms.ColumnHeader
        Me.lblView = New System.Windows.Forms.Label
        Me.cboViewBy = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblYear = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.gbPerfomanceEvaluation.SuspendLayout()
        Me.tabcAssessments.SuspendLayout()
        Me.tabpAssessmentResults.SuspendLayout()
        Me.tabpAssessmentRemarks.SuspendLayout()
        Me.tabcRemarks.SuspendLayout()
        Me.tabpSelf.SuspendLayout()
        Me.tabpAppriasers.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbPerfomanceEvaluation
        '
        Me.gbPerfomanceEvaluation.BorderColor = System.Drawing.Color.Black
        Me.gbPerfomanceEvaluation.Checked = False
        Me.gbPerfomanceEvaluation.CollapseAllExceptThis = False
        Me.gbPerfomanceEvaluation.CollapsedHoverImage = Nothing
        Me.gbPerfomanceEvaluation.CollapsedNormalImage = Nothing
        Me.gbPerfomanceEvaluation.CollapsedPressedImage = Nothing
        Me.gbPerfomanceEvaluation.CollapseOnLoad = False
        Me.gbPerfomanceEvaluation.Controls.Add(Me.lnkShow)
        Me.gbPerfomanceEvaluation.Controls.Add(Me.cboGroup)
        Me.gbPerfomanceEvaluation.Controls.Add(Me.lblGroup)
        Me.gbPerfomanceEvaluation.Controls.Add(Me.tabcAssessments)
        Me.gbPerfomanceEvaluation.Controls.Add(Me.lblView)
        Me.gbPerfomanceEvaluation.Controls.Add(Me.cboViewBy)
        Me.gbPerfomanceEvaluation.Controls.Add(Me.lblPeriod)
        Me.gbPerfomanceEvaluation.Controls.Add(Me.cboPeriod)
        Me.gbPerfomanceEvaluation.Controls.Add(Me.lblYear)
        Me.gbPerfomanceEvaluation.Controls.Add(Me.cboYear)
        Me.gbPerfomanceEvaluation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbPerfomanceEvaluation.ExpandedHoverImage = Nothing
        Me.gbPerfomanceEvaluation.ExpandedNormalImage = Nothing
        Me.gbPerfomanceEvaluation.ExpandedPressedImage = Nothing
        Me.gbPerfomanceEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPerfomanceEvaluation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPerfomanceEvaluation.HeaderHeight = 25
        Me.gbPerfomanceEvaluation.HeaderMessage = ""
        Me.gbPerfomanceEvaluation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPerfomanceEvaluation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPerfomanceEvaluation.HeightOnCollapse = 0
        Me.gbPerfomanceEvaluation.LeftTextSpace = 0
        Me.gbPerfomanceEvaluation.Location = New System.Drawing.Point(0, 0)
        Me.gbPerfomanceEvaluation.Name = "gbPerfomanceEvaluation"
        Me.gbPerfomanceEvaluation.OpenHeight = 300
        Me.gbPerfomanceEvaluation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPerfomanceEvaluation.ShowBorder = True
        Me.gbPerfomanceEvaluation.ShowCheckBox = False
        Me.gbPerfomanceEvaluation.ShowCollapseButton = False
        Me.gbPerfomanceEvaluation.ShowDefaultBorderColor = True
        Me.gbPerfomanceEvaluation.ShowDownButton = False
        Me.gbPerfomanceEvaluation.ShowHeader = True
        Me.gbPerfomanceEvaluation.Size = New System.Drawing.Size(581, 418)
        Me.gbPerfomanceEvaluation.TabIndex = 0
        Me.gbPerfomanceEvaluation.Temp = 0
        Me.gbPerfomanceEvaluation.Text = "Performance Evaluation"
        Me.gbPerfomanceEvaluation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkShow
        '
        Me.lnkShow.BackColor = System.Drawing.Color.Transparent
        Me.lnkShow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkShow.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkShow.Location = New System.Drawing.Point(486, 5)
        Me.lnkShow.Name = "lnkShow"
        Me.lnkShow.Size = New System.Drawing.Size(92, 15)
        Me.lnkShow.TabIndex = 18
        Me.lnkShow.TabStop = True
        Me.lnkShow.Text = "[ Show Data]"
        Me.lnkShow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboGroup
        '
        Me.cboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGroup.DropDownWidth = 200
        Me.cboGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGroup.FormattingEnabled = True
        Me.cboGroup.Location = New System.Drawing.Point(413, 59)
        Me.cboGroup.Name = "cboGroup"
        Me.cboGroup.Size = New System.Drawing.Size(156, 21)
        Me.cboGroup.TabIndex = 16
        '
        'lblGroup
        '
        Me.lblGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroup.Location = New System.Drawing.Point(352, 61)
        Me.lblGroup.Name = "lblGroup"
        Me.lblGroup.Size = New System.Drawing.Size(55, 15)
        Me.lblGroup.TabIndex = 15
        Me.lblGroup.Text = "Group"
        Me.lblGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabcAssessments
        '
        Me.tabcAssessments.Controls.Add(Me.tabpAssessmentResults)
        Me.tabcAssessments.Controls.Add(Me.tabpAssessmentRemarks)
        Me.tabcAssessments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcAssessments.Location = New System.Drawing.Point(11, 68)
        Me.tabcAssessments.Name = "tabcAssessments"
        Me.tabcAssessments.SelectedIndex = 0
        Me.tabcAssessments.Size = New System.Drawing.Size(558, 338)
        Me.tabcAssessments.TabIndex = 16
        '
        'tabpAssessmentResults
        '
        Me.tabpAssessmentResults.Controls.Add(Me.lvAssessments)
        Me.tabpAssessmentResults.Location = New System.Drawing.Point(4, 22)
        Me.tabpAssessmentResults.Name = "tabpAssessmentResults"
        Me.tabpAssessmentResults.Size = New System.Drawing.Size(550, 312)
        Me.tabpAssessmentResults.TabIndex = 0
        Me.tabpAssessmentResults.Text = "Performance Evaluation"
        Me.tabpAssessmentResults.UseVisualStyleBackColor = True
        '
        'lvAssessments
        '
        Me.lvAssessments.BackColorOnChecked = False
        Me.lvAssessments.ColumnHeaders = Nothing
        Me.lvAssessments.CompulsoryColumns = ""
        Me.lvAssessments.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvAssessments.FullRowSelect = True
        Me.lvAssessments.GridLines = True
        Me.lvAssessments.GroupingColumn = Nothing
        Me.lvAssessments.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAssessments.HideSelection = False
        Me.lvAssessments.Location = New System.Drawing.Point(0, 0)
        Me.lvAssessments.MinColumnWidth = 50
        Me.lvAssessments.MultiSelect = False
        Me.lvAssessments.Name = "lvAssessments"
        Me.lvAssessments.OptionalColumns = ""
        Me.lvAssessments.ShowMoreItem = False
        Me.lvAssessments.ShowSaveItem = False
        Me.lvAssessments.ShowSelectAll = True
        Me.lvAssessments.ShowSizeAllColumnsToFit = True
        Me.lvAssessments.Size = New System.Drawing.Size(550, 312)
        Me.lvAssessments.Sortable = True
        Me.lvAssessments.TabIndex = 14
        Me.lvAssessments.UseCompatibleStateImageBehavior = False
        Me.lvAssessments.View = System.Windows.Forms.View.Details
        '
        'tabpAssessmentRemarks
        '
        Me.tabpAssessmentRemarks.Controls.Add(Me.tabcRemarks)
        Me.tabpAssessmentRemarks.Location = New System.Drawing.Point(4, 22)
        Me.tabpAssessmentRemarks.Name = "tabpAssessmentRemarks"
        Me.tabpAssessmentRemarks.Size = New System.Drawing.Size(550, 312)
        Me.tabpAssessmentRemarks.TabIndex = 1
        Me.tabpAssessmentRemarks.Text = "Assessment Remarks"
        Me.tabpAssessmentRemarks.UseVisualStyleBackColor = True
        '
        'tabcRemarks
        '
        Me.tabcRemarks.Alignment = System.Windows.Forms.TabAlignment.Left
        Me.tabcRemarks.Controls.Add(Me.tabpSelf)
        Me.tabcRemarks.Controls.Add(Me.tabpAppriasers)
        Me.tabcRemarks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabcRemarks.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.tabcRemarks.HeaderAlignment = System.Drawing.ContentAlignment.MiddleCenter
        Me.tabcRemarks.HeaderBackColor = System.Drawing.Color.DarkGray
        Me.tabcRemarks.HeaderBorderColor = System.Drawing.Color.WhiteSmoke
        Me.tabcRemarks.HeaderFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcRemarks.HeaderPadding = New System.Windows.Forms.Padding(0)
        Me.tabcRemarks.HeaderSelectedBackColor = System.Drawing.Color.LightGray
        Me.tabcRemarks.ItemSize = New System.Drawing.Size(32, 100)
        Me.tabcRemarks.Location = New System.Drawing.Point(0, 0)
        Me.tabcRemarks.Multiline = True
        Me.tabcRemarks.Name = "tabcRemarks"
        Me.tabcRemarks.SelectedIndex = 0
        Me.tabcRemarks.Size = New System.Drawing.Size(550, 312)
        Me.tabcRemarks.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tabcRemarks.TabIndex = 18
        '
        'tabpSelf
        '
        Me.tabpSelf.Controls.Add(Me.lvImprovement)
        Me.tabpSelf.Location = New System.Drawing.Point(104, 4)
        Me.tabpSelf.Name = "tabpSelf"
        Me.tabpSelf.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpSelf.Size = New System.Drawing.Size(442, 304)
        Me.tabpSelf.TabIndex = 0
        Me.tabpSelf.Text = "Self"
        Me.tabpSelf.UseVisualStyleBackColor = True
        '
        'lvImprovement
        '
        Me.lvImprovement.BackColorOnChecked = False
        Me.lvImprovement.ColumnHeaders = Nothing
        Me.lvImprovement.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhI_Improvement, Me.colhI_ActivityReq, Me.colhI_Support, Me.colhI_Course, Me.colhI_Timeframe, Me.colhI_OtherTraning})
        Me.lvImprovement.CompulsoryColumns = ""
        Me.lvImprovement.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvImprovement.FullRowSelect = True
        Me.lvImprovement.GridLines = True
        Me.lvImprovement.GroupingColumn = Nothing
        Me.lvImprovement.HideSelection = False
        Me.lvImprovement.Location = New System.Drawing.Point(3, 3)
        Me.lvImprovement.MinColumnWidth = 50
        Me.lvImprovement.MultiSelect = False
        Me.lvImprovement.Name = "lvImprovement"
        Me.lvImprovement.OptionalColumns = ""
        Me.lvImprovement.ShowMoreItem = False
        Me.lvImprovement.ShowSaveItem = False
        Me.lvImprovement.ShowSelectAll = True
        Me.lvImprovement.ShowSizeAllColumnsToFit = True
        Me.lvImprovement.Size = New System.Drawing.Size(436, 298)
        Me.lvImprovement.Sortable = True
        Me.lvImprovement.TabIndex = 339
        Me.lvImprovement.UseCompatibleStateImageBehavior = False
        Me.lvImprovement.View = System.Windows.Forms.View.Details
        '
        'colhI_Improvement
        '
        Me.colhI_Improvement.Tag = "colhI_Improvement"
        Me.colhI_Improvement.Text = "Major Area for Improvement"
        Me.colhI_Improvement.Width = 160
        '
        'colhI_ActivityReq
        '
        Me.colhI_ActivityReq.Tag = "colhI_ActivityReq"
        Me.colhI_ActivityReq.Text = "Activity/Action Required to Meet Need"
        Me.colhI_ActivityReq.Width = 200
        '
        'colhI_Support
        '
        Me.colhI_Support.Tag = "colhI_Support"
        Me.colhI_Support.Text = "Support required from Assessor"
        Me.colhI_Support.Width = 180
        '
        'colhI_Course
        '
        Me.colhI_Course.Tag = "colhI_Course"
        Me.colhI_Course.Text = "Training or Learning Objective"
        Me.colhI_Course.Width = 160
        '
        'colhI_Timeframe
        '
        Me.colhI_Timeframe.Tag = "colhI_Timeframe"
        Me.colhI_Timeframe.Text = "Timeframe"
        Me.colhI_Timeframe.Width = 90
        '
        'colhI_OtherTraning
        '
        Me.colhI_OtherTraning.Tag = "colhI_OtherTraning"
        Me.colhI_OtherTraning.Text = "Other Training"
        Me.colhI_OtherTraning.Width = 160
        '
        'tabpAppriasers
        '
        Me.tabpAppriasers.Controls.Add(Me.lvPersonalDevelop)
        Me.tabpAppriasers.Location = New System.Drawing.Point(104, 4)
        Me.tabpAppriasers.Name = "tabpAppriasers"
        Me.tabpAppriasers.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpAppriasers.Size = New System.Drawing.Size(442, 304)
        Me.tabpAppriasers.TabIndex = 1
        Me.tabpAppriasers.Text = "Assessor's"
        Me.tabpAppriasers.UseVisualStyleBackColor = True
        '
        'lvPersonalDevelop
        '
        Me.lvPersonalDevelop.BackColorOnChecked = False
        Me.lvPersonalDevelop.ColumnHeaders = Nothing
        Me.lvPersonalDevelop.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhP_Development, Me.colhP_ActivityReq, Me.colhP_Support, Me.colhP_Course, Me.colhP_Timeframe, Me.colhP_OtherTraning, Me.objcolhAppraiser})
        Me.lvPersonalDevelop.CompulsoryColumns = ""
        Me.lvPersonalDevelop.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvPersonalDevelop.FullRowSelect = True
        Me.lvPersonalDevelop.GridLines = True
        Me.lvPersonalDevelop.GroupingColumn = Nothing
        Me.lvPersonalDevelop.HideSelection = False
        Me.lvPersonalDevelop.Location = New System.Drawing.Point(3, 3)
        Me.lvPersonalDevelop.MinColumnWidth = 50
        Me.lvPersonalDevelop.MultiSelect = False
        Me.lvPersonalDevelop.Name = "lvPersonalDevelop"
        Me.lvPersonalDevelop.OptionalColumns = ""
        Me.lvPersonalDevelop.ShowMoreItem = False
        Me.lvPersonalDevelop.ShowSaveItem = False
        Me.lvPersonalDevelop.ShowSelectAll = True
        Me.lvPersonalDevelop.ShowSizeAllColumnsToFit = True
        Me.lvPersonalDevelop.Size = New System.Drawing.Size(436, 298)
        Me.lvPersonalDevelop.Sortable = True
        Me.lvPersonalDevelop.TabIndex = 340
        Me.lvPersonalDevelop.UseCompatibleStateImageBehavior = False
        Me.lvPersonalDevelop.View = System.Windows.Forms.View.Details
        '
        'colhP_Development
        '
        Me.colhP_Development.Tag = "colhP_Development"
        Me.colhP_Development.Text = "Major Area for Development"
        Me.colhP_Development.Width = 160
        '
        'colhP_ActivityReq
        '
        Me.colhP_ActivityReq.Tag = "colhP_ActivityReq"
        Me.colhP_ActivityReq.Text = "Activity/Action Required to Meet Need"
        Me.colhP_ActivityReq.Width = 200
        '
        'colhP_Support
        '
        Me.colhP_Support.Tag = "colhP_Support"
        Me.colhP_Support.Text = "Support required from Assessor"
        Me.colhP_Support.Width = 180
        '
        'colhP_Course
        '
        Me.colhP_Course.Tag = "colhP_Course"
        Me.colhP_Course.Text = "Training or Learning Objective"
        Me.colhP_Course.Width = 160
        '
        'colhP_Timeframe
        '
        Me.colhP_Timeframe.Tag = "colhP_Timeframe"
        Me.colhP_Timeframe.Text = "Timeframe"
        Me.colhP_Timeframe.Width = 90
        '
        'colhP_OtherTraning
        '
        Me.colhP_OtherTraning.Tag = "colhP_OtherTraning"
        Me.colhP_OtherTraning.Text = "Other Training"
        Me.colhP_OtherTraning.Width = 160
        '
        'objcolhAppraiser
        '
        Me.objcolhAppraiser.Tag = "objcolhAppraiser"
        Me.objcolhAppraiser.Text = ""
        Me.objcolhAppraiser.Width = 0
        '
        'lblView
        '
        Me.lblView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblView.Location = New System.Drawing.Point(352, 35)
        Me.lblView.Name = "lblView"
        Me.lblView.Size = New System.Drawing.Size(55, 15)
        Me.lblView.TabIndex = 5
        Me.lblView.Text = "View By"
        Me.lblView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboViewBy
        '
        Me.cboViewBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboViewBy.DropDownWidth = 200
        Me.cboViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboViewBy.FormattingEnabled = True
        Me.cboViewBy.Location = New System.Drawing.Point(413, 32)
        Me.cboViewBy.Name = "cboViewBy"
        Me.cboViewBy.Size = New System.Drawing.Size(156, 21)
        Me.cboViewBy.TabIndex = 6
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(180, 35)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(43, 15)
        Me.lblPeriod.TabIndex = 3
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(225, 32)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(121, 21)
        Me.cboPeriod.TabIndex = 4
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(8, 36)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(39, 15)
        Me.lblYear.TabIndex = 1
        Me.lblYear.Text = "Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.DropDownWidth = 200
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(53, 33)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(121, 21)
        Me.cboYear.TabIndex = 2
        '
        'objfrmDiary_Perf_Eval
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(581, 418)
        Me.Controls.Add(Me.gbPerfomanceEvaluation)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "objfrmDiary_Perf_Eval"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.gbPerfomanceEvaluation.ResumeLayout(False)
        Me.tabcAssessments.ResumeLayout(False)
        Me.tabpAssessmentResults.ResumeLayout(False)
        Me.tabpAssessmentRemarks.ResumeLayout(False)
        Me.tabcRemarks.ResumeLayout(False)
        Me.tabpSelf.ResumeLayout(False)
        Me.tabpAppriasers.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbPerfomanceEvaluation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblView As System.Windows.Forms.Label
    Friend WithEvents cboViewBy As System.Windows.Forms.ComboBox
    Friend WithEvents tabcAssessments As System.Windows.Forms.TabControl
    Friend WithEvents tabpAssessmentResults As System.Windows.Forms.TabPage
    Friend WithEvents lvAssessments As eZee.Common.eZeeListView
    Friend WithEvents tabpAssessmentRemarks As System.Windows.Forms.TabPage
    Friend WithEvents tabcRemarks As eZee.Common.eZeeTabControlEx
    Friend WithEvents tabpSelf As System.Windows.Forms.TabPage
    Friend WithEvents lvImprovement As eZee.Common.eZeeListView
    Friend WithEvents colhI_Improvement As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_ActivityReq As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_Support As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_Course As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_Timeframe As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhI_OtherTraning As System.Windows.Forms.ColumnHeader
    Friend WithEvents tabpAppriasers As System.Windows.Forms.TabPage
    Friend WithEvents lvPersonalDevelop As eZee.Common.eZeeListView
    Friend WithEvents colhP_Development As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_ActivityReq As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_Support As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_Course As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_Timeframe As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhP_OtherTraning As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhAppraiser As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroup As System.Windows.Forms.Label
    Friend WithEvents lnkShow As System.Windows.Forms.LinkLabel
End Class
