﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeDiary

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeUnkid As Integer = -1
    'Gajanan [26-Dec-2019] -- Start   
    Private mblnOnlyEmployeeDetailShow As Boolean = False
    'Gajanan [26-Dec-2019] -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intUnkid As Integer, Optional ByVal onlyShowDetail As Boolean = False) As Boolean
		'Gajanan [26-Dec-2019] -- ADD [onlyShowDetail]   
        Try
            mintEmployeeUnkid = intUnkid
            'Gajanan [26-Dec-2019] -- Start   
            mblnOnlyEmployeeDetailShow = onlyShowDetail
            'Gajanan [26-Dec-2019] -- End
            Me.ShowDialog()
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods & Functions "

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub DoOperation(ByVal strName As String)
    '    For Each ctrl As Control In Me.flpDiaryOperations.Controls
    '        If TypeOf ctrl Is eZee.Common.eZeeLightButton Then
    '            If CStr(ctrl.Name).ToUpper = strName.ToUpper Then
    '                CType(ctrl, eZee.Common.eZeeLightButton).Selected = True
    '            Else
    '                CType(ctrl, eZee.Common.eZeeLightButton).Selected = False
    '            End If
    '        End If
    '    Next
    'End Sub
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    Private Sub DisplayForm(ByVal frm As Form)
        Try
            Dim ofrm As Form = Nothing
            Select Case frm.Name.ToUpper
                Case "OBJFRMDIARY_DETAIL"
                    ofrm = New objfrmDiary_Detail(mintEmployeeUnkid)
                Case "OBJFRMDIARY_LEAVE"
                    ofrm = New objfrmDiary_Leave(mintEmployeeUnkid)
                Case "OBJFRMDIARY_OCCASION"
                    ofrm = New objfrmDiary_Occasion(mintEmployeeUnkid)
                Case "OBJFRMDIARY_DATES"
                    ofrm = New objfrmDiary_Dates(mintEmployeeUnkid)
                Case "OBJFRMDIARY_HOLIDAY"
                    ofrm = New objfrmDiary_Holiday(mintEmployeeUnkid)
                Case "OBJFRMDIARY_DISCIPLINE"
                    ofrm = New objfrmDiary_Discipline(mintEmployeeUnkid)
                Case "OBJFRMDIARY_TRAINING"
                    ofrm = New objfrmDiary_Training(mintEmployeeUnkid)
                    'S.SANDEEP [ 18 AUG 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "OBJFRMEMPLOYEE_MOVEMENT"
                    ofrm = New objfrmEmployee_Movement(mintEmployeeUnkid)
                Case "OBJFRMDIARY_ADDRESS"
                    ofrm = New objfrmDiary_Address(mintEmployeeUnkid)
                Case "OBJFRMDIARY_DEPANDANTS"
                    ofrm = New objfrmDiary_Depandants(mintEmployeeUnkid)
                Case "OBJFRMDIARY_EXPERIENCE"
                    ofrm = New objfrmDiary_Experience(mintEmployeeUnkid)
                Case "OBJFRMDIARY_REFERENCES"
                    ofrm = New objfrmDiary_References(mintEmployeeUnkid)
                Case "OBJFRMDIARY_CASSETS"
                    ofrm = New objfrmDiary_CAssets(mintEmployeeUnkid)
                Case "OBJFRMDIARY_BENEFITS"
                    ofrm = New objfrmDiary_Benefits(mintEmployeeUnkid)
                Case "OBJFRMDIARY_LOAN_ADVANCE"
                    ofrm = New objfrmDiary_Loan_Advance(mintEmployeeUnkid)
                Case "OBJFRMDIARY_PERF_EVAL"
                    ofrm = New objfrmDiary_Perf_Eval(mintEmployeeUnkid)
                    'S.SANDEEP [ 18 AUG 2012 ] -- END

                    'S.SANDEEP [ 13 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "OBJFRMDIARY_MEMBERSHIPS"
                    ofrm = New objfrmDiary_Memberships(mintEmployeeUnkid)
                Case "OBJFRMDIARY_BANKS"
                    ofrm = New objfrmDiary_Banks(mintEmployeeUnkid)
                    'S.SANDEEP [ 13 DEC 2012 ] -- END

                    'S.SANDEEP [ 26 SEPT 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case "OBJFRMDAIRY_SHIFT_POLICY"
                    ofrm = New objfrmDairy_Shift_Policy(mintEmployeeUnkid)
                    'S.SANDEEP [ 26 SEPT 2013 ] -- END
            End Select

            If ofrm IsNot Nothing Then
                ofrm.TopLevel = False
                ofrm.Size = pnlData.Size
                ofrm.Dock = DockStyle.Fill
                pnlData.Controls.Add(ofrm)
                ofrm.Show()
                ofrm.BringToFront()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DisplayForm", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnTrainingEnrollment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTrainingEnrollment.Click
        Try
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call DoOperation(btnTrainingEnrollment.Name)
            'S.SANDEEP [ 07 NOV 2011 ] -- END


            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Training" Then
                    Call DisplayForm(objfrmDiary_Training)
                End If
            Else
                Call DisplayForm(objfrmDiary_Training)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnTrainingEnrollment_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDiscipline_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDiscipline.Click
        Try


            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call DoOperation(btnDiscipline.Name)
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Discipline" Then
                    Call DisplayForm(objfrmDiary_Discipline)
                End If
            Else
                Call DisplayForm(objfrmDiary_Discipline)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnTrainingEnrollment_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnHolidays_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHolidays.Click
        Try
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call DoOperation(btnHolidays.Name)
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Holiday" Then
                    Call DisplayForm(objfrmDiary_Holiday)
                End If
            Else
                Call DisplayForm(objfrmDiary_Holiday)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnHolidays_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDates_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDates.Click
        Try
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call DoOperation(btnDates.Name)
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Dates" Then
                    Call DisplayForm(objfrmDiary_Dates)
                End If
            Else
                Call DisplayForm(objfrmDiary_Dates)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDates_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnOccasion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOccasion.Click
        Try
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call DoOperation(btnOccasion.Name)
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Occasion" Then
                    Call DisplayForm(objfrmDiary_Occasion)
                End If
            Else
                Call DisplayForm(objfrmDiary_Occasion)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOccasion_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnLeave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLeave.Click
        Try
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call DoOperation(btnLeave.Name)
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Leave" Then
                    Call DisplayForm(objfrmDiary_Leave)
                End If
            Else
                Call DisplayForm(objfrmDiary_Leave)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnLeave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetails.Click
        Try

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Call DoOperation(btnDetails.Name)
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Detail" Then
                    Call DisplayForm(objfrmDiary_Detail)
                End If
            Else
                Call DisplayForm(objfrmDiary_Detail)
            End If
            btnDetails.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDetails_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 18 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub btnEmployeeMovement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmployeeMovement.Click
        Try
            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmEmployee_Movement" Then
                    Call DisplayForm(objfrmEmployee_Movement)
                End If
            Else
                Call DisplayForm(objfrmEmployee_Movement)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEmployeeMovement_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAddress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddress.Click
        Try
            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Address" Then
                    Call DisplayForm(objfrmDiary_Address)
                End If
            Else
                Call DisplayForm(objfrmDiary_Address)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddress_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDependants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDependants.Click
        Try
            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Depandants" Then
                    Call DisplayForm(objfrmDiary_Depandants)
                End If
            Else
                Call DisplayForm(objfrmDiary_Depandants)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDependants_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExperience_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExperience.Click
        Try
            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Experience" Then
                    Call DisplayForm(objfrmDiary_Experience)
                End If
            Else
                Call DisplayForm(objfrmDiary_Experience)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExperience_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReferences_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReferences.Click
        Try
            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_References" Then
                    Call DisplayForm(objfrmDiary_References)
                End If
            Else
                Call DisplayForm(objfrmDiary_References)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReferences_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCompanyAssets_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompanyAssets.Click
        Try
            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_CAssets" Then
                    Call DisplayForm(objfrmDiary_CAssets)
                End If
            Else
                Call DisplayForm(objfrmDiary_CAssets)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCompanyAssets_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnBenefits_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBenefits.Click
        Try
            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Benefits" Then
                    Call DisplayForm(objfrmDiary_Benefits)
                End If
            Else
                Call DisplayForm(objfrmDiary_Benefits)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnBenefits_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnLoan_Advances_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoan_Advances.Click
        Try
            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Loan_Advance" Then
                    Call DisplayForm(objfrmDiary_Loan_Advance)
                End If
            Else
                Call DisplayForm(objfrmDiary_Loan_Advance)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnLoan_Advances_Click", mstrModuleName)
        End Try    
    End Sub

    Private Sub btnPerformance_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPerformance.Click
        Try
            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Perf_Eval" Then
                    Call DisplayForm(objfrmDiary_Perf_Eval)
                End If
            Else
                Call DisplayForm(objfrmDiary_Perf_Eval)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPerformance_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 18 AUG 2012 ] -- END

    'S.SANDEEP [ 13 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub btnEmployeeMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmployeeMembership.Click
        Try
            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Memberships" Then
                    Call DisplayForm(objfrmDiary_Memberships)
                End If
            Else
                Call DisplayForm(objfrmDiary_Memberships)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEmployeeMembership_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEmployeeBanks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmployeeBanks.Click
        Try
            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDiary_Banks" Then
                    Call DisplayForm(objfrmDiary_Banks)
                End If
            Else
                Call DisplayForm(objfrmDiary_Banks)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEmployeeBanks_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 13 DEC 2012 ] -- END

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 26 SEPT 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub btnShift_Policy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShift_Policy.Click
        Try
            If pnlData.Controls.Count > 0 Then
                If pnlData.Controls.Item(0).Name <> "objfrmDairy_Shift_Policy" Then
                    Call DisplayForm(objfrmDairy_Shift_Policy)
                End If
            Else
                Call DisplayForm(objfrmDairy_Shift_Policy)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEmployeeBanks_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 26 SEPT 2013 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeDiary_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(Me.Name)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End


            btnDetails.Selected = True
            'S.SANDEEP [ 13 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            btnEmployeeMembership.Enabled = User._Object.Privilege._AllowToViewMembershipOnDiary
            btnEmployeeBanks.Enabled = User._Object.Privilege._AllowToViewBanksOnDiary
            'S.SANDEEP [ 13 DEC 2012 ] -- END
            Call btnDetails_Click(sender, e)

            'Gajanan [26-Dec-2019] -- Start   
            If mblnOnlyEmployeeDetailShow = True Then
                Me.Size = New Size((Me.Width - objSpcDiary.Panel1.Width) + 10, Me.Height)
                Me.Location = New Point(Me.Location.X + 50, Me.Location.Y)
                objSpcDiary.Panel1Collapsed = True

                Me.Text = Language.getMessage(mstrModuleName, 1, "Employee Detail")
            End If
            'Gajanan [26-Dec-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeDiary_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnDetails.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDetails.GradientForeColor = GUI._ButttonFontColor

			Me.btnLeave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnLeave.GradientForeColor = GUI._ButttonFontColor

			Me.btnOccasion.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOccasion.GradientForeColor = GUI._ButttonFontColor

			Me.btnDates.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDates.GradientForeColor = GUI._ButttonFontColor

			Me.btnHolidays.GradientBackColor = GUI._ButttonBackColor 
			Me.btnHolidays.GradientForeColor = GUI._ButttonFontColor

			Me.btnDiscipline.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDiscipline.GradientForeColor = GUI._ButttonFontColor

			Me.btnTrainingEnrollment.GradientBackColor = GUI._ButttonBackColor 
			Me.btnTrainingEnrollment.GradientForeColor = GUI._ButttonFontColor

			Me.btnEmployeeMovement.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEmployeeMovement.GradientForeColor = GUI._ButttonFontColor

			Me.btnAddress.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAddress.GradientForeColor = GUI._ButttonFontColor

			Me.btnDependants.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDependants.GradientForeColor = GUI._ButttonFontColor

			Me.btnExperience.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExperience.GradientForeColor = GUI._ButttonFontColor

			Me.btnReferences.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReferences.GradientForeColor = GUI._ButttonFontColor

			Me.btnCompanyAssets.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCompanyAssets.GradientForeColor = GUI._ButttonFontColor

			Me.btnBenefits.GradientBackColor = GUI._ButttonBackColor 
			Me.btnBenefits.GradientForeColor = GUI._ButttonFontColor

			Me.btnPerformance.GradientBackColor = GUI._ButttonBackColor 
			Me.btnPerformance.GradientForeColor = GUI._ButttonFontColor

			Me.btnLoan_Advances.GradientBackColor = GUI._ButttonBackColor 
			Me.btnLoan_Advances.GradientForeColor = GUI._ButttonFontColor

			Me.btnEmployeeMembership.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEmployeeMembership.GradientForeColor = GUI._ButttonFontColor

			Me.btnEmployeeBanks.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEmployeeBanks.GradientForeColor = GUI._ButttonFontColor

			Me.btnShift_Policy.GradientBackColor = GUI._ButttonBackColor 
			Me.btnShift_Policy.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnDetails.Text = Language._Object.getCaption(Me.btnDetails.Name, Me.btnDetails.Text)
			Me.btnLeave.Text = Language._Object.getCaption(Me.btnLeave.Name, Me.btnLeave.Text)
			Me.btnOccasion.Text = Language._Object.getCaption(Me.btnOccasion.Name, Me.btnOccasion.Text)
			Me.btnDates.Text = Language._Object.getCaption(Me.btnDates.Name, Me.btnDates.Text)
			Me.btnHolidays.Text = Language._Object.getCaption(Me.btnHolidays.Name, Me.btnHolidays.Text)
			Me.btnDiscipline.Text = Language._Object.getCaption(Me.btnDiscipline.Name, Me.btnDiscipline.Text)
			Me.btnTrainingEnrollment.Text = Language._Object.getCaption(Me.btnTrainingEnrollment.Name, Me.btnTrainingEnrollment.Text)
			Me.btnEmployeeMovement.Text = Language._Object.getCaption(Me.btnEmployeeMovement.Name, Me.btnEmployeeMovement.Text)
			Me.btnAddress.Text = Language._Object.getCaption(Me.btnAddress.Name, Me.btnAddress.Text)
			Me.btnDependants.Text = Language._Object.getCaption(Me.btnDependants.Name, Me.btnDependants.Text)
			Me.btnExperience.Text = Language._Object.getCaption(Me.btnExperience.Name, Me.btnExperience.Text)
			Me.btnReferences.Text = Language._Object.getCaption(Me.btnReferences.Name, Me.btnReferences.Text)
			Me.btnCompanyAssets.Text = Language._Object.getCaption(Me.btnCompanyAssets.Name, Me.btnCompanyAssets.Text)
			Me.btnBenefits.Text = Language._Object.getCaption(Me.btnBenefits.Name, Me.btnBenefits.Text)
			Me.btnPerformance.Text = Language._Object.getCaption(Me.btnPerformance.Name, Me.btnPerformance.Text)
			Me.btnLoan_Advances.Text = Language._Object.getCaption(Me.btnLoan_Advances.Name, Me.btnLoan_Advances.Text)
			Me.btnEmployeeMembership.Text = Language._Object.getCaption(Me.btnEmployeeMembership.Name, Me.btnEmployeeMembership.Text)
			Me.btnEmployeeBanks.Text = Language._Object.getCaption(Me.btnEmployeeBanks.Name, Me.btnEmployeeBanks.Text)
			Me.btnShift_Policy.Text = Language._Object.getCaption(Me.btnShift_Policy.Name, Me.btnShift_Policy.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

'#Region " Private Variables "
'    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
'    Private mblnCancel As Boolean = True
'    Private menAction As enAction = enAction.ADD_ONE
'    Private dsEmployeeData As DataSet = Nothing

'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
'        Try
'            menAction = eAction
'            Me.ShowDialog()
'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function

'#End Region

'#Region " Private Methods "

'    Private Sub SetColor()
'        Try

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetValue()
'        Dim objEmployee As New clsEmployee_Master
'        Try
'            imgImageControl._FileName = ""

'            objEmployee._Employeeunkid = CInt(txtCode.Tag)

'            Dim objCommon As New clsCommon_Master
'            objCommon._Masterunkid = objEmployee._Titalunkid
'            txttitle.Text = objCommon._Name

'            txtFirstname.Text = objEmployee._Firstname.Trim
'            txtSurname.Text = objEmployee._Surname.Trim
'            txtAddress1.Text = objEmployee._Present_Address1
'            txtAddress2.Text = objEmployee._Present_Address2

'            If objEmployee._Present_Countryunkid > 0 Then
'                Dim objMasterData As New clsMasterData
'                Dim dsCountry = objMasterData.getCountryList("List", False, objEmployee._Present_Countryunkid)
'                If dsCountry.Tables("List").Rows.Count > 0 Then
'                    txtCountry.Text = dsCountry.Tables("List").Rows(0)("country_name").ToString
'                End If
'            End If

'            'Zipcode
'            Dim objZipCode As New clszipcode_master
'            objZipCode._Zipcodeunkid = objEmployee._Present_Postcodeunkid
'            txtPostCode.Text = objZipCode._Zipcode_Code

'            'FOR STATE 
'            Dim objState As New clsstate_master
'            objState._Stateunkid = objEmployee._Stationunkid
'            txtState.Text = objState._Name

'            'FOR CITY
'            Dim objCity As New clscity_master
'            objCity._Cityunkid = objEmployee._Present_Post_Townunkid
'            txtPostTown.Text = objCity._Name


'            txtProvince.Text = objEmployee._Present_Provicnce
'            txtMobile.Text = objEmployee._Present_Mobile
'            txtTelNo.Text = objEmployee._Present_Tel_No
'            txtFax.Text = objEmployee._Present_Fax
'            txtAlternativeNo.Text = objEmployee._Present_Alternateno
'            txtEmail.Text = objEmployee._Present_Email

'            'FOR EMPLOYEE IMAGE
'            'Anjan (22 Feb 2011)-Start
'            'Issue : On windows 2003 server this folder of MyPictures is not there , so it gives error.
'            'imgImageControl._FilePath = My.Computer.FileSystem.SpecialDirectories.MyPictures
'            imgImageControl._FilePath = ConfigParameter._Object._PhotoPath
'            'Anjan (22 Feb 2011)-End

'            imgImageControl._FileName = imgImageControl._FileName & "\" & objEmployee._ImagePath

'            'FOR STATION
'            Dim objStation As New clsStation
'            objStation._Stationunkid = objEmployee._Stationunkid
'            txtBranch.Text = objStation._Code
'            objStationName.Text = objStation._Name

'            'FOR DEPARTMENT GROUP
'            Dim objDepartmentgrp As New clsDepartmentGroup
'            objDepartmentgrp._Deptgroupunkid = objEmployee._Deptgroupunkid
'            txtDeptGroup.Text = objDepartmentgrp._Code
'            objDeptGroupName.Text = objDepartmentgrp._Name


'            'FOR DEPARTMENT
'            Dim objDepartment As New clsDepartment
'            objDepartment._Departmentunkid = objEmployee._Departmentunkid
'            txtDepartment.Text = objDepartment._Code
'            objDeparmentName.Text = objDepartment._Name
'            txtDept.Text = objDepartment._Name

'            'FOR SECTION
'            Dim objSection As New clsSections
'            objSection._Sectionunkid = objEmployee._Sectionunkid
'            txtSection.Text = objSection._Code
'            objSectionName.Text = objSection._Name

'            'FOR UNIT
'            Dim objUnit As New clsUnits
'            objUnit._Unitunkid = objEmployee._Unitunkid
'            txtUnit.Text = objUnit._Code
'            objUnitName.Text = objUnit._Name

'            'FOR JOB GROUP
'            Dim objJobGrp As New clsJobGroup
'            objJobGrp._Jobgroupunkid = objEmployee._Jobgroupunkid
'            txtJobGroup.Text = objJobGrp._Code
'            objJobGroupname.Text = objJobGrp._Name

'            'FOR JOB
'            Dim objJob As New clsJobs
'            objJob._Jobunkid = objEmployee._Jobunkid
'            txtJob.Text = objJob._Job_Code
'            objJobName.Text = objJob._Job_Name

'            'Sandeep [ 01 FEB 2011 ] -- START
'            ' ''FOR ACCESS
'            'Dim objAccess As New clsAccess
'            'objAccess._Accessunkid = objEmployee._Accessunkid
'            'txtAccess.Text = objAccess._Code
'            'objAccessName.Text = objAccess._Name
'            'Sandeep [ 01 FEB 2011 ] -- END 

'            'FOR CLASS GROUP
'            Dim objClassGroup As New clsClassGroup
'            objClassGroup._Classgroupunkid = objEmployee._Classgroupunkid
'            txtClassGroup.Text = objClassGroup._Code
'            objClsGroupName.Text = objClassGroup._Name

'            'FOR CLASS
'            Dim objClass As New clsClass
'            objClass._Classesunkid = objEmployee._Classunkid
'            txtClass.Text = objClass._Code
'            objClassName.Text = objClass._Name

'            'Sandeep [ 01 FEB 2011 ] -- START
'            ''FOR SERVICE
'            'Dim objService As New clsServices
'            'objService._Serviceunkid = objEmployee._Sectionunkid
'            'txtService.Text = objService._Code
'            'objServiceName.Text = objService._Name
'            'Sandeep [ 01 FEB 2011 ] -- END 

'            'FOR GRADE GROUP
'            Dim objGradeGroup As New clsGradeGroup
'            objGradeGroup._Gradegroupunkid = objEmployee._Gradegroupunkid
'            txtGradeGroup.Text = objGradeGroup._Code
'            objGradeGroupName.Text = objGradeGroup._Name

'            'FOR GRADE
'            Dim objGrade As New clsGrade
'            objGrade._Gradeunkid = objEmployee._Gradeunkid
'            txtGrade.Text = objGrade._Code
'            objGradeName.Text = objGrade._Name

'            'FOR GRADE LEVEL
'            Dim objGradeLevel As New clsGradeLevel
'            objGradeLevel._Gradelevelunkid = objEmployee._Gradelevelunkid
'            txtGradeLevel.Text = objGradeLevel._Code
'            objGradeLevelName.Text = objGradeLevel._Name


'            'FOR SHIFT
'            Dim objshift As New clsshift_master
'            objshift._Shiftunkid = objEmployee._Shiftunkid
'            txtShift.Text = objshift._Shiftcode
'            objShiftName.Text = objshift._Shiftname


'            'FOR EMPLOYEE TYPE
'            objCommon._Masterunkid = objEmployee._Employmenttypeunkid
'            txtEmployeeType.Text = objCommon._Name

'            If objEmployee._Appointeddate <> Nothing Then
'                txtAppointedDate.Text = objEmployee._Appointeddate.ToShortDateString
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        Finally
'            objEmployee = Nothing
'        End Try
'    End Sub

'    Private Sub GenerateList()
'        Try
'            Dim objLeaveType As New clsleavetype_master
'            Dim dsLeave As DataSet = objLeaveType.GetList("List", True)

'            If dsLeave IsNot Nothing Then
'                Dim lvItem As ListViewItem

'                lvItem = New ListViewItem
'                lvItem.Text = Language.getMessage(mstrModuleName, 1, "Holiday")
'                lvItem.Tag = -1
'                lvItem.SubItems.Add("Holiday")
'                lstEmployeedata.Items.Add(lvItem)

'                lvItem = New ListViewItem
'                lvItem.Text = Language.getMessage(mstrModuleName, 2, "Birthday")
'                lvItem.Tag = -2
'                lvItem.SubItems.Add("Birthday")
'                lstEmployeedata.Items.Add(lvItem)


'                lvItem = New ListViewItem
'                lvItem.Text = Language.getMessage(mstrModuleName, 3, "Anniversary")
'                lvItem.Tag = -3
'                lvItem.SubItems.Add("Anniversary")
'                lstEmployeedata.Items.Add(lvItem)


'                For i As Integer = 0 To dsLeave.Tables("List").Rows.Count - 1
'                    lvItem = New ListViewItem
'                    lvItem.Text = dsLeave.Tables("List").Rows(i)("leavename").ToString
'                    lvItem.Tag = dsLeave.Tables("List").Rows(i)("leavetypeunkid").ToString
'                    lvItem.SubItems.Add("Leave")
'                    lstEmployeedata.Items.Add(lvItem)
'                Next
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GenerateList", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GenerateColumns()
'        Try
'            dsEmployeeData = New DataSet
'            dsEmployeeData.Tables.Add("EmployeeData")
'            dsEmployeeData.Tables("EmployeeData").Columns.Add(New DataColumn("Items", System.Type.GetType("System.String")))
'            dsEmployeeData.Tables("EmployeeData").Columns.Add(New DataColumn("Date", System.Type.GetType("System.DateTime")))
'            dsEmployeeData.Tables("EmployeeData").Columns.Add(New DataColumn("Description", System.Type.GetType("System.String")))

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GenerateColumns", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillGrid()
'        Try
'            lvEmployeeData.Items.Clear()
'            Dim lvItem As ListViewItem = Nothing
'            If dsEmployeeData.Tables("EmployeeData") IsNot Nothing Then

'                For Each dr As DataRow In dsEmployeeData.Tables("EmployeeData").Rows
'                    lvItem = New ListViewItem
'                    lvItem.Text = dr("Items").ToString

'                    If dr("Date") IsNot DBNull.Value Then
'                        lvItem.SubItems.Add(CDate(dr("Date").ToString).ToShortDateString)
'                    Else
'                        lvItem.SubItems.Add("")
'                    End If

'                    lvItem.SubItems.Add(dr("Description").ToString)
'                    lvEmployeeData.Items.Add(lvItem)
'                Next

'                If lvEmployeeData.Items.Count > 30 Then
'                    colhDescription.Width = colhDescription.Width - 25
'                Else
'                    colhDescription.Width = colhDescription.Width
'                End If

'                lvEmployeeData.GridLines = False
'                lvEmployeeData.GroupingColumn = colhItems
'                lvEmployeeData.DisplayGroups(True)

'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Form's Events "

'    Private Sub frmEmployeeDiary_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            GenerateList()
'            GenerateColumns()

'            'Pinkal (10-Mar-2011) -- Start

'            MonthCalendar.MinDate = FinancialYear._Object._Database_Start_Date
'            MonthCalendar.MaxDate = FinancialYear._Object._Database_End_Date

'            'Pinkal (10-Mar-2011) -- End

'            Dim FirstDate As New DateTime(MonthCalendar.SelectionStart.Year, MonthCalendar.SelectionStart.Month, 1)
'            Dim LastDate As New DateTime(MonthCalendar.SelectionStart.Year, MonthCalendar.SelectionStart.Month, Date.DaysInMonth(MonthCalendar.SelectionStart.Year, MonthCalendar.SelectionStart.Month))
'            MonthCalendar_DateChanged(sender, New DateRangeEventArgs(FirstDate, LastDate))
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmEmployeeDiary_Load", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Button's Events "


'    'S.SANDEEP [ 29 JUNE 2011 ] -- START
'    'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
'    'Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objbtnSearchEmpDiary.Click
'        Dim objEmployee As New clsEmployee_Master
'        Dim objfrm As New frmCommonSearch
'        Dim dsList As DataSet = Nothing
'        Try
'            'S.SANDEEP [ 29 JUNE 2011 ] -- START
'            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
'            'dsList = objEmployee.GetEmployeeList("Employee", False, True)
'            dsList = objEmployee.GetEmployeeList("Employee", False, Not ConfigParameter._Object._IsIncludeInactiveEmp)
'            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
'            objfrm.DataSource = dsList.Tables("Employee")
'            objfrm.ValueMember = "employeeunkid"
'            objfrm.DisplayMember = "employeename"
'            objfrm.CodeMember = "employeecode"
'            If objfrm.DisplayDialog Then
'                txtCode.Text = objfrm.SelectedAlias
'                txtCode.Tag = objfrm.SelectedValue

'                txtEmpcode.Text = objfrm.SelectedAlias
'                txtEmpcode.Tag = objfrm.SelectedValue
'                txtEmployeeName.Text = objfrm.SelectedText

'                GetValue()

'                Select Case sender.ToString.ToUpper
'                    Case "OBJBTNSEARCHEMPDIARY"
'                        Dim FirstDate As New DateTime(MonthCalendar.SelectionStart.Year, MonthCalendar.SelectionStart.Month, 1)
'                        Dim LastDate As New DateTime(MonthCalendar.SelectionStart.Year, MonthCalendar.SelectionStart.Month, Date.DaysInMonth(MonthCalendar.SelectionStart.Year, MonthCalendar.SelectionStart.Month))
'                        MonthCalendar_DateChanged(sender, New DateRangeEventArgs(FirstDate, LastDate))
'                End Select
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        Finally
'            objfrm = Nothing
'            objEmployee = Nothing
'            dsList.Dispose()
'        End Try
'    End Sub

'    'Private Sub objbtnSearchEmpDiary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmpDiary.Click
'    '    Dim objEmployee As New clsEmployee_Master
'    '    Dim objfrm As New frmCommonSearch
'    '    Dim dsList As DataSet = Nothing
'    '    Try

'    '        'S.SANDEEP [ 29 JUNE 2011 ] -- START
'    '        'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
'    '        'dsList = objEmployee.GetEmployeeList("Employee", False, True)
'    '        dsList = objEmployee.GetEmployeeList("Employee", False, Not ConfigParameter._Object._IsIncludeInactiveEmp)
'    '        'S.SANDEEP [ 29 JUNE 2011 ] -- END 

'    '        objfrm.DataSource = dsList.Tables("Employee")
'    '        objfrm.ValueMember = "employeeunkid"
'    '        objfrm.DisplayMember = "employeename"
'    '        objfrm.CodeMember = "employeecode"
'    '        If objfrm.DisplayDialog Then
'    '            txtEmpcode.Text = objfrm.SelectedAlias
'    '            txtEmpcode.Tag = objfrm.SelectedValue

'    '            txtCode.Tag = objfrm.SelectedValue
'    '            txtCode.Text = txtEmpcode.Text

'    '            txtEmployeeName.Text = objfrm.SelectedText
'    '            GetValue()

'    '            Dim FirstDate As New DateTime(MonthCalendar.SelectionStart.Year, MonthCalendar.SelectionStart.Month, 1)
'    '            Dim LastDate As New DateTime(MonthCalendar.SelectionStart.Year, MonthCalendar.SelectionStart.Month, Date.DaysInMonth(MonthCalendar.SelectionStart.Year, MonthCalendar.SelectionStart.Month))
'    '            MonthCalendar_DateChanged(sender, New DateRangeEventArgs(FirstDate, LastDate))
'    '        End If

'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "objbtnSearchEmpDiary_Click", mstrModuleName)
'    '    Finally
'    '        objfrm = Nothing
'    '        objEmployee = Nothing
'    '        dsList.Dispose()
'    '    End Try
'    'End Sub

'    'S.SANDEEP [ 29 JUNE 2011 ] -- END 

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Me.Close()
'    End Sub

'#End Region

'#Region "ListView's Event"

'    Private Sub lstEmployeedata_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lstEmployeedata.ItemChecked
'        Dim dsData As DataSet = Nothing
'        Dim dtFill As DataTable = Nothing
'        Try

'            If e.Item.SubItems(colhRemarks.Index).Text.Trim = "Holiday" Then

'                'START FOR EMPLOYEE HOLIDAY

'                If e.Item.Checked Then

'                    Dim objEmpholiday As New clsemployee_holiday
'                    dsData = objEmpholiday.GetList("List")
'                    dtFill = New DataView(dsData.Tables("List"), "employeeunkid=" & CInt(txtEmpcode.Tag), "holidaydate asc", DataViewRowState.CurrentRows).ToTable

'                    For Each dr As DataRow In dtFill.Rows
'                        If dr("holidaydate") IsNot DBNull.Value Then
'                            If eZeeDate.convertDate(dr("holidaydate").ToString).Month = MonthCalendar.SelectionStart.Month And eZeeDate.convertDate(dr("holidaydate").ToString).Year = MonthCalendar.SelectionStart.Year Then
'                                Dim dr1 As DataRow = dsEmployeeData.Tables("Employeedata").NewRow
'                                dr1("Items") = e.Item.Text
'                                dr1("Date") = eZeeDate.convertDate(dr("holidaydate").ToString)
'                                dr1("Description") = dr("holidayname")
'                                dsEmployeeData.Tables("Employeedata").Rows.Add(dr1)
'                            End If
'                        End If
'                    Next

'                ElseIf e.Item.Checked = False Then

'                    Dim drRow As DataRow() = dsEmployeeData.Tables("EmployeeData").Select("Items='" & e.Item.Text & "'")
'                    If drRow.Length > 0 Then
'                        For Each dr As DataRow In drRow
'                            dsEmployeeData.Tables("Employeedata").Rows.Remove(dr)
'                        Next
'                    End If
'                End If

'                'END FOR EMPLOYEE HOLIDAY

'            ElseIf e.Item.SubItems(colhRemarks.Index).Text.Trim = "Birthday" Then

'                'START FOR EMPLOYEE BIRTHDAY
'                If e.Item.Checked Then

'                    Dim objemployee As New clsEmployee_Master
'                    objemployee._Employeeunkid = CInt(txtEmpcode.Tag)

'                    If objemployee._Birthdate <> Nothing Then
'                        If objemployee._Birthdate.Month = MonthCalendar.SelectionStart.Month Then
'                            Dim dr1 As DataRow = dsEmployeeData.Tables("Employeedata").NewRow
'                            dr1("Items") = e.Item.Text
'                            dr1("Date") = eZeeDate.convertDate(MonthCalendar.SelectionStart.Year & objemployee._Birthdate.Month.ToString("0#") & objemployee._Birthdate.Day.ToString("0#"))
'                            dr1("Description") = e.Item.Text
'                            dsEmployeeData.Tables("Employeedata").Rows.Add(dr1)
'                        End If
'                    End If

'                ElseIf e.Item.Checked = False Then

'                    Dim drRow As DataRow() = dsEmployeeData.Tables("EmployeeData").Select("Items='" & e.Item.Text & "'")
'                    If drRow.Length > 0 Then
'                        For Each dr As DataRow In drRow
'                            dsEmployeeData.Tables("Employeedata").Rows.Remove(dr)
'                        Next
'                    End If
'                End If
'                'END FOR EMPLOYEE BIRTHDAY

'            ElseIf e.Item.SubItems(colhRemarks.Index).Text.Trim = "Anniversary" Then

'                'START FOR EMPLOYEE ANNIVERSARY
'                If e.Item.Checked Then

'                    Dim objemployee As New clsEmployee_Master
'                    objemployee._Employeeunkid = CInt(txtEmpcode.Tag)

'                    If objemployee._Anniversary_Date <> Nothing Then
'                        If objemployee._Anniversary_Date.Month = MonthCalendar.SelectionStart.Month Then
'                            Dim dr1 As DataRow = dsEmployeeData.Tables("Employeedata").NewRow
'                            dr1("Items") = e.Item.Text
'                            dr1("Date") = eZeeDate.convertDate(MonthCalendar.SelectionStart.Year & objemployee._Anniversary_Date.Month.ToString("0#") & objemployee._Anniversary_Date.Day.ToString("0#"))
'                            dr1("Description") = e.Item.Text
'                            dsEmployeeData.Tables("Employeedata").Rows.Add(dr1)
'                        End If
'                    End If

'                ElseIf e.Item.Checked = False Then

'                    Dim drRow As DataRow() = dsEmployeeData.Tables("EmployeeData").Select("Items='" & e.Item.Text & "'")
'                    If drRow.Length > 0 Then
'                        For Each dr As DataRow In drRow
'                            dsEmployeeData.Tables("Employeedata").Rows.Remove(dr)
'                        Next
'                    End If
'                End If
'                'END FOR EMPLOYEE ANNIVERSARY


'            ElseIf e.Item.SubItems(colhRemarks.Index).Text.Trim = "Leave" Then

'                'START FOR EMPLOYEE LEAVE
'                If e.Item.Checked Then

'                    Dim objLeaveIssue As New clsleaveissue_Tran
'                    dsData = objLeaveIssue.GetList("LeaveData", True)
'                    dtFill = New DataView(dsData.Tables("LeaveData"), "employeeunkid = " & CInt(txtEmpcode.Tag) & " AND leavetypeunkid=" & CInt(e.Item.Tag), "leavedate asc", DataViewRowState.CurrentRows).ToTable

'                    For Each dr As DataRow In dtFill.Rows
'                        If eZeeDate.convertDate(dr("leavedate").ToString).Month = MonthCalendar.SelectionStart.Month And eZeeDate.convertDate(dr("leavedate").ToString).Year = MonthCalendar.SelectionStart.Year Then
'                            Dim dr1 As DataRow = dsEmployeeData.Tables("Employeedata").NewRow
'                            dr1("Items") = e.Item.Text
'                            dr1("Date") = eZeeDate.convertDate(dr("leavedate").ToString)
'                            dr1("Description") = e.Item.Text
'                            dsEmployeeData.Tables("Employeedata").Rows.Add(dr1)
'                        End If
'                    Next

'                ElseIf e.Item.Checked = False Then

'                    Dim drRow As DataRow() = dsEmployeeData.Tables("EmployeeData").Select("Items='" & e.Item.Text & "'")
'                    If drRow.Length > 0 Then
'                        For Each dr As DataRow In drRow
'                            dsEmployeeData.Tables("Employeedata").Rows.Remove(dr)
'                        Next
'                    End If
'                End If

'                'END FOR EMPLOYEE LEAVE
'                End If

'                FillGrid()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lstEmployeedata_ItemChecked", mstrModuleName)
'        Finally
'            If dtFill IsNot Nothing Then dtFill.Dispose()
'            If dsData IsNot Nothing Then dsData.Dispose()
'        End Try
'    End Sub

'#End Region

'#Region "MonthCalendar's Event"

'    Private Sub MonthCalendar_DateChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles MonthCalendar.DateChanged
'        Try
'            dsEmployeeData.Tables("Employeedata").Rows.Clear()
'            For i As Integer = 0 To lstEmployeedata.CheckedItems.Count - 1
'                lstEmployeedata_ItemChecked(New Object, New ItemCheckedEventArgs(lstEmployeedata.CheckedItems(i)))
'            Next
'            lblAsOn.Text = "As On Month " & MonthName(MonthCalendar.SelectionStart.Month) & " " & MonthCalendar.SelectionStart.Year
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "MonthCalendar_DateChanged", mstrModuleName)
'        End Try
'    End Sub

'#End Region