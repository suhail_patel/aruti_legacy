﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Memberships

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDairy_Memberships_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            lvMembershipInfo.GridLines = False
            Call Fill_Data()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objfrmDairy_Memberships_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dtTable As DataTable
        Dim objMemTran As New clsMembershipTran
        'S.SANDEEP [ 03 AUG 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim objIdentity As New clsIdentity_tran
        'S.SANDEEP [ 03 AUG 2013 ] -- END
        Dim lvItem As ListViewItem
        Try
            objMemTran._EmployeeUnkid = mintEmployeeId
            dtTable = objMemTran._DataList.Copy
            lvMembershipInfo.Items.Clear()
            For Each dRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dRow.Item("ccategory").ToString
                lvItem.SubItems.Add(dRow.Item("catagory").ToString)
                lvItem.SubItems.Add(dRow.Item("membership").ToString)
                lvItem.SubItems.Add(dRow.Item("membershipno").ToString)
                lvItem.SubItems.Add(dRow.Item("remark").ToString)

                lvMembershipInfo.Items.Add(lvItem)
            Next
            lvMembershipInfo.GroupingColumn = objcolhDCategory
            lvMembershipInfo.DisplayGroups(True)
            lvMembershipInfo.GridLines = False

            'S.SANDEEP [ 03 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objIdentity._EmployeeUnkid = mintEmployeeId
            dtTable = objIdentity._DataList.Copy

            For Each dRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = dRow.Item("identities").ToString
                lvItem.SubItems.Add(dRow.Item("serial_no").ToString)
                lvItem.SubItems.Add(dRow.Item("identity_no").ToString)
                lvItem.SubItems.Add(dRow.Item("country").ToString)

                lvIdendtifyInformation.Items.Add(lvItem)
            Next
            'S.SANDEEP [ 03 AUG 2013 ] -- END

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elMemberships.Text = Language._Object.getCaption(Me.elMemberships.Name, Me.elMemberships.Text)
			Me.colhMCategory.Text = Language._Object.getCaption(CStr(Me.colhMCategory.Tag), Me.colhMCategory.Text)
			Me.colhMembership.Text = Language._Object.getCaption(CStr(Me.colhMembership.Tag), Me.colhMembership.Text)
			Me.colhMNumber.Text = Language._Object.getCaption(CStr(Me.colhMNumber.Tag), Me.colhMNumber.Text)
			Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
			Me.elIdentities.Text = Language._Object.getCaption(Me.elIdentities.Name, Me.elIdentities.Text)
			Me.colhIdType.Text = Language._Object.getCaption(CStr(Me.colhIdType.Tag), Me.colhIdType.Text)
			Me.colhIdSerialNo.Text = Language._Object.getCaption(CStr(Me.colhIdSerialNo.Tag), Me.colhIdSerialNo.Text)
			Me.colhIdNo.Text = Language._Object.getCaption(CStr(Me.colhIdNo.Tag), Me.colhIdNo.Text)
			Me.colhIssueCountry.Text = Language._Object.getCaption(CStr(Me.colhIssueCountry.Tag), Me.colhIssueCountry.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class