﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmDiary_Leave

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDiary"
    Private objLeaveForm As clsleaveform
    Private mintEmployeeId As Integer = -1

#End Region

#Region " Contructor "

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal intEmpId As Integer)
        InitializeComponent()
        objLeaveForm = New clsleaveform
        mintEmployeeId = intEmpId
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmDiary_Leave_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (10-Jul-2014) -- Start
            'Enhancement - Language Settings
            Language.setLanguage(mstrModuleName)
            OtherSettings()
            'Pinkal (10-Jul-2014) -- End
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmDiary_Leave_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    Private Sub Fill_Data()
        Dim dsList As New DataSet
        Dim dtTable As New DataTable
        Try

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'dsList = objLeaveForm.GetList("List", True)
            dsList = objLeaveForm.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                        , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting _
                                                        , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, -1, True, -1, "")
            'Pinkal (24-Aug-2015) -- End



            dtTable = New DataView(dsList.Tables(0), "employeeunkid = '" & mintEmployeeId & "' AND startdate < '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "'", "", DataViewRowState.CurrentRows).ToTable

            For Each dtRow As DataRow In dtTable.Rows
                Dim lvItem As New ListViewItem
                lvItem.UseItemStyleForSubItems = False

                lvItem.Text = dtRow.Item("leavename").ToString
                lvItem.BackColor = Color.FromArgb(CInt(dtRow.Item("color")))
                lvItem.ForeColor = Color.White

                lvItem.SubItems.Add(dtRow.Item("formno").ToString)
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("startdate").ToString).ToShortDateString)
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("returndate").ToString).ToShortDateString)
                lvItem.SubItems.Add(dtRow.Item("status").ToString)

                lvPreviousLeaveList.Items.Add(lvItem)

                lvItem = Nothing
            Next

            dtTable = New DataView(dsList.Tables(0), "employeeunkid = '" & mintEmployeeId & "' AND startdate > '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "'", "", DataViewRowState.CurrentRows).ToTable

            For Each dtRow As DataRow In dtTable.Rows
                Dim lvItem As New ListViewItem
                lvItem.UseItemStyleForSubItems = False

                lvItem.Text = dtRow.Item("leavename").ToString
                lvItem.BackColor = Color.FromArgb(CInt(dtRow.Item("color")))
                lvItem.ForeColor = Color.White

                lvItem.SubItems.Add(dtRow.Item("formno").ToString)
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("startdate").ToString).ToShortDateString)
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("returndate").ToString).ToShortDateString)
                lvItem.SubItems.Add(dtRow.Item("status").ToString)

                lvNextLeaveList.Items.Add(lvItem)

                lvItem = Nothing

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub lvLeaveList_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvNextLeaveList.ItemSelectionChanged, lvPreviousLeaveList.ItemSelectionChanged
        Try
            e.Item.Selected = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLeaveList_ItemSelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetLanguage()
		Try
            Me.Text = Language._Object.getCaption(mstrModuleName, Me.Text)
			
			Me.elPreviousLeave.Text = Language._Object.getCaption(Me.elPreviousLeave.Name, Me.elPreviousLeave.Text)
			Me.elForecastedLeave.Text = Language._Object.getCaption(Me.elForecastedLeave.Name, Me.elForecastedLeave.Text)
			Me.colhPreLeaveType.Text = Language._Object.getCaption(CStr(Me.colhPreLeaveType.Tag), Me.colhPreLeaveType.Text)
			Me.colhPreLeaveFrom.Text = Language._Object.getCaption(CStr(Me.colhPreLeaveFrom.Tag), Me.colhPreLeaveFrom.Text)
			Me.colhPreStartDate.Text = Language._Object.getCaption(CStr(Me.colhPreStartDate.Tag), Me.colhPreStartDate.Text)
			Me.colhPreReturnDate.Text = Language._Object.getCaption(CStr(Me.colhPreReturnDate.Tag), Me.colhPreReturnDate.Text)
			Me.colhPreStatus.Text = Language._Object.getCaption(CStr(Me.colhPreStatus.Tag), Me.colhPreStatus.Text)
			Me.colhNxtLeaveType.Text = Language._Object.getCaption(CStr(Me.colhNxtLeaveType.Tag), Me.colhNxtLeaveType.Text)
			Me.colhNxtFormNo.Text = Language._Object.getCaption(CStr(Me.colhNxtFormNo.Tag), Me.colhNxtFormNo.Text)
			Me.colhNxtStDate.Text = Language._Object.getCaption(CStr(Me.colhNxtStDate.Tag), Me.colhNxtStDate.Text)
			Me.colhNxtEndDate.Text = Language._Object.getCaption(CStr(Me.colhNxtEndDate.Tag), Me.colhNxtEndDate.Text)
			Me.colhNxtStatus.Text = Language._Object.getCaption(CStr(Me.colhNxtStatus.Tag), Me.colhNxtStatus.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class