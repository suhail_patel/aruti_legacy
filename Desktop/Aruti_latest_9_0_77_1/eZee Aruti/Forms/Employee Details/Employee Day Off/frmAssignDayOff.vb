﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssignDayOff

    Private mstrModuleName As String = "frmAssignDayOff"

#Region " Private Variables "
    Private menAction As enAction = enAction.ADD_ONE
    Private mblnCancel As Boolean = True
    Private mintEmployeeDayOffTranUnkid As Integer = -1
    Private mdtEmployee As DataTable
    Private dView As DataView
    Private objEmp As clsEmployee_Master
    Private objEmpdayoff As clsemployee_dayoff_Tran
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintEmployeeDayOffTranUnkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintEmployeeDayOffTranUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods/Function "

    Private Sub Fill_Emp_Grid()
        Try
            Dim dsEmp As New DataSet : Dim sValue As String = String.Empty
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsEmp = objEmp.GetList("List", True, , dtpToDate.Value.Date, dtpToDate.Value.Date, , , , mstrAdvanceFilter)
            dsEmp = objEmp.GetList(FinancialYear._Object._DatabaseName, _
                                   User._Object._Userunkid, _
                                   FinancialYear._Object._YearUnkid, _
                                   Company._Object._Companyunkid, _
                                   dtpToDate.Value.Date, _
                                   dtpToDate.Value.Date, _
                                   ConfigParameter._Object._UserAccessModeSetting, _
                                   True, False, "List", _
                                   ConfigParameter._Object._ShowFirstAppointmentDate, , , _
                                   mstrAdvanceFilter)
            'S.SANDEEP [04 JUN 2015] -- END

            If sValue.Trim.Length > 0 Then
                mdtEmployee = New DataView(dsEmp.Tables("List"), "employeeunkid NOT IN(" & sValue & ")", "", DataViewRowState.CurrentRows).ToTable
            Else
                mdtEmployee = New DataView(dsEmp.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
            dView = mdtEmployee.DefaultView
            dgvEmp.AutoGenerateColumns = False

            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "name"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"

            dgvEmp.DataSource = dView

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Emp_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function isValid_Data() As Boolean
        Try
            Dim mstrEmpID As String = ""
            mdtEmployee.AcceptChanges()
            Dim dtmp() As DataRow = mdtEmployee.Select("ischeck=true")

            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one employee in order to assign dayoff to employee(s)."), enMsgBoxStyle.Information)
                Return False
            Else
                For i As Integer = 0 To dtmp.Length - 1
                    mstrEmpID &= CInt(dtmp(i)("employeeunkid")) & ","
                Next
                If mstrEmpID.Trim.Length > 0 Then
                    mstrEmpID = mstrEmpID.Trim.Substring(0, mstrEmpID.Trim.Length - 1)
                End If
            End If



            'Pinkal (20-Jan-2014) -- Start
            'Enhancement : Oman Changes
            If dtpFromDate.Value.Date > dtpToDate.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "To Date must be greater than From date.Please Select Correct To Date."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                dtpToDate.Select()
                Return False
            End If
            'Pinkal (20-Jan-2014) -- End



            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ConfigParameter._Object._IsArutiDemo Then

                Dim objMaster As New clsMasterData
                Dim iPeriod As Integer = 0

                'Pinkal (20-Jan-2014) -- Start
                'Enhancement : Oman Changes
                'iPeriod = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpToDate.Value.Date, , FinancialYear._Object._YearUnkid, True)
                iPeriod = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpToDate.Value.Date)
                'Pinkal (20-Jan-2014) -- End

                Dim objPrd As New clscommom_period_Tran
                If iPeriod > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPrd._Periodunkid = iPeriod
                    objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriod
                    'Sohail (21 Aug 2015) -- End
                    If objPrd._Statusid = enStatusType.Close Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot assign dayoff. Reason : Period is already closed for the selected date."), enMsgBoxStyle.Information)
                        Return False
                    End If

                    Dim objLeaveTran As New clsTnALeaveTran

                    'Pinkal (03-Jan-2014) -- Start
                    'Enhancement : Oman Changes

                    'If objLeaveTran.IsPayrollProcessDone(objPrd._Periodunkid, mstrEmpID, objPrd._End_Date.Date) Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "you can't assign day off to checked employee(s).Reason:Payroll Process already done for checked employee(s) for last date of current open period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    '    Return False
                    'End If

                    Dim mdtTnADate As DateTime = Nothing
                    If objPrd._TnA_EndDate.Date < dtpFromDate.Value.Date Then
                        mdtTnADate = dtpFromDate.Value.Date
                    Else
                        mdtTnADate = objPrd._TnA_EndDate.Date
                    End If

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objLeaveTran.IsPayrollProcessDone(objPrd._Periodunkid, mstrEmpID, mdtTnADate.Date, enModuleReference.TnA) Then
                    If objLeaveTran.IsPayrollProcessDone(objPrd._Periodunkid(FinancialYear._Object._DatabaseName), mstrEmpID, mdtTnADate.Date, enModuleReference.TnA) Then
                        'Sohail (21 Aug 2015) -- End
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "you can't assign day off to checked employee(s).Reason:Payroll Process already done for checked employee(s) for last date of current open period."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "you can't assign day off to checked employee(s).Reason:Payroll Process already done for checked employee(s) for last date of current open period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 9, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                        Return False
                    End If

                    'Pinkal (03-Jan-2014) -- End

                End If
                objMaster = Nothing

            End If



            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isVaild_Data", mstrModuleName)
        Finally
        End Try
    End Function

    Private Function SetAssignDayOff(ByVal dtmp() As DataRow) As DataTable
        Dim mdtEmpDayOff As DataTable = Nothing
        Try
            mdtEmpDayOff = New DataTable
            mdtEmpDayOff.Columns.Add("employeeunkid", Type.GetType("System.Int32"))
            mdtEmpDayOff.Columns("employeeunkid").DefaultValue = -1
            mdtEmpDayOff.Columns.Add("dayoffdate", Type.GetType("System.DateTime"))
            mdtEmpDayOff.Columns("dayoffdate").DefaultValue = Nothing
            Dim drRow As DataRow = Nothing
            Dim intDays As Integer = CInt(DateDiff(DateInterval.Day, dtpFromDate.Value.Date, dtpToDate.Value.Date.AddDays(1)))
            For i As Integer = 0 To dtmp.Length - 1
                For j As Integer = 0 To intDays - 1
                    drRow = mdtEmpDayOff.NewRow()
                    drRow("employeeunkid") = dtmp(i)("employeeunkid")
                    drRow("dayoffdate") = dtpFromDate.Value.Date.AddDays(j)
                    mdtEmpDayOff.Rows.Add(drRow)
                Next
            Next
            mdtEmpDayOff.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetAssignDayOff", mstrModuleName)
        End Try
        Return mdtEmpDayOff
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmAssignDayOff_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmp = New clsEmployee_Master
        objEmpdayoff = New clsemployee_dayoff_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Pinkal (20-Jan-2014) -- Start
            'Enhancement : Oman Changes [voltamp]
            'dtpToDate.MinDate = FinancialYear._Object._Database_Start_Date
            'Pinkal (20-Jan-2014) -- End

            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignDayOff_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            If isValid_Data() = False Then Exit Sub
            Dim dtmp() As DataRow = mdtEmployee.Select("ischeck=true")
            Dim mblnInsertFlag As Boolean = False
            Dim mdtEmpDayOff As DataTable = SetAssignDayOff(dtmp)
            objEmpdayoff._Userunkid = User._Object._Userunkid


            Dim mblnIsExist As Boolean = False
            Dim mintEmployeeID As Integer = 0
            Dim mintCount As Integer = 0

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objEmpdayoff._FormName = mstrModuleName
            objEmpdayoff._LoginEmployeeunkid = 0
            objEmpdayoff._ClientIP = getIP()
            objEmpdayoff._HostName = getHostName()
            objEmpdayoff._FromWeb = False
            objEmpdayoff._AuditUserId = User._Object._Userunkid
objEmpdayoff._CompanyUnkid = Company._Object._Companyunkid
            objEmpdayoff._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            For i As Integer = 0 To mdtEmpDayOff.Rows.Count - 1
                If objEmpdayoff.isExist(CDate(mdtEmpDayOff.Rows(i)("dayoffdate")), CInt(mdtEmpDayOff.Rows(i)("employeeunkid"))) = False Then
                    objEmpdayoff._Employeeunkid = CInt(mdtEmpDayOff.Rows(i)("employeeunkid"))
                    objEmpdayoff._Dayoffdate = CDate(mdtEmpDayOff.Rows(i)("dayoffdate"))
                    mblnInsertFlag = objEmpdayoff.Insert()
                Else

                    If mintEmployeeID <> CInt(mdtEmpDayOff.Rows(i)("employeeunkid")) Then
                        dgvEmp.Rows(mintCount).Cells(dgcolhEcode.Index).Style.ForeColor = Color.Red
                        dgvEmp.Rows(mintCount).Cells(dgcolhEcode.Index).Style.SelectionForeColor = Color.Red
                        dgvEmp.Rows(mintCount).Cells(dgcolhEName.Index).Style.ForeColor = Color.Red
                        dgvEmp.Rows(mintCount).Cells(dgcolhEName.Index).Style.SelectionForeColor = Color.Red
                        mintEmployeeID = CInt(mdtEmpDayOff.Rows(i)("employeeunkid"))
                        mintCount += 1
                    End If

                    If mblnIsExist = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "There are some of checked employee(s), to whom day off is already assigned between the selected date range due to this those employee will be skipped for this day off process which are highlighted in red."), enMsgBoxStyle.Information)
                        mblnIsExist = True : Continue For
                    Else
                        Continue For
                    End If

                End If
            Next

            If mblnInsertFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Day Off assigned successfully."), enMsgBoxStyle.Information)
                dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                objchkEmployee.Checked = False
                objEmpdayoff._Empdayofftranunkid = -1
            ElseIf objEmpdayoff._Message <> "" Then
                eZeeMsgBox.Show(objEmpdayoff._Message, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Problem in assigning dayoff."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            If mstrAdvanceFilter.Trim.Length > 0 Then
                mstrAdvanceFilter = "" : Call Fill_Emp_Grid()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "DatePicker Event"

    Private Sub dtpFromDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFromDate.ValueChanged, dtpToDate.ValueChanged
        Try
            Call Fill_Emp_Grid()
            objchkEmployee.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpFromDate_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvEmp.Rows.Count > 0 Then
                        If dgvEmp.SelectedRows(0).Index = dgvEmp.Rows(dgvEmp.RowCount - 1).Index Then Exit Sub
                        dgvEmp.Rows(dgvEmp.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvEmp.Rows.Count > 0 Then
                        If dgvEmp.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvEmp.Rows(dgvEmp.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' OR " & _
                            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearch.Text & "%'"
            End If
            dView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Link Button Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                Call Fill_Emp_Grid()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region "DataGrid Event"

    Private Sub dgvEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmp.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvEmp.IsCurrentCellDirty Then
                    Me.dgvEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dView.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmp_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " CheckBox Events "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvEmp.CellContentClick, AddressOf dgvEmp_CellContentClick
            For Each dr As DataRowView In dView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvEmp.Refresh()
            AddHandler dgvEmp.CellContentClick, AddressOf dgvEmp_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbFilter.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilter.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.Name, Me.gbFilter.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please check atleast one employee in order to assign dayoff to employee(s).")
            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot assign dayoff. Reason : Period is already closed for the selected date.")
            Language.setMessage(mstrModuleName, 3, "There are some of checked employee(s), to whom day off is already assigned between the selected date range due to this those employee will be skipped for this day off process which are highlighted in red.")
            Language.setMessage(mstrModuleName, 4, "Day Off assigned successfully.")
            Language.setMessage(mstrModuleName, 5, "Problem in assigning dayoff.")
            Language.setMessage(mstrModuleName, 7, "you can't assign day off to checked employee(s).Reason:Payroll Process already done for checked employee(s) for last date of current open period.")
            Language.setMessage(mstrModuleName, 8, "To Date must be greater than From date.Please Select Correct To Date.")
			Language.setMessage(mstrModuleName, 9, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class