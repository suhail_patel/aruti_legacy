﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text
Imports System.IO
Imports System.IO.Packaging
#End Region

Public Class frmAppRejEmpData

#Region " Private Varaibles "

    Private objApprovalData As clsEmployeeDataApproval
    Private ReadOnly mstrModuleName As String = "frmAppRejEmpData"
    Private mdvData As DataView
    Private intPrivilegeId As Integer = 0
    Private eScreenType As enScreenName
    Private mstrAdvanceFilter As String = String.Empty
    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.


    Private imgView As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.download_16)
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone

    Private mdtDocuments As DataTable
    Private objDocument As clsScan_Attach_Documents
    'Gajanan [17-DEC-2018] -- End

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim rowIndex As Integer = -1
    'Gajanan [22-Feb-2019] -- End

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                            ConfigParameter._Object._UserAccessModeSetting, True, False, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                If CInt(.SelectedValue) <= 0 Then
                    .Text = ""
                End If
            End With

            dsList = objApprovalData.EmployeeDataApprovalList(User._Object._Userunkid, Company._Object._Companyunkid, User._Object._Languageunkid, "List", True)
            With cboEmployeeData
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objApprovalData.getOperationTypeList("List", True)
            With cboOperationType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP |15-APR-2019| -- START
            Dim objPeriod As New clscommom_period_Tran
            Dim dsCombo As New DataSet
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            With cboEffectivePeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
            objPeriod = Nothing
            'S.SANDEEP |15-APR-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            Dim mstrFilterstring As String = String.Empty

            If txtApprover.Text.Trim.Length > 0 Then
                If CInt(cboEmployee.SelectedValue) > 0 Then
                    mstrFilterstring = " EM.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'"
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    mstrFilterstring &= mstrAdvanceFilter
                End If
            End If

            Dim dt As DataTable = objApprovalData.GetEmployeeApprovalData(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, intPrivilegeId, User._Object._Userunkid, CInt(txtLevel.Tag), False, eScreenType, ConfigParameter._Object._EmployeeAsOnDate, mstrFilterstring, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), Nothing, False)

            dgvData.AutoGenerateColumns = False

            objdgcolhicheck.DataPropertyName = "iCheck"
            objdgcolhIsGrp.DataPropertyName = "isgrp"
            objdgcolhEmpid.DataPropertyName = "employeeunkid"
            objcolhqualificationgroupunkid.DataPropertyName = "qualificationgroupunkid"
            objcolhqualificationunkid.DataPropertyName = "qualificationunkid"

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            objcolhqualificationtranunkid.DataPropertyName = "qualificationtranunkid"
            'Gajanan [22-Feb-2019] -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objcolhlinkedmasterid.DataPropertyName = "linkedmasterid"
            'Gajanan [17-DEC-2018] -- End


            dgcolhSkillCategory.DataPropertyName = "scategory"
            dgcolhSkill.DataPropertyName = "skill"
            dgcolhDescription.DataPropertyName = "description"

            dgcolhQualifyGroup.DataPropertyName = "qualificationgrpname"
            dgcolhQualification.DataPropertyName = "qualificationname"
            dgcolhAwardDate.DataPropertyName = "award_start_date"
            dgcolhAwardToDate.DataPropertyName = "award_end_date"
            dgcolhInstitute.DataPropertyName = "institute_name"
            dgcolhRefNo.DataPropertyName = "reference_no"

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            objcolhformname.DataPropertyName = "form_name"

            'S.SANDEEP |26-APR-2019| -- START
            'objcolhnewattachdocumentid.DataPropertyName = "newattachdocumentid"
            'objcolhdeleteattachdocumentid.DataPropertyName = "deleteattachdocumentid"
            'S.SANDEEP |26-APR-2019| -- END

            'Gajanan [22-Feb-2019] -- End


            dgcolhCompany.DataPropertyName = "cname"
            dgcolhJob.DataPropertyName = "old_job"
            dgcolhStartDate.DataPropertyName = "start_date"
            dgcolhEndDate.DataPropertyName = "end_date"
            dgcolhSupervisor.DataPropertyName = "supervisor"
            dgcolhRemark.DataPropertyName = "remark"

            dgcolhRefreeName.DataPropertyName = "rname"
            dgcolhCountry.DataPropertyName = "Country"
            dgcolhIdNo.DataPropertyName = "Company"
            dgcolhEmail.DataPropertyName = "Email"
            dgcolhTelNo.DataPropertyName = "telephone_no"
            dgcolhMobile.DataPropertyName = "mobile_no"


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            dgcolhDBName.DataPropertyName = "dependantname"
            dgcolhDBRelation.DataPropertyName = "relation"
            dgcolhDBbirthdate.DataPropertyName = "birthdate"
            dgcolhDBIdentifyNo.DataPropertyName = "identify_no"
            dgcolhDBGender.DataPropertyName = "gender"
            objcolhdepedantunkid.DataPropertyName = "dpndtbeneficetranunkid"
            'Gajanan [22-Feb-2019] -- End

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            dgcolhIdType.DataPropertyName = "IdType"
            dgcolhSrNo.DataPropertyName = "serial_no"
            dgcolhIdentityNo.DataPropertyName = "identity_no"
            dgcolhIdCountry.DataPropertyName = "Country"
            dgcolhIdIssuePlace.DataPropertyName = "issued_place"
            dgcolhIdIssueDate.DataPropertyName = "issue_date"
            dgcolhIdExpiryDate.DataPropertyName = "expiry_date"
            dgcolhIdDLClass.DataPropertyName = "dl_class"
            'Gajanan [22-Feb-2019] -- End


            'S.SANDEEP |15-APR-2019| -- START
            dgcolhCategory.DataPropertyName = "Category"
            dgcolhmembershipname.DataPropertyName = "membershipname"
            dgcolhmembershipno.DataPropertyName = "membershipno"
            dgcolhperiod_name.DataPropertyName = "period_name"

            dgcolhissue_date.DataPropertyName = "issue_date"
            dgcolhissue_date.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat

            dgcolhstart_date.DataPropertyName = "start_date"
            dgcolhstart_date.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat

            dgcolhexpiry_date.DataPropertyName = "expiry_date"
            dgcolhexpiry_date.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat

            dgcolhMemRemark.DataPropertyName = "remark"
            'S.SANDEEP |15-APR-2019| -- END





            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Select Case eScreenType
                Case enScreenName.frmAddressList
                    dgcolhAddressType.DataPropertyName = "addresstype"
                    dgcolhAddress1.DataPropertyName = "address1"
                    dgcolhAddress2.DataPropertyName = "address2"
                    dgcolhAddressCountry.DataPropertyName = "country"
                    dgcolhAddressState.DataPropertyName = "state"
                    dgcolhAddressCity.DataPropertyName = "city"
                    dgcolhAddressZipCode.DataPropertyName = "zipcode_code"
                    dgcolhAddressProvince.DataPropertyName = "provicnce"
                    dgcolhAddressRoad.DataPropertyName = "road"
                    dgcolhAddressEstate.DataPropertyName = "estate"
                    dgcolhAddressProvince1.DataPropertyName = "provicnce1"
                    dgcolhAddressRoad1.DataPropertyName = "Road1"
                    dgcolhAddressChiefdom.DataPropertyName = "chiefdom"
                    dgcolhAddressVillage.DataPropertyName = "village"
                    dgcolhAddressTown.DataPropertyName = "town"
                    dgcolhAddressMobile.DataPropertyName = "mobile"
                    dgcolhAddressTel_no.DataPropertyName = "tel_no"
                    dgcolhAddressPlotNo.DataPropertyName = "plotNo"
                    dgcolhAddressAltNo.DataPropertyName = "alternateno"
                    dgcolhAddressEmail.DataPropertyName = "email"
                    dgcolhAddressFax.DataPropertyName = "fax"
                    'S.SANDEEP |26-APR-2019| -- START
                    objcolhnewattachdocumentid.DataPropertyName = "newattachdocumentid"
                    objcolhdeleteattachdocumentid.DataPropertyName = "deleteattachdocumentid"
                    'S.SANDEEP |26-APR-2019| -- END

                Case enScreenName.frmEmergencyAddressList
                    dgcolhAddressType.DataPropertyName = "addresstype"
                    dgcolhFirstname.DataPropertyName = "firstname"
                    dgcolhLastname.DataPropertyName = "lastname"
                    dgcolhAddressCountry.DataPropertyName = "country"
                    dgcolhAddressState.DataPropertyName = "state"
                    dgcolhAddressCity.DataPropertyName = "city"
                    dgcolhAddressZipCode.DataPropertyName = "zipcode_code"
                    dgcolhAddressProvince.DataPropertyName = "provicnce"
                    dgcolhAddressRoad.DataPropertyName = "road"
                    dgcolhAddressEstate.DataPropertyName = "estate"
                    dgcolhAddressPlotNo.DataPropertyName = "plotNo"
                    dgcolhAddressMobile.DataPropertyName = "mobile"
                    dgcolhAddressAltNo.DataPropertyName = "alternateno"
                    dgcolhAddressTel_no.DataPropertyName = "tel_no"
                    dgcolhAddressFax.DataPropertyName = "fax"
                    dgcolhAddressEmail.DataPropertyName = "email"

                    'S.SANDEEP |26-APR-2019| -- START
                    objcolhnewattachdocumentid.DataPropertyName = "newattachdocumentid"
                    objcolhdeleteattachdocumentid.DataPropertyName = "deleteattachdocumentid"
                    'S.SANDEEP |26-APR-2019| -- END

            End Select
            'Gajanan [18-Mar-2019] -- End


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            dgcolhBirthCountry.DataPropertyName = "country"
            dgcolhBirthState.DataPropertyName = "state"
            dgcolhBirthCity.DataPropertyName = "city"
            dgcolhBirthCertificateNo.DataPropertyName = "birthcertificateno"
            dgcolhBirthWard.DataPropertyName = "birth_ward"
            dgcolhBirthVillage.DataPropertyName = "birth_village"
            dgcolhBirthTown1.DataPropertyName = "town"
            dgcolhBirthVillage1.DataPropertyName = "village"
            dgcolhBirthChiefdom.DataPropertyName = "chiefdom"

            dgcolhComplexion.DataPropertyName = "Complexion"
            dgcolhBloodGroup.DataPropertyName = "BloodGroup"
            dgcolhEyeColor.DataPropertyName = "EyeColor"
            dgcolhNationality.DataPropertyName = "Nationality"
            dgcolhEthinCity.DataPropertyName = "Ethnicity"
            dgcolhReligion.DataPropertyName = "Religion"
            dgcolhHair.DataPropertyName = "HairColor"
            dgcolhMaritalStatus.DataPropertyName = "Maritalstatus"
            dgcolhExtraTel.DataPropertyName = "ExtTelephoneno"
            dgcolhLanguage1.DataPropertyName = "language1"
            dgcolhLanguage2.DataPropertyName = "language2"
            dgcolhLanguage3.DataPropertyName = "language3"
            dgcolhLanguage4.DataPropertyName = "language4"
            dgcolhHeight.DataPropertyName = "height"
            dgcolhWeight.DataPropertyName = "weight"
            dgcolhMaritalDate.DataPropertyName = "anniversary_date"
            dgcolhAllergies.DataPropertyName = "Allergies"
            dgcolhSportsHobbies.DataPropertyName = "sports_hobbies"

            'Gajanan [17-April-2019] -- End




            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            dgcolhVoidReason.DataPropertyName = "voidreason"
            'Gajanan [17-April-2019] -- End

            'Sohail (03 Jul 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            dgcolhInactiveReason.DataPropertyName = "reason"
            'Sohail (03 Jul 2019) -- End

            'mdvData = dt.DefaultView
            'dgvData.DataSource = mdvData


            If eScreenType = enScreenName.frmQualificationsList Then
                For Each xdgvr As DataGridViewRow In dgvData.Rows
                    If (CStr(xdgvr.Cells(objcolhqualificationgroupunkid.Index).Value).Trim.Length > 0 AndAlso CInt(xdgvr.Cells(objcolhqualificationunkid.Index).Value) <= 0) And _
                        (CStr(xdgvr.Cells(objcolhqualificationunkid.Index).Value).Trim.Length > 0 AndAlso CInt(xdgvr.Cells(objcolhqualificationunkid.Index).Value) <= 0) Then
                        xdgvr.DefaultCellStyle.ForeColor = Color.Blue
                        lblotherqualificationnote.Visible = True
                    End If
                Next
            Else
                lblotherqualificationnote.Visible = False
            End If

            Select Case eScreenType
                Case enScreenName.frmQualificationsList
                    dgcolhQualifyGroup.Visible = True
                    dgcolhQualification.Visible = True
                    dgcolhAwardDate.Visible = True
                    dgcolhAwardDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat
                    dgcolhAwardToDate.Visible = True
                    dgcolhAwardToDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat
                    dgcolhInstitute.Visible = True
                    dgcolhRefNo.Visible = True
                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    objdgcolhDownloadDocument.Visible = True
                    'objcolhformname.Visible = True
                    'objcolhnewattachdocumentid.Visible = True
                    'objcolhdeleteattachdocumentid.Visible = True
                    'Gajanan [17-DEC-2018] -- End

                    'S.SANDEEP |26-APR-2019| -- START
                    objcolhnewattachdocumentid.DataPropertyName = "newattachdocumentid"
                    objcolhdeleteattachdocumentid.DataPropertyName = "deleteattachdocumentid"
                    'S.SANDEEP |26-APR-2019| -- END

                Case enScreenName.frmJobHistory_ExperienceList
                    dgcolhCompany.Visible = True
                    dgcolhJob.Visible = True
                    dgcolhStartDate.Visible = True
                    dgcolhStartDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat
                    dgcolhEndDate.Visible = True
                    dgcolhEndDate.DefaultCellStyle.Format = ConfigParameter._Object._CompanyDateFormat
                    dgcolhSupervisor.Visible = True
                    dgcolhRemark.Visible = True


                    'Gajanan [5-Dec-2019] -- Start   
                    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                    objdgcolhDownloadDocument.Visible = True
                    objcolhnewattachdocumentid.DataPropertyName = "newattachdocumentid"
                    objcolhdeleteattachdocumentid.DataPropertyName = "deleteattachdocumentid"
                    'Gajanan [5-Dec-2019] -- End


                Case enScreenName.frmEmployeeRefereeList
                    dgcolhRefreeName.Visible = True
                    dgcolhCountry.Visible = True
                    dgcolhIdNo.Visible = True
                    dgcolhEmail.Visible = True
                    dgcolhTelNo.Visible = True
                    dgcolhMobile.Visible = True

                Case enScreenName.frmEmployee_Skill_List
                    dgcolhSkillCategory.Visible = True
                    dgcolhSkill.Visible = True
                    dgcolhDescription.Visible = True


                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                Case enScreenName.frmDependantsAndBeneficiariesList
                    dgcolhDBName.Visible = True
                    dgcolhDBRelation.Visible = True
                    dgcolhDBbirthdate.Visible = True
                    dgcolhDBIdentifyNo.Visible = True
                    dgcolhDBGender.Visible = True
                    objdgcolhDownloadDocument.Visible = True
                    'Gajanan [22-Feb-2019] -- End

                    'S.SANDEEP |26-APR-2019| -- START
                    objcolhnewattachdocumentid.DataPropertyName = "newattachdocumentid"
                    objcolhdeleteattachdocumentid.DataPropertyName = "deleteattachdocumentid"
                    'S.SANDEEP |26-APR-2019| -- END

                    'Sohail (03 Jul 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    dgcolhInactiveReason.Visible = True
                    'Sohail (03 Jul 2019) -- End

                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Case enScreenName.frmIdentityInfoList
                    dgcolhIdType.Visible = True
                    dgcolhSrNo.Visible = True
                    dgcolhIdentityNo.Visible = True
                    dgcolhIdCountry.Visible = True
                    dgcolhIdIssuePlace.Visible = True
                    dgcolhIdIssueDate.Visible = True
                    dgcolhIdExpiryDate.Visible = True
                    dgcolhIdDLClass.Visible = True
                    'Gajanan [22-Feb-2019] -- End

                    'S.SANDEEP |26-APR-2019| -- START
                    objdgcolhDownloadDocument.Visible = True
                    objcolhnewattachdocumentid.DataPropertyName = "newattachdocumentid"
                    objcolhdeleteattachdocumentid.DataPropertyName = "deleteattachdocumentid"
                    'S.SANDEEP |26-APR-2019| -- END

                    'Gajanan [18-Mar-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Case enScreenName.frmAddressList
                    dgcolhAddressType.Visible = True
                    dgcolhAddress1.Visible = True
                    dgcolhAddress2.Visible = True
                    dgcolhAddressCountry.Visible = True
                    dgcolhAddressState.Visible = True
                    dgcolhAddressCity.Visible = True
                    dgcolhAddressZipCode.Visible = True
                    dgcolhAddressProvince.Visible = True
                    dgcolhAddressRoad.Visible = True
                    dgcolhAddressEstate.Visible = True
                    dgcolhAddressProvince1.Visible = True
                    dgcolhAddressRoad1.Visible = True
                    dgcolhAddressChiefdom.Visible = True
                    dgcolhAddressVillage.Visible = True
                    dgcolhAddressTown.Visible = True
                    dgcolhAddressMobile.Visible = True
                    dgcolhAddressTel_no.Visible = True
                    dgcolhAddressPlotNo.Visible = True
                    dgcolhAddressAltNo.Visible = True
                    dgcolhAddressEmail.Visible = True
                    dgcolhAddressFax.Visible = True

                    'S.SANDEEP |26-APR-2019| -- START
                    objdgcolhDownloadDocument.Visible = True
                    objcolhnewattachdocumentid.DataPropertyName = "newattachdocumentid"
                    objcolhdeleteattachdocumentid.DataPropertyName = "deleteattachdocumentid"
                    objcolhAddressTypeId.DataPropertyName = "addresstypeid"
                    'S.SANDEEP |26-APR-2019| -- END


                Case enScreenName.frmEmergencyAddressList
                    dgcolhAddressType.Visible = True
                    dgcolhFirstname.Visible = True
                    dgcolhLastname.Visible = True
                    dgcolhAddressCountry.Visible = True
                    dgcolhAddressState.Visible = True
                    dgcolhAddressCity.Visible = True
                    dgcolhAddressZipCode.Visible = True
                    dgcolhAddressProvince.Visible = True
                    dgcolhAddressRoad.Visible = True
                    dgcolhAddressEstate.Visible = True
                    dgcolhAddressPlotNo.Visible = True
                    dgcolhAddressMobile.Visible = True
                    dgcolhAddressAltNo.Visible = True
                    dgcolhAddressTel_no.Visible = True
                    dgcolhAddressFax.Visible = True
                    dgcolhAddressEmail.Visible = True

                    'Gajanan [18-Mar-2019] -- End

                    'S.SANDEEP |26-APR-2019| -- START
                    objdgcolhDownloadDocument.Visible = True
                    objcolhnewattachdocumentid.DataPropertyName = "newattachdocumentid"
                    objcolhdeleteattachdocumentid.DataPropertyName = "deleteattachdocumentid"
                    objcolhAddressTypeId.DataPropertyName = "addresstypeid"
                    'S.SANDEEP |26-APR-2019| -- END

                    'S.SANDEEP |15-APR-2019| -- START
                Case enScreenName.frmMembershipInfoList
                    dgcolhCategory.Visible = True
                    dgcolhmembershipname.Visible = True
                    dgcolhmembershipno.Visible = True
                    dgcolhperiod_name.Visible = True
                    dgcolhissue_date.Visible = True
                    dgcolhstart_date.Visible = True
                    dgcolhexpiry_date.Visible = True
                    dgcolhMemRemark.Visible = True
                    'S.SANDEEP |15-APR-2019| -- END


        'Gajanan [17-April-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.





                Case enScreenName.frmBirthinfo
                    dgcolhBirthCountry.Visible = True
                    dgcolhBirthState.Visible = True
                    dgcolhBirthCity.Visible = True
                    dgcolhBirthCertificateNo.Visible = True
                    dgcolhBirthWard.Visible = True
                    dgcolhBirthVillage.Visible = True
                    dgcolhBirthTown1.Visible = True
                    dgcolhBirthVillage1.Visible = True
                    dgcolhBirthChiefdom.Visible = True

                    'S.SANDEEP |26-APR-2019| -- START
                    objdgcolhDownloadDocument.Visible = True
                    objcolhnewattachdocumentid.DataPropertyName = "newattachdocumentid"
                    objcolhdeleteattachdocumentid.DataPropertyName = "deleteattachdocumentid"
                    objcolhAddressTypeId.DataPropertyName = "isbirthinfo"
                    'S.SANDEEP |26-APR-2019| -- END

                Case enScreenName.frmOtherinfo
                    dgcolhComplexion.Visible = True
                    dgcolhBloodGroup.Visible = True
                    dgcolhEyeColor.Visible = True
                    dgcolhNationality.Visible = True
                    dgcolhEthinCity.Visible = True
                    dgcolhReligion.Visible = True
                    dgcolhHair.Visible = True
                    dgcolhMaritalStatus.Visible = True
                    dgcolhExtraTel.Visible = True
                    dgcolhLanguage1.Visible = True
                    dgcolhLanguage2.Visible = True
                    dgcolhLanguage3.Visible = True
                    dgcolhLanguage4.Visible = True
                    dgcolhHeight.Visible = True
                    dgcolhWeight.Visible = True
                    dgcolhMaritalDate.Visible = True
                    dgcolhAllergies.Visible = True
                    dgcolhDisabilities.Visible = True
                    dgcolhSportsHobbies.Visible = True

                    'S.SANDEEP |26-APR-2019| -- START
                    objdgcolhDownloadDocument.Visible = True
                    objcolhnewattachdocumentid.DataPropertyName = "newattachdocumentid"
                    objcolhdeleteattachdocumentid.DataPropertyName = "deleteattachdocumentid"
                    objcolhAddressTypeId.DataPropertyName = "isbirthinfo"
                    'S.SANDEEP |26-APR-2019| -- END

        'Gajanan [17-April-2019] -- End


            End Select



            If CInt(cboOperationType.SelectedValue) = clsEmployeeDataApproval.enOperationType.DELETED Then
                dgcolhVoidReason.Visible = True
            End If


        'Gajanan [17-April-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.




            mdvData = dt.DefaultView
            dgvData.DataSource = mdvData


            If eScreenType = enScreenName.frmQualificationsList Then
                For Each xdgvr As DataGridViewRow In dgvData.Rows
                    If (CStr(xdgvr.Cells(objcolhqualificationgroupunkid.Index).Value).Trim.Length > 0 AndAlso CInt(xdgvr.Cells(objcolhqualificationunkid.Index).Value) <= 0) And _
                        (CStr(xdgvr.Cells(objcolhqualificationunkid.Index).Value).Trim.Length > 0 AndAlso CInt(xdgvr.Cells(objcolhqualificationunkid.Index).Value) <= 0) Then
                        xdgvr.DefaultCellStyle.ForeColor = Color.Blue
                        lblotherqualificationnote.Visible = True
                    End If
                Next
            Else
                lblotherqualificationnote.Visible = False
            End If
        'Gajanan [17-April-2019] -- End


            'Gajanan [27-May-2019] -- Start              
            Dim xColName As DataGridViewColumn = Nothing

            If CInt(eScreenType) = CInt(enScreenName.frmQualificationsList) Then
                xColName = dgcolhQualifyGroup
            ElseIf CInt(eScreenType) = CInt(enScreenName.frmEmployee_Skill_List) Then

                xColName = dgcolhSkillCategory
            ElseIf CInt(eScreenType) = CInt(enScreenName.frmEmployeeRefereeList) Then
                xColName = dgcolhRefreeName
            ElseIf CInt(eScreenType) = CInt(enScreenName.frmJobHistory_ExperienceList) Then
                xColName = dgcolhCompany
            ElseIf CInt(eScreenType) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
                xColName = dgcolhDBName
            ElseIf CInt(eScreenType) = CInt(enScreenName.frmIdentityInfoList) Then
                xColName = dgcolhIdType
            ElseIf CInt(eScreenType) = CInt(enScreenName.frmAddressList) Then
                xColName = dgcolhAddressType
            ElseIf CInt(eScreenType) = CInt(enScreenName.frmEmergencyAddressList) Then
                xColName = dgcolhAddressType
            ElseIf CInt(eScreenType) = CInt(enScreenName.frmMembershipInfoList) Then
                xColName = dgcolhCategory

            ElseIf CInt(eScreenType) = CInt(enScreenName.frmOtherinfo) Then
                xColName = dgcolhComplexion

            ElseIf CInt(eScreenType) = CInt(enScreenName.frmBirthinfo) Then
                xColName = dgcolhBirthCountry
            End If


            If xColName IsNot Nothing Then
                Dim dtmp() As DataRow = dt.Select("isgrp = 1", xColName.DataPropertyName)
                If dtmp.Length > 0 Then
                    Dim strGrpName As String = String.Empty
                    For i As Integer = 0 To dtmp.Length - 1
                        If strGrpName <> dtmp(i)(xColName.DataPropertyName).ToString() Then
                            strGrpName = dtmp(i)(xColName.DataPropertyName).ToString()
                        Else
                            dt.Rows.Remove(dtmp(i))
                        End If
                    Next
                End If

            End If
            'Gajanan [27-May-2019] -- End





            SetGridStyle()
            dgvData.PerformLayout()
            dgvData.Refresh()
            'S.SANDEEP |15-APR-2019| -- START
            If dgvData.RowCount <= 0 Then
                If tblPanel.RowStyles.Item(0).SizeType = SizeType.Absolute Then
                    tblPanel.RowStyles.Item(0).Height = 2
                End If
            End If
            'S.SANDEEP |15-APR-2019| -- END
    		'Gajanan [9-NOV-2019] -- Start   
    		'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
            objbtnSearch.ShowResult(dt.Select("isgrp = 0").Count.ToString())
            'Gajanan [9-NOV-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetApproverInfo()
        Dim objUMapping As New clsemp_appUsermapping
        Dim dsInfo As New DataSet
        Try
            dsInfo = objUMapping.GetList(enEmpApproverType.APPR_EMPLOYEE, "List", True, " AND hremp_appusermapping.isactive = 1 ", User._Object._Userunkid, Nothing)
            If dsInfo.Tables("List").Rows.Count > 0 Then
                txtApprover.Text = dsInfo.Tables("List").Rows(0)("User").ToString()
                txtApprover.Tag = CInt(dsInfo.Tables("List").Rows(0)("mappingunkid"))

                txtLevel.Text = dsInfo.Tables("List").Rows(0)("Level").ToString()
                txtLevel.Tag = CInt(dsInfo.Tables("List").Rows(0)("priority"))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetApproverInfo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function EscapeLikeValue(ByVal value As String) As String
        Dim sb As New StringBuilder(value.Length)
        For i As Integer = 0 To value.Length - 1
            Dim c As Char = value(i)
            Select Case c
                Case "]"c, "["c, "%"c, "*"c
                    sb.Append("[").Append(c).Append("]")
                    Exit Select
                Case "'"c
                    sb.Append("''")
                    Exit Select
                Case Else
                    sb.Append(c)
                    Exit Select
            End Select
        Next
        Return sb.ToString()
    End Function

    Private Function GetFilterString(ByVal strCriteria As String) As String
        Dim strFilter As String = String.Empty
        Try
            If dgvData.RowCount > 0 Then
                If CInt(cboEmployeeData.SelectedValue) > 0 Then
                    Dim vrow As DataRowView = CType(cboEmployeeData.SelectedItem, DataRowView)
                    Dim row As DataRow = vrow.Row
                    Dim eScrType As enScreenName : eScrType = CType(row.Item("Id"), enScreenName)
                    Select Case eScrType
                        Case enScreenName.frmQualificationsList
                            strFilter = dgcolhQualifyGroup.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhQualification.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAwardDate.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAwardToDate.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhInstitute.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhRefNo.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' "

                        Case enScreenName.frmJobHistory_ExperienceList
                            strFilter = dgcolhCompany.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhJob.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhStartDate.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhEndDate.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhSupervisor.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhRemark.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' "

                        Case enScreenName.frmEmployeeRefereeList
                            strFilter = dgcolhRefreeName.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhCountry.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhIdNo.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhEmail.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhTelNo.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhMobile.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' "

                        Case enScreenName.frmEmployee_Skill_List
                            strFilter = dgcolhSkillCategory.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhSkill.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhDescription.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' "


                            'Gajanan [18-Mar-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        Case enScreenName.frmAddressList
                            strFilter = dgcolhAddressType.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddress1.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddress2.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressCountry.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressState.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressCity.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressProvince.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressRoad.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressEstate.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressProvince1.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressRoad1.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressChiefdom.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressVillage.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressTown.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressMobile.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressTel_no.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressPlotNo.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressFax.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressEmail.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                        dgcolhAddressAltNo.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' "

                        Case enScreenName.frmEmergencyAddressList
                            strFilter = dgcolhAddressType.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhFirstname.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhLastname.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhAddressCountry.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhAddressState.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhAddressCity.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhAddressProvince.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhAddressRoad.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhAddressEstate.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhAddressPlotNo.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhAddressMobile.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhAddressAltNo.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhAddressTel_no.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhAddressFax.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                       dgcolhAddressEmail.DataPropertyName & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' "

                            'Gajanan [18-Mar-2019] -- End


                    End Select
                    If strFilter.Trim.Length > 0 Then strFilter &= " OR isgrp = 1 "
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFilterString", mstrModuleName)
        Finally
        End Try
        Return strFilter
    End Function

    Private Sub SetGridColums()
        Try
            For Each dgCol As DataGridViewColumn In dgvData.Columns
                If dgCol.Index > 0 Then dgCol.Visible = False
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColums", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetGridStyle()
        Try

            Dim iCellIndex As Integer = 0

            If CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'iCellIndex = dgcolhQualifyGroup.Index
                If CInt(cboOperationType.SelectedValue) = clsEmployeeDataApproval.enOperationType.DELETED Then
                    iCellIndex = dgcolhVoidReason.Index
                Else
                iCellIndex = dgcolhQualifyGroup.Index

                End If
                'Gajanan [17-April-2019] -- End


            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmEmployee_Skill_List) Then
                'Gajanan [27-May-2019] -- Start              
                'iCellIndex = dgcolhSkillCategory.Index
                'Gajanan [27-May-2019] -- End

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                If CInt(cboOperationType.SelectedValue) = clsEmployeeDataApproval.enOperationType.DELETED Then
                    iCellIndex = dgcolhVoidReason.Index
                Else

                    'Gajanan [27-May-2019] -- Start              
                    'iCellIndex = dgcolhQualifyGroup.Index
                    iCellIndex = dgcolhSkillCategory.Index
                    'Gajanan [27-May-2019] -- End


                End If
                'Gajanan [17-April-2019] -- End
            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmEmployeeRefereeList) Then

                'Gajanan [27-May-2019] -- Start              
                'iCellIndex = dgcolhRefreeName.Index
                'Gajanan [27-May-2019] -- End

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                If CInt(cboOperationType.SelectedValue) = clsEmployeeDataApproval.enOperationType.DELETED Then
                    iCellIndex = dgcolhVoidReason.Index
                Else

                    'Gajanan [27-May-2019] -- Start              
                    'iCellIndex = dgcolhQualifyGroup.Index
                    iCellIndex = dgcolhRefreeName.Index
                    'Gajanan [27-May-2019] -- End


                End If
                'Gajanan [17-April-2019] -- End

            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmJobHistory_ExperienceList) Then

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'iCellIndex = dgcolhCompany.Index

                If CInt(cboOperationType.SelectedValue) = clsEmployeeDataApproval.enOperationType.DELETED Then
                    iCellIndex = dgcolhVoidReason.Index
                Else
                iCellIndex = dgcolhCompany.Index
                End If
                'Gajanan [17-April-2019] -- End
                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'iCellIndex = dgcolhDBName.Index

                If CInt(cboOperationType.SelectedValue) = clsEmployeeDataApproval.enOperationType.DELETED Then
                    iCellIndex = dgcolhVoidReason.Index
                Else
                iCellIndex = dgcolhDBName.Index

                End If
                'Gajanan [17-April-2019] -- End

            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmIdentityInfoList) Then
                iCellIndex = dgcolhIdType.Index
                'Gajanan [22-Feb-2019] -- End

                'Gajanan [18-Mar-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmAddressList) Then
                iCellIndex = dgcolhAddressType.Index

            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmEmergencyAddressList) Then
                iCellIndex = dgcolhAddressType.Index
                'Gajanan [18-Mar-2019] -- End

                'S.SANDEEP |15-APR-2019| -- START
            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmMembershipInfoList) Then
                iCellIndex = dgcolhCategory.Index
                'S.SANDEEP |15-APR-2019| -- END


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmBirthinfo) Then
                iCellIndex = dgcolhBirthCountry.Index

            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmOtherinfo) Then
                iCellIndex = dgcolhComplexion.Index
                'Gajanan [17-April-2019] -- End

            End If

            'S.SANDEEP |26-APR-2019| -- START
            'Dim intEndIndex As Integer = dgvData.Columns.Cast(Of DataGridViewColumn)().Where(Function(x) x.Visible = True).Count()
            Dim intEndIndex As Integer = dgvData.Columns.Cast(Of DataGridViewColumn)().Last().Index
            'S.SANDEEP |26-APR-2019| -- END

            Dim dr = From dgdr As DataGridViewRow In dgvData.Rows.Cast(Of DataGridViewRow)() Where CBool(dgdr.Cells(objdgcolhIsGrp.Index).Value) = True Select dgdr
            Dim pCell As New clsMergeCell
        'Gajanan [17-April-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.




            'dr.ToList.ForEach(Function(x) SetRowStyle(x, iCellIndex, pCell, iCellIndex + intEndIndex))
              dr.ToList.ForEach(Function(x) SetRowStyle(x, iCellIndex, pCell, intEndIndex))
        'Gajanan [17-April-2019] -- End

            'Dim pCell As New clsMergeCell
            'For Each dgvRow As DataGridViewRow In dgvData.Rows
            '    If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = True Then
            '        'pCell.MakeMerge(dgvData, dgvRow.Index, 1, dgvRow.Cells.Count - 1, Color.Gray, Color.White, dgvRow.Cells(dgcolhSkillCategory.Index).Value.ToString, "", picStayView.Image)
            '        pCell.MakeMerge(dgvData, dgvRow.Index, iCellIndex, dgvRow.Cells.Count - 1, Color.Gray, Color.White, dgvRow.Cells(iCellIndex).Value.ToString, "", picStayView.Image)
            '    End If
            'Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridStyle", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetRowStyle(ByVal xRow As DataGridViewRow, ByVal iCellIndex As Integer, ByVal pCell As clsMergeCell, ByVal intEndIndex As Integer) As Boolean
    	'Gajanan [9-NOV-2019] -- Start   
    	'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
        'pCell.MakeMerge(dgvData, xRow.Index, iCellIndex, intEndIndex, Color.Gray, Color.White, xRow.Cells(iCellIndex).Value.ToString, "", picStayView.Image)
        Dim dgvcsHeader As New DataGridViewCellStyle
        dgvcsHeader.ForeColor = Color.White
        dgvcsHeader.SelectionBackColor = Color.Gray
        dgvcsHeader.SelectionForeColor = Color.White
        dgvcsHeader.BackColor = Color.Gray
        xRow.DefaultCellStyle = dgvcsHeader
        dgvData.Columns(iCellIndex).Width = 200
        'Gajanan [9-NOV-2019] -- End
    End Function
    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.

    Private Sub AddFileToZip(ByVal zipFilename As String, ByVal fileToAdd As List(Of String), Optional ByVal compression As CompressionOption = CompressionOption.Normal)
        Try
            Using zip As Package = System.IO.Packaging.Package.Open(zipFilename, FileMode.OpenOrCreate)
                For Each item As String In fileToAdd
                    Try
                    Dim destFilename As String = ".\" & Path.GetFileName(item)
                    Dim uri As Uri = PackUriHelper.CreatePartUri(New Uri(destFilename, UriKind.Relative))
                    If zip.PartExists(uri) Then
                        zip.DeletePart(uri)
                    End If
                    Dim part As PackagePart = zip.CreatePart(uri, "", compression)
                    Using fileStream As New FileStream(item, FileMode.Open, FileAccess.Read)
                        Using dest As Stream = part.GetStream()
                            CopyStream(CType(fileStream, Stream), part.GetStream())
                        End Using
                    End Using
                    Catch ex As System.ArgumentException
                        Continue For
                    Catch ex As System.UriFormatException
                        Continue For
                    End Try
                Next
            End Using


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddFileToZip", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Const bufSize As Integer = (5859 * 1024)
        Dim buf(bufSize - 1) As Byte
        Dim bytesRead As Integer = 0

        bytesRead = source.Read(buf, 0, bufSize)
        Do While bytesRead > 0
            target.Write(buf, 0, bytesRead)
            bytesRead = source.Read(buf, 0, bufSize)
        Loop
    End Sub
    'Gajanan [17-DEC-2018] -- End

      'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private Function SetColumnData(ByVal dr As DataRow) As Boolean
        dr("loginemployeeunkid") = 0
        dr("isvoid") = False
        dr("voidreason") = ""
        dr("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
        dr("audittype") = enAuditType.ADD
        dr("audituserunkid") = User._Object._Userunkid
        dr("ip") = getIP()
        dr("host") = getHostName()
        dr("form_name") = mstrModuleName
        dr("isweb") = False
        dr("mappingunkid") = CInt(txtApprover.Tag)
    End Function
    'Gajanan [22-Feb-2019] -- End

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Private Function DownloadDocumnet(ByVal dt As DataTable) As Boolean
    Private Function DownloadDocument(ByVal dt As DataTable, ByVal mstrFoldername As String) As Boolean
        'Gajanan [17-April-2019] -- End

        Try
            Dim xPath As String = ""
            Dim strError As String = ""
            Dim strLocalPath As String = String.Empty
            Dim fileToAdd As New List(Of String)




            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Dim mstrFoldername As String
            'If CInt(cboEmployeeData.SelectedValue) = enScreenName.frmQualificationsList Then
            '    mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString
            'ElseIf CInt(cboEmployeeData.SelectedValue) = enScreenName.frmDependantsAndBeneficiariesList Then
            '    mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DEPENDANTS).Tables(0).Rows(0)("Name").ToString
            'End If

            'Gajanan [17-April-2019] -- End


            'S.SANDEEP |31-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Employee Bio Data UAT]
            'For Each xRow As DataRow In dt.Rows
            '    If IsDBNull(xRow("filepath")) = False Then

            '        If IsSelfServiceExist() Then
            '            Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xRow("filepath").ToString, xRow("fileuniquename").ToString, mstrFoldername, strError, ConfigParameter._Object._ArutiSelfServiceURL)
            '            If imagebyte IsNot Nothing Then
            '                strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
            '                Dim ms As New MemoryStream(imagebyte)
            '                Dim fs As New FileStream(strLocalPath, FileMode.Create)
            '                ms.WriteTo(fs)
            '                ms.Close()
            '                fs.Close()
            '                fs.Dispose()
            '            End If
            '            fileToAdd.Add(strLocalPath)
            '        Else
            '            If CInt(cboEmployeeData.SelectedValue) = enScreenName.frmQualificationsList Then
            '                strLocalPath = ConfigParameter._Object._Document_Path & "\" & (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString & "\" & xRow("fileuniquename").ToString
            '            ElseIf CInt(cboEmployeeData.SelectedValue) = enScreenName.frmDependantsAndBeneficiariesList Then
            '                strLocalPath = ConfigParameter._Object._Document_Path & "\" & (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DEPENDANTS).Tables(0).Rows(0)("Name").ToString & "\" & xRow("fileuniquename").ToString
            '            End If

            '            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFoldername) = False Then
            '                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFoldername)
            '            End If
            '            fileToAdd.Add(strLocalPath)
            '        End If
            '    End If
            'Next

            For Each xRow As DataRow In dt.Rows
                If IsDBNull(xRow("file_data")) = False Then
                    strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                    Dim ms As New MemoryStream(CType(xRow("file_data"), Byte()))
                    Dim fs As New FileStream(strLocalPath, FileMode.Create)
                    ms.WriteTo(fs)
                    ms.Close()
                    fs.Close()
                    fs.Dispose()
                    If strLocalPath <> "" Then
                        fileToAdd.Add(strLocalPath)
                    End If
                ElseIf IsDBNull(xRow("file_data")) = True Then
                    If IsSelfServiceExist() Then
                        Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xRow("filepath").ToString, xRow("fileuniquename").ToString, mstrFoldername, strError, ConfigParameter._Object._ArutiSelfServiceURL)
                        If imagebyte IsNot Nothing Then
                            strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                            Dim ms As New MemoryStream(imagebyte)
                            Dim fs As New FileStream(strLocalPath, FileMode.Create)
                            ms.WriteTo(fs)
                            ms.Close()
                            fs.Close()
                            fs.Dispose()
                        End If
                        fileToAdd.Add(strLocalPath)
                    Else
                        strLocalPath = xRow("filepath").ToString()
                        If IO.File.Exists(strLocalPath) = True Then
                        fileToAdd.Add(strLocalPath)
                    End If
                End If
                End If
            Next
            'S.SANDEEP |31-MAY-2019| -- END

            

            If fileToAdd.Count > 0 Then
                Dim sfd As New SaveFileDialog() : sfd.Filter = "Zip File (.zip)|*.zip"
                If sfd.ShowDialog() = DialogResult.OK Then
                    Dim fl As New System.IO.FileInfo(sfd.FileName)
                    AddFileToZip(fl.FullName, fileToAdd, CompressionOption.Normal)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "File(s) downloaded successfully to the selected location."), enMsgBoxStyle.Information)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DownloadDocument", mstrModuleName)
        Finally
        End Try

    End Function
    'Gajanan [22-Feb-2019] -- End

#End Region

#Region " Form's Events "

    Private Sub frmAppRejEmpData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objApprovalData = New clsEmployeeDataApproval
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call SetGridColums()
            Call OtherSettings()
            Call SetApproverInfo()
            Call FillCombo()
            lblotherqualificationnote.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAppRejEmpData_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAppRejEmpData_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objApprovalData = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeDataApproval.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeDataApproval"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If cboEmployee.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            mstrAdvanceFilter = ""

            objbtnReset.ShowResult(dgvData.RowCount.ToString)
            lblotherqualificationnote.Visible = False
            If Not TypeOf (sender) Is ComboBox Then
            cboEmployeeData.SelectedIndex = 0
            End If
            txtSearch.Text = ""
            cboEmployee.SelectedIndex = 0

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If Not TypeOf (sender) Is ComboBox Then
            cboOperationType.SelectedIndex = 0
            End If
            'Gajanan [22-Feb-2019] -- End

            dgvData.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboEmployeeData.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Data is mandatory information. Please select employee data to continue"), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Gajanan [17-DEC-2018] -- Start
            If CInt(cboOperationType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Opration Type is mandatory information. Please select opration type to continue"), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Gajanan [17-DEC-2018] -- End
            Call SetGridColums()
            Call FillGrid()
    		'Gajanan [9-NOV-2019] -- Start   
    		'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
            'objbtnSearch.ShowResult(dgvData.RowCount.ToString)
            'Gajanan [9-NOV-2019] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnShowMyReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowMyReport.Click
        Try

            Dim frm As New frmViewEmployeeDataApproval
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim mstrFilterstring As String = String.Empty

            If CInt(cboEmployee.SelectedValue) > 0 Then
                mstrFilterstring = " EM.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'"
            End If

            If mstrAdvanceFilter.Length > 0 Then
                mstrFilterstring &= mstrAdvanceFilter
            End If

            Dim mintPriviledge As Integer = 0
            Dim menScreenName As enScreenName

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Select Case eScreenType
            '    Case enScreenName.frmQualificationsList
            '        mintPriviledge = enUserPriviledge.AllowToApproveRejectEmployeeQualifications
            '        menScreenName = enScreenName.frmQualificationsList
            '    Case enScreenName.frmJobHistory_ExperienceList
            '        mintPriviledge = enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences
            '        menScreenName = enScreenName.frmJobHistory_ExperienceList

            '    Case enScreenName.frmEmployee_Skill_List
            '        mintPriviledge = enUserPriviledge.AllowToApproveRejectEmployeeReferences
            '        menScreenName = enScreenName.frmEmployee_Skill_List

            '    Case enScreenName.frmEmployeeRefereeList
            '        mintPriviledge = enUserPriviledge.AllowToApproveRejectEmployeeReferences
            '        menScreenName = enScreenName.frmEmployeeRefereeList
            'End Select
            Dim itmp() As DataRow = CType(cboEmployeeData.DataSource, DataTable).Select("Id = '" & CInt(cboEmployeeData.SelectedValue) & "'")
            If itmp.Length > 0 Then
                mintPriviledge = CInt(itmp(0).Item("PrivilegeId"))
            End If
            menScreenName = CType(CInt(cboEmployeeData.SelectedValue), enScreenName)
            'Gajanan [22-Feb-2019] -- End

        'Gajanan [17-April-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'frm.displayDialog(User._Object._Userunkid, 0, mintPriviledge, menScreenName, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), mstrFilterstring, False)
            frm.displayDialog(User._Object._Userunkid, CInt(txtLevel.Tag), mintPriviledge, menScreenName, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), mstrFilterstring, True)
            'Gajanan [17-April-2019] -- End

            If frm IsNot Nothing Then frm.Dispose()
            Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprovalinfo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnReject.Click
        Try
            If mdvData IsNot Nothing Then
                Dim dtTable As DataTable = mdvData.ToTable()
                'S.SANDEEP |15-APR-2019| -- START
                'If dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)(objdgcolhicheck.DataPropertyName) = True).Count() <= 0 Then
                If dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)(objdgcolhicheck.DataPropertyName) = True And CBool(x.Field(Of Integer)("isgrp")) = False).Count() <= 0 Then
                    'S.SANDEEP |15-APR-2019| -- END
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please check atleast one employee in order to perform further operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                'S.SANDEEP |15-APR-2019| -- START
                If CType(sender, eZee.Common.eZeeLightButton).Name = btnApprove.Name Then
                    If eScreenType = enScreenName.frmMembershipInfoList Then
                        Dim row = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)(objdgcolhicheck.DataPropertyName) = True And CBool(x.Field(Of Integer)("isgrp")) = False)
                        If row IsNot Nothing AndAlso row.Count > 0 Then
                            Dim blnIsProcessed As Boolean = False
                            For Each r In row

                                'Gajanan [17-April-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                If CInt(r("periodunkid")) <= 0 Then Continue For
                                'Gajanan [17-April-2019] -- End

                                Dim objPdata As New clscommom_period_Tran
                                objPdata._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(r("periodunkid"))
                                Dim objProcessed As New clsTnALeaveTran
                                If objProcessed.IsPayrollProcessDone(CInt(r("periodunkid")), CStr(r("employeeunkid")), objPdata._End_Date.Date) = True Then
                                    blnIsProcessed = True
                                    Dim intIndex As Integer = dtTable.Rows.IndexOf(r)
                                    dtTable.Rows(dtTable.Rows.IndexOf(r))(objdgcolhicheck.DataPropertyName) = False
                                    dtTable.AcceptChanges()
                                    If intIndex <> -1 Then
                                        dgvData.Rows(intIndex).DefaultCellStyle.ForeColor = Color.Red
                                        dgvData.Rows(intIndex).Cells(objdgcolhicheck.Index).Value = False
                                    End If
                                End If
                                objProcessed = Nothing
                            Next
                            If blnIsProcessed Then
                                If tblPanel.RowStyles.Item(0).SizeType = SizeType.Absolute Then
                                    tblPanel.RowStyles.Item(0).Height = 34
                                End If
                                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 84, "Sorry, You cannot assign this membership. Reason : Payroll is already processed for the current period."), enMsgBoxStyle.Information)
                            End If
                        End If
                    End If
                End If
                'S.SANDEEP |15-APR-2019| -- END

                Select Case CType(sender, eZee.Common.eZeeLightButton).Name
                    Case btnApprove.Name
                        If txtRemark.Text.Trim.Length <= 0 Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to approve employee(s) without remark?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If
                        End If
                    Case btnReject.Name
                        If txtRemark.Text.Trim.Length <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Rejection remark is mandatory information. Please enter enter remark to continue."), enMsgBoxStyle.Information)
                            txtRemark.Focus()
                            Exit Sub
                        End If
                End Select

                Dim mstrFilterstring As String = String.Empty

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    mstrFilterstring = " EM.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'"
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    mstrFilterstring &= mstrAdvanceFilter
                End If


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'dtAppr = objApprovalData.GetNextEmployeeApprovers(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, intPrivilegeId, eScreenType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), Nothing, CInt(txtLevel.Tag), False, mstrFilterstring, False)
                Dim dtAppr As DataTable : Dim drtemp() As DataRow = Nothing : Dim strCheckedEmpIds As String = "" : Dim strFinalCheckedEmpIds As String = ""

                Dim mblnIsRejection As Boolean = False
                Dim iStatusunkid As clsEmployee_Master.EmpApprovalStatus
                Select Case CType(sender, eZee.Common.eZeeLightButton).Name
                    Case btnApprove.Name
                        iStatusunkid = clsEmployee_Master.EmpApprovalStatus.Approved
                    Case btnReject.Name
                        iStatusunkid = clsEmployee_Master.EmpApprovalStatus.Rejected
                        mblnIsRejection = True
                End Select


                If mblnIsRejection Then
                    dtAppr = objApprovalData.GetNextEmployeeApprovers(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, intPrivilegeId, eScreenType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), Nothing, 0, False, mstrFilterstring, False)
                Else
                dtAppr = objApprovalData.GetNextEmployeeApprovers(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, intPrivilegeId, eScreenType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), Nothing, CInt(txtLevel.Tag), False, mstrFilterstring, False)
                End If
                'Gajanan [17-April-2019] -- End



                If dtAppr.Rows.Count > 0 Then
                    strCheckedEmpIds = String.Join(",", dtAppr.AsEnumerable().Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())
                Else
                    strCheckedEmpIds = ""
                End If

                If strCheckedEmpIds.Trim.Length > 0 Then
                    drtemp = dtTable.Select("employeeunkid not in (" & strCheckedEmpIds & ") AND icheck = true ")
                Else
                    drtemp = dtTable.Select("icheck = true ")
                End If

                If drtemp.Length > 0 Then
                    For index As Integer = 0 To drtemp.Length - 1
                        drtemp(index)("isfinal") = True
                    Next
                End If

                dtTable.AcceptChanges()
                dtTable = New DataView(dtTable, "icheck = true and isgrp = 0", "", DataViewRowState.CurrentRows).ToTable


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                drtemp = Nothing

                Dim strCheckedData As String = String.Join("','", dtTable.AsEnumerable().Select(Function(y) y.Field(Of String)("tranguid").ToString).Distinct().ToArray())


                If strCheckedData.Trim.Length > 0 Then
                    drtemp = dtAppr.Select("tranguid in ('" & strCheckedData & "')")
                End If


                If drtemp.Length > 0 Then
                    For index As Integer = 0 To drtemp.Length - 1
                        drtemp(index)("icheck") = True
                    Next
                End If

                dtAppr.AcceptChanges()
                'Gajanan [17-April-2019] -- End


                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmIdentityInfoList) Then
                    drtemp = dtTable.Select("")
                    drtemp.ToList().ForEach(Function(x) SetColumnData(x))
                    dtTable.AcceptChanges()
                End If
                'Gajanan [22-Feb-2019] -- End


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                'Dim mblnIsRejection As Boolean = False
                'Dim iStatusunkid As clsEmployee_Master.EmpApprovalStatus
                'Select Case CType(sender, eZee.Common.eZeeLightButton).Name
                '    Case btnApprove.Name
                '        iStatusunkid = clsEmployee_Master.EmpApprovalStatus.Approved
                '    Case btnReject.Name
                '        iStatusunkid = clsEmployee_Master.EmpApprovalStatus.Rejected
                '        mblnIsRejection = True
                'End Select


                strCheckedEmpIds = String.Join(",", dtTable.AsEnumerable().Where(Function(s) s.Field(Of Integer)("isfinal") = 0).Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())



                strFinalCheckedEmpIds = String.Join(",", dtTable.AsEnumerable().Where(Function(s) s.Field(Of Integer)("isfinal") = 1).Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())
                'Gajanan [17-April-2019] -- End

                If objApprovalData.InsertAll(eScreenType, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), dtTable, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrModuleName, False, iStatusunkid, txtRemark.Text, CInt(txtApprover.Tag), Company._Object._Companyunkid, _
                                             FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), _
                                             Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, _
                                             User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, _
                                             ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                             User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CreateADUserFromEmpMst, ConfigParameter._Object._UserMustchangePwdOnNextLogon, , _
                                             getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP) Then

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.



                    'If mblnIsRejection = False Then
                    '    objApprovalData.SendNotification(2, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eScreenType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), CInt(txtLevel.Tag), strCheckedEmpIds, txtRemark.Text)
                    'Else
                    '    objApprovalData.SendNotification(3, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eScreenType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), CInt(txtLevel.Tag), strCheckedEmpIds, txtRemark.Text, ConfigParameter._Object._EmployeeDataRejectedUserIds)
                    'End If

                    If strCheckedEmpIds.Length > 0 Then
                    If mblnIsRejection = False Then
                            objApprovalData.SendNotification(2, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eScreenType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), CInt(txtLevel.Tag), strCheckedEmpIds, txtRemark.Text, Nothing, "", dtAppr, True)
                    Else
                            objApprovalData.SendNotification(3, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eScreenType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), CInt(txtLevel.Tag), strCheckedEmpIds, txtRemark.Text, ConfigParameter._Object._EmployeeDataRejectedUserIds, "", dtAppr, True)
                    End If
                    End If

                    If strFinalCheckedEmpIds.Length > 0 Then
                        If mblnIsRejection Then
                            objApprovalData.SendNotification(3, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eScreenType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), CInt(txtLevel.Tag), strFinalCheckedEmpIds, txtRemark.Text, Nothing, "", dtTable, True)
                    Else
                            objApprovalData.SendNotification(4, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eScreenType, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, CType(CInt(cboOperationType.SelectedValue), clsEmployeeDataApproval.enOperationType), CInt(txtLevel.Tag), strFinalCheckedEmpIds, txtRemark.Text, Nothing, "", dtTable, True)
                        End If
                    End If
                    'Gajanan [17-April-2019] -- End

                Else
                    If objApprovalData._Message <> "" Then
                        eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

                'Gajanan [3-April-2019] -- Start
                txtRemark.Text = ""
                'Gajanan [3-April-2019] -- End

                Call FillGrid()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If cboEmployee.DataSource Is Nothing Then Exit Sub

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployeeData_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployeeData.SelectedIndexChanged
        Try
            If CInt(cboEmployeeData.SelectedValue) > 0 Then
                Dim vrow As DataRowView = CType(cboEmployeeData.SelectedItem, DataRowView)
                Dim row As DataRow = vrow.Row
                intPrivilegeId = CInt(row.Item("PrivilegeId"))
                eScreenType = CType(row.Item("Id"), enScreenName)
                btnShowMyReport.Enabled = True
            Else
                intPrivilegeId = 0
                btnShowMyReport.Enabled = False
            End If
            'S.SANDEEP |15-APR-2019| -- START
            If tblPanel.RowStyles.Item(0).SizeType = SizeType.Absolute Then
                tblPanel.RowStyles.Item(0).Height = 2
            End If
            'S.SANDEEP |15-APR-2019| -- END
            'Call SetGridColums()
            'Call FillGrid()
            Call objbtnReset_Click(cboEmployeeData, New EventArgs)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMovement_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim str As String = "" : Dim strother As String = ""
        Try
            If mdvData Is Nothing Then Exit Sub
            If (txtSearch.Text.Length > 0) Then
                str = GetFilterString(txtSearch.Text)
                'str = "(" & str & ") "
                'strother = str
                'Dim dr() As DataRow = mdvData.Table.Select(str & " AND isgrp = 0 ")
                'If dr.Length > 0 Then
                '    str = objdgcolhEmpid.DataPropertyName & " IN (" & String.Join(",", dr.AsEnumerable().Select(Function(x) x.Field(Of Integer)(objdgcolhEmpid.DataPropertyName).ToString()).ToArray()) & ") "
                'End If
            End If
            mdvData.RowFilter = str '& " OR " & strother
            objbtnSearch.ShowResult(dgvData.RowCount.ToString())
            SetGridStyle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "AEM"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP |15-APR-2019| -- START
    Private Sub lnkSet_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSet.LinkClicked
        Try
            If mdvData IsNot Nothing Then
                If CInt(cboEffectivePeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Effective period is mandatory information. Please select effective period to continue."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                If radApplytoAll.Checked = False AndAlso radApplytoChecked.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Please tick atleast one operation type whether you want to apply this period to all or checked transaction."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim iRow As IEnumerable(Of DataRow) = Nothing
                If radApplytoChecked.Checked Then
                    iRow = mdvData.Table.AsEnumerable().Where(Function(x) x.Field(Of Boolean)(objdgcolhicheck.DataPropertyName) = True And CBool(x.Field(Of Integer)("isgrp")) = False)
                ElseIf radApplytoAll.Checked Then
                    iRow = mdvData.Table.AsEnumerable().Where(Function(x) CBool(x.Field(Of Integer)("isgrp")) = False)
                End If
                If iRow IsNot Nothing AndAlso iRow.Count > 0 Then
                    For Each ro In iRow
                        Dim intIndex As Integer = mdvData.Table.Rows.IndexOf(ro)
                        mdvData.Table.Rows(mdvData.Table.Rows.IndexOf(ro))(dgcolhperiod_name.DataPropertyName) = cboEffectivePeriod.Text
                        mdvData.Table.Rows(mdvData.Table.Rows.IndexOf(ro))("periodunkid") = cboEffectivePeriod.SelectedValue
                        mdvData.Table.Rows(mdvData.Table.Rows.IndexOf(ro))("effetiveperiodid") = cboEffectivePeriod.SelectedValue
                        mdvData.Table.AcceptChanges()
                    Next
                    dgvData.DataSource = mdvData.Table
                    SetGridStyle()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSet_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |15-APR-2019| -- END

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            If mdvData Is Nothing Then Exit Sub
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            For Each dr As DataRowView In mdvData
                dr.Item(objdgcolhicheck.DataPropertyName) = CBool(objchkAll.CheckState)
            Next
            dgvData.Refresh()
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Datagrid Event(s) "

    Private Sub dgvData_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellClick
        Try

            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhDownloadDocument.Index AndAlso dgvData.Rows(e.RowIndex).Cells(objdgcolhDownloadDocument.Index).Value Is imgView Then
                Dim Location As Point = dgvData.GetCellDisplayRectangle(objdgcolhDownloadDocument.Index, e.RowIndex, False).Location
                cmDownloadAttachment.Show(dgvData, New Point(Location.X + 10, Location.Y + 10))
                rowIndex = e.RowIndex
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellClick", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If e.ColumnIndex = objdgcolhicheck.Index Then
                If Me.dgvData.IsCurrentCellDirty Then
                    Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = Nothing
                If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    drRow = mdvData.Table.Select(objdgcolhEmpid.DataPropertyName & " = '" & dgvData.Rows(e.RowIndex).Cells(objdgcolhEmpid.Index).Value.ToString() & "'", "")
                    If drRow.Length > 0 Then
                        For index As Integer = 0 To drRow.Length - 1
                            drRow(index)("iCheck") = CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhicheck.Index).Value)
                        Next
                    End If
                End If
                    mdvData.Table.AcceptChanges()
                drRow = mdvData.ToTable.Select("iCheck = true", "")

                If drRow.Length > 0 Then
                    If mdvData.ToTable.Rows.Count = drRow.Length Then
                        objchkAll.CheckState = CheckState.Checked
                    Else
                        objchkAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAll.CheckState = CheckState.Unchecked
                End If
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.


            ElseIf e.ColumnIndex = objdgcolhDownloadDocument.Index Then

                'Dim xPath As String = ""
                'Dim strError As String = ""
                'Dim mdtTran As DataTable = objDocument.GetQulificationAttachment(CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhEmpid.Index).Value), enScanAttactRefId.QUALIFICATIONS, CInt(dgvData.Rows(e.RowIndex).Cells(objcolhlinkedmasterid.Index).Value), ConfigParameter._Object._Document_Path)

                'Dim strLocalPath As String = String.Empty
                'Dim fileToAdd As New List(Of String)
                'Dim mstrFoldername As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString

                'For Each xRow As DataRow In mdtTran.Rows
                '    If IsDBNull(xRow("filepath")) = False Then

                '        If IsSelfServiceExist() Then
                '            Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xRow("filepath").ToString, xRow("fileuniquename").ToString, mstrFoldername, strError, ConfigParameter._Object._ArutiSelfServiceURL)
                '            If imagebyte IsNot Nothing Then
                '                strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                '                Dim ms As New MemoryStream(imagebyte)
                '                Dim fs As New FileStream(strLocalPath, FileMode.Create)
                '                ms.WriteTo(fs)
                '                ms.Close()
                '                fs.Close()
                '                fs.Dispose()
                '            End If
                '            fileToAdd.Add(strLocalPath)
                '        Else
                '            strLocalPath = ConfigParameter._Object._Document_Path & "\" & (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString & "\" & xRow("fileuniquename").ToString
                '            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFoldername) = False Then
                '                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFoldername)
                '            End If
                '            fileToAdd.Add(strLocalPath)
                '        End If
                '    End If
                'Next

                'If fileToAdd.Count > 0 Then
                '    Dim sfd As New SaveFileDialog() : sfd.Filter = "Zip File (.zip)|*.zip"
                '    If sfd.ShowDialog() = DialogResult.OK Then
                '        Dim fl As New System.IO.FileInfo(sfd.FileName)
                '        AddFileToZip(fl.FullName, fileToAdd, CompressionOption.Normal)
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "File(s) downloaded successfully to the selected location."), enMsgBoxStyle.Information)
                '    End If
                'End If

                'Gajanan [17-DEC-2018] -- End

            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            Call SetGridStyle()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub
    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private Sub dgvData_DataBindingComplete(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
        'S.SANDEEP |26-APR-2019| -- START
        'For Each xdgvr As DataGridViewRow In dgvData.Rows

        '    If (CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) AndAlso CInt(xdgvr.Cells(objdgcolhIsGrp.Index).Value) <= 0) Or _
        '        (CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) AndAlso CInt(xdgvr.Cells(objdgcolhIsGrp.Index).Value) <= 0) Then
        '        objDocument = New clsScan_Attach_Documents

        '        If CInt(cboOperationType.SelectedValue) = clsEmployeeDataApproval.enOperationType.ADDED AndAlso xdgvr.Cells(objcolhnewattachdocumentid.Index).Value.ToString().Length > 0 Then
        '            If CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
        '                mdtDocuments = objDocument.GetQulificationAttachment(CInt(xdgvr.Cells(objdgcolhEmpid.Index).Value), _
        '                                                                     CInt(enScanAttactRefId.QUALIFICATIONS), _
        '                                                                     CInt(xdgvr.Cells(objcolhqualificationtranunkid.Index).Value), _
        '                                                                     ConfigParameter._Object._Document_Path, 0, _
        '                                                                     xdgvr.Cells(objcolhnewattachdocumentid.Index).Value.ToString, _
        '                                                                     True) 'S.SANDEEP |26-APR-2019| -- START {True} -- END
        '            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
        '                mdtDocuments = objDocument.GetQulificationAttachment(CInt(xdgvr.Cells(objdgcolhEmpid.Index).Value), _
        '                                                                     enScanAttactRefId.DEPENDANTS, _
        '                                                                     CInt(xdgvr.Cells(objcolhdepedantunkid.Index).Value), _
        '                                                                     ConfigParameter._Object._Document_Path, 0, _
        '                                                                     xdgvr.Cells(objcolhnewattachdocumentid.Index).Value.ToString, _
        '                                                                     True) 'S.SANDEEP |26-APR-2019| -- START {True} -- END
        '                'S.SANDEEP |26-APR-2019| -- START
        '            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmIdentityInfoList) Then

        '            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmAddressList) Then

        '            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmEmergencyAddressList) Then

        '            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmBirthinfo) Then

        '            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmOtherinfo) Then

        '                'S.SANDEEP |26-APR-2019| -- END
        '            End If
        '        End If

        '        If IsNothing(mdtDocuments) = False AndAlso mdtDocuments.Rows.Count > 0 Then
        '            xdgvr.Cells(objdgcolhDownloadDocument.Index).Value = imgView
        '            xdgvr.Cells(objdgcolhDownloadDocument.Index).ToolTipText = Language.getMessage(mstrModuleName, 30, "Download Attached Document(s)")
        '        Else
        '            xdgvr.Cells(objdgcolhDownloadDocument.Index).Value = imgBlank
        '        End If
        '    End If
        'Next
        Try
            If objdgcolhDownloadDocument.Visible = True Then
                For Each xdgvr As DataGridViewRow In dgvData.Rows
                    If CInt(xdgvr.Cells(objdgcolhIsGrp.Index).Value) = 1 Then
                        xdgvr.Cells(objdgcolhDownloadDocument.Index).Value = imgBlank
                    Else
                        If xdgvr.Cells(objcolhnewattachdocumentid.Index).Value Is Nothing Or _
                           xdgvr.Cells(objcolhdeleteattachdocumentid.Index).Value Is Nothing Then
                            Continue For
                End If

                        'If mdtDocuments.Rows.Count > 0 Then
                        If xdgvr.Cells(objcolhnewattachdocumentid.Index).Value.ToString().Trim.Length > 0 Or _
                           xdgvr.Cells(objcolhdeleteattachdocumentid.Index).Value.ToString.Trim.Length > 0 Then
        
                    xdgvr.Cells(objdgcolhDownloadDocument.Index).Value = imgView
                    xdgvr.Cells(objdgcolhDownloadDocument.Index).ToolTipText = Language.getMessage(mstrModuleName, 30, "Download Attached Document(s)")
                Else
                    xdgvr.Cells(objdgcolhDownloadDocument.Index).Value = imgBlank
                End If
            End If
        Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
        Finally
        End Try
        'S.SANDEEP |26-APR-2019| -- END
    End Sub
    'Gajanan [17-DEC-2018] -- End
#End Region


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
#Region "Context Menu Items event"

    'S.SANDEEP |26-APR-2019| -- START
    Private Sub btndownloadallattachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndownloadallattachment.Click, btndownloaddeletedattachment.Click, btndownloadnewattachment.Click
        Try
            Dim mdtTran As DataTable = Nothing
            Dim objDocument As New clsScan_Attach_Documents
            Dim strScanAttachRefTranId As String = String.Empty

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim mstrFoldername As String
            'Gajanan [17-April-2019] -- End

            GC.Collect()


            Select Case CType(sender, ToolStripMenuItem).Name
                Case btndownloadallattachment.Name 'ALL ATTACHMENT
                    If dgvData.Rows(rowIndex).Cells(objcolhnewattachdocumentid.Index).Value IsNot Nothing AndAlso _
                       dgvData.Rows(rowIndex).Cells(objcolhnewattachdocumentid.Index).Value.ToString.Trim.Length > 0 Then
                        strScanAttachRefTranId &= "," & dgvData.Rows(rowIndex).Cells(objcolhnewattachdocumentid.Index).Value.ToString()
                    End If
                    If dgvData.Rows(rowIndex).Cells(objcolhdeleteattachdocumentid.Index).Value IsNot Nothing AndAlso _
                       dgvData.Rows(rowIndex).Cells(objcolhdeleteattachdocumentid.Index).Value.ToString.Trim.Length > 0 Then
                        strScanAttachRefTranId &= "," & dgvData.Rows(rowIndex).Cells(objcolhdeleteattachdocumentid.Index).Value.ToString()
                    End If
                    If strScanAttachRefTranId.Trim.Length > 0 Then strScanAttachRefTranId = Mid(strScanAttachRefTranId, 2)
                Case btndownloadnewattachment.Name 'NEW ATTACHMENT
                    If dgvData.Rows(rowIndex).Cells(objcolhnewattachdocumentid.Index).Value IsNot Nothing AndAlso _
                       dgvData.Rows(rowIndex).Cells(objcolhnewattachdocumentid.Index).Value.ToString.Trim.Length > 0 Then
                        strScanAttachRefTranId &= "," & dgvData.Rows(rowIndex).Cells(objcolhnewattachdocumentid.Index).Value.ToString()
                    End If
                    If strScanAttachRefTranId.Trim.Length > 0 Then strScanAttachRefTranId = Mid(strScanAttachRefTranId, 2)
                Case btndownloaddeletedattachment.Name 'DELETE ATTACHMENT
                    If dgvData.Rows(rowIndex).Cells(objcolhdeleteattachdocumentid.Index).Value IsNot Nothing AndAlso _
                       dgvData.Rows(rowIndex).Cells(objcolhdeleteattachdocumentid.Index).Value.ToString.Trim.Length > 0 Then
                        strScanAttachRefTranId &= "," & dgvData.Rows(rowIndex).Cells(objcolhdeleteattachdocumentid.Index).Value.ToString()
                    End If
                    If strScanAttachRefTranId.Trim.Length > 0 Then strScanAttachRefTranId = Mid(strScanAttachRefTranId, 2)
            End Select
            Dim eAttachmentType As enScanAttactRefId = Nothing
            Select Case CInt(cboEmployeeData.SelectedValue)

                'Gajanan [5-Dec-2019] -- Start   
                'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                Case enScreenName.frmJobHistory_ExperienceList
                    eAttachmentType = enScanAttactRefId.JOBHISTORYS
                    mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.JOBHISTORYS).Tables(0).Rows(0)("Name").ToString
                    'Gajanan [5-Dec-2019] -- End

                Case enScreenName.frmQualificationsList
                    eAttachmentType = enScanAttactRefId.QUALIFICATIONS
                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString
                    'Gajanan [17-April-2019] -- End
                Case enScreenName.frmDependantsAndBeneficiariesList
                    eAttachmentType = enScanAttactRefId.DEPENDANTS
                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DEPENDANTS).Tables(0).Rows(0)("Name").ToString
                    'Gajanan [17-April-2019] -- End
                Case enScreenName.frmIdentityInfoList
                    eAttachmentType = enScanAttactRefId.IDENTITYS
                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.IDENTITYS).Tables(0).Rows(0)("Name").ToString
                    'Gajanan [17-April-2019] -- End
                Case enScreenName.frmAddressList
                    Select Case CInt(dgvData.Rows(rowIndex).Cells(objcolhAddressTypeId.Index).Value)
                        Case clsEmployeeAddress_approval_tran.enAddressType.PRESENT
                            eAttachmentType = enScanAttactRefId.PERSONAL_ADDRESS_TYPE
                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.PERSONAL_ADDRESS_TYPE).Tables(0).Rows(0)("Name").ToString
                            'Gajanan [17-April-2019] -- End

                        Case clsEmployeeAddress_approval_tran.enAddressType.DOMICILE
                            eAttachmentType = enScanAttactRefId.DOMICILE_ADDRESS_TYPE
                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DOMICILE_ADDRESS_TYPE).Tables(0).Rows(0)("Name").ToString
                            'Gajanan [17-April-2019] -- End
                        Case clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT
                            eAttachmentType = enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE
                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE).Tables(0).Rows(0)("Name").ToString
                            'Gajanan [17-April-2019] -- End
                    End Select
                Case enScreenName.frmEmergencyAddressList
                    Select Case CInt(dgvData.Rows(rowIndex).Cells(objcolhAddressTypeId.Index).Value)
                        Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1
                            eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT1
                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMERGENCY_CONTACT1).Tables(0).Rows(0)("Name").ToString
                            'Gajanan [17-April-2019] -- End
                        Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2
                            eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT2
                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMERGENCY_CONTACT2).Tables(0).Rows(0)("Name").ToString
                            'Gajanan [17-April-2019] -- End
                        Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3
                            eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT3
                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMERGENCY_CONTACT3).Tables(0).Rows(0)("Name").ToString
                            'Gajanan [17-April-2019] -- End
                    End Select
                Case enScreenName.frmBirthinfo
                    If CBool(dgvData.Rows(rowIndex).Cells(objcolhAddressTypeId.Index).Value) = True Then
                        eAttachmentType = enScanAttactRefId.EMPLOYEE_BIRTHINFO
                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMPLOYEE_BIRTHINFO).Tables(0).Rows(0)("Name").ToString
                        'Gajanan [17-April-2019] -- End
                    End If
                Case enScreenName.frmOtherinfo
                    If CBool(dgvData.Rows(rowIndex).Cells(objcolhAddressTypeId.Index).Value) = False Then
                        eAttachmentType = enScanAttactRefId.EMPLYOEE_OTHERLDETAILS
                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMPLYOEE_OTHERLDETAILS).Tables(0).Rows(0)("Name").ToString
                        'Gajanan [17-April-2019] -- End
                End If
            End Select

            If eAttachmentType <> Nothing AndAlso strScanAttachRefTranId.Trim.Length > 0 Then

                mdtTran = objDocument.GetQulificationAttachment(CInt(dgvData.Rows(rowIndex).Cells(objdgcolhEmpid.Index).Value), eAttachmentType, 0, _
                                                            ConfigParameter._Object._Document_Path, 0, strScanAttachRefTranId, True)

                If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'DownloadDocumnet(mdtTran)
                    DownloadDocument(mdtTran, mstrFoldername)
                    'Gajanan [17-April-2019] -- End
                End If
                GC.Collect()

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "There is attached document(s) available."), enMsgBoxStyle.Information)
                End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btndownloadallattachment_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'Private Sub btndownloadallattachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndownloadallattachment.Click
    '    Try
    '        Dim mdtTran As DataTable = Nothing

    '        If CInt(cboOperationType.SelectedValue) = clsEmployeeDataApproval.enOperationType.ADDED Then

    '            If CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
    '                mdtTran = objDocument.GetQulificationAttachment(CInt(dgvData.Rows(rowIndex).Cells(objdgcolhEmpid.Index).Value), _
    '                                                                enScanAttactRefId.QUALIFICATIONS, _
    '                                                                CInt(dgvData.Rows(rowIndex).Cells(objcolhqualificationtranunkid.Index).Value), _
    '                                                                ConfigParameter._Object._Document_Path, 0, _
    '                                                                dgvData.Rows(rowIndex).Cells(objcolhnewattachdocumentid.Index).Value.ToString)

    '            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
    '                mdtTran = objDocument.GetQulificationAttachment(CInt(dgvData.Rows(rowIndex).Cells(objdgcolhEmpid.Index).Value), _
    '                                                                enScanAttactRefId.DEPENDANTS, _
    '                                                                CInt(dgvData.Rows(rowIndex).Cells(objcolhdepedantunkid.Index).Value), _
    '                                                                ConfigParameter._Object._Document_Path, 0, _
    '                                                                dgvData.Rows(rowIndex).Cells(objcolhnewattachdocumentid.Index).Value.ToString())
    '            End If

    '        Else
    '            If CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
    '                mdtTran = objDocument.GetQulificationAttachment(CInt(dgvData.Rows(rowIndex).Cells(objdgcolhEmpid.Index).Value), _
    '                                                                enScanAttactRefId.QUALIFICATIONS, _
    '                                                                CInt(dgvData.Rows(rowIndex).Cells(objcolhqualificationtranunkid.Index).Value), _
    '                                                                ConfigParameter._Object._Document_Path, 0, "")

    '            ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
    '                mdtTran = objDocument.GetQulificationAttachment(CInt(dgvData.Rows(rowIndex).Cells(objdgcolhEmpid.Index).Value), _
    '                                                                enScanAttactRefId.DEPENDANTS, _
    '                                                                CInt(dgvData.Rows(rowIndex).Cells(objcolhdepedantunkid.Index).Value), _
    '                                                                ConfigParameter._Object._Document_Path, 0, "")

    '            End If
    '        End If

    '        DownloadDocumnet(mdtTran)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btndownloadallattachment_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub btndownloadnewattachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndownloadnewattachment.Click
    '    Try

    '        If CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
    '            If dgvData.Rows(rowIndex).Cells(objcolhnewattachdocumentid.Index).Value.ToString.Length > 0 Then
    '                Dim mdtTran As DataTable = objDocument.GetQulificationAttachment(CInt(dgvData.Rows(rowIndex).Cells(objdgcolhEmpid.Index).Value), _
    '                                                                                 enScanAttactRefId.QUALIFICATIONS, _
    '                                                                                 0, ConfigParameter._Object._Document_Path, _
    '                                                                                 1, dgvData.Rows(rowIndex).Cells(objcolhnewattachdocumentid.Index).Value.ToString)
    '                DownloadDocumnet(mdtTran)
    '            Else
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 102, "There is no newly added attached document(s) available."), enMsgBoxStyle.Information)
    '                Exit Sub
    '            End If
    '        ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
    '            If dgvData.Rows(rowIndex).Cells(objcolhnewattachdocumentid.Index).Value.ToString.Length > 0 Then
    '                Dim mdtTran As DataTable = objDocument.GetQulificationAttachment(CInt(dgvData.Rows(rowIndex).Cells(objdgcolhEmpid.Index).Value), _
    '                                                                                 enScanAttactRefId.DEPENDANTS, 0, _
    '                                                                                 ConfigParameter._Object._Document_Path, 1, _
    '                                                                                 dgvData.Rows(rowIndex).Cells(objcolhnewattachdocumentid.Index).Value.ToString)
    '                DownloadDocumnet(mdtTran)
    '            Else
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 102, "There is no newly added attached document(s) available."), enMsgBoxStyle.Information)
    '                Exit Sub
    '            End If
    '        End If



    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btndownloadnewattachment_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub btndownloaddeletedattachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndownloaddeletedattachment.Click
    '    Try
    '        If CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
    '            If dgvData.Rows(rowIndex).Cells(objcolhdeleteattachdocumentid.Index).Value.ToString.Length > 0 Then
    '                Dim mdtTran As DataTable = objDocument.GetQulificationAttachment(CInt(dgvData.Rows(rowIndex).Cells(objdgcolhEmpid.Index).Value), _
    '                                                                                 enScanAttactRefId.QUALIFICATIONS, 0, _
    '                                                                                 ConfigParameter._Object._Document_Path, 0, _
    '                                                                                 dgvData.Rows(rowIndex).Cells(objcolhdeleteattachdocumentid.Index).Value.ToString)
    '                DownloadDocumnet(mdtTran)
    '            Else
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "There is no deleted attached document(s) available."), enMsgBoxStyle.Information)
    '            End If
    '        ElseIf CInt(cboEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
    '            If dgvData.Rows(rowIndex).Cells(objcolhdeleteattachdocumentid.Index).Value.ToString.Length > 0 Then
    '                Dim mdtTran As DataTable = objDocument.GetQulificationAttachment(CInt(dgvData.Rows(rowIndex).Cells(objdgcolhEmpid.Index).Value), enScanAttactRefId.DEPENDANTS, 0, ConfigParameter._Object._Document_Path, 0, dgvData.Rows(rowIndex).Cells(objcolhdeleteattachdocumentid.Index).Value.ToString)
    '                DownloadDocumnet(mdtTran)
    '            Else
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "There is no deleted attached document(s) available."), enMsgBoxStyle.Information)
    '            End If
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btndownloaddeletedattachment_Click", mstrModuleName)
    '    End Try
    'End Sub
    'S.SANDEEP |26-APR-2019| -- END

#End Region
    'Gajanan [22-Feb-2019] -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnShowMyReport.GradientBackColor = GUI._ButttonBackColor
            Me.btnShowMyReport.GradientForeColor = GUI._ButttonFontColor

            Me.btnApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnReject.GradientBackColor = GUI._ButttonBackColor
            Me.btnReject.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
            Me.lblLoggedInUser.Text = Language._Object.getCaption(Me.lblLoggedInUser.Name, Me.lblLoggedInUser.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.btnShowMyReport.Text = Language._Object.getCaption(Me.btnShowMyReport.Name, Me.btnShowMyReport.Text)
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.btnReject.Text = Language._Object.getCaption(Me.btnReject.Name, Me.btnReject.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblEmployeeData.Text = Language._Object.getCaption(Me.lblEmployeeData.Name, Me.lblEmployeeData.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
			Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
			Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
			Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
			Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
			Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
			Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
			Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
			Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
			Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
			Me.DataGridViewTextBoxColumn22.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn22.Name, Me.DataGridViewTextBoxColumn22.HeaderText)
			Me.DataGridViewTextBoxColumn23.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn23.Name, Me.DataGridViewTextBoxColumn23.HeaderText)
			Me.lblotherqualificationnote.Text = Language._Object.getCaption(Me.lblotherqualificationnote.Name, Me.lblotherqualificationnote.Text)
			Me.DataGridViewTextBoxColumn24.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn24.Name, Me.DataGridViewTextBoxColumn24.HeaderText)
			Me.DataGridViewTextBoxColumn25.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn25.Name, Me.DataGridViewTextBoxColumn25.HeaderText)
			Me.lblSelectOperationType.Text = Language._Object.getCaption(Me.lblSelectOperationType.Name, Me.lblSelectOperationType.Text)
			Me.btndownloadallattachment.Text = Language._Object.getCaption(Me.btndownloadallattachment.Name, Me.btndownloadallattachment.Text)
			Me.btndownloadnewattachment.Text = Language._Object.getCaption(Me.btndownloadnewattachment.Name, Me.btndownloadnewattachment.Text)
			Me.btndownloaddeletedattachment.Text = Language._Object.getCaption(Me.btndownloaddeletedattachment.Name, Me.btndownloaddeletedattachment.Text)
			Me.DataGridViewTextBoxColumn26.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn26.Name, Me.DataGridViewTextBoxColumn26.HeaderText)
			Me.DataGridViewTextBoxColumn27.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn27.Name, Me.DataGridViewTextBoxColumn27.HeaderText)
			Me.DataGridViewTextBoxColumn28.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn28.Name, Me.DataGridViewTextBoxColumn28.HeaderText)
			Me.DataGridViewTextBoxColumn29.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn29.Name, Me.DataGridViewTextBoxColumn29.HeaderText)
			Me.DataGridViewTextBoxColumn30.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn30.Name, Me.DataGridViewTextBoxColumn30.HeaderText)
			Me.DataGridViewTextBoxColumn31.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn31.Name, Me.DataGridViewTextBoxColumn31.HeaderText)
			Me.DataGridViewTextBoxColumn32.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn32.Name, Me.DataGridViewTextBoxColumn32.HeaderText)
			Me.DataGridViewTextBoxColumn33.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn33.Name, Me.DataGridViewTextBoxColumn33.HeaderText)
			Me.DataGridViewTextBoxColumn34.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn34.Name, Me.DataGridViewTextBoxColumn34.HeaderText)
			Me.DataGridViewTextBoxColumn35.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn35.Name, Me.DataGridViewTextBoxColumn35.HeaderText)
			Me.dgcolhSkillCategory.HeaderText = Language._Object.getCaption(Me.dgcolhSkillCategory.Name, Me.dgcolhSkillCategory.HeaderText)
			Me.dgcolhSkill.HeaderText = Language._Object.getCaption(Me.dgcolhSkill.Name, Me.dgcolhSkill.HeaderText)
			Me.dgcolhDescription.HeaderText = Language._Object.getCaption(Me.dgcolhDescription.Name, Me.dgcolhDescription.HeaderText)
			Me.dgcolhQualifyGroup.HeaderText = Language._Object.getCaption(Me.dgcolhQualifyGroup.Name, Me.dgcolhQualifyGroup.HeaderText)
			Me.dgcolhQualification.HeaderText = Language._Object.getCaption(Me.dgcolhQualification.Name, Me.dgcolhQualification.HeaderText)
			Me.dgcolhAwardDate.HeaderText = Language._Object.getCaption(Me.dgcolhAwardDate.Name, Me.dgcolhAwardDate.HeaderText)
			Me.dgcolhAwardToDate.HeaderText = Language._Object.getCaption(Me.dgcolhAwardToDate.Name, Me.dgcolhAwardToDate.HeaderText)
			Me.dgcolhInstitute.HeaderText = Language._Object.getCaption(Me.dgcolhInstitute.Name, Me.dgcolhInstitute.HeaderText)
			Me.dgcolhRefNo.HeaderText = Language._Object.getCaption(Me.dgcolhRefNo.Name, Me.dgcolhRefNo.HeaderText)
			Me.dgcolhCompany.HeaderText = Language._Object.getCaption(Me.dgcolhCompany.Name, Me.dgcolhCompany.HeaderText)
			Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
			Me.dgcolhStartDate.HeaderText = Language._Object.getCaption(Me.dgcolhStartDate.Name, Me.dgcolhStartDate.HeaderText)
			Me.dgcolhEndDate.HeaderText = Language._Object.getCaption(Me.dgcolhEndDate.Name, Me.dgcolhEndDate.HeaderText)
			Me.dgcolhSupervisor.HeaderText = Language._Object.getCaption(Me.dgcolhSupervisor.Name, Me.dgcolhSupervisor.HeaderText)
			Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)
			Me.dgcolhRefreeName.HeaderText = Language._Object.getCaption(Me.dgcolhRefreeName.Name, Me.dgcolhRefreeName.HeaderText)
			Me.dgcolhCountry.HeaderText = Language._Object.getCaption(Me.dgcolhCountry.Name, Me.dgcolhCountry.HeaderText)
			Me.dgcolhIdNo.HeaderText = Language._Object.getCaption(Me.dgcolhIdNo.Name, Me.dgcolhIdNo.HeaderText)
			Me.dgcolhEmail.HeaderText = Language._Object.getCaption(Me.dgcolhEmail.Name, Me.dgcolhEmail.HeaderText)
			Me.dgcolhTelNo.HeaderText = Language._Object.getCaption(Me.dgcolhTelNo.Name, Me.dgcolhTelNo.HeaderText)
			Me.dgcolhMobile.HeaderText = Language._Object.getCaption(Me.dgcolhMobile.Name, Me.dgcolhMobile.HeaderText)
			Me.dgcolhDBName.HeaderText = Language._Object.getCaption(Me.dgcolhDBName.Name, Me.dgcolhDBName.HeaderText)
			Me.dgcolhDBRelation.HeaderText = Language._Object.getCaption(Me.dgcolhDBRelation.Name, Me.dgcolhDBRelation.HeaderText)
			Me.dgcolhDBbirthdate.HeaderText = Language._Object.getCaption(Me.dgcolhDBbirthdate.Name, Me.dgcolhDBbirthdate.HeaderText)
			Me.dgcolhDBIdentifyNo.HeaderText = Language._Object.getCaption(Me.dgcolhDBIdentifyNo.Name, Me.dgcolhDBIdentifyNo.HeaderText)
			Me.dgcolhDBGender.HeaderText = Language._Object.getCaption(Me.dgcolhDBGender.Name, Me.dgcolhDBGender.HeaderText)
			Me.dgcolhIdType.HeaderText = Language._Object.getCaption(Me.dgcolhIdType.Name, Me.dgcolhIdType.HeaderText)
			Me.dgcolhSrNo.HeaderText = Language._Object.getCaption(Me.dgcolhSrNo.Name, Me.dgcolhSrNo.HeaderText)
			Me.dgcolhIdentityNo.HeaderText = Language._Object.getCaption(Me.dgcolhIdentityNo.Name, Me.dgcolhIdentityNo.HeaderText)
			Me.dgcolhIdCountry.HeaderText = Language._Object.getCaption(Me.dgcolhIdCountry.Name, Me.dgcolhIdCountry.HeaderText)
			Me.dgcolhIdIssuePlace.HeaderText = Language._Object.getCaption(Me.dgcolhIdIssuePlace.Name, Me.dgcolhIdIssuePlace.HeaderText)
			Me.dgcolhIdIssueDate.HeaderText = Language._Object.getCaption(Me.dgcolhIdIssueDate.Name, Me.dgcolhIdIssueDate.HeaderText)
			Me.dgcolhIdExpiryDate.HeaderText = Language._Object.getCaption(Me.dgcolhIdExpiryDate.Name, Me.dgcolhIdExpiryDate.HeaderText)
			Me.dgcolhIdDLClass.HeaderText = Language._Object.getCaption(Me.dgcolhIdDLClass.Name, Me.dgcolhIdDLClass.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Please check atleast one employee in order to perform further operation.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Rejection remark is mandatory information. Please enter enter remark to continue.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to approve employee(s) without remark?")
            Language.setMessage(mstrModuleName, 4, "Employee Data is mandatory information. Please select employee data to continue")
			Language.setMessage(mstrModuleName, 13, "File(s) downloaded successfully to the selected location.")
			Language.setMessage(mstrModuleName, 30, "Download Attached Document(s)")
			Language.setMessage(mstrModuleName, 100, "Opration Type is mandatory information. Please select opration type to continue")
			Language.setMessage(mstrModuleName, 101, "There is no deleted attached document(s) available.")
			Language.setMessage(mstrModuleName, 102, "There is no newly added attached document(s) available.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class