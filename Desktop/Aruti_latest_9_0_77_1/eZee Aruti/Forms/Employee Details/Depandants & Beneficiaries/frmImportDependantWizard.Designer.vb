﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportDependantWizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportDependantWizard))
        Me.eZeeWizImportEmp = New eZee.Common.eZeeWizard
        Me.WizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.cboEffective_Date = New System.Windows.Forms.ComboBox
        Me.cboDEPENDANT_MIDDLENAME = New System.Windows.Forms.ComboBox
        Me.lblDepMiddleName = New System.Windows.Forms.Label
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.lblGender = New System.Windows.Forms.Label
        Me.cboGENDER = New System.Windows.Forms.ComboBox
        Me.lblIdNo = New System.Windows.Forms.Label
        Me.cboID_NO = New System.Windows.Forms.ComboBox
        Me.lblMobile = New System.Windows.Forms.Label
        Me.cboMOBILENO = New System.Windows.Forms.ComboBox
        Me.lblTelephone = New System.Windows.Forms.Label
        Me.cboTELEPHONE = New System.Windows.Forms.ComboBox
        Me.lblSurname = New System.Windows.Forms.Label
        Me.cboEMPLOYEE_SURNAME = New System.Windows.Forms.ComboBox
        Me.lblFirsName = New System.Windows.Forms.Label
        Me.cboEMPLOYEE_FIRSTNAME = New System.Windows.Forms.ComboBox
        Me.cboISBENEFICIARY = New System.Windows.Forms.ComboBox
        Me.lblIsBeneficiary = New System.Windows.Forms.Label
        Me.objlblSign6 = New System.Windows.Forms.Label
        Me.cboRELATION = New System.Windows.Forms.ComboBox
        Me.lblRelation = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.lblAddress = New System.Windows.Forms.Label
        Me.cboADDRESS = New System.Windows.Forms.ComboBox
        Me.cboBIRTHDATE = New System.Windows.Forms.ComboBox
        Me.lblBirthDate = New System.Windows.Forms.Label
        Me.objlblSign5 = New System.Windows.Forms.Label
        Me.cboEMPLOYEE_CODE = New System.Windows.Forms.ComboBox
        Me.cboDEPENDANT_LASTNAME = New System.Windows.Forms.ComboBox
        Me.lblDepFirstName = New System.Windows.Forms.Label
        Me.cboDEPENDANT_FIRSTNAME = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.objlblSign4 = New System.Windows.Forms.Label
        Me.lblDepLastName = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.objlblSign2 = New System.Windows.Forms.Label
        Me.cboPincode = New System.Windows.Forms.ComboBox
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblCountry = New System.Windows.Forms.Label
        Me.lblNationality = New System.Windows.Forms.Label
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.cboNationality = New System.Windows.Forms.ComboBox
        Me.lblState = New System.Windows.Forms.Label
        Me.lblPincode = New System.Windows.Forms.Label
        Me.cboCity = New System.Windows.Forms.ComboBox
        Me.lblCity = New System.Windows.Forms.Label
        Me.WizPageSelectFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblMessage2 = New System.Windows.Forms.Label
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.WizPageImporting = New eZee.Common.eZeeWizardPage(Me.components)
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhlogindate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.objbuttonBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblSign7 = New System.Windows.Forms.Label
        Me.eZeeWizImportEmp.SuspendLayout()
        Me.WizPageMapping.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.WizPageSelectFile.SuspendLayout()
        Me.WizPageImporting.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsFilter.SuspendLayout()
        Me.pnlInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeWizImportEmp
        '
        Me.eZeeWizImportEmp.Controls.Add(Me.WizPageMapping)
        Me.eZeeWizImportEmp.Controls.Add(Me.WizPageSelectFile)
        Me.eZeeWizImportEmp.Controls.Add(Me.WizPageImporting)
        Me.eZeeWizImportEmp.Controls.Add(Me.objbuttonBack)
        Me.eZeeWizImportEmp.Controls.Add(Me.objbuttonCancel)
        Me.eZeeWizImportEmp.Controls.Add(Me.objbuttonNext)
        Me.eZeeWizImportEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.eZeeWizImportEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeWizImportEmp.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.eZeeWizImportEmp.Location = New System.Drawing.Point(0, 0)
        Me.eZeeWizImportEmp.Name = "eZeeWizImportEmp"
        Me.eZeeWizImportEmp.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.WizPageSelectFile, Me.WizPageMapping, Me.WizPageImporting})
        Me.eZeeWizImportEmp.SaveEnabled = True
        Me.eZeeWizImportEmp.SaveText = "Save && Finish"
        Me.eZeeWizImportEmp.SaveVisible = False
        Me.eZeeWizImportEmp.SetSaveIndexBeforeFinishIndex = False
        Me.eZeeWizImportEmp.Size = New System.Drawing.Size(698, 421)
        Me.eZeeWizImportEmp.TabIndex = 1
        Me.eZeeWizImportEmp.WelcomeImage = Nothing
        '
        'WizPageMapping
        '
        Me.WizPageMapping.Controls.Add(Me.gbFieldMapping)
        Me.WizPageMapping.Controls.Add(Me.cboPincode)
        Me.WizPageMapping.Controls.Add(Me.cboCountry)
        Me.WizPageMapping.Controls.Add(Me.lblCountry)
        Me.WizPageMapping.Controls.Add(Me.lblNationality)
        Me.WizPageMapping.Controls.Add(Me.cboState)
        Me.WizPageMapping.Controls.Add(Me.cboNationality)
        Me.WizPageMapping.Controls.Add(Me.lblState)
        Me.WizPageMapping.Controls.Add(Me.lblPincode)
        Me.WizPageMapping.Controls.Add(Me.cboCity)
        Me.WizPageMapping.Controls.Add(Me.lblCity)
        Me.WizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.WizPageMapping.Name = "WizPageMapping"
        Me.WizPageMapping.Size = New System.Drawing.Size(698, 373)
        Me.WizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageMapping.TabIndex = 8
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.objlblSign7)
        Me.gbFieldMapping.Controls.Add(Me.lblEffectiveDate)
        Me.gbFieldMapping.Controls.Add(Me.cboEffective_Date)
        Me.gbFieldMapping.Controls.Add(Me.cboDEPENDANT_MIDDLENAME)
        Me.gbFieldMapping.Controls.Add(Me.lblDepMiddleName)
        Me.gbFieldMapping.Controls.Add(Me.lnkAutoMap)
        Me.gbFieldMapping.Controls.Add(Me.lblGender)
        Me.gbFieldMapping.Controls.Add(Me.cboGENDER)
        Me.gbFieldMapping.Controls.Add(Me.lblIdNo)
        Me.gbFieldMapping.Controls.Add(Me.cboID_NO)
        Me.gbFieldMapping.Controls.Add(Me.lblMobile)
        Me.gbFieldMapping.Controls.Add(Me.cboMOBILENO)
        Me.gbFieldMapping.Controls.Add(Me.lblTelephone)
        Me.gbFieldMapping.Controls.Add(Me.cboTELEPHONE)
        Me.gbFieldMapping.Controls.Add(Me.lblSurname)
        Me.gbFieldMapping.Controls.Add(Me.cboEMPLOYEE_SURNAME)
        Me.gbFieldMapping.Controls.Add(Me.lblFirsName)
        Me.gbFieldMapping.Controls.Add(Me.cboEMPLOYEE_FIRSTNAME)
        Me.gbFieldMapping.Controls.Add(Me.cboISBENEFICIARY)
        Me.gbFieldMapping.Controls.Add(Me.lblIsBeneficiary)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign6)
        Me.gbFieldMapping.Controls.Add(Me.cboRELATION)
        Me.gbFieldMapping.Controls.Add(Me.lblRelation)
        Me.gbFieldMapping.Controls.Add(Me.lblCaption)
        Me.gbFieldMapping.Controls.Add(Me.lblAddress)
        Me.gbFieldMapping.Controls.Add(Me.cboADDRESS)
        Me.gbFieldMapping.Controls.Add(Me.cboBIRTHDATE)
        Me.gbFieldMapping.Controls.Add(Me.lblBirthDate)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign5)
        Me.gbFieldMapping.Controls.Add(Me.cboEMPLOYEE_CODE)
        Me.gbFieldMapping.Controls.Add(Me.cboDEPENDANT_LASTNAME)
        Me.gbFieldMapping.Controls.Add(Me.lblDepFirstName)
        Me.gbFieldMapping.Controls.Add(Me.cboDEPENDANT_FIRSTNAME)
        Me.gbFieldMapping.Controls.Add(Me.lblEmployeeCode)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign4)
        Me.gbFieldMapping.Controls.Add(Me.lblDepLastName)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign1)
        Me.gbFieldMapping.Controls.Add(Me.objlblSign2)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(164, 0)
        Me.gbFieldMapping.Margin = New System.Windows.Forms.Padding(0)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(534, 373)
        Me.gbFieldMapping.TabIndex = 0
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(24, 206)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(96, 17)
        Me.lblEffectiveDate.TabIndex = 119
        Me.lblEffectiveDate.Text = "Effective Date"
        Me.lblEffectiveDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEffective_Date
        '
        Me.cboEffective_Date.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEffective_Date.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEffective_Date.FormattingEnabled = True
        Me.cboEffective_Date.Location = New System.Drawing.Point(126, 204)
        Me.cboEffective_Date.Name = "cboEffective_Date"
        Me.cboEffective_Date.Size = New System.Drawing.Size(143, 21)
        Me.cboEffective_Date.TabIndex = 120
        '
        'cboDEPENDANT_MIDDLENAME
        '
        Me.cboDEPENDANT_MIDDLENAME.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDEPENDANT_MIDDLENAME.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDEPENDANT_MIDDLENAME.FormattingEnabled = True
        Me.cboDEPENDANT_MIDDLENAME.Location = New System.Drawing.Point(126, 95)
        Me.cboDEPENDANT_MIDDLENAME.Name = "cboDEPENDANT_MIDDLENAME"
        Me.cboDEPENDANT_MIDDLENAME.Size = New System.Drawing.Size(143, 21)
        Me.cboDEPENDANT_MIDDLENAME.TabIndex = 117
        '
        'lblDepMiddleName
        '
        Me.lblDepMiddleName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepMiddleName.Location = New System.Drawing.Point(24, 97)
        Me.lblDepMiddleName.Name = "lblDepMiddleName"
        Me.lblDepMiddleName.Size = New System.Drawing.Size(96, 17)
        Me.lblDepMiddleName.TabIndex = 116
        Me.lblDepMiddleName.Text = "Dep. MiddleName"
        Me.lblDepMiddleName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAutoMap.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAutoMap.Location = New System.Drawing.Point(409, 342)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(113, 20)
        Me.lnkAutoMap.TabIndex = 114
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "Auto Map"
        Me.lnkAutoMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblGender
        '
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(24, 260)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(96, 17)
        Me.lblGender.TabIndex = 74
        Me.lblGender.Text = "Gender"
        Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGENDER
        '
        Me.cboGENDER.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGENDER.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGENDER.FormattingEnabled = True
        Me.cboGENDER.Location = New System.Drawing.Point(126, 258)
        Me.cboGENDER.Name = "cboGENDER"
        Me.cboGENDER.Size = New System.Drawing.Size(143, 21)
        Me.cboGENDER.TabIndex = 75
        '
        'lblIdNo
        '
        Me.lblIdNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdNo.Location = New System.Drawing.Point(281, 179)
        Me.lblIdNo.Name = "lblIdNo"
        Me.lblIdNo.Size = New System.Drawing.Size(90, 17)
        Me.lblIdNo.TabIndex = 32
        Me.lblIdNo.Text = "Id. No"
        Me.lblIdNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboID_NO
        '
        Me.cboID_NO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboID_NO.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboID_NO.FormattingEnabled = True
        Me.cboID_NO.Location = New System.Drawing.Point(377, 177)
        Me.cboID_NO.Name = "cboID_NO"
        Me.cboID_NO.Size = New System.Drawing.Size(143, 21)
        Me.cboID_NO.TabIndex = 33
        '
        'lblMobile
        '
        Me.lblMobile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMobile.Location = New System.Drawing.Point(281, 152)
        Me.lblMobile.Name = "lblMobile"
        Me.lblMobile.Size = New System.Drawing.Size(90, 17)
        Me.lblMobile.TabIndex = 28
        Me.lblMobile.Text = "Mobile"
        Me.lblMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMOBILENO
        '
        Me.cboMOBILENO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMOBILENO.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMOBILENO.FormattingEnabled = True
        Me.cboMOBILENO.Location = New System.Drawing.Point(377, 150)
        Me.cboMOBILENO.Name = "cboMOBILENO"
        Me.cboMOBILENO.Size = New System.Drawing.Size(143, 21)
        Me.cboMOBILENO.TabIndex = 29
        '
        'lblTelephone
        '
        Me.lblTelephone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTelephone.Location = New System.Drawing.Point(281, 125)
        Me.lblTelephone.Name = "lblTelephone"
        Me.lblTelephone.Size = New System.Drawing.Size(90, 17)
        Me.lblTelephone.TabIndex = 26
        Me.lblTelephone.Text = "Telephone"
        Me.lblTelephone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTELEPHONE
        '
        Me.cboTELEPHONE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTELEPHONE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTELEPHONE.FormattingEnabled = True
        Me.cboTELEPHONE.Location = New System.Drawing.Point(377, 123)
        Me.cboTELEPHONE.Name = "cboTELEPHONE"
        Me.cboTELEPHONE.Size = New System.Drawing.Size(143, 21)
        Me.cboTELEPHONE.TabIndex = 27
        '
        'lblSurname
        '
        Me.lblSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSurname.Location = New System.Drawing.Point(281, 69)
        Me.lblSurname.Name = "lblSurname"
        Me.lblSurname.Size = New System.Drawing.Size(90, 17)
        Me.lblSurname.TabIndex = 22
        Me.lblSurname.Text = "Emp. Surname"
        Me.lblSurname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEMPLOYEE_SURNAME
        '
        Me.cboEMPLOYEE_SURNAME.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEMPLOYEE_SURNAME.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEMPLOYEE_SURNAME.FormattingEnabled = True
        Me.cboEMPLOYEE_SURNAME.Location = New System.Drawing.Point(377, 67)
        Me.cboEMPLOYEE_SURNAME.Name = "cboEMPLOYEE_SURNAME"
        Me.cboEMPLOYEE_SURNAME.Size = New System.Drawing.Size(143, 21)
        Me.cboEMPLOYEE_SURNAME.TabIndex = 23
        '
        'lblFirsName
        '
        Me.lblFirsName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirsName.Location = New System.Drawing.Point(281, 42)
        Me.lblFirsName.Name = "lblFirsName"
        Me.lblFirsName.Size = New System.Drawing.Size(90, 17)
        Me.lblFirsName.TabIndex = 20
        Me.lblFirsName.Text = "Emp. Firstname"
        Me.lblFirsName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEMPLOYEE_FIRSTNAME
        '
        Me.cboEMPLOYEE_FIRSTNAME.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEMPLOYEE_FIRSTNAME.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEMPLOYEE_FIRSTNAME.FormattingEnabled = True
        Me.cboEMPLOYEE_FIRSTNAME.Location = New System.Drawing.Point(377, 40)
        Me.cboEMPLOYEE_FIRSTNAME.Name = "cboEMPLOYEE_FIRSTNAME"
        Me.cboEMPLOYEE_FIRSTNAME.Size = New System.Drawing.Size(143, 21)
        Me.cboEMPLOYEE_FIRSTNAME.TabIndex = 21
        '
        'cboISBENEFICIARY
        '
        Me.cboISBENEFICIARY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboISBENEFICIARY.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboISBENEFICIARY.FormattingEnabled = True
        Me.cboISBENEFICIARY.Location = New System.Drawing.Point(126, 177)
        Me.cboISBENEFICIARY.Name = "cboISBENEFICIARY"
        Me.cboISBENEFICIARY.Size = New System.Drawing.Size(143, 21)
        Me.cboISBENEFICIARY.TabIndex = 9
        '
        'lblIsBeneficiary
        '
        Me.lblIsBeneficiary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIsBeneficiary.Location = New System.Drawing.Point(24, 179)
        Me.lblIsBeneficiary.Name = "lblIsBeneficiary"
        Me.lblIsBeneficiary.Size = New System.Drawing.Size(96, 17)
        Me.lblIsBeneficiary.TabIndex = 8
        Me.lblIsBeneficiary.Text = "Beneficiary"
        Me.lblIsBeneficiary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign6
        '
        Me.objlblSign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign6.ForeColor = System.Drawing.Color.Red
        Me.objlblSign6.Location = New System.Drawing.Point(8, 180)
        Me.objlblSign6.Name = "objlblSign6"
        Me.objlblSign6.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign6.TabIndex = 72
        Me.objlblSign6.Text = "*"
        Me.objlblSign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboRELATION
        '
        Me.cboRELATION.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRELATION.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRELATION.FormattingEnabled = True
        Me.cboRELATION.Location = New System.Drawing.Point(126, 150)
        Me.cboRELATION.Name = "cboRELATION"
        Me.cboRELATION.Size = New System.Drawing.Size(143, 21)
        Me.cboRELATION.TabIndex = 7
        '
        'lblRelation
        '
        Me.lblRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRelation.Location = New System.Drawing.Point(24, 152)
        Me.lblRelation.Name = "lblRelation"
        Me.lblRelation.Size = New System.Drawing.Size(96, 17)
        Me.lblRelation.TabIndex = 6
        Me.lblRelation.Text = "Relation"
        Me.lblRelation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(247, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(284, 19)
        Me.lblCaption.TabIndex = 34
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAddress
        '
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(281, 97)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(90, 17)
        Me.lblAddress.TabIndex = 24
        Me.lblAddress.Text = "Address"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboADDRESS
        '
        Me.cboADDRESS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboADDRESS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboADDRESS.FormattingEnabled = True
        Me.cboADDRESS.Location = New System.Drawing.Point(377, 95)
        Me.cboADDRESS.Name = "cboADDRESS"
        Me.cboADDRESS.Size = New System.Drawing.Size(143, 21)
        Me.cboADDRESS.TabIndex = 25
        '
        'cboBIRTHDATE
        '
        Me.cboBIRTHDATE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBIRTHDATE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBIRTHDATE.FormattingEnabled = True
        Me.cboBIRTHDATE.Location = New System.Drawing.Point(126, 231)
        Me.cboBIRTHDATE.Name = "cboBIRTHDATE"
        Me.cboBIRTHDATE.Size = New System.Drawing.Size(143, 21)
        Me.cboBIRTHDATE.TabIndex = 11
        '
        'lblBirthDate
        '
        Me.lblBirthDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBirthDate.Location = New System.Drawing.Point(24, 233)
        Me.lblBirthDate.Name = "lblBirthDate"
        Me.lblBirthDate.Size = New System.Drawing.Size(96, 17)
        Me.lblBirthDate.TabIndex = 10
        Me.lblBirthDate.Text = "Birth Date"
        Me.lblBirthDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign5
        '
        Me.objlblSign5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign5.ForeColor = System.Drawing.Color.Red
        Me.objlblSign5.Location = New System.Drawing.Point(8, 153)
        Me.objlblSign5.Name = "objlblSign5"
        Me.objlblSign5.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign5.TabIndex = 12
        Me.objlblSign5.Text = "*"
        Me.objlblSign5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboEMPLOYEE_CODE
        '
        Me.cboEMPLOYEE_CODE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEMPLOYEE_CODE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEMPLOYEE_CODE.FormattingEnabled = True
        Me.cboEMPLOYEE_CODE.Location = New System.Drawing.Point(126, 40)
        Me.cboEMPLOYEE_CODE.Name = "cboEMPLOYEE_CODE"
        Me.cboEMPLOYEE_CODE.Size = New System.Drawing.Size(143, 21)
        Me.cboEMPLOYEE_CODE.TabIndex = 1
        '
        'cboDEPENDANT_LASTNAME
        '
        Me.cboDEPENDANT_LASTNAME.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDEPENDANT_LASTNAME.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDEPENDANT_LASTNAME.FormattingEnabled = True
        Me.cboDEPENDANT_LASTNAME.Location = New System.Drawing.Point(126, 123)
        Me.cboDEPENDANT_LASTNAME.Name = "cboDEPENDANT_LASTNAME"
        Me.cboDEPENDANT_LASTNAME.Size = New System.Drawing.Size(143, 21)
        Me.cboDEPENDANT_LASTNAME.TabIndex = 5
        '
        'lblDepFirstName
        '
        Me.lblDepFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepFirstName.Location = New System.Drawing.Point(24, 69)
        Me.lblDepFirstName.Name = "lblDepFirstName"
        Me.lblDepFirstName.Size = New System.Drawing.Size(96, 17)
        Me.lblDepFirstName.TabIndex = 2
        Me.lblDepFirstName.Text = "Dep. Firstname"
        Me.lblDepFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDEPENDANT_FIRSTNAME
        '
        Me.cboDEPENDANT_FIRSTNAME.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDEPENDANT_FIRSTNAME.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDEPENDANT_FIRSTNAME.FormattingEnabled = True
        Me.cboDEPENDANT_FIRSTNAME.Location = New System.Drawing.Point(126, 67)
        Me.cboDEPENDANT_FIRSTNAME.Name = "cboDEPENDANT_FIRSTNAME"
        Me.cboDEPENDANT_FIRSTNAME.Size = New System.Drawing.Size(143, 21)
        Me.cboDEPENDANT_FIRSTNAME.TabIndex = 3
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(24, 42)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(96, 17)
        Me.lblEmployeeCode.TabIndex = 0
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign4
        '
        Me.objlblSign4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign4.ForeColor = System.Drawing.Color.Red
        Me.objlblSign4.Location = New System.Drawing.Point(8, 126)
        Me.objlblSign4.Name = "objlblSign4"
        Me.objlblSign4.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign4.TabIndex = 9
        Me.objlblSign4.Text = "*"
        Me.objlblSign4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblDepLastName
        '
        Me.lblDepLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepLastName.Location = New System.Drawing.Point(24, 125)
        Me.lblDepLastName.Name = "lblDepLastName"
        Me.lblDepLastName.Size = New System.Drawing.Size(96, 17)
        Me.lblDepLastName.TabIndex = 4
        Me.lblDepLastName.Text = "Dep. Lastname"
        Me.lblDepLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(8, 42)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign1.TabIndex = 0
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblSign2
        '
        Me.objlblSign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign2.ForeColor = System.Drawing.Color.Red
        Me.objlblSign2.Location = New System.Drawing.Point(8, 69)
        Me.objlblSign2.Name = "objlblSign2"
        Me.objlblSign2.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign2.TabIndex = 3
        Me.objlblSign2.Text = "*"
        Me.objlblSign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboPincode
        '
        Me.cboPincode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPincode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPincode.FormattingEnabled = True
        Me.cboPincode.Location = New System.Drawing.Point(536, 219)
        Me.cboPincode.Name = "cboPincode"
        Me.cboPincode.Size = New System.Drawing.Size(143, 21)
        Me.cboPincode.TabIndex = 19
        Me.cboPincode.Visible = False
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(536, 275)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(143, 21)
        Me.cboCountry.TabIndex = 13
        Me.cboCountry.Visible = False
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(440, 277)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(90, 17)
        Me.lblCountry.TabIndex = 12
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCountry.Visible = False
        '
        'lblNationality
        '
        Me.lblNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNationality.Location = New System.Drawing.Point(440, 248)
        Me.lblNationality.Name = "lblNationality"
        Me.lblNationality.Size = New System.Drawing.Size(90, 17)
        Me.lblNationality.TabIndex = 30
        Me.lblNationality.Text = "Nationality"
        Me.lblNationality.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblNationality.Visible = False
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(285, 246)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(143, 21)
        Me.cboState.TabIndex = 15
        Me.cboState.Visible = False
        '
        'cboNationality
        '
        Me.cboNationality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNationality.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNationality.FormattingEnabled = True
        Me.cboNationality.Location = New System.Drawing.Point(536, 246)
        Me.cboNationality.Name = "cboNationality"
        Me.cboNationality.Size = New System.Drawing.Size(143, 21)
        Me.cboNationality.TabIndex = 31
        Me.cboNationality.Visible = False
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(183, 248)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(96, 17)
        Me.lblState.TabIndex = 14
        Me.lblState.Text = "State"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblState.Visible = False
        '
        'lblPincode
        '
        Me.lblPincode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPincode.Location = New System.Drawing.Point(440, 221)
        Me.lblPincode.Name = "lblPincode"
        Me.lblPincode.Size = New System.Drawing.Size(90, 17)
        Me.lblPincode.TabIndex = 18
        Me.lblPincode.Text = "Pincode"
        Me.lblPincode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblPincode.Visible = False
        '
        'cboCity
        '
        Me.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCity.FormattingEnabled = True
        Me.cboCity.Location = New System.Drawing.Point(285, 273)
        Me.cboCity.Name = "cboCity"
        Me.cboCity.Size = New System.Drawing.Size(143, 21)
        Me.cboCity.TabIndex = 17
        Me.cboCity.Visible = False
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(183, 275)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(96, 17)
        Me.lblCity.TabIndex = 16
        Me.lblCity.Text = "City"
        Me.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblCity.Visible = False
        '
        'WizPageSelectFile
        '
        Me.WizPageSelectFile.Controls.Add(Me.lblMessage2)
        Me.WizPageSelectFile.Controls.Add(Me.lblMessage)
        Me.WizPageSelectFile.Controls.Add(Me.lblTitle)
        Me.WizPageSelectFile.Controls.Add(Me.btnOpenFile)
        Me.WizPageSelectFile.Controls.Add(Me.txtFilePath)
        Me.WizPageSelectFile.Controls.Add(Me.lblSelectfile)
        Me.WizPageSelectFile.Location = New System.Drawing.Point(0, 0)
        Me.WizPageSelectFile.Name = "WizPageSelectFile"
        Me.WizPageSelectFile.Size = New System.Drawing.Size(698, 373)
        Me.WizPageSelectFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageSelectFile.TabIndex = 7
        '
        'lblMessage2
        '
        Me.lblMessage2.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage2.ForeColor = System.Drawing.Color.Maroon
        Me.lblMessage2.Location = New System.Drawing.Point(189, 311)
        Me.lblMessage2.Name = "lblMessage2"
        Me.lblMessage2.Size = New System.Drawing.Size(497, 51)
        Me.lblMessage2.TabIndex = 19
        Me.lblMessage2.Text = "Please set all date(s) columns in selected file as blank if not available."
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(189, 49)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(497, 34)
        Me.lblMessage.TabIndex = 18
        Me.lblMessage.Text = "This wizard will import Employee 'Dependants' records made from other system."
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(188, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(498, 23)
        Me.lblTitle.TabIndex = 17
        Me.lblTitle.Text = "Employee Dependants Import Wizard"
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(663, 202)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(23, 20)
        Me.btnOpenFile.TabIndex = 16
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(189, 202)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(468, 21)
        Me.txtFilePath.TabIndex = 15
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(189, 173)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(228, 20)
        Me.lblSelectfile.TabIndex = 14
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'WizPageImporting
        '
        Me.WizPageImporting.Controls.Add(Me.dgData)
        Me.WizPageImporting.Controls.Add(Me.btnFilter)
        Me.WizPageImporting.Controls.Add(Me.pnlInfo)
        Me.WizPageImporting.Location = New System.Drawing.Point(0, 0)
        Me.WizPageImporting.Name = "WizPageImporting"
        Me.WizPageImporting.Size = New System.Drawing.Size(698, 373)
        Me.WizPageImporting.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.WizPageImporting.TabIndex = 9
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.colhlogindate, Me.colhStatus, Me.colhMessage, Me.objcolhstatus, Me.objcolhDate})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(674, 265)
        Me.dgData.TabIndex = 19
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 30
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Dependant Name"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        Me.colhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'colhlogindate
        '
        Me.colhlogindate.HeaderText = "Date"
        Me.colhlogindate.Name = "colhlogindate"
        Me.colhlogindate.ReadOnly = True
        Me.colhlogindate.Visible = False
        Me.colhlogindate.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 175
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'objcolhDate
        '
        Me.objcolhDate.HeaderText = "objcolhDate"
        Me.objcolhDate.Name = "objcolhDate"
        Me.objcolhDate.ReadOnly = True
        Me.objcolhDate.Visible = False
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 340)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(107, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 21
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(674, 51)
        Me.pnlInfo.TabIndex = 2
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(552, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(438, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(552, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(484, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(597, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(438, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(597, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(484, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbuttonBack
        '
        Me.objbuttonBack.BackColor = System.Drawing.Color.White
        Me.objbuttonBack.BackgroundImage = CType(resources.GetObject("objbuttonBack.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonBack.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonBack.Enabled = False
        Me.objbuttonBack.FlatAppearance.BorderSize = 0
        Me.objbuttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonBack.ForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonBack.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Location = New System.Drawing.Point(501, 429)
        Me.objbuttonBack.Name = "objbuttonBack"
        Me.objbuttonBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonBack.TabIndex = 6
        Me.objbuttonBack.Text = "Back"
        Me.objbuttonBack.UseVisualStyleBackColor = False
        '
        'objbuttonCancel
        '
        Me.objbuttonCancel.BackColor = System.Drawing.Color.White
        Me.objbuttonCancel.BackgroundImage = CType(resources.GetObject("objbuttonCancel.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonCancel.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.objbuttonCancel.FlatAppearance.BorderSize = 0
        Me.objbuttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonCancel.ForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonCancel.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Location = New System.Drawing.Point(669, 429)
        Me.objbuttonCancel.Name = "objbuttonCancel"
        Me.objbuttonCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonCancel.TabIndex = 5
        Me.objbuttonCancel.Text = "Cancel"
        Me.objbuttonCancel.UseVisualStyleBackColor = False
        '
        'objbuttonNext
        '
        Me.objbuttonNext.BackColor = System.Drawing.Color.White
        Me.objbuttonNext.BackgroundImage = CType(resources.GetObject("objbuttonNext.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonNext.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonNext.FlatAppearance.BorderSize = 0
        Me.objbuttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonNext.ForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Location = New System.Drawing.Point(585, 429)
        Me.objbuttonNext.Name = "objbuttonNext"
        Me.objbuttonNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonNext.TabIndex = 4
        Me.objbuttonNext.Text = "Next"
        Me.objbuttonNext.UseVisualStyleBackColor = False
        '
        'objlblSign7
        '
        Me.objlblSign7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign7.ForeColor = System.Drawing.Color.Red
        Me.objlblSign7.Location = New System.Drawing.Point(8, 206)
        Me.objlblSign7.Name = "objlblSign7"
        Me.objlblSign7.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign7.TabIndex = 122
        Me.objlblSign7.Text = "*"
        Me.objlblSign7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'frmImportDependantWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(698, 421)
        Me.Controls.Add(Me.eZeeWizImportEmp)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportDependantWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Dependant Wizard"
        Me.eZeeWizImportEmp.ResumeLayout(False)
        Me.WizPageMapping.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.WizPageSelectFile.ResumeLayout(False)
        Me.WizPageSelectFile.PerformLayout()
        Me.WizPageImporting.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsFilter.ResumeLayout(False)
        Me.pnlInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeWizImportEmp As eZee.Common.eZeeWizard
    Friend WithEvents WizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents cboADDRESS As System.Windows.Forms.ComboBox
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents cboCity As System.Windows.Forms.ComboBox
    Friend WithEvents lblPincode As System.Windows.Forms.Label
    Friend WithEvents cboPincode As System.Windows.Forms.ComboBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents cboBIRTHDATE As System.Windows.Forms.ComboBox
    Friend WithEvents lblBirthDate As System.Windows.Forms.Label
    Friend WithEvents objlblSign5 As System.Windows.Forms.Label
    Friend WithEvents cboEMPLOYEE_CODE As System.Windows.Forms.ComboBox
    Friend WithEvents cboDEPENDANT_LASTNAME As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepFirstName As System.Windows.Forms.Label
    Friend WithEvents cboDEPENDANT_FIRSTNAME As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents objlblSign4 As System.Windows.Forms.Label
    Friend WithEvents lblDepLastName As System.Windows.Forms.Label
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents objlblSign2 As System.Windows.Forms.Label
    Friend WithEvents WizPageSelectFile As eZee.Common.eZeeWizardPage
    Friend WithEvents lblMessage2 As System.Windows.Forms.Label
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents WizPageImporting As eZee.Common.eZeeWizardPage
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents objbuttonBack As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonNext As eZee.Common.eZeeLightButton
    Friend WithEvents cboRELATION As System.Windows.Forms.ComboBox
    Friend WithEvents lblRelation As System.Windows.Forms.Label
    Friend WithEvents cboISBENEFICIARY As System.Windows.Forms.ComboBox
    Friend WithEvents lblIsBeneficiary As System.Windows.Forms.Label
    Friend WithEvents objlblSign6 As System.Windows.Forms.Label
    Friend WithEvents lblTelephone As System.Windows.Forms.Label
    Friend WithEvents cboTELEPHONE As System.Windows.Forms.ComboBox
    Friend WithEvents lblSurname As System.Windows.Forms.Label
    Friend WithEvents cboEMPLOYEE_SURNAME As System.Windows.Forms.ComboBox
    Friend WithEvents lblFirsName As System.Windows.Forms.Label
    Friend WithEvents cboEMPLOYEE_FIRSTNAME As System.Windows.Forms.ComboBox
    Friend WithEvents lblIdNo As System.Windows.Forms.Label
    Friend WithEvents cboID_NO As System.Windows.Forms.ComboBox
    Friend WithEvents lblNationality As System.Windows.Forms.Label
    Friend WithEvents cboNationality As System.Windows.Forms.ComboBox
    Friend WithEvents lblMobile As System.Windows.Forms.Label
    Friend WithEvents cboMOBILENO As System.Windows.Forms.ComboBox
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhlogindate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents cboGENDER As System.Windows.Forms.ComboBox
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
    Friend WithEvents cboDEPENDANT_MIDDLENAME As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepMiddleName As System.Windows.Forms.Label
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents cboEffective_Date As System.Windows.Forms.ComboBox
    Friend WithEvents objlblSign7 As System.Windows.Forms.Label
End Class
