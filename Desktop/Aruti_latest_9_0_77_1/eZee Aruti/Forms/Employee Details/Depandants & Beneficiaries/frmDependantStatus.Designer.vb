﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDependantStatus
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDependantStatus))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblDependant = New System.Windows.Forms.Label
        Me.cboDependant = New System.Windows.Forms.ComboBox
        Me.dtBirthdate = New System.Windows.Forms.DateTimePicker
        Me.lblBirthdate = New System.Windows.Forms.Label
        Me.lblGender = New System.Windows.Forms.Label
        Me.cboGender = New System.Windows.Forms.ComboBox
        Me.lnkAllocations = New System.Windows.Forms.LinkLabel
        Me.txtDBPassportNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblDBIdNo = New System.Windows.Forms.Label
        Me.cboDBRelation = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblDBRelation = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objeZeeFooter = New eZee.Common.eZeeFooter
        Me.btnSetActive = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSetInactive = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgvDependantStatus = New System.Windows.Forms.DataGridView
        Me.objdgcolhicheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objColDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgColhBlank = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDependant = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRelation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEffectiveDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhActive = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhDependandstatustranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhDpndtbeneficetranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhROWNO = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.cboReason = New System.Windows.Forms.ComboBox
        Me.lblReason = New System.Windows.Forms.Label
        Me.objbtnAddReason = New eZee.Common.eZeeGradientButton
        Me.objSearchReason = New eZee.Common.eZeeGradientButton
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objeZeeFooter.SuspendLayout()
        CType(Me.dgvDependantStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblDependant)
        Me.gbFilterCriteria.Controls.Add(Me.cboDependant)
        Me.gbFilterCriteria.Controls.Add(Me.dtBirthdate)
        Me.gbFilterCriteria.Controls.Add(Me.lblBirthdate)
        Me.gbFilterCriteria.Controls.Add(Me.lblGender)
        Me.gbFilterCriteria.Controls.Add(Me.cboGender)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocations)
        Me.gbFilterCriteria.Controls.Add(Me.txtDBPassportNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblDBIdNo)
        Me.gbFilterCriteria.Controls.Add(Me.cboDBRelation)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblDBRelation)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 12)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(845, 89)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(652, 33)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(73, 15)
        Me.lblStatus.TabIndex = 24
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 250
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(731, 30)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(99, 21)
        Me.cboStatus.TabIndex = 25
        '
        'lblDependant
        '
        Me.lblDependant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDependant.Location = New System.Drawing.Point(8, 63)
        Me.lblDependant.Name = "lblDependant"
        Me.lblDependant.Size = New System.Drawing.Size(69, 15)
        Me.lblDependant.TabIndex = 21
        Me.lblDependant.Text = "Dependant"
        '
        'cboDependant
        '
        Me.cboDependant.DropDownWidth = 300
        Me.cboDependant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDependant.FormattingEnabled = True
        Me.cboDependant.Location = New System.Drawing.Point(83, 61)
        Me.cboDependant.Name = "cboDependant"
        Me.cboDependant.Size = New System.Drawing.Size(138, 21)
        Me.cboDependant.TabIndex = 22
        '
        'dtBirthdate
        '
        Me.dtBirthdate.Checked = False
        Me.dtBirthdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtBirthdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtBirthdate.Location = New System.Drawing.Point(521, 33)
        Me.dtBirthdate.Name = "dtBirthdate"
        Me.dtBirthdate.ShowCheckBox = True
        Me.dtBirthdate.Size = New System.Drawing.Size(121, 21)
        Me.dtBirthdate.TabIndex = 10
        '
        'lblBirthdate
        '
        Me.lblBirthdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBirthdate.Location = New System.Drawing.Point(459, 37)
        Me.lblBirthdate.Name = "lblBirthdate"
        Me.lblBirthdate.Size = New System.Drawing.Size(56, 13)
        Me.lblBirthdate.TabIndex = 9
        Me.lblBirthdate.Text = "Birth Date"
        '
        'lblGender
        '
        Me.lblGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGender.Location = New System.Drawing.Point(238, 36)
        Me.lblGender.Name = "lblGender"
        Me.lblGender.Size = New System.Drawing.Size(73, 15)
        Me.lblGender.TabIndex = 5
        Me.lblGender.Text = "Gender"
        Me.lblGender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGender
        '
        Me.cboGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGender.DropDownWidth = 250
        Me.cboGender.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGender.FormattingEnabled = True
        Me.cboGender.Location = New System.Drawing.Point(317, 33)
        Me.cboGender.Name = "cboGender"
        Me.cboGender.Size = New System.Drawing.Size(128, 21)
        Me.cboGender.TabIndex = 13
        '
        'lnkAllocations
        '
        Me.lnkAllocations.AutoSize = True
        Me.lnkAllocations.Location = New System.Drawing.Point(709, 7)
        Me.lnkAllocations.Name = "lnkAllocations"
        Me.lnkAllocations.Size = New System.Drawing.Size(69, 13)
        Me.lnkAllocations.TabIndex = 19
        Me.lnkAllocations.TabStop = True
        Me.lnkAllocations.Text = "Allocations"
        '
        'txtDBPassportNo
        '
        Me.txtDBPassportNo.Flags = 0
        Me.txtDBPassportNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBPassportNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDBPassportNo.Location = New System.Drawing.Point(521, 60)
        Me.txtDBPassportNo.Name = "txtDBPassportNo"
        Me.txtDBPassportNo.Size = New System.Drawing.Size(121, 21)
        Me.txtDBPassportNo.TabIndex = 17
        '
        'lblDBIdNo
        '
        Me.lblDBIdNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBIdNo.Location = New System.Drawing.Point(459, 62)
        Me.lblDBIdNo.Name = "lblDBIdNo"
        Me.lblDBIdNo.Size = New System.Drawing.Size(56, 13)
        Me.lblDBIdNo.TabIndex = 16
        Me.lblDBIdNo.Text = "ID. No"
        Me.lblDBIdNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDBRelation
        '
        Me.cboDBRelation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDBRelation.DropDownWidth = 300
        Me.cboDBRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDBRelation.FormattingEnabled = True
        Me.cboDBRelation.Location = New System.Drawing.Point(317, 60)
        Me.cboDBRelation.Name = "cboDBRelation"
        Me.cboDBRelation.Size = New System.Drawing.Size(128, 21)
        Me.cboDBRelation.TabIndex = 12
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(69, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        '
        'lblDBRelation
        '
        Me.lblDBRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBRelation.Location = New System.Drawing.Point(241, 63)
        Me.lblDBRelation.Name = "lblDBRelation"
        Me.lblDBRelation.Size = New System.Drawing.Size(70, 15)
        Me.lblDBRelation.TabIndex = 11
        Me.lblDBRelation.Text = "Relation"
        Me.lblDBRelation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(83, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(138, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(818, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(795, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'objeZeeFooter
        '
        Me.objeZeeFooter.BorderColor = System.Drawing.Color.Silver
        Me.objeZeeFooter.Controls.Add(Me.btnSetActive)
        Me.objeZeeFooter.Controls.Add(Me.btnSetInactive)
        Me.objeZeeFooter.Controls.Add(Me.btnClose)
        Me.objeZeeFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objeZeeFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objeZeeFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objeZeeFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objeZeeFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objeZeeFooter.Location = New System.Drawing.Point(0, 402)
        Me.objeZeeFooter.Name = "objeZeeFooter"
        Me.objeZeeFooter.Size = New System.Drawing.Size(869, 55)
        Me.objeZeeFooter.TabIndex = 122
        '
        'btnSetActive
        '
        Me.btnSetActive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSetActive.BackColor = System.Drawing.Color.White
        Me.btnSetActive.BackgroundImage = CType(resources.GetObject("btnSetActive.BackgroundImage"), System.Drawing.Image)
        Me.btnSetActive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSetActive.BorderColor = System.Drawing.Color.Empty
        Me.btnSetActive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSetActive.FlatAppearance.BorderSize = 0
        Me.btnSetActive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetActive.ForeColor = System.Drawing.Color.Black
        Me.btnSetActive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSetActive.GradientForeColor = System.Drawing.Color.Black
        Me.btnSetActive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSetActive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSetActive.Location = New System.Drawing.Point(658, 13)
        Me.btnSetActive.Name = "btnSetActive"
        Me.btnSetActive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSetActive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSetActive.Size = New System.Drawing.Size(97, 30)
        Me.btnSetActive.TabIndex = 120
        Me.btnSetActive.Text = "Set &Active"
        Me.btnSetActive.UseVisualStyleBackColor = True
        '
        'btnSetInactive
        '
        Me.btnSetInactive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSetInactive.BackColor = System.Drawing.Color.White
        Me.btnSetInactive.BackgroundImage = CType(resources.GetObject("btnSetInactive.BackgroundImage"), System.Drawing.Image)
        Me.btnSetInactive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSetInactive.BorderColor = System.Drawing.Color.Empty
        Me.btnSetInactive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSetInactive.FlatAppearance.BorderSize = 0
        Me.btnSetInactive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSetInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSetInactive.ForeColor = System.Drawing.Color.Black
        Me.btnSetInactive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSetInactive.GradientForeColor = System.Drawing.Color.Black
        Me.btnSetInactive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSetInactive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSetInactive.Location = New System.Drawing.Point(658, 13)
        Me.btnSetInactive.Name = "btnSetInactive"
        Me.btnSetInactive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSetInactive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSetInactive.Size = New System.Drawing.Size(97, 30)
        Me.btnSetInactive.TabIndex = 119
        Me.btnSetInactive.Text = "Set &Inactive"
        Me.btnSetInactive.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(761, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 117
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgvDependantStatus
        '
        Me.dgvDependantStatus.AllowUserToAddRows = False
        Me.dgvDependantStatus.AllowUserToDeleteRows = False
        Me.dgvDependantStatus.AllowUserToResizeRows = False
        Me.dgvDependantStatus.BackgroundColor = System.Drawing.Color.White
        Me.dgvDependantStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvDependantStatus.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhicheck, Me.objColDelete, Me.objdgColhBlank, Me.dgcolhEmployee, Me.dgcolhDependant, Me.dgcolhRelation, Me.dgcolhGender, Me.dgcolhBirthDate, Me.dgcolhEffectiveDate, Me.dgcolhActive, Me.dgcolhReason, Me.objdgcolhIsGroup, Me.objdgcolhDependandstatustranunkid, Me.objdgcolhEmpid, Me.objdgcolhDpndtbeneficetranunkid, Me.objdgcolhROWNO})
        Me.dgvDependantStatus.Location = New System.Drawing.Point(12, 107)
        Me.dgvDependantStatus.Name = "dgvDependantStatus"
        Me.dgvDependantStatus.RowHeadersVisible = False
        Me.dgvDependantStatus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDependantStatus.Size = New System.Drawing.Size(845, 257)
        Me.dgvDependantStatus.TabIndex = 123
        '
        'objdgcolhicheck
        '
        Me.objdgcolhicheck.HeaderText = ""
        Me.objdgcolhicheck.Name = "objdgcolhicheck"
        Me.objdgcolhicheck.Width = 30
        '
        'objColDelete
        '
        Me.objColDelete.HeaderText = ""
        Me.objColDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objColDelete.Name = "objColDelete"
        Me.objColDelete.ReadOnly = True
        Me.objColDelete.Width = 25
        '
        'objdgColhBlank
        '
        Me.objdgColhBlank.HeaderText = ""
        Me.objdgColhBlank.Name = "objdgColhBlank"
        Me.objdgColhBlank.ReadOnly = True
        Me.objdgColhBlank.Width = 30
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.Width = 50
        '
        'dgcolhDependant
        '
        Me.dgcolhDependant.HeaderText = "Dependant"
        Me.dgcolhDependant.Name = "dgcolhDependant"
        Me.dgcolhDependant.ReadOnly = True
        Me.dgcolhDependant.Width = 210
        '
        'dgcolhRelation
        '
        Me.dgcolhRelation.HeaderText = "Relation"
        Me.dgcolhRelation.Name = "dgcolhRelation"
        Me.dgcolhRelation.ReadOnly = True
        '
        'dgcolhGender
        '
        Me.dgcolhGender.HeaderText = "Gender"
        Me.dgcolhGender.Name = "dgcolhGender"
        Me.dgcolhGender.ReadOnly = True
        Me.dgcolhGender.Width = 60
        '
        'dgcolhBirthDate
        '
        Me.dgcolhBirthDate.HeaderText = "Birth Date"
        Me.dgcolhBirthDate.Name = "dgcolhBirthDate"
        Me.dgcolhBirthDate.ReadOnly = True
        Me.dgcolhBirthDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhBirthDate.Width = 80
        '
        'dgcolhEffectiveDate
        '
        Me.dgcolhEffectiveDate.HeaderText = "Effective Date"
        Me.dgcolhEffectiveDate.Name = "dgcolhEffectiveDate"
        Me.dgcolhEffectiveDate.ReadOnly = True
        Me.dgcolhEffectiveDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEffectiveDate.Width = 80
        '
        'dgcolhActive
        '
        Me.dgcolhActive.HeaderText = "Active"
        Me.dgcolhActive.Name = "dgcolhActive"
        Me.dgcolhActive.ReadOnly = True
        Me.dgcolhActive.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhActive.Width = 60
        '
        'dgcolhReason
        '
        Me.dgcolhReason.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhReason.HeaderText = "Reason"
        Me.dgcolhReason.Name = "dgcolhReason"
        Me.dgcolhReason.ReadOnly = True
        Me.dgcolhReason.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhReason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhIsGroup
        '
        Me.objdgcolhIsGroup.HeaderText = "IsGroup"
        Me.objdgcolhIsGroup.Name = "objdgcolhIsGroup"
        Me.objdgcolhIsGroup.ReadOnly = True
        Me.objdgcolhIsGroup.Visible = False
        '
        'objdgcolhDependandstatustranunkid
        '
        Me.objdgcolhDependandstatustranunkid.HeaderText = "Dependandstatustranunkid"
        Me.objdgcolhDependandstatustranunkid.Name = "objdgcolhDependandstatustranunkid"
        Me.objdgcolhDependandstatustranunkid.ReadOnly = True
        Me.objdgcolhDependandstatustranunkid.Visible = False
        '
        'objdgcolhEmpid
        '
        Me.objdgcolhEmpid.HeaderText = "objdgcolhEmpid"
        Me.objdgcolhEmpid.Name = "objdgcolhEmpid"
        Me.objdgcolhEmpid.ReadOnly = True
        Me.objdgcolhEmpid.Visible = False
        '
        'objdgcolhDpndtbeneficetranunkid
        '
        Me.objdgcolhDpndtbeneficetranunkid.HeaderText = "dpndtbeneficetranunkid"
        Me.objdgcolhDpndtbeneficetranunkid.Name = "objdgcolhDpndtbeneficetranunkid"
        Me.objdgcolhDpndtbeneficetranunkid.ReadOnly = True
        Me.objdgcolhDpndtbeneficetranunkid.Visible = False
        '
        'objdgcolhROWNO
        '
        Me.objdgcolhROWNO.HeaderText = "objdgcolhROWNO"
        Me.objdgcolhROWNO.Name = "objdgcolhROWNO"
        Me.objdgcolhROWNO.ReadOnly = True
        Me.objdgcolhROWNO.Visible = False
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(20, 114)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 124
        Me.objchkAll.UseVisualStyleBackColor = True
        '        
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.Checked = False
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(95, 370)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.ShowCheckBox = True
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(121, 21)
        Me.dtpEffectiveDate.TabIndex = 126
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(12, 374)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(77, 13)
        Me.lblEffectiveDate.TabIndex = 125
        Me.lblEffectiveDate.Text = "Effective Date"
        '
        'cboReason
        '
        Me.cboReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReason.DropDownWidth = 300
        Me.cboReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReason.FormattingEnabled = True
        Me.cboReason.Location = New System.Drawing.Point(329, 370)
        Me.cboReason.Name = "cboReason"
        Me.cboReason.Size = New System.Drawing.Size(128, 21)
        Me.cboReason.TabIndex = 128
        '
        'lblReason
        '
        Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReason.Location = New System.Drawing.Point(253, 373)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(70, 15)
        Me.lblReason.TabIndex = 127
        Me.lblReason.Text = "Reason"
        Me.lblReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddReason
        '
        Me.objbtnAddReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReason.BorderSelected = False
        Me.objbtnAddReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReason.Location = New System.Drawing.Point(463, 370)
        Me.objbtnAddReason.Name = "objbtnAddReason"
        Me.objbtnAddReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReason.TabIndex = 208
        '
        'objSearchReason
        '
        Me.objSearchReason.BackColor = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchReason.BorderSelected = False
        Me.objSearchReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchReason.Location = New System.Drawing.Point(490, 370)
        Me.objSearchReason.Name = "objSearchReason"
        Me.objSearchReason.Size = New System.Drawing.Size(21, 21)
        Me.objSearchReason.TabIndex = 209
        '
        'frmDependantStatus
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(869, 457)
        Me.Controls.Add(Me.objSearchReason)
        Me.Controls.Add(Me.objbtnAddReason)
        Me.Controls.Add(Me.cboReason)
        Me.Controls.Add(Me.lblReason)
        Me.Controls.Add(Me.dtpEffectiveDate)
        Me.Controls.Add(Me.lblEffectiveDate)
        Me.Controls.Add(Me.objchkAll)
        Me.Controls.Add(Me.dgvDependantStatus)
        Me.Controls.Add(Me.objeZeeFooter)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDependantStatus"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Dependant Status"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objeZeeFooter.ResumeLayout(False)
        CType(Me.dgvDependantStatus, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblDependant As System.Windows.Forms.Label
    Friend WithEvents cboDependant As System.Windows.Forms.ComboBox
    Friend WithEvents dtBirthdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblBirthdate As System.Windows.Forms.Label
    Friend WithEvents lblGender As System.Windows.Forms.Label
    Friend WithEvents cboGender As System.Windows.Forms.ComboBox
    Friend WithEvents lnkAllocations As System.Windows.Forms.LinkLabel
    Friend WithEvents txtDBPassportNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDBIdNo As System.Windows.Forms.Label
    Friend WithEvents cboDBRelation As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblDBRelation As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents objeZeeFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSetActive As eZee.Common.eZeeLightButton
    Friend WithEvents btnSetInactive As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dgvDependantStatus As System.Windows.Forms.DataGridView
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents cboReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents objbtnAddReason As eZee.Common.eZeeGradientButton
    Friend WithEvents objSearchReason As eZee.Common.eZeeGradientButton
    Friend WithEvents objdgcolhicheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objColDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgColhBlank As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDependant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRelation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEffectiveDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhActive As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhDependandstatustranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhDpndtbeneficetranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhROWNO As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
