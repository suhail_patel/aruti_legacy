﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System.IO

Public Class frmDependantsAndBeneficiaries

#Region " Private Variables "
    Dim mblnIsBebeficiaries As Boolean = False
    Private ReadOnly mstrModuleName As String = "frmDependantsAndBeneficiaries"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objDependants_Benefice As clsDependants_Beneficiary_tran
    Private mintDependants_Benefice_Unkid As Integer = -1
    Private mintSelectedEmployee As Integer = -1
    'Sandeep [ 21 Aug 2010 ] -- Start
    Private mdtMemTran As DataTable
    Private objDependantMemberTran As clsDependants_Membership_tran
    Private intMSelectedIdx As Integer = -1
    Private mdtBenefitTran As DataTable
    Private objDependantBenefitTran As clsDependants_Benefit_tran
    Private intBSelectedIdx As Integer = -1
    Private mintSeletedValue As Integer = 0
    'Sandeep [ 21 Aug 2010 ] -- End 

    'Sandeep [ 17 DEC 2010 ] -- Start
    Dim dblTotalPercentage As Double = 0
    Dim intTotalBeneficiary As Integer = -1
    Dim dblNewPercentage As Double = 0

    'Anjan (11 May 2011)-Start

    'Dim dblAddedPercent As Double = 0
    Dim dblAddedPercent As Decimal = 0
    'Anjan (11 May 2011)-End 


    'Sandeep [ 17 DEC 2010 ] -- End

    'S.SANDEEP [ 14 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 14 AUG 2012 ] -- END


    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim dicNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 18 SEP 2012 ] -- END

    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Private mdtDependants As DataTable = Nothing
    Private objDocument As New clsScan_Attach_Documents
    'SHANI (20 JUN 2015) -- End 

    'SHANI (16 JUL 2015) -- Start
    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    'Upload Image folder to access those attachemts from Server and 
    'all client machines if ArutiSelfService is installed otherwise save then on Document path
    Private mstrFolderName As String = ""
    'SHANI (16 JUL 2015) -- End 


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objADependant As clsDependant_beneficiaries_approval_tran

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim DependantApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmDependantsAndBeneficiariesList)))
    Private mstrTranGuid As String = String.Empty

    Private mintdbimageid As Integer = -1
    Dim objDependantImg As New clsdependant_Images
    'Gajanan [22-Feb-2019] -- End

    'Gajanan [9-April-2019] -- Start
    Dim isEmployeeApprove As Boolean = False
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End



    'Gajanan [21-June-2019] -- Start      
    Private OldData As clsDependants_Beneficiary_tran
    'Gajanan [21-June-2019] -- End
#End Region

#Region " Properties "
    Public WriteOnly Property _IsBeneficiar() As Boolean
        Set(ByVal value As Boolean)
            mblnIsBebeficiaries = value
        End Set
    End Property
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmployeeUnkid As Integer = -1) As Boolean
        'Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        'S.SANDEEP [ 14 AUG 2012 ] -- START -- END
        Try
            mintDependants_Benefice_Unkid = intUnkId
            menAction = eAction
            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mintEmployeeUnkid = intEmployeeUnkid
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            Me.ShowDialog()

            intUnkId = mintDependants_Benefice_Unkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtAddressLine1.BackColor = GUI.ColorOptional
            txtBenefitAmount.BackColor = GUI.ColorComp
            txtBenefitPercent.BackColor = GUI.ColorComp
            txtDBFirstName.BackColor = GUI.ColorComp
            txtDBIdNo.BackColor = GUI.ColorOptional
            txtDBLastName.BackColor = GUI.ColorComp
            txtDBMiddleName.BackColor = GUI.ColorComp
            txtDBPersonalNo.BackColor = GUI.ColorOptional
            txtDBPostBox.BackColor = GUI.ColorOptional
            txtDBTelNo.BackColor = GUI.ColorOptional
            txtEmail.BackColor = GUI.ColorOptional
            txtMembershipNo.BackColor = GUI.ColorOptional
            cboBenefitGroup.BackColor = GUI.ColorComp
            cboBenefitType.BackColor = GUI.ColorComp
            cboDBNationality.BackColor = GUI.ColorOptional
            cboDBPostCountry.BackColor = GUI.ColorOptional
            cboDBPostTown.BackColor = GUI.ColorOptional
            cboDBRelation.BackColor = GUI.ColorOptional
            cboDBRelation.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            cboMedicalNo.BackColor = GUI.ColorOptional
            cboPostCode.BackColor = GUI.ColorOptional
            cboPostState.BackColor = GUI.ColorOptional
            cboGender.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtAddressLine1.Text = objDependants_Benefice._Address
            'txtBenefitAmount.Decimal = objDependants_Benefice._Benefit_Amount
            'txtBenefitPercent.Decimal = objDependants_Benefice._Benefit_Percent
            'cboBenefitGroup.SelectedValue = objDependants_Benefice._Benefitgroupunkid
            'cboBenefitType.SelectedValue = objDependants_Benefice._Benefitplanunkid
            If objDependants_Benefice._Birthdate <> Nothing Then
                dtpDBBirthDate.Value = objDependants_Benefice._Birthdate
            Else
                dtpDBBirthDate.Checked = False
            End If
            cboDBPostTown.SelectedValue = objDependants_Benefice._Cityunkid
            cboDBPostCountry.SelectedValue = objDependants_Benefice._Countryunkid
            txtEmail.Text = objDependants_Benefice._Email
            cboEmployee.SelectedValue = objDependants_Benefice._Employeeunkid
            txtDBFirstName.Text = objDependants_Benefice._First_Name
            txtDBIdNo.Text = objDependants_Benefice._Identify_No
            objDependants_Benefice._Isdependant = objDependants_Benefice._Isdependant
            txtDBLastName.Text = objDependants_Benefice._Last_Name
            'txtMembershipNo.Text = objDependants_Benefice._Membership_No
            'cboMedicalNo.SelectedValue = objDependants_Benefice._Membershipunkid
            txtDBMiddleName.Text = objDependants_Benefice._Middle_Name
            txtDBPersonalNo.Text = objDependants_Benefice._Mobile_No
            cboDBNationality.SelectedValue = objDependants_Benefice._Nationalityunkid
            txtDBPostBox.Text = objDependants_Benefice._Post_Box
            cboDBRelation.SelectedValue = objDependants_Benefice._Relationunkid
            cboPostState.SelectedValue = objDependants_Benefice._Stateunkid
            txtDBTelNo.Text = objDependants_Benefice._Telephone_No
            cboPostCode.SelectedValue = objDependants_Benefice._Zipcodeunkid
            chkBeneficiaries.Checked = CBool(objDependants_Benefice._Isdependant)
            cboGender.SelectedValue = objDependants_Benefice._Gender
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If objDependants_Benefice._Effective_date <> Nothing Then
                dtpEffectiveDate.Value = objDependants_Benefice._Effective_date
            Else
                dtpEffectiveDate.Checked = False
            End If
            'Sohail (18 May 2019) -- End


            'Pinkal (27-Mar-2013) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsImgInDataBase Then
                imgDependants._Image = Nothing
                If objDependants_Benefice._Photo IsNot Nothing Then
                    Dim ms As New System.IO.MemoryStream(objDependants_Benefice._Photo, 0, objDependants_Benefice._Photo.Length)
                    imgDependants._Image = Image.FromStream(ms)
                End If
            Else
                'imgDependants._FileName = imgDependants._FileName & "\" & objDependants_Benefice._ImagePath

                'S.SANDEEP [27-Mar-2018] -- START
                'ISSUE/ENHANCEMENT : {Internal_Finding}
                imgDependants._FileName = objDependants_Benefice._ImagePath
                'S.SANDEEP [27-Mar-2018] -- END

                If objDependants_Benefice._ImagePath <> "" Then
                    'S.SANDEEP [27-Mar-2018] -- START
                    'ISSUE/ENHANCEMENT : {Internal_Finding}
                    'imgDependants._Image = Image.FromFile(ConfigParameter._Object._PhotoPath & "\" & objDependants_Benefice._ImagePath)
                    If System.IO.File.Exists(ConfigParameter._Object._PhotoPath & "\" & objDependants_Benefice._ImagePath) Then
                    imgDependants._Image = Image.FromFile(ConfigParameter._Object._PhotoPath & "\" & objDependants_Benefice._ImagePath)
                End If
                    'S.SANDEEP [27-Mar-2018] -- END
                End If



            End If

            'Pinkal (27-Mar-2013) -- End

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mdtDependants = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DEPENDANTS, mintDependants_Benefice_Unkid)

            'S.SANDEEP |02-MAR-2022| -- START
            'mdtDependants = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DEPENDANTS, mintDependants_Benefice_Unkid, ConfigParameter._Object._Document_Path)
            mdtDependants = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DEPENDANTS, mintDependants_Benefice_Unkid, ConfigParameter._Object._Document_Path, 1, IIf(mintDependants_Benefice_Unkid <= 0, "0", "").ToString())
            'S.SANDEEP |02-MAR-2022| -- END


            'Shani(24-Aug-2015) -- End


            'S.SANDEEP [17-OCT-2017] -- START
            'ISSUE/ENHANCEMENT : ALL ATTACHEMENT COMING IN THE LIST IF EMPLOYEE IS NOT SELECTED IN ADD MODE
            If menAction <> enAction.EDIT_ONE Then
                If mdtDependants IsNot Nothing AndAlso mdtDependants.Rows.Count > 0 Then
                    mdtDependants.Rows.Clear()
                End If
            End If
            'S.SANDEEP [17-OCT-2017] -- END



            'SHANI (20 JUN 2015) -- End 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'objDependants_Benefice._Address = txtAddressLine1.Text
            ''objDependants_Benefice._Benefit_Amount = txtBenefitAmount.Decimal
            ''objDependants_Benefice._Benefit_Percent = txtBenefitPercent.Decimal
            ''objDependants_Benefice._Benefitgroupunkid = CInt(cboBenefitGroup.SelectedValue)
            ''objDependants_Benefice._Benefitplanunkid = CInt(cboBenefitType.SelectedValue)
            'If ConfigParameter._Object._IsDependant_AgeLimit_Set = True Then
            '    objDependants_Benefice._Birthdate = dtpDBBirthDate.Value
            'Else
            '    If dtpDBBirthDate.Checked Then
            '        objDependants_Benefice._Birthdate = dtpDBBirthDate.Value
            '    Else
            '        objDependants_Benefice._Birthdate = Nothing
            '    End If
            'End If
            'objDependants_Benefice._Cityunkid = CInt(cboDBPostTown.SelectedValue)
            'objDependants_Benefice._Countryunkid = CInt(cboDBPostCountry.SelectedValue)
            'objDependants_Benefice._Email = txtEmail.Text
            'objDependants_Benefice._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'objDependants_Benefice._First_Name = txtDBFirstName.Text
            'objDependants_Benefice._Identify_No = txtDBIdNo.Text

            ''Pinkal (24-Jun-2011) -- Start
            ''ISSUE : CHECK FOR ACTIVE EMPLOYEE
            'objDependants_Benefice._Isdependant = chkBeneficiaries.Checked
            ''objDependants_Benefice._Isdependant = Not chkBeneficiaries.Checked
            ''Pinkal (24-Jun-2011) -- End

            'objDependants_Benefice._Last_Name = txtDBLastName.Text
            ''objDependants_Benefice._Membership_No = txtMembershipNo.Text
            ''objDependants_Benefice._Membershipunkid = CInt(cboMedicalNo.SelectedValue)
            'objDependants_Benefice._Middle_Name = txtDBMiddleName.Text
            'objDependants_Benefice._Mobile_No = txtDBPersonalNo.Text
            'objDependants_Benefice._Nationalityunkid = CInt(cboDBNationality.SelectedValue)
            'objDependants_Benefice._Post_Box = txtDBPostBox.Text
            'objDependants_Benefice._Relationunkid = CInt(cboDBRelation.SelectedValue)
            'objDependants_Benefice._Stateunkid = CInt(cboPostState.SelectedValue)
            'objDependants_Benefice._Telephone_No = txtDBTelNo.Text
            'If mintDependants_Benefice_Unkid = -1 Then
            '    objDependants_Benefice._Isvoid = False
            '    objDependants_Benefice._Userunkid = 1
            '    objDependants_Benefice._Voiddatetime = Nothing
            '    objDependants_Benefice._Voidreason = ""
            'Else
            '    objDependants_Benefice._Isvoid = False
            '    objDependants_Benefice._Userunkid = 1
            '    objDependants_Benefice._Voiddatetime = Nothing
            '    objDependants_Benefice._Voidreason = ""
            'End If
            'objDependants_Benefice._Zipcodeunkid = CInt(cboPostCode.SelectedValue)
            'objDependants_Benefice._Gender = CInt(cboGender.SelectedValue)


            ''Pinkal (27-Mar-2013) -- Start
            ''Enhancement : TRA Changes

            'objDependants_Benefice._CompanyId = ConfigParameter._Object._Companyunkid
            'objDependants_Benefice._blnImgInDb = ConfigParameter._Object._IsImgInDataBase

            'If ConfigParameter._Object._IsImgInDataBase Then


            '    'Pinkal (01-Apr-2013) -- Start
            '    'Enhancement : TRA Changes

            '    'If imgDependants._Image IsNot Nothing Then
            '    '    If System.IO.File.Exists((imgDependants._FilePath & "\" & imgDependants._FileName)) Then
            '    '        objDependants_Benefice._Photo = ImageCompression(imgDependants._FilePath & "\" & imgDependants._FileName)
            '    '    End If
            '    'Else
            '    '    objDependants_Benefice._Photo = Nothing
            '    'End If

            '    If imgDependants._Image IsNot Nothing Then
            '        If System.IO.File.Exists((imgDependants._OriginalFilePath)) Then
            '            objDependants_Benefice._Photo = ImageCompression(imgDependants._OriginalFilePath)
            '        End If
            '    Else
            '        objDependants_Benefice._Photo = Nothing
            '    End If

            '    'Pinkal (01-Apr-2013) -- End

            'Else

            '    'Pinkal (01-Apr-2013) -- Start
            '    'Enhancement : TRA Changes

            '    objDependants_Benefice._ImagePath = imgDependants._FileName
            '    imgDependants.SaveImage()

            '    'Pinkal (01-Apr-2013) -- End

            'End If

            ''Pinkal (27-Mar-2013) -- End




            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'If DependantApprovalFlowVal Is Nothing Then
            If DependantApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then

                'Gajanan [17-April-2019] -- End

                objADependant._Audittype = enAuditType.ADD
                objADependant._Audituserunkid = User._Object._Userunkid

                objADependant._Address = txtAddressLine1.Text
                If ConfigParameter._Object._IsDependant_AgeLimit_Set = True Then
                    objADependant._Birthdate = dtpDBBirthDate.Value
                Else
                    If dtpDBBirthDate.Checked Then
                        objADependant._Birthdate = dtpDBBirthDate.Value
                    Else
                        objADependant._Birthdate = Nothing
                    End If
                End If
                objADependant._Cityunkid = CInt(cboDBPostTown.SelectedValue)
                objADependant._Countryunkid = CInt(cboDBPostCountry.SelectedValue)
                objADependant._Email = txtEmail.Text
                objADependant._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objADependant._First_Name = txtDBFirstName.Text
                objADependant._Identify_No = txtDBIdNo.Text
                objADependant._Isdependant = chkBeneficiaries.Checked

                objADependant._Last_Name = txtDBLastName.Text
                objADependant._Middle_Name = txtDBMiddleName.Text
                objADependant._Mobile_No = txtDBPersonalNo.Text
                objADependant._Nationalityunkid = CInt(cboDBNationality.SelectedValue)
                objADependant._Post_Box = txtDBPostBox.Text
                objADependant._Relationunkid = CInt(cboDBRelation.SelectedValue)
                objADependant._Stateunkid = CInt(cboPostState.SelectedValue)
                objADependant._Telephone_No = txtDBTelNo.Text
                objADependant._Zipcodeunkid = CInt(cboPostCode.SelectedValue)
                objADependant._Gender = CInt(cboGender.SelectedValue)
                objADependant._CompanyId = ConfigParameter._Object._Companyunkid
                objADependant._blnImgInDb = ConfigParameter._Object._IsImgInDataBase

                objADependant._Isvoid = False
                objADependant._Tranguid = Guid.NewGuid.ToString()
                objADependant._Transactiondate = Now
                objADependant._Approvalremark = ""
                objADependant._Isweb = False
                objADependant._Isfinal = False
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                objADependant._Effective_date = dtpEffectiveDate.Value
                objADependant._IsFromStatus = False
                'Sohail (18 May 2019) -- End

                objADependant._Ip = getIP()
                objADependant._Host = getHostName()
                objADependant._Form_Name = mstrModuleName
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                objADependant._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sohail (18 May 2019) -- End
                objADependant._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

                If menAction <> enAction.EDIT_ONE Then
                    objADependant._Operationtypeid = clsEmployeeDataApproval.enOperationType.ADDED
                    objADependant._Dpndtbeneficetranunkid = -1
                Else
                    objADependant._Dpndtbeneficetranunkid = mintDependants_Benefice_Unkid
                    objADependant._Operationtypeid = clsEmployeeDataApproval.enOperationType.EDITED
                End If

                If ConfigParameter._Object._IsImgInDataBase Then
                    If imgDependants._Image IsNot Nothing Then
                        If System.IO.File.Exists((imgDependants._OriginalFilePath)) Then
                            objADependant._Photo = ImageCompression(imgDependants._OriginalFilePath)
                        End If
                    Else
                        objADependant._Photo = Nothing
                    End If

                    objADependant._Deletedattachedimageid = objDependants_Benefice._OldDatabaseImageUnkid
                Else
                    If imgDependants._OriginalFilePath.Trim.Length > 0 Then
                        Dim img As Image = Image.FromFile(imgDependants._OriginalFilePath)
                        Dim mstrFileName As String = Guid.NewGuid.ToString() + ".jpg"
                        img.Save(imgDependants._FilePath + "\" + mstrFileName, System.Drawing.Imaging.ImageFormat.Jpeg)
                        img = Nothing
                        objADependant._ImagePath = mstrFileName
                        'imgDependants.SaveImage(False)
                    Else
                        objADependant._ImagePath = imgDependants._FileName
                    End If
                End If
            Else
            objDependants_Benefice._Address = txtAddressLine1.Text
            If ConfigParameter._Object._IsDependant_AgeLimit_Set = True Then
                objDependants_Benefice._Birthdate = dtpDBBirthDate.Value
            Else
                If dtpDBBirthDate.Checked Then
                    objDependants_Benefice._Birthdate = dtpDBBirthDate.Value
                Else
                    objDependants_Benefice._Birthdate = Nothing
                End If
            End If
            objDependants_Benefice._Cityunkid = CInt(cboDBPostTown.SelectedValue)
            objDependants_Benefice._Countryunkid = CInt(cboDBPostCountry.SelectedValue)
            objDependants_Benefice._Email = txtEmail.Text
            objDependants_Benefice._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objDependants_Benefice._First_Name = txtDBFirstName.Text
            objDependants_Benefice._Identify_No = txtDBIdNo.Text
            objDependants_Benefice._Isdependant = chkBeneficiaries.Checked

            objDependants_Benefice._Last_Name = txtDBLastName.Text
            objDependants_Benefice._Middle_Name = txtDBMiddleName.Text
            objDependants_Benefice._Mobile_No = txtDBPersonalNo.Text
            objDependants_Benefice._Nationalityunkid = CInt(cboDBNationality.SelectedValue)
            objDependants_Benefice._Post_Box = txtDBPostBox.Text
            objDependants_Benefice._Relationunkid = CInt(cboDBRelation.SelectedValue)
            objDependants_Benefice._Stateunkid = CInt(cboPostState.SelectedValue)
            objDependants_Benefice._Telephone_No = txtDBTelNo.Text

                objDependants_Benefice._Zipcodeunkid = CInt(cboPostCode.SelectedValue)
                objDependants_Benefice._Gender = CInt(cboGender.SelectedValue)
                objDependants_Benefice._CompanyId = ConfigParameter._Object._Companyunkid
                objDependants_Benefice._blnImgInDb = ConfigParameter._Object._IsImgInDataBase

                objDependants_Benefice._Isvoid = False
                objDependants_Benefice._Userunkid = 1
                objDependants_Benefice._Voiddatetime = Nothing
                objDependants_Benefice._Voidreason = ""
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                objDependants_Benefice._Effective_date = dtpEffectiveDate.Value
                objDependants_Benefice._IsFromStatus = False
                'Sohail (18 May 2019) -- End

            If ConfigParameter._Object._IsImgInDataBase Then
                If imgDependants._Image IsNot Nothing Then
                    If System.IO.File.Exists((imgDependants._OriginalFilePath)) Then
                        objDependants_Benefice._Photo = ImageCompression(imgDependants._OriginalFilePath)
                    End If
                Else
                    objDependants_Benefice._Photo = Nothing
                End If
            Else
                objDependants_Benefice._ImagePath = imgDependants._FileName
                    If DependantApprovalFlowVal Is Nothing Then
                imgDependants.SaveImage()
                    End If
                End If

            End If

            'Gajanan [22-Feb-2019] -- End




        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objMaster As New clsMasterData
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master
        Dim objMembership As New clsmembership_master
        'Dim objBenefit As New clsbenefitplan_master

        Try

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsCombos = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If menAction = enAction.EDIT_ONE Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If menAction = enAction.EDIT_ONE Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , , , , False)
            'End If


            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If


            'dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                        User._Object._Userunkid, _
            '                        FinancialYear._Object._YearUnkid, _
            '                        Company._Object._Companyunkid, _
            '                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                        ConfigParameter._Object._UserAccessModeSetting, _
            '                        True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)


            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                    mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", _
                                    True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)

            'S.SANDEEP [20-JUN-2018] -- End

            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 08 OCT 2012 ] -- END

            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

            dsCombos = objMaster.getCountryList("Country", True)
            With cboDBPostCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("Country").Copy
                .SelectedValue = 0
            End With

            With cboDBNationality
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("Country")
                .SelectedValue = 0
            End With


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151104)
            'dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation")
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation", -1, False, False)
            'Shani(24-Aug-2015) -- End

            With cboDBRelation
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Relation")
                .SelectedValue = 0
            End With

            'Sandeep [ 21 Aug 2010 ] -- Start
            'dsCombos = objMembership.getListForCombo("Membership", True)
            'With cboMedicalNo
            '    .ValueMember = "membershipunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Membership")
            '    .SelectedValue = 0
            'End With
            'Sandeep [ 21 Aug 2010 ] -- End

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.BENEFIT_GROUP, True, "BenefitGroup")
            With cboBenefitGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("BenefitGroup")
                .SelectedValue = 0
            End With


            'Sandeep [ 21 Aug 2010 ] -- Start
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, True, "MembershipGroup")
            With cboMemCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("MembershipGroup")
                .SelectedValue = 0
            End With

            dsCombos = objMaster.GetPaymentBy("PayBy")
            With cboValueBasis
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("PayBy")
                .SelectedValue = 0
            End With
            'Sandeep [ 21 Aug 2010 ] -- End 


            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            dsCombos = objMaster.getGenderList("List", True)
            With cboGender
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 23 JAN 2012 ] -- END

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            'SHANI (20 JUN 2015) -- End

            'dsCombos = objBenefit.getComboList("Benefit", True)
            'With cboBenefitType
            '    .ValueMember = "benefitplanunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Benefit")
            '    .SelectedValue = 0
            'End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub


    'Gajanan [21-June-2019] -- Start      
    'Private Function IsValid() As Boolean
    Private Function IsValid(Optional ByVal IsFromAttachment As Boolean = False) As Boolean
        'Gajanan [21-June-2019] -- End
        Try

            'Gajanan [21-June-2019] -- Start      
            Dim blnIsChange As Boolean = True


            If IsFromAttachment = False AndAlso IsNothing(OldData) = False Then
                Dim Birthdate As Date = CDate(IIf(dtpDBBirthDate.Checked, dtpDBBirthDate.Value, Nothing))
                If OldData._First_Name = txtDBFirstName.Text AndAlso _
                   OldData._Middle_Name = txtDBMiddleName.Text AndAlso _
                   OldData._Last_Name = txtDBLastName.Text AndAlso _
                   OldData._Birthdate = Birthdate AndAlso _
                   OldData._Gender = CInt(cboGender.SelectedValue) AndAlso _
                   OldData._Relationunkid = CInt(cboDBRelation.SelectedValue) AndAlso _
                   OldData._Isdependant = CBool(chkBeneficiaries.Checked) AndAlso _
                   OldData._Address = txtAddressLine1.Text AndAlso _
                   OldData._Countryunkid = CInt(cboDBPostCountry.SelectedValue) AndAlso _
                   OldData._Stateunkid = CInt(cboPostState.SelectedValue) AndAlso _
                   OldData._Cityunkid = CInt(cboDBPostTown.SelectedValue) AndAlso _
                   OldData._Zipcodeunkid = CInt(cboPostCode.SelectedValue) AndAlso _
                   OldData._Nationalityunkid = CInt(cboDBNationality.SelectedValue) AndAlso _
                   OldData._Email = txtEmail.Text AndAlso _
                   OldData._Post_Box = txtDBPostBox.Text AndAlso _
                   OldData._Telephone_No = txtDBTelNo.Text AndAlso _
                   OldData._Identify_No = txtDBIdNo.Text AndAlso _
                   OldData._Mobile_No = txtDBPersonalNo.Text AndAlso _
                   OldData._Effective_date = dtpEffectiveDate.Value Then

                    blnIsChange = False
                End If
            End If

            If IsFromAttachment = False AndAlso blnIsChange = False AndAlso IsNothing(mdtDependants) = False AndAlso CInt(mdtDependants.Select("AUD <> '' ").Count) > 0 Then
                blnIsChange = True
            End If

            If IsFromAttachment = False AndAlso blnIsChange = False AndAlso IsNothing(mdtMemTran) = False AndAlso CInt(mdtMemTran.Select("AUD <> '' ").Count) > 0 Then
                blnIsChange = True
            End If

            If IsFromAttachment = False AndAlso blnIsChange = False AndAlso IsNothing(mdtBenefitTran) = False AndAlso CInt(mdtBenefitTran.Select("AUD <> '' ").Count) > 0 Then
                blnIsChange = True
            End If

            If IsFromAttachment = False AndAlso blnIsChange = False Then
                If ConfigParameter._Object._IsImgInDataBase Then

                    If imgDependants._Image IsNot Nothing Then

                        If objDependants_Benefice._Photo IsNot Nothing Then
                            Dim ms As New System.IO.MemoryStream(objDependants_Benefice._Photo, 0, objDependants_Benefice._Photo.Length)
                            Dim msold As New System.IO.MemoryStream(OldData._Photo, 0, OldData._Photo.Length)

                            If msold.Length = ms.Length Then

                                Dim msArray1 = msold.ToArray()
                                Dim msArray2 = ms.ToArray()

                                If msArray1.SequenceEqual(msArray2) Then
                                    blnIsChange = False
                                Else
                                    blnIsChange = True
                                End If
                            End If
                        Else
                            If imgDependants._OriginalFilePath.Trim.Length > 0 Then

                                If OldData._ImagePath = imgDependants._OriginalFilePath Then
                                    blnIsChange = False
                                Else
                                    blnIsChange = True
                                End If
                            End If
                        End If
                    End If
                End If
            End If


            If blnIsChange = False Then
                If menAction = enAction.EDIT_ONE Then
                    objDependants_Benefice = Nothing
                    OldData = Nothing
                    Me.Close()
                    Return False
                End If
            End If
            'Gajanan [21-June-2019] -- End




            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If txtDBFirstName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Firstname cannot be blank. Firstname is compulsory information."), enMsgBoxStyle.Information)
                txtDBFirstName.Focus()
                Return False
            End If


            'Sandeep [ 23 Oct 2010 ] -- Start
            'If txtDBMiddleName.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Middlename cannot be blank. Middlename is compulsory information."), enMsgBoxStyle.Information)
            '    txtDBMiddleName.Focus()
            '    Return False
            'End If
            'Sandeep [ 23 Oct 2010 ] -- End 

            If txtDBLastName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Lastname cannot be blank. Lastname is compulsory information."), enMsgBoxStyle.Information)
                txtDBLastName.Focus()
                Return False
            End If

            If CInt(cboDBRelation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Relation is compulsory information. Please select relation to continue."), enMsgBoxStyle.Information)
                cboDBRelation.Focus()
                Return False
            End If

            'If mblnIsBebeficiaries = True Then
            '    If CInt(cboBenefitGroup.SelectedValue) <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Benefit Group is compulsory information. Please select Benefit Group to continue."), enMsgBoxStyle.Information)
            '        cboBenefitGroup.Focus()
            '        Return False
            '    End If

            '    If CInt(cboBenefitType.SelectedValue) <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Benefit is compulsory information. Please select Benefit to continue."), enMsgBoxStyle.Information)
            '        cboBenefitType.Focus()
            '        Return False
            '    End If

            '    'Sandeep [ 14 Aug 2010 ] -- Start
            '    If txtBenefitAmount.Decimal = 0 And txtBenefitPercent.Decimal = 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please set the benefit percent or amount for this beneficiary."), enMsgBoxStyle.Information)
            '        txtBenefitAmount.Focus()
            '        Return False
            '    End If
            '    'Sandeep [ 14 Aug 2010 ] -- End 

            'Else
            '    If CInt(cboMedicalNo.SelectedValue) <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Membership is compulsory information. Please select Membership to continue."), enMsgBoxStyle.Information)
            '        cboMedicalNo.Focus()
            '        Return False
            '    End If

            '    If txtMembershipNo.Text.Trim = "" Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Membership No. cannot be blank. Membership No. is compulsory information."), enMsgBoxStyle.Information)
            '        txtMembershipNo.Focus()
            '        Return False
            '    End If
            'End If
            'Sandeep [ 14 Aug 2010 ] -- Start
            'S.SANDEEP [ 11 MAR 2014 ] -- START
            'Dim strExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
            Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
            'S.SANDEEP [ 11 MAR 2014 ] -- END
            If txtEmail.Text.Length > 0 Then
                If Expression.IsMatch(txtEmail.Text.Trim) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Invalid Email.Please Enter Valid Email"), enMsgBoxStyle.Information)
                    txtEmail.Focus()
                    Return False
                End If
            End If


            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep it Optional
            'If txtDBIdNo.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Identification No cannot be blank. Identification No is compulsory information."), enMsgBoxStyle.Information)
            '    txtDBIdNo.Focus()
            '    Return False
            'End If
            'Sandeep [ 17 DEC 2010 ] -- End 

            'Sandeep [ 14 Aug 2010 ] -- End 


            'Sandeep [ 23 Oct 2010 ] -- Start

            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep it Optional
            'If lvMembershipInfo.Items.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Dependant membership is compulsory information. Please add atleast one membership to continue."), enMsgBoxStyle.Information)
            '    cboMemCategory.Focus()
            '    Return False
            'End If
            'Sandeep [ 17 DEC 2010 ] -- End 



            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES , Discussed With Mr. Rutta On Date : 07/12/2011 for Importation of File and Making Optional.
            'If chkBeneficiaries.Checked = True Then
            '    If lvBenefit.Items.Count <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Dependant's Benefit information is compulsory information. Please add atleast one benefit to continue."), enMsgBoxStyle.Information)
            '        cboBenefitGroup.Focus()
            '        Return False
            '    End If
            'End If
            'S.SANDEEP [ 07 NOV 2011 ] -- END


            'Sandeep [ 23 Oct 2010 ] -- End 

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If ConfigParameter._Object._IsDependant_AgeLimit_Set Then
                If dtpDBBirthDate.Value.Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Date of birth cannot be greater than or equal to today's date."), enMsgBoxStyle.Information)
                    dtpDBBirthDate.Focus()
                    Return False
                End If

                If CInt(txtAgeLimit.Text) <> 0 Then
                    If DateDiff(DateInterval.Year, dtpDBBirthDate.Value.Date, ConfigParameter._Object._CurrentDateAndTime.Date) > CDbl(txtAgeLimit.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, particular person cannot be consider as dependants or beneficairy. Reason : the age is greater than the limit set."), enMsgBoxStyle.Information)
                        dtpDBBirthDate.Focus()
                        Return False
                    End If
                End If
            End If
            'S.SANDEEP [ 07 NOV 2011 ] -- END


            'Pinkal (27-Mar-2013) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._IsImgInDataBase Then
                If System.IO.File.Exists(imgDependants._OriginalFilePath) Then
                    Dim mintByes As Integer = 6291456    ' 6 MB = 6291456 Bytes
                    Dim fl As New System.IO.FileInfo(imgDependants._OriginalFilePath)
                    If fl.Length > mintByes Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Sorry,You cannot upload file greater than ") & (mintByes / 1024) / 1024 & Language.getMessage(mstrModuleName, 34, " MB."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
            End If

            'Pinkal (27-Mar-2013) -- End


            'Pinkal (1-Sep-2014) -- Start
            'Enhancement - TRA GENDER COMPULOSRY IN EMPLOYEE AND APPLICANT MASTER AS WELL AS IMPORTATION AND PUT APPOINTMENT CHECK BOX ON EMPLOYEE MASTER
            'If CInt(cboGender.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Gender is mandatory information.Please Select Gender."), enMsgBoxStyle.Information)
            '    cboGender.Select()
            '    Return False
            'End If
            'Pinkal (1-Sep-2014) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If menAction <> enAction.EDIT_ONE Then
                If dtpEffectiveDate.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 48, "Please set effective date."), enMsgBoxStyle.Information)
                    dtpEffectiveDate.Focus()
                    Return False
                ElseIf eZeeDate.convertDate(dtpEffectiveDate.Value) > eZeeDate.convertDate(DateAndTime.Today.Date) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Effective date should not be greater than current date."), enMsgBoxStyle.Information)
                    dtpEffectiveDate.Focus()
                    Return False
                End If
            End If
            'Sohail (18 May 2019) -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If DependantApprovalFlowVal Is Nothing Then
            If DependantApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then

                If objApprovalData.IsApproverPresent(enScreenName.frmDependantsAndBeneficiariesList, FinancialYear._Object._DatabaseName, _
                                                     ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                     FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                     User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(cboEmployee.SelectedValue.ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)
                    Return False
                End If
                'Gajanan [17-April-2019] -- End


                Dim intOperationType As Integer = 0
                Dim strMsg As String = ""


                'Gajanan [27-May-2019] -- Start              

                'If objDependants_Benefice.isExist(txtDBFirstName.Text, txtDBMiddleName.Text, txtDBLastName.Text, txtDBIdNo.Text, CInt(cboEmployee.SelectedValue), mintDependants_Benefice_Unkid, Nothing) = True Then
                If objDependants_Benefice.isExist(txtDBFirstName.Text, txtDBLastName.Text, txtDBMiddleName.Text, txtDBIdNo.Text, CInt(cboEmployee.SelectedValue), mintDependants_Benefice_Unkid, Nothing) = True Then

                    'Gajanan [27-May-2019] -- End

                    If chkBeneficiaries.Checked = False Then


                        'Gajanan [27-May-2019] -- Start              
                        'strMsg = Language.getMessage(mstrModuleName, 1013, "This Dependant is already defined. Please define new Dependant.")
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1013, "This Dependant is already defined. Please define new Dependant."), enMsgBoxStyle.Information)
                        Return False
                        'Gajanan [27-May-2019] -- End

                    Else

                        'Gajanan [27-May-2019] -- Start              
                        'strMsg = Language.getMessage(mstrModuleName, 1014, "This Beneficiary is already defined. Please define new Beneficiary.")
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1014, "This Beneficiary is already defined. Please define new Beneficiary."), enMsgBoxStyle.Information)
                        Return False
                        'Gajanan [27-May-2019] -- End

                    End If

                End If

                'Gajanan [27-May-2019] -- Start              
                'If objADependant.isExist(txtDBFirstName.Text, txtDBMiddleName.Text, txtDBLastName.Text, txtDBIdNo.Text, CInt(cboEmployee.SelectedValue), "", Nothing, False, intOperationType) = True Then
                If objADependant.isExist(txtDBFirstName.Text, txtDBLastName.Text, txtDBMiddleName.Text, txtDBIdNo.Text, CInt(cboEmployee.SelectedValue), "", Nothing, False, intOperationType) = True Then
                    'Gajanan [27-May-2019] -- End

                    If intOperationType > 0 Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                If menAction = enAction.EDIT_ONE Then
                                    If chkBeneficiaries.Checked = False Then

                                        'Gajanan [27-May-2019] -- Start              


                                        'strMsg = Language.getMessage(mstrModuleName, 1001, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in edit mode.")
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1001, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in edit mode."), enMsgBoxStyle.Information)
                                        Return False
                                        'Gajanan [27-May-2019] -- End

                                    Else

                                        'Gajanan [27-May-2019] -- Start              
                                        'strMsg = Language.getMessage(mstrModuleName, 1002, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in edit mode.")
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1002, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in edit mode."), enMsgBoxStyle.Information)
                                        Return False
                                        'Gajanan [27-May-2019] -- End

                                    End If
                                Else
                                    If chkBeneficiaries.Checked = False Then

                                        'Gajanan [27-May-2019] -- Start              


                                        'strMsg = Language.getMessage(mstrModuleName, 1003, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in edit mode.")
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1003, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in edit mode."), enMsgBoxStyle.Information)
                                        Return False
                                        'Gajanan [27-May-2019] -- End

                                    Else

                                        'Gajanan [27-May-2019] -- Start              
                                        'strMsg = Language.getMessage(mstrModuleName, 1004, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in edit mode.")
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1004, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in edit mode."), enMsgBoxStyle.Information)
                                        Return False
                                        'Gajanan [27-May-2019] -- End

                                    End If
                                End If
                            Case clsEmployeeDataApproval.enOperationType.DELETED

                                If menAction = enAction.EDIT_ONE Then
                                    If chkBeneficiaries.Checked = False Then

                                        'Gajanan [27-May-2019] -- Start              
                                        'strMsg = Language.getMessage(mstrModuleName, 1005, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in deleted mode.")
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1005, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in deleted mode."), enMsgBoxStyle.Information)
                                        Return False
                                        'Gajanan [27-May-2019] -- End

                                    Else

                                        'Gajanan [27-May-2019] -- Start              


                                        'strMsg = Language.getMessage(mstrModuleName, 1006, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in deleted mode.")
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1006, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in deleted mode."), enMsgBoxStyle.Information)
                                        Return False
                                        'Gajanan [27-May-2019] -- End

                                    End If
                                Else
                                    If chkBeneficiaries.Checked = False Then

                                        'Gajanan [27-May-2019] -- Start              
                                        'strMsg = Language.getMessage(mstrModuleName, 1007, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in deleted mode.")
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1007, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in deleted mode."), enMsgBoxStyle.Information)
                                        Return False
                                        'Gajanan [27-May-2019] -- End

                                    Else

                                        'Gajanan [27-May-2019] -- Start              
                                        'strMsg = Language.getMessage(mstrModuleName, 1008, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in deleted mode.")
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1008, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in deleted mode."), enMsgBoxStyle.Information)
                                        Return False
                                        'Gajanan [27-May-2019] -- End

                                    End If
                                End If


                            Case clsEmployeeDataApproval.enOperationType.ADDED

                                If menAction = enAction.EDIT_ONE Then
                                    If chkBeneficiaries.Checked = False Then

                                        'Gajanan [27-May-2019] -- Start              
                                        'strMsg = Language.getMessage(mstrModuleName, 1009, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in added mode.")
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1009, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in added mode."), enMsgBoxStyle.Information)
                                        Return False
                                        'Gajanan [27-May-2019] -- End
                                    Else

                                        'Gajanan [27-May-2019] -- Start              
                                        'strMsg = Language.getMessage(mstrModuleName, 1010, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in added mode.")
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1010, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in added mode."), enMsgBoxStyle.Information)
                                        Return False
                                        'Gajanan [27-May-2019] -- End

                                    End If
                                Else
                                    If chkBeneficiaries.Checked = False Then

                                        'Gajanan [27-May-2019] -- Start              
                                        'strMsg = Language.getMessage(mstrModuleName, 1011, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in added mode.")
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1011, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in added mode."), enMsgBoxStyle.Information)
                                        Return False
                                        'Gajanan [27-May-2019] -- End

                                    Else

                                        'Gajanan [27-May-2019] -- Start              
                                        'strMsg = Language.getMessage(mstrModuleName, 1012, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in added mode.")
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1012, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in added mode."), enMsgBoxStyle.Information)
                                        Return False
                                        'Gajanan [27-May-2019] -- End

                                    End If
                                End If
                        End Select
                    End If
                End If
                If strMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub FillMemberShipList()
        Try
            lvMembershipInfo.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtMemTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem
                    Dim objComm_Mster As New clsCommon_Master
                    objComm_Mster._Masterunkid = CInt(dtRow.Item("membership_categoryunkid"))
                    lvItem.Text = objComm_Mster._Name
                    objComm_Mster = Nothing
                    Dim objMembership As New clsmembership_master
                    objMembership._Membershipunkid = CInt(dtRow.Item("membershipunkid"))
                    lvItem.SubItems.Add(objMembership._Membershipname)
                    lvItem.SubItems.Add(dtRow.Item("membershipno").ToString)
                    lvItem.SubItems.Add(dtRow.Item("membership_categoryunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("membershipunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)

                    lvItem.Tag = dtRow.Item("dpndtmembershiptranunkid")

                    lvMembershipInfo.Items.Add(lvItem)
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMemberShipList", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetMemberShip()
        Try
            cboMedicalNo.SelectedValue = 0
            cboMemCategory.SelectedValue = 0
            txtMembershipNo.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetMemberShip", mstrModuleName)
        End Try
    End Sub



    'Pinkal (20-Sep-2017) -- Start
    'Enhancement - Working Dependanct Benefit Plan Changes for PACRA.


    'Private Sub FillBenefitList()
    '    Try
    '        lvBenefit.Items.Clear()
    '        Dim lvItem As ListViewItem
    '        For Each dtRow As DataRow In mdtBenefitTran.Rows
    '            If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
    '                lvItem = New ListViewItem

    '                Dim objComm_Mster As New clsCommon_Master
    '                objComm_Mster._Masterunkid = CInt(dtRow.Item("benefitgroupunkid"))
    '                lvItem.Text = objComm_Mster._Name
    '                objComm_Mster = Nothing

    '                Dim objBenefitPlan As New clsbenefitplan_master
    '                objBenefitPlan._Benefitplanunkid = CInt(dtRow.Item("benefitplanunkid"))
    '                lvItem.SubItems.Add(objBenefitPlan._Benefitplanname)
    '                objBenefitPlan = Nothing

    '                RemoveHandler cboValueBasis.SelectedValueChanged, AddressOf cboValueBasis_SelectedValueChanged
    '                cboValueBasis.SelectedValue = CInt(dtRow.Item("value_id").ToString)
    '                AddHandler cboValueBasis.SelectedValueChanged, AddressOf cboValueBasis_SelectedValueChanged

    '                lvItem.SubItems.Add(cboValueBasis.Text.ToString)
    '                lvItem.SubItems.Add(dtRow.Item("benefit_percent").ToString())

    '                'lvItem.SubItems.Add(dtRow.Item("benefit_amount").ToString)

    '                lvItem.SubItems.Add(dtRow.Item("benefitgroupunkid").ToString)
    '                lvItem.SubItems.Add(dtRow.Item("benefitplanunkid").ToString)
    '                lvItem.SubItems.Add(dtRow.Item("value_id").ToString)
    '                lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
    '                lvItem.Tag = dtRow.Item("dpndtbenefittranunkid")
    '                lvBenefit.Items.Add(lvItem)

    '            End If
    '        Next
    '        cboValueBasis.SelectedValue = mintSeletedValue
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillBenefitList", mstrModuleName)
    '    End Try
    'End Sub


    Private Sub FillBenefitList()
        Try

            Dim mstrBenefitGrpId As String = String.Join(",", mdtBenefitTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("benefitgroupunkid").ToString()).Distinct().ToArray())

            If mstrBenefitGrpId.Trim.Length > 0 Then
                Dim arBenefitGrp() As String = mstrBenefitGrpId.Split(CChar(","))
                If arBenefitGrp.Length > 0 Then
                    For i As Integer = 0 To arBenefitGrp.Length - 1

                        If mdtBenefitTran.Columns.Contains("SortId") = False Then
                            Dim dcColumn As New DataColumn("SortId", Type.GetType("System.Int16"))
                            dcColumn.DefaultValue = 0
                            mdtBenefitTran.Columns.Add(dcColumn)
                        End If

                        If mdtBenefitTran.Select("dpndtbenefittranunkid = -999 AND benefitgroupunkid = " & CInt(arBenefitGrp(i))).Length <= 0 Then
                            Dim drRow As DataRow = mdtBenefitTran.NewRow()
                            drRow("dpndtbenefittranunkid") = -999
                            drRow("benefitplanunkid") = -1
                            drRow("value_id") = mintSeletedValue
                            drRow("benefitgroupunkid") = CInt(arBenefitGrp(i))
                            drRow("benefit_group") = Language.getMessage(mstrModuleName, 46, "Total")
                            drRow("benefit_percent") = CDec(mdtBenefitTran.Compute("SUM(benefit_percent)", "AUD <> 'D' AND benefitgroupunkid = " & CInt(arBenefitGrp(i))))
                            drRow("benefit_amount") = CDec(mdtBenefitTran.Compute("SUM(benefit_amount)", "AUD <> 'D' AND benefitgroupunkid = " & CInt(arBenefitGrp(i))))
                            drRow("SortId") = 999
                            mdtBenefitTran.Rows.Add(drRow)
                        Else
                            Dim drPercentage() As DataRow = mdtBenefitTran.Select("dpndtbenefittranunkid = -999 AND benefitgroupunkid = " & CInt(arBenefitGrp(i)))
                            If drPercentage.Length > 0 Then
                                drPercentage(0)("benefit_percent") = CDec(mdtBenefitTran.Compute("SUM(benefit_percent)", "AUD <> 'D' AND dpndtbenefittranunkid <> -999 AND benefitgroupunkid = " & CInt(arBenefitGrp(i))))
                                drPercentage(0)("benefit_amount") = CDec(mdtBenefitTran.Compute("SUM(benefit_amount)", "AUD <> 'D' AND dpndtbenefittranunkid <> -999 AND benefitgroupunkid = " & CInt(arBenefitGrp(i))))
                                drPercentage(0)("SortId") = 999
                                mdtBenefitTran.AcceptChanges()
                            End If
                        End If
                    Next

                End If

                mdtBenefitTran = New DataView(mdtBenefitTran, "", "benefitgroupunkid,SortId", DataViewRowState.CurrentRows).ToTable()

            End If

            RemoveHandler cboValueBasis.SelectedValueChanged, AddressOf cboValueBasis_SelectedValueChanged

            lvBenefit.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtBenefitTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem
                    lvItem.Text = dtRow.Item("benefit_group").ToString()
                    lvItem.SubItems.Add(dtRow.Item("benefit_type").ToString())
                    cboValueBasis.SelectedValue = CInt(dtRow.Item("value_id").ToString)

                    If CInt(dtRow.Item("dpndtbenefittranunkid")) <> -999 Then
                    lvItem.SubItems.Add(cboValueBasis.Text.ToString)
                    Else
                        lvItem.SubItems.Add("")
                    End If

                    lvItem.SubItems.Add(CDec(dtRow.Item("benefit_percent")).ToString("#0.00"))
                    lvItem.SubItems.Add(Format(CDec(dtRow.Item("benefit_amount")), GUI.fmtCurrency))
                    lvItem.SubItems.Add(dtRow.Item("benefitgroupunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("benefitplanunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("value_id").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    lvItem.Tag = dtRow.Item("dpndtbenefittranunkid")

                    If CInt(dtRow.Item("dpndtbenefittranunkid")) = -999 Then
                        lvItem.Font = New Font(lvItem.Font, FontStyle.Bold)
                        lvItem.BackColor = Color.Blue
                        lvItem.ForeColor = Color.White
                    End If
                    lvBenefit.Items.Add(lvItem)

                End If
            Next

            If lvBenefit.Items.Count > 4 Then
                colhAmount.Width = 150 - 18
            Else
                colhAmount.Width = 150
            End If

            AddHandler cboValueBasis.SelectedValueChanged, AddressOf cboValueBasis_SelectedValueChanged


            cboValueBasis.SelectedValue = mintSeletedValue
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillBenefitList", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Sep-2017) -- End

    Private Sub ResetBenefits()
        Try
            cboBenefitGroup.SelectedValue = 0
            cboBenefitType.SelectedValue = 0
            cboValueBasis.SelectedValue = 0
            txtBenefitAmount.Decimal = 0
            txtBenefitPercent.Decimal = 0

            'Pinkal (20-Sep-2017) -- Start
            'Enhancement - Working Dependant Benefit Plan Changes for PACRA.
            txtBenefitAmount.Enabled = True
            txtBenefitPercent.Enabled = True
            cboBenefitGroup.Enabled = True
            cboBenefitType.Enabled = True
            cboBenefitGroup.Focus()
            'Pinkal (20-Sep-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetBenefits", mstrModuleName)
        End Try
    End Sub

    Private Function IsPercentageProper(ByVal intOperation As Integer) As Boolean
        Try
            If CInt(cboValueBasis.SelectedValue) = 2 Then
                Select Case intOperation
                    Case 1  'ADD
                        If (txtTotalPercentage.Decimal + txtBenefitPercent.Decimal) > 100 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Benefit Percentage cannot be greater than 100 Percent."), enMsgBoxStyle.Information)
                            txtBenefitPercent.Focus()
                            Return False
                        Else
                            txtTotalPercentage.Decimal = txtTotalPercentage.Decimal + txtBenefitPercent.Decimal
                            txtRemainingPercent.Decimal = 100 - txtTotalPercentage.Decimal
                        End If
                    Case 2  'EDIT

                        'Anjan (11 Jun 2011)-Start
                        'Dim dblDiff As Double = 0
                        Dim dblDiff As Decimal = 0
                        'Anjan (11 Jun 2011)-End 


                        dblDiff = dblAddedPercent - txtBenefitPercent.Decimal
                        If (txtTotalPercentage.Decimal - dblDiff) > 100 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Benefit Percentage cannot be greater than 100 Percent."), enMsgBoxStyle.Information)
                            txtBenefitPercent.Focus()
                            Return False
                        Else
                            txtTotalPercentage.Decimal = txtTotalPercentage.Decimal - dblDiff
                            txtRemainingPercent.Decimal = 100 - txtTotalPercentage.Decimal
                        End If
                    Case 3  'DELETE
                        txtTotalPercentage.Decimal = txtTotalPercentage.Decimal - txtBenefitPercent.Decimal
                        txtRemainingPercent.Decimal = 100 - txtTotalPercentage.Decimal
                End Select
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsPercentageProper", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub SetVisibility()

        Try
            objbtnAddBenefitGroup.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddBenefitType.Enabled = User._Object.Privilege._AddBenefitPlan
            objbtnAddMemCategory.Enabled = User._Object.Privilege._AddCommonMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    Private Function Set_Notification(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Dim objEmployee As New clsEmployee_Master
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            'S.SANDEEP [04 JUN 2015] -- END

            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If ConfigParameter._Object._Notify_EmplData.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & "<b>" & getTitleCase(StrUserName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee dependant(s).</span></p>")
                StrMessage.Append(" " & Language.getMessage(mstrModuleName, 47, "This is to inform you that changes have been made for employee dependant(s)") & "</span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Following information has been changed by user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 144, "Following information has been changed by user") & " " & "<b>" & " " & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 145, "from Machine") & " " & "<b>" & getHostName.ToString & "</b>" & " " & Language.getMessage("frmEmployeeMaster", 146, "and IPAddress") & " " & "<b>" & getIP.ToString & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append(vbCrLf)
                'Gajanan (21 Nov 2018) -- Start

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("<TABLE border = '1' WIDTH = '50%'  style='margin-left: 25px'>")
                StrMessage.Append("<TABLE border = '1' WIDTH = '50%'")
                'Gajanan [27-Mar-2019] -- End

                'Gajanan (21 Nov 2018) -- End

                StrMessage.Append(vbCrLf)
                'S.SANDEEP [ 18 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append("<TD align = 'LEFT' COLSPAN = '3'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 31, "Employee : ") & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & "</span></b></TD>")
                StrMessage.Append("</TR>")
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append("<TD align = 'LEFT' COLSPAN = '3'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 32, "Depandant : ") & txtDBFirstName.Text & " " & IIf(txtDBMiddleName.Text <> "", txtDBMiddleName.Text, "").ToString & " " & txtDBLastName.Text & "</span></b></TD>")
                StrMessage.Append("</TR>")
                'S.SANDEEP [ 18 DEC 2012 ] -- END
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:15%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 24, "Field") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 25, "Old Value") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 26, "New Value") & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")
                For Each sId As String In ConfigParameter._Object._Notify_EmplData.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enEmployeeData.DEPENDENTS

                            If objDependants_Benefice._First_Name.Trim <> txtDBFirstName.Text.Trim Or objDependants_Benefice._Middle_Name.Trim <> txtDBMiddleName.Text.Trim Or objDependants_Benefice._Last_Name.Trim <> txtDBLastName.Text.Trim Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 27, "Dependant Full Name") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objDependants_Benefice._First_Name.Trim & " " & objDependants_Benefice._Middle_Name.Trim & " " & objDependants_Benefice._Last_Name.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtDBFirstName.Text.Trim & " " & txtDBMiddleName.Text.Trim & " " & txtDBLastName.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objDependants_Benefice._Birthdate.Date <> dtpDBBirthDate.Value.Date Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 28, "Birth Date") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(objDependants_Benefice._Birthdate <> Nothing, objDependants_Benefice._Birthdate.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & dtpDBBirthDate.Value.Date & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objDependants_Benefice._Relationunkid <> CInt(cboDBRelation.SelectedValue) Then
                                Dim objRelation As New clsCommon_Master
                                objRelation._Masterunkid = objDependants_Benefice._Relationunkid
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 29, "Relation") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objRelation._Name.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboDBRelation.SelectedValue) <= 0, "", cboDBRelation.Text.Trim)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objDependants_Benefice._Gender <> CInt(cboGender.SelectedValue) Then
                                Dim objGender As New clsMasterData
                                Dim dsList As DataSet = objGender.getGenderList("List", False)

                                Dim drRow() As DataRow = dsList.Tables(0).Select("id <> 0 AND id = " & objDependants_Benefice._Gender)

                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 30, "Gender") & "</span></TD>")
                                If drRow.Length > 0 Then
                                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & drRow(0)("Name").ToString() & "</span></TD>")
                                Else
                                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & " " & "</span></TD>")
                                End If
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboGender.SelectedValue) <= 0, "", cboGender.Text.Trim)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If


                    End Select
                Next
            End If
            StrMessage.Append("</TABLE>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If menAction = enAction.EDIT_ONE Then
                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 35, "Notification of Changes in Employee Dependant(s).")
                            objSendMail._Message = dicNotification(sKey)
                            'S.SANDEEP [ 28 JAN 2014 ] -- START
                            'Sohail (17 Dec 2014) -- Start
                            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                            'objSendMail._Form_Name = mstrModuleName
                            objSendMail._Form_Name = "" 'Please pass Form Name for WEB
                            'Sohail (17 Dec 2014) -- End
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                            objSendMail._UserUnkid = User._Object._Userunkid
                            objSendMail._SenderAddress = User._Object._Email
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                            With objSendMail
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            'S.SANDEEP [ 28 JAN 2014 ] -- END
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(Company._Object._Companyunkid)
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If

            ElseIf menAction <> enAction.EDIT_ONE Then
                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 36, "Notifications to newly added Employee Dependant(s).")
                            Dim sMsg As String = dicNotification(sKey)
                            'Set_Notification(User._Object._Firstname.Trim & " " & User._Object._Lastname.Trim)
                            objSendMail._Message = sMsg
                            With objSendMail
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(Company._Object._Companyunkid)
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 18 SEP 2012 ] -- END

    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtDependants.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtDependants.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Employee_Module
                dRow("scanattachrefid") = enScanAttactRefId.DEPENDANTS
                dRow("transactionunkid") = mintDependants_Benefice_Unkid
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = dtpAttachmentDate.Value.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                dRow("filesize_kb") = f.Length / 1024
                'SHANI (16 JUL 2015) -- End 

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                dRow("form_name") = mstrModuleName
                'Gajanan [22-Feb-2019] -- End

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            mdtDependants.Rows.Add(dRow)
            Call fillDependantAttachment()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddDocumentAttachment", mstrModuleName)
        End Try
    End Sub

    Private Sub fillDependantAttachment()
        Dim dtView As DataView
        Try
            If mdtDependants Is Nothing Then Exit Sub

            dtView = New DataView(mdtDependants, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvDepedantAttachment.AutoGenerateColumns = False
            colhName.DataPropertyName = "filename"
            colhSize.DataPropertyName = "filesize"
            objcolhGUID.DataPropertyName = "GUID"
            objcolhScanUnkId.DataPropertyName = "scanattachtranunkid"
            dgvDepedantAttachment.DataSource = dtView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'SHANI (20 JUN 2015) -- End 



#End Region

#Region " Form's Events "
    Private Sub frmDependantsAndBeneficiaries_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objDependants_Benefice = Nothing
    End Sub

    Private Sub frmDependantsAndBeneficiaries_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmDependantsAndBeneficiaries_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmDependantsAndBeneficiaries_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDependants_Benefice = New clsDependants_Beneficiary_tran
        'Gajanan [9-April-2019] -- Start
        objApprovalData = New clsEmployeeDataApproval
        'Gajanan [9-April-2019] -- End

        'Sandeep [ 21 Aug 2010 ] -- Start
        objDependantMemberTran = New clsDependants_Membership_tran
        objDependantBenefitTran = New clsDependants_Benefit_tran
        'Sandeep [ 21 Aug 2010 ] -- End 

        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        objADependant = New clsDependant_beneficiaries_approval_tran
        'Gajanan [22-Feb-2019] -- End

        Try
            Call Set_Logo(Me, gApplicationType)
            'Call SetCaptions()
            'Hemant (30 Nov 2018) -- Start
            'Enhancement : Including Language Settings For Scan/Attachment Button
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Hemant (30 Nov 2018) -- End

            

            Call SetVisibility()

            Call SetColor()
            Call FillCombo()


            'Pinkal (27-Mar-2013) -- Start
            'Enhancement : TRA Changes
            objDependants_Benefice._CompanyId = ConfigParameter._Object._Companyunkid
            objDependants_Benefice._blnImgInDb = ConfigParameter._Object._IsImgInDataBase
            'Pinkal (27-Mar-2013) -- End


            If menAction = enAction.EDIT_ONE Then
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                dtpEffectiveDate.Enabled = False
                'Sohail (18 May 2019) -- End
                objDependants_Benefice._Dpndtbeneficetranunkid = mintDependants_Benefice_Unkid

                'Gajanan [21-June-2019] -- Start      
                OldData = objDependants_Benefice
                'Gajanan [21-June-2019] -- End

            Else
                cboEmployee.Enabled = True
                objbtnSearchEmployee.Enabled = True
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                dtpEffectiveDate.Enabled = True
                'Sohail (18 May 2019) -- End
            End If



            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If ConfigParameter._Object._IsDependant_AgeLimit_Set = False Then
                dtpDBBirthDate.ShowCheckBox = True
                dtpDBBirthDate.Checked = False
                RemoveHandler cboDBRelation.SelectedIndexChanged, AddressOf cboDBRelation_SelectedIndexChanged
            End If

            lblLimit.Visible = ConfigParameter._Object._IsDependant_AgeLimit_Set
            txtAgeLimit.Visible = ConfigParameter._Object._IsDependant_AgeLimit_Set
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            'S.SANDEEP [02 JAN 2017] -- START
            'ENHANCEMENT : GETTING YEAR,MONTH,DAY DIFFERENCE
            Call dtpDBBirthDate_ValueChanged(New Object(), New EventArgs())
            'S.SANDEEP [02 JAN 2017] -- END

            Call GetValue()

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            Call fillDependantAttachment()
            'SHANI (20 JUN 2015) -- End 

            'Sandeep [ 21 Aug 2010 ] -- Start
            objDependantMemberTran._DependantTranUnkid = mintDependants_Benefice_Unkid
            mdtMemTran = objDependantMemberTran._DataTable
            Call FillMemberShipList()

            objDependantBenefitTran._DependantTranUnkid = mintDependants_Benefice_Unkid
            mdtBenefitTran = objDependantBenefitTran._DataTable
            Call FillBenefitList()
            'Sandeep [ 21 Aug 2010 ] -- End 

            cboEmployee.Focus()
            imgDependants._FilePath = ConfigParameter._Object._PhotoPath

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END


            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes
            imgDependants._ShowAddButton = User._Object.Privilege._AllowToAddEditPhoto
            imgDependants._ShowDeleteButton = User._Object.Privilege._AllowToDeletePhoto
            'Pinkal (01-Apr-2013) -- End


            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DEPENDANTS).Tables(0).Rows(0)("Name").ToString
            'SHANI (16 JUL 2015) -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDependants_Beneficiary_tran.SetMessages()

            objfrm._Other_ModuleNames = "clsDependants_Beneficiary_tran"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END
#End Region

#Region " Button's Events "
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            'SHANI (27 JUL 2015) -- Start
            'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
            Cursor.Current = Cursors.WaitCursor
            'SHANI (27 JUL 2015) -- End

            If IsValid() = False Then Exit Sub

            'SHANI (27 JUL 2015) -- Start
            'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
            If ConfigParameter._Object._DependantDocsAttachmentMandatory Then
                If mdtDependants Is Nothing OrElse mdtDependants.Select("AUD <> 'D'").Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'SHANI (27 JUL 2015) -- End 

            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dicNotification = New Dictionary(Of String, String)
            If menAction = enAction.EDIT_ONE Then
                If ConfigParameter._Object._Notify_EmplData.Trim.Length > 0 Then
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    For Each sId As String In ConfigParameter._Object._Notify_EmplData.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        dicNotification.Add(objUsr._Email, StrMessage)
                    Next
                    objUsr = Nothing
                End If

            ElseIf menAction <> enAction.EDIT_ONE Then
                If ConfigParameter._Object._Notify_EmplData.Trim.Length > 0 Then
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    For Each sId As String In ConfigParameter._Object._Notify_EmplData.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)

                        If dicNotification.ContainsKey(objUsr._Email) = False Then
                            dicNotification.Add(objUsr._Email, StrMessage)
                        End If

                    Next
                    objUsr = Nothing
                End If
            End If

            'S.SANDEEP [ 18 SEP 2012 ] -- END




            Call SetValue()


            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151104)
            'Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
            If IsNothing(mdtDependants) = False AndAlso mdtDependants.Rows.Count > 0 Then
                Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                'Shani(24-Aug-2015) -- End

                Dim strError As String = ""

                For Each dRow As DataRow In mdtDependants.Rows
                    If dRow("AUD").ToString = "A" AndAlso dRow("localpath").ToString <> "" Then
                        Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                        If blnIsIISInstalled Then

                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If clsFileUploadDownload.UploadFile(CStr(dRow("localpath")), mstrFolderName, strFileName, strError) = False Then
                            If clsFileUploadDownload.UploadFile(CStr(dRow("localpath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                'Shani(24-Aug-2015) -- End

                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            Else
                                Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                dRow("fileuniquename") = strFileName
                                If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                    strPath += "/"
                                End If
                                strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                                dRow("filepath") = strPath
                                dRow.AcceptChanges()
                            End If
                        Else
                            If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                    Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                                End If
                                File.Copy(CStr(dRow("localpath")), strDocLocalPath, True)
                                dRow("fileuniquename") = strFileName
                                dRow("filepath") = strDocLocalPath
                                dRow.AcceptChanges()
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If

                        End If
                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFileName As String = dRow("fileuniquename").ToString
                        If blnIsIISInstalled Then

                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError) = False Then
                            'Hemant [8-April-2019] -- Start
                            'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                                'Hemant [8-April-2019] -- End
                                'Shani(24-Aug-2015) -- End

                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        Else
                            If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If File.Exists(strDocLocalPath) Then
                                    File.Delete(strDocLocalPath)
                                End If
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    End If
                Next

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151104)
            End If
            'Shani(24-Aug-2015) -- End

            'SHANI (16 JUL 2015) -- End 

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim drow_mem As DataRow() = mdtMemTran.Select("AUD <> 'D' and dpndtbeneficetranunkid <= 0")
            If drow_mem.Length > 0 Then
                mdtMemTran = drow_mem.CopyToDataTable()
                drow_mem = Nothing
            End If

            drow_mem = mdtBenefitTran.Select("AUD <> 'D' and dpndtbeneficetranunkid <= 0")
            If drow_mem.Length > 0 Then
                mdtBenefitTran = drow_mem.CopyToDataTable()
                drow_mem = Nothing
            End If
            'Gajanan [22-Feb-2019] -- End

            With objDependants_Benefice
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim ExtraFilter As String = "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and first_name = '" & txtDBFirstName.Text & "' and last_name ='" & txtDBLastName.Text & "' and relationunkid =" & cboDBRelation.SelectedValue.ToString() & ""
            Dim ExtraFilterForQuery As String = "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and first_name = '" & txtDBFirstName.Text & "' and last_name ='" & txtDBLastName.Text & "' and relationunkid =" & cboDBRelation.SelectedValue.ToString() & ""


            If txtDBMiddleName.ToString.Trim.Length > 0 Then
                ExtraFilter &= "and middle_name ='" & txtDBMiddleName.Text & "'"
                ExtraFilterForQuery &= "and middle_name ='" & txtDBMiddleName.Text & "'"
            End If
            'Gajanan [17-April-2019] -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            With objDependants_Benefice
                ._FormName = mstrModuleName
                ._Loginemployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._Isweb = False
                ._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With
            'Sohail (18 May 2019) -- End

            If menAction = enAction.EDIT_ONE Then


                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'blnFlag = objDependants_Benefice.Update(mdtMemTran, mdtBenefitTran, mdtDependants)


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                'If DependantApprovalFlowVal Is Nothing Then
                If DependantApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                    'Gajanan [17-April-2019] -- End


                    blnFlag = objADependant.Insert(Nothing, mdtMemTran, mdtBenefitTran, mdtDependants)

                    If blnFlag = False AndAlso objADependant._Message <> "" Then
                        eZeeMsgBox.Show(objADependant._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    Else
                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                        ConfigParameter._Object._UserAccessModeSetting, _
                                                        Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                        CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                        enScreenName.frmDependantsAndBeneficiariesList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                        User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                        User._Object._Username, clsEmployeeDataApproval.enOperationType.EDITED, 0, cboEmployee.SelectedValue.ToString(), "", Nothing, _
                                                        ExtraFilter, Nothing, False, , ExtraFilterForQuery)

                    End If
                Else
                blnFlag = objDependants_Benefice.Update(mdtMemTran, mdtBenefitTran, mdtDependants)
                End If
                'Gajanan [22-Feb-2019] -- End
            Else
                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'blnFlag = objDependants_Benefice.Insert(mdtMemTran, mdtBenefitTran, mdtDependants)


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If DependantApprovalFlowVal Is Nothing Then
                If DependantApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                    'Gajanan [17-April-2019] -- End

                    blnFlag = objADependant.Insert(Nothing, mdtMemTran, mdtBenefitTran, mdtDependants)
                    If blnFlag = False AndAlso objADependant._Message <> "" Then
                        eZeeMsgBox.Show(objADependant._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    Else
                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                       ConfigParameter._Object._UserAccessModeSetting, _
                                                       Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                       CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                       enScreenName.frmDependantsAndBeneficiariesList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                       User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                       User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, 0, cboEmployee.SelectedValue.ToString(), , , _
                                                       ExtraFilter, Nothing, False, , _
                                                       ExtraFilterForQuery, Nothing)
                    End If
            Else
                blnFlag = objDependants_Benefice.Insert(mdtMemTran, mdtBenefitTran, mdtDependants)
            End If
                'Gajanan [22-Feb-2019] -- End
            End If

            If blnFlag = False And objDependants_Benefice._Message <> "" Then
                eZeeMsgBox.Show(objDependants_Benefice._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then


                'Pinkal (01-Apr-2013) -- Start
                'Enhancement : TRA Changes

                'Pinkal (27-Mar-2013) -- Start
                'Enhancement : TRA Changes

                'If ConfigParameter._Object._IsImgInDataBase Then
                '    If imgDependants._FileName <> "" Then
                '        If System.IO.File.Exists(imgDependants._FilePath & "/" & imgDependants._FileName) Then
                '            Try
                '                System.IO.File.Delete(imgDependants._FilePath & "/" & imgDependants._FileName)
                '            Catch ex As Exception
                '            End Try
                '        End If
                '    End If
                'End If

                'Pinkal (27-Mar-2013) -- End

                'Pinkal (01-Apr-2013) -- End

                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objDependants_Benefice = Nothing
                    objDependants_Benefice = New clsDependants_Beneficiary_tran
                    Call GetValue()

                    'SHANI (20 JUN 2015) -- Start
                    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                    cboDocumentType.SelectedValue = 0
                    dtpAttachmentDate.Value = Today.Date
                    Call fillDependantAttachment()
                    cboEmployee.Enabled = True
                    'SHANI (20 JUN 2015) -- End 

                    cboEmployee.Focus()
                Else
                    mintDependants_Benefice_Unkid = objDependants_Benefice._Dpndtbeneficetranunkid
                    Me.Close()
                End If
            End If

            If mintSelectedEmployee <> -1 Then
                cboEmployee.SelectedValue = mintSelectedEmployee
            End If

            mdtMemTran.Clear()
            Call FillMemberShipList()

            mdtBenefitTran.Clear()
            Call FillBenefitList()


            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            'Sandeep [ 17 DEC 2010 ] -- Start
            Call cboEmployee_SelectedValueChanged(sender, e)
            'Sandeep [ 17 DEC 2010 ] -- End 

            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [09-AUG-2018] -- START
            'If blnFlag = True Then
            '   trd = New Thread(AddressOf Send_Notification)
            '   trd.IsBackground = True
            '   trd.Start()
            'End If


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'If ConfigParameter._Object._SkipEmployeeApprovalFlow = False Then
            If IsNothing(DependantApprovalFlowVal) = False Then
                'Gajanan [27-Mar-2019] -- End
            If blnFlag = True Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If
            End If
            'S.SANDEEP [09-AUG-2018] -- END

            'S.SANDEEP [ 18 SEP 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
            'SHANI (27 JUL 2015) -- Start
            'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
        Finally
            Cursor.Current = Cursors.Default
            'SHANI (27 JUL 2015) -- End 
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Private Sub btnAddAttachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAttachment.Click
        Try

            'Gajanan [21-June-2019] -- Start      
            'If IsValid() = False Then Exit Sub
            If IsValid(True) = False Then Exit Sub
            'Gajanan [21-June-2019] -- End

            If CInt(cboDocumentType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, "Document Type is compulsory information. Please select Document Type to continue."), enMsgBoxStyle.Information)
                cboDocumentType.Focus()
                Exit Sub
            End If

            If (ofdAttachment.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                Dim f As New System.IO.FileInfo(ofdAttachment.FileName)
                Call AddDocumentAttachment(f, ofdAttachment.FileName.ToString)
                f = Nothing

                If dgvDepedantAttachment.Rows.Count > 0 Then
                    cboEmployee.Enabled = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddAttachment_Click", mstrModuleName)
        End Try
    End Sub
    'SHANI (20 JUN 2015) -- End 

    'Sandeep [ 21 Aug 2010 ] -- Start
#Region " Membership Info "
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvMembershipInfo.SelectedItems.Count > 0 Then
                If intMSelectedIdx > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvMembershipInfo.Items(intMSelectedIdx).Tag) = -1 Then
                        drTemp = mdtMemTran.Select("GUID = '" & lvMembershipInfo.Items(intMSelectedIdx).SubItems(objcolhMGUID.Index).Text & "'")
                    Else
                        drTemp = mdtMemTran.Select("dpndtmembershiptranunkid = " & CInt(lvMembershipInfo.Items(intMSelectedIdx).Tag))
                    End If
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        drTemp(0).Item("isvoid") = True
                        drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                        drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        drTemp(0).Item("voidreason") = Language.getMessage(mstrModuleName, 22, "Incorrect Benefit Entry.")
                        Call FillMemberShipList()
                    End If
                End If
            End If
            Call ResetMemberShip()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEditMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditMembership.Click
        Try
            If lvMembershipInfo.SelectedItems.Count > 0 Then
                If intMSelectedIdx > -1 Then
                    Dim drTemp As DataRow()

                    'S.SANDEEP [ 12 OCT 2011 ] -- START
                    'If CInt(lvMembershipInfo.Items(intMSelectedIdx).Tag) = -1 Then
                    '    drTemp = mdtBenefitTran.Select("GUID = '" & lvMembershipInfo.Items(intMSelectedIdx).SubItems(objcolhMGUID.Index).Text & "'")
                    'Else
                    '    drTemp = mdtBenefitTran.Select("dpndtmembershiptranunkid = " & CInt(lvMembershipInfo.Items(intMSelectedIdx).Tag))
                    'End If
                    If CInt(lvMembershipInfo.Items(intMSelectedIdx).Tag) = -1 Then
                        drTemp = mdtMemTran.Select("GUID = '" & lvMembershipInfo.Items(intMSelectedIdx).SubItems(objcolhMGUID.Index).Text & "'")
                    Else
                        drTemp = mdtMemTran.Select("dpndtmembershiptranunkid = " & CInt(lvMembershipInfo.Items(intMSelectedIdx).Tag))
                    End If
                    'S.SANDEEP [ 12 OCT 2011 ] -- END 




                    'Sandeep [ 09 Oct 2010 ] -- Start
                    'Issues Reported by Vimal


                    'S.SANDEEP [ 12 OCT 2011 ] -- START
                    'Dim dtMRow As DataRow() = mdtMemTran.Select("membershipunkid = " & CInt(cboMedicalNo.SelectedValue) & " AND AUD <> 'D' ")
                    'If dtMRow.Length > 0 Then
                    '    If dtMRow Is drTemp Then
                    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Selected Membership is already added to the list."), enMsgBoxStyle.Information)
                    '        Exit Sub
                    '    End If
                    'End If
                    'S.SANDEEP [ 12 OCT 2011 ] -- END 

                    'Sandeep [ 09 Oct 2010 ] -- End 


                    If drTemp.Length > 0 Then
                        With drTemp(0)

                            'S.SANDEEP [ 12 OCT 2011 ] -- START
                            '.Item("dpndtmembershiptranunkid") = -1
                            .Item("dpndtmembershiptranunkid") = .Item("dpndtmembershiptranunkid")
                            'S.SANDEEP [ 12 OCT 2011 ] -- END 
                            .Item("dpndtbeneficetranunkid") = mintDependants_Benefice_Unkid
                            .Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
                            .Item("membershipunkid") = CInt(cboMedicalNo.SelectedValue)
                            .Item("membershipno") = txtMembershipNo.Text
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                '.Item("AUD") = "A"
                                .Item("AUD") = "U"
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 
                            End If
                            .Item("GUID") = Guid.NewGuid.ToString
                            .AcceptChanges()
                        End With
                        Call FillMemberShipList()
                    End If
                End If
                Call ResetMemberShip()
            End If

            'Sandeep [ 09 Oct 2010 ] -- Start
            'Issues Reported by Vimal
            cboMemCategory.Enabled = True
            cboMedicalNo.Enabled = True
            'Sandeep [ 09 Oct 2010 ] -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditMembership_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAddMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddMembership.Click
        If CInt(cboMemCategory.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Membership Category is compulsory information. Please select Membership Category to continue."), enMsgBoxStyle.Information)
            cboMemCategory.Focus()
            Exit Sub
        End If

        If CInt(cboMedicalNo.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Membership is compulsory information. Please select Membership to continue."), enMsgBoxStyle.Information)
            cboMedicalNo.Focus()
            Exit Sub
        End If

        If txtMembershipNo.Text.Trim = "" Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Membership No. cannot be blank. Membership No. is compulsory information."), enMsgBoxStyle.Information)
            txtMembershipNo.Focus()
            Exit Sub
        End If

        Dim dtMRow As DataRow() = mdtMemTran.Select("membershipunkid = " & CInt(cboMedicalNo.SelectedValue) & " AND AUD <> 'D' ")
        If dtMRow.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Selected Membership is already added to the list."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            Dim dtMemRow As DataRow
            dtMemRow = mdtMemTran.NewRow

            dtMemRow.Item("dpndtmembershiptranunkid") = -1
            dtMemRow.Item("dpndtbeneficetranunkid") = mintDependants_Benefice_Unkid
            dtMemRow.Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
            dtMemRow.Item("membershipunkid") = CInt(cboMedicalNo.SelectedValue)
            dtMemRow.Item("membershipno") = txtMembershipNo.Text
            dtMemRow.Item("AUD") = "A"
            dtMemRow.Item("GUID") = Guid.NewGuid.ToString

            mdtMemTran.Rows.Add(dtMemRow)

            Call FillMemberShipList()
            Call ResetMemberShip()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddMembership_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Benefit Info "

    Private Sub btnDeleteBenefit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteBenefit.Click
        Try
            If lvBenefit.SelectedItems.Count > 0 Then
                If intBSelectedIdx > -1 Then


                    'Pinkal (20-Sep-2017) -- Start
                    'Enhancement - Working Dependant Benefit Plan Changes for PACRA.
                    If CInt(lvBenefit.Items(intBSelectedIdx).Tag) = -999 Then Exit Sub
                    'Pinkal (20-Sep-2017) -- End

                    Dim drTemp As DataRow()
                    If CInt(lvBenefit.Items(intBSelectedIdx).Tag) = -1 Then
                        drTemp = mdtBenefitTran.Select("GUID = '" & lvBenefit.Items(intBSelectedIdx).SubItems(objcolhBGUID.Index).Text & "'")
                    Else
                        drTemp = mdtBenefitTran.Select("dpndtbenefittranunkid = " & CInt(lvBenefit.Items(intBSelectedIdx).Tag))
                    End If

                    If IsPercentageProper(3) = False Then Exit Sub
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        drTemp(0).Item("isvoid") = True
                        drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                        drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        drTemp(0).Item("voidreason") = Language.getMessage(mstrModuleName, 22, "Incorrect Benefit Entry.")

                        'Pinkal (20-Sep-2017) -- Start
                        'Enhancement - Working Dependant Benefit Plan Changes for PACRA.
                        If mdtBenefitTran.Select("AUD <> 'D' AND dpndtbenefittranunkid <> -999 AND benefitgroupunkid = " & CInt(lvBenefit.Items(intBSelectedIdx).SubItems(objcolhBenefitGrpId.Index).Text)).Length <= 0 Then
                            Dim drRow As DataRow() = mdtBenefitTran.Select("dpndtbenefittranunkid = -999 AND benefitgroupunkid = " & CInt(lvBenefit.Items(intBSelectedIdx).SubItems(objcolhBenefitGrpId.Index).Text))
                            If drRow.Length > 0 Then
                                mdtBenefitTran.Rows.Remove(drRow(0))
                                mdtBenefitTran.AcceptChanges()
                            End If
                        End If



                        'Pinkal (20-Sep-2017) -- End


                        Call FillBenefitList()
                    End If
                End If
            End If
            Call ResetBenefits()
            cboBenefitGroup.Enabled = True
            cboBenefitType.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteBenefit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEditBenefit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditBenefit.Click
        Try
            If lvBenefit.SelectedItems.Count > 0 Then
                If intBSelectedIdx > -1 Then

                    'Pinkal (20-Sep-2017) -- Start
                    'Enhancement - Working Dependant Benefit Plan Changes for PACRA.
                    If CInt(lvBenefit.Items(intBSelectedIdx).Tag) = -999 Then Exit Sub
                    'Pinkal (20-Sep-2017) -- End

                    Dim drTemp As DataRow()
                    If CInt(lvBenefit.Items(intBSelectedIdx).Tag) = -1 Then
                        drTemp = mdtBenefitTran.Select("GUID = '" & lvBenefit.Items(intBSelectedIdx).SubItems(objcolhBGUID.Index).Text & "'")
                    Else
                        drTemp = mdtBenefitTran.Select("dpndtbenefittranunkid = " & CInt(lvBenefit.Items(intBSelectedIdx).Tag))
                    End If

                    'Sandeep [ 17 DEC 2010 ] -- Start
                    If IsPercentageProper(2) = False Then Exit Sub
                    'Sandeep [ 17 DEC 2010 ] -- End 

                    If drTemp.Length > 0 Then
                        With drTemp(0)

                            'Sandeep [ 17 DEC 2010 ] -- Start
                            '.Item("dpndtbenefittranunkid") = -1
                            .Item("dpndtbenefittranunkid") = CInt(lvBenefit.Items(intBSelectedIdx).Tag)
                            'Sandeep [ 17 DEC 2010 ] -- End 
                            .Item("dpndtbeneficetranunkid") = mintDependants_Benefice_Unkid
                            .Item("benefitgroupunkid") = CInt(cboBenefitGroup.SelectedValue)
                            .Item("benefitplanunkid") = CInt(cboBenefitType.SelectedValue)
                            .Item("value_id") = CInt(cboValueBasis.SelectedValue)
                            .Item("benefit_percent") = txtBenefitPercent.Decimal
                            .Item("benefit_amount") = txtBenefitAmount.Decimal
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            .Item("GUID") = Guid.NewGuid.ToString
                            .AcceptChanges()
                        End With
                        Call FillBenefitList()
                    End If
                End If
                Call ResetBenefits()
            End If
            'Sandeep [ 09 Oct 2010 ] -- Start
            'Issues Reported by Vimal
            cboBenefitGroup.Enabled = True
            cboBenefitType.Enabled = True
            'Sandeep [ 09 Oct 2010 ] -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditBenefit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAddBenefit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddBenefit.Click
        If CInt(cboBenefitGroup.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Benefit Group is compulsory information. Please select Benefit Group to continue."), enMsgBoxStyle.Information)
            cboBenefitGroup.Focus()
            Exit Sub
        End If

        If CInt(cboBenefitType.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Benefit is compulsory information. Please select Benefit to continue."), enMsgBoxStyle.Information)
            cboBenefitType.Focus()
            Exit Sub
        End If

        If CInt(cboValueBasis.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Benefit In is compulsory information. Please select Benefit In to continue."), enMsgBoxStyle.Information)
            cboValueBasis.Focus()
            Exit Sub
        End If

        Select Case CInt(cboValueBasis.SelectedValue)
            Case 1  'VALUE
                If txtBenefitAmount.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Amount cannot be blank .Amount is compulsory information."), enMsgBoxStyle.Information)
                    txtBenefitAmount.Focus()
                    Exit Sub
                End If

            Case 2  'PERCENTAGE
                If txtBenefitPercent.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Percent cannot be blank .Percent is compulsory information."), enMsgBoxStyle.Information)
                    txtBenefitPercent.Focus()
                    Exit Sub
                End If

                'Sandeep [ 17 DEC 2010 ] -- Start
                If txtBenefitPercent.Decimal > 100 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Percent cannot be greater than 100."), enMsgBoxStyle.Information)
                    txtBenefitPercent.Decimal = 0
                    txtBenefitPercent.Focus()
                    Exit Sub
                End If
                'Sandeep [ 17 DEC 2010 ] -- End 

        End Select

        Dim dtMRow As DataRow() = mdtBenefitTran.Select("benefitplanunkid = " & CInt(cboBenefitType.SelectedValue) & " AND AUD <> 'D' ")
        If dtMRow.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Selected Benefit is already added to the list."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Try

            'Sandeep [ 17 DEC 2010 ] -- Start
            If IsPercentageProper(1) = False Then Exit Sub
            'Sandeep [ 17 DEC 2010 ] -- End 


            Dim dtBenfRow As DataRow
            dtBenfRow = mdtBenefitTran.NewRow

            dtBenfRow.Item("dpndtbenefittranunkid") = -1
            dtBenfRow.Item("dpndtbeneficetranunkid") = mintDependants_Benefice_Unkid
            dtBenfRow.Item("benefitgroupunkid") = CInt(cboBenefitGroup.SelectedValue)
            dtBenfRow.Item("benefitplanunkid") = CInt(cboBenefitType.SelectedValue)
            dtBenfRow.Item("value_id") = CInt(cboValueBasis.SelectedValue)
            dtBenfRow.Item("benefit_percent") = txtBenefitPercent.Decimal
            dtBenfRow.Item("benefit_amount") = txtBenefitAmount.Decimal
            dtBenfRow.Item("AUD") = "A"
            dtBenfRow.Item("GUID") = Guid.NewGuid.ToString


            'Pinkal (20-Sep-2017) -- Start
            'Enhancement - Working Dependant Benefit Plan Changes for PACRA.
            dtBenfRow.Item("benefit_group") = cboBenefitGroup.Text
            dtBenfRow.Item("benefit_type") = cboBenefitType.Text
            'Pinkal (20-Sep-2017) -- End


            mdtBenefitTran.Rows.Add(dtBenfRow)

            Call FillBenefitList()
            Call ResetBenefits()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddBenefit_Click", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sandeep [ 21 Aug 2010 ] -- End

#End Region

#Region " Controls "

    Private Sub cboDBPostTown_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDBPostTown.SelectedIndexChanged
        Dim objZipcode As New clszipcode_master
        Dim dsCombos As New DataSet
        Try
            If CInt(cboDBPostTown.SelectedValue) > 0 Then
                dsCombos = objZipcode.GetList("Zipcode", True, True, CInt(cboDBPostTown.SelectedValue))
                With cboPostCode
                    .ValueMember = "zipcodeunkid"
                    .DisplayMember = "zipcode_no"
                    .DataSource = dsCombos.Tables("Zipcode")
                    'Sandeep [ 14 Aug 2010 ] -- Start
                    '.SelectedValue = 0
                    If mintDependants_Benefice_Unkid = -1 Then
                        .SelectedValue = 0
                    Else
                        .SelectedValue = objDependants_Benefice._Zipcodeunkid
                    End If
                    'Sandeep [ 14 Aug 2010 ] -- End 
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDBPostTown_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPostState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPostState.SelectedIndexChanged
        Dim objCity As New clscity_master
        Dim dsCombos As New DataSet
        Try
            If CInt(cboPostState.SelectedValue) > 0 Then
                dsCombos = objCity.GetList("City", True, True, CInt(cboPostState.SelectedValue))
                With cboDBPostTown
                    .ValueMember = "cityunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("City")
                    'Sandeep [ 14 Aug 2010 ] -- Start
                    '.SelectedValue = 0
                    If mintDependants_Benefice_Unkid = -1 Then
                        .SelectedValue = 0
                    Else
                        .SelectedValue = objDependants_Benefice._Cityunkid
                    End If
                    'Sandeep [ 14 Aug 2010 ] -- End 
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPostState_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDBPostCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDBPostCountry.SelectedIndexChanged
        Dim objState As New clsstate_master
        Dim dsCombos As New DataSet
        Try
            If CInt(cboDBPostCountry.SelectedValue) > 0 Then
                dsCombos = objState.GetList("State", True, True, CInt(cboDBPostCountry.SelectedValue))
                With cboPostState
                    .ValueMember = "stateunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("State")
                    'Sandeep [ 14 Aug 2010 ] -- Start
                    '.SelectedValue = 0
                    If mintDependants_Benefice_Unkid = -1 Then
                        .SelectedValue = 0
                    Else
                        .SelectedValue = objDependants_Benefice._Stateunkid
                    End If
                    'Sandeep [ 14 Aug 2010 ] -- End 
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDBPostCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub


    'Sandeep [ 17 DEC 2010 ] -- Start
    'Private Sub cboEmployee_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectionChangeCommitted
    '    Try
    '        mintSelectedEmployee = CInt(cboEmployee.SelectedValue)
    '        'Sandeep [ 17 DEC 2010 ] -- Start
    '        Call objDependants_Benefice.GetAllocate_Percent(CInt(cboEmployee.SelectedValue), intTotalBeneficiary, dblTotalPercentage)
    '        txtTotBeneficiaries.Decimal = intTotalBeneficiary
    '        txtTotalPercentage.Decimal = dblTotalPercentage
    '        txtRemainingPercent.Decimal = cdec(100) - dblTotalPercentage
    '        'Sandeep [ 17 DEC 2010 ] -- End 
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboEmployee_SelectionChangeCommitted", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub cboEmployee_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedValueChanged
        Try



            'Gajanan [25-May-2019] -- Start              
            If IsNothing(cboEmployee.SelectedValue) = False AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
                'Gajanan [25-May-2019] -- End

            mintSelectedEmployee = CInt(cboEmployee.SelectedValue)



            'Sandeep [ 17 DEC 2010 ] -- Start

            'Pinkal (20-Sep-2017) -- Start
            'Enhancement - Working Dependant Benefit Plan Changes for PACRA.
            'Call objDependants_Benefice.GetAllocate_Percent(CInt(cboEmployee.SelectedValue), intTotalBeneficiary, dblTotalPercentage)
            'txtTotBeneficiaries.Decimal = intTotalBeneficiary
            'txtTotalPercentage.Double = dblTotalPercentage
            'txtRemainingPercent.Double = CDbl(100) - dblTotalPercentage
            'Pinkal (20-Sep-2017) -- End


            'Sandeep [ 17 DEC 2010 ] -- End 

            Dim itmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid= '" & CInt(cboEmployee.SelectedValue) & "'")
            If itmp.Length > 0 Then
                isEmployeeApprove = CBool(itmp(0).Item("isapproved").ToString())
            Else
                isEmployeeApprove = False
            End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 17 DEC 2010 ] -- End 




    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub txtBenefitAmount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBenefitAmount.TextChanged
        Try
            If txtBenefitAmount.Decimal > 0 Then
                txtBenefitPercent.Decimal = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtBenefitAmount_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtBenefitPercent_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBenefitPercent.TextChanged
        Try
            If txtBenefitPercent.Decimal > 100 Then

                'Pinkal (20-Sep-2017) -- Start
                'Enhancement - Working Dependant Benefit Plan Changes for PACRA.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Benefit Percentage cannot be greater than 100 Percent. Please enter correct Benefit Percent."), enMsgBoxStyle.Information)
                'Pinkal (20-Sep-2017) -- End

                'Sandeep [ 17 DEC 2010 ] -- Start
                'txtBenefitAmount.Decimal = 0
                txtBenefitPercent.Decimal = 0
                'Sandeep [ 17 DEC 2010 ] -- End 
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtBenefitPercent_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboBenefitGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBenefitGroup.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objBenefit As New clsbenefitplan_master
        Try
            If CInt(cboBenefitGroup.SelectedValue) > 0 Then
                dsCombos = objBenefit.getComboList("Benefit", True, CInt(cboBenefitGroup.SelectedValue))
                With cboBenefitType
                    .ValueMember = "benefitplanunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Benefit")
                    .SelectedValue = 0
                End With

                'Pinkal (20-Sep-2017) -- Start
                'Enhancement - Working Dependant Benefit Plan Changes for PACRA. 
                If mdtBenefitTran IsNot Nothing AndAlso mdtBenefitTran.Rows.Count > 0 Then
                    If Not IsDBNull(mdtBenefitTran.Compute("SUM(benefit_percent)", "dpndtbenefittranunkid <> -999 AND AUD <> 'D' AND benefitgroupunkid = " & CInt(cboBenefitGroup.SelectedValue))) Then
                        txtTotalPercentage.Decimal = Convert.ToDecimal(mdtBenefitTran.Compute("SUM(benefit_percent)", "dpndtbenefittranunkid <> -999 AND AUD <> 'D' AND benefitgroupunkid = " & CInt(cboBenefitGroup.SelectedValue)))
            End If
                End If
                txtRemainingPercent.Decimal = 100 - txtTotalPercentage.Decimal
            Else
                txtTotalPercentage.Decimal = 0
                txtRemainingPercent.Decimal = 0
                'Pinkal (20-Sep-2017) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBenefitGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 21 Aug 2010 ] -- Start
    Private Sub cboMemCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMemCategory.SelectedIndexChanged
        Try
            If CInt(cboMemCategory.SelectedValue) > 0 Then
                Dim dsCombos As New DataSet
                Dim objMembership As New clsmembership_master
                dsCombos = objMembership.getListForCombo("Membership", True, CInt(cboMemCategory.SelectedValue))
                With cboMedicalNo
                    .ValueMember = "membershipunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Membership")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMemCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvMembershipInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvMembershipInfo.Click
        Try
            If lvMembershipInfo.SelectedItems.Count > 0 Then
                intMSelectedIdx = lvMembershipInfo.SelectedItems(0).Index
                cboMemCategory.SelectedValue = 0
                cboMedicalNo.SelectedValue = 0

                cboMemCategory.SelectedValue = CInt(lvMembershipInfo.SelectedItems(0).SubItems(objcolhMembershipCatId.Index).Text)
                cboMedicalNo.SelectedValue = CInt(lvMembershipInfo.SelectedItems(0).SubItems(objcolhMembershipUnkid.Index).Text)
                txtMembershipNo.Text = CStr(lvMembershipInfo.SelectedItems(0).SubItems(colhMembershipNo.Index).Text)

                'Sandeep [ 09 Oct 2010 ] -- Start
                'Issues Reported by Vimal
                cboMemCategory.Enabled = False
                cboMedicalNo.Enabled = False
            Else
                cboMemCategory.Enabled = True
                cboMedicalNo.Enabled = True
                'Sandeep [ 09 Oct 2010 ] -- End 
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMembershipInfo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lvBenefit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvBenefit.Click
        Try
            If lvBenefit.SelectedItems.Count > 0 Then
                intBSelectedIdx = lvBenefit.SelectedItems(0).Index
                cboBenefitGroup.SelectedValue = 0
                cboBenefitType.SelectedValue = 0
                cboValueBasis.SelectedValue = 0


                cboBenefitGroup.SelectedValue = CInt(lvBenefit.SelectedItems(0).SubItems(objcolhBenefitGrpId.Index).Text)
                cboBenefitType.SelectedValue = CInt(lvBenefit.SelectedItems(0).SubItems(objcolhBenefitId.Index).Text)
                cboValueBasis.SelectedValue = CInt(lvBenefit.SelectedItems(0).SubItems(objcolhValueBasisId.Index).Text)
                txtBenefitPercent.Text = CStr(lvBenefit.SelectedItems(0).SubItems(colhPercent.Index).Text)
                txtBenefitAmount.Text = CStr(lvBenefit.SelectedItems(0).SubItems(colhAmount.Index).Text)


                'Sandeep [ 17 DEC 2010 ] -- Start
                dblAddedPercent = txtBenefitPercent.Decimal
                'Sandeep [ 17 DEC 2010 ] -- End


                'Sandeep [ 09 Oct 2010 ] -- Start
                'Issues Reported by Vimal
                cboBenefitGroup.Enabled = False
                cboBenefitType.Enabled = False
            Else
                cboBenefitGroup.Enabled = True
                cboBenefitType.Enabled = True
                'Sandeep [ 09 Oct 2010 ] -- End 
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvBenefit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub cboValueBasis_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboValueBasis.SelectedValueChanged
        Try

            mintSeletedValue = CInt(cboValueBasis.SelectedValue)

            Select Case CInt(cboValueBasis.SelectedValue)
                Case 1  'Value
                    txtBenefitPercent.Decimal = 0
                    txtBenefitPercent.Enabled = False
                    txtBenefitAmount.Enabled = True
                Case 2  'Percent
                    txtBenefitAmount.Decimal = 0
                    txtBenefitAmount.Enabled = False
                    txtBenefitPercent.Enabled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboValueBasis_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkCopyAddress_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopyAddress.LinkClicked
        Try
            Dim objEmp As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            'S.SANDEEP [04 JUN 2015] -- END
            txtAddressLine1.Text = objEmp._Present_Address1 & " " & objEmp._Present_Address2
            cboDBPostCountry.SelectedValue = objEmp._Present_Countryunkid
            cboPostState.SelectedValue = objEmp._Present_Stateunkid
            cboDBPostTown.SelectedValue = objEmp._Present_Post_Townunkid
            cboPostCode.SelectedValue = objEmp._Present_Postcodeunkid
            cboDBNationality.SelectedValue = objEmp._Nationalityunkid
            txtDBTelNo.Text = objEmp._Present_Tel_No
            txtDBPersonalNo.Text = objEmp._Present_Mobile
            objEmp = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCopyAddress_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 21 Aug 2010 ] -- End 


    'Sandeep [ 30 September 2010 ] -- Start
    Private Sub objbtnAddReminder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReminder.Click
        Try
            Dim frm As New frmReminder_AddEdit
            'Sandeep [ 09 Oct 2010 ] -- Start
            'Issues Reported by Vimal
            frm._TitleName = Language.getMessage(mstrModuleName, 18, "Birthday Reminder")
            Dim dtDate As String = Now.Year & dtpDBBirthDate.Value.Month.ToString("0#") & dtpDBBirthDate.Value.Day.ToString("0#")
            'Sandeep [ 09 Oct 2010 ] -- End 
            frm._StartDate = CDate(eZeeDate.convertDate(dtDate) & " " & Format(Now, "hh:mm:ss tt"))
            frm.displayDialog(-1, enAction.ADD_ONE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 30 September 2010 ] -- End

    'Sandeep [ 15 DEC 2010 ] -- Start
    'Issue : Mr. Rutta's Comment
    Private Sub objAddMedMem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAddMedMem.Click
        Dim frm As New frmMembership_AddEdit
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objMemShip As New clsmembership_master
                dsList = objMemShip.getListForCombo("MEMSHIP", True, CInt(cboMemCategory.SelectedValue))
                With cboMedicalNo
                    .ValueMember = "membershipunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("MEMSHIP")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objMemShip = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAddMedMem_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddMemCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddMemCategory.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, True, "MAMCAT")
                With cboMemCategory
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("MAMCAT")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddMemCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddRelation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddRelation.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.RELATIONS, enAction.ADD_ONE)
            If intRefId > 0 Then
                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If ConfigParameter._Object._IsDependant_AgeLimit_Set Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Do you want to set the age limit for new relation now?"), _
                                       CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim ofrm As New frmSetDependantAgeLimit

                        If User._Object._Isrighttoleft = True Then
                            ofrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            ofrm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(ofrm)
                        End If
                        ofrm.displayDialog(intRefId)
                    End If
                End If
                'S.SANDEEP [ 07 NOV 2011 ] -- END

                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation")
                With cboDBRelation
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Relation")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddRelation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddBenefitGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddBenefitGroup.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.BENEFIT_GROUP, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.BENEFIT_GROUP, True, "BENGRP")
                With cboBenefitGroup
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("BENGRP")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddBenefitGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddBenefitType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddBenefitType.Click
        Dim frm As New frmBenefitplan_AddEdit
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objBenPlan As New clsbenefitplan_master
                dsList = objBenPlan.getComboList("BENP", True, CInt(cboBenefitGroup.SelectedValue))
                With cboBenefitType
                    .ValueMember = "benefitplanunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("BENP")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objBenPlan = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddBenefitType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sandeep [ 15 DEC 2010 ] -- End 

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboDBRelation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDBRelation.SelectedIndexChanged
        Try
            If CInt(cboDBRelation.SelectedValue) > 0 Then
                Dim objCMaster As New clsCommon_Master
                objCMaster._Masterunkid = CInt(cboDBRelation.SelectedValue)
                txtAgeLimit.Text = CStr(objCMaster._Dependant_Limit)
                objCMaster = Nothing
            Else
                txtAgeLimit.Text = CStr(0)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDBRelation_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dtpDBBirthDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDBBirthDate.ValueChanged
        Try
            'S.SANDEEP [02 JAN 2017] -- START
            'ENHANCEMENT : GETTING YEAR,MONTH,DAY DIFFERENCE
            'txtAge.Text = CStr(DateDiff(DateInterval.Year, dtpDBBirthDate.Value.Date, ConfigParameter._Object._CurrentDateAndTime.Date))
            Dim objMst As New clsMasterData
            Dim iYearId, iMonthId, iDayId As Integer : iYearId = 0 : iMonthId = 0 : iDayId = 0
            objMst.GetDateDifferenceYMD(ConfigParameter._Object._CurrentDateAndTime.Date, dtpDBBirthDate.Value.Date, iYearId, iMonthId, iDayId)
            objlblAge.Text = Language.getMessage(mstrModuleName, 42, "Age In") & " " & Language.getMessage(mstrModuleName, 43, "Year:") & " " & iYearId.ToString & ", " & Language.getMessage(mstrModuleName, 44, "Month:") & " " & iMonthId.ToString
            objMst = Nothing
            'S.SANDEEP [02 JAN 2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpDBBirthDate_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 07 NOV 2011 ] -- END
    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Private Sub dgvDepedantAttachment_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDepedantAttachment.CellContentClick
        Try
            If e.RowIndex >= 0 Then
                Dim xrow() As DataRow

                If CInt(dgvDepedantAttachment.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) > 0 Then
                    xrow = mdtDependants.Select("scanattachtranunkid = " & CInt(dgvDepedantAttachment.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) & "")
                Else
                    xrow = mdtDependants.Select("GUID = '" & dgvDepedantAttachment.Rows(e.RowIndex).Cells(objcolhGUID.Index).Value.ToString & "'")
                End If

                If e.ColumnIndex = objcohDelete.Index Then
                    If xrow.Length > 0 Then
                        If MessageBox.Show(Language.getMessage(mstrModuleName, 39, "Are you sure you want to delete this attachment?"), "Aruti", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                            xrow(0).Item("AUD") = "D"
                            Call fillDependantAttachment()
                        End If
                    End If
                ElseIf e.ColumnIndex = objcolhDownload.Index Then
                    Dim xPath As String = ""

                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("file_path").ToString
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If

                    Dim finfo As New IO.FileInfo(xPath)

                    sfdAttachment.Filter = "File Format (" & finfo.Extension & ")|*" & finfo.Extension & ""
                    sfdAttachment.FileName = xrow(0).Item("filename").ToString

                    If sfdAttachment.ShowDialog = Windows.Forms.DialogResult.OK Then

                        'SHANI (16 JUL 2015) -- Start
                        'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                        'Upload Image folder to access those attachemts from Server and 
                        'all client machines if ArutiSelfService is installed otherwise save then on Document path
                        'IO.File.Copy(xPath, sfdAttachment.FileName, True)
                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                            Dim strError As String = ""
                            If blnIsIISInstalled Then

                                'Shani(24-Aug-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'Dim filebytes As Byte() = clsFileUploadDownload.DownloadFile(xrow(0).Item("filepath").ToString, xrow(0).Item("fileuniquename").ToString, mstrFolderName, strError)
                                Dim filebytes As Byte() = clsFileUploadDownload.DownloadFile(xrow(0).Item("filepath").ToString, xrow(0).Item("fileuniquename").ToString, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)
                                'Shani(24-Aug-2015) -- End

                                If strError <> "" Then
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.OkOnly)
                                    Exit Sub
                                End If

                                'SHANI (27 JUL 2015) -- Start
                                'Enhancement - 
                                If filebytes IsNot Nothing Then
                                    Dim ms As New MemoryStream(filebytes)
                                    Dim fs As New FileStream(sfdAttachment.FileName, FileMode.Create)
                                    ms.WriteTo(fs)

                                    ms.Close()
                                    fs.Close()
                                    fs.Dispose()
                                Else
                                    If System.IO.File.Exists(xrow(0).Item("filepath").ToString) Then
                                        xPath = xrow(0).Item("filepath").ToString

                                        Dim strFileUniqueName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & "_" & DateTime.Now.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(CStr(xrow(0).Item("filepath").ToString))

                                        'Shani(24-Aug-2015) -- Start
                                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                        'If clsFileUploadDownload.UploadFile(xrow(0).Item("filepath").ToString, mstrFolderName, strFileUniqueName, strError) = False Then
                                        If clsFileUploadDownload.UploadFile(xrow(0).Item("filepath").ToString, mstrFolderName, strFileUniqueName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                            'Shani(24-Aug-2015) -- End

                                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                            Exit Try
                                        Else
                                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                                strPath += "/"
                                            End If
                                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileUniqueName
                                            xrow(0).Item("AUD") = "U"
                                            xrow(0).Item("fileuniquename") = strFileUniqueName
                                            xrow(0).Item("filepath") = strPath
                                            xrow(0).Item("filesize") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).Item("filesize_kb") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).AcceptChanges()
                                            Dim objScanAttach As New clsScan_Attach_Documents
                                            objScanAttach._Datatable = mdtDependants
                                            With objScanAttach
                                                ._FormName = mstrModuleName
                                                ._LoginEmployeeunkid = 0
                                                ._ClientIP = getIP()
                                                ._HostName = getHostName()
                                                ._FromWeb = False
                                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                            End With
                                            objScanAttach.InsertUpdateDelete_Documents()
                                            If objDocument._Message <> "" Then
                                                objScanAttach = Nothing
                                                eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
                                                Exit Try
                                            End If
                                            objScanAttach = Nothing
                                            xrow(0).Item("AUD") = ""
                                            xrow(0).AcceptChanges()
                                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                                        End If
                                    Else
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 45, "sorry, file you are trying to access does not exists on Aruti self service application folder."), enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                End If
                                'SHANI (27 JUL 2015) -- End 
                            Else
                                IO.File.Copy(xPath, sfdAttachment.FileName, True)
                            End If
                        Else
                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                        End If
                        'SHANI (16 JUL 2015) -- End 
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDepedantAttachment_CellContentClick :", mstrModuleName)
        End Try
    End Sub
    'SHANI (20 JUN 2015) -- End 
#End Region


    'Pinkal (20-Sep-2017) -- Start
    'Enhancement - Working Dependant Benefit Plan Changes for PACRA.

#Region "ListView Events"

    Private Sub lvBenefit_ItemSelectionChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvBenefit.ItemSelectionChanged
        Try
            If e.ItemIndex < 0 Then Exit Sub
            If e.Item.BackColor = Color.Blue Then
                ResetBenefits()
                e.Item.Selected = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvBenefit_ItemSelectionChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvBenefit_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvBenefit.SelectedIndexChanged
        Try
            If lvBenefit.SelectedItems.Count <= 0 Then
                ResetBenefits()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvBenefit_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (20-Sep-2017) -- End





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbDBInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDBInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbAddMembership.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAddMembership.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbBenefitInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBenefitInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.dbDocumentAttachment.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.dbDocumentAttachment.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditMembership.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditMembership.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddMembership.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddMembership.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteBenefit.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteBenefit.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditBenefit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditBenefit.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddBenefit.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddBenefit.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddAttachment.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddAttachment.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.gbDBInfo.Text = Language._Object.getCaption(Me.gbDBInfo.Name, Me.gbDBInfo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblDBResidentialNo.Text = Language._Object.getCaption(Me.lblDBResidentialNo.Name, Me.lblDBResidentialNo.Text)
            Me.lblDBMobileNo.Text = Language._Object.getCaption(Me.lblDBMobileNo.Name, Me.lblDBMobileNo.Text)
            Me.lblDBAddress.Text = Language._Object.getCaption(Me.lblDBAddress.Name, Me.lblDBAddress.Text)
            Me.lblDBIdNo.Text = Language._Object.getCaption(Me.lblDBIdNo.Name, Me.lblDBIdNo.Text)
            Me.lblDBEmail.Text = Language._Object.getCaption(Me.lblDBEmail.Name, Me.lblDBEmail.Text)
            Me.lblDBPostCountry.Text = Language._Object.getCaption(Me.lblDBPostCountry.Name, Me.lblDBPostCountry.Text)
            Me.lblDBNationality.Text = Language._Object.getCaption(Me.lblDBNationality.Name, Me.lblDBNationality.Text)
            Me.lblDBPostCode.Text = Language._Object.getCaption(Me.lblDBPostCode.Name, Me.lblDBPostCode.Text)
            Me.lblDBPostTown.Text = Language._Object.getCaption(Me.lblDBPostTown.Name, Me.lblDBPostTown.Text)
            Me.lblDBPostBox.Text = Language._Object.getCaption(Me.lblDBPostBox.Name, Me.lblDBPostBox.Text)
            Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
            Me.imgDependants.Text = Language._Object.getCaption(Me.imgDependants.Name, Me.imgDependants.Text)
            Me.lblBenefitGroup.Text = Language._Object.getCaption(Me.lblBenefitGroup.Name, Me.lblBenefitGroup.Text)
            Me.lblDBBenefitType.Text = Language._Object.getCaption(Me.lblDBBenefitType.Name, Me.lblDBBenefitType.Text)
            Me.lblBenefitsInPercent.Text = Language._Object.getCaption(Me.lblBenefitsInPercent.Name, Me.lblBenefitsInPercent.Text)
            Me.lblDBBenefitInAmount.Text = Language._Object.getCaption(Me.lblDBBenefitInAmount.Name, Me.lblDBBenefitInAmount.Text)
            Me.lblDBFirstName.Text = Language._Object.getCaption(Me.lblDBFirstName.Name, Me.lblDBFirstName.Text)
            Me.lblDBMiddleName.Text = Language._Object.getCaption(Me.lblDBMiddleName.Name, Me.lblDBMiddleName.Text)
            Me.lblDBLastName.Text = Language._Object.getCaption(Me.lblDBLastName.Name, Me.lblDBLastName.Text)
            Me.lblDBRelation.Text = Language._Object.getCaption(Me.lblDBRelation.Name, Me.lblDBRelation.Text)
            Me.lblDBBirthDate.Text = Language._Object.getCaption(Me.lblDBBirthDate.Name, Me.lblDBBirthDate.Text)
            Me.tabpMembership.Text = Language._Object.getCaption(Me.tabpMembership.Name, Me.tabpMembership.Text)
            Me.tabpBenefitInfo.Text = Language._Object.getCaption(Me.tabpBenefitInfo.Name, Me.tabpBenefitInfo.Text)
            Me.lblMemberNo.Text = Language._Object.getCaption(Me.lblMemberNo.Name, Me.lblMemberNo.Text)
            Me.lblDBMedicalMembershipNo.Text = Language._Object.getCaption(Me.lblDBMedicalMembershipNo.Name, Me.lblDBMedicalMembershipNo.Text)
            Me.chkBeneficiaries.Text = Language._Object.getCaption(Me.chkBeneficiaries.Name, Me.chkBeneficiaries.Text)
            Me.lnkCopyAddress.Text = Language._Object.getCaption(Me.lnkCopyAddress.Name, Me.lnkCopyAddress.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEditMembership.Text = Language._Object.getCaption(Me.btnEditMembership.Name, Me.btnEditMembership.Text)
            Me.btnAddMembership.Text = Language._Object.getCaption(Me.btnAddMembership.Name, Me.btnAddMembership.Text)
            Me.gbAddMembership.Text = Language._Object.getCaption(Me.gbAddMembership.Name, Me.gbAddMembership.Text)
            Me.colhMembership.Text = Language._Object.getCaption(CStr(Me.colhMembership.Tag), Me.colhMembership.Text)
            Me.colhMembershipNo.Text = Language._Object.getCaption(CStr(Me.colhMembershipNo.Tag), Me.colhMembershipNo.Text)
            Me.gbBenefitInfo.Text = Language._Object.getCaption(Me.gbBenefitInfo.Name, Me.gbBenefitInfo.Text)
            Me.lblValueBasis.Text = Language._Object.getCaption(Me.lblValueBasis.Name, Me.lblValueBasis.Text)
            Me.btnDeleteBenefit.Text = Language._Object.getCaption(Me.btnDeleteBenefit.Name, Me.btnDeleteBenefit.Text)
            Me.btnEditBenefit.Text = Language._Object.getCaption(Me.btnEditBenefit.Name, Me.btnEditBenefit.Text)
            Me.btnAddBenefit.Text = Language._Object.getCaption(Me.btnAddBenefit.Name, Me.btnAddBenefit.Text)
            Me.colhBenefitGroup.Text = Language._Object.getCaption(CStr(Me.colhBenefitGroup.Tag), Me.colhBenefitGroup.Text)
            Me.colhBenefitType.Text = Language._Object.getCaption(CStr(Me.colhBenefitType.Tag), Me.colhBenefitType.Text)
            Me.colhValueBasis.Text = Language._Object.getCaption(CStr(Me.colhValueBasis.Tag), Me.colhValueBasis.Text)
            Me.colhPercent.Text = Language._Object.getCaption(CStr(Me.colhPercent.Tag), Me.colhPercent.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)
            Me.lblMembershipCategory.Text = Language._Object.getCaption(Me.lblMembershipCategory.Name, Me.lblMembershipCategory.Text)
            Me.colhMembershipType.Text = Language._Object.getCaption(CStr(Me.colhMembershipType.Tag), Me.colhMembershipType.Text)
            Me.lblInfo.Text = Language._Object.getCaption(Me.lblInfo.Name, Me.lblInfo.Text)
            Me.lblLimit.Text = Language._Object.getCaption(Me.lblLimit.Name, Me.lblLimit.Text)
            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
            Me.tabAttachment.Text = Language._Object.getCaption(Me.tabAttachment.Name, Me.tabAttachment.Text)
            Me.dbDocumentAttachment.Text = Language._Object.getCaption(Me.dbDocumentAttachment.Name, Me.dbDocumentAttachment.Text)
            Me.colhName.HeaderText = Language._Object.getCaption(Me.colhName.Name, Me.colhName.HeaderText)
            Me.colhSize.HeaderText = Language._Object.getCaption(Me.colhSize.Name, Me.colhSize.HeaderText)
            Me.btnAddAttachment.Text = Language._Object.getCaption(Me.btnAddAttachment.Name, Me.btnAddAttachment.Text)
            Me.lblDocumentType.Text = Language._Object.getCaption(Me.lblDocumentType.Name, Me.lblDocumentType.Text)
            Me.lblAttachmentDate.Text = Language._Object.getCaption(Me.lblAttachmentDate.Name, Me.lblAttachmentDate.Text)
            Me.lblTotalRemainingPercent.Text = Language._Object.getCaption(Me.lblTotalRemainingPercent.Name, Me.lblTotalRemainingPercent.Text)
            Me.lblTotalPercent.Text = Language._Object.getCaption(Me.lblTotalPercent.Name, Me.lblTotalPercent.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Firstname cannot be blank. Firstname is compulsory information.")
            Language.setMessage(mstrModuleName, 3, "Lastname cannot be blank. Lastname is compulsory information.")
            Language.setMessage(mstrModuleName, 4, "Relation is compulsory information. Please select relation to continue.")
            Language.setMessage(mstrModuleName, 5, "Invalid Email.Please Enter Valid Email")
            Language.setMessage(mstrModuleName, 6, "Selected information is already present for particular employee.")
            Language.setMessage(mstrModuleName, 7, "Benefit is compulsory information. Please select Benefit to continue.")
            Language.setMessage(mstrModuleName, 8, "Benefit In is compulsory information. Please select Benefit In to continue.")
            Language.setMessage(mstrModuleName, 9, "Amount cannot be blank .Amount is compulsory information.")
            Language.setMessage(mstrModuleName, 10, "Percent cannot be blank .Percent is compulsory information.")
            Language.setMessage(mstrModuleName, 11, "Percent cannot be greater than 100.")
            Language.setMessage(mstrModuleName, 12, "Selected Benefit is already added to the list.")
            Language.setMessage(mstrModuleName, 13, "Membership is compulsory information. Please select Membership to continue.")
            Language.setMessage(mstrModuleName, 14, "Membership No. cannot be blank. Membership No. is compulsory information.")
            Language.setMessage(mstrModuleName, 15, "Membership Category is compulsory information. Please select Membership Category to continue.")
            Language.setMessage(mstrModuleName, 16, "Selected Membership is already added to the list.")
            Language.setMessage(mstrModuleName, 18, "Birthday Reminder")
            Language.setMessage(mstrModuleName, 19, "Date of birth cannot be greater than or equal to today's date.")
            Language.setMessage(mstrModuleName, 20, "Sorry, particular person cannot be consider as dependants or beneficairy. Reason : the age is greater than the limit set.")
            Language.setMessage(mstrModuleName, 21, "Benefit Percentage cannot be greater than 100 Percent.")
            Language.setMessage(mstrModuleName, 22, "Incorrect Benefit Entry.")
            Language.setMessage(mstrModuleName, 23, "Do you want to set the age limit for new relation now?")
            Language.setMessage(mstrModuleName, 24, "Field")
            Language.setMessage(mstrModuleName, 25, "Old Value")
            Language.setMessage(mstrModuleName, 26, "New Value")
            Language.setMessage(mstrModuleName, 27, "Dependant Full Name")
            Language.setMessage(mstrModuleName, 28, "Birth Date")
            Language.setMessage(mstrModuleName, 29, "Relation")
            Language.setMessage(mstrModuleName, 30, "Gender")
            Language.setMessage(mstrModuleName, 31, "Employee :")
            Language.setMessage(mstrModuleName, 32, "Depandant :")
            Language.setMessage(mstrModuleName, 33, "Sorry,You cannot upload file greater than")
            Language.setMessage(mstrModuleName, 34, " MB.")
            Language.setMessage(mstrModuleName, 35, "Notification of Changes in Employee Dependant(s).")
            Language.setMessage(mstrModuleName, 36, "Notifications to newly added Employee Dependant(s).")
            Language.setMessage(mstrModuleName, 37, "Benefit Group is compulsory information. Please select Benefit Group to continue.")
            Language.setMessage(mstrModuleName, 38, "Document Type is compulsory information. Please select Document Type to continue.")
            Language.setMessage(mstrModuleName, 39, "Are you sure you want to delete this attachment?")
            Language.setMessage(mstrModuleName, 40, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation.")
            Language.setMessage(mstrModuleName, 41, "Configuration Path does not Exist.")
            Language.setMessage(mstrModuleName, 42, "Age In")
            Language.setMessage(mstrModuleName, 43, "Year:")
            Language.setMessage(mstrModuleName, 44, "Month:")
            Language.setMessage(mstrModuleName, 45, "sorry, file you are trying to access does not exists on Aruti self service application folder.")
            Language.setMessage(mstrModuleName, 46, "Total")
			Language.setMessage(mstrModuleName, 47, "This is to inform you that changes have been made for employee dependant(s)")
			Language.setMessage(mstrModuleName, 48, "Please set effective date.")
			Language.setMessage(mstrModuleName, 49, "Effective date should not be greater than current date.")
			Language.setMessage("frmEmployeeMaster", 144, "Following information has been changed by user")
			Language.setMessage("frmEmployeeMaster", 145, "from Machine")
			Language.setMessage("frmEmployeeMaster", 146, "and IPAddress")
			Language.setMessage("frmEmployeeMaster", 147, "Dear")
			Language.setMessage(mstrModuleName, 1001, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in edit mode.")
			Language.setMessage(mstrModuleName, 1002, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in edit mode.")
			Language.setMessage(mstrModuleName, 1003, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in edit mode.")
			Language.setMessage(mstrModuleName, 1004, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in edit mode.")
			Language.setMessage(mstrModuleName, 1005, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in deleted mode.")
			Language.setMessage(mstrModuleName, 1006, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in deleted mode.")
			Language.setMessage(mstrModuleName, 1007, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in deleted mode.")
			Language.setMessage(mstrModuleName, 1008, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in deleted mode.")
			Language.setMessage(mstrModuleName, 1009, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in added mode.")
			Language.setMessage(mstrModuleName, 1010, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in added mode.")
			Language.setMessage(mstrModuleName, 1011, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in added mode.")
			Language.setMessage(mstrModuleName, 1012, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in added mode.")
			Language.setMessage(mstrModuleName, 1013, "This Dependant is already defined. Please define new Dependant.")
			Language.setMessage(mstrModuleName, 1014, "This Beneficiary is already defined. Please define new Beneficiary.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class