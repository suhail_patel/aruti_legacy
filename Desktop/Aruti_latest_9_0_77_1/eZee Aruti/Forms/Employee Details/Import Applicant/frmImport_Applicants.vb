﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

#End Region

Public Class frmImport_Applicants

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmImport_Applicants"
    Dim objImportApplicant As clsImportApplicant
    Dim mdtTran As DataTable
    Private objImport As clsImportApplicant

    Private objApplicant As clsApplicant_master
    Private objGLevel As clsGradeLevel
    Private objSalaryScale As clsWagesTran
    Private objSalaryGrade As clsGrade


    'Pinkal (02-Oct-2012) -- Start
    'Enhancement : TRA Changes
    Dim sMsg As String = String.Empty
    Dim mstrOriginal As String = String.Empty
    'Pinkal (02-Oct-2012) -- End


#End Region

#Region " Form's Events "

    Private Sub frmImport_Applicants_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objApplicant = Nothing
            'objSalaryHead = Nothing
            'objGradeGrp = Nothing
            objSalaryGrade = Nothing
            objGLevel = Nothing
            objSalaryScale = Nothing
            objImport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImport_Applicants_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmImport_Applicants_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objApplicant = New clsApplicant_master
        'objSalaryHead = New clsTransactionHead
        'objGradeGrp = New clsGradeGroup
        objSalaryGrade = New clsGrade
        objGLevel = New clsGradeLevel
        objSalaryScale = New clsWagesTran
        objImport = New clsImportApplicant
        Try

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()


            'Pinkal (01-Dec-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._DisplayNameSetting <> enDisplayNameSetting.NONE Then

                If ConfigParameter._Object._DisplayNameSetting = enDisplayNameSetting.EMPLOYEECODE Then
                    dgcolhDisplayName.Visible = False
                    dgcolhEmail.Visible = False
                Else
                    dgcolhDisplayName.Visible = True
                    dgcolhEmail.Visible = True
                End If
            Else
                dgcolhDisplayName.Visible = False
                dgcolhEmail.Visible = False
            End If

            'Pinkal (01-Dec-2012) -- End

            'Sohail (10 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            If ConfigParameter._Object._RetirementBy = 1 Then
                dtpRetirementDate.Enabled = False
            Else
                dtpRetirementDate.Enabled = True
            End If
            'Sohail (10 Apr 2013) -- End

            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            Dim objHead As New clsTransactionHead
            Dim dsHead As DataSet = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", False, 0, 0, -1, True, True, "typeof_id <> " & enTypeOf.Salary & " AND isdefault = 1 ", False, False, False, 2)
            If dsHead.Tables(0).Rows.Count > 0 Then
                chkAssignDefaulTranHeads.Checked = True
                chkAssignDefaulTranHeads.Visible = True
            Else
                chkAssignDefaulTranHeads.Checked = False
                chkAssignDefaulTranHeads.Visible = False
            End If
            'Sohail (18 Feb 2019) -- End

            'S.SANDEEP [ 06 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            gbApplicantInfo.Tag = gbApplicantInfo.Text
            'S.SANDEEP [ 06 APR 2013 ] -- END


            'S.SANDEEP [ 06 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            btnImport.Enabled = False
            'S.SANDEEP [ 06 APR 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImport_Applicants_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsImportApplicant.SetMessages()
            objfrm._Other_ModuleNames = "clsImportApplicant"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchBranch.Click, _
                                                                                                             objbtnSearchDeptGrp.Click, _
                                                                                                             objbtnSearchDepartment.Click, _
                                                                                                             objbtnSearchSecGroup.Click, _
                                                                                                             objbtnSearchSection.Click, _
                                                                                                             objbtnSearchUnitGrp.Click, _
                                                                                                             objbtnSearchUnits.Click, _
                                                                                                             objbtnSearchTeam.Click, _
                                                                                                             objbtnSearchJobGroup.Click, _
                                                                                                             objbtnSearchJob.Click, _
                                                                                                             objbtnSearchGradeLevel.Click, _
                                                                                                             objbtnSearchClassGrp.Click, _
                                                                                                             objbtnSearchClass.Click, _
                                                                                                             objbtnSearchCostCenter.Click
        Dim frm As New frmCommonSearch
        Dim cbo As New ComboBox
        Try
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case "OBJBTNSEARCHBRANCH"
                    cbo = cboStation
                Case "OBJBTNSEARCHDEPTGRP"
                    cbo = cboDepartmentGrp
                Case "OBJBTNSEARCHDEPARTMENT"
                    cbo = cboDepartment
                Case "OBJBTNSEARCHSECGROUP"
                    cbo = cboSectionGroup
                Case "OBJBTNSEARCHSECTION"
                    cbo = cboSections
                Case "OBJBTNSEARCHUNITGRP"
                    cbo = cboUnitGroup
                Case "OBJBTNSEARCHUNITS"
                    cbo = cboUnits
                Case "OBJBTNSEARCHTEAM"
                    cbo = cboTeams
                Case "OBJBTNSEARCHJOBGROUP"
                    cbo = cboJobGroup
                Case "OBJBTNSEARCHJOB"
                    cbo = cboJob
                Case "OBJBTNSEARCHGRADEGRP"
                    cbo = cboGradeGroup
                Case "OBJBTNSEARCHCLASSGRP"
                    cbo = cboClassGroup
                Case "OBJBTNSEARCHCLASS"
                    cbo = cboClass
                Case "OBJBTNSEARCHCOSTCENTER"
                    cbo = cboCostCenter
            End Select

            If cbo.DataSource IsNot Nothing Then
                With frm
                    .DisplayMember = cbo.DisplayMember
                    .ValueMember = cbo.ValueMember
                    .CodeMember = ""
                    .DataSource = CType(cbo.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    cbo.Focus()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim blnFlag As Boolean = False
        Try


            'Pinkal (01-Dec-2012) -- Start
            'Enhancement : TRA Changes
            If mdtTran Is Nothing Then Exit Sub
            'Pinkal (01-Dec-2012) -- End



            'Sohail (13 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Dim dsEmp = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            Dim dsEmp = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
            'Sohail (21 Aug 2015) -- End
            If dsEmp.Tables("Period").Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please first Create atleast One Period."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sohail (13 Jan 2012) -- End




            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            ''Pinkal (20-Jan-2012) -- Start
            ''Enhancement : TRA Changes
            'If IsValid() = False Then Exit Sub
            ''Pinkal (20-Jan-2012) -- End
            Dim dInvalid() As DataRow = Nothing


            'S.SANDEEP [ 06 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dInvalid = mdtTran.Select("appointeddate IS NULL")
            'If dInvalid.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "In some of the Applicant(s), Appointment Date is not set. Please set Appointment Date in order to import Applicant(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'dInvalid = mdtTran.Select("departmentunkid <=0")
            'If dInvalid.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "In some of the Applicant(s), Department is not set. Please set Department in order to import Applicant(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'dInvalid = mdtTran.Select("jobunkid <=0")
            'If dInvalid.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "In some of the Applicant(s), Job is not set. Please set Job in order to import Applicant(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'dInvalid = mdtTran.Select("gradegroupunkid <=0")
            'If dInvalid.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "In some of the Applicant(s), Grade Group is not set. Please set Grade Group in order to import Applicant(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'dInvalid = mdtTran.Select("gradeunkid <=0")
            'If dInvalid.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "In some of the Applicant(s), Grade is not set. Please set Grade in order to import Applicant(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'dInvalid = mdtTran.Select("gradelevelunkid <=0")
            'If dInvalid.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "In some of the Applicant(s), Grade Level is not set. Please set Grade Level in order to import Applicant(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'dInvalid = mdtTran.Select("tranheadunkid <=0")
            'If dInvalid.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "In some of the Applicant(s), Salary Head is not set. Please set Salary Head in order to import Applicant(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'dInvalid = mdtTran.Select("costcenterunkid <=0")
            'If dInvalid.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "In some of the Applicant(s), Cost Center is not set. Please set Cost Center in order to import Applicant(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'dInvalid = mdtTran.Select("employmentunkid <=0")
            'If dInvalid.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "In some of the Applicant(s), Employment Type is not set. Please set Employment Type in order to import Applicant(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'dInvalid = mdtTran.Select("shiftunkid <=0")
            'If dInvalid.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "In some of the Applicant(s), Shift is not set. Please set Shift in order to import Applicant(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'dInvalid = mdtTran.Select("jobgroupunkid <=0")
            'If dInvalid.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "In some of the Applicant(s), Job Group is not set. Please set Job Group in order to import Applicant(s)."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            dInvalid = mdtTran.Select("IsCheck = false")
            If dInvalid.Length = mdtTran.Rows.Count Then
                If dInvalid.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Please check atleast one applicant in order to import."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            dInvalid = mdtTran.Select("appointeddate IS NULL AND IsCheck = true")
            If dInvalid.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "The Appointment Date for some of the Applicants is not set. Please set Appointment Date in order to import Applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Sohail (10 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            dInvalid = mdtTran.Select("retirementdate IS NULL AND IsCheck = true")
            If dInvalid.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "The Retirement Date for some of the Applicants is not set. Please set Retirement Date in order to import Applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sohail (10 Apr 2013) -- End

            dInvalid = mdtTran.Select("departmentunkid <=0 AND IsCheck = true")
            If dInvalid.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "For some of the Applicants Department is not set. Please set Department in order to import Applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            dInvalid = mdtTran.Select("jobunkid <=0 AND IsCheck = true")
            If dInvalid.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Job for some of the Applicants is not set. Please set Job in order to import Applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            dInvalid = mdtTran.Select("gradegroupunkid <=0 AND IsCheck = true")
            If dInvalid.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "For some of the Applicants Grade Group is not set. Please set Grade Group in order to import Applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            dInvalid = mdtTran.Select("gradeunkid <=0 AND IsCheck = true")
            If dInvalid.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "The Grade for some of the Applicants is not set. Please set Grade in order to import Applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            dInvalid = mdtTran.Select("gradelevelunkid <=0 AND IsCheck = true")
            If dInvalid.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "The Grade Level for some of the Applicants is not set. Please set Grade Level in order to import Applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            dInvalid = mdtTran.Select("tranheadunkid <=0 AND IsCheck = true")
            If dInvalid.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Salary Head for some of the Applicants is not set. Please set Salary Head in order to import Applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            dInvalid = mdtTran.Select("costcenterunkid <=0 AND IsCheck = true")
            If dInvalid.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "The Cost Center for some of the Applicants is not set. Please set Cost Center in order to import Applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            dInvalid = mdtTran.Select("employmentunkid <=0 AND IsCheck = true")
            If dInvalid.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "The Employment Type for some of the Applicants is not set. Please set Employment Type in order to import Applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            dInvalid = mdtTran.Select("shiftunkid <=0 AND IsCheck = true")
            If dInvalid.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "The Shift for some of the Applicants is not set. Please set Shift in order to import Applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            dInvalid = mdtTran.Select("jobgroupunkid <=0 AND IsCheck = true")
            If dInvalid.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "The Job Group for some of the Applicants is not set. Please set Job Group in order to import Applicant(s)."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'S.SANDEEP [ 16 JUL 2014 ] -- START
            If ConfigParameter._Object._UserAccessModeSetting.Trim.Length > 0 Then
                For Each iMode As Integer In ConfigParameter._Object._UserAccessModeSetting.Split(CChar(","))
                    Select Case iMode
                        Case enAllocation.CLASS_GROUP
                            dInvalid = mdtTran.Select("classgroupunkid <=0 AND IsCheck = true")
                            If dInvalid.Length > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "In some of the Applicant(s), Class Group is not set. Please set Class Group in order to import Applicant(s)."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        Case enAllocation.CLASSES
                            dInvalid = mdtTran.Select("classunkid <=0 AND IsCheck = true")
                            If dInvalid.Length > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "In some of the Applicant(s), Class is not set. Please set Class in order to import Applicant(s)."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                    End Select
                Next
            End If
            'S.SANDEEP [ 16 JUL 2014 ] -- END


            'S.SANDEEP [ 06 APR 2013 ] -- END


            'S.SANDEEP [ 28 FEB 2012 ] -- END


            'Pinkal (01-Dec-2012) -- Start
            'Enhancement : TRA Changes

            If ConfigParameter._Object._DisplayNameSetting <> enDisplayNameSetting.NONE And ConfigParameter._Object._DisplayNameSetting <> enDisplayNameSetting.EMPLOYEECODE Then

                Dim drrow() As DataRow = Nothing
                If rdApplyChked.Checked Then

                    drrow = mdtTran.Select("IsChanged = true AND IsCheck = true AND (IsDisplaynameExist = true OR IsEmailExist = true)")
                    If drrow.Length > 0 Then
                        For i As Integer = 0 To drrow.Length - 1
                            If CBool(drrow(i)("IsDisplaynameExist")) = True Then
                                dgvApplicantData.Rows(mdtTran.Rows.IndexOf(drrow(i))).Cells(dgcolhDisplayName.Index).Selected = True
                            ElseIf CBool(drrow(i)("IsEmailExist")) = True Then
                                dgvApplicantData.Rows(mdtTran.Rows.IndexOf(drrow(i))).Cells(dgcolhEmail.Index).Selected = True
                            End If
                            Exit For
                        Next
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Some applicants have same display name or email address which are highlighted in list, as those have been allocated to Employee already. Please change highlighted display name or email address."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                ElseIf rdApplyToAll.Checked Then

                    drrow = mdtTran.Select("IsChanged = true  AND (IsDisplaynameExist = true OR IsEmailExist = true)")
                    If drrow.Length > 0 Then
                        For i As Integer = 0 To drrow.Length - 1
                            If CBool(drrow(i)("IsDisplaynameExist")) = True Then
                                dgvApplicantData.Rows(mdtTran.Rows.IndexOf(drrow(i))).Cells(dgcolhDisplayName.Index).Selected = True
                            ElseIf CBool(drrow(i)("IsEmailExist")) = True Then
                                dgvApplicantData.Rows(mdtTran.Rows.IndexOf(drrow(i))).Cells(dgcolhEmail.Index).Selected = True
                            End If
                            Exit For
                        Next
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Some applicants have same display name or email address which are highlighted in list, as those have been allocated to Employee already. Please change highlighted display name or email address."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

            End If

            'Pinkal (01-Dec-2012) -- End

            'Sohail (02 Mar 2020) -- Start
            'Ifakara Issue # : Message "Actual date should not be greater than selected period end date"  on editing first salary change.
            objImport._BaseCountryunkid = Company._Object._Localization_Country
            'Sohail (02 Mar 2020) -- End

            'S.SANDEEP [ 06 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim dtTable As DataTable = New DataView(mdtTran, "IsChanged = true", "", DataViewRowState.CurrentRows).ToTable
            Dim dtTable As DataTable = New DataView(mdtTran, "IsChanged = true AND IsCheck = true", "", DataViewRowState.CurrentRows).ToTable
            'S.SANDEEP [ 06 APR 2013 ] -- END


            If dtTable IsNot Nothing And dtTable.Rows.Count > 0 Then

                With objImport
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objImport.ImportApplicant(dtTable)

                'Nilay (27 Apr 2016) -- Start
                'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
                '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
                'blnFlag = objImport.ImportApplicant(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, dtTable, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._IsArutiDemo, Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._EmployeeCodeNotype, ConfigParameter._Object._DisplayNameSetting, ConfigParameter._Object._EmployeeCodePrifix, ConfigParameter._Object._RetirementValue, ConfigParameter._Object._FRetirementValue, ConfigParameter._Object._IsPasswordAutoGenerated, ConfigParameter._Object._RetirementBy, ConfigParameter._Object._FRetirementBy)


                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                'blnFlag = objImport.ImportApplicant(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                '                                    Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                '                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, dtTable, User._Object.Privilege._AllowToApproveEarningDeduction, _
                '                                    ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._IsArutiDemo, _
                '                                    Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, _
                '                                    ConfigParameter._Object._EmployeeCodeNotype, ConfigParameter._Object._DisplayNameSetting, _
                '                                    ConfigParameter._Object._EmployeeCodePrifix, ConfigParameter._Object._RetirementValue, _
                '                                    ConfigParameter._Object._FRetirementValue, ConfigParameter._Object._IsPasswordAutoGenerated, _
                '                                    ConfigParameter._Object._RetirementBy, ConfigParameter._Object._FRetirementBy, _
                '                                    ConfigParameter._Object._SalaryAnniversarySetting, ConfigParameter._Object._SalaryAnniversaryMonthBy, , , getHostName, getIP, User._Object._Username, enLogin_Mode.DESKTOP)

                blnFlag = objImport.ImportApplicant(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, dtTable, User._Object.Privilege._AllowToApproveEarningDeduction, _
                                                    ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._IsArutiDemo, _
                                                    Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, _
                                                    ConfigParameter._Object._EmployeeCodeNotype, ConfigParameter._Object._DisplayNameSetting, _
                                                    ConfigParameter._Object._EmployeeCodePrifix, ConfigParameter._Object._RetirementValue, _
                                                    ConfigParameter._Object._FRetirementValue, ConfigParameter._Object._IsPasswordAutoGenerated, _
                                                    ConfigParameter._Object._RetirementBy, ConfigParameter._Object._FRetirementBy, _
                                                  ConfigParameter._Object._SalaryAnniversarySetting, ConfigParameter._Object._SalaryAnniversaryMonthBy, _
                                                  ConfigParameter._Object._CreateADUserFromEmpMst, True, "", getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP)


                'Pinkal (18-Aug-2018) -- End

                'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END

                'Nilay (27 Apr 2016) -- End

                'Sohail (21 Aug 2015) -- End

                'S.SANDEEP [ 23 JAN 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
                If blnFlag = True And objImport._Message <> "" Then
                    eZeeMsgBox.Show(objImport._Message, enMsgBoxStyle.Information)
                    'S.SANDEEP [ 23 JAN 2012 ] -- END
                ElseIf blnFlag = True Then


                    'Pinkal (01-Dec-2012) -- Start
                    'Enhancement : TRA Changes
                    If ConfigParameter._Object._DisplayNameSetting = enDisplayNameSetting.EMPLOYEECODE Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Email Address for all applicants which are imported as Employees have been set with their Employee Code, so if you would like to change their Email Address then please go to Employee add/edit screen and change them accordingly."), enMsgBoxStyle.Information)
                    End If
                    'Pinkal (01-Dec-2012) -- End


                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Applicant(s) imported successfully."), enMsgBoxStyle.Information)
                    Call Fill_Grid()
                    'S.SANDEEP [ 06 APR 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    Call Set_Applicant_Count()
                    'S.SANDEEP [ 06 APR 2013 ] -- END
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Applicant(s) importation failed."), enMsgBoxStyle.Information)
            End If


            If blnFlag = True Then
                sMsg = objImport._ApprovalString
                mstrOriginal = sMsg
                Dim trd As New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If

            'Pinkal (02-Oct-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 28 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboVacancy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Vacancy is mandatory information. Please select Vacancy to continue."), enMsgBoxStyle.Information)
                cboVacancy.Focus()
                Exit Sub
            End If

            Call Generate_Table()


            'S.SANDEEP [ 06 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call Set_Applicant_Count()
            'S.SANDEEP [ 06 APR 2013 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboVacType.SelectedValue = 0
            dgvApplicantData.DataSource = Nothing


            'Pinkal (01-Dec-2012) -- Start
            'Enhancement : TRA Changes
            If mdtTran IsNot Nothing Then mdtTran.Rows.Clear()
            'Pinkal (01-Dec-2012) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 28 FEB 2012 ] -- END

#End Region

#Region " Private Methods "

    Private Sub Generate_Table()
        Try
            mdtTran = New DataTable("Applicant")
            mdtTran.Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("IsChanged", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("applicant", System.Type.GetType("System.String")).DefaultValue = ""

            'Pinkal (01-Dec-2012) -- Start
            'Enhancement : TRA Changes
            mdtTran.Columns.Add("firstname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("othername", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("surname", System.Type.GetType("System.String")).DefaultValue = ""
            'Pinkal (01-Dec-2012) -- End

            mdtTran.Columns.Add("tranheadname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("gradegroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("grade", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("gradelevel", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("applicantunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("tranheadunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("gradegroupunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("gradeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("gradelevelunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("scale", System.Type.GetType("System.Decimal")).DefaultValue = 0


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            mdtTran.Columns.Add("stationunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("station", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("deptgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("deptgroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("departmentunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("department", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("sectiongrpunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("sectiongroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("sectionunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("section", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("unitgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("unitgroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("unitunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("unit", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("teamunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("team", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("jobgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("jobgroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("jobunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("job", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("classgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("classgroup", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("classunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("class", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("costcenterunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("costcenter", System.Type.GetType("System.String")).DefaultValue = ""
            'Pinkal (20-Jan-2012) -- End

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mdtTran.Columns.Add("employmentunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("employment", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("appointeddate", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("retirementdate", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value 'Sohail (10 Apr 2013)
            mdtTran.Columns.Add("shiftunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("shift", System.Type.GetType("System.String")).DefaultValue = ""
            'S.SANDEEP [ 28 FEB 2012 ] -- END


            'Pinkal (01-Dec-2012) -- Start
            'Enhancement : TRA Changes
            mdtTran.Columns.Add("displayname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("email", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("IsDisplayNameExist", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("IsEmailExist", System.Type.GetType("System.Boolean")).DefaultValue = False
            'Pinkal (01-Dec-2012) -- End

            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            mdtTran.Columns.Add("AssignDefaulTranHeads", System.Type.GetType("System.Boolean")).DefaultValue = False
            'Sohail (18 Feb 2019) -- End


            Call Fill_Grid()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_Table", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsList As New DataSet
        Dim objAnalysisMaster As New clsInterviewAnalysis_master


        'Pinkal (01-Dec-2012) -- Start
        'Enhancement : TRA Changes
        Dim objEmp As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        'Pinkal (01-Dec-2012) -- End


        Try
            mdtTran.Rows.Clear()
            'Sohail (12 Nov 2020) -- Start
            'Internal Issue # : - External applicants from External Internal vacancies are not coming on Import Eligible Applicant screen.
            'dsList = objAnalysisMaster.GetList_FinalApplicant("Applicant", False, , , CInt(cboVacancy.SelectedValue), enShortListing_Status.SC_APPROVED)
            dsList = objAnalysisMaster.GetList_FinalApplicant("Applicant", False, CInt(enVacancyType.EXTERNAL_VACANCY), , CInt(cboVacancy.SelectedValue), enShortListing_Status.SC_APPROVED)
            'Sohail (12 Nov 2020) -- End
            For Each dRow As DataRow In dsList.Tables("Applicant").Rows
                Dim dtRow As DataRow = mdtTran.NewRow
                dtRow.Item("applicant") = dRow.Item("applicantname")

                'Pinkal (01-Dec-2012) -- Start
                'Enhancement : TRA Changes
                dtRow.Item("firstname") = dRow.Item("firstname")
                dtRow.Item("othername") = dRow.Item("othername")
                dtRow.Item("surname") = dRow.Item("surname")
                'Pinkal (01-Dec-2012) -- End

                dtRow.Item("applicantunkid") = dRow.Item("applicantunkid")
                'S.SANDEEP [ 28 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                dtRow.Item("stationunkid") = dRow.Item("BranchId")
                dtRow.Item("station") = dRow.Item("Branch")
                dtRow.Item("deptgroupunkid") = dRow.Item("DeptGroupId")
                dtRow.Item("deptgroup") = dRow.Item("DeptGroup")
                dtRow.Item("departmentunkid") = dRow.Item("DeptId")
                dtRow.Item("department") = dRow.Item("Dept")
                dtRow.Item("sectionunkid") = dRow.Item("SectionId")
                dtRow.Item("section") = dRow.Item("Section")
                dtRow.Item("unitunkid") = dRow.Item("UnitId")
                dtRow.Item("unit") = dRow.Item("Unit")
                dtRow.Item("jobgroupunkid") = dRow.Item("JobGrpId")
                dtRow.Item("jobgroup") = dRow.Item("JobGrp")
                dtRow.Item("jobunkid") = dRow.Item("JobId")
                dtRow.Item("job") = dRow.Item("Job")
                dtRow.Item("employmentunkid") = dRow.Item("EmplTypeId")
                dtRow.Item("employment") = dRow.Item("EmplType")
                dtRow.Item("gradegroupunkid") = dRow.Item("GradeGrpId")
                dtRow.Item("gradegroup") = dRow.Item("GradeGrp")
                dtRow.Item("gradeunkid") = dRow.Item("GradeId")
                dtRow.Item("grade") = dRow.Item("Grade")
                'S.SANDEEP [ 28 FEB 2012 ] -- END

                'Hemant (12 Dec 2020) -- Start
                'Issue : got a problem importing eligible applicant from a newly applied applicants
                dtRow.Item("sectiongrpunkid") = dRow.Item("SectionGrpId")
                dtRow.Item("sectiongroup") = dRow.Item("SectionGrp")
                dtRow.Item("unitgroupunkid") = dRow.Item("UnitGrpId")
                dtRow.Item("unitgroup") = dRow.Item("UnitGrp")
                dtRow.Item("teamunkid") = dRow.Item("TeamId")
                dtRow.Item("team") = dRow.Item("Team")
                dtRow.Item("classgroupunkid") = dRow.Item("ClassGrpId")
                dtRow.Item("classgroup") = dRow.Item("ClassGrp")
                dtRow.Item("classunkid") = dRow.Item("ClassId")
                dtRow.Item("class") = dRow.Item("Class")
                dtRow.Item("costcenterunkid") = dRow.Item("CostCenterId")
                dtRow.Item("costcenter") = dRow.Item("CostCenter")
                dtRow.Item("gradelevelunkid") = dRow.Item("GradeLevelId")
                dtRow.Item("gradelevel") = dRow.Item("GradeLevel")
                'Hemant (12 Dec 2020) -- End

                'Pinkal (01-Dec-2012) -- Start
                'Enhancement : TRA Changes


                Select Case ConfigParameter._Object._DisplayNameSetting

                    Case enDisplayNameSetting.NONE
                        dtRow.Item("displayname") = dtRow.Item("firstname").ToString().Trim & " " & dtRow.Item("surname").ToString().Trim
                        dtRow.Item("email") = dtRow.Item("email").ToString().Trim

                    Case enDisplayNameSetting.EMPLOYEECODE

                        If ConfigParameter._Object._EmployeeCodeNotype = 0 Then
                            dtRow.Item("displayname") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & Now.Millisecond.ToString
                        End If
                        If ConfigParameter._Object._CompanyDomain.Trim.Length > 0 Then
                            dtRow.Item("email") = dtRow.Item("displayname").ToString().Trim & "@" & ConfigParameter._Object._CompanyDomain.Trim
                        End If

                    Case enDisplayNameSetting.FLASTNAME
                        dtRow.Item("displayname") = dtRow.Item("firstname").ToString().Trim.Substring(0, 1) & dtRow.Item("surname").ToString().Trim
                        If ConfigParameter._Object._CompanyDomain.Trim.Length > 0 Then
                            dtRow.Item("email") = dtRow.Item("firstname").ToString().Trim.Substring(0, 1) & dtRow.Item("surname").ToString().Trim & "@" & ConfigParameter._Object._CompanyDomain.Trim
                        End If

                    Case enDisplayNameSetting.FIRSTNAME_LASTNAME
                        dtRow.Item("displayname") = dtRow.Item("firstname").ToString().Trim & "." & dtRow.Item("surname").ToString().Trim
                        If ConfigParameter._Object._CompanyDomain.Trim.Length > 0 Then
                            dtRow.Item("email") = dtRow.Item("firstname").ToString().Trim & "." & dtRow.Item("surname").ToString().Trim & "@" & ConfigParameter._Object._CompanyDomain.Trim
                        End If

                    Case enDisplayNameSetting.LASTNAME_FIRSTNAME
                        dtRow.Item("displayname") = dtRow.Item("surname").ToString() & "." & dtRow.Item("firstname").ToString().Trim
                        If ConfigParameter._Object._CompanyDomain.Trim.Length > 0 Then
                            dtRow.Item("email") = dtRow.Item("surname").ToString() & "." & dtRow.Item("firstname").ToString().Trim & "@" & ConfigParameter._Object._CompanyDomain.Trim
                        End If

                End Select

                If dtRow.Item("displayname").ToString().Trim.Length > 0 Then
                    If objEmp.isExist("", dtRow.Item("displayname").ToString().Trim, -1) = True Then
                        dtRow.Item("IsDisplayNameExist") = True
                    Else
                        dtRow.Item("IsDisplayNameExist") = False
                    End If
                End If

                If dtRow.Item("email").ToString().Trim.Length > 0 Then
                    If objMaster.IsEmailPresent("hremployee_master", dtRow.Item("email").ToString().Trim, -1, "employeeunkid") = True Then
                        dtRow.Item("IsEmailExist") = True
                    Else
                        dtRow.Item("IsEmailExist") = False
                    End If
                End If


                'Pinkal (01-Dec-2012) -- End


                mdtTran.Rows.Add(dtRow)
            Next

            dgvApplicantData.AutoGenerateColumns = False

            objdgcolhApplicantId.DataPropertyName = "applicantunkid"
            objdgcolhCheck.DataPropertyName = "IsCheck"
            objdgcolhGradeGrpId.DataPropertyName = "gradegroupunkid"
            objdgcolhGradeId.DataPropertyName = "gradeunkid"
            objdgcolhGradeLevelId.DataPropertyName = "gradelevelunkid"
            objdgcolhSalaryHeadId.DataPropertyName = "tranheadunkid"
            dgcolhApplicant.DataPropertyName = "applicant"
            dgcolhGrade.DataPropertyName = "grade"
            dgcolhGradeGrp.DataPropertyName = "gradegroup"
            dgcolhGradeLevel.DataPropertyName = "gradelevel"
            dgcolhSalaryHead.DataPropertyName = "tranheadname"
            dgcolhScale.DataPropertyName = "scale"


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            objdgcolhbranchunkid.DataPropertyName = "stationunkid"
            objdgcolhdeptgrpId.DataPropertyName = "deptgroupunkid"
            objdgcolhdeptId.DataPropertyName = "departmentunkid"
            objdgcolhsectiongrpId.DataPropertyName = "sectiongrpunkid"
            objdgcolhsectionId.DataPropertyName = "sectionunkid"
            objdgcolhunitgroupId.DataPropertyName = "unitgroupunkid"
            objdgcolhunitId.DataPropertyName = "unitunkid"
            objdgcolhteamId.DataPropertyName = "teamunkid"
            objdgcolhjobgroupId.DataPropertyName = "jobgroupunkid"
            objdgcolhjobId.DataPropertyName = "jobunkid"
            objdgcolhclassgroupId.DataPropertyName = "classgroupunkid"
            objdgcolhclassId.DataPropertyName = "classunkid"
            objdgcolhcostcenterId.DataPropertyName = "costcenterunkid"

            dgcolhbranch.DataPropertyName = "station"
            dgcolhDepartment.DataPropertyName = "department"
            dgcolhSection.DataPropertyName = "section"
            dgcolhJob.DataPropertyName = "job"
            dgcolhCostcenter.DataPropertyName = "costcenter"

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dgcolhEmplType.DataPropertyName = "employment"
            objdgcolhEmplTypeId.DataPropertyName = "employmentunkid"
            dgcolhAppointedDate.DataPropertyName = "appointeddate"
            dgcolhRetirementDate.DataPropertyName = "retirementdate" 'Sohail (10 Apr 2013)
            objdgcolhShiftId.DataPropertyName = "shiftunkid"
            dgcolhShift.DataPropertyName = "shift"
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            'Pinkal (20-Jan-2012) -- End

            'Hemant (12 Dec 2020) -- Start
            'Issue : got a problem importing eligible applicant from a newly applied applicants
            dgcolhSectionGrp.DataPropertyName = "SectionGroup"
            dgcolhUnitGrp.DataPropertyName = "UnitGroup"
            dgcolhTeam.DataPropertyName = "Team"
            dgcolhClassGrp.DataPropertyName = "ClassGroup"
            dgcolhClass.DataPropertyName = "Class"
            'Hemant (12 Dec 2020) -- End

            'Pinkal (01-Dec-2012) -- Start
            'Enhancement : TRA Changes
            dgcolhDisplayName.DataPropertyName = "displayname"
            dgcolhEmail.DataPropertyName = "email"
            'Pinkal (01-Dec-2012) -- End



            dgvApplicantData.DataSource = mdtTran

            dgcolhScale.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhScale.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhScale.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            lnkSetChanges.Focus()


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
            dsList.Dispose()
            objAnalysisMaster = Nothing
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objCostCenter As New clscostcenter_master
        Dim objSalaryHead As New clsTransactionHead
        Dim objGradeGrp As New clsGradeGroup
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        'S.SANDEEP [ 28 FEB 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim objCMaster As New clsCommon_Master
        Dim objVacType As New clsVacancy

        'Pinkal (03-Jul-2013) -- Start
        'Enhancement : TRA Changes
        'Dim objShift As New clsshift_master
        Dim objShift As New clsNewshift_master
        'Pinkal (03-Jul-2013) -- End

        'S.SANDEEP [ 28 FEB 2012 ] -- END
        Dim dsList As New DataSet
        Try

            dsList = objStation.getComboList("Station", True)
            With cboStation

                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Station")

            End With

            dsList = objDeptGrp.getComboList("DeptGrp", True)
            With cboDepartmentGrp

                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("DeptGrp")

            End With

            dsList = objDepartment.getComboList("Department", True)
            With cboDepartment

                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Department")

            End With

            dsList = objSection.getComboList("Section", True)
            With cboSections

                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Section")

            End With

            dsList = objUnit.getComboList("Unit", True)
            With cboUnits

                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Unit")

            End With

            dsList = objJobGrp.getComboList("JobGrp", True)
            With cboJobGroup

                .ValueMember = "jobgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("JobGrp")

            End With

            dsList = objJob.getComboList("Job", True)
            With cboJob

                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Job")

            End With

            dsList = objClassGrp.getComboList("ClassGrp", True)
            With cboClassGroup

                .ValueMember = "classgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("ClassGrp")

            End With

            dsList = objClass.getComboList("Class", True)
            With cboClass

                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Class")

            End With

            dsList = objCostCenter.getComboList("CostCenter", True)
            With cboCostCenter

                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsList.Tables("CostCenter")

            End With

            dsList = objSectionGrp.getComboList("List", True)
            With cboSectionGroup
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With

            dsList = objUnitGroup.getComboList("List", True)
            With cboUnitGroup
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With

            dsList = objTeam.getComboList("List", True)
            With cboTeams
                .ValueMember = "teamunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objSalaryHead.getComboList("Head", True, 0, 0, enTypeOf.Salary)
            dsList = objSalaryHead.getComboList(FinancialYear._Object._DatabaseName, "Head", True, 0, 0, enTypeOf.Salary)
            'Sohail (21 Aug 2015) -- End
            With cboSalaryHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Head")
                .SelectedValue = 0
            End With

            dsList = objGradeGrp.getComboList("Group", True)
            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Group")
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "List")
            With cboEmplType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objVacType.getVacancyType
            'S.SANDEEP [ 09 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Sohail (12 Nov 2020) -- Start
            'Internal Issue # : - External applicants from External Internal vacancies are not coming on Import Eligible Applicant screen.
            'Dim dTable As DataTable = New DataView(dsList.Tables(0), "Id < " & enVacancyType.INTERNAL_VACANCY, "Id", DataViewRowState.CurrentRows).ToTable
            Dim dTable As DataTable = New DataView(dsList.Tables(0), "Id <> " & enVacancyType.INTERNAL_VACANCY, "Id", DataViewRowState.CurrentRows).ToTable
            'Sohail (12 Nov 2020) -- End
            'S.SANDEEP [ 09 NOV 2012 ] -- END
            With cboVacType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                'S.SANDEEP [ 09 NOV 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                '.DataSource = dsList.Tables(0)
                .DataSource = dTable
                'S.SANDEEP [ 09 NOV 2012 ] -- END
                .SelectedValue = 0
            End With

            dsList = objShift.getListForCombo("List", True)
            With cboShift
                .ValueMember = "shiftunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 28 FEB 2012 ] -- END



            'cboOperation.Items.Clear()
            'cboOperation.Items.Add(Language.getMessage(mstrModuleName, 1, "Apply to All"))
            'cboOperation.Items.Add(Language.getMessage(mstrModuleName, 2, "Apply to Checked"))
            'cboOperation.SelectedIndex = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If CInt(cboStation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Branch is mandatory information. Please select Branch to continue."), enMsgBoxStyle.Information)
                cboStation.Focus()
                Return False
            End If

            If CInt(cboDepartment.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Department is mandatory information. Please select Department to continue."), enMsgBoxStyle.Information)
                cboDepartment.Focus()
                Return False
            End If

            If CInt(cboSections.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Section is mandatory information. Please select Section to continue."), enMsgBoxStyle.Information)
                cboSections.Focus()
                Return False
            End If

            If CInt(cboJobGroup.SelectedValue) <= 0 Then

            End If

            If CInt(cboJob.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Job is mandatory information. Please select Job to continue."), enMsgBoxStyle.Information)
                cboJob.Focus()
                Return False
            End If

            If CInt(cboGradeGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Grade Group is mandatory information. Please select Grade Group to continue."), enMsgBoxStyle.Information)
                cboGradeGroup.Focus()
                Return False
            End If

            If CInt(cboSalaryGrade.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Salary Grade is mandatory information. Please select Salary Grade to continue."), enMsgBoxStyle.Information)
                cboSalaryGrade.Focus()
                Return False
            End If

            If CInt(cboGradeLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Grade Level is mandatory information. Please select Grade Level to continue."), enMsgBoxStyle.Information)
                cboGradeLevel.Focus()
                Return False
            End If

            If txtScale.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Scale is mandatory information. Please provide Scale to continue."), enMsgBoxStyle.Information)
                txtScale.Focus()
                Return False
            End If

            If CInt(cboCostCenter.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Cost Center is mandatory information. Please select Cost Center to continue."), enMsgBoxStyle.Information)
                cboCostCenter.Focus()
                Return False
            End If


            If CInt(cboSalaryHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Salary Head is mandatory information. Please select Salary Head to continue."), enMsgBoxStyle.Information)
                cboSalaryHead.Focus()
                Return False
            End If

            If rdApplyChked.Checked = False And rdApplyToAll.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please Select Operation in order to import eligible applicant."), enMsgBoxStyle.Information)
                Exit Function
            End If

            'Pinkal (20-Jan-2012) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub CommonSearching(ByVal cbo As ComboBox)
        Dim frm As New frmCommonSearch
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If cbo.DataSource Is Nothing Then Exit Sub
            'Pinkal (20-Jan-2012) -- End

            With frm
                .ValueMember = cbo.ValueMember
                .DisplayMember = cbo.DisplayMember
                .DataSource = CType(cbo.DataSource, DataTable)
                .CodeMember = ""
            End With

            If frm.DisplayDialog Then
                cbo.SelectedValue = frm.SelectedValue
                cbo.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub


    'Pinkal (02-Oct-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub Send_Notification()
        Try
            Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
            Dim dUList As New DataSet
            Dim objSend As New clsSendMail
            dUList = objUsr.Get_UserBy_PrivilegeId(344)
            If dUList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dUList.Tables(0).Rows
                    objSend._Subject = Language.getMessage(mstrModuleName, 15, "Notification to Approve newly Hired Employee.")
                    objSend._ToEmail = CStr(dRow.Item("UEmail"))
                    sMsg = sMsg.Replace("#_UserName", CStr(dRow.Item("UName")))
                    'S.SANDEEP [ 03 FEB 2014 ] -- START
                    sMsg &= "<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & vbCrLf

                    'S.SANDEEP [21-NOV-2018] -- START
                    'sMsg &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Or click link below.<BR><BR></span></p>" & vbCrLf
                    'sMsg &= "<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & vbCrLf
                    'sMsg &= ConfigParameter._Object._ArutiSelfServiceURL & "/HR/wPgEmployeeProfile.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(Company._Object._Companyunkid & "|" & dRow.Item("UId").ToString & "|" & FinancialYear._Object._YearUnkid & "|" & "1")) & "</span></p>" & vbCrLf
                    'S.SANDEEP [21-NOV-2018] -- END


                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'sMsg &= "<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>" & vbCrLf

                    sMsg &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"

                    'Gajanan [27-Mar-2019] -- End

                    sMsg &= "</span></p>" & vbCrLf
                    sMsg &= "</BODY></HTML>"
                    'S.SANDEEP [ 03 FEB 2014 ] -- END
                    objSend._Message = sMsg
                    'S.SANDEEP [ 28 JAN 2014 ] -- START
                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    'objSend._Form_Name = mstrModuleName
                    objSend._Form_Name = "" 'Please pass Form Name for WEB
                    'Sohail (17 Dec 2014) -- End
                    objSend._LogEmployeeUnkid = -1
                    objSend._OperationModeId = enLogin_Mode.DESKTOP
                    objSend._UserUnkid = User._Object._Userunkid
                    objSend._SenderAddress = User._Object._Email
                    objSend._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                    With objSend
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    'S.SANDEEP [ 28 JAN 2014 ] -- END
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objSend.SendMail()
                    objSend.SendMail(Company._Object._Companyunkid)
                    'Sohail (30 Nov 2017) -- End
                    sMsg = mstrOriginal
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub

    'Pinkal (02-Oct-2012) -- End

    'S.SANDEEP [ 06 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Set_Applicant_Count()
        Try
            gbApplicantInfo.Text = CStr(gbApplicantInfo.Tag)
            If dgvApplicantData.RowCount > 0 Then
                gbApplicantInfo.Text = gbApplicantInfo.Text & " (" & dgvApplicantData.RowCount & ")"
            Else
                gbApplicantInfo.Text = CStr(gbApplicantInfo.Tag)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Applicant_Count", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 06 APR 2013 ] -- END


#End Region

#Region " Controls "

    'S.SANDEEP [21-NOV-2018] -- START
    Private Sub cboClassGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboClassGroup.SelectedIndexChanged
        Try
            Dim dsCombos As New DataSet
            Dim objclass As New clsClass
            dsCombos = objclass.getComboList("Classgrp", True, CInt(cboClassGroup.SelectedValue))
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Classgrp")
            End With
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboClassGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [21-NOV-2018] -- END

    Private Sub cboGradeGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeGroup.SelectedIndexChanged
        Try
            If CInt(cboGradeGroup.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                dsList = objSalaryGrade.getComboList("Grade", True, CInt(cboGradeGroup.SelectedValue))
                With cboSalaryGrade
                    .ValueMember = "gradeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Grade")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSalaryGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSalaryGrade.SelectedIndexChanged
        Try
            If CInt(cboSalaryGrade.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                dsList = objGLevel.getComboList("GradeLevel", True, CInt(cboSalaryGrade.SelectedValue))
                With cboGradeLevel
                    .ValueMember = "gradelevelunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("GradeLevel")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSalaryGrade_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGradeLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeLevel.SelectedIndexChanged
        Try
            Dim decScale As Decimal = 0 'Sohail (11 May 2011)
            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'objSalaryScale.GetSalary(CInt(cboGradeGroup.SelectedValue), CInt(cboSalaryGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), decScale)
            objSalaryScale.GetSalary(CInt(cboGradeGroup.SelectedValue), CInt(cboSalaryGrade.SelectedValue), CInt(cboGradeLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), decScale)
            'Sohail (27 Apr 2016) -- End
            txtScale.Decimal = decScale
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchGradeLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGradeLevel.Click
        Try
            Call CommonSearching(cboGradeLevel)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGradeLevel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchSalGrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSalGrade.Click
        Try
            Call CommonSearching(cboSalaryGrade)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSalGrade_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchGGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGGroup.Click
        Try
            Call CommonSearching(cboGradeGroup)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchSalaryHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSalaryHead.Click
        Try
            Call CommonSearching(cboSalaryHead)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchSalaryHead_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetChanges_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetChanges.LinkClicked
        Try


            'Sohail (10 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            If dtpRetirementDate.Enabled = True AndAlso dtpRetirementDate.Value.Date <= dtpAppointedDate.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Sorry, Retirement Date should be greater than Appointment Date."), enMsgBoxStyle.Information)
                dtpRetirementDate.Focus()
                Exit Try
            End If
            'Sohail (10 Apr 2013) -- End

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If IsValid() = False Then Exit Sub
            'S.SANDEEP [ 28 FEB 2012 ] -- END


            Dim dtTemp() As DataRow = Nothing


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'Select Case cboOperation.SelectedIndex
            '    Case 0
            '        For Each dRow As DataRow In mdtTran.Rows
            '            dRow.Item("tranheadname") = cboSalaryHead.Text
            '            dRow.Item("tranheadunkid") = cboSalaryHead.SelectedValue
            '            dRow.Item("gradegroup") = cboGradeGroup.Text
            '            dRow.Item("gradegroupunkid") = cboGradeGroup.SelectedValue
            '            dRow.Item("grade") = cboSalaryGrade.Text
            '            dRow.Item("gradeunkid") = cboSalaryGrade.SelectedValue
            '            dRow.Item("gradelevel") = cboGradeLevel.Text
            '            dRow.Item("gradelevelunkid") = cboGradeLevel.SelectedValue
            '            dRow.Item("scale") = Format(CDec(txtScale.Decimal), GUI.fmtCurrency)
            '            dRow.Item("IsChanged") = True
            '        Next
            '    Case 1
            '        dtTemp = mdtTran.Select("IsCheck = true")
            '        For i As Integer = 0 To dtTemp.Length - 1
            '            dtTemp(i).Item("tranheadname") = cboSalaryHead.Text
            '            dtTemp(i).Item("tranheadunkid") = cboSalaryHead.SelectedValue
            '            dtTemp(i).Item("gradegroup") = cboGradeGroup.Text
            '            dtTemp(i).Item("gradegroupunkid") = cboGradeGroup.SelectedValue
            '            dtTemp(i).Item("grade") = cboSalaryGrade.Text
            '            dtTemp(i).Item("gradeunkid") = cboSalaryGrade.SelectedValue
            '            dtTemp(i).Item("gradelevel") = cboGradeLevel.Text
            '            dtTemp(i).Item("gradelevelunkid") = cboGradeLevel.SelectedValue
            '            dtTemp(i).Item("scale") = Format(CDec(txtScale.Decimal), GUI.fmtCurrency)
            '            dtTemp(i).Item("IsChanged") = True
            '        Next
            'End Select


            'Pinkal (01-Dec-2012) -- Start
            'Enhancement : TRA Changes

            If mdtTran Is Nothing Then Exit Sub

            'Pinkal (01-Dec-2012) -- End


            If rdApplyToAll.Checked Then

                For Each dRow As DataRow In mdtTran.Rows
                    If CInt(cboSalaryHead.SelectedValue) > 0 Then
                        dRow.Item("tranheadunkid") = cboSalaryHead.SelectedValue
                        dRow.Item("tranheadname") = cboSalaryHead.Text
                    End If

                    'Sohail (18 Feb 2019) -- Start
                    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                    dRow.Item("AssignDefaulTranHeads") = chkAssignDefaulTranHeads.Checked
                    'Sohail (18 Feb 2019) -- End

                    If CInt(cboGradeGroup.SelectedValue) > 0 Then
                        dRow.Item("gradegroupunkid") = cboGradeGroup.SelectedValue
                        dRow.Item("gradegroup") = cboGradeGroup.Text
                    End If

                    If CInt(cboSalaryGrade.SelectedValue) > 0 Then
                        dRow.Item("gradeunkid") = cboSalaryGrade.SelectedValue
                        dRow.Item("grade") = cboSalaryGrade.Text
                    End If

                    If CInt(cboGradeLevel.SelectedValue) > 0 Then
                        dRow.Item("gradelevelunkid") = cboGradeLevel.SelectedValue
                        dRow.Item("gradelevel") = cboGradeLevel.Text
                    End If

                    dRow.Item("scale") = Format(CDec(txtScale.Decimal), GUI.fmtCurrency)

                    If CInt(cboStation.SelectedValue) > 0 Then
                        dRow.Item("stationunkid") = CInt(cboStation.SelectedValue)
                        dRow.Item("station") = cboStation.Text
                    End If

                    If CInt(cboDepartmentGrp.SelectedValue) > 0 Then
                        dRow.Item("deptgroupunkid") = CInt(cboDepartmentGrp.SelectedValue)
                        dRow.Item("deptgroup") = cboDepartmentGrp.Text
                    End If

                    If CInt(cboDepartment.SelectedValue) > 0 Then
                        dRow.Item("departmentunkid") = CInt(cboDepartment.SelectedValue)
                        dRow.Item("department") = cboDepartment.Text
                    End If

                    If CInt(cboSectionGroup.SelectedValue) > 0 Then
                        dRow.Item("sectiongrpunkid") = CInt(cboSectionGroup.SelectedValue)
                        dRow.Item("sectiongroup") = cboSections.Text
                    End If

                    If CInt(cboSections.SelectedValue) > 0 Then
                        dRow.Item("sectionunkid") = CInt(cboSections.SelectedValue)
                        dRow.Item("section") = cboSections.Text
                    End If

                    If CInt(cboUnitGroup.SelectedValue) > 0 Then
                        dRow.Item("unitgroupunkid") = CInt(cboUnitGroup.SelectedValue)
                        dRow.Item("unitgroup") = cboUnitGroup.Text
                    End If

                    If CInt(cboUnits.SelectedValue) > 0 Then
                        dRow.Item("unitunkid") = CInt(cboUnits.SelectedValue)
                        dRow.Item("unit") = cboUnits.Text
                    End If

                    If CInt(cboTeams.SelectedValue) > 0 Then
                        dRow.Item("teamunkid") = CInt(cboTeams.SelectedValue)
                        dRow.Item("team") = cboTeams.Text
                    End If

                    If CInt(cboJobGroup.SelectedValue) > 0 Then
                        dRow.Item("jobgroupunkid") = CInt(cboJobGroup.SelectedValue)
                        dRow.Item("jobgroup") = cboJobGroup.Text
                    End If

                    If CInt(cboJob.SelectedValue) > 0 Then
                        dRow.Item("jobunkid") = CInt(cboJob.SelectedValue)
                        dRow.Item("job") = cboJob.Text
                    End If

                    If CInt(cboClassGroup.SelectedValue) > 0 Then
                        dRow.Item("classgroupunkid") = CInt(cboClassGroup.SelectedValue)
                        dRow.Item("classgroup") = cboClassGroup.Text
                    End If

                    If CInt(cboClass.SelectedValue) > 0 Then
                        dRow.Item("classunkid") = CInt(cboClass.SelectedValue)
                        dRow.Item("class") = cboClass.Text
                    End If

                    If CInt(cboCostCenter.SelectedValue) > 0 Then
                        dRow.Item("costcenterunkid") = CInt(cboCostCenter.SelectedValue)
                        dRow.Item("costcenter") = cboCostCenter.Text
                    End If

                    If CInt(cboEmplType.SelectedValue) > 0 Then
                        dRow.Item("employmentunkid") = CInt(cboEmplType.SelectedValue)
                        dRow.Item("employment") = cboEmplType.Text
                    End If

                    dRow.Item("appointeddate") = dtpAppointedDate.Value.Date
                    dRow.Item("retirementdate") = dtpRetirementDate.Value.Date 'Sohail (10 Apr 2013)

                    If CInt(cboShift.SelectedValue) > 0 Then
                        dRow.Item("shiftunkid") = CInt(cboShift.SelectedValue)
                        dRow.Item("shift") = cboShift.Text
                    End If

                    dRow.Item("IsChanged") = True

                Next

            ElseIf rdApplyChked.Checked Then
                dtTemp = mdtTran.Select("IsCheck = true")
                For i As Integer = 0 To dtTemp.Length - 1
                    If CInt(cboSalaryHead.SelectedValue) > 0 Then
                        dtTemp(i).Item("tranheadunkid") = cboSalaryHead.SelectedValue
                        dtTemp(i).Item("tranheadname") = cboSalaryHead.Text
                    End If

                    'Sohail (18 Feb 2019) -- Start
                    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                    dtTemp(i).Item("AssignDefaulTranHeads") = chkAssignDefaulTranHeads.Checked
                    'Sohail (18 Feb 2019) -- End

                    If CInt(cboGradeGroup.SelectedValue) > 0 Then
                        dtTemp(i).Item("gradegroupunkid") = cboGradeGroup.SelectedValue
                        dtTemp(i).Item("gradegroup") = cboGradeGroup.Text
                    End If

                    If CInt(cboSalaryGrade.SelectedValue) > 0 Then
                        dtTemp(i).Item("gradeunkid") = cboSalaryGrade.SelectedValue
                        dtTemp(i).Item("grade") = cboSalaryGrade.Text
                    End If

                    If CInt(cboGradeLevel.SelectedValue) > 0 Then
                        dtTemp(i).Item("gradelevelunkid") = cboGradeLevel.SelectedValue
                        dtTemp(i).Item("gradelevel") = cboGradeLevel.Text
                    End If

                    dtTemp(i).Item("scale") = Format(CDec(txtScale.Decimal), GUI.fmtCurrency)

                    If CInt(cboStation.SelectedValue) > 0 Then
                        dtTemp(i).Item("stationunkid") = CInt(cboStation.SelectedValue)
                        dtTemp(i).Item("station") = cboStation.Text
                    End If

                    If CInt(cboDepartmentGrp.SelectedValue) > 0 Then
                        dtTemp(i).Item("deptgroupunkid") = CInt(cboDepartmentGrp.SelectedValue)
                        dtTemp(i).Item("deptgroup") = cboDepartmentGrp.Text
                    End If

                    If CInt(cboDepartment.SelectedValue) > 0 Then
                        dtTemp(i).Item("departmentunkid") = CInt(cboDepartment.SelectedValue)
                        dtTemp(i).Item("department") = cboDepartment.Text
                    End If

                    If CInt(cboSectionGroup.SelectedValue) > 0 Then
                        dtTemp(i).Item("sectiongrpunkid") = CInt(cboSectionGroup.SelectedValue)
                        dtTemp(i).Item("sectiongroup") = cboSections.Text
                    End If

                    If CInt(cboSections.SelectedValue) > 0 Then
                        dtTemp(i).Item("sectionunkid") = CInt(cboSections.SelectedValue)
                        dtTemp(i).Item("section") = cboSections.Text
                    End If

                    If CInt(cboUnitGroup.SelectedValue) > 0 Then
                        dtTemp(i).Item("unitgroupunkid") = CInt(cboUnitGroup.SelectedValue)
                        dtTemp(i).Item("unitgroup") = cboUnitGroup.Text
                    End If

                    If CInt(cboUnits.SelectedValue) > 0 Then
                        dtTemp(i).Item("unitunkid") = CInt(cboUnits.SelectedValue)
                        dtTemp(i).Item("unit") = cboUnits.Text
                    End If

                    If CInt(cboTeams.SelectedValue) > 0 Then
                        dtTemp(i).Item("teamunkid") = CInt(cboTeams.SelectedValue)
                        dtTemp(i).Item("team") = cboTeams.Text
                    End If

                    If CInt(cboJobGroup.SelectedValue) > 0 Then
                        dtTemp(i).Item("jobgroupunkid") = CInt(cboJobGroup.SelectedValue)
                        dtTemp(i).Item("jobgroup") = cboJobGroup.Text
                    End If

                    If CInt(cboJob.SelectedValue) > 0 Then
                        dtTemp(i).Item("jobunkid") = CInt(cboJob.SelectedValue)
                        dtTemp(i).Item("job") = cboJob.Text
                    End If

                    If CInt(cboClassGroup.SelectedValue) > 0 Then
                        dtTemp(i).Item("classgroupunkid") = CInt(cboClassGroup.SelectedValue)
                        dtTemp(i).Item("classgroup") = cboClassGroup.Text
                    End If

                    If CInt(cboClass.SelectedValue) > 0 Then
                        dtTemp(i).Item("classunkid") = CInt(cboClass.SelectedValue)
                        dtTemp(i).Item("class") = cboClass.Text
                    End If

                    If CInt(cboCostCenter.SelectedValue) > 0 Then
                        dtTemp(i).Item("costcenterunkid") = CInt(cboCostCenter.SelectedValue)
                        dtTemp(i).Item("costcenter") = cboCostCenter.Text
                    End If

                    If CInt(cboEmplType.SelectedValue) > 0 Then
                        dtTemp(i).Item("employmentunkid") = CInt(cboEmplType.SelectedValue)
                        dtTemp(i).Item("employment") = cboEmplType.Text
                    End If

                    dtTemp(i).Item("appointeddate") = dtpAppointedDate.Value.Date
                    dtTemp(i).Item("retirementdate") = dtpRetirementDate.Value.Date 'Sohail (10 Apr 2013)

                    If CInt(cboShift.SelectedValue) > 0 Then
                        dtTemp(i).Item("shiftunkid") = CInt(cboShift.SelectedValue)
                        dtTemp(i).Item("shift") = cboShift.Text
                    End If

                    dtTemp(i).Item("IsChanged") = True
                Next

            End If

            'Pinkal (20-Jan-2012) -- End



            'Pinkal (01-Dec-2012) -- Start
            'Enhancement : TRA Changes

            If mdtTran IsNot Nothing Then mdtTran.AcceptChanges()
            dgvApplicantData_DataSourceChanged(sender, New EventArgs())

            'Pinkal (01-Dec-2012) -- End



            'S.SANDEEP [ 06 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            btnImport.Enabled = True
            'S.SANDEEP [ 06 APR 2013 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetChanges_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 28 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnMoveUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnMoveUp.Click
        Try
            SplitContainer1.Panel1Collapsed = True
            objbtnMoveUp.Visible = False : objbtnMoveDown.Visible = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnMoveUp_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnMoveDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnMoveDown.Click
        Try
            SplitContainer1.Panel1Collapsed = False
            objbtnMoveUp.Visible = True : objbtnMoveDown.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnMoveDown_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboVacType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacType.SelectedIndexChanged
        Try
            Dim objVac As New clsVacancy
            Dim dsVac As New DataSet

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsVac = objVac.getComboList(True, "List", , CInt(cboVacType.SelectedValue))
            dsVac = objVac.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacType.SelectedValue))
            'Shani(24-Aug-2015) -- End

            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsVac.Tables("List")
                .SelectedValue = 0
            End With
            objVac = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Try
            Call CommonSearching(cboVacancy)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmplType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmplType.Click
        Try
            Call CommonSearching(cboEmplType)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmplType_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchShift.Click
        Try
            Call CommonSearching(cboShift)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchShift_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 28 FEB 2012 ] -- END

    'S.SANDEEP [ 21 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboStation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStation.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.DEPARTMENT_GROUP)) = True Then
                Dim objDeptGrp As New clsDepartmentGroup
                Dim dsCombos As New DataSet

                dsCombos = objDeptGrp.getComboList(CInt(cboStation.SelectedValue), "DeptGrp", True)
                With cboDepartmentGrp
                    .ValueMember = "deptgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("DeptGrp")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboStation_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDepartmentGrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepartmentGrp.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.DEPARTMENT)) = True Then
                Dim objDepartment As New clsDepartment
                Dim dsCombos As New DataSet

                dsCombos = objDepartment.getComboList(CInt(cboDepartmentGrp.SelectedValue), "Department", True)
                With cboDepartment
                    .ValueMember = "departmentunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Department")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboDepartmentGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDepartment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepartment.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.SECTION_GROUP)) = True Then
                Dim objSectionGrp As New clsSectionGroup
                Dim dsCombos As New DataSet

                dsCombos = objSectionGrp.getComboList(CInt(cboDepartment.SelectedValue), "List", True)
                With cboSectionGroup
                    .ValueMember = "sectiongroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboDepartment_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSectionGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSectionGroup.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.SECTION)) = True Then
                Dim objSection As New clsSections
                Dim dsCombos As New DataSet

                dsCombos = objSection.getComboList(CInt(cboSectionGroup.SelectedValue), "Section", True)
                With cboSections
                    .ValueMember = "sectionunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Section")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboSectionGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboUnitGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUnitGroup.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.UNIT)) = True Then
                Dim dsCombos As New DataSet
                Dim objUnit As New clsUnits
                dsCombos = objUnit.getComboList("Unit", True, , CInt(cboUnitGroup.SelectedValue))
                With cboUnits
                    .ValueMember = "unitunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Unit")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboUnitGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSections_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSections.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.UNIT_GROUP)) = True Then
                Dim objUnitGroup As New clsUnitGroup
                Dim dsCombos As New DataSet
                dsCombos = objUnitGroup.getComboList("List", True, CInt(cboSections.SelectedValue))
                With cboUnitGroup
                    .ValueMember = "unitgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboSections_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboUnits_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnits.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.TEAM)) = True Then
                Dim dsCombos As New DataSet
                Dim objTeam As New clsTeams
                dsCombos = objTeam.getComboList("List", True, CInt(cboUnits.SelectedValue))
                With cboTeams
                    .ValueMember = "teamunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboUnits_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 21 SEP 2012 ] -- END

    'S.SANDEEP [ 06 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                If objchkAll.CheckState <> CheckState.Indeterminate Then
                    For Each dr As DataRow In mdtTran.Rows
                        dr("IsCheck") = objchkAll.Checked
                    Next
                    mdtTran.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 06 APR 2013 ] -- END

#End Region

#Region "Datagrid Event"

    Private Sub dgvApplicantData_DataSourceChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvApplicantData.DataSourceChanged
        Try

            If dgvApplicantData.DataSource IsNot Nothing AndAlso (ConfigParameter._Object._DisplayNameSetting <> enDisplayNameSetting.NONE And ConfigParameter._Object._DisplayNameSetting <> enDisplayNameSetting.EMPLOYEECODE) Then

                Dim drRow() As DataRow = mdtTran.Select("IsDisplayNameExist = true OR IsEmailExist = true")
                If drRow.Length > 0 Then

                    For i As Integer = 0 To drRow.Length - 1

                        If CBool(drRow(i)("IsDisplayNameExist")) = True Then
                            dgvApplicantData.Rows(mdtTran.Rows.IndexOf(drRow(i))).Cells(dgcolhDisplayName.Index).Style.ForeColor = Color.Red
                        End If

                        If CBool(drRow(i)("IsEmailExist")) = True Then
                            dgvApplicantData.Rows(mdtTran.Rows.IndexOf(drRow(i))).Cells(dgcolhEmail.Index).Style.ForeColor = Color.Red
                        End If

                    Next
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvApplicantData_DataSourceChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvApplicantData_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvApplicantData.CellValueChanged
        Try
            If e.RowIndex < 0 Then Exit Sub

            If ConfigParameter._Object._DisplayNameSetting <> enDisplayNameSetting.NONE And ConfigParameter._Object._DisplayNameSetting <> enDisplayNameSetting.EMPLOYEECODE Then

                If e.ColumnIndex = dgcolhDisplayName.Index Then


                    mdtTran.Rows(e.RowIndex)("displayname") = dgvApplicantData.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString()

                    If ConfigParameter._Object._CompanyDomain.Trim.Length > 0 AndAlso (ConfigParameter._Object._DisplayNameSetting <> enDisplayNameSetting.NONE And ConfigParameter._Object._DisplayNameSetting <> enDisplayNameSetting.EMPLOYEECODE) Then
                        mdtTran.Rows(e.RowIndex)("email") = mdtTran.Rows(e.RowIndex)("displayname").ToString() & "@" & ConfigParameter._Object._CompanyDomain.Trim
                    End If

                    If dgvApplicantData.Rows(e.RowIndex).Cells(dgcolhDisplayName.Index).Value.ToString().Trim.Length > 0 Then
                        Dim objEmp As New clsEmployee_Master
                        Dim objMaster As New clsMasterData

                        If objEmp.isExist("", dgvApplicantData.Rows(e.RowIndex).Cells(dgcolhDisplayName.Index).Value.ToString().Trim, -1) = True Then
                            mdtTran.Rows(e.RowIndex)("IsDisplayNameExist") = True
                        Else
                            mdtTran.Rows(e.RowIndex)("IsDisplayNameExist") = False
                        End If

                        If objMaster.IsEmailPresent("hremployee_master", dgvApplicantData.Rows(e.RowIndex).Cells(dgcolhEmail.Index).Value.ToString().Trim, -1, "employeeunkid") = True Then
                            mdtTran.Rows(e.RowIndex)("IsEmailExist") = True
                        Else
                            mdtTran.Rows(e.RowIndex)("IsEmailExist") = False
                        End If

                    End If
                    mdtTran.AcceptChanges()
                End If
                dgvApplicantData_DataSourceChanged(sender, New EventArgs())
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvApplicantData_CellEndEdit", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 06 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub dgvApplicantData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvApplicantData.CellContentClick
        Try
            If e.ColumnIndex = objdgcolhCheck.Index Then
                mdtTran.Rows(e.RowIndex)(e.ColumnIndex) = Not CBool(mdtTran.Rows(e.RowIndex)(e.ColumnIndex))
                dgvApplicantData.RefreshEdit()
                Dim drRow As DataRow() = mdtTran.Select("IsCheck = true", "")
                If drRow.Length > 0 Then
                    If mdtTran.Rows.Count = drRow.Length Then
                        objchkAll.CheckState = CheckState.Checked
                    Else
                        objchkAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAll.CheckState = CheckState.Unchecked
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvApplicantData_CellContentClick", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 06 APR 2013 ] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbApplicantInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbApplicantInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbOperation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbOperation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbAllocations.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAllocations.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnImport.GradientBackColor = GUI._ButttonBackColor
            Me.btnImport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbApplicantInfo.Text = Language._Object.getCaption(Me.gbApplicantInfo.Name, Me.gbApplicantInfo.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
            Me.lnkSetChanges.Text = Language._Object.getCaption(Me.lnkSetChanges.Name, Me.lnkSetChanges.Text)
            Me.rdApplyChked.Text = Language._Object.getCaption(Me.rdApplyChked.Name, Me.rdApplyChked.Text)
            Me.rdApplyToAll.Text = Language._Object.getCaption(Me.rdApplyToAll.Name, Me.rdApplyToAll.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
            Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
            Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
            Me.DataGridViewTextBoxColumn22.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn22.Name, Me.DataGridViewTextBoxColumn22.HeaderText)
            Me.DataGridViewTextBoxColumn23.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn23.Name, Me.DataGridViewTextBoxColumn23.HeaderText)
            Me.DataGridViewTextBoxColumn24.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn24.Name, Me.DataGridViewTextBoxColumn24.HeaderText)
            Me.DataGridViewTextBoxColumn25.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn25.Name, Me.DataGridViewTextBoxColumn25.HeaderText)
            Me.DataGridViewTextBoxColumn26.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn26.Name, Me.DataGridViewTextBoxColumn26.HeaderText)
            Me.DataGridViewTextBoxColumn27.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn27.Name, Me.DataGridViewTextBoxColumn27.HeaderText)
            Me.DataGridViewTextBoxColumn28.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn28.Name, Me.DataGridViewTextBoxColumn28.HeaderText)
            Me.DataGridViewTextBoxColumn29.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn29.Name, Me.DataGridViewTextBoxColumn29.HeaderText)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
            Me.gbOperation.Text = Language._Object.getCaption(Me.gbOperation.Name, Me.gbOperation.Text)
            Me.gbAllocations.Text = Language._Object.getCaption(Me.gbAllocations.Name, Me.gbAllocations.Text)
            Me.lblVacType.Text = Language._Object.getCaption(Me.lblVacType.Name, Me.lblVacType.Text)
            Me.DataGridViewTextBoxColumn30.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn30.Name, Me.DataGridViewTextBoxColumn30.HeaderText)
            Me.DataGridViewTextBoxColumn31.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn31.Name, Me.DataGridViewTextBoxColumn31.HeaderText)
            Me.DataGridViewTextBoxColumn32.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn32.Name, Me.DataGridViewTextBoxColumn32.HeaderText)
            Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
            Me.lblEmplType.Text = Language._Object.getCaption(Me.lblEmplType.Name, Me.lblEmplType.Text)
            Me.lblSalaryHead.Text = Language._Object.getCaption(Me.lblSalaryHead.Name, Me.lblSalaryHead.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
            Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.Name, Me.lblClassGroup.Text)
            Me.lblScale.Text = Language._Object.getCaption(Me.lblScale.Name, Me.lblScale.Text)
            Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
            Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblUnits.Text = Language._Object.getCaption(Me.lblUnits.Name, Me.lblUnits.Text)
            Me.lblTeam.Text = Language._Object.getCaption(Me.lblTeam.Name, Me.lblTeam.Text)
            Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.Name, Me.lblJobGroup.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblAppointedDate.Text = Language._Object.getCaption(Me.lblAppointedDate.Name, Me.lblAppointedDate.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.Name, Me.lblDepartmentGroup.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.Name, Me.lblSectionGroup.Text)
            Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.DataGridViewTextBoxColumn33.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn33.Name, Me.DataGridViewTextBoxColumn33.HeaderText)
            Me.DataGridViewTextBoxColumn34.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn34.Name, Me.DataGridViewTextBoxColumn34.HeaderText)
            Me.DataGridViewTextBoxColumn35.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn35.Name, Me.DataGridViewTextBoxColumn35.HeaderText)
            Me.DataGridViewTextBoxColumn36.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn36.Name, Me.DataGridViewTextBoxColumn36.HeaderText)
            Me.DataGridViewTextBoxColumn37.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn37.Name, Me.DataGridViewTextBoxColumn37.HeaderText)
            Me.DataGridViewTextBoxColumn38.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn38.Name, Me.DataGridViewTextBoxColumn38.HeaderText)
            Me.lblRetirementDate.Text = Language._Object.getCaption(Me.lblRetirementDate.Name, Me.lblRetirementDate.Text)
            Me.dgcolhApplicant.HeaderText = Language._Object.getCaption(Me.dgcolhApplicant.Name, Me.dgcolhApplicant.HeaderText)
            Me.dgcolhDisplayName.HeaderText = Language._Object.getCaption(Me.dgcolhDisplayName.Name, Me.dgcolhDisplayName.HeaderText)
            Me.dgcolhEmail.HeaderText = Language._Object.getCaption(Me.dgcolhEmail.Name, Me.dgcolhEmail.HeaderText)
            Me.dgcolhAppointedDate.HeaderText = Language._Object.getCaption(Me.dgcolhAppointedDate.Name, Me.dgcolhAppointedDate.HeaderText)
            Me.dgcolhRetirementDate.HeaderText = Language._Object.getCaption(Me.dgcolhRetirementDate.Name, Me.dgcolhRetirementDate.HeaderText)
            Me.dgcolhbranch.HeaderText = Language._Object.getCaption(Me.dgcolhbranch.Name, Me.dgcolhbranch.HeaderText)
            Me.dgcolhDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhDepartment.Name, Me.dgcolhDepartment.HeaderText)
            Me.dgcolhSection.HeaderText = Language._Object.getCaption(Me.dgcolhSection.Name, Me.dgcolhSection.HeaderText)
            Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
            Me.dgcolhGradeGrp.HeaderText = Language._Object.getCaption(Me.dgcolhGradeGrp.Name, Me.dgcolhGradeGrp.HeaderText)
            Me.dgcolhGrade.HeaderText = Language._Object.getCaption(Me.dgcolhGrade.Name, Me.dgcolhGrade.HeaderText)
            Me.dgcolhGradeLevel.HeaderText = Language._Object.getCaption(Me.dgcolhGradeLevel.Name, Me.dgcolhGradeLevel.HeaderText)
            Me.dgcolhScale.HeaderText = Language._Object.getCaption(Me.dgcolhScale.Name, Me.dgcolhScale.HeaderText)
            Me.dgcolhCostcenter.HeaderText = Language._Object.getCaption(Me.dgcolhCostcenter.Name, Me.dgcolhCostcenter.HeaderText)
            Me.dgcolhSalaryHead.HeaderText = Language._Object.getCaption(Me.dgcolhSalaryHead.Name, Me.dgcolhSalaryHead.HeaderText)
            Me.dgcolhEmplType.HeaderText = Language._Object.getCaption(Me.dgcolhEmplType.Name, Me.dgcolhEmplType.HeaderText)
            Me.dgcolhShift.HeaderText = Language._Object.getCaption(Me.dgcolhShift.Name, Me.dgcolhShift.HeaderText)
			Me.chkAssignDefaulTranHeads.Text = Language._Object.getCaption(Me.chkAssignDefaulTranHeads.Name, Me.chkAssignDefaulTranHeads.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please first Create atleast One Period.")
            Language.setMessage(mstrModuleName, 2, "Salary Head is mandatory information. Please select Salary Head to continue.")
            Language.setMessage(mstrModuleName, 3, "Grade Group is mandatory information. Please select Grade Group to continue.")
            Language.setMessage(mstrModuleName, 4, "Salary Grade is mandatory information. Please select Salary Grade to continue.")
            Language.setMessage(mstrModuleName, 5, "Grade Level is mandatory information. Please select Grade Level to continue.")
            Language.setMessage(mstrModuleName, 6, "Scale is mandatory information. Please provide Scale to continue.")
            Language.setMessage(mstrModuleName, 7, "Applicant(s) imported successfully.")
            Language.setMessage(mstrModuleName, 8, "Applicant(s) importation failed.")
            Language.setMessage(mstrModuleName, 9, "Branch is mandatory information. Please select Branch to continue.")
            Language.setMessage(mstrModuleName, 10, "Department is mandatory information. Please select Department to continue.")
            Language.setMessage(mstrModuleName, 11, "Section is mandatory information. Please select Section to continue.")
            Language.setMessage(mstrModuleName, 12, "Job is mandatory information. Please select Job to continue.")
            Language.setMessage(mstrModuleName, 13, "Cost Center is mandatory information. Please select Cost Center to continue.")
            Language.setMessage(mstrModuleName, 14, "Please Select Operation in order to import eligible applicant.")
            Language.setMessage(mstrModuleName, 15, "Notification to Approve newly Hired Employee.")
            Language.setMessage(mstrModuleName, 16, "Vacancy is mandatory information. Please select Vacancy to continue.")
            Language.setMessage(mstrModuleName, 17, "The Appointment Date for some of the Applicants is not set. Please set Appointment Date in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 18, "For some of the Applicants Department is not set. Please set Department in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 19, "Job for some of the Applicants is not set. Please set Job in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 20, "For some of the Applicants Grade Group is not set. Please set Grade Group in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 21, "The Grade for some of the Applicants is not set. Please set Grade in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 22, "The Grade Level for some of the Applicants is not set. Please set Grade Level in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 23, "Salary Head for some of the Applicants is not set. Please set Salary Head in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 24, "The Cost Center for some of the Applicants is not set. Please set Cost Center in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 25, "The Employment Type for some of the Applicants is not set. Please set Employment Type in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 26, "The Shift for some of the Applicants is not set. Please set Shift in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 27, "The Job Group for some of the Applicants is not set. Please set Job Group in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 28, "Email Address for all applicants which are imported as Employees have been set with their Employee Code, so if you would like to change their Email Address then please go to Employee add/edit screen and change them accordingly.")
            Language.setMessage(mstrModuleName, 29, "Some applicants have same display name or email address which are highlighted in list, as those have been allocated to Employee already. Please change highlighted display name or email address.")
            Language.setMessage(mstrModuleName, 31, "Please check atleast one applicant in order to import.")
            Language.setMessage(mstrModuleName, 32, "The Retirement Date for some of the Applicants is not set. Please set Retirement Date in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 33, "Sorry, Retirement Date should be greater than Appointment Date.")
            Language.setMessage(mstrModuleName, 34, "In some of the Applicant(s), Class Group is not set. Please set Class Group in order to import Applicant(s).")
            Language.setMessage(mstrModuleName, 35, "In some of the Applicant(s), Class is not set. Please set Class in order to import Applicant(s).")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
