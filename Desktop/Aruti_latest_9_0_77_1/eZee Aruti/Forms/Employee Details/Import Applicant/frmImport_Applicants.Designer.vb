﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImport_Applicants
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImport_Applicants))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.pnlApplicantInfo = New System.Windows.Forms.Panel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.gbAllocations = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlContainer = New System.Windows.Forms.Panel
        Me.chkAssignDefaulTranHeads = New System.Windows.Forms.CheckBox
        Me.lblRetirementDate = New System.Windows.Forms.Label
        Me.dtpRetirementDate = New System.Windows.Forms.DateTimePicker
        Me.cboShift = New System.Windows.Forms.ComboBox
        Me.lblShift = New System.Windows.Forms.Label
        Me.cboEmplType = New System.Windows.Forms.ComboBox
        Me.lblEmplType = New System.Windows.Forms.Label
        Me.objbtnSearchShift = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmplType = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchCostCenter = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchClass = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchSalaryHead = New eZee.Common.eZeeGradientButton
        Me.lblSalaryHead = New System.Windows.Forms.Label
        Me.cboSalaryHead = New System.Windows.Forms.ComboBox
        Me.objbtnSearchClassGrp = New eZee.Common.eZeeGradientButton
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.lblClass = New System.Windows.Forms.Label
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.lblScale = New System.Windows.Forms.Label
        Me.objbtnSearchJob = New eZee.Common.eZeeGradientButton
        Me.cboGradeGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchGradeLevel = New eZee.Common.eZeeGradientButton
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.objbtnSearchGGroup = New eZee.Common.eZeeGradientButton
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.cboSalaryGrade = New System.Windows.Forms.ComboBox
        Me.objbtnSearchUnits = New eZee.Common.eZeeGradientButton
        Me.lblGrade = New System.Windows.Forms.Label
        Me.lblUnits = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.objbtnSearchSalGrade = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchJobGroup = New eZee.Common.eZeeGradientButton
        Me.cboUnits = New System.Windows.Forms.ComboBox
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.cboTeams = New System.Windows.Forms.ComboBox
        Me.lblTeam = New System.Windows.Forms.Label
        Me.lblJobGroup = New System.Windows.Forms.Label
        Me.objbtnSearchTeam = New eZee.Common.eZeeGradientButton
        Me.lblJob = New System.Windows.Forms.Label
        Me.lblAppointedDate = New System.Windows.Forms.Label
        Me.lblBranch = New System.Windows.Forms.Label
        Me.objbtnSearchDeptGrp = New eZee.Common.eZeeGradientButton
        Me.lblDepartmentGroup = New System.Windows.Forms.Label
        Me.dtpAppointedDate = New System.Windows.Forms.DateTimePicker
        Me.cboDepartmentGrp = New System.Windows.Forms.ComboBox
        Me.objbtnSearchBranch = New eZee.Common.eZeeGradientButton
        Me.cboStation = New System.Windows.Forms.ComboBox
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.objbtnSearchDepartment = New eZee.Common.eZeeGradientButton
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.cboSectionGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchSecGroup = New eZee.Common.eZeeGradientButton
        Me.lblSectionGroup = New System.Windows.Forms.Label
        Me.cboSections = New System.Windows.Forms.ComboBox
        Me.lblUnitGroup = New System.Windows.Forms.Label
        Me.objbtnSearchSection = New eZee.Common.eZeeGradientButton
        Me.lblSection = New System.Windows.Forms.Label
        Me.cboUnitGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchUnitGrp = New eZee.Common.eZeeGradientButton
        Me.txtScale = New eZee.TextBox.NumericTextBox
        Me.gbApplicantInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnMoveDown = New eZee.Common.eZeeGradientButton
        Me.objbtnMoveUp = New eZee.Common.eZeeGradientButton
        Me.objpnlGrid = New System.Windows.Forms.Panel
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dgvApplicantData = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhApplicant = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDisplayName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAppointedDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRetirementDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhbranch = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDepartment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSection = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradeGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGrade = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradeLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhScale = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCostcenter = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSalaryHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmplType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhShift = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApplicantId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhbranchunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhdeptgrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhdeptId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhsectiongrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhsectionId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhunitgroupId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhunitId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhteamId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhjobgroupId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhjobId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGradeGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGradeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGradeLevelId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhclassgroupId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhclassId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhcostcenterId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSalaryHeadId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmplTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhShiftId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsdisplaynameexist = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsemailexist = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbOperation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.rdApplyChked = New System.Windows.Forms.RadioButton
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.lnkSetChanges = New System.Windows.Forms.LinkLabel
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.rdApplyToAll = New System.Windows.Forms.RadioButton
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboVacType = New System.Windows.Forms.ComboBox
        Me.lblVacType = New System.Windows.Forms.Label
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblVacancy = New System.Windows.Forms.Label
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn36 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn38 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhApplicant = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDisplayName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAppointedDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRetirementDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhbranch = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDepartment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSection = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradeGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGrade = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGradeLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhScale = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCostcenter = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSalaryHead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmplType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhShift = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApplicantId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhbranchunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhdeptgrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhdeptId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhsectiongrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhsectionId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhunitgroupId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhunitId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhteamId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhjobgroupId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhjobId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGradeGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGradeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGradeLevelId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhclassgroupId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhclassId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhcostcenterId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSalaryHeadId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmplTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhShiftId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsdisplaynameexist = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsemailexist = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhSectionGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUnitGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTeam = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhClassGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhClass = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.pnlApplicantInfo.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.gbAllocations.SuspendLayout()
        Me.pnlContainer.SuspendLayout()
        Me.gbApplicantInfo.SuspendLayout()
        Me.objpnlGrid.SuspendLayout()
        CType(Me.dgvApplicantData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbOperation.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.pnlApplicantInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(872, 596)
        Me.pnlMainInfo.TabIndex = 0
        '
        'pnlApplicantInfo
        '
        Me.pnlApplicantInfo.Controls.Add(Me.SplitContainer1)
        Me.pnlApplicantInfo.Controls.Add(Me.gbOperation)
        Me.pnlApplicantInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlApplicantInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlApplicantInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlApplicantInfo.Name = "pnlApplicantInfo"
        Me.pnlApplicantInfo.Size = New System.Drawing.Size(872, 541)
        Me.pnlApplicantInfo.TabIndex = 3
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 70)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.gbAllocations)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.gbApplicantInfo)
        Me.SplitContainer1.Size = New System.Drawing.Size(866, 465)
        Me.SplitContainer1.SplitterDistance = 255
        Me.SplitContainer1.SplitterWidth = 2
        Me.SplitContainer1.TabIndex = 250
        '
        'gbAllocations
        '
        Me.gbAllocations.BorderColor = System.Drawing.Color.Black
        Me.gbAllocations.Checked = False
        Me.gbAllocations.CollapseAllExceptThis = False
        Me.gbAllocations.CollapsedHoverImage = Nothing
        Me.gbAllocations.CollapsedNormalImage = Nothing
        Me.gbAllocations.CollapsedPressedImage = Nothing
        Me.gbAllocations.CollapseOnLoad = False
        Me.gbAllocations.Controls.Add(Me.pnlContainer)
        Me.gbAllocations.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAllocations.ExpandedHoverImage = Nothing
        Me.gbAllocations.ExpandedNormalImage = Nothing
        Me.gbAllocations.ExpandedPressedImage = Nothing
        Me.gbAllocations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAllocations.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAllocations.HeaderHeight = 25
        Me.gbAllocations.HeaderMessage = ""
        Me.gbAllocations.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAllocations.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAllocations.HeightOnCollapse = 0
        Me.gbAllocations.LeftTextSpace = 0
        Me.gbAllocations.Location = New System.Drawing.Point(0, 0)
        Me.gbAllocations.Name = "gbAllocations"
        Me.gbAllocations.OpenHeight = 300
        Me.gbAllocations.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAllocations.ShowBorder = True
        Me.gbAllocations.ShowCheckBox = False
        Me.gbAllocations.ShowCollapseButton = False
        Me.gbAllocations.ShowDefaultBorderColor = True
        Me.gbAllocations.ShowDownButton = False
        Me.gbAllocations.ShowHeader = True
        Me.gbAllocations.Size = New System.Drawing.Size(864, 253)
        Me.gbAllocations.TabIndex = 0
        Me.gbAllocations.Temp = 0
        Me.gbAllocations.Text = "Allocations"
        Me.gbAllocations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlContainer
        '
        Me.pnlContainer.AutoScroll = True
        Me.pnlContainer.Controls.Add(Me.chkAssignDefaulTranHeads)
        Me.pnlContainer.Controls.Add(Me.lblRetirementDate)
        Me.pnlContainer.Controls.Add(Me.dtpRetirementDate)
        Me.pnlContainer.Controls.Add(Me.cboShift)
        Me.pnlContainer.Controls.Add(Me.lblShift)
        Me.pnlContainer.Controls.Add(Me.cboEmplType)
        Me.pnlContainer.Controls.Add(Me.lblEmplType)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchShift)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchEmplType)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchCostCenter)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchClass)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchSalaryHead)
        Me.pnlContainer.Controls.Add(Me.lblSalaryHead)
        Me.pnlContainer.Controls.Add(Me.cboSalaryHead)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchClassGrp)
        Me.pnlContainer.Controls.Add(Me.lblCostCenter)
        Me.pnlContainer.Controls.Add(Me.cboCostCenter)
        Me.pnlContainer.Controls.Add(Me.lblClass)
        Me.pnlContainer.Controls.Add(Me.cboClass)
        Me.pnlContainer.Controls.Add(Me.cboClassGroup)
        Me.pnlContainer.Controls.Add(Me.lblClassGroup)
        Me.pnlContainer.Controls.Add(Me.lblScale)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchJob)
        Me.pnlContainer.Controls.Add(Me.cboGradeGroup)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchGradeLevel)
        Me.pnlContainer.Controls.Add(Me.lblGradeGroup)
        Me.pnlContainer.Controls.Add(Me.lblGradeLevel)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchGGroup)
        Me.pnlContainer.Controls.Add(Me.cboGradeLevel)
        Me.pnlContainer.Controls.Add(Me.cboSalaryGrade)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchUnits)
        Me.pnlContainer.Controls.Add(Me.lblGrade)
        Me.pnlContainer.Controls.Add(Me.lblUnits)
        Me.pnlContainer.Controls.Add(Me.cboJob)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchSalGrade)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchJobGroup)
        Me.pnlContainer.Controls.Add(Me.cboUnits)
        Me.pnlContainer.Controls.Add(Me.cboJobGroup)
        Me.pnlContainer.Controls.Add(Me.cboTeams)
        Me.pnlContainer.Controls.Add(Me.lblTeam)
        Me.pnlContainer.Controls.Add(Me.lblJobGroup)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchTeam)
        Me.pnlContainer.Controls.Add(Me.lblJob)
        Me.pnlContainer.Controls.Add(Me.lblAppointedDate)
        Me.pnlContainer.Controls.Add(Me.lblBranch)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchDeptGrp)
        Me.pnlContainer.Controls.Add(Me.lblDepartmentGroup)
        Me.pnlContainer.Controls.Add(Me.dtpAppointedDate)
        Me.pnlContainer.Controls.Add(Me.cboDepartmentGrp)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchBranch)
        Me.pnlContainer.Controls.Add(Me.cboStation)
        Me.pnlContainer.Controls.Add(Me.cboDepartment)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchDepartment)
        Me.pnlContainer.Controls.Add(Me.lblDepartment)
        Me.pnlContainer.Controls.Add(Me.cboSectionGroup)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchSecGroup)
        Me.pnlContainer.Controls.Add(Me.lblSectionGroup)
        Me.pnlContainer.Controls.Add(Me.cboSections)
        Me.pnlContainer.Controls.Add(Me.lblUnitGroup)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchSection)
        Me.pnlContainer.Controls.Add(Me.lblSection)
        Me.pnlContainer.Controls.Add(Me.cboUnitGroup)
        Me.pnlContainer.Controls.Add(Me.objbtnSearchUnitGrp)
        Me.pnlContainer.Controls.Add(Me.txtScale)
        Me.pnlContainer.Location = New System.Drawing.Point(2, 26)
        Me.pnlContainer.Name = "pnlContainer"
        Me.pnlContainer.Size = New System.Drawing.Size(862, 222)
        Me.pnlContainer.TabIndex = 263
        '
        'chkAssignDefaulTranHeads
        '
        Me.chkAssignDefaulTranHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAssignDefaulTranHeads.Location = New System.Drawing.Point(560, 197)
        Me.chkAssignDefaulTranHeads.Name = "chkAssignDefaulTranHeads"
        Me.chkAssignDefaulTranHeads.Size = New System.Drawing.Size(241, 17)
        Me.chkAssignDefaulTranHeads.TabIndex = 324
        Me.chkAssignDefaulTranHeads.Text = "Assign Default Transaction Heads"
        Me.chkAssignDefaulTranHeads.UseVisualStyleBackColor = True
        '
        'lblRetirementDate
        '
        Me.lblRetirementDate.BackColor = System.Drawing.Color.Transparent
        Me.lblRetirementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRetirementDate.Location = New System.Drawing.Point(6, 199)
        Me.lblRetirementDate.Name = "lblRetirementDate"
        Me.lblRetirementDate.Size = New System.Drawing.Size(74, 17)
        Me.lblRetirementDate.TabIndex = 323
        Me.lblRetirementDate.Text = "Retire. Dt."
        Me.lblRetirementDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpRetirementDate
        '
        Me.dtpRetirementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpRetirementDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpRetirementDate.Location = New System.Drawing.Point(86, 197)
        Me.dtpRetirementDate.Name = "dtpRetirementDate"
        Me.dtpRetirementDate.Size = New System.Drawing.Size(118, 21)
        Me.dtpRetirementDate.TabIndex = 322
        '
        'cboShift
        '
        Me.cboShift.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboShift.DropDownWidth = 300
        Me.cboShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboShift.FormattingEnabled = True
        Me.cboShift.Location = New System.Drawing.Point(644, 170)
        Me.cboShift.Name = "cboShift"
        Me.cboShift.Size = New System.Drawing.Size(157, 21)
        Me.cboShift.TabIndex = 319
        '
        'lblShift
        '
        Me.lblShift.BackColor = System.Drawing.SystemColors.Control
        Me.lblShift.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShift.Location = New System.Drawing.Point(556, 172)
        Me.lblShift.Name = "lblShift"
        Me.lblShift.Size = New System.Drawing.Size(82, 17)
        Me.lblShift.TabIndex = 320
        Me.lblShift.Text = "Shift"
        Me.lblShift.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmplType
        '
        Me.cboEmplType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmplType.DropDownWidth = 300
        Me.cboEmplType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmplType.FormattingEnabled = True
        Me.cboEmplType.Location = New System.Drawing.Point(644, 143)
        Me.cboEmplType.Name = "cboEmplType"
        Me.cboEmplType.Size = New System.Drawing.Size(157, 21)
        Me.cboEmplType.TabIndex = 316
        '
        'lblEmplType
        '
        Me.lblEmplType.BackColor = System.Drawing.SystemColors.Control
        Me.lblEmplType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmplType.Location = New System.Drawing.Point(556, 145)
        Me.lblEmplType.Name = "lblEmplType"
        Me.lblEmplType.Size = New System.Drawing.Size(82, 17)
        Me.lblEmplType.TabIndex = 317
        Me.lblEmplType.Text = "Empl. Type"
        Me.lblEmplType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchShift
        '
        Me.objbtnSearchShift.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchShift.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchShift.BorderSelected = False
        Me.objbtnSearchShift.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchShift.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchShift.Location = New System.Drawing.Point(807, 170)
        Me.objbtnSearchShift.Name = "objbtnSearchShift"
        Me.objbtnSearchShift.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchShift.TabIndex = 321
        '
        'objbtnSearchEmplType
        '
        Me.objbtnSearchEmplType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmplType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmplType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmplType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmplType.BorderSelected = False
        Me.objbtnSearchEmplType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmplType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmplType.Location = New System.Drawing.Point(807, 143)
        Me.objbtnSearchEmplType.Name = "objbtnSearchEmplType"
        Me.objbtnSearchEmplType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmplType.TabIndex = 318
        '
        'objbtnSearchCostCenter
        '
        Me.objbtnSearchCostCenter.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCostCenter.BorderSelected = False
        Me.objbtnSearchCostCenter.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCostCenter.Location = New System.Drawing.Point(807, 87)
        Me.objbtnSearchCostCenter.Name = "objbtnSearchCostCenter"
        Me.objbtnSearchCostCenter.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCostCenter.TabIndex = 314
        '
        'objbtnSearchClass
        '
        Me.objbtnSearchClass.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchClass.BorderSelected = False
        Me.objbtnSearchClass.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchClass.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchClass.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchClass.Location = New System.Drawing.Point(807, 61)
        Me.objbtnSearchClass.Name = "objbtnSearchClass"
        Me.objbtnSearchClass.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchClass.TabIndex = 296
        '
        'objbtnSearchSalaryHead
        '
        Me.objbtnSearchSalaryHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSalaryHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSalaryHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSalaryHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSalaryHead.BorderSelected = False
        Me.objbtnSearchSalaryHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSalaryHead.Image = CType(resources.GetObject("objbtnSearchSalaryHead.Image"), System.Drawing.Image)
        Me.objbtnSearchSalaryHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSalaryHead.Location = New System.Drawing.Point(807, 116)
        Me.objbtnSearchSalaryHead.Name = "objbtnSearchSalaryHead"
        Me.objbtnSearchSalaryHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSalaryHead.TabIndex = 280
        '
        'lblSalaryHead
        '
        Me.lblSalaryHead.BackColor = System.Drawing.SystemColors.Control
        Me.lblSalaryHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalaryHead.Location = New System.Drawing.Point(556, 118)
        Me.lblSalaryHead.Name = "lblSalaryHead"
        Me.lblSalaryHead.Size = New System.Drawing.Size(82, 17)
        Me.lblSalaryHead.TabIndex = 279
        Me.lblSalaryHead.Text = "Salary Head"
        Me.lblSalaryHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSalaryHead
        '
        Me.cboSalaryHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSalaryHead.DropDownWidth = 300
        Me.cboSalaryHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSalaryHead.FormattingEnabled = True
        Me.cboSalaryHead.Location = New System.Drawing.Point(644, 116)
        Me.cboSalaryHead.Name = "cboSalaryHead"
        Me.cboSalaryHead.Size = New System.Drawing.Size(157, 21)
        Me.cboSalaryHead.TabIndex = 278
        '
        'objbtnSearchClassGrp
        '
        Me.objbtnSearchClassGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchClassGrp.BorderSelected = False
        Me.objbtnSearchClassGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchClassGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchClassGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchClassGrp.Location = New System.Drawing.Point(807, 35)
        Me.objbtnSearchClassGrp.Name = "objbtnSearchClassGrp"
        Me.objbtnSearchClassGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchClassGrp.TabIndex = 294
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(556, 91)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(82, 17)
        Me.lblCostCenter.TabIndex = 293
        Me.lblCostCenter.Text = "Cost Center"
        Me.lblCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenter.DropDownWidth = 300
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(644, 89)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(157, 21)
        Me.cboCostCenter.TabIndex = 277
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(556, 64)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(82, 17)
        Me.lblClass.TabIndex = 290
        Me.lblClass.Text = "Class"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClass
        '
        Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClass.DropDownWidth = 300
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(644, 62)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(157, 21)
        Me.cboClass.TabIndex = 276
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClassGroup.DropDownWidth = 300
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(644, 35)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(157, 21)
        Me.cboClassGroup.TabIndex = 275
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(556, 37)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(82, 17)
        Me.lblClassGroup.TabIndex = 288
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblScale
        '
        Me.lblScale.BackColor = System.Drawing.SystemColors.Control
        Me.lblScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScale.Location = New System.Drawing.Point(556, 10)
        Me.lblScale.Name = "lblScale"
        Me.lblScale.Size = New System.Drawing.Size(82, 17)
        Me.lblScale.TabIndex = 287
        Me.lblScale.Text = "Salary"
        Me.lblScale.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchJob
        '
        Me.objbtnSearchJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJob.BorderSelected = False
        Me.objbtnSearchJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJob.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJob.Location = New System.Drawing.Point(528, 89)
        Me.objbtnSearchJob.Name = "objbtnSearchJob"
        Me.objbtnSearchJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJob.TabIndex = 312
        '
        'cboGradeGroup
        '
        Me.cboGradeGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeGroup.DropDownWidth = 300
        Me.cboGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGroup.FormattingEnabled = True
        Me.cboGradeGroup.Location = New System.Drawing.Point(365, 116)
        Me.cboGradeGroup.Name = "cboGradeGroup"
        Me.cboGradeGroup.Size = New System.Drawing.Size(157, 21)
        Me.cboGradeGroup.TabIndex = 272
        '
        'objbtnSearchGradeLevel
        '
        Me.objbtnSearchGradeLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGradeLevel.BorderSelected = False
        Me.objbtnSearchGradeLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGradeLevel.Image = CType(resources.GetObject("objbtnSearchGradeLevel.Image"), System.Drawing.Image)
        Me.objbtnSearchGradeLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGradeLevel.Location = New System.Drawing.Point(528, 170)
        Me.objbtnSearchGradeLevel.Name = "objbtnSearchGradeLevel"
        Me.objbtnSearchGradeLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGradeLevel.TabIndex = 286
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.BackColor = System.Drawing.SystemColors.Control
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(277, 118)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(82, 17)
        Me.lblGradeGroup.TabIndex = 281
        Me.lblGradeGroup.Text = "Grade Group"
        Me.lblGradeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.BackColor = System.Drawing.SystemColors.Control
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(277, 172)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(82, 17)
        Me.lblGradeLevel.TabIndex = 285
        Me.lblGradeLevel.Text = "Grade Level"
        Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchGGroup
        '
        Me.objbtnSearchGGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGGroup.BorderSelected = False
        Me.objbtnSearchGGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGGroup.Image = CType(resources.GetObject("objbtnSearchGGroup.Image"), System.Drawing.Image)
        Me.objbtnSearchGGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGGroup.Location = New System.Drawing.Point(528, 116)
        Me.objbtnSearchGGroup.Name = "objbtnSearchGGroup"
        Me.objbtnSearchGGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGGroup.TabIndex = 282
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeLevel.DropDownWidth = 300
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(365, 170)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(157, 21)
        Me.cboGradeLevel.TabIndex = 274
        '
        'cboSalaryGrade
        '
        Me.cboSalaryGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSalaryGrade.DropDownWidth = 300
        Me.cboSalaryGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSalaryGrade.FormattingEnabled = True
        Me.cboSalaryGrade.Location = New System.Drawing.Point(365, 143)
        Me.cboSalaryGrade.Name = "cboSalaryGrade"
        Me.cboSalaryGrade.Size = New System.Drawing.Size(157, 21)
        Me.cboSalaryGrade.TabIndex = 273
        '
        'objbtnSearchUnits
        '
        Me.objbtnSearchUnits.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUnits.BorderSelected = False
        Me.objbtnSearchUnits.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUnits.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUnits.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUnits.Location = New System.Drawing.Point(528, 8)
        Me.objbtnSearchUnits.Name = "objbtnSearchUnits"
        Me.objbtnSearchUnits.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUnits.TabIndex = 311
        '
        'lblGrade
        '
        Me.lblGrade.BackColor = System.Drawing.SystemColors.Control
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(277, 145)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(82, 17)
        Me.lblGrade.TabIndex = 283
        Me.lblGrade.Text = "Salary Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblUnits
        '
        Me.lblUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnits.Location = New System.Drawing.Point(277, 10)
        Me.lblUnits.Name = "lblUnits"
        Me.lblUnits.Size = New System.Drawing.Size(82, 17)
        Me.lblUnits.TabIndex = 305
        Me.lblUnits.Text = "Units"
        Me.lblUnits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 300
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(365, 89)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(157, 21)
        Me.cboJob.TabIndex = 271
        '
        'objbtnSearchSalGrade
        '
        Me.objbtnSearchSalGrade.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSalGrade.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSalGrade.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSalGrade.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSalGrade.BorderSelected = False
        Me.objbtnSearchSalGrade.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSalGrade.Image = CType(resources.GetObject("objbtnSearchSalGrade.Image"), System.Drawing.Image)
        Me.objbtnSearchSalGrade.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSalGrade.Location = New System.Drawing.Point(528, 143)
        Me.objbtnSearchSalGrade.Name = "objbtnSearchSalGrade"
        Me.objbtnSearchSalGrade.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSalGrade.TabIndex = 284
        '
        'objbtnSearchJobGroup
        '
        Me.objbtnSearchJobGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJobGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJobGroup.BorderSelected = False
        Me.objbtnSearchJobGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJobGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJobGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJobGroup.Location = New System.Drawing.Point(528, 62)
        Me.objbtnSearchJobGroup.Name = "objbtnSearchJobGroup"
        Me.objbtnSearchJobGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJobGroup.TabIndex = 309
        '
        'cboUnits
        '
        Me.cboUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnits.DropDownWidth = 300
        Me.cboUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnits.FormattingEnabled = True
        Me.cboUnits.Location = New System.Drawing.Point(365, 8)
        Me.cboUnits.Name = "cboUnits"
        Me.cboUnits.Size = New System.Drawing.Size(157, 21)
        Me.cboUnits.TabIndex = 268
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJobGroup.DropDownWidth = 300
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.Location = New System.Drawing.Point(365, 62)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(157, 21)
        Me.cboJobGroup.TabIndex = 270
        '
        'cboTeams
        '
        Me.cboTeams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTeams.DropDownWidth = 300
        Me.cboTeams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeams.FormattingEnabled = True
        Me.cboTeams.Location = New System.Drawing.Point(365, 35)
        Me.cboTeams.Name = "cboTeams"
        Me.cboTeams.Size = New System.Drawing.Size(157, 21)
        Me.cboTeams.TabIndex = 269
        '
        'lblTeam
        '
        Me.lblTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.Location = New System.Drawing.Point(277, 37)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(82, 17)
        Me.lblTeam.TabIndex = 306
        Me.lblTeam.Text = "Team"
        Me.lblTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobGroup
        '
        Me.lblJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobGroup.Location = New System.Drawing.Point(277, 64)
        Me.lblJobGroup.Name = "lblJobGroup"
        Me.lblJobGroup.Size = New System.Drawing.Size(82, 17)
        Me.lblJobGroup.TabIndex = 307
        Me.lblJobGroup.Text = "Job Group"
        Me.lblJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchTeam
        '
        Me.objbtnSearchTeam.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTeam.BorderSelected = False
        Me.objbtnSearchTeam.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTeam.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTeam.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTeam.Location = New System.Drawing.Point(528, 35)
        Me.objbtnSearchTeam.Name = "objbtnSearchTeam"
        Me.objbtnSearchTeam.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTeam.TabIndex = 310
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(277, 91)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(82, 17)
        Me.lblJob.TabIndex = 308
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAppointedDate
        '
        Me.lblAppointedDate.BackColor = System.Drawing.Color.Transparent
        Me.lblAppointedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointedDate.Location = New System.Drawing.Point(7, 172)
        Me.lblAppointedDate.Name = "lblAppointedDate"
        Me.lblAppointedDate.Size = New System.Drawing.Size(74, 17)
        Me.lblAppointedDate.TabIndex = 315
        Me.lblAppointedDate.Text = "Appointed Dt."
        Me.lblAppointedDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(7, 10)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(74, 17)
        Me.lblBranch.TabIndex = 289
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchDeptGrp
        '
        Me.objbtnSearchDeptGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDeptGrp.BorderSelected = False
        Me.objbtnSearchDeptGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDeptGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDeptGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDeptGrp.Location = New System.Drawing.Point(250, 35)
        Me.objbtnSearchDeptGrp.Name = "objbtnSearchDeptGrp"
        Me.objbtnSearchDeptGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDeptGrp.TabIndex = 295
        '
        'lblDepartmentGroup
        '
        Me.lblDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartmentGroup.Location = New System.Drawing.Point(7, 37)
        Me.lblDepartmentGroup.Name = "lblDepartmentGroup"
        Me.lblDepartmentGroup.Size = New System.Drawing.Size(74, 17)
        Me.lblDepartmentGroup.TabIndex = 292
        Me.lblDepartmentGroup.Text = "Dept. Group"
        Me.lblDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAppointedDate
        '
        Me.dtpAppointedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAppointedDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAppointedDate.Location = New System.Drawing.Point(87, 170)
        Me.dtpAppointedDate.Name = "dtpAppointedDate"
        Me.dtpAppointedDate.Size = New System.Drawing.Size(118, 21)
        Me.dtpAppointedDate.TabIndex = 313
        '
        'cboDepartmentGrp
        '
        Me.cboDepartmentGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartmentGrp.DropDownWidth = 300
        Me.cboDepartmentGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartmentGrp.FormattingEnabled = True
        Me.cboDepartmentGrp.Location = New System.Drawing.Point(87, 35)
        Me.cboDepartmentGrp.Name = "cboDepartmentGrp"
        Me.cboDepartmentGrp.Size = New System.Drawing.Size(157, 21)
        Me.cboDepartmentGrp.TabIndex = 263
        '
        'objbtnSearchBranch
        '
        Me.objbtnSearchBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBranch.BorderSelected = False
        Me.objbtnSearchBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBranch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBranch.Location = New System.Drawing.Point(250, 8)
        Me.objbtnSearchBranch.Name = "objbtnSearchBranch"
        Me.objbtnSearchBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBranch.TabIndex = 291
        '
        'cboStation
        '
        Me.cboStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStation.DropDownWidth = 300
        Me.cboStation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStation.FormattingEnabled = True
        Me.cboStation.Location = New System.Drawing.Point(87, 8)
        Me.cboStation.Name = "cboStation"
        Me.cboStation.Size = New System.Drawing.Size(157, 21)
        Me.cboStation.TabIndex = 262
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.DropDownWidth = 300
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(87, 62)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(157, 21)
        Me.cboDepartment.TabIndex = 264
        '
        'objbtnSearchDepartment
        '
        Me.objbtnSearchDepartment.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDepartment.BorderSelected = False
        Me.objbtnSearchDepartment.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDepartment.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDepartment.Location = New System.Drawing.Point(250, 62)
        Me.objbtnSearchDepartment.Name = "objbtnSearchDepartment"
        Me.objbtnSearchDepartment.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDepartment.TabIndex = 298
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(7, 64)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(74, 17)
        Me.lblDepartment.TabIndex = 297
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSectionGroup
        '
        Me.cboSectionGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSectionGroup.DropDownWidth = 300
        Me.cboSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGroup.FormattingEnabled = True
        Me.cboSectionGroup.Location = New System.Drawing.Point(87, 89)
        Me.cboSectionGroup.Name = "cboSectionGroup"
        Me.cboSectionGroup.Size = New System.Drawing.Size(157, 21)
        Me.cboSectionGroup.TabIndex = 265
        '
        'objbtnSearchSecGroup
        '
        Me.objbtnSearchSecGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSecGroup.BorderSelected = False
        Me.objbtnSearchSecGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSecGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSecGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSecGroup.Location = New System.Drawing.Point(250, 89)
        Me.objbtnSearchSecGroup.Name = "objbtnSearchSecGroup"
        Me.objbtnSearchSecGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSecGroup.TabIndex = 300
        '
        'lblSectionGroup
        '
        Me.lblSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSectionGroup.Location = New System.Drawing.Point(7, 91)
        Me.lblSectionGroup.Name = "lblSectionGroup"
        Me.lblSectionGroup.Size = New System.Drawing.Size(74, 17)
        Me.lblSectionGroup.TabIndex = 299
        Me.lblSectionGroup.Text = "Sec. Group"
        Me.lblSectionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSections
        '
        Me.cboSections.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSections.DropDownWidth = 300
        Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSections.FormattingEnabled = True
        Me.cboSections.Location = New System.Drawing.Point(87, 116)
        Me.cboSections.Name = "cboSections"
        Me.cboSections.Size = New System.Drawing.Size(157, 21)
        Me.cboSections.TabIndex = 266
        '
        'lblUnitGroup
        '
        Me.lblUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitGroup.Location = New System.Drawing.Point(7, 145)
        Me.lblUnitGroup.Name = "lblUnitGroup"
        Me.lblUnitGroup.Size = New System.Drawing.Size(74, 17)
        Me.lblUnitGroup.TabIndex = 302
        Me.lblUnitGroup.Text = "Unit Group"
        Me.lblUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchSection
        '
        Me.objbtnSearchSection.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSection.BorderSelected = False
        Me.objbtnSearchSection.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSection.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSection.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSection.Location = New System.Drawing.Point(250, 116)
        Me.objbtnSearchSection.Name = "objbtnSearchSection"
        Me.objbtnSearchSection.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSection.TabIndex = 303
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(7, 118)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(74, 17)
        Me.lblSection.TabIndex = 301
        Me.lblSection.Text = "Sections"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnitGroup
        '
        Me.cboUnitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnitGroup.DropDownWidth = 300
        Me.cboUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGroup.FormattingEnabled = True
        Me.cboUnitGroup.Location = New System.Drawing.Point(87, 143)
        Me.cboUnitGroup.Name = "cboUnitGroup"
        Me.cboUnitGroup.Size = New System.Drawing.Size(157, 21)
        Me.cboUnitGroup.TabIndex = 267
        '
        'objbtnSearchUnitGrp
        '
        Me.objbtnSearchUnitGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUnitGrp.BorderSelected = False
        Me.objbtnSearchUnitGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUnitGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUnitGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUnitGrp.Location = New System.Drawing.Point(250, 143)
        Me.objbtnSearchUnitGrp.Name = "objbtnSearchUnitGrp"
        Me.objbtnSearchUnitGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUnitGrp.TabIndex = 304
        '
        'txtScale
        '
        Me.txtScale.AllowNegative = True
        Me.txtScale.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtScale.DigitsInGroup = 0
        Me.txtScale.Flags = 0
        Me.txtScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScale.Location = New System.Drawing.Point(644, 8)
        Me.txtScale.MaxDecimalPlaces = 6
        Me.txtScale.MaxWholeDigits = 21
        Me.txtScale.Name = "txtScale"
        Me.txtScale.Prefix = ""
        Me.txtScale.RangeMax = 1.7976931348623157E+308
        Me.txtScale.RangeMin = -1.7976931348623157E+308
        Me.txtScale.Size = New System.Drawing.Size(157, 21)
        Me.txtScale.TabIndex = 14
        Me.txtScale.Text = "0.00"
        Me.txtScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'gbApplicantInfo
        '
        Me.gbApplicantInfo.BorderColor = System.Drawing.Color.Black
        Me.gbApplicantInfo.Checked = False
        Me.gbApplicantInfo.CollapseAllExceptThis = False
        Me.gbApplicantInfo.CollapsedHoverImage = Nothing
        Me.gbApplicantInfo.CollapsedNormalImage = Nothing
        Me.gbApplicantInfo.CollapsedPressedImage = Nothing
        Me.gbApplicantInfo.CollapseOnLoad = False
        Me.gbApplicantInfo.Controls.Add(Me.objbtnMoveDown)
        Me.gbApplicantInfo.Controls.Add(Me.objbtnMoveUp)
        Me.gbApplicantInfo.Controls.Add(Me.objpnlGrid)
        Me.gbApplicantInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbApplicantInfo.ExpandedHoverImage = Nothing
        Me.gbApplicantInfo.ExpandedNormalImage = Nothing
        Me.gbApplicantInfo.ExpandedPressedImage = Nothing
        Me.gbApplicantInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApplicantInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApplicantInfo.HeaderHeight = 25
        Me.gbApplicantInfo.HeaderMessage = ""
        Me.gbApplicantInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbApplicantInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApplicantInfo.HeightOnCollapse = 0
        Me.gbApplicantInfo.LeftTextSpace = 0
        Me.gbApplicantInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbApplicantInfo.Name = "gbApplicantInfo"
        Me.gbApplicantInfo.OpenHeight = 300
        Me.gbApplicantInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApplicantInfo.ShowBorder = True
        Me.gbApplicantInfo.ShowCheckBox = False
        Me.gbApplicantInfo.ShowCollapseButton = False
        Me.gbApplicantInfo.ShowDefaultBorderColor = True
        Me.gbApplicantInfo.ShowDownButton = False
        Me.gbApplicantInfo.ShowHeader = True
        Me.gbApplicantInfo.Size = New System.Drawing.Size(864, 206)
        Me.gbApplicantInfo.TabIndex = 0
        Me.gbApplicantInfo.Tag = "Applicant Info"
        Me.gbApplicantInfo.Temp = 0
        Me.gbApplicantInfo.Text = "Applicant Info"
        Me.gbApplicantInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnMoveDown
        '
        Me.objbtnMoveDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnMoveDown.BackColor = System.Drawing.Color.Transparent
        Me.objbtnMoveDown.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnMoveDown.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnMoveDown.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnMoveDown.BorderSelected = False
        Me.objbtnMoveDown.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnMoveDown.Image = Global.Aruti.Main.My.Resources.Resources.MoveDown_16
        Me.objbtnMoveDown.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnMoveDown.Location = New System.Drawing.Point(840, 2)
        Me.objbtnMoveDown.Name = "objbtnMoveDown"
        Me.objbtnMoveDown.Size = New System.Drawing.Size(21, 21)
        Me.objbtnMoveDown.TabIndex = 253
        Me.objbtnMoveDown.Visible = False
        '
        'objbtnMoveUp
        '
        Me.objbtnMoveUp.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnMoveUp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnMoveUp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnMoveUp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnMoveUp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnMoveUp.BorderSelected = False
        Me.objbtnMoveUp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnMoveUp.Image = Global.Aruti.Main.My.Resources.Resources.MoveUp_16
        Me.objbtnMoveUp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnMoveUp.Location = New System.Drawing.Point(840, 2)
        Me.objbtnMoveUp.Name = "objbtnMoveUp"
        Me.objbtnMoveUp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnMoveUp.TabIndex = 251
        '
        'objpnlGrid
        '
        Me.objpnlGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objpnlGrid.Controls.Add(Me.objchkAll)
        Me.objpnlGrid.Controls.Add(Me.dgvApplicantData)
        Me.objpnlGrid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlGrid.Location = New System.Drawing.Point(3, 26)
        Me.objpnlGrid.Name = "objpnlGrid"
        Me.objpnlGrid.Size = New System.Drawing.Size(858, 178)
        Me.objpnlGrid.TabIndex = 213
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(7, 5)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 20
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'dgvApplicantData
        '
        Me.dgvApplicantData.AllowUserToAddRows = False
        Me.dgvApplicantData.AllowUserToDeleteRows = False
        Me.dgvApplicantData.AllowUserToResizeRows = False
        Me.dgvApplicantData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvApplicantData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvApplicantData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvApplicantData.ColumnHeadersHeight = 21
        Me.dgvApplicantData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvApplicantData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhApplicant, Me.dgcolhDisplayName, Me.dgcolhEmail, Me.dgcolhAppointedDate, Me.dgcolhRetirementDate, Me.dgcolhbranch, Me.dgcolhDepartment, Me.dgcolhSection, Me.dgcolhJob, Me.dgcolhGradeGrp, Me.dgcolhGrade, Me.dgcolhGradeLevel, Me.dgcolhScale, Me.dgcolhCostcenter, Me.dgcolhSalaryHead, Me.dgcolhEmplType, Me.dgcolhShift, Me.objdgcolhApplicantId, Me.objdgcolhbranchunkid, Me.objdgcolhdeptgrpId, Me.objdgcolhdeptId, Me.objdgcolhsectiongrpId, Me.objdgcolhsectionId, Me.objdgcolhunitgroupId, Me.objdgcolhunitId, Me.objdgcolhteamId, Me.objdgcolhjobgroupId, Me.objdgcolhjobId, Me.objdgcolhGradeGrpId, Me.objdgcolhGradeId, Me.objdgcolhGradeLevelId, Me.objdgcolhclassgroupId, Me.objdgcolhclassId, Me.objdgcolhcostcenterId, Me.objdgcolhSalaryHeadId, Me.objdgcolhEmplTypeId, Me.objdgcolhShiftId, Me.objdgcolhIsdisplaynameexist, Me.objdgcolhIsemailexist, Me.dgcolhSectionGrp, Me.dgcolhUnitGrp, Me.dgcolhTeam, Me.dgcolhClassGrp, Me.dgcolhClass})
        Me.dgvApplicantData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvApplicantData.Location = New System.Drawing.Point(0, 0)
        Me.dgvApplicantData.MultiSelect = False
        Me.dgvApplicantData.Name = "dgvApplicantData"
        Me.dgvApplicantData.RowHeadersVisible = False
        Me.dgvApplicantData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvApplicantData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvApplicantData.Size = New System.Drawing.Size(858, 178)
        Me.dgvApplicantData.TabIndex = 19
        '
        'gbOperation
        '
        Me.gbOperation.BorderColor = System.Drawing.Color.Black
        Me.gbOperation.Checked = False
        Me.gbOperation.CollapseAllExceptThis = False
        Me.gbOperation.CollapsedHoverImage = Nothing
        Me.gbOperation.CollapsedNormalImage = Nothing
        Me.gbOperation.CollapsedPressedImage = Nothing
        Me.gbOperation.CollapseOnLoad = False
        Me.gbOperation.Controls.Add(Me.rdApplyChked)
        Me.gbOperation.Controls.Add(Me.EZeeStraightLine1)
        Me.gbOperation.Controls.Add(Me.lnkSetChanges)
        Me.gbOperation.Controls.Add(Me.EZeeStraightLine2)
        Me.gbOperation.Controls.Add(Me.rdApplyToAll)
        Me.gbOperation.ExpandedHoverImage = Nothing
        Me.gbOperation.ExpandedNormalImage = Nothing
        Me.gbOperation.ExpandedPressedImage = Nothing
        Me.gbOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOperation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOperation.HeaderHeight = 25
        Me.gbOperation.HeaderMessage = ""
        Me.gbOperation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbOperation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOperation.HeightOnCollapse = 0
        Me.gbOperation.LeftTextSpace = 0
        Me.gbOperation.Location = New System.Drawing.Point(503, 3)
        Me.gbOperation.Name = "gbOperation"
        Me.gbOperation.OpenHeight = 300
        Me.gbOperation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOperation.ShowBorder = True
        Me.gbOperation.ShowCheckBox = False
        Me.gbOperation.ShowCollapseButton = False
        Me.gbOperation.ShowDefaultBorderColor = True
        Me.gbOperation.ShowDownButton = False
        Me.gbOperation.ShowHeader = True
        Me.gbOperation.Size = New System.Drawing.Size(366, 62)
        Me.gbOperation.TabIndex = 1
        Me.gbOperation.Temp = 0
        Me.gbOperation.Text = "Operation"
        Me.gbOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rdApplyChked
        '
        Me.rdApplyChked.BackColor = System.Drawing.Color.Transparent
        Me.rdApplyChked.Location = New System.Drawing.Point(10, 32)
        Me.rdApplyChked.Name = "rdApplyChked"
        Me.rdApplyChked.Size = New System.Drawing.Size(130, 17)
        Me.rdApplyChked.TabIndex = 214
        Me.rdApplyChked.TabStop = True
        Me.rdApplyChked.Text = "Apply to Checked"
        Me.rdApplyChked.UseVisualStyleBackColor = False
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(268, 25)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(5, 36)
        Me.EZeeStraightLine1.TabIndex = 252
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'lnkSetChanges
        '
        Me.lnkSetChanges.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetChanges.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetChanges.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSetChanges.Location = New System.Drawing.Point(279, 32)
        Me.lnkSetChanges.Name = "lnkSetChanges"
        Me.lnkSetChanges.Size = New System.Drawing.Size(74, 17)
        Me.lnkSetChanges.TabIndex = 216
        Me.lnkSetChanges.TabStop = True
        Me.lnkSetChanges.Text = "Set Changes"
        Me.lnkSetChanges.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(146, 25)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(7, 36)
        Me.EZeeStraightLine2.TabIndex = 251
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'rdApplyToAll
        '
        Me.rdApplyToAll.BackColor = System.Drawing.Color.Transparent
        Me.rdApplyToAll.Location = New System.Drawing.Point(159, 32)
        Me.rdApplyToAll.Name = "rdApplyToAll"
        Me.rdApplyToAll.Size = New System.Drawing.Size(94, 17)
        Me.rdApplyToAll.TabIndex = 213
        Me.rdApplyToAll.TabStop = True
        Me.rdApplyToAll.Text = "Apply To All"
        Me.rdApplyToAll.UseVisualStyleBackColor = False
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboVacType)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(3, 3)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(494, 62)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboVacType
        '
        Me.cboVacType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacType.DropDownWidth = 200
        Me.cboVacType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacType.FormattingEnabled = True
        Me.cboVacType.Location = New System.Drawing.Point(89, 33)
        Me.cboVacType.Name = "cboVacType"
        Me.cboVacType.Size = New System.Drawing.Size(106, 21)
        Me.cboVacType.TabIndex = 254
        '
        'lblVacType
        '
        Me.lblVacType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacType.Location = New System.Drawing.Point(8, 36)
        Me.lblVacType.Name = "lblVacType"
        Me.lblVacType.Size = New System.Drawing.Size(75, 15)
        Me.lblVacType.TabIndex = 253
        Me.lblVacType.Text = "Vacancy Type"
        Me.lblVacType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(465, 33)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 250
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 350
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(260, 33)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(199, 21)
        Me.cboVacancy.TabIndex = 251
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(468, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 139
        Me.objbtnReset.TabStop = False
        '
        'lblVacancy
        '
        Me.lblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancy.Location = New System.Drawing.Point(201, 35)
        Me.lblVacancy.Name = "lblVacancy"
        Me.lblVacancy.Size = New System.Drawing.Size(53, 15)
        Me.lblVacancy.TabIndex = 250
        Me.lblVacancy.Text = "Vacancy"
        Me.lblVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(443, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 138
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnImport)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 541)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(872, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(763, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 21
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.BackColor = System.Drawing.Color.White
        Me.btnImport.BackgroundImage = CType(resources.GetObject("btnImport.BackgroundImage"), System.Drawing.Image)
        Me.btnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnImport.BorderColor = System.Drawing.Color.Empty
        Me.btnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.Color.Black
        Me.btnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnImport.GradientForeColor = System.Drawing.Color.Black
        Me.btnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Location = New System.Drawing.Point(660, 13)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Size = New System.Drawing.Size(97, 30)
        Me.btnImport.TabIndex = 20
        Me.btnImport.Text = "&Import"
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Applicants"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Branch"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 5
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Department"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.HeaderText = "Section"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Job"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Grade Group"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Grade"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Grade Level"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Scale"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Width = 120
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Cost Center"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Width = 120
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Salary Head"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "objdgcolhApplicantId"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Visible = False
        Me.DataGridViewTextBoxColumn12.Width = 120
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "objdgcolhbranchunkid"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "objdgcolhdeptgrpId"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "objdgcolhdeptId"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "objdgcolhsectiongrpId"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "objdgcolhsectionId"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "objdgcolhunitgroupId"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "objdgcolhunitId"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "objdgcolhteamId"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.Visible = False
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "objdgcolhjobgroupId"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.Visible = False
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "objdgcolhjobId"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.Visible = False
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "objdgcolhGradeGrpId"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        Me.DataGridViewTextBoxColumn23.Visible = False
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.HeaderText = "objdgcolhGradeId"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        Me.DataGridViewTextBoxColumn24.Visible = False
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.HeaderText = "objdgcolhGradeLevelId"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        Me.DataGridViewTextBoxColumn25.Visible = False
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.HeaderText = "objdgcolhclassgroupId"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.ReadOnly = True
        Me.DataGridViewTextBoxColumn26.Visible = False
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.HeaderText = "objdgcolhclassId"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.ReadOnly = True
        Me.DataGridViewTextBoxColumn27.Visible = False
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.HeaderText = "objdgcolhcostcenterId"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = True
        Me.DataGridViewTextBoxColumn28.Visible = False
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.HeaderText = "objdgcolhSalaryHeadId"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = True
        Me.DataGridViewTextBoxColumn29.Visible = False
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.HeaderText = "objdgcolhSalaryHeadId"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.ReadOnly = True
        Me.DataGridViewTextBoxColumn30.Visible = False
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.HeaderText = "objdgcolhEmplTypeId"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.ReadOnly = True
        Me.DataGridViewTextBoxColumn31.Visible = False
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.HeaderText = "objdgcolhEmplTypeId"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.ReadOnly = True
        Me.DataGridViewTextBoxColumn32.Visible = False
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.HeaderText = "objdgcolhcostcenterId"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.ReadOnly = True
        Me.DataGridViewTextBoxColumn33.Visible = False
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.HeaderText = "objdgcolhSalaryHeadId"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.Visible = False
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.HeaderText = "objdgcolhEmplTypeId"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        Me.DataGridViewTextBoxColumn35.Visible = False
        '
        'DataGridViewTextBoxColumn36
        '
        Me.DataGridViewTextBoxColumn36.HeaderText = "objdgcolhShiftId"
        Me.DataGridViewTextBoxColumn36.Name = "DataGridViewTextBoxColumn36"
        Me.DataGridViewTextBoxColumn36.Visible = False
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.HeaderText = "Isdisplaynameexist"
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        Me.DataGridViewTextBoxColumn37.ReadOnly = True
        Me.DataGridViewTextBoxColumn37.Visible = False
        '
        'DataGridViewTextBoxColumn38
        '
        Me.DataGridViewTextBoxColumn38.HeaderText = "Isemailexist"
        Me.DataGridViewTextBoxColumn38.Name = "DataGridViewTextBoxColumn38"
        Me.DataGridViewTextBoxColumn38.ReadOnly = True
        Me.DataGridViewTextBoxColumn38.Visible = False
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhApplicant
        '
        Me.dgcolhApplicant.HeaderText = "Applicants"
        Me.dgcolhApplicant.Name = "dgcolhApplicant"
        Me.dgcolhApplicant.ReadOnly = True
        Me.dgcolhApplicant.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDisplayName
        '
        Me.dgcolhDisplayName.HeaderText = "Display Name"
        Me.dgcolhDisplayName.Name = "dgcolhDisplayName"
        Me.dgcolhDisplayName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEmail
        '
        Me.dgcolhEmail.HeaderText = "Email"
        Me.dgcolhEmail.Name = "dgcolhEmail"
        Me.dgcolhEmail.ReadOnly = True
        Me.dgcolhEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAppointedDate
        '
        Me.dgcolhAppointedDate.HeaderText = "Appointed Date"
        Me.dgcolhAppointedDate.Name = "dgcolhAppointedDate"
        Me.dgcolhAppointedDate.ReadOnly = True
        Me.dgcolhAppointedDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhRetirementDate
        '
        Me.dgcolhRetirementDate.HeaderText = "Retirement Date"
        Me.dgcolhRetirementDate.Name = "dgcolhRetirementDate"
        Me.dgcolhRetirementDate.ReadOnly = True
        Me.dgcolhRetirementDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhbranch
        '
        Me.dgcolhbranch.HeaderText = "Branch"
        Me.dgcolhbranch.Name = "dgcolhbranch"
        Me.dgcolhbranch.ReadOnly = True
        Me.dgcolhbranch.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDepartment
        '
        Me.dgcolhDepartment.HeaderText = "Department"
        Me.dgcolhDepartment.Name = "dgcolhDepartment"
        Me.dgcolhDepartment.ReadOnly = True
        Me.dgcolhDepartment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSection
        '
        Me.dgcolhSection.HeaderText = "Section"
        Me.dgcolhSection.Name = "dgcolhSection"
        Me.dgcolhSection.ReadOnly = True
        Me.dgcolhSection.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhJob
        '
        Me.dgcolhJob.HeaderText = "Job"
        Me.dgcolhJob.Name = "dgcolhJob"
        Me.dgcolhJob.ReadOnly = True
        Me.dgcolhJob.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGradeGrp
        '
        Me.dgcolhGradeGrp.HeaderText = "Grade Group"
        Me.dgcolhGradeGrp.Name = "dgcolhGradeGrp"
        Me.dgcolhGradeGrp.ReadOnly = True
        Me.dgcolhGradeGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGrade
        '
        Me.dgcolhGrade.HeaderText = "Grade"
        Me.dgcolhGrade.Name = "dgcolhGrade"
        Me.dgcolhGrade.ReadOnly = True
        Me.dgcolhGrade.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhGradeLevel
        '
        Me.dgcolhGradeLevel.HeaderText = "Grade Level"
        Me.dgcolhGradeLevel.Name = "dgcolhGradeLevel"
        Me.dgcolhGradeLevel.ReadOnly = True
        Me.dgcolhGradeLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhScale
        '
        Me.dgcolhScale.HeaderText = "Scale"
        Me.dgcolhScale.Name = "dgcolhScale"
        Me.dgcolhScale.ReadOnly = True
        Me.dgcolhScale.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhScale.Width = 120
        '
        'dgcolhCostcenter
        '
        Me.dgcolhCostcenter.HeaderText = "Cost Center"
        Me.dgcolhCostcenter.Name = "dgcolhCostcenter"
        Me.dgcolhCostcenter.ReadOnly = True
        Me.dgcolhCostcenter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhSalaryHead
        '
        Me.dgcolhSalaryHead.HeaderText = "Salary Head"
        Me.dgcolhSalaryHead.Name = "dgcolhSalaryHead"
        Me.dgcolhSalaryHead.ReadOnly = True
        Me.dgcolhSalaryHead.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEmplType
        '
        Me.dgcolhEmplType.HeaderText = "Employment Type"
        Me.dgcolhEmplType.Name = "dgcolhEmplType"
        Me.dgcolhEmplType.ReadOnly = True
        Me.dgcolhEmplType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhShift
        '
        Me.dgcolhShift.HeaderText = "Shift"
        Me.dgcolhShift.Name = "dgcolhShift"
        Me.dgcolhShift.ReadOnly = True
        Me.dgcolhShift.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhApplicantId
        '
        Me.objdgcolhApplicantId.HeaderText = "objdgcolhApplicantId"
        Me.objdgcolhApplicantId.Name = "objdgcolhApplicantId"
        Me.objdgcolhApplicantId.Visible = False
        '
        'objdgcolhbranchunkid
        '
        Me.objdgcolhbranchunkid.HeaderText = "objdgcolhbranchunkid"
        Me.objdgcolhbranchunkid.Name = "objdgcolhbranchunkid"
        Me.objdgcolhbranchunkid.Visible = False
        '
        'objdgcolhdeptgrpId
        '
        Me.objdgcolhdeptgrpId.HeaderText = "objdgcolhdeptgrpId"
        Me.objdgcolhdeptgrpId.Name = "objdgcolhdeptgrpId"
        Me.objdgcolhdeptgrpId.ReadOnly = True
        Me.objdgcolhdeptgrpId.Visible = False
        '
        'objdgcolhdeptId
        '
        Me.objdgcolhdeptId.HeaderText = "objdgcolhdeptId"
        Me.objdgcolhdeptId.Name = "objdgcolhdeptId"
        Me.objdgcolhdeptId.ReadOnly = True
        Me.objdgcolhdeptId.Visible = False
        '
        'objdgcolhsectiongrpId
        '
        Me.objdgcolhsectiongrpId.HeaderText = "objdgcolhsectiongrpId"
        Me.objdgcolhsectiongrpId.Name = "objdgcolhsectiongrpId"
        Me.objdgcolhsectiongrpId.ReadOnly = True
        Me.objdgcolhsectiongrpId.Visible = False
        '
        'objdgcolhsectionId
        '
        Me.objdgcolhsectionId.HeaderText = "objdgcolhsectionId"
        Me.objdgcolhsectionId.Name = "objdgcolhsectionId"
        Me.objdgcolhsectionId.ReadOnly = True
        Me.objdgcolhsectionId.Visible = False
        '
        'objdgcolhunitgroupId
        '
        Me.objdgcolhunitgroupId.HeaderText = "objdgcolhunitgroupId"
        Me.objdgcolhunitgroupId.Name = "objdgcolhunitgroupId"
        Me.objdgcolhunitgroupId.ReadOnly = True
        Me.objdgcolhunitgroupId.Visible = False
        '
        'objdgcolhunitId
        '
        Me.objdgcolhunitId.HeaderText = "objdgcolhunitId"
        Me.objdgcolhunitId.Name = "objdgcolhunitId"
        Me.objdgcolhunitId.ReadOnly = True
        Me.objdgcolhunitId.Visible = False
        '
        'objdgcolhteamId
        '
        Me.objdgcolhteamId.HeaderText = "objdgcolhteamId"
        Me.objdgcolhteamId.Name = "objdgcolhteamId"
        Me.objdgcolhteamId.ReadOnly = True
        Me.objdgcolhteamId.Visible = False
        '
        'objdgcolhjobgroupId
        '
        Me.objdgcolhjobgroupId.HeaderText = "objdgcolhjobgroupId"
        Me.objdgcolhjobgroupId.Name = "objdgcolhjobgroupId"
        Me.objdgcolhjobgroupId.ReadOnly = True
        Me.objdgcolhjobgroupId.Visible = False
        '
        'objdgcolhjobId
        '
        Me.objdgcolhjobId.HeaderText = "objdgcolhjobId"
        Me.objdgcolhjobId.Name = "objdgcolhjobId"
        Me.objdgcolhjobId.ReadOnly = True
        Me.objdgcolhjobId.Visible = False
        '
        'objdgcolhGradeGrpId
        '
        Me.objdgcolhGradeGrpId.HeaderText = "objdgcolhGradeGrpId"
        Me.objdgcolhGradeGrpId.Name = "objdgcolhGradeGrpId"
        Me.objdgcolhGradeGrpId.Visible = False
        '
        'objdgcolhGradeId
        '
        Me.objdgcolhGradeId.HeaderText = "objdgcolhGradeId"
        Me.objdgcolhGradeId.Name = "objdgcolhGradeId"
        Me.objdgcolhGradeId.Visible = False
        '
        'objdgcolhGradeLevelId
        '
        Me.objdgcolhGradeLevelId.HeaderText = "objdgcolhGradeLevelId"
        Me.objdgcolhGradeLevelId.Name = "objdgcolhGradeLevelId"
        Me.objdgcolhGradeLevelId.Visible = False
        '
        'objdgcolhclassgroupId
        '
        Me.objdgcolhclassgroupId.HeaderText = "objdgcolhclassgroupId"
        Me.objdgcolhclassgroupId.Name = "objdgcolhclassgroupId"
        Me.objdgcolhclassgroupId.ReadOnly = True
        Me.objdgcolhclassgroupId.Visible = False
        '
        'objdgcolhclassId
        '
        Me.objdgcolhclassId.HeaderText = "objdgcolhclassId"
        Me.objdgcolhclassId.Name = "objdgcolhclassId"
        Me.objdgcolhclassId.ReadOnly = True
        Me.objdgcolhclassId.Visible = False
        '
        'objdgcolhcostcenterId
        '
        Me.objdgcolhcostcenterId.HeaderText = "objdgcolhcostcenterId"
        Me.objdgcolhcostcenterId.Name = "objdgcolhcostcenterId"
        Me.objdgcolhcostcenterId.ReadOnly = True
        Me.objdgcolhcostcenterId.Visible = False
        '
        'objdgcolhSalaryHeadId
        '
        Me.objdgcolhSalaryHeadId.HeaderText = "objdgcolhSalaryHeadId"
        Me.objdgcolhSalaryHeadId.Name = "objdgcolhSalaryHeadId"
        Me.objdgcolhSalaryHeadId.Visible = False
        '
        'objdgcolhEmplTypeId
        '
        Me.objdgcolhEmplTypeId.HeaderText = "objdgcolhEmplTypeId"
        Me.objdgcolhEmplTypeId.Name = "objdgcolhEmplTypeId"
        Me.objdgcolhEmplTypeId.Visible = False
        '
        'objdgcolhShiftId
        '
        Me.objdgcolhShiftId.HeaderText = "objdgcolhShiftId"
        Me.objdgcolhShiftId.Name = "objdgcolhShiftId"
        Me.objdgcolhShiftId.Visible = False
        '
        'objdgcolhIsdisplaynameexist
        '
        Me.objdgcolhIsdisplaynameexist.HeaderText = "Isdisplaynameexist"
        Me.objdgcolhIsdisplaynameexist.Name = "objdgcolhIsdisplaynameexist"
        Me.objdgcolhIsdisplaynameexist.ReadOnly = True
        Me.objdgcolhIsdisplaynameexist.Visible = False
        '
        'objdgcolhIsemailexist
        '
        Me.objdgcolhIsemailexist.HeaderText = "Isemailexist"
        Me.objdgcolhIsemailexist.Name = "objdgcolhIsemailexist"
        Me.objdgcolhIsemailexist.ReadOnly = True
        Me.objdgcolhIsemailexist.Visible = False
        '
        'dgcolhSectionGrp
        '
        Me.dgcolhSectionGrp.HeaderText = "Section Group"
        Me.dgcolhSectionGrp.Name = "dgcolhSectionGrp"
        Me.dgcolhSectionGrp.ReadOnly = True
        '
        'dgcolhUnitGrp
        '
        Me.dgcolhUnitGrp.HeaderText = "Unit Group"
        Me.dgcolhUnitGrp.Name = "dgcolhUnitGrp"
        Me.dgcolhUnitGrp.ReadOnly = True
        '
        'dgcolhTeam
        '
        Me.dgcolhTeam.HeaderText = "Team"
        Me.dgcolhTeam.Name = "dgcolhTeam"
        Me.dgcolhTeam.ReadOnly = True
        '
        'dgcolhClassGrp
        '
        Me.dgcolhClassGrp.HeaderText = "Class Group"
        Me.dgcolhClassGrp.Name = "dgcolhClassGrp"
        Me.dgcolhClassGrp.ReadOnly = True
        '
        'dgcolhClass
        '
        Me.dgcolhClass.HeaderText = "Class"
        Me.dgcolhClass.Name = "dgcolhClass"
        Me.dgcolhClass.ReadOnly = True
        '
        'frmImport_Applicants
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(872, 596)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImport_Applicants"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Applicants"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlApplicantInfo.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.gbAllocations.ResumeLayout(False)
        Me.pnlContainer.ResumeLayout(False)
        Me.pnlContainer.PerformLayout()
        Me.gbApplicantInfo.ResumeLayout(False)
        Me.objpnlGrid.ResumeLayout(False)
        Me.objpnlGrid.PerformLayout()
        CType(Me.dgvApplicantData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbOperation.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbApplicantInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnImport As eZee.Common.eZeeLightButton
    Friend WithEvents txtScale As eZee.TextBox.NumericTextBox
    Friend WithEvents dgvApplicantData As System.Windows.Forms.DataGridView
    Friend WithEvents objpnlGrid As System.Windows.Forms.Panel
    Friend WithEvents pnlApplicantInfo As System.Windows.Forms.Panel
    Friend WithEvents lnkSetChanges As System.Windows.Forms.LinkLabel
    Friend WithEvents rdApplyChked As System.Windows.Forms.RadioButton
    Friend WithEvents rdApplyToAll As System.Windows.Forms.RadioButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancy As System.Windows.Forms.Label
    Friend WithEvents gbOperation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents gbAllocations As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnMoveUp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnMoveDown As eZee.Common.eZeeGradientButton
    Friend WithEvents cboVacType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacType As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlContainer As System.Windows.Forms.Panel
    Friend WithEvents cboShift As System.Windows.Forms.ComboBox
    Friend WithEvents lblShift As System.Windows.Forms.Label
    Friend WithEvents cboEmplType As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmplType As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchShift As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmplType As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchCostCenter As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchClass As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchSalaryHead As eZee.Common.eZeeGradientButton
    Friend WithEvents lblSalaryHead As System.Windows.Forms.Label
    Friend WithEvents cboSalaryHead As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchClassGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents lblScale As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchJob As eZee.Common.eZeeGradientButton
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchGradeLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchGGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents cboSalaryGrade As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchUnits As eZee.Common.eZeeGradientButton
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents lblUnits As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchSalGrade As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchJobGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboUnits As System.Windows.Forms.ComboBox
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboTeams As System.Windows.Forms.ComboBox
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents lblJobGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchTeam As eZee.Common.eZeeGradientButton
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents lblAppointedDate As System.Windows.Forms.Label
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchDeptGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDepartmentGroup As System.Windows.Forms.Label
    Friend WithEvents dtpAppointedDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboDepartmentGrp As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboStation As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchDepartment As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents cboSectionGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchSecGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents lblSectionGroup As System.Windows.Forms.Label
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchSection As eZee.Common.eZeeGradientButton
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents cboUnitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchUnitGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn36 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn37 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn38 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents lblRetirementDate As System.Windows.Forms.Label
    Friend WithEvents dtpRetirementDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkAssignDefaulTranHeads As System.Windows.Forms.CheckBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhApplicant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDisplayName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAppointedDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRetirementDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhbranch As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDepartment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSection As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGradeGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGrade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGradeLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhScale As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCostcenter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSalaryHead As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmplType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhShift As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApplicantId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhbranchunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhdeptgrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhdeptId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhsectiongrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhsectionId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhunitgroupId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhunitId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhteamId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhjobgroupId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhjobId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGradeGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGradeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGradeLevelId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhclassgroupId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhclassId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhcostcenterId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSalaryHeadId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmplTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhShiftId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsdisplaynameexist As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsemailexist As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhSectionGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUnitGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTeam As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhClassGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhClass As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
