﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmployeeReferee

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployeeReferee"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objReferee As clsEmployee_Refree_tran
    Private mintRefereeUnkid As Integer = -1
    Private mintSeletedEmpId As Integer = -1

    'S.SANDEEP [ 14 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 14 AUG 2012 ] -- END

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objARefreeTran As clsemployee_refree_approval_tran

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim RefreeApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmEmployeeRefereeList)))
    'Gajanan [17-DEC-2018] -- End

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim isEmployeeApprove As Boolean = False
    'Gajanan [22-Feb-2019] -- End


    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End

    'Gajanan [21-June-2019] -- Start      
    Private OldData As clsEmployee_Refree_tran
    'Gajanan [21-June-2019] -- End
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmployeeUnkid As Integer = -1) As Boolean 'S.SANDEEP [ 14 AUG 2012 ] -- START -- END
        'Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintRefereeUnkid = intUnkId
            menAction = eAction

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mintEmployeeUnkid = intEmployeeUnkid
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            Me.ShowDialog()

            intUnkId = mintRefereeUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboCountry.BackColor = GUI.ColorOptional
            cboEmployee.BackColor = GUI.ColorComp
            cboGender.BackColor = GUI.ColorOptional
            cboState.BackColor = GUI.ColorOptional
            cboTown.BackColor = GUI.ColorOptional
            txtAddress.BackColor = GUI.ColorOptional
            txtCompany.BackColor = GUI.ColorOptional
            txtRefreeName.BackColor = GUI.ColorComp
            txtAddress.BackColor = GUI.ColorOptional
            txtEmailAddress.BackColor = GUI.ColorOptional
            txtTelNo.BackColor = GUI.ColorOptional
            txtMobileNo.BackColor = GUI.ColorOptional
            'Sandeep [ 17 DEC 2010 ] -- Start
            cboRefereeType.BackColor = GUI.ColorOptional
            'Sandeep [ 17 DEC 2010 ] -- End 

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtPosition.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 28 FEB 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'objReferee._Address = txtAddress.Text
            'objReferee._Cityunkid = CInt(cboTown.SelectedValue)
            'objReferee._Countryunkid = CInt(cboCountry.SelectedValue)
            'objReferee._Email = txtEmailAddress.Text
            'objReferee._Employeeunkid = CInt(cboEmployee.SelectedValue)
            ''objReferee._Gender = cboGender.Text
            'objReferee._Gender = CInt(cboGender.SelectedValue)
            'objReferee._Company = txtCompany.Text
            'objReferee._Mobile_No = txtMobileNo.Text
            'objReferee._Name = txtRefreeName.Text
            'objReferee._Stateunkid = CInt(cboState.SelectedValue)
            'objReferee._Telephone_No = txtTelNo.Text
            'If mintRefereeUnkid = -1 Then
            '    objReferee._Userunkid = User._Object._Userunkid
            '    objReferee._Isvoid = False
            '    objReferee._Voiddatetime = Nothing
            '    objReferee._Voidreason = ""
            '    objReferee._Voiduserunkid = -1
            'Else
            '    objReferee._Userunkid = objReferee._Userunkid
            '    objReferee._Isvoid = objReferee._Isvoid
            '    objReferee._Voiddatetime = objReferee._Voiddatetime
            '    objReferee._Voidreason = objReferee._Voidreason
            '    objReferee._Voiduserunkid = objReferee._Voiduserunkid
            'End If

            ''Sandeep [ 17 DEC 2010 ] -- Start
            'objReferee._Relationunkid = CInt(cboRefereeType.SelectedValue)
            ''Sandeep [ 17 DEC 2010 ] -- End

            ''S.SANDEEP [ 28 FEB 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'objReferee._Ref_Position = txtPosition.Text
            ''S.SANDEEP [ 28 FEB 2012 ] -- END

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If RefreeApprovalFlowVal Is Nothing Then
            If RefreeApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                'Gajanan [22-Feb-2019] -- End
                objARefreeTran._Audittype = enAuditType.ADD
                objARefreeTran._Audituserunkid = User._Object._Userunkid
                objARefreeTran._Address = txtAddress.Text
                objARefreeTran._Cityunkid = CInt(cboTown.SelectedValue)
                objARefreeTran._Countryunkid = CInt(cboCountry.SelectedValue)
                objARefreeTran._Email = txtEmailAddress.Text
                objARefreeTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objARefreeTran._Gender = CInt(cboGender.SelectedValue)
                objARefreeTran._Company = txtCompany.Text
                objARefreeTran._Mobile_No = txtMobileNo.Text
                objARefreeTran._Name = txtRefreeName.Text
                objARefreeTran._Stateunkid = CInt(cboState.SelectedValue)
                objARefreeTran._Telephone_No = txtTelNo.Text
                objARefreeTran._Relationunkid = CInt(cboRefereeType.SelectedValue)
                objARefreeTran._Ref_Position = txtPosition.Text
                objARefreeTran._Isvoid = False
                objARefreeTran._Tranguid = Guid.NewGuid.ToString()
                objARefreeTran._Transactiondate = Now
                objARefreeTran._Approvalremark = ""
                objARefreeTran._Isweb = False
                objARefreeTran._Isfinal = False
                objARefreeTran._Ip = getIP()
                objARefreeTran._Host = getHostName()
                objARefreeTran._Form_Name = mstrModuleName
                objARefreeTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                If menAction <> enAction.EDIT_ONE Then
                    objARefreeTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.ADDED
                    objARefreeTran._Refereetranunkid = -1
                Else
                    objARefreeTran._Refereetranunkid = mintRefereeUnkid
                    objARefreeTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.EDITED
                End If
            Else
            objReferee._Address = txtAddress.Text
            objReferee._Cityunkid = CInt(cboTown.SelectedValue)
            objReferee._Countryunkid = CInt(cboCountry.SelectedValue)
            objReferee._Email = txtEmailAddress.Text
            objReferee._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objReferee._Gender = CInt(cboGender.SelectedValue)
            objReferee._Company = txtCompany.Text
            objReferee._Mobile_No = txtMobileNo.Text
            objReferee._Name = txtRefreeName.Text
            objReferee._Stateunkid = CInt(cboState.SelectedValue)
            objReferee._Telephone_No = txtTelNo.Text
            If mintRefereeUnkid = -1 Then
                objReferee._Userunkid = User._Object._Userunkid
                objReferee._Isvoid = False
                objReferee._Voiddatetime = Nothing
                objReferee._Voidreason = ""
                objReferee._Voiduserunkid = -1
            Else
                objReferee._Userunkid = objReferee._Userunkid
                objReferee._Isvoid = objReferee._Isvoid
                objReferee._Voiddatetime = objReferee._Voiddatetime
                objReferee._Voidreason = objReferee._Voidreason
                objReferee._Voiduserunkid = objReferee._Voiduserunkid
            End If
            objReferee._Relationunkid = CInt(cboRefereeType.SelectedValue)
            objReferee._Ref_Position = txtPosition.Text
            End If
            'Gajanan [17-DEC-2018] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtAddress.Text = objReferee._Address
            cboTown.SelectedValue = objReferee._Cityunkid
            cboCountry.SelectedValue = objReferee._Countryunkid
            txtEmailAddress.Text = objReferee._Email
            cboEmployee.SelectedValue = objReferee._Employeeunkid
            'cboGender.Text = objReferee._Gender
            cboGender.SelectedValue = objReferee._Gender
            txtCompany.Text = objReferee._Company
            txtMobileNo.Text = objReferee._Mobile_No
            txtRefreeName.Text = objReferee._Name
            cboState.SelectedValue = objReferee._Stateunkid
            txtTelNo.Text = objReferee._Telephone_No
            'Sandeep [ 17 DEC 2010 ] -- Start
            cboRefereeType.SelectedValue = CInt(objReferee._Relationunkid)
            'Sandeep [ 17 DEC 2010 ] -- End
            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtPosition.Text = objReferee._Ref_Position
            'S.SANDEEP [ 28 FEB 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        'Sandeep [ 17 DEC 2010 ] -- Start
        Dim objCMaster As New clsCommon_Master
        'Sandeep [ 17 DEC 2010 ] -- End 
        Try

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsList = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If menAction = enAction.EDIT_ONE Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If menAction = enAction.EDIT_ONE Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , , , , False)
            'End If


            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployeeRefereeList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, _
                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


            'S.SANDEEP [20-JUN-2018] -- End


            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 08 OCT 2012 ] -- END

            
            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            dsList = objMaster.getCountryList("Country", True)
            With cboCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsList.Tables("Country")
                .SelectedValue = 0
            End With
            dsList = objMaster.getGenderList("Gender", True)
            With cboGender
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Gender")
                .SelectedValue = 0
            End With
            'Sandeep [ 17 DEC 2010 ] -- Start
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation", , True)
            With cboRefereeType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("RELATION")
                .SelectedValue = 0
            End With
            'Sandeep [ 17 DEC 2010 ] -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            'Gajanan [21-June-2019] -- Start      
            If IsNothing(OldData) = False Then
                If OldData._Name = txtRefreeName.Text AndAlso _
                   OldData._Relationunkid = CInt(cboRefereeType.SelectedValue) AndAlso _
                   OldData._Address = txtAddress.Text AndAlso _
                   OldData._Countryunkid = CInt(cboCountry.SelectedValue) AndAlso _
                   OldData._Stateunkid = CInt(cboState.SelectedValue) AndAlso _
                   OldData._Cityunkid = CInt(cboTown.SelectedValue) AndAlso _
                   OldData._Gender = CInt(cboGender.SelectedValue) AndAlso _
                   OldData._Telephone_No = txtTelNo.Text AndAlso _
                   OldData._Mobile_No = txtMobileNo.Text AndAlso _
                   OldData._Email = txtEmailAddress.Text AndAlso _
                   OldData._Company = txtCompany.Text AndAlso _
                   OldData._Ref_Position = txtPosition.Text Then

                    If menAction = enAction.EDIT_ONE Then
                        objReferee = Nothing
                        OldData = Nothing
                        Me.Close()
                        Return False
                    End If
                Else
                    OldData = Nothing
                End If
            End If
            'Gajanan [21-June-2019] -- End



            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If txtRefreeName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Referee Name cannot be blank. Referee Name is compulsory information."), enMsgBoxStyle.Information)
                txtRefreeName.Focus()
                Return False
            End If

            'Sandeep [ 14 Aug 2010 ] -- Start

            'S.SANDEEP [ 11 MAR 2014 ] -- START
            'Dim strExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
            Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
            'S.SANDEEP [ 11 MAR 2014 ] -- END

            If txtEmailAddress.Text.Length > 0 Then
                If Expression.IsMatch(txtEmailAddress.Text.Trim) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Invalid Email.Please Enter Valid Email"), enMsgBoxStyle.Information)
                    txtEmailAddress.Focus()
                    Return False
                End If
            End If
            'Sandeep [ 14 Aug 2010 ] -- End 

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If RefreeApprovalFlowVal Is Nothing Then

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If objApprovalData.IsApproverPresent(enScreenName.frmEmployeeRefereeList, FinancialYear._Object._DatabaseName, _
                                                      ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                      FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences), _
                                                      User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(cboEmployee.SelectedValue.ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)
                    Return False
                End If
                'Gajanan [17-April-2019] -- End


                If objReferee.isExist(CInt(cboEmployee.SelectedValue), txtRefreeName.Text.Trim, mintRefereeUnkid, Nothing) = True Then
                    eZeeMsgBox.Show(Language.getMessage("clsEmployee_Refree_tran", 1, "This Referee is already defined. Please define new Referee."), enMsgBoxStyle.Information)
                    Return False
                End If

                Dim intOperationType As Integer = 0
                Dim strMsg As String = ""
                If objARefreeTran.isExist(CInt(cboEmployee.SelectedValue), txtRefreeName.Text.Trim, Nothing, -1, "", Nothing, False, intOperationType) = True Then
                    If intOperationType > 0 Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                If menAction = enAction.EDIT_ONE Then
                                    strMsg = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in edit mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 5, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.DELETED
                                If menAction = enAction.EDIT_ONE Then
                                    strMsg = Language.getMessage(mstrModuleName, 6, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in deleted mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 7, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.ADDED
                                If menAction = enAction.EDIT_ONE Then
                                    strMsg = Language.getMessage(mstrModuleName, 8, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in added mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 9, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
                                End If
                        End Select
                    End If
                End If
                If strMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub SetVisibility()

        Try
            objbtnAddRefereeType.Enabled = User._Object.Privilege._AddCommonMasters

            'Gajanan [17-DEC-2018] -- Start
            If mintRefereeUnkid > 0 Then
                cboEmployee.Enabled = False
            Else
                cboEmployee.Enabled = True
            End If
            'Gajanan [17-DEC-2018] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmEmployeeReferee_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objReferee = Nothing
    End Sub

    Private Sub frmEmployeeReferee_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSaveInfo.PerformClick()
        End If
    End Sub

    Private Sub frmEmployeeReferee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployeeReferee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objReferee = New clsEmployee_Refree_tran
        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        objARefreeTran = New clsemployee_refree_approval_tran
        'Gajanan [17-DEC-2018] -- End

        'Gajanan [9-April-2019] -- Start
        objApprovalData = New clsEmployeeDataApproval
        'Gajanan [9-April-2019] -- End
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objReferee._Refereetranunkid = mintRefereeUnkid
                'Sandeep [ 17 Aug 2010 ] -- Start
                cboEmployee.Enabled = False
                'Sandeep [ 09 Oct 2010 ] -- Start
                'Issues Reported by Vimal
                objbtnSearchEmployee.Enabled = False
                'Sandeep [ 09 Oct 2010 ] -- End 
                'Sandeep [ 17 Aug 2010 ] -- End 

                'Gajanan [21-June-2019] -- Start      
                OldData = objReferee
                'Gajanan [21-June-2019] -- End
            End If

            Call GetValue()

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            cboEmployee.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeReferences_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Refree_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Refree_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveInfo.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objReferee._FormName = mstrModuleName
            objReferee._LoginEmployeeunkid = 0
            objReferee._ClientIP = getIP()
            objReferee._HostName = getHostName()
            objReferee._FromWeb = False
            objReferee._AuditUserId = User._Object._Userunkid
objReferee._CompanyUnkid = Company._Object._Companyunkid
            objReferee._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'blnFlag = objReferee.Update()

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If RefreeApprovalFlowVal Is Nothing AndAlso mintRefereeUnkid > 0 Then
                If RefreeApprovalFlowVal Is Nothing AndAlso mintRefereeUnkid > 0 AndAlso isEmployeeApprove = True Then
                    'Gajanan [22-Feb-2019] -- End

                    blnFlag = objARefreeTran.Insert(Company._Object._Companyunkid)
                    If blnFlag = False AndAlso objARefreeTran._Message <> "" Then
                        eZeeMsgBox.Show(objARefreeTran._Message, enMsgBoxStyle.Information)
                        Exit Sub
                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Else
                        If blnFlag <> False Then
                            objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                             ConfigParameter._Object._UserAccessModeSetting, _
                                                             Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                             CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences), _
                                                             enScreenName.frmEmployeeRefereeList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                             User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                             User._Object._Username, clsEmployeeDataApproval.enOperationType.EDITED, , cboEmployee.SelectedValue.ToString(), , , _
                                                             "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and name = '" & txtRefreeName.Text & "' ", Nothing, False, , _
                                                             "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and name = '" & txtRefreeName.Text & "' ", Nothing)
                        End If
                        'Gajanan [17-April-2019] -- End

                    End If
            Else
                blnFlag = objReferee.Update()
            End If
                'Gajanan [17-DEC-2018] -- End
            Else

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'blnFlag = objReferee.Insert()


                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                'If RefreeApprovalFlowVal Is Nothing AndAlso mintRefereeUnkid <= 0 Then
                If RefreeApprovalFlowVal Is Nothing AndAlso mintRefereeUnkid <= 0 AndAlso isEmployeeApprove = True Then
                    'Gajanan [22-Feb-2019] -- End
                    blnFlag = objARefreeTran.Insert(Company._Object._Companyunkid)
                    If blnFlag = False AndAlso objARefreeTran._Message <> "" Then
                        eZeeMsgBox.Show(objARefreeTran._Message, enMsgBoxStyle.Information)
                        Exit Sub
                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.


                    Else
                        If blnFlag <> False Then
                            objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                             ConfigParameter._Object._UserAccessModeSetting, _
                                                             Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                             CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences), _
                                                             enScreenName.frmEmployeeRefereeList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                             User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                             User._Object._Username, clsEmployeeDataApproval.enOperationType.ADDED, , cboEmployee.SelectedValue.ToString(), , , _
                                 "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and name = '" & txtRefreeName.Text & "' ", Nothing, False, , _
                                 "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and name = '" & txtRefreeName.Text & "' ", Nothing)

                        End If
                        'Gajanan [17-April-2019] -- End

                    End If
            Else
                blnFlag = objReferee.Insert()
            If blnFlag = False And objReferee._Message <> "" Then
                eZeeMsgBox.Show(objReferee._Message, enMsgBoxStyle.Information)
            End If
                End If
                'Gajanan [17-DEC-2018] -- End

            End If

            'If blnFlag = False And objReferee._Message <> "" Then
            '    eZeeMsgBox.Show(objReferee._Message, enMsgBoxStyle.Information)
            'End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objReferee = Nothing
                    objReferee = New clsEmployee_Refree_tran
                    Call GetValue()
                    cboEmployee.Focus()
                Else
                    mintRefereeUnkid = objReferee._Refereetranunkid
                    Me.Close()
                End If
            End If

            If mintSeletedEmpId <> -1 Then
                cboEmployee.SelectedValue = mintSeletedEmpId
            End If

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objfrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If objfrm.DisplayDialog Then
                cboEmployee.SelectedValue = objfrm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "
    Private Sub cboState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged
        Dim objCity As New clscity_master
        Dim dsList As New DataSet
        Try
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsList = objCity.GetList("City", True, True, CInt(cboState.SelectedValue))
                With cboTown
                    .ValueMember = "cityunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("City")
                    'Sandeep [ 14 Aug 2010 ] -- Start
                    '.SelectedValue = 0
                    If mintRefereeUnkid = -1 Then
                        .SelectedValue = 0
                    Else
                        .SelectedValue = objReferee._Cityunkid
                    End If
                    'Sandeep [ 14 Aug 2010 ] -- End 
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboState_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Dim objState As New clsstate_master
        Dim dsList As New DataSet
        Try
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsList = objState.GetList("State", True, True, CInt(cboCountry.SelectedValue))
                With cboState
                    .ValueMember = "stateunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("State")
                    'Sandeep [ 14 Aug 2010 ] -- Start
                    '.SelectedValue = 0
                    If mintRefereeUnkid = -1 Then
                        .SelectedValue = 0
                    Else
                        .SelectedValue = objReferee._Stateunkid
                    End If
                    'Sandeep [ 14 Aug 2010 ] -- End 
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectionChangeCommitted
        Try
            mintSeletedEmpId = CInt(cboEmployee.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.


    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Dim itmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid= '" & CInt(cboEmployee.SelectedValue) & "'")
            If itmp.Length > 0 Then
                isEmployeeApprove = CBool(itmp(0).Item("isapproved").ToString())
            Else
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'isEmployeeApprove = True
                isEmployeeApprove = False
                'Gajanan [17-April-2019] -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Gajanan [22-Feb-2019] -- End



#End Region

    'Sandeep [ 17 DEC 2010 ] -- Start
    Private Sub objbtnAddRefereeType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddRefereeType.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.RELATIONS, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation", , True)
                With cboRefereeType
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Relation")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddRefereeType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sandeep [ 17 DEC 2010 ] -- End 

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbRefereeInformation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbRefereeInformation.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveInfo.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveInfo.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbRefereeInformation.Text = Language._Object.getCaption(Me.gbRefereeInformation.Name, Me.gbRefereeInformation.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSaveInfo.Text = Language._Object.getCaption(Me.btnSaveInfo.Name, Me.btnSaveInfo.Text)
			Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
			Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
			Me.lblMobile.Text = Language._Object.getCaption(Me.lblMobile.Name, Me.lblMobile.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.lblTelNo.Text = Language._Object.getCaption(Me.lblTelNo.Name, Me.lblTelNo.Text)
			Me.lblIDNo.Text = Language._Object.getCaption(Me.lblIDNo.Name, Me.lblIDNo.Text)
			Me.lblPostTown.Text = Language._Object.getCaption(Me.lblPostTown.Name, Me.lblPostTown.Text)
			Me.lblPostCountry.Text = Language._Object.getCaption(Me.lblPostCountry.Name, Me.lblPostCountry.Text)
			Me.lblRefereeName.Text = Language._Object.getCaption(Me.lblRefereeName.Name, Me.lblRefereeName.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lnRefreeInfo.Text = Language._Object.getCaption(Me.lnRefreeInfo.Name, Me.lnRefreeInfo.Text)
			Me.lnEmployeeName.Text = Language._Object.getCaption(Me.lnEmployeeName.Name, Me.lnEmployeeName.Text)
			Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
			Me.lblRefereeType.Text = Language._Object.getCaption(Me.lblRefereeType.Name, Me.lblRefereeType.Text)
			Me.lblPosition.Text = Language._Object.getCaption(Me.lblPosition.Name, Me.lblPosition.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 2, "Referee Name cannot be blank. Referee Name is compulsory information.")
			Language.setMessage(mstrModuleName, 3, "Invalid Email.Please Enter Valid Email")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class