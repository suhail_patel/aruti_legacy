﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeRehireList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmEmployeeRehireList"
    Private objEReHire As clsemployee_rehire_tran
    'S.SANDEEP [07-Feb-2018] -- START
    'ISSUE/ENHANCEMENT : Preventing Deletion for old year Data
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    'S.SANDEEP [07-Feb-2018] -- END

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    Private mintEmployeeID As Integer = -1
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombos As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objEmployee.GetEmployeeList("Emp", True, True)


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB


            'dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                        User._Object._Userunkid, _
            '                                        FinancialYear._Object._YearUnkid, _
            '                                        Company._Object._Companyunkid, _
            '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                        ConfigParameter._Object._UserAccessModeSetting, _
            '                                        True, True, "Emp", True)

            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                  True, True, "Emp", True, , , , , , , , , , , , , , , mstrAdvanceFilter, True)
            'Gajanan [11-Dec-2019] -- End

            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
                .Text = ""
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmployee = Nothing : dsCombos.Dispose()
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsData As New DataSet
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

            'S.SANDEEP [07-Feb-2018] -- START
            'ISSUE/ENHANCEMENT : Preventing Deletion for old year Data
            dsData = objEReHire.GetList("List", FinancialYear._Object._Database_Start_Date, CInt(cboEmployee.SelectedValue))
            'S.SANDEEP [07-Feb-2018] -- End

            dgvHistory.AutoGenerateColumns = False

            dgcolhChangeDate.DataPropertyName = "EffDate"
            dgcolhRehireDate.DataPropertyName = "rdate"
            dgcolhReason.DataPropertyName = "CReason"
            objdgcolhRehiretranunkid.DataPropertyName = "rehiretranunkid"

            'S.SANDEEP [07-Feb-2018] -- START
            'ISSUE/ENHANCEMENT : Preventing Deletion for old year Data
            objdgcolhAllowOpr.DataPropertyName = "allowopr"
            'S.SANDEEP [07-Feb-2018] -- END

            dgvHistory.DataSource = dsData.Tables(0)


            objdgcolhEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEdit.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhDelete.DefaultCellStyle.SelectionForeColor = Color.White

            

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisibility()
        Try
            btnAddNew.Enabled = User._Object.Privilege._AllowToRehireEmployee
            'S.SANDEEP [21-Mar-2018] -- START
            'ISSUE/ENHANCEMENT : {#ARUTI-57}
            'objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditRehireEmployeeDetails
            'S.SANDEEP [21-Mar-2018] -- END
            objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteRehireEmployeeDetails

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- End
#End Region

#Region "Property"
    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeID = value
        End Set
    End Property
#End Region

#Region " Form's Events "

    Private Sub frmEmployeeRehireList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEReHire = New clsemployee_rehire_tran
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If mintEmployeeID > 0 Then
                cboEmployee.SelectedValue = mintEmployeeID
            End If
            'Gajanan [11-Dec-2019] -- End

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeRehireList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_rehire_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_rehire_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Dim frm As New frmEmployeeRehire
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call Fill_Grid()
            End If

            'Shani (08-Dec-2016) -- Start
            'Enhancement -  Add Employee Allocaion/Date Privilage
            btnAddNew.Enabled = User._Object.Privilege._AllowtoSetEmpReinstatementDate
            'Shani (08-Dec-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If cboEmployee.DataSource Is Nothing Then Exit Sub

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                If .DisplayDialog = True Then
                    cboEmployee.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
            FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region " Combobox Event(s) "

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Call Fill_Grid()
            Else
                dgvHistory.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvHistory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHistory.CellClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case objdgcolhEdit.Index
                    Dim frm As New frmEmployeeRehire

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    If frm.displayDialog(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhRehiretranunkid.Index).Value), _
                                         enAction.EDIT_ONE, _
                                         CInt(cboEmployee.SelectedValue)) Then
                        Call Fill_Grid()
                    End If

                    If frm IsNot Nothing Then frm.Dispose()
                Case objdgcolhDelete.Index
                    'S.SANDEEP [07-Feb-2018] -- START
                    'ISSUE/ENHANCEMENT : Preventing Deletion for old year Data
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub
                    'S.SANDEEP [07-Feb-2018] -- End

                    'S.SANDEEP [21-Mar-2018] -- START
                    'ISSUE/ENHANCEMENT : {#ARUTI-57}
                    objEReHire._Rehiretranunkid = CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhRehiretranunkid.Index).Value)
                    Dim strmsg As String = String.Empty
                    strmsg = objEReHire.Allow_Void_Rehire(objEReHire._Employeeunkid, objEReHire._Reinstatment_Date)
                    If strmsg.Trim.Length > 0 Then
                        eZeeMsgBox.Show(strmsg, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'S.SANDEEP [21-Mar-2018] -- END

                    Dim xStrVoidReason As String = String.Empty
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.displayDialog(enVoidCategoryType.EMPLOYEE, xStrVoidReason)
                    If xStrVoidReason.Trim.Length <= 0 Then Exit Sub
                    objEReHire._Isvoid = True
                    objEReHire._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objEReHire._Voidreason = xStrVoidReason
                    objEReHire._Voiduserunkid = User._Object._Userunkid
                    objEReHire._Userunkid = User._Object._Userunkid
                    'S.SANDEEP [01 JUN 2016] -- START
                    'If objEReHire.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhRehiretranunkid.Index).Value)) = False Then

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objEReHire._FormName = mstrModuleName
                    objEReHire._LoginEmployeeunkid = 0
                    objEReHire._ClientIP = getIP()
                    objEReHire._HostName = getHostName()
                    objEReHire._FromWeb = False
                    objEReHire._AuditUserId = User._Object._Userunkid
objEReHire._CompanyUnkid = Company._Object._Companyunkid
                    objEReHire._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                    'If objEReHire.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhRehiretranunkid.Index).Value), _
                    '                     FinancialYear._Object._DatabaseName, _
                    '                     FinancialYear._Object._YearUnkid, _
                    '                     Company._Object._Companyunkid, _
                    '                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                     ConfigParameter._Object._UserAccessModeSetting, True, _
                    '                     ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, _
                    '                     ConfigParameter._Object._CurrentDateAndTime) = False Then

                    If objEReHire.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhRehiretranunkid.Index).Value), _
                                         FinancialYear._Object._DatabaseName, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, True, _
                                         ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, _
                                        ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst) = False Then

                        'Pinkal (18-Aug-2018) -- End

                        'S.SANDEEP [01 JUN 2016] -- END
                        If objEReHire._Message <> "" Then
                            eZeeMsgBox.Show(objEReHire._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If
                    Call Fill_Grid()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_CellClick", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [07-Feb-2018] -- START
    'ISSUE/ENHANCEMENT : Preventing Deletion for old year Data
    Private Sub dgvHistory_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvHistory.DataBindingComplete
        For Each xdgvr As DataGridViewRow In dgvHistory.Rows
            If CBool(xdgvr.Cells(objdgcolhAllowOpr.Index).Value) = False Then
                xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
            End If
        Next
    End Sub
    'S.SANDEEP [07-Feb-2018] -- End
    

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbEmployeeRehireList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeRehireList.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddNew.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbEmployeeRehireList.Text = Language._Object.getCaption(Me.gbEmployeeRehireList.Name, Me.gbEmployeeRehireList.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnAddNew.Text = Language._Object.getCaption(Me.btnAddNew.Name, Me.btnAddNew.Text)
            Me.dgcolhChangeDate.HeaderText = Language._Object.getCaption(Me.dgcolhChangeDate.Name, Me.dgcolhChangeDate.HeaderText)
            Me.dgcolhRehireDate.HeaderText = Language._Object.getCaption(Me.dgcolhRehireDate.Name, Me.dgcolhRehireDate.HeaderText)
            Me.dgcolhReason.HeaderText = Language._Object.getCaption(Me.dgcolhReason.Name, Me.dgcolhReason.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class