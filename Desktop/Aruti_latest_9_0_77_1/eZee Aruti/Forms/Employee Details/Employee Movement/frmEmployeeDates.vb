﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System

#End Region

Public Class frmEmployeeDates

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmEmployeeDates"
    Private objEDates As clsemployee_dates_tran
    Private mAction As enAction = enAction.ADD_CONTINUE
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    Private mstrEmployeeCode As String = String.Empty
    Private xOldDates As New DataSet
    Private mintDatesTypeId As Integer = 0
    Private xMasterType As Integer = 0
    Private mdtReinstatementDate As Date = Nothing
    Private mdtAppointmentDate As Date = Nothing
    Private xCurrentDates As Dictionary(Of Integer, String)

    'S.SANDEEP [20-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}
    Private objADate As clsDates_Approval_Tran
    'S.SANDEEP [20-JUN-2018] -- END

    'S.SANDEEP [09-AUG-2018] -- START
    Private imgInfo As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.information)
    'S.SANDEEP [09-AUG-2018] -- END

    'S.SANDEEP [15-NOV-2018] -- START
    Private mintTransactionId As Integer = 0
    'S.SANDEEP [15-NOV-2018] -- END

    'S.SANDEEP |17-JAN-2019| -- START
    Private imgView As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.doc_view)
    'S.SANDEEP |17-JAN-2019| -- END

    'S.SANDEEP |10-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {#0003807}
    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    'S.SANDEEP |10-MAY-2019| -- END

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    'Gajanan [11-Dec-2019] -- End

    Private mintDisciplineunkid As Integer = -1

#End Region

#Region " Properties "

    Public WriteOnly Property _DateTypeId() As Integer
        Set(ByVal value As Integer)
            mintDatesTypeId = value
        End Set
    End Property


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objCMaster As New clsCommon_Master
        Dim objELReason As New clsAction_Reason
        Dim dsCombos As New DataSet
        Try



            'Pinkal (09-Apr-2015) -- Start
            'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, False)
            'Else
            '    If ConfigParameter._Object._IsIncludeInactiveEmp Then
            '        dsCombos = objEmployee.GetEmployeeList("Emp", True, True)
            '    Else
            '        dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            '    End If
            'End If


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            'Dim blnIncludeInactiveEmployeeFlag As Boolean = False
            'If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
            '    'S.SANDEEP [02-NOV-2017] -- START
            '    'blnIncludeInactiveEmployeeFlag = True
            '    'S.SANDEEP [02-NOV-2017] -- END
            'Else
            '    blnIncludeInactiveEmployeeFlag = ConfigParameter._Object._IsIncludeInactiveEmp
            'End If
            'dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                                True, blnIncludeInactiveEmployeeFlag, "Emp", True, , , , , , , , , , , , , , , , True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            ''S.SANDEEP [04 JUN 2015] -- END

            ''Pinkal (09-Apr-2015) -- End

            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombos.Tables("Emp")
            '    .SelectedValue = 0
            '    .Text = ""
            'End With
            FillEmployeeCombo()

            'Gajanan [11-Dec-2019] -- End


            dsCombos = objCMaster.getComboList(CType(xMasterType, clsCommon_Master.enCommonMaster), True, "List")
            With cboChangeReason
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose() : objEmployee = Nothing : objCMaster = Nothing
        End Try
    End Sub

    Private Sub Set_Form_Controls()
        Try
            'chkExclude.Checked = False : chkExclude.Enabled = False
            btnOperations.Visible = False
            Select Case mintDatesTypeId
                Case enEmp_Dates_Transaction.DT_PROBATION
                    objgbDatesInformation.Text = Language.getMessage(mstrModuleName, 100, "Probation Date Information")
                    xMasterType = clsCommon_Master.enCommonMaster.PROBATION
                    objlblStartFrom.Text = Language.getMessage(mstrModuleName, 101, "From Date")
                    objlblEndTo.Text = Language.getMessage(mstrModuleName, 102, "To Date")

                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    objgbDatesInformation.Text = Language.getMessage(mstrModuleName, 103, "Suspension Date Information")
                    xMasterType = clsCommon_Master.enCommonMaster.SUSPENSION
                    objlblStartFrom.Text = Language.getMessage(mstrModuleName, 101, "From Date")
                    objlblEndTo.Text = Language.getMessage(mstrModuleName, 102, "To Date")

                Case enEmp_Dates_Transaction.DT_TERMINATION
                    objgbDatesInformation.Text = Language.getMessage(mstrModuleName, 104, "Termination Date Information")
                    objlblStartFrom.Text = Language.getMessage(mstrModuleName, 105, "EOC Date")
                    objlblEndTo.Text = Language.getMessage(mstrModuleName, 106, "Leaving Date")
                    xMasterType = clsCommon_Master.enCommonMaster.TERMINATION

                    'Hemant (22 June 2019) -- Start
                    'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
                    'chkExclude.Checked = True
                    'Hemant (22 June 2019) -- End
                    'chkExclude.Enabled = True


                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    objgbDatesInformation.Text = Language.getMessage(mstrModuleName, 107, "Retirement Date Information")
                    objlblStartFrom.Text = Language.getMessage(mstrModuleName, 108, "Retirement Date")
                    'dtpEndDate.Enabled = False
                    objlblEndTo.Text = ""
                    xMasterType = clsCommon_Master.enCommonMaster.RETIREMENTS
                    objdgcolhEndDate.Visible = False
                    RemoveHandler dtpStartDate.ValueChanged, AddressOf dtpStartDate_ValueChanged

                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    btnOperations.Visible = True
                    objgbDatesInformation.Text = Language.getMessage(mstrModuleName, 109, "Confirmation Date Information")
                    objlblStartFrom.Text = Language.getMessage(mstrModuleName, 110, "Confirmation Date")
                    'dtpEndDate.Enabled = False
                    objlblEndTo.Text = ""
                    xMasterType = clsCommon_Master.enCommonMaster.CONFIRMATION
                    objdgcolhEndDate.Visible = False
                    RemoveHandler dtpStartDate.ValueChanged, AddressOf dtpStartDate_ValueChanged

                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    objgbDatesInformation.Text = Language.getMessage(mstrModuleName, 13, "Exemption Date Information")
                    xMasterType = clsCommon_Master.enCommonMaster.EXEMPTION
                    objlblStartFrom.Text = Language.getMessage(mstrModuleName, 101, "From Date")
                    objlblEndTo.Text = Language.getMessage(mstrModuleName, 102, "To Date")
                    'Sohail (21 Oct 2019) -- End

            End Select

            objdgcolhStartDate.HeaderText = objlblStartFrom.Text
            objdgcolhEndDate.HeaderText = objlblEndTo.Text

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Form_Controls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsData As New DataSet
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

            'S.SANDEEP [02-NOV-2017] -- START
            If CInt(cboEmployee.SelectedValue) <= 0 Then Exit Sub
            'S.SANDEEP [02-NOV-2017] -- END

            'S.SANDEEP [16 Jan 2016] -- START
            'dsData = objEDates.GetList("List", CType(mintDatesTypeId, enEmp_Dates_Transaction), CInt(cboEmployee.SelectedValue))
            dsData = objEDates.GetList("List", CType(mintDatesTypeId, enEmp_Dates_Transaction), _
                                       FinancialYear._Object._Database_Start_Date.Date, _
                                       CInt(cboEmployee.SelectedValue))
            'S.SANDEEP [16 Jan 2016] -- END

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "tranguid"
                .DefaultValue = ""
            End With
            dsData.Tables(0).Columns.Add(dcol)

            'S.SANDEEP |17-JAN-2019| -- START
            dcol = New DataColumn
            With dcol
                .DataType = GetType(System.Int32)
                .ColumnName = "operationtypeid"
                .DefaultValue = clsEmployeeMovmentApproval.enOperationType.ADDED
            End With
            dsData.Tables(0).Columns.Add(dcol)

            dcol = New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "OperationType"
                .DefaultValue = ""
            End With
            dsData.Tables(0).Columns.Add(dcol)
            'S.SANDEEP |17-JAN-2019| -- END

            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                Dim dsPending As New DataSet
                dsPending = objADate.GetList("List", CType(mintDatesTypeId, enEmp_Dates_Transaction), _
                                       FinancialYear._Object._Database_Start_Date.Date, _
                                       CInt(cboEmployee.SelectedValue))
                If dsPending.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In dsPending.Tables(0).Rows
                        dsData.Tables(0).ImportRow(row)
                    Next
                End If
            End If
            'S.SANDEEP [20-JUN-2018] -- END

            dgvHistory.AutoGenerateColumns = False

            dgcolhChangeDate.DataPropertyName = "EffDate"
            objdgcolhStartDate.DataPropertyName = "dDate1"
            objdgcolhEndDate.DataPropertyName = "dDate2"
            dgcolhReason.DataPropertyName = "CReason"
            objdgcolhdatetranunkid.DataPropertyName = "datestranunkid"
            objdgcolhFromEmp.DataPropertyName = "isfromemployee"
            objdgcolhActionReasonId.DataPropertyName = "actionreasonunkid"
            objdgcolhExclude.DataPropertyName = "isexclude_payroll"
            objdgcolhAppointdate.DataPropertyName = "adate"
            'S.SANDEEP [14 APR 2015] -- START
            objdgcolhrehiretranunkid.DataPropertyName = "rehiretranunkid"
            'S.SANDEEP [14 APR 2015] -- END

            'S.SANDEEP [16 Jan 2016] -- START
            objdgcolhallowopr.DataPropertyName = "allowopr"
            'S.SANDEEP [16 Jan 2016] -- END
            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            objdgcolhtranguid.DataPropertyName = "tranguid"
            'S.SANDEEP [20-JUN-2018] -- END

            'S.SANDEEP |17-JAN-2019| -- START
            objdgcolhOperationTypeId.DataPropertyName = "operationtypeid"
            objdgcolhOperationType.DataPropertyName = "OperationType"
            'S.SANDEEP |17-JAN-2019| -- END
            dgvHistory.DataSource = dsData.Tables(0)

            objdgcolhEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEdit.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhDelete.DefaultCellStyle.SelectionForeColor = Color.White


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_Dates()
        Dim objCommon As New clsCommon_Master

        Try
            Dim dsDate As New DataSet
            dsDate = objEDates.Get_Current_Dates(Now.Date, mintDatesTypeId, CInt(cboEmployee.SelectedValue))
            If dsDate.Tables(0).Rows.Count > 0 Then
                'S.SANDEEP [17-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Error When Date1 Is Null}
                'dtpStartDate.Value = CDate(dsDate.Tables(0).Rows(0).Item("date1"))
                If IsDBNull(dsDate.Tables(0).Rows(0).Item("date1")) = False Then
                dtpStartDate.Value = CDate(dsDate.Tables(0).Rows(0).Item("date1"))
                End If
                'S.SANDEEP [17-May-2018] -- END
                If IsDBNull(dsDate.Tables(0).Rows(0).Item("date2")) = False Then
                    dtpEndDate.Value = CDate(dsDate.Tables(0).Rows(0).Item("date2"))
                End If
            End If


            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            mintDisciplineunkid = -1


            'Gajanan [02-June-2020] -- Start
            'Enhancement Configure Option That Allow Period Closer Before XX Days.
            'dsDate = objCommon.getDesciplineRefNoComboList(mintEmployeeUnkid, "refnoList")
            Dim objDiscFileMaster As New clsDiscipline_file_master
            dsDate = objDiscFileMaster.GetList(FinancialYear._Object._DatabaseName, _
                                    User._Object._Userunkid, _
                                    FinancialYear._Object._YearUnkid, _
                                    Company._Object._Companyunkid, _
                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                    ConfigParameter._Object._IsIncludeInactiveEmp, "refnoList", , _
                                    "  hrdiscipline_file_master.involved_employeeunkid = '" & mintEmployeeUnkid & "'")
            'Gajanan [02-June-2020] -- End


            If IsNothing(dsDate.Tables("refnoList")) = False AndAlso dsDate.Tables("refnoList").Rows.Count > 0 Then
                lnkSetSuspension.Enabled = True
            Else
                lnkSetSuspension.Enabled = False
            End If
            'Gajanan [18-May-2020] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Dates", mstrModuleName)
        Finally
            objCommon = Nothing
        End Try
    End Sub

    Private Function Valid_Dates() As Boolean
        Try
            If dtpEffectiveDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue."), enMsgBoxStyle.Information)
                dtpEffectiveDate.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            Select Case mintDatesTypeId
                Case enEmp_Dates_Transaction.DT_PROBATION
                    If dtpStartDate.Checked = False AndAlso dtpEndDate.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Probation start date and end date are mandatory information. Please provide start date and end date."), enMsgBoxStyle.Information)
                        dtpStartDate.Focus()
                        Return False
                    End If
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    If dtpStartDate.Checked = False AndAlso dtpEndDate.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Suspension start date and end date are mandatory information. Please provide start date and end date."), enMsgBoxStyle.Information)
                        dtpStartDate.Focus()
                        Return False
                    End If

                    'Pinkal (09-Apr-2015) -- Start
                    'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA

                    'Case enEmp_Dates_Transaction.DT_TERMINATION
                    '    If dtpStartDate.Checked = False AndAlso dtpEndDate.Checked = False Then
                    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, EOC date and Leaving date are mandatory information. Please provide eoc date and leaving date."), enMsgBoxStyle.Information)
                    '        dtpStartDate.Focus()
                    '        Return False
                    '    End If

                    'Pinkal (09-Apr-2015) -- End

                    'S.SANDEEP [12 MAY 2015] -- START

                    'S.SANDEEP |02-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
                    If dtpEndDate.Checked = True AndAlso dtpStartDate.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 166, "Sorry, suspension from date is mandatory information when suspended to date is ticked."), enMsgBoxStyle.Information)
                        dtpStartDate.Focus()
                        Return False
                    End If
                    'S.SANDEEP |02-MAR-2020| -- END

                Case enEmp_Dates_Transaction.DT_TERMINATION
                    Dim objEmployee As New clsEmployee_Master
                    If dtpStartDate.Checked = True Then
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objEmployee.IsEmpTerminated(dtpStartDate.Value.Date, CInt(cboEmployee.SelectedValue)) = False Then

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                        'Sohail (09 Oct 2019) -- Start
                        'NMB Enhancement # : allow to change eoc / retirement date in current open or last closed period even after salary is paid or period is closed.
                        'If objEmployee.IsEmpTerminated(dtpStartDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                        If objEmployee.IsEmpTerminated(dtpStartDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, blnSkipForLastClosedPeriod:=User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod) = False Then
                            'Sohail (09 Oct 2019) -- End
                            'If objEmployee.IsEmpTerminated(dtpStartDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName) = False Then
                            'S.SANDEEP [04 JUN 2015] -- END

                            'Sohail (21 Aug 2015) -- End
                            eZeeMsgBox.Show(objEmployee._Message, enMsgBoxStyle.Information)
                            Exit Function
                        End If
                    End If

                    If dtpEndDate.Checked = True Then
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'If objEmployee.IsEmpTerminated(dtpEndDate.Value.Date, CInt(cboEmployee.SelectedValue)) = False Then

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'Sohail (09 Oct 2019) -- Start
                        'NMB Enhancement # : allow to change eoc / retirement date in current open or last closed period even after salary is paid or period is closed.
                        'If objEmployee.IsEmpTerminated(dtpEndDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                        If objEmployee.IsEmpTerminated(dtpEndDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, blnSkipForLastClosedPeriod:=User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod) = False Then
                            'Sohail (09 Oct 2019) -- End
                            'If objEmployee.IsEmpTerminated(dtpEndDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName) = False Then
                            'S.SANDEEP [04 JUN 2015] -- END

                            'Sohail (21 Aug 2015) -- End
                            eZeeMsgBox.Show(objEmployee._Message, enMsgBoxStyle.Information)
                            Exit Function
                        End If
                    End If
                    objEmployee = Nothing
                    'S.SANDEEP [12 MAY 2015] -- END


                    'S.SANDEEP [22-MAR-2017] -- START
                    'ISSUE/ENHANCEMENT : Tanapa PE Forms Statutory Report
                    Dim objTnALeaveTran As New clsTnALeaveTran
                    Dim objMData As New clsMasterData : Dim iPeriod As Integer = 0
                    iPeriod = objMData.getCurrentPeriodID(enModuleReference.Payroll, dtpStartDate.Value.Date, FinancialYear._Object._YearUnkid, enStatusType.Open, True)
                    If iPeriod <= 0 Then
                        objMData.getCurrentPeriodID(enModuleReference.Payroll, dtpEndDate.Value.Date, FinancialYear._Object._YearUnkid, enStatusType.Open, True)
                    End If
                    'Sohail (09 Oct 2019) -- Start
                    'NMB Enhancement # : privilege  Allow to change date on Closed Period on user master to change eoc / termination date in paid period or closed period.
                    'If iPeriod > 0 Then
                    'Sohail (13 Jan 2022) -- Start
                    'Enhancement : OLD-216 : Delink the setting "Allow to Terminate Employee If Payment is Done" with privilege "Allow to change EOC on closed period" and allow the setting to terminate employee if payment is done even for users who don’t have that privilege.
                    'If iPeriod > 0 AndAlso User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod = True Then
                    'Sohail (21 Jan 2022) -- Start
                    'Enhancement : OLD-216 : Delink the setting "Allow to Terminate Employee If Payment is Done" with privilege "Allow to change EOC on closed period" and allow the setting to terminate employee if payment is done even for users who don’t have that privilege.
                    'If iPeriod > 0 AndAlso (User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod = True OrElse ConfigParameter._Object._AllowTerminationIfPaymentDone = True) Then
                    If iPeriod > 0 Then
                        'Sohail (21 Jan 2022) -- End
                        'Sohail (13 Jan 2022) -- End
                        'Sohail (09 Oct 2019) -- End
                        Dim objPrd As New clscommom_period_Tran
                        objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriod

                        'S.SANDEEP |27-JUN-2020| -- START
                        'ISSUE/ENHANCEMENT : NMB ENHANCEMENT
                        'If objTnALeaveTran.IsPayrollProcessDone(iPeriod, CStr(cboEmployee.SelectedValue), objPrd._End_Date, enModuleReference.Payroll) Then
                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 113, "Sorry, you cannot exclude selected employee. Reason:Payroll Process already done for the employee for last date of current open period."), enMsgBoxStyle.Information)
                        '    Return False
                        'End If
                        If ConfigParameter._Object._AllowTerminationIfPaymentDone = False Then
                        If objTnALeaveTran.IsPayrollProcessDone(iPeriod, CStr(cboEmployee.SelectedValue), objPrd._End_Date, enModuleReference.Payroll) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 113, "Sorry, you cannot exclude selected employee. Reason:Payroll Process already done for the employee for last date of current open period."), enMsgBoxStyle.Information)
                            Return False
                        End If
                    End If
                        'S.SANDEEP |27-JUN-2020| -- END


                    End If
                    objTnALeaveTran = Nothing
                    'S.SANDEEP [22-MAR-2017] -- END

                    'S.SANDEEP [28-Feb-2018] -- START
                    'ISSUE/ENHANCEMENT : {#0002032}
                    If objEDates.IsPendingSalaryIncrement(CInt(cboEmployee.SelectedValue), dtpStartDate.Value.Date) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Sorry, You cannot terminate this employee. Reason there are pending salary increment information present."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    'S.SANDEEP [28-Feb-2018] -- END

                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    If dtpStartDate.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Retirement date is mandatory information. Please provide retirement date."), enMsgBoxStyle.Information)
                        dtpStartDate.Focus()
                        Return False
                    End If

                    'S.SANDEEP [12 MAY 2015] -- START
                    Dim objEmployee As New clsEmployee_Master
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objEmployee.IsEmpTerminated(dtpStartDate.Value.Date, CInt(cboEmployee.SelectedValue), True) = False Then

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    If objEmployee.IsEmpTerminated(dtpStartDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, True) = False Then
                        'If objEmployee.IsEmpTerminated(dtpStartDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, True) = False Then
                        'S.SANDEEP [04 JUN 2015] -- END

                        'Sohail (21 Aug 2015) -- End
                        eZeeMsgBox.Show(objEmployee._Message, enMsgBoxStyle.Information)
                        Exit Function
                    End If
                    objEmployee = Nothing
                    'S.SANDEEP [12 MAY 2015] -- END

                    'S.SANDEEP [28-Feb-2018] -- START
                    'ISSUE/ENHANCEMENT : {#0002032}
                    If objEDates.IsPendingSalaryIncrement(CInt(cboEmployee.SelectedValue), dtpStartDate.Value.Date) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Sorry, You cannot retire this employee. Reason there are pending salary increment information present."), enMsgBoxStyle.Information)
                        Return False
                    End If
                    'S.SANDEEP [28-Feb-2018] -- END

                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    If dtpStartDate.Checked = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Exemption date is mandatory information. Please provide Exemption date."), enMsgBoxStyle.Information)
                        dtpStartDate.Focus()
                        Return False
                    End If
                    'Sohail (21 Oct 2019) -- End

            End Select

            'S.SANDEEP [04 APR 2015] -- START
            If dtpEffectiveDate.Value.Date > dtpStartDate.Value.Date Then
                eZeeMsgBox.Show(objlblStartFrom.Text & " " & Language.getMessage(mstrModuleName, 10, "cannot be less then effective date."), enMsgBoxStyle.Information)
                dtpStartDate.Focus()
                Return False
            End If
            'S.SANDEEP [04 APR 2015] -- END



            'Pinkal (09-Apr-2015) -- Start
            'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            'If dtpEndDate.Visible = True Then
            If dtpEndDate.Visible = True AndAlso dtpEndDate.Checked = True Then
                'Sohail (21 Oct 2019) -- End
                If dtpEndDate.Value.Date < dtpStartDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, end date cannot be less then start date."), enMsgBoxStyle.Information)
                    dtpEndDate.Focus()
                    Return False
                End If
            End If
            'Pinkal (09-Apr-2015) -- End

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            'If CInt(cboChangeReason.SelectedValue) <= 0 Then

            If CInt(cboChangeReason.SelectedValue) <= 0 AndAlso cboChangeReason.Enabled = True Then
                'Sohail (21 Oct 2019) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Change Reason is mandatory information. Please select Change Reason to continue."), enMsgBoxStyle.Information)
                cboChangeReason.Focus()
                Return False
            End If

            If mdtAppointmentDate <> Nothing Then
                If mdtAppointmentDate <> dtpEffectiveDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, This is system generate entry. Please set effective date as employee appointment date."), enMsgBoxStyle.Information)
                    dtpEffectiveDate.Focus()
                    Return False
                End If
            End If

            'TODO <CHECK APPOINT DATE FOR RETIRE AND LEAVING>


            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                Dim objPMovement As New clsEmployeeMovmentApproval

                Dim intPrivilegeId As Integer = 0
                Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                Select Case mintDatesTypeId
                    Case enEmp_Dates_Transaction.DT_PROBATION
                        intPrivilegeId = 1194 : eMovement = clsEmployeeMovmentApproval.enMovementType.PROBATION
                    Case enEmp_Dates_Transaction.DT_RETIREMENT
                        intPrivilegeId = 1198 : eMovement = clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                    Case enEmp_Dates_Transaction.DT_SUSPENSION
                        intPrivilegeId = 1196 : eMovement = clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                    Case enEmp_Dates_Transaction.DT_TERMINATION
                        intPrivilegeId = 1197 : eMovement = clsEmployeeMovmentApproval.enMovementType.TERMINATION
                    Case enEmp_Dates_Transaction.DT_CONFIRMATION
                        intPrivilegeId = 1195 : eMovement = clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                    Case enEmp_Dates_Transaction.DT_EXEMPTION
                        intPrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeExemption : eMovement = clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                        'Sohail (21 Oct 2019) -- End
                End Select

                Dim strMsg As String = String.Empty
                strMsg = objPMovement.IsOtherMovmentApproverPresent(eMovement, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CType(mintDatesTypeId, enEmp_Dates_Transaction), CInt(cboEmployee.SelectedValue))
                If strMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    objPMovement = Nothing
                    Return False
                End If
                objPMovement = Nothing

                'S.SANDEEP |16-JAN-2019| -- START
                If objEDates.isExist(dtpEffectiveDate.Value.Date, Nothing, Nothing, mintDatesTypeId, CInt(cboEmployee.SelectedValue), mintTransactionId) Then
                    Select Case mintDatesTypeId
                        Case enEmp_Dates_Transaction.DT_PROBATION
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 1, "Sorry, porbation information is already present for the selected effective date."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_CONFIRMATION
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 16, "Sorry, confirmation information is already present for the selected effective date."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_SUSPENSION
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 2, "Sorry, suspension information is already present for the selected effective date."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_TERMINATION
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 3, "Sorry, termination information is already present for the selected effective date."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_REHIRE
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 17, "Sorry, re-hire information is already present for the selected effective date."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_RETIREMENT
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 4, "Sorry, retirement information is already present for the selected effective date."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 19, "Sorry, appointment information is already present for the selected effective date."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 20, "Sorry, birthdate information is already present for the selected effective date."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 21, "Sorry, first appointment date information is already present for the selected effective date."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 22, "Sorry, anniversary information is already present for the selected effective date."), enMsgBoxStyle.Information)
                            'Sohail (21 Oct 2019) -- Start
                            'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                        Case enEmp_Dates_Transaction.DT_EXEMPTION
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 27, "Sorry, exemption information is already present for the selected effective date."), enMsgBoxStyle.Information)
                            'Sohail (21 Oct 2019) -- End

                    End Select
                    Return False
                End If

                If objEDates.isExist(Nothing, dtpStartDate.Value.Date, dtpEndDate.Value.Date, mintDatesTypeId, CInt(cboEmployee.SelectedValue), mintTransactionId) Then
                    Select Case mintDatesTypeId
                        Case enEmp_Dates_Transaction.DT_PROBATION
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 5, "Sorry, This probation date range is already defined. Please define new date range for this probation."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_CONFIRMATION
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 15, "Sorry, This confirmation information is already present."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_SUSPENSION
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 6, "Sorry, This suspension date range is already defined. Please define new date range for this suspension."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_TERMINATION
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 7, "Sorry, This termination information is already present."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_REHIRE
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 18, "Sorry, This re-hire information is already present."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_RETIREMENT
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 8, "Sorry, This retirement information is already present."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 23, "Sorry, This appointment date information is already present."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 24, "Sorry, This birthdate date information is already present."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 25, "Sorry, This first appointment date information is already present."), enMsgBoxStyle.Information)
                        Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 26, "Sorry, This anniversary information is already present."), enMsgBoxStyle.Information)
                            'Sohail (21 Oct 2019) -- Start
                            'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                        Case enEmp_Dates_Transaction.DT_EXEMPTION
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 28, "Sorry, This exemption information is already present."), enMsgBoxStyle.Information)
                            'Sohail (21 Oct 2019) -- End
                    End Select
                    Return False
                End If
                'S.SANDEEP |16-JAN-2019| -- END

            End If
            'S.SANDEEP [20-JUN-2018] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Valid_Dates", mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function

    Private Sub SetValue()
        Try
            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'objEDates._Effectivedate = dtpEffectiveDate.Value
            'objEDates._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'objEDates._Date1 = dtpStartDate.Value

            ''Pinkal (09-Apr-2015) -- Start
            ''Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA
            'If dtpEndDate.Visible = True Then
            '    objEDates._Date2 = dtpEndDate.Value
            'Else
            '    objEDates._Date2 = Nothing
            'End If
            ''Pinkal (09-Apr-2015) -- End
            'If mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION Then
            '    objEDates._Isconfirmed = True
            'Else
            '    objEDates._Isconfirmed = False
            'End If
            'objEDates._Datetypeunkid = mintDatesTypeId
            'objEDates._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            'objEDates._Isvoid = False
            'objEDates._Statusunkid = 0
            'objEDates._Userunkid = User._Object._Userunkid
            'objEDates._Voiddatetime = Nothing
            'objEDates._Voidreason = ""
            'objEDates._Voiduserunkid = -1
            ''S.SANDEEP [04 APR 2015] -- START
            'If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
            '    objEDates._Actionreasonunkid = CInt(cboChangeReason.SelectedValue)
            '    objEDates._Isexclude_payroll = chkExclude.Checked
            'End If
            ''S.SANDEEP [04 APR 2015] -- END

            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
            objEDates._Effectivedate = dtpEffectiveDate.Value
            objEDates._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEDates._Date1 = dtpStartDate.Value

            'Pinkal (09-Apr-2015) -- Start
            'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                'If dtpEndDate.Visible = True Then
                If dtpEndDate.Visible = True AndAlso dtpEndDate.Checked = True Then
                    'Sohail (21 Oct 2019) -- End
                objEDates._Date2 = dtpEndDate.Value
            Else
                objEDates._Date2 = Nothing
            End If
            'Pinkal (09-Apr-2015) -- End
            If mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION Then
                objEDates._Isconfirmed = True
            Else
                objEDates._Isconfirmed = False
            End If
            objEDates._Datetypeunkid = mintDatesTypeId
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                'objEDates._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                If cboChangeReason.Enabled = True Then
            objEDates._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                    objEDates._Leaveissueunkid = 0
                Else
                    objEDates._Changereasonunkid = CInt(cboChangeReason.Tag)
                    objEDates._Leaveissueunkid = CInt(lblReason.Tag)
                End If
                'Sohail (21 Oct 2019) -- End
            objEDates._Isvoid = False
            objEDates._Statusunkid = 0
            objEDates._Userunkid = User._Object._Userunkid
            objEDates._Voiddatetime = Nothing
            objEDates._Voidreason = ""
            objEDates._Voiduserunkid = -1
            'S.SANDEEP [04 APR 2015] -- START
            If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                objEDates._Actionreasonunkid = CInt(cboChangeReason.SelectedValue)
                objEDates._Isexclude_payroll = chkExclude.Checked
            End If
            'S.SANDEEP [04 APR 2015] -- END


                'Pinkal (07-Mar-2020) -- Start
                'Enhancement - Changes Related to Payroll UAT for NMB.
                If dtpActualDate.Checked Then
                    objEDates._ActualDate = dtpActualDate.Value
                Else
                    objEDates._ActualDate = Nothing
                End If
                'Pinkal (07-Mar-2020) -- End


                'Gajanan [18-May-2020] -- Start
                'Enhancement:Discipline Module Enhancement NMB
                objEDates._Disciplinefileunkid = mintDisciplineunkid
                'Gajanan [18-May-2020] -- End

            Else
                'Gajanan [11-Dec-2019] -- Start   
                'Enhancement:Worked On December Cut Over Enhancement For NMB
                objADate = New clsDates_Approval_Tran
                'Gajanan [11-Dec-2019] -- End
                objADate._Audittype = enAuditType.ADD
                objADate._Audituserunkid = User._Object._Userunkid
                objADate._Effectivedate = dtpEffectiveDate.Value
                objADate._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objADate._Date1 = dtpStartDate.Value

                'Pinkal (09-Apr-2015) -- Start
                'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                'If dtpEndDate.Visible = True Then
                If dtpEndDate.Visible = True AndAlso dtpEndDate.Checked = True Then
                    'Sohail (21 Oct 2019) -- End
                    objADate._Date2 = dtpEndDate.Value
                Else
                    objADate._Date2 = Nothing
                End If
                'Pinkal (09-Apr-2015) -- End
                If mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION Then
                    objADate._Isconfirmed = True
                Else
                    objADate._Isconfirmed = False
                End If
                objADate._Datetypeunkid = mintDatesTypeId
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                'objADate._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                If cboChangeReason.Enabled = True Then
                objADate._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                    objADate._Leaveissueunkid = 0
                Else
                    objADate._Changereasonunkid = CInt(cboChangeReason.Tag)
                    objADate._Leaveissueunkid = CInt(lblReason.Tag)
                End If
                'Sohail (21 Oct 2019) -- End
                objADate._Isvoid = False
                objADate._Statusunkid = 0
                objADate._Voiddatetime = Nothing
                objADate._Voidreason = ""
                objADate._Voiduserunkid = -1
                'S.SANDEEP [04 APR 2015] -- START
                If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                    objADate._Actionreasonunkid = CInt(cboChangeReason.SelectedValue)
                    objADate._Isexclude_Payroll = chkExclude.Checked
                End If
                objADate._Isvoid = False
                objADate._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                objADate._Voiddatetime = Nothing
                objADate._Voidreason = ""
                objADate._Voiduserunkid = -1
                objADate._Tranguid = Guid.NewGuid.ToString()
                objADate._Transactiondate = Now
                objADate._Remark = ""
                objADate._Rehiretranunkid = 0
                objADate._Mappingunkid = 0
                objADate._Isweb = False
                objADate._Isfinal = False
                objADate._Ip = getIP()
                objADate._Hostname = getHostName()
                objADate._Form_Name = mstrModuleName

                'S.SANDEEP |17-JAN-2019| -- START
                If mAction = enAction.EDIT_ONE Then
                    objADate._Datestranunkid = mintTransactionId
                    objADate._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    objADate._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                'S.SANDEEP |17-JAN-2019| -- END
            End If
            'S.SANDEEP [20-JUN-2018] -- END

            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            If dtpActualDate.Checked Then
                objADate._ActualDate = dtpActualDate.Value
            Else
                objADate._ActualDate = Nothing
            End If
            'Pinkal (07-Mar-2020) -- End


            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            objADate._Disciplinefileunkid = mintDisciplineunkid
            'Gajanan [18-May-2020] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetEditValue()
        Try
            dtpEffectiveDate.Value = objEDates._Effectivedate
            cboEmployee.SelectedValue = objEDates._Employeeunkid
            dtpStartDate.Value = objEDates._Date1

            'Pinkal (09-Apr-2015) -- Start
            'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            'If dtpEndDate.Visible = True Then
            If dtpEndDate.Visible = True AndAlso objEDates._Date2 <> Nothing Then
                'Sohail (21 Oct 2019) -- End
                dtpEndDate.Value = objEDates._Date2
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            Else
                If dtpEndDate.Checked = True Then dtpEndDate.Checked = False
                'Sohail (21 Oct 2019) -- End
            End If
            'Pinkal (09-Apr-2015) -- End


            chkExclude.Checked = objEDates._Isexclude_payroll
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : Employee exemption start date cannot be changed if that transaction is from leave for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            'cboChangeReason.SelectedValue = objEDates._Changereasonunkid
            If objEDates._Leaveissueunkid > 0 Then
                cboChangeReason.SelectedValue = 0
                cboChangeReason.Tag = CInt(objEDates._Changereasonunkid)
                lblReason.Tag = CInt(objEDates._Leaveissueunkid)
                cboChangeReason.Enabled = False
                objbtnAddReason.Enabled = False
                objSearchReason.Enabled = False
                dtpEffectiveDate.Enabled = False
                dtpStartDate.Enabled = False
            Else
            cboChangeReason.SelectedValue = objEDates._Changereasonunkid
                cboChangeReason.Tag = 0
                lblReason.Tag = 0 'Leaveissueunkid
                cboChangeReason.Enabled = True
                objbtnAddReason.Enabled = True
                objSearchReason.Enabled = True
                dtpEffectiveDate.Enabled = True
                dtpStartDate.Enabled = True
            End If
            'Sohail (21 Oct 2019) -- End


            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            If objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION Then
                If objEDates._ActualDate <> Nothing Then
                    dtpActualDate.Value = objEDates._ActualDate
                Else
                    dtpActualDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpActualDate.Checked = False
                End If
            End If
            'Pinkal (07-Mar-2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetEditValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            dtpEffectiveDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpEffectiveDate.Checked = False
            dtpStartDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpStartDate.Checked = False
            dtpEndDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpEndDate.Checked = False
            cboChangeReason.SelectedValue = 0
            txtDate.Text = ""
            pnlData.Visible = False
            mdtAppointmentDate = Nothing

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            mintTransactionId = 0
            'Gajanan [11-Dec-2019] -- End


            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            dtpActualDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpActualDate.Checked = False
            'Pinkal (07-Mar-2020) -- End


            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            mintDisciplineunkid = -1
            'Gajanan [18-May-2020] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearControls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetDatesForMail()
        Try
            xCurrentDates = New Dictionary(Of Integer, String)
            Select Case mintDatesTypeId
                Case enEmp_Dates_Transaction.DT_PROBATION
                    If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                        xCurrentDates.Add(enEmployeeDates.PROBATION_DATE, dtpStartDate.Value.Date.ToShortDateString & " - " & dtpEndDate.Value.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.PROBATION_DATE, "&nbsp;")
                    End If
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                        xCurrentDates.Add(enEmployeeDates.SUSPENSION_DATE, dtpStartDate.Value.Date.ToShortDateString & " - " & dtpEndDate.Value.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.SUSPENSION_DATE, "&nbsp;")
                    End If
                Case enEmp_Dates_Transaction.DT_TERMINATION
                    If dtpStartDate.Checked = True Then
                        xCurrentDates.Add(enEmployeeDates.EOC_DATE, dtpStartDate.Value.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.EOC_DATE, "&nbsp;")
                    End If

                    If dtpEndDate.Checked = True Then
                        xCurrentDates.Add(enEmployeeDates.LEAVING_DATE, dtpEndDate.Value.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.LEAVING_DATE, "&nbsp;")
                    End If

                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    If dtpStartDate.Checked = True Then
                        xCurrentDates.Add(enEmployeeDates.RETIREMENT_DATE, dtpStartDate.Value.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.RETIREMENT_DATE, "&nbsp;")
                    End If

                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    If dtpStartDate.Checked = True Then
                        xCurrentDates.Add(enEmployeeDates.CONFIRM_DATE, dtpStartDate.Value.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.CONFIRM_DATE, "&nbsp;")
                    End If

                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                        xCurrentDates.Add(enEmployeeDates.EXEMPTION_DATE, dtpStartDate.Value.Date.ToShortDateString & " - " & dtpEndDate.Value.Date.ToShortDateString)
                    ElseIf dtpStartDate.Checked = True Then
                        xCurrentDates.Add(enEmployeeDates.EXEMPTION_DATE, dtpStartDate.Value.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.EXEMPTION_DATE, "&nbsp;")
                    End If
                    'Sohail (21 Oct 2019) -- End
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDatesForMail", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (09-Apr-2015) -- Start
    'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA
    Private Sub SetVisibility()
        Try

            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            lnkSetSuspension.Visible = False
            'Gajanan [18-May-2020] -- End

            If mintDatesTypeId = enEmp_Dates_Transaction.DT_PROBATION Then

                'Shani (08-Dec-2016) -- Start
                'Enhancement -  Add Employee Allocaion/Date Privilage
                btnSave.Enabled = User._Object.Privilege._AllowtoSetEmpProbationDate
                'Shani (08-Dec-2016) -- End
                chkExclude.Visible = False
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.

                'S.SANDEEP |17-JAN-2019| -- START
                'objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditProbationEmployeeDetails
                If User._Object.Privilege._AllowToEditProbationEmployeeDetails = False Then
                    objdgcolhEdit.Image = imgBlank
                End If
                'S.SANDEEP |17-JAN-2019| -- END
                objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteProbationEmployeeDetails
                'Varsha Rana (17-Oct-2017) -- End

            ElseIf mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION OrElse mintDatesTypeId = enEmp_Dates_Transaction.DT_RETIREMENT Then
                chkExclude.Visible = False
                objlblEndTo.Visible = False
                dtpEndDate.Visible = False
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.


                'S.SANDEEP |17-JAN-2019| -- START
                'objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditConfirmationEmployeeDetails
                'objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteConfirmationEmployeeDetails


                'objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditRetiredEmployeeDetails
                'objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteRetiredEmployeeDetails
                If mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION Then
                    'objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditConfirmationEmployeeDetails
                    If User._Object.Privilege._AllowToEditConfirmationEmployeeDetails = False Then
                        objdgcolhEdit.Image = imgBlank
                    End If
                    objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteConfirmationEmployeeDetails
                Else
                    'objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditRetiredEmployeeDetails
                    If User._Object.Privilege._AllowToEditRetiredEmployeeDetails = False Then
                        objdgcolhEdit.Image = imgBlank
                    End If
                    objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteRetiredEmployeeDetails
                End If
                'S.SANDEEP |17-JAN-2019| -- END

                'Varsha Rana (17-Oct-2017) -- End

                'Shani (08-Dec-2016) -- Start
                'Enhancement -  Add Employee Allocaion/Date Privilage
                If mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION Then
                    btnSave.Enabled = User._Object.Privilege._AllowtoChangeConfirmationDate
                    'Varsha Rana (17-Oct-2017) -- Start
                    'Enhancement - Give user privileges.
                    objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditConfirmationEmployeeDetails
                    objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteConfirmationEmployeeDetails

                    mnuSalaryChange.Enabled = User._Object.Privilege._AddSalaryIncrement
                    mnuBenefits.Enabled = User._Object.Privilege._AllowToAssignBenefitGroup
                    mnuTransfers.Enabled = User._Object.Privilege._AllowToChangeEmpTransfers
                    mnuRecategorization.Enabled = User._Object.Privilege._AllowToChangeEmpRecategorize
                    'Varsha Rana (17-Oct-2017) -- End
                Else
                    btnSave.Enabled = User._Object.Privilege._AllowtoSetEmpReinstatementDate
                End If
                'Shani (08-Dec-2016) -- End





            ElseIf mintDatesTypeId = enEmp_Dates_Transaction.DT_SUSPENSION Then
                chkExclude.Visible = False

                lnkSetSuspension.Visible = True
                'Shani (08-Dec-2016) -- Start
                'Enhancement -  Add Employee Allocaion/Date Privilage
                btnSave.Enabled = User._Object.Privilege._AllowtoSetEmpSuspensionDate
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.

                'S.SANDEEP |17-JAN-2019| -- START
                'objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditSuspensionEmployeeDetails
                If User._Object.Privilege._AllowToEditSuspensionEmployeeDetails = False Then
                    objdgcolhEdit.Image = imgBlank
                End If
                'S.SANDEEP |17-JAN-2019| -- END


                objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteSuspensionEmployeeDetails
                'Varsha Rana (17-Oct-2017) -- End

            ElseIf mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then

                dtpStartDate.Enabled = User._Object.Privilege._AllowtoSetEmploymentEndDate
                dtpEndDate.Enabled = User._Object.Privilege._AllowtoSetLeavingDate
                If dtpStartDate.Enabled = False AndAlso dtpEndDate.Enabled = False Then
                    btnSave.Enabled = False
                End If
                'Shani (08-Dec-2016) -- End
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.

                'S.SANDEEP |17-JAN-2019| -- START
                'objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditTerminationEmployeeDetails
                If User._Object.Privilege._AllowToEditTerminationEmployeeDetails = False Then
                    objdgcolhEdit.Image = imgBlank
                End If
                'S.SANDEEP |17-JAN-2019| -- END

                objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteTerminationEmployeeDetails
                'Varsha Rana (17-Oct-2017) -- End


                'Pinkal (07-Mar-2020) -- Start
                'Enhancement - Changes Related to Payroll UAT for NMB.
                LblActualDate.Visible = True
                dtpActualDate.Visible = True
                dtpActualDate.Checked = False
                'Pinkal (07-Mar-2020) -- End

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            ElseIf mintDatesTypeId = enEmp_Dates_Transaction.DT_EXEMPTION Then
                chkExclude.Visible = False
                btnSave.Enabled = User._Object.Privilege._AllowToSetEmployeeExemptionDate

                If User._Object.Privilege._AllowToEditEmployeeExemptionDate = False Then
                    objdgcolhEdit.Image = imgBlank
                End If

                objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteEmployeeExemptionDate
                'Sohail (21 Oct 2019) -- End

            End If

            'Sohail (09 Oct 2019) -- Start
            'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
            lblPaymentDate.Visible = False
            txtPaymentDate.Visible = False
            If CInt(cboEmployee.SelectedValue) > 0 Then
                If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION OrElse mintDatesTypeId = enEmp_Dates_Transaction.DT_RETIREMENT Then
                    Dim objTnA As New clsTnALeaveTran
                    Dim ds As DataSet = objTnA.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, False, True, "List", CInt(cboEmployee.SelectedValue), 0, "prtnaleave_tran.processdate DESC")
                    If ds.Tables(0).Rows.Count > 0 Then
                        txtPaymentDate.Text = eZeeDate.convertDate(ds.Tables(0).Rows(0).Item("employee_enddate").ToString).ToShortDateString
                        lblPaymentDate.Visible = True
                        txtPaymentDate.Visible = True
                    End If
                End If
            End If
            'Sohail (09 Oct 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Pinkal (09-Apr-2015) -- End


    'Pinkal (02-Dec-2016) -- Start
    'Enhancement - Working on Leave Accrue Enhancement when employee terminated.
    Private Function UpdateLeaveBalance(ByVal drRow As DataRow, ByVal objLeaveAccrue As clsleavebalance_tran) As Boolean
        Dim mblnFlag As Boolean = False
        Dim mdtEndDate As Date = Nothing
        Try
            objLeaveAccrue._LeaveBalanceunkid = CInt(drRow("leavebalanceunkid"))

            With objLeaveAccrue
                ._FormName = mstrModuleName
                ._LoginEmployeeUnkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With

            If dtpStartDate.Value.Date < dtpEndDate.Value.Date Then
                mdtEndDate = dtpStartDate.Value.Date
            Else
                mdtEndDate = dtpEndDate.Value.Date
            End If

            If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                If mdtEndDate.Date > FinancialYear._Object._Database_End_Date.Date Then
                    mdtEndDate = FinancialYear._Object._Database_End_Date.Date
                End If
            ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                If mdtEndDate.Date > objLeaveAccrue._Startdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).AddDays(-1) Then
                    mdtEndDate = objLeaveAccrue._Startdate.Date.AddMonths(ConfigParameter._Object._ELCmonths).AddDays(-1)
                End If
            End If


            If objLeaveAccrue._IsshortLeave = False AndAlso objLeaveAccrue._Ispaid = True Then

                If ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Yearly Then
                    objLeaveAccrue._AccrueAmount = objLeaveAccrue._LeaveBF + objLeaveAccrue._AdjustmentAmt + (objLeaveAccrue._Daily_Amount * CInt(DateDiff(DateInterval.Day, objLeaveAccrue._Startdate.Date, mdtEndDate.Date.AddDays(1))))

                ElseIf ConfigParameter._Object._LeaveAccrueTenureSetting = enLeaveAccrueTenureSetting.Monthly Then

                    Dim xMonth As Integer = CInt(DateDiff(DateInterval.Month, objLeaveAccrue._Startdate.Date, mdtEndDate.Date.AddMonths(1)))

                    If ConfigParameter._Object._LeaveAccrueDaysAfterEachMonth > mdtEndDate.Date.Day Then
                        xMonth = xMonth - 1
                    End If

                    objLeaveAccrue._AccrueAmount = objLeaveAccrue._LeaveBF + objLeaveAccrue._AdjustmentAmt + (xMonth * objLeaveAccrue._Monthly_Accrue)
                End If

                objLeaveAccrue._Remaining_Balance = objLeaveAccrue._AccrueAmount - objLeaveAccrue._IssueAmount

            End If

            objLeaveAccrue._Enddate = mdtEndDate.Date

            If objLeaveAccrue.Update() = False Then
                eZeeMsgBox.Show(objLeaveAccrue._Message)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateLeaveBalance", mstrModuleName)
        End Try
    End Function

    'Pinkal (02-Dec-2016) -- End


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub FillEmployeeCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Try

            Dim blnIncludeInactiveEmployeeFlag As Boolean = False
            If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                'S.SANDEEP [02-NOV-2017] -- START
                'blnIncludeInactiveEmployeeFlag = True
                'S.SANDEEP [02-NOV-2017] -- END
            Else
                blnIncludeInactiveEmployeeFlag = ConfigParameter._Object._IsIncludeInactiveEmp
            End If
          
            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, blnIncludeInactiveEmployeeFlag, "Emp", True, , , , , , , , , , , , , , , mstrAdvanceFilter, True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
                .Text = ""
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "fillEmployeeCombo", mstrModuleName)
        Finally
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End


#End Region

#Region " Form's Events "

    Private Sub frmEmployeeDates_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEDates = New clsemployee_dates_tran

        'S.SANDEEP [20-JUN-2018] -- START
        'ISSUE/ENHANCEMENT : {Ref#244}
        objADate = New clsDates_Approval_Tran
        'S.SANDEEP [20-JUN-2018] -- END

        Try
            Call Set_Form_Controls()
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            End If
            'Gajanan [11-Dec-2019] -- End

            mnuBenefits.Enabled = False
            mnuRecategorization.Enabled = False
            mnuSalaryChange.Enabled = False
            mnuTransfers.Enabled = False
            pnlData.Visible = False
            'Sohail (09 Oct 2019) -- Start
            'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
            lblPaymentDate.Visible = False
            txtPaymentDate.Visible = False
            'Sohail (09 Oct 2019) -- End


            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            LblActualDate.Visible = False
            dtpActualDate.Checked = False
            dtpActualDate.Visible = False
            'Pinkal (07-Mar-2020) -- End


            'Pinkal (09-Apr-2015) -- Start
            'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA
            Call SetVisibility()
            'Pinkal (09-Apr-2015) -- End
            'S.SANDEEP [14 APR 2015] -- START
            objlblCaption.Visible = False
            'S.SANDEEP [14 APR 2015] -- END

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            lblPendingData.Visible = False
            'S.SANDEEP [20-JUN-2018] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeDates_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_dates_tran.SetMessages()
            objfrm._Other_ModuleNames = "clshremployee_dates_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Valid_Dates() = False Then Exit Sub
            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objEDates._FormName = mstrModuleName
            objEDates._LoginEmployeeunkid = 0
            objEDates._ClientIP = getIP()
            objEDates._HostName = getHostName()
            objEDates._FromWeb = False
            objEDates._AuditUserId = User._Object._Userunkid
objEDates._CompanyUnkid = Company._Object._Companyunkid
            objEDates._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
            'If mAction = enAction.EDIT_ONE Then
            '    blnFlag = objEDates.Update()
            'Else
            '    blnFlag = objEDates.Insert()
            'End If
            'Hemant (01 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES.
            'Hemant (27 Sep 2019) -- Start
            'If chkExclude.Checked = False Then
            If chkExclude.Visible = True AndAlso chkExclude.Checked = False Then
                'Hemant (27 Sep 2019) -- End
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 142, "Exclude from payroll process is not set.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 143, "Are you sure you do not want to Exclude Payroll Process for this Employee?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Information, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    chkExclude.Focus()
                    Exit Sub
                End If
            End If
            'Hemant (01 Aug 2019) -- End

            If mAction = enAction.EDIT_ONE Then
                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'blnFlag = objEDates.Update(Company._Object._Companyunkid)

                'S.SANDEEP |17-JAN-2019| -- START
                'blnFlag = objEDates.Update(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid)
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                blnFlag = objEDates.Update(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid)
                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, exemption Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If

                    blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, exemption Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, exemption Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, exemption Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If

                    blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, exemption Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, exemption Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    blnFlag = objADate.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED)

                    If blnFlag = False AndAlso objADate._Message <> "" Then
                        eZeeMsgBox.Show(objADate._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                End If


                'S.SANDEEP |17-JAN-2019| -- END
                'Pinkal (18-Aug-2018) -- End
            Else


                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}

                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'blnFlag = objEDates.Insert(Company._Object._Companyunkid)
                    blnFlag = objEDates.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid)
                    'Pinkal (18-Aug-2018) -- End


                    'Hemant (01 Aug 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES.
                    'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Do you want to add Benefit Coverage for selected employee ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Do you want to add Benefit Coverage for selected employee ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        'Hemant (01 Aug 2019) -- End
                        Dim objFrm As New frmEmployeeBenefitCoverage
                        objFrm.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue))
                    End If
                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, exemption Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If

                    blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, exemption Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, exemption Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, exemption Information is already present in approval process with selected effective date and allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If

                    blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, exemption Information is already present in approval process with selected effective date."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, exemption Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    blnFlag = objADate.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED)

                    If blnFlag = False AndAlso objADate._Message <> "" Then
                        eZeeMsgBox.Show(objADate._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                End If
                'S.SANDEEP [20-JUN-2018] -- END

            End If
            'S.SANDEEP [10-MAY-2017] -- END

            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo Then
                If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                    Dim mblnIsELC As Boolean = False
                    Dim mblnIsOpenelc As Boolean = False
                    If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        mblnIsELC = False
                        mblnIsOpenelc = False
                    ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        mblnIsELC = True
                        mblnIsOpenelc = True
                    End If

                    Dim objLeaveAccrue As New clsleavebalance_tran
                    Dim dsList As DataSet = objLeaveAccrue.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                                           , Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                                           , True, True, True, True, False, CInt(cboEmployee.SelectedValue), mblnIsOpenelc, mblnIsELC _
                                                                                           , False, "", Nothing, False)

                    'FOR PAID LEAVE AND SHORT LEAVE = FALSE
                    Dim DrRow = From dr In dsList.Tables(0) Where CBool(dr.Item("isshortleave")) = False AndAlso CBool(dr.Item("ispaid")) = True Select dr
                    If DrRow.Count > 0 Then
                        DrRow.ToList.ForEach(Function(x) UpdateLeaveBalance(x, objLeaveAccrue))
                    End If

                    'FOR PAID LEAVE AND SHORT LEAVE = TRUE
                    DrRow = Nothing
                    DrRow = From dr In dsList.Tables(0) Where CBool(dr.Item("isshortleave")) = True AndAlso CBool(dr.Item("ispaid")) = True Select dr
                    If DrRow.Count > 0 Then
                        DrRow.ToList.ForEach(Function(x) UpdateLeaveBalance(x, objLeaveAccrue))
                    End If

                    dsList = Nothing
                    DrRow = Nothing
                    objLeaveAccrue = Nothing
                End If


            End If
            'Pinkal (16-Dec-2016) -- End

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim intSelectedEmployee As Integer = 0
            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                intSelectedEmployee = CInt(cboEmployee.SelectedValue)
            End If
            'S.SANDEEP [20-JUN-2018] -- END

            'S.SANDEEP [02-NOV-2017] -- START
            If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                Call FillCombo()
                cboEmployee.SelectedValue = 0
            End If
            'S.SANDEEP [02-NOV-2017] -- END

            If blnFlag = False AndAlso objEDates._Message <> "" Then
                eZeeMsgBox.Show(objEDates._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                'If mAction = enAction.EDIT_ONE Then mAction = enAction.ADD_CONTINUE
                'Call Fill_Grid()
                'Sohail (21 Oct 2019) -- End

                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'Call SetDatesForMail()
                ''S.SANDEEP [08 DEC 2016] -- START
                ''ENHANCEMENT : Issue for Wrong User Name Sent in Email (Edit By User : <UserName>)
                ''Call objEDates.SendEmails(mintDatesTypeId, CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.Text, xCurrentDates, ConfigParameter._Object._Notify_Dates, dtpEffectiveDate.Value.Date)
                ''Sohail (30 Nov 2017) -- Start
                ''SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                ''Call objEDates.SendEmails(mintDatesTypeId, CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.Text, xCurrentDates, ConfigParameter._Object._Notify_Dates, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP)
                'Call objEDates.SendEmails(mintDatesTypeId, CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.Text, xCurrentDates, ConfigParameter._Object._Notify_Dates, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)
                ''Sohail (30 Nov 2017) -- End
                ''S.SANDEEP [08 DEC 2016] -- END
                'Call Set_Dates()

                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then

                    'Gajanan [4-May-2021] -- Start


                    'Pinkal (18-May-2021) -- Start
                    'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
                    Call SetDatesForMail()
                    'Pinkal (18-May-2021) -- End

                    ''S.SANDEEP |10-MAY-2019| -- START
                    ''ISSUE/ENHANCEMENT : {#0003807}
                    ''Call objEDates.SendEmails(mintDatesTypeId, CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.Text, xCurrentDates, ConfigParameter._Object._Notify_Dates, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)
                    'Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, ConfigParameter._Object._Notify_Dates, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)
                    ''S.SANDEEP |10-MAY-2019| -- END


                    'Pinkal (18-May-2021) -- Start
                    'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.

                    'xCurrentDates = New Dictionary(Of Integer, String)
                    'Select Case mintDatesTypeId
                    '    Case enEmp_Dates_Transaction.DT_PROBATION
                    '        xCurrentDates.Clear()
                    '        If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                    '            xCurrentDates.Add(enEmployeeDates.PROBATION_DATE, dtpStartDate.Value.Date.ToShortDateString & " - " & dtpEndDate.Value.Date.ToShortDateString)
                    '        Else
                    '            xCurrentDates.Add(enEmployeeDates.PROBATION_DATE, "&nbsp;")
                    '        End If
                    '        Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, ConfigParameter._Object._ProbationDateUserNotification, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)



                    '    Case enEmp_Dates_Transaction.DT_SUSPENSION
                    '        xCurrentDates.Clear()
                    '        If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                    '            xCurrentDates.Add(enEmployeeDates.SUSPENSION_DATE, dtpStartDate.Value.Date.ToShortDateString & " - " & dtpEndDate.Value.Date.ToShortDateString)
                    '        Else
                    '            xCurrentDates.Add(enEmployeeDates.SUSPENSION_DATE, "&nbsp;")
                    '        End If
                    '        Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, ConfigParameter._Object._SuspensionDateUserNotification, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)

                    '    Case enEmp_Dates_Transaction.DT_TERMINATION
                    '        xCurrentDates.Clear()
                    '        If dtpStartDate.Checked = True Then
                    '            xCurrentDates.Add(enEmployeeDates.EOC_DATE, dtpStartDate.Value.Date.ToShortDateString)
                    '        Else
                    '            xCurrentDates.Add(enEmployeeDates.EOC_DATE, "&nbsp;")
                    '        End If
                    '        Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, ConfigParameter._Object._EocDateUserNotification, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)

                    '        xCurrentDates.Clear()
                    '        If dtpEndDate.Checked = True Then
                    '            xCurrentDates.Add(enEmployeeDates.LEAVING_DATE, dtpEndDate.Value.Date.ToShortDateString)
                    '        Else
                    '            xCurrentDates.Add(enEmployeeDates.LEAVING_DATE, "&nbsp;")
                    '        End If
                    '        Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, ConfigParameter._Object._LeavingDateUserNotification, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)

                    '    Case enEmp_Dates_Transaction.DT_RETIREMENT
                    '        If dtpStartDate.Checked = True Then
                    '            xCurrentDates.Add(enEmployeeDates.RETIREMENT_DATE, dtpStartDate.Value.Date.ToShortDateString)
                    '        Else
                    '            xCurrentDates.Add(enEmployeeDates.RETIREMENT_DATE, "&nbsp;")
                    '        End If
                    '        Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, ConfigParameter._Object._RetirementDateUserNotification, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)

                    '    Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    '        xCurrentDates.Clear()
                    '        If dtpStartDate.Checked = True Then
                    '            xCurrentDates.Add(enEmployeeDates.CONFIRM_DATE, dtpStartDate.Value.Date.ToShortDateString)
                    '        Else
                    '            xCurrentDates.Add(enEmployeeDates.CONFIRM_DATE, "&nbsp;")
                    '        End If
                    '        Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, ConfigParameter._Object._ConfirmDateUserNotification, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)

                    '        'Sohail (21 Oct 2019) -- Start
                    '        'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                    '    Case enEmp_Dates_Transaction.DT_EXEMPTION
                    '        xCurrentDates.Clear()
                    '        If dtpStartDate.Checked = True AndAlso dtpEndDate.Checked = True Then
                    '            xCurrentDates.Add(enEmployeeDates.EXEMPTION_DATE, dtpStartDate.Value.Date.ToShortDateString & " - " & dtpEndDate.Value.Date.ToShortDateString)
                    '        ElseIf dtpStartDate.Checked = True Then
                    '            xCurrentDates.Add(enEmployeeDates.EXEMPTION_DATE, dtpStartDate.Value.Date.ToShortDateString)
                    '        Else
                    '            xCurrentDates.Add(enEmployeeDates.EXEMPTION_DATE, "&nbsp;")
                    '        End If
                    '        Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, ConfigParameter._Object._ExemptionDateUserNotification, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)

                    '        'Sohail (21 Oct 2019) -- End
                    'End Select

                    Dim mstrUserNotificationIds As String = ""
                    Select Case mintDatesTypeId
                        Case enEmp_Dates_Transaction.DT_PROBATION
                            mstrUserNotificationIds = ConfigParameter._Object._ProbationDateUserNotification

                        Case enEmp_Dates_Transaction.DT_SUSPENSION
                            mstrUserNotificationIds = ConfigParameter._Object._SuspensionDateUserNotification

                        Case enEmp_Dates_Transaction.DT_TERMINATION
                            'EOC DATE
                            xCurrentDates.Clear()
                            If dtpStartDate.Checked = True Then
                                xCurrentDates.Add(enEmployeeDates.EOC_DATE, dtpStartDate.Value.Date.ToShortDateString)
                            Else
                                xCurrentDates.Add(enEmployeeDates.EOC_DATE, "&nbsp;")
                            End If
                            Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, ConfigParameter._Object._EocDateUserNotification, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)

                            'LEAVING DATE
                            xCurrentDates.Clear()
                            If dtpEndDate.Checked = True Then
                                xCurrentDates.Add(enEmployeeDates.LEAVING_DATE, dtpEndDate.Value.Date.ToShortDateString)
                            Else
                                xCurrentDates.Add(enEmployeeDates.LEAVING_DATE, "&nbsp;")
                            End If
                            Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, ConfigParameter._Object._LeavingDateUserNotification, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)

                        Case enEmp_Dates_Transaction.DT_RETIREMENT
                            mstrUserNotificationIds = ConfigParameter._Object._RetirementDateUserNotification

                        Case enEmp_Dates_Transaction.DT_CONFIRMATION
                            mstrUserNotificationIds = ConfigParameter._Object._ConfirmDateUserNotification

                        Case enEmp_Dates_Transaction.DT_EXEMPTION
                            mstrUserNotificationIds = ConfigParameter._Object._ExemptionDateUserNotification

                    End Select

                    If mintDatesTypeId <> enEmp_Dates_Transaction.DT_TERMINATION Then
                        Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, mstrUserNotificationIds, dtpEffectiveDate.Value.Date, getHostName(), getIP(), User._Object._Username, User._Object._Userunkid, enLogin_Mode.DESKTOP, Company._Object._Companyunkid)
                            End If

                    'Pinkal (18-May-2021) -- End

                    'Gajanan [4-May-2021] -- End



                Else
                    Dim objPMovement As New clsEmployeeMovmentApproval
                    Dim intPrivilegeId As Integer = 0
                    Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                    Select Case mintDatesTypeId
                        Case enEmp_Dates_Transaction.DT_PROBATION
                            intPrivilegeId = 1194 : eMovement = clsEmployeeMovmentApproval.enMovementType.PROBATION
                        Case enEmp_Dates_Transaction.DT_RETIREMENT
                            intPrivilegeId = 1198 : eMovement = clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                        Case enEmp_Dates_Transaction.DT_SUSPENSION
                            intPrivilegeId = 1196 : eMovement = clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                        Case enEmp_Dates_Transaction.DT_TERMINATION
                            intPrivilegeId = 1197 : eMovement = clsEmployeeMovmentApproval.enMovementType.TERMINATION
                        Case enEmp_Dates_Transaction.DT_CONFIRMATION
                            intPrivilegeId = 1195 : eMovement = clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                            'Sohail (21 Oct 2019) -- Start
                            'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                        Case enEmp_Dates_Transaction.DT_EXEMPTION
                            intPrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeExemption : eMovement = clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                            'Sohail (21 Oct 2019) -- End
                    End Select
                    'S.SANDEEP |17-JAN-2019| -- START
                    'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, CType(mintDatesTypeId, enEmp_Dates_Transaction), 0, intSelectedEmployee.ToString(), "")
                    Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                    If mAction = enAction.EDIT_ONE Then
                        eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                    Else
                        eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                    End If
                    objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, eOperType, False, CType(mintDatesTypeId, enEmp_Dates_Transaction), 0, intSelectedEmployee.ToString(), "")
                    'S.SANDEEP |17-JAN-2019| -- END
                    objPMovement = Nothing
                End If
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                If mAction = enAction.EDIT_ONE Then mAction = enAction.ADD_CONTINUE
                Call Fill_Grid()
                'Sohail (21 Oct 2019) -- End
                Call Set_Dates()
                'S.SANDEEP [20-JUN-2018] -- END

            End If
            Call ClearControls()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objSearchReason.Click
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim xCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchEmployee.Name.ToUpper
                    xCbo = cboEmployee
                Case objSearchReason.Name.ToUpper
                    xCbo = cboChangeReason
            End Select
            If xCbo.DataSource Is Nothing Then Exit Sub

            With frm
                .ValueMember = xCbo.ValueMember
                .DisplayMember = xCbo.DisplayMember
                If xCbo.Name = cboEmployee.Name Then
                    .CodeMember = "employeecode"
                End If
                .DataSource = CType(xCbo.DataSource, DataTable)
                If .DisplayDialog = True Then
                    xCbo.SelectedValue = .SelectedValue
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, xMasterType, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(CType(xMasterType, clsCommon_Master.enCommonMaster), True, "List")
                With cboChangeReason
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
            FillEmployeeCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End
#End Region

#Region " Combobox Event(s) "


    'Pinkal (09-Apr-2015) -- Start
    'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA

    'Private Sub cboEmployee_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboEmployee.KeyDown
    '    Try
    '        If (e.KeyCode >= 65 AndAlso e.KeyCode <= 90) Or (e.KeyCode >= 97 AndAlso e.KeyCode <= 122) Or (e.KeyCode >= 47 AndAlso e.KeyCode <= 57) Then
    '            Dim frm As New frmCommonSearch
    '            With frm
    '                .ValueMember = cboEmployee.ValueMember
    '                .DisplayMember = cboEmployee.DisplayMember
    '                .DataSource = CType(cboEmployee.DataSource, DataTable)
    '                .CodeMember = "employeecode"
    '                .SelectedText = cboEmployee.Text
    '            End With
    '            If frm.DisplayDialog Then
    '                cboEmployee.SelectedValue = frm.SelectedValue
    '                If CInt(cboEmployee.SelectedValue) <= 0 Then cboEmployee.Text = ""
    '            Else
    '                cboEmployee.Text = ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (09-Apr-2015) -- End

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim xtmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                If xtmp.Length > 0 Then
                    mstrEmployeeCode = CStr(xtmp(0).Item("employeecode"))
                    'S.SANDEEP |10-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : {#0003807}
                    mintEmployeeUnkid = CInt(cboEmployee.SelectedValue)
                    mstrEmployeeName = cboEmployee.Text
                    'S.SANDEEP |10-MAY-2019| -- END
                End If

                'S.SANDEEP [14 APR 2015] -- START
                objlblCaption.Visible = False
                'S.SANDEEP [14 APR 2015] -- END

                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : STOP ENTRY POSTING FOR TERMINATED EMPLOYEE
                If btnSave.Enabled = False Then btnSave.Enabled = True
                If objdgcolhEdit.Visible = False Then objdgcolhEdit.Visible = True
                If objdgcolhDelete.Visible = False Then objdgcolhDelete.Visible = True
                'S.SANDEEP [04-AUG-2017] -- END


                'S.SANDEEP [07-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001988}
                If mAction = enAction.EDIT_ONE Then
                    If objEDates._Datestranunkid > 0 Then objEDates._Datestranunkid = 0
                    mAction = enAction.ADD_CONTINUE
                End If
                'S.SANDEEP [07-Feb-2018] -- END

                Call ClearControls()
                Call Fill_Grid()
                Call Set_Dates()

                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                'mnuBenefits.Enabled = True
                'mnuRecategorization.Enabled = True
                'mnuSalaryChange.Enabled = True
                'mnuTransfers.Enabled = True
                Call SetVisibility()
                'Varsha Rana (17-Oct-2017) -- End

                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : STOP ENTRY POSTING FOR TERMINATED EMPLOYEE
                If ConfigParameter._Object._IsIncludeInactiveEmp Then
                    Dim objEmp As New clsEmployee_Master
                    objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date) = CInt(cboEmployee.SelectedValue)
                    If objEmp._Empl_Enddate <> Nothing Then
                        If objEmp._Empl_Enddate <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    If objEmp._Termination_From_Date <> Nothing Then
                        If objEmp._Termination_From_Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    If objEmp._Termination_To_Date <> Nothing Then
                        If objEmp._Termination_To_Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    objEmp = Nothing
                End If
                'S.SANDEEP [04-AUG-2017] -- END
            Else
                mnuBenefits.Enabled = False
                mnuRecategorization.Enabled = False
                mnuSalaryChange.Enabled = False
                mnuTransfers.Enabled = False
                dgvHistory.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvHistory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHistory.CellClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            mdtAppointmentDate = Nothing
            txtDate.Text = ""
            pnlData.Visible = False
            Select Case e.ColumnIndex
                'S.SANDEEP [09-AUG-2018] -- START
                Case objdgcolhViewPending.Index
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhViewPending.Index).Value Is imgInfo Then
                        Dim frm As New frmViewMovementApproval
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        Dim intMovementtypeId As Integer = 0
                        Dim intPrivilegeId As Integer = 0
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                intMovementtypeId = clsEmployeeMovmentApproval.enMovementType.PROBATION
                                intPrivilegeId = 1194
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                intMovementtypeId = clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                                intPrivilegeId = 1195
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                intMovementtypeId = clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                                intPrivilegeId = 1198
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                intMovementtypeId = clsEmployeeMovmentApproval.enMovementType.TERMINATION
                                intPrivilegeId = 1197
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                intMovementtypeId = clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                                intPrivilegeId = 1196
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                intMovementtypeId = clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                                intPrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeExemption
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        'S.SANDEEP |17-JAN-2019| -- START
                        ''frm.displayDialog(User._Object._Userunkid, 0, intPrivilegeId, CType(intMovementtypeId, clsEmployeeMovmentApproval.enMovementType), CType(mintDatesTypeId, enEmp_Dates_Transaction), False, "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue), False)
                        'Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                        'If mAction = enAction.EDIT_ONE Then
                        '    eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                        'Else
                        '    eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                        'End If
                        frm.displayDialog(User._Object._Userunkid, 0, intPrivilegeId, CType(intMovementtypeId, clsEmployeeMovmentApproval.enMovementType), CType(mintDatesTypeId, enEmp_Dates_Transaction), False, CType(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhOperationTypeId.Index).Value, clsEmployeeMovmentApproval.enOperationType), "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue), False)
                        'S.SANDEEP |17-JAN-2019| -- END

                        If frm IsNot Nothing Then frm.Dispose()
                        Exit Sub
                    End If
                    'S.SANDEEP [09-AUG-2018] -- END

                Case objdgcolhEdit.Index
                    'S.SANDEEP |17-JAN-2019| -- START
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgView Then
                        If CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdatetranunkid.Index).Value) > 0 Then
                            Dim dr As DataRow() = CType(dgvHistory.DataSource, DataTable).Select(objdgcolhdatetranunkid.DataPropertyName & " = " & CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdatetranunkid.Index).Value) & " AND " & objdgcolhtranguid.DataPropertyName & "= ''")
                            If dr.Length > 0 Then
                                Dim index As Integer = CType(dgvHistory.DataSource, DataTable).Rows.IndexOf(dr(0))
                                dgvHistory.Rows(index).Selected = True
                                dgvHistory.FirstDisplayedScrollingRowIndex = index
                            End If
                        End If
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END



                    mAction = enAction.EDIT_ONE
                    'S.SANDEEP [16 Jan 2016] -- START
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgBlank Then Exit Sub
                    'S.SANDEEP [16 Jan 2016] -- END
                    objEDates._Datestranunkid = CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdatetranunkid.Index).Value)

                    'S.SANDEEP [15-NOV-2018] -- START
                    mintTransactionId = objEDates._Datestranunkid
                    'S.SANDEEP [15-NOV-2018] -- END

                    'S.SANDEEP |17-JAN-2019| -- START
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        Dim Flag As Boolean = objADate.isExist(Nothing, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransactionId)
                        If Flag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 140, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END

                    'S.SANDEEP |27-JUN-2020| -- START
                    'ISSUE/ENHANCEMENT : NMB ENHANCEMENT
                    If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                        Dim objTnALeaveTran As New clsTnALeaveTran
                        Dim objMData As New clsMasterData : Dim iPeriod As Integer = 0
                        iPeriod = objMData.getCurrentPeriodID(enModuleReference.Payroll, dtpEffectiveDate.Value.Date, FinancialYear._Object._YearUnkid, enStatusType.Open, True)
                        If iPeriod <= 0 Then
                            objMData.getCurrentPeriodID(enModuleReference.Payroll, dtpStartDate.Value.Date, FinancialYear._Object._YearUnkid, enStatusType.Open, True)
                        End If
                        If iPeriod <= 0 Then
                            objMData.getCurrentPeriodID(enModuleReference.Payroll, dtpEndDate.Value.Date, FinancialYear._Object._YearUnkid, enStatusType.Open, True)
                        End If

                        'Sohail (13 Jan 2022) -- Start
                        'Enhancement : OLD-216 : Delink the setting "Allow to Terminate Employee If Payment is Done" with privilege "Allow to change EOC on closed period" and allow the setting to terminate employee if payment is done even for users who don’t have that privilege.
                        'If iPeriod > 0 AndAlso User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod = True Then
                        'Sohail (21 Jan 2022) -- Start
                        'Enhancement : OLD-216 : Delink the setting "Allow to Terminate Employee If Payment is Done" with privilege "Allow to change EOC on closed period" and allow the setting to terminate employee if payment is done even for users who don’t have that privilege.
                        'If iPeriod > 0 AndAlso (User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod = True OrElse ConfigParameter._Object._AllowTerminationIfPaymentDone = True) Then
                        If iPeriod > 0 Then
                            'Sohail (21 Jan 2022) -- End
                            'Sohail (13 Jan 2022) -- End
                            Dim objPrd As New clscommom_period_Tran
                            objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriod

                            If ConfigParameter._Object._AllowTerminationIfPaymentDone = False Then
                                If objTnALeaveTran.IsPayrollProcessDone(iPeriod, CStr(cboEmployee.SelectedValue), objPrd._End_Date, enModuleReference.Payroll) Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 113, "Sorry, you cannot exclude selected employee. Reason:Payroll Process already done for the employee for last date of current open period."), enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                        End If
                        objTnALeaveTran = Nothing
                    End If
                    'S.SANDEEP |27-JUN-2020| -- END



                    Call SetEditValue()
                    If CBool(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhFromEmp.Index).Value) = True Then
                        mdtAppointmentDate = eZeeDate.convertDate(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhAppointdate.Index).Value.ToString)
                        txtDate.Text = mdtAppointmentDate.Date.ToShortDateString
                        pnlData.Visible = True
                    End If
                Case objdgcolhDelete.Index
                    mAction = enAction.ADD_ONE
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub

                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                    objEDates._Datestranunkid = CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdatetranunkid.Index).Value)
                    If objEDates._Leaveissueunkid > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 190, "Sorry, You can not perform any opration, Reason:This is auto generated entry from leave module."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'Sohail (21 Oct 2019) -- End

                    'S.SANDEEP |17-JAN-2019| -- START
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        Dim Flag As Boolean = objADate.isExist(Nothing, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdatetranunkid.Index).Value))
                        If Flag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 141, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END
                    'Pinkal (09-Apr-2015) -- Start
                    'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA

                    If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                        Dim objMaster As New clsMasterData
                        Dim mintPeriodID As Integer = 0

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'mintPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, CDate(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhStartDate.Index).Value))
                        'If mintPeriodID <= 0 Then
                        '    mintPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, CDate(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEndDate.Index).Value))
                        'End If

                        mintPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, CDate(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhStartDate.Index).Value), FinancialYear._Object._YearUnkid)
                        If mintPeriodID <= 0 Then
                            mintPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, CDate(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEndDate.Index).Value), FinancialYear._Object._YearUnkid)
                        End If
                        'S.SANDEEP [04 JUN 2015] -- END

                        Dim objPeriod As New clscommom_period_Tran
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objPeriod._Periodunkid = mintPeriodID
                        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodID
                        'Sohail (21 Aug 2015) -- End
                        If objPeriod._Statusid = enStatusType.Close Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "You cannot delete this transaction.Reason: Period is already closed for the selected date."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Exit Sub
                        End If
                        objPeriod = Nothing
                    End If

                    'Pinkal (09-Apr-2015) -- End

                    Dim xStrVoidReason As String = String.Empty
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.displayDialog(enVoidCategoryType.EMPLOYEE, xStrVoidReason)
                    If xStrVoidReason.Trim.Length <= 0 Then Exit Sub
                    objEDates._Isvoid = True
                    objEDates._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objEDates._Voidreason = xStrVoidReason
                    objEDates._Voiduserunkid = User._Object._Userunkid
                    objEDates._Userunkid = User._Object._Userunkid

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objEDates._FormName = mstrModuleName
                    objEDates._LoginEmployeeunkid = 0
                    objEDates._ClientIP = getIP()
                    objEDates._HostName = getHostName()
                    objEDates._FromWeb = False
                    objEDates._AuditUserId = User._Object._Userunkid
objEDates._CompanyUnkid = Company._Object._Companyunkid
                    objEDates._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    'S.SANDEEP [10-MAY-2017] -- START
                    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If objEDates.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdatetranunkid.Index).Value), Company._Object._Companyunkid) = False Then

                    'S.SANDEEP |17-JAN-2019| -- START
                    'If objEDates.Delete(ConfigParameter._Object._CreateADUserFromEmpMst, CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdatetranunkid.Index).Value), Company._Object._Companyunkid) = False Then
                    '    'Pinkal (18-Aug-2018) -- End
                    '    'If objEDates.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdatetranunkid.Index).Value)) = False Then
                    '    'S.SANDEEP [10-MAY-2017] -- END
                    '    If objEDates._Message <> "" Then
                    '        eZeeMsgBox.Show(objEDates._Message, enMsgBoxStyle.Information)
                    '    End If
                    '    Exit Sub
                    'End If
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        objADate._Isvoid = False
                        objADate._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objADate._Voidreason = xStrVoidReason
                        objADate._Voiduserunkid = -1
                        objADate._Audituserunkid = User._Object._Userunkid
                        objADate._Isweb = False
                        objADate._Ip = getIP()
                        objADate._Hostname = getHostName()
                        objADate._Form_Name = mstrModuleName
                        objADate._Audituserunkid = User._Object._Userunkid

                        If objADate.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdatetranunkid.Index).Value), clsEmployeeMovmentApproval.enOperationType.DELETED, Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, Nothing) = False Then
                            If objADate._Message <> "" Then
                                eZeeMsgBox.Show(objADate._Message, enMsgBoxStyle.Information)
                            End If
                            Exit Sub
                        End If

                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set

                        Dim objPMovement As New clsEmployeeMovmentApproval
                        Dim intPrivilegeId As Integer = 0
                        Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                intPrivilegeId = 1194 : eMovement = clsEmployeeMovmentApproval.enMovementType.PROBATION
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                intPrivilegeId = 1198 : eMovement = clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                intPrivilegeId = 1196 : eMovement = clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                intPrivilegeId = 1197 : eMovement = clsEmployeeMovmentApproval.enMovementType.TERMINATION
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                intPrivilegeId = 1195 : eMovement = clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                intPrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeExemption : eMovement = clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                        End Select

                        objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, _
                                                      Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                      intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, _
                                                      User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                                      User._Object._Username, clsEmployeeMovmentApproval.enOperationType.DELETED, _
                                                      False, CType(mintDatesTypeId, enEmp_Dates_Transaction), 0, cboEmployee.SelectedValue.ToString, "")

                        'Sohail (21 Oct 2019) -- End


                    Else
                    If objEDates.Delete(ConfigParameter._Object._CreateADUserFromEmpMst, CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhdatetranunkid.Index).Value), Company._Object._Companyunkid) = False Then
                        If objEDates._Message <> "" Then
                            eZeeMsgBox.Show(objEDates._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If
                    End If
                    'S.SANDEEP |17-JAN-2019| -- End


                    Call Fill_Grid()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_CellClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvHistory_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvHistory.DataBindingComplete
        Try
            For Each xdgvr As DataGridViewRow In dgvHistory.Rows
                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'If CBool(xdgvr.Cells(objdgcolhFromEmp.Index).Value) = True Then
                '    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                'End If
                If CStr(xdgvr.Cells(objdgcolhtranguid.Index).Value).Trim.Length > 0 Then
                    xdgvr.DefaultCellStyle.BackColor = Color.PowderBlue
                    xdgvr.DefaultCellStyle.ForeColor = Color.Black
                    xdgvr.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                    'S.SANDEEP [09-AUG-2018] -- START
                    xdgvr.Cells(objdgcolhViewPending.Index).Value = imgInfo
                    'S.SANDEEP [09-AUG-2018] -- END
                    lblPendingData.Visible = True

                    'S.SANDEEP |17-JAN-2019| -- START
                    xdgvr.Cells(objdgcolhViewPending.Index).ToolTipText = xdgvr.Cells(objdgcolhOperationType.Index).Value.ToString
                    If CInt(xdgvr.Cells(objdgcolhOperationTypeId.Index).Value) = clsEmployeeMovmentApproval.enOperationType.EDITED Then
                        xdgvr.Cells(objdgcolhEdit.Index).Value = imgView
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END
                Else
                If CBool(xdgvr.Cells(objdgcolhFromEmp.Index).Value) = True Then
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                End If
                End If
                'S.SANDEEP [20-JUN-2018] -- END

                'S.SANDEEP [14 APR 2015] -- START
                If CInt(xdgvr.Cells(objdgcolhrehiretranunkid.Index).Value) > 0 Then
                    For xCellIdx As Integer = 0 To xdgvr.Cells.Count - 1
                        If xCellIdx > 1 Then
                            xdgvr.Cells(xCellIdx).Style.BackColor = Color.Orange
                            xdgvr.Cells(xCellIdx).Style.ForeColor = Color.Black
                        End If
                    Next
                    xdgvr.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                    xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    objlblCaption.Visible = True
                End If
                'S.SANDEEP [14 APR 2015] -- END

                'S.SANDEEP [16 Jan 2016] -- START
                If CStr(xdgvr.Cells(objdgcolhtranguid.Index).Value).Trim.Length > 0 Then
                If CBool(xdgvr.Cells(objdgcolhFromEmp.Index).Value) = False Then
                    If CBool(xdgvr.Cells(objdgcolhallowopr.Index).Value) = False Then
                        xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                        xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    End If
                End If
                End If
                ''S.SANDEEP [16 Jan 2016] -- END

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Datepicker Event(s) "

    Private Sub dtpStartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpStartDate.ValueChanged
        Try
            If dtpEndDate.Visible = True Then
                'S.SANDEEP |02-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
                'dtpEndDate.Checked = dtpStartDate.Checked
                'dtpEndDate.Value = dtpStartDate.Value
                If mintDatesTypeId <> enEmp_Dates_Transaction.DT_SUSPENSION Then
                dtpEndDate.Checked = dtpStartDate.Checked
                dtpEndDate.Value = dtpStartDate.Value
            End If
                'S.SANDEEP |02-MAR-2020| -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpStartDate_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Context Menu Events "

    Private Sub mnuSalaryChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSalaryChange.Click
        Dim frm As New frmSalaryIncrement_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSalaryChange_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuTransfers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTransfers.Click
        Dim frm As New frmEmployeeTransfers
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuTransfers_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuRecategorization_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuRecategorization.Click
        Dim frm As New frmEmployeeRecategorize
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._EmployeeId = CInt(cboEmployee.SelectedValue)
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRecategorization_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuBenefits_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuBenefits.Click
        'Pinkal (08-Jan-2016) -- Start
        'Enhancement - Working on Employee Benefit changes given By Rutta.
        'Dim frm As New frmEmployeeBenefitCoverage
        'Try
        '    If User._Object._Isrighttoleft = True Then
        '        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
        '        frm.RightToLeftLayout = True
        '        Call Language.ctlRightToLeftlayOut(frm)
        '    End If
        '    frm.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue))
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "mnuBenefits_Click", mstrModuleName)
        'Finally
        '    If frm IsNot Nothing Then frm.Dispose()
        'End Try
        'Sohail (17 Sep 2019) -- Start
        'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
        'Dim frm As New frmAssignGroupBenefit
        'Try
        '    If User._Object._Isrighttoleft = True Then
        '        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
        '        frm.RightToLeftLayout = True
        '        Call Language.ctlRightToLeftlayOut(frm)
        '    End If
        '    frm.displayDialog(enAction.ADD_ONE, CInt(cboEmployee.SelectedValue), CDate(IIf(dtpEffectiveDate.Checked, dtpEffectiveDate.Value.Date, Nothing)))
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "mnuBenefits_Click", mstrModuleName)
        'Finally
        '    If frm IsNot Nothing Then frm.Dispose()
        'End Try
        Dim frm As New frmEmployeeBenefitCoverage
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(cboEmployee.SelectedValue))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuBenefits_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
        'Sohail (17 Sep 2019) -- End
        'Pinkal (08-Jan-2016) -- End
    End Sub

#End Region

#Region " Other Control Events "

    Private Sub lnkSetSuspension_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetSuspension.LinkClicked
        Try
            Dim frm As New frmViewDisciplineChargesInfo
            If frm.displayDialog(CInt(cboEmployee.SelectedValue), mintDisciplineunkid) Then

                If mintDisciplineunkid > 0 Then
                    cboChangeReason.Enabled = False
                    cboChangeReason.SelectedIndex = 0
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetSuspension_LinkClicked", mstrModuleName)
        End Try
    End Sub
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.objgbDatesInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.objgbDatesInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
            Me.lblEmplReason.Text = Language._Object.getCaption(Me.lblEmplReason.Name, Me.lblEmplReason.Text)
            Me.chkExclude.Text = Language._Object.getCaption(Me.chkExclude.Name, Me.chkExclude.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuSalaryChange.Text = Language._Object.getCaption(Me.mnuSalaryChange.Name, Me.mnuSalaryChange.Text)
            Me.mnuBenefits.Text = Language._Object.getCaption(Me.mnuBenefits.Name, Me.mnuBenefits.Text)
            Me.mnuTransfers.Text = Language._Object.getCaption(Me.mnuTransfers.Name, Me.mnuTransfers.Text)
            Me.mnuRecategorization.Text = Language._Object.getCaption(Me.mnuRecategorization.Name, Me.mnuRecategorization.Text)
            Me.lblAppointmentdate.Text = Language._Object.getCaption(Me.lblAppointmentdate.Name, Me.lblAppointmentdate.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.lblPendingData.Text = Language._Object.getCaption(Me.lblPendingData.Name, Me.lblPendingData.Text)
			Me.dgcolhChangeDate.HeaderText = Language._Object.getCaption(Me.dgcolhChangeDate.Name, Me.dgcolhChangeDate.HeaderText)
			Me.dgcolhReason.HeaderText = Language._Object.getCaption(Me.dgcolhReason.Name, Me.dgcolhReason.HeaderText)
			Me.lblPaymentDate.Text = Language._Object.getCaption(Me.lblPaymentDate.Name, Me.lblPaymentDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Probation start date and end date are mandatory information. Please provide start date and end date.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Suspension start date and end date are mandatory information. Please provide start date and end date.")
            Language.setMessage(mstrModuleName, 5, "Sorry, end date cannot be less then start date.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Change Reason is mandatory information. Please select Change Reason to continue.")
			Language.setMessage("clsemployee_dates_tran", 7, "Sorry, This termination information is already present.")
            Language.setMessage(mstrModuleName, 8, "Sorry, Retirement date is mandatory information. Please provide retirement date.")
            Language.setMessage(mstrModuleName, 9, "Sorry, This is system generate entry. Please set effective date as employee appointment date.")
            Language.setMessage(mstrModuleName, 10, "cannot be less then effective date.")
            Language.setMessage(mstrModuleName, 11, "You cannot delete this transaction.Reason: Period is already closed for the selected date.")
            Language.setMessage(mstrModuleName, 12, "Do you want to add Benefit Coverage for selected employee ?")
			Language.setMessage(mstrModuleName, 13, "Exemption Date Information")
			Language.setMessage(mstrModuleName, 14, "Sorry, Exemption date is mandatory information. Please provide Exemption date.")
			Language.setMessage("clsemployee_dates_tran", 15, "Sorry, This confirmation information is already present.")
			Language.setMessage("clsemployee_dates_tran", 16, "Sorry, confirmation information is already present for the selected effective date.")
			Language.setMessage("clsemployee_dates_tran", 17, "Sorry, re-hire information is already present for the selected effective date.")
			Language.setMessage("clsemployee_dates_tran", 18, "Sorry, This re-hire information is already present.")
			Language.setMessage("clsemployee_dates_tran", 19, "Sorry, appointment information is already present for the selected effective date.")
			Language.setMessage("clsemployee_dates_tran", 20, "Sorry, birthdate information is already present for the selected effective date.")
			Language.setMessage("clsemployee_dates_tran", 21, "Sorry, first appointment date information is already present for the selected effective date.")
			Language.setMessage("clsemployee_dates_tran", 22, "Sorry, anniversary information is already present for the selected effective date.")
			Language.setMessage("clsemployee_dates_tran", 23, "Sorry, This appointment date information is already present.")
			Language.setMessage("clsemployee_dates_tran", 24, "Sorry, This birthdate date information is already present.")
			Language.setMessage("clsemployee_dates_tran", 25, "Sorry, This first appointment date information is already present.")
			Language.setMessage("clsemployee_dates_tran", 26, "Sorry, This anniversary information is already present.")
			Language.setMessage("clsemployee_dates_tran", 27, "Sorry, exemption information is already present for the selected effective date.")
			Language.setMessage("clsemployee_dates_tran", 28, "Sorry, This exemption information is already present.")
            Language.setMessage(mstrModuleName, 100, "Probation Date Information")
            Language.setMessage(mstrModuleName, 101, "From Date")
            Language.setMessage(mstrModuleName, 102, "To Date")
            Language.setMessage(mstrModuleName, 103, "Suspension Date Information")
            Language.setMessage(mstrModuleName, 104, "Termination Date Information")
            Language.setMessage(mstrModuleName, 105, "EOC Date")
            Language.setMessage(mstrModuleName, 106, "Leaving Date")
            Language.setMessage(mstrModuleName, 107, "Retirement Date Information")
            Language.setMessage(mstrModuleName, 108, "Retirement Date")
            Language.setMessage(mstrModuleName, 109, "Confirmation Date Information")
            Language.setMessage(mstrModuleName, 110, "Confirmation Date")
            Language.setMessage(mstrModuleName, 111, "Sorry, You cannot terminate this employee. Reason there are pending salary increment information present.")
            Language.setMessage(mstrModuleName, 112, "Sorry, You cannot retire this employee. Reason there are pending salary increment information present.")
            Language.setMessage(mstrModuleName, 113, "Sorry, you cannot exclude selected employee. Reason:Payroll Process already done for the employee for last date of current open period.")
			Language.setMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 140, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process.")
			Language.setMessage(mstrModuleName, 141, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process.")
            Language.setMessage(mstrModuleName, 142, "Exclude from payroll process is not set.")
            Language.setMessage(mstrModuleName, 143, "Are you sure you do not want to Exclude Payroll Process for this Employee?")
			Language.setMessage(mstrModuleName, 190, "Sorry, You can not perform any opration, Reason:This is auto generated entry from leave module.")
			Language.setMessage("clsemployee_dates_tran", 1, "Sorry, porbation information is already present for the selected effective date.")
			Language.setMessage("clsemployee_dates_tran", 2, "Sorry, suspension information is already present for the selected effective date.")
			Language.setMessage("clsemployee_dates_tran", 3, "Sorry, termination information is already present for the selected effective date.")
			Language.setMessage("clsemployee_dates_tran", 4, "Sorry, retirement information is already present for the selected effective date.")
			Language.setMessage("clsemployee_dates_tran", 5, "Sorry, This probation date range is already defined. Please define new date range for this probation.")
			Language.setMessage("clsemployee_dates_tran", 6, "Sorry, This suspension date range is already defined. Please define new date range for this suspension.")
			Language.setMessage("clsemployee_dates_tran", 8, "Sorry, This retirement information is already present.")
			Language.setMessage(mstrModuleName, 15, "Sorry, exemption Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 16, "Sorry, exemption Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 17, "Sorry, exemption Information is already present in approval process with selected allocation combination.")
			Language.setMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination.")
			Language.setMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date.")
			Language.setMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>



End Class