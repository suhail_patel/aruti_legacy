﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

#End Region

Public Class frmEmployeeRecategorize

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmEmployeeRecategorize"
    Private mAction As enAction = enAction.ADD_CONTINUE
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    Private mstrEmployeeCode As String = String.Empty
    Private mdtAppointmentDate As DateTime = Nothing

    Private objECategorize As New clsemployee_categorization_Tran
    Private mintEmployeeID As Integer = -1
    Private xCurrentAllocation As Dictionary(Of Integer, String)
    'S.SANDEEP [20-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}
    Private objACategorization As clsCategorization_Approval_Tran
    'S.SANDEEP [20-JUN-2018] -- END

    'Pinkal (10-May-2017) -- Start
    'Enhancement - Opening Wrong form (Employee Master) From Traning Registration Cancel List On Employee Recategorization .
    Private mintJobID As Integer = -1
    Private mblnCancel As Boolean = True
    Private mblnCloseAfterSave As Boolean = False
    'Pinkal (10-May-2017) -- End

    'S.SANDEEP [09-AUG-2018] -- START
    Private imgInfo As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.information)
    'S.SANDEEP [09-AUG-2018] -- END

    'S.SANDEEP [15-NOV-2018] -- START
    Private mintTransactionId As Integer = 0
    'S.SANDEEP [15-NOV-2018] -- END


    'S.SANDEEP |17-JAN-2019| -- START
    Private imgView As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.doc_view)
    'S.SANDEEP |17-JAN-2019| -- END

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    'Gajanan [11-Dec-2019] -- End

    'Gajanan [26-Dec-2019] -- Start   
    Dim mSize As Size
    'Gajanan [26-Dec-2019] -- End

#End Region


#Region "Property"

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeID = value
        End Set
    End Property

    'Pinkal (10-May-2017) -- Start
    'Enhancement - Opening Wrong form (Employee Master) From Traning Registration Cancel List On Employee Recategorization .

    Public ReadOnly Property _JobID() As Integer
        Get
            Return mintJobID
        End Get
    End Property

    'Pinkal (10-May-2017) -- End


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, ByVal xCloseAfterSave As Boolean, ByRef xJobID As Integer) As Boolean
        Try
            mAction = eAction
            mblnCloseAfterSave = xCloseAfterSave
            Me.ShowDialog()
            xJobID = mintJobID
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeRecategorize_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'S.SANDEEP [20-JUN-2018] -- START
        'ISSUE/ENHANCEMENT : {Ref#244}
        objACategorization = New clsCategorization_Approval_Tran
        'S.SANDEEP [20-JUN-2018] -- END

        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Set_Logo(Me, gApplicationType)
            FillCombo()
            If mintEmployeeID > 0 Then
                cboEmployee.SelectedValue = mintEmployeeID
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
            End If

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            'SetVisibility()
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End


            pnlData.Visible = False
            'S.SANDEEP [14 APR 2015] -- START
            objlblCaption.Visible = False
            'S.SANDEEP [14 APR 2015] -- END

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            lblPendingData.Visible = False
            'S.SANDEEP [20-JUN-2018] -- END

            AddHandler cboGrade.KeyDown, AddressOf ComboBox_KeyDown
            AddHandler cboGrade.KeyPress, AddressOf ComboBox_KeyPress
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeRecategorize_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_categorization_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_categorization_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetVisibility()
        Dim objJob As New clsJobs
        Try
            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            'pnlGrade.Visible = objJob.IsJobsByGrades()
            'Shani(18-JUN-2016) -- End

            'Shani (08-Dec-2016) -- Start
            'Enhancement -  Add Employee Allocaion/Date Privilage
            btnSave.Enabled = User._Object.Privilege._AllowToChangeEmpRecategorize
            'Shani (08-Dec-2016) -- End


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.

            'S.SANDEEP |17-JAN-2019| -- START
            'objdgcolhEdit.Visible = User._Object.Privilege._AllowToEditTransferEmployeeDetails
            If User._Object.Privilege._AllowToEditRecategorizeEmployeeDetails = False Then
                objdgcolhEdit.Image = imgBlank
            End If
            'S.SANDEEP |17-JAN-2019| -- END
            objdgcolhDelete.Visible = User._Object.Privilege._AllowToDeleteRecategorizeEmployeeDetails
            'Varsha Rana (17-Oct-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
            objJob = Nothing
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objJobGrp As New clsJobGroup
        Dim objGrade As New clsGrade
        Dim objGradeLvl As New clsGradeLevel
        Dim objCommon As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp Then
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, True)
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB

            'dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                        User._Object._Userunkid, _
            '                                        FinancialYear._Object._YearUnkid, _
            '                                        Company._Object._Companyunkid, _
            '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                        ConfigParameter._Object._UserAccessModeSetting, _
            '                                        True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , , True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            ''S.SANDEEP [04 JUN 2015] -- END
            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "employeename"
            '    .DataSource = dsCombos.Tables("Emp")
            '    .SelectedValue = 0
            '    .Text = ""
            'End With
            FillEmployeeCombo()
            'Gajanan [11-Dec-2019] -- End


            dsCombos = objJobGrp.getComboList("List", True)
            With cboJobGroup
                .ValueMember = "jobgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
                .Text = ""
            End With

            dsCombos = objGrade.getComboList("List", True)
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
                .Text = ""
            End With

            dsCombos = objGradeLvl.getComboList("List", True)
            With cboGradeLevel
                .ValueMember = "gradelevelunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
                .Text = ""
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RECATEGORIZE, True, "List")
            With cboChangeReason
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
                .Text = ""
            End With

            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            dsCombos = (New clsStation).getComboList("List", True)
            With cboStation
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            cboStation.SelectedValue = 0

            dsCombos = (New clsDepartmentGroup).getComboList("List", True)
            With cboDepartmentGrp
                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            cboDepartmentGrp.SelectedValue = 0

            dsCombos = (New clsDepartment).getComboList("List", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            cboDepartment.SelectedValue = 0

            dsCombos = (New clsSectionGroup).getComboList("List", True)
            With cboSectionGroup
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            cboSectionGroup.SelectedValue = 0

            dsCombos = (New clsSections).getComboList("Section", True)
            With cboSections
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Section")
                .SelectedValue = 0
            End With

            dsCombos = (New clsUnitGroup).getComboList("List", True)
            With cboUnitGroup
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            cboUnitGroup.SelectedValue = 0

            dsCombos = (New clsUnits).getComboList("Units", True)
            With cboUnits
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Units")
                .SelectedValue = 0
            End With

            dsCombos = (New clsTeams).getComboList("List", True)
            With cboTeams
                .ValueMember = "teamunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dsCombos = (New clsClassGroup).getComboList("List", True)
            With cboClassGroup
                .ValueMember = "classgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            cboClassGroup.SelectedValue = 0

            dsCombos = (New clsClass).getComboList("List", True)
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            cboClass.SelectedValue = 0

            dsCombos = objGrade.getComboList("Grade", True)
            With cboAllocationGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Grade")
                .SelectedValue = 0
            End With

            dsCombos = (New clsJobs).getComboList("List", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
                .Text = ""
            End With
            'Shani(18-JUN-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objCommon = Nothing
            objGrade = Nothing
            objEmployee = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub Set_ReCategorization()
        Try
            Dim dsCategorize As New DataSet
            dsCategorize = objECategorize.Get_Current_Job(Now.Date, CInt(cboEmployee.SelectedValue))
            If dsCategorize.Tables(0).Rows.Count > 0 Then

                'Shani(18-JUN-2016) -- Start
                'Enhancement : Add Allocation Column in Job master[Rutta]
                'cboJobGroup.SelectedValue = CInt(dsCategorize.Tables(0).Rows(0).Item("jobgroupunkid"))
                cboJob.SelectedValue = CInt(dsCategorize.Tables(0).Rows(0).Item("jobunkid"))
                'If CInt(dsCategorize.Tables(0).Rows(0).Item("gradeunkid")) > 0 Then
                '    cboGrade.SelectedValue = CInt(dsCategorize.Tables(0).Rows(0).Item("gradeunkid"))
                'Else
                '    cboGrade.Text = ""
                'End If
                'If CInt(dsCategorize.Tables(0).Rows(0).Item("gradelevelunkid")) > 0 Then
                '    cboGradeLevel.SelectedValue = CInt(dsCategorize.Tables(0).Rows(0).Item("gradelevelunkid"))
                'Else
                '    cboGradeLevel.Text = ""
                'End If
                'Shani(18-JUN-2016) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_ReCategorization", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsData As New DataSet
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End
            dsData = objECategorize.GetList("List", CInt(cboEmployee.SelectedValue))

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "tranguid"
                .DefaultValue = ""
            End With
            dsData.Tables(0).Columns.Add(dcol)

            '
            'S.SANDEEP |17-JAN-2019| -- START
            dcol = New DataColumn
            With dcol
                .DataType = GetType(System.Int32)
                .ColumnName = "operationtypeid"
                .DefaultValue = clsEmployeeMovmentApproval.enOperationType.ADDED
            End With
            dsData.Tables(0).Columns.Add(dcol)

            dcol = New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "OperationType"
                .DefaultValue = ""
            End With
            dsData.Tables(0).Columns.Add(dcol)
            'S.SANDEEP |17-JAN-2019| -- END


            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                Dim dsPending As New DataSet
                dsPending = objACategorization.GetList("List", CInt(cboEmployee.SelectedValue))
                If dsPending.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In dsPending.Tables(0).Rows
                        dsData.Tables(0).ImportRow(row)
                    Next
                End If
            End If
            'S.SANDEEP [20-JUN-2018] -- END


            dgvHistory.AutoGenerateColumns = False
            dgcolhChangeDate.DataPropertyName = "effectivedate"
            dgcolhJobGroup.DataPropertyName = "JobGroup"
            dgcolhJob.DataPropertyName = "Job"
            dgcolhReason.DataPropertyName = "Reason"
            objdgcolhrecategorizeunkid.DataPropertyName = "categorizationtranunkid"
            objdgcolhFromEmp.DataPropertyName = "isfromemployee"
            objdgcolhAppointdate.DataPropertyName = "adate"
            'S.SANDEEP [14 APR 2015] -- START
            objdgcolhrehiretranunkid.DataPropertyName = "rehiretranunkid"
            'S.SANDEEP [14 APR 2015] -- END

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            objdgcolhtranguid.DataPropertyName = "tranguid"
            'S.SANDEEP [20-JUN-2018] -- END

            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            'objcolhBranchUnkid.DataPropertyName = "jobbranchunkid"
            'objcolhDeprtmentGrpUnkid.DataPropertyName = "jobdepartmentgrpunkid"
            'objcolhDepartmentUnkid.DataPropertyName = "jobdepartmentunkid"
            'objcolhSectionGrpUnkid.DataPropertyName = "jobsectiongrpunkid"
            'objcolhSectionUnkid.DataPropertyName = "jobsectionunkid"
            'objcolhUnitGrpUnkid.DataPropertyName = "jobunitgrpunkid"
            'objcolhUnitUnkid.DataPropertyName = "jobunitunkid"
            'objcolhTeamUnkid.DataPropertyName = "teamunkid"
            'objcolhClassGrpUnkid.DataPropertyName = "jobclassgroupunkid"
            'objcolhClassUnkid.DataPropertyName = "jobclassunkid"
            'objcolhGradeUnkid.DataPropertyName = "jobgradeunkid"
            'Shani(18-JUN-2016) -- End

            'S.SANDEEP |17-JAN-2019| -- START
            objdgcolhOperationTypeId.DataPropertyName = "operationtypeid"
            objdgcolhOperationType.DataPropertyName = "OperationType"
            'S.SANDEEP |17-JAN-2019| -- END

            dgvHistory.DataSource = dsData.Tables(0)

            objdgcolhEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEdit.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhDelete.DefaultCellStyle.SelectionForeColor = Color.White

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If dtpEffectiveDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue."), enMsgBoxStyle.Information)
                dtpEffectiveDate.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Employee is mandatory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            'S.SANDEEP [16 Jan 2016] -- START
            'BASED ON RUTTA'S COMMNTS {While on register new employee job group is optional}
            'If CInt(cboJobGroup.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Job Group is mandatory information. Please selecte atleast Job Group to continue."), enMsgBoxStyle.Information)
            '    cboJobGroup.Focus()
            '    Return False
            'End If
            'S.SANDEEP [16 Jan 2016] -- END

            If CInt(cboJob.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Job is mandatory information. Please select atleast one Job to continue."), enMsgBoxStyle.Information)
                cboJob.Focus()
                Return False
            End If

            If CInt(cboChangeReason.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Change Reason is mandatory information. Please select Change Reason to continue."), enMsgBoxStyle.Information)
                cboChangeReason.Focus()
                Return False
            End If

            If mdtAppointmentDate <> Nothing Then
                If mdtAppointmentDate <> dtpEffectiveDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, This is system generate entry. Please set effective date as employee appointment date."), enMsgBoxStyle.Information)
                    dtpEffectiveDate.Focus()
                    Return False
                End If
            End If

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}



            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If objECategorize.isExist(dtpEffectiveDate.Value.Date, CInt(cboEmployee.SelectedValue), CInt(cboJobGroup.SelectedValue), CInt(cboJob.SelectedValue), -1, -1, 0) Then
                eZeeMsgBox.Show(Language.getMessage("clsemployee_categorization_Tran", 1, "Sorry, Re-Categorize information is already present for the selected effective date."), enMsgBoxStyle.Information)
                Return False
            End If
            'Gajanan [11-Dec-2019] -- End


            'S.SANDEEP [30-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#290}
            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                'S.SANDEEP [30-AUG-2018] -- END
                Dim strMsg As String = String.Empty
                Dim objPMovement As New clsEmployeeMovmentApproval
                'S.SANDEEP [18-SEP-2018] -- START                
                'strMsg = objPMovement.IsTransferCategorizationApprovers(clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1193, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, CInt(cboJobGroup.SelectedValue), CInt(cboJob.SelectedValue))
                strMsg = objPMovement.IsOtherMovmentApproverPresent(clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1193, User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, Nothing, CInt(cboEmployee.SelectedValue), Nothing)
                'S.SANDEEP [18-SEP-2018] -- END
                If strMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    objPMovement = Nothing
                    Return False
                End If
                objPMovement = Nothing

                'S.SANDEEP |16-JAN-2019| -- START
                If objECategorize.isExist(dtpEffectiveDate.Value.Date, CInt(cboEmployee.SelectedValue), -1, -1, -1, -1, mintTransactionId) Then 'S.SANDEEP |17-JAN-2019| -- START {mintTransactionId} -- END
                    eZeeMsgBox.Show(Language.getMessage("clsemployee_categorization_Tran", 1, "Sorry, Re-Categorize information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    Return False
                End If

                Dim dsList As New DataSet
                dsList = objECategorize.Get_Current_Job(dtpEffectiveDate.Value.Date, CInt(cboEmployee.SelectedValue))
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    If CInt(dsList.Tables(0).Rows(0)("jobunkid")) = CInt(cboJob.SelectedValue) AndAlso CInt(dsList.Tables(0).Rows(0)("jobgroupunkid")) = CInt(cboJobGroup.SelectedValue) Then
                        eZeeMsgBox.Show(Language.getMessage("clsemployee_categorization_Tran", 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                dsList = Nothing

                If objECategorize.isExist(dtpEffectiveDate.Value.Date, CInt(cboEmployee.SelectedValue), CInt(cboJobGroup.SelectedValue), CInt(cboJob.SelectedValue), -1, -1, mintTransactionId) Then 'S.SANDEEP |17-JAN-2019| -- START {mintTransactionId} -- END
                    eZeeMsgBox.Show(Language.getMessage("clsemployee_categorization_Tran", 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date."), enMsgBoxStyle.Information)
                    Return False
                End If
                'S.SANDEEP |16-JAN-2019| -- END

            End If
            'S.SANDEEP [20-JUN-2018] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub SetEditValue()
        Dim objJob As New clsJobs
        Try
            dtpEffectiveDate.Value = objECategorize._Effectivedate
            cboEmployee.SelectedValue = objECategorize._Employeeunkid
            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            'cboJobGroup.SelectedValue = objECategorize._JobGroupunkid
            'Shani(18-JUN-2016) -- End
            cboJob.SelectedValue = objECategorize._Jobunkid
            cboGrade.SelectedValue = objECategorize._Gradeunkid
            cboGradeLevel.SelectedValue = objECategorize._Gradelevelunkid
            cboChangeReason.SelectedValue = objECategorize._Changereasonunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetEditValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'objECategorize._Effectivedate = dtpEffectiveDate.Value
            'objCategorize._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'objCategorize._JobGroupunkid = CInt(cboJobGroup.SelectedValue)
            'objCategorize._Jobunkid = CInt(cboJob.SelectedValue)
            'objCategorize._Gradeunkid = CInt(cboGrade.SelectedValue)
            'objCategorize._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
            'objCategorize._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            'objCategorize._Isvoid = False
            'objCategorize._Statusunkid = 0
            'objCategorize._Userunkid = User._Object._Userunkid
            'objCategorize._Voiddatetime = Nothing
            'objCategorize._Voidreason = ""
            'objCategorize._Voiduserunkid = -1

            'S.SANDEEP [15-NOV-2018] -- START
            'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then

            'S.SANDEEP |17-JAN-2019| -- START
            'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Or mintTransactionId > 0 Then
            If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                'S.SANDEEP |17-JAN-2019| -- END

                objECategorize._Effectivedate = dtpEffectiveDate.Value
                objECategorize._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objECategorize._JobGroupunkid = CInt(cboJobGroup.SelectedValue)
                objECategorize._Jobunkid = CInt(cboJob.SelectedValue)
                objECategorize._Gradeunkid = CInt(cboGrade.SelectedValue)
                objECategorize._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
                objECategorize._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                objECategorize._Isvoid = False
                objECategorize._Statusunkid = 0
                objECategorize._Userunkid = User._Object._Userunkid
                objECategorize._Voiddatetime = Nothing
                objECategorize._Voidreason = ""
                objECategorize._Voiduserunkid = -1

                With objECategorize
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
                    ._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With


            Else
                'Gajanan [11-Dec-2019] -- Start   
                'Enhancement:Worked On December Cut Over Enhancement For NMB
                objACategorization = New clsCategorization_Approval_Tran
                'Gajanan [11-Dec-2019] -- End
                objACategorization._Audittype = enAuditType.ADD
                objACategorization._Audituserunkid = User._Object._Userunkid
                objACategorization._Effectivedate = dtpEffectiveDate.Value
                objACategorization._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objACategorization._Jobgroupunkid = CInt(cboJobGroup.SelectedValue)
                objACategorization._Jobunkid = CInt(cboJob.SelectedValue)
                objACategorization._Gradeunkid = CInt(cboGrade.SelectedValue)
                objACategorization._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
                objACategorization._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                objACategorization._Isvoid = False
                objACategorization._Statusunkid = 0
                objACategorization._Voiddatetime = Nothing
                objACategorization._Voidreason = ""
                objACategorization._Voiduserunkid = -1
                objACategorization._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                objACategorization._Tranguid = Guid.NewGuid.ToString()
                objACategorization._Transactiondate = Now
                objACategorization._Remark = ""
                objACategorization._Rehiretranunkid = 0
                objACategorization._Mappingunkid = 0
                objACategorization._Isweb = False
                objACategorization._Isfinal = False
                objACategorization._Ip = getIP()
                objACategorization._Hostname = getHostName()
                objACategorization._Form_Name = mstrModuleName

                'S.SANDEEP |17-JAN-2019| -- START
                If mAction = enAction.EDIT_ONE Then
                    objACategorization._Categorizationtranunkid = mintTransactionId
                    objACategorization._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    objACategorization._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                'S.SANDEEP |17-JAN-2019| -- END
            End If

            'S.SANDEEP [20-JUN-2018] -- END
            'Pinkal (10-May-2017) -- Start
            'Enhancement - Opening Wrong form (Employee Master) From Traning Registration Cancel List On Employee Recategorization .
            mintJobID = CInt(cboJob.SelectedValue)
            'Pinkal (10-May-2017) -- End
            'S.SANDEEP [15-NOV-2018] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            dtpEffectiveDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpEffectiveDate.Checked = False
            cboJobGroup.SelectedValue = 0
            cboJob.SelectedValue = 0
            cboGrade.SelectedValue = 0
            cboGradeLevel.SelectedValue = 0
            cboChangeReason.SelectedValue = 0
            pnlData.Visible = False
            SetVisibility()

            'Pinkal (10-May-2017) -- Start
            'Enhancement - Opening Wrong form (Employee Master) From Traning Registration Cancel List On Employee Recategorization .
            cboEmployee.SelectedValue = 0
            cboEmployee.Text = ""
            'Pinkal (10-May-2017) -- End

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            mintTransactionId = 0
            'Gajanan [11-Dec-2019] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearControls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetAllocationForMail()
        Try
            xCurrentAllocation = New Dictionary(Of Integer, String)

            '''''''''''' JOB GROUP
            If CInt(cboJobGroup.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.JOB_GROUP, cboJobGroup.Text)
            Else
                xCurrentAllocation.Add(enAllocation.JOB_GROUP, "&nbsp;")
            End If

            '''''''''''' JOB
            If CInt(cboJob.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.JOBS, cboJob.Text)
            Else
                xCurrentAllocation.Add(enAllocation.JOBS, "&nbsp;")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetAllocationForMail", mstrModuleName)
        Finally
        End Try
    End Sub

    'Shani(18-JUN-2016) -- Start
    'Enhancement : Add Allocation Column in Job master[Rutta]
    Private Sub ResetAllocation()
        Try
            cboStation.SelectedValue = 0
            cboDepartmentGrp.SelectedValue = 0
            cboDepartment.SelectedValue = 0
            cboSectionGroup.SelectedValue = 0
            cboSections.SelectedValue = 0
            cboUnitGroup.SelectedValue = 0
            cboUnits.SelectedValue = 0
            cboTeams.SelectedValue = 0
            cboClassGroup.SelectedValue = 0
            cboClass.SelectedValue = 0
            cboAllocationGrade.SelectedValue = 0
            cboJobGroup.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetAllocation", mstrModuleName)
        End Try
    End Sub
    'Shani(18-JUN-2016) -- End


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub FillEmployeeCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master

        Try
            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, , , , , , , , , , , , , , , mstrAdvanceFilter, True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            'S.SANDEEP [04 JUN 2015] -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
                .Text = ""
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "fillEmployeeCombo", mstrModuleName)
        Finally
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End


#End Region

#Region " Combobox Event(s) "

    Private Sub ComboBox_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Try
            e.Handled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ComboBox_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ComboBox_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs)
        Try
            If e.KeyCode = Keys.Right Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Home Or e.KeyCode = Keys.End Or e.KeyCode = Keys.Tab Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ComboBox_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (09-Apr-2015) -- Start
    'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA

    'Private Sub cboEmployee_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboEmployee.KeyDown
    '    Try
    '        If (e.KeyCode >= 65 AndAlso e.KeyCode <= 90) Or (e.KeyCode >= 97 AndAlso e.KeyCode <= 122) Or (e.KeyCode >= 47 AndAlso e.KeyCode <= 57) Then
    '            Dim frm As New frmCommonSearch
    '            With frm
    '                .ValueMember = cboEmployee.ValueMember
    '                .DisplayMember = cboEmployee.DisplayMember
    '                .DataSource = CType(cboEmployee.DataSource, DataTable)
    '                .CodeMember = "employeecode"
    '                .SelectedText = cboEmployee.Text
    '            End With
    '            If frm.DisplayDialog Then
    '                cboEmployee.SelectedValue = frm.SelectedValue
    '                If CInt(cboEmployee.SelectedValue) <= 0 Then cboEmployee.Text = ""
    '            Else
    '                cboEmployee.Text = ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (09-Apr-2015) -- End

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim xtmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                If xtmp.Length > 0 Then
                    mstrEmployeeCode = CStr(xtmp(0).Item("employeecode"))
                End If
                'S.SANDEEP [14 APR 2015] -- START
                objlblCaption.Visible = False
                'S.SANDEEP [14 APR 2015] -- END

                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : STOP ENTRY POSTING FOR TERMINATED EMPLOYEE
                If btnSave.Enabled = False Then btnSave.Enabled = True
                If objdgcolhEdit.Visible = False Then objdgcolhEdit.Visible = True
                If objdgcolhDelete.Visible = False Then objdgcolhDelete.Visible = True
                'S.SANDEEP [04-AUG-2017] -- END

                'S.SANDEEP [07-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001988}
                If mAction = enAction.EDIT_ONE Then
                    If objECategorize._Categorizationtranunkid > 0 Then objECategorize._Categorizationtranunkid = 0
                    mAction = enAction.ADD_CONTINUE
                End If
                'S.SANDEEP [07-Feb-2018] -- END

                'Shani(18-JUN-2016) -- Start
                'Enhancement : Add Allocation Column in Job master[Rutta]
                Call ResetAllocation()
                'Shani(18-JUN-2016) -- End
                Call Fill_Grid()
                Set_ReCategorization()

                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : STOP ENTRY POSTING FOR TERMINATED EMPLOYEE
                If ConfigParameter._Object._IsIncludeInactiveEmp Then
                    Dim objEmp As New clsEmployee_Master
                    objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date) = CInt(cboEmployee.SelectedValue)
                    If objEmp._Empl_Enddate <> Nothing Then
                        If objEmp._Empl_Enddate <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    If objEmp._Termination_From_Date <> Nothing Then
                        If objEmp._Termination_From_Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    If objEmp._Termination_To_Date <> Nothing Then
                        If objEmp._Termination_To_Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                            btnSave.Enabled = False
                            objdgcolhEdit.Visible = False
                            objdgcolhDelete.Visible = False
                        End If
                    End If
                    objEmp = Nothing
                End If
                'S.SANDEEP [04-AUG-2017] -- END
            Else
                dgvHistory.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub


    'Shani(18-JUN-2016) -- Start
    'Enhancement : Add Allocation Column in Job master[Rutta]
    'Private Sub cboJobGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJobGroup.SelectedIndexChanged
    Private Sub cboJobGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJobGroup.SelectedIndexChanged, _
                                                                                                                     cboStation.SelectedIndexChanged, _
                                                                                                                     cboDepartmentGrp.SelectedIndexChanged, _
                                                                                                                     cboDepartment.SelectedIndexChanged, _
                                                                                                                     cboSectionGroup.SelectedIndexChanged, _
                                                                                                                     cboSections.SelectedIndexChanged, _
                                                                                                                     cboUnitGroup.SelectedIndexChanged, _
                                                                                                                     cboUnits.SelectedIndexChanged, _
                                                                                                                     cboTeams.SelectedIndexChanged, _
                                                                                                                     cboClassGroup.SelectedIndexChanged, _
                                                                                                                     cboClass.SelectedIndexChanged, _
                                                                                                                     cboAllocationGrade.SelectedIndexChanged
        'Shani(18-JUN-2016) -- End


        'Shani(18-JUN-2016) -- Start
        'Enhancement : Add Allocation Column in Job master[Rutta]
        'Dim objJob As New clsJobs
        'Shani(18-JUN-2016) -- End

        Dim dsCombos As New DataSet
        Try

            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            'dsCombos = objJob.getComboList("List", True, CInt(cboJobGroup.SelectedValue))
            dsCombos = objECategorize.getJobComboList("List", True, _
                                                     CInt(cboStation.SelectedValue), _
                                                     CInt(cboDepartmentGrp.SelectedValue), _
                                                     CInt(cboDepartment.SelectedValue), _
                                                     CInt(cboSectionGroup.SelectedValue), _
                                                     CInt(cboSections.SelectedValue), _
                                                     CInt(cboUnitGroup.SelectedValue), _
                                                     CInt(cboUnits.SelectedValue), _
                                                     CInt(cboTeams.SelectedValue), _
                                                     CInt(cboClassGroup.SelectedValue), _
                                                     CInt(cboClass.SelectedValue), _
                                                     CInt(cboAllocationGrade.SelectedValue), , _
                                                     CInt(cboJobGroup.SelectedValue))
            'Shani(18-JUN-2016) -- End

            'RemoveHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
            'Shani (10-Nov-2016) -- Start
            Dim intProvJobUnkId As Integer = CInt(cboJob.SelectedValue)
            'Shani (10-Nov-2016) -- End
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                'Shani (10-Nov-2016) -- Start
                '.SelectedValue = 0
                Try
                    .SelectedValue = intProvJobUnkId
                Catch ex As Exception
                    .SelectedValue = 0
                End Try
                If .SelectedValue Is Nothing Then
                    .SelectedValue = 0
                End If
                'Shani (10-Nov-2016) -- End
            End With
            'AddHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJobGroup_SelectedIndexChanged", mstrModuleName)
        Finally
            dsCombos = Nothing
        End Try
    End Sub

    Private Sub cboJob_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJob.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objJob As New clsJobs
        Try
            ''Shani(18-JUN-2016) -- Start
            'RemoveHandler cboStation.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'RemoveHandler cboDepartmentGrp.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'RemoveHandler cboDepartment.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'RemoveHandler cboSectionGroup.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'RemoveHandler cboSections.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'RemoveHandler cboUnitGroup.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'RemoveHandler cboUnits.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'RemoveHandler cboTeams.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'RemoveHandler cboClassGroup.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'RemoveHandler cboClass.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'RemoveHandler cboAllocationGrade.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'RemoveHandler cboJobGroup.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            ''Shani(18-JUN-2016) -- End
            If CInt(cboJob.SelectedValue) > 0 Then
                dsCombos = objJob.GetJobsByGrades(CInt(cboJob.SelectedValue))
                If dsCombos IsNot Nothing AndAlso dsCombos.Tables(0).Rows.Count > 0 Then
                    Dim dtTable As DataTable = New DataView(CType(cboGrade.DataSource, DataTable), "gradeunkid = " & CInt(dsCombos.Tables(0).Rows(0)("jobgradeunkid")), "", DataViewRowState.CurrentRows).ToTable
                    If dtTable.Rows.Count > 0 Then
                        cboGrade.SelectedValue = CInt(dtTable.Rows(0)("gradeunkid"))
                    Else
                        cboGrade.SelectedValue = 0
                        cboGrade.Text = ""
                    End If
                Else
                    cboGrade.SelectedValue = 0
                    cboGrade.Text = ""
                End If

                ''Shani(18-JUN-2016) -- Start
                ''Enhancement : Add Allocation Column in Job master[Rutta]
                'objJob._Jobunkid = CInt(cboJob.SelectedValue)
                'cboStation.SelectedValue = objJob._JobBranchUnkid
                'cboDepartmentGrp.SelectedValue = objJob._DepartmentGrpUnkId
                'cboDepartment.SelectedValue = objJob._JobDepartmentunkid
                'cboSectionGroup.SelectedValue = objJob._SectionGrpUnkId
                'cboSections.SelectedValue = objJob._Jobsectionunkid
                'cboUnitGroup.SelectedValue = objJob._UnitGrpUnkId
                'cboUnits.SelectedValue = objJob._Jobunitunkid
                'cboTeams.SelectedValue = objJob._Teamunkid
                'cboClassGroup.SelectedValue = objJob._JobClassGroupunkid
                'cboClass.SelectedValue = objJob._ClassUnkid
                'cboAllocationGrade.SelectedValue = objJob._Jobgradeunkid
                'cboJobGroup.SelectedValue = objJob._Jobgroupunkid
                ''Shani(18-JUN-2016) -- End


            Else
                If cboGrade.DataSource IsNot Nothing Then
                    cboGrade.SelectedValue = 0
                    cboGrade.Text = ""
                End If
                ''Shani(18-JUN-2016) -- Start
                ''Enhancement : Add Allocation Column in Job master[Rutta]
                'cboStation.SelectedValue = 0
                'cboDepartmentGrp.SelectedValue = 0
                'cboDepartment.SelectedValue = 0
                'cboSectionGroup.SelectedValue = 0
                'cboSections.SelectedValue = 0
                'cboUnitGroup.SelectedValue = 0
                'cboUnits.SelectedValue = 0
                'cboTeams.SelectedValue = 0
                'cboClassGroup.SelectedValue = 0
                'cboClass.SelectedValue = 0
                'cboAllocationGrade.SelectedValue = 0
                'cboJobGroup.SelectedValue = 0
                ''Shani(18-JUN-2016) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJob_SelectedIndexChanged", mstrModuleName)
        Finally
            objJob = Nothing
            dsCombos.Dispose()
            ''Shani(18-JUN-2016) -- Start  
            'AddHandler cboStation.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'AddHandler cboDepartmentGrp.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'AddHandler cboDepartment.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'AddHandler cboSectionGroup.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'AddHandler cboSections.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'AddHandler cboUnitGroup.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'AddHandler cboUnits.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'AddHandler cboTeams.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'AddHandler cboClassGroup.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'AddHandler cboClass.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'AddHandler cboAllocationGrade.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            'AddHandler cboJobGroup.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            ''Shani(18-JUN-2016) -- End  
        End Try
    End Sub

#End Region

#Region " Button's Events "

    'Shani(18-JUN-2016) -- Start
    'Enhancement : Add Allocation Column in Job master[Rutta]
    'Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objbtnSearchJob.Click, objSearchReason.Click, objbtnSearchJobGrp.Click
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objbtnSearchJob.Click, objSearchReason.Click, objbtnSearchJobGrp.Click, objbtnSearchBranch.Click, objbtnSearchDeptGrp.Click, objbtnSearchDepartment.Click, objbtnSearchSecGroup.Click, objbtnSearchSection.Click, objbtnSearchUnitGrp.Click, objbtnSearchUnits.Click, objbtnSearchTeam.Click, objbtnSearchClassGrp.Click, objbtnSearchClass.Click, objbtnSearchGrade.Click
        'Shani(18-JUN-2016) -- End
        Dim frm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim xCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchEmployee.Name.ToUpper
                    xCbo = cboEmployee
                Case objbtnSearchJobGrp.Name.ToUpper
                    xCbo = cboJobGroup
                Case objbtnSearchJob.Name.ToUpper
                    xCbo = cboJob
                Case objSearchReason.Name.ToUpper
                    xCbo = cboChangeReason

                    'Shani(18-JUN-2016) -- Start
                    'Enhancement : Add Allocation Column in Job master[Rutta]
                Case objbtnSearchBranch.Name.ToUpper
                    xCbo = cboStation
                Case objbtnSearchDeptGrp.Name.ToUpper
                    xCbo = cboDepartmentGrp
                Case objbtnSearchDepartment.Name.ToUpper
                    xCbo = cboDepartment
                Case objbtnSearchSecGroup.Name.ToUpper
                    xCbo = cboSectionGroup
                Case objbtnSearchSection.Name.ToUpper
                    xCbo = cboSections
                Case objbtnSearchUnitGrp.Name.ToUpper
                    xCbo = cboUnitGroup
                Case objbtnSearchUnits.Name.ToUpper
                    xCbo = cboUnits
                Case objbtnSearchTeam.Name.ToUpper
                    xCbo = cboTeams
                Case objbtnSearchClass.Name.ToUpper
                    xCbo = cboClassGroup
                Case objbtnSearchClass.Name.ToUpper
                    xCbo = cboClass
                Case objbtnSearchGrade.Name.ToUpper
                    xCbo = cboAllocationGrade
                    'Shani(18-JUN-2016) -- End
            End Select

            If xCbo.DataSource Is Nothing Then Exit Sub

            With frm
                .ValueMember = xCbo.ValueMember
                .DisplayMember = xCbo.DisplayMember
                If xCbo.Name = cboEmployee.Name Then
                    .CodeMember = "employeecode"
                End If
                .DataSource = CType(xCbo.DataSource, DataTable)
                If .DisplayDialog = True Then
                    xCbo.SelectedValue = .SelectedValue
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.RECATEGORIZE, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RECATEGORIZE, True, "List")
                With cboChangeReason
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then Exit Sub
            Call SetValue()

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            Dim objfrmMovementMigration As New frmMovementMigration
            Dim DtMigration As DataTable = Nothing
            'Gajanan [23-SEP-2019] -- End


            'Gajanan [26-OCT-2019] -- Start    
            'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
            Dim mblnApplyApprovalMigration As Boolean = ConfigParameter._Object._ApplyMigrationEnforcement
            'Gajanan [26-OCT-2019] -- End


            If mAction = enAction.EDIT_ONE Then
                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
                '    blnFlag = objCategorize.Update()
                'Else
                '    blnFlag = objCategorize.Insert()

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'blnFlag = objCategorize.Update(Company._Object._Companyunkid)


                'S.SANDEEP |17-JAN-2019| -- START


                'blnFlag = objCategorize.Update(Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst)

                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then

                    'Gajanan [23-SEP-2019] -- Start    
                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    'blnFlag = objECategorize.Update(Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst)


                    'Gajanan [26-OCT-2019] -- Start    
                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  

                    'If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.EDITED, False, CInt(cboEmployee.SelectedValue)) Then
                    '    blnFlag = objECategorize.Update(Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, DtMigration, False, False)
                    'End If


                    If ConfigParameter._Object._ApplyMigrationEnforcement Then
                    If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.EDITED, False, CInt(cboEmployee.SelectedValue)) Then

                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                            'blnFlag = objECategorize.Update(Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, DtMigration, False, False)
                            blnFlag = objECategorize.Update(Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName, Nothing, DtMigration, False, False)
                            'Pinkal (12-Oct-2020) -- End
                    End If
                    Else
                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                        'blnFlag = objECategorize.Update(Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, DtMigration, False, False)
                        blnFlag = objECategorize.Update(Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName, Nothing, DtMigration, False, False)
                        'Pinkal (12-Oct-2020) -- End
                    End If
                    'Gajanan [26-OCT-2019] -- End

                    'Gajanan [23-SEP-2019] -- End

                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    'Gajanan [23-SEP-2019] -- Start    
                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    'blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED)


                    'Gajanan [26-OCT-2019] -- Start    
                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  

                    'If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.EDITED, True, CInt(cboEmployee.SelectedValue)) Then
                    '    blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, Nothing, False, DtMigration, True)
                    'End If

                    If ConfigParameter._Object._ApplyMigrationEnforcement Then
                    If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.EDITED, True, CInt(cboEmployee.SelectedValue)) Then


                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                            'blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED _
                            '                                                  , ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, False, DtMigration, True)

                            blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED _
                                                                            , ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName, Nothing, False, DtMigration, True)

                            'Pinkal (12-Oct-2020) -- End
                    End If
                    Else

                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                        'blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED _
                        '                                                  , ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, False, DtMigration, True)

                        blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED _
                                                                          , ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName _
                                                                          , Nothing, False, DtMigration, True)

                        'Pinkal (12-Oct-2020) -- End

                    End If
                    'Gajanan [26-OCT-2019] -- End

                    'Gajanan [23-SEP-2019] -- End

                    If blnFlag = False AndAlso objACategorization._Message <> "" Then
                        eZeeMsgBox.Show(objACategorization._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END

                'Pinkal (18-Aug-2018) -- End

            Else

                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'blnFlag = objETransfers.Insert(Company._Object._Companyunkid)
                ''Pinkal (21-Dec-2015) -- Start
                ''Enhancement - Working on Employee Benefit changes given By Rutta.
                'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Do you want to add Benefit Coverage for selected employee ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                '    Dim objFrm As New frmEmployeeBenefitCoverage
                '    objFrm.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue))
                'End If
                ''Pinkal (21-Dec-2015) -- End
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'blnFlag = objECategorize.Insert(Company._Object._Companyunkid)

                    'Gajanan [23-SEP-2019] -- Start    
                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    'blnFlag = objECategorize.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid)

                    'Gajanan [26-OCT-2019] -- Start    
                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                    'If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.ADDED, False, CInt(cboEmployee.SelectedValue)) Then
                    '    blnFlag = objECategorize.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid)
                    'End If

                    If ConfigParameter._Object._ApplyMigrationEnforcement Then
                    If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.ADDED, False, CInt(cboEmployee.SelectedValue)) Then

                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                            'blnFlag = objECategorize.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, Nothing, DtMigration)
                            blnFlag = objECategorize.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, Nothing, DtMigration)
                            'Pinkal (12-Oct-2020) -- End
                    End If
                    Else
                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                        'blnFlag = objECategorize.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid)
                        blnFlag = objECategorize.Insert(ConfigParameter._Object._CreateADUserFromEmpMst, Company._Object._Companyunkid, FinancialYear._Object._DatabaseName)
                        'Pinkal (12-Oct-2020) -- End
                    End If
                    'Gajanan [26-OCT-2019] -- End


                    'Gajanan [23-SEP-2019] -- End


                    'Pinkal (18-Aug-2018) -- End

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Do you want to add Benefit Coverage for selected employee ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmEmployeeBenefitCoverage
                        objFrm.displayDialog(-1, enAction.ADD_ONE, CInt(cboEmployee.SelectedValue))
                    End If

                Else

                    'S.SANDEEP |17-JAN-2019| -- START

                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 110, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 110, "Sorry, Information is already present in approval process with selected date and allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Sorry, Information is already present in approval process with selected date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Sorry, Information is already present in approval process with selected allocation combination."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END


                    'Gajanan [23-SEP-2019] -- Start    
                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    'blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED)

                    'Gajanan [26-OCT-2019] -- Start    
                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  

                    'If objfrmMovementMigration.displayDialog(DtMigration, 0, clsEmployeeMovmentApproval.enOperationType.ADDED, False, CInt(cboEmployee.SelectedValue)) Then
                    '    blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, Nothing, False, DtMigration, True)
                    'End If

                    If ConfigParameter._Object._ApplyMigrationEnforcement Then
                    If objfrmMovementMigration.displayDialog(DtMigration, 0, clsEmployeeMovmentApproval.enOperationType.ADDED, False, CInt(cboEmployee.SelectedValue)) Then

                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                            'blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED _
                            '                                                  , ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, False, DtMigration, True)

                            blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED _
                                                                            , ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName _
                                                                            , Nothing, False, DtMigration, True)


                            'Pinkal (12-Oct-2020) -- End

                    End If
                    Else

                        'Pinkal (12-Oct-2020) -- Start
                        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                        'blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED _
                        '                                                    , ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, False, DtMigration, True)

                        blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.ADDED _
                                                                          , ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName _
                                                                          , Nothing, False, DtMigration, True)

                        'Pinkal (12-Oct-2020) -- End

                    End If

                    'Gajanan [26-OCT-2019] -- End

                    'Gajanan [23-SEP-2019] -- End

                    If blnFlag = False AndAlso objACategorization._Message <> "" Then
                        eZeeMsgBox.Show(objACategorization._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                End If
                'S.SANDEEP [20-JUN-2018] -- END

                'S.SANDEEP [10-MAY-2017] -- END
            End If
            If blnFlag = False AndAlso objECategorize._Message <> "" Then
                eZeeMsgBox.Show(objECategorize._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                If blnFlag Then
                If mAction = enAction.EDIT_ONE Then mAction = enAction.ADD_CONTINUE
                'Shani(18-JUN-2016) -- Start
                'Enhancement : Add Allocation Column in Job master[Rutta]
                Call ResetAllocation()
                'Shani(18-JUN-2016) -- End
                Call Fill_Grid()

                'Call SetAllocationForMail()
                ''Sohail (30 Nov 2017) -- Start
                ''SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                ''Call objCategorize.SendEmails(CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.Text, xCurrentAllocation, ConfigParameter._Object._Notify_Allocation, dtpEffectiveDate.Value.Date)
                'Call objCategorize.SendEmails(CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.Text, xCurrentAllocation, ConfigParameter._Object._Notify_Allocation, dtpEffectiveDate.Value.Date, Company._Object._Companyunkid)
                ''Sohail (30 Nov 2017) -- End
                'Call Set_ReCategorization()

                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'S.SANDEEP [15-NOV-2018] -- START

                'S.SANDEEP |17-JAN-2019| -- START
                If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
                    'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Or mintTransactionId > 0 Then
                    'S.SANDEEP |17-JAN-2019| -- END

                    With objECategorize
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
                        ._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With

                    'S.SANDEEP [15-NOV-2018] -- END
                Call SetAllocationForMail()
                Call objECategorize.SendEmails(CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.Text, xCurrentAllocation, ConfigParameter._Object._Notify_Allocation, dtpEffectiveDate.Value.Date, Company._Object._Companyunkid)
                Else
                    Dim objPMovement As New clsEmployeeMovmentApproval
                    'S.SANDEEP |17-JAN-2019| -- START
                    'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1193, clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, Nothing, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                    If mAction = enAction.EDIT_ONE Then
                        eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                    Else
                        eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                    End If
                    objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, 1193, clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, eOperType, False, Nothing, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    'S.SANDEEP |17-JAN-2019| -- END
                    objPMovement = Nothing
                End If
                Call Set_ReCategorization()

                'S.SANDEEP [20-JUN-2018] -- END

                'Pinkal (10-May-2017) -- Start
                'Enhancement - Opening Wrong form (Employee Master) From Traning Registration Cancel List On Employee Recategorization .
                If mblnCloseAfterSave Then
                    mblnCancel = False
                    Me.Close()
            End If
                'Pinkal (10-May-2017) -- End
            End If
            End If

            Call ClearControls()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            'S.SANDEEP [15-NOV-2018] -- START
            'S.SANDEEP |17-JAN-2019| -- START
            'mintTransactionId = 0
            'S.SANDEEP |17-JAN-2019| -- END
            'S.SANDEEP [15-NOV-2018] -- END
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString

            FillEmployeeCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End

    'Gajanan [26-Dec-2019] -- Start   
    Private Sub gbFilterCriteria_HeaderClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbFilterCriteria.HeaderClick
        Try
            If gbFilterCriteria.Collapsed = True Then
                Me.Size = New Size(Me.Width, (Me.Height - (gbFilterCriteria.OpenHeight)) + gbFilterCriteria.HeaderHeight)
            Else
                Me.Size = New Size(Me.Width, (Me.Height + (gbFilterCriteria.OpenHeight)) - gbFilterCriteria.HeaderHeight)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbFilterCriteria_HeaderClick", mstrModuleName)
        End Try
    End Sub
    'Gajanan [26-Dec-2019] -- End
#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvHistory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHistory.CellClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            mdtAppointmentDate = Nothing
            txtDate.Text = ""
            pnlData.Visible = False
            Select Case e.ColumnIndex
                'S.SANDEEP [09-AUG-2018] -- START
                Case objdgcolhViewPending.Index
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhViewPending.Index).Value Is imgInfo Then
                        Dim frm As New frmViewMovementApproval
                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        'S.SANDEEP |17-JAN-2019| -- START
                        'frm.displayDialog(User._Object._Userunkid, 0, 1193, clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, Nothing, False, "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue), False)
                        Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                        If mAction = enAction.EDIT_ONE Then
                            eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                        Else
                            eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                        End If
                        frm.displayDialog(User._Object._Userunkid, 0, 1193, clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, Nothing, False, eOperType, "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue), False)
                        'S.SANDEEP |17-JAN-2019| -- END

                        If frm IsNot Nothing Then frm.Dispose()
                        Exit Sub
                    End If
                    'S.SANDEEP [09-AUG-2018] -- END

                Case objdgcolhEdit.Index
                    'S.SANDEEP |17-JAN-2019| -- START
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgView Then
                        If CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhrecategorizeunkid.Index).Value) > 0 Then
                            Dim dr As DataRow() = CType(dgvHistory.DataSource, DataTable).Select(objdgcolhrecategorizeunkid.DataPropertyName & " = " & CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhrecategorizeunkid.Index).Value) & " AND " & objdgcolhtranguid.DataPropertyName & "= ''")
                            If dr.Length > 0 Then
                                Dim index As Integer = CType(dgvHistory.DataSource, DataTable).Rows.IndexOf(dr(0))
                                dgvHistory.Rows(index).Selected = True
                                dgvHistory.FirstDisplayedScrollingRowIndex = index
                            End If
                        End If
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END


                    mAction = enAction.EDIT_ONE
                    'S.SANDEEP [14 APR 2015] -- START
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgBlank Then Exit Sub
                    'S.SANDEEP [14 APR 2015] -- END
                    objECategorize._Categorizationtranunkid = CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhrecategorizeunkid.Index).Value)
                    'Shani(18-JUN-2016) -- Start
                    'Enhancement : Add Allocation Column in Job master[Rutta]

                    'S.SANDEEP [15-NOV-2018] -- START
                    mintTransactionId = objECategorize._Categorizationtranunkid
                    'S.SANDEEP [15-NOV-2018] -- END

                    Call ResetAllocation()
                    'Shani(18-JUN-2016) -- End


                    'S.SANDEEP |17-JAN-2019| -- START
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        Dim Flag As Boolean = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransactionId)
                        If Flag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 113, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'S.SANDEEP |17-JAN-2019| -- END
                    End If

                    Call SetEditValue()
                    If CBool(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhFromEmp.Index).Value) = True Then
                        mdtAppointmentDate = eZeeDate.convertDate(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhAppointdate.Index).Value.ToString)
                        txtDate.Text = mdtAppointmentDate.Date.ToShortDateString
                        pnlData.Visible = True
                    End If
                Case objdgcolhDelete.Index
                    mAction = enAction.ADD_ONE
                    If dgvHistory.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub

                    'S.SANDEEP |17-JAN-2019| -- START
                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        Dim Flag As Boolean = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhrecategorizeunkid.Index).Value))
                        If Flag Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 114, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END

                    Dim xStrVoidReason As String = String.Empty
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    frm.displayDialog(enVoidCategoryType.EMPLOYEE, xStrVoidReason)
                    If xStrVoidReason.Trim.Length <= 0 Then Exit Sub
                    objECategorize._Isvoid = True
                    objECategorize._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objECategorize._Voidreason = xStrVoidReason
                    objECategorize._Voiduserunkid = User._Object._Userunkid
                    objECategorize._Userunkid = User._Object._Userunkid

                    With objECategorize
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
                        ._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With

                    'S.SANDEEP [10-MAY-2017] -- START
                    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If objCategorize.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhrecategorizeunkid.Index).Value), Company._Object._Companyunkid) = False Then
                    'If objCategorize.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhrecategorizeunkid.Index).Value), Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst) = False Then
                    '    'Pinkal (18-Aug-2018) -- End
                    '    'If objCategorize.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhrecategorizeunkid.Index).Value)) = False Then
                    '    'S.SANDEEP [10-MAY-2017] -- END
                    '    If objCategorize._Message <> "" Then
                    '        eZeeMsgBox.Show(objCategorize._Message, enMsgBoxStyle.Information)
                    '    End If
                    '    Exit Sub
                    'End If

                    'Gajanan [23-SEP-2019] -- Start    
                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    Dim objfrmMovementMigration As New frmMovementMigration
                    Dim DtMigration As DataTable = Nothing
                    'Gajanan [23-SEP-2019] -- End

                    If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow = False Then
                        objACategorization._Isvoid = False
                        objACategorization._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objACategorization._Voidreason = xStrVoidReason
                        objACategorization._Voiduserunkid = -1
                        objACategorization._Audituserunkid = User._Object._Userunkid
                        objACategorization._Isweb = False
                        objACategorization._Ip = getIP()
                        objACategorization._Hostname = getHostName()
                        objACategorization._Form_Name = mstrModuleName
                        objACategorization._Audituserunkid = User._Object._Userunkid


                        'Gajanan [23-SEP-2019] -- Start    
                        'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                        If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.DELETED, False, CInt(cboEmployee.SelectedValue)) Then
                            'Gajanan [23-SEP-2019] -- End

                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                            'If objACategorization.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhrecategorizeunkid.Index).Value), clsEmployeeMovmentApproval.enOperationType.DELETED, Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, DtMigration, True, False) = False Then
                            If objACategorization.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhrecategorizeunkid.Index).Value), clsEmployeeMovmentApproval.enOperationType.DELETED _
                                                                    , Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, FinancialYear._Object._DatabaseName _
                                                                    , Nothing, DtMigration, True, False) = False Then
                                'Pinkal (12-Oct-2020) -- End


                            If objACategorization._Message <> "" Then
                                eZeeMsgBox.Show(objACategorization._Message, enMsgBoxStyle.Information)
                            End If
                            Exit Sub
                        End If
                        End If

                    Else

                        'Gajanan [23-SEP-2019] -- Start    
                        'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                        If objfrmMovementMigration.displayDialog(DtMigration, mintTransactionId, clsEmployeeMovmentApproval.enOperationType.DELETED, False, CInt(cboEmployee.SelectedValue)) Then
                            'If objECategorize.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhrecategorizeunkid.Index).Value), Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst) = False Then
                            'Gajanan [23-SEP-2019] -- End

                            'Pinkal (12-Oct-2020) -- Start
                            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                            'If objECategorize.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhrecategorizeunkid.Index).Value), Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst, Nothing, DtMigration, False, False) = False Then
                            If objECategorize.Delete(CInt(dgvHistory.Rows(e.RowIndex).Cells(objdgcolhrecategorizeunkid.Index).Value), Company._Object._Companyunkid, ConfigParameter._Object._CreateADUserFromEmpMst _
                                                                , FinancialYear._Object._DatabaseName, Nothing, DtMigration, False, False) = False Then
                                'Pinkal (12-Oct-2020) -- End


                            If objECategorize._Message <> "" Then
                                eZeeMsgBox.Show(objECategorize._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If
                    End If

                    End If

                    'Shani(18-JUN-2016) -- Start
                    'Enhancement : Add Allocation Column in Job master[Rutta]
                    Call ResetAllocation()
                    'Shani(18-JUN-2016) -- End
                    Call Fill_Grid()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_CellClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvHistory_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvHistory.DataBindingComplete
        Try
            For Each xdgvr As DataGridViewRow In dgvHistory.Rows

                'S.SANDEEP [20-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'If CBool(xdgvr.Cells(objdgcolhFromEmp.Index).Value) = True Then
                '    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                'End If
                If CStr(xdgvr.Cells(objdgcolhtranguid.Index).Value).Trim.Length > 0 Then
                    xdgvr.DefaultCellStyle.BackColor = Color.PowderBlue
                    xdgvr.DefaultCellStyle.ForeColor = Color.Black
                    xdgvr.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                    'S.SANDEEP [09-AUG-2018] -- START
                    xdgvr.Cells(objdgcolhViewPending.Index).Value = imgInfo
                    'S.SANDEEP [09-AUG-2018] -- END
                    lblPendingData.Visible = True

                    'S.SANDEEP |17-JAN-2019| -- START
                    xdgvr.Cells(objdgcolhViewPending.Index).ToolTipText = xdgvr.Cells(objdgcolhOperationType.Index).Value.ToString
                    If CInt(xdgvr.Cells(objdgcolhOperationTypeId.Index).Value) = clsEmployeeMovmentApproval.enOperationType.EDITED Then
                        xdgvr.Cells(objdgcolhEdit.Index).Value = imgView
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END


                Else
                If CBool(xdgvr.Cells(objdgcolhFromEmp.Index).Value) = True Then
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                End If
                End If
                'S.SANDEEP [20-JUN-2018] -- END



                'S.SANDEEP [14 APR 2015] -- START
                If CInt(xdgvr.Cells(objdgcolhrehiretranunkid.Index).Value) > 0 Then
                    For xCellIdx As Integer = 0 To xdgvr.Cells.Count - 1
                        If xCellIdx > 1 Then
                            xdgvr.Cells(xCellIdx).Style.BackColor = Color.Orange
                            xdgvr.Cells(xCellIdx).Style.ForeColor = Color.Black
                        End If
                    Next
                    xdgvr.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                    xdgvr.Cells(objdgcolhEdit.Index).Value = imgBlank
                    objlblCaption.Visible = True
                End If
                'S.SANDEEP [14 APR 2015] -- END
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHistory_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbHeadInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbHeadInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbHeadInformation.Text = Language._Object.getCaption(Me.gbHeadInformation.Name, Me.gbHeadInformation.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblChangeReason.Text = Language._Object.getCaption(Me.lblChangeReason.Name, Me.lblChangeReason.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.lblAppointmentdate.Text = Language._Object.getCaption(Me.lblAppointmentdate.Name, Me.lblAppointmentdate.Text)
            Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.Name, Me.lblJobGroup.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.Name, Me.lblDepartmentGroup.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.Name, Me.lblSectionGroup.Text)
            Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.Name, Me.lblClassGroup.Text)
            Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
            Me.lblUnits.Text = Language._Object.getCaption(Me.lblUnits.Name, Me.lblUnits.Text)
            Me.lblTeam.Text = Language._Object.getCaption(Me.lblTeam.Name, Me.lblTeam.Text)
            Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
            Me.lblAllocationGrade.Text = Language._Object.getCaption(Me.lblAllocationGrade.Name, Me.lblAllocationGrade.Text)
            Me.dgcolhChangeDate.HeaderText = Language._Object.getCaption(Me.dgcolhChangeDate.Name, Me.dgcolhChangeDate.HeaderText)
            Me.dgcolhJobGroup.HeaderText = Language._Object.getCaption(Me.dgcolhJobGroup.Name, Me.dgcolhJobGroup.HeaderText)
            Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
            Me.dgcolhReason.HeaderText = Language._Object.getCaption(Me.dgcolhReason.Name, Me.dgcolhReason.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Job is mandatory information. Please select atleast one Job to continue.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Change Reason is mandatory information. Please select Change Reason to continue.")
            Language.setMessage(mstrModuleName, 6, "Sorry, This is system generate entry. Please set effective date as employee appointment date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class