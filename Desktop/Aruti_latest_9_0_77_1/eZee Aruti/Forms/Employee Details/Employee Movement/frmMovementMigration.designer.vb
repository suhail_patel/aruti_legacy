﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMovementMigration
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMovementMigration))
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblScreenTypeApprover = New eZee.Common.eZeeLine
        Me.dgvApproverMigration = New System.Windows.Forms.DataGridView
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.pnlExpenseType = New System.Windows.Forms.Panel
        Me.cboExCategory = New System.Windows.Forms.ComboBox
        Me.lblExpenseCat = New System.Windows.Forms.Label
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.objbtnSearchToApprover = New eZee.Common.eZeeGradientButton
        Me.lblCaption1 = New System.Windows.Forms.Label
        Me.cboNewApprover = New System.Windows.Forms.ComboBox
        Me.objbtnSearchFromApprover = New eZee.Common.eZeeGradientButton
        Me.lblNewApprover = New System.Windows.Forms.Label
        Me.cboOldApprover = New System.Windows.Forms.ComboBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.pnlOldLevel = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboOldLevel = New System.Windows.Forms.ComboBox
        Me.pnlNewLevel = New System.Windows.Forms.Panel
        Me.objbtnSearchToApproverLevel = New eZee.Common.eZeeGradientButton
        Me.lblNewLevel = New System.Windows.Forms.Label
        Me.cboNewLevel = New System.Windows.Forms.ComboBox
        Me.pnlPerformanceApproverType = New System.Windows.Forms.Panel
        Me.radReviewer = New System.Windows.Forms.RadioButton
        Me.radAssessor = New System.Windows.Forms.RadioButton
        Me.pnlAssesmentApprovalOT = New System.Windows.Forms.Panel
        Me.radVoidAssessment = New System.Windows.Forms.RadioButton
        Me.radOverWriteAssessment = New System.Windows.Forms.RadioButton
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboScreenType = New System.Windows.Forms.ComboBox
        Me.objelLine2 = New eZee.Common.eZeeLine
        Me.btnRetain = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblnote = New System.Windows.Forms.Label
        Me.btnSet = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblapprovernote = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhdgScreenType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhdgOldApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhdgNewApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhdgIsSkip = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhOldApproverId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhNewApproverId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhtransactionid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhemployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhassesoroprationtype = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhmoduletype = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhscreentype = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhisactive = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        CType(Me.dgvApproverMigration, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlExpenseType.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pnlOldLevel.SuspendLayout()
        Me.pnlNewLevel.SuspendLayout()
        Me.pnlPerformanceApproverType.SuspendLayout()
        Me.pnlAssesmentApprovalOT.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblapprovernote)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblScreenTypeApprover)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.dgvApproverMigration)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.FlowLayoutPanel2)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.EZeeFooter1)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objelLine2)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.btnRetain)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblnote)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.btnSet)
        Me.EZeeCollapsibleContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(0, 0)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(1012, 490)
        Me.EZeeCollapsibleContainer1.TabIndex = 0
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Transfer Migration"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblScreenTypeApprover
        '
        Me.lblScreenTypeApprover.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lblScreenTypeApprover.Location = New System.Drawing.Point(454, 34)
        Me.lblScreenTypeApprover.Name = "lblScreenTypeApprover"
        Me.lblScreenTypeApprover.Size = New System.Drawing.Size(543, 14)
        Me.lblScreenTypeApprover.TabIndex = 263
        Me.lblScreenTypeApprover.Text = "Allocations"
        Me.lblScreenTypeApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgvApproverMigration
        '
        Me.dgvApproverMigration.AllowUserToAddRows = False
        Me.dgvApproverMigration.AllowUserToDeleteRows = False
        Me.dgvApproverMigration.AllowUserToResizeColumns = False
        Me.dgvApproverMigration.AllowUserToResizeRows = False
        Me.dgvApproverMigration.BackgroundColor = System.Drawing.Color.White
        Me.dgvApproverMigration.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvApproverMigration.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhdgScreenType, Me.colhdgOldApprover, Me.colhdgNewApprover, Me.objcolhdgIsSkip, Me.objcolhOldApproverId, Me.objcolhNewApproverId, Me.objcolhtransactionid, Me.objcolhemployeeunkid, Me.objcolhassesoroprationtype, Me.objcolhmoduletype, Me.objcolhscreentype, Me.objcolhisactive})
        Me.dgvApproverMigration.Location = New System.Drawing.Point(454, 54)
        Me.dgvApproverMigration.Margin = New System.Windows.Forms.Padding(0, 0, 0, 3)
        Me.dgvApproverMigration.Name = "dgvApproverMigration"
        Me.dgvApproverMigration.RowHeadersVisible = False
        Me.dgvApproverMigration.Size = New System.Drawing.Size(546, 280)
        Me.dgvApproverMigration.TabIndex = 122
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.Panel2)
        Me.FlowLayoutPanel2.Controls.Add(Me.pnlExpenseType)
        Me.FlowLayoutPanel2.Controls.Add(Me.Panel4)
        Me.FlowLayoutPanel2.Controls.Add(Me.Panel1)
        Me.FlowLayoutPanel2.Controls.Add(Me.pnlOldLevel)
        Me.FlowLayoutPanel2.Controls.Add(Me.pnlNewLevel)
        Me.FlowLayoutPanel2.Controls.Add(Me.pnlPerformanceApproverType)
        Me.FlowLayoutPanel2.Controls.Add(Me.pnlAssesmentApprovalOT)
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(11, 34)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(440, 300)
        Me.FlowLayoutPanel2.TabIndex = 261
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cboEmployee)
        Me.Panel2.Controls.Add(Me.objbtnSearchEmployee)
        Me.Panel2.Controls.Add(Me.lblEmployee)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(5, 5)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(5, 5, 5, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(429, 28)
        Me.Panel2.TabIndex = 0
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 450
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(121, 3)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(281, 21)
        Me.cboEmployee.TabIndex = 243
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(408, 3)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(18, 21)
        Me.objbtnSearchEmployee.TabIndex = 244
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 3)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(77, 21)
        Me.lblEmployee.TabIndex = 242
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlExpenseType
        '
        Me.pnlExpenseType.Controls.Add(Me.cboExCategory)
        Me.pnlExpenseType.Controls.Add(Me.lblExpenseCat)
        Me.pnlExpenseType.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlExpenseType.Location = New System.Drawing.Point(5, 33)
        Me.pnlExpenseType.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.pnlExpenseType.Name = "pnlExpenseType"
        Me.pnlExpenseType.Size = New System.Drawing.Size(429, 28)
        Me.pnlExpenseType.TabIndex = 2
        '
        'cboExCategory
        '
        Me.cboExCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExCategory.FormattingEnabled = True
        Me.cboExCategory.Location = New System.Drawing.Point(121, 3)
        Me.cboExCategory.Name = "cboExCategory"
        Me.cboExCategory.Size = New System.Drawing.Size(281, 21)
        Me.cboExCategory.TabIndex = 334
        '
        'lblExpenseCat
        '
        Me.lblExpenseCat.BackColor = System.Drawing.Color.Transparent
        Me.lblExpenseCat.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpenseCat.Location = New System.Drawing.Point(5, 5)
        Me.lblExpenseCat.Name = "lblExpenseCat"
        Me.lblExpenseCat.Size = New System.Drawing.Size(69, 20)
        Me.lblExpenseCat.TabIndex = 335
        Me.lblExpenseCat.Text = "Expense Category"
        Me.lblExpenseCat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.objbtnSearchToApprover)
        Me.Panel4.Controls.Add(Me.lblCaption1)
        Me.Panel4.Controls.Add(Me.cboNewApprover)
        Me.Panel4.Controls.Add(Me.objbtnSearchFromApprover)
        Me.Panel4.Controls.Add(Me.lblNewApprover)
        Me.Panel4.Controls.Add(Me.cboOldApprover)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel4.Location = New System.Drawing.Point(5, 61)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(429, 60)
        Me.Panel4.TabIndex = 1
        '
        'objbtnSearchToApprover
        '
        Me.objbtnSearchToApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchToApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchToApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchToApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchToApprover.BorderSelected = False
        Me.objbtnSearchToApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchToApprover.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchToApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchToApprover.Location = New System.Drawing.Point(409, 33)
        Me.objbtnSearchToApprover.Name = "objbtnSearchToApprover"
        Me.objbtnSearchToApprover.Size = New System.Drawing.Size(17, 21)
        Me.objbtnSearchToApprover.TabIndex = 338
        '
        'lblCaption1
        '
        Me.lblCaption1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption1.Location = New System.Drawing.Point(8, 3)
        Me.lblCaption1.Name = "lblCaption1"
        Me.lblCaption1.Size = New System.Drawing.Size(80, 21)
        Me.lblCaption1.TabIndex = 334
        Me.lblCaption1.Text = "From Approver"
        Me.lblCaption1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNewApprover
        '
        Me.cboNewApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNewApprover.DropDownWidth = 300
        Me.cboNewApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNewApprover.FormattingEnabled = True
        Me.cboNewApprover.Location = New System.Drawing.Point(121, 33)
        Me.cboNewApprover.Name = "cboNewApprover"
        Me.cboNewApprover.Size = New System.Drawing.Size(281, 21)
        Me.cboNewApprover.TabIndex = 335
        '
        'objbtnSearchFromApprover
        '
        Me.objbtnSearchFromApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchFromApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchFromApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchFromApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchFromApprover.BorderSelected = False
        Me.objbtnSearchFromApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchFromApprover.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchFromApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchFromApprover.Location = New System.Drawing.Point(408, 3)
        Me.objbtnSearchFromApprover.Name = "objbtnSearchFromApprover"
        Me.objbtnSearchFromApprover.Size = New System.Drawing.Size(18, 21)
        Me.objbtnSearchFromApprover.TabIndex = 337
        '
        'lblNewApprover
        '
        Me.lblNewApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewApprover.Location = New System.Drawing.Point(8, 32)
        Me.lblNewApprover.Name = "lblNewApprover"
        Me.lblNewApprover.Size = New System.Drawing.Size(68, 21)
        Me.lblNewApprover.TabIndex = 336
        Me.lblNewApprover.Text = "To Approver"
        Me.lblNewApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOldApprover
        '
        Me.cboOldApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOldApprover.DropDownWidth = 300
        Me.cboOldApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOldApprover.FormattingEnabled = True
        Me.cboOldApprover.Location = New System.Drawing.Point(121, 3)
        Me.cboOldApprover.Name = "cboOldApprover"
        Me.cboOldApprover.Size = New System.Drawing.Size(281, 21)
        Me.cboOldApprover.TabIndex = 333
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lnkAllocation)
        Me.Panel1.Location = New System.Drawing.Point(5, 121)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(429, 18)
        Me.Panel1.TabIndex = 120
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(333, 2)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(69, 13)
        Me.lnkAllocation.TabIndex = 234
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'pnlOldLevel
        '
        Me.pnlOldLevel.Controls.Add(Me.Label1)
        Me.pnlOldLevel.Controls.Add(Me.cboOldLevel)
        Me.pnlOldLevel.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlOldLevel.Location = New System.Drawing.Point(5, 139)
        Me.pnlOldLevel.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.pnlOldLevel.Name = "pnlOldLevel"
        Me.pnlOldLevel.Size = New System.Drawing.Size(429, 28)
        Me.pnlOldLevel.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 21)
        Me.Label1.TabIndex = 335
        Me.Label1.Text = "From Approver Level"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOldLevel
        '
        Me.cboOldLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOldLevel.DropDownWidth = 300
        Me.cboOldLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOldLevel.FormattingEnabled = True
        Me.cboOldLevel.Location = New System.Drawing.Point(121, 3)
        Me.cboOldLevel.Name = "cboOldLevel"
        Me.cboOldLevel.Size = New System.Drawing.Size(281, 21)
        Me.cboOldLevel.TabIndex = 334
        '
        'pnlNewLevel
        '
        Me.pnlNewLevel.Controls.Add(Me.objbtnSearchToApproverLevel)
        Me.pnlNewLevel.Controls.Add(Me.lblNewLevel)
        Me.pnlNewLevel.Controls.Add(Me.cboNewLevel)
        Me.pnlNewLevel.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlNewLevel.Location = New System.Drawing.Point(5, 167)
        Me.pnlNewLevel.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.pnlNewLevel.Name = "pnlNewLevel"
        Me.pnlNewLevel.Size = New System.Drawing.Size(429, 28)
        Me.pnlNewLevel.TabIndex = 7
        '
        'objbtnSearchToApproverLevel
        '
        Me.objbtnSearchToApproverLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchToApproverLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchToApproverLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchToApproverLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchToApproverLevel.BorderSelected = False
        Me.objbtnSearchToApproverLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchToApproverLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchToApproverLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchToApproverLevel.Location = New System.Drawing.Point(408, 3)
        Me.objbtnSearchToApproverLevel.Name = "objbtnSearchToApproverLevel"
        Me.objbtnSearchToApproverLevel.Size = New System.Drawing.Size(17, 21)
        Me.objbtnSearchToApproverLevel.TabIndex = 339
        '
        'lblNewLevel
        '
        Me.lblNewLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewLevel.Location = New System.Drawing.Point(8, 3)
        Me.lblNewLevel.Name = "lblNewLevel"
        Me.lblNewLevel.Size = New System.Drawing.Size(108, 21)
        Me.lblNewLevel.TabIndex = 337
        Me.lblNewLevel.Text = "To Approver Level"
        Me.lblNewLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNewLevel
        '
        Me.cboNewLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNewLevel.DropDownWidth = 300
        Me.cboNewLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNewLevel.FormattingEnabled = True
        Me.cboNewLevel.Location = New System.Drawing.Point(121, 3)
        Me.cboNewLevel.Name = "cboNewLevel"
        Me.cboNewLevel.Size = New System.Drawing.Size(281, 21)
        Me.cboNewLevel.TabIndex = 336
        '
        'pnlPerformanceApproverType
        '
        Me.pnlPerformanceApproverType.Controls.Add(Me.radReviewer)
        Me.pnlPerformanceApproverType.Controls.Add(Me.radAssessor)
        Me.pnlPerformanceApproverType.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlPerformanceApproverType.Location = New System.Drawing.Point(5, 195)
        Me.pnlPerformanceApproverType.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.pnlPerformanceApproverType.Name = "pnlPerformanceApproverType"
        Me.pnlPerformanceApproverType.Size = New System.Drawing.Size(429, 28)
        Me.pnlPerformanceApproverType.TabIndex = 3
        '
        'radReviewer
        '
        Me.radReviewer.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.radReviewer.BackColor = System.Drawing.Color.Transparent
        Me.radReviewer.Location = New System.Drawing.Point(111, 2)
        Me.radReviewer.Name = "radReviewer"
        Me.radReviewer.Size = New System.Drawing.Size(92, 22)
        Me.radReviewer.TabIndex = 3
        Me.radReviewer.TabStop = True
        Me.radReviewer.Text = "Reviewer"
        Me.radReviewer.UseVisualStyleBackColor = False
        '
        'radAssessor
        '
        Me.radAssessor.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.radAssessor.BackColor = System.Drawing.Color.Transparent
        Me.radAssessor.Location = New System.Drawing.Point(10, 2)
        Me.radAssessor.Name = "radAssessor"
        Me.radAssessor.Size = New System.Drawing.Size(92, 23)
        Me.radAssessor.TabIndex = 2
        Me.radAssessor.TabStop = True
        Me.radAssessor.Text = "Assessor"
        Me.radAssessor.UseVisualStyleBackColor = False
        '
        'pnlAssesmentApprovalOT
        '
        Me.pnlAssesmentApprovalOT.Controls.Add(Me.radVoidAssessment)
        Me.pnlAssesmentApprovalOT.Controls.Add(Me.radOverWriteAssessment)
        Me.pnlAssesmentApprovalOT.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlAssesmentApprovalOT.Location = New System.Drawing.Point(444, 0)
        Me.pnlAssesmentApprovalOT.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.pnlAssesmentApprovalOT.Name = "pnlAssesmentApprovalOT"
        Me.pnlAssesmentApprovalOT.Size = New System.Drawing.Size(429, 90)
        Me.pnlAssesmentApprovalOT.TabIndex = 8
        '
        'radVoidAssessment
        '
        Me.radVoidAssessment.ForeColor = System.Drawing.Color.Red
        Me.radVoidAssessment.Location = New System.Drawing.Point(8, 44)
        Me.radVoidAssessment.Name = "radVoidAssessment"
        Me.radVoidAssessment.Size = New System.Drawing.Size(405, 37)
        Me.radVoidAssessment.TabIndex = 172
        Me.radVoidAssessment.TabStop = True
        Me.radVoidAssessment.Text = "Void assessment done for migrating assessor/reviewer for the selected employee fo" & _
            "r all open assessment period."
        Me.radVoidAssessment.UseVisualStyleBackColor = True
        '
        'radOverWriteAssessment
        '
        Me.radOverWriteAssessment.ForeColor = System.Drawing.Color.Red
        Me.radOverWriteAssessment.Location = New System.Drawing.Point(8, 5)
        Me.radOverWriteAssessment.Name = "radOverWriteAssessment"
        Me.radOverWriteAssessment.Size = New System.Drawing.Size(407, 38)
        Me.radOverWriteAssessment.TabIndex = 171
        Me.radOverWriteAssessment.TabStop = True
        Me.radOverWriteAssessment.Text = "Overwrite assessment done for migrating assessor/reviewer for the selected employ" & _
            "ee for all open assessment period."
        Me.radOverWriteAssessment.UseVisualStyleBackColor = True
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.cboScreenType)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 435)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(1012, 55)
        Me.EZeeFooter1.TabIndex = 240
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(908, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 88
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(808, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 87
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'cboScreenType
        '
        Me.cboScreenType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScreenType.DropDownWidth = 300
        Me.cboScreenType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboScreenType.FormattingEnabled = True
        Me.cboScreenType.Items.AddRange(New Object() {"Leave", "Claim"})
        Me.cboScreenType.Location = New System.Drawing.Point(11, 13)
        Me.cboScreenType.Name = "cboScreenType"
        Me.cboScreenType.Size = New System.Drawing.Size(25, 21)
        Me.cboScreenType.TabIndex = 119
        Me.cboScreenType.Visible = False
        '
        'objelLine2
        '
        Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine2.Location = New System.Drawing.Point(11, 381)
        Me.objelLine2.Name = "objelLine2"
        Me.objelLine2.Size = New System.Drawing.Size(1080, 13)
        Me.objelLine2.TabIndex = 255
        Me.objelLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRetain
        '
        Me.btnRetain.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRetain.BackColor = System.Drawing.Color.White
        Me.btnRetain.BackgroundImage = CType(resources.GetObject("btnRetain.BackgroundImage"), System.Drawing.Image)
        Me.btnRetain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRetain.BorderColor = System.Drawing.Color.Empty
        Me.btnRetain.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRetain.FlatAppearance.BorderSize = 0
        Me.btnRetain.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRetain.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRetain.ForeColor = System.Drawing.Color.Black
        Me.btnRetain.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRetain.GradientForeColor = System.Drawing.Color.Black
        Me.btnRetain.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRetain.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRetain.Location = New System.Drawing.Point(885, 397)
        Me.btnRetain.Name = "btnRetain"
        Me.btnRetain.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRetain.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRetain.Size = New System.Drawing.Size(116, 30)
        Me.btnRetain.TabIndex = 259
        Me.btnRetain.Text = "&Retain Approver"
        Me.btnRetain.UseVisualStyleBackColor = True
        '
        'lblnote
        '
        Me.lblnote.ForeColor = System.Drawing.Color.Blue
        Me.lblnote.Location = New System.Drawing.Point(460, 339)
        Me.lblnote.Name = "lblnote"
        Me.lblnote.Size = New System.Drawing.Size(541, 13)
        Me.lblnote.TabIndex = 250
        Me.lblnote.Text = "Note: This color indicates assign new approver for selected employee"
        Me.lblnote.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnSet
        '
        Me.btnSet.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSet.BackColor = System.Drawing.Color.White
        Me.btnSet.BackgroundImage = CType(resources.GetObject("btnSet.BackgroundImage"), System.Drawing.Image)
        Me.btnSet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSet.BorderColor = System.Drawing.Color.Empty
        Me.btnSet.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSet.FlatAppearance.BorderSize = 0
        Me.btnSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSet.ForeColor = System.Drawing.Color.Black
        Me.btnSet.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSet.GradientForeColor = System.Drawing.Color.Black
        Me.btnSet.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSet.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSet.Location = New System.Drawing.Point(11, 397)
        Me.btnSet.Name = "btnSet"
        Me.btnSet.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSet.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSet.Size = New System.Drawing.Size(81, 30)
        Me.btnSet.TabIndex = 247
        Me.btnSet.Text = "&Set"
        Me.btnSet.UseVisualStyleBackColor = True
        '
        'lblapprovernote
        '
        Me.lblapprovernote.ForeColor = System.Drawing.Color.Red
        Me.lblapprovernote.Location = New System.Drawing.Point(461, 360)
        Me.lblapprovernote.Name = "lblapprovernote"
        Me.lblapprovernote.Size = New System.Drawing.Size(541, 13)
        Me.lblapprovernote.TabIndex = 265
        Me.lblapprovernote.Text = "Note: This color indicates already assign approver is Inactive"
        Me.lblapprovernote.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblapprovernote.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.FillWeight = 145.3488!
        Me.DataGridViewTextBoxColumn1.HeaderText = "Screen Type"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.FillWeight = 54.65117!
        Me.DataGridViewTextBoxColumn2.HeaderText = "Old Approver"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.FillWeight = 50.0!
        Me.DataGridViewTextBoxColumn3.HeaderText = "New Approver"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Skip"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "ScreenTypeId"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "OldApproverId"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "NewApproverId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "transectionid"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "employeeunkid"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "assesoroprationtype"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "moduletype"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "screentype"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'colhdgScreenType
        '
        Me.colhdgScreenType.FillWeight = 145.3488!
        Me.colhdgScreenType.HeaderText = "Screen Type"
        Me.colhdgScreenType.Name = "colhdgScreenType"
        Me.colhdgScreenType.ReadOnly = True
        Me.colhdgScreenType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhdgScreenType.Visible = False
        Me.colhdgScreenType.Width = 5
        '
        'colhdgOldApprover
        '
        Me.colhdgOldApprover.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhdgOldApprover.FillWeight = 50.0!
        Me.colhdgOldApprover.HeaderText = "Old Approver"
        Me.colhdgOldApprover.Name = "colhdgOldApprover"
        Me.colhdgOldApprover.ReadOnly = True
        Me.colhdgOldApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhdgNewApprover
        '
        Me.colhdgNewApprover.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhdgNewApprover.FillWeight = 50.0!
        Me.colhdgNewApprover.HeaderText = "New Approver"
        Me.colhdgNewApprover.Name = "colhdgNewApprover"
        Me.colhdgNewApprover.ReadOnly = True
        Me.colhdgNewApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objcolhdgIsSkip
        '
        Me.objcolhdgIsSkip.HeaderText = "Skip"
        Me.objcolhdgIsSkip.Name = "objcolhdgIsSkip"
        Me.objcolhdgIsSkip.Visible = False
        '
        'objcolhOldApproverId
        '
        Me.objcolhOldApproverId.HeaderText = "OldApproverId"
        Me.objcolhOldApproverId.Name = "objcolhOldApproverId"
        Me.objcolhOldApproverId.Visible = False
        '
        'objcolhNewApproverId
        '
        Me.objcolhNewApproverId.HeaderText = "NewApproverId"
        Me.objcolhNewApproverId.Name = "objcolhNewApproverId"
        Me.objcolhNewApproverId.Visible = False
        '
        'objcolhtransactionid
        '
        Me.objcolhtransactionid.HeaderText = "transectionid"
        Me.objcolhtransactionid.Name = "objcolhtransactionid"
        Me.objcolhtransactionid.ReadOnly = True
        Me.objcolhtransactionid.Visible = False
        '
        'objcolhemployeeunkid
        '
        Me.objcolhemployeeunkid.HeaderText = "employeeunkid"
        Me.objcolhemployeeunkid.Name = "objcolhemployeeunkid"
        Me.objcolhemployeeunkid.ReadOnly = True
        Me.objcolhemployeeunkid.Visible = False
        '
        'objcolhassesoroprationtype
        '
        Me.objcolhassesoroprationtype.HeaderText = "assesoroprationtype"
        Me.objcolhassesoroprationtype.Name = "objcolhassesoroprationtype"
        Me.objcolhassesoroprationtype.ReadOnly = True
        Me.objcolhassesoroprationtype.Visible = False
        '
        'objcolhmoduletype
        '
        Me.objcolhmoduletype.HeaderText = "moduletype"
        Me.objcolhmoduletype.Name = "objcolhmoduletype"
        Me.objcolhmoduletype.ReadOnly = True
        Me.objcolhmoduletype.Visible = False
        '
        'objcolhscreentype
        '
        Me.objcolhscreentype.HeaderText = "screentype"
        Me.objcolhscreentype.Name = "objcolhscreentype"
        Me.objcolhscreentype.ReadOnly = True
        Me.objcolhscreentype.Visible = False
        '
        'objcolhisactive
        '
        Me.objcolhisactive.HeaderText = "isactive"
        Me.objcolhisactive.Name = "objcolhisactive"
        Me.objcolhisactive.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "isskip"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "isfromapproval"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "oprationtype"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "isprocess"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'frmMovementMigration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1012, 490)
        Me.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMovementMigration"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Movement Migration"
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        CType(Me.dgvApproverMigration, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.pnlExpenseType.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.pnlOldLevel.ResumeLayout(False)
        Me.pnlNewLevel.ResumeLayout(False)
        Me.pnlPerformanceApproverType.ResumeLayout(False)
        Me.pnlAssesmentApprovalOT.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboExCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblExpenseCat As System.Windows.Forms.Label
    Friend WithEvents radReviewer As System.Windows.Forms.RadioButton
    Friend WithEvents radAssessor As System.Windows.Forms.RadioButton
    Friend WithEvents lblNewApprover As System.Windows.Forms.Label
    Friend WithEvents cboNewApprover As System.Windows.Forms.ComboBox
    Friend WithEvents lblCaption1 As System.Windows.Forms.Label
    Friend WithEvents cboOldApprover As System.Windows.Forms.ComboBox
    Friend WithEvents cboOldLevel As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboNewLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblNewLevel As System.Windows.Forms.Label
    Friend WithEvents radOverWriteAssessment As System.Windows.Forms.RadioButton
    Friend WithEvents radVoidAssessment As System.Windows.Forms.RadioButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents dgvApproverMigration As System.Windows.Forms.DataGridView
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objbtnSearchToApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchFromApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlExpenseType As System.Windows.Forms.Panel
    Friend WithEvents pnlPerformanceApproverType As System.Windows.Forms.Panel
    Friend WithEvents pnlOldLevel As System.Windows.Forms.Panel
    Friend WithEvents pnlNewLevel As System.Windows.Forms.Panel
    Friend WithEvents pnlAssesmentApprovalOT As System.Windows.Forms.Panel
    Friend WithEvents btnSet As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnSearchToApproverLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblnote As System.Windows.Forms.Label
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents btnRetain As eZee.Common.eZeeLightButton
    Friend WithEvents cboScreenType As System.Windows.Forms.ComboBox
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents lblScreenTypeApprover As eZee.Common.eZeeLine
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents colhdgScreenType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhdgOldApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhdgNewApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhdgIsSkip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhOldApproverId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhNewApproverId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhtransactionid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhemployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhassesoroprationtype As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhmoduletype As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhscreentype As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhisactive As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblapprovernote As System.Windows.Forms.Label

End Class
