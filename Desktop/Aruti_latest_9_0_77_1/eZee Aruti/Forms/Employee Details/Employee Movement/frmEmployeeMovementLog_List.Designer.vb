﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeMovementLog_List
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeMovementLog_List))
        Me.pnlEmployeeLog = New System.Windows.Forms.Panel
        Me.lvEmployeeMovementLog = New eZee.Common.eZeeVirtualListView(Me.components)
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhDate = New System.Windows.Forms.ColumnHeader
        Me.colhBranch = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhSection = New System.Windows.Forms.ColumnHeader
        Me.colhJob = New System.Windows.Forms.ColumnHeader
        Me.colhUser = New System.Windows.Forms.ColumnHeader
        Me.colhUnit = New System.Windows.Forms.ColumnHeader
        Me.colhGrade = New System.Windows.Forms.ColumnHeader
        Me.colhGradeLevel = New System.Windows.Forms.ColumnHeader
        Me.colhScale = New System.Windows.Forms.ColumnHeader
        Me.colhClass = New System.Windows.Forms.ColumnHeader
        Me.colhCostCenter = New System.Windows.Forms.ColumnHeader
        Me.colhDeptGrp = New System.Windows.Forms.ColumnHeader
        Me.colhSectionGrp = New System.Windows.Forms.ColumnHeader
        Me.colhUnitGrp = New System.Windows.Forms.ColumnHeader
        Me.colhTeam = New System.Windows.Forms.ColumnHeader
        Me.colhGradeGrp = New System.Windows.Forms.ColumnHeader
        Me.colhJobGrp = New System.Windows.Forms.ColumnHeader
        Me.colhClassGrp = New System.Windows.Forms.ColumnHeader
        Me.colhAllocationReason = New System.Windows.Forms.ColumnHeader
        Me.colhAppointedDate = New System.Windows.Forms.ColumnHeader
        Me.colhConfirmationDate = New System.Windows.Forms.ColumnHeader
        Me.colhSuspensionFrom = New System.Windows.Forms.ColumnHeader
        Me.colhSuspensionTo = New System.Windows.Forms.ColumnHeader
        Me.colhProbationFrom = New System.Windows.Forms.ColumnHeader
        Me.colhProbationTo = New System.Windows.Forms.ColumnHeader
        Me.colhEOCDate = New System.Windows.Forms.ColumnHeader
        Me.colhLeavingDate = New System.Windows.Forms.ColumnHeader
        Me.colhRetirementDate = New System.Windows.Forms.ColumnHeader
        Me.colhReinstatementDate = New System.Windows.Forms.ColumnHeader
        Me.mnuJobLog = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cpHeader = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objstLine1 = New eZee.Common.eZeeStraightLine
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblJob = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblClass = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblGrade = New System.Windows.Forms.Label
        Me.cboUnits = New System.Windows.Forms.ComboBox
        Me.cboSections = New System.Windows.Forms.ComboBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.lblUnits = New System.Windows.Forms.Label
        Me.lblSection = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlEmployeeLog.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlEmployeeLog
        '
        Me.pnlEmployeeLog.Controls.Add(Me.lvEmployeeMovementLog)
        Me.pnlEmployeeLog.Controls.Add(Me.gbFilterCriteria)
        Me.pnlEmployeeLog.Controls.Add(Me.objFooter)
        Me.pnlEmployeeLog.Controls.Add(Me.eZeeHeader)
        Me.pnlEmployeeLog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEmployeeLog.Location = New System.Drawing.Point(0, 0)
        Me.pnlEmployeeLog.Name = "pnlEmployeeLog"
        Me.pnlEmployeeLog.Size = New System.Drawing.Size(846, 510)
        Me.pnlEmployeeLog.TabIndex = 0
        '
        'lvEmployeeMovementLog
        '
        Me.lvEmployeeMovementLog.ColumnHeaders = Nothing
        Me.lvEmployeeMovementLog.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhEmployee, Me.colhDate, Me.colhBranch, Me.colhDepartment, Me.colhSection, Me.colhJob, Me.colhUser, Me.colhUnit, Me.colhGrade, Me.colhGradeLevel, Me.colhScale, Me.colhClass, Me.colhCostCenter, Me.colhDeptGrp, Me.colhSectionGrp, Me.colhUnitGrp, Me.colhTeam, Me.colhGradeGrp, Me.colhJobGrp, Me.colhClassGrp, Me.colhAllocationReason, Me.colhAppointedDate, Me.colhConfirmationDate, Me.colhSuspensionFrom, Me.colhSuspensionTo, Me.colhProbationFrom, Me.colhProbationTo, Me.colhEOCDate, Me.colhLeavingDate, Me.colhRetirementDate, Me.colhReinstatementDate})
        Me.lvEmployeeMovementLog.CompulsoryColumns = resources.GetString("lvEmployeeMovementLog.CompulsoryColumns")
        Me.lvEmployeeMovementLog.ContextMenuStrip = Me.mnuJobLog
        Me.lvEmployeeMovementLog.ContextMenuStripHeader = Me.cpHeader
        Me.lvEmployeeMovementLog.FullRowSelect = True
        Me.lvEmployeeMovementLog.GridLines = True
        Me.lvEmployeeMovementLog.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvEmployeeMovementLog.HideSelection = False
        Me.lvEmployeeMovementLog.Location = New System.Drawing.Point(13, 159)
        Me.lvEmployeeMovementLog.LVItems = Nothing
        Me.lvEmployeeMovementLog.MinColumnWidth = 50
        Me.lvEmployeeMovementLog.MultiSelect = False
        Me.lvEmployeeMovementLog.Name = "lvEmployeeMovementLog"
        Me.lvEmployeeMovementLog.OptionalColumns = ""
        Me.lvEmployeeMovementLog.OwnerDraw = True
        Me.lvEmployeeMovementLog.ShowMoreItem = False
        Me.lvEmployeeMovementLog.ShowSaveItem = False
        Me.lvEmployeeMovementLog.ShowSizeAllColumnsToFit = True
        Me.lvEmployeeMovementLog.Size = New System.Drawing.Size(821, 290)
        Me.lvEmployeeMovementLog.TabIndex = 4
        Me.lvEmployeeMovementLog.UseCompatibleStateImageBehavior = False
        Me.lvEmployeeMovementLog.View = System.Windows.Forms.View.Details
        Me.lvEmployeeMovementLog.VirtualMode = True
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 110
        '
        'colhDate
        '
        Me.colhDate.Tag = "colhDate"
        Me.colhDate.Text = "Date"
        Me.colhDate.Width = 90
        '
        'colhBranch
        '
        Me.colhBranch.Tag = "colhBranch"
        Me.colhBranch.Text = "Branch"
        Me.colhBranch.Width = 93
        '
        'colhDepartment
        '
        Me.colhDepartment.DisplayIndex = 4
        Me.colhDepartment.Tag = "colhDepartment"
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 110
        '
        'colhSection
        '
        Me.colhSection.DisplayIndex = 6
        Me.colhSection.Tag = "colhSection"
        Me.colhSection.Text = "Section"
        Me.colhSection.Width = 110
        '
        'colhJob
        '
        Me.colhJob.DisplayIndex = 15
        Me.colhJob.Tag = "colhJob"
        Me.colhJob.Text = "Job"
        Me.colhJob.Width = 100
        '
        'colhUser
        '
        Me.colhUser.DisplayIndex = 19
        Me.colhUser.Tag = "colhUser"
        Me.colhUser.Text = "User"
        Me.colhUser.Width = 100
        '
        'colhUnit
        '
        Me.colhUnit.DisplayIndex = 8
        Me.colhUnit.Tag = "colhUnit"
        Me.colhUnit.Text = "Unit"
        Me.colhUnit.Width = 90
        '
        'colhGrade
        '
        Me.colhGrade.DisplayIndex = 11
        Me.colhGrade.Tag = "colhGrade"
        Me.colhGrade.Text = "Grade"
        Me.colhGrade.Width = 100
        '
        'colhGradeLevel
        '
        Me.colhGradeLevel.DisplayIndex = 12
        Me.colhGradeLevel.Tag = "colhGradeLevel"
        Me.colhGradeLevel.Text = "Grade Level"
        Me.colhGradeLevel.Width = 100
        '
        'colhScale
        '
        Me.colhScale.DisplayIndex = 13
        Me.colhScale.Tag = "colhScale"
        Me.colhScale.Text = "Scale"
        Me.colhScale.Width = 100
        '
        'colhClass
        '
        Me.colhClass.DisplayIndex = 17
        Me.colhClass.Tag = "colhClass"
        Me.colhClass.Text = "Class"
        Me.colhClass.Width = 100
        '
        'colhCostCenter
        '
        Me.colhCostCenter.DisplayIndex = 18
        Me.colhCostCenter.Tag = "colhCostCenter"
        Me.colhCostCenter.Text = "Cost Center"
        Me.colhCostCenter.Width = 100
        '
        'colhDeptGrp
        '
        Me.colhDeptGrp.DisplayIndex = 3
        Me.colhDeptGrp.Tag = "colhDeptGrp"
        Me.colhDeptGrp.Text = "Department Group"
        Me.colhDeptGrp.Width = 110
        '
        'colhSectionGrp
        '
        Me.colhSectionGrp.DisplayIndex = 5
        Me.colhSectionGrp.Tag = "colhSectionGrp"
        Me.colhSectionGrp.Text = "Section Group"
        Me.colhSectionGrp.Width = 110
        '
        'colhUnitGrp
        '
        Me.colhUnitGrp.DisplayIndex = 7
        Me.colhUnitGrp.Tag = "colhUnitGrp"
        Me.colhUnitGrp.Text = "Unit Group"
        Me.colhUnitGrp.Width = 110
        '
        'colhTeam
        '
        Me.colhTeam.DisplayIndex = 9
        Me.colhTeam.Tag = "colhTeam"
        Me.colhTeam.Text = "Team"
        Me.colhTeam.Width = 110
        '
        'colhGradeGrp
        '
        Me.colhGradeGrp.DisplayIndex = 10
        Me.colhGradeGrp.Tag = "colhGradeGrp"
        Me.colhGradeGrp.Text = "Grade Group"
        Me.colhGradeGrp.Width = 110
        '
        'colhJobGrp
        '
        Me.colhJobGrp.DisplayIndex = 14
        Me.colhJobGrp.Tag = "colhJobGrp"
        Me.colhJobGrp.Text = "Job Group"
        Me.colhJobGrp.Width = 110
        '
        'colhClassGrp
        '
        Me.colhClassGrp.DisplayIndex = 16
        Me.colhClassGrp.Tag = "colhClassGrp"
        Me.colhClassGrp.Text = "Class Group"
        Me.colhClassGrp.Width = 110
        '
        'colhAllocationReason
        '
        Me.colhAllocationReason.Tag = "colhAllocationReason"
        Me.colhAllocationReason.Text = "Allocation Reason"
        Me.colhAllocationReason.Width = 90
        '
        'colhAppointedDate
        '
        Me.colhAppointedDate.Tag = "colhAppointedDate"
        Me.colhAppointedDate.Text = "Appointed Date"
        Me.colhAppointedDate.Width = 80
        '
        'colhConfirmationDate
        '
        Me.colhConfirmationDate.Tag = "colhConfirmationDate"
        Me.colhConfirmationDate.Text = "Confirmation Date"
        Me.colhConfirmationDate.Width = 80
        '
        'colhSuspensionFrom
        '
        Me.colhSuspensionFrom.Tag = "colhSuspensionFrom"
        Me.colhSuspensionFrom.Text = "Suspension Date From"
        Me.colhSuspensionFrom.Width = 80
        '
        'colhSuspensionTo
        '
        Me.colhSuspensionTo.Tag = "colhSuspensionTo"
        Me.colhSuspensionTo.Text = "Suspension Date To"
        Me.colhSuspensionTo.Width = 80
        '
        'colhProbationFrom
        '
        Me.colhProbationFrom.Tag = "colhProbationFrom"
        Me.colhProbationFrom.Text = "Probation Date From"
        Me.colhProbationFrom.Width = 80
        '
        'colhProbationTo
        '
        Me.colhProbationTo.Tag = "colhProbationTo"
        Me.colhProbationTo.Text = "Probation Date To"
        Me.colhProbationTo.Width = 80
        '
        'colhEOCDate
        '
        Me.colhEOCDate.Tag = "colhEOCDate"
        Me.colhEOCDate.Text = "EOC Date"
        Me.colhEOCDate.Width = 80
        '
        'colhLeavingDate
        '
        Me.colhLeavingDate.Tag = "colhLeavingDate"
        Me.colhLeavingDate.Text = "Leaving Date"
        Me.colhLeavingDate.Width = 80
        '
        'colhRetirementDate
        '
        Me.colhRetirementDate.Tag = "colhRetirementDate"
        Me.colhRetirementDate.Text = "Retirement Date"
        Me.colhRetirementDate.Width = 80
        '
        'colhReinstatementDate
        '
        Me.colhReinstatementDate.Tag = "colhReinstatementDate"
        Me.colhReinstatementDate.Text = "Reinstatement Date"
        Me.colhReinstatementDate.Width = 80
        '
        'mnuJobLog
        '
        Me.mnuJobLog.Name = "mnuJobLog"
        Me.mnuJobLog.Size = New System.Drawing.Size(61, 4)
        '
        'cpHeader
        '
        Me.cpHeader.Name = "mnuJobLog"
        Me.cpHeader.Size = New System.Drawing.Size(61, 4)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objstLine1)
        Me.gbFilterCriteria.Controls.Add(Me.dtpDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboClass)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblClass)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.cboGrade)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lblGrade)
        Me.gbFilterCriteria.Controls.Add(Me.cboUnits)
        Me.gbFilterCriteria.Controls.Add(Me.cboSections)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.lblUnits)
        Me.gbFilterCriteria.Controls.Add(Me.lblSection)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(13, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(821, 89)
        Me.gbFilterCriteria.TabIndex = 3
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objstLine1
        '
        Me.objstLine1.BackColor = System.Drawing.Color.Transparent
        Me.objstLine1.ForeColor = System.Drawing.Color.Gray
        Me.objstLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objstLine1.Location = New System.Drawing.Point(260, 24)
        Me.objstLine1.Name = "objstLine1"
        Me.objstLine1.Size = New System.Drawing.Size(17, 64)
        Me.objstLine1.TabIndex = 108
        Me.objstLine1.Text = "EZeeStraightLine2"
        '
        'dtpDate
        '
        Me.dtpDate.Checked = False
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(88, 60)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.ShowCheckBox = True
        Me.dtpDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpDate.TabIndex = 105
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(8, 63)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(74, 15)
        Me.lblDate.TabIndex = 104
        Me.lblDate.Text = "Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(283, 63)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(71, 15)
        Me.lblJob.TabIndex = 20
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(360, 60)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(104, 21)
        Me.cboJob.TabIndex = 21
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(233, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 86
        '
        'cboClass
        '
        Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(708, 58)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(104, 21)
        Me.cboClass.TabIndex = 103
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(88, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(139, 21)
        Me.cboEmployee.TabIndex = 87
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(644, 61)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(58, 15)
        Me.lblClass.TabIndex = 102
        Me.lblClass.Text = "Class"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(74, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(794, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(534, 59)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(104, 21)
        Me.cboGrade.TabIndex = 101
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(771, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(470, 61)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(58, 15)
        Me.lblGrade.TabIndex = 100
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnits
        '
        Me.cboUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnits.FormattingEnabled = True
        Me.cboUnits.Location = New System.Drawing.Point(708, 31)
        Me.cboUnits.Name = "cboUnits"
        Me.cboUnits.Size = New System.Drawing.Size(104, 21)
        Me.cboUnits.TabIndex = 19
        '
        'cboSections
        '
        Me.cboSections.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSections.FormattingEnabled = True
        Me.cboSections.Location = New System.Drawing.Point(534, 32)
        Me.cboSections.Name = "cboSections"
        Me.cboSections.Size = New System.Drawing.Size(104, 21)
        Me.cboSections.TabIndex = 18
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(283, 36)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(71, 15)
        Me.lblDepartment.TabIndex = 14
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(360, 33)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(104, 21)
        Me.cboDepartment.TabIndex = 15
        '
        'lblUnits
        '
        Me.lblUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnits.Location = New System.Drawing.Point(644, 34)
        Me.lblUnits.Name = "lblUnits"
        Me.lblUnits.Size = New System.Drawing.Size(58, 15)
        Me.lblUnits.TabIndex = 17
        Me.lblUnits.Text = "Units"
        Me.lblUnits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(470, 35)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(58, 15)
        Me.lblSection.TabIndex = 16
        Me.lblSection.Text = "Sections"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 455)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(846, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(741, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 69
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(846, 58)
        Me.eZeeHeader.TabIndex = 1
        Me.eZeeHeader.Title = "Employee Movement"
        '
        'frmEmployeeMovementLog_List
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 510)
        Me.Controls.Add(Me.pnlEmployeeLog)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeMovementLog_List"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Movement"
        Me.pnlEmployeeLog.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlEmployeeLog As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents cboUnits As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnits As System.Windows.Forms.Label
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents mnuJobLog As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents lvEmployeeMovementLog As eZee.Common.eZeeVirtualListView
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBranch As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSection As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUnit As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJob As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGrade As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhScale As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClass As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCostCenter As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUser As System.Windows.Forms.ColumnHeader
    Friend WithEvents cpHeader As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGradeLevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents objstLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents colhDeptGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSectionGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUnitGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTeam As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGradeGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJobGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClassGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAllocationReason As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAppointedDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhConfirmationDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSuspensionFrom As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSuspensionTo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhProbationFrom As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhProbationTo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEOCDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLeavingDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRetirementDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhReinstatementDate As System.Windows.Forms.ColumnHeader
End Class
