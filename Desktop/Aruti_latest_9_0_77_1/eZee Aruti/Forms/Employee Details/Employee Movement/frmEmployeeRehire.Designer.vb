﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeRehire
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeRehire))
        Me.tabcEmployeeDetails = New System.Windows.Forms.TabControl
        Me.tabpMainInfo = New System.Windows.Forms.TabPage
        Me.gbEmployeeAllocation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkAssignDefaulTranHeads = New System.Windows.Forms.CheckBox
        Me.objbtnSearchUnits = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchTeam = New eZee.Common.eZeeGradientButton
        Me.lblBranch = New System.Windows.Forms.Label
        Me.objbtnSearchJobGroup = New eZee.Common.eZeeGradientButton
        Me.cboSections = New System.Windows.Forms.ComboBox
        Me.objbtnSearchJob = New eZee.Common.eZeeGradientButton
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.objbtnSearchGradeGrp = New eZee.Common.eZeeGradientButton
        Me.lblSection = New System.Windows.Forms.Label
        Me.objbtnSearchGrades = New eZee.Common.eZeeGradientButton
        Me.txtScale = New eZee.TextBox.NumericTextBox
        Me.objbtnSearchClass = New eZee.Common.eZeeGradientButton
        Me.lblScale = New System.Windows.Forms.Label
        Me.objbtnSearchClassGrp = New eZee.Common.eZeeGradientButton
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.objbtnSearchGradeLevel = New eZee.Common.eZeeGradientButton
        Me.lblDepartmentGroup = New System.Windows.Forms.Label
        Me.objbtnSearchCostCenter = New eZee.Common.eZeeGradientButton
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.objbtnSearchUnitGrp = New eZee.Common.eZeeGradientButton
        Me.lblGrade = New System.Windows.Forms.Label
        Me.objbtnSearchSection = New eZee.Common.eZeeGradientButton
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.objbtnSearchDepartment = New eZee.Common.eZeeGradientButton
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.objbtnSearchSecGroup = New eZee.Common.eZeeGradientButton
        Me.lblUnits = New System.Windows.Forms.Label
        Me.objbtnSearchDeptGrp = New eZee.Common.eZeeGradientButton
        Me.cboUnits = New System.Windows.Forms.ComboBox
        Me.objbtnSearchBranch = New eZee.Common.eZeeGradientButton
        Me.cboLevel = New System.Windows.Forms.ComboBox
        Me.lblTeam = New System.Windows.Forms.Label
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.cboTeams = New System.Windows.Forms.ComboBox
        Me.cboDepartmentGrp = New System.Windows.Forms.ComboBox
        Me.lblUnitGroup = New System.Windows.Forms.Label
        Me.lblJobGroup = New System.Windows.Forms.Label
        Me.cboUnitGroup = New System.Windows.Forms.ComboBox
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.lblSectionGroup = New System.Windows.Forms.Label
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.cboSectionGroup = New System.Windows.Forms.ComboBox
        Me.objbtnAddGradeGrp = New eZee.Common.eZeeGradientButton
        Me.cboStation = New System.Windows.Forms.ComboBox
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.objbtnAddCostCenter = New eZee.Common.eZeeGradientButton
        Me.lblClass = New System.Windows.Forms.Label
        Me.objbtnAddScale = New eZee.Common.eZeeGradientButton
        Me.lblJob = New System.Windows.Forms.Label
        Me.objbtnAddJob = New eZee.Common.eZeeGradientButton
        Me.objbtnAddDepartment = New eZee.Common.eZeeGradientButton
        Me.cboGradeGroup = New System.Windows.Forms.ComboBox
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.lblTransactionHead = New System.Windows.Forms.Label
        Me.objbtnAddGrade = New eZee.Common.eZeeGradientButton
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.objbtnAddTranHead = New eZee.Common.eZeeGradientButton
        Me.cboTransactionHead = New System.Windows.Forms.ComboBox
        Me.objbtnAddGradeLevel = New eZee.Common.eZeeGradientButton
        Me.tabpAdditionalInfo = New System.Windows.Forms.TabPage
        Me.gbWorkPermit = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtResidentIssuePlace = New eZee.TextBox.AlphanumericTextBox
        Me.lblResidentIssuePlace = New System.Windows.Forms.Label
        Me.txtResidentPermitNo = New eZee.TextBox.AlphanumericTextBox
        Me.dtpResidentExpiryDate = New System.Windows.Forms.DateTimePicker
        Me.lblResidentPermitNo = New System.Windows.Forms.Label
        Me.lblResidentExpiryDate = New System.Windows.Forms.Label
        Me.cboResidentIssueCountry = New System.Windows.Forms.ComboBox
        Me.dtpResidentIssueDate = New System.Windows.Forms.DateTimePicker
        Me.lblResidentIssueCountry = New System.Windows.Forms.Label
        Me.lblResidentIssueDate = New System.Windows.Forms.Label
        Me.elResidentPermit = New eZee.Common.eZeeLine
        Me.elWorkPermit = New eZee.Common.eZeeLine
        Me.txtPlaceofIssue = New eZee.TextBox.AlphanumericTextBox
        Me.dtpExpiryDate = New System.Windows.Forms.DateTimePicker
        Me.lblExpiryDate = New System.Windows.Forms.Label
        Me.lblPlaceOfIssue = New System.Windows.Forms.Label
        Me.txtWorkPermitNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblIssueDate = New System.Windows.Forms.Label
        Me.lblWorkPermitNo = New System.Windows.Forms.Label
        Me.dtpIssueDate = New System.Windows.Forms.DateTimePicker
        Me.lblIssueCountry = New System.Windows.Forms.Label
        Me.cboIssueCountry = New System.Windows.Forms.ComboBox
        Me.gbDatesDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblBirthDate = New System.Windows.Forms.Label
        Me.txtBirthDate = New System.Windows.Forms.TextBox
        Me.lblAppointmentdate = New System.Windows.Forms.Label
        Me.lblRetirementDate = New System.Windows.Forms.Label
        Me.txtAppDate = New System.Windows.Forms.TextBox
        Me.dtpConfirmationDate = New System.Windows.Forms.DateTimePicker
        Me.dtpRetirementDate = New System.Windows.Forms.DateTimePicker
        Me.lblConfirmationDate = New System.Windows.Forms.Label
        Me.chkExclude = New System.Windows.Forms.CheckBox
        Me.dtpLeavingDate = New System.Windows.Forms.DateTimePicker
        Me.lblLeavingDate = New System.Windows.Forms.Label
        Me.dtpEndEmplDate = New System.Windows.Forms.DateTimePicker
        Me.lblProbationTo = New System.Windows.Forms.Label
        Me.lblEmplDate = New System.Windows.Forms.Label
        Me.dtpProbationDateTo = New System.Windows.Forms.DateTimePicker
        Me.lblProbationFrom = New System.Windows.Forms.Label
        Me.dtpProbationDateFrom = New System.Windows.Forms.DateTimePicker
        Me.tabpMembership = New System.Windows.Forms.TabPage
        Me.pnlMembershipinfo = New System.Windows.Forms.Panel
        Me.gbMembershipInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlMembershipDataInfo = New System.Windows.Forms.Panel
        Me.lvMembershipInfo = New eZee.Common.eZeeListView(Me.components)
        Me.colhMembershipCategory = New System.Windows.Forms.ColumnHeader
        Me.colhMembershipType = New System.Windows.Forms.ColumnHeader
        Me.colhMembershipNo = New System.Windows.Forms.ColumnHeader
        Me.colhMemIssueDate = New System.Windows.Forms.ColumnHeader
        Me.colhStartDate = New System.Windows.Forms.ColumnHeader
        Me.colhEndDate = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhMemCatUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolMembershipUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhMGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhccategory = New System.Windows.Forms.ColumnHeader
        Me.objcolhccategoryId = New System.Windows.Forms.ColumnHeader
        Me.btnDeleteMembership = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblMembershipName = New System.Windows.Forms.Label
        Me.lnkActivateMembership = New System.Windows.Forms.LinkLabel
        Me.btnEditMembership = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAddMembership = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblMembershipRemark = New System.Windows.Forms.Label
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.cboMemCategory = New System.Windows.Forms.ComboBox
        Me.dtpMemIssueDate = New System.Windows.Forms.DateTimePicker
        Me.lblMemExpiryDate = New System.Windows.Forms.Label
        Me.lblMembershipNo = New System.Windows.Forms.Label
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnAddMembership = New eZee.Common.eZeeGradientButton
        Me.txtMembershipNo = New eZee.TextBox.AlphanumericTextBox
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.lblMemIssueDate = New System.Windows.Forms.Label
        Me.lblMembershipType = New System.Windows.Forms.Label
        Me.cboMembershipType = New System.Windows.Forms.ComboBox
        Me.lblActivationDate = New System.Windows.Forms.Label
        Me.objbtnAddMemCategory = New eZee.Common.eZeeGradientButton
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objgbDatesInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.dtpReinstatementDate = New System.Windows.Forms.DateTimePicker
        Me.lblReinstatement = New System.Windows.Forms.Label
        Me.objelLine2 = New eZee.Common.eZeeLine
        Me.objbtnAddReason = New eZee.Common.eZeeGradientButton
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.objSearchReason = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboChangeReason = New System.Windows.Forms.ComboBox
        Me.lblReason = New System.Windows.Forms.Label
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.dtpActualDate = New System.Windows.Forms.DateTimePicker
        Me.LblActualDate = New System.Windows.Forms.Label
        Me.tabcEmployeeDetails.SuspendLayout()
        Me.tabpMainInfo.SuspendLayout()
        Me.gbEmployeeAllocation.SuspendLayout()
        Me.tabpAdditionalInfo.SuspendLayout()
        Me.gbWorkPermit.SuspendLayout()
        Me.gbDatesDetails.SuspendLayout()
        Me.tabpMembership.SuspendLayout()
        Me.pnlMembershipinfo.SuspendLayout()
        Me.gbMembershipInfo.SuspendLayout()
        Me.pnlMembershipDataInfo.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.objgbDatesInformation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabcEmployeeDetails
        '
        Me.tabcEmployeeDetails.Controls.Add(Me.tabpMainInfo)
        Me.tabcEmployeeDetails.Controls.Add(Me.tabpAdditionalInfo)
        Me.tabcEmployeeDetails.Controls.Add(Me.tabpMembership)
        Me.tabcEmployeeDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcEmployeeDetails.Location = New System.Drawing.Point(12, 76)
        Me.tabcEmployeeDetails.Multiline = True
        Me.tabcEmployeeDetails.Name = "tabcEmployeeDetails"
        Me.tabcEmployeeDetails.SelectedIndex = 0
        Me.tabcEmployeeDetails.Size = New System.Drawing.Size(805, 284)
        Me.tabcEmployeeDetails.TabIndex = 25
        '
        'tabpMainInfo
        '
        Me.tabpMainInfo.Controls.Add(Me.gbEmployeeAllocation)
        Me.tabpMainInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpMainInfo.Name = "tabpMainInfo"
        Me.tabpMainInfo.Size = New System.Drawing.Size(797, 258)
        Me.tabpMainInfo.TabIndex = 0
        Me.tabpMainInfo.Text = "Allocation/Analysis"
        Me.tabpMainInfo.UseVisualStyleBackColor = True
        '
        'gbEmployeeAllocation
        '
        Me.gbEmployeeAllocation.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeAllocation.Checked = False
        Me.gbEmployeeAllocation.CollapseAllExceptThis = False
        Me.gbEmployeeAllocation.CollapsedHoverImage = Nothing
        Me.gbEmployeeAllocation.CollapsedNormalImage = Nothing
        Me.gbEmployeeAllocation.CollapsedPressedImage = Nothing
        Me.gbEmployeeAllocation.CollapseOnLoad = True
        Me.gbEmployeeAllocation.Controls.Add(Me.chkAssignDefaulTranHeads)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchUnits)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchTeam)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblBranch)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchJobGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboSections)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchJob)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblDepartment)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchGradeGrp)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblSection)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchGrades)
        Me.gbEmployeeAllocation.Controls.Add(Me.txtScale)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchClass)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblScale)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchClassGrp)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboDepartment)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchGradeLevel)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblDepartmentGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchCostCenter)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblClassGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchUnitGrp)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblGrade)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchSection)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboClassGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchDepartment)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboGrade)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchSecGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblUnits)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchDeptGrp)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboUnits)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnSearchBranch)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboLevel)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblTeam)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboJobGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboTeams)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboDepartmentGrp)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblUnitGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblJobGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboUnitGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblGradeLevel)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblSectionGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboClass)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboSectionGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnAddGradeGrp)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboStation)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboJob)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnAddCostCenter)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblClass)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnAddScale)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblJob)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnAddJob)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnAddDepartment)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboGradeGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboCostCenter)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblTransactionHead)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnAddGrade)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblGradeGroup)
        Me.gbEmployeeAllocation.Controls.Add(Me.lblCostCenter)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnAddTranHead)
        Me.gbEmployeeAllocation.Controls.Add(Me.cboTransactionHead)
        Me.gbEmployeeAllocation.Controls.Add(Me.objbtnAddGradeLevel)
        Me.gbEmployeeAllocation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbEmployeeAllocation.ExpandedHoverImage = Nothing
        Me.gbEmployeeAllocation.ExpandedNormalImage = Nothing
        Me.gbEmployeeAllocation.ExpandedPressedImage = Nothing
        Me.gbEmployeeAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeAllocation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeAllocation.HeaderHeight = 25
        Me.gbEmployeeAllocation.HeaderMessage = ""
        Me.gbEmployeeAllocation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeAllocation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeAllocation.HeightOnCollapse = 178
        Me.gbEmployeeAllocation.LeftTextSpace = 0
        Me.gbEmployeeAllocation.Location = New System.Drawing.Point(0, 0)
        Me.gbEmployeeAllocation.Margin = New System.Windows.Forms.Padding(0)
        Me.gbEmployeeAllocation.Name = "gbEmployeeAllocation"
        Me.gbEmployeeAllocation.OpenHeight = 260
        Me.gbEmployeeAllocation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeAllocation.ShowBorder = True
        Me.gbEmployeeAllocation.ShowCheckBox = False
        Me.gbEmployeeAllocation.ShowCollapseButton = False
        Me.gbEmployeeAllocation.ShowDefaultBorderColor = True
        Me.gbEmployeeAllocation.ShowDownButton = False
        Me.gbEmployeeAllocation.ShowHeader = True
        Me.gbEmployeeAllocation.Size = New System.Drawing.Size(797, 258)
        Me.gbEmployeeAllocation.TabIndex = 0
        Me.gbEmployeeAllocation.Temp = 0
        Me.gbEmployeeAllocation.Text = "Employee Allocation"
        Me.gbEmployeeAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkAssignDefaulTranHeads
        '
        Me.chkAssignDefaulTranHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAssignDefaulTranHeads.Location = New System.Drawing.Point(536, 199)
        Me.chkAssignDefaulTranHeads.Name = "chkAssignDefaulTranHeads"
        Me.chkAssignDefaulTranHeads.Size = New System.Drawing.Size(248, 17)
        Me.chkAssignDefaulTranHeads.TabIndex = 326
        Me.chkAssignDefaulTranHeads.Text = "Assign Default Transaction Heads"
        Me.chkAssignDefaulTranHeads.UseVisualStyleBackColor = True
        '
        'objbtnSearchUnits
        '
        Me.objbtnSearchUnits.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnits.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUnits.BorderSelected = False
        Me.objbtnSearchUnits.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUnits.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUnits.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUnits.Location = New System.Drawing.Point(479, 37)
        Me.objbtnSearchUnits.Name = "objbtnSearchUnits"
        Me.objbtnSearchUnits.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUnits.TabIndex = 183
        '
        'objbtnSearchTeam
        '
        Me.objbtnSearchTeam.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTeam.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTeam.BorderSelected = False
        Me.objbtnSearchTeam.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTeam.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTeam.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTeam.Location = New System.Drawing.Point(479, 64)
        Me.objbtnSearchTeam.Name = "objbtnSearchTeam"
        Me.objbtnSearchTeam.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTeam.TabIndex = 182
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(14, 39)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(73, 17)
        Me.lblBranch.TabIndex = 124
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchJobGroup
        '
        Me.objbtnSearchJobGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJobGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJobGroup.BorderSelected = False
        Me.objbtnSearchJobGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJobGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJobGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJobGroup.Location = New System.Drawing.Point(479, 91)
        Me.objbtnSearchJobGroup.Name = "objbtnSearchJobGroup"
        Me.objbtnSearchJobGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJobGroup.TabIndex = 181
        '
        'cboSections
        '
        Me.cboSections.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSections.DropDownWidth = 200
        Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSections.FormattingEnabled = True
        Me.cboSections.Location = New System.Drawing.Point(93, 145)
        Me.cboSections.Name = "cboSections"
        Me.cboSections.Size = New System.Drawing.Size(121, 21)
        Me.cboSections.TabIndex = 134
        '
        'objbtnSearchJob
        '
        Me.objbtnSearchJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJob.BorderSelected = False
        Me.objbtnSearchJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJob.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJob.Location = New System.Drawing.Point(506, 118)
        Me.objbtnSearchJob.Name = "objbtnSearchJob"
        Me.objbtnSearchJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJob.TabIndex = 180
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(14, 93)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(73, 17)
        Me.lblDepartment.TabIndex = 128
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchGradeGrp
        '
        Me.objbtnSearchGradeGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGradeGrp.BorderSelected = False
        Me.objbtnSearchGradeGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGradeGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGradeGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGradeGrp.Location = New System.Drawing.Point(506, 145)
        Me.objbtnSearchGradeGrp.Name = "objbtnSearchGradeGrp"
        Me.objbtnSearchGradeGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGradeGrp.TabIndex = 179
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(14, 147)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(73, 17)
        Me.lblSection.TabIndex = 133
        Me.lblSection.Text = "Sections"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchGrades
        '
        Me.objbtnSearchGrades.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGrades.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGrades.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGrades.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGrades.BorderSelected = False
        Me.objbtnSearchGrades.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGrades.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGrades.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGrades.Location = New System.Drawing.Point(506, 172)
        Me.objbtnSearchGrades.Name = "objbtnSearchGrades"
        Me.objbtnSearchGrades.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGrades.TabIndex = 178
        '
        'txtScale
        '
        Me.txtScale.AllowNegative = True
        Me.txtScale.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtScale.DigitsInGroup = 0
        Me.txtScale.Flags = 0
        Me.txtScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScale.Location = New System.Drawing.Point(609, 64)
        Me.txtScale.MaxDecimalPlaces = 6
        Me.txtScale.MaxWholeDigits = 21
        Me.txtScale.Name = "txtScale"
        Me.txtScale.Prefix = ""
        Me.txtScale.RangeMax = 1.7976931348623157E+308
        Me.txtScale.RangeMin = -1.7976931348623157E+308
        Me.txtScale.Size = New System.Drawing.Size(121, 21)
        Me.txtScale.TabIndex = 156
        Me.txtScale.Text = "0"
        Me.txtScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnSearchClass
        '
        Me.objbtnSearchClass.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchClass.BorderSelected = False
        Me.objbtnSearchClass.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchClass.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchClass.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchClass.Location = New System.Drawing.Point(736, 118)
        Me.objbtnSearchClass.Name = "objbtnSearchClass"
        Me.objbtnSearchClass.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchClass.TabIndex = 177
        '
        'lblScale
        '
        Me.lblScale.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScale.Location = New System.Drawing.Point(533, 66)
        Me.lblScale.Name = "lblScale"
        Me.lblScale.Size = New System.Drawing.Size(70, 17)
        Me.lblScale.TabIndex = 155
        Me.lblScale.Text = "Scale"
        Me.lblScale.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchClassGrp
        '
        Me.objbtnSearchClassGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchClassGrp.BorderSelected = False
        Me.objbtnSearchClassGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchClassGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchClassGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchClassGrp.Location = New System.Drawing.Point(736, 91)
        Me.objbtnSearchClassGrp.Name = "objbtnSearchClassGrp"
        Me.objbtnSearchClassGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchClassGrp.TabIndex = 176
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.DropDownWidth = 200
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(93, 91)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(121, 21)
        Me.cboDepartment.TabIndex = 129
        '
        'objbtnSearchGradeLevel
        '
        Me.objbtnSearchGradeLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGradeLevel.BorderSelected = False
        Me.objbtnSearchGradeLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGradeLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGradeLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGradeLevel.Location = New System.Drawing.Point(763, 37)
        Me.objbtnSearchGradeLevel.Name = "objbtnSearchGradeLevel"
        Me.objbtnSearchGradeLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGradeLevel.TabIndex = 175
        '
        'lblDepartmentGroup
        '
        Me.lblDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartmentGroup.Location = New System.Drawing.Point(14, 66)
        Me.lblDepartmentGroup.Name = "lblDepartmentGroup"
        Me.lblDepartmentGroup.Size = New System.Drawing.Size(73, 17)
        Me.lblDepartmentGroup.TabIndex = 126
        Me.lblDepartmentGroup.Text = "Dept. Group"
        Me.lblDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCostCenter
        '
        Me.objbtnSearchCostCenter.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCostCenter.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCostCenter.BorderSelected = False
        Me.objbtnSearchCostCenter.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCostCenter.Location = New System.Drawing.Point(763, 145)
        Me.objbtnSearchCostCenter.Name = "objbtnSearchCostCenter"
        Me.objbtnSearchCostCenter.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCostCenter.TabIndex = 174
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(533, 93)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(70, 17)
        Me.lblClassGroup.TabIndex = 158
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchUnitGrp
        '
        Me.objbtnSearchUnitGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUnitGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUnitGrp.BorderSelected = False
        Me.objbtnSearchUnitGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUnitGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUnitGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUnitGrp.Location = New System.Drawing.Point(220, 172)
        Me.objbtnSearchUnitGrp.Name = "objbtnSearchUnitGrp"
        Me.objbtnSearchUnitGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUnitGrp.TabIndex = 173
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(274, 174)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(72, 17)
        Me.lblGrade.TabIndex = 149
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchSection
        '
        Me.objbtnSearchSection.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSection.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSection.BorderSelected = False
        Me.objbtnSearchSection.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSection.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSection.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSection.Location = New System.Drawing.Point(220, 145)
        Me.objbtnSearchSection.Name = "objbtnSearchSection"
        Me.objbtnSearchSection.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSection.TabIndex = 172
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClassGroup.DropDownWidth = 200
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(609, 91)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboClassGroup.TabIndex = 159
        '
        'objbtnSearchDepartment
        '
        Me.objbtnSearchDepartment.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDepartment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDepartment.BorderSelected = False
        Me.objbtnSearchDepartment.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDepartment.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDepartment.Location = New System.Drawing.Point(247, 91)
        Me.objbtnSearchDepartment.Name = "objbtnSearchDepartment"
        Me.objbtnSearchDepartment.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDepartment.TabIndex = 171
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.DropDownWidth = 200
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(352, 172)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(121, 21)
        Me.cboGrade.TabIndex = 150
        '
        'objbtnSearchSecGroup
        '
        Me.objbtnSearchSecGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchSecGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchSecGroup.BorderSelected = False
        Me.objbtnSearchSecGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchSecGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchSecGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchSecGroup.Location = New System.Drawing.Point(220, 118)
        Me.objbtnSearchSecGroup.Name = "objbtnSearchSecGroup"
        Me.objbtnSearchSecGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchSecGroup.TabIndex = 170
        '
        'lblUnits
        '
        Me.lblUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnits.Location = New System.Drawing.Point(274, 39)
        Me.lblUnits.Name = "lblUnits"
        Me.lblUnits.Size = New System.Drawing.Size(72, 17)
        Me.lblUnits.TabIndex = 137
        Me.lblUnits.Text = "Units"
        Me.lblUnits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchDeptGrp
        '
        Me.objbtnSearchDeptGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeptGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDeptGrp.BorderSelected = False
        Me.objbtnSearchDeptGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDeptGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDeptGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDeptGrp.Location = New System.Drawing.Point(220, 66)
        Me.objbtnSearchDeptGrp.Name = "objbtnSearchDeptGrp"
        Me.objbtnSearchDeptGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDeptGrp.TabIndex = 169
        '
        'cboUnits
        '
        Me.cboUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnits.DropDownWidth = 200
        Me.cboUnits.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnits.FormattingEnabled = True
        Me.cboUnits.Location = New System.Drawing.Point(352, 37)
        Me.cboUnits.Name = "cboUnits"
        Me.cboUnits.Size = New System.Drawing.Size(121, 21)
        Me.cboUnits.TabIndex = 138
        '
        'objbtnSearchBranch
        '
        Me.objbtnSearchBranch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchBranch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchBranch.BorderSelected = False
        Me.objbtnSearchBranch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchBranch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchBranch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchBranch.Location = New System.Drawing.Point(220, 37)
        Me.objbtnSearchBranch.Name = "objbtnSearchBranch"
        Me.objbtnSearchBranch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchBranch.TabIndex = 168
        '
        'cboLevel
        '
        Me.cboLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLevel.DropDownWidth = 200
        Me.cboLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLevel.FormattingEnabled = True
        Me.cboLevel.Location = New System.Drawing.Point(609, 37)
        Me.cboLevel.Name = "cboLevel"
        Me.cboLevel.Size = New System.Drawing.Size(121, 21)
        Me.cboLevel.TabIndex = 153
        '
        'lblTeam
        '
        Me.lblTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.Location = New System.Drawing.Point(274, 66)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(72, 17)
        Me.lblTeam.TabIndex = 139
        Me.lblTeam.Text = "Team"
        Me.lblTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJobGroup.DropDownWidth = 200
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.Location = New System.Drawing.Point(352, 91)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboJobGroup.TabIndex = 142
        '
        'cboTeams
        '
        Me.cboTeams.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTeams.DropDownWidth = 200
        Me.cboTeams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeams.FormattingEnabled = True
        Me.cboTeams.Location = New System.Drawing.Point(352, 64)
        Me.cboTeams.Name = "cboTeams"
        Me.cboTeams.Size = New System.Drawing.Size(121, 21)
        Me.cboTeams.TabIndex = 140
        '
        'cboDepartmentGrp
        '
        Me.cboDepartmentGrp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartmentGrp.DropDownWidth = 200
        Me.cboDepartmentGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartmentGrp.FormattingEnabled = True
        Me.cboDepartmentGrp.Location = New System.Drawing.Point(93, 64)
        Me.cboDepartmentGrp.Name = "cboDepartmentGrp"
        Me.cboDepartmentGrp.Size = New System.Drawing.Size(121, 21)
        Me.cboDepartmentGrp.TabIndex = 127
        '
        'lblUnitGroup
        '
        Me.lblUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitGroup.Location = New System.Drawing.Point(14, 174)
        Me.lblUnitGroup.Name = "lblUnitGroup"
        Me.lblUnitGroup.Size = New System.Drawing.Size(73, 17)
        Me.lblUnitGroup.TabIndex = 135
        Me.lblUnitGroup.Text = "Unit Group"
        Me.lblUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobGroup
        '
        Me.lblJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobGroup.Location = New System.Drawing.Point(274, 93)
        Me.lblJobGroup.Name = "lblJobGroup"
        Me.lblJobGroup.Size = New System.Drawing.Size(72, 17)
        Me.lblJobGroup.TabIndex = 141
        Me.lblJobGroup.Text = "Job Group"
        Me.lblJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnitGroup
        '
        Me.cboUnitGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnitGroup.DropDownWidth = 200
        Me.cboUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGroup.FormattingEnabled = True
        Me.cboUnitGroup.Location = New System.Drawing.Point(93, 172)
        Me.cboUnitGroup.Name = "cboUnitGroup"
        Me.cboUnitGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboUnitGroup.TabIndex = 136
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(533, 39)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(70, 17)
        Me.lblGradeLevel.TabIndex = 152
        Me.lblGradeLevel.Text = "Grade Level"
        Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSectionGroup
        '
        Me.lblSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSectionGroup.Location = New System.Drawing.Point(14, 120)
        Me.lblSectionGroup.Name = "lblSectionGroup"
        Me.lblSectionGroup.Size = New System.Drawing.Size(73, 17)
        Me.lblSectionGroup.TabIndex = 131
        Me.lblSectionGroup.Text = "Sec. Group"
        Me.lblSectionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClass
        '
        Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClass.DropDownWidth = 200
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(609, 118)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(121, 21)
        Me.cboClass.TabIndex = 161
        '
        'cboSectionGroup
        '
        Me.cboSectionGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSectionGroup.DropDownWidth = 200
        Me.cboSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGroup.FormattingEnabled = True
        Me.cboSectionGroup.Location = New System.Drawing.Point(93, 118)
        Me.cboSectionGroup.Name = "cboSectionGroup"
        Me.cboSectionGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboSectionGroup.TabIndex = 132
        '
        'objbtnAddGradeGrp
        '
        Me.objbtnAddGradeGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGradeGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGradeGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGradeGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGradeGrp.BorderSelected = False
        Me.objbtnAddGradeGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGradeGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGradeGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGradeGrp.Location = New System.Drawing.Point(479, 145)
        Me.objbtnAddGradeGrp.Name = "objbtnAddGradeGrp"
        Me.objbtnAddGradeGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGradeGrp.TabIndex = 148
        '
        'cboStation
        '
        Me.cboStation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStation.DropDownWidth = 200
        Me.cboStation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStation.FormattingEnabled = True
        Me.cboStation.Location = New System.Drawing.Point(93, 37)
        Me.cboStation.Name = "cboStation"
        Me.cboStation.Size = New System.Drawing.Size(121, 21)
        Me.cboStation.TabIndex = 125
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 200
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(352, 118)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(121, 21)
        Me.cboJob.TabIndex = 144
        '
        'objbtnAddCostCenter
        '
        Me.objbtnAddCostCenter.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCostCenter.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCostCenter.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCostCenter.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCostCenter.BorderSelected = False
        Me.objbtnAddCostCenter.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCostCenter.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddCostCenter.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCostCenter.Location = New System.Drawing.Point(736, 145)
        Me.objbtnAddCostCenter.Name = "objbtnAddCostCenter"
        Me.objbtnAddCostCenter.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCostCenter.TabIndex = 164
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(533, 120)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(70, 17)
        Me.lblClass.TabIndex = 160
        Me.lblClass.Text = "Class"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddScale
        '
        Me.objbtnAddScale.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddScale.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddScale.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddScale.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddScale.BorderSelected = False
        Me.objbtnAddScale.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddScale.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddScale.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddScale.Location = New System.Drawing.Point(736, 64)
        Me.objbtnAddScale.Name = "objbtnAddScale"
        Me.objbtnAddScale.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddScale.TabIndex = 157
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(274, 120)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(72, 17)
        Me.lblJob.TabIndex = 143
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddJob
        '
        Me.objbtnAddJob.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddJob.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddJob.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddJob.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddJob.BorderSelected = False
        Me.objbtnAddJob.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddJob.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddJob.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddJob.Location = New System.Drawing.Point(479, 118)
        Me.objbtnAddJob.Name = "objbtnAddJob"
        Me.objbtnAddJob.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddJob.TabIndex = 145
        '
        'objbtnAddDepartment
        '
        Me.objbtnAddDepartment.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddDepartment.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddDepartment.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddDepartment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddDepartment.BorderSelected = False
        Me.objbtnAddDepartment.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddDepartment.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddDepartment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddDepartment.Location = New System.Drawing.Point(220, 91)
        Me.objbtnAddDepartment.Name = "objbtnAddDepartment"
        Me.objbtnAddDepartment.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddDepartment.TabIndex = 130
        '
        'cboGradeGroup
        '
        Me.cboGradeGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeGroup.DropDownWidth = 200
        Me.cboGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGroup.FormattingEnabled = True
        Me.cboGradeGroup.Location = New System.Drawing.Point(352, 145)
        Me.cboGradeGroup.Name = "cboGradeGroup"
        Me.cboGradeGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboGradeGroup.TabIndex = 147
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenter.DropDownWidth = 200
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(609, 145)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(121, 21)
        Me.cboCostCenter.TabIndex = 163
        '
        'lblTransactionHead
        '
        Me.lblTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionHead.Location = New System.Drawing.Point(533, 174)
        Me.lblTransactionHead.Name = "lblTransactionHead"
        Me.lblTransactionHead.Size = New System.Drawing.Size(70, 17)
        Me.lblTransactionHead.TabIndex = 165
        Me.lblTransactionHead.Text = "Trans. Head"
        Me.lblTransactionHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddGrade
        '
        Me.objbtnAddGrade.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGrade.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGrade.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGrade.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGrade.BorderSelected = False
        Me.objbtnAddGrade.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGrade.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGrade.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGrade.Location = New System.Drawing.Point(479, 172)
        Me.objbtnAddGrade.Name = "objbtnAddGrade"
        Me.objbtnAddGrade.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGrade.TabIndex = 151
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(274, 147)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(72, 17)
        Me.lblGradeGroup.TabIndex = 146
        Me.lblGradeGroup.Text = "Grade Group"
        Me.lblGradeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(533, 147)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(70, 17)
        Me.lblCostCenter.TabIndex = 162
        Me.lblCostCenter.Text = "Cost Center"
        Me.lblCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddTranHead
        '
        Me.objbtnAddTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddTranHead.BorderSelected = False
        Me.objbtnAddTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddTranHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddTranHead.Location = New System.Drawing.Point(736, 172)
        Me.objbtnAddTranHead.Name = "objbtnAddTranHead"
        Me.objbtnAddTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddTranHead.TabIndex = 167
        Me.objbtnAddTranHead.Visible = False
        '
        'cboTransactionHead
        '
        Me.cboTransactionHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransactionHead.DropDownWidth = 200
        Me.cboTransactionHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransactionHead.FormattingEnabled = True
        Me.cboTransactionHead.Location = New System.Drawing.Point(609, 172)
        Me.cboTransactionHead.Name = "cboTransactionHead"
        Me.cboTransactionHead.Size = New System.Drawing.Size(121, 21)
        Me.cboTransactionHead.TabIndex = 166
        '
        'objbtnAddGradeLevel
        '
        Me.objbtnAddGradeLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGradeLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGradeLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGradeLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGradeLevel.BorderSelected = False
        Me.objbtnAddGradeLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGradeLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGradeLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGradeLevel.Location = New System.Drawing.Point(736, 37)
        Me.objbtnAddGradeLevel.Name = "objbtnAddGradeLevel"
        Me.objbtnAddGradeLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGradeLevel.TabIndex = 154
        '
        'tabpAdditionalInfo
        '
        Me.tabpAdditionalInfo.Controls.Add(Me.gbWorkPermit)
        Me.tabpAdditionalInfo.Controls.Add(Me.gbDatesDetails)
        Me.tabpAdditionalInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpAdditionalInfo.Name = "tabpAdditionalInfo"
        Me.tabpAdditionalInfo.Size = New System.Drawing.Size(797, 258)
        Me.tabpAdditionalInfo.TabIndex = 2
        Me.tabpAdditionalInfo.Text = "Dates/Permit Info."
        Me.tabpAdditionalInfo.UseVisualStyleBackColor = True
        '
        'gbWorkPermit
        '
        Me.gbWorkPermit.BorderColor = System.Drawing.Color.Black
        Me.gbWorkPermit.Checked = False
        Me.gbWorkPermit.CollapseAllExceptThis = False
        Me.gbWorkPermit.CollapsedHoverImage = Nothing
        Me.gbWorkPermit.CollapsedNormalImage = Nothing
        Me.gbWorkPermit.CollapsedPressedImage = Nothing
        Me.gbWorkPermit.CollapseOnLoad = False
        Me.gbWorkPermit.Controls.Add(Me.txtResidentIssuePlace)
        Me.gbWorkPermit.Controls.Add(Me.lblResidentIssuePlace)
        Me.gbWorkPermit.Controls.Add(Me.txtResidentPermitNo)
        Me.gbWorkPermit.Controls.Add(Me.dtpResidentExpiryDate)
        Me.gbWorkPermit.Controls.Add(Me.lblResidentPermitNo)
        Me.gbWorkPermit.Controls.Add(Me.lblResidentExpiryDate)
        Me.gbWorkPermit.Controls.Add(Me.cboResidentIssueCountry)
        Me.gbWorkPermit.Controls.Add(Me.dtpResidentIssueDate)
        Me.gbWorkPermit.Controls.Add(Me.lblResidentIssueCountry)
        Me.gbWorkPermit.Controls.Add(Me.lblResidentIssueDate)
        Me.gbWorkPermit.Controls.Add(Me.elResidentPermit)
        Me.gbWorkPermit.Controls.Add(Me.elWorkPermit)
        Me.gbWorkPermit.Controls.Add(Me.txtPlaceofIssue)
        Me.gbWorkPermit.Controls.Add(Me.dtpExpiryDate)
        Me.gbWorkPermit.Controls.Add(Me.lblExpiryDate)
        Me.gbWorkPermit.Controls.Add(Me.lblPlaceOfIssue)
        Me.gbWorkPermit.Controls.Add(Me.txtWorkPermitNo)
        Me.gbWorkPermit.Controls.Add(Me.lblIssueDate)
        Me.gbWorkPermit.Controls.Add(Me.lblWorkPermitNo)
        Me.gbWorkPermit.Controls.Add(Me.dtpIssueDate)
        Me.gbWorkPermit.Controls.Add(Me.lblIssueCountry)
        Me.gbWorkPermit.Controls.Add(Me.cboIssueCountry)
        Me.gbWorkPermit.Dock = System.Windows.Forms.DockStyle.Right
        Me.gbWorkPermit.ExpandedHoverImage = Nothing
        Me.gbWorkPermit.ExpandedNormalImage = Nothing
        Me.gbWorkPermit.ExpandedPressedImage = Nothing
        Me.gbWorkPermit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbWorkPermit.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbWorkPermit.HeaderHeight = 25
        Me.gbWorkPermit.HeaderMessage = ""
        Me.gbWorkPermit.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbWorkPermit.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbWorkPermit.HeightOnCollapse = 0
        Me.gbWorkPermit.LeftTextSpace = 0
        Me.gbWorkPermit.Location = New System.Drawing.Point(407, 0)
        Me.gbWorkPermit.Name = "gbWorkPermit"
        Me.gbWorkPermit.OpenHeight = 119
        Me.gbWorkPermit.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbWorkPermit.ShowBorder = True
        Me.gbWorkPermit.ShowCheckBox = False
        Me.gbWorkPermit.ShowCollapseButton = False
        Me.gbWorkPermit.ShowDefaultBorderColor = True
        Me.gbWorkPermit.ShowDownButton = False
        Me.gbWorkPermit.ShowHeader = True
        Me.gbWorkPermit.Size = New System.Drawing.Size(390, 258)
        Me.gbWorkPermit.TabIndex = 1
        Me.gbWorkPermit.Temp = 0
        Me.gbWorkPermit.Text = "Permit Information"
        Me.gbWorkPermit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtResidentIssuePlace
        '
        Me.txtResidentIssuePlace.Flags = 0
        Me.txtResidentIssuePlace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResidentIssuePlace.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtResidentIssuePlace.Location = New System.Drawing.Point(93, 217)
        Me.txtResidentIssuePlace.Name = "txtResidentIssuePlace"
        Me.txtResidentIssuePlace.Size = New System.Drawing.Size(290, 21)
        Me.txtResidentIssuePlace.TabIndex = 29
        '
        'lblResidentIssuePlace
        '
        Me.lblResidentIssuePlace.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResidentIssuePlace.Location = New System.Drawing.Point(9, 220)
        Me.lblResidentIssuePlace.Name = "lblResidentIssuePlace"
        Me.lblResidentIssuePlace.Size = New System.Drawing.Size(77, 15)
        Me.lblResidentIssuePlace.TabIndex = 28
        Me.lblResidentIssuePlace.Text = "Place Of Issue"
        Me.lblResidentIssuePlace.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtResidentPermitNo
        '
        Me.txtResidentPermitNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtResidentPermitNo.Flags = 0
        Me.txtResidentPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtResidentPermitNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtResidentPermitNo.Location = New System.Drawing.Point(93, 163)
        Me.txtResidentPermitNo.Name = "txtResidentPermitNo"
        Me.txtResidentPermitNo.Size = New System.Drawing.Size(112, 21)
        Me.txtResidentPermitNo.TabIndex = 25
        '
        'dtpResidentExpiryDate
        '
        Me.dtpResidentExpiryDate.Checked = False
        Me.dtpResidentExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpResidentExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpResidentExpiryDate.Location = New System.Drawing.Point(277, 190)
        Me.dtpResidentExpiryDate.Name = "dtpResidentExpiryDate"
        Me.dtpResidentExpiryDate.ShowCheckBox = True
        Me.dtpResidentExpiryDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpResidentExpiryDate.TabIndex = 33
        '
        'lblResidentPermitNo
        '
        Me.lblResidentPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResidentPermitNo.Location = New System.Drawing.Point(9, 166)
        Me.lblResidentPermitNo.Name = "lblResidentPermitNo"
        Me.lblResidentPermitNo.Size = New System.Drawing.Size(78, 15)
        Me.lblResidentPermitNo.TabIndex = 24
        Me.lblResidentPermitNo.Text = "Permit No."
        Me.lblResidentPermitNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblResidentExpiryDate
        '
        Me.lblResidentExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResidentExpiryDate.Location = New System.Drawing.Point(205, 193)
        Me.lblResidentExpiryDate.Name = "lblResidentExpiryDate"
        Me.lblResidentExpiryDate.Size = New System.Drawing.Size(66, 15)
        Me.lblResidentExpiryDate.TabIndex = 32
        Me.lblResidentExpiryDate.Text = "Expiry Date"
        Me.lblResidentExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResidentIssueCountry
        '
        Me.cboResidentIssueCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResidentIssueCountry.DropDownWidth = 120
        Me.cboResidentIssueCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResidentIssueCountry.FormattingEnabled = True
        Me.cboResidentIssueCountry.Location = New System.Drawing.Point(93, 190)
        Me.cboResidentIssueCountry.Name = "cboResidentIssueCountry"
        Me.cboResidentIssueCountry.Size = New System.Drawing.Size(112, 21)
        Me.cboResidentIssueCountry.TabIndex = 27
        '
        'dtpResidentIssueDate
        '
        Me.dtpResidentIssueDate.Checked = False
        Me.dtpResidentIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpResidentIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpResidentIssueDate.Location = New System.Drawing.Point(277, 163)
        Me.dtpResidentIssueDate.Name = "dtpResidentIssueDate"
        Me.dtpResidentIssueDate.ShowCheckBox = True
        Me.dtpResidentIssueDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpResidentIssueDate.TabIndex = 31
        '
        'lblResidentIssueCountry
        '
        Me.lblResidentIssueCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResidentIssueCountry.Location = New System.Drawing.Point(9, 193)
        Me.lblResidentIssueCountry.Name = "lblResidentIssueCountry"
        Me.lblResidentIssueCountry.Size = New System.Drawing.Size(78, 15)
        Me.lblResidentIssueCountry.TabIndex = 26
        Me.lblResidentIssueCountry.Text = "Issue Country"
        Me.lblResidentIssueCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblResidentIssueDate
        '
        Me.lblResidentIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResidentIssueDate.Location = New System.Drawing.Point(205, 166)
        Me.lblResidentIssueDate.Name = "lblResidentIssueDate"
        Me.lblResidentIssueDate.Size = New System.Drawing.Size(66, 15)
        Me.lblResidentIssueDate.TabIndex = 30
        Me.lblResidentIssueDate.Text = "Issue Date"
        Me.lblResidentIssueDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elResidentPermit
        '
        Me.elResidentPermit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elResidentPermit.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elResidentPermit.Location = New System.Drawing.Point(3, 141)
        Me.elResidentPermit.Name = "elResidentPermit"
        Me.elResidentPermit.Size = New System.Drawing.Size(378, 14)
        Me.elResidentPermit.TabIndex = 23
        Me.elResidentPermit.Text = "Resident Permit"
        '
        'elWorkPermit
        '
        Me.elWorkPermit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elWorkPermit.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elWorkPermit.Location = New System.Drawing.Point(3, 32)
        Me.elWorkPermit.Name = "elWorkPermit"
        Me.elWorkPermit.Size = New System.Drawing.Size(378, 14)
        Me.elWorkPermit.TabIndex = 22
        Me.elWorkPermit.Text = "Work Permit"
        '
        'txtPlaceofIssue
        '
        Me.txtPlaceofIssue.Flags = 0
        Me.txtPlaceofIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlaceofIssue.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPlaceofIssue.Location = New System.Drawing.Point(93, 106)
        Me.txtPlaceofIssue.Name = "txtPlaceofIssue"
        Me.txtPlaceofIssue.Size = New System.Drawing.Size(290, 21)
        Me.txtPlaceofIssue.TabIndex = 5
        '
        'dtpExpiryDate
        '
        Me.dtpExpiryDate.Checked = False
        Me.dtpExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpExpiryDate.Location = New System.Drawing.Point(277, 79)
        Me.dtpExpiryDate.Name = "dtpExpiryDate"
        Me.dtpExpiryDate.ShowCheckBox = True
        Me.dtpExpiryDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpExpiryDate.TabIndex = 9
        '
        'lblExpiryDate
        '
        Me.lblExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpiryDate.Location = New System.Drawing.Point(205, 82)
        Me.lblExpiryDate.Name = "lblExpiryDate"
        Me.lblExpiryDate.Size = New System.Drawing.Size(66, 15)
        Me.lblExpiryDate.TabIndex = 8
        Me.lblExpiryDate.Text = "Expiry Date"
        Me.lblExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPlaceOfIssue
        '
        Me.lblPlaceOfIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlaceOfIssue.Location = New System.Drawing.Point(9, 109)
        Me.lblPlaceOfIssue.Name = "lblPlaceOfIssue"
        Me.lblPlaceOfIssue.Size = New System.Drawing.Size(77, 15)
        Me.lblPlaceOfIssue.TabIndex = 4
        Me.lblPlaceOfIssue.Text = "Place Of Issue"
        Me.lblPlaceOfIssue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtWorkPermitNo
        '
        Me.txtWorkPermitNo.Flags = 0
        Me.txtWorkPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkPermitNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtWorkPermitNo.Location = New System.Drawing.Point(93, 52)
        Me.txtWorkPermitNo.Name = "txtWorkPermitNo"
        Me.txtWorkPermitNo.Size = New System.Drawing.Size(106, 21)
        Me.txtWorkPermitNo.TabIndex = 1
        '
        'lblIssueDate
        '
        Me.lblIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIssueDate.Location = New System.Drawing.Point(205, 55)
        Me.lblIssueDate.Name = "lblIssueDate"
        Me.lblIssueDate.Size = New System.Drawing.Size(66, 15)
        Me.lblIssueDate.TabIndex = 6
        Me.lblIssueDate.Text = "Issue Date"
        Me.lblIssueDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblWorkPermitNo
        '
        Me.lblWorkPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkPermitNo.Location = New System.Drawing.Point(9, 55)
        Me.lblWorkPermitNo.Name = "lblWorkPermitNo"
        Me.lblWorkPermitNo.Size = New System.Drawing.Size(78, 15)
        Me.lblWorkPermitNo.TabIndex = 0
        Me.lblWorkPermitNo.Text = "Permit No."
        Me.lblWorkPermitNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpIssueDate
        '
        Me.dtpIssueDate.Checked = False
        Me.dtpIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpIssueDate.Location = New System.Drawing.Point(277, 52)
        Me.dtpIssueDate.Name = "dtpIssueDate"
        Me.dtpIssueDate.ShowCheckBox = True
        Me.dtpIssueDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpIssueDate.TabIndex = 7
        '
        'lblIssueCountry
        '
        Me.lblIssueCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIssueCountry.Location = New System.Drawing.Point(9, 82)
        Me.lblIssueCountry.Name = "lblIssueCountry"
        Me.lblIssueCountry.Size = New System.Drawing.Size(78, 15)
        Me.lblIssueCountry.TabIndex = 2
        Me.lblIssueCountry.Text = "Issue Country"
        Me.lblIssueCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIssueCountry
        '
        Me.cboIssueCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIssueCountry.DropDownWidth = 120
        Me.cboIssueCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIssueCountry.FormattingEnabled = True
        Me.cboIssueCountry.Location = New System.Drawing.Point(93, 79)
        Me.cboIssueCountry.Name = "cboIssueCountry"
        Me.cboIssueCountry.Size = New System.Drawing.Size(106, 21)
        Me.cboIssueCountry.TabIndex = 3
        '
        'gbDatesDetails
        '
        Me.gbDatesDetails.BorderColor = System.Drawing.Color.Black
        Me.gbDatesDetails.Checked = False
        Me.gbDatesDetails.CollapseAllExceptThis = False
        Me.gbDatesDetails.CollapsedHoverImage = Nothing
        Me.gbDatesDetails.CollapsedNormalImage = Nothing
        Me.gbDatesDetails.CollapsedPressedImage = Nothing
        Me.gbDatesDetails.CollapseOnLoad = False
        Me.gbDatesDetails.Controls.Add(Me.lblBirthDate)
        Me.gbDatesDetails.Controls.Add(Me.txtBirthDate)
        Me.gbDatesDetails.Controls.Add(Me.lblAppointmentdate)
        Me.gbDatesDetails.Controls.Add(Me.lblRetirementDate)
        Me.gbDatesDetails.Controls.Add(Me.txtAppDate)
        Me.gbDatesDetails.Controls.Add(Me.dtpConfirmationDate)
        Me.gbDatesDetails.Controls.Add(Me.dtpRetirementDate)
        Me.gbDatesDetails.Controls.Add(Me.lblConfirmationDate)
        Me.gbDatesDetails.Controls.Add(Me.chkExclude)
        Me.gbDatesDetails.Controls.Add(Me.dtpLeavingDate)
        Me.gbDatesDetails.Controls.Add(Me.lblLeavingDate)
        Me.gbDatesDetails.Controls.Add(Me.dtpEndEmplDate)
        Me.gbDatesDetails.Controls.Add(Me.lblProbationTo)
        Me.gbDatesDetails.Controls.Add(Me.lblEmplDate)
        Me.gbDatesDetails.Controls.Add(Me.dtpProbationDateTo)
        Me.gbDatesDetails.Controls.Add(Me.lblProbationFrom)
        Me.gbDatesDetails.Controls.Add(Me.dtpProbationDateFrom)
        Me.gbDatesDetails.Dock = System.Windows.Forms.DockStyle.Left
        Me.gbDatesDetails.ExpandedHoverImage = Nothing
        Me.gbDatesDetails.ExpandedNormalImage = Nothing
        Me.gbDatesDetails.ExpandedPressedImage = Nothing
        Me.gbDatesDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDatesDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDatesDetails.HeaderHeight = 25
        Me.gbDatesDetails.HeaderMessage = ""
        Me.gbDatesDetails.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbDatesDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDatesDetails.HeightOnCollapse = 0
        Me.gbDatesDetails.LeftTextSpace = 0
        Me.gbDatesDetails.Location = New System.Drawing.Point(0, 0)
        Me.gbDatesDetails.Name = "gbDatesDetails"
        Me.gbDatesDetails.OpenHeight = 198
        Me.gbDatesDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDatesDetails.ShowBorder = True
        Me.gbDatesDetails.ShowCheckBox = False
        Me.gbDatesDetails.ShowCollapseButton = False
        Me.gbDatesDetails.ShowDefaultBorderColor = True
        Me.gbDatesDetails.ShowDownButton = False
        Me.gbDatesDetails.ShowHeader = True
        Me.gbDatesDetails.Size = New System.Drawing.Size(405, 258)
        Me.gbDatesDetails.TabIndex = 3
        Me.gbDatesDetails.Temp = 0
        Me.gbDatesDetails.Text = "Employee Dates"
        Me.gbDatesDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBirthDate
        '
        Me.lblBirthDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBirthDate.Location = New System.Drawing.Point(13, 64)
        Me.lblBirthDate.Name = "lblBirthDate"
        Me.lblBirthDate.Size = New System.Drawing.Size(116, 17)
        Me.lblBirthDate.TabIndex = 185
        Me.lblBirthDate.Text = "Birthdate"
        Me.lblBirthDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBirthDate
        '
        Me.txtBirthDate.BackColor = System.Drawing.Color.White
        Me.txtBirthDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBirthDate.Location = New System.Drawing.Point(135, 61)
        Me.txtBirthDate.Name = "txtBirthDate"
        Me.txtBirthDate.ReadOnly = True
        Me.txtBirthDate.Size = New System.Drawing.Size(107, 21)
        Me.txtBirthDate.TabIndex = 186
        '
        'lblAppointmentdate
        '
        Me.lblAppointmentdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointmentdate.Location = New System.Drawing.Point(13, 37)
        Me.lblAppointmentdate.Name = "lblAppointmentdate"
        Me.lblAppointmentdate.Size = New System.Drawing.Size(116, 17)
        Me.lblAppointmentdate.TabIndex = 183
        Me.lblAppointmentdate.Text = "Appointment Date"
        Me.lblAppointmentdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblRetirementDate
        '
        Me.lblRetirementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRetirementDate.Location = New System.Drawing.Point(13, 221)
        Me.lblRetirementDate.Name = "lblRetirementDate"
        Me.lblRetirementDate.Size = New System.Drawing.Size(116, 17)
        Me.lblRetirementDate.TabIndex = 14
        Me.lblRetirementDate.Text = "Retirement Date"
        Me.lblRetirementDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAppDate
        '
        Me.txtAppDate.BackColor = System.Drawing.Color.White
        Me.txtAppDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAppDate.Location = New System.Drawing.Point(135, 37)
        Me.txtAppDate.Name = "txtAppDate"
        Me.txtAppDate.ReadOnly = True
        Me.txtAppDate.Size = New System.Drawing.Size(107, 21)
        Me.txtAppDate.TabIndex = 184
        '
        'dtpConfirmationDate
        '
        Me.dtpConfirmationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpConfirmationDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpConfirmationDate.Location = New System.Drawing.Point(135, 88)
        Me.dtpConfirmationDate.Name = "dtpConfirmationDate"
        Me.dtpConfirmationDate.Size = New System.Drawing.Size(107, 21)
        Me.dtpConfirmationDate.TabIndex = 26
        '
        'dtpRetirementDate
        '
        Me.dtpRetirementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpRetirementDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpRetirementDate.Location = New System.Drawing.Point(135, 223)
        Me.dtpRetirementDate.Name = "dtpRetirementDate"
        Me.dtpRetirementDate.Size = New System.Drawing.Size(107, 21)
        Me.dtpRetirementDate.TabIndex = 15
        '
        'lblConfirmationDate
        '
        Me.lblConfirmationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblConfirmationDate.Location = New System.Drawing.Point(13, 90)
        Me.lblConfirmationDate.Name = "lblConfirmationDate"
        Me.lblConfirmationDate.Size = New System.Drawing.Size(116, 17)
        Me.lblConfirmationDate.TabIndex = 25
        Me.lblConfirmationDate.Text = "Confirmation Date"
        Me.lblConfirmationDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkExclude
        '
        Me.chkExclude.CheckAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkExclude.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExclude.Location = New System.Drawing.Point(248, 170)
        Me.chkExclude.Name = "chkExclude"
        Me.chkExclude.Size = New System.Drawing.Size(153, 43)
        Me.chkExclude.TabIndex = 36
        Me.chkExclude.Text = "Exclude From Payroll Process for EOC Date / Leaving Date"
        Me.chkExclude.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkExclude.UseVisualStyleBackColor = True
        '
        'dtpLeavingDate
        '
        Me.dtpLeavingDate.Checked = False
        Me.dtpLeavingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpLeavingDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpLeavingDate.Location = New System.Drawing.Point(135, 196)
        Me.dtpLeavingDate.Name = "dtpLeavingDate"
        Me.dtpLeavingDate.ShowCheckBox = True
        Me.dtpLeavingDate.Size = New System.Drawing.Size(107, 21)
        Me.dtpLeavingDate.TabIndex = 5
        '
        'lblLeavingDate
        '
        Me.lblLeavingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeavingDate.Location = New System.Drawing.Point(13, 198)
        Me.lblLeavingDate.Name = "lblLeavingDate"
        Me.lblLeavingDate.Size = New System.Drawing.Size(116, 17)
        Me.lblLeavingDate.TabIndex = 4
        Me.lblLeavingDate.Text = "Leaving Date"
        Me.lblLeavingDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEndEmplDate
        '
        Me.dtpEndEmplDate.Checked = False
        Me.dtpEndEmplDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndEmplDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndEmplDate.Location = New System.Drawing.Point(135, 169)
        Me.dtpEndEmplDate.Name = "dtpEndEmplDate"
        Me.dtpEndEmplDate.ShowCheckBox = True
        Me.dtpEndEmplDate.Size = New System.Drawing.Size(107, 21)
        Me.dtpEndEmplDate.TabIndex = 28
        '
        'lblProbationTo
        '
        Me.lblProbationTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProbationTo.Location = New System.Drawing.Point(13, 144)
        Me.lblProbationTo.Name = "lblProbationTo"
        Me.lblProbationTo.Size = New System.Drawing.Size(116, 17)
        Me.lblProbationTo.TabIndex = 12
        Me.lblProbationTo.Text = "Prob. Date To"
        Me.lblProbationTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmplDate
        '
        Me.lblEmplDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmplDate.Location = New System.Drawing.Point(13, 171)
        Me.lblEmplDate.Name = "lblEmplDate"
        Me.lblEmplDate.Size = New System.Drawing.Size(116, 17)
        Me.lblEmplDate.TabIndex = 27
        Me.lblEmplDate.Text = "End Of Contract Date"
        Me.lblEmplDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpProbationDateTo
        '
        Me.dtpProbationDateTo.Checked = False
        Me.dtpProbationDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProbationDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpProbationDateTo.Location = New System.Drawing.Point(135, 142)
        Me.dtpProbationDateTo.Name = "dtpProbationDateTo"
        Me.dtpProbationDateTo.ShowCheckBox = True
        Me.dtpProbationDateTo.Size = New System.Drawing.Size(107, 21)
        Me.dtpProbationDateTo.TabIndex = 13
        '
        'lblProbationFrom
        '
        Me.lblProbationFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProbationFrom.Location = New System.Drawing.Point(13, 117)
        Me.lblProbationFrom.Name = "lblProbationFrom"
        Me.lblProbationFrom.Size = New System.Drawing.Size(116, 17)
        Me.lblProbationFrom.TabIndex = 2
        Me.lblProbationFrom.Text = "Probation Date From"
        Me.lblProbationFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpProbationDateFrom
        '
        Me.dtpProbationDateFrom.Checked = False
        Me.dtpProbationDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpProbationDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpProbationDateFrom.Location = New System.Drawing.Point(135, 115)
        Me.dtpProbationDateFrom.Name = "dtpProbationDateFrom"
        Me.dtpProbationDateFrom.ShowCheckBox = True
        Me.dtpProbationDateFrom.Size = New System.Drawing.Size(107, 21)
        Me.dtpProbationDateFrom.TabIndex = 3
        '
        'tabpMembership
        '
        Me.tabpMembership.Controls.Add(Me.pnlMembershipinfo)
        Me.tabpMembership.Location = New System.Drawing.Point(4, 22)
        Me.tabpMembership.Name = "tabpMembership"
        Me.tabpMembership.Size = New System.Drawing.Size(797, 258)
        Me.tabpMembership.TabIndex = 4
        Me.tabpMembership.Text = "Membership"
        Me.tabpMembership.UseVisualStyleBackColor = True
        '
        'pnlMembershipinfo
        '
        Me.pnlMembershipinfo.Controls.Add(Me.gbMembershipInfo)
        Me.pnlMembershipinfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMembershipinfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMembershipinfo.Name = "pnlMembershipinfo"
        Me.pnlMembershipinfo.Size = New System.Drawing.Size(797, 258)
        Me.pnlMembershipinfo.TabIndex = 1
        '
        'gbMembershipInfo
        '
        Me.gbMembershipInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMembershipInfo.Checked = False
        Me.gbMembershipInfo.CollapseAllExceptThis = False
        Me.gbMembershipInfo.CollapsedHoverImage = Nothing
        Me.gbMembershipInfo.CollapsedNormalImage = Nothing
        Me.gbMembershipInfo.CollapsedPressedImage = Nothing
        Me.gbMembershipInfo.CollapseOnLoad = False
        Me.gbMembershipInfo.Controls.Add(Me.pnlMembershipDataInfo)
        Me.gbMembershipInfo.Controls.Add(Me.btnDeleteMembership)
        Me.gbMembershipInfo.Controls.Add(Me.lblMembershipName)
        Me.gbMembershipInfo.Controls.Add(Me.lnkActivateMembership)
        Me.gbMembershipInfo.Controls.Add(Me.btnEditMembership)
        Me.gbMembershipInfo.Controls.Add(Me.btnAddMembership)
        Me.gbMembershipInfo.Controls.Add(Me.lblMembershipRemark)
        Me.gbMembershipInfo.Controls.Add(Me.dtpStartDate)
        Me.gbMembershipInfo.Controls.Add(Me.cboMemCategory)
        Me.gbMembershipInfo.Controls.Add(Me.dtpMemIssueDate)
        Me.gbMembershipInfo.Controls.Add(Me.lblMemExpiryDate)
        Me.gbMembershipInfo.Controls.Add(Me.lblMembershipNo)
        Me.gbMembershipInfo.Controls.Add(Me.txtRemark)
        Me.gbMembershipInfo.Controls.Add(Me.objbtnAddMembership)
        Me.gbMembershipInfo.Controls.Add(Me.txtMembershipNo)
        Me.gbMembershipInfo.Controls.Add(Me.dtpEndDate)
        Me.gbMembershipInfo.Controls.Add(Me.lblMemIssueDate)
        Me.gbMembershipInfo.Controls.Add(Me.lblMembershipType)
        Me.gbMembershipInfo.Controls.Add(Me.cboMembershipType)
        Me.gbMembershipInfo.Controls.Add(Me.lblActivationDate)
        Me.gbMembershipInfo.Controls.Add(Me.objbtnAddMemCategory)
        Me.gbMembershipInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbMembershipInfo.ExpandedHoverImage = Nothing
        Me.gbMembershipInfo.ExpandedNormalImage = Nothing
        Me.gbMembershipInfo.ExpandedPressedImage = Nothing
        Me.gbMembershipInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMembershipInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMembershipInfo.HeaderHeight = 25
        Me.gbMembershipInfo.HeaderMessage = ""
        Me.gbMembershipInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMembershipInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMembershipInfo.HeightOnCollapse = 0
        Me.gbMembershipInfo.LeftTextSpace = 0
        Me.gbMembershipInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbMembershipInfo.Name = "gbMembershipInfo"
        Me.gbMembershipInfo.OpenHeight = 351
        Me.gbMembershipInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMembershipInfo.ShowBorder = True
        Me.gbMembershipInfo.ShowCheckBox = False
        Me.gbMembershipInfo.ShowCollapseButton = False
        Me.gbMembershipInfo.ShowDefaultBorderColor = True
        Me.gbMembershipInfo.ShowDownButton = False
        Me.gbMembershipInfo.ShowHeader = True
        Me.gbMembershipInfo.Size = New System.Drawing.Size(797, 258)
        Me.gbMembershipInfo.TabIndex = 0
        Me.gbMembershipInfo.Temp = 0
        Me.gbMembershipInfo.Text = "Memberships"
        Me.gbMembershipInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMembershipDataInfo
        '
        Me.pnlMembershipDataInfo.Controls.Add(Me.lvMembershipInfo)
        Me.pnlMembershipDataInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMembershipDataInfo.Location = New System.Drawing.Point(104, 124)
        Me.pnlMembershipDataInfo.Name = "pnlMembershipDataInfo"
        Me.pnlMembershipDataInfo.Size = New System.Drawing.Size(658, 128)
        Me.pnlMembershipDataInfo.TabIndex = 48
        '
        'lvMembershipInfo
        '
        Me.lvMembershipInfo.BackColorOnChecked = True
        Me.lvMembershipInfo.ColumnHeaders = Nothing
        Me.lvMembershipInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhMembershipCategory, Me.colhMembershipType, Me.colhMembershipNo, Me.colhMemIssueDate, Me.colhStartDate, Me.colhEndDate, Me.colhRemark, Me.objcolhMemCatUnkid, Me.objcolMembershipUnkid, Me.objcolhMGUID, Me.objcolhccategory, Me.objcolhccategoryId})
        Me.lvMembershipInfo.CompulsoryColumns = ""
        Me.lvMembershipInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvMembershipInfo.FullRowSelect = True
        Me.lvMembershipInfo.GridLines = True
        Me.lvMembershipInfo.GroupingColumn = Nothing
        Me.lvMembershipInfo.HideSelection = False
        Me.lvMembershipInfo.Location = New System.Drawing.Point(0, 0)
        Me.lvMembershipInfo.MinColumnWidth = 50
        Me.lvMembershipInfo.MultiSelect = False
        Me.lvMembershipInfo.Name = "lvMembershipInfo"
        Me.lvMembershipInfo.OptionalColumns = ""
        Me.lvMembershipInfo.ShowMoreItem = False
        Me.lvMembershipInfo.ShowSaveItem = False
        Me.lvMembershipInfo.ShowSelectAll = True
        Me.lvMembershipInfo.ShowSizeAllColumnsToFit = True
        Me.lvMembershipInfo.Size = New System.Drawing.Size(658, 128)
        Me.lvMembershipInfo.Sortable = True
        Me.lvMembershipInfo.TabIndex = 0
        Me.lvMembershipInfo.UseCompatibleStateImageBehavior = False
        Me.lvMembershipInfo.View = System.Windows.Forms.View.Details
        '
        'colhMembershipCategory
        '
        Me.colhMembershipCategory.Tag = "colhMembershipCategory"
        Me.colhMembershipCategory.Text = "Mem. Category"
        Me.colhMembershipCategory.Width = 110
        '
        'colhMembershipType
        '
        Me.colhMembershipType.Tag = "colhMembershipType"
        Me.colhMembershipType.Text = "Membership"
        Me.colhMembershipType.Width = 100
        '
        'colhMembershipNo
        '
        Me.colhMembershipNo.Tag = "colhMembershipNo"
        Me.colhMembershipNo.Text = "Mem. No"
        Me.colhMembershipNo.Width = 80
        '
        'colhMemIssueDate
        '
        Me.colhMemIssueDate.Tag = "colhMemIssueDate"
        Me.colhMemIssueDate.Text = "Issue Date"
        Me.colhMemIssueDate.Width = 80
        '
        'colhStartDate
        '
        Me.colhStartDate.Tag = "colhStartDate"
        Me.colhStartDate.Text = "Start Date"
        Me.colhStartDate.Width = 80
        '
        'colhEndDate
        '
        Me.colhEndDate.Tag = "colhEndDate"
        Me.colhEndDate.Text = "End Date"
        Me.colhEndDate.Width = 80
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Remark"
        Me.colhRemark.Width = 0
        '
        'objcolhMemCatUnkid
        '
        Me.objcolhMemCatUnkid.Tag = "objcolhMemCatUnkid"
        Me.objcolhMemCatUnkid.Text = ""
        Me.objcolhMemCatUnkid.Width = 0
        '
        'objcolMembershipUnkid
        '
        Me.objcolMembershipUnkid.Tag = "objcolMembershipUnkid"
        Me.objcolMembershipUnkid.Text = ""
        Me.objcolMembershipUnkid.Width = 0
        '
        'objcolhMGUID
        '
        Me.objcolhMGUID.Tag = "objcolhMGUID"
        Me.objcolhMGUID.Text = ""
        Me.objcolhMGUID.Width = 0
        '
        'objcolhccategory
        '
        Me.objcolhccategory.Tag = "objcolhccategory"
        Me.objcolhccategory.Width = 0
        '
        'objcolhccategoryId
        '
        Me.objcolhccategoryId.Tag = "objcolhccategoryId"
        Me.objcolhccategoryId.Text = ""
        Me.objcolhccategoryId.Width = 0
        '
        'btnDeleteMembership
        '
        Me.btnDeleteMembership.BackColor = System.Drawing.Color.White
        Me.btnDeleteMembership.BackgroundImage = CType(resources.GetObject("btnDeleteMembership.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteMembership.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteMembership.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteMembership.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteMembership.FlatAppearance.BorderSize = 0
        Me.btnDeleteMembership.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteMembership.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteMembership.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteMembership.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteMembership.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteMembership.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteMembership.Location = New System.Drawing.Point(675, 91)
        Me.btnDeleteMembership.Name = "btnDeleteMembership"
        Me.btnDeleteMembership.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteMembership.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteMembership.Size = New System.Drawing.Size(87, 29)
        Me.btnDeleteMembership.TabIndex = 16
        Me.btnDeleteMembership.Text = "&Delete"
        Me.btnDeleteMembership.UseVisualStyleBackColor = True
        '
        'lblMembershipName
        '
        Me.lblMembershipName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembershipName.Location = New System.Drawing.Point(12, 39)
        Me.lblMembershipName.Name = "lblMembershipName"
        Me.lblMembershipName.Size = New System.Drawing.Size(86, 16)
        Me.lblMembershipName.TabIndex = 0
        Me.lblMembershipName.Text = "Mem. Category"
        Me.lblMembershipName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkActivateMembership
        '
        Me.lnkActivateMembership.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkActivateMembership.BackColor = System.Drawing.Color.Transparent
        Me.lnkActivateMembership.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkActivateMembership.Location = New System.Drawing.Point(595, 4)
        Me.lnkActivateMembership.Name = "lnkActivateMembership"
        Me.lnkActivateMembership.Size = New System.Drawing.Size(192, 17)
        Me.lnkActivateMembership.TabIndex = 61
        Me.lnkActivateMembership.TabStop = True
        Me.lnkActivateMembership.Text = "Activate Membership"
        Me.lnkActivateMembership.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnEditMembership
        '
        Me.btnEditMembership.BackColor = System.Drawing.Color.White
        Me.btnEditMembership.BackgroundImage = CType(resources.GetObject("btnEditMembership.BackgroundImage"), System.Drawing.Image)
        Me.btnEditMembership.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEditMembership.BorderColor = System.Drawing.Color.Empty
        Me.btnEditMembership.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEditMembership.FlatAppearance.BorderSize = 0
        Me.btnEditMembership.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditMembership.ForeColor = System.Drawing.Color.Black
        Me.btnEditMembership.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEditMembership.GradientForeColor = System.Drawing.Color.Black
        Me.btnEditMembership.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditMembership.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEditMembership.Location = New System.Drawing.Point(582, 91)
        Me.btnEditMembership.Name = "btnEditMembership"
        Me.btnEditMembership.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEditMembership.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEditMembership.Size = New System.Drawing.Size(87, 29)
        Me.btnEditMembership.TabIndex = 15
        Me.btnEditMembership.Text = "&Edit"
        Me.btnEditMembership.UseVisualStyleBackColor = True
        '
        'btnAddMembership
        '
        Me.btnAddMembership.BackColor = System.Drawing.Color.White
        Me.btnAddMembership.BackgroundImage = CType(resources.GetObject("btnAddMembership.BackgroundImage"), System.Drawing.Image)
        Me.btnAddMembership.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddMembership.BorderColor = System.Drawing.Color.Empty
        Me.btnAddMembership.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddMembership.FlatAppearance.BorderSize = 0
        Me.btnAddMembership.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddMembership.ForeColor = System.Drawing.Color.Black
        Me.btnAddMembership.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddMembership.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddMembership.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddMembership.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddMembership.Location = New System.Drawing.Point(489, 91)
        Me.btnAddMembership.Name = "btnAddMembership"
        Me.btnAddMembership.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddMembership.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddMembership.Size = New System.Drawing.Size(87, 30)
        Me.btnAddMembership.TabIndex = 14
        Me.btnAddMembership.Text = "&Add"
        Me.btnAddMembership.UseVisualStyleBackColor = True
        '
        'lblMembershipRemark
        '
        Me.lblMembershipRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembershipRemark.Location = New System.Drawing.Point(12, 98)
        Me.lblMembershipRemark.Name = "lblMembershipRemark"
        Me.lblMembershipRemark.Size = New System.Drawing.Size(86, 16)
        Me.lblMembershipRemark.TabIndex = 12
        Me.lblMembershipRemark.Text = "Remark"
        Me.lblMembershipRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(104, 64)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(136, 21)
        Me.dtpStartDate.TabIndex = 9
        '
        'cboMemCategory
        '
        Me.cboMemCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMemCategory.DropDownWidth = 180
        Me.cboMemCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMemCategory.FormattingEnabled = True
        Me.cboMemCategory.Location = New System.Drawing.Point(104, 37)
        Me.cboMemCategory.Name = "cboMemCategory"
        Me.cboMemCategory.Size = New System.Drawing.Size(136, 21)
        Me.cboMemCategory.TabIndex = 1
        '
        'dtpMemIssueDate
        '
        Me.dtpMemIssueDate.Checked = False
        Me.dtpMemIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpMemIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpMemIssueDate.Location = New System.Drawing.Point(365, 64)
        Me.dtpMemIssueDate.Name = "dtpMemIssueDate"
        Me.dtpMemIssueDate.ShowCheckBox = True
        Me.dtpMemIssueDate.Size = New System.Drawing.Size(136, 21)
        Me.dtpMemIssueDate.TabIndex = 7
        '
        'lblMemExpiryDate
        '
        Me.lblMemExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMemExpiryDate.Location = New System.Drawing.Point(534, 66)
        Me.lblMemExpiryDate.Name = "lblMemExpiryDate"
        Me.lblMemExpiryDate.Size = New System.Drawing.Size(86, 16)
        Me.lblMemExpiryDate.TabIndex = 10
        Me.lblMemExpiryDate.Text = "Expiry Date"
        Me.lblMemExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMembershipNo
        '
        Me.lblMembershipNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembershipNo.Location = New System.Drawing.Point(534, 39)
        Me.lblMembershipNo.Name = "lblMembershipNo"
        Me.lblMembershipNo.Size = New System.Drawing.Size(86, 16)
        Me.lblMembershipNo.TabIndex = 4
        Me.lblMembershipNo.Text = "Membership No."
        Me.lblMembershipNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(104, 96)
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(379, 21)
        Me.txtRemark.TabIndex = 13
        '
        'objbtnAddMembership
        '
        Me.objbtnAddMembership.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddMembership.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddMembership.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddMembership.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddMembership.BorderSelected = False
        Me.objbtnAddMembership.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddMembership.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddMembership.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddMembership.Location = New System.Drawing.Point(507, 37)
        Me.objbtnAddMembership.Name = "objbtnAddMembership"
        Me.objbtnAddMembership.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddMembership.TabIndex = 57
        '
        'txtMembershipNo
        '
        Me.txtMembershipNo.Flags = 0
        Me.txtMembershipNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMembershipNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMembershipNo.Location = New System.Drawing.Point(626, 37)
        Me.txtMembershipNo.Name = "txtMembershipNo"
        Me.txtMembershipNo.Size = New System.Drawing.Size(136, 21)
        Me.txtMembershipNo.TabIndex = 5
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(626, 64)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(136, 21)
        Me.dtpEndDate.TabIndex = 11
        '
        'lblMemIssueDate
        '
        Me.lblMemIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMemIssueDate.Location = New System.Drawing.Point(273, 66)
        Me.lblMemIssueDate.Name = "lblMemIssueDate"
        Me.lblMemIssueDate.Size = New System.Drawing.Size(86, 16)
        Me.lblMemIssueDate.TabIndex = 6
        Me.lblMemIssueDate.Text = "Issue Date"
        Me.lblMemIssueDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMembershipType
        '
        Me.lblMembershipType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembershipType.Location = New System.Drawing.Point(273, 39)
        Me.lblMembershipType.Name = "lblMembershipType"
        Me.lblMembershipType.Size = New System.Drawing.Size(86, 16)
        Me.lblMembershipType.TabIndex = 2
        Me.lblMembershipType.Text = "Membership "
        Me.lblMembershipType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembershipType
        '
        Me.cboMembershipType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembershipType.DropDownWidth = 180
        Me.cboMembershipType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembershipType.FormattingEnabled = True
        Me.cboMembershipType.Location = New System.Drawing.Point(365, 37)
        Me.cboMembershipType.Name = "cboMembershipType"
        Me.cboMembershipType.Size = New System.Drawing.Size(136, 21)
        Me.cboMembershipType.TabIndex = 3
        '
        'lblActivationDate
        '
        Me.lblActivationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActivationDate.Location = New System.Drawing.Point(12, 66)
        Me.lblActivationDate.Name = "lblActivationDate"
        Me.lblActivationDate.Size = New System.Drawing.Size(86, 16)
        Me.lblActivationDate.TabIndex = 8
        Me.lblActivationDate.Text = "Start Date"
        Me.lblActivationDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddMemCategory
        '
        Me.objbtnAddMemCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddMemCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddMemCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddMemCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddMemCategory.BorderSelected = False
        Me.objbtnAddMemCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddMemCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddMemCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddMemCategory.Location = New System.Drawing.Point(246, 37)
        Me.objbtnAddMemCategory.Name = "objbtnAddMemCategory"
        Me.objbtnAddMemCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddMemCategory.TabIndex = 56
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objgbDatesInformation)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(829, 455)
        Me.pnlMain.TabIndex = 26
        '
        'objgbDatesInformation
        '
        Me.objgbDatesInformation.BorderColor = System.Drawing.Color.Black
        Me.objgbDatesInformation.Checked = False
        Me.objgbDatesInformation.CollapseAllExceptThis = False
        Me.objgbDatesInformation.CollapsedHoverImage = Nothing
        Me.objgbDatesInformation.CollapsedNormalImage = Nothing
        Me.objgbDatesInformation.CollapsedPressedImage = Nothing
        Me.objgbDatesInformation.CollapseOnLoad = False
        Me.objgbDatesInformation.Controls.Add(Me.dtpActualDate)
        Me.objgbDatesInformation.Controls.Add(Me.LblActualDate)
        Me.objgbDatesInformation.Controls.Add(Me.dtpReinstatementDate)
        Me.objgbDatesInformation.Controls.Add(Me.lblReinstatement)
        Me.objgbDatesInformation.Controls.Add(Me.objelLine2)
        Me.objgbDatesInformation.Controls.Add(Me.tabcEmployeeDetails)
        Me.objgbDatesInformation.Controls.Add(Me.objbtnAddReason)
        Me.objgbDatesInformation.Controls.Add(Me.objelLine1)
        Me.objgbDatesInformation.Controls.Add(Me.objSearchReason)
        Me.objgbDatesInformation.Controls.Add(Me.objbtnSearchEmployee)
        Me.objgbDatesInformation.Controls.Add(Me.cboChangeReason)
        Me.objgbDatesInformation.Controls.Add(Me.lblReason)
        Me.objgbDatesInformation.Controls.Add(Me.dtpEffectiveDate)
        Me.objgbDatesInformation.Controls.Add(Me.cboEmployee)
        Me.objgbDatesInformation.Controls.Add(Me.lblEffectiveDate)
        Me.objgbDatesInformation.Controls.Add(Me.lblEmployee)
        Me.objgbDatesInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objgbDatesInformation.ExpandedHoverImage = Nothing
        Me.objgbDatesInformation.ExpandedNormalImage = Nothing
        Me.objgbDatesInformation.ExpandedPressedImage = Nothing
        Me.objgbDatesInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbDatesInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbDatesInformation.HeaderHeight = 25
        Me.objgbDatesInformation.HeaderMessage = ""
        Me.objgbDatesInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.objgbDatesInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbDatesInformation.HeightOnCollapse = 0
        Me.objgbDatesInformation.LeftTextSpace = 0
        Me.objgbDatesInformation.Location = New System.Drawing.Point(0, 0)
        Me.objgbDatesInformation.Name = "objgbDatesInformation"
        Me.objgbDatesInformation.OpenHeight = 300
        Me.objgbDatesInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbDatesInformation.ShowBorder = True
        Me.objgbDatesInformation.ShowCheckBox = False
        Me.objgbDatesInformation.ShowCollapseButton = False
        Me.objgbDatesInformation.ShowDefaultBorderColor = True
        Me.objgbDatesInformation.ShowDownButton = False
        Me.objgbDatesInformation.ShowHeader = True
        Me.objgbDatesInformation.Size = New System.Drawing.Size(829, 405)
        Me.objgbDatesInformation.TabIndex = 27
        Me.objgbDatesInformation.Temp = 0
        Me.objgbDatesInformation.Text = "Re-Hire Employee"
        Me.objgbDatesInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpReinstatementDate
        '
        Me.dtpReinstatementDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpReinstatementDate.Checked = False
        Me.dtpReinstatementDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpReinstatementDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpReinstatementDate.Location = New System.Drawing.Point(716, 37)
        Me.dtpReinstatementDate.Name = "dtpReinstatementDate"
        Me.dtpReinstatementDate.ShowCheckBox = True
        Me.dtpReinstatementDate.Size = New System.Drawing.Size(102, 21)
        Me.dtpReinstatementDate.TabIndex = 216
        '
        'lblReinstatement
        '
        Me.lblReinstatement.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReinstatement.Location = New System.Drawing.Point(602, 40)
        Me.lblReinstatement.Name = "lblReinstatement"
        Me.lblReinstatement.Size = New System.Drawing.Size(108, 15)
        Me.lblReinstatement.TabIndex = 215
        Me.lblReinstatement.Text = "Reinstatement Date"
        '
        'objelLine2
        '
        Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine2.Location = New System.Drawing.Point(12, 361)
        Me.objelLine2.Name = "objelLine2"
        Me.objelLine2.Size = New System.Drawing.Size(806, 10)
        Me.objelLine2.TabIndex = 214
        Me.objelLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddReason
        '
        Me.objbtnAddReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReason.BorderSelected = False
        Me.objbtnAddReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReason.Location = New System.Drawing.Point(495, 375)
        Me.objbtnAddReason.Name = "objbtnAddReason"
        Me.objbtnAddReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReason.TabIndex = 207
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(12, 63)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(805, 10)
        Me.objelLine1.TabIndex = 205
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objSearchReason
        '
        Me.objSearchReason.BackColor = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchReason.BorderSelected = False
        Me.objSearchReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchReason.Location = New System.Drawing.Point(522, 375)
        Me.objSearchReason.Name = "objSearchReason"
        Me.objSearchReason.Size = New System.Drawing.Size(21, 21)
        Me.objSearchReason.TabIndex = 204
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(575, 37)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 4
        '
        'cboChangeReason
        '
        Me.cboChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChangeReason.DropDownWidth = 400
        Me.cboChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChangeReason.FormattingEnabled = True
        Me.cboChangeReason.Location = New System.Drawing.Point(147, 375)
        Me.cboChangeReason.Name = "cboChangeReason"
        Me.cboChangeReason.Size = New System.Drawing.Size(342, 21)
        Me.cboChangeReason.TabIndex = 203
        '
        'lblReason
        '
        Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReason.Location = New System.Drawing.Point(12, 378)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(129, 15)
        Me.lblReason.TabIndex = 202
        Me.lblReason.Text = "Reinstatement Reason"
        Me.lblReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Checked = False
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(101, 37)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.ShowCheckBox = True
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(103, 21)
        Me.dtpEffectiveDate.TabIndex = 1
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 450
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(278, 37)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(291, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(12, 40)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(83, 15)
        Me.lblEffectiveDate.TabIndex = 0
        Me.lblEffectiveDate.Text = "Effective Date"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(210, 40)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(62, 15)
        Me.lblEmployee.TabIndex = 2
        Me.lblEmployee.Text = "Employee"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 405)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(829, 50)
        Me.objFooter.TabIndex = 26
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(724, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(607, 11)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(111, 30)
        Me.btnSave.TabIndex = 37
        Me.btnSave.Text = "&Save Changes"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'dtpActualDate
        '
        Me.dtpActualDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpActualDate.Checked = False
        Me.dtpActualDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpActualDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpActualDate.Location = New System.Drawing.Point(716, 2)
        Me.dtpActualDate.Name = "dtpActualDate"
        Me.dtpActualDate.ShowCheckBox = True
        Me.dtpActualDate.Size = New System.Drawing.Size(102, 21)
        Me.dtpActualDate.TabIndex = 219
        '
        'LblActualDate
        '
        Me.LblActualDate.BackColor = System.Drawing.SystemColors.Control
        Me.LblActualDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblActualDate.Location = New System.Drawing.Point(602, 6)
        Me.LblActualDate.Name = "LblActualDate"
        Me.LblActualDate.Size = New System.Drawing.Size(108, 15)
        Me.LblActualDate.TabIndex = 218
        Me.LblActualDate.Text = "Actual Date"
        '
        'frmEmployeeRehire
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(829, 455)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeRehire"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Re-Hire Employee"
        Me.tabcEmployeeDetails.ResumeLayout(False)
        Me.tabpMainInfo.ResumeLayout(False)
        Me.gbEmployeeAllocation.ResumeLayout(False)
        Me.gbEmployeeAllocation.PerformLayout()
        Me.tabpAdditionalInfo.ResumeLayout(False)
        Me.gbWorkPermit.ResumeLayout(False)
        Me.gbWorkPermit.PerformLayout()
        Me.gbDatesDetails.ResumeLayout(False)
        Me.gbDatesDetails.PerformLayout()
        Me.tabpMembership.ResumeLayout(False)
        Me.pnlMembershipinfo.ResumeLayout(False)
        Me.gbMembershipInfo.ResumeLayout(False)
        Me.gbMembershipInfo.PerformLayout()
        Me.pnlMembershipDataInfo.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.objgbDatesInformation.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabcEmployeeDetails As System.Windows.Forms.TabControl
    Friend WithEvents tabpMainInfo As System.Windows.Forms.TabPage
    Friend WithEvents gbEmployeeAllocation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tabpAdditionalInfo As System.Windows.Forms.TabPage
    Friend WithEvents gbWorkPermit As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtPlaceofIssue As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPlaceOfIssue As System.Windows.Forms.Label
    Friend WithEvents dtpExpiryDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblExpiryDate As System.Windows.Forms.Label
    Friend WithEvents dtpIssueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblIssueDate As System.Windows.Forms.Label
    Friend WithEvents lblIssueCountry As System.Windows.Forms.Label
    Friend WithEvents cboIssueCountry As System.Windows.Forms.ComboBox
    Friend WithEvents txtWorkPermitNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblWorkPermitNo As System.Windows.Forms.Label
    Friend WithEvents gbDatesDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkExclude As System.Windows.Forms.CheckBox
    Friend WithEvents dtpEndEmplDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEmplDate As System.Windows.Forms.Label
    Friend WithEvents dtpConfirmationDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblConfirmationDate As System.Windows.Forms.Label
    Friend WithEvents dtpRetirementDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblRetirementDate As System.Windows.Forms.Label
    Friend WithEvents dtpProbationDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblProbationFrom As System.Windows.Forms.Label
    Friend WithEvents dtpLeavingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpProbationDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblLeavingDate As System.Windows.Forms.Label
    Friend WithEvents lblProbationTo As System.Windows.Forms.Label
    Friend WithEvents tabpMembership As System.Windows.Forms.TabPage
    Friend WithEvents pnlMembershipinfo As System.Windows.Forms.Panel
    Friend WithEvents gbMembershipInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkActivateMembership As System.Windows.Forms.LinkLabel
    Friend WithEvents lblMembershipName As System.Windows.Forms.Label
    Friend WithEvents objbtnAddMembership As eZee.Common.eZeeGradientButton
    Friend WithEvents lblMembershipType As System.Windows.Forms.Label
    Friend WithEvents objbtnAddMemCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents cboMembershipType As System.Windows.Forms.ComboBox
    Friend WithEvents cboMemCategory As System.Windows.Forms.ComboBox
    Friend WithEvents txtMembershipNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMembershipRemark As System.Windows.Forms.Label
    Friend WithEvents btnDeleteMembership As eZee.Common.eZeeLightButton
    Friend WithEvents lblMembershipNo As System.Windows.Forms.Label
    Friend WithEvents btnAddMembership As eZee.Common.eZeeLightButton
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMemIssueDate As System.Windows.Forms.Label
    Friend WithEvents dtpMemIssueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblActivationDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnlMembershipDataInfo As System.Windows.Forms.Panel
    Friend WithEvents lvMembershipInfo As eZee.Common.eZeeListView
    Friend WithEvents colhMembershipCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMembershipType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMembershipNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMemIssueDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMemCatUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolMembershipUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhccategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhccategoryId As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblMemExpiryDate As System.Windows.Forms.Label
    Friend WithEvents btnEditMembership As eZee.Common.eZeeLightButton
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objgbDatesInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents objbtnAddReason As eZee.Common.eZeeGradientButton
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents objSearchReason As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents dtpReinstatementDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblReinstatement As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchUnits As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchTeam As eZee.Common.eZeeGradientButton
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchJobGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchJob As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchGradeGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchGrades As eZee.Common.eZeeGradientButton
    Friend WithEvents txtScale As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnSearchClass As eZee.Common.eZeeGradientButton
    Friend WithEvents lblScale As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchClassGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchGradeLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDepartmentGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCostCenter As eZee.Common.eZeeGradientButton
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchUnitGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchSection As eZee.Common.eZeeGradientButton
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchDepartment As eZee.Common.eZeeGradientButton
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchSecGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents lblUnits As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchDeptGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents cboUnits As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchBranch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboTeams As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartmentGrp As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitGroup As System.Windows.Forms.Label
    Friend WithEvents lblJobGroup As System.Windows.Forms.Label
    Friend WithEvents cboUnitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents lblSectionGroup As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboSectionGroup As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddGradeGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents cboStation As System.Windows.Forms.ComboBox
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddCostCenter As eZee.Common.eZeeGradientButton
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents objbtnAddScale As eZee.Common.eZeeGradientButton
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents objbtnAddJob As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddDepartment As eZee.Common.eZeeGradientButton
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransactionHead As System.Windows.Forms.Label
    Friend WithEvents objbtnAddGrade As eZee.Common.eZeeGradientButton
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents objbtnAddTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents cboTransactionHead As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddGradeLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents lblAppointmentdate As System.Windows.Forms.Label
    Friend WithEvents txtAppDate As System.Windows.Forms.TextBox
    Friend WithEvents lblBirthDate As System.Windows.Forms.Label
    Friend WithEvents txtBirthDate As System.Windows.Forms.TextBox
    Friend WithEvents elWorkPermit As eZee.Common.eZeeLine
    Friend WithEvents txtResidentIssuePlace As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblResidentIssuePlace As System.Windows.Forms.Label
    Friend WithEvents txtResidentPermitNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpResidentExpiryDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblResidentPermitNo As System.Windows.Forms.Label
    Friend WithEvents lblResidentExpiryDate As System.Windows.Forms.Label
    Friend WithEvents cboResidentIssueCountry As System.Windows.Forms.ComboBox
    Friend WithEvents dtpResidentIssueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblResidentIssueCountry As System.Windows.Forms.Label
    Friend WithEvents lblResidentIssueDate As System.Windows.Forms.Label
    Friend WithEvents elResidentPermit As eZee.Common.eZeeLine
    Friend WithEvents chkAssignDefaulTranHeads As System.Windows.Forms.CheckBox
    Friend WithEvents dtpActualDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblActualDate As System.Windows.Forms.Label
End Class
