﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeRehireList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeRehireList))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbEmployeeRehireList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objelLine3 = New eZee.Common.eZeeLine
        Me.pnl1 = New System.Windows.Forms.Panel
        Me.dgvHistory = New System.Windows.Forms.DataGridView
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhChangeDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRehireDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhRehiretranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAllowOpr = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnAddNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.Panel1.SuspendLayout()
        Me.gbEmployeeRehireList.SuspendLayout()
        Me.pnl1.SuspendLayout()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbEmployeeRehireList)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(651, 354)
        Me.Panel1.TabIndex = 0
        '
        'gbEmployeeRehireList
        '
        Me.gbEmployeeRehireList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeRehireList.Checked = False
        Me.gbEmployeeRehireList.CollapseAllExceptThis = False
        Me.gbEmployeeRehireList.CollapsedHoverImage = Nothing
        Me.gbEmployeeRehireList.CollapsedNormalImage = Nothing
        Me.gbEmployeeRehireList.CollapsedPressedImage = Nothing
        Me.gbEmployeeRehireList.CollapseOnLoad = False
        Me.gbEmployeeRehireList.Controls.Add(Me.lnkAllocation)
        Me.gbEmployeeRehireList.Controls.Add(Me.objelLine3)
        Me.gbEmployeeRehireList.Controls.Add(Me.pnl1)
        Me.gbEmployeeRehireList.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmployeeRehireList.Controls.Add(Me.cboEmployee)
        Me.gbEmployeeRehireList.Controls.Add(Me.lblEmployee)
        Me.gbEmployeeRehireList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbEmployeeRehireList.ExpandedHoverImage = Nothing
        Me.gbEmployeeRehireList.ExpandedNormalImage = Nothing
        Me.gbEmployeeRehireList.ExpandedPressedImage = Nothing
        Me.gbEmployeeRehireList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeRehireList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeRehireList.HeaderHeight = 25
        Me.gbEmployeeRehireList.HeaderMessage = ""
        Me.gbEmployeeRehireList.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbEmployeeRehireList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeRehireList.HeightOnCollapse = 0
        Me.gbEmployeeRehireList.LeftTextSpace = 0
        Me.gbEmployeeRehireList.Location = New System.Drawing.Point(0, 0)
        Me.gbEmployeeRehireList.Name = "gbEmployeeRehireList"
        Me.gbEmployeeRehireList.OpenHeight = 300
        Me.gbEmployeeRehireList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeRehireList.ShowBorder = True
        Me.gbEmployeeRehireList.ShowCheckBox = False
        Me.gbEmployeeRehireList.ShowCollapseButton = False
        Me.gbEmployeeRehireList.ShowDefaultBorderColor = True
        Me.gbEmployeeRehireList.ShowDownButton = False
        Me.gbEmployeeRehireList.ShowHeader = True
        Me.gbEmployeeRehireList.Size = New System.Drawing.Size(651, 304)
        Me.gbEmployeeRehireList.TabIndex = 3
        Me.gbEmployeeRehireList.Temp = 0
        Me.gbEmployeeRehireList.Text = "Employee Rehire List"
        Me.gbEmployeeRehireList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine3
        '
        Me.objelLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine3.Location = New System.Drawing.Point(9, 61)
        Me.objelLine3.Name = "objelLine3"
        Me.objelLine3.Size = New System.Drawing.Size(630, 13)
        Me.objelLine3.TabIndex = 17
        Me.objelLine3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnl1
        '
        Me.pnl1.Controls.Add(Me.dgvHistory)
        Me.pnl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl1.Location = New System.Drawing.Point(12, 77)
        Me.pnl1.Name = "pnl1"
        Me.pnl1.Size = New System.Drawing.Size(627, 221)
        Me.pnl1.TabIndex = 173
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToResizeRows = False
        Me.dgvHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhEdit, Me.objdgcolhDelete, Me.dgcolhChangeDate, Me.dgcolhRehireDate, Me.dgcolhReason, Me.objdgcolhRehiretranunkid, Me.objdgcolhAllowOpr})
        Me.dgvHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHistory.Location = New System.Drawing.Point(0, 0)
        Me.dgvHistory.MultiSelect = False
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.Size = New System.Drawing.Size(627, 221)
        Me.dgvHistory.TabIndex = 0
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Visible = False
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'dgcolhChangeDate
        '
        Me.dgcolhChangeDate.Frozen = True
        Me.dgcolhChangeDate.HeaderText = "Effective Date"
        Me.dgcolhChangeDate.Name = "dgcolhChangeDate"
        Me.dgcolhChangeDate.ReadOnly = True
        Me.dgcolhChangeDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhRehireDate
        '
        Me.dgcolhRehireDate.HeaderText = "Reinstatement Date"
        Me.dgcolhRehireDate.Name = "dgcolhRehireDate"
        Me.dgcolhRehireDate.ReadOnly = True
        Me.dgcolhRehireDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRehireDate.Width = 110
        '
        'dgcolhReason
        '
        Me.dgcolhReason.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhReason.HeaderText = "Reason"
        Me.dgcolhReason.Name = "dgcolhReason"
        Me.dgcolhReason.ReadOnly = True
        Me.dgcolhReason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhRehiretranunkid
        '
        Me.objdgcolhRehiretranunkid.HeaderText = "objdgcolhRehiretranunkid"
        Me.objdgcolhRehiretranunkid.Name = "objdgcolhRehiretranunkid"
        Me.objdgcolhRehiretranunkid.ReadOnly = True
        Me.objdgcolhRehiretranunkid.Visible = False
        '
        'objdgcolhAllowOpr
        '
        Me.objdgcolhAllowOpr.HeaderText = "objdgcolhAllowOpr"
        Me.objdgcolhAllowOpr.Name = "objdgcolhAllowOpr"
        Me.objdgcolhAllowOpr.ReadOnly = True
        Me.objdgcolhAllowOpr.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(618, 37)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 4
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 450
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(95, 37)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(517, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(12, 40)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(77, 15)
        Me.lblEmployee.TabIndex = 2
        Me.lblEmployee.Text = "Employee"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnAddNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 304)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(651, 50)
        Me.objFooter.TabIndex = 2
        '
        'btnAddNew
        '
        Me.btnAddNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddNew.BackColor = System.Drawing.Color.White
        Me.btnAddNew.BackgroundImage = CType(resources.GetObject("btnAddNew.BackgroundImage"), System.Drawing.Image)
        Me.btnAddNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddNew.BorderColor = System.Drawing.Color.Empty
        Me.btnAddNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddNew.FlatAppearance.BorderSize = 0
        Me.btnAddNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddNew.ForeColor = System.Drawing.Color.Black
        Me.btnAddNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddNew.Location = New System.Drawing.Point(12, 11)
        Me.btnAddNew.Name = "btnAddNew"
        Me.btnAddNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddNew.Size = New System.Drawing.Size(121, 30)
        Me.btnAddNew.TabIndex = 1
        Me.btnAddNew.Text = "&Rehire Employee"
        Me.btnAddNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(546, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Effective Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Reinstatement Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 150
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Reason"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "objdgcolhRehiretranunkid"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(573, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(76, 13)
        Me.lnkAllocation.TabIndex = 235
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'frmEmployeeRehireList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(651, 354)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeRehireList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Rehire List"
        Me.Panel1.ResumeLayout(False)
        Me.gbEmployeeRehireList.ResumeLayout(False)
        Me.pnl1.ResumeLayout(False)
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbEmployeeRehireList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objelLine3 As eZee.Common.eZeeLine
    Friend WithEvents pnl1 As System.Windows.Forms.Panel
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents btnAddNew As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhChangeDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRehireDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhRehiretranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAllowOpr As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
End Class
