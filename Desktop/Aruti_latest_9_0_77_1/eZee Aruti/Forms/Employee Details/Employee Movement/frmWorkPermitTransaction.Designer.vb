﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWorkPermitTransaction
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWorkPermitTransaction))
        Me.objelLine2 = New eZee.Common.eZeeLine
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbWPInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlData = New System.Windows.Forms.Panel
        Me.lblAppointmentdate = New System.Windows.Forms.Label
        Me.txtDate = New System.Windows.Forms.TextBox
        Me.objbtnAddReason = New eZee.Common.eZeeGradientButton
        Me.dtpIssueDate = New System.Windows.Forms.DateTimePicker
        Me.lblIssueDate = New System.Windows.Forms.Label
        Me.objelLine3 = New eZee.Common.eZeeLine
        Me.lblExpiryDate = New System.Windows.Forms.Label
        Me.cboChangeReason = New System.Windows.Forms.ComboBox
        Me.dtpExpiryDate = New System.Windows.Forms.DateTimePicker
        Me.objSearchReason = New eZee.Common.eZeeGradientButton
        Me.lblReason = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.objelLine1 = New eZee.Common.eZeeLine
        Me.objbtnSearchCountry = New eZee.Common.eZeeGradientButton
        Me.txtPlaceofIssue = New eZee.TextBox.AlphanumericTextBox
        Me.lblPlaceOfIssue = New System.Windows.Forms.Label
        Me.pnl1 = New System.Windows.Forms.Panel
        Me.dgvHistory = New System.Windows.Forms.DataGridView
        Me.objdgcolhViewPending = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhOperationType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChangeDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPermitNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCountryName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIssuePlace = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhIssueDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhExpiryDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhpermittranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFromEmp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAppointdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhrehiretranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhtranguid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhOperationTypeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.dtpEffectiveDate = New System.Windows.Forms.DateTimePicker
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblIssueCountry = New System.Windows.Forms.Label
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.cboIssueCountry = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.txtWorkPermitNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblWorkPermitNo = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.lblPendingData = New System.Windows.Forms.Label
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.pnlMain.SuspendLayout()
        Me.gbWPInformation.SuspendLayout()
        Me.pnlData.SuspendLayout()
        Me.pnl1.SuspendLayout()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'objelLine2
        '
        Me.objelLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine2.Location = New System.Drawing.Point(12, 61)
        Me.objelLine2.Name = "objelLine2"
        Me.objelLine2.Size = New System.Drawing.Size(633, 10)
        Me.objelLine2.TabIndex = 5
        Me.objelLine2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbWPInformation)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(657, 437)
        Me.pnlMain.TabIndex = 38
        '
        'gbWPInformation
        '
        Me.gbWPInformation.BorderColor = System.Drawing.Color.Black
        Me.gbWPInformation.Checked = False
        Me.gbWPInformation.CollapseAllExceptThis = False
        Me.gbWPInformation.CollapsedHoverImage = Nothing
        Me.gbWPInformation.CollapsedNormalImage = Nothing
        Me.gbWPInformation.CollapsedPressedImage = Nothing
        Me.gbWPInformation.CollapseOnLoad = False
        Me.gbWPInformation.Controls.Add(Me.lnkAllocation)
        Me.gbWPInformation.Controls.Add(Me.pnlData)
        Me.gbWPInformation.Controls.Add(Me.objbtnAddReason)
        Me.gbWPInformation.Controls.Add(Me.dtpIssueDate)
        Me.gbWPInformation.Controls.Add(Me.lblIssueDate)
        Me.gbWPInformation.Controls.Add(Me.objelLine3)
        Me.gbWPInformation.Controls.Add(Me.lblExpiryDate)
        Me.gbWPInformation.Controls.Add(Me.cboChangeReason)
        Me.gbWPInformation.Controls.Add(Me.dtpExpiryDate)
        Me.gbWPInformation.Controls.Add(Me.objSearchReason)
        Me.gbWPInformation.Controls.Add(Me.lblReason)
        Me.gbWPInformation.Controls.Add(Me.btnSave)
        Me.gbWPInformation.Controls.Add(Me.objelLine1)
        Me.gbWPInformation.Controls.Add(Me.objbtnSearchCountry)
        Me.gbWPInformation.Controls.Add(Me.txtPlaceofIssue)
        Me.gbWPInformation.Controls.Add(Me.lblPlaceOfIssue)
        Me.gbWPInformation.Controls.Add(Me.objelLine2)
        Me.gbWPInformation.Controls.Add(Me.pnl1)
        Me.gbWPInformation.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbWPInformation.Controls.Add(Me.dtpEffectiveDate)
        Me.gbWPInformation.Controls.Add(Me.cboEmployee)
        Me.gbWPInformation.Controls.Add(Me.lblIssueCountry)
        Me.gbWPInformation.Controls.Add(Me.lblEffectiveDate)
        Me.gbWPInformation.Controls.Add(Me.cboIssueCountry)
        Me.gbWPInformation.Controls.Add(Me.lblEmployee)
        Me.gbWPInformation.Controls.Add(Me.txtWorkPermitNo)
        Me.gbWPInformation.Controls.Add(Me.lblWorkPermitNo)
        Me.gbWPInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbWPInformation.ExpandedHoverImage = Nothing
        Me.gbWPInformation.ExpandedNormalImage = Nothing
        Me.gbWPInformation.ExpandedPressedImage = Nothing
        Me.gbWPInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbWPInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbWPInformation.HeaderHeight = 25
        Me.gbWPInformation.HeaderMessage = ""
        Me.gbWPInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbWPInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbWPInformation.HeightOnCollapse = 0
        Me.gbWPInformation.LeftTextSpace = 0
        Me.gbWPInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbWPInformation.Name = "gbWPInformation"
        Me.gbWPInformation.OpenHeight = 300
        Me.gbWPInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbWPInformation.ShowBorder = True
        Me.gbWPInformation.ShowCheckBox = False
        Me.gbWPInformation.ShowCollapseButton = False
        Me.gbWPInformation.ShowDefaultBorderColor = True
        Me.gbWPInformation.ShowDownButton = False
        Me.gbWPInformation.ShowHeader = True
        Me.gbWPInformation.Size = New System.Drawing.Size(657, 387)
        Me.gbWPInformation.TabIndex = 0
        Me.gbWPInformation.Temp = 0
        Me.gbWPInformation.Text = "Work Permit Information"
        Me.gbWPInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.lblAppointmentdate)
        Me.pnlData.Controls.Add(Me.txtDate)
        Me.pnlData.Location = New System.Drawing.Point(350, 143)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(175, 25)
        Me.pnlData.TabIndex = 187
        '
        'lblAppointmentdate
        '
        Me.lblAppointmentdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppointmentdate.Location = New System.Drawing.Point(3, 5)
        Me.lblAppointmentdate.Name = "lblAppointmentdate"
        Me.lblAppointmentdate.Size = New System.Drawing.Size(62, 15)
        Me.lblAppointmentdate.TabIndex = 183
        Me.lblAppointmentdate.Text = "Appt. Date"
        Me.lblAppointmentdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDate
        '
        Me.txtDate.BackColor = System.Drawing.Color.White
        Me.txtDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.Location = New System.Drawing.Point(71, 2)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.ReadOnly = True
        Me.txtDate.Size = New System.Drawing.Size(101, 21)
        Me.txtDate.TabIndex = 184
        '
        'objbtnAddReason
        '
        Me.objbtnAddReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddReason.BorderSelected = False
        Me.objbtnAddReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddReason.Location = New System.Drawing.Point(323, 145)
        Me.objbtnAddReason.Name = "objbtnAddReason"
        Me.objbtnAddReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddReason.TabIndex = 21
        '
        'dtpIssueDate
        '
        Me.dtpIssueDate.Checked = False
        Me.dtpIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpIssueDate.Location = New System.Drawing.Point(101, 101)
        Me.dtpIssueDate.Name = "dtpIssueDate"
        Me.dtpIssueDate.ShowCheckBox = True
        Me.dtpIssueDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpIssueDate.TabIndex = 12
        '
        'lblIssueDate
        '
        Me.lblIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIssueDate.Location = New System.Drawing.Point(9, 104)
        Me.lblIssueDate.Name = "lblIssueDate"
        Me.lblIssueDate.Size = New System.Drawing.Size(86, 15)
        Me.lblIssueDate.TabIndex = 11
        Me.lblIssueDate.Text = "Issue Date"
        Me.lblIssueDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objelLine3
        '
        Me.objelLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine3.Location = New System.Drawing.Point(9, 125)
        Me.objelLine3.Name = "objelLine3"
        Me.objelLine3.Size = New System.Drawing.Size(636, 12)
        Me.objelLine3.TabIndex = 17
        Me.objelLine3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblExpiryDate
        '
        Me.lblExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpiryDate.Location = New System.Drawing.Point(213, 104)
        Me.lblExpiryDate.Name = "lblExpiryDate"
        Me.lblExpiryDate.Size = New System.Drawing.Size(77, 15)
        Me.lblExpiryDate.TabIndex = 13
        Me.lblExpiryDate.Text = "Expiry Date"
        Me.lblExpiryDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboChangeReason
        '
        Me.cboChangeReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChangeReason.DropDownWidth = 400
        Me.cboChangeReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChangeReason.FormattingEnabled = True
        Me.cboChangeReason.Location = New System.Drawing.Point(101, 145)
        Me.cboChangeReason.Name = "cboChangeReason"
        Me.cboChangeReason.Size = New System.Drawing.Size(189, 21)
        Me.cboChangeReason.TabIndex = 19
        '
        'dtpExpiryDate
        '
        Me.dtpExpiryDate.Checked = False
        Me.dtpExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpExpiryDate.Location = New System.Drawing.Point(296, 101)
        Me.dtpExpiryDate.Name = "dtpExpiryDate"
        Me.dtpExpiryDate.ShowCheckBox = True
        Me.dtpExpiryDate.Size = New System.Drawing.Size(105, 21)
        Me.dtpExpiryDate.TabIndex = 14
        '
        'objSearchReason
        '
        Me.objSearchReason.BackColor = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchReason.BorderSelected = False
        Me.objSearchReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchReason.Location = New System.Drawing.Point(296, 145)
        Me.objSearchReason.Name = "objSearchReason"
        Me.objSearchReason.Size = New System.Drawing.Size(21, 21)
        Me.objSearchReason.TabIndex = 20
        '
        'lblReason
        '
        Me.lblReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReason.Location = New System.Drawing.Point(9, 147)
        Me.lblReason.Name = "lblReason"
        Me.lblReason.Size = New System.Drawing.Size(86, 17)
        Me.lblReason.TabIndex = 18
        Me.lblReason.Text = "Change Reason"
        Me.lblReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(531, 140)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(114, 30)
        Me.btnSave.TabIndex = 22
        Me.btnSave.Text = "&Save Changes"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'objelLine1
        '
        Me.objelLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelLine1.Location = New System.Drawing.Point(12, 173)
        Me.objelLine1.Name = "objelLine1"
        Me.objelLine1.Size = New System.Drawing.Size(633, 7)
        Me.objelLine1.TabIndex = 23
        Me.objelLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCountry
        '
        Me.objbtnSearchCountry.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCountry.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCountry.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCountry.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCountry.BorderSelected = False
        Me.objbtnSearchCountry.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCountry.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCountry.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCountry.Location = New System.Drawing.Point(621, 74)
        Me.objbtnSearchCountry.Name = "objbtnSearchCountry"
        Me.objbtnSearchCountry.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCountry.TabIndex = 10
        '
        'txtPlaceofIssue
        '
        Me.txtPlaceofIssue.Flags = 0
        Me.txtPlaceofIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlaceofIssue.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPlaceofIssue.Location = New System.Drawing.Point(478, 101)
        Me.txtPlaceofIssue.Name = "txtPlaceofIssue"
        Me.txtPlaceofIssue.Size = New System.Drawing.Size(137, 21)
        Me.txtPlaceofIssue.TabIndex = 16
        '
        'lblPlaceOfIssue
        '
        Me.lblPlaceOfIssue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlaceOfIssue.Location = New System.Drawing.Point(407, 104)
        Me.lblPlaceOfIssue.Name = "lblPlaceOfIssue"
        Me.lblPlaceOfIssue.Size = New System.Drawing.Size(65, 15)
        Me.lblPlaceOfIssue.TabIndex = 15
        Me.lblPlaceOfIssue.Text = "Issue Place"
        Me.lblPlaceOfIssue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnl1
        '
        Me.pnl1.Controls.Add(Me.dgvHistory)
        Me.pnl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl1.Location = New System.Drawing.Point(12, 183)
        Me.pnl1.Name = "pnl1"
        Me.pnl1.Size = New System.Drawing.Size(633, 198)
        Me.pnl1.TabIndex = 173
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToResizeRows = False
        Me.dgvHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhViewPending, Me.objdgcolhEdit, Me.objdgcolhDelete, Me.objdgcolhOperationType, Me.dgcolhChangeDate, Me.dgcolhPermitNo, Me.dgcolhCountryName, Me.dgcolhIssuePlace, Me.dgcolhIssueDate, Me.dgcolhExpiryDate, Me.dgcolhReason, Me.objdgcolhpermittranunkid, Me.objdgcolhFromEmp, Me.objdgcolhAppointdate, Me.objdgcolhrehiretranunkid, Me.objdgcolhtranguid, Me.objdgcolhOperationTypeId})
        Me.dgvHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHistory.Location = New System.Drawing.Point(0, 0)
        Me.dgvHistory.MultiSelect = False
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.Size = New System.Drawing.Size(633, 198)
        Me.dgvHistory.TabIndex = 0
        '
        'objdgcolhViewPending
        '
        Me.objdgcolhViewPending.Frozen = True
        Me.objdgcolhViewPending.HeaderText = ""
        Me.objdgcolhViewPending.Image = Global.Aruti.Main.My.Resources.Resources.blankImage
        Me.objdgcolhViewPending.Name = "objdgcolhViewPending"
        Me.objdgcolhViewPending.ReadOnly = True
        Me.objdgcolhViewPending.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhViewPending.Width = 25
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'objdgcolhOperationType
        '
        Me.objdgcolhOperationType.Frozen = True
        Me.objdgcolhOperationType.HeaderText = "OperationType"
        Me.objdgcolhOperationType.Name = "objdgcolhOperationType"
        Me.objdgcolhOperationType.ReadOnly = True
        Me.objdgcolhOperationType.Visible = False
        '
        'dgcolhChangeDate
        '
        Me.dgcolhChangeDate.HeaderText = "Effective Date"
        Me.dgcolhChangeDate.Name = "dgcolhChangeDate"
        Me.dgcolhChangeDate.ReadOnly = True
        Me.dgcolhChangeDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhPermitNo
        '
        Me.dgcolhPermitNo.HeaderText = "Permit No."
        Me.dgcolhPermitNo.Name = "dgcolhPermitNo"
        Me.dgcolhPermitNo.ReadOnly = True
        Me.dgcolhPermitNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhCountryName
        '
        Me.dgcolhCountryName.HeaderText = "Country"
        Me.dgcolhCountryName.Name = "dgcolhCountryName"
        Me.dgcolhCountryName.ReadOnly = True
        Me.dgcolhCountryName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIssuePlace
        '
        Me.dgcolhIssuePlace.HeaderText = "Issue Place"
        Me.dgcolhIssuePlace.Name = "dgcolhIssuePlace"
        Me.dgcolhIssuePlace.ReadOnly = True
        Me.dgcolhIssuePlace.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhIssueDate
        '
        Me.dgcolhIssueDate.HeaderText = "Issue Date"
        Me.dgcolhIssueDate.Name = "dgcolhIssueDate"
        Me.dgcolhIssueDate.ReadOnly = True
        Me.dgcolhIssueDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhExpiryDate
        '
        Me.dgcolhExpiryDate.HeaderText = "Expiry Date"
        Me.dgcolhExpiryDate.Name = "dgcolhExpiryDate"
        Me.dgcolhExpiryDate.ReadOnly = True
        Me.dgcolhExpiryDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhReason
        '
        Me.dgcolhReason.HeaderText = "Reason"
        Me.dgcolhReason.Name = "dgcolhReason"
        Me.dgcolhReason.ReadOnly = True
        Me.dgcolhReason.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhpermittranunkid
        '
        Me.objdgcolhpermittranunkid.HeaderText = "objdgcolhpermittranunkid"
        Me.objdgcolhpermittranunkid.Name = "objdgcolhpermittranunkid"
        Me.objdgcolhpermittranunkid.ReadOnly = True
        Me.objdgcolhpermittranunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhpermittranunkid.Visible = False
        '
        'objdgcolhFromEmp
        '
        Me.objdgcolhFromEmp.HeaderText = "objdgcolhFromEmp"
        Me.objdgcolhFromEmp.Name = "objdgcolhFromEmp"
        Me.objdgcolhFromEmp.ReadOnly = True
        Me.objdgcolhFromEmp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhFromEmp.Visible = False
        '
        'objdgcolhAppointdate
        '
        Me.objdgcolhAppointdate.HeaderText = "objdgcolhAppointdate"
        Me.objdgcolhAppointdate.Name = "objdgcolhAppointdate"
        Me.objdgcolhAppointdate.ReadOnly = True
        Me.objdgcolhAppointdate.Visible = False
        '
        'objdgcolhrehiretranunkid
        '
        Me.objdgcolhrehiretranunkid.HeaderText = "objdgcolhrehiretranunkid"
        Me.objdgcolhrehiretranunkid.Name = "objdgcolhrehiretranunkid"
        Me.objdgcolhrehiretranunkid.ReadOnly = True
        Me.objdgcolhrehiretranunkid.Visible = False
        '
        'objdgcolhtranguid
        '
        Me.objdgcolhtranguid.HeaderText = "objdgcolhtranguid"
        Me.objdgcolhtranguid.Name = "objdgcolhtranguid"
        Me.objdgcolhtranguid.ReadOnly = True
        Me.objdgcolhtranguid.Visible = False
        '
        'objdgcolhOperationTypeId
        '
        Me.objdgcolhOperationTypeId.HeaderText = "objdgcolhOperationTypeId"
        Me.objdgcolhOperationTypeId.Name = "objdgcolhOperationTypeId"
        Me.objdgcolhOperationTypeId.ReadOnly = True
        Me.objdgcolhOperationTypeId.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(621, 37)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 4
        '
        'dtpEffectiveDate
        '
        Me.dtpEffectiveDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Checked = False
        Me.dtpEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEffectiveDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEffectiveDate.Location = New System.Drawing.Point(101, 37)
        Me.dtpEffectiveDate.Name = "dtpEffectiveDate"
        Me.dtpEffectiveDate.ShowCheckBox = True
        Me.dtpEffectiveDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpEffectiveDate.TabIndex = 1
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 450
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(296, 37)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(319, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'lblIssueCountry
        '
        Me.lblIssueCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIssueCountry.Location = New System.Drawing.Point(213, 77)
        Me.lblIssueCountry.Name = "lblIssueCountry"
        Me.lblIssueCountry.Size = New System.Drawing.Size(77, 15)
        Me.lblIssueCountry.TabIndex = 8
        Me.lblIssueCountry.Text = "Country"
        Me.lblIssueCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(12, 40)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(83, 15)
        Me.lblEffectiveDate.TabIndex = 0
        Me.lblEffectiveDate.Text = "Effective Date"
        '
        'cboIssueCountry
        '
        Me.cboIssueCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIssueCountry.DropDownWidth = 120
        Me.cboIssueCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIssueCountry.FormattingEnabled = True
        Me.cboIssueCountry.Location = New System.Drawing.Point(296, 74)
        Me.cboIssueCountry.Name = "cboIssueCountry"
        Me.cboIssueCountry.Size = New System.Drawing.Size(319, 21)
        Me.cboIssueCountry.TabIndex = 9
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(213, 40)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(77, 15)
        Me.lblEmployee.TabIndex = 2
        Me.lblEmployee.Text = "Employee"
        '
        'txtWorkPermitNo
        '
        Me.txtWorkPermitNo.Flags = 0
        Me.txtWorkPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWorkPermitNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtWorkPermitNo.Location = New System.Drawing.Point(101, 74)
        Me.txtWorkPermitNo.Name = "txtWorkPermitNo"
        Me.txtWorkPermitNo.Size = New System.Drawing.Size(106, 21)
        Me.txtWorkPermitNo.TabIndex = 7
        '
        'lblWorkPermitNo
        '
        Me.lblWorkPermitNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkPermitNo.Location = New System.Drawing.Point(9, 77)
        Me.lblWorkPermitNo.Name = "lblWorkPermitNo"
        Me.lblWorkPermitNo.Size = New System.Drawing.Size(86, 15)
        Me.lblWorkPermitNo.TabIndex = 6
        Me.lblWorkPermitNo.Text = "Work Permit No."
        Me.lblWorkPermitNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lblPendingData)
        Me.objFooter.Controls.Add(Me.objlblCaption)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 387)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(657, 50)
        Me.objFooter.TabIndex = 1
        '
        'lblPendingData
        '
        Me.lblPendingData.BackColor = System.Drawing.Color.PowderBlue
        Me.lblPendingData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPendingData.ForeColor = System.Drawing.Color.Black
        Me.lblPendingData.Location = New System.Drawing.Point(391, 18)
        Me.lblPendingData.Name = "lblPendingData"
        Me.lblPendingData.Size = New System.Drawing.Size(155, 17)
        Me.lblPendingData.TabIndex = 9
        Me.lblPendingData.Text = "Pending Approval"
        Me.lblPendingData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblPendingData.Visible = False
        '
        'objlblCaption
        '
        Me.objlblCaption.BackColor = System.Drawing.Color.Orange
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.ForeColor = System.Drawing.Color.Black
        Me.objlblCaption.Location = New System.Drawing.Point(10, 18)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(155, 17)
        Me.objlblCaption.TabIndex = 8
        Me.objlblCaption.Text = "Employee Rehired"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(552, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Location = New System.Drawing.Point(577, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(76, 13)
        Me.lnkAllocation.TabIndex = 234
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'frmWorkPermitTransaction
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(657, 437)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmWorkPermitTransaction"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Work Permit"
        Me.pnlMain.ResumeLayout(False)
        Me.gbWPInformation.ResumeLayout(False)
        Me.gbWPInformation.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        Me.pnlData.PerformLayout()
        Me.pnl1.ResumeLayout(False)
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objelLine2 As eZee.Common.eZeeLine
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbWPInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents pnl1 As System.Windows.Forms.Panel
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents dtpEffectiveDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents txtPlaceofIssue As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPlaceOfIssue As System.Windows.Forms.Label
    Friend WithEvents dtpExpiryDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblExpiryDate As System.Windows.Forms.Label
    Friend WithEvents dtpIssueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblIssueDate As System.Windows.Forms.Label
    Friend WithEvents lblIssueCountry As System.Windows.Forms.Label
    Friend WithEvents cboIssueCountry As System.Windows.Forms.ComboBox
    Friend WithEvents txtWorkPermitNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblWorkPermitNo As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCountry As eZee.Common.eZeeGradientButton
    Friend WithEvents objelLine1 As eZee.Common.eZeeLine
    Friend WithEvents lblReason As System.Windows.Forms.Label
    Friend WithEvents cboChangeReason As System.Windows.Forms.ComboBox
    Friend WithEvents objSearchReason As eZee.Common.eZeeGradientButton
    Friend WithEvents objelLine3 As eZee.Common.eZeeLine
    Friend WithEvents objbtnAddReason As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents lblAppointmentdate As System.Windows.Forms.Label
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents lblPendingData As System.Windows.Forms.Label
    Friend WithEvents objdgcolhViewPending As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhOperationType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChangeDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPermitNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCountryName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIssuePlace As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhIssueDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhExpiryDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhpermittranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFromEmp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAppointdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhrehiretranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhtranguid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhOperationTypeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
End Class
