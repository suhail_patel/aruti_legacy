﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEmployeeRehire

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmEmployeeRehire"
    Private objEReHire As clsemployee_rehire_tran
    Private mAction As enAction = enAction.ADD_ONE
    Private mdtAppointmentDate As Date = Nothing
    Private mstrEmployeeCode As String = String.Empty
    Private xCurrentAllocation As Dictionary(Of Integer, String)
    Private objEmp As clsEmployee_Master
    Private mdtMemTran As DataTable
    Private mintMemberTypeId As Integer = -1
    Private objMem_Tran As clsMembershipTran
    Private StrEDMessage As String = String.Empty
    Private xPeriodUnkid As Integer = 0
    Private mintRehireTranunkid As Integer = 0
    Private mblnCancel As Boolean = True
    Private mintEmployeeUnkid As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmployeeUnkid As Integer = 0) As Boolean
        Try
            mintRehireTranunkid = intUnkId
            mintEmployeeUnkid = intEmployeeUnkid
            mAction = eAction

            Me.ShowDialog()

            intUnkId = mintRehireTranunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSectionGrp As New clsSectionGroup
        Dim objSection As New clsSections
        Dim objUnitGroup As New clsUnitGroup
        Dim objUnit As New clsUnits
        Dim objTeam As New clsTeams
        Dim objClassGrp As New clsClassGroup
        Dim objCMaster As New clsCommon_Master
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objGradeGrp As New clsGradeGroup
        Dim objCostCenter As New clscostcenter_master
        Dim objTranHead As New clsTransactionHead
        Dim objCountry As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            Dim dtList As DataTable = Nothing
            If mAction = enAction.EDIT_ONE Then
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsCombos = objEmployee.GetEmployeeList("Emp", False, , mintEmployeeUnkid)
                dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, True, "Emp", True, mintEmployeeUnkid) 'S.SANDEEP [23-OCT-2017] -- START {False -> True} -- END

                'S.SANDEEP [04 JUN 2015] -- END
                dtList = dsCombos.Tables(0).Copy
            Else
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                Dim dsList As New DataSet
                'dsList = objEmployee.GetEmployeeList("Emp", True, True)
                dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, True, "Emp", True)

                'dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                    User._Object._Userunkid, _
                                                    FinancialYear._Object._YearUnkid, _
                                                    Company._Object._Companyunkid, _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                    True, False, "Emp", True)
                'S.SANDEEP [04 JUN 2015] -- END


                'Pinkal (12-May-2018) -- Start
                'Enhancement - Optimize Performance of employee combo

                'Dim dtTerm As IEnumerable(Of DataRow) = From r In dsList.Tables(0).AsEnumerable() _
                '             Where Not dsCombos.Tables(0).AsEnumerable().Any(Function(r2) r("employeeunkid").ToString = r2("employeeunkid").ToString) Select r

                ''S.SANDEEP [10 DEC 2015] -- START
                ''dtList = dtTerm.CopyToDataTable()
                'If dtTerm IsNot Nothing AndAlso dtTerm.Count > 0 Then
                '    dtList = dtTerm.CopyToDataTable()
                'Else
                '    dtList = dsList.Tables(0).Clone
                'End If
                ''S.SANDEEP [10 DEC 2015] -- END

                dtList = (From t1 In dsList.Tables(0).AsEnumerable() _
                                     Group Join t2 In dsCombos.Tables(0).AsEnumerable() On t1.Field(Of Integer)("employeeunkid") Equals t2.Field(Of Integer)("employeeunkid") Into tg = Group _
                                     From tcheck In tg.DefaultIfEmpty() _
                                     Where tcheck Is Nothing _
                                     Select t1).CopyToDataTable()

                If dtList Is Nothing Then
                    dtList = CType(dsList.Tables(0), DataTable).Select("employeeunkid = 0").CopyToDataTable()
                End If

                'Pinkal (12-May-2018) -- End

            End If

            RemoveHandler cboEmployee.SelectedIndexChanged, AddressOf cboEmployee_SelectedIndexChanged
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dtList
                .SelectedValue = 0
                .Text = ""
            End With
            AddHandler cboEmployee.SelectedIndexChanged, AddressOf cboEmployee_SelectedIndexChanged

            dsCombos = objStation.getComboList("Station", True)
            With cboStation
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Station")
                .SelectedValue = 0
            End With

            dsCombos = objDeptGrp.getComboList("DeptGrp", True)
            With cboDepartmentGrp
                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("DeptGrp")
                .SelectedValue = 0
            End With

            dsCombos = objDepartment.getComboList("Department", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Department")
                .SelectedValue = 0
            End With

            dsCombos = objSection.getComboList("Section", True)
            With cboSections
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Section")
                .SelectedValue = 0
            End With

            dsCombos = objUnit.getComboList("Unit", True)
            With cboUnits
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Unit")
                .SelectedValue = 0
            End With

            RemoveHandler cboClassGroup.SelectedIndexChanged, AddressOf cboClassGroup_SelectedIndexChanged
            dsCombos = objClassGrp.getComboList("ClassGrp", True)
            With cboClassGroup
                .ValueMember = "classgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("ClassGrp")
                .SelectedValue = 0
            End With
            AddHandler cboClassGroup.SelectedIndexChanged, AddressOf cboClassGroup_SelectedIndexChanged

            dsCombos = objSectionGrp.getComboList("List", True)
            With cboSectionGroup
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dsCombos = objUnitGroup.getComboList("List", True)
            With cboUnitGroup
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dsCombos = objTeam.getComboList("List", True)
            With cboTeams
                .ValueMember = "teamunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RE_HIRE, True, "List")
            With cboChangeReason
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dsCombos = objJobGrp.getComboList("JobGrp", True)
            With cboJobGroup
                .ValueMember = "jobgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("JobGrp")
            End With
            dsCombos = objJob.getComboList("Job", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Job")
            End With
            dsCombos = objGradeGrp.getComboList("GradeGrp", True)
            RemoveHandler cboGradeGroup.SelectedIndexChanged, AddressOf cboGradeGroup_SelectedIndexChanged
            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("GradeGrp")
            End With
            AddHandler cboGradeGroup.SelectedIndexChanged, AddressOf cboGradeGroup_SelectedIndexChanged

            dsCombos = objCostCenter.getComboList("CostCenter", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsCombos.Tables("CostCenter")
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("TranHead", True, 0, 0, enTypeOf.Salary)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, 0, 0, enTypeOf.Salary)
            'Sohail (21 Aug 2015) -- End
            With cboTransactionHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("TranHead")
            End With

            dsCombos = objCountry.getCountryList("List", True)
            With cboIssueCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            With cboResidentIssueCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("List").Copy
                .SelectedValue = 0
            End With
            'S.SANDEEP [04-Jan-2018] -- END

            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, True, "MemCategory")
            With cboMemCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("MemCategory")
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
            objEmployee = Nothing : objStation = Nothing : objCountry = Nothing
            objDeptGrp = Nothing : objDepartment = Nothing
            objSectionGrp = Nothing : objSection = Nothing
            objUnitGroup = Nothing : objUnit = Nothing
            objTeam = Nothing : objClassGrp = Nothing
            objCMaster = Nothing : objJobGrp = Nothing
            objJob = Nothing : objGradeGrp = Nothing
            objCostCenter = Nothing : objTranHead = Nothing
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            dtpEffectiveDate.Value = ConfigParameter._Object._CurrentDateAndTime : dtpEffectiveDate.Checked = False
            cboStation.SelectedValue = 0
            cboDepartmentGrp.SelectedValue = 0
            cboDepartment.SelectedValue = 0
            cboSectionGroup.SelectedValue = 0
            cboSections.SelectedValue = 0
            cboUnitGroup.SelectedValue = 0
            cboUnits.SelectedValue = 0
            cboTeams.SelectedValue = 0
            cboClassGroup.SelectedValue = 0
            cboClass.SelectedValue = 0
            cboChangeReason.SelectedValue = 0
            mdtAppointmentDate = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearControls", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_Current_Transfers()
        Dim dsTransfer As New DataSet
        Dim objETransfer As New clsemployee_transfer_tran
        Try
            'S.SANDEEP [04 JUL 2016] -- START
            Dim dtEffDate As Date = Nothing
            If mAction = enAction.EDIT_ONE Then
                dtEffDate = dtpEffectiveDate.Value
            Else
                dtEffDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [04 JUL 2016] -- END

            If mAction = enAction.EDIT_ONE Then

            End If

            'S.SANDEEP [01 JUN 2016] -- START
            'dsTransfer = objETransfer.Get_Current_Allocation(Now.Date, CInt(cboEmployee.SelectedValue))
            dsTransfer = objETransfer.Get_Current_Allocation(dtEffDate, CInt(cboEmployee.SelectedValue))
            'S.SANDEEP [01 JUN 2016] -- END
            If dsTransfer.Tables(0).Rows.Count > 0 Then
                cboStation.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("stationunkid")
                cboDepartmentGrp.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("deptgroupunkid")
                cboDepartment.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("departmentunkid")
                cboSectionGroup.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("sectiongroupunkid")
                cboSections.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("sectionunkid")
                cboUnitGroup.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("unitgroupunkid")
                cboUnits.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("unitunkid")
                cboTeams.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("teamunkid")
                cboClassGroup.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("classgroupunkid")
                cboClass.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("classunkid")
            Else
                If objEmp IsNot Nothing Then
                    With objEmp
                        cboStation.SelectedValue = ._Stationunkid
                        cboDepartmentGrp.SelectedValue = ._Deptgroupunkid
                        cboDepartment.SelectedValue = ._Departmentunkid
                        cboSectionGroup.SelectedValue = ._Sectiongroupunkid
                        cboSections.SelectedValue = ._Sectionunkid
                        cboUnitGroup.SelectedValue = ._Unitgroupunkid
                        cboUnits.SelectedValue = ._Unitunkid
                        cboTeams.SelectedValue = ._Teamunkid
                        cboClassGroup.SelectedValue = ._Classgroupunkid
                        cboClass.SelectedValue = ._Classunkid
                    End With
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Current_Transfers", mstrModuleName)
        Finally
            dsTransfer.Dispose() : objETransfer = Nothing
        End Try
    End Sub

    Private Sub Set_Current_Recategorization()
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim dsCategorize As New DataSet
        Try
            'S.SANDEEP [04 JUL 2016] -- START
            Dim dtEffDate As Date = Nothing
            If mAction = enAction.EDIT_ONE Then
                dtEffDate = dtpEffectiveDate.Value
            Else
                dtEffDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [04 JUL 2016] -- END
            'S.SANDEEP [01 JUN 2016] -- START
            'dsCategorize = objCategorize.Get_Current_Job(Now.Date, CInt(cboEmployee.SelectedValue))
            dsCategorize = objCategorize.Get_Current_Job(dtEffDate, CInt(cboEmployee.SelectedValue))
            'S.SANDEEP [01 JUN 2016] -- END
            If dsCategorize.Tables(0).Rows.Count > 0 Then
                cboJobGroup.SelectedValue = CInt(dsCategorize.Tables(0).Rows(0).Item("jobgroupunkid"))
                cboJob.SelectedValue = CInt(dsCategorize.Tables(0).Rows(0).Item("jobunkid"))
            Else
                If objEmp IsNot Nothing Then
                    With objEmp
                        cboJobGroup.SelectedValue = ._Jobgroupunkid
                        cboJob.SelectedValue = ._Jobunkid
                    End With
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Current_Recategorization", mstrModuleName)
        Finally
            dsCategorize = Nothing : objCategorize = Nothing
        End Try
    End Sub

    'S.SANDEEP [04-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 120
    Private Sub Set_Current_ResidentPermit()
        Dim objEWPermit As New clsemployee_workpermit_tran
        Dim dsWorkPermit As New DataSet
        Try
            Dim dtEffDate As Date = Nothing
            If mAction = enAction.EDIT_ONE Then
                dtEffDate = dtpEffectiveDate.Value
            Else
                dtEffDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If

            dsWorkPermit = objEWPermit.Get_Current_WorkPermit(dtEffDate, True, CInt(cboEmployee.SelectedValue))
            If dsWorkPermit.Tables(0).Rows.Count > 0 Then
                txtResidentPermitNo.Text = dsWorkPermit.Tables(0).Rows(0).Item("work_permit_no").ToString
                If IsDBNull(dsWorkPermit.Tables(0).Rows(0).Item("issue_date")) = False Then
                    dtpResidentIssueDate.Value = CDate(dsWorkPermit.Tables(0).Rows(0).Item("issue_date"))
                    dtpResidentIssueDate.Checked = True
                End If
                If IsDBNull(dsWorkPermit.Tables(0).Rows(0).Item("expiry_date")) = False Then
                    dtpResidentExpiryDate.Value = CDate(dsWorkPermit.Tables(0).Rows(0).Item("expiry_date"))
                    dtpResidentExpiryDate.Checked = True
                End If
                cboResidentIssueCountry.SelectedValue = dsWorkPermit.Tables(0).Rows(0).Item("workcountryunkid")
                txtResidentIssuePlace.Text = CStr(dsWorkPermit.Tables(0).Rows(0).Item("issue_place"))
            Else
                If objEmp IsNot Nothing Then
                    With objEmp
                        txtResidentPermitNo.Text = ._ResidentPermitNo
                        If ._ResidentIssueDate <> Nothing Then
                            dtpResidentIssueDate.Value = ._ResidentIssueDate
                        End If
                        If ._ResidentExpiryDate <> Nothing Then
                            dtpResidentExpiryDate.Value = ._ResidentExpiryDate
                        End If
                        cboResidentIssueCountry.SelectedValue = ._Workcountryunkid
                        txtResidentIssuePlace.Text = ._ResidentIssuePlace
                    End With
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Current_ResidentPermit", mstrModuleName)
        Finally
            objEWPermit = Nothing : dsWorkPermit.Dispose()
        End Try
    End Sub
    'S.SANDEEP [04-Jan-2018] -- END

    Private Sub Set_Current_WorkPermit()
        Dim objEWPermit As New clsemployee_workpermit_tran
        Dim dsWorkPermit As New DataSet
        Try
            'S.SANDEEP [04 JUL 2016] -- START
            Dim dtEffDate As Date = Nothing
            If mAction = enAction.EDIT_ONE Then
                dtEffDate = dtpEffectiveDate.Value
            Else
                dtEffDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [04 JUL 2016] -- END

            'S.SANDEEP [01 JUN 2016] -- START
            'dsWorkPermit = objEWPermit.Get_Current_WorkPermit(Now.Date, CInt(cboEmployee.SelectedValue))
            dsWorkPermit = objEWPermit.Get_Current_WorkPermit(dtEffDate, False, CInt(cboEmployee.SelectedValue)) 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 -- END
            'S.SANDEEP [01 JUN 2016] -- END
            If dsWorkPermit.Tables(0).Rows.Count > 0 Then
                txtWorkPermitNo.Text = dsWorkPermit.Tables(0).Rows(0).Item("work_permit_no").ToString
                If IsDBNull(dsWorkPermit.Tables(0).Rows(0).Item("issue_date")) = False Then
                    dtpIssueDate.Value = CDate(dsWorkPermit.Tables(0).Rows(0).Item("issue_date"))
                    dtpIssueDate.Checked = True 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 -- END
                End If
                If IsDBNull(dsWorkPermit.Tables(0).Rows(0).Item("expiry_date")) = False Then
                    dtpExpiryDate.Value = CDate(dsWorkPermit.Tables(0).Rows(0).Item("expiry_date"))
                    dtpExpiryDate.Checked = True 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 -- END
                End If
                cboIssueCountry.SelectedValue = dsWorkPermit.Tables(0).Rows(0).Item("workcountryunkid")
                txtPlaceofIssue.Text = CStr(dsWorkPermit.Tables(0).Rows(0).Item("issue_place"))
            Else
                If objEmp IsNot Nothing Then
                    With objEmp
                        txtWorkPermitNo.Text = ._Work_Permit_No
                        If ._Work_Permit_Issue_Date <> Nothing Then
                            dtpIssueDate.Value = ._Work_Permit_Issue_Date
                        End If
                        If ._Work_Permit_Expiry_Date <> Nothing Then
                            dtpExpiryDate.Value = ._Work_Permit_Expiry_Date
                        End If
                        cboIssueCountry.SelectedValue = ._Workcountryunkid
                        txtPlaceofIssue.Text = ._Work_Permit_Issue_Place
                    End With
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_WorkPermit", mstrModuleName)
        Finally
            objEWPermit = Nothing : dsWorkPermit.Dispose()
        End Try
    End Sub

    Private Sub Set_Current_Details()
        Dim dsList As New DataSet
        Dim objECCT As New clsemployee_cctranhead_tran
        Try
            'S.SANDEEP [04 JUL 2016] -- START
            Dim dtEffDate As Date = Nothing
            If mAction = enAction.EDIT_ONE Then
                dtEffDate = dtpEffectiveDate.Value
            Else
                dtEffDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [04 JUL 2016] -- END

            'S.SANDEEP [01 JUN 2016] -- START
            'dsList = objECCT.Get_Current_CostCenter_TranHeads(Now.Date, True, CInt(cboEmployee.SelectedValue))
            dsList = objECCT.Get_Current_CostCenter_TranHeads(dtEffDate, True, CInt(cboEmployee.SelectedValue))
            'S.SANDEEP [01 JUN 2016] -- END
            If dsList.Tables(0).Rows.Count > 0 Then
                cboCostCenter.SelectedValue = dsList.Tables(0).Rows(0).Item("cctranheadvalueid")
            Else
                If objEmp IsNot Nothing Then
                    With objEmp
                        cboCostCenter.SelectedValue = ._Costcenterunkid
                    End With

                End If
            End If

            'S.SANDEEP [01 JUN 2016] -- START
            'dsList = objECCT.Get_Current_CostCenter_TranHeads(Now.Date, False, CInt(cboEmployee.SelectedValue))
            dsList = objECCT.Get_Current_CostCenter_TranHeads(dtEffDate, False, CInt(cboEmployee.SelectedValue))
            'S.SANDEEP [01 JUN 2016] -- END
            If dsList.Tables(0).Rows.Count > 0 Then
                cboTransactionHead.SelectedValue = dsList.Tables(0).Rows(0).Item("cctranheadvalueid")
            Else
                If objEmp IsNot Nothing Then
                    With objEmp
                        cboTransactionHead.SelectedValue = ._Tranhedunkid
                    End With
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Details", mstrModuleName)
        Finally
            dsList.Dispose() : objECCT = Nothing
        End Try
    End Sub

    Private Sub Set_Current_Dates()
        Dim dsDate As New DataSet
        Dim objEDates As New clsemployee_dates_tran
        Try

            'S.SANDEEP [04 JUL 2016] -- START
            Dim dtEffDate As Date = Nothing
            If mAction = enAction.EDIT_ONE Then
                dtEffDate = dtpEffectiveDate.Value
            Else
                dtEffDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [04 JUL 2016] -- END

            'S.SANDEEP [01 JUN 2016] -- START
            'dsDate = objEDates.Get_Current_Dates(Now.Date, enEmp_Dates_Transaction.DT_PROBATION, CInt(cboEmployee.SelectedValue))
            dsDate = objEDates.Get_Current_Dates(dtEffDate, enEmp_Dates_Transaction.DT_PROBATION, CInt(cboEmployee.SelectedValue))
            'S.SANDEEP [01 JUN 2016] -- END
            If dsDate.Tables(0).Rows.Count > 0 Then
                dtpProbationDateFrom.Value = CDate(dsDate.Tables(0).Rows(0).Item("date1"))
                dtpProbationDateFrom.Checked = True
                If IsDBNull(dsDate.Tables(0).Rows(0).Item("date2")) = False Then
                    dtpProbationDateTo.Value = CDate(dsDate.Tables(0).Rows(0).Item("date2"))
                    dtpProbationDateTo.Checked = True
                End If
            Else
                If objEmp IsNot Nothing Then
                    With objEmp
                        If ._Probation_From_Date <> Nothing Then
                            dtpProbationDateFrom.Value = ._Probation_From_Date
                        End If
                        If ._Probation_To_Date <> Nothing Then
                            dtpProbationDateTo.Value = ._Probation_To_Date
                        End If
                    End With
                End If
            End If

            'S.SANDEEP [01 JUN 2016] -- START
            'dsDate = objEDates.Get_Current_Dates(Now.Date, enEmp_Dates_Transaction.DT_CONFIRMATION, CInt(cboEmployee.SelectedValue))
            dsDate = objEDates.Get_Current_Dates(dtEffDate, enEmp_Dates_Transaction.DT_CONFIRMATION, CInt(cboEmployee.SelectedValue))
            'S.SANDEEP [01 JUN 2016] -- END
            If dsDate.Tables(0).Rows.Count > 0 Then
                dtpConfirmationDate.Value = CDate(dsDate.Tables(0).Rows(0).Item("date1"))
            Else
                If objEmp IsNot Nothing Then
                    With objEmp
                        dtpConfirmationDate.Value = ._Confirmation_Date
                    End With
                End If
            End If

            'S.SANDEEP [01 JUN 2016] -- START
            'dsDate = objEDates.Get_Current_Dates(Now.Date, enEmp_Dates_Transaction.DT_TERMINATION, CInt(cboEmployee.SelectedValue))
            dsDate = objEDates.Get_Current_Dates(dtEffDate, enEmp_Dates_Transaction.DT_TERMINATION, CInt(cboEmployee.SelectedValue))
            'S.SANDEEP [01 JUN 2016] -- END
            If dsDate.Tables(0).Rows.Count > 0 Then
                'S.SANDEEP [21-Mar-2018] -- START
                'ISSUE/ENHANCEMENT : {#ARUTI-57}
                'dtpEndEmplDate.Value = CDate(dsDate.Tables(0).Rows(0).Item("date1"))
                'dtpEndEmplDate.Checked = True
                If IsDBNull(dsDate.Tables(0).Rows(0).Item("date1")) = False Then
                dtpEndEmplDate.Value = CDate(dsDate.Tables(0).Rows(0).Item("date1"))
                dtpEndEmplDate.Checked = True
                Else
                    dtpEndEmplDate.Checked = False
                End If
                'S.SANDEEP [21-Mar-2018] -- END
                
                If IsDBNull(dsDate.Tables(0).Rows(0).Item("date2")) = False Then
                    dtpLeavingDate.Value = CDate(dsDate.Tables(0).Rows(0).Item("date2"))
                    dtpLeavingDate.Checked = True
                Else
                    dtpLeavingDate.Checked = False
                End If
                chkExclude.Checked = CBool(dsDate.Tables(0).Rows(0).Item("isexclude_payroll"))
            Else
                If objEmp IsNot Nothing Then
                    With objEmp
                        If ._Empl_Enddate <> Nothing Then
                            dtpEndEmplDate.Value = ._Empl_Enddate
                        End If
                        If ._Termination_From_Date <> Nothing Then
                            dtpLeavingDate.Value = ._Termination_From_Date
                        End If
                        chkExclude.Checked = ._Isexclude_Payroll
                    End With
                End If
            End If

            'S.SANDEEP [01 JUN 2016] -- START
            'dsDate = objEDates.Get_Current_Dates(Now.Date, enEmp_Dates_Transaction.DT_RETIREMENT, CInt(cboEmployee.SelectedValue))
            dsDate = objEDates.Get_Current_Dates(dtEffDate, enEmp_Dates_Transaction.DT_RETIREMENT, CInt(cboEmployee.SelectedValue))
            'S.SANDEEP [01 JUN 2016] -- END
            If dsDate.Tables(0).Rows.Count > 0 Then
                dtpRetirementDate.Value = CDate(dsDate.Tables(0).Rows(0).Item("date1"))
            Else
                If objEmp IsNot Nothing Then
                    With objEmp
                        dtpRetirementDate.Value = ._Termination_To_Date
                    End With
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Dates", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_Current_Salary_Grades()
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData
        Try
            'S.SANDEEP [04 JUL 2016] -- START
            Dim dtEffDate As Date = Nothing
            If mAction = enAction.EDIT_ONE Then
                dtEffDate = DateTime.Today
            Else
                dtEffDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'S.SANDEEP [04 JUL 2016] -- END

            dsList = objMaster.Get_Current_Scale("Salary", CInt(cboEmployee.SelectedValue), dtEffDate)
            If dsList.Tables("Salary").Rows.Count > 0 Then
                cboGradeGroup.SelectedValue = CInt(dsList.Tables("Salary").Rows(0).Item("gradegroupunkid").ToString)
                cboGrade.SelectedValue = CInt(dsList.Tables("Salary").Rows(0).Item("gradeunkid").ToString)
                cboLevel.SelectedValue = CInt(dsList.Tables("Salary").Rows(0).Item("gradelevelunkid").ToString)
            Else
                If objEmp IsNot Nothing Then
                    With objEmp
                        cboGradeGroup.SelectedValue = ._Gradegroupunkid
                        cboGrade.SelectedValue = ._Gradeunkid
                        cboLevel.SelectedValue = ._Gradelevelunkid
                    End With
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Current_Salary_Grades", mstrModuleName)
        Finally
            dsList.Dispose() : objMaster = Nothing
        End Try
    End Sub

    Private Sub FillMembershipList()
        Try
            lvMembershipInfo.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtMemTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem

                    Dim objComm_Mster As New clsCommon_Master
                    objComm_Mster._Masterunkid = CInt(dtRow.Item("membership_categoryunkid"))
                    lvItem.Text = objComm_Mster._Name
                    objComm_Mster = Nothing

                    Dim objMembership As New clsmembership_master
                    objMembership._Membershipunkid = CInt(dtRow.Item("membershipunkid"))
                    lvItem.SubItems.Add(objMembership._Membershipname)
                    lvItem.SubItems.Add(dtRow.Item("membershipno").ToString)

                    If IsDBNull(dtRow.Item("issue_date")) Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(CDate(dtRow.Item("issue_date").ToString).ToShortDateString)
                    End If

                    If IsDBNull(dtRow.Item("start_date")) Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(CDate(dtRow.Item("start_date").ToString).ToShortDateString)
                    End If

                    If IsDBNull(dtRow.Item("expiry_date")) Then
                        lvItem.SubItems.Add("")
                    Else
                        lvItem.SubItems.Add(CDate(dtRow.Item("expiry_date").ToString).ToShortDateString)
                    End If

                    lvItem.SubItems.Add(dtRow.Item("remark").ToString)
                    lvItem.SubItems.Add(dtRow.Item("membership_categoryunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("membershipunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    lvItem.SubItems.Add(dtRow.Item("ccategory").ToString)
                    lvItem.SubItems.Add(dtRow.Item("ccategoryid").ToString)
                    lvItem.Tag = dtRow.Item("membershiptranunkid")

                    lvMembershipInfo.Items.Add(lvItem)

                    lvItem = Nothing
                End If
            Next

            lvMembershipInfo.SortBy(objcolhccategoryId.Index, SortOrder.Ascending)
            lvMembershipInfo.GroupingColumn = objcolhccategory
            lvMembershipInfo.DisplayGroups(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMembershipList", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetMembership()
        Try
            cboMembershipType.SelectedValue = 0
            cboMemCategory.SelectedValue = 0
            txtMembershipNo.Text = ""
            dtpMemIssueDate.Checked = False
            dtpStartDate.Checked = False
            dtpEndDate.Checked = False
            txtRemark.Text = ""
            cboMemCategory.Enabled = True : cboMembershipType.Enabled = True
            objbtnAddMemCategory.Enabled = True : objbtnAddMembership.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetMembership", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid_Process() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Employee is mandatory information. Please select employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If dtpEffectiveDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Effective date is mandatory information. Please set effective date to continue."), enMsgBoxStyle.Information)
                dtpEffectiveDate.Focus()
                Return False
            End If

            If dtpReinstatementDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Reinstatement date is mandatory information. Please set reinstatement date to continue."), enMsgBoxStyle.Information)
                dtpReinstatementDate.Focus()
                Return False
            End If

            'S.SANDEEP [29 APR 2015] -- START
            If dtpReinstatementDate.Value.Date < CDate(txtAppDate.Tag) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, Reinstatement date cannot be less than appointment date. Please set correct reinstatement date to continue."), enMsgBoxStyle.Information)
                dtpReinstatementDate.Focus()
                Return False
            End If
            'S.SANDEEP [29 APR 2015] -- END

            If dtpReinstatementDate.Value.Date < dtpEffectiveDate.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Reinstatement date cannot be less than effective date. Please set correct reinstatement date to continue."), enMsgBoxStyle.Information)
                dtpReinstatementDate.Focus()
                Return False
            End If

            If dtpProbationDateFrom.Checked = True AndAlso dtpProbationDateTo.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Probation end date is mandatory information when probation start date is checked."), enMsgBoxStyle.Information)
                dtpProbationDateTo.Focus()
                Return False
            ElseIf dtpProbationDateFrom.Checked = False AndAlso dtpProbationDateTo.Checked = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Probation start date is mandatory information when probation end date is checked"), enMsgBoxStyle.Information)
                dtpProbationDateFrom.Focus()
                Return False
            End If

            If dtpProbationDateFrom.Checked = True Then
                If dtpProbationDateFrom.Value.Date < dtpReinstatementDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Probation start date cannot be less than the reinstatement date."), enMsgBoxStyle.Information)
                    dtpProbationDateFrom.Focus()
                    Return False
                End If
            End If

            If dtpProbationDateFrom.Checked = True AndAlso dtpProbationDateTo.Checked = True Then
                If dtpProbationDateTo.Value.Date < dtpProbationDateFrom.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Probation end date cannot be less then probation start date."), enMsgBoxStyle.Information)
                    dtpProbationDateFrom.Focus()
                    Return False
                End If
            End If

            If dtpEndEmplDate.Checked = True AndAlso dtpLeavingDate.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Leaving end date is mandatory information when end of contract date is checked."), enMsgBoxStyle.Information)
                dtpLeavingDate.Focus()
                Return False
            ElseIf dtpEndEmplDate.Checked = False AndAlso dtpLeavingDate.Checked = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, End of contract date is mandatory information when leaving date is checked"), enMsgBoxStyle.Information)
                dtpEndEmplDate.Focus()
                Return False
            End If

            'S.SANDEEP [29 APR 2015] -- START
            If dtpEndEmplDate.Checked = True Then
                If dtpEndEmplDate.Value.Date <= dtpReinstatementDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, End of contract date cannot be less than or equal to Reinstatement date. Please set correct End of contract date to continue."), enMsgBoxStyle.Information)
                    dtpReinstatementDate.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP [29 APR 2015] -- END

            If dtpEndEmplDate.Checked = True AndAlso dtpLeavingDate.Checked = True Then
                If dtpLeavingDate.Value.Date < dtpEndEmplDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, Leaving date cannot be less than end of contract date."), enMsgBoxStyle.Information)
                    dtpEndEmplDate.Focus()
                    Return False
                End If
            End If

            If dtpRetirementDate.Value.Date <= dtpReinstatementDate.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Retirement date cannot be less than or equal to reinstatement date."), enMsgBoxStyle.Information)
                dtpRetirementDate.Focus()
                Return False
            End If

            'S.SANDEEP [12 MAY 2015] -- START
            Dim objEmployee As New clsEmployee_Master
            If dtpEndEmplDate.Checked = True Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objEmployee.IsEmpTerminated(dtpEndEmplDate.Value.Date, CInt(cboEmployee.SelectedValue)) = False Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Sohail (09 Oct 2019) -- Start
                'NMB Enhancement # : allow to change eoc / retirement date in current open or last closed period even after salary is paid or period is closed.
                'If objEmployee.IsEmpTerminated(dtpEndEmplDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                If objEmployee.IsEmpTerminated(dtpEndEmplDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, blnSkipForLastClosedPeriod:=User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod) = False Then
                    'Sohail (09 Oct 2019) -- End
                    'If objEmployee.IsEmpTerminated(dtpEndEmplDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName) = False Then
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (21 Aug 2015) -- End
                    eZeeMsgBox.Show(objEmployee._Message, enMsgBoxStyle.Information)
                    Exit Function
                End If
            End If

            If dtpLeavingDate.Checked = True Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objEmployee.IsEmpTerminated(dtpLeavingDate.Value.Date, CInt(cboEmployee.SelectedValue)) = False Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Sohail (09 Oct 2019) -- Start
                'NMB Enhancement # : allow to change eoc / retirement date in current open or last closed period even after salary is paid or period is closed.
                'If objEmployee.IsEmpTerminated(dtpLeavingDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid) = False Then
                If objEmployee.IsEmpTerminated(dtpLeavingDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, blnSkipForLastClosedPeriod:=User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod) = False Then
                    'Sohail (09 Oct 2019) -- End
                    'If objEmployee.IsEmpTerminated(dtpLeavingDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName) = False Then
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (21 Aug 2015) -- End
                    eZeeMsgBox.Show(objEmployee._Message, enMsgBoxStyle.Information)
                    Exit Function
                End If
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objEmployee.IsEmpTerminated(dtpRetirementDate.Value.Date, CInt(cboEmployee.SelectedValue), True) = False Then

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If objEmployee.IsEmpTerminated(dtpRetirementDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, True) = False Then
                'If objEmployee.IsEmpTerminated(dtpRetirementDate.Value.Date, CInt(cboEmployee.SelectedValue), FinancialYear._Object._DatabaseName, True) = False Then
                'S.SANDEEP [04 JUN 2015] -- END

                'Sohail (21 Aug 2015) -- End
                eZeeMsgBox.Show(objEmployee._Message, enMsgBoxStyle.Information)
                Exit Function
            End If
            objEmployee = Nothing
            'S.SANDEEP [12 MAY 2015] -- END

            If (dtpIssueDate.Checked = True And dtpExpiryDate.Checked = False) Or (dtpIssueDate.Checked = False And dtpExpiryDate.Checked = True) Then
                eZeeMsgBox.Show(Language.getMessage("frmWorkPermitTransaction", 5, "Sorry, issue date and expiry date are madatory information."), enMsgBoxStyle.Information)
                dtpExpiryDate.Focus()
                Return False
            End If

            If dtpIssueDate.Checked = True AndAlso dtpExpiryDate.Checked = True Then
                If dtpExpiryDate.Value.Date <= dtpIssueDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage("frmWorkPermitTransaction", 6, "Sorry, expiry date cannot be less or equal to issue date."), enMsgBoxStyle.Information)
                    dtpExpiryDate.Focus()
                    Return False
                End If
            End If

            '******************* EFFECTIVE DATE VALIDATION *****************' # START #

            'S.SANDEEP [04 JUL 2016] -- START
            If mAction <> enAction.EDIT_ONE Then
                If dtpEffectiveDate.Tag Is Nothing Then dtpEffectiveDate.Tag = eZeeDate.convertDate(dtpEffectiveDate.Value.Date.AddDays(-1)).ToString
            End If
            'S.SANDEEP [04 JUL 2016] -- END

            If dtpEffectiveDate.Tag.ToString <> eZeeDate.convertDate(dtpEffectiveDate.Value).ToString Then 'S.SANDEEP [01 JUN 2016] -- START -- END

                '#############< EMPLOYEE TRANSFERS >#################### --> START
                Dim objETransfer As New clsemployee_transfer_tran

                If objETransfer.isExist(dtpEffectiveDate.Value.Date, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, CInt(cboEmployee.SelectedValue)) Then
                    eZeeMsgBox.Show(Language.getMessage("clsemployee_transfer_tran", 1, "Sorry, transfer information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    objETransfer = Nothing
                    Return False
                End If

                objETransfer = Nothing
                '#############< EMPLOYEE TRANSFERS >#################### --> END


                '#############< EMPLOYEE RECATEGORIZATION >#################### --> START
                Dim objERecategorize As New clsemployee_categorization_Tran

                If objERecategorize.isExist(dtpEffectiveDate.Value.Date, CInt(cboEmployee.SelectedValue), -1, -1, -1, -1, -1) Then
                    eZeeMsgBox.Show(Language.getMessage("clsemployee_categorization_Tran", 1, "Sorry, Re-Categorize information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    objERecategorize = Nothing
                    Return False
                End If

                objERecategorize = Nothing
                '#############< EMPLOYEE RECATEGORIZATION >#################### --> END


                '#############< EMPLOYEE WORK PERMIT >#################### --> START
                Dim objEWPrmit As New clsemployee_workpermit_tran

                If objEWPrmit.isExist(False, dtpEffectiveDate.Value.Date, , CInt(cboEmployee.SelectedValue)) Then
                    eZeeMsgBox.Show(Language.getMessage("clsemployee_workpermit_tran", 1, "Sorry, work permit information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    objEWPrmit = Nothing
                    Return False
                End If

                'S.SANDEEP [23-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002339|ARUTI-214}
                If dtpExpiryDate.Checked = True AndAlso dtpExpiryDate.Value.Date < dtpReinstatementDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, Work Permit expiry date must be greater than reinstatement date."), enMsgBoxStyle.Information)
                    objEWPrmit = Nothing
                    Return False
                End If
                'S.SANDEEP [23-JUN-2018] -- END

                objEWPrmit = Nothing
                '#############< EMPLOYEE WORK PERMIT >#################### --> END

                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                '#############< EMPLOYEE RESIDENT PERMIT >#################### --> START
                objEWPrmit = New clsemployee_workpermit_tran

                If objEWPrmit.isExist(True, dtpEffectiveDate.Value.Date, , CInt(cboEmployee.SelectedValue)) Then
                    eZeeMsgBox.Show(Language.getMessage("clsemployee_workpermit_tran", 4, "Sorry, resident permit information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    objEWPrmit = Nothing
                    Return False
                End If

                'S.SANDEEP [23-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002339|ARUTI-214}
                If dtpResidentExpiryDate.Checked = True AndAlso dtpResidentExpiryDate.Value.Date < dtpReinstatementDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, Resident permit expiry date must be greater than reinstatement date."), enMsgBoxStyle.Information)
                    objEWPrmit = Nothing
                    Return False
                End If
                'S.SANDEEP [23-JUN-2018] -- END

                objEWPrmit = Nothing
                '#############< EMPLOYEE RESIDENT PERMIT >#################### --> END
                'S.SANDEEP [04-Jan-2018] -- END


                '#############< EMPLOYEE DATES >#################### --> START
                Dim objEDates As New clsemployee_dates_tran

                'S.SANDEEP [20 JUL 2016] -- START
                Dim dsList As New DataSet
                'S.SANDEEP [20 JUL 2016] -- END

                '/* -- CONFIRMATION DATE VALIDATION --*/'

                'S.SANDEEP [20 JUL 2016] -- START
                'If objEDates.isExist(dtpEffectiveDate.Value.Date, Nothing, Nothing, enEmp_Dates_Transaction.DT_CONFIRMATION, CInt(cboEmployee.SelectedValue)) Then
                '    eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 16, "Sorry, confirmation information is already present for the selected effective date."), enMsgBoxStyle.Information)
                '    objEDates = Nothing
                '    Return False
                'End If

                dsList = objEDates.Get_Current_Dates(dtpEffectiveDate.Value.Date, enEmp_Dates_Transaction.DT_CONFIRMATION, CInt(cboEmployee.SelectedValue))
                If dsList.Tables(0).Rows.Count > 0 Then
                    If dsList.Tables(0).Rows(0).Item("ddate1").ToString <> eZeeDate.convertDate(dtpConfirmationDate.Value) Then
                        If objEDates.isExist(dtpEffectiveDate.Value.Date, Nothing, Nothing, enEmp_Dates_Transaction.DT_CONFIRMATION, CInt(cboEmployee.SelectedValue)) Then
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 16, "Sorry, confirmation information is already present for the selected effective date."), enMsgBoxStyle.Information)
                            objEDates = Nothing
                            Return False
                        End If
                    End If
                End If
                'S.SANDEEP [20 JUL 2016] -- END

                '/* -- CONFIRMATION DATE VALIDATION --*/'


                '/* -- PROBATION DATE VALIDATION --*/'
                'S.SANDEEP [20 JUL 2016] -- START
                'If objEDates.isExist(dtpEffectiveDate.Value.Date, Nothing, Nothing, enEmp_Dates_Transaction.DT_PROBATION, CInt(cboEmployee.SelectedValue)) Then
                '    eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 1, "Sorry, porbation information is already present for the selected effective date."), enMsgBoxStyle.Information)
                '    objEDates = Nothing
                '    Return False
                'End If
                If dtpProbationDateFrom.Checked = True AndAlso dtpProbationDateTo.Checked = True Then
                    dsList = objEDates.Get_Current_Dates(dtpEffectiveDate.Value.Date, enEmp_Dates_Transaction.DT_PROBATION, CInt(cboEmployee.SelectedValue))
                    If dsList.Tables(0).Rows.Count > 0 Then
                        If dsList.Tables(0).Rows(0).Item("ddate1").ToString <> eZeeDate.convertDate(dtpProbationDateFrom.Value) AndAlso _
                           dsList.Tables(0).Rows(0).Item("ddate2").ToString <> eZeeDate.convertDate(dtpProbationDateTo.Value) Then

                            If objEDates.isExist(dtpEffectiveDate.Value.Date, Nothing, Nothing, enEmp_Dates_Transaction.DT_PROBATION, CInt(cboEmployee.SelectedValue)) Then
                                eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 1, "Sorry, porbation information is already present for the selected effective date."), enMsgBoxStyle.Information)
                                objEDates = Nothing
                                Return False
                            End If

                        End If
                    End If
                End If
                'S.SANDEEP [20 JUL 2016] -- END

                '/* -- PROBATION DATE VALIDATION --*/'


                '/* -- TERMINATION DATE VALIDATION --*/'

                'S.SANDEEP [20 JUL 2016] -- START

                'S.SANDEEP [21-Mar-2018] -- START
                'ISSUE/ENHANCEMENT : {#ARUTI-57}
                            If objEDates.isExist(dtpEffectiveDate.Value.Date, Nothing, Nothing, enEmp_Dates_Transaction.DT_TERMINATION, CInt(cboEmployee.SelectedValue)) Then
                                eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 3, "Sorry, termination information is already present for the selected effective date."), enMsgBoxStyle.Information)
                                objEDates = Nothing
                                Return False
                            End If

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                If dtpEndEmplDate.Checked = True AndAlso dtpLeavingDate.Checked = True Then
                    If objEDates.isExist(Nothing, dtpEndEmplDate.Value.Date, dtpLeavingDate.Value.Date, enEmp_Dates_Transaction.DT_TERMINATION, CInt(cboEmployee.SelectedValue)) Then
                        eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 7, "Sorry, This termination information is already present."), enMsgBoxStyle.Information)
                        objEDates = Nothing
                        Return False
                    End If
                End If
                'S.SANDEEP |21-AUG-2019| -- END


                'If dtpEndEmplDate.Checked = True AndAlso dtpLeavingDate.Checked = True Then
                '    dsList = objEDates.Get_Current_Dates(dtpEffectiveDate.Value.Date, enEmp_Dates_Transaction.DT_TERMINATION, CInt(cboEmployee.SelectedValue))
                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        If dsList.Tables(0).Rows(0).Item("ddate1").ToString <> eZeeDate.convertDate(dtpEndEmplDate.Value) AndAlso _
                '           dsList.Tables(0).Rows(0).Item("ddate2").ToString <> eZeeDate.convertDate(dtpLeavingDate.Value) Then

                '            If objEDates.isExist(dtpEffectiveDate.Value.Date, Nothing, Nothing, enEmp_Dates_Transaction.DT_TERMINATION, CInt(cboEmployee.SelectedValue)) Then
                '                eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 3, "Sorry, termination information is already present for the selected effective date."), enMsgBoxStyle.Information)
                '                objEDates = Nothing
                '                Return False
                '            End If

                '        End If
                '    End If
                'End If
                'S.SANDEEP [21-Mar-2018] -- END


                'S.SANDEEP [20 JUL 2016] -- END


                'S.SANDEEP [22-MAR-2017] -- START
                'ISSUE/ENHANCEMENT : Tanapa PE Forms Statutory Report
                Dim objTnALeaveTran As New clsTnALeaveTran
                Dim objMData As New clsMasterData : Dim iPeriod As Integer = 0
                iPeriod = objMData.getCurrentPeriodID(enModuleReference.Payroll, dtpEndEmplDate.Value.Date, FinancialYear._Object._YearUnkid, enStatusType.Open, True)
                If iPeriod <= 0 Then
                    objMData.getCurrentPeriodID(enModuleReference.Payroll, dtpLeavingDate.Value.Date, FinancialYear._Object._YearUnkid, enStatusType.Open, True)
                End If
                'Sohail (09 Oct 2019) -- Start
                'NMB Enhancement # : privilege  Allow to change date on Closed Period on user master to change eoc / termination date in paid period or closed period.
                'If iPeriod > 0 Then
                If iPeriod > 0 AndAlso User._Object.Privilege._AllowToChangeEOCLeavingDateOnClosedPeriod = True Then
                    'Sohail (09 Oct 2019) -- End
                    Dim objPrd As New clscommom_period_Tran
                    objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriod
                    If objTnALeaveTran.IsPayrollProcessDone(iPeriod, CStr(cboEmployee.SelectedValue), objPrd._End_Date, enModuleReference.Payroll) Then
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, you cannot exclude selected employee. Reason:Payroll Process already done for the employee for last date of current open period."), enMsgBoxStyle.Information)
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, you cannot exclude selected employee. Reason:Payroll Process already done for the employee for last date of current open period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 20, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                        Return False
                    End If
                End If
                objTnALeaveTran = Nothing
                'S.SANDEEP [22-MAR-2017] -- END

                '/* -- TERMINATION DATE VALIDATION --*/'


                '/* -- RETIREMENT DATE VALIDATION --*/'

                'S.SANDEEP [20 JUL 2016] -- START
                'If objEDates.isExist(dtpEffectiveDate.Value.Date, Nothing, Nothing, enEmp_Dates_Transaction.DT_RETIREMENT, CInt(cboEmployee.SelectedValue)) Then
                '    eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 4, "Sorry, retirement information is already present for the selected effective date."), enMsgBoxStyle.Information)
                '    objEDates = Nothing
                '    Return False
                'End If

                dsList = objEDates.Get_Current_Dates(dtpEffectiveDate.Value.Date, enEmp_Dates_Transaction.DT_RETIREMENT, CInt(cboEmployee.SelectedValue))
                If dsList.Tables(0).Rows.Count > 0 Then
                    If dsList.Tables(0).Rows(0).Item("ddate1").ToString <> eZeeDate.convertDate(dtpRetirementDate.Value) Then

                        If objEDates.isExist(dtpEffectiveDate.Value.Date, Nothing, Nothing, enEmp_Dates_Transaction.DT_RETIREMENT, CInt(cboEmployee.SelectedValue)) Then
                            eZeeMsgBox.Show(Language.getMessage("clsemployee_dates_tran", 4, "Sorry, retirement information is already present for the selected effective date."), enMsgBoxStyle.Information)
                            objEDates = Nothing
                            Return False
                        End If
                    End If
                End If
                'S.SANDEEP [20 JUL 2016] -- END

                '/* -- RETIREMENT DATE VALIDATION --*/'

                objEDates = Nothing
                '#############< EMPLOYEE DATES >#################### --> END


                '#############< EMPLOYEE COST-CENTER & TRANSACTION HEAD CHANGES >#################### --> START
                Dim objECC As New clsemployee_cctranhead_tran

                If objECC.isExist(dtpEffectiveDate.Value.Date, -1, True, CInt(cboEmployee.SelectedValue)) Then
                    eZeeMsgBox.Show(Language.getMessage("clsemployee_cctranhead_tran", 2, "Sorry, cost center information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    cboCostCenter.Focus()
                    objECC = Nothing
                    Return False
                End If

                If objECC.isExist(dtpEffectiveDate.Value.Date, -1, False, CInt(cboEmployee.SelectedValue)) Then
                    eZeeMsgBox.Show(Language.getMessage("clsemployee_cctranhead_tran", 1, "Sorry, transaction head information is already present for the selected effective date."), enMsgBoxStyle.Information)
                    cboTransactionHead.Focus()
                    objECC = Nothing
                    Return False
                End If

                objECC = Nothing
                '#############< EMPLOYEE COST-CENTER & TRANSACTION HEAD CHANGES >#################### --> END

                '#############< EMPLOYEE SALARY CHANGES >#################### --> START

                'S.SANDEEP |06-JUL-2020| -- START
                'ISSUE/ENHANCEMENT : NMB ENHANCEMENT (REHIRE ON CLOSE PERIOD)
                'Dim objMst As New clsMasterData

                ''S.SANDEEP [04 JUN 2015] -- START
                ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                ''xPeriodUnkid = objMst.getCurrentPeriodID(enModuleReference.Payroll, dtpEffectiveDate.Value.Date, enStatusType.Open, , True)
                'xPeriodUnkid = objMst.getCurrentPeriodID(enModuleReference.Payroll, dtpEffectiveDate.Value.Date, FinancialYear._Object._YearUnkid, enStatusType.Open, True)
                ''S.SANDEEP [04 JUN 2015] -- END

                'objMst = Nothing
                'If xPeriodUnkid <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, there is no period defined in payroll, for the selected effective date."), enMsgBoxStyle.Information)
                '    Return False
                'Else
                '    Dim objSalInc As New clsSalaryIncrement
                '    If objSalInc.isExistOnSameDate(dtpEffectiveDate.Value.Date, CInt(cboEmployee.SelectedValue)) Then
                '        eZeeMsgBox.Show(Language.getMessage("clsSalaryIncrement", 1, "Sorry! Increment for the employee on the selected date is already done."), enMsgBoxStyle.Information)
                '        objSalInc = Nothing
                '        Return False
                '    End If
                'End If

                If ConfigParameter._Object._AllowRehireOnClosedPeriod = False Then
                Dim objMst As New clsMasterData
                xPeriodUnkid = objMst.getCurrentPeriodID(enModuleReference.Payroll, dtpEffectiveDate.Value.Date, FinancialYear._Object._YearUnkid, enStatusType.Open, True)
                objMst = Nothing
                If xPeriodUnkid <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, there is no period defined in payroll, for the selected effective date."), enMsgBoxStyle.Information)
                    Return False
                    End If
                End If        

                    Dim objSalInc As New clsSalaryIncrement
                    If objSalInc.isExistOnSameDate(dtpEffectiveDate.Value.Date, CInt(cboEmployee.SelectedValue)) Then
                        eZeeMsgBox.Show(Language.getMessage("clsSalaryIncrement", 1, "Sorry! Increment for the employee on the selected date is already done."), enMsgBoxStyle.Information)
                        objSalInc = Nothing
                        Return False
                    End If
                'S.SANDEEP |06-JUL-2020| -- END

                

                'If xPeriodUnkid > 0 Then
                '    Dim objTnA As New clsTnALeaveTran
                '    Dim objPrd As New clscommom_period_Tran
                '    objPrd._Periodunkid = xPeriodUnkid
                '    If objTnA.IsPayrollProcessDone(xPeriodUnkid, cboEmployee.SelectedValue.ToString, objPrd._End_Date) = True Then
                '        eZeeMsgBox.Show(Language.getMessage("frmSalaryIncrement_AddEdit", 23, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to give salary change."), enMsgBoxStyle.Information)
                '        objTnA = Nothing
                '        objPrd = Nothing
                '        Exit Try
                '    End If
                '    objTnA = Nothing
                'End If
                '#############< EMPLOYEE SALARY CHANGES >#################### --> END

            End If 'S.SANDEEP [01 JUN 2016] -- START -- END
            '******************* EFFECTIVE DATE VALIDATION *****************' # END #

            If CInt(cboTransactionHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Transaction head is mandatory information. Please select Transaction head to continue."), enMsgBoxStyle.Information)
                cboTransactionHead.Focus()
                Return False
            End If

            If CInt(cboChangeReason.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Change reason is mandatory information. Please select reason to continue."), enMsgBoxStyle.Information)
                cboChangeReason.Focus()
                Return False
            End If
           'S.SANDEEP |13-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : No Validation Kept for Mandatory Data Which are there in Employee Master

            If CInt(cboDepartment.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 6, "Department is compulsory information. Please select Department."), enMsgBoxStyle.Information)
                cboDepartment.Focus()
                Return False
            End If

            If CInt(cboJob.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 7, "Job is compulsory information. Please select Job."), enMsgBoxStyle.Information)
                cboJob.Focus()
                Return False
            End If

            If CInt(cboGradeGroup.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 8, "Grade Group is compulsory information. Please select Grade Group."), enMsgBoxStyle.Information)
                cboGradeGroup.Focus()
                Return False
            End If

            If CInt(cboGrade.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 9, "Grade is compulsory information. Please select Grade."), enMsgBoxStyle.Information)
                cboGrade.Focus()
                Return False
            End If

            If CInt(cboLevel.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 10, "Grade Level is compulsory information. Please select Grade Level."), enMsgBoxStyle.Information)
                cboLevel.Focus()
                Return False
            End If

            If CDec(txtScale.Decimal) < 0 Or txtScale.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 24, "Scale cannot be blank or zero. Please set Scale from  Payroll - > Payroll Transaction -> Wages. to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            If CInt(cboCostCenter.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 33, "Cost Center is compulsory information. Please select Cost Center to continue."), enMsgBoxStyle.Information)
                cboCostCenter.Focus()
                Return False
            End If
            'S.SANDEEP |13-MAR-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid_Process", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub SetValue()
        Try
            objEReHire._Effectivedate = dtpEffectiveDate.Value
            objEReHire._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEReHire._Reinstatment_Date = dtpReinstatementDate.Value.Date
            objEReHire._Changereasonunkid = CInt(cboChangeReason.SelectedValue)


            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            If dtpActualDate.Checked Then
                objEReHire._ActualDate = dtpActualDate.Value
            Else
                objEReHire._ActualDate = Nothing
            End If
            'Pinkal (07-Mar-2020) -- End

            '########################## EMPLOYEE TRANSFERS ########################## -- START
            objEReHire._Stationunkid = CInt(cboStation.SelectedValue)
            objEReHire._Deptgroupunkid = CInt(cboDepartmentGrp.SelectedValue)
            objEReHire._Departmentunkid = CInt(cboDepartment.SelectedValue)
            objEReHire._Sectiongroupunkid = CInt(cboSectionGroup.SelectedValue)
            objEReHire._Sectionunkid = CInt(cboSections.SelectedValue)
            objEReHire._Unitgroupunkid = CInt(cboUnitGroup.SelectedValue)
            objEReHire._Unitunkid = CInt(cboUnits.SelectedValue)
            objEReHire._Teamunkid = CInt(cboTeams.SelectedValue)
            objEReHire._Classgroupunkid = CInt(cboClassGroup.SelectedValue)
            objEReHire._Classunkid = CInt(cboClass.SelectedValue)
            '########################## EMPLOYEE TRANSFERS ########################## -- END

            '########################## EMPLOYEE DATES ########################## -- START
            objEReHire._ConfirmationDate = dtpConfirmationDate.Value.Date
            If dtpEndEmplDate.Checked = True Then
                objEReHire._EOCDate = dtpEndEmplDate.Value.Date
            Else
                objEReHire._EOCDate = Nothing
            End If
            objEReHire._ExcludeFromPayroll = chkExclude.Checked
            If dtpLeavingDate.Checked = True Then
                objEReHire._LeavingDate = dtpLeavingDate.Value.Date
            Else
                objEReHire._LeavingDate = Nothing
            End If
            If dtpProbationDateFrom.Checked = True Then
                objEReHire._ProbationFromDate = dtpProbationDateFrom.Value.Date
            Else
                objEReHire._ProbationFromDate = Nothing
            End If
            If dtpProbationDateTo.Checked = True Then
                objEReHire._ProbationToDate = dtpProbationDateTo.Value.Date
            Else
                objEReHire._ProbationToDate = Nothing
            End If
            objEReHire._RetirementDate = dtpRetirementDate.Value.Date
            '########################## EMPLOYEE DATES ########################## -- END

            '########################## EMPLOYEE WORK PERMIT ########################## -- START
            If dtpIssueDate.Checked = True Then
                objEReHire._Issue_Date = dtpIssueDate.Value.Date
            Else
                objEReHire._Issue_Date = Nothing
            End If
            If dtpExpiryDate.Checked = True Then
                objEReHire._Expiry_Date = dtpExpiryDate.Value.Date
            Else
                objEReHire._Expiry_Date = Nothing
            End If
            objEReHire._Issue_Place = txtPlaceofIssue.Text
            objEReHire._Work_Permit_No = txtWorkPermitNo.Text
            objEReHire._Workcountryunkid = CInt(cboIssueCountry.SelectedValue)
            '########################## EMPLOYEE WORK PERMIT ########################## -- END

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            '########################## EMPLOYEE RESIDENT PERMIT ########################## -- START
            If dtpResidentIssueDate.Checked = True Then
                objEReHire._ResidentIssue_Date = dtpResidentIssueDate.Value.Date
            Else
                objEReHire._ResidentIssue_Date = Nothing
            End If
            If dtpResidentExpiryDate.Checked = True Then
                objEReHire._ResidentExpiry_Date = dtpResidentExpiryDate.Value.Date
            Else
                objEReHire._ResidentExpiry_Date = Nothing
            End If
            objEReHire._ResidentIssue_Place = txtResidentIssuePlace.Text
            objEReHire._Resident_Permit_No = txtResidentPermitNo.Text
            objEReHire._Residentcountryunkid = CInt(cboResidentIssueCountry.SelectedValue)
            '########################## EMPLOYEE RESIDENT PERMIT ########################## -- END
            'S.SANDEEP [04-Jan-2018] -- END
            

            '########################## EMPLOYEE SALARY ########################## -- START
            objEReHire._SGradeGrpId = CInt(cboGradeGroup.SelectedValue)
            objEReHire._SGradelevelunkid = CInt(cboLevel.SelectedValue)
            objEReHire._SGradeunkid = CInt(cboGrade.SelectedValue)
            objEReHire._EmployeeScale = txtScale.Decimal
            If CInt(cboTransactionHead.SelectedValue) > 0 Then
                Dim objTHead As New clsTransactionHead
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTHead._Tranheadunkid = CInt(cboTransactionHead.SelectedValue)
                objTHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTransactionHead.SelectedValue)
                'Sohail (21 Aug 2015) -- End
                If objTHead._Trnheadtype_Id = enTranHeadType.Informational Then
                    objEReHire._InfoSalHeadUnkid = CInt(cboTransactionHead.SelectedValue)
                    objEReHire._IsCopyPrevoiusSLAB = True
                End If
                objTHead = Nothing
            End If
            objEReHire._PeriodUnkid = xPeriodUnkid
            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            objEReHire._AssignDefaultTransactionHeads = chkAssignDefaulTranHeads.Checked
            'Sohail (18 Feb 2019) -- End

            '########################## EMPLOYEE SALARY ########################## -- END

            '########################## EMPLOYEE RECATEGORIZE ########################## -- START
            objEReHire._JobGroupunkid = CInt(cboJobGroup.SelectedValue)
            objEReHire._Jobunkid = CInt(cboJob.SelectedValue)
            Dim objJob As New clsJobs
            objJob._Jobunkid = CInt(cboJob.SelectedValue)
            objEReHire._JGradeunkid = objJob._Jobgradeunkid
            objEReHire._JGradelevelunkid = 0
            objJob = Nothing
            '########################## EMPLOYEE RECATEGORIZE ########################## -- END

            '########################## EMPLOYEE COST-CENTER & TRANSACTION HEADS ########################## -- START
            objEReHire._CostCenterUnkid = CInt(cboCostCenter.SelectedValue)
            objEReHire._TransactionHeadUnkid = CInt(cboTransactionHead.SelectedValue)
            '########################## EMPLOYEE COST-CENTER & TRANSACTION HEADS ########################## -- END
            objEReHire._Isvoid = False
            objEReHire._Statusunkid = 0
            objEReHire._Userunkid = User._Object._Userunkid
            objEReHire._Voiddatetime = Nothing
            objEReHire._Voidreason = ""
            objEReHire._Voiduserunkid = -1
            'Sohail (02 Mar 2020) -- Start
            'Ifakara Issue # : Message "Actual date should not be greater than selected period end date"  on editing first salary change.
            objEReHire._BaseCountryunkid = Company._Object._Localization_Country
            'Sohail (02 Mar 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeRehire_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEReHire = New clsemployee_rehire_tran
        objEmp = New clsEmployee_Master
        objMem_Tran = New clsMembershipTran
        Try
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Set_Logo(Me, gApplicationType)
            Call FillCombo()
            If mAction = enAction.EDIT_ONE Then
                objEReHire._Rehiretranunkid = mintRehireTranunkid
                cboEmployee.SelectedValue = objEReHire._Employeeunkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
                dtpEffectiveDate.Value = objEReHire._Effectivedate
                'S.SANDEEP [01 JUN 2016] -- START
                dtpEffectiveDate.Tag = eZeeDate.convertDate(objEReHire._Effectivedate)
                'S.SANDEEP [01 JUN 2016] -- END
                dtpReinstatementDate.Value = objEReHire._Reinstatment_Date
                'S.SANDEEP [01 JUN 2016] -- START
                dtpReinstatementDate.Tag = eZeeDate.convertDate(objEReHire._Reinstatment_Date)
                'S.SANDEEP [01 JUN 2016] -- END
                cboChangeReason.SelectedValue = objEReHire._Changereasonunkid
                'Sohail (18 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                chkAssignDefaulTranHeads.Checked = False
                chkAssignDefaulTranHeads.Visible = False
            Else
                Dim objHead As New clsTransactionHead
                Dim dsHead As DataSet = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", False, 0, 0, -1, True, True, "typeof_id <> " & enTypeOf.Salary & " AND isdefault = 1 ", False, False, False, 2)
                If dsHead.Tables(0).Rows.Count > 0 Then
                    chkAssignDefaulTranHeads.Checked = True
                    chkAssignDefaulTranHeads.Visible = True
                Else
                    chkAssignDefaulTranHeads.Checked = False
                    chkAssignDefaulTranHeads.Visible = False
                End If
                'Sohail (18 Feb 2019) -- End
            End If
            StrEDMessage = Language.getMessage("frmEmployeeMaster", 93, "This membership is mapped with transaction heads which will be posted to Earning and Deduction for this particular employee. If that head is flat rate then it will be posted with ZERO by default.")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeRehire_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_rehire_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_rehire_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid_Process() = False Then Exit Sub
            Call SetValue()
            Dim dsCurrScale As New DataSet
            Dim objMst As New clsMasterData
            dsCurrScale = objMst.Get_Current_Scale("Salary", CInt(cboEmployee.SelectedValue), DateTime.Today)
            objMst = Nothing

            'S.SANDEEP [02-NOV-2017] -- START
            If objEReHire.IsValidRehire(CInt(cboEmployee.SelectedValue), dtpReinstatementDate.Value.Date, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Nothing) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, you cannot rehire employee on selected rehire date. Reason this employee is already active on this date."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'S.SANDEEP [02-NOV-2017] -- END

            With objEReHire
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With

            If mAction = enAction.EDIT_ONE Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objEReHire.Update(dsCurrScale, mdtMemTran)

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'blnFlag = objEReHire.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, dsCurrScale, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, mdtMemTran, , True, "")
                'Sohail (18 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                'blnFlag = objEReHire.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                '                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                '                                        , ConfigParameter._Object._UserAccessModeSetting, True, True, dsCurrScale, User._Object.Privilege._AllowToApproveEarningDeduction _
                '                                        , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, mdtMemTran, , True, "")
                'Hemant (02 Aug 2019) -- Start
                'ISSUE/ENHANCEMENT#0004017(ZURI) :  basic salary pending for rehired employee (e & d) screen. Once we rehire an employee, their basic salaries show as pending when we try to process payroll.
                'blnFlag = objEReHire.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                '                                       , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                '                                       , ConfigParameter._Object._UserAccessModeSetting, True, True, dsCurrScale, User._Object.Privilege._AllowToApproveEarningDeduction _
                '                                       , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, mdtMemTran, , True, "", dtpReinstatementDate.Value.Date, ConfigParameter._Object._IsArutiDemo.ToString, Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserMustchangePwdOnNextLogon, ConfigParameter._Object._CreateADUserFromEmpMst)
                blnFlag = objEReHire.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                        , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                       , ConfigParameter._Object._UserAccessModeSetting, True, True, dsCurrScale, True _
                                                        , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, mdtMemTran, , True, "", dtpReinstatementDate.Value.Date, ConfigParameter._Object._IsArutiDemo.ToString, Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserMustchangePwdOnNextLogon, ConfigParameter._Object._CreateADUserFromEmpMst)
                'Hemant (02 Aug 2019) -- End
               
                'Sohail (18 Feb 2019) -- End
                'Pinkal (18-Aug-2018) -- End


                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objEReHire.Insert(dsCurrScale, mdtMemTran)

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'blnFlag = objEReHire.Insert(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, dsCurrScale, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, mdtMemTran, , True, "")
                'Sohail (18 Feb 2019) -- Start
                'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
                'blnFlag = objEReHire.Insert(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, dsCurrScale, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, mdtMemTran, , True, "")
                'Hemant (02 Aug 2019) -- Start
                'ISSUE/ENHANCEMENT#0004017(ZURI) :  basic salary pending for rehired employee (e & d) screen. Once we rehire an employee, their basic salaries show as pending when we try to process payroll.
                'blnFlag = objEReHire.Insert(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, dsCurrScale, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, mdtMemTran, , True, "", dtpReinstatementDate.Value.Date, ConfigParameter._Object._IsArutiDemo.ToString, Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserMustchangePwdOnNextLogon)
                blnFlag = objEReHire.Insert(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, dsCurrScale, True, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, mdtMemTran, , True, "", dtpReinstatementDate.Value.Date, ConfigParameter._Object._IsArutiDemo.ToString, Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserMustchangePwdOnNextLogon)
                'Hemant (02 Aug 2019) -- End
                'Sohail (18 Feb 2019) -- End
                'Pinkal (18-Aug-2018) -- End


                'Sohail (21 Aug 2015) -- End
            End If
            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Employee re-hired successfully."), enMsgBoxStyle.Information)
                Me.Close()
            Else
                If objEReHire._Message <> "" Then
                    eZeeMsgBox.Show(objEReHire._Message, enMsgBoxStyle.Information)
                End If
                Exit Try
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnAddReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReason.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.RE_HIRE, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RE_HIRE, True, "List")
                With cboChangeReason
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, _
                                                                                                               objbtnSearchBranch.Click, _
                                                                                                               objbtnSearchDeptGrp.Click, _
                                                                                                               objbtnSearchDepartment.Click, _
                                                                                                               objbtnSearchSecGroup.Click, _
                                                                                                               objbtnSearchSection.Click, _
                                                                                                               objbtnSearchUnitGrp.Click, _
                                                                                                               objbtnSearchGrades.Click, _
                                                                                                               objbtnSearchGradeGrp.Click, _
                                                                                                               objbtnSearchJob.Click, _
                                                                                                               objbtnSearchJobGroup.Click, _
                                                                                                               objbtnSearchTeam.Click, _
                                                                                                               objbtnSearchUnits.Click, _
                                                                                                               objbtnSearchCostCenter.Click, _
                                                                                                               objbtnSearchClass.Click, _
                                                                                                               objbtnSearchClassGrp.Click, _
                                                                                                               objbtnSearchGradeLevel.Click, _
                                                                                                               objSearchReason.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim xCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchEmployee.Name.ToUpper
                    xCbo = cboEmployee
                Case objbtnSearchBranch.Name.ToUpper
                    xCbo = cboStation
                Case objbtnSearchDeptGrp.Name.ToUpper
                    xCbo = cboDepartmentGrp
                Case objbtnSearchDepartment.Name.ToUpper
                    xCbo = cboDepartment
                Case objbtnSearchSecGroup.Name.ToUpper
                    xCbo = cboSectionGroup
                Case objbtnSearchSection.Name.ToUpper
                    xCbo = cboSections
                Case objbtnSearchUnitGrp.Name.ToUpper
                    xCbo = cboUnitGroup
                Case objbtnSearchGrades.Name.ToUpper
                    xCbo = cboGradeGroup
                Case objbtnSearchGradeGrp.Name.ToUpper
                    xCbo = cboGradeGroup
                Case objbtnSearchJob.Name.ToUpper
                    xCbo = cboJob
                Case objbtnSearchJobGroup.Name.ToUpper
                    xCbo = cboJobGroup
                Case objbtnSearchTeam.Name.ToUpper
                    xCbo = cboTeams
                Case objbtnSearchUnits.Name.ToUpper
                    xCbo = cboUnits
                Case objbtnSearchCostCenter.Name.ToUpper
                    xCbo = cboCostCenter
                Case objbtnSearchClass.Name.ToUpper
                    xCbo = cboClass
                Case objbtnSearchClassGrp.Name.ToUpper
                    xCbo = cboClassGroup
                Case objbtnSearchGradeLevel.Name.ToUpper
                    xCbo = cboLevel
                Case objSearchReason.Name.ToUpper
                    xCbo = cboChangeReason
            End Select
            If xCbo.DataSource Is Nothing Then Exit Sub

            With frm
                .ValueMember = xCbo.ValueMember
                .DisplayMember = xCbo.DisplayMember
                If xCbo.Name = cboEmployee.Name Then
                    .CodeMember = "employeecode"
                End If
                .DataSource = CType(xCbo.DataSource, DataTable)
                If .DisplayDialog = True Then
                    xCbo.SelectedValue = .SelectedValue
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddDepartment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddDepartment.Click
        Try
            Dim intRefId As Integer = -1
            Dim frm As New frmDepartment_AddEdit

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > -1 Then
                Dim objDept As New clsDepartment
                Dim dsList As New DataSet
                dsList = objDept.getComboList("Dept", True)
                With cboDepartment
                    .ValueMember = "departmentunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Dept")
                    .SelectedValue = intRefId
                End With
                objDept = Nothing
                dsList = Nothing
            End If
            frm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddDepartment_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddJob.Click
        Try
            Dim intRefId As Integer = -1
            Dim frm As New frmJobs_AddEdit

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > -1 Then
                Dim objJob As New clsJobs
                Dim dsList As New DataSet
                dsList = objJob.getComboList("Job", True)
                With cboJob
                    .ValueMember = "jobunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Job")
                    .SelectedValue = intRefId
                End With
                objJob = Nothing
                dsList = Nothing
            End If
            frm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddJob_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGradeGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGradeGrp.Click
        Try
            Dim intRefId As Integer = -1
            Dim frm As New frmGradeGroup_AddEdit

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > -1 Then
                Dim objGradeGrp As New clsGradeGroup
                Dim dsList As New DataSet
                dsList = objGradeGrp.getComboList("GradeGrp", True)
                With cboGradeGroup
                    .ValueMember = "gradegroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("GradeGrp")
                    .SelectedValue = intRefId
                End With
                objGradeGrp = Nothing
                dsList = Nothing
            End If
            frm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGradeGrp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGrade.Click
        Try
            Dim intRefId As Integer = -1
            Dim frm As New frmGrade_AddEdit

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > -1 Then
                Dim objGrade As New clsGrade
                Dim dsList As New DataSet
                dsList = objGrade.getComboList("Grade", True, CInt(cboGradeGroup.SelectedValue))
                With cboGrade
                    .ValueMember = "gradeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Grade")
                    .SelectedValue = intRefId
                End With
                objGrade = Nothing
                dsList = Nothing
            End If
            frm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGrade_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGradeLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGradeLevel.Click
        Try
            Dim intRefId As Integer = -1
            Dim frm As New frmGradeLevel_AddEdit

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > -1 Then
                Dim objGLevel As New clsGradeLevel
                Dim dsList As New DataSet
                dsList = objGLevel.getComboList("Level", True, CInt(cboGrade.SelectedValue))
                With cboLevel
                    .ValueMember = "gradelevelunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Level")
                    .SelectedValue = 0
                End With
                objGLevel = Nothing
                dsList = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGradeLevel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddCostCenter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCostCenter.Click
        Try
            Dim intRefId As Integer = -1
            Dim frm As New frmCostcenter_AddEdit

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > -1 Then
                Dim objCCenter As New clscostcenter_master
                Dim dsList As New DataSet
                dsList = objCCenter.getComboList("CCenter", True)
                With cboCostCenter
                    .ValueMember = "costcenterunkid"
                    .DisplayMember = "costcentername"
                    .DataSource = dsList.Tables("CCenter")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddCostCenter_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddScale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddScale.Click
        Dim frm As New frmWagetable_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddScale_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddMemCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddMemCategory.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, True, "MEMCAT")
                With cboMemCategory
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("MEMCAT")
                    .SelectedValue = 0
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddMemCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddMembership.Click
        Dim frm As New frmMembership_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objMemShip As New clsmembership_master
                dsList = objMemShip.getListForCombo("MEMSHIP", True, CInt(cboMemCategory.SelectedValue))
                With cboMembershipType
                    .ValueMember = "membershipunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("MEMSHIP")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objMemShip = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddMembership_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Membership Tab "

    Private Sub btnAddMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddMembership.Click
        Try
            If CInt(cboMemCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 45, "Membership Category is compulsory information, it can not be blank. Please select a Membership Category to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CInt(cboMembershipType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 46, "Membership is compulsory information, it can not be blank. Please select a Membership to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtMRow As DataRow() = mdtMemTran.Select("membershipunkid = " & CInt(cboMembershipType.SelectedValue) & " AND AUD <> 'D' ")

            If dtMRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 47, "Selected Membership is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If dtpStartDate.Checked = True And dtpMemIssueDate.Checked = True Then
                If dtpStartDate.Value.Date <= dtpMemIssueDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 43, "Start date cannot be less then or equal to Issue date."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            If dtpEndDate.Checked = True And dtpStartDate.Checked = True Then
                If dtpEndDate.Value.Date <= dtpStartDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 44, "End date cannot be less then or equal to Start date."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            Dim objMembership As New clsmembership_master
            objMembership._Membershipunkid = CInt(cboMembershipType.SelectedValue)
            If objMembership._CTransHead_Id > 0 Then
                dtMRow = mdtMemTran.Select("cotrnheadid = " & CInt(objMembership._CTransHead_Id) & " AND AUD <> 'D' AND isactive = True")
                If dtMRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 94, "Sorry, you cannot assign this membership to this employee.") & vbCrLf & _
                                                       Language.getMessage("frmEmployeeMaster", 95, "Reason : Same Employer Contribution Head is mapped with other membership assigned to this employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            Dim iCurrPeriod As Integer = -1
            Dim objPdata As New clscommom_period_Tran
            Dim objMData As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'iCurrPeriod = objMData.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open, FinancialYear._Object._YearUnkid)
            iCurrPeriod = objMData.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPdata._Periodunkid = iCurrPeriod
            objPdata._Periodunkid(FinancialYear._Object._DatabaseName) = iCurrPeriod
            'Sohail (21 Aug 2015) -- End

            Dim objProcessed As New clsTnALeaveTran
            If objProcessed.IsPayrollProcessDone(iCurrPeriod, CStr(CInt(cboEmployee.SelectedValue)), objPdata._End_Date.Date) = True Then
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 84, "Sorry, You cannot assign this membership. Reason : Payroll is already processed for the current period."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 84, "Sorry, You cannot assign this membership. Reason : Payroll is already processed for the current period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 20, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Exit Sub
            End If
            objProcessed = Nothing

            Dim dtMemRow As DataRow
            dtMemRow = mdtMemTran.NewRow
            dtMemRow.Item("membershiptranunkid") = -1
            dtMemRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
            dtMemRow.Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
            dtMemRow.Item("membershipunkid") = CInt(cboMembershipType.SelectedValue)
            dtMemRow.Item("membershipno") = txtMembershipNo.Text
            If dtpMemIssueDate.Checked = True Then
                dtMemRow.Item("issue_date") = dtpMemIssueDate.Value
            Else
                dtMemRow.Item("issue_date") = DBNull.Value
            End If
            If dtpStartDate.Checked = True Then
                dtMemRow.Item("start_date") = dtpStartDate.Value
            Else
                dtMemRow.Item("start_date") = DBNull.Value
            End If
            If dtpEndDate.Checked = True Then
                dtMemRow.Item("expiry_date") = dtpEndDate.Value
            Else
                dtMemRow.Item("expiry_date") = DBNull.Value
            End If
            dtMemRow.Item("remark") = txtRemark.Text
            dtMemRow.Item("isactive") = True
            dtMemRow.Item("AUD") = "A"
            dtMemRow.Item("GUID") = Guid.NewGuid().ToString
            dtMemRow.Item("ccategory") = Language.getMessage("clsMembershipTran", 1, "Active")
            dtMemRow.Item("ccategoryid") = 1
            dtMemRow.Item("isdeleted") = False
            If objMembership._CTransHead_Id > 0 Then
                If objMem_Tran.Can_Assign_Membership(objMembership._CTransHead_Id, CInt(cboEmployee.SelectedValue)) = True Then
                    eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 96, "Sorry, you cannot assign this membership to this employee.") & vbCrLf & _
                                                        Language.getMessage("frmEmployeeMaster", 97, "Reason : Same Employer Contribution Head is mapped with other membership assigned to this employee."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            If objMembership._ETransHead_Id > 0 Or objMembership._CTransHead_Id > 0 Then
                eZeeMsgBox.Show(StrEDMessage, enMsgBoxStyle.Information)
                Dim frm As New frmMembershipHead
                Dim intEffectivePeriod As Integer = -1
                If frm.displayDialog(intEffectivePeriod, objMembership._ETransHead_Id, objMembership._CTransHead_Id, cboMembershipType.Text, CInt(cboEmployee.SelectedValue)) = True Then
                    dtMemRow.Item("emptrnheadid") = objMembership._ETransHead_Id
                    dtMemRow.Item("cotrnheadid") = objMembership._CTransHead_Id
                    dtMemRow.Item("effetiveperiodid") = intEffectivePeriod
                    dtMemRow.Item("copyedslab") = frm._CopyPreviousED_Slab
                    dtMemRow.Item("overwriteslab") = frm._OverwritePreviousED
                    dtMemRow.Item("overwritehead") = frm._OverwriteHead
                Else
                    Exit Sub
                End If
                objMembership = Nothing
            Else
                dtMemRow.Item("emptrnheadid") = -1
                dtMemRow.Item("cotrnheadid") = -1
                dtMemRow.Item("effetiveperiodid") = -1
            End If
            mdtMemTran.Rows.Add(dtMemRow)

            Call FillMembershipList()
            Call ResetMembership()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddMembership_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEditMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditMembership.Click
        Try
            If lvMembershipInfo.SelectedItems.Count > 0 Then
                If mintMemberTypeId > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvMembershipInfo.Items(mintMemberTypeId).Tag) = -1 Then
                        drTemp = mdtMemTran.Select("GUID = '" & lvMembershipInfo.Items(mintMemberTypeId).SubItems(objcolhMGUID.Index).Text & "'")
                    Else
                        drTemp = mdtMemTran.Select("membershiptranunkid = " & CInt(lvMembershipInfo.Items(mintMemberTypeId).Tag))
                    End If

                    If dtpStartDate.Checked = True And dtpMemIssueDate.Checked = True Then
                        If dtpStartDate.Value.Date <= dtpMemIssueDate.Value.Date Then
                            eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 43, "Start date cannot be less then or equal to Issue date."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If

                    If dtpEndDate.Checked = True And dtpStartDate.Checked = True Then
                        If dtpEndDate.Value.Date <= dtpStartDate.Value.Date Then
                            eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 44, "End date cannot be less then or equal to Start date."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If

                    If drTemp.Length > 0 Then
                        With drTemp(0)
                            .Item("membershiptranunkid") = CInt(lvMembershipInfo.Items(mintMemberTypeId).Tag)
                            .Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                            .Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
                            .Item("membershipunkid") = CInt(cboMembershipType.SelectedValue)
                            .Item("membershipno") = txtMembershipNo.Text
                            If dtpMemIssueDate.Checked = True Then
                                .Item("issue_date") = dtpMemIssueDate.Value
                            Else
                                .Item("issue_date") = DBNull.Value
                            End If

                            If dtpStartDate.Checked = True Then
                                .Item("start_date") = dtpStartDate.Value
                            Else
                                .Item("start_date") = DBNull.Value
                            End If
                            If dtpEndDate.Checked = True Then
                                .Item("expiry_date") = dtpEndDate.Value
                            Else
                                .Item("expiry_date") = DBNull.Value
                            End If
                            .Item("remark") = txtRemark.Text
                            .Item("isactive") = True
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            .Item("ccategory") = .Item("ccategory")
                            .Item("ccategoryid") = .Item("ccategoryid")
                            .Item("isdeleted") = .Item("isdeleted")
                            .AcceptChanges()
                        End With
                        Call FillMembershipList()
                    End If
                End If
                Call ResetMembership()
            End If
            cboMemCategory.Enabled = True
            cboMembershipType.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditMembership_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDeleteMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteMembership.Click
        Try
            If lvMembershipInfo.SelectedItems.Count <= 0 Then Exit Sub

            If mAction = enAction.EDIT_ONE AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
                Dim ObjMemberShip As New clsmembership_master
                Dim StrMsg As String = String.Empty
                ObjMemberShip._Membershipunkid = CInt(lvMembershipInfo.SelectedItems(0).SubItems(objcolMembershipUnkid.Index).Text)
                If ObjMemberShip._ETransHead_Id > 0 Or ObjMemberShip._CTransHead_Id > 0 Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'StrMsg = objMem_Tran.IsHead_Mapped(CInt(cboEmployee.SelectedValue), ObjMemberShip._ETransHead_Id, ObjMemberShip._CTransHead_Id)
                    StrMsg = objMem_Tran.IsHead_Mapped(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboEmployee.SelectedValue), ObjMemberShip._ETransHead_Id, ObjMemberShip._CTransHead_Id, False, "")
                    'Sohail (21 Aug 2015) -- End
                    If StrMsg <> "" Then
                        eZeeMsgBox.Show(StrMsg, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                If mintMemberTypeId > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvMembershipInfo.Items(mintMemberTypeId).Tag) = -1 Then
                        drTemp = mdtMemTran.Select("GUID = '" & lvMembershipInfo.Items(mintMemberTypeId).SubItems(objcolhMGUID.Index).Text & "'")
                    Else
                        If (New clsMembershipTran).isUsed(CInt(lvMembershipInfo.Items(mintMemberTypeId).Tag), CInt(cboEmployee.SelectedValue)) = True Then
                            eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 42, "Sorry, You can not delete this Membership information. Reason : This Membership information in use."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        drTemp = mdtMemTran.Select("membershiptranunkid = '" & CInt(lvMembershipInfo.Items(mintMemberTypeId).Tag) & "'")
                    End If
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        If ObjMemberShip._ETransHead_Id <= 0 AndAlso ObjMemberShip._CTransHead_Id <= 0 Then
                            drTemp(0).Item("isdeleted") = True
                        End If
                        Call FillMembershipList()
                    End If
                End If
            Else
                Dim drTemp As DataRow()
                drTemp = mdtMemTran.Select("GUID = '" & lvMembershipInfo.SelectedItems(0).SubItems(objcolhMGUID.Index).Text & "'")
                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                End If
                Call FillMembershipList()
            End If
            Call ResetMembership()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteMembership_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lvMembershipInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvMembershipInfo.Click
        Try
            If lvMembershipInfo.SelectedItems.Count > 0 Then
                mintMemberTypeId = lvMembershipInfo.SelectedItems(0).Index
                cboMemCategory.SelectedValue = 0
                cboMembershipType.SelectedValue = 0
                cboMemCategory.SelectedValue = CInt(lvMembershipInfo.SelectedItems(0).SubItems(objcolhMemCatUnkid.Index).Text)
                cboMembershipType.SelectedValue = CInt(lvMembershipInfo.SelectedItems(0).SubItems(objcolMembershipUnkid.Index).Text)
                txtMembershipNo.Text = lvMembershipInfo.SelectedItems(0).SubItems(colhMembershipNo.Index).Text
                If lvMembershipInfo.SelectedItems(0).SubItems(colhMemIssueDate.Index).Text = "" Then
                    dtpMemIssueDate.Checked = False
                Else
                    dtpMemIssueDate.Value = CDate(lvMembershipInfo.SelectedItems(0).SubItems(colhMemIssueDate.Index).Text)
                    dtpMemIssueDate.Checked = True
                End If

                If lvMembershipInfo.SelectedItems(0).SubItems(colhStartDate.Index).Text = "" Then
                    dtpStartDate.Checked = False
                Else
                    dtpStartDate.Value = CDate(lvMembershipInfo.SelectedItems(0).SubItems(colhStartDate.Index).Text)
                    dtpStartDate.Checked = True
                End If

                If lvMembershipInfo.SelectedItems(0).SubItems(colhEndDate.Index).Text = "" Then
                    dtpEndDate.Checked = False
                Else
                    dtpEndDate.Value = CDate(lvMembershipInfo.SelectedItems(0).SubItems(colhEndDate.Index).Text)
                    dtpEndDate.Checked = True
                End If
                txtRemark.Text = lvMembershipInfo.SelectedItems(0).SubItems(colhRemark.Index).Text
                cboMemCategory.Enabled = False
                cboMembershipType.Enabled = False
            Else
                cboMemCategory.Enabled = True
                cboMembershipType.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMembershipInfo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lvMembershipInfo_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvMembershipInfo.ItemSelectionChanged
        Try
            If lvMembershipInfo.SelectedItems.Count > 0 Then
                If CInt(lvMembershipInfo.SelectedItems(0).SubItems(objcolhccategoryId.Index).Text) = 2 Then
                    lnkActivateMembership.Visible = True
                    btnAddMembership.Enabled = False : btnEditMembership.Enabled = False : btnDeleteMembership.Enabled = False
                Else
                    lnkActivateMembership.Visible = False
                    btnAddMembership.Enabled = True : btnEditMembership.Enabled = True : btnDeleteMembership.Enabled = True
                End If
            Else
                btnAddMembership.Enabled = True : btnEditMembership.Enabled = True : btnDeleteMembership.Enabled = True
                cboMemCategory.Enabled = True
                cboMembershipType.Enabled = True
                objbtnAddMembership.Enabled = True : objbtnAddMemCategory.Enabled = True
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "lvMembershipInfo_ItemSelectionChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkActivateMembership_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkActivateMembership.Click
        Try
            'S.SANDEEP [22-Jan-2018] -- START
            If lvMembershipInfo.Items.Count <= 0 Then Exit Sub
            'S.SANDEEP [22-Jan-2018] -- END

            If lvMembershipInfo.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvMembershipInfo.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtMemTran.Select("GUID = '" & lvMembershipInfo.SelectedItems(0).SubItems(objcolhMGUID.Index).Text & "'")
                Else
                    drTemp = mdtMemTran.Select("membershiptranunkid = " & CInt(lvMembershipInfo.SelectedItems(0).Tag))
                End If

                If dtpStartDate.Checked = True And dtpMemIssueDate.Checked = True Then
                    If dtpStartDate.Value.Date <= dtpMemIssueDate.Value.Date Then
                        eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 43, "Start date cannot be less then or equal to Issue date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

                If dtpEndDate.Checked = True And dtpStartDate.Checked = True Then
                    If dtpEndDate.Value.Date <= dtpStartDate.Value.Date Then
                        eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 44, "End date cannot be less then or equal to Start date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If

                If drTemp.Length > 0 Then
                    Dim objMembership As New clsmembership_master
                    objMembership._Membershipunkid = CInt(cboMembershipType.SelectedValue)
                    If objMembership._ETransHead_Id > 0 Or objMembership._CTransHead_Id > 0 Then
                        If objMembership._CTransHead_Id > 0 Then
                            Dim dtMRow As DataRow() = mdtMemTran.Select("cotrnheadid = " & CInt(objMembership._CTransHead_Id) & " AND AUD <> 'D' AND isactive = True")
                            If dtMRow.Length > 0 Then
                                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 101, "Sorry, you cannot activate this membership to this employee.") & vbCrLf & _
                                                                  Language.getMessage("frmEmployeeMaster", 102, "Reason : Same Employer Contribution Head is mapped with other membership assigned to this employee."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If

                        If objMembership._CTransHead_Id > 0 Then
                            If objMem_Tran.Can_Assign_Membership(objMembership._CTransHead_Id, CInt(cboEmployee.SelectedValue)) = True Then
                                eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 101, "Sorry, you cannot activate this membership to this employee.") & vbCrLf & _
                                                                    Language.getMessage("frmEmployeeMaster", 102, "Reason : Same Employer Contribution Head is mapped with other membership assigned to this employee."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If

                        Dim frm As New frmMembershipHead
                        Dim intEffectivePeriod As Integer = -1
                        If frm.displayDialog(intEffectivePeriod, objMembership._ETransHead_Id, objMembership._CTransHead_Id, cboMembershipType.Text, CInt(cboEmployee.SelectedValue)) = True Then
                            With drTemp(0)
                                .Item("emptrnheadid") = objMembership._ETransHead_Id
                                .Item("cotrnheadid") = objMembership._CTransHead_Id
                                .Item("effetiveperiodid") = intEffectivePeriod
                                .Item("ccategory") = Language.getMessage("clsMembershipTran", 1, "Active")
                                .Item("ccategoryid") = 1
                                .Item("isdeleted") = False
                                .Item("isactive") = True
                                .Item("copyedslab") = frm._CopyPreviousED_Slab
                                .Item("overwriteslab") = frm._OverwritePreviousED
                                .Item("overwritehead") = frm._OverwriteHead
                                If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                                    .Item("AUD") = "U"
                                End If
                            End With
                        End If
                    End If
                End If
            End If
            mdtMemTran.AcceptChanges()
            FillMembershipList()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "lnkActivateMembership_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Datepicker Event(s) "

    Private Sub dtpStartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpStartDate.ValueChanged
        Try
            dtpEndDate.Checked = dtpStartDate.Checked
            dtpEndDate.Value = dtpStartDate.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpStartDate_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dtpProbationDateFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpProbationDateFrom.ValueChanged
        Try
            dtpProbationDateTo.Checked = dtpProbationDateFrom.Checked
            dtpProbationDateTo.Value = dtpProbationDateFrom.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpProbationDateFrom_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dtpEndEmplDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpEndEmplDate.ValueChanged
        Try
            dtpLeavingDate.Checked = dtpEndEmplDate.Checked
            dtpLeavingDate.Value = dtpEndEmplDate.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpProbationDateFrom_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dtpReinstatementDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpReinstatementDate.ValueChanged
        Try
            Dim dtDate As Date = Nothing
            If ConfigParameter._Object._ConfirmationMonth > 0 Then
                dtDate = dtpReinstatementDate.Value.Date.AddMonths(ConfigParameter._Object._ConfirmationMonth)
                dtpConfirmationDate.Value = dtDate
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpReinstatementDate_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim xtmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                If xtmp.Length > 0 Then
                    mstrEmployeeCode = CStr(xtmp(0).Item("employeecode"))
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END

                'S.SANDEEP [29 APR 2015] -- START
                txtAppDate.Text = ""
                txtBirthDate.Text = ""

                txtAppDate.Text = objEmp._Appointeddate.ToShortDateString
                txtAppDate.Tag = objEmp._Appointeddate
                If objEmp._Birthdate <> Nothing Then
                    txtBirthDate.Text = objEmp._Birthdate.ToShortDateString
                    Dim dtRetireDate As Date = Nothing
                    Select Case objEmp._Gender
                        Case 1  'MALE
                            If ConfigParameter._Object._RetirementBy = 1 Then
                                dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._RetirementValue, objEmp._Birthdate.Date)
                                dtpRetirementDate.Value = dtRetireDate
                            End If
                        Case 2  'FEMALE
                            If ConfigParameter._Object._FRetirementBy = 1 Then
                                dtRetireDate = DateAdd(DateInterval.Year, ConfigParameter._Object._FRetirementValue, objEmp._Birthdate.Date)
                                dtpRetirementDate.Value = dtRetireDate
                            End If
                    End Select
                End If
                'S.SANDEEP [29 APR 2015] -- END
                Call ClearControls()
                Call Set_Current_Transfers()
                Call Set_Current_Recategorization()
                Call Set_Current_WorkPermit()
                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                Call Set_Current_ResidentPermit()
                'S.SANDEEP [04-Jan-2018] -- END
                Call Set_Current_Dates()
                Call Set_Current_Details()
                Call Set_Current_Salary_Grades()
                objMem_Tran._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
                mdtMemTran = objMem_Tran._DataList
                Call FillMembershipList()
            Else
                'dgvHistory.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboGradeGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeGroup.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objGrade As New clsGrade
        Try
            If CInt(cboGradeGroup.SelectedValue) > 0 Then
                dsCombos = objGrade.getComboList("Grade", True, CInt(cboGradeGroup.SelectedValue))
                With cboGrade
                    .ValueMember = "gradeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Grade")
                End With
            Else
                objbtnSearchGrades.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objGradeLevel As New clsGradeLevel
        Try
            If CInt(cboGrade.SelectedValue) > 0 Then
                dsCombos = objGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
                With cboLevel
                    .ValueMember = "gradelevelunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("GradeLevel")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLevel.SelectedIndexChanged
        Dim objWagesTran As New clsWagesTran
        Dim decScale As Decimal = 0
        Dim objMaster As New clsMasterData
        Dim objWages As New clsWagesTran
        Dim dsList As DataSet
        Try
            'Sohail (27 Apr 2016) -- Start
            'Enhancement - 59.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
            '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
            'objWagesTran.GetSalary(CInt(cboGradeGroup.SelectedValue), _
            '                   CInt(cboGrade.SelectedValue), _
            '                   CInt(cboLevel.SelectedValue), _
            '                   decScale)
            objWagesTran.GetSalary(CInt(cboGradeGroup.SelectedValue), _
                               CInt(cboGrade.SelectedValue), _
                               CInt(cboLevel.SelectedValue), _
                               DateTime.Today.Date, _
                               decScale)
            'Sohail (27 Apr 2016) -- End
            dsList = objMaster.Get_Current_Scale("Salary", CInt(cboEmployee.SelectedValue), DateTime.Today)
            If dsList.Tables("Salary").Rows.Count > 0 Then
                decScale = CDec(dsList.Tables("Salary").Rows(0).Item("newscale").ToString)
            End If
            txtScale.Text = CStr(Format(decScale, GUI.fmtCurrency))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboStation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStation.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.DEPARTMENT_GROUP)) = True Then
                Dim objDeptGrp As New clsDepartmentGroup
                Dim dsCombos As New DataSet

                dsCombos = objDeptGrp.getComboList(CInt(cboStation.SelectedValue), "DeptGrp", True)
                With cboDepartmentGrp
                    .ValueMember = "deptgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("DeptGrp")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStation_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboDepartmentGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepartmentGrp.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.DEPARTMENT)) = True Then
                Dim objDepartment As New clsDepartment
                Dim dsCombos As New DataSet

                dsCombos = objDepartment.getComboList(CInt(cboDepartmentGrp.SelectedValue), "Department", True)
                With cboDepartment
                    .ValueMember = "departmentunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Department")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDepartmentGrp_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboDepartment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepartment.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.SECTION_GROUP)) = True Then
                Dim objSectionGrp As New clsSectionGroup
                Dim dsCombos As New DataSet

                dsCombos = objSectionGrp.getComboList(CInt(cboDepartment.SelectedValue), "List", True)
                With cboSectionGroup
                    .ValueMember = "sectiongroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDepartment_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboSectionGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSectionGroup.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.SECTION)) = True Then
                Dim objSection As New clsSections
                Dim dsCombos As New DataSet

                dsCombos = objSection.getComboList(CInt(cboSectionGroup.SelectedValue), "Section", True)
                With cboSections
                    .ValueMember = "sectionunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Section")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSectionGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboSections_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSections.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.UNIT_GROUP)) = True Then
                Dim objUnitGroup As New clsUnitGroup
                Dim dsCombos As New DataSet

                dsCombos = objUnitGroup.getComboList("List", True, CInt(cboSections.SelectedValue))
                With cboUnitGroup
                    .ValueMember = "unitgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSections_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboUnitGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnitGroup.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.UNIT_GROUP)) = True Then
                Dim dsCombos As New DataSet
                Dim objUnit As New clsUnits
                dsCombos = objUnit.getComboList("Unit", True, , CInt(cboUnitGroup.SelectedValue))
                With cboUnits
                    .ValueMember = "unitunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Unit")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUnitGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboUnits_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnits.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.TEAM)) = True Then
                Dim dsCombos As New DataSet
                Dim objTeam As New clsTeams
                dsCombos = objTeam.getComboList("List", True, CInt(cboUnits.SelectedValue))
                With cboTeams
                    .ValueMember = "teamunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUnits_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboClassGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboClassGroup.SelectedIndexChanged
        Try
            Dim dsCombos As New DataSet
            Dim objclass As New clsClass
            dsCombos = objclass.getComboList("Classgrp", True, CInt(cboClassGroup.SelectedValue))
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Classgrp")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboClassGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboMemCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMemCategory.SelectedIndexChanged
        Try
            If CInt(cboMemCategory.SelectedValue) > 0 Then
                Dim dsCombos As New DataSet
                Dim objMembership As New clsmembership_master

                dsCombos = objMembership.getListForCombo("Membership", True, CInt(cboMemCategory.SelectedValue))
                With cboMembershipType
                    .ValueMember = "membershipunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Membership")
                End With
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMemCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboJobGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJobGroup.SelectedIndexChanged
        Try
            If CInt(cboJobGroup.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objJobs As New clsJobs
                dsList = objJobs.getComboList("Jobs", True, _
                                              CInt(cboJobGroup.SelectedValue), _
                                              CInt(cboUnits.SelectedValue), _
                                              CInt(cboSections.SelectedValue), _
                                              CInt(cboGrade.SelectedValue))
                With cboJob
                    .ValueMember = "jobunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Jobs")
                    .SelectedValue = 0
                End With
                dsList.Dispose()
                objJobs = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJobGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbEmployeeAllocation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeAllocation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbWorkPermit.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbWorkPermit.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbDatesDetails.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDatesDetails.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbMembershipInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMembershipInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.objgbDatesInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.objgbDatesInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDeleteMembership.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteMembership.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddMembership.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddMembership.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditMembership.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditMembership.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.tabpMainInfo.Text = Language._Object.getCaption(Me.tabpMainInfo.Name, Me.tabpMainInfo.Text)
            Me.gbEmployeeAllocation.Text = Language._Object.getCaption(Me.gbEmployeeAllocation.Name, Me.gbEmployeeAllocation.Text)
            Me.tabpAdditionalInfo.Text = Language._Object.getCaption(Me.tabpAdditionalInfo.Name, Me.tabpAdditionalInfo.Text)
            Me.gbWorkPermit.Text = Language._Object.getCaption(Me.gbWorkPermit.Name, Me.gbWorkPermit.Text)
            Me.lblPlaceOfIssue.Text = Language._Object.getCaption(Me.lblPlaceOfIssue.Name, Me.lblPlaceOfIssue.Text)
            Me.lblExpiryDate.Text = Language._Object.getCaption(Me.lblExpiryDate.Name, Me.lblExpiryDate.Text)
            Me.lblIssueDate.Text = Language._Object.getCaption(Me.lblIssueDate.Name, Me.lblIssueDate.Text)
            Me.lblIssueCountry.Text = Language._Object.getCaption(Me.lblIssueCountry.Name, Me.lblIssueCountry.Text)
            Me.lblWorkPermitNo.Text = Language._Object.getCaption(Me.lblWorkPermitNo.Name, Me.lblWorkPermitNo.Text)
            Me.gbDatesDetails.Text = Language._Object.getCaption(Me.gbDatesDetails.Name, Me.gbDatesDetails.Text)
            Me.chkExclude.Text = Language._Object.getCaption(Me.chkExclude.Name, Me.chkExclude.Text)
            Me.lblEmplDate.Text = Language._Object.getCaption(Me.lblEmplDate.Name, Me.lblEmplDate.Text)
            Me.lblConfirmationDate.Text = Language._Object.getCaption(Me.lblConfirmationDate.Name, Me.lblConfirmationDate.Text)
            Me.lblRetirementDate.Text = Language._Object.getCaption(Me.lblRetirementDate.Name, Me.lblRetirementDate.Text)
            Me.lblProbationFrom.Text = Language._Object.getCaption(Me.lblProbationFrom.Name, Me.lblProbationFrom.Text)
            Me.lblLeavingDate.Text = Language._Object.getCaption(Me.lblLeavingDate.Name, Me.lblLeavingDate.Text)
            Me.lblProbationTo.Text = Language._Object.getCaption(Me.lblProbationTo.Name, Me.lblProbationTo.Text)
            Me.tabpMembership.Text = Language._Object.getCaption(Me.tabpMembership.Name, Me.tabpMembership.Text)
            Me.gbMembershipInfo.Text = Language._Object.getCaption(Me.gbMembershipInfo.Name, Me.gbMembershipInfo.Text)
            Me.lnkActivateMembership.Text = Language._Object.getCaption(Me.lnkActivateMembership.Name, Me.lnkActivateMembership.Text)
            Me.lblMembershipName.Text = Language._Object.getCaption(Me.lblMembershipName.Name, Me.lblMembershipName.Text)
            Me.lblMembershipType.Text = Language._Object.getCaption(Me.lblMembershipType.Name, Me.lblMembershipType.Text)
            Me.lblMembershipRemark.Text = Language._Object.getCaption(Me.lblMembershipRemark.Name, Me.lblMembershipRemark.Text)
            Me.btnDeleteMembership.Text = Language._Object.getCaption(Me.btnDeleteMembership.Name, Me.btnDeleteMembership.Text)
            Me.lblMembershipNo.Text = Language._Object.getCaption(Me.lblMembershipNo.Name, Me.lblMembershipNo.Text)
            Me.btnAddMembership.Text = Language._Object.getCaption(Me.btnAddMembership.Name, Me.btnAddMembership.Text)
            Me.lblMemIssueDate.Text = Language._Object.getCaption(Me.lblMemIssueDate.Name, Me.lblMemIssueDate.Text)
            Me.lblActivationDate.Text = Language._Object.getCaption(Me.lblActivationDate.Name, Me.lblActivationDate.Text)
            Me.colhMembershipCategory.Text = Language._Object.getCaption(CStr(Me.colhMembershipCategory.Tag), Me.colhMembershipCategory.Text)
            Me.colhMembershipType.Text = Language._Object.getCaption(CStr(Me.colhMembershipType.Tag), Me.colhMembershipType.Text)
            Me.colhMembershipNo.Text = Language._Object.getCaption(CStr(Me.colhMembershipNo.Tag), Me.colhMembershipNo.Text)
            Me.colhMemIssueDate.Text = Language._Object.getCaption(CStr(Me.colhMemIssueDate.Tag), Me.colhMemIssueDate.Text)
            Me.colhStartDate.Text = Language._Object.getCaption(CStr(Me.colhStartDate.Tag), Me.colhStartDate.Text)
            Me.colhEndDate.Text = Language._Object.getCaption(CStr(Me.colhEndDate.Tag), Me.colhEndDate.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
            Me.lblMemExpiryDate.Text = Language._Object.getCaption(Me.lblMemExpiryDate.Name, Me.lblMemExpiryDate.Text)
            Me.btnEditMembership.Text = Language._Object.getCaption(Me.btnEditMembership.Name, Me.btnEditMembership.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblReinstatement.Text = Language._Object.getCaption(Me.lblReinstatement.Name, Me.lblReinstatement.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblScale.Text = Language._Object.getCaption(Me.lblScale.Name, Me.lblScale.Text)
            Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.Name, Me.lblDepartmentGroup.Text)
            Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.Name, Me.lblClassGroup.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblUnits.Text = Language._Object.getCaption(Me.lblUnits.Name, Me.lblUnits.Text)
            Me.lblTeam.Text = Language._Object.getCaption(Me.lblTeam.Name, Me.lblTeam.Text)
            Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
            Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.Name, Me.lblJobGroup.Text)
            Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
            Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.Name, Me.lblSectionGroup.Text)
            Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblTransactionHead.Text = Language._Object.getCaption(Me.lblTransactionHead.Name, Me.lblTransactionHead.Text)
            Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lblAppointmentdate.Text = Language._Object.getCaption(Me.lblAppointmentdate.Name, Me.lblAppointmentdate.Text)
            Me.lblBirthDate.Text = Language._Object.getCaption(Me.lblBirthDate.Name, Me.lblBirthDate.Text)
            Me.elWorkPermit.Text = Language._Object.getCaption(Me.elWorkPermit.Name, Me.elWorkPermit.Text)
            Me.lblResidentIssuePlace.Text = Language._Object.getCaption(Me.lblResidentIssuePlace.Name, Me.lblResidentIssuePlace.Text)
            Me.lblResidentPermitNo.Text = Language._Object.getCaption(Me.lblResidentPermitNo.Name, Me.lblResidentPermitNo.Text)
            Me.lblResidentExpiryDate.Text = Language._Object.getCaption(Me.lblResidentExpiryDate.Name, Me.lblResidentExpiryDate.Text)
            Me.lblResidentIssueCountry.Text = Language._Object.getCaption(Me.lblResidentIssueCountry.Name, Me.lblResidentIssueCountry.Text)
            Me.lblResidentIssueDate.Text = Language._Object.getCaption(Me.lblResidentIssueDate.Name, Me.lblResidentIssueDate.Text)
            Me.elResidentPermit.Text = Language._Object.getCaption(Me.elResidentPermit.Name, Me.elResidentPermit.Text)
            Me.chkAssignDefaulTranHeads.Text = Language._Object.getCaption(Me.chkAssignDefaulTranHeads.Name, Me.chkAssignDefaulTranHeads.Text)
            Me.LblActualDate.Text = Language._Object.getCaption(Me.LblActualDate.Name, Me.LblActualDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Employee is mandatory information. Please select employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Effective date is mandatory information. Please set effective date to continue.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Reinstatement date is mandatory information. Please set reinstatement date to continue.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Reinstatement date cannot be less than effective date. Please set correct reinstatement date to continue.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Probation end date is mandatory information when probation start date is checked.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Probation start date is mandatory information when probation end date is checked")
            Language.setMessage(mstrModuleName, 7, "Sorry, Probation start date cannot be less than the reinstatement date.")
            Language.setMessage("frmEmployeeMaster", 8, "Grade Group is compulsory information. Please select Grade Group.")
            Language.setMessage(mstrModuleName, 9, "Sorry, Probation end date cannot be less then probation start date.")
            Language.setMessage(mstrModuleName, 10, "Sorry, Leaving end date is mandatory information when end of contract date is checked.")
            Language.setMessage(mstrModuleName, 11, "Sorry, End of contract date is mandatory information when leaving date is checked")
            Language.setMessage(mstrModuleName, 12, "Sorry, Leaving date cannot be less than end of contract date.")
            Language.setMessage(mstrModuleName, 13, "Sorry, Retirement date cannot be less than or equal to reinstatement date.")
            Language.setMessage(mstrModuleName, 14, "Sorry, there is no period defined in payroll, for the selected effective date.")
            Language.setMessage(mstrModuleName, 15, "Sorry, Transaction head is mandatory information. Please select Transaction head to continue.")
            Language.setMessage("clsemployee_dates_tran", 16, "Sorry, confirmation information is already present for the selected effective date.")
            Language.setMessage(mstrModuleName, 17, "Employee re-hired successfully.")
            Language.setMessage(mstrModuleName, 18, "Sorry, Reinstatement date cannot be less than appointment date. Please set correct reinstatement date to continue.")
            Language.setMessage(mstrModuleName, 19, "Sorry, End of contract date cannot be less than or equal to Reinstatement date. Please set correct End of contract date to continue.")
            Language.setMessage(mstrModuleName, 20, "Do you want to void Payroll?")
            Language.setMessage(mstrModuleName, 21, "Sorry, Work Permit expiry date must be greater than reinstatement date.")
            Language.setMessage(mstrModuleName, 22, "Sorry, Resident permit expiry date must be greater than reinstatement date.")
            Language.setMessage("frmEmployeeMaster", 24, "Scale cannot be blank or zero. Please set Scale from  Payroll - > Payroll Transaction -> Wages. to continue.")
            Language.setMessage("frmEmployeeMaster", 33, "Cost Center is compulsory information. Please select Cost Center to continue.")
            Language.setMessage("frmEmployeeMaster", 42, "Sorry, You can not delete this Membership information. Reason : This Membership information in use.")
            Language.setMessage("frmEmployeeMaster", 43, "Start date cannot be less then or equal to Issue date.")
            Language.setMessage("frmEmployeeMaster", 44, "End date cannot be less then or equal to Start date.")
            Language.setMessage("frmEmployeeMaster", 45, "Membership Category is compulsory information, it can not be blank. Please select a Membership Category to continue.")
            Language.setMessage("frmEmployeeMaster", 46, "Membership is compulsory information, it can not be blank. Please select a Membership to continue.")
            Language.setMessage("frmEmployeeMaster", 47, "Selected Membership is already added to the list.")
            Language.setMessage(mstrModuleName, 84, "Sorry, You cannot assign this membership. Reason : Payroll is already processed for the current period.")
            Language.setMessage("frmEmployeeMaster", 93, "This membership is mapped with transaction heads which will be posted to Earning and Deduction for this particular employee. If that head is flat rate then it will be posted with ZERO by default.")
            Language.setMessage("frmEmployeeMaster", 94, "Sorry, you cannot assign this membership to this employee.")
            Language.setMessage("frmEmployeeMaster", 95, "Reason : Same Employer Contribution Head is mapped with other membership assigned to this employee.")
            Language.setMessage("frmEmployeeMaster", 96, "Sorry, you cannot assign this membership to this employee.")
            Language.setMessage("frmEmployeeMaster", 97, "Reason : Same Employer Contribution Head is mapped with other membership assigned to this employee.")
            Language.setMessage(mstrModuleName, 100, "Sorry, you cannot exclude selected employee. Reason:Payroll Process already done for the employee for last date of current open period.")
            Language.setMessage("frmEmployeeMaster", 101, "Sorry, you cannot activate this membership to this employee.")
            Language.setMessage("frmEmployeeMaster", 102, "Reason : Same Employer Contribution Head is mapped with other membership assigned to this employee.")
            Language.setMessage("clsemployee_workpermit_tran", 1, "Sorry, work permit information is already present for the selected effective date.")
            Language.setMessage("clsemployee_cctranhead_tran", 2, "Sorry, cost center information is already present for the selected effective date.")
            Language.setMessage("clsemployee_dates_tran", 3, "Sorry, termination information is already present for the selected effective date.")
            Language.setMessage("clsemployee_workpermit_tran", 4, "Sorry, resident permit information is already present for the selected effective date.")
            Language.setMessage("frmWorkPermitTransaction", 5, "Sorry, issue date and expiry date are madatory information.")
            Language.setMessage("frmWorkPermitTransaction", 6, "Sorry, expiry date cannot be less or equal to issue date.")
            Language.setMessage("clsemployee_dates_tran", 7, "Sorry, This termination information is already present.")
            Language.setMessage("frmEmployeeMaster", 9, "Grade is compulsory information. Please select Grade.")
            Language.setMessage("frmEmployeeMaster", 10, "Grade Level is compulsory information. Please select Grade Level.")
            Language.setMessage(mstrModuleName, 16, "Sorry, Change reason is mandatory information. Please select reason to continue.")
            Language.setMessage(mstrModuleName, 20, "Sorry, you cannot rehire employee on selected rehire date. Reason this employee is already active on this date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class