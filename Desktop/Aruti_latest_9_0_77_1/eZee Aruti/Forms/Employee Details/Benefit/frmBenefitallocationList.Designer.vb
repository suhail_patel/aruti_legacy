﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBenefitallocationList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBenefitallocationList))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.cboJobGroup = New System.Windows.Forms.ComboBox
        Me.lblJobGroup = New System.Windows.Forms.Label
        Me.cboCostcenter = New System.Windows.Forms.ComboBox
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.lblGradeGroup = New System.Windows.Forms.Label
        Me.cboGradeGroup = New System.Windows.Forms.ComboBox
        Me.lblTeam = New System.Windows.Forms.Label
        Me.cboTeams = New System.Windows.Forms.ComboBox
        Me.lblUnitGroup = New System.Windows.Forms.Label
        Me.cboUnitGroup = New System.Windows.Forms.ComboBox
        Me.lblSectionGroup = New System.Windows.Forms.Label
        Me.cboSectionGroup = New System.Windows.Forms.ComboBox
        Me.cboDepartmentGrp = New System.Windows.Forms.ComboBox
        Me.lblDepartmentGroup = New System.Windows.Forms.Label
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblGrade = New System.Windows.Forms.Label
        Me.cboUnit = New System.Windows.Forms.ComboBox
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblUnit = New System.Windows.Forms.Label
        Me.lblBranch = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.cboSections = New System.Windows.Forms.ComboBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.lblClass = New System.Windows.Forms.Label
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.lvBenefitAllocation = New eZee.Common.eZeeListView(Me.components)
        Me.colhBenefitgroup = New System.Windows.Forms.ColumnHeader
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhBranch = New System.Windows.Forms.ColumnHeader
        Me.colhDeptGrp = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhSectionGrp = New System.Windows.Forms.ColumnHeader
        Me.colhSection = New System.Windows.Forms.ColumnHeader
        Me.colhUnitGrp = New System.Windows.Forms.ColumnHeader
        Me.colhUnit = New System.Windows.Forms.ColumnHeader
        Me.colhTeam = New System.Windows.Forms.ColumnHeader
        Me.colhGradeGrp = New System.Windows.Forms.ColumnHeader
        Me.colhGrade = New System.Windows.Forms.ColumnHeader
        Me.colhGradeLevel = New System.Windows.Forms.ColumnHeader
        Me.colhJobGrp = New System.Windows.Forms.ColumnHeader
        Me.colhJob = New System.Windows.Forms.ColumnHeader
        Me.colhClassGrp = New System.Windows.Forms.ColumnHeader
        Me.colhClass = New System.Windows.Forms.ColumnHeader
        Me.colhCostCenter = New System.Windows.Forms.ColumnHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblActiveInactive = New System.Windows.Forms.Label
        Me.cboActiveInactive = New System.Windows.Forms.ComboBox
        Me.btnActive = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnInactive = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblActiveInactive)
        Me.gbFilterCriteria.Controls.Add(Me.cboActiveInactive)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboClassGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblClassGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboJobGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblJobGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboCostcenter)
        Me.gbFilterCriteria.Controls.Add(Me.lblCostCenter)
        Me.gbFilterCriteria.Controls.Add(Me.lblGradeLevel)
        Me.gbFilterCriteria.Controls.Add(Me.cboGradeLevel)
        Me.gbFilterCriteria.Controls.Add(Me.lblGradeGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboGradeGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblTeam)
        Me.gbFilterCriteria.Controls.Add(Me.cboTeams)
        Me.gbFilterCriteria.Controls.Add(Me.lblUnitGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboUnitGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblSectionGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboSectionGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartmentGrp)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartmentGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboGrade)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.lblGrade)
        Me.gbFilterCriteria.Controls.Add(Me.cboUnit)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lblUnit)
        Me.gbFilterCriteria.Controls.Add(Me.lblBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.lblJob)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.cboSections)
        Me.gbFilterCriteria.Controls.Add(Me.lblSection)
        Me.gbFilterCriteria.Controls.Add(Me.lblClass)
        Me.gbFilterCriteria.Controls.Add(Me.cboClass)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(8, 4)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 67
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(872, 141)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.BackColor = System.Drawing.Color.Transparent
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(579, 5)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(76, 14)
        Me.lblPeriod.TabIndex = 221
        Me.lblPeriod.Text = "As On Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 215
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(663, 3)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(121, 21)
        Me.cboPeriod.TabIndex = 220
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownWidth = 200
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(731, 59)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(130, 21)
        Me.cboClassGroup.TabIndex = 158
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(650, 61)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(75, 17)
        Me.lblClassGroup.TabIndex = 157
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJobGroup
        '
        Me.cboJobGroup.DropDownWidth = 200
        Me.cboJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobGroup.FormattingEnabled = True
        Me.cboJobGroup.Location = New System.Drawing.Point(508, 113)
        Me.cboJobGroup.Name = "cboJobGroup"
        Me.cboJobGroup.Size = New System.Drawing.Size(130, 21)
        Me.cboJobGroup.TabIndex = 156
        '
        'lblJobGroup
        '
        Me.lblJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobGroup.Location = New System.Drawing.Point(428, 115)
        Me.lblJobGroup.Name = "lblJobGroup"
        Me.lblJobGroup.Size = New System.Drawing.Size(77, 17)
        Me.lblJobGroup.TabIndex = 155
        Me.lblJobGroup.Text = "Job Group"
        Me.lblJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCostcenter
        '
        Me.cboCostcenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostcenter.FormattingEnabled = True
        Me.cboCostcenter.Location = New System.Drawing.Point(731, 113)
        Me.cboCostcenter.Name = "cboCostcenter"
        Me.cboCostcenter.Size = New System.Drawing.Size(130, 21)
        Me.cboCostcenter.TabIndex = 15
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(650, 115)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(75, 17)
        Me.lblCostCenter.TabIndex = 14
        Me.lblCostCenter.Text = "Cost Center"
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(428, 88)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(77, 17)
        Me.lblGradeLevel.TabIndex = 153
        Me.lblGradeLevel.Text = "Grade Level"
        Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownWidth = 200
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(508, 86)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(130, 21)
        Me.cboGradeLevel.TabIndex = 154
        '
        'lblGradeGroup
        '
        Me.lblGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeGroup.Location = New System.Drawing.Point(428, 34)
        Me.lblGradeGroup.Name = "lblGradeGroup"
        Me.lblGradeGroup.Size = New System.Drawing.Size(77, 17)
        Me.lblGradeGroup.TabIndex = 152
        Me.lblGradeGroup.Text = "Grade Group"
        Me.lblGradeGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGradeGroup
        '
        Me.cboGradeGroup.DropDownWidth = 200
        Me.cboGradeGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeGroup.FormattingEnabled = True
        Me.cboGradeGroup.Location = New System.Drawing.Point(508, 32)
        Me.cboGradeGroup.Name = "cboGradeGroup"
        Me.cboGradeGroup.Size = New System.Drawing.Size(130, 21)
        Me.cboGradeGroup.TabIndex = 151
        '
        'lblTeam
        '
        Me.lblTeam.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTeam.Location = New System.Drawing.Point(218, 115)
        Me.lblTeam.Name = "lblTeam"
        Me.lblTeam.Size = New System.Drawing.Size(70, 17)
        Me.lblTeam.TabIndex = 148
        Me.lblTeam.Text = "Team"
        Me.lblTeam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTeams
        '
        Me.cboTeams.DropDownWidth = 200
        Me.cboTeams.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTeams.FormattingEnabled = True
        Me.cboTeams.Location = New System.Drawing.Point(294, 113)
        Me.cboTeams.Name = "cboTeams"
        Me.cboTeams.Size = New System.Drawing.Size(122, 21)
        Me.cboTeams.TabIndex = 149
        '
        'lblUnitGroup
        '
        Me.lblUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnitGroup.Location = New System.Drawing.Point(218, 61)
        Me.lblUnitGroup.Name = "lblUnitGroup"
        Me.lblUnitGroup.Size = New System.Drawing.Size(70, 17)
        Me.lblUnitGroup.TabIndex = 145
        Me.lblUnitGroup.Text = "Unit Group"
        Me.lblUnitGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUnitGroup
        '
        Me.cboUnitGroup.DropDownWidth = 200
        Me.cboUnitGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnitGroup.FormattingEnabled = True
        Me.cboUnitGroup.Location = New System.Drawing.Point(294, 59)
        Me.cboUnitGroup.Name = "cboUnitGroup"
        Me.cboUnitGroup.Size = New System.Drawing.Size(122, 21)
        Me.cboUnitGroup.TabIndex = 146
        '
        'lblSectionGroup
        '
        Me.lblSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSectionGroup.Location = New System.Drawing.Point(8, 115)
        Me.lblSectionGroup.Name = "lblSectionGroup"
        Me.lblSectionGroup.Size = New System.Drawing.Size(75, 16)
        Me.lblSectionGroup.TabIndex = 143
        Me.lblSectionGroup.Text = "Sec. Group"
        Me.lblSectionGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSectionGroup
        '
        Me.cboSectionGroup.DropDownWidth = 200
        Me.cboSectionGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSectionGroup.FormattingEnabled = True
        Me.cboSectionGroup.Location = New System.Drawing.Point(86, 113)
        Me.cboSectionGroup.Name = "cboSectionGroup"
        Me.cboSectionGroup.Size = New System.Drawing.Size(122, 21)
        Me.cboSectionGroup.TabIndex = 144
        '
        'cboDepartmentGrp
        '
        Me.cboDepartmentGrp.DropDownWidth = 200
        Me.cboDepartmentGrp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartmentGrp.FormattingEnabled = True
        Me.cboDepartmentGrp.Location = New System.Drawing.Point(86, 59)
        Me.cboDepartmentGrp.Name = "cboDepartmentGrp"
        Me.cboDepartmentGrp.Size = New System.Drawing.Size(122, 21)
        Me.cboDepartmentGrp.TabIndex = 142
        '
        'lblDepartmentGroup
        '
        Me.lblDepartmentGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartmentGroup.Location = New System.Drawing.Point(8, 61)
        Me.lblDepartmentGroup.Name = "lblDepartmentGroup"
        Me.lblDepartmentGroup.Size = New System.Drawing.Size(75, 16)
        Me.lblDepartmentGroup.TabIndex = 141
        Me.lblDepartmentGroup.Text = "Dept. Group"
        Me.lblDepartmentGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrade
        '
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(508, 59)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(130, 21)
        Me.cboGrade.TabIndex = 13
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(846, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 139
        Me.objbtnReset.TabStop = False
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(428, 61)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(77, 17)
        Me.lblGrade.TabIndex = 12
        Me.lblGrade.Text = "Grade"
        '
        'cboUnit
        '
        Me.cboUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUnit.FormattingEnabled = True
        Me.cboUnit.Location = New System.Drawing.Point(294, 86)
        Me.cboUnit.Name = "cboUnit"
        Me.cboUnit.Size = New System.Drawing.Size(122, 21)
        Me.cboUnit.TabIndex = 9
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(822, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 138
        Me.objbtnSearch.TabStop = False
        '
        'lblUnit
        '
        Me.lblUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnit.Location = New System.Drawing.Point(219, 88)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(70, 17)
        Me.lblUnit.TabIndex = 8
        Me.lblUnit.Text = "Unit"
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 34)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(75, 16)
        Me.lblBranch.TabIndex = 0
        Me.lblBranch.Text = "Branch"
        '
        'cboBranch
        '
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(86, 32)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(122, 21)
        Me.cboBranch.TabIndex = 1
        '
        'cboJob
        '
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(731, 32)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(130, 21)
        Me.cboJob.TabIndex = 11
        '
        'cboDepartment
        '
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(86, 86)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(122, 21)
        Me.cboDepartment.TabIndex = 3
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(650, 34)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(75, 17)
        Me.lblJob.TabIndex = 10
        Me.lblJob.Text = "Job"
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 88)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(75, 16)
        Me.lblDepartment.TabIndex = 2
        Me.lblDepartment.Text = "Department"
        '
        'cboSections
        '
        Me.cboSections.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSections.FormattingEnabled = True
        Me.cboSections.Location = New System.Drawing.Point(294, 32)
        Me.cboSections.Name = "cboSections"
        Me.cboSections.Size = New System.Drawing.Size(122, 21)
        Me.cboSections.TabIndex = 5
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(218, 34)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(70, 17)
        Me.lblSection.TabIndex = 4
        Me.lblSection.Text = "Section"
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(650, 88)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(75, 17)
        Me.lblClass.TabIndex = 6
        Me.lblClass.Text = "Class"
        '
        'cboClass
        '
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(731, 86)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(130, 21)
        Me.cboClass.TabIndex = 7
        '
        'lvBenefitAllocation
        '
        Me.lvBenefitAllocation.BackColorOnChecked = True
        Me.lvBenefitAllocation.ColumnHeaders = Nothing
        Me.lvBenefitAllocation.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhBenefitgroup, Me.colhPeriod, Me.colhBranch, Me.colhDeptGrp, Me.colhDepartment, Me.colhSectionGrp, Me.colhSection, Me.colhUnitGrp, Me.colhUnit, Me.colhTeam, Me.colhGradeGrp, Me.colhGrade, Me.colhGradeLevel, Me.colhJobGrp, Me.colhJob, Me.colhClassGrp, Me.colhClass, Me.colhCostCenter})
        Me.lvBenefitAllocation.CompulsoryColumns = ""
        Me.lvBenefitAllocation.FullRowSelect = True
        Me.lvBenefitAllocation.GridLines = True
        Me.lvBenefitAllocation.GroupingColumn = Nothing
        Me.lvBenefitAllocation.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvBenefitAllocation.HideSelection = False
        Me.lvBenefitAllocation.Location = New System.Drawing.Point(9, 152)
        Me.lvBenefitAllocation.MinColumnWidth = 50
        Me.lvBenefitAllocation.MultiSelect = False
        Me.lvBenefitAllocation.Name = "lvBenefitAllocation"
        Me.lvBenefitAllocation.OptionalColumns = ""
        Me.lvBenefitAllocation.ShowMoreItem = False
        Me.lvBenefitAllocation.ShowSaveItem = False
        Me.lvBenefitAllocation.ShowSelectAll = True
        Me.lvBenefitAllocation.ShowSizeAllColumnsToFit = True
        Me.lvBenefitAllocation.Size = New System.Drawing.Size(869, 247)
        Me.lvBenefitAllocation.Sortable = True
        Me.lvBenefitAllocation.TabIndex = 1
        Me.lvBenefitAllocation.UseCompatibleStateImageBehavior = False
        Me.lvBenefitAllocation.View = System.Windows.Forms.View.Details
        '
        'colhBenefitgroup
        '
        Me.colhBenefitgroup.Tag = "colhBenefitgroup"
        Me.colhBenefitgroup.Text = "Benefit Group"
        Me.colhBenefitgroup.Width = 110
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhPeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 100
        '
        'colhBranch
        '
        Me.colhBranch.Tag = "colhBranch"
        Me.colhBranch.Text = "Branch"
        Me.colhBranch.Width = 100
        '
        'colhDeptGrp
        '
        Me.colhDeptGrp.Tag = "colhDeptGrp"
        Me.colhDeptGrp.Text = "Department Group"
        Me.colhDeptGrp.Width = 110
        '
        'colhDepartment
        '
        Me.colhDepartment.Tag = "colhDepartment"
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 100
        '
        'colhSectionGrp
        '
        Me.colhSectionGrp.Tag = "colhSectionGrp"
        Me.colhSectionGrp.Text = "Section Group"
        Me.colhSectionGrp.Width = 110
        '
        'colhSection
        '
        Me.colhSection.Tag = "colhSection"
        Me.colhSection.Text = "Section"
        Me.colhSection.Width = 100
        '
        'colhUnitGrp
        '
        Me.colhUnitGrp.Tag = "colhUnitGrp"
        Me.colhUnitGrp.Text = "Unit Group"
        Me.colhUnitGrp.Width = 110
        '
        'colhUnit
        '
        Me.colhUnit.Tag = "colhUnit"
        Me.colhUnit.Text = "Unit"
        Me.colhUnit.Width = 100
        '
        'colhTeam
        '
        Me.colhTeam.Tag = "colhTeam"
        Me.colhTeam.Text = "Team"
        Me.colhTeam.Width = 110
        '
        'colhGradeGrp
        '
        Me.colhGradeGrp.Tag = "colhGradeGrp"
        Me.colhGradeGrp.Text = "Grade Group"
        Me.colhGradeGrp.Width = 110
        '
        'colhGrade
        '
        Me.colhGrade.Tag = "colhGrade"
        Me.colhGrade.Text = "Grade"
        Me.colhGrade.Width = 100
        '
        'colhGradeLevel
        '
        Me.colhGradeLevel.Tag = "colhGradeLevel"
        Me.colhGradeLevel.Text = "Grade Level"
        Me.colhGradeLevel.Width = 110
        '
        'colhJobGrp
        '
        Me.colhJobGrp.Tag = "colhJobGrp"
        Me.colhJobGrp.Text = "Job Group"
        Me.colhJobGrp.Width = 110
        '
        'colhJob
        '
        Me.colhJob.Tag = "colhJob"
        Me.colhJob.Text = "Job"
        Me.colhJob.Width = 100
        '
        'colhClassGrp
        '
        Me.colhClassGrp.Tag = "colhClassGrp"
        Me.colhClassGrp.Text = "Class Group"
        Me.colhClassGrp.Width = 110
        '
        'colhClass
        '
        Me.colhClass.Tag = "colhClass"
        Me.colhClass.Text = "Class"
        Me.colhClass.Width = 100
        '
        'colhCostCenter
        '
        Me.colhCostCenter.Tag = "colhCostCenter"
        Me.colhCostCenter.Text = "Cost Center"
        Me.colhCostCenter.Width = 100
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnInactive)
        Me.EZeeFooter1.Controls.Add(Me.btnActive)
        Me.EZeeFooter1.Controls.Add(Me.btnDelete)
        Me.EZeeFooter1.Controls.Add(Me.btnEdit)
        Me.EZeeFooter1.Controls.Add(Me.btnNew)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 407)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(889, 55)
        Me.EZeeFooter1.TabIndex = 157
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(690, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 84
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(490, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 83
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(394, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 82
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(786, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 81
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblActiveInactive
        '
        Me.lblActiveInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblActiveInactive.Location = New System.Drawing.Point(336, 5)
        Me.lblActiveInactive.Name = "lblActiveInactive"
        Me.lblActiveInactive.Size = New System.Drawing.Size(90, 15)
        Me.lblActiveInactive.TabIndex = 304
        Me.lblActiveInactive.Text = "Active/Inactive"
        '
        'cboActiveInactive
        '
        Me.cboActiveInactive.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboActiveInactive.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboActiveInactive.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActiveInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActiveInactive.FormattingEnabled = True
        Me.cboActiveInactive.Location = New System.Drawing.Point(432, 2)
        Me.cboActiveInactive.Name = "cboActiveInactive"
        Me.cboActiveInactive.Size = New System.Drawing.Size(105, 21)
        Me.cboActiveInactive.TabIndex = 303
        '
        'btnActive
        '
        Me.btnActive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnActive.BackColor = System.Drawing.Color.White
        Me.btnActive.BackgroundImage = CType(resources.GetObject("btnActive.BackgroundImage"), System.Drawing.Image)
        Me.btnActive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnActive.BorderColor = System.Drawing.Color.Empty
        Me.btnActive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnActive.FlatAppearance.BorderSize = 0
        Me.btnActive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActive.ForeColor = System.Drawing.Color.Black
        Me.btnActive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnActive.GradientForeColor = System.Drawing.Color.Black
        Me.btnActive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnActive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnActive.Location = New System.Drawing.Point(586, 13)
        Me.btnActive.Name = "btnActive"
        Me.btnActive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnActive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnActive.Size = New System.Drawing.Size(98, 30)
        Me.btnActive.TabIndex = 85
        Me.btnActive.Text = "Set &Active"
        Me.btnActive.UseVisualStyleBackColor = True
        '
        'btnInactive
        '
        Me.btnInactive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnInactive.BackColor = System.Drawing.Color.White
        Me.btnInactive.BackgroundImage = CType(resources.GetObject("btnInactive.BackgroundImage"), System.Drawing.Image)
        Me.btnInactive.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnInactive.BorderColor = System.Drawing.Color.Empty
        Me.btnInactive.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnInactive.FlatAppearance.BorderSize = 0
        Me.btnInactive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnInactive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInactive.ForeColor = System.Drawing.Color.Black
        Me.btnInactive.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnInactive.GradientForeColor = System.Drawing.Color.Black
        Me.btnInactive.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInactive.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnInactive.Location = New System.Drawing.Point(586, 13)
        Me.btnInactive.Name = "btnInactive"
        Me.btnInactive.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnInactive.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnInactive.Size = New System.Drawing.Size(98, 30)
        Me.btnInactive.TabIndex = 86
        Me.btnInactive.Text = "Set &Inactive"
        Me.btnInactive.UseVisualStyleBackColor = True
        '
        'frmBenefitallocationList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(889, 462)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.lvBenefitAllocation)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBenefitallocationList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Benefit Allocation List"
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents cboSections As System.Windows.Forms.ComboBox
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents cboUnit As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnit As System.Windows.Forms.Label
    Friend WithEvents lvBenefitAllocation As eZee.Common.eZeeListView
    Friend WithEvents colhBranch As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSection As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUnit As System.Windows.Forms.ColumnHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents colhJob As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClass As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGrade As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhBenefitgroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents cboCostcenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents colhCostCenter As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboDepartmentGrp As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartmentGroup As System.Windows.Forms.Label
    Friend WithEvents lblSectionGroup As System.Windows.Forms.Label
    Friend WithEvents cboSectionGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblUnitGroup As System.Windows.Forms.Label
    Friend WithEvents cboUnitGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblTeam As System.Windows.Forms.Label
    Friend WithEvents cboTeams As System.Windows.Forms.ComboBox
    Friend WithEvents cboGradeGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblGradeGroup As System.Windows.Forms.Label
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents cboJobGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblJobGroup As System.Windows.Forms.Label
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents colhDeptGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSectionGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhUnitGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTeam As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGradeGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhGradeLevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJobGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhClassGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblActiveInactive As System.Windows.Forms.Label
    Friend WithEvents cboActiveInactive As System.Windows.Forms.ComboBox
    Friend WithEvents btnActive As eZee.Common.eZeeLightButton
    Friend WithEvents btnInactive As eZee.Common.eZeeLightButton
End Class
