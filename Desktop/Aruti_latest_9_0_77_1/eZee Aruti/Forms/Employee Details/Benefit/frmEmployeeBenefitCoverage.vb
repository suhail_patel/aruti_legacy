﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading

Public Class frmEmployeeBenefitCoverage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployeeBenefitCoverage"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objBenefit As clsBenefitCoverage_tran
    Private mintEmpBenefitTranUnkid As Integer = -1
    Private mintSelectedEmployee As Integer = -1
    Private mdtTable As DataTable 'Sohail (23 Jan 2012)
    'S.SANDEEP [ 14 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 14 AUG 2012 ] -- END

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim dicNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 18 SEP 2012 ] -- END

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intEmployeeUnkid As Integer = -1) As Boolean 'S.SANDEEP [ 14 AUG 2012 ] -- START -- END
        'Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintEmpBenefitTranUnkid = intUnkId
            menAction = eAction

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mintEmployeeUnkid = intEmployeeUnkid
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            Me.ShowDialog()

            intUnkId = mintEmpBenefitTranUnkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboBenefitPlan.BackColor = GUI.ColorComp
            cboBenefitGroup.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            cboValueBasis.BackColor = GUI.ColorComp
            txtBenefitAmount.BackColor = GUI.ColorComp
            txtDBBenefitInPercent.BackColor = GUI.ColorComp
            'Sandeep [ 21 Aug 2010 ] -- Start
            cboTransactionHead.BackColor = GUI.ColorComp
            'Sandeep [ 21 Aug 2010 ] -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try


            'Pinkal (21-Dec-2015) -- Start
            'Enhancement - Working on Employee Benefit changes given By Rutta.
            If menAction = enAction.ADD_ONE AndAlso mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
            End If
            'Pinkal (21-Dec-2015) -- End


            cboBenefitGroup.SelectedValue = objBenefit._Benefitgroupunkid
            cboBenefitPlan.SelectedValue = objBenefit._Benefitplanunkid
            cboEmployee.SelectedValue = objBenefit._Employeeunkid

            'Vimal (01 Nov 2010) -- Start 
            'cboValueBasis.SelectedValue = objBenefit._Value_Basis
            'txtBenefitAmount.Text = CStr(objBenefit._Amount)
            'txtDBBenefitInPercent.Text = CStr(objBenefit._Percentage)
            'If objBenefit._Start_Date <> Nothing Then
            '    dtpAssignDate.Value = objBenefit._Start_Date
            'End If
            'If objBenefit._End_Date <> Nothing Then
            '    dtpEndDate.Value = objBenefit._End_Date
            '    dtpEndDate.Checked = True
            'End If
            'Vimal (01 Nov 2010) -- End
            objBenefit._Isvoid = objBenefit._Isvoid
            objBenefit._Userunkid = objBenefit._Userunkid
            objBenefit._Voiddatetime = objBenefit._Voiddatetime
            objBenefit._Voidreason = objBenefit._Voidreason
            objBenefit._Voiduserunkid = objBenefit._Voiduserunkid

            'Sandeep [ 21 Aug 2010 ] -- Start
            cboTransactionHead.SelectedValue = objBenefit._Tranheadunkid
            'Sandeep [ 21 Aug 2010 ] -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objBenefit._Benefitgroupunkid = CInt(cboBenefitGroup.SelectedValue)
            objBenefit._Benefitplanunkid = CInt(cboBenefitPlan.SelectedValue)
            objBenefit._Employeeunkid = CInt(cboEmployee.SelectedValue)

            'Vimal (01 Nov 2010) -- Start 
            'objBenefit._Value_Basis = CInt(cboValueBasis.SelectedValue)
            'objBenefit._Amount = cdec(txtBenefitAmount.Text)
            'objBenefit._Percentage = cdec(IIf(txtDBBenefitInPercent.Text = "", 0, txtDBBenefitInPercent.Text))
            'objBenefit._Start_Date = dtpAssignDate.Value
            'If dtpEndDate.Checked = True Then
            '    objBenefit._End_Date = dtpEndDate.Value
            'Else
            '    objBenefit._End_Date = Nothing
            'End If
            'Vimal (01 Nov 2010) -- End

            'Vimal (01 Nov 2010) -- Start 
            If txtAmount.Enabled = True Then

                'Anjan (11 May 2011)-Start
                'If txtAmount.Text.Trim = "" Then
                '    txtAmount.Text = "0"
                'End If
                objBenefit._Amount = txtAmount.Decimal
                'Anjan (11 May 2011)-End 


            Else
                objBenefit._Amount = 0
            End If
            'Vimal (01 Nov 2010) -- End




            If mintEmpBenefitTranUnkid = -1 Then
                objBenefit._Isvoid = False
                objBenefit._Userunkid = 1
                objBenefit._Voiddatetime = Nothing
                objBenefit._Voidreason = ""
                objBenefit._Voiduserunkid = -1
            Else
                objBenefit._Isvoid = objBenefit._Isvoid
                objBenefit._Userunkid = objBenefit._Userunkid
                objBenefit._Voiddatetime = objBenefit._Voiddatetime
                objBenefit._Voidreason = objBenefit._Voidreason
                objBenefit._Voiduserunkid = objBenefit._Voiduserunkid
            End If
            'Sandeep [ 21 Aug 2010 ] -- Start
            objBenefit._Tranheadunkid = CInt(cboTransactionHead.SelectedValue)
            'Sandeep [ 21 Aug 2010 ] -- End 

            'Sohail (23 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            For Each dtRow As DataRow In mdtTable.Select("IsChecked = 1 ")
                dtRow.Item("edunkid") = -1
                dtRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dtRow.Item("tranheadunkid") = CInt(cboTransactionHead.SelectedValue)
                dtRow.Item("trnheadname") = cboTransactionHead.Text
                dtRow.Item("batchtransactionunkid") = -1

                dtRow.Item("currencyid") = 0
                dtRow.Item("vendorid") = -1
                dtRow.Item("costcenterunkid") = 0

                dtRow.Item("userunkid") = User._Object._Userunkid
                dtRow.Item("isvoid") = False
                dtRow.Item("voiduserunkid") = -1
                dtRow.Item("voiddatetime") = DBNull.Value
                dtRow.Item("voidreason") = ""
                
                If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                    dtRow.Item("isapproved") = True
                    dtRow.Item("approveruserunkid") = User._Object._Userunkid
                Else
                    dtRow.Item("isapproved") = False
                    dtRow.Item("approveruserunkid") = -1
                End If
                dtRow.Item("AUD") = "A"

                mdtTable.AcceptChanges()
            Next
            'Sohail (23 Jan 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master
        Dim objMaster As New clsMasterData
        'Sandeep [ 21 Aug 2010 ] -- Start
        Dim objTranHead As New clsTransactionHead
        'Sandeep [ 21 Aug 2010 ] -- End 

        Try

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsCombos = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If menAction = enAction.EDIT_ONE Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , , , , False)
            'End If


            'Sandeep (14-Sep-2018) -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)
            'Sandeep (14-Sep-2018) -- Start

            'S.SANDEEP [04 JUN 2015] -- END

            'If menAction = enAction.EDIT_ONE Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If
            'S.SANDEEP [ 08 OCT 2012 ] -- END

            
            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.BENEFIT_GROUP, True, "BG")
            With cboBenefitGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("BG")
                .SelectedValue = 0
            End With


            'Vimal (01 Nov 2010) -- Start 
            'dsCombos = objMaster.GetPaymentBy("ValueBasis")
            'With cboValueBasis
            '    .ValueMember = "Id"
            '    .DisplayMember = "NAME"
            '    .DataSource = dsCombos.Tables("ValueBasis")
            '    .SelectedValue = 0
            'End With
            'Vimal (01 Nov 2010) -- End

            'Sandeep [ 21 Aug 2010 ] -- Start

            'Vimal (01 Nov 2010) -- Start 
            'dsCombos = objTranHead.getComboList("TranHead", True)
            'With cboTransactionHead
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("TranHead")
            '    .SelectedValue = 0
            'End With





            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsCombos = objTranHead.getComboList("TranHead", True, , , enTypeOf.BENEFIT)
            Dim dTable As New DataTable
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("TranHead", True, , , enTypeOf.BENEFIT)
            'dTable = objTranHead.getComboList("TranHead", , , , enTypeOf.Allowance).Tables(0)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , enTypeOf.BENEFIT)
            dTable = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", , , , enTypeOf.Allowance).Tables(0)
            'Sohail (21 Aug 2015) -- End
            dsCombos.Tables(0).Merge(dTable)
            'S.SANDEEP [ 20 MARCH 2012 ] -- END
            With cboTransactionHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("TranHead")
                .SelectedValue = 0
            End With

            'Vimal (01 Nov 2010) -- End



            'Sandeep [ 21 Aug 2010 ] -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Dim objTnA As New clsTnALeaveTran 'Sohail (16 Oct 2012)
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboBenefitGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Benefit Group is compulsory information. Please select Benefit Group to continue."), enMsgBoxStyle.Information)
                cboBenefitGroup.Focus()
                Return False
            End If

            If CInt(cboBenefitPlan.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Benefit Plan is compulsory information. Please select Benefit Plan to continue."), enMsgBoxStyle.Information)
                cboBenefitPlan.Focus()
                Return False
            End If

            'Vimal (01 Nov 2010) -- Start 
            'If txtAmount.Enabled = True AndAlso txtAmount.Decimal <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Enter Benefit Amount."), enMsgBoxStyle.Information)
            '    txtAmount.Focus()
            '    Return False
            'End If
            'Vimal (01 Nov 2010) -- End

            'Vimal (01 Nov 2010) -- Start 
            'If CInt(cboValueBasis.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Value Basis is compulsory information. Please select Value Basis to continue."), enMsgBoxStyle.Information)
            '    cboValueBasis.Focus()
            '    Return False
            'End If

            'Select Case CInt(cboValueBasis.SelectedValue)
            '    Case 1  'VALUE
            '        'Sandeep [ 14 Aug 2010 ] -- Start
            '        'If cdec(txtBenefitAmount.Text.Trim) <= 0 Then
            '        If cdec(txtBenefitAmount.Decimal) <= 0 Then
            '            'Sandeep [ 14 Aug 2010 ] -- End 
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Amount cannot be blank. Amount is compulsory information."), enMsgBoxStyle.Information)
            '            txtBenefitAmount.Focus()
            '            Return False
            '        End If
            '    Case 2  'PERCENT
            '        'Sandeep [ 14 Aug 2010 ] -- Start
            '        'If cdec(txtDBBenefitInPercent.Text.Trim) <= 0 Then
            '        If cdec(txtDBBenefitInPercent.Decimal) <= 0 Then
            '            'Sandeep [ 14 Aug 2010 ] -- End 
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Percent cannot be blank. Percent is compulsory information."), enMsgBoxStyle.Information)
            '            txtDBBenefitInPercent.Focus()
            '            Return False
            '        End If
            'End Select

            'Sandeep [ 09 Oct 2010 ] -- Start
            'Issues Reported by Vimal
            'If dtpEndDate.Checked = True Then
            'If dtpEndDate.Value.Date <= dtpAssignDate.Value.Date Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "End Date cannot be less then or equal to assigned date."), enMsgBoxStyle.Information)
            '    Return False
            'End If
            'End If
            'Sandeep [ 09 Oct 2010 ] -- End 

            'Vimal (01 Nov 2010) -- End

            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES
            ''Sandeep [ 01 FEB 2011 ] -- START
            'If CInt(cboTransactionHead.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Transaction Head is compulsory information. Please select Transaction Head to continue."), enMsgBoxStyle.Information)
            '    cboTransactionHead.Focus()
            '    Return False
            'End If
            ''Sandeep [ 01 FEB 2011 ] -- END 

            ''Sohail (23 Jan 2012) -- Start
            ''TRA - ENHANCEMENT
            'If mdtTable.Select("IsChecked = 1 ").Length <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select atleat one Period to assign this Benefit on ED."), enMsgBoxStyle.Information)
            '    dgvAssignED.Focus()
            '    Return False
            'End If
            ''Sohail (23 Jan 2012) -- End

            ''Sohail (16 Oct 2012) -- Start
            ''TRA - ENHANCEMENT
            'For Each dtRow As DataRow In mdtTable.Select("IsChecked = 1 ")
            '    If objTnA.IsPayrollProcessDone(CInt(dtRow.Item("periodunkid")), cboEmployee.SelectedValue.ToString, eZeeDate.convertDate(dtRow.Item("end_date").ToString)) = True Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Process Payroll is already done for last date of one of the selected Period. Please Void Process Payroll first for selected employee to assign Benefit."), enMsgBoxStyle.Information)
            '        Exit Try
            '    End If
            'Next
            ''Sohail (16 Oct 2012) -- End
            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = True Or ConfigParameter._Object._IsArutiDemo = True Then
            If CInt(cboTransactionHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Transaction Head is compulsory information. Please select Transaction Head to continue."), enMsgBoxStyle.Information)
                cboTransactionHead.Focus()
                Return False
            End If

            If mdtTable.Select("IsChecked = 1 ").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select atleat one Period to assign this Benefit on ED."), enMsgBoxStyle.Information)
                dgvAssignED.Focus()
                Return False
            End If

            For Each dtRow As DataRow In mdtTable.Select("IsChecked = 1 ")
                If objTnA.IsPayrollProcessDone(CInt(dtRow.Item("periodunkid")), cboEmployee.SelectedValue.ToString, eZeeDate.convertDate(dtRow.Item("end_date").ToString)) = True Then
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Process Payroll is already done for last date of one of the selected Period. Please Void Process Payroll first for selected employee to assign Benefit."), enMsgBoxStyle.Information)
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Process Payroll is already done for last date of one of the selected Period. Please Void Process Payroll first for selected employee to assign Benefit.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 16, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                    Exit Try
                End If
            Next
            End If
            'S.SANDEEP [ 15 April 2013 ] -- END

            

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
            'Sohail (16 Oct 2012) -- Start
            'TRA - ENHANCEMENT
        Finally

            'Sohail (16 Oct 2012) -- End
        End Try
    End Function


    Private Sub SetVisibility()

        Try
            objbtnAddBenefitPlan.Enabled = User._Object.Privilege._AddBenefitPlan
            objbtnAddGroup.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddTranHead.Enabled = User._Object.Privilege._AddTransactionHead


            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES
            If ConfigParameter._Object._IsArutiDemo = False Then
                cboTransactionHead.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                objbtnAddTranHead.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                chkCopyPreviousEDSlab.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                dgvAssignED.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
                objchkSelectAll.Enabled = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management)
            End If
            'S.SANDEEP [ 15 April 2013 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


    'Sohail (23 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillGridview()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet
        Dim drED As DataRow
        Try
            mdtTable.Rows.Clear()
            If mdtTable.Columns.Contains("IsChecked") = False Then
                Dim dCol As DataColumn = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dCol.DefaultValue = False
                mdtTable.Columns.Add(dCol)
            End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End

            For Each dsRow As DataRow In dsList.Tables("Period").Rows

                drED = mdtTable.NewRow
                drED.Item("edunkid") = -1
                'drED.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                'drED.Item("tranheadunkid") = CInt(cboTransactionHead.SelectedValue)
                'drED.Item("trnheadname") = cboTransactionHead.Text
                'drED.Item("batchtransactionunkid") = -1

                'If txtAmount.Text.Length > 0 Then
                '    drED.Item("amount") = txtAmount.Decimal
                'Else
                '    drED.Item("amount") = 0
                'End If
                'drED.Item("currencyid") = 0
                'drED.Item("vendorid") = CInt(cboMembershipType.SelectedValue)
                'drED.Item("userunkid") = User._Object._Userunkid
                'drED.Item("isvoid") = False
                'drED.Item("voiduserunkid") = -1
                'drED.Item("voiddatetime") = DBNull.Value
                'drED.Item("voidreason") = ""
                'If CInt(cboMembershipType.SelectedValue) > 0 Then
                '    drED.Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
                'Else
                '    drED.Item("membership_categoryunkid") = 0
                'End If
                'If User._Object.Privilege._AllowToApproveEarningDeduction = True Then
                '    drED.Item("isapproved") = True
                '    drED.Item("approveruserunkid") = User._Object._Userunkid
                'Else
                '    drED.Item("isapproved") = False
                '    drED.Item("approveruserunkid") = -1
                'End If
                drED.Item("periodunkid") = CInt(dsRow.Item("periodunkid"))
                drED.Item("period_name") = dsRow.Item("name").ToString
                drED.Item("start_date") = dsRow.Item("start_date").ToString
                drED.Item("end_date") = dsRow.Item("end_date").ToString
                'drED.Item("costcenterunkid") = CInt(cboCostCenter.SelectedValue)
                'drED.Item("medicalrefno") = txtMedicalRefNo.Text.Trim() 

                'drED.Item("AUD") = "A"

                mdtTable.Rows.Add(drED)

            Next

            dgvAssignED.AutoGenerateColumns = False
            colhCheck.DataPropertyName = "IsChecked"
            colhEDUnkId.DataPropertyName = "edunkid"
            colhPeriodUnkID.DataPropertyName = "periodunkid"
            colhPeriodName.DataPropertyName = "period_name"
            colhAmount.DataPropertyName = "amount"


            'Pinkal (13-Mar-2018) -- Start
            'Bug - RefID 29 - After Assign Benefit To Employee The Amount Display In ED But Not Display In Employee Benefit.
            colhAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            'Pinkal (13-Mar-2018) -- End

            colhMedicalRefNo.DataPropertyName = "medicalrefno"

            dgvAssignED.DataSource = mdtTable
            dgvAssignED.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGridview", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub CheckAllPeriod(ByVal blnCheckAll As Boolean)
        Try
            For Each dRow As DataRow In mdtTable.Rows
                dRow.Item("IsChecked") = objchkSelectAll.Checked
                dRow.AcceptChanges()
            Next
            dgvAssignED.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub
    'Sohail (23 Jan 2012) -- End

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    Private Function Set_Notification(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Dim objEmployee As New clsEmployee_Master
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            'S.SANDEEP [04 JUN 2015] -- END

            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If ConfigParameter._Object._Notify_EmplData.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & "<b>" & getTitleCase(StrUserName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee : <b>" & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 142, "This is to inform you that changes have been made for employee") & " " & "<b>" & " " & getTitleCase(objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmployee._Employeecode & "</b>. Following information has been changed by user : <b>" & User._Object._Firstname & " " & User._Object._Lastname & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 143, "with employeecode") & " " & "<b>" & " " & objEmployee._Employeecode & "</b>." & " " & Language.getMessage("frmEmployeeMaster", 144, "Following information has been changed by user") & " " & "<b>" & " " & getTitleCase(User._Object._Firstname & " " & User._Object._Lastname) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 145, "from Machine") & " " & "<b>" & getHostName.ToString & "</b>" & " " & Language.getMessage("frmEmployeeMaster", 146, "and IPAddress") & " " & "<b>" & getIP.ToString & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append(vbCrLf)

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'Gajanan (21 Nov 2018) -- Start
                'StrMessage.Append("<TABLE border = '1' WIDTH = '50%' style='margin-left: 25px'>")
                StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")

                'Gajanan (21 Nov 2018) -- End
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:15%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 12, "Field") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 13, "Old Value") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 14, "New Value") & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")
                For Each sId As String In ConfigParameter._Object._Notify_EmplData.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enEmployeeData.BENEFITS

                            If objBenefit._Benefitgroupunkid <> CInt(cboBenefitGroup.SelectedValue) Then
                                Dim objCommon As New clsCommon_Master
                                objCommon._Masterunkid = objBenefit._Benefitgroupunkid
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 9, "Benefit Group") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objCommon._Name.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboBenefitGroup.SelectedValue) > 0, cboBenefitGroup.Text.Trim, "")) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objBenefit._Benefitplanunkid <> CInt(cboBenefitPlan.SelectedValue) Then
                                Dim objplan As New clsbenefitplan_master
                                objplan._Benefitplanunkid = objBenefit._Benefitplanunkid
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 8, "Benefit Plan") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objplan._Benefitplanname.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboBenefitPlan.SelectedValue) > 0, cboBenefitPlan.Text.Trim, "")) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objBenefit._Tranheadunkid <> CInt(cboTransactionHead.SelectedValue) Then
                                Dim objHead As New clsTransactionHead
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'objHead._Tranheadunkid = objBenefit._Tranheadunkid
                                objHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = objBenefit._Tranheadunkid
                                'Sohail (21 Aug 2015) -- End
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & Language.getMessage(mstrModuleName, 7, "Transaction Head") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objHead._Trnheadname.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboTransactionHead.SelectedValue) > 0, cboTransactionHead.Text.Trim, "")) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                    End Select
                Next
            End If
            StrMessage.Append("</TABLE>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language

            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")

            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Notification", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If menAction = enAction.EDIT_ONE Then
                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 10, "Notification of Changes in Employee Benefit(s).")
                            objSendMail._Message = dicNotification(sKey)
                            'S.SANDEEP [ 28 JAN 2014 ] -- START
                            'Sohail (17 Dec 2014) -- Start
                            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                            'objSendMail._Form_Name = mstrModuleName
                            objSendMail._Form_Name = "" 'Please pass Form Name for WEB
                            'Sohail (17 Dec 2014) -- End
                            objSendMail._LogEmployeeUnkid = -1
                            objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                            objSendMail._UserUnkid = User._Object._Userunkid
                            objSendMail._SenderAddress = User._Object._Email
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                            With objSendMail
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            'S.SANDEEP [ 28 JAN 2014 ] -- END
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(Company._Object._Companyunkid)
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If

            ElseIf menAction <> enAction.EDIT_ONE Then
                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 11, "Notifications to newly added Employee Benefit(s).")
                            Dim sMsg As String = dicNotification(sKey)
                            'Set_Notification(User._Object._Firstname.Trim & " " & User._Object._Lastname.Trim)
                            objSendMail._Message = sMsg
                            With objSendMail
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(Company._Object._Companyunkid)
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 18 SEP 2012 ] -- END

#End Region

#Region " Form's Events "
    Private Sub frmEmployeeBenefitCoverage_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objBenefit = Nothing
    End Sub

    Private Sub frmEmployeeBenefitCoverage_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmEmployeeBenefitCoverage_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployeeBenefitCoverage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBenefit = New clsBenefitCoverage_tran
        Dim objED As New clsEarningDeduction
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetVisibility()

            Call SetColor()
            Call FillCombo()

            'Sohail (23 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            '*** Get Structure of Earning Deduction
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objED._Employeeunkid = 0
            objED._Employeeunkid(FinancialYear._Object._DatabaseName) = 0
            'Sohail (21 Aug 2015) -- End
            mdtTable = objED._DataSource
            Call FillGridview()
            'Sohail (23 Jan 2012) -- End

            If menAction = enAction.EDIT_ONE Then
                objBenefit._Benefitcoverageunkid = mintEmpBenefitTranUnkid
                'Sandeep [ 17 Aug 2010 ] -- Start
                cboEmployee.Enabled = False
                'Sandeep [ 09 Oct 2010 ] -- Start
                'Issues Reported by Vimal
                objbtnSearchEmployee.Enabled = False
                'Sandeep [ 09 Oct 2010 ] -- End 
                'Sandeep [ 17 Aug 2010 ] -- End 

            End If

            Call GetValue()

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            cboEmployee.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeBenefitCoverage_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBenefitCoverage_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsBenefitCoverage_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Events "

    Private Sub btnSaveInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub


            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            dicNotification = New Dictionary(Of String, String)

            If menAction = enAction.EDIT_ONE Then
                If ConfigParameter._Object._Notify_EmplData.Trim.Length > 0 Then
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    For Each sId As String In ConfigParameter._Object._Notify_EmplData.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        dicNotification.Add(objUsr._Email, StrMessage)
                    Next
                    objUsr = Nothing
                End If

            ElseIf menAction <> enAction.EDIT_ONE Then
                If ConfigParameter._Object._Notify_EmplData.Trim.Length > 0 Then
                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                For Each sId As String In ConfigParameter._Object._Notify_EmplData.Split(CChar("||"))(2).Split(CChar(","))
                    objUsr._Userunkid = CInt(sId)
                    StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        If dicNotification.ContainsKey(objUsr._Email) = False Then
                            dicNotification.Add(objUsr._Email, StrMessage)
                        End If

                Next
                objUsr = Nothing
            End If
            End If

            'S.SANDEEP [ 18 SEP 2012 ] -- END

            Call SetValue()

            'Sohail (23 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Dim dtTable As DataTable = New DataView(mdtTable, "IsChecked = 1 ", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (23 Jan 2012) -- End

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objBenefit._FormName = mstrModuleName
            objBenefit._LoginEmployeeunkid = 0
            objBenefit._ClientIP = getIP()
            objBenefit._HostName = getHostName()
            objBenefit._FromWeb = False
            objBenefit._AuditUserId = User._Object._Userunkid
objBenefit._CompanyUnkid = Company._Object._Companyunkid
            objBenefit._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                'Sohail (23 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'blnFlag = objBenefit.Update
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objBenefit.Update(dtTable)
                blnFlag = objBenefit.Update(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, dtTable, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, "")
                'Sohail (21 Aug 2015) -- End
                'Sohail (23 Jan 2012) -- End
            Else
                'Sohail (23 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'blnFlag = objBenefit.Insert
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objBenefit.Insert(dtTable)
                blnFlag = objBenefit.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, dtTable, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, True, "")
                'Sohail (21 Aug 2015) -- End
                'Sohail (23 Jan 2012) -- End
            End If

            If blnFlag = False And objBenefit._Message <> "" Then
                eZeeMsgBox.Show(objBenefit._Message, enMsgBoxStyle.Information)
            End If


            If blnFlag Then
                mblnCancel = False
                'Vimal (01 Nov 2010) -- Start 
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Do you want to exempt this Transaction Head for some pay periods?"), CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim frm As New frmEmployee_Exemption_AddEdit
                    frm.displayDialog(-1, enAction.ADD_ONE, , , CInt(cboEmployee.SelectedValue), CInt(cboTransactionHead.SelectedValue))
                End If
                'Vimal (01 Nov 2010) -- End
                If menAction = enAction.ADD_CONTINUE Then
                    objBenefit = Nothing
                    objBenefit = New clsBenefitCoverage_tran
                    Call GetValue()
                    cboEmployee.Focus()
                    Call FillGridview() 'Sohail (23 Jan 2012)
                Else
                    mintEmpBenefitTranUnkid = objBenefit._Benefitcoverageunkid
                    Me.Close()
                End If
            End If

            If mintSelectedEmployee <> -1 Then
                cboEmployee.SelectedValue = mintSelectedEmployee
            End If

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If blnFlag = True Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If
            'S.SANDEEP [ 18 SEP 2012 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveInfo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "
    Private Sub cboBenefitType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBenefitGroup.SelectedIndexChanged
        Try
            If CInt(cboBenefitGroup.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objBenefit As New clsbenefitplan_master
                dsList = objBenefit.getComboList("Benefit", True, CInt(cboBenefitGroup.SelectedValue))
                With cboBenefitPlan
                    .ValueMember = "benefitplanunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Benefit")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBenefitType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Vimal (01 Nov 2010) -- Start 
    'Private Sub cboValueBasis_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboValueBasis.SelectedIndexChanged
    '    Try
    '        Select Case CInt(cboValueBasis.SelectedValue)
    '            Case 1  'VALUE
    '                txtDBBenefitInPercent.Enabled = False
    '                txtDBBenefitInPercent.Text = ""
    '                txtBenefitAmount.Enabled = True
    '            Case 2  'PERCENT
    '                txtBenefitAmount.Enabled = False
    '                txtDBBenefitInPercent.Enabled = True
    '                txtBenefitAmount.Text = CStr(0)
    '        End Select
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboValueBasis_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Vimal (01 Nov 2010) -- End

    Private Sub objbtnAddBenefitPlan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddBenefitPlan.Click
        Dim frm As New frmBenefitplan_AddEdit
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 


            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                Dim dsList As New DataSet
                Dim objBenefit As New clsbenefitplan_master
                dsList = objBenefit.getComboList("Benefit", True, CInt(cboBenefitGroup.SelectedValue))
                With cboBenefitPlan
                    .ValueMember = "benefitplanunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Benefit")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddBenefitPlan_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1

        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.BENEFIT_GROUP, enAction.ADD_ONE)
            Dim dsList As New DataSet
            Dim objBGroup As New clsCommon_Master
            dsList = objBGroup.getComboList(clsCommon_Master.enCommonMaster.BENEFIT_GROUP, True, "BGroup")
            With cboBenefitGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("BGroup")
                .SelectedValue = intRefId
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'Vimal (01 Nov 2010) -- Start 
    'Private Sub txtDBBenefitInPercent_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If txtDBBenefitInPercent.Text.Trim = "" Or txtDBBenefitInPercent.Text.Trim = "." Then Exit Sub
    '        If cdec(txtDBBenefitInPercent.Text.Trim) > 100 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please enter proper benefit plan percentage."), enMsgBoxStyle.Information)
    '            txtDBBenefitInPercent.Text = CStr(0)
    '            txtDBBenefitInPercent.Focus()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try
    'End Sub
    'Vimal (01 Nov 2010) -- End

    Private Sub cboEmployee_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectionChangeCommitted
        Try
            mintSelectedEmployee = CInt(cboEmployee.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 21 Aug 2010 ] -- Start
    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Try
            Dim objFrm As New frmCommonSearch
            Try
                With objFrm
                    .ValueMember = cboTransactionHead.ValueMember
                    .DisplayMember = cboTransactionHead.DisplayMember
                    .DataSource = CType(cboTransactionHead.DataSource, DataTable)
                    .CodeMember = "code"
                End With
                If objFrm.DisplayDialog Then
                    cboTransactionHead.SelectedValue = objFrm.SelectedValue
                End If
                cboTransactionHead.Focus()
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 21 Aug 2010 ] -- End 


    'Vimal (01 Nov 2010) -- Start 
    'Private Sub cboTransactionHead_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTransactionHead.SelectedValueChanged
    '    Try
    '        Dim objTranHead As New clsTransactionHead
    '        objTranHead._Tranheadunkid = CInt(cboTransactionHead.SelectedValue)
    '        lblTranHeadDesc.Text = objTranHead._Formula
    '        objTranHead = Nothing
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboTransactionHead_SelectedValueChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Vimal (01 Nov 2010) -- End
    'Anjan (03 Nov 2010)-Start
    Private Sub objbtnAddTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddTranHead.Click
        Dim frm As New frmTransactionHead_AddEdit
        Dim intRefId As Integer = -1
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            If frm.displayDialog(intRefId, enAction.ADD_ONE, False, True) Then

                Dim dsList As New DataSet
                Dim objTransaction As New clsTransactionHead
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objTransaction.getComboList("TranHead", True, enTranHeadType.EarningForEmployees, , enTypeOf.BENEFIT)
                dsList = objTransaction.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, enTranHeadType.EarningForEmployees, , enTypeOf.BENEFIT)
                'Sohail (21 Aug 2015) -- End
                With cboTransactionHead
                    .ValueMember = "tranheadunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("TranHead")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddTranHead_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan (03 Nov 2010)-End
    'Vimal (01 Nov 2010) -- Start 
    Private Sub cboTransactionHead_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTransactionHead.SelectedValueChanged
        Try
            If CInt(cboTransactionHead.SelectedValue) <= 0 Then Exit Sub 'Sohail (23 Jan 2012)

            Dim objTranHead As New clsTransactionHead
            Dim objED As New clsEarningDeduction
            Dim intEdUnkId As Integer 'Sohail (23 Jan 2012)

            'Sohail (23 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            For i As Integer = 0 To dgvAssignED.Rows.Count - 1
                With dgvAssignED.Rows(i)
                    'Sohail (23 Jan 2012) -- End

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objTranHead._Tranheadunkid = CInt(cboTransactionHead.SelectedValue)
                    objTranHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboTransactionHead.SelectedValue)
                    'Sohail (21 Aug 2015) -- End
                    txtTranHeadDesc.Text = objTranHead._Formula

                    'Sohail (23 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                    '    txtAmount.Enabled = True
                    '    txtAmount.BackColor = GUI.ColorComp
                    'Else
                    '    txtAmount.Enabled = False
                    '    txtAmount.BackColor = GUI.ColorOptional
                    '    txtAmount.Text = "0"
                    'End If
                    If objTranHead._Calctype_Id = enCalcType.AsComputedValue OrElse _
                                   objTranHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab OrElse _
                                   objTranHead._Calctype_Id = enCalcType.AsUserDefinedValue OrElse _
                                   objTranHead._Calctype_Id = enCalcType.DEFINED_SALARY OrElse _
                                   objTranHead._Calctype_Id = enCalcType.OnAttendance OrElse _
                                   objTranHead._Calctype_Id = enCalcType.OnHourWorked OrElse _
                                   objTranHead._Calctype_Id = enCalcType.OverTimeHours OrElse _
                                   objTranHead._Calctype_Id = enCalcType.NET_PAY OrElse _
                                   objTranHead._Calctype_Id = enCalcType.TOTAL_DEDUCTION OrElse _
                                   objTranHead._Calctype_Id = enCalcType.TOTAL_EARNING OrElse _
                                   objTranHead._Calctype_Id = enCalcType.ShortHours OrElse _
                                   objTranHead._Calctype_Id = enCalcType.TAXABLE_EARNING_TOTAL OrElse _
                                   objTranHead._Calctype_Id = enCalcType.NON_TAXABLE_EARNING_TOTAL OrElse _
                                   objTranHead._Calctype_Id = enCalcType.NON_CASH_BENEFIT_TOTAL _
                                   Then

                        .Cells(colhAmount.Index).ReadOnly = True
                        .Cells(colhAmount.Index).Style.BackColor = GUI.ColorOptional
            Else
                        .Cells(colhAmount.Index).ReadOnly = False
                        .Cells(colhAmount.Index).Style.BackColor = GUI.ColorComp
            End If
                    'Sohail (23 Jan 2012) -- End

                    'Sohail (23 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Dim mintEmpId As Integer = objED.GetEDUnkID(CInt(cboEmployee.SelectedValue), CInt(cboTransactionHead.SelectedValue))
                    'objED._Edunkid = mintEmpId
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'intEdUnkId = objED.GetEDUnkID(CInt(cboEmployee.SelectedValue), CInt(cboTransactionHead.SelectedValue), CInt(.Cells(colhPeriodUnkID.Index).Value))
                    'objED._Edunkid = intEdUnkId
                    intEdUnkId = objED.GetEDUnkID(FinancialYear._Object._DatabaseName, CInt(cboEmployee.SelectedValue), CInt(cboTransactionHead.SelectedValue), CInt(.Cells(colhPeriodUnkID.Index).Value))
                    objED._Edunkid(Nothing, FinancialYear._Object._DatabaseName) = intEdUnkId
                    'Sohail (21 Aug 2015) -- End
                    If intEdUnkId > 0 Then
                        .Cells(colhAmount.Index).Value = objED._Amount
                        .Cells(colhMedicalRefNo.Index).Value = objED._MedicalRefNo
                    Else
                        .Cells(colhAmount.Index).Value = 0
                        .Cells(colhMedicalRefNo.Index).Value = ""
                    End If
            'Anjan (11 May 2011)-Start
                    'txtAmount.Text = Format(objED._Amount, GUI.fmtCurrency)
            'Anjan (11 May 2011)-End 
                    'Sohail (23 Jan 2012) -- End


                    'Sohail (23 Jan 2012) -- Start
                    'TRA - ENHANCEMENT
                End With
            Next
            objED = Nothing
            'Sohail (23 Jan 2012) -- End
            objTranHead = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTransactionHead_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub
    'Vimal (01 Nov 2010) -- End
#End Region

    'Sohail (23 Jan 2012) -- Start
    'TRA - ENHANCEMENT
#Region " Datagridview Events "
    Private Sub dgvAssignED_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvAssignED.DataError
        Try
            'If e.ColumnIndex = dgvAssignED.Columns(colhAmount.Index).Index Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please Enter Proper Amount."), enMsgBoxStyle.Information)
            '    e.Cancel = True
            '    Exit Sub
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAssignED_DataError", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvAssignED_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvAssignED.EditingControlShowing
        Dim tb As TextBox
        Try
            If dgvAssignED.CurrentCell.ColumnIndex = colhAmount.Index AndAlso TypeOf e.Control Is TextBox Then
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                AddHandler tb.KeyPress, AddressOf tb_keypress
            Else
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAssignED_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Listview's Events "
    'Private Sub lvPeriodList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs)

    '    Try
    '        If lvPeriodList.CheckedItems.Count <= 0 Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Unchecked
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvPeriodList.CheckedItems.Count < lvPeriodList.Items.Count Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Indeterminate
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvPeriodList.CheckedItems.Count = lvPeriodList.Items.Count Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Checked
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvPeriodList_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAllPeriod(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (23 Jan 2012) -- End
    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbBenefitPlan.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBenefitPlan.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbBenefitPlan.Text = Language._Object.getCaption(Me.gbBenefitPlan.Name, Me.gbBenefitPlan.Text)
			Me.lblBenefitType.Text = Language._Object.getCaption(Me.lblBenefitType.Name, Me.lblBenefitType.Text)
			Me.lblBenefitsInPercent.Text = Language._Object.getCaption(Me.lblBenefitsInPercent.Name, Me.lblBenefitsInPercent.Text)
			Me.lblBenefitInAmount.Text = Language._Object.getCaption(Me.lblBenefitInAmount.Name, Me.lblBenefitInAmount.Text)
			Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
			Me.lnBenefitInfo.Text = Language._Object.getCaption(Me.lnBenefitInfo.Name, Me.lnBenefitInfo.Text)
			Me.lnEmployeeName.Text = Language._Object.getCaption(Me.lnEmployeeName.Name, Me.lnEmployeeName.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblBenefitPlan.Text = Language._Object.getCaption(Me.lblBenefitPlan.Name, Me.lblBenefitPlan.Text)
			Me.lblValueBasis.Text = Language._Object.getCaption(Me.lblValueBasis.Name, Me.lblValueBasis.Text)
			Me.lblTransactionHead.Text = Language._Object.getCaption(Me.lblTransactionHead.Name, Me.lblTransactionHead.Text)
			Me.lblFormula.Text = Language._Object.getCaption(Me.lblFormula.Name, Me.lblFormula.Text)
			Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
			Me.lnAssignedED.Text = Language._Object.getCaption(Me.lnAssignedED.Name, Me.lnAssignedED.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.Name, Me.chkCopyPreviousEDSlab.Text)
			Me.colhCheck.HeaderText = Language._Object.getCaption(Me.colhCheck.Name, Me.colhCheck.HeaderText)
			Me.colhEDUnkId.HeaderText = Language._Object.getCaption(Me.colhEDUnkId.Name, Me.colhEDUnkId.HeaderText)
			Me.colhPeriodUnkID.HeaderText = Language._Object.getCaption(Me.colhPeriodUnkID.Name, Me.colhPeriodUnkID.HeaderText)
			Me.colhPeriodName.HeaderText = Language._Object.getCaption(Me.colhPeriodName.Name, Me.colhPeriodName.HeaderText)
			Me.colhAmount.HeaderText = Language._Object.getCaption(Me.colhAmount.Name, Me.colhAmount.HeaderText)
			Me.colhMedicalRefNo.HeaderText = Language._Object.getCaption(Me.colhMedicalRefNo.Name, Me.colhMedicalRefNo.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 2, "Benefit Group is compulsory information. Please select Benefit Group to continue.")
			Language.setMessage(mstrModuleName, 3, "Benefit Plan is compulsory information. Please select Benefit Plan to continue.")
			Language.setMessage(mstrModuleName, 4, "Transaction Head is compulsory information. Please select Transaction Head to continue.")
			Language.setMessage(mstrModuleName, 5, "Do you want to exempt this Transaction Head for some pay periods?")
			Language.setMessage(mstrModuleName, 6, "Please select atleat one Period to assign this Benefit on ED.")
			Language.setMessage(mstrModuleName, 7, "Transaction Head")
			Language.setMessage(mstrModuleName, 8, "Benefit Plan")
			Language.setMessage(mstrModuleName, 9, "Benefit Group")
			Language.setMessage(mstrModuleName, 10, "Notification of Changes in Employee Benefit(s).")
			Language.setMessage(mstrModuleName, 11, "Notifications to newly added Employee Benefit(s).")
			Language.setMessage(mstrModuleName, 12, "Field")
			Language.setMessage(mstrModuleName, 13, "Old Value")
			Language.setMessage(mstrModuleName, 14, "New Value")
			Language.setMessage(mstrModuleName, 15, "Sorry, Process Payroll is already done for last date of one of the selected Period. Please Void Process Payroll first for selected employee to assign Benefit.")
			Language.setMessage(mstrModuleName, 16, "Do you want to void Payroll?")
			Language.setMessage("frmEmployeeMaster", 142, "This is to inform you that changes have been made for employee")
			Language.setMessage("frmEmployeeMaster", 143, "with employeecode")
			Language.setMessage("frmEmployeeMaster", 144, "Following information has been changed by user")
			Language.setMessage("frmEmployeeMaster", 145, "from Machine")
			Language.setMessage("frmEmployeeMaster", 146, "and IPAddress")
			Language.setMessage("frmEmployeeMaster", 147, "Dear")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class