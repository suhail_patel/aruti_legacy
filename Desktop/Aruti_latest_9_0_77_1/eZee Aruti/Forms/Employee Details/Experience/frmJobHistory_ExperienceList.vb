﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmJobHistory_ExperienceList

#Region " Private Varaibles "
    Private objJobExperience As clsJobExperience_tran
    Private ReadOnly mstrModuleName As String = "frmJobHistory_ExperienceList"
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private mstrAdvanceFilter As String = ""
    'Anjan (21 Nov 2011)-End 

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone
    Private mintTransactionId As Integer = 0
    Private objAExperienceTran As clsJobExperience_approval_tran

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim ExperienceApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmJobHistory_ExperienceList)))
    Private intParentRowIndex As Integer = -1
    'Gajanan [17-DEC-2018] -- End

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim isEmployeeApprove As Boolean = False
    'Gajanan [22-Feb-2019] -- End


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private objApprovalData As clsEmployeeDataApproval
    'Gajanan [17-April-2019] -- End

#End Region

    'S.SANDEEP [ 14 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Property "
    Dim mintEmployeeUnkid As Integer = -1

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

#End Region
    'S.SANDEEP [ 14 AUG 2012 ] -- END

#Region " Private Functions "
    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objJobs As New clsJobs
        Try

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'dsList = objEmp.GetEmployeeList("Emp", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [ 08 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, )
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , , , , False)
            'End If


            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmJobHistory_ExperienceList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If


            'dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                User._Object._Userunkid, _
            '                                FinancialYear._Object._YearUnkid, _
            '                                Company._Object._Companyunkid, _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                ConfigParameter._Object._UserAccessModeSetting, _
            '                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                       mblnOnlyApproved, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True, _
                                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


            'S.SANDEEP [20-JUN-2018] -- End



            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 08 OCT 2012 ] -- END


            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            If User._Object.Privilege._AllowToViewEmpExperienceList = True Then                'Pinkal (02-Jul-2012) -- Start

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'dsList = objJobExperience.GetList("JobHistory")

                ''Anjan (02 Mar 2012)-Start
                ''ENHANCEMENT : TRA COMMENTS on Andrew sir's Request

                ''If CInt(cboJobs.SelectedValue) > 0 Then
                ''    StrSearching &= "AND JobId = " & CInt(cboJobs.SelectedValue) & " "
                ''End If
                'If txtJob.Text.Trim <> "" Then
                '    StrSearching &= "AND Jobs LIKE '%" & txtJob.Text & "%'" & " "
                'End If


                ''Anjan (02 Mar 2012)-End


                'If CInt(cboEmployee.SelectedValue) > 0 Then
                '    StrSearching &= "AND EmpId = " & CInt(cboEmployee.SelectedValue) & " "
                'End If

                'If txtCompany.Text.Trim <> "" Then
                '    StrSearching &= "AND Company LIKE '%" & txtCompany.Text & "%'" & " "
                'End If

                'If txtSupervisor.Text.Trim <> "" Then
                '    StrSearching &= "AND Supervisor LIKE '%" & txtSupervisor.Text & "%'" & " "
                'End If

                'If dtpStartDate.Checked = True And dtpEndDate.Checked = True Then
                '    StrSearching &= "AND StartDate >= '" & eZeeDate.convertDate(dtpStartDate.Value) & "' AND EndDate <= '" & eZeeDate.convertDate(dtpEndDate.Value) & "'" & " "
                'End If

                ''Anjan (21 Nov 2011)-Start
                ''ENHANCEMENT : TRA COMMENTS
                'If mstrAdvanceFilter.Length > 0 Then
                '    StrSearching &= "AND " & mstrAdvanceFilter
                'End If
                ''Anjan (21 Nov 2011)-End 

                'If StrSearching.Length > 0 Then
                '    StrSearching = StrSearching.Substring(3)
                '    dtTable = New DataView(dsList.Tables("JobHistory"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = New DataView(dsList.Tables("JobHistory"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                'End If

                If txtJob.Text.Trim <> "" Then
                    StrSearching &= "AND old_job LIKE '%" & txtJob.Text & "%'" & " "
                End If

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If txtCompany.Text.Trim <> "" Then
                    StrSearching &= "AND ISNULL(hremp_experience_tran.company,'') LIKE '%" & txtCompany.Text & "%'" & " "
                End If

                If txtSupervisor.Text.Trim <> "" Then
                    StrSearching &= "AND ISNULL(hremp_experience_tran.supervisor,'') LIKE '%" & txtSupervisor.Text & "%'" & " "
                End If

                If dtpStartDate.Checked = True And dtpEndDate.Checked = True Then
                    StrSearching &= "AND CONVERT(CHAR(8),hremp_experience_tran.start_date,112) >= '" & eZeeDate.convertDate(dtpStartDate.Value) & "' AND CONVERT(CHAR(8),hremp_experience_tran.end_date,112) <= '" & eZeeDate.convertDate(dtpEndDate.Value) & "'" & " "
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    StrSearching &= "AND " & mstrAdvanceFilter
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If


                'Pinkal (28-Dec-2015) -- Start
                'Enhancement - Working on Changes in SS for Employee Master.

                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .
                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmJobHistory_ExperienceList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If

                'dsList = objJobExperience.GetList(FinancialYear._Object._DatabaseName, _
                '                                  User._Object._Userunkid, _
                '                                  FinancialYear._Object._YearUnkid, _
                '                                  Company._Object._Companyunkid, _
                '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                  ConfigParameter._Object._UserAccessModeSetting, True, _
                '                                  ConfigParameter._Object._IsIncludeInactiveEmp, "JobHistory", , StrSearching).


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .

                'dsList = objJobExperience.GetList(FinancialYear._Object._DatabaseName, _
                '                                   User._Object._Userunkid, _
                '                                   FinancialYear._Object._YearUnkid, _
                '                                   Company._Object._Companyunkid, _
                '                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                   ConfigParameter._Object._UserAccessModeSetting, True, _
                '                                ConfigParameter._Object._IsIncludeInactiveEmp, "JobHistory", , StrSearching, True)

                dsList = objJobExperience.GetList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                                   ConfigParameter._Object._IsIncludeInactiveEmp, "JobHistory", , StrSearching, True, mblnAddApprovalCondition)

                'S.SANDEEP [20-JUN-2018] -- End


                'Pinkal (28-Dec-2015) -- End

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim dcol As New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "tranguid"
                    .DefaultValue = ""
                End With
                dsList.Tables(0).Columns.Add(dcol)


                If ExperienceApprovalFlowVal Is Nothing Then
                    Dim dsPending As New DataSet
                    dsPending = objAExperienceTran.GetList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, mblnOnlyApproved, _
                                              ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", , StrSearching, True, mblnAddApprovalCondition)

                    If dsPending.Tables(0).Rows.Count > 0 Then
                        For Each row As DataRow In dsPending.Tables(0).Rows
                            dsList.Tables(0).ImportRow(row)
                        Next
                    End If
                End If
                'Gajanan [17-DEC-2018] -- End

                dtTable = New DataView(dsList.Tables("JobHistory"), "", "EmpName", DataViewRowState.CurrentRows).ToTable

                lvJobHistory.BeginUpdate()
                'S.SANDEEP [04 JUN 2015] -- END

                lvJobHistory.Items.Clear()

                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("EmpName").ToString
                    lvItem.SubItems.Add(dtRow.Item("Company").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Jobs").ToString)
                    'S.SANDEEP [ 07 NOV 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("StartDate").ToString).ToShortDateString)
                    'lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("EndDate").ToString).ToShortDateString)
                    If dtRow.Item("StartDate").ToString.Trim.Length > 0 Then
                        lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("StartDate").ToString).ToShortDateString)
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    If dtRow.Item("EndDate").ToString.Trim.Length > 0 Then
                        lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("EndDate").ToString).ToShortDateString)
                    Else
                        lvItem.SubItems.Add("")
                    End If
                    'S.SANDEEP [ 07 NOV 2011 ] -- END
                    lvItem.SubItems.Add(dtRow.Item("Supervisor").ToString)
                    lvItem.SubItems.Add(dtRow.Item("Remark").ToString)
                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    lvItem.SubItems.Add(dtRow.Item("tranguid").ToString)
                    lvItem.SubItems(objdgcolhtranguid.Index).Tag = dtRow.Item("operationtypeid").ToString()
                    lvItem.SubItems.Add(dtRow.Item("EmpId").ToString)

                    If dtRow.Item("tranguid").ToString.Length > 0 Then
                        lvItem.BackColor = Color.PowderBlue
                        lvItem.ForeColor = Color.Black
                        lvItem.Font = New Font(Me.Font, FontStyle.Bold)
                        objtblPanel.Visible = True
                        btnApprovalinfo.Visible = False
                    End If
                    lvItem.SubItems.Add(dtRow("OperationType").ToString())
                    'Gajanan [17-DEC-2018] -- End

                    lvItem.Tag = dtRow.Item("ExpId")

                    lvJobHistory.Items.Add(lvItem)

                    lvItem = Nothing
                Next

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                colhOperationType.DisplayIndex = 0
                'Gajanan [17-DEC-2018] -- End

                If lvJobHistory.Items.Count > 5 Then
                    colhRemark.Width = 150 - 20
                Else
                    colhRemark.Width = 150
                End If

                lvJobHistory.GroupingColumn = colhEmployee
                lvJobHistory.DisplayGroups(True)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                lvJobHistory.EndUpdate()
                'S.SANDEEP [04 JUN 2015] -- END

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddEmployeeExperience
            btnEdit.Enabled = User._Object.Privilege._EditEmployeeExperience
            btnDelete.Enabled = User._Object.Privilege._DeleteEmployeeExperience

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            mnuExportExperience.Enabled = User._Object.Privilege._AllowtoExportEmpExperience
            mnuImportExperience.Enabled = User._Object.Privilege._AllowtoImportEmpExperience
            mnuScanAttachDocuments.Enabled = User._Object.Privilege._AllowToScanAttachDocument
            'Anjan (25 Oct 2012)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmJobHistory_ExperienceList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objJobExperience = New clsJobExperience_tran
        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        objAExperienceTran = New clsJobExperience_approval_tran
        'Gajanan [17-DEC-2018] -- End

        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()

            Call FillCombo()

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            objtblPanel.Visible = False
            btnApprovalinfo.Visible = False
            'Gajanan [17-DEC-2018] -- End

            'Call FillList()
            Call SetVisibility()

            If lvJobHistory.Items.Count > 0 Then lvJobHistory.Items(0).Selected = True
            lvJobHistory.Select()

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
                cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False
                Call FillList()
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobHistory_ExperienceList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobHistory_ExperienceList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvJobHistory.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmJobHistory_ExperienceList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmJobHistory_ExperienceList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objJobExperience = Nothing
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsJobExperience_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsJobExperience_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Buton's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        'Gajanan [9-April-2019] -- Start
        objApprovalData = New clsEmployeeDataApproval
        'Gajanan [9-April-2019] -- End


        If lvJobHistory.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvJobHistory.Select()
            Exit Sub
        End If

        Try

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim itmp() As DataRow = CType(cboEmployee.DataSource, DataTable).Select("employeeunkid= '" & CInt(lvJobHistory.SelectedItems(0).Tag) & "'")
            If itmp.Length > 0 Then
                isEmployeeApprove = CBool(itmp(0).Item("isapproved").ToString())
            Else
                isEmployeeApprove = True
            End If
            'Gajanan [22-Feb-2019] -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'If ExperienceApprovalFlowVal Is Nothing Then
            If ExperienceApprovalFlowVal Is Nothing Then


                
                If objApprovalData.IsApproverPresent(enScreenName.frmJobHistory_ExperienceList, FinancialYear._Object._DatabaseName, _
                                                      ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, _
                                                      FinancialYear._Object._YearUnkid, CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences), _
                                                      User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, CInt(lvJobHistory.SelectedItems(0).SubItems(objdgcolhempid.Index).Text), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    eZeeMsgBox.Show(objApprovalData._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'Gajanan [17-April-2019] -- End

                Dim item As ListViewItem = Nothing
                item = lvJobHistory.Items.Cast(Of ListViewItem).Where(Function(x) CInt(x.Tag) = CInt(lvJobHistory.SelectedItems(0).Tag) And x.SubItems(objdgcolhtranguid.Index).Text.Trim.Length > 0).FirstOrDefault()
                If item IsNot Nothing Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process."), enMsgBoxStyle.Information)
                    lvJobHistory.SelectedItems(0).Selected = False
                    Exit Sub
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvJobHistory.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objJobExperience._Isvoid = True

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objJobExperience._Voiduserunkid = 1
                'objJobExperience._Voidreason = "Testing"
                'objJobExperience._Voidatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))

                objJobExperience._Voiduserunkid = User._Object._Userunkid
                Dim frm As New frmReasonSelection
                'Anjan (02 Sep 2011)-Start
                'Issue : Including Language Settings.
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                'Anjan (02 Sep 2011)-End 

                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.EMPLOYEE, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objJobExperience._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                'objJobExperience._Voidatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                objJobExperience._Voidatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objJobExperience._FormName = mstrModuleName
                objJobExperience._LoginEmployeeunkid = 0
                objJobExperience._ClientIP = getIP()
                objJobExperience._HostName = getHostName()
                objJobExperience._FromWeb = False
                objJobExperience._AuditUserId = User._Object._Userunkid
objJobExperience._CompanyUnkid = Company._Object._Companyunkid
                objJobExperience._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If ExperienceApprovalFlowVal Is Nothing Then
                If ExperienceApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                    'Gajanan [22-Feb-2019] -- End
                    objAExperienceTran._Isvoid = True
                    objAExperienceTran._Audituserunkid = User._Object._Userunkid
                    objAExperienceTran._Isweb = False
                    objAExperienceTran._Ip = getIP()
                    objAExperienceTran._Host = getHostName()
                    objAExperienceTran._Form_Name = mstrModuleName
                    If objAExperienceTran.Delete(CInt(lvJobHistory.SelectedItems(0).Tag), mstrVoidReason, Company._Object._Companyunkid, Nothing) = False Then
                        If objAExperienceTran._Message <> "" Then
                            eZeeMsgBox.Show(objAExperienceTran._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub

                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.


                    Else
                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                         CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences), _
                                         enScreenName.frmJobHistory_ExperienceList, ConfigParameter._Object._EmployeeAsOnDate, _
                                         User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, _
                                         User._Object._Username, clsEmployeeDataApproval.enOperationType.DELETED, , lvJobHistory.SelectedItems(0).SubItems(objdgcolhempid.Index).Text, , , _
                                         " experiencetranunkid = " & CInt(lvJobHistory.SelectedItems(0).Tag), Nothing, , , _
                                         " experiencetranunkid = " & CInt(lvJobHistory.SelectedItems(0).Tag), Nothing)

                        'Gajanan [17-April-2019] -- End

                    End If
                Else
                    If objJobExperience.Delete(CInt(lvJobHistory.SelectedItems(0).Tag)) = False Then
                        If objJobExperience._Message <> "" Then
                            eZeeMsgBox.Show(objJobExperience._Message, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If
                End If


                lvJobHistory.SelectedItems(0).Remove()

                If lvJobHistory.Items.Count <= 0 Then
                    Exit Try
                End If
            End If
            lvJobHistory.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvJobHistory.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvJobHistory.Select()
            Exit Sub
        End If
        Dim frm As New frmEmployeeExperience
        Try
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If ExperienceApprovalFlowVal Is Nothing Then
                Dim item As ListViewItem = Nothing
                item = lvJobHistory.Items.Cast(Of ListViewItem).Where(Function(x) CInt(x.Tag) = CInt(lvJobHistory.SelectedItems(0).Tag) And x.SubItems(objdgcolhtranguid.Index).Text.Trim.Length > 0).FirstOrDefault()
                If item IsNot Nothing Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process."), enMsgBoxStyle.Information)
                    lvJobHistory.SelectedItems(0).Selected = False
                    Exit Sub
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvJobHistory.SelectedItems(0).Index

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If frm.displayDialog(CInt(lvJobHistory.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
            If frm.displayDialog(CInt(lvJobHistory.SelectedItems(0).Tag), enAction.EDIT_ONE, mintEmployeeUnkid) Then
                'S.SANDEEP [ 14 AUG 2012 ] -- END

                'Anjan (17 May 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                'Call FillList()
                lvJobHistory.Groups.Clear()
                lvJobHistory.Items.Clear()
                'Anjan (17 May 2012)-End 

                'S.SANDEEP [ 14 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If mintEmployeeUnkid > 0 Then
                    Call FillList()
                End If
                'S.SANDEEP [ 14 AUG 2012 ] -- END

            End If
            frm = Nothing


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmEmployeeExperience
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
            If frm.displayDialog(-1, enAction.ADD_CONTINUE, mintEmployeeUnkid) Then
                'S.SANDEEP [ 14 AUG 2012 ] -- END
                'Call FillList()
                lvJobHistory.Groups.Clear()
                lvJobHistory.Items.Clear()

                'S.SANDEEP [ 14 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If mintEmployeeUnkid > 0 Then
                    Call FillList()
                End If
                'S.SANDEEP [ 14 AUG 2012 ] -- END

            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            'S.SANDEEP [ 14 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'cboEmployee.SelectedValue = 0
            If mintEmployeeUnkid > 0 Then
                cboEmployee.SelectedValue = mintEmployeeUnkid
            Else
                cboEmployee.SelectedValue = 0
            End If
            'S.SANDEEP [ 14 AUG 2012 ] -- END

            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            txtCompany.Text = ""
            'Anjan (02 Mar 2012)-End 


            txtCompany.Text = ""
            txtSupervisor.Text = ""
            dtpStartDate.Checked = False
            dtpEndDate.Checked = False
            'Anjan (21 Nov 2011)-Start
            'ENHANCEMENT : TRA COMMENTS
            mstrAdvanceFilter = ""
            'Anjan (21 Nov 2011)-End 
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With
            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
            cboEmployee.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnApprovalinfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprovalinfo.Click
        Try
            Dim empid As Integer = 0
            If lvJobHistory.SelectedItems.Count > 0 Then
                empid = CInt(lvJobHistory.SelectedItems(0).SubItems(objdgcolhempid.Index).Text)
            End If
            Dim frm As New frmViewEmployeeDataApproval
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(User._Object._Userunkid, 0, enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences, enScreenName.frmJobHistory_ExperienceList, CType(lvJobHistory.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Tag, clsEmployeeDataApproval.enOperationType), "EM.employeeunkid = " & empid, False)
            If frm IsNot Nothing Then frm.Dispose()
            Exit Sub
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprovalinfo_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
#Region " Controls Events "

    'S.SANDEEP [ 18 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub mnuScanAttachDocuments_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanAttachDocuments.Click
        Dim frm As New frmScanOrAttachmentInfo
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            'S.SANDEEP |26-APR-2019| -- START
            'frm.displayDialog(Language.getMessage(mstrModuleName, 4, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.ADD_ONE, "", mstrModuleName) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
            frm.displayDialog(Language.getMessage(mstrModuleName, 4, "Select Employee"), enImg_Email_RefId.Employee_Module, enAction.ADD_ONE, "", mstrModuleName, True) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
            'S.SANDEEP |26-APR-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuScanAttachDocuments_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 18 FEB 2012 ] -- END

    Private Sub mnuImportExperience_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportExperience.Click
        Dim frm As New frmImportExperienceWizard
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportExperience_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuExportExperience_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportExperience.Click
        'S.SANDEEP [12-Jan-2018] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 0001843
        'Dim IExcel As New ExcelData
        'S.SANDEEP [12-Jan-2018] -- END
        Dim dsList As New DataSet
        Dim path As String = String.Empty
        Dim strFilePath As String = String.Empty
        Dim dlgSaveFile As New SaveFileDialog
        Dim ObjFile As System.IO.FileInfo
        Try
            dlgSaveFile.Filter = "Execl files(*.xlsx)|*.xlsx|XML files (*.xml)|*.xml"
            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                ObjFile = New System.IO.FileInfo(dlgSaveFile.FileName)
                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'strFilePath = ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)                
                strFilePath = ObjFile.DirectoryName & "\"
                strFilePath &= ObjFile.Name.Substring(0, ObjFile.Name.Length - 4) & "_" & eZeeDate.convertDate(Now)
                'S.SANDEEP [12-Jan-2018] -- END
                strFilePath &= ObjFile.Extension

                dsList = objJobExperience.GetExperienceData_Export()

                Select Case dlgSaveFile.FilterIndex
                    Case 1   'XLS
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'IExcel.Export(strFilePath, dsList)
                        OpenXML_Export(strFilePath, dsList)
                        'S.SANDEEP [12-Jan-2018] -- END
                    Case 2   'XML
                        dsList.WriteXml(strFilePath)
                End Select
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "File exported successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportExperience_Click", mstrModuleName)
        Finally
            'S.SANDEEP [12-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 0001843
            'IExcel = Nothing
            'S.SANDEEP [12-Jan-2018] -- END
            dsList.Dispose() : path = String.Empty : strFilePath = String.Empty : dlgSaveFile = Nothing : ObjFile = Nothing
        End Try
    End Sub
    'Anjan (21 Nov 2011)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Anjan (21 Nov 2011)-End 

#End Region

#Region " Listview Events "

    Private Sub lvJobHistory_ItemSelectionChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvJobHistory.ItemSelectionChanged
        Try
            If lvJobHistory.SelectedItems.Count > 0 Then

                If intParentRowIndex <> -1 Then
                    lvJobHistory.Items(intParentRowIndex).BackColor = Color.White
                    intParentRowIndex = -1
                End If

                If lvJobHistory.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Text.Length > 0 Then

                    If lvJobHistory.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Tag IsNot Nothing Then
                        If CInt(lvJobHistory.SelectedItems(0).SubItems(objdgcolhtranguid.Index).Tag) = clsEmployeeDataApproval.enOperationType.EDITED Then
                            Dim item As ListViewItem = lvJobHistory.Items.Cast(Of ListViewItem).Where(Function(x) CInt(x.Tag) = CInt(lvJobHistory.SelectedItems(0).Tag) And x.SubItems(objdgcolhtranguid.Index).Text = "").FirstOrDefault()
                            If item IsNot Nothing Then intParentRowIndex = item.Index
                            lvJobHistory.Items(intParentRowIndex).BackColor = Color.LightCoral
                        End If
                    End If

                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                    btnOperation.Visible = False
                    objtblPanel.Visible = True
                    btnApprovalinfo.Visible = True
                Else
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                    btnOperation.Visible = True
                    btnApprovalinfo.Visible = False
                    objtblPanel.Visible = False
                End If
            Else
                btnEdit.Enabled = True
                btnDelete.Enabled = True
                btnOperation.Visible = True
                btnApprovalinfo.Visible = False
                objtblPanel.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvSkillList_ItemSelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'S.SANDEEP [ 07 NOV 2011 ] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

			Me.btnApprovalinfo.GradientBackColor = GUI._ButttonBackColor 
			Me.btnApprovalinfo.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblSupervisor.Text = Language._Object.getCaption(Me.lblSupervisor.Name, Me.lblSupervisor.Text)
            Me.lblInstitution.Text = Language._Object.getCaption(Me.lblInstitution.Name, Me.lblInstitution.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhCompany.Text = Language._Object.getCaption(CStr(Me.colhCompany.Tag), Me.colhCompany.Text)
            Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
            Me.colhStartDate.Text = Language._Object.getCaption(CStr(Me.colhStartDate.Tag), Me.colhStartDate.Text)
            Me.colhEndDate.Text = Language._Object.getCaption(CStr(Me.colhEndDate.Tag), Me.colhEndDate.Text)
            Me.colhSupervisor.Text = Language._Object.getCaption(CStr(Me.colhSupervisor.Tag), Me.colhSupervisor.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuScanAttachDocuments.Text = Language._Object.getCaption(Me.mnuScanAttachDocuments.Name, Me.mnuScanAttachDocuments.Text)
            Me.mnuImportExperience.Text = Language._Object.getCaption(Me.mnuImportExperience.Name, Me.mnuImportExperience.Text)
            Me.mnuExportExperience.Text = Language._Object.getCaption(Me.mnuExportExperience.Name, Me.mnuExportExperience.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.btnApprovalinfo.Text = Language._Object.getCaption(Me.btnApprovalinfo.Name, Me.btnApprovalinfo.Text)
			Me.colhOperationType.Text = Language._Object.getCaption(CStr(Me.colhOperationType.Tag), Me.colhOperationType.Text)
			Me.lblParentData.Text = Language._Object.getCaption(Me.lblParentData.Name, Me.lblParentData.Text)
			Me.lblPendingData.Text = Language._Object.getCaption(Me.lblPendingData.Name, Me.lblPendingData.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction ?")
            Language.setMessage(mstrModuleName, 3, "File exported successfully.")
            Language.setMessage(mstrModuleName, 4, "Select Employee")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process.")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class