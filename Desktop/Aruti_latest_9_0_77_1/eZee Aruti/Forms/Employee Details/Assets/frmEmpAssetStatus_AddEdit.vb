﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmpAssetStatus_AddEdit

#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmEmpAssetStatus_AddEdit"
    Private mintTranUnkid As Integer = -1
    Private mblnCancel As Boolean = True
    Private objEmployeeAsset As clsEmployee_Assets_Tran
    Private objEmployee As clsEmployee_Master
    Private objAsset As clsCommon_Master
    Private objAssetStatus As clsEmployee_Assets_Status_Tran
    Private menStatus As enEmpAssetStatus
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal enStatus As enEmpAssetStatus) As Boolean
        menStatus = enStatus

        mintTranUnkid = intUnkId

        Me.ShowDialog()

        intUnkId = mintTranUnkid

        Return Not mblnCancel
    End Function
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtEmployeeName.BackColor = GUI.ColorComp
            txtAsset.BackColor = GUI.ColorComp
            txtRemarks.BackColor = GUI.ColorOptional
            cboStatus.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objMaster As New clsMasterData
        Try
            dsCombos = objMaster.getComboListForEmployeeAseetStatus("Status", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Status")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Info()
        Try
            txtEmployeeName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " "
            txtAsset.Text = objAsset._Name
            cboStatus.SelectedValue = CInt(menStatus)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Info", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objAssetStatus._Isvoid = False
            objAssetStatus._Assetstranunkid = objEmployeeAsset._Assetstranunkid
            objAssetStatus._Remark = txtRemarks.Text
            objAssetStatus._Userunkid = User._Object._Userunkid
            objAssetStatus._Status_Date = System.DateTime.Today
            objAssetStatus._Voiddatetime = Nothing
            objAssetStatus._Voiduserunkid = -1
            objAssetStatus._Statusunkid = CInt(cboStatus.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEmpAssetStatus_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmployeeAsset = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpAssetStatus_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmpAssetStatus_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                SendKeys.Send("{Tab}")
            ElseIf e.Control = True AndAlso e.KeyCode = Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpAssetStatus_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub
    Private Sub frmEmpAssetStatus_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmployeeAsset = New clsEmployee_Assets_Tran
        objEmployee = New clsEmployee_Master
        objAsset = New clsCommon_Master
        objAssetStatus = New clsEmployee_Assets_Status_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()

            objEmployeeAsset._Assetstranunkid = mintTranUnkid

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = objEmployeeAsset._Employeeunkid
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objEmployeeAsset._Employeeunkid
            'S.SANDEEP [04 JUN 2015] -- END

            objAsset._Masterunkid = objEmployeeAsset._Assetunkid
            cboStatus.Enabled = False

            Call Fill_Info()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmpAssetStatus_AddEdit_Load", mstrModuleName)
        End Try
    End Sub


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


    'Pinkal (06-May-2014) -- End


#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim dtpStatus As Date = Nothing
        Try


            If CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Status. Status is mandatory information."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Exit Sub
            Else
                Dim dsList = objAssetStatus.GetLastStatus("Status", mintTranUnkid)
                If dsList.Tables("Status").Rows.Count > 0 Then
                    With dsList.Tables("Status").Rows(0)
                        dtpStatus = CDate(.Item("status_date").ToString)
                    End With
                    If dtpStatus.Date > System.DateTime.Today.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Status date should be greater than last change status date : ") & dtpStatus.Date, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objAssetStatus._FormName = mstrModuleName
            objAssetStatus._LoginEmployeeunkid = 0
            objAssetStatus._ClientIP = getIP()
            objAssetStatus._HostName = getHostName()
            objAssetStatus._FromWeb = False
            objAssetStatus._AuditUserId = User._Object._Userunkid
objAssetStatus._CompanyUnkid = Company._Object._Companyunkid
            objAssetStatus._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END


            blnFlag = objAssetStatus.Insert

            If blnFlag Then
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "
   
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbEmpAssetStatusInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbEmpAssetStatusInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbEmpAssetStatusInfo.Text = Language._Object.getCaption(Me.gbEmpAssetStatusInfo.Name, Me.gbEmpAssetStatusInfo.Text)
			Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblAsset.Text = Language._Object.getCaption(Me.lblAsset.Name, Me.lblAsset.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Status. Status is mandatory information.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Status date should be greater than last change status date :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class