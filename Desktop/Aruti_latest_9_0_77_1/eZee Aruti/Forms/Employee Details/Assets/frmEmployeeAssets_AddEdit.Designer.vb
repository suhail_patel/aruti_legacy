﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeAssets_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeAssets_AddEdit))
        Me.pnlEmployeeAsset = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbAssetRegister = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtSerialNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblSerialNo = New System.Windows.Forms.Label
        Me.objbtnAddCondition = New eZee.Common.eZeeGradientButton
        Me.objbtnAddAsset = New eZee.Common.eZeeGradientButton
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtAssetNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblAssetNo = New System.Windows.Forms.Label
        Me.lnAssetInfo = New eZee.Common.eZeeLine
        Me.lnEmployeeName = New eZee.Common.eZeeLine
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboCondition = New System.Windows.Forms.ComboBox
        Me.lblCondition = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.dtpAssignedDate = New System.Windows.Forms.DateTimePicker
        Me.cboAsset = New System.Windows.Forms.ComboBox
        Me.lblAsset = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSaveInfo = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlEmployeeAsset.SuspendLayout()
        Me.gbAssetRegister.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlEmployeeAsset
        '
        Me.pnlEmployeeAsset.Controls.Add(Me.eZeeHeader)
        Me.pnlEmployeeAsset.Controls.Add(Me.gbAssetRegister)
        Me.pnlEmployeeAsset.Controls.Add(Me.objFooter)
        Me.pnlEmployeeAsset.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEmployeeAsset.Location = New System.Drawing.Point(0, 0)
        Me.pnlEmployeeAsset.Name = "pnlEmployeeAsset"
        Me.pnlEmployeeAsset.Size = New System.Drawing.Size(510, 364)
        Me.pnlEmployeeAsset.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(510, 60)
        Me.eZeeHeader.TabIndex = 26
        Me.eZeeHeader.Title = "Company Asset"
        '
        'gbAssetRegister
        '
        Me.gbAssetRegister.BorderColor = System.Drawing.Color.Black
        Me.gbAssetRegister.Checked = False
        Me.gbAssetRegister.CollapseAllExceptThis = False
        Me.gbAssetRegister.CollapsedHoverImage = Nothing
        Me.gbAssetRegister.CollapsedNormalImage = Nothing
        Me.gbAssetRegister.CollapsedPressedImage = Nothing
        Me.gbAssetRegister.CollapseOnLoad = False
        Me.gbAssetRegister.Controls.Add(Me.txtSerialNo)
        Me.gbAssetRegister.Controls.Add(Me.lblSerialNo)
        Me.gbAssetRegister.Controls.Add(Me.objbtnAddCondition)
        Me.gbAssetRegister.Controls.Add(Me.objbtnAddAsset)
        Me.gbAssetRegister.Controls.Add(Me.txtRemark)
        Me.gbAssetRegister.Controls.Add(Me.lblRemark)
        Me.gbAssetRegister.Controls.Add(Me.txtAssetNo)
        Me.gbAssetRegister.Controls.Add(Me.lblAssetNo)
        Me.gbAssetRegister.Controls.Add(Me.lnAssetInfo)
        Me.gbAssetRegister.Controls.Add(Me.lnEmployeeName)
        Me.gbAssetRegister.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbAssetRegister.Controls.Add(Me.cboEmployee)
        Me.gbAssetRegister.Controls.Add(Me.lblEmployee)
        Me.gbAssetRegister.Controls.Add(Me.cboCondition)
        Me.gbAssetRegister.Controls.Add(Me.lblCondition)
        Me.gbAssetRegister.Controls.Add(Me.lblDate)
        Me.gbAssetRegister.Controls.Add(Me.dtpAssignedDate)
        Me.gbAssetRegister.Controls.Add(Me.cboAsset)
        Me.gbAssetRegister.Controls.Add(Me.lblAsset)
        Me.gbAssetRegister.ExpandedHoverImage = Nothing
        Me.gbAssetRegister.ExpandedNormalImage = Nothing
        Me.gbAssetRegister.ExpandedPressedImage = Nothing
        Me.gbAssetRegister.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssetRegister.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssetRegister.HeaderHeight = 25
        Me.gbAssetRegister.HeaderMessage = ""
        Me.gbAssetRegister.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbAssetRegister.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssetRegister.HeightOnCollapse = 0
        Me.gbAssetRegister.LeftTextSpace = 0
        Me.gbAssetRegister.Location = New System.Drawing.Point(12, 66)
        Me.gbAssetRegister.Name = "gbAssetRegister"
        Me.gbAssetRegister.OpenHeight = 275
        Me.gbAssetRegister.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssetRegister.ShowBorder = True
        Me.gbAssetRegister.ShowCheckBox = False
        Me.gbAssetRegister.ShowCollapseButton = False
        Me.gbAssetRegister.ShowDefaultBorderColor = True
        Me.gbAssetRegister.ShowDownButton = False
        Me.gbAssetRegister.ShowHeader = True
        Me.gbAssetRegister.Size = New System.Drawing.Size(486, 235)
        Me.gbAssetRegister.TabIndex = 0
        Me.gbAssetRegister.Temp = 0
        Me.gbAssetRegister.Text = "Asset Register"
        Me.gbAssetRegister.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSerialNo
        '
        Me.txtSerialNo.Flags = 0
        Me.txtSerialNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerialNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSerialNo.Location = New System.Drawing.Point(327, 129)
        Me.txtSerialNo.Name = "txtSerialNo"
        Me.txtSerialNo.Size = New System.Drawing.Size(122, 21)
        Me.txtSerialNo.TabIndex = 23
        '
        'lblSerialNo
        '
        Me.lblSerialNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerialNo.Location = New System.Drawing.Point(258, 132)
        Me.lblSerialNo.Name = "lblSerialNo"
        Me.lblSerialNo.Size = New System.Drawing.Size(65, 15)
        Me.lblSerialNo.TabIndex = 22
        Me.lblSerialNo.Text = "Serial No."
        Me.lblSerialNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddCondition
        '
        Me.objbtnAddCondition.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCondition.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCondition.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCondition.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCondition.BorderSelected = False
        Me.objbtnAddCondition.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCondition.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddCondition.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCondition.Location = New System.Drawing.Point(233, 156)
        Me.objbtnAddCondition.Name = "objbtnAddCondition"
        Me.objbtnAddCondition.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCondition.TabIndex = 19
        '
        'objbtnAddAsset
        '
        Me.objbtnAddAsset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddAsset.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddAsset.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddAsset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddAsset.BorderSelected = False
        Me.objbtnAddAsset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddAsset.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddAsset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddAsset.Location = New System.Drawing.Point(233, 102)
        Me.objbtnAddAsset.Name = "objbtnAddAsset"
        Me.objbtnAddAsset.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddAsset.TabIndex = 7
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(105, 183)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(344, 44)
        Me.txtRemark.TabIndex = 17
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(18, 185)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(83, 15)
        Me.lblRemark.TabIndex = 16
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAssetNo
        '
        Me.txtAssetNo.Flags = 0
        Me.txtAssetNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAssetNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAssetNo.Location = New System.Drawing.Point(105, 129)
        Me.txtAssetNo.Name = "txtAssetNo"
        Me.txtAssetNo.Size = New System.Drawing.Size(122, 21)
        Me.txtAssetNo.TabIndex = 9
        '
        'lblAssetNo
        '
        Me.lblAssetNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssetNo.Location = New System.Drawing.Point(18, 132)
        Me.lblAssetNo.Name = "lblAssetNo"
        Me.lblAssetNo.Size = New System.Drawing.Size(83, 15)
        Me.lblAssetNo.TabIndex = 8
        Me.lblAssetNo.Text = "Asset No."
        Me.lblAssetNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnAssetInfo
        '
        Me.lnAssetInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnAssetInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnAssetInfo.Location = New System.Drawing.Point(8, 80)
        Me.lnAssetInfo.Name = "lnAssetInfo"
        Me.lnAssetInfo.Size = New System.Drawing.Size(165, 19)
        Me.lnAssetInfo.TabIndex = 4
        Me.lnAssetInfo.Text = "Assets Info"
        Me.lnAssetInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnEmployeeName
        '
        Me.lnEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmployeeName.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmployeeName.Location = New System.Drawing.Point(8, 36)
        Me.lnEmployeeName.Name = "lnEmployeeName"
        Me.lnEmployeeName.Size = New System.Drawing.Size(165, 17)
        Me.lnEmployeeName.TabIndex = 0
        Me.lnEmployeeName.Text = "Employee Info"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(455, 56)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 3
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(105, 56)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(344, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(18, 59)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(83, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        '
        'cboCondition
        '
        Me.cboCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCondition.FormattingEnabled = True
        Me.cboCondition.Items.AddRange(New Object() {"Damaged", "Good", "Used"})
        Me.cboCondition.Location = New System.Drawing.Point(105, 156)
        Me.cboCondition.Name = "cboCondition"
        Me.cboCondition.Size = New System.Drawing.Size(122, 21)
        Me.cboCondition.TabIndex = 13
        '
        'lblCondition
        '
        Me.lblCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCondition.Location = New System.Drawing.Point(18, 159)
        Me.lblCondition.Name = "lblCondition"
        Me.lblCondition.Size = New System.Drawing.Size(83, 15)
        Me.lblCondition.TabIndex = 12
        Me.lblCondition.Text = "Condition"
        Me.lblCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(258, 105)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(65, 15)
        Me.lblDate.TabIndex = 10
        Me.lblDate.Text = "Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpAssignedDate
        '
        Me.dtpAssignedDate.Checked = False
        Me.dtpAssignedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAssignedDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAssignedDate.Location = New System.Drawing.Point(327, 102)
        Me.dtpAssignedDate.Name = "dtpAssignedDate"
        Me.dtpAssignedDate.Size = New System.Drawing.Size(122, 21)
        Me.dtpAssignedDate.TabIndex = 11
        '
        'cboAsset
        '
        Me.cboAsset.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAsset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAsset.FormattingEnabled = True
        Me.cboAsset.Location = New System.Drawing.Point(105, 102)
        Me.cboAsset.Name = "cboAsset"
        Me.cboAsset.Size = New System.Drawing.Size(122, 21)
        Me.cboAsset.TabIndex = 6
        '
        'lblAsset
        '
        Me.lblAsset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsset.Location = New System.Drawing.Point(18, 105)
        Me.lblAsset.Name = "lblAsset"
        Me.lblAsset.Size = New System.Drawing.Size(83, 15)
        Me.lblAsset.TabIndex = 5
        Me.lblAsset.Text = "Asset"
        Me.lblAsset.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSaveInfo)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 309)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(510, 55)
        Me.objFooter.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(405, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 25
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSaveInfo
        '
        Me.btnSaveInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveInfo.BackColor = System.Drawing.Color.White
        Me.btnSaveInfo.BackgroundImage = CType(resources.GetObject("btnSaveInfo.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveInfo.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveInfo.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveInfo.FlatAppearance.BorderSize = 0
        Me.btnSaveInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveInfo.ForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveInfo.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveInfo.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.Location = New System.Drawing.Point(306, 13)
        Me.btnSaveInfo.Name = "btnSaveInfo"
        Me.btnSaveInfo.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveInfo.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveInfo.Size = New System.Drawing.Size(93, 30)
        Me.btnSaveInfo.TabIndex = 24
        Me.btnSaveInfo.Text = "&Save"
        Me.btnSaveInfo.UseVisualStyleBackColor = True
        '
        'frmEmployeeAssets_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(510, 364)
        Me.Controls.Add(Me.pnlEmployeeAsset)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeAssets_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Company Asset"
        Me.pnlEmployeeAsset.ResumeLayout(False)
        Me.gbAssetRegister.ResumeLayout(False)
        Me.gbAssetRegister.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlEmployeeAsset As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbAssetRegister As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboCondition As System.Windows.Forms.ComboBox
    Friend WithEvents lblCondition As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents dtpAssignedDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboAsset As System.Windows.Forms.ComboBox
    Friend WithEvents lblAsset As System.Windows.Forms.Label
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSaveInfo As eZee.Common.eZeeLightButton
    Friend WithEvents lnEmployeeName As eZee.Common.eZeeLine
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lnAssetInfo As eZee.Common.eZeeLine
    Friend WithEvents txtAssetNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAssetNo As System.Windows.Forms.Label
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objbtnAddAsset As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddCondition As eZee.Common.eZeeGradientButton
    Friend WithEvents txtSerialNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSerialNo As System.Windows.Forms.Label
End Class
