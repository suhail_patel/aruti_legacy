﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDeletefingerPrint
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDeletefingerPrint))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnDeleteFP = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objDepartment = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.elAssignInfo = New eZee.Common.eZeeLine
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblfingerIndex = New System.Windows.Forms.Label
        Me.cboFingerIndex = New System.Windows.Forms.ComboBox
        Me.lblImage = New System.Windows.Forms.Label
        Me.imgImageControl = New eZee.Common.eZeeImageControl
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblCardNo = New System.Windows.Forms.Label
        Me.txtCardNo = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboDeviceCode = New System.Windows.Forms.ComboBox
        Me.lblDeviceCode = New System.Windows.Forms.Label
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnDeleteFP)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 242)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(413, 55)
        Me.EZeeFooter1.TabIndex = 3
        '
        'btnDeleteFP
        '
        Me.btnDeleteFP.BackColor = System.Drawing.Color.White
        Me.btnDeleteFP.BackgroundImage = CType(resources.GetObject("btnDeleteFP.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteFP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteFP.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteFP.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteFP.FlatAppearance.BorderSize = 0
        Me.btnDeleteFP.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteFP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteFP.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteFP.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteFP.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteFP.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteFP.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteFP.Location = New System.Drawing.Point(185, 13)
        Me.btnDeleteFP.Name = "btnDeleteFP"
        Me.btnDeleteFP.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteFP.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteFP.Size = New System.Drawing.Size(121, 30)
        Me.btnDeleteFP.TabIndex = 4
        Me.btnDeleteFP.Text = "&Delete FingerPrint"
        Me.btnDeleteFP.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(312, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objDepartment
        '
        Me.objDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objDepartment.Location = New System.Drawing.Point(147, 84)
        Me.objDepartment.Name = "objDepartment"
        Me.objDepartment.Size = New System.Drawing.Size(252, 15)
        Me.objDepartment.TabIndex = 181
        Me.objDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(24, 84)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(99, 15)
        Me.lblDepartment.TabIndex = 177
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elAssignInfo
        '
        Me.elAssignInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elAssignInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elAssignInfo.Location = New System.Drawing.Point(12, 9)
        Me.elAssignInfo.Name = "elAssignInfo"
        Me.elAssignInfo.Size = New System.Drawing.Size(428, 16)
        Me.elAssignInfo.TabIndex = 175
        Me.elAssignInfo.Text = "Employee Info"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(24, 59)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(99, 15)
        Me.lblEmployee.TabIndex = 172
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblfingerIndex
        '
        Me.lblfingerIndex.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfingerIndex.Location = New System.Drawing.Point(24, 217)
        Me.lblfingerIndex.Name = "lblfingerIndex"
        Me.lblfingerIndex.Size = New System.Drawing.Size(99, 15)
        Me.lblfingerIndex.TabIndex = 182
        Me.lblfingerIndex.Text = "Finger Index"
        Me.lblfingerIndex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFingerIndex
        '
        Me.cboFingerIndex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFingerIndex.FormattingEnabled = True
        Me.cboFingerIndex.Location = New System.Drawing.Point(147, 214)
        Me.cboFingerIndex.Name = "cboFingerIndex"
        Me.cboFingerIndex.Size = New System.Drawing.Size(104, 21)
        Me.cboFingerIndex.TabIndex = 3
        '
        'lblImage
        '
        Me.lblImage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImage.Location = New System.Drawing.Point(24, 110)
        Me.lblImage.Name = "lblImage"
        Me.lblImage.Size = New System.Drawing.Size(99, 15)
        Me.lblImage.TabIndex = 184
        Me.lblImage.Text = "Image"
        Me.lblImage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'imgImageControl
        '
        Me.imgImageControl._FileMode = True
        Me.imgImageControl._Image = Nothing
        Me.imgImageControl._ShowAddButton = True
        Me.imgImageControl._ShowAt = eZee.Common.eZeeImageControl.PreviewAt.BOTTOM
        Me.imgImageControl._ShowDeleteButton = True
        Me.imgImageControl._ShowPreview = False
        Me.imgImageControl.BackColor = System.Drawing.Color.Transparent
        Me.imgImageControl.Enabled = False
        Me.imgImageControl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imgImageControl.Location = New System.Drawing.Point(147, 110)
        Me.imgImageControl.Name = "imgImageControl"
        Me.imgImageControl.Size = New System.Drawing.Size(126, 93)
        Me.imgImageControl.TabIndex = 185
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(147, 56)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(225, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblCardNo
        '
        Me.lblCardNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCardNo.Location = New System.Drawing.Point(24, 217)
        Me.lblCardNo.Name = "lblCardNo"
        Me.lblCardNo.Size = New System.Drawing.Size(99, 15)
        Me.lblCardNo.TabIndex = 189
        Me.lblCardNo.Text = "Card No"
        Me.lblCardNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCardNo
        '
        Me.txtCardNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtCardNo.Flags = 0
        Me.txtCardNo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCardNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCardNo.Location = New System.Drawing.Point(147, 214)
        Me.txtCardNo.Name = "txtCardNo"
        Me.txtCardNo.ReadOnly = True
        Me.txtCardNo.Size = New System.Drawing.Size(225, 20)
        Me.txtCardNo.TabIndex = 3
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(378, 56)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 190
        '
        'cboDeviceCode
        '
        Me.cboDeviceCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeviceCode.FormattingEnabled = True
        Me.cboDeviceCode.Location = New System.Drawing.Point(147, 29)
        Me.cboDeviceCode.Name = "cboDeviceCode"
        Me.cboDeviceCode.Size = New System.Drawing.Size(225, 21)
        Me.cboDeviceCode.TabIndex = 1
        '
        'lblDeviceCode
        '
        Me.lblDeviceCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeviceCode.Location = New System.Drawing.Point(24, 32)
        Me.lblDeviceCode.Name = "lblDeviceCode"
        Me.lblDeviceCode.Size = New System.Drawing.Size(99, 15)
        Me.lblDeviceCode.TabIndex = 192
        Me.lblDeviceCode.Text = "Device Code"
        Me.lblDeviceCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmDeletefingerPrint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(413, 297)
        Me.Controls.Add(Me.cboDeviceCode)
        Me.Controls.Add(Me.lblDeviceCode)
        Me.Controls.Add(Me.objbtnSearchEmployee)
        Me.Controls.Add(Me.lblCardNo)
        Me.Controls.Add(Me.txtCardNo)
        Me.Controls.Add(Me.cboEmployee)
        Me.Controls.Add(Me.imgImageControl)
        Me.Controls.Add(Me.lblImage)
        Me.Controls.Add(Me.objDepartment)
        Me.Controls.Add(Me.lblDepartment)
        Me.Controls.Add(Me.elAssignInfo)
        Me.Controls.Add(Me.lblEmployee)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.cboFingerIndex)
        Me.Controls.Add(Me.lblfingerIndex)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDeletefingerPrint"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Delete Employee FingerPrint "
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnDeleteFP As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objDepartment As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents elAssignInfo As eZee.Common.eZeeLine
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblfingerIndex As System.Windows.Forms.Label
    Friend WithEvents cboFingerIndex As System.Windows.Forms.ComboBox
    Friend WithEvents lblImage As System.Windows.Forms.Label
    Friend WithEvents imgImageControl As eZee.Common.eZeeImageControl
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblCardNo As System.Windows.Forms.Label
    Friend WithEvents txtCardNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDeviceCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblDeviceCode As System.Windows.Forms.Label
End Class
