﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmfingerPrint_enroll
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmfingerPrint_enroll))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnStartEnroll = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSaveEnrollment = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objDepartment = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.elAssignInfo = New eZee.Common.eZeeLine
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblfingerIndex = New System.Windows.Forms.Label
        Me.cboFingerIndex = New System.Windows.Forms.ComboBox
        Me.lblImage = New System.Windows.Forms.Label
        Me.imgImageControl = New eZee.Common.eZeeImageControl
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.txtCardNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblCardNo = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblDeviceCode = New System.Windows.Forms.Label
        Me.cboDeviceCode = New System.Windows.Forms.ComboBox
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnStartEnroll)
        Me.EZeeFooter1.Controls.Add(Me.btnSaveEnrollment)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 246)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(409, 55)
        Me.EZeeFooter1.TabIndex = 3
        '
        'btnStartEnroll
        '
        Me.btnStartEnroll.BackColor = System.Drawing.Color.White
        Me.btnStartEnroll.BackgroundImage = CType(resources.GetObject("btnStartEnroll.BackgroundImage"), System.Drawing.Image)
        Me.btnStartEnroll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnStartEnroll.BorderColor = System.Drawing.Color.Empty
        Me.btnStartEnroll.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnStartEnroll.FlatAppearance.BorderSize = 0
        Me.btnStartEnroll.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStartEnroll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStartEnroll.ForeColor = System.Drawing.Color.Black
        Me.btnStartEnroll.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnStartEnroll.GradientForeColor = System.Drawing.Color.Black
        Me.btnStartEnroll.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStartEnroll.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnStartEnroll.Location = New System.Drawing.Point(80, 13)
        Me.btnStartEnroll.Name = "btnStartEnroll"
        Me.btnStartEnroll.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnStartEnroll.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnStartEnroll.Size = New System.Drawing.Size(109, 30)
        Me.btnStartEnroll.TabIndex = 4
        Me.btnStartEnroll.Text = "Start &Enrollment"
        Me.btnStartEnroll.UseVisualStyleBackColor = True
        '
        'btnSaveEnrollment
        '
        Me.btnSaveEnrollment.BackColor = System.Drawing.Color.White
        Me.btnSaveEnrollment.BackgroundImage = CType(resources.GetObject("btnSaveEnrollment.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveEnrollment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveEnrollment.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveEnrollment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveEnrollment.FlatAppearance.BorderSize = 0
        Me.btnSaveEnrollment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveEnrollment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveEnrollment.ForeColor = System.Drawing.Color.Black
        Me.btnSaveEnrollment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveEnrollment.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveEnrollment.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveEnrollment.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveEnrollment.Location = New System.Drawing.Point(195, 13)
        Me.btnSaveEnrollment.Name = "btnSaveEnrollment"
        Me.btnSaveEnrollment.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveEnrollment.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveEnrollment.Size = New System.Drawing.Size(110, 30)
        Me.btnSaveEnrollment.TabIndex = 5
        Me.btnSaveEnrollment.Text = "&Save Enrollment"
        Me.btnSaveEnrollment.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(311, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objDepartment
        '
        Me.objDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objDepartment.Location = New System.Drawing.Point(147, 84)
        Me.objDepartment.Name = "objDepartment"
        Me.objDepartment.Size = New System.Drawing.Size(245, 15)
        Me.objDepartment.TabIndex = 181
        Me.objDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(24, 84)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(112, 15)
        Me.lblDepartment.TabIndex = 177
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elAssignInfo
        '
        Me.elAssignInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elAssignInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elAssignInfo.Location = New System.Drawing.Point(12, 10)
        Me.elAssignInfo.Name = "elAssignInfo"
        Me.elAssignInfo.Size = New System.Drawing.Size(428, 16)
        Me.elAssignInfo.TabIndex = 175
        Me.elAssignInfo.Text = "Employee Info"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(24, 59)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(112, 15)
        Me.lblEmployee.TabIndex = 172
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblfingerIndex
        '
        Me.lblfingerIndex.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfingerIndex.Location = New System.Drawing.Point(24, 217)
        Me.lblfingerIndex.Name = "lblfingerIndex"
        Me.lblfingerIndex.Size = New System.Drawing.Size(94, 15)
        Me.lblfingerIndex.TabIndex = 182
        Me.lblfingerIndex.Text = "Finger Index"
        Me.lblfingerIndex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFingerIndex
        '
        Me.cboFingerIndex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFingerIndex.FormattingEnabled = True
        Me.cboFingerIndex.Items.AddRange(New Object() {"Select ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"})
        Me.cboFingerIndex.Location = New System.Drawing.Point(147, 214)
        Me.cboFingerIndex.Name = "cboFingerIndex"
        Me.cboFingerIndex.Size = New System.Drawing.Size(104, 21)
        Me.cboFingerIndex.TabIndex = 3
        '
        'lblImage
        '
        Me.lblImage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImage.Location = New System.Drawing.Point(24, 110)
        Me.lblImage.Name = "lblImage"
        Me.lblImage.Size = New System.Drawing.Size(112, 15)
        Me.lblImage.TabIndex = 184
        Me.lblImage.Text = "Image"
        Me.lblImage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'imgImageControl
        '
        Me.imgImageControl._FileMode = True
        Me.imgImageControl._Image = Nothing
        Me.imgImageControl._ShowAddButton = True
        Me.imgImageControl._ShowAt = eZee.Common.eZeeImageControl.PreviewAt.BOTTOM
        Me.imgImageControl._ShowDeleteButton = True
        Me.imgImageControl._ShowPreview = False
        Me.imgImageControl.BackColor = System.Drawing.Color.Transparent
        Me.imgImageControl.Enabled = False
        Me.imgImageControl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.imgImageControl.Location = New System.Drawing.Point(147, 110)
        Me.imgImageControl.Name = "imgImageControl"
        Me.imgImageControl.Size = New System.Drawing.Size(126, 93)
        Me.imgImageControl.TabIndex = 185
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(147, 56)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(225, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'txtCardNo
        '
        Me.txtCardNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtCardNo.Flags = 0
        Me.txtCardNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCardNo.Location = New System.Drawing.Point(147, 214)
        Me.txtCardNo.Name = "txtCardNo"
        Me.txtCardNo.ReadOnly = True
        Me.txtCardNo.Size = New System.Drawing.Size(225, 20)
        Me.txtCardNo.TabIndex = 3
        '
        'lblCardNo
        '
        Me.lblCardNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCardNo.Location = New System.Drawing.Point(24, 217)
        Me.lblCardNo.Name = "lblCardNo"
        Me.lblCardNo.Size = New System.Drawing.Size(112, 15)
        Me.lblCardNo.TabIndex = 187
        Me.lblCardNo.Text = "Card No"
        Me.lblCardNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(378, 56)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 188
        '
        'lblDeviceCode
        '
        Me.lblDeviceCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeviceCode.Location = New System.Drawing.Point(24, 32)
        Me.lblDeviceCode.Name = "lblDeviceCode"
        Me.lblDeviceCode.Size = New System.Drawing.Size(112, 15)
        Me.lblDeviceCode.TabIndex = 189
        Me.lblDeviceCode.Text = "Device Code"
        Me.lblDeviceCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDeviceCode
        '
        Me.cboDeviceCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeviceCode.FormattingEnabled = True
        Me.cboDeviceCode.Location = New System.Drawing.Point(147, 27)
        Me.cboDeviceCode.Name = "cboDeviceCode"
        Me.cboDeviceCode.Size = New System.Drawing.Size(225, 21)
        Me.cboDeviceCode.TabIndex = 1
        '
        'frmfingerPrint_enroll
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(409, 301)
        Me.Controls.Add(Me.cboDeviceCode)
        Me.Controls.Add(Me.lblDeviceCode)
        Me.Controls.Add(Me.objbtnSearchEmployee)
        Me.Controls.Add(Me.lblCardNo)
        Me.Controls.Add(Me.txtCardNo)
        Me.Controls.Add(Me.cboEmployee)
        Me.Controls.Add(Me.imgImageControl)
        Me.Controls.Add(Me.lblImage)
        Me.Controls.Add(Me.cboFingerIndex)
        Me.Controls.Add(Me.lblfingerIndex)
        Me.Controls.Add(Me.objDepartment)
        Me.Controls.Add(Me.lblDepartment)
        Me.Controls.Add(Me.elAssignInfo)
        Me.Controls.Add(Me.lblEmployee)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmfingerPrint_enroll"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee FingerPrint Enrollment"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnStartEnroll As eZee.Common.eZeeLightButton
    Friend WithEvents btnSaveEnrollment As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objDepartment As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents elAssignInfo As eZee.Common.eZeeLine
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblfingerIndex As System.Windows.Forms.Label
    Friend WithEvents cboFingerIndex As System.Windows.Forms.ComboBox
    Friend WithEvents lblImage As System.Windows.Forms.Label
    Friend WithEvents imgImageControl As eZee.Common.eZeeImageControl
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents txtCardNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCardNo As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblDeviceCode As System.Windows.Forms.Label
    Friend WithEvents cboDeviceCode As System.Windows.Forms.ComboBox
End Class
