﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index= 6

Public Class frmDeletefingerPrint

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmDeletefingerPrint"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objFingerPrint As clsEmployee_Finger
    Dim objfingerDevice As FingerPrintDevice
    'Dim mintMachineSrNo As Integer = 1
    Dim objConnection As Object = Nothing
    Dim intFingerIndex As Integer = -1
    Dim objEmp As clsEmployee_Master

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Dim dsMachineSetting As New DataSet
    Dim objDictConnection As New Dictionary(Of String, Object)
    'Pinkal (20-Jan-2012) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try

    End Function

#End Region

#Region "Form's Event"

    Private Sub frmfingerPrint_enroll_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objFingerPrint = New clsEmployee_Finger
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 



            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            'If ConfigParameter._Object._CommunicationTypeId = enCommunctionType.FingerPrint Then
            '    lblfingerIndex.Visible = True
            '    cboFingerIndex.Visible = True
            '    lblCardNo.Visible = False
            '    txtCardNo.Visible = False
            '    btnDeleteFP.Text = "&Delete FingerPrint"

            'ElseIf ConfigParameter._Object._CommunicationTypeId = enCommunctionType.RFID Then
            '    lblfingerIndex.Visible = False
            '    cboFingerIndex.Visible = False
            '    lblCardNo.Visible = True
            '    txtCardNo.Visible = True
            '    btnDeleteFP.Text = "&Delete Card"

            'End If



            If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") Then
                dsMachineSetting.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")

                For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1
                    If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid")) = 1 Then   'FingerPrint
                lblfingerIndex.Visible = True
                cboFingerIndex.Visible = True
                lblCardNo.Visible = False
                txtCardNo.Visible = False
                btnDeleteFP.Text = "&Delete FingerPrint"

                    ElseIf CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid")) = 2 Then  'RFID
                lblfingerIndex.Visible = False
                cboFingerIndex.Visible = False
                lblCardNo.Visible = True
                txtCardNo.Visible = True
                btnDeleteFP.Text = "&Delete Card"

            End If

                Next
            End If

            InitializeDevice()
            FillCombo()


           
            'If ConfigParameter._Object._CommunicationTypeId = enCommunctionType.FingerPrint And ConfigParameter._Object._DeviceId = enFingerPrintDevice.ZKSoftware Then
            '    cboFingerIndex.SelectedIndex = 0
            'End If

            '  cboEmployee.Select()

            cboDeviceCode.Select()

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmfingerPrint_enroll_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmfingerPrint_enroll_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmfingerPrint_enroll_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmfingerPrint_enroll_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
        If objConnection IsNot Nothing Then
            CType(objConnection, zkemkeeper.CZKEM).Disconnect()
            GC.RemoveMemoryPressure(objConnection.ToString.Length)
            objConnection = Nothing
        End If
        objfingerDevice = Nothing
        objFingerPrint = Nothing
            Me.Dispose(True)
        Catch ex As AccessViolationException

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmfingerPrint_enroll_FormClosed", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_Finger.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Finger"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region "Private Methods"

    Private Sub InitializeDevice()
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If System.IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") Then

                For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                    'Dim DeviceId As enFingerPrintDevice = CType(ConfigParameter._Object._DeviceId, enFingerPrintDevice)
                    Dim DeviceId As Integer = CInt(dsMachineSetting.Tables(0).Rows(i)("commdeviceid").ToString())

                    'Dim ConnetionTypeId As enFingerPrintConnetionType = CType(ConfigParameter._Object._ConnectionTypeId, enFingerPrintConnetionType)
                    Dim ConnetionTypeId As Integer = CInt(dsMachineSetting.Tables(0).Rows(i)("connectiontypeid").ToString())

                    'If dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString() <> "" Then mintMachineSrNo = CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString())

            If DeviceId > 0 Then

                objfingerDevice = New FingerPrintDevice

                Select Case DeviceId

                            Case enFingerPrintDevice.DigitalPersona   'DigitalPersona

                            Case enFingerPrintDevice.SecuGen  'SecuGen

                            Case enFingerPrintDevice.ZKSoftware '.ZKSoftware

                                objConnection = CType(objfingerDevice.SearchDevice(dsMachineSetting.Tables(0).Rows(i)("ip").ToString(), dsMachineSetting.Tables(0).Rows(i)("port").ToString(), DeviceId, ConnetionTypeId), zkemkeeper.CZKEM)

                        If objConnection IsNot Nothing Then
                                    If CType(objConnection, zkemkeeper.CZKEM).RegEvent(CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString()), 65535) Then   'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)

                                        If objDictConnection.ContainsKey(dsMachineSetting.Tables(0).Rows(i)("ip").ToString()) = False Then
                                            objDictConnection.Add(dsMachineSetting.Tables(0).Rows(i)("ip").ToString(), objConnection)
                                        End If

                            End If
                        End If

                                'Sohail (02 Nov 2013) -- Start
                                'TRA - ENHANCEMENT
                            Case enFingerPrintDevice.BioStar
                                Dim result As Integer
                                Dim m_Handle As Integer = 0

                                result = clsBioStar.BS_OpenSocket(dsMachineSetting.Tables(0).Rows(i)("ip").ToString(), CInt(dsMachineSetting.Tables(0).Rows(i)("port").ToString()), m_Handle)
                                If result <> clsBioStar.BS_SUCCESS Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Unable to connect the device.") & ":" & result.ToString, enMsgBoxStyle.Information)
                                Else
                                    result = clsBioStar.BS_CloseSocket(m_Handle)
                                End If
                                'Sohail (02 Nov 2013) -- End

                End Select

            End If
                Next
            Else
                eZeeMsgBox.Show(Language.getMessage("frmEmployeeList", 17, "There is no Device setting found in Aruti configuration.Please Define Device setting in Aruti Configuration -> Device Settings."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InitializeDevice", mstrModuleName)
        End Try
    End Sub

    Private Sub GetEmployeedata()
        Try
            'FOR EMPLOYEE


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If CInt(cboEmployee.SelectedValue) > 0 Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END



                'Anjan (22 Feb 2011)-Start
                'Issue : On windows 2003 server this folder of MyPictures is not there , so it gives error.
                'imgImageControl._FilePath = My.Computer.FileSystem.SpecialDirectories.MyPictures
                imgImageControl._FilePath = ConfigParameter._Object._PhotoPath
                'Anjan (22 Feb 2011)-End

                If objEmp._ImagePath <> "" Then
                    imgImageControl._FileName = imgImageControl._FileName & "\" & objEmp._ImagePath
                Else
                    imgImageControl._FileName = ""
                End If


                'Pinkal (15-Oct-2013) -- Start
                'Enhancement : TRA Changes

                'Dim objShiftMst As New clsNewshift_master
                'objShiftMst._Shiftunkid = objEmp._Shiftunkid
                'objShift.Text = objShiftMst._Shiftname

                'Pinkal (15-Oct-2013) -- End


                Dim objDeptMst As New clsDepartment
                objDeptMst._Departmentunkid = objEmp._Departmentunkid
                objDepartment.Text = objDeptMst._Name

            End If

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeedata", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            objEmp = New clsEmployee_Master

            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'Dim dsList As DataSet = objEmp.GetEmployeeList("Employee", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim dsList As DataSet = objEmp.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            Dim dsList As DataSet

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 

            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = dsList.Tables("Employee")


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes
            If IO.File.Exists(AppSettings._Object._ApplicationPath & "DeviceSetting.xml") Then

                dsList = New DataSet
                dsList.ReadXml(AppSettings._Object._ApplicationPath & "DeviceSetting.xml")

                Dim drrow As DataRow = dsList.Tables(0).NewRow
                drrow("deviceunkid") = 0
                drrow("devicecode") = Language.getMessage(mstrModuleName, 7, "Select")
                dsList.Tables(0).Rows.InsertAt(drrow, 0)

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If i = 0 Then
                        cboDeviceCode.Items.Add(dsList.Tables(0).Rows(i)("devicecode").ToString().Trim)
                    Else
                        cboDeviceCode.Items.Add(dsList.Tables(0).Rows(i)("devicecode").ToString().Trim.Remove(dsList.Tables(0).Rows(i)("devicecode").ToString().Trim.IndexOf("||"), _
                                                                                                              dsList.Tables(0).Rows(i)("devicecode").ToString().Trim.Length - dsList.Tables(0).Rows(i)("devicecode").ToString().Trim.IndexOf("||")))
                    End If

                Next

                cboDeviceCode.SelectedIndex = 0
            End If
            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub DeleteEnrollFingerPrint()
        Try


            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("devicecode = '" & cboDeviceCode.Text & "||" & getIP() & "'")

                If dr.Length > 0 Then


                    Select Case CInt(dr(0)("commdeviceid").ToString())

                        Case enFingerPrintDevice.DigitalPersona 'DigitalPersona

                        Case enFingerPrintDevice.SecuGen  'SecuGen

                        Case enFingerPrintDevice.ZKSoftware  'ZKSoftware


                            'Pinkal (30-Jul-2013) -- Start
                            'Enhancement : TRA Changes

                            Dim sOption As String = "~ZKFPVersion"
                            Dim sValue As String = ""

                            'Pinkal (30-Jul-2013) -- End

                    If CInt(cboFingerIndex.SelectedIndex) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Finger Index is compulsory information.Please select Finger Index."), enMsgBoxStyle.Information)
                        cboFingerIndex.Select()
                        Exit Sub
                    End If

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this employee's finger print?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                                If objDictConnection.ContainsKey(dr(0)("Ip").ToString()) Then
                                    objConnection = objDictConnection(dr(0)("Ip").ToString())
                                End If



                                'Pinkal (30-Jul-2013) -- Start
                                'Enhancement : TRA Changes

                                If CType(objConnection, zkemkeeper.CZKEM).GetSysOption(CInt(dr(0)("machinesrno").ToString()), sOption, sValue) Then

                                    If sValue = "" OrElse sValue = "9" Then

                                If CType(objConnection, zkemkeeper.CZKEM).DeleteEnrollData(CInt(dr(0)("machinesrno").ToString()), CInt(cboEmployee.SelectedValue), CInt(dr(0)("machinesrno").ToString()), CInt(cboFingerIndex.Text)) Then


                                            'S.SANDEEP [28-May-2018] -- START
                                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                                            objFingerPrint._FormName = mstrModuleName
                                            objFingerPrint._LoginEmployeeunkid = 0
                                            objFingerPrint._ClientIP = getIP()
                                            objFingerPrint._HostName = getHostName()
                                            objFingerPrint._FromWeb = False
                                            objFingerPrint._AuditUserId = User._Object._Userunkid
objFingerPrint._CompanyUnkid = Company._Object._Companyunkid
                                            objFingerPrint._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                            'S.SANDEEP [28-May-2018] -- END

                                    If objFingerPrint.Delete(CInt(cboEmployee.SelectedValue), CInt(cboFingerIndex.Text), cboDeviceCode.Text & "||" & getIP()) Then
                                        CType(objConnection, zkemkeeper.CZKEM).RefreshData(CInt(dr(0)("machinesrno").ToString()))
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "This Employee Finger Print has been deleted successfully."), enMsgBoxStyle.Information)
                                cboEmployee_SelectedValueChanged(Nothing, New EventArgs())
                                        cboDeviceCode.Select()
                            End If

                        End If

                                    ElseIf sValue = "10" Then

                                        If CType(objConnection, zkemkeeper.CZKEM).SSR_DeleteEnrollData(CInt(dr(0)("machinesrno").ToString()), CInt(cboEmployee.SelectedValue).ToString(), CInt(cboFingerIndex.Text)) Then


                                            'S.SANDEEP [28-May-2018] -- START
                                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                                            objFingerPrint._FormName = mstrModuleName
                                            objFingerPrint._LoginEmployeeunkid = 0
                                            objFingerPrint._ClientIP = getIP()
                                            objFingerPrint._HostName = getHostName()
                                            objFingerPrint._FromWeb = False
                                            objFingerPrint._AuditUserId = User._Object._Userunkid
objFingerPrint._CompanyUnkid = Company._Object._Companyunkid
                                            objFingerPrint._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                            'S.SANDEEP [28-May-2018] -- END

                                            If objFingerPrint.Delete(CInt(cboEmployee.SelectedValue), CInt(cboFingerIndex.Text), cboDeviceCode.Text & "||" & getIP()) Then
                                                CType(objConnection, zkemkeeper.CZKEM).RefreshData(CInt(dr(0)("machinesrno").ToString()))
                                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "This Employee Finger Print has been deleted successfully."), enMsgBoxStyle.Information)
                                                cboEmployee_SelectedValueChanged(Nothing, New EventArgs())
                                                cboDeviceCode.Select()
                                            End If

                                        End If

                                    End If

                                End If


                                'Pinkal (30-Jul-2013) -- End

                    End If

                            'Sohail (02 Nov 2013) -- Start
                            'TRA - ENHANCEMENT
                        Case enFingerPrintDevice.BioStar
                            Dim result As Integer = 0
                            Dim m_Handle As Integer = 0
                            Dim deviceId As UInteger = 0
                            Dim m_DeviceType As Integer = 0

                            result = clsBioStar.BS_OpenSocket(dr(0)("ip").ToString(), CInt(dr(0)("port").ToString()), m_Handle)
                            If result <> clsBioStar.BS_SUCCESS Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Unable to connect the device.") & ":" & result.ToString, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                            clsBioStar.BS_GetDeviceID(m_Handle, deviceId, m_DeviceType)
                            If deviceId <= 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Unable to connect the device."), enMsgBoxStyle.Information)
                                result = clsBioStar.BS_CloseSocket(m_Handle)
                                Exit Sub
                            End If
                            clsBioStar.BS_SetDeviceID(m_Handle, deviceId, m_DeviceType)

                            result = clsBioStar.BS_DeleteUser(m_Handle, CUInt(cboEmployee.SelectedValue))
                            If result <> clsBioStar.BS_SUCCESS Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to delete fingerprint.") & ":" & result.ToString, enMsgBoxStyle.Information)
                                result = clsBioStar.BS_CloseSocket(m_Handle)
                                Exit Sub
                            End If
                            result = clsBioStar.BS_CloseSocket(m_Handle)

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objFingerPrint._FormName = mstrModuleName
                            objFingerPrint._LoginEmployeeunkid = 0
                            objFingerPrint._ClientIP = getIP()
                            objFingerPrint._HostName = getHostName()
                            objFingerPrint._FromWeb = False
                            objFingerPrint._AuditUserId = User._Object._Userunkid
objFingerPrint._CompanyUnkid = Company._Object._Companyunkid
                            objFingerPrint._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END

                            If objFingerPrint.Delete(CInt(cboEmployee.SelectedValue), CInt(cboFingerIndex.Text), cboDeviceCode.Text & "||" & getIP()) Then
                                Dim objDeviceUser As New clsEmpid_devicemapping
                                Dim intUnkId As Integer = objDeviceUser.GetUnkId(CInt(cboEmployee.SelectedValue))
                                If intUnkId > 0 Then
                                    With objDeviceUser
                                        ._FormName = mstrModuleName
                                        ._LoginEmployeeunkid = 0
                                        ._ClientIP = getIP()
                                        ._HostName = getHostName()
                                        ._FromWeb = False
                                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    End With
                                    If objDeviceUser.Delete(intUnkId) = False Then
                                        Exit Sub
                                    End If
                                End If
                                cboEmployee_SelectedValueChanged(Nothing, New EventArgs())
                                cboDeviceCode.Select()
                            End If
                            'Sohail (02 Nov 2013) -- End

            End Select

                End If

                'For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                '    Select Case CInt(dsMachineSetting.Tables(0).Rows(i)("commdeviceid").ToString())

                '        Case 1 'DigitalPersona

                '        Case 2  'SecuGen

                '        Case 3  'ZKSoftware

                '            If CInt(cboFingerIndex.SelectedIndex) <= 0 Then
                '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Finger Index is compulsory information.Please select Finger Index."), enMsgBoxStyle.Information)
                '                cboFingerIndex.Select()
                '                Exit Sub
                '            End If

                '            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this employee's finger print?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                '                If CType(objConnection, zkemkeeper.CZKEM).DeleteEnrollData(CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString()), CInt(cboEmployee.SelectedValue), CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString()), CInt(cboFingerIndex.Text)) Then

                '                    If objFingerPrint.Delete(CInt(cboEmployee.SelectedValue), CInt(cboFingerIndex.Text), "") Then
                '                        CType(objConnection, zkemkeeper.CZKEM).RefreshData(CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString()))
                '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "This Employee Finger Print has been deleted successfully."), enMsgBoxStyle.Information)
                '                        cboEmployee_SelectedValueChanged(Nothing, New EventArgs())
                '                        cboEmployee.Select()
                '                    End If

                '                End If

                '            End If
                '    End Select

                'Next

            End If

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DeleteEnrollFingerPrint", mstrModuleName)
        End Try
    End Sub

    Private Sub DeleteEnrollCard()
        Try

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then


                Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("devicecode = '" & cboDeviceCode.Text & "||" & getIP() & "'")

                If dr.Length > 0 Then


                    Select Case CInt(dr(0)("commdeviceid").ToString())

                        Case enFingerPrintDevice.DigitalPersona 'DigitalPersona

                        Case enFingerPrintDevice.SecuGen 'SecuGen

                        Case enFingerPrintDevice.ZKSoftware 'ZKSoftware  

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure you want to delete this employee's enrolled card?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objCard As New clsSwipeCardData

                                If CType(objConnection, zkemkeeper.CZKEM).DeleteEnrollData(CInt(dr(0)("machinesrno").ToString()), CInt(cboEmployee.SelectedValue), CInt(dr(0)("machinesrno").ToString()), 12) Then

                            If objCard.Delete(CInt(cboEmployee.SelectedValue)) Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "This Employee card has been deleted successfully."), enMsgBoxStyle.Information)
                                cboEmployee_SelectedValueChanged(Nothing, New EventArgs())
                                cboEmployee.Select()
                            End If

                                    CType(objConnection, zkemkeeper.CZKEM).RefreshData(CInt(dr(0)("machinesrno").ToString()))
                                End If

                            End If

                    End Select

                        End If

                'For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                '    Select Case CInt(dsMachineSetting.Tables(0).Rows(i)("commdeviceid").ToString())

                '        Case 1 'DigitalPersona

                '        Case 2 'SecuGen

                '        Case 3 'ZKSoftware

                '            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure you want to delete this employee's enrolled card?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                '                Dim objCard As New clsSwipeCardData

                '                If CType(objConnection, zkemkeeper.CZKEM).DeleteEnrollData(CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString()), CInt(cboEmployee.SelectedValue), CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString()), 12) Then

                '                    If objCard.Delete(CInt(cboEmployee.SelectedValue)) Then
                '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "This Employee card has been deleted successfully."), enMsgBoxStyle.Information)
                '                        cboEmployee_SelectedValueChanged(Nothing, New EventArgs())
                '                        cboEmployee.Select()
                '                    End If

                '                    CType(objConnection, zkemkeeper.CZKEM).RefreshData(CInt(dsMachineSetting.Tables(0).Rows(i)("machinesrno").ToString()))
                '                End If

                '            End If

                '    End Select

                'Next

                    End If

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DeleteEnrollCard", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Dropdown Events"

    Private Sub cboEmployee_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedValueChanged
        Try

            GetEmployeedata()

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("devicecode = '" & cboDeviceCode.Text & "||" & getIP() & "'")

                If dr.Length > 0 Then

                    If CInt(dr(0)("commtypeid").ToString()) = 1 Then 'FingerPrint 

                Dim dtList As DataTable = Nothing

                        dtList = objFingerPrint.getEmployee_FingerData(CInt(cboEmployee.SelectedValue), dr(0)("devicecode").ToString())

                cboFingerIndex.Items.Clear()

                cboFingerIndex.Items.Insert(0, "Select")

                If dtList.Rows.Count > 0 Then

                            For j As Integer = 0 To dtList.Rows.Count - 1
                                cboFingerIndex.Items.Insert(j + 1, dtList.Rows(j)("finger_no"))
                    Next

                End If

                If cboFingerIndex.Items.Count > 0 Then cboFingerIndex.SelectedIndex = 0

                    ElseIf CInt(dr(0)("commtypeid").ToString()) = 2 Then  'RFID

                Dim mstrCard As String = Nothing
                Dim objcard As New clsSwipeCardData
                txtCardNo.Text = objcard.GetCardNo(CInt(cboEmployee.SelectedValue))

            End If

                End If

                'For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                '    If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 1 Then 'FingerPrint

                '        Dim dtList As DataTable = Nothing

                '        dtList = objFingerPrint.getEmployee_FingerData(CInt(cboEmployee.SelectedValue))

                '        cboFingerIndex.Items.Clear()

                '        cboFingerIndex.Items.Insert(0, "Select")

                '        If dtList.Rows.Count > 0 Then

                '            For j As Integer = 0 To dtList.Rows.Count - 1
                '                cboFingerIndex.Items.Insert(j + 1, dtList.Rows(j)("finger_no"))
                '            Next

                '        End If

                '        If cboFingerIndex.Items.Count > 0 Then cboFingerIndex.SelectedIndex = 0

                '    ElseIf CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 2 Then

                '        Dim mstrCard As String = Nothing
                '        Dim objcard As New clsSwipeCardData
                '        txtCardNo.Text = objcard.GetCardNo(CInt(cboEmployee.SelectedValue))

                '    End If

                'Next

            End If

            'Pinkal (20-Jan-2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDeviceCode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDeviceCode.SelectedIndexChanged
        Try
            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("devicecode = '" & cboDeviceCode.Text & "||" & getIP() & "'")

                If dr.Length > 0 Then

                    If CInt(dr(0)("commtypeid").ToString()) = 1 Then 'FingerPrint 

                        Dim dtList As DataTable = Nothing

                        dtList = objFingerPrint.getEmployee_FingerData(CInt(cboEmployee.SelectedValue), dr(0)("devicecode").ToString())

                        cboFingerIndex.Items.Clear()

                        cboFingerIndex.Items.Insert(0, "Select")

                        If dtList.Rows.Count > 0 Then

                            For j As Integer = 0 To dtList.Rows.Count - 1
                                cboFingerIndex.Items.Insert(j + 1, dtList.Rows(j)("finger_no"))
                            Next

                        End If

                        If cboFingerIndex.Items.Count > 0 Then cboFingerIndex.SelectedIndex = 0

                    ElseIf CInt(dr(0)("commtypeid").ToString()) = 2 Then  'RFID

                        Dim mstrCard As String = Nothing
                        Dim objcard As New clsSwipeCardData
                        txtCardNo.Text = objcard.GetCardNo(CInt(cboEmployee.SelectedValue))

                    End If

                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDeviceCode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Private Sub btnDeleteFP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteFP.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub
            End If

            'Pinkal (20-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If dsMachineSetting IsNot Nothing AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                Dim dr As DataRow() = dsMachineSetting.Tables(0).Select("devicecode = '" & cboDeviceCode.Text & "||" & getIP() & "'")

                If dr.Length > 0 Then

                    If CInt(dr(0)("commtypeid").ToString()) = 1 Then  'FingerPrint
                DeleteEnrollFingerPrint()
                    ElseIf CInt(dr(0)("commtypeid").ToString()) = 2 Then  'RFID
                DeleteEnrollCard()
            End If

                End If

                    'For i As Integer = 0 To dsMachineSetting.Tables(0).Rows.Count - 1

                    '    If CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 1 Then  'FingerPrint
                    '        DeleteEnrollFingerPrint()
                    '    ElseIf CInt(dsMachineSetting.Tables(0).Rows(i)("commtypeid").ToString()) = 2 Then  'RFID
                    '        DeleteEnrollCard()
                    '    End If

                    'Next

            End If

            'Pinkal (20-Jan-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteFP_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
        Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub
'S.SANDEEP [ 15 SEP 2011 ] -- START
    'ENHANCEMENT : CODE OPTIMIZATION
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .DisplayMember = cboEmployee.DisplayMember
                .ValueMember = cboEmployee.ValueMember
                .CodeMember = "employeecode"
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 15 SEP 2011 ] -- END
#End Region


 
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnDeleteFP.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDeleteFP.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDeleteFP.Text = Language._Object.getCaption(Me.btnDeleteFP.Name, Me.btnDeleteFP.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.elAssignInfo.Text = Language._Object.getCaption(Me.elAssignInfo.Name, Me.elAssignInfo.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblfingerIndex.Text = Language._Object.getCaption(Me.lblfingerIndex.Name, Me.lblfingerIndex.Text)
			Me.lblImage.Text = Language._Object.getCaption(Me.lblImage.Name, Me.lblImage.Text)
			Me.imgImageControl.Text = Language._Object.getCaption(Me.imgImageControl.Name, Me.imgImageControl.Text)
			Me.lblCardNo.Text = Language._Object.getCaption(Me.lblCardNo.Name, Me.lblCardNo.Text)
			Me.lblDeviceCode.Text = Language._Object.getCaption(Me.lblDeviceCode.Name, Me.lblDeviceCode.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please select Employee.")
			Language.setMessage(mstrModuleName, 2, "Finger Index is compulsory information.Please select Finger Index.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this employee's finger print?")
			Language.setMessage(mstrModuleName, 4, "This Employee Finger Print has been deleted successfully.")
			Language.setMessage(mstrModuleName, 5, "Are you sure you want to delete this employee's enrolled card?")
			Language.setMessage(mstrModuleName, 6, "This Employee card has been deleted successfully.")
			Language.setMessage(mstrModuleName, 7, "Select")
			Language.setMessage(mstrModuleName, 8, "Unable to delete fingerprint.")
			Language.setMessage(mstrModuleName, 9, "Unable to connect the device.")
			Language.setMessage("frmEmployeeList", 17, "There is no Device setting found in Aruti configuration.Please Define Device setting in Aruti Configuration -> Device Settings.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class