﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmShiftPolicyGlobalAssign
    Private mstrModuleName As String = "frmShiftPolicyGlobalAssign"

#Region " Private Variables "

    Private mdtEmployee As DataTable
    Private dView As DataView
    Private objEmp As clsEmployee_Master
    Private objEmp_Policy As clsemployee_policy_tran
    Private mstrAdvanceFilter As String = String.Empty
    Private objShiftTran As clsEmployee_Shift_Tran
    Private mdtShiftTran As DataTable

#End Region

#Region " Private Methods/Function "

    Private Sub Fill_Emp_Grid()
        Try
            Dim dsEmp As New DataSet : Dim sValue As String = String.Empty
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If dtpEffectiveDate.Enabled = True Then
            '    dsEmp = objEmp.GetList("List", True, , dtpEffectiveDate.Value.Date, dtpEffectiveDate.Value.Date, , , , mstrAdvanceFilter)
            'Else
            '    dsEmp = objEmp.GetList("List", True, , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , mstrAdvanceFilter)
            '    sValue = objEmp_Policy.Assigned_EmpIds()
            'End If

            Dim dtEffDate As Date = Nothing
            If dtpEffectiveDate.Enabled = True Then
                dtEffDate = dtpEffectiveDate.Value.Date
            Else
                dtEffDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                sValue = objEmp_Policy.Assigned_EmpIds()
            End If

            'S.SANDEEP [25-OCT-2017] -- START
            'dsEmp = objEmp.GetList(FinancialYear._Object._DatabaseName, _
            '                       User._Object._Userunkid, _
            '                       FinancialYear._Object._YearUnkid, _
            '                       Company._Object._Companyunkid, _
            '                       dtEffDate, _
            '                       dtEffDate, _
            '                       ConfigParameter._Object._UserAccessModeSetting, _
            '                        True, False, "List", _
            '                       ConfigParameter._Object._ShowFirstAppointmentDate, , , mstrAdvanceFilter)

            Dim StrCheck_Fields As String = String.Empty
            'Pinkal (20-Dec-2017) -- Start
            'Bug - Solved for Voltamp for when assign policy to employees.
            'StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name 


            'Pinkal (29-Jul-2019) -- Start
            'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.
            'StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Appointed_Date
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Appointed_Date & "," & clsEmployee_Master.EmpColEnum.Col_ShiftName
            'Pinkal (29-Jul-2019) -- End


            'Pinkal (20-Dec-2017) -- End


            'Pinkal (06-Nov-2017) -- Start
            'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
            If CInt(cboFilterShift.SelectedValue) > 0 Then
                StrCheck_Fields &= "," & clsEmployee_Master.EmpColEnum.Col_ShiftName
            ElseIf CInt(cboFilterPolicy.SelectedValue) > 0 Then
                StrCheck_Fields &= "," & clsEmployee_Master.EmpColEnum.Col_PolicyName
            End If
            'Pinkal (06-Nov-2017) -- End


            dsEmp = objEmp.GetListForDynamicField(StrCheck_Fields, _
                                                  FinancialYear._Object._DatabaseName, _
                                   User._Object._Userunkid, _
                                   FinancialYear._Object._YearUnkid, _
                                   Company._Object._Companyunkid, _
                                   dtEffDate, _
                                   dtEffDate, _
                                                  ConfigParameter._Object._UserAccessModeSetting, True, False, "List", , , mstrAdvanceFilter)
            'S.SANDEEP [25-OCT-2017] -- END
            
            'S.SANDEEP [04 JUN 2015] -- END

            If sValue.Trim.Length > 0 Then
                mdtEmployee = New DataView(dsEmp.Tables("List"), "employeeunkid NOT IN(" & sValue & ")", "", DataViewRowState.CurrentRows).ToTable
            Else
                'Pinkal (06-Nov-2017) -- Start
                'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
                If CInt(cboFilterShift.SelectedValue) > 0 Then
                    mdtEmployee = New DataView(dsEmp.Tables("List"), "shiftunkid = " & CInt(cboFilterShift.SelectedValue), "", DataViewRowState.CurrentRows).ToTable()
                ElseIf CInt(cboFilterPolicy.SelectedValue) > 0 Then
                    mdtEmployee = New DataView(dsEmp.Tables("List"), "policyunkid = " & CInt(cboFilterPolicy.SelectedValue), "", DataViewRowState.CurrentRows).ToTable()
                Else
                mdtEmployee = New DataView(dsEmp.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
            End If
                'Pinkal (06-Nov-2017) -- End
            End If

            'S.SANDEEP [25-OCT-2017] -- START
            mdtEmployee.Columns(1).ColumnName = "employeecode"
            mdtEmployee.Columns(2).ColumnName = "name"
            'S.SANDEEP [25-OCT-2017] -- END


            'Pinkal (29-Jul-2019) -- Start
            'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.
            'mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
            If mdtEmployee.Columns.Contains("ischeck") = False Then
                Dim dcColumn As New DataColumn("ischeck", System.Type.GetType("System.Boolean"))
                dcColumn.DefaultValue = False
                mdtEmployee.Columns.Add(dcColumn)
            End If
            'Pinkal (29-Jul-2019) -- End




            dView = mdtEmployee.DefaultView
            dgvEmp.AutoGenerateColumns = False

            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "name"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"


            'Pinkal (29-Jul-2019) -- Start
            'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.
            dgcolhShift.DataPropertyName = "Shiftname"
            'Pinkal (29-Jul-2019) -- End


            dgvEmp.DataSource = dView


            'Pinkal (06-Nov-2017) -- Start
            'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
            objAlloacationReset.ShowResult(CStr(dgvEmp.RowCount))
            'Pinkal (06-Nov-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Emp_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Combo()
        Dim dsCombo As New DataSet
        Dim objShift As New clsNewshift_master
        Dim objPolicy As New clspolicy_master
        Try
            dsCombo = objShift.getListForCombo("List", True)
            With cboShift
                .ValueMember = "shiftunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            'Pinkal (06-Nov-2017) -- Start
            'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
            Dim objCommonMst As New clsCommon_Master
            dsCombo = objCommonMst.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "List")
            With cboFilterShiftType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
            dsCombo = Nothing
            objCommonMst = Nothing
            'Pinkal (06-Nov-2017) -- End


            RemoveHandler cboOperation.SelectedIndexChanged, AddressOf cboOperation_SelectedIndexChanged
            With cboOperation
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Shift Assignment"))
                If ConfigParameter._Object._PolicyManagementTNA = True Then
                    .Items.Add(Language.getMessage(mstrModuleName, 3, "Policy Assignment"))
                End If
                .SelectedIndex = 0
            End With
            AddHandler cboOperation.SelectedIndexChanged, AddressOf cboOperation_SelectedIndexChanged

            dsCombo = objPolicy.getListForCombo("List", True)
            With cboPolicy
                .ValueMember = "policyunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            'Pinkal (06-Nov-2017) -- Start
            'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
            With cboFilterPolicy
                .ValueMember = "policyunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List").Copy()
                .SelectedValue = 0
            End With
            'Pinkal (06-Nov-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            objShift = Nothing
        End Try
    End Sub

    Private Function isValid_Data() As Boolean
        Try

            Dim objMaster As New clsMasterData
            Dim iPeriod As Integer = 0
            Dim objPrd As New clscommom_period_Tran

            'Pinkal (20-Jan-2014) -- Start
            'Enhancement : Oman Changes
            'iPeriod = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpEffectiveDate.Value.Date, , FinancialYear._Object._YearUnkid, True)
            iPeriod = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpEffectiveDate.Value.Date)
            'Pinkal (20-Jan-2014) -- End


            If iPeriod > 0 Then
                'Dim objPrd As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPrd._Periodunkid = iPeriod
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = iPeriod
                'Sohail (21 Aug 2015) -- End
                If objPrd._Statusid = enStatusType.Close Then
                    If cboOperation.SelectedIndex = 1 Then 'SHIFT
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you cannot assign shift. Reason : Period is already closed for the selected date."), enMsgBoxStyle.Information)
                    ElseIf cboOperation.SelectedIndex = 2 Then 'POLICY
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot assign policy. Reason : Period is already closed for the selected date."), enMsgBoxStyle.Information)
                    End If
                    Return False
                End If
                'objPrd = Nothing
            End If
            objMaster = Nothing

            mdtEmployee.AcceptChanges()
            Dim dtmp() As DataRow = mdtEmployee.Select("ischeck=true")

            If dtmp.Length <= 0 Then
                If cboOperation.SelectedIndex = 1 Then 'SHIFT
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please check atleast one employee in order to assign shift to employee(s)."), enMsgBoxStyle.Information)
                ElseIf cboOperation.SelectedIndex = 2 Then 'POLICY
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please check atleast one employee in order to assign policy to employee(s)."), enMsgBoxStyle.Information)
                End If
                Return False
            End If

            'S.SANDEEP [ 08 NOV 2013 ] -- START
            If iPeriod > 0 Then
                Dim objTnALvTran As New clsTnALeaveTran
                Dim sValue As String = String.Empty
                Dim selectedTags As List(Of Integer) = dtmp.Select(Function(x) x.Field(Of Integer)("employeeunkid")).Distinct.ToList
                Dim result() As String = selectedTags.Select(Function(x) x.ToString()).ToArray()
                sValue = String.Join(", ", result)


                'Pinkal (03-Jan-2014) -- Start
                'Enhancement : Oman Changes

                'If objTnALvTran.IsPayrollProcessDone(iPeriod, sValue, objPrd._End_Date) = True Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, You cannot assigned shift/policy for the selected date.") & vbCrLf & _
                '             Language.getMessage(mstrModuleName, 29, "Reason : Payroll is already processed for the last date of selected date period."), enMsgBoxStyle.Information)
                '    Return False
                'End If

                Dim mdtTnADate As DateTime = Nothing
                If objPrd._TnA_EndDate.Date < dtpEffectiveDate.Value.Date Then
                    mdtTnADate = dtpEffectiveDate.Value.Date
                Else
                    mdtTnADate = objPrd._TnA_EndDate.Date
                End If

                If objTnALvTran.IsPayrollProcessDone(iPeriod, sValue, mdtTnADate.Date, enModuleReference.TnA) = True Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, You cannot assign shift/policy for the selected date.") & vbCrLf & _
                    '         Language.getMessage(mstrModuleName, 29, "Reason : Payroll is already processed for the last date of selected date period."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, You cannot assign shift/policy for the selected date.") & vbCrLf & Language.getMessage(mstrModuleName, 29, "Reason : Payroll is already processed for the last date of selected date period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 33, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    Return False
                End If

                'Pinkal (03-Jan-2014) -- End

            End If
            'S.SANDEEP [ 08 NOV 2013 ] -- END


            If ConfigParameter._Object._PolicyManagementTNA = True AndAlso cboOperation.SelectedIndex = 1 Then
                Dim iAssigned As String = objEmp_Policy.Assigned_EmpIds()
                If iAssigned.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot assign shift to selected employee(s). Reason, There is no policy assigned to the employee(s).") & vbCrLf & _
                                    Language.getMessage(mstrModuleName, 11, "To assign policy, please enable setting from Aruti Configuration -> Option -> Leave & Time Settings -> Time and Attendance Setting -> Policy Management for TnA.") & vbCrLf & _
                                    Language.getMessage(mstrModuleName, 12, "After this define policy from Time and Attendance and then assign policy to employee(s) from this screen."), enMsgBoxStyle.Information)
                    Return False
                End If
                If iAssigned.Trim.Length > 0 Then
                    dtmp = mdtEmployee.Select("ischeck = true AND employeeunkid NOT IN(" & iAssigned & ")")
                    If dtmp.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, there are some selected employee(s) to whom policy is not assigned. These employee(s) will be highlighted in read and shift will not be assigned."), enMsgBoxStyle.Information)
                        For i As Integer = 0 To dtmp.Length - 1
                            dgvEmp.Rows(mdtEmployee.Rows.IndexOf(dtmp(i))).DefaultCellStyle.ForeColor = Color.Red
                        Next
                        Return False
                    End If
                End If
            End If

            If cboOperation.SelectedIndex = 1 Then
                If lvAssignedShift.Items.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please add atleast one shift in order to assign shift to employee(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
            ElseIf cboOperation.SelectedIndex = 2 Then
                If CInt(cboPolicy.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Policy is mandatory information. Please select Policy to continue."), enMsgBoxStyle.Information)
                    cboPolicy.Focus()
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isVaild_Data", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Fill_Shift_List()
        Try
            lvAssignedShift.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtShiftTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem
                    lvItem.Text = CDate(dtRow.Item("effectivedate").ToString).Date.ToShortDateString
                    lvItem.SubItems.Add(dtRow.Item("shifttype").ToString)
                    lvItem.SubItems.Add(dtRow.Item("shiftname").ToString)
                    lvItem.Tag = dtRow.Item("GUID").ToString
                    lvAssignedShift.Items.Add(lvItem)
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Shift_List", mstrModuleName)
        Finally
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisibility()
        Try
            btnSave.Enabled = User._Object.Privilege._AllowToAddShiftPolicyAssignment
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End

#End Region

#Region " Form's Events "

    Private Sub frmShiftPolicyGlobalAssign_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmp = New clsEmployee_Master
        objEmp_Policy = New clsemployee_policy_tran
        objShiftTran = New clsEmployee_Shift_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Fill_Combo()
            objShiftTran._EmployeeUnkid = 0
            mdtShiftTran = objShiftTran._SDataTable.Copy

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

            'Pinkal (09-Jan-2014) -- Start
            'Enhancement : Oman Changes
            'dtpEffectiveDate.MinDate = FinancialYear._Object._Database_Start_Date
            'Pinkal (09-Jan-2014) -- End


            'S.SANDEEP [ 15 OCT 2013 ] -- START
            'ENHANCEMENT : ENHANCEMENT
            objlblValue.Text = ""
            'S.SANDEEP [ 15 OCT 2013 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmShiftPolicyGlobalAssign_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsEmployee_Shift_Tran.SetMessages()
            clsemployee_policy_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_Shift_Tran,clsemployee_policy_tran"
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnAssignShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssignShift.Click
        Try
            If CInt(cboShift.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Shift is mandatory information. Please select Shift."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            If mdtShiftTran IsNot Nothing AndAlso mdtShiftTran.Rows.Count > 0 Then
                Dim intCount As Integer = -1
                intCount = (From n In mdtShiftTran.AsEnumerable() Where n.Field(Of String)("AUD") <> "D" Select n.Field(Of String)("effdate")).Count()
                If intCount > 0 Then
                    Dim dtMaxdate As DateTime = eZeeDate.convertDate((From n In mdtShiftTran.AsEnumerable() Where n.Field(Of String)("AUD") <> "D" Select n.Field(Of String)("effdate")).Max())
                    Dim query = (From n In mdtShiftTran.AsEnumerable() Where n.Field(Of String)("AUD") <> "D" AndAlso n.Field(Of String)("effdate") = eZeeDate.convertDate(dtMaxdate) Select n)
                    Dim intshiftId As Integer = query(0)("shiftunkid")

                    'Pinkal (29-Jul-2019) -- Start
                    'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.

                    If eZeeDate.convertDate(dtMaxdate.Date) = eZeeDate.convertDate(dtpEffectiveDate.Value.Date) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot assign this shift.") & vbCrLf & _
                                                     Language.getMessage(mstrModuleName, 31, "Reason : shift is already assigned on this date."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    If chkOverwriteIfExist.Checked = False Then
                        If eZeeDate.convertDate(dtMaxdate.Date) > eZeeDate.convertDate(dtpEffectiveDate.Value.Date) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Sorry, you cannot assign the past date to the employee(s). Reason : future date entry is present which is") & " " & dtMaxdate.Date & ".", enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    End If

                    If intshiftId = CInt(cboShift.SelectedValue) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "This Shift is already added to the list. Please add another shift."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    'Pinkal (29-Jul-2019) -- End

                End If
            End If

            'Pinkal (29-Jul-2019) -- Start
            'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.


            If chkOverwriteIfExist.Checked = False Then

            If mdtShiftTran.Rows.Count > 0 Then
                Dim iShiftId As Integer = -1
                Dim dtOldDate As Date = Nothing
                Dim drTemp() As DataRow = mdtShiftTran.Select("AUD <> 'D'")
                If drTemp.Length > 0 Then
                    iShiftId = CInt(drTemp(drTemp.Length - 1).Item("shiftunkid"))
                    dtOldDate = eZeeDate.convertDate((drTemp(drTemp.Length - 1).Item("effdate")))
                    Dim objShiftTran As New clsEmployee_Shift_Tran
                    With objShiftTran
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    'If clsEmployee_Shift_Tran.Shift_Assignment(iShiftId, CInt(cboShift.SelectedValue)) = False Then
                    If clsEmployee_Shift_Tran.Shift_Assignment(iShiftId, CInt(cboShift.SelectedValue), dtOldDate, dtpEffectiveDate.Value.Date) = False Then

                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot assign this shift.") & vbCrLf & _
                                                            Language.getMessage(mstrModuleName, 7, "The possible reason is that current shift timings overlap with some existing shift timings."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If


            ElseIf chkOverwriteIfExist.Checked Then

                Dim objMst As New clsMasterData
                Dim mintPeriodId As Integer = objMst.getCurrentPeriodID(enModuleReference.Payroll, dtpEffectiveDate.Value.Date, FinancialYear._Object._YearUnkid, 0, False, True, Nothing, True)
                objMst = Nothing

                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodId

                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you cannot assign shift. Reason : Period is already closed for the selected date."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                objPeriod = Nothing

                Dim dtEmployee As DataTable = mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).ToList().CopyToDataTable()
                If dtEmployee IsNot Nothing AndAlso dtEmployee.Rows.Count > 0 Then
                    Dim mstrEmpId As String = String.Join(",", (From p In dtEmployee Where (CBool(p.Item("ischeck")) = True) Select (p.Item("employeeunkid").ToString)).ToArray())
                    Dim dtShiftList As DataTable = objShiftTran.Get_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                            , ConfigParameter._Object._UserAccessModeSetting, "List", dtpEffectiveDate.Value.Date, dtpEffectiveDate.Value.Date, , , , "hremployee_shift_tran.employeeunkid in (" & mstrEmpId & ")", Nothing)
                    If dtShiftList IsNot Nothing AndAlso dtShiftList.Rows.Count > 0 Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Shift is already assinged for the selected date to checked employee(s). Are you sure you want to overwrite existing shift with this new one?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        End If
                    End If
                End If
            End If

            'Pinkal (29-Jul-2019) -- End

            Dim dtSRow As DataRow
            dtSRow = mdtShiftTran.NewRow

            dtSRow.Item("shifttranunkid") = -1
            dtSRow.Item("employeeunkid") = 0
            dtSRow.Item("shiftunkid") = CInt(cboShift.SelectedValue)
            dtSRow.Item("isdefault") = False
            dtSRow.Item("userunkid") = User._Object._Userunkid
            dtSRow.Item("isvoid") = False
            dtSRow.Item("voiduserunkid") = -1
            dtSRow.Item("voiddatetime") = DBNull.Value
            dtSRow.Item("voidreason") = ""
            dtSRow.Item("AUD") = "A"
            dtSRow.Item("GUID") = Guid.NewGuid.ToString
            dtSRow.Item("effectivedate") = dtpEffectiveDate.Value
            If CInt(cboShift.SelectedValue) > 0 Then
                Dim dSType() As DataRow = CType(cboShift.DataSource, DataTable).Select("shiftunkid = '" & CInt(cboShift.SelectedValue) & "'")
                If dSType.Length > 0 Then
                    dtSRow.Item("shifttype") = dSType(0).Item("shifttype")
                End If
            End If

            dtSRow.Item("effdate") = eZeeDate.convertDate(dtpEffectiveDate.Value.Date)
            dtSRow.Item("shiftname") = cboShift.Text
            mdtShiftTran.Rows.Add(dtSRow)
            Call Fill_Shift_List()

            'Pinkal (29-Jul-2019) -- Start
            'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.
            chkOverwriteIfExist.Enabled = False
            'Pinkal (29-Jul-2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAssignShift_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeleteShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteShift.Click
        Try
            If lvAssignedShift.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                drTemp = mdtShiftTran.Select("GUID = '" & lvAssignedShift.SelectedItems(0).Tag.ToString & "'")
                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    Call Fill_Shift_List()
                End If
            End If
            If lvAssignedShift.Items.Count <= 0 Then
                dtpEffectiveDate.Enabled = True
                btnAssignShift.Enabled = True
                'Pinkal (29-Jul-2019) -- Start
                'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.
                chkOverwriteIfExist.Enabled = True
                'Pinkal (29-Jul-2019) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteShift_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            If isValid_Data() = False Then Exit Sub
            Dim dtmp() As DataRow = mdtEmployee.Select("ischeck=true")
            Dim mblnInsertFlag As Boolean = False
            Dim mblnLeaveAssigned As Boolean = False


            Application.DoEvents()
            objlblValue.Text = Language.getMessage(mstrModuleName, 26, "Processing : ") & "0/" & dtmp.Length.ToString

            Select Case cboOperation.SelectedIndex

                Case 1  'SHIFT ASSIGNMENT
                    Dim blnFlag As Boolean = False
                    Dim iShiftIds As String = String.Empty
                    Dim objleaveForm As New clsleaveform
                    Dim iCurrShiftId As Integer = 0 : Dim iSameShift As Boolean = False
                    Dim lRows As IEnumerable(Of DataGridViewRow) = From row As DataGridViewRow In dgvEmp.Rows Select row
                    Dim idx As Integer = -1

                    For iEmp As Integer = 0 To dtmp.Length - 1
                        Dim iLoop As Integer = iEmp
                        Dim rows = lRows.Cast(Of DataGridViewRow)().Where(Function(row) row.Cells(dgcolhEcode.Index).Value.ToString().Equals(dtmp(iLoop).Item("employeecode")))
                        idx = -1

                        For Each drrow As DataRow In mdtShiftTran.Rows
                            If drrow.Item("AUD").ToString = "D" Then Continue For


                            'Pinkal (29-Jul-2019) -- Start
                            'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.

                            If chkOverwriteIfExist.Checked = False Then

                            iCurrShiftId = objShiftTran.GetEmployee_Current_ShiftId(CDate(drrow.Item("effectivedate")).Date, CInt(dtmp(iEmp).Item("employeeunkid")))
                            If iCurrShiftId > 0 Then
                                If iCurrShiftId = CInt(drrow.Item("shiftunkid")) Then

                                    If rows.Count > 0 Then
                                        idx = rows(0).Index
                                        If idx >= 0 Then
                                            dgvEmp.Rows(idx).DefaultCellStyle.ForeColor = Color.Red
                                            dgvEmp.Rows(idx).DefaultCellStyle.SelectionForeColor = Color.Red
                                        End If
                                    End If
                                    iSameShift = True
                                    Continue For
                                End If
                            End If

                            If objShiftTran.isExist(CDate(drrow.Item("effectivedate")).Date, CInt(dtmp(iEmp).Item("employeeunkid")), , CInt(drrow.Item("shiftunkid"))).ToString.Trim.Length > 0 Then
                                If blnFlag = False Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, shift cannot be assigned to some of the selected employees. Reason : shift is already assigned to them for the selected or future date and will be highlighted in red."), enMsgBoxStyle.Information)
                                    If rows.Count > 0 Then
                                        idx = rows(0).Index
                                        If idx >= 0 Then
                                            dgvEmp.Rows(idx).DefaultCellStyle.ForeColor = Color.Red
                                            dgvEmp.Rows(idx).DefaultCellStyle.SelectionForeColor = Color.Red
                                        End If
                                    End If
                                    blnFlag = True : Continue For
                                Else
                                    If rows.Count > 0 Then
                                        idx = rows(0).Index
                                        If idx >= 0 Then
                                            dgvEmp.Rows(idx).DefaultCellStyle.ForeColor = Color.Red
                                            dgvEmp.Rows(idx).DefaultCellStyle.SelectionForeColor = Color.Red
                                        End If
                                    End If
                                    Continue For
                                End If
                            End If

                            Dim iOldSftid As Integer = objShiftTran.GetEmployee_Old_ShiftId(CDate(drrow.Item("effectivedate")).Date, CInt(dtmp(iEmp).Item("employeeunkid")))
                            If iOldSftid > 0 Then
                                'YOU CANNOT ASSIGN OTHER SHIFT FOR THE SAME DATE EVEN IF THE TIME IS NOT OVERLAPPING REASON SOME OTHER SHIFT IS ASSIGNED FOR PARTICULAR DATE.
                                'For Each dr As DataRow In mdtShiftTran.Rows
                                '    If clsEmployee_Shift_Tran.Shift_Assignment(iOldSftid, CInt(drrow.Item("shiftunkid"))) = False Then
                                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot assign this shift.") & vbCrLf & _
                                '                                            Language.getMessage(mstrModuleName, 7, "The possible reason is that current shift timings overlap with some existing shift timings."), enMsgBoxStyle.Information)
                                '        Exit Sub
                                '    End If
                                'Next
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry, shift cannot be assigned to some of the selected employee(s). Reason : shift is already assigned to them for the selected date."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If


                            'Pinkal (02-Jun-2017) -- Start
                            'Enhancement - Remove this Validation because Employee On Leave did not able to Assign Shift.

                            'If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) Then
                            '    If objleaveForm.ValidShiftForEmployee(CInt(dtmp(iEmp).Item("employeeunkid")), CDate(drrow.Item("effectivedate")).Date) Then
                            '        If rows.Count > 0 Then
                            '            idx = rows(0).Index
                            '            If idx >= 0 Then
                            '                dgvEmp.Rows(idx).DefaultCellStyle.ForeColor = Color.Red
                            '                dgvEmp.Rows(idx).DefaultCellStyle.SelectionForeColor = Color.Red
                            '            End If
                            '        End If
                            '        mblnLeaveAssigned = True
                            '        Continue For
                            '    End If
                            'End If
                            'Pinkal (02-Jun-2017) -- End

                            End If

                            'Pinkal (29-Jul-2019) -- End

                            Dim iTemp() = mdtShiftTran.Select("shiftunkid = '" & CInt(drrow.Item("shiftunkid")) & "' AND effdate = '" & drrow.Item("effdate").ToString() & "' AND AUD <> 'D'")
                            'Pinkal (07-Oct-2015) -- End

                            objShiftTran._EmployeeUnkid = CInt(dtmp(iEmp).Item("employeeunkid"))
                            objShiftTran._SDataTable = iTemp.CopyToDataTable().Copy

                            With objShiftTran
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            
                            'Pinkal (29-Jul-2019) -- Start
                            'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.
                            'mblnInsertFlag = objShiftTran.InsertUpdateDelete()
                            mblnInsertFlag = objShiftTran.InsertUpdateDelete(0, Nothing, chkOverwriteIfExist.Checked)
                            'Pinkal (29-Jul-2019) -- End

                            If mblnInsertFlag = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Problem in assigning shift."), enMsgBoxStyle.Information)
                                Exit For
                            End If

                        Next
                        Application.DoEvents()
                        objlblValue.Text = Language.getMessage(mstrModuleName, 26, "Processing : ") & (iEmp + 1) & "/" & dtmp.Length.ToString
                    Next

                    If iSameShift Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry, shift cannot be assigned to some of the selected employee(s). Reason : Same shift is assigned to them for the selected or previous date and will be highlighted in red."), enMsgBoxStyle.Information)
                        gbEmployeeInfo.Enabled = True
                        mdtShiftTran.Rows.Clear() : lvAssignedShift.Items.Clear()
                        objchkEmployee.Checked = False
                    End If

                    If mblnLeaveAssigned Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, shift cannot be assigned to some of the selected employee(s). Reason : selected employee(s) has already applied leave for the selected or future date and will be highlighted in red."), enMsgBoxStyle.Information)
                        gbEmployeeInfo.Enabled = True
                        mdtShiftTran.Rows.Clear() : lvAssignedShift.Items.Clear()
                        objchkEmployee.Checked = False
                    ElseIf mblnInsertFlag = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "shift assigned successfully."), enMsgBoxStyle.Information)
                        Call cboOperation_SelectedIndexChanged(sender, e)
                    End If

                Case 2  'POLICY ASSIGNMENT

                    'S.SANDEEP [ 04 FEB 2014 ] -- START
                    Dim lRows As IEnumerable(Of DataGridViewRow) = From row As DataGridViewRow In dgvEmp.Rows Select row
                    Dim idx As Integer = -1
                    'S.SANDEEP [ 04 FEB 2014 ] -- END

                    Dim blnFlag As Boolean = False
                    For i As Integer = 0 To dtmp.Length - 1


                        Dim iLoop As Integer = i
                        Dim rows = lRows.Cast(Of DataGridViewRow)().Where(Function(row) row.Cells(dgcolhEcode.Index).Value.ToString().Equals(dtmp(iLoop).Item("employeecode")))
                        idx = -1

                        If objEmp_Policy.isExist(CInt(cboPolicy.SelectedValue), CDate(IIf(dtpEffectiveDate.Enabled = True, dtpEffectiveDate.Value.Date, eZeeDate.convertDate(dtmp(i).Item("Appointed Date").ToString))), CInt(dtmp(i).Item("employeeunkid"))) = True Then
                            If blnFlag = False Then
                                If chkFirstPolicy.Checked = True Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, policy cannot be assigned to some of the selected employee(s). Reason : Policy is already assigned to them and will be highlighted in red."), enMsgBoxStyle.Information)
                                Else
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, policy cannot be assigned to some of the selected employee(s). Reason : Policy is already assigned to them for the selected date and will be highlighted in red."), enMsgBoxStyle.Information)
                                End If

                                If rows.Count > 0 Then
                                    idx = rows(0).Index
                                    If idx >= 0 Then
                                        dgvEmp.Rows(idx).DefaultCellStyle.ForeColor = Color.Red
                                        dgvEmp.Rows(idx).DefaultCellStyle.SelectionForeColor = Color.Red
                                        blnFlag = True
                                    End If
                                End If
                                Continue For
                            Else
                                If rows.Count > 0 Then
                                    idx = rows(0).Index
                                    If idx >= 0 Then
                                        dgvEmp.Rows(idx).DefaultCellStyle.ForeColor = Color.Red
                                        dgvEmp.Rows(idx).DefaultCellStyle.SelectionForeColor = Color.Red
                                    End If
                                End If
                                Continue For

                            End If
                        Else

                            If objEmp_Policy.isExist(CDate(IIf(dtpEffectiveDate.Enabled = True, dtpEffectiveDate.Value.Date, eZeeDate.convertDate(dtmp(i).Item("Appointed Date").ToString))), CInt(dtmp(i).Item("employeeunkid"))) > 0 Then
                                If blnFlag = False Then
                                    If chkFirstPolicy.Checked = True Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, policy cannot be assigned to some of the selected employee(s). Reason : Policy is already assigned to them and will be highlighted in red."), enMsgBoxStyle.Information)
                                    Else
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, policy cannot be assigned to some of the selected employee(s). Reason : Policy is already assigned to them for the selected date and will be highlighted in red."), enMsgBoxStyle.Information)
                                    End If
                                    If rows.Count > 0 Then
                                        idx = rows(0).Index
                                        If idx >= 0 Then
                                            dgvEmp.Rows(idx).DefaultCellStyle.ForeColor = Color.Red
                                            dgvEmp.Rows(idx).DefaultCellStyle.SelectionForeColor = Color.Red
                                            blnFlag = True
                                        End If
                                    End If
                                    Continue For
                                Else
                                    If rows.Count > 0 Then
                                        idx = rows(0).Index
                                        If idx >= 0 Then
                                            dgvEmp.Rows(idx).DefaultCellStyle.ForeColor = Color.Red
                                            dgvEmp.Rows(idx).DefaultCellStyle.SelectionForeColor = Color.Red
                                        End If
                                    End If
                                    Continue For
                                End If

                            End If

                            objEmp_Policy._Effectivedate = CDate(IIf(dtpEffectiveDate.Enabled = True, dtpEffectiveDate.Value.Date, eZeeDate.convertDate(dtmp(i).Item("Appointed Date").ToString)))

                            objEmp_Policy._Employeeunkid = CInt(dtmp(i).Item("employeeunkid"))
                            objEmp_Policy._Isvoid = False
                            objEmp_Policy._Policyunkid = CInt(cboPolicy.SelectedValue)
                            objEmp_Policy._Userunkid = User._Object._Userunkid
                            objEmp_Policy._Voiddatetime = Nothing
                            objEmp_Policy._Voidreason = ""
                            objEmp_Policy._Voiduserunkid = -1

                            With objEmp_Policy
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With

                            mblnInsertFlag = objEmp_Policy.Insert()
                            If mblnInsertFlag = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Problem in assigning policy."), enMsgBoxStyle.Information)
                                Exit For
                            End If
                        End If

                        Application.DoEvents()
                        objlblValue.Text = Language.getMessage(mstrModuleName, 26, "Processing : ") & (i + 1) & "/" & dtmp.Length.ToString
                    Next
                    If mblnInsertFlag = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Policy assigned successfully."), enMsgBoxStyle.Information)
                        Call cboOperation_SelectedIndexChanged(sender, e)
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            'S.SANDEEP [ 15 OCT 2013 ] -- START
            'ENHANCEMENT : ENHANCEMENT
            Me.Cursor = Cursors.Default
            'S.SANDEEP [ 15 OCT 2013 ] -- END            
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchShift.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboShift.ValueMember
                .DisplayMember = cboShift.DisplayMember
                .DataSource = CType(cboShift.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboShift.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchShift_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchPolicy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPolicy.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboPolicy.ValueMember
                .DisplayMember = cboPolicy.DisplayMember
                .CodeMember = "policycode"
                .DataSource = CType(cboPolicy.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboPolicy.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPolicy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddShift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddShift.Click
        Dim frm As New frmNewShift_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim intRefId As Integer = -1
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objShift As New clsNewshift_master
                dsList = objShift.getListForCombo("Shift", True)
                With cboShift
                    .ValueMember = "shiftunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Shift")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objShift = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddShift_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddPolicy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddPolicy.Click
        Dim frm As New frmPolicy_AddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim intRefId As Integer = -1
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objPolicy As New clspolicy_master
                dsList = objPolicy.getListForCombo("List", True)
                With cboPolicy
                    .ValueMember = "policyunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objPolicy = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddPolicy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            If mstrAdvanceFilter.Trim.Length > 0 Then
                mstrAdvanceFilter = "" : Call Fill_Emp_Grid()
            End If
            'Pinkal (06-Nov-2017) -- Start
            'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
            cboFilterShiftType.SelectedIndex = 0
            cboFilterShift.SelectedIndex = 0
            cboFilterPolicy.SelectedIndex = 0
            objAlloacationReset.ShowResult(CStr(dgvEmp.RowCount))
            'Pinkal (06-Nov-2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (06-Nov-2017) -- Start
    'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.

    Private Sub objbtnSearchFilterShiftType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFilterShiftType.Click, objbtnSearchFilterShift.Click, objbtnSearchFilterPolicy.Click
        Dim frm As New frmCommonSearch
        Try
            Dim icbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToString.ToUpper
                Case objbtnSearchFilterShiftType.Name.ToUpper
                    icbo = cboFilterShiftType
                Case objbtnSearchFilterShift.Name.ToUpper
                    icbo = cboFilterShift
                Case objbtnSearchFilterPolicy.Name.ToUpper
                    icbo = cboFilterPolicy
            End Select

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = icbo.ValueMember
                .DisplayMember = icbo.DisplayMember
                .DataSource = CType(icbo.DataSource, DataTable)
                If .DisplayDialog Then
                    icbo.SelectedValue = .SelectedValue
                End If
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFilterShiftType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    'Pinkal (06-Nov-2017) -- End


#End Region

#Region " Controls Events "

    Private Sub cboOperation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOperation.SelectedIndexChanged
        Try
            chkFirstPolicy.Checked = False
            dtpEffectiveDate.Enabled = True
            objlblValue.Text = ""

            Select Case cboOperation.SelectedIndex
                Case 0  'SELECT
                    gbEmployeeInfo.Enabled = False : gbPolicy.Enabled = False : gbShiftAssignment.Enabled = False
                    chkFirstPolicy.Enabled = False

                    'Pinkal (06-Nov-2017) -- Start
                    'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
                    LblFilterShiftType.Visible = True
                    cboFilterShiftType.Visible = True
                    LblFilterShift.Visible = True
                    cboFilterShift.Visible = True
                    objbtnSearchFilterShift.Visible = True
                    objbtnSearchFilterShiftType.Visible = True
                    LblFilterPolicy.Visible = False
                    cboFilterPolicy.Visible = False
                    objbtnSearchFilterPolicy.Visible = False
                    cboFilterShiftType.SelectedIndex = 0
                    cboFilterShift.SelectedIndex = 0
                    cboFilterPolicy.SelectedIndex = 0
                    'Pinkal (06-Nov-2017) -- End

                    'Pinkal (29-Jul-2019) -- Start
                    'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.
                    chkOverwriteIfExist.Checked = False
                    chkOverwriteIfExist.Visible = False
                    'Pinkal (29-Jul-2019) -- End

                Case 1  'SHIFT
                    gbShiftAssignment.Enabled = True : gbEmployeeInfo.Enabled = True
                    gbPolicy.Enabled = False : chkFirstPolicy.Enabled = False
                    mdtShiftTran.Rows.Clear() : lvAssignedShift.Items.Clear()
                    cboShift.SelectedValue = 0

                    'Pinkal (06-Nov-2017) -- Start
                    'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
                    LblFilterShiftType.Visible = True
                    cboFilterShiftType.Visible = True
                    LblFilterShift.Visible = True
                    cboFilterShift.Visible = True
                    objbtnSearchFilterShift.Visible = True
                    objbtnSearchFilterShiftType.Visible = True
                    LblFilterPolicy.Visible = False
                    cboFilterPolicy.Visible = False
                    objbtnSearchFilterPolicy.Visible = False
                    cboFilterPolicy.SelectedIndex = 0
                    'Pinkal (06-Nov-2017) -- End


                    'Pinkal (29-Jul-2019) -- Start
                    'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.
                    chkOverwriteIfExist.Checked = False
                    chkOverwriteIfExist.Enabled = True
                    chkOverwriteIfExist.Visible = True
                    'Pinkal (29-Jul-2019) -- End


                Case 2  'POLICY
                    gbShiftAssignment.Enabled = False
                    gbPolicy.Enabled = True : chkFirstPolicy.Enabled = True : gbEmployeeInfo.Enabled = True

                    'Pinkal (06-Nov-2017) -- Start
                    'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
                    LblFilterShiftType.Visible = False
                    cboFilterShiftType.Visible = False
                    LblFilterShift.Visible = False
                    cboFilterShift.Visible = False
                    objbtnSearchFilterShift.Visible = False
                    objbtnSearchFilterShiftType.Visible = False
                    cboFilterPolicy.Visible = True
                    LblFilterPolicy.Visible = True
                    objbtnSearchFilterPolicy.Visible = True
                    cboFilterShift.SelectedIndex = 0
                    'Pinkal (06-Nov-2017) -- End

                    'Pinkal (29-Jul-2019) -- Start
                    'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.
                    chkOverwriteIfExist.Checked = False
                    chkOverwriteIfExist.Visible = False
                    'Pinkal (29-Jul-2019) -- End

                    mdtShiftTran.Rows.Clear() : lvAssignedShift.Items.Clear()
            End Select
            Call Fill_Emp_Grid() : objchkEmployee.Checked = False
            dtpEffectiveDate.Enabled = True
            btnAssignShift.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOperation_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkFirstPolicy_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFirstPolicy.CheckedChanged
        Try
            If chkFirstPolicy.Checked = True Then
                dtpEffectiveDate.Enabled = False
            Else
                dtpEffectiveDate.Enabled = True
            End If
            Call Fill_Emp_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkFirstPolicy_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dtpEffectiveDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpEffectiveDate.ValueChanged
        Try
            Call Fill_Emp_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpEffectiveDate_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                Call Fill_Emp_Grid()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvEmp.Rows.Count > 0 Then
                        If dgvEmp.SelectedRows(0).Index = dgvEmp.Rows(dgvEmp.RowCount - 1).Index Then Exit Sub
                        dgvEmp.Rows(dgvEmp.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvEmp.Rows.Count > 0 Then
                        If dgvEmp.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvEmp.Rows(dgvEmp.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' OR " & _
                            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearch.Text & "%'"
            End If
            dView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmp.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvEmp.IsCurrentCellDirty Then
                    Me.dgvEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dView.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmp_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvEmp.CellContentClick, AddressOf dgvEmp_CellContentClick
            For Each dr As DataRowView In dView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvEmp.Refresh()
            AddHandler dgvEmp.CellContentClick, AddressOf dgvEmp_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    'Pinkal (06-Nov-2017) -- Start
    'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.

#Region "ComboBox Events"

    Private Sub cboFilterShiftType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFilterShiftType.SelectedIndexChanged
        Try
            Dim objShift As New clsNewshift_master
            Dim dsList As DataSet = objShift.getListForCombo("List", True, CInt(cboFilterShiftType.SelectedValue))
            With cboFilterShift
                .ValueMember = "shiftunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFilterShiftType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFilterShift_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFilterShift.SelectedIndexChanged, cboFilterPolicy.SelectedIndexChanged
        Try
            Fill_Emp_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFilterShift_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (06-Nov-2017) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPolicy.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPolicy.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbShiftAssignment.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbShiftAssignment.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilter.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilter.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteShift.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteShift.GradientForeColor = GUI._ButttonFontColor

            Me.btnAssignShift.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssignShift.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.gbPolicy.Text = Language._Object.getCaption(Me.gbPolicy.Name, Me.gbPolicy.Text)
            Me.gbShiftAssignment.Text = Language._Object.getCaption(Me.gbShiftAssignment.Name, Me.gbShiftAssignment.Text)
            Me.lblPolicy.Text = Language._Object.getCaption(Me.lblPolicy.Name, Me.lblPolicy.Text)
            Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.Name, Me.lblShift.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
            Me.btnDeleteShift.Text = Language._Object.getCaption(Me.btnDeleteShift.Name, Me.btnDeleteShift.Text)
            Me.btnAssignShift.Text = Language._Object.getCaption(Me.btnAssignShift.Name, Me.btnAssignShift.Text)
            Me.colhShiftType.Text = Language._Object.getCaption(CStr(Me.colhShiftType.Tag), Me.colhShiftType.Text)
            Me.colhShiftName.Text = Language._Object.getCaption(CStr(Me.colhShiftName.Tag), Me.colhShiftName.Text)
            Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.Name, Me.gbFilter.Text)
            Me.lblOperation.Text = Language._Object.getCaption(Me.lblOperation.Name, Me.lblOperation.Text)
            Me.chkFirstPolicy.Text = Language._Object.getCaption(Me.chkFirstPolicy.Name, Me.chkFirstPolicy.Text)
            Me.colhEffectiveDate.Text = Language._Object.getCaption(CStr(Me.colhEffectiveDate.Tag), Me.colhEffectiveDate.Text)
			Me.LblFilterShift.Text = Language._Object.getCaption(Me.LblFilterShift.Name, Me.LblFilterShift.Text)
			Me.LblFilterShiftType.Text = Language._Object.getCaption(Me.LblFilterShiftType.Name, Me.LblFilterShiftType.Text)
			Me.LblFilterPolicy.Text = Language._Object.getCaption(Me.LblFilterPolicy.Name, Me.LblFilterPolicy.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.dgcolhShift.HeaderText = Language._Object.getCaption(Me.dgcolhShift.Name, Me.dgcolhShift.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.chkOverwriteIfExist.Text = Language._Object.getCaption(Me.chkOverwriteIfExist.Name, Me.chkOverwriteIfExist.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Shift Assignment")
            Language.setMessage(mstrModuleName, 3, "Policy Assignment")
            Language.setMessage(mstrModuleName, 4, "Shift is mandatory information. Please select Shift.")
            Language.setMessage(mstrModuleName, 5, "This Shift is already added to the list. Please add another shift.")
            Language.setMessage(mstrModuleName, 6, "Sorry, you cannot assign this shift.")
            Language.setMessage(mstrModuleName, 7, "The possible reason is that current shift timings overlap with some existing shift timings.")
            Language.setMessage(mstrModuleName, 8, "Please check atleast one employee in order to assign shift to employee(s).")
            Language.setMessage(mstrModuleName, 9, "Please check atleast one employee in order to assign policy to employee(s).")
            Language.setMessage(mstrModuleName, 10, "Sorry, you cannot assign shift to selected employee(s). Reason, There is no policy assigned to the employee(s).")
            Language.setMessage(mstrModuleName, 11, "To assign policy, please enable setting from Aruti Configuration -> Option -> Leave & Time Settings -> Time and Attendance Setting -> Policy Management for TnA.")
            Language.setMessage(mstrModuleName, 12, "After this define policy from Time and Attendance and then assign policy to employee(s) from this screen.")
            Language.setMessage(mstrModuleName, 13, "Sorry, there are some selected employee(s) to whom policy is not assigned. These employee(s) will be highlighted in read and shift will not be assigned.")
            Language.setMessage(mstrModuleName, 14, "Sorry, policy cannot be assigned to some of the selected employee(s). Reason : Policy is already assigned to them and will be highlighted in red.")
            Language.setMessage(mstrModuleName, 15, "Sorry, policy cannot be assigned to some of the selected employee(s). Reason : Policy is already assigned to them for the selected date and will be highlighted in red.")
            Language.setMessage(mstrModuleName, 16, "Problem in assigning policy.")
            Language.setMessage(mstrModuleName, 17, "Policy assigned successfully.")
            Language.setMessage(mstrModuleName, 18, "Please add atleast one shift in order to assign shift to employee(s).")
            Language.setMessage(mstrModuleName, 19, "Policy is mandatory information. Please select Policy to continue.")
            Language.setMessage(mstrModuleName, 20, "Problem in assigning shift.")
            Language.setMessage(mstrModuleName, 21, "shift assigned successfully.")
            Language.setMessage(mstrModuleName, 22, "Sorry, shift cannot be assigned to some of the selected employees. Reason : shift is already assigned to them for the selected or future date and will be highlighted in red.")
            Language.setMessage(mstrModuleName, 23, "Sorry, you cannot assign shift. Reason : Period is already closed for the selected date.")
            Language.setMessage(mstrModuleName, 24, "Sorry, you cannot assign policy. Reason : Period is already closed for the selected date.")
            Language.setMessage(mstrModuleName, 25, "Sorry, shift cannot be assigned to some of the selected employee(s). Reason : selected employee(s) has already applied leave for the selected or future date and will be highlighted in red.")
            Language.setMessage(mstrModuleName, 26, "Processing :")
            Language.setMessage(mstrModuleName, 27, "Sorry, shift cannot be assigned to some of the selected employee(s). Reason : Same shift is assigned to them for the selected or previous date and will be highlighted in red.")
            Language.setMessage(mstrModuleName, 28, "Sorry, You cannot assign shift/policy for the selected date.")
            Language.setMessage(mstrModuleName, 29, "Reason : Payroll is already processed for the last date of selected date period.")
            Language.setMessage(mstrModuleName, 30, "Sorry, shift cannot be assigned to some of the selected employee(s). Reason : shift is already assigned to them for the selected date.")
            Language.setMessage(mstrModuleName, 31, "Reason : shift is already assigned on this date.")
            Language.setMessage(mstrModuleName, 32, "Sorry, you cannot assign the past date to the employee(s). Reason : future date entry is present which is")
			Language.setMessage(mstrModuleName, 33, "Do you want to void Payroll?")
            Language.setMessage(mstrModuleName, 34, "Shift is already assinged for the selected date to checked employee(s). Are you sure you want to overwrite existing shift with this new one?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
'FOR CASE 1 OLD LOGIC TO INSERT INTO EMPLOYEE SHIFT TRAN TABLE 

'For iEmp As Integer = 0 To dtmp.Length - 1
'    For Each drrow As DataRow In mdtShiftTran.Rows
'        If drrow.Item("AUD").ToString() <> "D" Then
'            iShiftIds = objShiftTran.isExist(CDate(drrow.Item("effectivedate")).Date, CInt(dtmp(iEmp).Item("employeeunkid")))
'            If iShiftIds.Trim.Length > 0 Then
'                Dim dTab As DataTable = New DataView(mdtShiftTran, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable
'                Dim dsft() As DataRow = dTab.Select("shiftunkid NOT IN(" & iShiftIds & ") AND effectivedate = '" & CDate(drrow.Item("effectivedate")) & "'")
'                If dsft.Length <= 0 Then
'                    If blnFlag = False Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, shift cannot be assigned to some of the selected employee(s). Reason : shift is already assigned to them for the selected or future date and will be highlighted in red."), enMsgBoxStyle.Information)
'                        dgvEmp.Rows(mdtEmployee.Rows.IndexOf(dtmp(iEmp))).DefaultCellStyle.ForeColor = Color.Red
'                        dgvEmp.Rows(mdtEmployee.Rows.IndexOf(dtmp(iEmp))).DefaultCellStyle.SelectionForeColor = Color.Red
'                        blnFlag = True : Continue For
'                    Else
'                        dgvEmp.Rows(mdtEmployee.Rows.IndexOf(dtmp(iEmp))).DefaultCellStyle.ForeColor = Color.Red
'                        dgvEmp.Rows(mdtEmployee.Rows.IndexOf(dtmp(iEmp))).DefaultCellStyle.SelectionForeColor = Color.Red
'                        Continue For
'                    End If
'                Else
'                    dTab = New DataView(mdtShiftTran, "AUD <> 'D' AND shiftunkid NOT IN(" & iShiftIds & ")", "", DataViewRowState.CurrentRows).ToTable
'                    Dim iOldSftid As Integer = objShiftTran.GetEmployee_Old_ShiftId(CDate(drrow.Item("effectivedate")).Date, CInt(dtmp(iEmp).Item("employeeunkid")))
'                    If iOldSftid > 0 Then
'                        For Each dr As DataRow In dTab.Rows
'                            If clsEmployee_Shift_Tran.Shift_Assignment(iOldSftid, CInt(dr.Item("shiftunkid"))) = False Then
'                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot assign this shift.") & vbCrLf & _
'                                                                    Language.getMessage(mstrModuleName, 7, "The possible reason is that current shift timings overlap with some existing shift timings."), enMsgBoxStyle.Information)
'                                Exit Sub
'                            End If
'                        Next
'                    End If
'                    objShiftTran._EmployeeUnkid = CInt(dtmp(iEmp).Item("employeeunkid"))
'                    objShiftTran._SDataTable = dTab.Copy

'                    If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) Then
'                        If objleaveForm.ValidShiftForEmployee(CInt(dtmp(iEmp).Item("employeeunkid")), CDate(drrow.Item("effectivedate")).Date) Then
'                            dgvEmp.Rows(mdtEmployee.Rows.IndexOf(dtmp(iEmp))).DefaultCellStyle.ForeColor = Color.Red
'                            dgvEmp.Rows(mdtEmployee.Rows.IndexOf(dtmp(iEmp))).DefaultCellStyle.SelectionForeColor = Color.Red
'                            mblnLeaveAssigned = True
'                            Continue For
'                        End If
'                    End If

'                    mblnInsertFlag = objShiftTran.InsertUpdateDelete()
'                    If mblnInsertFlag = False Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Problem in assigning shift."), enMsgBoxStyle.Information)
'                        Exit For
'                    End If
'                End If
'            Else
'                objShiftTran._EmployeeUnkid = CInt(dtmp(iEmp).Item("employeeunkid"))
'                objShiftTran._SDataTable = mdtShiftTran.Copy

'                If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) Then
'                    If objleaveForm.ValidShiftForEmployee(CInt(dtmp(iEmp).Item("employeeunkid")), CDate(drrow.Item("effectivedate")).Date) Then
'                        dgvEmp.Rows(mdtEmployee.Rows.IndexOf(dtmp(iEmp))).DefaultCellStyle.ForeColor = Color.Red
'                        dgvEmp.Rows(mdtEmployee.Rows.IndexOf(dtmp(iEmp))).DefaultCellStyle.SelectionForeColor = Color.Red
'                        mblnLeaveAssigned = True
'                        Continue For
'                    End If
'                End If

'                mblnInsertFlag = objShiftTran.InsertUpdateDelete()
'                If mblnInsertFlag = False Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Problem in assigning shift."), enMsgBoxStyle.Information)
'                    Exit For
'                End If
'            End If
'        End If
'    Next
'    'S.SANDEEP [ 15 OCT 2013 ] -- START
'    'ENHANCEMENT : ENHANCEMENT
'    Application.DoEvents()
'    objlblValue.Text = Language.getMessage(mstrModuleName, 26, "Processing : ") & (iEmp + 1) & "/" & dtmp.Length.ToString
'    'S.SANDEEP [ 15 OCT 2013 ] -- END
'Next


''Pinkal (15-Oct-2013) -- Start
''Enhancement : TRA Changes
'objleaveForm = Nothing
'If mblnLeaveAssigned Then
'    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, shift cannot be assigned to some of the selected employee(s). Reason : selected employee(s) was already applied leave for the selected or future date and will be highlighted in red."), enMsgBoxStyle.Information)
'    gbEmployeeInfo.Enabled = True
'    mdtShiftTran.Rows.Clear() : lvAssignedShift.Items.Clear()
'    objchkEmployee.Checked = False
'ElseIf mblnInsertFlag = True Then
'    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "shift assigned successfully."), enMsgBoxStyle.Information)
'    Call cboOperation_SelectedIndexChanged(sender, e)
'End If

''Pinkal (15-Oct-2013) -- End