﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmViewAssigned_Policy

#Region " Private Varaibles "

    Private ReadOnly mstrModuleName As String = "frmViewAssigned_Policy"
    Private mstrAdvanceFilter As String = ""
    Private objEPloicy As clsemployee_policy_tran
    Dim iTable As DataTable = Nothing

#End Region

#Region " Private Functions "

    Private Sub FillCombo()
        Dim objEMaster As New clsEmployee_Master
        Dim objPMaster As New clspolicy_master
        Dim dsList As New DataSet
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEMaster.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , , , , False)
            'Else
            '    dsList = objEMaster.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , , , , False)
            'End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            'dsList = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                    User._Object._Userunkid, _
            '                                    FinancialYear._Object._YearUnkid, _
            '                                    Company._Object._Companyunkid, _
            '                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                    ConfigParameter._Object._UserAccessModeSetting, _
            '                                    True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsList = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                           True, False, "Emp", True)

            'Pinkal (06-Jan-2016) -- End

            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            dsList = objPMaster.getListForCombo("List", True)
            With cboPolicy
                .ValueMember = "policyunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Try
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges 
            If User._Object.Privilege._AllowToViewAssignedPolicyList = False Then Exit Sub
            'Varsha Rana (17-Oct-2017) -- End

            'Pinkal (03-Jan-2014) -- Start
            'Enhancement : Oman Changes

            ' iTable = objEPloicy.Get_List("List", dtpDate1.Value.Date, dtpDate2.Value.Date, CInt(cboEmployee.SelectedValue), CInt(cboPolicy.SelectedValue), mstrAdvanceFilter)

            Dim mdtDate1 As Date = Nothing
            Dim mdtDate2 As Date = Nothing

            If dtpDate1.Checked Then
                mdtDate1 = dtpDate1.Value.Date
            End If

            If dtpDate2.Checked Then
                mdtDate2 = dtpDate2.Value.Date
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'iTable = objEPloicy.Get_List("List", mdtDate1.Date, mdtDate2.Date, CInt(cboEmployee.SelectedValue), CInt(cboPolicy.SelectedValue), mstrAdvanceFilter)
            iTable = objEPloicy.Get_List(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         ConfigParameter._Object._IsIncludeInactiveEmp, "List", _
                                         mdtDate1.Date, _
                                         mdtDate2.Date, _
                                         CInt(cboEmployee.SelectedValue), _
                                         CInt(cboPolicy.SelectedValue), _
                                         mstrAdvanceFilter)
            'Shani(24-Aug-2015) -- End 


            'Pinkal (03-Jan-2014) -- End

            dgvData.AutoGenerateColumns = False
            objdgcolhcheck.DataPropertyName = "ischeck"
            objdgcolhpolicytranid.DataPropertyName = "emppolicytranunkid"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgcolhCode.DataPropertyName = "ecode"
            dgcolhEffectiveDate.DataPropertyName = "effectivedate"
            dgcolhEmployee.DataPropertyName = "ename"
            dgcolhPolicy.DataPropertyName = "policyname"
            dgvData.DataSource = iTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteAssignedPolicy
            'Varsha Rana (17-Oct-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmViewAssigned_Policy_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEPloicy = New clsemployee_policy_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call SetVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmViewAssigned_Policy_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployee_policy_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployee_policy_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If iTable IsNot Nothing Then
                Dim dtmp() As DataRow = iTable.Select("ischeck = True")
                If dtmp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one employee in order to perform further operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                If dtmp.Length > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete checked transaction(s)?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim blnFlag, blnShown As Boolean
                        blnFlag = False : blnShown = False
                        Dim iReason As String = String.Empty
                        Dim frm As New frmReasonSelection

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        frm.displayDialog(enVoidCategoryType.TNA, iReason)
                        If iReason.Length <= 0 Then Exit Sub

                        objEPloicy._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objEPloicy._Voidreason = iReason
                        objEPloicy._Voiduserunkid = User._Object._Userunkid
                        objEPloicy._Isvoid = True
                        Dim iMessage As String = String.Empty
                        For iRow As Integer = 0 To dtmp.Length - 1
                            iMessage = ""
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objEPloicy.isUsed(CInt(dtmp(iRow).Item("employeeunkid")), CDate(dtmp(iRow).Item("effectivedate")), iMessage)
                            objEPloicy.isUsed(CInt(dtmp(iRow).Item("employeeunkid")), CDate(dtmp(iRow).Item("effectivedate")), iMessage, FinancialYear._Object._DatabaseName)
                            'Sohail (21 Aug 2015) -- End
                            If iMessage <> "" Then
                                If blnShown = False Then
                                    eZeeMsgBox.Show(iMessage, enMsgBoxStyle.Information)
                                    blnShown = True
                                    Continue For
                                Else
                                    blnShown = True
                                    Continue For
                                End If
                            End If
                            'S.SANDEEP [ 31 OCT 2013 ] -- START
                            objEPloicy._Effectivedate = CDate(dtmp(iRow).Item("effectivedate"))
                            objEPloicy._Policyunkid = CInt(dtmp(iRow).Item("policyunkid"))
                            objEPloicy._Employeeunkid = CInt(dtmp(iRow).Item("employeeunkid"))

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objEPloicy._FormName = mstrModuleName
                            objEPloicy._LoginEmployeeunkid = 0
                            objEPloicy._ClientIP = getIP()
                            objEPloicy._HostName = getHostName()
                            objEPloicy._FromWeb = False
                            objEPloicy._AuditUserId = User._Object._Userunkid
objEPloicy._CompanyUnkid = Company._Object._Companyunkid
                            objEPloicy._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END

                            'S.SANDEEP [ 31 OCT 2013 ] -- END
                            blnFlag = objEPloicy.Delete(CInt(dtmp(iRow).Item("emppolicytranunkid")))
                            If blnFlag = False Then
                                eZeeMsgBox.Show(objEPloicy._Message, enMsgBoxStyle.Information)
                                dgvData.Rows(iTable.Rows.IndexOf(dtmp(iRow))).DefaultCellStyle.ForeColor = Color.Red
                                Exit For
                            End If
                        Next
                        If blnFlag = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Policy removed successfully."), enMsgBoxStyle.Information)
                            Call Fill_Grid()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboPolicy.SelectedValue = 0
            dtpDate1.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpDate2.Value = ConfigParameter._Object._CurrentDateAndTime
            objChkAll.Checked = False
            iTable = Nothing : dgvData.DataSource = Nothing : mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Control's Events "

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            If iTable IsNot Nothing Then
                For Each dr As DataRow In iTable.Rows
                    dr.Item("ischeck") = CBool(objChkAll.CheckState)
                Next
                dgvData.Refresh()
            End If
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub dgvData_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

            If e.ColumnIndex = objdgcolhcheck.Index Then

                If Me.dgvData.IsCurrentCellDirty Then
                    Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                iTable.AcceptChanges()
                Dim drRow As DataRow() = iTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If iTable.Rows.Count = drRow.Length Then
                        objChkAll.CheckState = CheckState.Checked
                    Else
                        objChkAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objChkAll.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPolicy.Text = Language._Object.getCaption(Me.lblPolicy.Name, Me.lblPolicy.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.dgcolhCode.HeaderText = Language._Object.getCaption(Me.dgcolhCode.Name, Me.dgcolhCode.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhEffectiveDate.HeaderText = Language._Object.getCaption(Me.dgcolhEffectiveDate.Name, Me.dgcolhEffectiveDate.HeaderText)
            Me.dgcolhPolicy.HeaderText = Language._Object.getCaption(Me.dgcolhPolicy.Name, Me.dgcolhPolicy.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please check atleast one employee in order to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete checked transaction(s)?")
            Language.setMessage(mstrModuleName, 3, "Policy removed successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class