﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSwipeCardData
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSwipeCardData))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbSwipeCardInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblMessage = New System.Windows.Forms.Label
        Me.txtData = New System.Windows.Forms.TextBox
        Me.pnlMain.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.gbSwipeCardInformation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objefFormFooter)
        Me.pnlMain.Controls.Add(Me.gbSwipeCardInformation)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(342, 150)
        Me.pnlMain.TabIndex = 1
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnSave)
        Me.objefFormFooter.Controls.Add(Me.btnClose)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 95)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(342, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(145, 12)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(241, 12)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'gbSwipeCardInformation
        '
        Me.gbSwipeCardInformation.BorderColor = System.Drawing.Color.Black
        Me.gbSwipeCardInformation.Checked = False
        Me.gbSwipeCardInformation.CollapseAllExceptThis = False
        Me.gbSwipeCardInformation.CollapsedHoverImage = Nothing
        Me.gbSwipeCardInformation.CollapsedNormalImage = Nothing
        Me.gbSwipeCardInformation.CollapsedPressedImage = Nothing
        Me.gbSwipeCardInformation.CollapseOnLoad = False
        Me.gbSwipeCardInformation.Controls.Add(Me.lblMessage)
        Me.gbSwipeCardInformation.Controls.Add(Me.txtData)
        Me.gbSwipeCardInformation.ExpandedHoverImage = Nothing
        Me.gbSwipeCardInformation.ExpandedNormalImage = Nothing
        Me.gbSwipeCardInformation.ExpandedPressedImage = Nothing
        Me.gbSwipeCardInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSwipeCardInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSwipeCardInformation.HeaderHeight = 25
        Me.gbSwipeCardInformation.HeightOnCollapse = 0
        Me.gbSwipeCardInformation.LeftTextSpace = 0
        Me.gbSwipeCardInformation.Location = New System.Drawing.Point(9, 9)
        Me.gbSwipeCardInformation.Name = "gbSwipeCardInformation"
        Me.gbSwipeCardInformation.OpenHeight = 300
        Me.gbSwipeCardInformation.Padding = New System.Windows.Forms.Padding(3)
        Me.gbSwipeCardInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSwipeCardInformation.ShowBorder = True
        Me.gbSwipeCardInformation.ShowCheckBox = False
        Me.gbSwipeCardInformation.ShowCollapseButton = False
        Me.gbSwipeCardInformation.ShowDefaultBorderColor = True
        Me.gbSwipeCardInformation.ShowDownButton = False
        Me.gbSwipeCardInformation.ShowHeader = True
        Me.gbSwipeCardInformation.Size = New System.Drawing.Size(324, 81)
        Me.gbSwipeCardInformation.TabIndex = 0
        Me.gbSwipeCardInformation.Temp = 0
        Me.gbSwipeCardInformation.Text = "Swipe Card Data"
        Me.gbSwipeCardInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMessage
        '
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(8, 32)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(307, 13)
        Me.lblMessage.TabIndex = 0
        Me.lblMessage.Text = "Please swipe your card"
        '
        'txtData
        '
        Me.txtData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtData.Location = New System.Drawing.Point(8, 50)
        Me.txtData.Name = "txtData"
        Me.txtData.Size = New System.Drawing.Size(308, 21)
        Me.txtData.TabIndex = 1
        '
        'frmSwipeCardData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(342, 150)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSwipeCardData"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Swipe Card "
        Me.pnlMain.ResumeLayout(False)
        Me.objefFormFooter.ResumeLayout(False)
        Me.gbSwipeCardInformation.ResumeLayout(False)
        Me.gbSwipeCardInformation.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbSwipeCardInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents txtData As System.Windows.Forms.TextBox
End Class
