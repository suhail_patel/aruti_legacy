﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmSwipeCardData

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmSwipeCardData"
    Private mintEmployeeUnkId As Integer = -1
    Private objClsCarData As clsSwipeCardData
#End Region

#Region " Display Dialog "
    Public Sub displayDialog(ByVal intUserUnkId As Integer)
        Try
            mintEmployeeUnkId = intUserUnkId
            Me.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Function "
    Private Sub SetValue()
        Try
            objClsCarData._EmployeeUnkId = mintEmployeeUnkId
            objClsCarData._CardData = txtData.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form "
    Private Sub frmSwipeCardData_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            objClsCarData = New clsSwipeCardData

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmSwipeCardData_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSwipeCardData_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            txtData.Focus()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmSwipeCardData_Shown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSwipeCardData_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{tab}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSwipeCardData_KeyPress", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End
#End Region

#Region " Button's "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If txtData.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "MSR Track Data can not be blank. it is compulsory information."), enMsgBoxStyle.Information)
                txtData.Focus()
                Exit Sub
            End If

            If objClsCarData.IsUsed(txtData.Text, mintEmployeeUnkId) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "This Card is already assigned to another User, please use another Card."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Call SetValue()


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objClsCarData._FormName = mstrModuleName
            objClsCarData._LoginEmployeeunkid = 0
            objClsCarData._ClientIP = getIP()
            objClsCarData._HostName = getHostName()
            objClsCarData._FromWeb = False
            objClsCarData._AuditUserId = User._Object._Userunkid
            objClsCarData._CompanyUnkid = Company._Object._Companyunkid
            objClsCarData._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objClsCarData.Insert() Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbSwipeCardInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSwipeCardInformation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbSwipeCardInformation.Text = Language._Object.getCaption(Me.gbSwipeCardInformation.Name, Me.gbSwipeCardInformation.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "MSR Track Data can not be blank. it is compulsory information.")
            Language.setMessage(mstrModuleName, 2, "This Card is already assigned to another User, please use another Card.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class