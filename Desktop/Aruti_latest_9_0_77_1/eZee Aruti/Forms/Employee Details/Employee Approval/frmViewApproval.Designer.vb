﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewApproval
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewApproval))
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objchkCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objchkView = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDepartment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhClassGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhClass = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPriority = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApprUsrId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhMappingId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhiRead = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objefemailFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objefemailFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objchkCheck, Me.objchkView, Me.dgcolhEmployee, Me.dgcolhDepartment, Me.dgcolhJob, Me.dgcolhClassGrp, Me.dgcolhClass, Me.dgcolhApprover, Me.dgcolhLevel, Me.dgcolhPriority, Me.dgcolhStatus, Me.objdgcolhEmpId, Me.objdgcolhApprUsrId, Me.objdgcolhMappingId, Me.objdgcolhiRead})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.Size = New System.Drawing.Size(849, 413)
        Me.dgvData.TabIndex = 8
        '
        'objchkCheck
        '
        Me.objchkCheck.Frozen = True
        Me.objchkCheck.HeaderText = ""
        Me.objchkCheck.Name = "objchkCheck"
        Me.objchkCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objchkCheck.Visible = False
        Me.objchkCheck.Width = 25
        '
        'objchkView
        '
        Me.objchkView.Frozen = True
        Me.objchkView.HeaderText = ""
        Me.objchkView.Image = Global.Aruti.Main.My.Resources.Resources.doc_view
        Me.objchkView.Name = "objchkView"
        Me.objchkView.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objchkView.Visible = False
        Me.objchkView.Width = 25
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.Frozen = True
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmployee.Width = 200
        '
        'dgcolhDepartment
        '
        Me.dgcolhDepartment.Frozen = True
        Me.dgcolhDepartment.HeaderText = "Department"
        Me.dgcolhDepartment.Name = "dgcolhDepartment"
        Me.dgcolhDepartment.ReadOnly = True
        Me.dgcolhDepartment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhJob
        '
        Me.dgcolhJob.Frozen = True
        Me.dgcolhJob.HeaderText = "Job"
        Me.dgcolhJob.Name = "dgcolhJob"
        Me.dgcolhJob.ReadOnly = True
        Me.dgcolhJob.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhClassGrp
        '
        Me.dgcolhClassGrp.Frozen = True
        Me.dgcolhClassGrp.HeaderText = "Class Group"
        Me.dgcolhClassGrp.Name = "dgcolhClassGrp"
        Me.dgcolhClassGrp.ReadOnly = True
        Me.dgcolhClassGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhClass
        '
        Me.dgcolhClass.Frozen = True
        Me.dgcolhClass.HeaderText = "Class"
        Me.dgcolhClass.Name = "dgcolhClass"
        Me.dgcolhClass.ReadOnly = True
        Me.dgcolhClass.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhApprover
        '
        Me.dgcolhApprover.HeaderText = "Approver"
        Me.dgcolhApprover.Name = "dgcolhApprover"
        Me.dgcolhApprover.ReadOnly = True
        Me.dgcolhApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhApprover.Width = 110
        '
        'dgcolhLevel
        '
        Me.dgcolhLevel.HeaderText = "Level"
        Me.dgcolhLevel.Name = "dgcolhLevel"
        Me.dgcolhLevel.ReadOnly = True
        Me.dgcolhLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhPriority
        '
        Me.dgcolhPriority.HeaderText = "Priority"
        Me.dgcolhPriority.Name = "dgcolhPriority"
        Me.dgcolhPriority.ReadOnly = True
        Me.dgcolhPriority.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhPriority.Visible = False
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.ReadOnly = True
        Me.objdgcolhEmpId.Visible = False
        '
        'objdgcolhApprUsrId
        '
        Me.objdgcolhApprUsrId.HeaderText = "objdgcolhApprUsrId"
        Me.objdgcolhApprUsrId.Name = "objdgcolhApprUsrId"
        Me.objdgcolhApprUsrId.ReadOnly = True
        Me.objdgcolhApprUsrId.Visible = False
        '
        'objdgcolhMappingId
        '
        Me.objdgcolhMappingId.HeaderText = "objdgcolhMappingId"
        Me.objdgcolhMappingId.Name = "objdgcolhMappingId"
        Me.objdgcolhMappingId.ReadOnly = True
        Me.objdgcolhMappingId.Visible = False
        '
        'objdgcolhiRead
        '
        Me.objdgcolhiRead.HeaderText = "objdgcolhiRead"
        Me.objdgcolhiRead.Name = "objdgcolhiRead"
        Me.objdgcolhiRead.Visible = False
        '
        'objefemailFooter
        '
        Me.objefemailFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefemailFooter.Controls.Add(Me.btnClose)
        Me.objefemailFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefemailFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefemailFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefemailFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objefemailFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objefemailFooter.Location = New System.Drawing.Point(0, 413)
        Me.objefemailFooter.Name = "objefemailFooter"
        Me.objefemailFooter.Size = New System.Drawing.Size(849, 55)
        Me.objefemailFooter.TabIndex = 9
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(743, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 85
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmViewApproval
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(849, 468)
        Me.Controls.Add(Me.dgvData)
        Me.Controls.Add(Me.objefemailFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmViewApproval"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "My Report"
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objefemailFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objefemailFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objchkCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objchkView As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDepartment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhClassGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhClass As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPriority As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApprUsrId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhMappingId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhiRead As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
