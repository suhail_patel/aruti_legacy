﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmEmployeeApproval

#Region " Private Varaibles "

    Private objEmpApprovalTran As New clsemployeeapproval_Tran
    Private ReadOnly mstrModuleName As String = "frmEmployeeApproval"
    Private mdvData As DataView

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsEmp As New DataSet
        Try
            dsEmp = objEmpApprovalTran.GetEmployeeList(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, User._Object._Userunkid)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsEmp.Tables("List")
                .SelectedValue = 0
                If CInt(.SelectedValue) <= 0 Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            If txtApprover.Text.Trim.Length > 0 Then
                Dim dtTable As New DataTable
                dtTable = objEmpApprovalTran.GetEmployeeApprovalData(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, 344, User._Object._Userunkid, CInt(txtLevel.Tag), chkMyApprovals.Checked)

                dgvData.AutoGenerateColumns = False

                objchkCheck.DataPropertyName = "icheck"
                dgcolhEmployee.DataPropertyName = "Employee"
                dgcolhDepartment.DataPropertyName = "Department"
                dgcolhJob.DataPropertyName = "Job"
                dgcolhClassGrp.DataPropertyName = "ClassGroup"
                dgcolhClass.DataPropertyName = "Class"
                dgcolhApprover.DataPropertyName = "username"
                dgcolhLevel.DataPropertyName = "Level"
                dgcolhPriority.DataPropertyName = "priority"
                dgcolhStatus.DataPropertyName = "Status"
                objdgcolhEmpId.DataPropertyName = "employeeunkid"
                objdgcolhApprUsrId.DataPropertyName = "userunkid"
                objdgcolhMappingId.DataPropertyName = "mappingid"
                objdgcolhiRead.DataPropertyName = "iRead"

                mdvData = dtTable.DefaultView

                dgvData.DataSource = mdvData

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetApproverInfo()
        Dim objUMapping As New clsemp_appUsermapping
        Dim dsInfo As New DataSet
        Try
            dsInfo = objUMapping.GetList(enEmpApproverType.APPR_EMPLOYEE, "List", True, " AND hremp_appusermapping.isactive = 1 ", User._Object._Userunkid, Nothing)
            If dsInfo.Tables("List").Rows.Count > 0 Then
                txtApprover.Text = dsInfo.Tables("List").Rows(0)("User").ToString()
                txtApprover.Tag = CInt(dsInfo.Tables("List").Rows(0)("mappingunkid"))

                txtLevel.Text = dsInfo.Tables("List").Rows(0)("Level").ToString()
                txtLevel.Tag = CInt(dsInfo.Tables("List").Rows(0)("priority"))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetApproverInfo", mstrModuleName)
        Finally
            objUMapping = Nothing
        End Try
    End Sub

    Private Function EscapeLikeValue(ByVal value As String) As String
        Dim sb As New StringBuilder(value.Length)
        For i As Integer = 0 To value.Length - 1
            Dim c As Char = value(i)
            Select Case c
                Case "]"c, "["c, "%"c, "*"c
                    sb.Append("[").Append(c).Append("]")
                    Exit Select
                Case "'"c
                    sb.Append("''")
                    Exit Select
                Case Else
                    sb.Append(c)
                    Exit Select
            End Select
        Next
        Return sb.ToString()
    End Function

    Private Sub SetGridFormat()
        Try
            RemoveHandler dgvData.DataBindingComplete, AddressOf dgvData_DataBindingComplete
            For Each dgrow As DataGridViewRow In dgvData.Rows
                If CInt(User._Object._Userunkid) <> CInt(dgrow.Cells(objdgcolhApprUsrId.Index).Value) Then
                    If CInt(dgrow.Cells(objdgcolhiRead.Index).Value) <= 0 Then dgrow.Cells(objdgcolhiRead.Index).Value = 2
                End If

                Select Case CInt(dgrow.Cells(objdgcolhiRead.Index).Value)
                    Case 1
                        dgrow.Cells(objchkCheck.Index).ReadOnly = True
                        dgrow.DefaultCellStyle.ForeColor = Color.Blue
                    Case 2
                        dgrow.Cells(objchkCheck.Index).ReadOnly = True
                        dgrow.DefaultCellStyle.ForeColor = Color.Gray
                End Select
            Next
            AddHandler dgvData.DataBindingComplete, AddressOf dgvData_DataBindingComplete
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridFormat", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetValue() As Boolean
        Try
            objEmpApprovalTran._Tranguid = Guid.NewGuid().ToString()
            objEmpApprovalTran._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            objEmpApprovalTran._Mappingunkid = CInt(txtApprover.Tag)            
            objEmpApprovalTran._Isvoid = False
            objEmpApprovalTran._Audittype = 1
            objEmpApprovalTran._Audituserunkid = User._Object._Userunkid
            objEmpApprovalTran._Form_Name = mstrModuleName
            objEmpApprovalTran._Hostname = getHostName()
            objEmpApprovalTran._Ip = getIP()
            objEmpApprovalTran._Isweb = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeApproval_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpApprovalTran = New clsemployeeapproval_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetApproverInfo()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeApproval_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeApproval_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmpApprovalTran = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsemployeeapproval_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsemployeeapproval_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnReject.Click
        Try
            If mdvData IsNot Nothing Then
                'Dim objLevel As New clsempapproverlevel_master
                'Dim intMaxPriority As Integer = 0
                'intMaxPriority = objLevel.GetMaxPriority()
                'objLevel = Nothing

                Dim dtTable As DataTable = mdvData.ToTable()

                If dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)(objchkCheck.DataPropertyName) = True).Count() <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Please check atleast one employee in order to perform further operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Select Case CType(sender, eZee.Common.eZeeLightButton).Name
                    Case btnApprove.Name
                        If txtRemark.Text.Trim.Length <= 0 Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Are you sure you want to approve employee(s) without remark?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If
                        End If
                    Case btnReject.Name
                        If txtRemark.Text.Trim.Length <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Rejection remark is mandatory information. Please enter enter remark to continue."), enMsgBoxStyle.Information)
                            txtRemark.Focus()
                            Exit Sub
                        End If
                End Select
                

                Dim strCheckedEmpIds As String = ""
                Dim dtAppr As DataTable

                strCheckedEmpIds = String.Join(",", dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("icheck") = True).Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())

                dtAppr = objEmpApprovalTran.GetNextEmployeeApprovers(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, strCheckedEmpIds, ConfigParameter._Object._UserAccessModeSetting, 344, Nothing, False, CInt(txtLevel.Tag))
                If dtAppr.Rows.Count > 0 Then
                    strCheckedEmpIds = String.Join(",", dtAppr.AsEnumerable().Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())
                Else
                    strCheckedEmpIds = ""
                End If
                Dim drtemp() As DataRow = Nothing
                If strCheckedEmpIds.Trim.Length > 0 Then
                    drtemp = dtTable.Select("employeeunkid not in (" & strCheckedEmpIds & ") AND icheck = true ")
                Else
                    drtemp = dtTable.Select("icheck = true ")
                End If
                If drtemp.Length > 0 Then
                    For index As Integer = 0 To drtemp.Length - 1
                        drtemp(index)("isfinal") = True
                    Next
                End If
                dtTable.AcceptChanges()

                Call SetValue()
                Dim mblnIsRejection As Boolean = False
                objEmpApprovalTran._Remark = txtRemark.Text
                Select Case CType(sender, eZee.Common.eZeeLightButton).Name
                    Case btnApprove.Name
                        objEmpApprovalTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.Approved
                    Case btnReject.Name
                        objEmpApprovalTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.Rejected
                        mblnIsRejection = True
                End Select

                dtTable = New DataView(dtTable, "icheck = true", "", DataViewRowState.CurrentRows).ToTable

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'Sohail (27 Nov 2018) -- Start
                'NMB Issue - Email is sent to employee everytime whenver employee is updated and send company details to newly approved employee option is ticked in 75.1.
                'If objEmpApprovalTran.Insert(dtTable, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), False, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, ConfigParameter._Object._UserMustchangePwdOnNextLogon, getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP, ConfigParameter._Object._IsHRFlexcubeIntegrated, ConfigParameter._Object._FlexcubeServiceCollection, ConfigParameter._Object._FlexcubeAccountCategory, ConfigParameter._Object._FlexcubeAccountClass) = False Then
                If objEmpApprovalTran.Insert(dtTable, FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, CStr(ConfigParameter._Object._IsArutiDemo), Company._Object._Total_Active_Employee_ForAllCompany, ConfigParameter._Object._NoOfEmployees, User._Object._Userunkid, ConfigParameter._Object._DonotAttendanceinSeconds, ConfigParameter._Object._UserAccessModeSetting, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), False, ConfigParameter._Object._IsIncludeInactiveEmp, User._Object.Privilege._AllowToApproveEarningDeduction, ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CreateADUserFromEmpMst, ConfigParameter._Object._UserMustchangePwdOnNextLogon, ConfigParameter._Object._SendDetailToEmployee, ConfigParameter._Object._SMSCompanyDetailToNewEmp, getHostName(), getIP(), User._Object._Username, enLogin_Mode.DESKTOP, ConfigParameter._Object._IsHRFlexcubeIntegrated, ConfigParameter._Object._FlexcubeServiceCollection, ConfigParameter._Object._FlexcubeAccountCategory, ConfigParameter._Object._FlexcubeAccountClass, Company._Object._Senderaddress, ConfigParameter._Object._SMSGatewayEmail, ConfigParameter._Object._SMSGatewayEmailType) = False Then
                    'Sohail (27 Nov 2018) -- End
                    'S.SANDEEP [26-SEP-2018] -- START
                    'ConfigParameter._Object._IsHRFlexcubeIntegrated, ConfigParameter._Object._FlexcubeServiceCollection, ConfigParameter._Object._FlexcubeAccountCategory, ConfigParameter._Object._FlexcubeAccountClass
                    'S.SANDEEP [26-SEP-2018] -- END
                     'Pinkal (18-Aug-2018) -- End
                    eZeeMsgBox.Show(objEmpApprovalTran._Message)
                Else
                    strCheckedEmpIds = String.Join(",", dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("icheck") = True).Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())
                    Call objEmpApprovalTran.SendApprlRejectNotification(mblnIsRejection, txtRemark.Text, FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, strCheckedEmpIds, ConfigParameter._Object._UserAccessModeSetting, 344, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Userunkid, User._Object._Email, ConfigParameter._Object._ArutiSelfServiceURL, CInt(txtLevel.Tag), ConfigParameter._Object._EmployeeRejectNotificationUserIds, ConfigParameter._Object._EmployeeRejectNotificationTemplateId)
                    FillGrid()
                    txtRemark.Text = ""
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If cboEmployee.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            mdvData.Table.Rows.Clear()
            objbtnReset.ShowResult(dgvData.RowCount.ToString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillGrid()
            objbtnSearch.ShowResult(dgvData.RowCount.ToString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnShowMyReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowMyReport.Click
        Dim frm As New frmViewApproval
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(User._Object._Userunkid, CInt(txtLevel.Tag))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnShowMyReport_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If cboEmployee.DataSource Is Nothing Then Exit Sub

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim str As String = ""
        Try
            If (txtSearch.Text.Length > 0) Then
                str = dgcolhEmployee.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhEmployee.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhDepartment.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhJob.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhClassGrp.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhClass.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhApprover.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhLevel.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' OR " & _
                      dgcolhStatus.DataPropertyName & " LIKE '%" & EscapeLikeValue(txtSearch.Text) & "%' "
            End If
            mdvData.RowFilter = str
            objbtnSearch.ShowResult(dgvData.RowCount.ToString())
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            For Each dr As DataRowView In mdvData
                If CInt(dr.Item(objdgcolhiRead.DataPropertyName)) <= 0 Then
                    dr.Item(objchkCheck.DataPropertyName) = CBool(objchkAll.CheckState)
                End If
            Next
            dgvData.Refresh()
            AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub chkMyApprovals_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMyApprovals.CheckedChanged
        Try
            Call FillGrid()
            objbtnSearch.ShowResult(dgvData.RowCount.ToString)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkMyApprovals_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Datagrid Event(s) "

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If e.ColumnIndex = objchkCheck.Index Then
                If Me.dgvData.IsCurrentCellDirty Then
                    Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = mdvData.ToTable.Select("icheck = true", "")
                If drRow.Length > 0 Then
                    If mdvData.ToTable.Rows.Count = drRow.Length Then
                        objchkAll.CheckState = CheckState.Checked
                    Else
                        objchkAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkAll.CheckState = CheckState.Unchecked
                End If
            ElseIf e.ColumnIndex = objchkView.Index Then
                Dim strData As String = ""
                Dim objListing As New ArutiReports.clsEmployee_Listing(User._Object._Languageunkid, Company._Object._Companyunkid)
                Dim blnIsFinYear As Boolean = False
                If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    blnIsFinYear = True
                End If

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'strData = objListing.ViewEmployeeDetails(CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value), _
                '                                         blnIsFinYear, GUI.fmtCurrency, ConfigParameter._Object._ShowFirstAppointmentDate, _
                '                                         User._Object.Privilege._AllowTo_View_Scale, _
                '                                         ConfigParameter._Object._EmpMandatoryFieldsIDs, ConfigParameter._Object._PendingEmployeeScreenIDs, _
                '                                         True, True, True, True)

                strData = objListing.ViewEmployeeDetails(CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value), _
                                                         blnIsFinYear, GUI.fmtCurrency, ConfigParameter._Object._ShowFirstAppointmentDate, _
                                                         User._Object.Privilege._AllowTo_View_Scale, _
                                                         ConfigParameter._Object._EmpMandatoryFieldsIDs, ConfigParameter._Object._PendingEmployeeScreenIDs, _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, True, True, True, True)
                'Pinkal (18-Aug-2018) -- End


                If strData.Trim.Length > 0 Then
                    Dim frm As New frmHTMLReportView
                    frm.displayDialog(strData, False)
                End If
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub

    Private Sub dgvData_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
        Try
            SetGridFormat()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnReject.GradientBackColor = GUI._ButttonBackColor
            Me.btnReject.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.btnReject.Text = Language._Object.getCaption(Me.btnReject.Name, Me.btnReject.Text)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhDepartment.HeaderText = Language._Object.getCaption(Me.dgcolhDepartment.Name, Me.dgcolhDepartment.HeaderText)
            Me.dgcolhJob.HeaderText = Language._Object.getCaption(Me.dgcolhJob.Name, Me.dgcolhJob.HeaderText)
            Me.dgcolhClassGrp.HeaderText = Language._Object.getCaption(Me.dgcolhClassGrp.Name, Me.dgcolhClassGrp.HeaderText)
            Me.dgcolhClass.HeaderText = Language._Object.getCaption(Me.dgcolhClass.Name, Me.dgcolhClass.HeaderText)
            Me.dgcolhApprover.HeaderText = Language._Object.getCaption(Me.dgcolhApprover.Name, Me.dgcolhApprover.HeaderText)
            Me.dgcolhLevel.HeaderText = Language._Object.getCaption(Me.dgcolhLevel.Name, Me.dgcolhLevel.HeaderText)
            Me.dgcolhPriority.HeaderText = Language._Object.getCaption(Me.dgcolhPriority.Name, Me.dgcolhPriority.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.lblLoggedInUser.Text = Language._Object.getCaption(Me.lblLoggedInUser.Name, Me.lblLoggedInUser.Text)
            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class