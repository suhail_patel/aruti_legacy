﻿Option Strict On

#Region " Import "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmOrbitBankAcctMapping

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmployeeBank_AddEdit"
    Private mintEffectivePeriodId As Integer = 0
    Private mintBankGroupId As Integer
    Private mintBranchId As Integer = 0
    Private mintAccountTypeId As Integer = 0
    Private mintDistributionModeId As Integer = 0
    Private mdecDistributionValue As Decimal = 0
    Private mdtPeriod_startdate As DateTime
    Private mdtPeriod_enddate As DateTime
    Private mintPeriod_Status As Integer
    Private mblnIsOkbutton As Boolean = False
#End Region

#Region " Properties "

    Public ReadOnly Property _EffectivePeriodId() As Integer
        Get
            Return mintEffectivePeriodId
        End Get
    End Property

    Public ReadOnly Property _BankGroupId() As Integer
        Get
            Return mintBankGroupId
        End Get
    End Property

    Public ReadOnly Property _BranchId() As Integer
        Get
            Return mintBranchId
        End Get
    End Property

    Public ReadOnly Property _AccountTypeId() As Integer
        Get
            Return mintAccountTypeId
        End Get
    End Property

    Public ReadOnly Property _DistributionModeId() As Integer
        Get
            Return mintDistributionModeId
        End Get
    End Property

    Public ReadOnly Property _DistributionValue() As Decimal
        Get
            Return mdecDistributionValue
        End Get
    End Property

    Public ReadOnly Property _IsMapped() As Boolean
        Get
            Return mblnIsOkbutton
        End Get
    End Property

#End Region

#Region " Form's Event "

    Private Sub frmOrbitBankAcctMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            SetColor()
            Call OtherSettings()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOrbitBankAcctMapping_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboAccountType.BackColor = GUI.ColorComp
            cboBankBranch.BackColor = GUI.ColorComp
            cboBankGroup.BackColor = GUI.ColorComp
            txtPercentage.BackColor = GUI.ColorComp
            cboMode.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objBank As New clspayrollgroup_master
        Dim objAccType As New clsBankAccType
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim intCurrPeriodID As Integer
        Try
            dsCombo = objBank.getListForCombo(enGroupType.BankGroup, "Banks", True)
            With cboBankGroup
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Banks")
                .SelectedValue = 0
            End With


            dsCombo = objAccType.getComboList(True, "AccType")
            With cboAccountType
                .ValueMember = "accounttypeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("AccType")
                .SelectedValue = 0
            End With

            dsCombo = objMaster.GetPaymentBy("Mode")
            With cboMode
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Mode")
                .SelectedValue = 0
            End With
            intCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = intCurrPeriodID
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objBank = Nothing
            objAccType = Nothing
            objMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If cboPeriod.SelectedValue Is Nothing Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, This Period is closed."), enMsgBoxStyle.Information)
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            ElseIf mintPeriod_Status = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, This Period is closed."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboBankGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Bank Group is compulsory information. Please select Bank Group to continue."), enMsgBoxStyle.Information)
                cboBankGroup.Focus()
                Return False
            End If

            If CInt(cboBankBranch.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Bank Branch is compulsory information. Please select Bank Branch to continue."), enMsgBoxStyle.Information)
                cboBankBranch.Focus()
                Return False
            End If

            If CInt(cboAccountType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Account Type is compulsory information. Please select Account Type to continue."), enMsgBoxStyle.Information)
                cboAccountType.Focus()
                Return False
            End If

            If CInt(cboMode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Please select Salary Distribution Mode."), enMsgBoxStyle.Information)
                cboMode.Focus()
                Return False
            End If

            If CInt(cboMode.SelectedValue) = enPaymentBy.Percentage AndAlso txtPercentage.Decimal > 100 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Percentage should not be greater than 100."), enMsgBoxStyle.Information)
                txtPercentage.Focus()
                Return False
            ElseIf CInt(cboMode.SelectedValue) = enPaymentBy.Percentage AndAlso txtPercentage.Decimal < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, Percentage should be greater than Zero."), enMsgBoxStyle.Information)
                txtPercentage.Focus()
                Return False
            ElseIf CInt(cboMode.SelectedValue) = enPaymentBy.Value AndAlso txtPercentage.Decimal < 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, Amount should be greater than Zero."), enMsgBoxStyle.Information)
                txtPercentage.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Button's Event "

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            If IsValid() = False Then
                Exit Sub
            End If
            mintEffectivePeriodId = CInt(cboPeriod.SelectedValue)
            mintBankGroupId = CInt(cboBankGroup.SelectedValue)
            mintBranchId = CInt(cboBankBranch.SelectedValue)
            mintAccountTypeId = CInt(cboAccountType.SelectedValue)
            mintDistributionModeId = CInt(cboMode.SelectedValue)
            mdecDistributionValue = txtPercentage.Decimal
            mblnIsOkbutton = True
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOK_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        mblnIsOkbutton = False
        Me.Close()
    End Sub

#End Region

#Region " Controls "

    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objBranch As New clsbankbranch_master
        Try
            dsCombos = objBranch.getListForCombo("Branch", True, CInt(cboBankGroup.SelectedValue))
            With cboBankBranch
                .ValueMember = "branchunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Branch")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBankGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtPercentage_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPercentage.LostFocus
        Try
            txtPercentage.Text = Format(txtPercentage.Decimal, "00.00")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtPercentage_LostFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchBankGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboBankGroup.ValueMember
                .DisplayMember = cboBankGroup.DisplayMember
                .DataSource = CType(cboBankGroup.DataSource, DataTable)
                .CodeMember = "Code"
            End With
            If objfrm.DisplayDialog Then
                cboBankGroup.SelectedValue = objfrm.SelectedValue
                cboBankGroup.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBankGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchBankBranch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchBankBranch.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            With objfrm
                .ValueMember = cboBankBranch.ValueMember
                .DisplayMember = cboBankBranch.DisplayMember
                .DataSource = CType(cboBankBranch.DataSource, DataTable)
                .CodeMember = "Code"
            End With
            If objfrm.DisplayDialog Then
                cboBankBranch.SelectedValue = objfrm.SelectedValue
                cboBankBranch.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchBankBranch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub cboMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Try
            Select Case CInt(cboMode.SelectedValue)
                Case enPaymentBy.Value
                    lblPerc.Text = ""
                Case Else
                    lblPerc.Text = Language.getMessage(mstrModuleName, 24, "(%)")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriod_startdate = objPeriod._Start_Date
                mdtPeriod_enddate = objPeriod._End_Date
                mintPeriod_Status = objPeriod._Statusid
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.btnOK.GradientBackColor = GUI._ButttonBackColor
            Me.btnOK.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnOK.Text = Language._Object.getCaption(Me.btnOK.Name, Me.btnOK.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblMode.Text = Language._Object.getCaption(Me.lblMode.Name, Me.lblMode.Text)
            Me.lblPerc.Text = Language._Object.getCaption(Me.lblPerc.Name, Me.lblPerc.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblSalaryDistrib.Text = Language._Object.getCaption(Me.lblSalaryDistrib.Name, Me.lblSalaryDistrib.Text)
            Me.lblAccountType.Text = Language._Object.getCaption(Me.lblAccountType.Name, Me.lblAccountType.Text)
            Me.lblBank.Text = Language._Object.getCaption(Me.lblBank.Name, Me.lblBank.Text)
            Me.lblBankGroup.Text = Language._Object.getCaption(Me.lblBankGroup.Name, Me.lblBankGroup.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Bank Branch is compulsory information. Please select Bank Branch to continue.")
            Language.setMessage(mstrModuleName, 3, "Bank Group is compulsory information. Please select Bank Group to continue.")
            Language.setMessage(mstrModuleName, 4, "Account Type is compulsory information. Please select Account Type to continue.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Percentage should not be greater than 100.")
            Language.setMessage(mstrModuleName, 19, "Please select Salary Distribution Mode.")
            Language.setMessage(mstrModuleName, 22, "Sorry, Percentage should be greater than Zero.")
            Language.setMessage(mstrModuleName, 23, "Sorry, Amount should be greater than Zero.")
            Language.setMessage(mstrModuleName, 24, "(%)")
            Language.setMessage(mstrModuleName, 26, "Sorry, This Period is closed.")
            Language.setMessage(mstrModuleName, 27, "Please select Period.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class