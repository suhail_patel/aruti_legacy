﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmEmployeeSalaryAnniversaryMonth_List

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmFundAdjustment_List"
    Private objEmpSalaryAnniversary As clsEmployee_salary_anniversary_tran
    Dim dtTable As DataTable
    Private mstrSearchText As String = "" 'Sohail (22 Sep 2016)
#End Region

#Region " Form's Events "

    Private Sub frmEmployeeSalaryAnniversaryMonth_List_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmEmployeeSalaryAnniversaryMonth_List_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeSalaryAnniversaryMonth_List_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeSalaryAnniversaryMonth_List_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And dgvSalaryAnniversaryMonthList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeSalaryAnniversaryMonth_List_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeSalaryAnniversaryMonth_List_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployee_salary_anniversary_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployee_salary_anniversary_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeSalaryAnniversaryMonth_List_LanguageClick", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeSalaryAnniversaryMonth_List_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call FillList()
            Call SetVisibility()

            Call SetDefaultSearchText() 'Sohail (22 Sep 2016)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeSalaryAnniversaryMonth_List_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Public Sub FillCombo()
        Dim dsCombos As DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                              "Emp", True, , , , , , , , , , , , , , , , , True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim strInnerFilter As String = String.Empty
            Dim strOuterFilter As String = String.Empty
            Dim dsList As New DataSet
            objEmpSalaryAnniversary = New clsEmployee_salary_anniversary_tran

            If User._Object.Privilege._AllowToViewSalaryAnniversaryMonth = False Then Exit Sub

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strInnerFilter &= "AND hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If nudFromMonth.Value > nudToMonth.Value Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Anniversary To Month should be greater than Anniversary From Month."), enMsgBoxStyle.Information)
                nudFromMonth.Focus()
                Exit Sub
            End If

            strOuterFilter &= "AND A.anniversarymonth BETWEEN " & nudFromMonth.Value & " AND " & nudToMonth.Value & " "

            strInnerFilter &= "AND hremployee_salary_anniversary_tran.empsalaryanniversaryunkid IS NOT NULL "

            If strInnerFilter.Trim.Length > 0 Then
                strInnerFilter = strInnerFilter.Substring(3)
            End If

            If strOuterFilter.Trim.Length > 0 Then
                strOuterFilter = strOuterFilter.Substring(3)
            End If

            dsList = objEmpSalaryAnniversary.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     dtpAsOnDate.Value.Date, dtpAsOnDate.Value.Date, _
                                                     ConfigParameter._Object._UserAccessModeSetting, True, _
                                                     ConfigParameter._Object._IsIncludeInactiveEmp, "List", , strInnerFilter, strOuterFilter, "employeename", , _
                                                     CBool(chkShowAllAsOnDate.Checked))

            dtTable = dsList.Tables("List")

            dgvSalaryAnniversaryMonthList.AutoGenerateColumns = False

            dgcolhEmployeeCode.DataPropertyName = "employeecode"
            dgcolhEmployeeName.DataPropertyName = "employeename"
            dgcolhEffectiveDate.DataPropertyName = "effectivedate"
            dgcolhAnniversaryMonth.DataPropertyName = "anniversarymonth"
            dgcolhEffectiveDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhAppointDate.DataPropertyName = "appointeddate"
            dgcolhAppointDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhConfirmationDate.DataPropertyName = "confirmation_date"
            dgcolhConfirmationDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            objdgcolhEmployeeUnkId.DataPropertyName = "employeeunkid"
            objdgcolhEmpSalaryAnniversaryunkid.DataPropertyName = "empsalaryanniversaryunkid"

            dgvSalaryAnniversaryMonthList.DataSource = dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(dgvSalaryAnniversaryMonthList.RowCount.ToString)
        End Try
    End Sub

    Private Sub ResetParameters()
        Try
            cboEmployee.SelectedValue = 0
            dtpAsOnDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            nudFromMonth.Value = nudFromMonth.Minimum
            nudToMonth.Value = nudToMonth.Maximum
            chkShowAllAsOnDate.Checked = True
            Call FillList()
            Call SetDefaultSearchText() 'Sohail (22 Sep 2016)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetParameters", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddSalaryAnniversaryMonth
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteSalaryAnniversaryMonth

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    'Sohail (22 Sep 2016) -- Start
    'Issue - 63.1 - Employees were not coming in list on assign screen once annivarsary transaction deleted.
    Private Sub SetDefaultSearchText()
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 4, "Type to Search")
            With cboEmployee
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (22 Sep 2016) -- End
#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim frm As New frmEmployeeSalaryIncrementAnniversary

            If frm.displayDialog(enAction.ADD_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim blnFlag As Boolean = True
        Dim strEmpSalAnniversaryIds As String = String.Empty
        Try
            If dgvSalaryAnniversaryMonthList.RowCount <= 0 Then
                Exit Sub
            End If

            Dim selectedRows As List(Of DataGridViewRow) = (From dRow In dgvSalaryAnniversaryMonthList.Rows.Cast(Of DataGridViewRow)() Where _
                                                            CBool(dRow.Cells("objcolhSelect").Value) = True).ToList()

            If selectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please check atleast one row for further operation."), enMsgBoxStyle.Information)
                dgvSalaryAnniversaryMonthList.Focus()
                Exit Sub
            Else
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to Delete this Salary Anniversary Month?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                    objEmpSalaryAnniversary = New clsEmployee_salary_anniversary_tran

                    Dim frm As New frmReasonSelection
                    Dim mstrVoidReason As String = String.Empty
                    frm.displayDialog(enVoidCategoryType.EMPLOYEE, mstrVoidReason)
                    If mstrVoidReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objEmpSalaryAnniversary._Voidreason = mstrVoidReason
                    End If
                    frm = Nothing

                    objEmpSalaryAnniversary._Isvoid = True
                    objEmpSalaryAnniversary._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objEmpSalaryAnniversary._Voiduserunkid = User._Object._Userunkid

                    For Each dRow As DataGridViewRow In selectedRows
                        strEmpSalAnniversaryIds &= "," & dRow.Cells(objdgcolhEmpSalaryAnniversaryunkid.Index).Value.ToString
                    Next
                    If strEmpSalAnniversaryIds.Trim.Length > 0 Then
                        strEmpSalAnniversaryIds = strEmpSalAnniversaryIds.Substring(1)


                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objEmpSalaryAnniversary._FormName = mstrModuleName
                        objEmpSalaryAnniversary._LoginEmployeeUnkid = 0
                        objEmpSalaryAnniversary._ClientIP = getIP()
                        objEmpSalaryAnniversary._HostName = getHostName()
                        objEmpSalaryAnniversary._FromWeb = False
                        objEmpSalaryAnniversary._AuditUserId = User._Object._Userunkid
objEmpSalaryAnniversary._CompanyUnkid = Company._Object._Companyunkid
                        objEmpSalaryAnniversary._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        If objEmpSalaryAnniversary.VoidAll(strEmpSalAnniversaryIds) = False Then
                            If objEmpSalaryAnniversary._Message <> "" Then
                                eZeeMsgBox.Show(objEmpSalaryAnniversary._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    End If
                End If
                FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Sohail (22 Sep 2016) -- Start
    'Issue - 63.1 - Employees were not coming in list on assign screen once annivarsary transaction deleted.
    'Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim objfrm As New frmCommonSearch

    '        If User._Object._Isrighttoleft = True Then
    '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objfrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objfrm)
    '        End If

    '        With objfrm
    '            .ValueMember = cboEmployee.ValueMember
    '            .DisplayMember = cboEmployee.DisplayMember
    '            .DataSource = CType(cboEmployee.DataSource, DataTable)
    '            .CodeMember = "employeecode"
    '        End With

    '        If objfrm.DisplayDialog Then
    '            cboEmployee.SelectedValue = objfrm.SelectedValue
    '            cboEmployee.Focus()
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (22 Sep 2016) -- End

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            Call ResetParameters()
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Checkbox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If dgvSalaryAnniversaryMonthList.RowCount > 0 Then
                For Each dRow As DataGridViewRow In dgvSalaryAnniversaryMonthList.Rows
                    dRow.Cells(objcolhSelect.Index).Value = chkSelectAll.Checked
                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sohail (22 Sep 2016) -- Start
    'Issue - 63.1 - Employees were not coming in list on assign screen once annivarsary transaction deleted.
#Region " Combobox Events "
    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Try
            With cboEmployee
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"

                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    cboEmployee.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                    cboEmployee.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Call SetDefaultSearchText()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (22 Sep 2016) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblAsOnDate.Text = Language._Object.getCaption(Me.lblAsOnDate.Name, Me.lblAsOnDate.Text)
			Me.lblAnniversaryMonthFrom.Text = Language._Object.getCaption(Me.lblAnniversaryMonthFrom.Name, Me.lblAnniversaryMonthFrom.Text)
			Me.lblAnniversaryMonthTo.Text = Language._Object.getCaption(Me.lblAnniversaryMonthTo.Name, Me.lblAnniversaryMonthTo.Text)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.chkShowAllAsOnDate.Text = Language._Object.getCaption(Me.chkShowAllAsOnDate.Name, Me.chkShowAllAsOnDate.Text)
			Me.dgcolhEmployeeCode.HeaderText = Language._Object.getCaption(Me.dgcolhEmployeeCode.Name, Me.dgcolhEmployeeCode.HeaderText)
			Me.dgcolhEmployeeName.HeaderText = Language._Object.getCaption(Me.dgcolhEmployeeName.Name, Me.dgcolhEmployeeName.HeaderText)
			Me.dgcolhEffectiveDate.HeaderText = Language._Object.getCaption(Me.dgcolhEffectiveDate.Name, Me.dgcolhEffectiveDate.HeaderText)
			Me.dgcolhAnniversaryMonth.HeaderText = Language._Object.getCaption(Me.dgcolhAnniversaryMonth.Name, Me.dgcolhAnniversaryMonth.HeaderText)
			Me.dgcolhAppointDate.HeaderText = Language._Object.getCaption(Me.dgcolhAppointDate.Name, Me.dgcolhAppointDate.HeaderText)
			Me.dgcolhConfirmationDate.HeaderText = Language._Object.getCaption(Me.dgcolhConfirmationDate.Name, Me.dgcolhConfirmationDate.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Anniversary To Month should be greater than Anniversary From Month.")
			Language.setMessage(mstrModuleName, 2, "Please check atleast one row for further operation.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to Delete this Salary Anniversary Month?")
			Language.setMessage(mstrModuleName, 4, "Type to Search")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class