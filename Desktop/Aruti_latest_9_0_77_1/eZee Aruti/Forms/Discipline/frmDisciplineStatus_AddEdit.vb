﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmDisciplineStatus_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplineStatus_AddEdit"
    Private mblnCancel As Boolean = True
    Private objDisciplineStatus As clsDiscipline_Status
    Private menAction As enAction = enAction.ADD_ONE
    Private mintDisciplineStatusUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintDisciplineStatusUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintDisciplineStatusUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional

            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            nudLevel.BackColor = GUI.ColorComp
            'Anjan (20 Mar 2012)-End 

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboStatusType.BackColor = GUI.ColorComp
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objDisciplineStatus._Code
        txtName.Text = objDisciplineStatus._Name
        txtDescription.Text = objDisciplineStatus._Description

        'Anjan (20 Mar 2012)-Start
        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        nudLevel.Value = CInt(IIf(objDisciplineStatus._Level <= 0, 1, objDisciplineStatus._Level))
        'Anjan (20 Mar 2012)-End 

        'S.SANDEEP [ 20 APRIL 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        If objDisciplineStatus._Statusmodeid > 0 Then
            cboStatusType.SelectedValue = objDisciplineStatus._Statusmodeid
        End If
        'S.SANDEEP [ 20 APRIL 2012 ] -- END

    End Sub

    Private Sub SetValue()
        objDisciplineStatus._Code = txtCode.Text
        objDisciplineStatus._Name = txtName.Text
        objDisciplineStatus._Description = txtDescription.Text

        'Anjan (20 Mar 2012)-Start
        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        objDisciplineStatus._Level = CInt(nudLevel.Value)
        'Anjan (20 Mar 2012)-End 

        'S.SANDEEP [ 20 APRIL 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        objDisciplineStatus._Statusmodeid = CInt(cboStatusType.SelectedValue)
        'S.SANDEEP [ 20 APRIL 2012 ] -- END
    End Sub

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub FillCombo()
        Dim dsList As New DataSet
        Try
            dsList = objDisciplineStatus.getStatusTypeComboList("List")
            With cboStatusType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = clsDiscipline_Status.enDisciplineStatusType.BOTH
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmDisciplineStatus_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objDisciplineStatus = Nothing
    End Sub

    Private Sub frmDisciplineStatus_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineType_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineStatus_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineStatus_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineStatus_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDisciplineStatus = New clsDiscipline_Status
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call setColor()

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call FillCombo()
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            If menAction = enAction.EDIT_ONE Then
                objDisciplineStatus._Disciplinestatusunkid = mintDisciplineStatusUnkid
                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If mintDisciplineStatusUnkid <= 3 Then
                    cboStatusType.Enabled = False
                End If
                'S.SANDEEP [ 20 APRIL 2012 ] -- END
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineStatus_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_Status.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_Status"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Discipline Status cannot be blank. Discipline Status is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Sub
            End If

            If CInt(nudLevel.Value) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Level can not be less than 1."), enMsgBoxStyle.Information)
                nudLevel.Focus()
                Exit Sub
            End If

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If CInt(cboStatusType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Status Type is mandatory information. Please select Status Type to continue."), enMsgBoxStyle.Information)
                cboStatusType.Focus()
                Exit Sub
            End If
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objDisciplineStatus._FormName = mstrModuleName
            objDisciplineStatus._LoginEmployeeunkid = 0
            objDisciplineStatus._ClientIP = getIP()
            objDisciplineStatus._HostName = getHostName()
            objDisciplineStatus._FromWeb = False
            objDisciplineStatus._AuditUserId = User._Object._Userunkid
objDisciplineStatus._CompanyUnkid = Company._Object._Companyunkid
            objDisciplineStatus._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objDisciplineStatus.Update()
            Else
                blnFlag = objDisciplineStatus.Insert()
            End If

            If blnFlag = False And objDisciplineStatus._Message <> "" Then
                eZeeMsgBox.Show(objDisciplineStatus._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objDisciplineStatus = Nothing
                    objDisciplineStatus = New clsDiscipline_Status
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintDisciplineStatusUnkid = objDisciplineStatus._Disciplinestatusunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call objFrm.displayDialog(txtName.Text, objDisciplineStatus._Name1, objDisciplineStatus._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbAccessInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAccessInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbAccessInfo.Text = Language._Object.getCaption(Me.gbAccessInfo.Name, Me.gbAccessInfo.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblAccCode.Text = Language._Object.getCaption(Me.lblAccCode.Name, Me.lblAccCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
			Me.lblLevelNotify.Text = Language._Object.getCaption(Me.lblLevelNotify.Name, Me.lblLevelNotify.Text)
			Me.lblStatusType.Text = Language._Object.getCaption(Me.lblStatusType.Name, Me.lblStatusType.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Discipline Status cannot be blank. Discipline Status is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Level can not be less than 1.")
			Language.setMessage(mstrModuleName, 4, "Status Type is mandatory information. Please select Status Type to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class