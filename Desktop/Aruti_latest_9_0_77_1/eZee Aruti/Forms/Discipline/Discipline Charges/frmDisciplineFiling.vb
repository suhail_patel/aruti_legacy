﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmDisciplineFiling

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplineFiling"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintDisciplineFileUnkid As Integer = -1
    Private objDiscChargeMaster As clsDiscipline_file_master
    Private objDiscChargeTran As clsDiscipline_file_tran
    Private mdtCharges As DataTable
    Private mdvCharges As DataView
    Private mintSeverity As Integer = 0
    Private mdtScanAttachment As DataTable = Nothing
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintDisciplineFileUnkid = intUnkId
            menAction = eAction

            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            lblInterdictionDate.Visible = ConfigParameter._Object._ShowInterdictionDate
            dtpInterdictiondate.Visible = ConfigParameter._Object._ShowInterdictionDate
            chkNotifyEmployee.Visible = ConfigParameter._Object._NotificationOnDisciplineFiling
            'S.SANDEEP |01-OCT-2019| -- END

            Me.ShowDialog()

            intUnkId = mintDisciplineFileUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtDepartment.BackColor = GUI.ColorOptional
            txtChargeDescription.BackColor = GUI.ColorComp
            txtJobTitle.BackColor = GUI.ColorOptional
            txtReferenceNo.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If objDiscChargeMaster._Chargedate <> Nothing Then
                dtpDisciplineDate.Value = objDiscChargeMaster._Chargedate
            End If
            If objDiscChargeMaster._Interdictiondate <> Nothing Then
                dtpInterdictiondate.Value = objDiscChargeMaster._Interdictiondate
                dtpInterdictiondate.Checked = True
            End If
            txtChargeDescription.Text = objDiscChargeMaster._Charge_Description
            txtReferenceNo.Text = objDiscChargeMaster._Reference_No
            cboEmployee.SelectedValue = objDiscChargeMaster._Involved_Employeeunkid
            chkNotifyEmployee.Checked = objDiscChargeMaster._NotifyEmployee
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            chkIsChargeVisibleOnEss.Checked = objDiscChargeMaster._IsVisibleOnEss
            'S.SANDEEP |11-NOV-2019| -- END



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objDiscChargeMaster._Chargedate = dtpDisciplineDate.Value

            If dtpInterdictiondate.Checked = True Then
                objDiscChargeMaster._Interdictiondate = dtpInterdictiondate.Value
            End If
            objDiscChargeMaster._Charge_Description = txtChargeDescription.Text
            objDiscChargeMaster._Reference_No = txtReferenceNo.Text
            objDiscChargeMaster._Involved_Employeeunkid = CInt(cboEmployee.SelectedValue)
            objDiscChargeMaster._NotifyEmployee = chkNotifyEmployee.Checked
            objDiscChargeMaster._Userunkid = User._Object._Userunkid
            If menAction = enAction.EDIT_ONE Then
                objDiscChargeMaster._Disciplinestatusunkid = objDiscChargeMaster._Disciplinestatusunkid
            Else
                objDiscChargeMaster._Disciplinestatusunkid = 2
            End If
            objDiscChargeMaster._LoginTypeId = enLogin_Mode.DESKTOP
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            objDiscChargeMaster._IsVisibleOnEss = chkIsChargeVisibleOnEss.Checked
            'S.SANDEEP |11-NOV-2019| -- END

            'S.SANDEEP |02-MAR-2022| -- START
            objDiscChargeMaster._Postuserunkid = User._Object._Userunkid
            'S.SANDEEP |02-MAR-2022| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
                .Text = ""
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objEmp = Nothing
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Person Involved is compulsory information.Please Select Person Involved."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Return False
            End If

            If dtpInterdictiondate.Checked = True Then
                Dim mdtOrgDate As Date = Nothing
                If objDiscChargeMaster.IsInterdictionDateExists(dtpInterdictiondate.Value, txtReferenceNo.Text, mdtOrgDate) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot set this interdiction date. Reason : For this Reference No Interdiction date is already set to as :") & " (" & mdtOrgDate & ")", enMsgBoxStyle.Information)
                    dtpInterdictiondate.Focus()
                    Return False
                End If

                If dtpInterdictiondate.Value.Date < dtpDisciplineDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, Interdiction date cannot be less than the charge date. Please set correct interdiction date to continue."), enMsgBoxStyle.Information)
                    dtpInterdictiondate.Focus()
                    Return False
                End If
            End If

            If txtChargeDescription.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Charge description is mandatory information. Charge description cannot be blank."), enMsgBoxStyle.Information)
                txtChargeDescription.Focus()
                Return False
            End If

            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            If menAction = enAction.EDIT_ONE AndAlso mintDisciplineFileUnkid > 0 Then
                If objDiscChargeMaster._IsVisibleOnEss <> chkIsChargeVisibleOnEss.Checked Then
                    Dim sMsg As String = String.Empty
                    sMsg = objDiscChargeTran.IsValidOperation(mintDisciplineFileUnkid)
                    If sMsg.Trim.Length > 0 Then
                        eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
            End If
            'S.SANDEEP |11-NOV-2019| -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub FillCharges_Tran()
        Try
            mdvCharges = mdtCharges.DefaultView
            mdvCharges.RowFilter = "AUD <> 'D'"

            dgvData.AutoGenerateColumns = False
            dgcolhIncident.DataPropertyName = "incident_description"
            dgcolhOffCategory.DataPropertyName = "charge_category"
            dgcolhOffence.DataPropertyName = "charge_descr"
            dgcolhSeverity.DataPropertyName = "charge_severity"
            objdgcolhoffencecategoryunkid.DataPropertyName = "offencecategoryunkid"
            objdgcolhoffenceunkid.DataPropertyName = "offenceunkid"
            objdgcolhGUID.DataPropertyName = "GUID"
            objdgcolhfileunkid.DataPropertyName = "disciplinefileunkid"
            objdgcolhfiletraunkid.DataPropertyName = "disciplinefiletranunkid"
            dgcolhResponseDate.DataPropertyName = "response_date"
            dgcolhResponseDate.DefaultCellStyle.Format = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhResponseText.DataPropertyName = "response_remark"
            dgcolhResponseType.DataPropertyName = "response_type"

            dgvData.DataSource = mdvCharges

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCharges_Tran", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Add_DataRow(ByVal dr As DataRow, ByVal intRowIdx As Integer, ByVal strScreenName As String) As Boolean
        Try
            Dim xRow As DataRow() = Nothing
            If CInt(dgvData.Rows(intRowIdx).Cells(objdgcolhfiletraunkid.Index).Value) > 0 Then
                xRow = mdtScanAttachment.Select("scanattachrefid = '" & enScanAttactRefId.DISCIPLINES & "' AND transactionunkid = '" & CInt(dgvData.Rows(intRowIdx).Cells(objdgcolhfiletraunkid.Index).Value) & "' AND form_name = '" & strScreenName & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            ElseIf CStr(dgvData.Rows(intRowIdx).Cells(objdgcolhGUID.Index).Value).Trim.Length > 0 Then
                xRow = mdtScanAttachment.Select("scanattachrefid = '" & enScanAttactRefId.DISCIPLINES & "' AND GUID = '" & CStr(dgvData.Rows(intRowIdx).Cells(objdgcolhGUID.Index).Value) & "' AND form_name = '" & strScreenName & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            End If
            If xRow.Length <= 0 Then
                mdtScanAttachment.ImportRow(dr)
            Else
                mdtScanAttachment.Rows.Remove(xRow(0))
                mdtScanAttachment.ImportRow(dr)
            End If
            mdtScanAttachment.AcceptChanges()
            'S.SANDEEP [10 SEP 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Add_DataRow", mstrModuleName)
        Finally
        End Try
        Return True
    End Function


#End Region

#Region " Form's Events "

    Private Sub frmDisciplineFiling_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDiscChargeMaster = New clsDiscipline_file_master
        objDiscChargeTran = New clsDiscipline_file_tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            If ConfigParameter._Object._DisciplineRefNoType = 1 Then
                txtReferenceNo.Enabled = False
            Else
                txtReferenceNo.Enabled = True
            End If

            'S.SANDEEP |13-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : DISCIPLINARY CHANGES
            If ConfigParameter._Object._AllowtocloseCaseWO_Proceedings Then
                btnSaveClose.Visible = True
            Else
                btnSaveClose.Visible = False
            End If
            'S.SANDEEP |13-MAY-2021| -- END

            SetColor()

            'Pinkal (19-Dec-2020) -- Start
            'Enhancement  -  Working on Discipline module for NMB.
            chkIsChargeVisibleOnEss.Visible = ConfigParameter._Object._AllowToViewESSVisibleWhileFilligDisciplineCharge
            'Pinkal (19-Dec-2020) -- End    

            If menAction = enAction.EDIT_ONE Then
                objDiscChargeMaster._Disciplinefileunkid = mintDisciplineFileUnkid
                chkNotifyEmployee.Enabled = False
            End If

            Call FillCombo()
            Call GetValue()

            objDiscChargeTran._Disciplinefileunkid = mintDisciplineFileUnkid
            mdtCharges = objDiscChargeTran._ChargesTable
            Call FillCharges_Tran()

            Dim objScanAttachment As New clsScan_Attach_Documents
            'S.SANDEEP |04-SEP-2021| -- START
            'objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
            objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), , Nothing, Nothing, "", CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
            'S.SANDEEP |04-SEP-2021| -- END

            Dim xRow As DataRow() = objScanAttachment._Datatable.Select("scanattachrefid = '" & enScanAttactRefId.DISCIPLINES & "'") ' AND form_name = '" & strfrmName & "'")
            'AsEnumerable().Where(Function(x) x.Field(Of Integer)("scanattachrefid") = enScanAttactRefId.DISCIPLINES And x.Field(Of String)("form_name") = "'" & frmAddEditCounts.Name & "'")
            If xRow.Count > 0 Then
                mdtScanAttachment = xRow.CopyToDataTable()
            Else
                mdtScanAttachment = objScanAttachment._Datatable.Clone
            End If
            objScanAttachment = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineFiling_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmDisciplineFiling_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineFiling_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineAnalysis_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objDiscChargeMaster = Nothing
        objDiscChargeTran = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_file_master.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_file_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    'S.SANDEEP |13-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : DISCIPLINARY CHANGES
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSaveClose.Click
        'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'S.SANDEEP |13-MAY-2021| -- END    
        Dim blnFlag As Boolean = False
        Dim mstrFolderName As String = ""
        Dim strError As String = ""
        Try
            If Validation() = False Then Exit Sub
            Call SetValue()

            'S.SANDEEP |13-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : DISCIPLINARY CHANGES
            Dim mblnIsSaveClose As Boolean = False
            If CType(sender, eZee.Common.eZeeLightButton).Name = btnSaveClose.Name Then
                objDiscChargeMaster._Disciplinestatusunkid = 1 'CASE CLOSED
                mblnIsSaveClose = True
            End If        
            'S.SANDEEP |13-MAY-2021| -- END


            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
            Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.DISCIPLINES) Select (p.Item("Name").ToString)).FirstOrDefault
            If mdtScanAttachment IsNot Nothing Then 'S.SANDEEP |16-JAN-2020| -- START { ISSUE/ENHANCEMENT : OBJECT REFERENCE ERROR (mdtScanAttachment) } -- END
            For Each dRow As DataRow In mdtScanAttachment.Rows
                If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                    Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                    Dim intScanAttachRefId As Integer = CInt(dRow("scanattachrefid"))

                    If blnIsIISInstalled Then
                        If clsFileUploadDownload.UploadFile(CStr(dRow("orgfilepath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                            dRow("fileuniquename") = strFileName
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                            dRow("filepath") = strPath
                            dRow.AcceptChanges()
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                            End If
                            File.Copy(CStr(dRow("orgfilepath")), strDocLocalPath, True)
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strDocLocalPath
                            dRow.AcceptChanges()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                ElseIf dRow("AUD").ToString = "U" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFileName As String = dRow("fileuniquename").ToString
                    Dim intScanattachrefid As Integer = CInt(dRow("scanattachrefid"))

                    If blnIsIISInstalled Then
                        Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then strPath += "/"
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If clsFileUploadDownload.UpdateFile(CStr(dRow("filepath")), strPath, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                            dRow("filepath") = strPath
                            dRow.AcceptChanges()
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                            End If
                            File.Move(CStr(dRow("filepath")), strDocLocalPath)

                            dRow("filepath") = strDocLocalPath
                            dRow.AcceptChanges()
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFileName As String = dRow("fileuniquename").ToString
                    If blnIsIISInstalled Then
                        'Hemant [8-April-2019] -- Start
                        'If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                        If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                            'Hemant [8-April-2019] -- End
                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Else
                        If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                            Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                            If File.Exists(strDocLocalPath) Then
                                File.Delete(strDocLocalPath)
                            End If
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                End If
            Next
            End If

            Dim lstName As New List(Of String)(New String() {mstrModuleName})
            If menAction = enAction.EDIT_ONE Then
                blnFlag = objDiscChargeMaster.Update(ConfigParameter._Object._CurrentDateAndTime, _
                                                     mdtScanAttachment, mstrModuleName, Company._Object._Companyunkid, _
                                                     ConfigParameter._Object._Document_Path, lstName, _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, mdtCharges, False, "", _
                                                     mblnIsSaveClose)
            Else
                blnFlag = objDiscChargeMaster.Insert(ConfigParameter._Object._CurrentDateAndTime, _
                                                     ConfigParameter._Object._DisciplineRefNoType, _
                                                     ConfigParameter._Object._DisciplineRefPrefix, _
                                                     Company._Object._Companyunkid, mdtScanAttachment, _
                                                     ConfigParameter._Object._Document_Path, lstName, _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     FinancialYear._Object._DatabaseName, mstrModuleName, mdtCharges)
            End If

            If blnFlag = False AndAlso objDiscChargeMaster._Message <> "" Then
                eZeeMsgBox.Show(objDiscChargeMaster._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objDiscChargeMaster = Nothing : objDiscChargeTran = Nothing
                    objDiscChargeMaster = New clsDiscipline_file_master
                    objDiscChargeTran = New clsDiscipline_file_tran
                    Call GetValue()
                    objDiscChargeTran._Disciplinefileunkid = -1
                    mdtCharges = objDiscChargeTran._ChargesTable
                    dtpDisciplineDate.Select()
                Else
                    mintDisciplineFileUnkid = objDiscChargeMaster._Disciplinefileunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                If .DisplayDialog Then
                    cboEmployee.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnScanAttachDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnScanAttachDoc.Click
        Dim frm As New frmScanOrAttachmentInfo
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If CInt(cboEmployee.SelectedValue) <= 0 Then Exit Sub
            frm._TransactionID = mintDisciplineFileUnkid


            'Pinkal (08-May-2021)-- Start
            'NMB Discipline Enhancement  -  Working on Discipline Enhancement for NMB.
            'frm._dtAttachment = mdtScanAttachment.Copy
            Dim dtTable As DataTable = Nothing
            Dim xRow = mdtScanAttachment.Select("scanattachrefid = '" & enScanAttactRefId.DISCIPLINES & "' AND transactionunkid = '" & mintDisciplineFileUnkid & "' AND form_name = '" & mstrModuleName & "' ")
            If xRow.Count > 0 Then
                frm._dtAttachment = xRow.ToList().CopyToDataTable()
            Else
                frm._dtAttachment = mdtScanAttachment.Clone
            End If
            'Pinkal (08-May-2021)-- End

            'S.SANDEEP |26-APR-2019| -- START
            'If frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), _
            '                     enImg_Email_RefId.Discipline_Module, enAction.ADD_ONE, "", mstrModuleName, CInt(cboEmployee.SelectedValue), enScanAttactRefId.DISCIPLINES, True, False) = True Then 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END



            If frm.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), _
                                 enImg_Email_RefId.Discipline_Module, enAction.ADD_ONE, "", mstrModuleName, True, CInt(cboEmployee.SelectedValue), enScanAttactRefId.DISCIPLINES, True, False) = True Then 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
                'S.SANDEEP |26-APR-2019| -- END

                'Pinkal (08-May-2021)-- Start
                'NMB Discipline Enhancement  -  Working on Discipline Enhancement for NMB.
                'mdtScanAttachment = frm._dtAttachment.Copy
                mdtScanAttachment.Merge(frm._dtAttachment.Copy(), True)
                'Pinkal (08-May-2021)-- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnScanAttachDoc_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                Dim objDept As New clsDepartment
                Dim objJobs As New clsJobs
                objDept._Departmentunkid = objEmp._Departmentunkid
                objJobs._Jobunkid = objEmp._Jobunkid
                txtDepartment.Text = objDept._Name
                txtJobTitle.Text = objJobs._Job_Name
                objDept = Nothing : objJobs = Nothing : objEmp = Nothing
            Else
                txtDepartment.Text = "" : txtJobTitle.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgvData_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellClick
        Dim frm As New frmAddEditCounts
        Try
            If e.RowIndex <= -1 Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Select Case e.ColumnIndex
                Case objdgcolhEdit.Index
                    If dgvData.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).Value Is imgBlank Then Exit Sub

                    If frm.displayDialog(CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value), _
                                         enAction.EDIT_ONE, _
                                         CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value), _
                                         CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfileunkid.Index).Value), dtpDisciplineDate.Value.Date, mdtCharges, CInt(cboEmployee.SelectedValue), mdtScanAttachment) Then
                        Call FillCharges_Tran()
                    End If

                Case objdgcolhDelete.Index
                    If dgvData.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).Value Is imgBlank Then Exit Sub

                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Are you sure you want to delete this charge count?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                        Dim frm1 As New frmReasonSelection
                        If User._Object._Isrighttoleft = True Then
                            frm1.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm1.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If
                        Dim mstrVoidReason As String = String.Empty

                        If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value) > 0 Then
                            Dim dRow() As DataRow = mdtCharges.Select("disciplinefiletranunkid = " & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value) & " AND AUD <> 'D'")
                            If dRow.Count > 0 Then
                                frm1.displayDialog(enVoidCategoryType.DISCIPLINE, mstrVoidReason)
                                If mstrVoidReason.Length <= 0 Then Exit Sub
                                dRow(0).Item("isvoid") = True
                                dRow(0).Item("voiduserunkid") = User._Object._Userunkid
                                dRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                                dRow(0).Item("voidreason") = mstrVoidReason
                                dRow(0).Item("AUD") = "D"
                            End If
                        Else
                            Dim dRow() As DataRow = mdtCharges.Select("GUID = '" & CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value) & "' AND AUD <> 'D'")
                            If dRow.Count > 0 Then
                                'dRow(0).Item("isvoid") = True
                                'dRow(0).Item("voiduserunkid") = User._Object._Userunkid
                                'dRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                                'dRow(0).Item("voidreason") = mstrVoidReason
                                'dRow(0).Item("AUD") = "D"
                                mdtCharges.Rows.Remove(dRow(0))
                            End If
                        End If
                    End If
                Case objdgcolhAttachment.Index
                    If dgvData.Rows(e.RowIndex).Cells(objdgcolhAttachment.Index).Value Is imgBlank Then Exit Sub

                    Dim frmSA As New frmScanOrAttachmentInfo
                    If User._Object._Isrighttoleft = True Then
                        frmSA.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frmSA.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frmSA)
                    End If

                    If CInt(cboEmployee.SelectedValue) <= 0 Then Exit Sub
                    frmSA._TransactionID = CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value)
                    frmSA._TransactionGuidString = CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value)

                    'Shani (20-Aug-2016) -- Start
                    'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
                    frmSA._TransactionMstUnkId = mintDisciplineFileUnkid
                    'Shani (20-Aug-2016) -- End
                    Dim dtTable As DataTable = Nothing
                    Dim objScanAttachment As New clsScan_Attach_Documents
                    Dim xRow As DataRow() = Nothing
                    If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value) > 0 Then
                        xRow = mdtScanAttachment.Select("scanattachrefid = '" & enScanAttactRefId.DISCIPLINES & "' AND transactionunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value) & "' AND form_name = '" & mstrModuleName & "' ")
                        Dim drcnt As DataRow() = mdtCharges.Select("disciplinefiletranunkid = '" & CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhfiletraunkid.Index).Value) & "' AND AUD <> 'D'")
                        If drcnt.Length > 0 Then
                            drcnt(0).Item("AUD") = "U"
                        End If
                    ElseIf CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value).Trim.Length > 0 Then
                        xRow = mdtScanAttachment.Select("scanattachrefid = '" & enScanAttactRefId.DISCIPLINES & "' AND GUID = '" & CStr(dgvData.Rows(e.RowIndex).Cells(objdgcolhGUID.Index).Value) & "' AND form_name = '" & mstrModuleName & "' ")
                    End If
                    If xRow.Count > 0 Then
                        dtTable = xRow.CopyToDataTable
                    Else
                        dtTable = objScanAttachment._Datatable.Clone
                    End If

                    frmSA._dtAttachment = dtTable.Copy

                    'S.SANDEEP |26-APR-2019| -- START
                    'If frmSA.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), _
                    '                                 enImg_Email_RefId.Discipline_Module, enAction.ADD_ONE, "", mstrModuleName, CInt(cboEmployee.SelectedValue), enScanAttactRefId.DISCIPLINES, True, False) = True Then
                    If frmSA.displayDialog(Language.getMessage(mstrModuleName, 3, "Select Employee"), _
                                                     enImg_Email_RefId.Discipline_Module, enAction.ADD_ONE, "", mstrModuleName, True, CInt(cboEmployee.SelectedValue), enScanAttactRefId.DISCIPLINES, True, False) = True Then
                        'S.SANDEEP |26-APR-2019| -- END
                        dtTable = frmSA._dtAttachment.Copy
                        frmSA._dtAttachment.Rows.Clear()

                        Dim dRows As DataRow() = dtTable.Select("")
                        dRows.ToList.ForEach(Function(x) Add_DataRow(x, e.RowIndex, mstrModuleName))
                        'mdtScanAttachment.Merge(dtTable)
                    End If

                    If frmSA IsNot Nothing Then frmSA.Dispose()
                Case dgcolhR_Docs.Index

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellClick", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub dgvData_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgvData.Scroll
        Try
            If e.OldValue <> e.NewValue Then
                dgvData.PerformLayout()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_Scroll", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_RowPostPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowPostPaintEventArgs) Handles dgvData.RowPostPaint
        Try
            Dim rect As Rectangle = dgvData.GetCellDisplayRectangle(dgcolhCount.Index, e.RowIndex, True)
            Dim strRowNumber As String = (e.RowIndex + 1).ToString()
            Dim size As SizeF = e.Graphics.MeasureString(strRowNumber, Me.Font)
            If dgvData.RowHeadersWidth < CInt(size.Width + 20) Then
                dgvData.RowHeadersWidth = CInt(size.Width + 20)
            End If
            Dim b As Brush = SystemBrushes.ControlText
            e.Graphics.DrawString(strRowNumber, Me.Font, b, rect.Location.X + 15, rect.Location.Y + ((rect.Height - size.Height) / 2))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_RowPostPaint", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
        Try
            For Each dgvRow As DataGridViewRow In dgvData.Rows
                If dgvRow.Cells(dgcolhResponseDate.Index).Value.ToString.Trim.Length <= 0 Then
                    CType(dgvRow.Cells(dgcolhR_Docs.Index), DataGridViewLinkCell).ActiveLinkColor = Color.Gray
                    CType(dgvRow.Cells(dgcolhR_Docs.Index), DataGridViewLinkCell).VisitedLinkColor = Color.Gray
                    CType(dgvRow.Cells(dgcolhR_Docs.Index), DataGridViewLinkCell).LinkColor = Color.Gray
                End If

                If objDiscChargeMaster._IsFinal = True Then
                    objdgcolhEdit.Visible = False
                    objdgcolhDelete.Visible = False
                    objdgcolhAttachment.Visible = False
                End If

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkAddCount_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAddCount.LinkClicked
        Dim frm As New frmAddEditCounts
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(-1, enAction.ADD_CONTINUE, "", mintDisciplineFileUnkid, dtpDisciplineDate.Value.Date, mdtCharges, CInt(cboEmployee.SelectedValue), mdtScanAttachment) Then
                Call FillCharges_Tran()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAddCount_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbDiscipilneFiling.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDiscipilneFiling.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnScanAttachDoc.GradientBackColor = GUI._ButttonBackColor
            Me.btnScanAttachDoc.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbDiscipilneFiling.Text = Language._Object.getCaption(Me.gbDiscipilneFiling.Name, Me.gbDiscipilneFiling.Text)
            Me.lblInterdictionDate.Text = Language._Object.getCaption(Me.lblInterdictionDate.Name, Me.lblInterdictionDate.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblJobTitle.Text = Language._Object.getCaption(Me.lblJobTitle.Name, Me.lblJobTitle.Text)
            Me.chkNotifyEmployee.Text = Language._Object.getCaption(Me.chkNotifyEmployee.Name, Me.chkNotifyEmployee.Text)
            Me.btnScanAttachDoc.Text = Language._Object.getCaption(Me.btnScanAttachDoc.Name, Me.btnScanAttachDoc.Text)
            Me.lblGeneralChargeDescr.Text = Language._Object.getCaption(Me.lblGeneralChargeDescr.Name, Me.lblGeneralChargeDescr.Text)
            Me.elLine1.Text = Language._Object.getCaption(Me.elLine1.Name, Me.elLine1.Text)
            Me.lnkAddCount.Text = Language._Object.getCaption(Me.lnkAddCount.Name, Me.lnkAddCount.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.dgcolhR_Docs.HeaderText = Language._Object.getCaption(Me.dgcolhR_Docs.Name, Me.dgcolhR_Docs.HeaderText)
            Me.dgcolhCount.HeaderText = Language._Object.getCaption(Me.dgcolhCount.Name, Me.dgcolhCount.HeaderText)
            Me.dgcolhOffCategory.HeaderText = Language._Object.getCaption(Me.dgcolhOffCategory.Name, Me.dgcolhOffCategory.HeaderText)
            Me.dgcolhOffence.HeaderText = Language._Object.getCaption(Me.dgcolhOffence.Name, Me.dgcolhOffence.HeaderText)
            Me.dgcolhIncident.HeaderText = Language._Object.getCaption(Me.dgcolhIncident.Name, Me.dgcolhIncident.HeaderText)
            Me.dgcolhSeverity.HeaderText = Language._Object.getCaption(Me.dgcolhSeverity.Name, Me.dgcolhSeverity.HeaderText)
            Me.dgcolhResponseType.HeaderText = Language._Object.getCaption(Me.dgcolhResponseType.Name, Me.dgcolhResponseType.HeaderText)
            Me.dgcolhResponseDate.HeaderText = Language._Object.getCaption(Me.dgcolhResponseDate.Name, Me.dgcolhResponseDate.HeaderText)
            Me.dgcolhResponseText.HeaderText = Language._Object.getCaption(Me.dgcolhResponseText.Name, Me.dgcolhResponseText.HeaderText)
            Me.chkIsChargeVisibleOnEss.Text = Language._Object.getCaption(Me.chkIsChargeVisibleOnEss.Name, Me.chkIsChargeVisibleOnEss.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Person Involved is compulsory information.Please Select Person Involved.")
            Language.setMessage(mstrModuleName, 3, "Select Employee")
            Language.setMessage(mstrModuleName, 4, "Sorry, you cannot set this interdiction date. Reason : For this Reference No Interdiction date is already set to as :")
            Language.setMessage(mstrModuleName, 12, "Configuration Path does not Exist.")
            Language.setMessage(mstrModuleName, 13, "Sorry, Charge description is mandatory information. Charge description cannot be blank.")
            Language.setMessage(mstrModuleName, 14, "Sorry, Interdiction date cannot be less than the charge date. Please set correct interdiction date to continue.")
            Language.setMessage(mstrModuleName, 15, "Are you sure you want to delete this charge count?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class