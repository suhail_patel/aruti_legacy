﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisciplineFilingList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisciplineFilingList))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dgvChargesList = New System.Windows.Forms.DataGridView
        Me.objemailFooter = New eZee.Common.eZeeFooter
        Me.btnOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboCategory = New System.Windows.Forms.ComboBox
        Me.lblCategory = New System.Windows.Forms.Label
        Me.cboChargeStatus = New System.Windows.Forms.ComboBox
        Me.lblChargeStatus = New System.Windows.Forms.Label
        Me.dtpChargeDtTo = New System.Windows.Forms.DateTimePicker
        Me.txtReferenceNo = New eZee.TextBox.AlphanumericTextBox
        Me.cboResponseType = New System.Windows.Forms.ComboBox
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.lblResponseType = New System.Windows.Forms.Label
        Me.lblChgDtTo = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.dtpChargeDtFrom = New System.Windows.Forms.DateTimePicker
        Me.lblChgDtFrom = New System.Windows.Forms.Label
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.mnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuAddEditResponse = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep5 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuSendNotification = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuScheduleHearing = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep7 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuSubmitProceedingsforApproval = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep2 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuInReopen = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExReopen = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep4 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuDisciplineCharge = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChargeDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInterdictionDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInvolved = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhGeneralDesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNotificationDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhFirstHearingDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNextHearingDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhChargeStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhDiscFileunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmailAddress = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCategoryId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStatusId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsFinal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        CType(Me.dgvChargesList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objemailFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.mnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objchkAll)
        Me.pnlMain.Controls.Add(Me.dgvChargesList)
        Me.pnlMain.Controls.Add(Me.objemailFooter)
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(944, 572)
        Me.pnlMain.TabIndex = 0
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(9, 108)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 8
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'dgvChargesList
        '
        Me.dgvChargesList.AllowUserToAddRows = False
        Me.dgvChargesList.AllowUserToDeleteRows = False
        Me.dgvChargesList.AllowUserToResizeRows = False
        Me.dgvChargesList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvChargesList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvChargesList.ColumnHeadersHeight = 35
        Me.dgvChargesList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvChargesList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhRefNo, Me.dgcolhChargeDate, Me.dgcolhInterdictionDate, Me.dgcolhInvolved, Me.dgcolhGeneralDesc, Me.dgcolhCount, Me.dgcolhNotificationDate, Me.dgcolhFirstHearingDate, Me.dgcolhNextHearingDate, Me.dgcolhChargeStatus, Me.dgcolhCategory, Me.objdgcolhDiscFileunkid, Me.objdgcolhEmailAddress, Me.objdgcolhEmpId, Me.objdgcolhCategoryId, Me.objdgcolhStatusId, Me.objdgcolhIsFinal})
        Me.dgvChargesList.Location = New System.Drawing.Point(2, 97)
        Me.dgvChargesList.MultiSelect = False
        Me.dgvChargesList.Name = "dgvChargesList"
        Me.dgvChargesList.RowHeadersVisible = False
        Me.dgvChargesList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvChargesList.Size = New System.Drawing.Size(940, 422)
        Me.dgvChargesList.TabIndex = 89
        '
        'objemailFooter
        '
        Me.objemailFooter.BorderColor = System.Drawing.Color.Silver
        Me.objemailFooter.Controls.Add(Me.btnOk)
        Me.objemailFooter.Controls.Add(Me.btnEClose)
        Me.objemailFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objemailFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objemailFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objemailFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objemailFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objemailFooter.Location = New System.Drawing.Point(0, 472)
        Me.objemailFooter.Name = "objemailFooter"
        Me.objemailFooter.Size = New System.Drawing.Size(944, 50)
        Me.objemailFooter.TabIndex = 88
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.BackColor = System.Drawing.Color.White
        Me.btnOk.BackgroundImage = CType(resources.GetObject("btnOk.BackgroundImage"), System.Drawing.Image)
        Me.btnOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOk.BorderColor = System.Drawing.Color.Empty
        Me.btnOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOk.FlatAppearance.BorderSize = 0
        Me.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.ForeColor = System.Drawing.Color.Black
        Me.btnOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Location = New System.Drawing.Point(732, 10)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOk.Size = New System.Drawing.Size(97, 30)
        Me.btnOk.TabIndex = 17
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnEClose
        '
        Me.btnEClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEClose.BackColor = System.Drawing.Color.White
        Me.btnEClose.BackgroundImage = CType(resources.GetObject("btnEClose.BackgroundImage"), System.Drawing.Image)
        Me.btnEClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEClose.BorderColor = System.Drawing.Color.Empty
        Me.btnEClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEClose.FlatAppearance.BorderSize = 0
        Me.btnEClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEClose.ForeColor = System.Drawing.Color.Black
        Me.btnEClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Location = New System.Drawing.Point(835, 10)
        Me.btnEClose.Name = "btnEClose"
        Me.btnEClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Size = New System.Drawing.Size(97, 30)
        Me.btnEClose.TabIndex = 16
        Me.btnEClose.Text = "&Close"
        Me.btnEClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblCategory)
        Me.gbFilterCriteria.Controls.Add(Me.cboChargeStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblChargeStatus)
        Me.gbFilterCriteria.Controls.Add(Me.dtpChargeDtTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtReferenceNo)
        Me.gbFilterCriteria.Controls.Add(Me.cboResponseType)
        Me.gbFilterCriteria.Controls.Add(Me.lblRefNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblResponseType)
        Me.gbFilterCriteria.Controls.Add(Me.lblChgDtTo)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.dtpChargeDtFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblChgDtFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblPersonalInvolved)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(2, 2)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(940, 94)
        Me.gbFilterCriteria.TabIndex = 6
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCategory
        '
        Me.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCategory.DropDownWidth = 300
        Me.cboCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCategory.FormattingEnabled = True
        Me.cboCategory.Location = New System.Drawing.Point(820, 61)
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(110, 21)
        Me.cboCategory.TabIndex = 129
        '
        'lblCategory
        '
        Me.lblCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategory.Location = New System.Drawing.Point(729, 63)
        Me.lblCategory.Name = "lblCategory"
        Me.lblCategory.Size = New System.Drawing.Size(85, 17)
        Me.lblCategory.TabIndex = 128
        Me.lblCategory.Text = "Category"
        Me.lblCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboChargeStatus
        '
        Me.cboChargeStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChargeStatus.DropDownWidth = 300
        Me.cboChargeStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChargeStatus.FormattingEnabled = True
        Me.cboChargeStatus.Location = New System.Drawing.Point(820, 34)
        Me.cboChargeStatus.Name = "cboChargeStatus"
        Me.cboChargeStatus.Size = New System.Drawing.Size(110, 21)
        Me.cboChargeStatus.TabIndex = 127
        '
        'lblChargeStatus
        '
        Me.lblChargeStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChargeStatus.Location = New System.Drawing.Point(729, 36)
        Me.lblChargeStatus.Name = "lblChargeStatus"
        Me.lblChargeStatus.Size = New System.Drawing.Size(85, 17)
        Me.lblChargeStatus.TabIndex = 126
        Me.lblChargeStatus.Text = "Charge Status"
        Me.lblChargeStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpChargeDtTo
        '
        Me.dtpChargeDtTo.Checked = False
        Me.dtpChargeDtTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpChargeDtTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpChargeDtTo.Location = New System.Drawing.Point(340, 61)
        Me.dtpChargeDtTo.Name = "dtpChargeDtTo"
        Me.dtpChargeDtTo.ShowCheckBox = True
        Me.dtpChargeDtTo.Size = New System.Drawing.Size(108, 21)
        Me.dtpChargeDtTo.TabIndex = 120
        '
        'txtReferenceNo
        '
        Me.txtReferenceNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtReferenceNo.Flags = 0
        Me.txtReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReferenceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtReferenceNo.Location = New System.Drawing.Point(119, 34)
        Me.txtReferenceNo.Name = "txtReferenceNo"
        Me.txtReferenceNo.Size = New System.Drawing.Size(118, 21)
        Me.txtReferenceNo.TabIndex = 114
        '
        'cboResponseType
        '
        Me.cboResponseType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResponseType.DropDownWidth = 300
        Me.cboResponseType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResponseType.FormattingEnabled = True
        Me.cboResponseType.Location = New System.Drawing.Point(546, 61)
        Me.cboResponseType.Name = "cboResponseType"
        Me.cboResponseType.Size = New System.Drawing.Size(150, 21)
        Me.cboResponseType.TabIndex = 124
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(12, 36)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(101, 17)
        Me.lblRefNo.TabIndex = 115
        Me.lblRefNo.Text = "Reference No."
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblResponseType
        '
        Me.lblResponseType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResponseType.Location = New System.Drawing.Point(454, 63)
        Me.lblResponseType.Name = "lblResponseType"
        Me.lblResponseType.Size = New System.Drawing.Size(85, 17)
        Me.lblResponseType.TabIndex = 123
        Me.lblResponseType.Text = "Reponse Type"
        Me.lblResponseType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblChgDtTo
        '
        Me.lblChgDtTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChgDtTo.Location = New System.Drawing.Point(243, 63)
        Me.lblChgDtTo.Name = "lblChgDtTo"
        Me.lblChgDtTo.Size = New System.Drawing.Size(91, 17)
        Me.lblChgDtTo.TabIndex = 121
        Me.lblChgDtTo.Text = "Charge Date To"
        Me.lblChgDtTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(702, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 117
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(340, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(356, 21)
        Me.cboEmployee.TabIndex = 116
        '
        'dtpChargeDtFrom
        '
        Me.dtpChargeDtFrom.Checked = False
        Me.dtpChargeDtFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpChargeDtFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpChargeDtFrom.Location = New System.Drawing.Point(119, 61)
        Me.dtpChargeDtFrom.Name = "dtpChargeDtFrom"
        Me.dtpChargeDtFrom.ShowCheckBox = True
        Me.dtpChargeDtFrom.Size = New System.Drawing.Size(118, 21)
        Me.dtpChargeDtFrom.TabIndex = 118
        '
        'lblChgDtFrom
        '
        Me.lblChgDtFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChgDtFrom.Location = New System.Drawing.Point(12, 63)
        Me.lblChgDtFrom.Name = "lblChgDtFrom"
        Me.lblChgDtFrom.Size = New System.Drawing.Size(101, 17)
        Me.lblChgDtFrom.TabIndex = 119
        Me.lblChgDtFrom.Text = "Charge Date From"
        Me.lblChgDtFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(243, 36)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(91, 17)
        Me.lblPersonalInvolved.TabIndex = 113
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(913, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(890, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnAdd)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 522)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(944, 50)
        Me.objFooter.TabIndex = 4
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 10)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(103, 30)
        Me.btnOperations.SplitButtonMenu = Me.mnuOperation
        Me.btnOperations.TabIndex = 125
        Me.btnOperations.Text = "Operations"
        '
        'mnuOperation
        '
        Me.mnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAddEditResponse, Me.objSep5, Me.mnuSendNotification, Me.mnuScheduleHearing, Me.objSep7, Me.mnuSubmitProceedingsforApproval, Me.objSep2, Me.mnuInReopen, Me.mnuExReopen, Me.objSep4, Me.mnuDisciplineCharge})
        Me.mnuOperation.Name = "mnuOperation"
        Me.mnuOperation.Size = New System.Drawing.Size(250, 182)
        '
        'mnuAddEditResponse
        '
        Me.mnuAddEditResponse.Name = "mnuAddEditResponse"
        Me.mnuAddEditResponse.Size = New System.Drawing.Size(249, 22)
        Me.mnuAddEditResponse.Text = "&View/Add/Edit Response"
        '
        'objSep5
        '
        Me.objSep5.Name = "objSep5"
        Me.objSep5.Size = New System.Drawing.Size(246, 6)
        '
        'mnuSendNotification
        '
        Me.mnuSendNotification.Name = "mnuSendNotification"
        Me.mnuSendNotification.Size = New System.Drawing.Size(249, 22)
        Me.mnuSendNotification.Tag = "mnuSendNotification"
        Me.mnuSendNotification.Text = "&Send Notification"
        '
        'mnuScheduleHearing
        '
        Me.mnuScheduleHearing.Name = "mnuScheduleHearing"
        Me.mnuScheduleHearing.Size = New System.Drawing.Size(249, 22)
        Me.mnuScheduleHearing.Tag = "mnuScheduleHearing"
        Me.mnuScheduleHearing.Text = "Schedule &Hearing"
        '
        'objSep7
        '
        Me.objSep7.Name = "objSep7"
        Me.objSep7.Size = New System.Drawing.Size(246, 6)
        Me.objSep7.Tag = "objSep7"
        '
        'mnuSubmitProceedingsforApproval
        '
        Me.mnuSubmitProceedingsforApproval.Name = "mnuSubmitProceedingsforApproval"
        Me.mnuSubmitProceedingsforApproval.Size = New System.Drawing.Size(249, 22)
        Me.mnuSubmitProceedingsforApproval.Tag = "mnuSubmitProceedingsforApproval"
        Me.mnuSubmitProceedingsforApproval.Text = "Submit Proceedings for Approval"
        Me.mnuSubmitProceedingsforApproval.Visible = False
        '
        'objSep2
        '
        Me.objSep2.Name = "objSep2"
        Me.objSep2.Size = New System.Drawing.Size(246, 6)
        Me.objSep2.Tag = "objSep2"
        Me.objSep2.Visible = False
        '
        'mnuInReopen
        '
        Me.mnuInReopen.Name = "mnuInReopen"
        Me.mnuInReopen.Size = New System.Drawing.Size(249, 22)
        Me.mnuInReopen.Tag = "mnuInReopen"
        Me.mnuInReopen.Text = "Open Charge &Internally"
        '
        'mnuExReopen
        '
        Me.mnuExReopen.Name = "mnuExReopen"
        Me.mnuExReopen.Size = New System.Drawing.Size(249, 22)
        Me.mnuExReopen.Tag = "mnuExReopen"
        Me.mnuExReopen.Text = "Open Charge &Externally"
        '
        'objSep4
        '
        Me.objSep4.Name = "objSep4"
        Me.objSep4.Size = New System.Drawing.Size(246, 6)
        Me.objSep4.Tag = "objSep4"
        '
        'mnuDisciplineCharge
        '
        Me.mnuDisciplineCharge.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrint, Me.mnuPreview})
        Me.mnuDisciplineCharge.Name = "mnuDisciplineCharge"
        Me.mnuDisciplineCharge.Size = New System.Drawing.Size(249, 22)
        Me.mnuDisciplineCharge.Tag = "mnuDisciplineCharge"
        Me.mnuDisciplineCharge.Text = "Discipline Charge"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(115, 22)
        Me.mnuPrint.Tag = "mnuPrint"
        Me.mnuPrint.Text = "&Print"
        '
        'mnuPreview
        '
        Me.mnuPreview.Name = "mnuPreview"
        Me.mnuPreview.Size = New System.Drawing.Size(115, 22)
        Me.mnuPreview.Tag = "mnuPreview"
        Me.mnuPreview.Text = "P&review"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(732, 10)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 124
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(629, 10)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 123
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(526, 10)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 122
        Me.btnAdd.Text = "&New"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(835, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 121
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Reference No"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Charge Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 80
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Interdiction Date"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 80
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Person Involved"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn5.HeaderText = "General Description"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 250
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "No. Of Count"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 50
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Notification Date"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 80
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Response Date"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 80
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Response Type"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 80
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "First Hearing Date"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 80
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Next Hearing Date"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        Me.DataGridViewTextBoxColumn11.Width = 80
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Charge Status"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Category"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "objdgcolhDiscFileunkid"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "objdgcolhEmailAddress"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "objdgcolhCategoryId"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhRefNo
        '
        Me.dgcolhRefNo.HeaderText = "Reference No"
        Me.dgcolhRefNo.Name = "dgcolhRefNo"
        Me.dgcolhRefNo.ReadOnly = True
        Me.dgcolhRefNo.Width = 90
        '
        'dgcolhChargeDate
        '
        Me.dgcolhChargeDate.HeaderText = "Charge Date"
        Me.dgcolhChargeDate.Name = "dgcolhChargeDate"
        Me.dgcolhChargeDate.ReadOnly = True
        Me.dgcolhChargeDate.Width = 80
        '
        'dgcolhInterdictionDate
        '
        Me.dgcolhInterdictionDate.HeaderText = "Interdiction Date"
        Me.dgcolhInterdictionDate.Name = "dgcolhInterdictionDate"
        Me.dgcolhInterdictionDate.ReadOnly = True
        Me.dgcolhInterdictionDate.Width = 80
        '
        'dgcolhInvolved
        '
        Me.dgcolhInvolved.HeaderText = "Person Involved"
        Me.dgcolhInvolved.Name = "dgcolhInvolved"
        Me.dgcolhInvolved.ReadOnly = True
        Me.dgcolhInvolved.Width = 200
        '
        'dgcolhGeneralDesc
        '
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhGeneralDesc.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhGeneralDesc.HeaderText = "General Description"
        Me.dgcolhGeneralDesc.Name = "dgcolhGeneralDesc"
        Me.dgcolhGeneralDesc.ReadOnly = True
        Me.dgcolhGeneralDesc.Width = 115
        '
        'dgcolhCount
        '
        Me.dgcolhCount.HeaderText = "No. Of Count"
        Me.dgcolhCount.Name = "dgcolhCount"
        Me.dgcolhCount.ReadOnly = True
        Me.dgcolhCount.Width = 50
        '
        'dgcolhNotificationDate
        '
        Me.dgcolhNotificationDate.HeaderText = "Notification Date"
        Me.dgcolhNotificationDate.Name = "dgcolhNotificationDate"
        Me.dgcolhNotificationDate.ReadOnly = True
        Me.dgcolhNotificationDate.Width = 80
        '
        'dgcolhFirstHearingDate
        '
        Me.dgcolhFirstHearingDate.HeaderText = "First Hearing Date"
        Me.dgcolhFirstHearingDate.Name = "dgcolhFirstHearingDate"
        Me.dgcolhFirstHearingDate.ReadOnly = True
        Me.dgcolhFirstHearingDate.Width = 80
        '
        'dgcolhNextHearingDate
        '
        Me.dgcolhNextHearingDate.HeaderText = "Next Hearing Date"
        Me.dgcolhNextHearingDate.Name = "dgcolhNextHearingDate"
        Me.dgcolhNextHearingDate.ReadOnly = True
        Me.dgcolhNextHearingDate.Width = 80
        '
        'dgcolhChargeStatus
        '
        Me.dgcolhChargeStatus.HeaderText = "Charge Status"
        Me.dgcolhChargeStatus.Name = "dgcolhChargeStatus"
        Me.dgcolhChargeStatus.ReadOnly = True
        '
        'dgcolhCategory
        '
        Me.dgcolhCategory.HeaderText = "Category"
        Me.dgcolhCategory.Name = "dgcolhCategory"
        Me.dgcolhCategory.ReadOnly = True
        Me.dgcolhCategory.Visible = False
        '
        'objdgcolhDiscFileunkid
        '
        Me.objdgcolhDiscFileunkid.HeaderText = "objdgcolhDiscFileunkid"
        Me.objdgcolhDiscFileunkid.Name = "objdgcolhDiscFileunkid"
        Me.objdgcolhDiscFileunkid.ReadOnly = True
        Me.objdgcolhDiscFileunkid.Visible = False
        '
        'objdgcolhEmailAddress
        '
        Me.objdgcolhEmailAddress.HeaderText = "objdgcolhEmailAddress"
        Me.objdgcolhEmailAddress.Name = "objdgcolhEmailAddress"
        Me.objdgcolhEmailAddress.ReadOnly = True
        Me.objdgcolhEmailAddress.Visible = False
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'objdgcolhCategoryId
        '
        Me.objdgcolhCategoryId.HeaderText = "objdgcolhCategoryId"
        Me.objdgcolhCategoryId.Name = "objdgcolhCategoryId"
        Me.objdgcolhCategoryId.Visible = False
        '
        'objdgcolhStatusId
        '
        Me.objdgcolhStatusId.HeaderText = "objdgcolhStatusId"
        Me.objdgcolhStatusId.Name = "objdgcolhStatusId"
        Me.objdgcolhStatusId.ReadOnly = True
        Me.objdgcolhStatusId.Visible = False
        '
        'objdgcolhIsFinal
        '
        Me.objdgcolhIsFinal.HeaderText = "objdgcolhIsFinal"
        Me.objdgcolhIsFinal.Name = "objdgcolhIsFinal"
        Me.objdgcolhIsFinal.ReadOnly = True
        Me.objdgcolhIsFinal.Visible = False
        '
        'frmDisciplineFilingList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(944, 572)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDisciplineFilingList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Disciplinary Charges List"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        CType(Me.dgvChargesList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objemailFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.mnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuAddEditResponse As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuInReopen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuExReopen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents objemailFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOk As eZee.Common.eZeeLightButton
    Friend WithEvents btnEClose As eZee.Common.eZeeLightButton
    Friend WithEvents mnuDisciplineCharge As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtReferenceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents dtpChargeDtTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblChgDtTo As System.Windows.Forms.Label
    Friend WithEvents dtpChargeDtFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblChgDtFrom As System.Windows.Forms.Label
    Friend WithEvents cboResponseType As System.Windows.Forms.ComboBox
    Friend WithEvents lblResponseType As System.Windows.Forms.Label
    Friend WithEvents dgvChargesList As System.Windows.Forms.DataGridView
    Friend WithEvents cboChargeStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblChargeStatus As System.Windows.Forms.Label
    Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblCategory As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents mnuSendNotification As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuScheduleHearing As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSubmitProceedingsforApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objSep4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChargeDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInterdictionDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInvolved As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhGeneralDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNotificationDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhFirstHearingDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNextHearingDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhChargeStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhDiscFileunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmailAddress As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCategoryId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStatusId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsFinal As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
