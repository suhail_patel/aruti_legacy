﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmDisciplineProceedingList

    Private ReadOnly mstrModuleName As String = "frmDisciplineProceedingList"
    Private objProceedingMaster As clsdiscipline_proceeding_master
    Private objProceedingTran As clsdiscipline_members_tran
    Private mdtEmployee As New DataTable
    Private mdtReference As New DataTable

#Region " Form's Events "

    Private Sub frmDisciplineProceedingList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmDisciplineProceedingList_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsdiscipline_members_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsdiscipline_members_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineProceedingList_LanguageClick", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineProceedingList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objProceedingMaster = New clsdiscipline_proceeding_master
        objProceedingTran = New clsdiscipline_members_tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

            Call FillCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineProceedingList_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objDscplnFileMaster As New clsDiscipline_file_master
            Dim objActionReason As New clsAction_Reason

            dsList = objDscplnFileMaster.GetComboList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                                      True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", , , , , , )

            mdtEmployee = dsList.Tables("List").DefaultView.ToTable(True, "involved_employeeunkid", "employeecode", "employeename")

            With cboEmployee
                .ValueMember = "involved_employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = mdtEmployee
                .SelectedValue = 0
            End With

            mdtReference = dsList.Tables("List")
            With cboReferenceNo
                .ValueMember = "disciplinefileunkid"
                .DisplayMember = "reference_no"
                .DataSource = mdtReference
                .SelectedValue = 0
            End With

            dsList = objActionReason.getComboList("List", True, True)
            With cboDisciplineAction
                .DisplayMember = "name"
                .ValueMember = "actionreasonunkid"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objProceedingMaster.getProceedingCountStatus("Count", True)
            With cboCountStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Count")
                .SelectedValue = 0
            End With

            dsList = objProceedingMaster.getComboApprovalStatus("Status", True)
            With cboStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Status")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Try
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            If User._Object.Privilege._AllowToViewDisciplineProceedingList = False Then Exit Sub
            'Varsha Rana (17-Oct-2017) -- End

            Dim dsList As New DataSet
            Dim strFilter As String = String.Empty

            'S.SANDEEP [22-NOV-2018] -- START
            'If CInt(cboEmployee.SelectedValue) > 0 Then
            '    strFilter &= "AND hrdiscipline_file_master.involved_employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            'End If

            'If CInt(cboReferenceNo.SelectedValue) > 0 Then
            '    strFilter &= "AND hrdiscipline_file_master.disciplinefileunkid = " & CInt(cboReferenceNo.SelectedValue) & " "
            'End If

            'If CInt(cboDisciplineAction.SelectedValue) > 0 Then
            '    strFilter &= "AND hrdiscipline_proceeding_tran.actionreasonunkid = " & CInt(cboDisciplineAction.SelectedValue) & " "
            'End If

            'If CInt(cboCountStatus.SelectedValue) > 0 Then
            '    strFilter &= "AND hrdiscipline_proceeding_tran.count_status = " & CInt(cboCountStatus.SelectedValue) & " "
            'End If

            'If CInt(cboStatus.SelectedValue) = clsdiscipline_proceeding_master.enFinalStatus.Approved Then
            '    strFilter &= "AND hrdiscipline_proceeding_tran.isapproved = 1 "
            'ElseIf CInt(cboStatus.SelectedValue) = clsdiscipline_proceeding_master.enFinalStatus.Pending Then
            '    strFilter &= "AND hrdiscipline_proceeding_tran.isapproved = 0 "
            'End If

            'If dtpChargeDtFrom.Checked = True Then
            '    strFilter &= "AND CONVERT(CHAR(8),hrdiscipline_file_master.chargedate,112) >= " & eZeeDate.convertDate(dtpChargeDtFrom.Value) & " "
            'End If

            'If dtpChargeDtTo.Checked = True Then
            '    strFilter &= "AND CONVERT(CHAR(8),hrdiscipline_file_master.chargedate,112) <= " & eZeeDate.convertDate(dtpChargeDtTo.Value) & " "
            'End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strFilter &= "AND hrdiscipline_file_master.involved_employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboReferenceNo.SelectedValue) > 0 Then
                strFilter &= "AND hrdiscipline_file_master.disciplinefileunkid = " & CInt(cboReferenceNo.SelectedValue) & " "
            End If

            If CInt(cboDisciplineAction.SelectedValue) > 0 Then
                strFilter &= "AND A.actionreasonunkid = " & CInt(cboDisciplineAction.SelectedValue) & " "
            End If

            If CInt(cboCountStatus.SelectedValue) > 0 Then
                strFilter &= "AND A.count_status = " & CInt(cboCountStatus.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) = clsdiscipline_proceeding_master.enFinalStatus.Approved Then
                strFilter &= "AND A.isapproved = 1 "
            ElseIf CInt(cboStatus.SelectedValue) = clsdiscipline_proceeding_master.enFinalStatus.Pending Then
                strFilter &= "AND A.isapproved = 0 "
            End If

            If dtpChargeDtFrom.Checked = True Then
                strFilter &= "AND CONVERT(CHAR(8),hrdiscipline_file_master.chargedate,112) >= " & eZeeDate.convertDate(dtpChargeDtFrom.Value) & " "
            End If

            If dtpChargeDtTo.Checked = True Then
                strFilter &= "AND CONVERT(CHAR(8),hrdiscipline_file_master.chargedate,112) <= " & eZeeDate.convertDate(dtpChargeDtTo.Value) & " "
            End If
            'S.SANDEEP [22-NOV-2018] -- END
            

            If strFilter.Trim.Length > 0 Then
                strFilter = strFilter.Substring(3)
            End If

            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'dsList = objProceedingMaster.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
            '                                     Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
            '                                     True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", strFilter)
            dsList = objProceedingMaster.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", strFilter, ConfigParameter._Object._AddProceedingAgainstEachCount)
            'S.SANDEEP |11-NOV-2019| -- END

            dgvChargesList.AutoGenerateColumns = False

            dgcolhRefNo.DataPropertyName = "reference_no"
            dgcolhChargeDate.DataPropertyName = "chargedate"
            dgcolhChargeDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhPersonInvloved.DataPropertyName = "person_involved"
            dgcolhCount.DataPropertyName = "NoOfCount"
            dgcolhCount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhCount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhCountDesc.DataPropertyName = "incident_desc"
            dgcolhOffcenceCat.DataPropertyName = "offence_category"
            dgcolhOffence.DataPropertyName = "offence"
            dgcolhCountStatus.DataPropertyName = "count_status"
            dgcolhLastResolutionDesc.DataPropertyName = "last_resolution"
            dgcolhDisciPenalty.DataPropertyName = "disciplinary_penalty"
            dgcolhFinalStatus.DataPropertyName = "status"
            dgcolhChargeDesc.DataPropertyName = "charge_description"
            objdgcolhProceedingMasterId.DataPropertyName = "disciplineproceedingmasterunkid"
            objdgcolhDisciplineFileId.DataPropertyName = "disciplinefileunkid"
            objdgcolhEmployeeId.DataPropertyName = "involved_employeeunkid"
            dgcolhCategory.DataPropertyName = "Category"
            'objdgcolhProceedingTranId.DataPropertyName = "disciplineproceedingtranunkid"
            'S.SANDEEP [24 MAY 2016] -- Start
            'Email Notification
            objdgcolhIsSubmitForApproval.DataPropertyName = "issubmitforapproval"
            objdgcolhIsApproved.DataPropertyName = "isapproved"
            'S.SANDEEP [24 MAY 2016] -- End

            dgvChargesList.DataSource = dsList.Tables("List")

            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            If ConfigParameter._Object._AddProceedingAgainstEachCount = False Then
                dgcolhCount.Visible = False
                dgcolhCountDesc.Visible = False
                dgcolhOffcenceCat.Visible = False
                dgcolhOffence.Visible = False
            End If
            'S.SANDEEP |01-OCT-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(dgvChargesList.RowCount.ToString)
        End Try
    End Sub

    Private Sub FilterReferenceNo()
        Try
            Dim strCsvData As String = ""
            strCsvData = String.Join(",", mdtReference.AsEnumerable().Where(Function(x) x.Field(Of Integer)("involved_employeeunkid") = CInt(cboEmployee.SelectedValue)).Select(Function(y) y.Field(Of Integer)("disciplinefileunkid").ToString).ToArray())

            Dim mdtView As DataView = New DataView(mdtReference, "disciplinefileunkid IN (" & strCsvData & ") ", "", DataViewRowState.CurrentRows)

            With cboReferenceNo
                .ValueMember = "disciplinefileunkid"
                .DisplayMember = "reference_no"
                .DataSource = mdtView.ToTable
                If mdtView.ToTable.Rows.Count = 1 Then
                    .SelectedValue = mdtView.ToTable.Rows(0).Item("disciplinefileunkid")
                Else
                    .SelectedValue = 0
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FilterReferenceNo", mstrModuleName)
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges
    Private Sub SetVisibility()

        Try

            btnAdd.Enabled = User._Object.Privilege._AllowToPostNewProceedings
            btnEdit.Enabled = User._Object.Privilege._AllowToEditViewProceedings
            'S.SANDEEP |26-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'mnuScanAttachDocs.Enabled = User._Object.Privilege._AllowToScanAttachmentDocuments
            'S.SANDEEP |26-OCT-2019| -- END
            mnuSubmitForApproval.Enabled = User._Object.Privilege._AllowToProceedingSubmitForApproval
            mnuApproveDisapproveProceedings.Enabled = User._Object.Privilege._AllowToApproveDisapproveProceedings
            mnuExemptTransHead.Enabled = User._Object.Privilege._AllowToExemptTransactionHead

            mnuPostTransHead.Enabled = User._Object.Privilege._AllowToPostTransactionHead
            mnuViewDisciplineHead.Enabled = User._Object.Privilege._AllowToViewDisciplineHead
            mnuDisciplineCharge.Enabled = User._Object.Privilege._AllowToPrintPreviewDisciplineCharge

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
    'Varsha Rana (17-Oct-2017) -- End

#End Region

#Region " Button's Events "

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim frm As New frmProceedingsApprovalAddEdit
            frm.displayDialog(enAction.ADD_CONTINUE, -1)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmProceedingsApprovalAddEdit
        Try
            If dgvChargesList.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operations."), enMsgBoxStyle.Information)
                dgvChargesList.Focus()
                Exit Sub
            Else
                If frm.displayDialog(enAction.EDIT_ONE, CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhProceedingMasterId.Index).Value)) Then
                    Call FillList()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    'Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    '    Dim blnFlag As Boolean
    '    Try
    '        If dgvChargesList.RowCount > 0 Then

    '            If dgvChargesList.Rows.Count <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operations."), enMsgBoxStyle.Information)
    '                dgvChargesList.Focus()
    '                Exit Sub
    '            End If

    '            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Hearing Schedule?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

    '                objProceedingMaster._Userunkid = User._Object._Userunkid
    '                objProceedingMaster._Isvoid = True
    '                objProceedingMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '                objProceedingMaster._Voiduserunkid = User._Object._Userunkid

    '                Dim frm As New frmReasonSelection
    '                If User._Object._Isrighttoleft = True Then
    '                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                    frm.RightToLeftLayout = True
    '                    Call Language.ctlRightToLeftlayOut(frm)
    '                End If
    '                Dim mstrVoidReason As String = String.Empty
    '                frm.displayDialog(enVoidCategoryType.DISCIPLINE, mstrVoidReason)
    '                If mstrVoidReason.Length <= 0 Then
    '                    Exit Sub
    '                Else
    '                    objProceedingMaster._Voidreason = mstrVoidReason
    '                End If
    '                frm = Nothing

    '                objProceedingTran._DisciplineProceedingMasterunkid = CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhProceedingMasterId.Index).Value)
    '                Dim mdtTran As DataTable = objProceedingTran._ProceedingTable

    '                For Each dRow As DataRow In mdtTran.Rows
    '                    If CInt(dRow.Item("disciplineproceedingtranunkid")) = CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhProceedingTranId.Index).Value) Then
    '                        dRow.Item("isvoid") = True
    '                        dRow.Item("voiduserunkid") = User._Object._Userunkid
    '                        dRow.Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
    '                        dRow.Item("voidreason") = mstrVoidReason
    '                        dRow.Item("AUD") = "D"
    '                    End If
    '                Next
    '                mdtTran.AcceptChanges()

    '                objProceedingTran._ProceedingTable = mdtTran

    '                blnFlag = objProceedingTran.Void_Counts(User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime)

    '                If blnFlag = False Then
    '                    If objProceedingMaster._Message <> "" Then
    '                        eZeeMsgBox.Show(objProceedingMaster._Message, enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If
    '                Else
    '                    Call FillList()
    '                End If

    '            End If
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim frm As New frmCommonSearch

            With frm
                .DisplayMember = cboEmployee.DisplayMember
                .ValueMember = cboEmployee.ValueMember
                .CodeMember = "employeecode"
                .DataSource = mdtEmployee
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                Call FilterReferenceNo()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchRefNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRefNo.Click
        Try
            Dim frm As New frmCommonSearch

            With frm
                .DisplayMember = cboReferenceNo.DisplayMember
                .ValueMember = cboReferenceNo.ValueMember
                .DataSource = CType(cboReferenceNo.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboReferenceNo.SelectedValue = frm.SelectedValue
                cboReferenceNo.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchRefNo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboReferenceNo.DataSource = mdtReference
            cboReferenceNo.SelectedValue = 0
            cboDisciplineAction.SelectedValue = 0
            cboCountStatus.SelectedValue = 0
            cboStatus.SelectedValue = 0
            dtpChargeDtFrom.Value = CDate(ConfigParameter._Object._CurrentDateAndTime.ToShortDateString)
            dtpChargeDtFrom.Checked = False
            dtpChargeDtTo.Value = CDate(ConfigParameter._Object._CurrentDateAndTime.ToShortDateString)
            dtpChargeDtTo.Checked = False
            dgvChargesList.DataSource = Nothing

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .DisplayMember = cboEmployee.DisplayMember
                    .ValueMember = cboEmployee.ValueMember
                    .CodeMember = "employeecode"
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                End With
                Dim ch As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = ch.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReferenceNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboReferenceNo.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .DisplayMember = cboReferenceNo.DisplayMember
                    .ValueMember = cboReferenceNo.ValueMember
                    .DataSource = CType(cboReferenceNo.DataSource, DataTable)
                End With
                Dim ch As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = ch.ToString
                If frm.DisplayDialog Then
                    cboReferenceNo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboReferenceNo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReferenceNo_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Context Menu Events "

    Private Sub mnuExemptTransHead_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExemptTransHead.Click
        Dim frm As New frmExemptDisciplineHeads
        Try
            If dgvChargesList.SelectedRows.Count > 0 Then
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                frm.displayDialog(CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhDisciplineFileId.Index).Value), _
                                  CStr(dgvChargesList.SelectedRows(0).Cells(dgcolhPersonInvloved.Index).Value), _
                                  CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhEmployeeId.Index).Value), _
                                  CStr(dgvChargesList.SelectedRows(0).Cells(dgcolhChargeDesc.Index).Value), _
                                  enAction.ADD_ONE)

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExemptTransHead_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPostTransHead_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPostTransHead.Click
        Dim frm As New frmPostDisciplineHeads
        Try
            If dgvChargesList.SelectedRows.Count > 0 Then
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                frm.displayDialog(CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhDisciplineFileId.Index).Value), _
                                  CStr(dgvChargesList.SelectedRows(0).Cells(dgcolhPersonInvloved.Index).Value), _
                                  CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhEmployeeId.Index).Value), _
                                  CStr(dgvChargesList.SelectedRows(0).Cells(dgcolhChargeDesc.Index).Value), _
                                  enAction.ADD_ONE)

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPostTransHead_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuExemptedHeadsList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExemptedHeadsList.Click
        Dim frm As New frmExemptDisciplinaryHeadsList
        Try
            If dgvChargesList.SelectedRows.Count > 0 Then
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                frm.displayDialog(CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhDisciplineFileId.Index).Value))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExemptedHeadsList_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPostedHeadsList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPostedHeadsList.Click
        Dim frm As New frmPostDisciplineHeadsList
        Try
            If dgvChargesList.SelectedRows.Count > 0 Then
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                frm.displayDialog(CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhDisciplineFileId.Index).Value))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPostedHeadsList_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        Try
            If dgvChargesList.SelectedRows.Count > 0 Then

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrint_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPreview.Click
        Try
            If dgvChargesList.SelectedRows.Count > 0 Then

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPreview_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuApproveDisapproveProceedings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuApproveDisapproveProceedings.Click
        Dim frm As New frmApproveDisapproveCharges
        Try
            'S.SANDEEP [29 SEP 2016] -- START
            If dgvChargesList.SelectedRows.Count <= 0 Then Exit Sub
            'S.SANDEEP [29 SEP 2016] -- END

            If CBool(dgvChargesList.SelectedRows(0).Cells(objdgcolhIsSubmitForApproval.Index).Value) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry you cannot approve selected proceeding detail. Reason : Selected proceeding detail is not yet submitted for approval."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CBool(User._Object.Privilege._AllowToApproveProceedingCounts) = True Then
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                If CBool(dgvChargesList.SelectedRows(0).Cells(objdgcolhIsApproved.Index).Value) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Selected proceeding detail is already approved."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If frm.displayDialog(CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhProceedingMasterId.Index).Value), _
                                     CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhDisciplineFileId.Index).Value)) Then
                    Call FillList()
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Privilege for approving the proceeding details is not assigned to you."), enMsgBoxStyle.Information)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuApproveDisapproveProceedings_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuScanAttachDocs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScanAttachDocs.Click
        Dim frm As New frmScanOrAttachmentInfo
        Dim dtScanAttachment As DataTable = Nothing
        Try
            If dgvChargesList.SelectedRows.Count <= 0 Then Exit Sub

            Dim objScanAttachment As New clsScan_Attach_Documents

            'S.SANDEEP |04-SEP-2021| -- START
            'objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
            objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), , , , , CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
            'S.SANDEEP |04-SEP-2021| -- END

            Dim xRow = objScanAttachment._Datatable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("scanattachrefid") = enScanAttactRefId.DISCIPLINES _
                                                                         And x.Field(Of Integer)("transactionunkid") = CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhDisciplineFileId.Index).Value) _
                                                                         And x.Field(Of String)("form_name") = "'" & mstrModuleName & "'")
            If xRow.Count > 0 Then
                dtScanAttachment = xRow.CopyToDataTable()
            Else
                dtScanAttachment = objScanAttachment._Datatable.Clone
            End If
            objScanAttachment = Nothing

            If dgvChargesList.SelectedRows.Count > 0 Then
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                frm._dtAttachment = dtScanAttachment
                'frm._TransactionID = CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhDisciplineFileId.Index).Value)
                frm._TransactionID = CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhProceedingMasterId.Index).Value)
                'S.SANDEEP |26-APR-2019| -- START
                'frm.displayDialog(Language.getMessage(mstrModuleName, 2, "Select Employee"), enImg_Email_RefId.Discipline_Module, enAction.ADD_ONE, "", mstrModuleName, CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhEmployeeId.Index).Value), enScanAttactRefId.DISCIPLINES, False, False) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
                frm.displayDialog(Language.getMessage(mstrModuleName, 2, "Select Employee"), enImg_Email_RefId.Discipline_Module, enAction.ADD_ONE, "", mstrModuleName, True, CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhEmployeeId.Index).Value), enScanAttactRefId.DISCIPLINES, False, False) 'S.SANDEEP [26 JUL 2016] -- START {mstrModuleName} -- END
                'S.SANDEEP |26-APR-2019| -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuScanAttachDocs_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuSubmitForApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSubmitForApproval.Click
        Try
            Dim blnFlag As Boolean

            'S.SANDEEP [29 SEP 2016] -- START
            If dgvChargesList.SelectedRows.Count <= 0 Then Exit Sub
            'S.SANDEEP [29 SEP 2016] -- END

            If CBool(dgvChargesList.SelectedRows(0).Cells(objdgcolhIsSubmitForApproval.Index).Value) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry you cannot Submit for approval for selected. Reason : Already submitted for approval."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            objProceedingMaster._Userunkid = User._Object._Userunkid
            objProceedingMaster._LoginTypeId = enLogin_Mode.DESKTOP

            '"FRMDISCIPLINEPROCEEDINGLIST", "FRMPROCEEDINGSAPPROVALADDEDIT", "FRMCHARGEPROCEEDINGDETAILS", "FRMAPPROVEDISAPPROVECHARGES"
            Dim lstName As New List(Of String)(New String() {"frmDisciplineProceedingList", "frmProceedingsApprovalAddEdit", "frmChargeProceedingDetails"})
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'blnFlag = objProceedingMaster.SubmitForApproval(CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhProceedingMasterId.Index).Value), _
            '                                                CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhDisciplineFileId.Index).Value), _
            '                                                FinancialYear._Object._YearUnkid, ConfigParameter._Object._CurrentDateAndTime, _
            '                                                Company._Object._Senderaddress, lstName, ConfigParameter._Object._Document_Path) 'S.SANDEEP [25 JUL 2016] -- START {Company._Object._Senderaddress} -- END 
            blnFlag = objProceedingMaster.SubmitForApproval(CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhProceedingMasterId.Index).Value), _
                                                            CInt(dgvChargesList.SelectedRows(0).Cells(objdgcolhDisciplineFileId.Index).Value), _
                                                            FinancialYear._Object._YearUnkid, ConfigParameter._Object._CurrentDateAndTime, _
                                                            Company._Object._Senderaddress, lstName, ConfigParameter._Object._Document_Path, Company._Object._Companyunkid)
            'Sohail (30 Nov 2017) -- End

            If blnFlag = False AndAlso objProceedingMaster._Message <> "" Then
                eZeeMsgBox.Show(objProceedingMaster._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If blnFlag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Submit for Approval Successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSubmitForApproval_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

            Me.btnOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnOk.GradientForeColor = GUI._ButttonFontColor

            Me.btnEClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnEClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
            Me.btnEClose.Text = Language._Object.getCaption(Me.btnEClose.Name, Me.btnEClose.Text)
            Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
            Me.lblChgDtTo.Text = Language._Object.getCaption(Me.lblChgDtTo.Name, Me.lblChgDtTo.Text)
            Me.lblChgDtFrom.Text = Language._Object.getCaption(Me.lblChgDtFrom.Name, Me.lblChgDtFrom.Text)
            Me.lblDisciplineAction.Text = Language._Object.getCaption(Me.lblDisciplineAction.Name, Me.lblDisciplineAction.Text)
            Me.lblCountStatus.Text = Language._Object.getCaption(Me.lblCountStatus.Name, Me.lblCountStatus.Text)
            Me.lblFinalStatus.Text = Language._Object.getCaption(Me.lblFinalStatus.Name, Me.lblFinalStatus.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
            Me.dgvChargesList.Text = Language._Object.getCaption(Me.dgvChargesList.Name, Me.dgvChargesList.Text)
            Me.mnuScanAttachDocs.Text = Language._Object.getCaption(Me.mnuScanAttachDocs.Name, Me.mnuScanAttachDocs.Text)
            Me.mnuSubmitForApproval.Text = Language._Object.getCaption(Me.mnuSubmitForApproval.Name, Me.mnuSubmitForApproval.Text)
            Me.mnuExemptTransHead.Text = Language._Object.getCaption(Me.mnuExemptTransHead.Name, Me.mnuExemptTransHead.Text)
            Me.mnuPostTransHead.Text = Language._Object.getCaption(Me.mnuPostTransHead.Name, Me.mnuPostTransHead.Text)
            Me.mnuViewDisciplineHead.Text = Language._Object.getCaption(Me.mnuViewDisciplineHead.Name, Me.mnuViewDisciplineHead.Text)
            Me.mnuExemptedHeadsList.Text = Language._Object.getCaption(Me.mnuExemptedHeadsList.Name, Me.mnuExemptedHeadsList.Text)
            Me.mnuPostedHeadsList.Text = Language._Object.getCaption(Me.mnuPostedHeadsList.Name, Me.mnuPostedHeadsList.Text)
            Me.mnuDisciplineCharge.Text = Language._Object.getCaption(Me.mnuDisciplineCharge.Name, Me.mnuDisciplineCharge.Text)
            Me.mnuPrint.Text = Language._Object.getCaption(Me.mnuPrint.Name, Me.mnuPrint.Text)
            Me.mnuPreview.Text = Language._Object.getCaption(Me.mnuPreview.Name, Me.mnuPreview.Text)
            Me.mnuApproveDisapproveProceedings.Text = Language._Object.getCaption(Me.mnuApproveDisapproveProceedings.Name, Me.mnuApproveDisapproveProceedings.Text)
            Me.dgcolhRefNo.HeaderText = Language._Object.getCaption(Me.dgcolhRefNo.Name, Me.dgcolhRefNo.HeaderText)
            Me.dgcolhChargeDate.HeaderText = Language._Object.getCaption(Me.dgcolhChargeDate.Name, Me.dgcolhChargeDate.HeaderText)
            Me.dgcolhPersonInvloved.HeaderText = Language._Object.getCaption(Me.dgcolhPersonInvloved.Name, Me.dgcolhPersonInvloved.HeaderText)
            Me.dgcolhCount.HeaderText = Language._Object.getCaption(Me.dgcolhCount.Name, Me.dgcolhCount.HeaderText)
            Me.dgcolhCountDesc.HeaderText = Language._Object.getCaption(Me.dgcolhCountDesc.Name, Me.dgcolhCountDesc.HeaderText)
            Me.dgcolhOffcenceCat.HeaderText = Language._Object.getCaption(Me.dgcolhOffcenceCat.Name, Me.dgcolhOffcenceCat.HeaderText)
            Me.dgcolhOffence.HeaderText = Language._Object.getCaption(Me.dgcolhOffence.Name, Me.dgcolhOffence.HeaderText)
            Me.dgcolhCountStatus.HeaderText = Language._Object.getCaption(Me.dgcolhCountStatus.Name, Me.dgcolhCountStatus.HeaderText)
            Me.dgcolhLastResolutionDesc.HeaderText = Language._Object.getCaption(Me.dgcolhLastResolutionDesc.Name, Me.dgcolhLastResolutionDesc.HeaderText)
            Me.dgcolhDisciPenalty.HeaderText = Language._Object.getCaption(Me.dgcolhDisciPenalty.Name, Me.dgcolhDisciPenalty.HeaderText)
            Me.dgcolhFinalStatus.HeaderText = Language._Object.getCaption(Me.dgcolhFinalStatus.Name, Me.dgcolhFinalStatus.HeaderText)
            Me.dgcolhChargeDesc.HeaderText = Language._Object.getCaption(Me.dgcolhChargeDesc.Name, Me.dgcolhChargeDesc.HeaderText)
            Me.dgcolhCategory.HeaderText = Language._Object.getCaption(Me.dgcolhCategory.Name, Me.dgcolhCategory.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select atleast one row for further operations.")
            Language.setMessage(mstrModuleName, 2, "Select Employee")
            Language.setMessage(mstrModuleName, 3, "Sorry you cannot Submit for approval for selected. Reason : Already submitted for approval.")
            Language.setMessage(mstrModuleName, 4, "Submit for Approval Successfully.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Selected proceeding detail is already approved.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Privilege for approving the proceeding details is not assigned to you.")
            Language.setMessage(mstrModuleName, 7, "Sorry you cannot approve selected proceeding detail. Reason : Selected proceeding detail is not yet submitted for approval.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class