﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmDisciplineStatusList

#Region " Private Variables "
    Private objDisciplineStatus As clsDiscipline_Status
    Private ReadOnly mstrModuleName As String = "frmDisciplineStatusList"
#End Region

#Region " Private Function "
    Private Sub FillList()
        Dim dsStatus As New DataSet
        Try

            If User._Object.Privilege._AllowToViewDisciplinaryStatusList = True Then     'Pinkal (09-Jul-2012) -- Start


            dsStatus = objDisciplineStatus.GetList("Status", True)
            Dim lvItem As ListViewItem

            lvDisciplineStatus.Items.Clear()
            For Each dtRow As DataRow In dsStatus.Tables("Status").Rows
                lvItem = New ListViewItem
                If CInt(dtRow("disciplinestatusunkid")) <= 3 Then
                    lvItem.ForeColor = Color.Gray
                End If
                lvItem.Text = dtRow("code").ToString
                lvItem.SubItems.Add(dtRow("name").ToString)

                'Anjan (20 Mar 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                lvItem.SubItems.Add(dtRow("statuslevel").ToString)
                'Anjan (20 Mar 2012)-End 

                lvItem.SubItems.Add(dtRow("description").ToString)

                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lvItem.SubItems.Add(dtRow.Item("StatusType").ToString)
                'S.SANDEEP [ 20 APRIL 2012 ] -- END

                lvItem.Tag = dtRow("disciplinestatusunkid")

                lvDisciplineStatus.Items.Add(lvItem)

            Next



            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If lvDisciplineStatus.Items.Count > 16 Then
            '    colhDescription.Width = 415 - 20
            'Else
            '    colhDescription.Width = 415
            'End If
            If lvDisciplineStatus.Items.Count > 16 Then
                colhDescription.Width = 250 - 20
            Else
                colhDescription.Width = 250
            End If
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            End If
            

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub


    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddDisciplineStatus
            btnEdit.Enabled = User._Object.Privilege._EditDisciplineStatus
            btnDelete.Enabled = User._Object.Privilege._DeleteDisciplineStatus
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmDisciplineStatusList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvDisciplineStatus.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineStatusList", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineStatusList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineStatusList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineStatusList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objDisciplineStatus = Nothing
    End Sub

    Private Sub frmDisciplineStatusList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDisciplineStatus = New clsDiscipline_Status
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            Call FillList()

            If lvDisciplineStatus.Items.Count > 0 Then lvDisciplineStatus.Items(0).Selected = True
            lvDisciplineStatus.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineTypeList_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_Status.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_Status"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmDisciplineStatus_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvDisciplineStatus.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline Status from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvDisciplineStatus.Select()
            Exit Sub
        End If
        'If lvDisciplineStatus.SelectedItems(0).ForeColor = Color.Gray Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You cannot Edit this Discipline Status. Reason : It's predefined."), enMsgBoxStyle.Information) '?1
        '    lvDisciplineStatus.Select()
        '    Exit Sub
        'End If
        Dim frm As New frmDisciplineStatus_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvDisciplineStatus.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(CInt(lvDisciplineStatus.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            frm = Nothing

            lvDisciplineStatus.Items(intSelectedIndex).Selected = True
            lvDisciplineStatus.EnsureVisible(intSelectedIndex)
            lvDisciplineStatus.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvDisciplineStatus.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline Status from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvDisciplineStatus.Select()
            Exit Sub
        End If

        If lvDisciplineStatus.SelectedItems(0).ForeColor = Color.Gray Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You cannot Delete this Discipline Status. Reason : It is predefined."), enMsgBoxStyle.Information) '?1
            lvDisciplineStatus.Select()
            Exit Sub
        End If


        'S.SANDEEP [28-May-2018] -- START
        'ISSUE/ENHANCEMENT : {Audit Trails} 
        objDisciplineStatus._FormName = mstrModuleName
        objDisciplineStatus._LoginEmployeeunkid = 0
        objDisciplineStatus._ClientIP = getIP()
        objDisciplineStatus._HostName = getHostName()
        objDisciplineStatus._FromWeb = False
        objDisciplineStatus._AuditUserId = User._Object._Userunkid
objDisciplineStatus._CompanyUnkid = Company._Object._Companyunkid
        objDisciplineStatus._AuditDate = ConfigParameter._Object._CurrentDateAndTime
        'S.SANDEEP [28-May-2018] -- END

        If objDisciplineStatus.isUsed(CInt(lvDisciplineStatus.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete this Discipline Status. Reason: This Discipline Status is in use."), enMsgBoxStyle.Information) '?2
            lvDisciplineStatus.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvDisciplineStatus.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Discipline Status?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objDisciplineStatus._FormName = mstrModuleName
                objDisciplineStatus._LoginEmployeeunkid = 0
                objDisciplineStatus._ClientIP = getIP()
                objDisciplineStatus._HostName = getHostName()
                objDisciplineStatus._FromWeb = False
                objDisciplineStatus._AuditUserId = User._Object._Userunkid
objDisciplineStatus._CompanyUnkid = Company._Object._Companyunkid
                objDisciplineStatus._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objDisciplineStatus.Delete(CInt(lvDisciplineStatus.SelectedItems(0).Tag))
                lvDisciplineStatus.SelectedItems(0).Remove()

                If lvDisciplineStatus.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvDisciplineStatus.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvDisciplineStatus.Items.Count - 1
                    lvDisciplineStatus.Items(intSelectedIndex).Selected = True
                    lvDisciplineStatus.EnsureVisible(intSelectedIndex)
                ElseIf lvDisciplineStatus.Items.Count <> 0 Then
                    lvDisciplineStatus.Items(intSelectedIndex).Selected = True
                    lvDisciplineStatus.EnsureVisible(intSelectedIndex)
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			

			Call SetLanguage()
			
			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
			Me.colhLevel.Text = Language._Object.getCaption(CStr(Me.colhLevel.Tag), Me.colhLevel.Text)
			Me.colhStatusType.Text = Language._Object.getCaption(CStr(Me.colhStatusType.Tag), Me.colhStatusType.Text)


        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Discipline Status from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "You cannot Delete this Discipline Status. Reason : It is predefined.")
			Language.setMessage(mstrModuleName, 3, "Sorry, You cannot delete this Discipline Status. Reason: This Discipline Status is in use.")
			Language.setMessage(mstrModuleName, 4, "Are you sure you want to delete this Discipline Status?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class