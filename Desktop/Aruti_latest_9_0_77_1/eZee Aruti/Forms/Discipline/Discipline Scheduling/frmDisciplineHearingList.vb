﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmDisciplineHearingList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmDisciplineHearingList"
    Private objHearingScheduleMaster As clshearing_schedule_master
    Private mdtEmployee As New DataTable
    Private mdtReference As New DataTable
#End Region

#Region " Form's Evenst "

    Private Sub frmDisciplineHearingList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Close()
    End Sub

    Private Sub frmDisciplineHearingList_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clshearing_schedule_master.SetMessages()
            clshearing_schedule_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clshearing_schedule_master,clshearing_schedule_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineHearingList_LanguageClick", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineHearingList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objHearingScheduleMaster = New clshearing_schedule_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

            Call FillCombo()

            btnNotify.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineHearingList_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objCommonMaster As New clsCommon_Master
            Dim objDscplnFileMaster As New clsDiscipline_file_master

            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE, True, "List")
            With cboCommittee
                .DisplayMember = "name"
                .ValueMember = "masterunkid"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objDscplnFileMaster.GetComboList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                                      True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", , , , , , )

            mdtEmployee = dsList.Tables("List").DefaultView.ToTable(True, "involved_employeeunkid", "employeecode", "employeename")

            With cboEmployee
                .ValueMember = "involved_employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = mdtEmployee
                .SelectedValue = 0
            End With

            mdtReference = dsList.Tables("List")
            With cboReferenceNo
                .ValueMember = "disciplinefileunkid"
                .DisplayMember = "reference_no"
                .DataSource = mdtReference
                .SelectedValue = 0
            End With

            dsList = objHearingScheduleMaster.getHearingStatus("Status", False)
            With cboStatus
                .DisplayMember = "name"
                .ValueMember = "id"
                .DataSource = dsList.Tables("Status")
                .SelectedValue = 1
            End With

            'S.SANDEEP |01-OCT-2021| -- START
            RemoveHandler cboNtfType.SelectedIndexChanged, AddressOf cboNtfType_SelectedIndexChanged
            With cboNtfType
                .Items.Clear()
                .Items.Add("")
                .Items.Add(Language.getMessage(mstrModuleName, 105, "Consolidate all hearing in a single mail to each member(s)."))
                .Items.Add(Language.getMessage(mstrModuleName, 106, "Each hearing in different mail with charge details to each member(s)."))
                .Items.Add(Language.getMessage(mstrModuleName, 107, "Each hearing in different mail without charge details to each member(s)."))
            End With
            Dim objConfig As New clsConfigOptions
            Dim iValue As String = objConfig.GetKeyValue(Company._Object._Companyunkid, "DEFAULTHEARINGNOTIFICATIONTYPE", Nothing)
            If iValue Is Nothing Then iValue = "0"
            Try
            cboNtfType.SelectedIndex = CInt(iValue)
            Catch ex As Exception
                cboNtfType.SelectedIndex = 0
            End Try
            AddHandler cboNtfType.SelectedIndexChanged, AddressOf cboNtfType_SelectedIndexChanged
            'S.SANDEEP |01-OCT-2021| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Try
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            If User._Object.Privilege._AllowToViewDisciplineHearingList = False Then Exit Sub
            'Varsha Rana (17-Oct-2017) -- End

            Dim dsList As New DataSet
            Dim strFilter As String = String.Empty

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strFilter &= "AND hrdiscipline_file_master.involved_employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboReferenceNo.SelectedValue) > 0 Then
                strFilter &= "AND hrdiscipline_file_master.disciplinefileunkid = " & CInt(cboReferenceNo.SelectedValue) & " "
            End If

            If CInt(cboCommittee.SelectedValue) > 0 Then
                strFilter &= "AND cfcommon_master.masterunkid = " & CInt(cboCommittee.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strFilter &= "AND hrhearing_schedule_master.status = " & CInt(cboStatus.SelectedValue) & " "
            End If

            If dtpHearingDateFrom.Checked = True Then
                strFilter &= "AND CONVERT(CHAR(8),hrhearing_schedule_master.hearing_date,112) >= " & eZeeDate.convertDate(dtpHearingDateFrom.Value) & " "
            End If

            If dtpHearingDateTo.Checked = True Then
                strFilter &= "AND CONVERT(CHAR(8),hrhearing_schedule_master.hearing_date,112) <= " & eZeeDate.convertDate(dtpHearingDateTo.Value) & " "
            End If

            If strFilter.Trim.Length > 0 Then
                strFilter = strFilter.Substring(3)
            End If

            dsList = objHearingScheduleMaster.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                                      True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", strFilter)
            'S.SANDEEP |01-OCT-2021| -- START
            Dim xCol As New DataColumn
            With xCol
                .DataType = GetType(System.Boolean)
                .DefaultValue = False
                .ColumnName = "objcolhCheck"
            End With
            dsList.Tables(0).Columns.Add(xCol)
            'S.SANDEEP |01-OCT-2021| -- END

            dgvHearingList.AutoGenerateColumns = False
            'S.SANDEEP |01-OCT-2021| -- START
            objdgcolhCheck.DataPropertyName = "objcolhCheck"
            'S.SANDEEP |01-OCT-2021| -- END
            dgcolhRefNo.DataPropertyName = "reference_no"
            dgcolhPersonInvolved.DataPropertyName = "person_involved"
            dgcolhHearingDate.DataPropertyName = "hearing_date"
            dgcolhHearingDate.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern
            dgcolhHearingTime.DataPropertyName = "hearning_time"
            dgcolhHearingTime.DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern
            dgcolhVenue.DataPropertyName = "hearning_venue"
            dgcolhCommittee.DataPropertyName = "committee"
            dgcolhRemark.DataPropertyName = "remark"
            dgcolhStatus.DataPropertyName = "statusname"
            objdgcolhhearingschedulemasterunkid.DataPropertyName = "hearingschedulemasterunkid"
            objdgcolhdisciplinefileunkid.DataPropertyName = "disciplinefileunkid"
            objdgcolhStatusId.DataPropertyName = "status"
            dgcolhNtfSent.DataPropertyName = "ISent"
            objdgcolhNtfSent.DataPropertyName = "iallsent"

            dgvHearingList.DataSource = dsList.Tables("List")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objbtnSearch.ShowResult(dgvHearingList.Rows.Count.ToString)
        End Try
    End Sub

    Private Sub FilterReferenceNo()
        Try
            Dim strCsvData As String = ""
            strCsvData = String.Join(",", mdtReference.AsEnumerable().Where(Function(x) x.Field(Of Integer)("involved_employeeunkid") = CInt(cboEmployee.SelectedValue)).Select(Function(y) y.Field(Of Integer)("disciplinefileunkid").ToString).ToArray())

            Dim mdtView As DataView = New DataView(mdtReference, "disciplinefileunkid IN (" & strCsvData & ") ", "", DataViewRowState.CurrentRows)

            With cboReferenceNo
                .ValueMember = "disciplinefileunkid"
                .DisplayMember = "reference_no"
                .DataSource = mdtView.ToTable
                If mdtView.ToTable.Rows.Count = 1 Then
                    .SelectedValue = mdtView.ToTable.Rows(0).Item("disciplinefileunkid")
                Else
                    .SelectedValue = 0
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FilterReferenceNo", mstrModuleName)
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges
    Private Sub SetVisibility()

        Try

            btnAdd.Enabled = User._Object.Privilege._AllowToAddDisciplineHearing
            btnEdit.Enabled = User._Object.Privilege._AllowToEditDisciplineHearing
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteDisciplineHearing
            btnMarkAsClose.Enabled = User._Object.Privilege._AllowToMarkAsClosedDisciplineHearing
          
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
    'Varsha Rana (17-Oct-2017) -- End

#End Region

#Region " Button's Events "

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim frm As New frmDisciplineHearingAddEdit
            frm.displayDialog(enAction.ADD_CONTINUE, -1)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmDisciplineHearingAddEdit
        Try
            If dgvHearingList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operations."), enMsgBoxStyle.Information)
                dgvHearingList.Focus()
                Exit Sub
            End If

            If frm.displayDialog(enAction.EDIT_ONE, CInt(dgvHearingList.SelectedRows(0).Cells(objdgcolhhearingschedulemasterunkid.Index).Value)) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim blnFlag As Boolean
        Try
            If dgvHearingList.RowCount > 0 Then

                If dgvHearingList.SelectedRows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operations."), enMsgBoxStyle.Information)
                    dgvHearingList.Focus()
                    Exit Sub
                End If

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Hearing Schedule?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                    'S.SANDEEP |11-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    Dim mdtScanAttachment As DataTable
                    Dim objScanAttachment As New clsScan_Attach_Documents
                    'objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
                    objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "", CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
                    Dim xRow = objScanAttachment._Datatable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("scanattachrefid") = enScanAttactRefId.DISCIPLINES _
                                                                                 And x.Field(Of Integer)("transactionunkid") = CInt(dgvHearingList.SelectedRows(0).Cells(objdgcolhhearingschedulemasterunkid.Index).Value) _
                                                                                 And x.Field(Of String)("form_name") = mstrModuleName)

                    If xRow.Count > 0 Then
                        mdtScanAttachment = xRow.CopyToDataTable()
                    Else
                        mdtScanAttachment = objScanAttachment._Datatable.Clone()
                    End If
                    objScanAttachment = Nothing

                    For Each dRow As DataRow In mdtScanAttachment.Rows
                        dRow.Item("isactive") = False
                        dRow.Item("AUD") = "D"
                    Next
                    'S.SANDEEP |11-NOV-2019| -- END

                    objHearingScheduleMaster._Userunkid = User._Object._Userunkid
                    objHearingScheduleMaster._Isvoid = True
                    objHearingScheduleMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objHearingScheduleMaster._Voiduserunkid = User._Object._Userunkid

                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    Dim mstrVoidReason As String = String.Empty
                    frm.displayDialog(enVoidCategoryType.DISCIPLINE, mstrVoidReason)
                    If mstrVoidReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objHearingScheduleMaster._Voidreason = mstrVoidReason
                    End If
                    frm = Nothing

                    Dim objHearingScheduleTran As New clshearing_schedule_Tran

                    'Pinkal (19-Dec-2020) -- Start
                    'Enhancement  -  Working on Discipline module for NMB.
                    'objHearingScheduleTran._Hearingschedulemasterunkid = CInt(dgvHearingList.SelectedRows(0).Cells(objdgcolhhearingschedulemasterunkid.Index).Value)
                    objHearingScheduleTran._Hearingschedulemasterunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(dgvHearingList.SelectedRows(0).Cells(objdgcolhhearingschedulemasterunkid.Index).Value)
                    'Pinkal (19-Dec-2020) -- End


                    Dim mdtTran As DataTable = objHearingScheduleTran._HeatingScheduleTranTable

                    For Each dRow As DataRow In mdtTran.Rows
                        dRow.Item("isvoid") = True
                        dRow.Item("voiduserunkid") = User._Object._Userunkid
                        dRow.Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dRow.Item("voidreason") = mstrVoidReason
                        dRow.Item("AUD") = "D"
                    Next

                    mdtTran.AcceptChanges()

                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'blnFlag = objHearingScheduleMaster.Delete(CInt(dgvHearingList.SelectedRows(0).Cells(objdgcolhhearingschedulemasterunkid.Index).Value), _
                    '                                          ConfigParameter._Object._CurrentDateAndTime, mdtTran)

                    'S.SANDEEP |11-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'blnFlag = objHearingScheduleMaster.Delete(CInt(dgvHearingList.SelectedRows(0).Cells(objdgcolhhearingschedulemasterunkid.Index).Value), _
                    '                                          ConfigParameter._Object._CurrentDateAndTime, mdtTran, Company._Object._Companyunkid)

                    'Pinkal (19-Dec-2020) -- Start
                    'Enhancement  -  Working on Discipline module for NMB.
                    'blnFlag = objHearingScheduleMaster.Delete(CInt(dgvHearingList.SelectedRows(0).Cells(objdgcolhhearingschedulemasterunkid.Index).Value), _
                    '                                          ConfigParameter._Object._CurrentDateAndTime, mdtTran, Company._Object._Companyunkid, mdtScanAttachment)

                    blnFlag = objHearingScheduleMaster.Delete(CInt(dgvHearingList.SelectedRows(0).Cells(objdgcolhhearingschedulemasterunkid.Index).Value), _
                                                              ConfigParameter._Object._CurrentDateAndTime, mdtTran, Company._Object._Companyunkid, mdtScanAttachment, ConfigParameter._Object._EmployeeAsOnDate)
                    'Pinkal (19-Dec-2020) -- End
                    
                    'S.SANDEEP |11-NOV-2019| -- END
                    
                    'Sohail (30 Nov 2017) -- End
                    If blnFlag = False Then
                        If objHearingScheduleMaster._Message <> "" Then
                            eZeeMsgBox.Show(objHearingScheduleMaster._Message, enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Else
                        Call FillList()
                    End If

                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnMarkAsClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMarkAsClose.Click
        Try
            If dgvHearingList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one row for further operations."), enMsgBoxStyle.Information)
                dgvHearingList.Focus()
                Exit Sub
            Else
                objHearingScheduleMaster._Hearingschedulemasterunkid = CInt(dgvHearingList.SelectedRows(0).Cells(objdgcolhhearingschedulemasterunkid.Index).Value)
                objHearingScheduleMaster._Status = clshearing_schedule_master.enHearingStatus.Close
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'If objHearingScheduleMaster.Update(ConfigParameter._Object._CurrentDateAndTime, Nothing) = False Then

                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                'If objHearingScheduleMaster.Update(ConfigParameter._Object._CurrentDateAndTime, Nothing, Company._Object._Companyunkid, Nothing, "") = False Then
                '    'Sohail (30 Nov 2017) -- End
                '    If objHearingScheduleMaster._Message <> "" Then
                '        eZeeMsgBox.Show(objHearingScheduleMaster._Message, enMsgBoxStyle.Information)
                '        Exit Sub
                '    End If
                'Else
                '    Call FillList()
                'End If
                If objHearingScheduleMaster.Update(ConfigParameter._Object._CurrentDateAndTime, Nothing, Company._Object._Companyunkid, Nothing, "", ConfigParameter._Object._EmployeeAsOnDate) = False Then
                    'Sohail (30 Nov 2017) -- End
                    If objHearingScheduleMaster._Message <> "" Then
                        eZeeMsgBox.Show(objHearingScheduleMaster._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Else
                    Call FillList()
                End If
                'Pinkal (19-Dec-2020) -- End

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnMarkAsClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim frm As New frmCommonSearch

            With frm
                .DisplayMember = cboEmployee.DisplayMember
                .ValueMember = cboEmployee.ValueMember
                .CodeMember = "employeecode"
                .DataSource = mdtEmployee
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                Call FilterReferenceNo()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchRefNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRefNo.Click
        Try
            Dim frm As New frmCommonSearch

            With frm
                .DisplayMember = cboReferenceNo.DisplayMember
                .ValueMember = cboReferenceNo.ValueMember
                .DataSource = CType(cboReferenceNo.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboReferenceNo.SelectedValue = frm.SelectedValue
                cboReferenceNo.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchRefNo_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchCommitte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCommitte.Click
        Try
            Dim frm As New frmCommonSearch

            With frm
                .DisplayMember = cboCommittee.DisplayMember
                .ValueMember = cboCommittee.ValueMember
                .DataSource = CType(cboCommittee.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboCommittee.SelectedValue = frm.SelectedValue
                cboCommittee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCommitte_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboReferenceNo.DataSource = mdtReference
            cboReferenceNo.SelectedValue = 0
            cboCommittee.SelectedValue = 0
            cboStatus.SelectedValue = 1
            dtpHearingDateFrom.Value = CDate(ConfigParameter._Object._CurrentDateAndTime.ToShortDateString)
            dtpHearingDateFrom.Checked = False
            dtpHearingDateTo.Value = CDate(ConfigParameter._Object._CurrentDateAndTime.ToShortDateString)
            dtpHearingDateTo.Checked = False
            dgvHearingList.DataSource = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP |13-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : DISCIPLINARY CHANGES
    Private Sub btnNotify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNotify.Click
        Dim objHearingTran As New clshearing_schedule_Tran
        Try
            If dgvHearingList.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 102, "Sorry, No hearing is scheduled in order to send notification."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim blnNoDataFlag As Boolean = False

            'S.SANDEEP |01-OCT-2021| -- START
            If cboNtfType.SelectedIndex <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 108, "Sorry, Default Notification type is not set. Please set it in order to send notification(s)."), enMsgBoxStyle.Information)
                cboNtfType.Focus()
                Exit Sub
            End If
            'S.SANDEEP |01-OCT-2021| -- END

            If dgvHearingList.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhCheck.Index).Value) = True).Count() <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 104, "Sorry, You have not checked any hearing for sending notification. Please check atleast one hearing in order to send notification."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim strCheckedMstIds As String = String.Join(",", dgvHearingList.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhCheck.Index).Value) = True).Select(Function(x) x.Cells(objdgcolhhearingschedulemasterunkid.Index).Value.ToString()).ToArray())

            If objHearingTran.NotifyMembers(cboNtfType.SelectedIndex, strCheckedMstIds, _
                                            Company._Object._Companyunkid, _
                                            enLogin_Mode.DESKTOP, _
                                            User._Object._Userunkid, _
                                            CStr(IIf(User._Object._Email = "", User._Object._Username, User._Object._Email)), _
                                            blnNoDataFlag, _
                                            ConfigParameter._Object._AllowToIncludeCaseInfoHearingNotification, _
                                            ConfigParameter._Object._AllowToNotifyChargedEmployeeOfHearing) Then
                'Hemant (24 Sep 2021) -- [ConfigParameter._Object._AllowToIncludeCaseInfoHearingNotification, ConfigParameter._Object._AllowToNotifyChargedEmployeeOfHearing ]
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Notification Sent."), enMsgBoxStyle.Information)
            Else
                If blnNoDataFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 103, "Notification sent already."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "Notification Sending failed."), enMsgBoxStyle.Information)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNotify_Click", mstrModuleName)
        Finally
            objHearingTran = Nothing
        End Try
    End Sub
    'S.SANDEEP |13-MAY-2021| -- END
    

#End Region

#Region " ComboBox's Events "

    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .DisplayMember = cboEmployee.DisplayMember
                    .ValueMember = cboEmployee.ValueMember
                    .CodeMember = "employeecode"
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                End With
                Dim ch As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = ch.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboReferenceNo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboReferenceNo.KeyPress
        Try
            If (AscW(e.KeyChar) >= 65 AndAlso AscW(e.KeyChar) <= 90) Or (AscW(e.KeyChar) >= 97 AndAlso AscW(e.KeyChar) <= 122) Or (AscW(e.KeyChar) >= 47 AndAlso AscW(e.KeyChar) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .DisplayMember = cboReferenceNo.DisplayMember
                    .ValueMember = cboReferenceNo.ValueMember
                    .DataSource = CType(cboReferenceNo.DataSource, DataTable)
                End With
                Dim ch As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = ch.ToString
                If frm.DisplayDialog Then
                    cboReferenceNo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboReferenceNo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReferenceNo_KeyPress", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP |01-OCT-2021| -- START
    Private Sub cboNtfType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNtfType.SelectedIndexChanged
        Dim objConfig As New clsConfigOptions
        Try
            objConfig._Companyunkid = Company._Object._Companyunkid
            objConfig._DefaultHearingNotificationType = cboNtfType.SelectedIndex
            objConfig.updateParam()
            objConfig.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboNtfType_SelectedIndexChanged", mstrModuleName)
        Finally
            objConfig = Nothing
        End Try
    End Sub
    'S.SANDEEP |01-OCT-2021| -- END

#End Region

    'S.SANDEEP |01-OCT-2021| -- START
#Region " Checkbox Event(s) "

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            If dgvHearingList.RowCount <= 0 Then Exit Sub
            For Each iRow As DataGridViewRow In dgvHearingList.Rows
                If iRow.Cells(objdgcolhCheck.Index).ReadOnly = False Then iRow.Cells(objdgcolhCheck.Index).Value = objChkAll.CheckState
            Next
            If objChkAll.CheckState = CheckState.Checked Then
                btnNotify.Enabled = True : btnDelete.Enabled = False : btnEdit.Enabled = False : btnMarkAsClose.Enabled = False
            Else
                btnNotify.Enabled = False : btnDelete.Enabled = True : btnEdit.Enabled = True : btnMarkAsClose.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP |01-OCT-2021| -- END

#Region " DataGrid's Events "

    'S.SANDEEP |01-OCT-2021| -- START
    Private Sub dgvHearingList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHearingList.CellContentClick, dgvHearingList.CellContentDoubleClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If dgvHearingList.RowCount <= 0 Then Exit Sub

            If Me.dgvHearingList.IsCurrentCellDirty Then
                Me.dgvHearingList.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

            Select Case e.ColumnIndex
                Case objdgcolhCheck.Index
                    If dgvHearingList.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhCheck.Index).Value) = True).Count() <= 0 Then
                        objChkAll.CheckState = CheckState.Unchecked
                        btnNotify.Enabled = False : btnDelete.Enabled = True : btnEdit.Enabled = True : btnMarkAsClose.Enabled = True
                    ElseIf dgvHearingList.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhCheck.Index).Value) = True).Count() < dgvHearingList.Rows.Cast(Of DataGridViewRow).AsEnumerable().Count() Then
                        objChkAll.CheckState = CheckState.Indeterminate
                        btnNotify.Enabled = True : btnDelete.Enabled = False : btnEdit.Enabled = False : btnMarkAsClose.Enabled = False
                    ElseIf dgvHearingList.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhCheck.Index).Value) = True).Count() = dgvHearingList.Rows.Cast(Of DataGridViewRow).AsEnumerable().Count() Then
                        objChkAll.CheckState = CheckState.Checked
                        btnNotify.Enabled = True : btnDelete.Enabled = False : btnEdit.Enabled = False : btnMarkAsClose.Enabled = False
                    End If
            End Select

            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHearingList_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvHearingList_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvHearingList.DataError

    End Sub

    Private Sub dgvHearingList_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvHearingList.DataBindingComplete
        Try
            For Each iGRow As DataGridViewRow In dgvHearingList.Rows
                If CBool(iGRow.Cells(objdgcolhNtfSent.Index).Value) = True Then
                    iGRow.Cells(objdgcolhCheck.Index).ReadOnly = True
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHearingList_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |01-OCT-2021| -- END

    Private Sub dgvHearingList_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvHearingList.SelectionChanged
        Try
            If dgvHearingList.RowCount <= 0 Then Exit Sub
            If dgvHearingList.SelectedRows.Count > 0 Then
                If objChkAll.CheckState = CheckState.Unchecked Then
                If CInt(dgvHearingList.SelectedRows(0).Cells(objdgcolhStatusId.Index).Value) = clshearing_schedule_master.enHearingStatus.Close Then
                    btnMarkAsClose.Enabled = False
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                Else
                    btnMarkAsClose.Enabled = True
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                End If
            End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvHearingList_SelectionChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnMarkAsClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnMarkAsClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnNotify.GradientBackColor = GUI._ButttonBackColor
            Me.btnNotify.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.Name, Me.lblRefNo.Text)
            Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.Name, Me.lblPersonalInvolved.Text)
            Me.lblDateTo.Text = Language._Object.getCaption(Me.lblDateTo.Name, Me.lblDateTo.Text)
            Me.lblHearingDate.Text = Language._Object.getCaption(Me.lblHearingDate.Name, Me.lblHearingDate.Text)
            Me.lblCommittee.Text = Language._Object.getCaption(Me.lblCommittee.Name, Me.lblCommittee.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.btnMarkAsClose.Text = Language._Object.getCaption(Me.btnMarkAsClose.Name, Me.btnMarkAsClose.Text)
            Me.btnNotify.Text = Language._Object.getCaption(Me.btnNotify.Name, Me.btnNotify.Text)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.lblNtfType.Text = Language._Object.getCaption(Me.lblNtfType.Name, Me.lblNtfType.Text)
            Me.dgcolhRefNo.HeaderText = Language._Object.getCaption(Me.dgcolhRefNo.Name, Me.dgcolhRefNo.HeaderText)
            Me.dgcolhPersonInvolved.HeaderText = Language._Object.getCaption(Me.dgcolhPersonInvolved.Name, Me.dgcolhPersonInvolved.HeaderText)
            Me.dgcolhHearingDate.HeaderText = Language._Object.getCaption(Me.dgcolhHearingDate.Name, Me.dgcolhHearingDate.HeaderText)
            Me.dgcolhHearingTime.HeaderText = Language._Object.getCaption(Me.dgcolhHearingTime.Name, Me.dgcolhHearingTime.HeaderText)
            Me.dgcolhVenue.HeaderText = Language._Object.getCaption(Me.dgcolhVenue.Name, Me.dgcolhVenue.HeaderText)
            Me.dgcolhCommittee.HeaderText = Language._Object.getCaption(Me.dgcolhCommittee.Name, Me.dgcolhCommittee.HeaderText)
            Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.dgcolhNtfSent.HeaderText = Language._Object.getCaption(Me.dgcolhNtfSent.Name, Me.dgcolhNtfSent.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select atleast one row for further operations.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Hearing Schedule?")
            Language.setMessage(mstrModuleName, 100, "Notification Sent.")
            Language.setMessage(mstrModuleName, 101, "Notification Sending failed.")
            Language.setMessage(mstrModuleName, 102, "Sorry, No hearing is scheduled in order to send notification.")
            Language.setMessage(mstrModuleName, 103, "Notification sent already.")
            Language.setMessage(mstrModuleName, 104, "Sorry, You have not checked any hearing for sending notification. Please check atleast one hearing in order to send notification.")
            Language.setMessage(mstrModuleName, 105, "Consolidate all hearing in a single mail to each member(s)")
            Language.setMessage(mstrModuleName, 106, "Each hearing in different mail with charges details to each member(s)")
            Language.setMessage(mstrModuleName, 107, "Each hearing in different mail without charges details to each member(s)")
            Language.setMessage(mstrModuleName, 108, "Sorry, Default Notification type is not set. Please set it in order to send notification(s).")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class