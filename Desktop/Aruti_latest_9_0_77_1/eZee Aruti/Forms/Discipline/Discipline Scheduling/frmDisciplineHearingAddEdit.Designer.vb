﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisciplineHearingAddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisciplineHearingAddEdit))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbHearingInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.tabcHearing_Details = New System.Windows.Forms.TabControl
        Me.tbpHearingDetail = New System.Windows.Forms.TabPage
        Me.dtpHearingDate = New System.Windows.Forms.DateTimePicker
        Me.lblHearingDate = New System.Windows.Forms.Label
        Me.dtpHearingTime = New System.Windows.Forms.DateTimePicker
        Me.lblHearingTime = New System.Windows.Forms.Label
        Me.lblVenue = New System.Windows.Forms.Label
        Me.txtVenue = New System.Windows.Forms.TextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.tbpCommittee = New System.Windows.Forms.TabPage
        Me.pnlData = New System.Windows.Forms.Panel
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.lnkAddMember = New System.Windows.Forms.LinkLabel
        Me.lblCommittee = New System.Windows.Forms.Label
        Me.cboCommittee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchCommitte = New eZee.Common.eZeeGradientButton
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.lnkAddAttachment = New System.Windows.Forms.LinkLabel
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.tabcDescription_Response = New System.Windows.Forms.TabControl
        Me.tabpChargeDescription = New System.Windows.Forms.TabPage
        Me.txtChargeDescription = New System.Windows.Forms.TextBox
        Me.tabpChargeCount = New System.Windows.Forms.TabPage
        Me.lvChargesCount = New eZee.Common.eZeeListView(Me.components)
        Me.colhCount = New System.Windows.Forms.ColumnHeader
        Me.colhIncident = New System.Windows.Forms.ColumnHeader
        Me.colhOffCategory = New System.Windows.Forms.ColumnHeader
        Me.colhOffence = New System.Windows.Forms.ColumnHeader
        Me.colhSeverity = New System.Windows.Forms.ColumnHeader
        Me.objcolhoffencecategoryunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhoffenceunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhfileunkid = New System.Windows.Forms.ColumnHeader
        Me.txtInterdictionDate = New System.Windows.Forms.TextBox
        Me.txtChargeDate = New System.Windows.Forms.TextBox
        Me.lblInterdictionDate = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.txtDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.txtJobTitle = New eZee.TextBox.AlphanumericTextBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblJobTitle = New System.Windows.Forms.Label
        Me.objbtnSearchRefNo = New eZee.Common.eZeeGradientButton
        Me.cboReferenceNo = New System.Windows.Forms.ComboBox
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhMembers = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDepartment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCompany = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhContactNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhcommitteetranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhhearningscheduletranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter.SuspendLayout()
        Me.gbHearingInfo.SuspendLayout()
        Me.tabcHearing_Details.SuspendLayout()
        Me.tbpHearingDetail.SuspendLayout()
        Me.tbpCommittee.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabcDescription_Response.SuspendLayout()
        Me.tabpChargeDescription.SuspendLayout()
        Me.tabpChargeCount.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 479)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(780, 55)
        Me.objFooter.TabIndex = 148
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(568, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(671, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbHearingInfo
        '
        Me.gbHearingInfo.BorderColor = System.Drawing.Color.Black
        Me.gbHearingInfo.Checked = False
        Me.gbHearingInfo.CollapseAllExceptThis = False
        Me.gbHearingInfo.CollapsedHoverImage = Nothing
        Me.gbHearingInfo.CollapsedNormalImage = Nothing
        Me.gbHearingInfo.CollapsedPressedImage = Nothing
        Me.gbHearingInfo.CollapseOnLoad = False
        Me.gbHearingInfo.Controls.Add(Me.tabcHearing_Details)
        Me.gbHearingInfo.Controls.Add(Me.lnkAddAttachment)
        Me.gbHearingInfo.Controls.Add(Me.objLine1)
        Me.gbHearingInfo.Controls.Add(Me.tabcDescription_Response)
        Me.gbHearingInfo.Controls.Add(Me.txtInterdictionDate)
        Me.gbHearingInfo.Controls.Add(Me.txtChargeDate)
        Me.gbHearingInfo.Controls.Add(Me.lblInterdictionDate)
        Me.gbHearingInfo.Controls.Add(Me.lblDate)
        Me.gbHearingInfo.Controls.Add(Me.txtDepartment)
        Me.gbHearingInfo.Controls.Add(Me.txtJobTitle)
        Me.gbHearingInfo.Controls.Add(Me.lblDepartment)
        Me.gbHearingInfo.Controls.Add(Me.lblJobTitle)
        Me.gbHearingInfo.Controls.Add(Me.objbtnSearchRefNo)
        Me.gbHearingInfo.Controls.Add(Me.cboReferenceNo)
        Me.gbHearingInfo.Controls.Add(Me.lblRefNo)
        Me.gbHearingInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbHearingInfo.Controls.Add(Me.cboEmployee)
        Me.gbHearingInfo.Controls.Add(Me.lblPersonalInvolved)
        Me.gbHearingInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbHearingInfo.ExpandedHoverImage = Nothing
        Me.gbHearingInfo.ExpandedNormalImage = Nothing
        Me.gbHearingInfo.ExpandedPressedImage = Nothing
        Me.gbHearingInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHearingInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbHearingInfo.HeaderHeight = 25
        Me.gbHearingInfo.HeaderMessage = ""
        Me.gbHearingInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHearingInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbHearingInfo.HeightOnCollapse = 0
        Me.gbHearingInfo.LeftTextSpace = 0
        Me.gbHearingInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbHearingInfo.Name = "gbHearingInfo"
        Me.gbHearingInfo.OpenHeight = 300
        Me.gbHearingInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbHearingInfo.ShowBorder = True
        Me.gbHearingInfo.ShowCheckBox = False
        Me.gbHearingInfo.ShowCollapseButton = False
        Me.gbHearingInfo.ShowDefaultBorderColor = True
        Me.gbHearingInfo.ShowDownButton = False
        Me.gbHearingInfo.ShowHeader = True
        Me.gbHearingInfo.Size = New System.Drawing.Size(780, 479)
        Me.gbHearingInfo.TabIndex = 149
        Me.gbHearingInfo.Temp = 0
        Me.gbHearingInfo.Text = "Scheduling Information"
        Me.gbHearingInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabcHearing_Details
        '
        Me.tabcHearing_Details.Controls.Add(Me.tbpHearingDetail)
        Me.tabcHearing_Details.Controls.Add(Me.tbpCommittee)
        Me.tabcHearing_Details.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcHearing_Details.Location = New System.Drawing.Point(12, 285)
        Me.tabcHearing_Details.Name = "tabcHearing_Details"
        Me.tabcHearing_Details.SelectedIndex = 0
        Me.tabcHearing_Details.Size = New System.Drawing.Size(756, 191)
        Me.tabcHearing_Details.TabIndex = 232
        '
        'tbpHearingDetail
        '
        Me.tbpHearingDetail.Controls.Add(Me.dtpHearingDate)
        Me.tbpHearingDetail.Controls.Add(Me.lblHearingDate)
        Me.tbpHearingDetail.Controls.Add(Me.dtpHearingTime)
        Me.tbpHearingDetail.Controls.Add(Me.lblHearingTime)
        Me.tbpHearingDetail.Controls.Add(Me.lblVenue)
        Me.tbpHearingDetail.Controls.Add(Me.txtVenue)
        Me.tbpHearingDetail.Controls.Add(Me.lblRemark)
        Me.tbpHearingDetail.Controls.Add(Me.txtRemark)
        Me.tbpHearingDetail.Location = New System.Drawing.Point(4, 22)
        Me.tbpHearingDetail.Name = "tbpHearingDetail"
        Me.tbpHearingDetail.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpHearingDetail.Size = New System.Drawing.Size(748, 165)
        Me.tbpHearingDetail.TabIndex = 0
        Me.tbpHearingDetail.Text = "Hearing Details"
        Me.tbpHearingDetail.UseVisualStyleBackColor = True
        '
        'dtpHearingDate
        '
        Me.dtpHearingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHearingDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHearingDate.Location = New System.Drawing.Point(104, 9)
        Me.dtpHearingDate.Name = "dtpHearingDate"
        Me.dtpHearingDate.Size = New System.Drawing.Size(125, 21)
        Me.dtpHearingDate.TabIndex = 172
        '
        'lblHearingDate
        '
        Me.lblHearingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHearingDate.Location = New System.Drawing.Point(6, 11)
        Me.lblHearingDate.Name = "lblHearingDate"
        Me.lblHearingDate.Size = New System.Drawing.Size(92, 17)
        Me.lblHearingDate.TabIndex = 171
        Me.lblHearingDate.Text = "Hearing Date"
        Me.lblHearingDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpHearingTime
        '
        Me.dtpHearingTime.CustomFormat = "hh:mm tt"
        Me.dtpHearingTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHearingTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpHearingTime.Location = New System.Drawing.Point(313, 9)
        Me.dtpHearingTime.Name = "dtpHearingTime"
        Me.dtpHearingTime.ShowUpDown = True
        Me.dtpHearingTime.Size = New System.Drawing.Size(86, 21)
        Me.dtpHearingTime.TabIndex = 173
        '
        'lblHearingTime
        '
        Me.lblHearingTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHearingTime.Location = New System.Drawing.Point(235, 11)
        Me.lblHearingTime.Name = "lblHearingTime"
        Me.lblHearingTime.Size = New System.Drawing.Size(72, 17)
        Me.lblHearingTime.TabIndex = 174
        Me.lblHearingTime.Text = "Time"
        Me.lblHearingTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVenue
        '
        Me.lblVenue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVenue.Location = New System.Drawing.Point(6, 38)
        Me.lblVenue.Name = "lblVenue"
        Me.lblVenue.Size = New System.Drawing.Size(92, 17)
        Me.lblVenue.TabIndex = 175
        Me.lblVenue.Text = "Venue"
        Me.lblVenue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVenue
        '
        Me.txtVenue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVenue.Location = New System.Drawing.Point(104, 36)
        Me.txtVenue.Name = "txtVenue"
        Me.txtVenue.Size = New System.Drawing.Size(295, 21)
        Me.txtVenue.TabIndex = 176
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(6, 65)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(92, 17)
        Me.lblRemark.TabIndex = 178
        Me.lblRemark.Text = "Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemark
        '
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.Location = New System.Drawing.Point(104, 63)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(638, 96)
        Me.txtRemark.TabIndex = 179
        '
        'tbpCommittee
        '
        Me.tbpCommittee.Controls.Add(Me.pnlData)
        Me.tbpCommittee.Controls.Add(Me.lnkAddMember)
        Me.tbpCommittee.Controls.Add(Me.lblCommittee)
        Me.tbpCommittee.Controls.Add(Me.cboCommittee)
        Me.tbpCommittee.Controls.Add(Me.objbtnSearchCommitte)
        Me.tbpCommittee.Controls.Add(Me.objchkAll)
        Me.tbpCommittee.Location = New System.Drawing.Point(4, 22)
        Me.tbpCommittee.Name = "tbpCommittee"
        Me.tbpCommittee.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpCommittee.Size = New System.Drawing.Size(748, 165)
        Me.tbpCommittee.TabIndex = 1
        Me.tbpCommittee.Text = "Committee"
        Me.tbpCommittee.UseVisualStyleBackColor = True
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.dgvData)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(18, 38)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(724, 121)
        Me.pnlData.TabIndex = 227
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.White
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhCheck, Me.objdgcolhEdit, Me.objdgcolhDelete, Me.dgcolhMembers, Me.dgcolhDepartment, Me.dgcolhCompany, Me.dgcolhContactNo, Me.dgcolhEmail, Me.objdgcolhcommitteetranunkid, Me.objdgcolhhearningscheduletranunkid, Me.objdgcolhGUID})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.Size = New System.Drawing.Size(724, 121)
        Me.dgvData.TabIndex = 223
        '
        'lnkAddMember
        '
        Me.lnkAddMember.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAddMember.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAddMember.Location = New System.Drawing.Point(652, 13)
        Me.lnkAddMember.Name = "lnkAddMember"
        Me.lnkAddMember.Size = New System.Drawing.Size(83, 17)
        Me.lnkAddMember.TabIndex = 226
        Me.lnkAddMember.TabStop = True
        Me.lnkAddMember.Text = "Add New"
        Me.lnkAddMember.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCommittee
        '
        Me.lblCommittee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCommittee.Location = New System.Drawing.Point(15, 13)
        Me.lblCommittee.Name = "lblCommittee"
        Me.lblCommittee.Size = New System.Drawing.Size(89, 17)
        Me.lblCommittee.TabIndex = 180
        Me.lblCommittee.Text = "Committee"
        Me.lblCommittee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCommittee
        '
        Me.cboCommittee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCommittee.DropDownWidth = 400
        Me.cboCommittee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCommittee.FormattingEnabled = True
        Me.cboCommittee.Location = New System.Drawing.Point(110, 11)
        Me.cboCommittee.Name = "cboCommittee"
        Me.cboCommittee.Size = New System.Drawing.Size(297, 21)
        Me.cboCommittee.TabIndex = 181
        '
        'objbtnSearchCommitte
        '
        Me.objbtnSearchCommitte.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCommitte.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCommitte.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCommitte.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCommitte.BorderSelected = False
        Me.objbtnSearchCommitte.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCommitte.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchCommitte.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCommitte.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCommitte.Location = New System.Drawing.Point(413, 11)
        Me.objbtnSearchCommitte.Name = "objbtnSearchCommitte"
        Me.objbtnSearchCommitte.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCommitte.TabIndex = 182
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkAll.Location = New System.Drawing.Point(374, 15)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 184
        Me.objchkAll.UseVisualStyleBackColor = True
        Me.objchkAll.Visible = False
        '
        'lnkAddAttachment
        '
        Me.lnkAddAttachment.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAddAttachment.Location = New System.Drawing.Point(632, 115)
        Me.lnkAddAttachment.Name = "lnkAddAttachment"
        Me.lnkAddAttachment.Size = New System.Drawing.Size(132, 17)
        Me.lnkAddAttachment.TabIndex = 227
        Me.lnkAddAttachment.TabStop = True
        Me.lnkAddAttachment.Text = "Add Attachment"
        Me.lnkAddAttachment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objLine1
        '
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(12, 274)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(756, 9)
        Me.objLine1.TabIndex = 177
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tabcDescription_Response
        '
        Me.tabcDescription_Response.Controls.Add(Me.tabpChargeDescription)
        Me.tabcDescription_Response.Controls.Add(Me.tabpChargeCount)
        Me.tabcDescription_Response.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcDescription_Response.Location = New System.Drawing.Point(12, 115)
        Me.tabcDescription_Response.Name = "tabcDescription_Response"
        Me.tabcDescription_Response.SelectedIndex = 0
        Me.tabcDescription_Response.Size = New System.Drawing.Size(756, 156)
        Me.tabcDescription_Response.TabIndex = 170
        '
        'tabpChargeDescription
        '
        Me.tabpChargeDescription.Controls.Add(Me.txtChargeDescription)
        Me.tabpChargeDescription.Location = New System.Drawing.Point(4, 22)
        Me.tabpChargeDescription.Name = "tabpChargeDescription"
        Me.tabpChargeDescription.Size = New System.Drawing.Size(748, 130)
        Me.tabpChargeDescription.TabIndex = 0
        Me.tabpChargeDescription.Text = "General Charge Description"
        Me.tabpChargeDescription.UseVisualStyleBackColor = True
        '
        'txtChargeDescription
        '
        Me.txtChargeDescription.BackColor = System.Drawing.Color.White
        Me.txtChargeDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtChargeDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChargeDescription.Location = New System.Drawing.Point(0, 0)
        Me.txtChargeDescription.Multiline = True
        Me.txtChargeDescription.Name = "txtChargeDescription"
        Me.txtChargeDescription.ReadOnly = True
        Me.txtChargeDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtChargeDescription.Size = New System.Drawing.Size(748, 130)
        Me.txtChargeDescription.TabIndex = 119
        '
        'tabpChargeCount
        '
        Me.tabpChargeCount.Controls.Add(Me.lvChargesCount)
        Me.tabpChargeCount.Location = New System.Drawing.Point(4, 22)
        Me.tabpChargeCount.Name = "tabpChargeCount"
        Me.tabpChargeCount.Size = New System.Drawing.Size(748, 130)
        Me.tabpChargeCount.TabIndex = 1
        Me.tabpChargeCount.Text = "Charge Count(s)"
        Me.tabpChargeCount.UseVisualStyleBackColor = True
        '
        'lvChargesCount
        '
        Me.lvChargesCount.BackColorOnChecked = True
        Me.lvChargesCount.ColumnHeaders = Nothing
        Me.lvChargesCount.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCount, Me.colhIncident, Me.colhOffCategory, Me.colhOffence, Me.colhSeverity, Me.objcolhoffencecategoryunkid, Me.objcolhoffenceunkid, Me.objcolhGUID, Me.objcolhfileunkid})
        Me.lvChargesCount.CompulsoryColumns = ""
        Me.lvChargesCount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvChargesCount.FullRowSelect = True
        Me.lvChargesCount.GridLines = True
        Me.lvChargesCount.GroupingColumn = Nothing
        Me.lvChargesCount.HideSelection = False
        Me.lvChargesCount.Location = New System.Drawing.Point(0, 0)
        Me.lvChargesCount.MinColumnWidth = 50
        Me.lvChargesCount.MultiSelect = False
        Me.lvChargesCount.Name = "lvChargesCount"
        Me.lvChargesCount.OptionalColumns = ""
        Me.lvChargesCount.ShowItemToolTips = True
        Me.lvChargesCount.ShowMoreItem = False
        Me.lvChargesCount.ShowSaveItem = False
        Me.lvChargesCount.ShowSelectAll = True
        Me.lvChargesCount.ShowSizeAllColumnsToFit = True
        Me.lvChargesCount.Size = New System.Drawing.Size(748, 130)
        Me.lvChargesCount.Sortable = True
        Me.lvChargesCount.TabIndex = 1
        Me.lvChargesCount.UseCompatibleStateImageBehavior = False
        Me.lvChargesCount.View = System.Windows.Forms.View.Details
        '
        'colhCount
        '
        Me.colhCount.Tag = "colhCount"
        Me.colhCount.Text = "Count"
        Me.colhCount.Width = 45
        '
        'colhIncident
        '
        Me.colhIncident.DisplayIndex = 3
        Me.colhIncident.Tag = "colhIncident"
        Me.colhIncident.Text = "Incident"
        Me.colhIncident.Width = 255
        '
        'colhOffCategory
        '
        Me.colhOffCategory.DisplayIndex = 1
        Me.colhOffCategory.Tag = "colhOffCategory"
        Me.colhOffCategory.Text = "Offence Category"
        Me.colhOffCategory.Width = 145
        '
        'colhOffence
        '
        Me.colhOffence.DisplayIndex = 2
        Me.colhOffence.Tag = "colhOffence"
        Me.colhOffence.Text = "Offence Description"
        Me.colhOffence.Width = 195
        '
        'colhSeverity
        '
        Me.colhSeverity.Tag = "colhSeverity"
        Me.colhSeverity.Text = "Severity"
        Me.colhSeverity.Width = 55
        '
        'objcolhoffencecategoryunkid
        '
        Me.objcolhoffencecategoryunkid.Tag = "objcolhoffencecategoryunkid"
        Me.objcolhoffencecategoryunkid.Width = 0
        '
        'objcolhoffenceunkid
        '
        Me.objcolhoffenceunkid.Tag = "objcolhoffenceunkid"
        Me.objcolhoffenceunkid.Width = 0
        '
        'objcolhGUID
        '
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Width = 0
        '
        'objcolhfileunkid
        '
        Me.objcolhfileunkid.Tag = "objcolhfileunkid"
        Me.objcolhfileunkid.Width = 0
        '
        'txtInterdictionDate
        '
        Me.txtInterdictionDate.BackColor = System.Drawing.Color.White
        Me.txtInterdictionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterdictionDate.Location = New System.Drawing.Point(536, 88)
        Me.txtInterdictionDate.Name = "txtInterdictionDate"
        Me.txtInterdictionDate.ReadOnly = True
        Me.txtInterdictionDate.Size = New System.Drawing.Size(205, 21)
        Me.txtInterdictionDate.TabIndex = 169
        '
        'txtChargeDate
        '
        Me.txtChargeDate.BackColor = System.Drawing.Color.White
        Me.txtChargeDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChargeDate.Location = New System.Drawing.Point(536, 61)
        Me.txtChargeDate.Name = "txtChargeDate"
        Me.txtChargeDate.ReadOnly = True
        Me.txtChargeDate.Size = New System.Drawing.Size(205, 21)
        Me.txtChargeDate.TabIndex = 168
        '
        'lblInterdictionDate
        '
        Me.lblInterdictionDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterdictionDate.Location = New System.Drawing.Point(438, 90)
        Me.lblInterdictionDate.Name = "lblInterdictionDate"
        Me.lblInterdictionDate.Size = New System.Drawing.Size(92, 17)
        Me.lblInterdictionDate.TabIndex = 167
        Me.lblInterdictionDate.Text = "Interdiction Date "
        Me.lblInterdictionDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(438, 63)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(92, 17)
        Me.lblDate.TabIndex = 166
        Me.lblDate.Text = "Charge Date"
        Me.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDepartment
        '
        Me.txtDepartment.BackColor = System.Drawing.SystemColors.Window
        Me.txtDepartment.Flags = 0
        Me.txtDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDepartment.Location = New System.Drawing.Point(110, 88)
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.ReadOnly = True
        Me.txtDepartment.Size = New System.Drawing.Size(295, 21)
        Me.txtDepartment.TabIndex = 165
        '
        'txtJobTitle
        '
        Me.txtJobTitle.BackColor = System.Drawing.SystemColors.Window
        Me.txtJobTitle.Flags = 0
        Me.txtJobTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobTitle.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtJobTitle.Location = New System.Drawing.Point(110, 61)
        Me.txtJobTitle.Name = "txtJobTitle"
        Me.txtJobTitle.ReadOnly = True
        Me.txtJobTitle.Size = New System.Drawing.Size(295, 21)
        Me.txtJobTitle.TabIndex = 164
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(12, 90)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(92, 17)
        Me.lblDepartment.TabIndex = 163
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblJobTitle
        '
        Me.lblJobTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobTitle.Location = New System.Drawing.Point(12, 63)
        Me.lblJobTitle.Name = "lblJobTitle"
        Me.lblJobTitle.Size = New System.Drawing.Size(92, 17)
        Me.lblJobTitle.TabIndex = 162
        Me.lblJobTitle.Text = "Job Title"
        Me.lblJobTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchRefNo
        '
        Me.objbtnSearchRefNo.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchRefNo.BorderSelected = False
        Me.objbtnSearchRefNo.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchRefNo.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchRefNo.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchRefNo.Location = New System.Drawing.Point(747, 34)
        Me.objbtnSearchRefNo.Name = "objbtnSearchRefNo"
        Me.objbtnSearchRefNo.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchRefNo.TabIndex = 161
        '
        'cboReferenceNo
        '
        Me.cboReferenceNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboReferenceNo.DropDownWidth = 400
        Me.cboReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReferenceNo.FormattingEnabled = True
        Me.cboReferenceNo.Location = New System.Drawing.Point(536, 34)
        Me.cboReferenceNo.Name = "cboReferenceNo"
        Me.cboReferenceNo.Size = New System.Drawing.Size(205, 21)
        Me.cboReferenceNo.TabIndex = 160
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(438, 36)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(92, 17)
        Me.lblRefNo.TabIndex = 159
        Me.lblRefNo.Text = "Reference No."
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(411, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 158
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(110, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(295, 21)
        Me.cboEmployee.TabIndex = 157
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(12, 36)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(92, 17)
        Me.lblPersonalInvolved.TabIndex = 156
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Members"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Department"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 150
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Company"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Contact No"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhcommitteetranunkid"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhDisciplineMembersTranUnkid"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhGUID"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Frozen = True
        Me.objcolhCheck.HeaderText = ""
        Me.objcolhCheck.Name = "objcolhCheck"
        Me.objcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhCheck.Visible = False
        Me.objcolhCheck.Width = 25
        '
        'objdgcolhEdit
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle1.NullValue = Nothing
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhEdit.DefaultCellStyle = DataGridViewCellStyle1
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle2.NullValue = Nothing
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        Me.objdgcolhDelete.DefaultCellStyle = DataGridViewCellStyle2
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'dgcolhMembers
        '
        Me.dgcolhMembers.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhMembers.HeaderText = "Members"
        Me.dgcolhMembers.Name = "dgcolhMembers"
        Me.dgcolhMembers.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDepartment
        '
        Me.dgcolhDepartment.HeaderText = "Department"
        Me.dgcolhDepartment.Name = "dgcolhDepartment"
        Me.dgcolhDepartment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhDepartment.Width = 150
        '
        'dgcolhCompany
        '
        Me.dgcolhCompany.HeaderText = "Company"
        Me.dgcolhCompany.Name = "dgcolhCompany"
        Me.dgcolhCompany.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhContactNo
        '
        Me.dgcolhContactNo.HeaderText = "Contact No"
        Me.dgcolhContactNo.Name = "dgcolhContactNo"
        Me.dgcolhContactNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEmail
        '
        Me.dgcolhEmail.HeaderText = "Email"
        Me.dgcolhEmail.Name = "dgcolhEmail"
        Me.dgcolhEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEmail.Width = 150
        '
        'objdgcolhcommitteetranunkid
        '
        Me.objdgcolhcommitteetranunkid.HeaderText = "objdgcolhcommitteetranunkid"
        Me.objdgcolhcommitteetranunkid.Name = "objdgcolhcommitteetranunkid"
        Me.objdgcolhcommitteetranunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhcommitteetranunkid.Visible = False
        '
        'objdgcolhhearningscheduletranunkid
        '
        Me.objdgcolhhearningscheduletranunkid.HeaderText = "objdgcolhDisciplineMembersTranUnkid"
        Me.objdgcolhhearningscheduletranunkid.Name = "objdgcolhhearningscheduletranunkid"
        Me.objdgcolhhearningscheduletranunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhhearningscheduletranunkid.Visible = False
        '
        'objdgcolhGUID
        '
        Me.objdgcolhGUID.HeaderText = "objdgcolhGUID"
        Me.objdgcolhGUID.Name = "objdgcolhGUID"
        Me.objdgcolhGUID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhGUID.Visible = False
        '
        'frmDisciplineHearingAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(780, 534)
        Me.Controls.Add(Me.gbHearingInfo)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDisciplineHearingAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Discipline Hearing Schedule Add/Edit"
        Me.objFooter.ResumeLayout(False)
        Me.gbHearingInfo.ResumeLayout(False)
        Me.gbHearingInfo.PerformLayout()
        Me.tabcHearing_Details.ResumeLayout(False)
        Me.tbpHearingDetail.ResumeLayout(False)
        Me.tbpHearingDetail.PerformLayout()
        Me.tbpCommittee.ResumeLayout(False)
        Me.tbpCommittee.PerformLayout()
        Me.pnlData.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabcDescription_Response.ResumeLayout(False)
        Me.tabpChargeDescription.ResumeLayout(False)
        Me.tabpChargeDescription.PerformLayout()
        Me.tabpChargeCount.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbHearingInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchCommitte As eZee.Common.eZeeGradientButton
    Friend WithEvents cboCommittee As System.Windows.Forms.ComboBox
    Friend WithEvents lblCommittee As System.Windows.Forms.Label
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents txtVenue As System.Windows.Forms.TextBox
    Friend WithEvents lblVenue As System.Windows.Forms.Label
    Friend WithEvents lblHearingTime As System.Windows.Forms.Label
    Friend WithEvents dtpHearingTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpHearingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblHearingDate As System.Windows.Forms.Label
    Friend WithEvents tabcDescription_Response As System.Windows.Forms.TabControl
    Friend WithEvents tabpChargeDescription As System.Windows.Forms.TabPage
    Friend WithEvents txtChargeDescription As System.Windows.Forms.TextBox
    Friend WithEvents tabpChargeCount As System.Windows.Forms.TabPage
    Friend WithEvents lvChargesCount As eZee.Common.eZeeListView
    Friend WithEvents colhCount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIncident As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhOffCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhOffence As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSeverity As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhoffencecategoryunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhoffenceunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhfileunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtInterdictionDate As System.Windows.Forms.TextBox
    Friend WithEvents txtChargeDate As System.Windows.Forms.TextBox
    Friend WithEvents lblInterdictionDate As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents txtDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtJobTitle As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblJobTitle As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchRefNo As eZee.Common.eZeeGradientButton
    Friend WithEvents cboReferenceNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents lnkAddAttachment As System.Windows.Forms.LinkLabel
    Friend WithEvents tabcHearing_Details As System.Windows.Forms.TabControl
    Friend WithEvents tbpHearingDetail As System.Windows.Forms.TabPage
    Friend WithEvents tbpCommittee As System.Windows.Forms.TabPage
    Friend WithEvents lnkAddMember As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhMembers As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDepartment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCompany As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhContactNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhcommitteetranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhhearningscheduletranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
