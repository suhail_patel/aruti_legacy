﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAddHearingMembers

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAddHearingMembers"
    Private menAction As enAction = enAction.ADD_ONE
    Private mblnCancel As Boolean = True
    Private mdtMemTran As DataTable
    Private mintHearningscheduletranunkid As Integer = -1
    Private mintHearingschedulemasterunkid As Integer = -1
    Private mintDisciplineFileTranUnkid As Integer = -1
    Private mintCommitteeTranunkid As Integer = -1
    Private mstrGUID As String = String.Empty
    Dim mstrAdvanceFilter As String = ""
    Dim mintDisciplineCommitteeId As Integer = 0
    Private mintInvolvedEmpId As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction, ByVal intDisciplineMembersTranUnkid As Integer, ByVal intDisciplineProceedingMasterUnkid As Integer, _
                                  ByVal intCommitteeTranUnkid As Integer, ByVal strGUID As String, _
                                  ByRef dtMembers As DataTable, ByVal intDisciplineCommitteeId As Integer, ByVal intInvolvedEmpId As Integer) As Boolean
        Try
            menAction = eAction
            mintHearningscheduletranunkid = intDisciplineMembersTranUnkid
            mintHearingschedulemasterunkid = intDisciplineProceedingMasterUnkid
            mintCommitteeTranunkid = intCommitteeTranUnkid
            mstrGUID = strGUID
            mdtMemTran = dtMembers
            mintDisciplineCommitteeId = intDisciplineCommitteeId
            mintInvolvedEmpId = intInvolvedEmpId

            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmAddHearingMembers_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.A Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddHearingMembers_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAddHearingMembers_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed

    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_Committee.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_Committee"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmAddHearingMembers_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()

            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then

                radEmployee.Enabled = False : radOthers.Enabled = False

                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                lnkAllocation.Enabled = False : objbtnReset.Enabled = False
                Dim dRow() As DataRow = Nothing
                If mintHearningscheduletranunkid <= 0 Then
                    dRow = mdtMemTran.Select("GUID = '" & mstrGUID & "' AND AUD <> 'D'")
                Else
                    dRow = mdtMemTran.Select("hearningscheduletranunkid = '" & mintHearningscheduletranunkid & "' AND AUD <> 'D'")
                End If

                'Pinkal (19-Dec-2020) -- End



                If dRow.Count > 0 Then
                    If CInt(dRow(0).Item("employeeunkid")) <= 0 Then
                        RemoveHandler radOthers.CheckedChanged, AddressOf radEmployee_CheckedChanged
                        radOthers.Checked = True
                        AddHandler radOthers.CheckedChanged, AddressOf radEmployee_CheckedChanged
                        txtName.Text = dRow(0).Item("members").ToString
                        txtPosition.Text = dRow(0).Item("department").ToString
                        txtComany.Text = dRow(0).Item("company").ToString
                        'Pinkal (22-Jan-2021) -- Start
                        'Enhancement NMB - Working Discipline changes required by NMB.
                        'txtTrainersContactNo.Text = dRow(0).Item("contactno").ToString
                        'txtEmail.Text = dRow(0).Item("email").ToString
                        txtTrainersContactNo.Text = dRow(0).Item("contactno").ToString
                        txtEmail.Text = dRow(0).Item("ex_email").ToString
                        'Pinkal (22-Jan-2021) -- End



                    ElseIf CInt(dRow(0).Item("employeeunkid")) > 0 Then
                        RemoveHandler radEmployee.CheckedChanged, AddressOf radEmployee_CheckedChanged
                        radEmployee.Checked = True
                        AddHandler radEmployee.CheckedChanged, AddressOf radEmployee_CheckedChanged
                        cboEmployee.SelectedValue = CStr(dRow(0).Item("employeeunkid"))
                        objlblDepartmentValue.Text = dRow(0).Item("department").ToString
                        objlblCompanyValue.Text = dRow(0).Item("company").ToString
                        'Pinkal (22-Jan-2021) -- Start
                        'Enhancement NMB - Working Discipline changes required by NMB.
                        'objlblEmployeeContactNo.Text = dRow(0).Item("contactno").ToString
                        ' objlblEmployeeEmail.Text = dRow(0).Item("email").ToString
                        objlblEmployeeContactNo.Text = dRow(0).Item("contactno").ToString
                        objlblEmployeeEmail.Text = dRow(0).Item("email").ToString
                        'Pinkal (22-Jan-2021) -- End


                    End If
                End If
                'Pinkal (22-Jan-2021) -- Start
                'Enhancement NMB - Working Discipline changes required by NMB.
                'Call radEmployee_CheckedChanged(New Object, New EventArgs())
                'Pinkal (22-Jan-2021) -- End


            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAddHearingMembers_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objEmpMaster As New clsEmployee_Master

            Dim StrFilter As String = ""
            If mintInvolvedEmpId > 0 Then
                StrFilter = " hremployee_master.employeeunkid <> " & mintInvolvedEmpId & " "
            End If
            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrFilter &= " AND " & mstrAdvanceFilter & " "
            End If

            dsList = objEmpMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, , , , , , , , , , , , , , , StrFilter)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillMemberTran()
        Try
            Select Case menAction
                Case enAction.ADD_ONE

                    Dim dRow As DataRow = Nothing

                    dRow = mdtMemTran.NewRow
                    dRow("hearningscheduletranunkid") = -1
                    dRow("hearingschedulemasterunkid") = mintHearingschedulemasterunkid
                    dRow("committeetranunkid") = mintCommitteeTranunkid
                    dRow("committeemasterunkid") = mintDisciplineCommitteeId
                    dRow("isvoid") = False
                    dRow("voiduserunkid") = -1
                    dRow("voiddatetime") = DBNull.Value
                    dRow("voidreason") = ""
                    dRow("AUD") = "A"
                    dRow("GUID") = Guid.NewGuid.ToString


                    If radEmployee.Checked = True Then
                        dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                        dRow("members") = cboEmployee.Text
                        dRow("member_name") = cboEmployee.Text
                        dRow("department") = objlblDepartmentValue.Text
                        dRow("company") = objlblCompanyValue.Text
                        dRow("contactno") = objlblEmployeeContactNo.Text
                        dRow("email") = objlblEmployeeEmail.Text
                    ElseIf radOthers.Checked = True Then
                        dRow("employeeunkid") = -1
                        dRow("members") = txtName.Text.Trim
                        dRow("member_name") = txtName.Text
                        dRow("department") = txtPosition.Text.Trim
                        dRow("company") = txtComany.Text.Trim
                        dRow("contactno") = txtTrainersContactNo.Text.Trim
                        dRow("email") = txtEmail.Text.Trim
                        dRow("ex_name") = txtName.Text.Trim
                        dRow("ex_company") = txtComany.Text.Trim
                        dRow("ex_department") = txtPosition.Text.Trim
                        dRow("ex_contactno") = txtTrainersContactNo.Text.Trim
                        dRow("ex_email") = txtEmail.Text.Trim
                    End If
                    mdtMemTran.Rows.Add(dRow)
                    mblnCancel = False

                Case enAction.EDIT_ONE

                    Dim dRow() As DataRow

                    If mintHearningscheduletranunkid > 0 Then
                        dRow = mdtMemTran.Select("hearningscheduletranunkid = " & mintHearningscheduletranunkid & " AND AUD <> 'D'")
                    Else
                        dRow = mdtMemTran.Select("GUID = '" & mstrGUID & "' AND AUD <> 'D'")
                    End If

                    If dRow.Count > 0 Then
                        dRow(0).Item("hearningscheduletranunkid") = IIf(mintHearningscheduletranunkid > 0, mintHearningscheduletranunkid, dRow(0).Item("hearningscheduletranunkid"))
                        dRow(0).Item("hearingschedulemasterunkid") = mintHearingschedulemasterunkid
                        dRow(0).Item("committeetranunkid") = IIf(mintCommitteeTranunkid > 0, mintCommitteeTranunkid, dRow(0).Item("committeetranunkid"))
                        dRow(0).Item("committeemasterunkid") = mintDisciplineCommitteeId
                        dRow(0).Item("isvoid") = False
                        dRow(0).Item("voiduserunkid") = -1
                        dRow(0).Item("voiddatetime") = DBNull.Value
                        dRow(0).Item("voidreason") = ""
                        dRow(0).Item("AUD") = IIf(dRow(0)("AUD").ToString <> "A", "U", dRow(0)("AUD").ToString)
                        dRow(0).Item("GUID") = Guid.NewGuid.ToString

                        If radEmployee.Checked = True Then
                            dRow(0).Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                            dRow(0).Item("members") = cboEmployee.Text
                            dRow(0).Item("member_name") = cboEmployee.Text
                            dRow(0).Item("department") = objlblDepartmentValue.Text
                            dRow(0).Item("company") = objlblCompanyValue.Text
                            dRow(0).Item("contactno") = objlblEmployeeContactNo.Text
                            dRow(0).Item("email") = objlblEmployeeEmail.Text
                        ElseIf radOthers.Checked = True Then
                            dRow(0).Item("employeeunkid") = -1
                            dRow(0).Item("members") = txtName.Text.Trim
                            dRow(0).Item("member_name") = txtName.Text
                            dRow(0).Item("department") = txtPosition.Text.Trim
                            dRow(0).Item("company") = txtComany.Text.Trim
                            dRow(0).Item("contactno") = txtTrainersContactNo.Text.Trim
                            dRow(0).Item("email") = txtEmail.Text.Trim

                            dRow(0)("ex_name") = txtName.Text.Trim
                            dRow(0)("ex_company") = txtComany.Text.Trim
                            dRow(0)("ex_department") = txtPosition.Text.Trim
                            dRow(0)("ex_contactno") = txtTrainersContactNo.Text.Trim
                            dRow(0)("ex_email") = txtEmail.Text.Trim

                        End If
                        dRow(0).AcceptChanges()
                    End If
                    mblnCancel = False

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMemberTran", mstrModuleName)
        End Try
    End Sub

    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtComany.BackColor = GUI.ColorOptional
            txtPosition.BackColor = GUI.ColorOptional
            txtTrainersContactNo.BackColor = GUI.ColorOptional
            txtEmail.BackColor = GUI.ColorOptional

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If radOthers.Checked Then
                If txtName.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Name is mandatory inforamation. Please enter Name."), enMsgBoxStyle.Information)
                    txtName.Focus()
                    Return False
                End If

                If txtEmail.Text.Trim.Length > 0 Then
                    Dim expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
                    If expression.IsMatch(txtEmail.Text.Trim) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Invalid Email. Please Enter Valid Email"), enMsgBoxStyle.Information)
                        txtEmail.Focus()
                        Return False
                    End If
                End If

            ElseIf radEmployee.Checked Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee is mandatory inforamation. Please select Employee."), enMsgBoxStyle.Information)
                    cboEmployee.Focus()
                    Return False
                End If
            End If

            If menAction = enAction.ADD_ONE Then
                If radEmployee.Checked = True Then
                    Dim dataRow() As DataRow = mdtMemTran.Select("employeeunkid = " & CInt(cboEmployee.SelectedValue) & "")
                    If dataRow.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot Add this Employee. Reason: Employee is already added."), enMsgBoxStyle.Information)
                        cboEmployee.Focus()
                        Return False
                    End If
                ElseIf radOthers.Checked = True Then
                    Dim dataRow() As DataRow = mdtMemTran.Select("members = '" & txtName.Text.Trim & "'")
                    If dataRow.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot Add this Member. Reason: Member is already added."), enMsgBoxStyle.Information)
                        txtName.Focus()
                        Return False
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
        Return True
    End Function

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If IsValidate() = False Then Exit Sub

            Call FillMemberTran()

            If mdtMemTran IsNot Nothing Then
                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboEmployee.DisplayMember
                .ValueMember = cboEmployee.ValueMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            mstrAdvanceFilter = ""
            FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " RadioButton's Events "

    Private Sub radEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEmployee.CheckedChanged, radOthers.CheckedChanged
        Try
            If CType(sender, RadioButton).Name.ToString.ToUpper = "RADEMPLOYEE" AndAlso CType(sender, RadioButton).Checked = True Then
                cboEmployee.Enabled = True
                cboEmployee.SelectedValue = 0
                objbtnSearchEmployee.Enabled = True
                pnlOthers.Enabled = False
                objlblCompanyValue.Text = ""
                objlblDepartmentValue.Text = ""
                objlblEmployeeContactNo.Text = ""
                objlblEmployeeEmail.Text = ""
                If menAction <> enAction.EDIT_ONE Then
                    lnkAllocation.Enabled = True : objbtnReset.Enabled = True
                End If


            ElseIf CType(sender, RadioButton).Name.ToString.ToUpper = "RADOTHERS" AndAlso CType(sender, RadioButton).Checked = True Then
                pnlOthers.Enabled = True
                cboEmployee.Enabled = False
                objlblCompanyValue.Text = ""
                objlblDepartmentValue.Text = ""
                objlblEmployeeContactNo.Text = ""
                objbtnSearchEmployee.Enabled = False
                cboEmployee.SelectedValue = 0
                objlblEmployeeEmail.Text = ""
                If menAction <> enAction.EDIT_ONE Then
                    lnkAllocation.Enabled = False : objbtnReset.Enabled = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then

                Dim objEmp As New clsEmployee_Master
                Dim objDept As New clsDepartment

                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

                objDept._Departmentunkid = objEmp._Departmentunkid
                objlblDepartmentValue.Text = objDept._Name
                objlblCompanyValue.Text = Company._Object._Name
                objlblEmployeeContactNo.Text = objEmp._Present_Tel_No
                objlblEmployeeEmail.Text = objEmp._Email

            Else
                objlblDepartmentValue.Text = ""
                objlblCompanyValue.Text = ""
                objlblEmployeeContactNo.Text = ""
                objlblEmployeeEmail.Text = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkButton Events"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim frm As New frmAdvanceSearch
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                FillCombo()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbCommitteAddEdit.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCommitteAddEdit.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbCommitteAddEdit.Text = Language._Object.getCaption(Me.gbCommitteAddEdit.Name, Me.gbCommitteAddEdit.Text)
			Me.lblEmpEmail.Text = Language._Object.getCaption(Me.lblEmpEmail.Name, Me.lblEmpEmail.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.radOthers.Text = Language._Object.getCaption(Me.radOthers.Name, Me.radOthers.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.lblTrainerContactNo.Text = Language._Object.getCaption(Me.lblTrainerContactNo.Name, Me.lblTrainerContactNo.Text)
			Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
			Me.lblPosition.Text = Language._Object.getCaption(Me.lblPosition.Name, Me.lblPosition.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblEmpContact.Text = Language._Object.getCaption(Me.lblEmpContact.Name, Me.lblEmpContact.Text)
			Me.lblEmpCompany.Text = Language._Object.getCaption(Me.lblEmpCompany.Name, Me.lblEmpCompany.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.radEmployee.Text = Language._Object.getCaption(Me.radEmployee.Name, Me.radEmployee.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Name is mandatory inforamation. Please enter Name.")
			Language.setMessage(mstrModuleName, 2, "Invalid Email. Please Enter Valid Email")
			Language.setMessage(mstrModuleName, 3, "Employee is mandatory inforamation. Please select Employee.")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot Add this Employee. Reason: Employee is already added.")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot Add this Member. Reason: Member is already added.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class