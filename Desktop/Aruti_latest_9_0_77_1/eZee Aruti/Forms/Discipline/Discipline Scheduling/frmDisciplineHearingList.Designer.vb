﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDisciplineHearingList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDisciplineHearingList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objChkAll = New System.Windows.Forms.CheckBox
        Me.dgvHearingList = New System.Windows.Forms.DataGridView
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblNtfType = New System.Windows.Forms.Label
        Me.cboNtfType = New System.Windows.Forms.ComboBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.objbtnSearchCommitte = New eZee.Common.eZeeGradientButton
        Me.cboCommittee = New System.Windows.Forms.ComboBox
        Me.lblCommittee = New System.Windows.Forms.Label
        Me.lblDateTo = New System.Windows.Forms.Label
        Me.lblHearingDate = New System.Windows.Forms.Label
        Me.dtpHearingDateTo = New System.Windows.Forms.DateTimePicker
        Me.dtpHearingDateFrom = New System.Windows.Forms.DateTimePicker
        Me.objbtnSearchRefNo = New eZee.Common.eZeeGradientButton
        Me.cboReferenceNo = New System.Windows.Forms.ComboBox
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnNotify = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnMarkAsClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPersonInvolved = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHearingDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhHearingTime = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhVenue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCommittee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNtfSent = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhNtfSent = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhhearingschedulemasterunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhdisciplinefileunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStatusId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        CType(Me.dgvHearingList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objChkAll)
        Me.pnlMain.Controls.Add(Me.dgvHearingList)
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(859, 487)
        Me.pnlMain.TabIndex = 0
        '
        'objChkAll
        '
        Me.objChkAll.AutoSize = True
        Me.objChkAll.Location = New System.Drawing.Point(10, 101)
        Me.objChkAll.Name = "objChkAll"
        Me.objChkAll.Size = New System.Drawing.Size(15, 14)
        Me.objChkAll.TabIndex = 190
        Me.objChkAll.UseVisualStyleBackColor = True
        '
        'dgvHearingList
        '
        Me.dgvHearingList.AllowUserToAddRows = False
        Me.dgvHearingList.AllowUserToDeleteRows = False
        Me.dgvHearingList.AllowUserToResizeRows = False
        Me.dgvHearingList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvHearingList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvHearingList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHearingList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhRefNo, Me.dgcolhPersonInvolved, Me.dgcolhHearingDate, Me.dgcolhHearingTime, Me.dgcolhVenue, Me.dgcolhCommittee, Me.dgcolhRemark, Me.dgcolhStatus, Me.dgcolhNtfSent, Me.objdgcolhNtfSent, Me.objdgcolhhearingschedulemasterunkid, Me.objdgcolhdisciplinefileunkid, Me.objdgcolhStatusId})
        Me.dgvHearingList.Location = New System.Drawing.Point(3, 95)
        Me.dgvHearingList.Name = "dgvHearingList"
        Me.dgvHearingList.RowHeadersVisible = False
        Me.dgvHearingList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvHearingList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvHearingList.Size = New System.Drawing.Size(854, 341)
        Me.dgvHearingList.TabIndex = 8
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblNtfType)
        Me.gbFilterCriteria.Controls.Add(Me.cboNtfType)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchCommitte)
        Me.gbFilterCriteria.Controls.Add(Me.cboCommittee)
        Me.gbFilterCriteria.Controls.Add(Me.lblCommittee)
        Me.gbFilterCriteria.Controls.Add(Me.lblDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblHearingDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpHearingDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.dtpHearingDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchRefNo)
        Me.gbFilterCriteria.Controls.Add(Me.cboReferenceNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblRefNo)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblPersonalInvolved)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(2, 2)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(855, 92)
        Me.gbFilterCriteria.TabIndex = 7
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNtfType
        '
        Me.lblNtfType.BackColor = System.Drawing.Color.Transparent
        Me.lblNtfType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNtfType.Location = New System.Drawing.Point(397, 4)
        Me.lblNtfType.Name = "lblNtfType"
        Me.lblNtfType.Size = New System.Drawing.Size(142, 17)
        Me.lblNtfType.TabIndex = 191
        Me.lblNtfType.Text = "Default Notification Type"
        Me.lblNtfType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNtfType
        '
        Me.cboNtfType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNtfType.DropDownWidth = 400
        Me.cboNtfType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNtfType.FormattingEnabled = True
        Me.cboNtfType.Location = New System.Drawing.Point(542, 2)
        Me.cboNtfType.Name = "cboNtfType"
        Me.cboNtfType.Size = New System.Drawing.Size(257, 21)
        Me.cboNtfType.TabIndex = 190
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 400
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(733, 35)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(109, 21)
        Me.cboStatus.TabIndex = 188
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(677, 37)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(50, 17)
        Me.lblStatus.TabIndex = 187
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCommitte
        '
        Me.objbtnSearchCommitte.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCommitte.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCommitte.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCommitte.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCommitte.BorderSelected = False
        Me.objbtnSearchCommitte.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCommitte.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchCommitte.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCommitte.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCommitte.Location = New System.Drawing.Point(650, 62)
        Me.objbtnSearchCommitte.Name = "objbtnSearchCommitte"
        Me.objbtnSearchCommitte.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCommitte.TabIndex = 185
        '
        'cboCommittee
        '
        Me.cboCommittee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCommittee.DropDownWidth = 400
        Me.cboCommittee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCommittee.FormattingEnabled = True
        Me.cboCommittee.Location = New System.Drawing.Point(494, 62)
        Me.cboCommittee.Name = "cboCommittee"
        Me.cboCommittee.Size = New System.Drawing.Size(150, 21)
        Me.cboCommittee.TabIndex = 184
        '
        'lblCommittee
        '
        Me.lblCommittee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCommittee.Location = New System.Drawing.Point(407, 64)
        Me.lblCommittee.Name = "lblCommittee"
        Me.lblCommittee.Size = New System.Drawing.Size(84, 17)
        Me.lblCommittee.TabIndex = 183
        Me.lblCommittee.Text = "Committee"
        Me.lblCommittee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDateTo
        '
        Me.lblDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateTo.Location = New System.Drawing.Point(223, 64)
        Me.lblDateTo.Name = "lblDateTo"
        Me.lblDateTo.Size = New System.Drawing.Size(49, 17)
        Me.lblDateTo.TabIndex = 176
        Me.lblDateTo.Text = "To Date"
        Me.lblDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblHearingDate
        '
        Me.lblHearingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHearingDate.Location = New System.Drawing.Point(10, 64)
        Me.lblHearingDate.Name = "lblHearingDate"
        Me.lblHearingDate.Size = New System.Drawing.Size(104, 17)
        Me.lblHearingDate.TabIndex = 175
        Me.lblHearingDate.Text = "Hearing Date From"
        Me.lblHearingDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpHearingDateTo
        '
        Me.dtpHearingDateTo.Checked = False
        Me.dtpHearingDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHearingDateTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHearingDateTo.Location = New System.Drawing.Point(278, 62)
        Me.dtpHearingDateTo.Name = "dtpHearingDateTo"
        Me.dtpHearingDateTo.ShowCheckBox = True
        Me.dtpHearingDateTo.Size = New System.Drawing.Size(96, 21)
        Me.dtpHearingDateTo.TabIndex = 174
        '
        'dtpHearingDateFrom
        '
        Me.dtpHearingDateFrom.Checked = False
        Me.dtpHearingDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpHearingDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpHearingDateFrom.Location = New System.Drawing.Point(120, 62)
        Me.dtpHearingDateFrom.Name = "dtpHearingDateFrom"
        Me.dtpHearingDateFrom.ShowCheckBox = True
        Me.dtpHearingDateFrom.Size = New System.Drawing.Size(96, 21)
        Me.dtpHearingDateFrom.TabIndex = 173
        '
        'objbtnSearchRefNo
        '
        Me.objbtnSearchRefNo.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchRefNo.BorderSelected = False
        Me.objbtnSearchRefNo.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchRefNo.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchRefNo.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchRefNo.Location = New System.Drawing.Point(650, 35)
        Me.objbtnSearchRefNo.Name = "objbtnSearchRefNo"
        Me.objbtnSearchRefNo.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchRefNo.TabIndex = 167
        '
        'cboReferenceNo
        '
        Me.cboReferenceNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboReferenceNo.DropDownWidth = 400
        Me.cboReferenceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReferenceNo.FormattingEnabled = True
        Me.cboReferenceNo.Location = New System.Drawing.Point(494, 35)
        Me.cboReferenceNo.Name = "cboReferenceNo"
        Me.cboReferenceNo.Size = New System.Drawing.Size(150, 21)
        Me.cboReferenceNo.TabIndex = 166
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(407, 37)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(84, 17)
        Me.lblRefNo.TabIndex = 165
        Me.lblRefNo.Text = "Reference No."
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(380, 35)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 164
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmployee.DropDownWidth = 400
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(120, 35)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(254, 21)
        Me.cboEmployee.TabIndex = 163
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(10, 37)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(104, 17)
        Me.lblPersonalInvolved.TabIndex = 162
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(828, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(805, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnNotify)
        Me.objFooter.Controls.Add(Me.btnMarkAsClose)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnAdd)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 437)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(859, 50)
        Me.objFooter.TabIndex = 5
        '
        'btnNotify
        '
        Me.btnNotify.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNotify.BackColor = System.Drawing.Color.White
        Me.btnNotify.BackgroundImage = CType(resources.GetObject("btnNotify.BackgroundImage"), System.Drawing.Image)
        Me.btnNotify.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNotify.BorderColor = System.Drawing.Color.Empty
        Me.btnNotify.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNotify.FlatAppearance.BorderSize = 0
        Me.btnNotify.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNotify.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNotify.ForeColor = System.Drawing.Color.Black
        Me.btnNotify.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNotify.GradientForeColor = System.Drawing.Color.Black
        Me.btnNotify.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNotify.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNotify.Location = New System.Drawing.Point(121, 10)
        Me.btnNotify.Name = "btnNotify"
        Me.btnNotify.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNotify.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNotify.Size = New System.Drawing.Size(122, 30)
        Me.btnNotify.TabIndex = 126
        Me.btnNotify.Text = "Send Notification"
        Me.btnNotify.UseVisualStyleBackColor = True
        '
        'btnMarkAsClose
        '
        Me.btnMarkAsClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMarkAsClose.BackColor = System.Drawing.Color.White
        Me.btnMarkAsClose.BackgroundImage = CType(resources.GetObject("btnMarkAsClose.BackgroundImage"), System.Drawing.Image)
        Me.btnMarkAsClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMarkAsClose.BorderColor = System.Drawing.Color.Empty
        Me.btnMarkAsClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnMarkAsClose.FlatAppearance.BorderSize = 0
        Me.btnMarkAsClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMarkAsClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMarkAsClose.ForeColor = System.Drawing.Color.Black
        Me.btnMarkAsClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnMarkAsClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnMarkAsClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMarkAsClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnMarkAsClose.Location = New System.Drawing.Point(12, 10)
        Me.btnMarkAsClose.Name = "btnMarkAsClose"
        Me.btnMarkAsClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMarkAsClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnMarkAsClose.Size = New System.Drawing.Size(103, 30)
        Me.btnMarkAsClose.TabIndex = 125
        Me.btnMarkAsClose.Text = "Mark As Closed"
        Me.btnMarkAsClose.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(647, 10)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 124
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(544, 10)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 123
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(441, 10)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 122
        Me.btnAdd.Text = "&New"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(750, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 121
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Reference No."
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Person Involved"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 250
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Hearing Date"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 90
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Time"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 50
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Venue"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 120
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Committee"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 150
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 200
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhhearingschedulemasterunkid"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        Me.DataGridViewTextBoxColumn8.Width = 75
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhdisciplinefileunkid"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhdisciplinefileunkid"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhStatusId"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Visible = False
        Me.DataGridViewTextBoxColumn11.Width = 5
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.FillWeight = 25.0!
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.MinimumWidth = 25
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhRefNo
        '
        Me.dgcolhRefNo.HeaderText = "Reference No."
        Me.dgcolhRefNo.Name = "dgcolhRefNo"
        Me.dgcolhRefNo.ReadOnly = True
        '
        'dgcolhPersonInvolved
        '
        Me.dgcolhPersonInvolved.HeaderText = "Person Involved"
        Me.dgcolhPersonInvolved.Name = "dgcolhPersonInvolved"
        Me.dgcolhPersonInvolved.ReadOnly = True
        Me.dgcolhPersonInvolved.Width = 250
        '
        'dgcolhHearingDate
        '
        Me.dgcolhHearingDate.HeaderText = "Hearing Date"
        Me.dgcolhHearingDate.Name = "dgcolhHearingDate"
        Me.dgcolhHearingDate.ReadOnly = True
        Me.dgcolhHearingDate.Width = 90
        '
        'dgcolhHearingTime
        '
        Me.dgcolhHearingTime.HeaderText = "Time"
        Me.dgcolhHearingTime.Name = "dgcolhHearingTime"
        Me.dgcolhHearingTime.ReadOnly = True
        Me.dgcolhHearingTime.Width = 75
        '
        'dgcolhVenue
        '
        Me.dgcolhVenue.HeaderText = "Venue"
        Me.dgcolhVenue.Name = "dgcolhVenue"
        Me.dgcolhVenue.ReadOnly = True
        Me.dgcolhVenue.Width = 120
        '
        'dgcolhCommittee
        '
        Me.dgcolhCommittee.HeaderText = "Committee"
        Me.dgcolhCommittee.Name = "dgcolhCommittee"
        Me.dgcolhCommittee.ReadOnly = True
        Me.dgcolhCommittee.Width = 150
        '
        'dgcolhRemark
        '
        Me.dgcolhRemark.HeaderText = "Remark"
        Me.dgcolhRemark.Name = "dgcolhRemark"
        Me.dgcolhRemark.ReadOnly = True
        Me.dgcolhRemark.Width = 200
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.Width = 75
        '
        'dgcolhNtfSent
        '
        Me.dgcolhNtfSent.HeaderText = "Notification Status"
        Me.dgcolhNtfSent.Name = "dgcolhNtfSent"
        '
        'objdgcolhNtfSent
        '
        Me.objdgcolhNtfSent.HeaderText = "objdgcolhNtfSent"
        Me.objdgcolhNtfSent.Name = "objdgcolhNtfSent"
        Me.objdgcolhNtfSent.Visible = False
        '
        'objdgcolhhearingschedulemasterunkid
        '
        Me.objdgcolhhearingschedulemasterunkid.HeaderText = "objdgcolhhearingschedulemasterunkid"
        Me.objdgcolhhearingschedulemasterunkid.Name = "objdgcolhhearingschedulemasterunkid"
        Me.objdgcolhhearingschedulemasterunkid.Visible = False
        '
        'objdgcolhdisciplinefileunkid
        '
        Me.objdgcolhdisciplinefileunkid.HeaderText = "objdgcolhdisciplinefileunkid"
        Me.objdgcolhdisciplinefileunkid.Name = "objdgcolhdisciplinefileunkid"
        Me.objdgcolhdisciplinefileunkid.Visible = False
        '
        'objdgcolhStatusId
        '
        Me.objdgcolhStatusId.HeaderText = "objdgcolhStatusId"
        Me.objdgcolhStatusId.Name = "objdgcolhStatusId"
        Me.objdgcolhStatusId.Visible = False
        Me.objdgcolhStatusId.Width = 5
        '
        'frmDisciplineHearingList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(859, 487)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDisciplineHearingList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Discipline Hearing List"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        CType(Me.dgvHearingList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchRefNo As eZee.Common.eZeeGradientButton
    Friend WithEvents cboReferenceNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents dtpHearingDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpHearingDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateTo As System.Windows.Forms.Label
    Friend WithEvents lblHearingDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCommitte As eZee.Common.eZeeGradientButton
    Friend WithEvents cboCommittee As System.Windows.Forms.ComboBox
    Friend WithEvents lblCommittee As System.Windows.Forms.Label
    Friend WithEvents dgvHearingList As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnMarkAsClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnNotify As eZee.Common.eZeeLightButton
    Friend WithEvents objChkAll As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblNtfType As System.Windows.Forms.Label
    Friend WithEvents cboNtfType As System.Windows.Forms.ComboBox
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPersonInvolved As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHearingDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhHearingTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhVenue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCommittee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNtfSent As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhNtfSent As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhhearingschedulemasterunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhdisciplinefileunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStatusId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
