﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmDisciplineTypeList

#Region " Private Varaibles "
    Private objDisciplineType As clsDisciplineType
    Private ReadOnly mstrModuleName As String = "frmDisciplineTypeList"
#End Region

#Region " Private Function "

    Private Sub fillList()
        Dim dsAccess As New DataSet
        Dim StrSearch As String = ""
        Dim dtTable As DataTable
        Try

            If User._Object.Privilege._AllowToViewDisciplinaryOffencesList = True Then       'Pinkal (09-Jul-2012) -- Start

                dsAccess = objDisciplineType.GetList("DisplineType")
                Dim lvItem As ListViewItem

                If CInt(cboOffenceCategory.SelectedValue) > 0 Then
                    StrSearch &= "AND offencecategoryunkid = '" & CInt(cboOffenceCategory.SelectedValue) & "' "
                End If

                If CInt(nudSeverity.Value) > 0 Then
                    StrSearch &= "AND severity = '" & CInt(nudSeverity.Value) & "' "
                End If



                'S.SANDEEP [ 07 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If StrSearch.Trim.Length > 0 Then
                '    StrSearch = StrSearch.Substring(3)
                '    dtTable = New DataView(dsAccess.Tables(0), StrSearch, "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = New DataView(dsAccess.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                'End If

                If StrSearch.Trim.Length > 0 Then
                    StrSearch = StrSearch.Substring(3)
                    dtTable = New DataView(dsAccess.Tables(0), StrSearch, "Offence_Category", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsAccess.Tables(0), "", "Offence_Category", DataViewRowState.CurrentRows).ToTable
                End If
                'S.SANDEEP [ 07 MAY 2012 ] -- END

                lvDisciplineType.Items.Clear()
                For Each drRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("code").ToString
                    lvItem.Tag = drRow("disciplinetypeunkid")
                    lvItem.SubItems.Add(drRow("name").ToString)
                    lvItem.SubItems.Add(drRow("severity").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)

                    'S.SANDEEP [ 20 MARCH 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
                    lvItem.SubItems.Add(drRow.Item("Offence_Category").ToString)
                    'S.SANDEEP [ 20 MARCH 2012 ] -- END

                    lvDisciplineType.Items.Add(lvItem)
                Next

                If lvDisciplineType.Items.Count > 16 Then
                    colhDescription.Width = 315 - 18
                Else
                    colhDescription.Width = 315
                End If

                lvDisciplineType.GroupingColumn = objcolhOffenceCategory
                lvDisciplineType.DisplayGroups(True)

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsAccess.Dispose()
        End Try
    End Sub

    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AddDisciplineOffence
            btnEdit.Enabled = User._Object.Privilege._EditDisciplineOffence
            btnDelete.Enabled = User._Object.Privilege._DeleteDisciplineOffence

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 20 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objCMaster As New clsCommon_Master
        Try
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY, True, "List")
            With cboOffenceCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose() : objCMaster = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 MARCH 2012 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmDisciplineTypeList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvDisciplineType.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineTypeList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineTypeList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineTypeList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineTypeList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDisciplineType = New clsDisciplineType
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()

            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            Call FillCombo()
            lvDisciplineType.GridLines = False
            'S.SANDEEP [ 20 MARCH 2012 ] -- END


            If lvDisciplineType.Items.Count > 0 Then lvDisciplineType.Items(0).Selected = True
            lvDisciplineType.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDisciplineTypeList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDisciplineTypeList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objDisciplineType = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDisciplineType.SetMessages()
            objfrm._Other_ModuleNames = "clsDisciplineType"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvDisciplineType.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline Type from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvDisciplineType.Select()
            Exit Sub
        End If

        If objDisciplineType.isUsed(CInt(lvDisciplineType.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Discipline Type. Reason: This Discipline Type is in use."), enMsgBoxStyle.Information) '?2
            lvDisciplineType.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvDisciplineType.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Discipline Type?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objDisciplineType._FormName = mstrModuleName
                objDisciplineType._LoginEmployeeunkid = 0
                objDisciplineType._ClientIP = getIP()
                objDisciplineType._HostName = getHostName()
                objDisciplineType._FromWeb = False
                objDisciplineType._AuditUserId = User._Object._Userunkid
objDisciplineType._CompanyUnkid = Company._Object._Companyunkid
                objDisciplineType._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objDisciplineType.Delete(CInt(lvDisciplineType.SelectedItems(0).Tag))
                lvDisciplineType.SelectedItems(0).Remove()

                If lvDisciplineType.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvDisciplineType.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvDisciplineType.Items.Count - 1
                    lvDisciplineType.Items(intSelectedIndex).Selected = True
                    lvDisciplineType.EnsureVisible(intSelectedIndex)
                ElseIf lvDisciplineType.Items.Count <> 0 Then
                    lvDisciplineType.Items(intSelectedIndex).Selected = True
                    lvDisciplineType.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvDisciplineType.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvDisciplineType.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Discipline Type from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvDisciplineType.Select()
            Exit Sub
        End If
        Dim frm As New frmDisciplineType_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvDisciplineType.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(CInt(lvDisciplineType.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frm = Nothing

            lvDisciplineType.Items(intSelectedIndex).Selected = True
            lvDisciplineType.EnsureVisible(intSelectedIndex)
            lvDisciplineType.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmDisciplineType_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call fillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 20 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboOffenceCategory.SelectedValue = 0
            nudSeverity.Value = 0
            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboOffenceCategory.ValueMember
                .DisplayMember = cboOffenceCategory.DisplayMember
                .DataSource = CType(cboOffenceCategory.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboOffenceCategory.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 20 MARCH 2012 ] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhType.Text = Language._Object.getCaption(CStr(Me.colhType.Tag), Me.colhType.Text)
            Me.colhSeverity.Text = Language._Object.getCaption(CStr(Me.colhSeverity.Tag), Me.colhSeverity.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblOffencecategory.Text = Language._Object.getCaption(Me.lblOffencecategory.Name, Me.lblOffencecategory.Text)
            Me.lblSeverity.Text = Language._Object.getCaption(Me.lblSeverity.Name, Me.lblSeverity.Text)


        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Discipline Type from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Discipline Type. Reason: This Discipline Type is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Discipline Type?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class