﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExemptDisciplineHeads
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExemptDisciplineHeads))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbExemptHeadInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblTypeOf = New System.Windows.Forms.Label
        Me.cboTypeOf = New System.Windows.Forms.ComboBox
        Me.objckhAll = New System.Windows.Forms.CheckBox
        Me.objStline1 = New eZee.Common.eZeeStraightLine
        Me.lvPeriods = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhChk = New System.Windows.Forms.ColumnHeader
        Me.colhStartDate = New System.Windows.Forms.ColumnHeader
        Me.colhEndDate = New System.Windows.Forms.ColumnHeader
        Me.objcolhPeriodName = New System.Windows.Forms.ColumnHeader
        Me.objSearchTransactionhead = New eZee.Common.eZeeGradientButton
        Me.lblIncident = New System.Windows.Forms.Label
        Me.txtIncident = New eZee.TextBox.AlphanumericTextBox
        Me.cboTransactionhead = New System.Windows.Forms.ComboBox
        Me.lblTransactionhead = New System.Windows.Forms.Label
        Me.cboTrnHeadType = New System.Windows.Forms.ComboBox
        Me.lblTrnHeadType = New System.Windows.Forms.Label
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.lblYear = New System.Windows.Forms.Label
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.txtPesonInvolved = New eZee.TextBox.AlphanumericTextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbExemptHeadInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbExemptHeadInfo)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(608, 303)
        Me.pnlMain.TabIndex = 0
        '
        'gbExemptHeadInfo
        '
        Me.gbExemptHeadInfo.BorderColor = System.Drawing.Color.Black
        Me.gbExemptHeadInfo.Checked = False
        Me.gbExemptHeadInfo.CollapseAllExceptThis = False
        Me.gbExemptHeadInfo.CollapsedHoverImage = Nothing
        Me.gbExemptHeadInfo.CollapsedNormalImage = Nothing
        Me.gbExemptHeadInfo.CollapsedPressedImage = Nothing
        Me.gbExemptHeadInfo.CollapseOnLoad = False
        Me.gbExemptHeadInfo.Controls.Add(Me.lblTypeOf)
        Me.gbExemptHeadInfo.Controls.Add(Me.cboTypeOf)
        Me.gbExemptHeadInfo.Controls.Add(Me.objckhAll)
        Me.gbExemptHeadInfo.Controls.Add(Me.objStline1)
        Me.gbExemptHeadInfo.Controls.Add(Me.lvPeriods)
        Me.gbExemptHeadInfo.Controls.Add(Me.objSearchTransactionhead)
        Me.gbExemptHeadInfo.Controls.Add(Me.lblIncident)
        Me.gbExemptHeadInfo.Controls.Add(Me.txtIncident)
        Me.gbExemptHeadInfo.Controls.Add(Me.cboTransactionhead)
        Me.gbExemptHeadInfo.Controls.Add(Me.lblTransactionhead)
        Me.gbExemptHeadInfo.Controls.Add(Me.cboTrnHeadType)
        Me.gbExemptHeadInfo.Controls.Add(Me.lblTrnHeadType)
        Me.gbExemptHeadInfo.Controls.Add(Me.cboYear)
        Me.gbExemptHeadInfo.Controls.Add(Me.lblYear)
        Me.gbExemptHeadInfo.Controls.Add(Me.lblPersonalInvolved)
        Me.gbExemptHeadInfo.Controls.Add(Me.txtPesonInvolved)
        Me.gbExemptHeadInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbExemptHeadInfo.ExpandedHoverImage = Nothing
        Me.gbExemptHeadInfo.ExpandedNormalImage = Nothing
        Me.gbExemptHeadInfo.ExpandedPressedImage = Nothing
        Me.gbExemptHeadInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbExemptHeadInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbExemptHeadInfo.HeaderHeight = 25
        Me.gbExemptHeadInfo.HeaderMessage = ""
        Me.gbExemptHeadInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbExemptHeadInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbExemptHeadInfo.HeightOnCollapse = 0
        Me.gbExemptHeadInfo.LeftTextSpace = 0
        Me.gbExemptHeadInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbExemptHeadInfo.Name = "gbExemptHeadInfo"
        Me.gbExemptHeadInfo.OpenHeight = 300
        Me.gbExemptHeadInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbExemptHeadInfo.ShowBorder = True
        Me.gbExemptHeadInfo.ShowCheckBox = False
        Me.gbExemptHeadInfo.ShowCollapseButton = False
        Me.gbExemptHeadInfo.ShowDefaultBorderColor = True
        Me.gbExemptHeadInfo.ShowDownButton = False
        Me.gbExemptHeadInfo.ShowHeader = True
        Me.gbExemptHeadInfo.Size = New System.Drawing.Size(608, 248)
        Me.gbExemptHeadInfo.TabIndex = 0
        Me.gbExemptHeadInfo.Temp = 0
        Me.gbExemptHeadInfo.Text = "Exempt Heads Info"
        Me.gbExemptHeadInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTypeOf
        '
        Me.lblTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTypeOf.Location = New System.Drawing.Point(278, 196)
        Me.lblTypeOf.Name = "lblTypeOf"
        Me.lblTypeOf.Size = New System.Drawing.Size(100, 15)
        Me.lblTypeOf.TabIndex = 11
        Me.lblTypeOf.Text = "Tran. Type Of"
        '
        'cboTypeOf
        '
        Me.cboTypeOf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTypeOf.FormattingEnabled = True
        Me.cboTypeOf.Location = New System.Drawing.Point(382, 193)
        Me.cboTypeOf.Name = "cboTypeOf"
        Me.cboTypeOf.Size = New System.Drawing.Size(192, 21)
        Me.cboTypeOf.TabIndex = 12
        '
        'objckhAll
        '
        Me.objckhAll.AutoSize = True
        Me.objckhAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objckhAll.Location = New System.Drawing.Point(21, 41)
        Me.objckhAll.Name = "objckhAll"
        Me.objckhAll.Size = New System.Drawing.Size(15, 14)
        Me.objckhAll.TabIndex = 1
        Me.objckhAll.UseVisualStyleBackColor = True
        '
        'objStline1
        '
        Me.objStline1.BackColor = System.Drawing.Color.Transparent
        Me.objStline1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objStline1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStline1.Location = New System.Drawing.Point(268, 36)
        Me.objStline1.Name = "objStline1"
        Me.objStline1.Size = New System.Drawing.Size(5, 205)
        Me.objStline1.TabIndex = 2
        Me.objStline1.Text = "EZeeStraightLine1"
        '
        'lvPeriods
        '
        Me.lvPeriods.BackColorOnChecked = False
        Me.lvPeriods.CheckBoxes = True
        Me.lvPeriods.ColumnHeaders = Nothing
        Me.lvPeriods.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhChk, Me.colhStartDate, Me.colhEndDate, Me.objcolhPeriodName})
        Me.lvPeriods.CompulsoryColumns = ""
        Me.lvPeriods.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvPeriods.FullRowSelect = True
        Me.lvPeriods.GridLines = True
        Me.lvPeriods.GroupingColumn = Nothing
        Me.lvPeriods.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvPeriods.HideSelection = False
        Me.lvPeriods.Location = New System.Drawing.Point(13, 36)
        Me.lvPeriods.MinColumnWidth = 50
        Me.lvPeriods.MultiSelect = False
        Me.lvPeriods.Name = "lvPeriods"
        Me.lvPeriods.OptionalColumns = ""
        Me.lvPeriods.ShowMoreItem = False
        Me.lvPeriods.ShowSaveItem = False
        Me.lvPeriods.ShowSelectAll = True
        Me.lvPeriods.ShowSizeAllColumnsToFit = True
        Me.lvPeriods.Size = New System.Drawing.Size(250, 205)
        Me.lvPeriods.Sortable = True
        Me.lvPeriods.TabIndex = 0
        Me.lvPeriods.UseCompatibleStateImageBehavior = False
        Me.lvPeriods.View = System.Windows.Forms.View.Details
        '
        'objcolhChk
        '
        Me.objcolhChk.Tag = "objcolhChk"
        Me.objcolhChk.Text = ""
        Me.objcolhChk.Width = 25
        '
        'colhStartDate
        '
        Me.colhStartDate.Tag = "colhStartDate"
        Me.colhStartDate.Text = "Start Date"
        Me.colhStartDate.Width = 102
        '
        'colhEndDate
        '
        Me.colhEndDate.Tag = "colhEndDate"
        Me.colhEndDate.Text = "End Date"
        Me.colhEndDate.Width = 102
        '
        'objcolhPeriodName
        '
        Me.objcolhPeriodName.Tag = "objcolhPeriodName"
        Me.objcolhPeriodName.Text = ""
        Me.objcolhPeriodName.Width = 0
        '
        'objSearchTransactionhead
        '
        Me.objSearchTransactionhead.BackColor = System.Drawing.Color.Transparent
        Me.objSearchTransactionhead.BackColor1 = System.Drawing.Color.Transparent
        Me.objSearchTransactionhead.BackColor2 = System.Drawing.Color.Transparent
        Me.objSearchTransactionhead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSearchTransactionhead.BorderSelected = False
        Me.objSearchTransactionhead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSearchTransactionhead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSearchTransactionhead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSearchTransactionhead.Location = New System.Drawing.Point(580, 220)
        Me.objSearchTransactionhead.Name = "objSearchTransactionhead"
        Me.objSearchTransactionhead.Size = New System.Drawing.Size(21, 21)
        Me.objSearchTransactionhead.TabIndex = 15
        '
        'lblIncident
        '
        Me.lblIncident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncident.Location = New System.Drawing.Point(278, 67)
        Me.lblIncident.Name = "lblIncident"
        Me.lblIncident.Size = New System.Drawing.Size(100, 15)
        Me.lblIncident.TabIndex = 5
        Me.lblIncident.Text = "Incident"
        Me.lblIncident.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIncident
        '
        Me.txtIncident.BackColor = System.Drawing.Color.White
        Me.txtIncident.Flags = 0
        Me.txtIncident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIncident.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtIncident.Location = New System.Drawing.Point(382, 63)
        Me.txtIncident.Multiline = True
        Me.txtIncident.Name = "txtIncident"
        Me.txtIncident.ReadOnly = True
        Me.txtIncident.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtIncident.Size = New System.Drawing.Size(192, 70)
        Me.txtIncident.TabIndex = 6
        '
        'cboTransactionhead
        '
        Me.cboTransactionhead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransactionhead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTransactionhead.FormattingEnabled = True
        Me.cboTransactionhead.Location = New System.Drawing.Point(382, 220)
        Me.cboTransactionhead.Name = "cboTransactionhead"
        Me.cboTransactionhead.Size = New System.Drawing.Size(192, 21)
        Me.cboTransactionhead.TabIndex = 14
        '
        'lblTransactionhead
        '
        Me.lblTransactionhead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTransactionhead.Location = New System.Drawing.Point(278, 223)
        Me.lblTransactionhead.Name = "lblTransactionhead"
        Me.lblTransactionhead.Size = New System.Drawing.Size(100, 15)
        Me.lblTransactionhead.TabIndex = 13
        Me.lblTransactionhead.Text = "Transaction Head"
        Me.lblTransactionhead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTrnHeadType
        '
        Me.cboTrnHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHeadType.FormattingEnabled = True
        Me.cboTrnHeadType.Location = New System.Drawing.Point(382, 166)
        Me.cboTrnHeadType.Name = "cboTrnHeadType"
        Me.cboTrnHeadType.Size = New System.Drawing.Size(192, 21)
        Me.cboTrnHeadType.TabIndex = 10
        '
        'lblTrnHeadType
        '
        Me.lblTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHeadType.Location = New System.Drawing.Point(278, 169)
        Me.lblTrnHeadType.Name = "lblTrnHeadType"
        Me.lblTrnHeadType.Size = New System.Drawing.Size(100, 15)
        Me.lblTrnHeadType.TabIndex = 9
        Me.lblTrnHeadType.Text = "Tran. Head Type"
        '
        'cboYear
        '
        Me.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Location = New System.Drawing.Point(382, 139)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(192, 21)
        Me.cboYear.TabIndex = 8
        '
        'lblYear
        '
        Me.lblYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYear.Location = New System.Drawing.Point(278, 142)
        Me.lblYear.Name = "lblYear"
        Me.lblYear.Size = New System.Drawing.Size(100, 15)
        Me.lblYear.TabIndex = 7
        Me.lblYear.Text = "Year"
        Me.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(278, 39)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(100, 15)
        Me.lblPersonalInvolved.TabIndex = 3
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPesonInvolved
        '
        Me.txtPesonInvolved.BackColor = System.Drawing.SystemColors.Window
        Me.txtPesonInvolved.Flags = 0
        Me.txtPesonInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPesonInvolved.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPesonInvolved.Location = New System.Drawing.Point(382, 36)
        Me.txtPesonInvolved.Name = "txtPesonInvolved"
        Me.txtPesonInvolved.ReadOnly = True
        Me.txtPesonInvolved.Size = New System.Drawing.Size(192, 21)
        Me.txtPesonInvolved.TabIndex = 4
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 248)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(608, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(396, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(499, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmExemptDisciplineHeads
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(608, 303)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExemptDisciplineHeads"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Exempt Heads"
        Me.pnlMain.ResumeLayout(False)
        Me.gbExemptHeadInfo.ResumeLayout(False)
        Me.gbExemptHeadInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbExemptHeadInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtPesonInvolved As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblYear As System.Windows.Forms.Label
    Friend WithEvents cboTrnHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrnHeadType As System.Windows.Forms.Label
    Friend WithEvents cboTransactionhead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTransactionhead As System.Windows.Forms.Label
    Friend WithEvents txtIncident As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblIncident As System.Windows.Forms.Label
    Friend WithEvents objSearchTransactionhead As eZee.Common.eZeeGradientButton
    Friend WithEvents lvPeriods As eZee.Common.eZeeListView
    Friend WithEvents objStline1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objcolhChk As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStartDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEndDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPeriodName As System.Windows.Forms.ColumnHeader
    Friend WithEvents objckhAll As System.Windows.Forms.CheckBox
    Friend WithEvents lblTypeOf As System.Windows.Forms.Label
    Friend WithEvents cboTypeOf As System.Windows.Forms.ComboBox
End Class
