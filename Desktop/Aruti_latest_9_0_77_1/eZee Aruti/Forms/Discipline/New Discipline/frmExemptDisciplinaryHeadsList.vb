﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmExemptDisciplinaryHeadsList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmExemptedDisciplinaryHeadsList"
    Private mblnCancel As Boolean = True
    Private mintDisciplineUnkid As Integer = -1
    Private objEmpExemption As clsemployee_exemption_Tran

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intFileUnkid As Integer) As Boolean
        Try
            mintDisciplineUnkid = intFileUnkid

            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Fill_ExemptionList()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Dim dtTable As DataTable
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmpExemption.GetList("List", True, , CInt(cboTransactionHead.SelectedValue), CInt(cboPayPeriod.SelectedValue), CInt(cboExempApprovStatus.SelectedValue), mintDisciplineUnkid)
            dsList = objEmpExemption.GetList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, _
                                             ConfigParameter._Object._IsIncludeInactiveEmp, _
                                             "List", True, , , CInt(cboTransactionHead.SelectedValue), CInt(cboPayPeriod.SelectedValue), _
                                             CInt(cboExempApprovStatus.SelectedValue), mintDisciplineUnkid)
            'S.SANDEEP [04 JUN 2015] -- END


            dtTable = New DataView(dsList.Tables("List"), "", "start_date", DataViewRowState.CurrentRows).ToTable

            RemoveHandler lvExemptedHeads.SelectedIndexChanged, AddressOf lvExemptedHeads_SelectedIndexChanged
            RemoveHandler lvExemptedHeads.ItemChecked, AddressOf lvExemptedHeads_ItemChecked
            RemoveHandler objEchkall.CheckedChanged, AddressOf objEchkall_CheckedChanged

            lvExemptedHeads.Items.Clear()

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow("employeename").ToString)
                lvItem.SubItems.Add(dtRow("trnheadname").ToString)
                If CBool(dtRow.Item("isapproved")) = True Then
                    lvItem.ForeColor = Color.Blue
                    lvItem.SubItems.Add(Language.getMessage("clsDiscipline_Resolution", 1, "Approved").ToString)
                Else
                    lvItem.SubItems.Add(Language.getMessage("clsDiscipline_Resolution", 2, "Pending").ToString)
                End If
                lvItem.SubItems.Add(dtRow("period").ToString())
                lvItem.SubItems.Add(dtRow.Item("periodunkid").ToString)
                lvItem.Tag = dtRow.Item("exemptionunkid")

                lvExemptedHeads.Items.Add(lvItem)
            Next

            lvExemptedHeads.GridLines = False
            lvExemptedHeads.GroupingColumn = objcolhEPeriod
            lvExemptedHeads.DisplayGroups(True)

            If lvExemptedHeads.Items.Count >= 4 Then
                colhETName.Width = 300 - 20
            Else
                colhETName.Width = 300
            End If

            If CInt(cboExempApprovStatus.SelectedValue) = enExempHeadApprovalStatus.Approved Then
                btnDisapprove.Visible = True
                btnApprove.Visible = False
            ElseIf CInt(cboExempApprovStatus.SelectedValue) = enExempHeadApprovalStatus.Pending Then
                btnApprove.Visible = True
                btnDisapprove.Visible = False
            Else
                btnApprove.Visible = False
                btnDisapprove.Visible = False
            End If

            AddHandler lvExemptedHeads.ItemChecked, AddressOf lvExemptedHeads_ItemChecked
            AddHandler objEchkall.CheckedChanged, AddressOf objEchkall_CheckedChanged
            AddHandler lvExemptedHeads.SelectedIndexChanged, AddressOf lvExemptedHeads_SelectedIndexChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_ExemptionList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Combo()
        Dim objTranhead As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Dim dsFill As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsFill = objTranhead.getComboList("TranHead", True)
            dsFill = objTranhead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True)
            'Sohail (21 Aug 2015) -- End
            cboTransactionHead.ValueMember = "tranheadunkid"
            cboTransactionHead.DisplayMember = "name"
            cboTransactionHead.DataSource = dsFill.Tables("TranHead")

            dsFill = objMaster.getComboListForExemptHeadApprovalStatus("ExempApproval", True)
            With cboExempApprovStatus
                .BeginUpdate()
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = New DataView(dsFill.Tables("ExempApproval"), "Id <> " & enExempHeadApprovalStatus.All & " ", "", DataViewRowState.CurrentRows).ToTable
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate()
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "PayPeriod", True)
            dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PayPeriod", True)
            'Sohail (21 Aug 2015) -- End
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsFill.Tables("PayPeriod")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteDisciplineExemption
            btnApprove.Enabled = User._Object.Privilege._AllowToApproveEmpExemtion
            btnDisapprove.Enabled = User._Object.Privilege._AllowToApproveEmpExemtion
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmViewDisciplinaryHeads_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpExemption = New clsemployee_exemption_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Call SetVisibility()

            Language.setLanguage(Me.Name)

            Call OtherSettings()

            Call Fill_Combo()

            Call Fill_ExemptionList()

            lvExemptedHeads.GridLines = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmViewDisciplinaryHeads_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmViewDisciplinaryHeads_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objEmpExemption = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_Resolution.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_Resolution"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvExemptedHeads.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Check Exemption from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvExemptedHeads.Select()
                Exit Sub
            End If

            Dim objPeriod As New clscommom_period_Tran
            Dim StrIds As String = String.Empty
            Dim blnIsUsed As Boolean = False

            For Each lvItem As ListViewItem In lvExemptedHeads.CheckedItems
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(lvItem.SubItems(objcolhEPeriodId.Index).Tag)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvItem.SubItems(objcolhEPeriodId.Index).Tag)
                'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    blnIsUsed = True
                    Exit For
                End If
                StrIds &= "," & lvItem.Tag.ToString
            Next

            If blnIsUsed = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot Delete checked entry(s). Reason: Period is closed for some checked entries."), enMsgBoxStyle.Information)
                lvExemptedHeads.Select()
                Exit Try
            End If

            If StrIds.Length > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Are you sure you want to delete this Exemption?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If
                    Dim mstrVoidReason As String = String.Empty
                    frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                    If mstrVoidReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objEmpExemption._Voidreason = mstrVoidReason
                    End If
                    frm = Nothing
                    objEmpExemption._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime

                    objEmpExemption._Voiduserunkid = User._Object._Userunkid

                    StrIds = Mid(StrIds, 2)

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objEmpExemption._FormName = mstrModuleName
                    objEmpExemption._LoginEmployeeUnkid = 0
                    objEmpExemption._ClientIP = getIP()
                    objEmpExemption._HostName = getHostName()
                    objEmpExemption._FromWeb = False
                    objEmpExemption._AuditUserId = User._Object._Userunkid
objEmpExemption._CompanyUnkid = Company._Object._Companyunkid
                    objEmpExemption._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmpExemption.Delete(StrIds)
                    objEmpExemption.Delete(StrIds, ConfigParameter._Object._CurrentDateAndTime)
                    'S.SANDEEP [04 JUN 2015] -- END


                    Call Fill_ExemptionList()

                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objSearchTransactionhead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchTransactionhead.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboTransactionHead.DisplayMember
                .ValueMember = cboTransactionHead.ValueMember
                .DataSource = CType(cboTransactionHead.DataSource, DataTable)
                .CodeMember = "code"
            End With
            If frm.DisplayDialog Then
                cboTransactionHead.SelectedValue = frm.SelectedValue
                cboTransactionHead.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchTransactionhead_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_ExemptionList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboExempApprovStatus.SelectedValue = 0
            cboPayPeriod.SelectedValue = 0
            cboTransactionHead.SelectedValue = 0
            Call Fill_ExemptionList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnDisapprove.Click
        Try
            Dim strMsg As String = ""
            Dim blnIsApprove As Boolean
            Dim btn As eZee.Common.eZeeLightButton = CType(sender, eZee.Common.eZeeLightButton)
            Select Case btn.Name.ToUpper
                Case "BTNAPPROVE"
                    blnIsApprove = True
                    strMsg = Language.getMessage(mstrModuleName, 4, "Are you sure you want to Approve selected ED Heads?")
                Case "BTNDISAPPROVE"
                    blnIsApprove = False
                    strMsg = Language.getMessage(mstrModuleName, 5, "Are you sure you want to Dispprove selected ED Heads?")
            End Select

            If lvExemptedHeads.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please Check atleast one ED Heads to Approve/Disapprove."), enMsgBoxStyle.Information)
                lvExemptedHeads.Focus()
                Exit Sub
            ElseIf (eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No) Then
                Exit Sub
            End If

            Dim blnResult As Boolean
            Dim blnRefresh As Boolean = False

            For Each lvItem As ListViewItem In lvExemptedHeads.CheckedItems
                With objEmpExemption
                    ._Exemptionunkid = CInt(lvItem.Tag)
                    ._Isapproved = blnIsApprove
                    ._Approveruserunkid = User._Object._Userunkid
                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    ._FormName = mstrModuleName
                    ._LoginEmployeeUnkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'blnResult = .Update()
                    blnResult = .Update(ConfigParameter._Object._CurrentDateAndTime)
                    'S.SANDEEP [04 JUN 2015] -- END

                    If blnResult = False AndAlso objEmpExemption._Message <> "" Then
                        eZeeMsgBox.Show(._Message, enMsgBoxStyle.Information)
                        Exit Try
                    End If
                    blnRefresh = True
                End With
            Next
            If blnRefresh = True Then
                Call Fill_ExemptionList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Evensts "

    Private Sub objEchkall_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objEchkall.CheckedChanged
        Try
            RemoveHandler lvExemptedHeads.ItemChecked, AddressOf lvExemptedHeads_ItemChecked
            For Each lvItem As ListViewItem In lvExemptedHeads.Items
                lvItem.Checked = CBool(objEchkall.CheckState)
            Next
            AddHandler lvExemptedHeads.ItemChecked, AddressOf lvExemptedHeads_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objEchkall_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvExemptedHeads_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvExemptedHeads.ItemChecked
        Try
            RemoveHandler objEchkall.CheckedChanged, AddressOf objEchkall_CheckedChanged
            If lvExemptedHeads.CheckedItems.Count <= 0 Then
                objEchkall.CheckState = CheckState.Unchecked
            ElseIf lvExemptedHeads.CheckedItems.Count < lvExemptedHeads.Items.Count Then
                objEchkall.CheckState = CheckState.Indeterminate
            ElseIf lvExemptedHeads.CheckedItems.Count = lvExemptedHeads.Items.Count Then
                objEchkall.CheckState = CheckState.Checked
            End If
            AddHandler objEchkall.CheckedChanged, AddressOf objEchkall_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvExemptedHeads_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvExemptedHeads_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvExemptedHeads.SelectedIndexChanged
        Try
            If lvExemptedHeads.SelectedItems.Count > 0 Then
                If lvExemptedHeads.SelectedItems(0).ForeColor = Color.Blue Then
                    btnDelete.Enabled = False
                Else
                    btnDelete.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvExemptedHeads_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnDisapprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnDisapprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.colhETName.Text = Language._Object.getCaption(CStr(Me.colhETName.Tag), Me.colhETName.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.colhEStatus.Text = Language._Object.getCaption(CStr(Me.colhEStatus.Tag), Me.colhEStatus.Text)
            Me.btnDisapprove.Text = Language._Object.getCaption(Me.btnDisapprove.Name, Me.btnDisapprove.Text)
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
            Me.lblAccess.Text = Language._Object.getCaption(Me.lblAccess.Name, Me.lblAccess.Text)
            Me.lblApprovalStatus.Text = Language._Object.getCaption(Me.lblApprovalStatus.Name, Me.lblApprovalStatus.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsDiscipline_Resolution", 1, "Approved")
            Language.setMessage("clsDiscipline_Resolution", 2, "Pending")
            Language.setMessage(mstrModuleName, 1, "Are you sure you want to delete this Exemption?")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot Delete checked entry(s). Reason: Period is closed for some checked entries.")
            Language.setMessage(mstrModuleName, 3, "Please Check Exemption from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 4, "Are you sure you want to Approve selected ED Heads?")
            Language.setMessage(mstrModuleName, 5, "Are you sure you want to Dispprove selected ED Heads?")
            Language.setMessage(mstrModuleName, 6, "Please Check atleast one ED Heads to Approve/Disapprove.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class