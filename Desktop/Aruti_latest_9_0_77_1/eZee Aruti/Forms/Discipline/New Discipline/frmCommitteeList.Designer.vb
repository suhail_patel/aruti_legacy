﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommitteeList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommitteeList))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchMCategory = New eZee.Common.eZeeGradientButton
        Me.lblMembersCategory = New System.Windows.Forms.Label
        Me.cboMCategory = New System.Windows.Forms.ComboBox
        Me.radAll = New System.Windows.Forms.RadioButton
        Me.radExternal = New System.Windows.Forms.RadioButton
        Me.radInternal = New System.Windows.Forms.RadioButton
        Me.lblName = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchCommittee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblCommitteMaster = New System.Windows.Forms.Label
        Me.cboCommitteMaster = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lvCommitteMember = New eZee.Common.eZeeListView(Me.components)
        Me.colhType = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhCompany = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhContactNo = New System.Windows.Forms.ColumnHeader
        Me.objcolhCommitte = New System.Windows.Forms.ColumnHeader
        Me.objcolhMasterId = New System.Windows.Forms.ColumnHeader
        Me.colhMCategory = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.colhEmail = New System.Windows.Forms.ColumnHeader
        Me.Panel1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbFilterCriteria)
        Me.Panel1.Controls.Add(Me.lvCommitteMember)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Controls.Add(Me.eZeeHeader)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(749, 470)
        Me.Panel1.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchMCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblMembersCategory)
        Me.gbFilterCriteria.Controls.Add(Me.cboMCategory)
        Me.gbFilterCriteria.Controls.Add(Me.radAll)
        Me.gbFilterCriteria.Controls.Add(Me.radExternal)
        Me.gbFilterCriteria.Controls.Add(Me.radInternal)
        Me.gbFilterCriteria.Controls.Add(Me.lblName)
        Me.gbFilterCriteria.Controls.Add(Me.txtName)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchCommittee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblCommitteMaster)
        Me.gbFilterCriteria.Controls.Add(Me.cboCommitteMaster)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 90
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(726, 91)
        Me.gbFilterCriteria.TabIndex = 9
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchMCategory
        '
        Me.objbtnSearchMCategory.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMCategory.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMCategory.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMCategory.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMCategory.BorderSelected = False
        Me.objbtnSearchMCategory.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMCategory.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMCategory.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMCategory.Location = New System.Drawing.Point(698, 60)
        Me.objbtnSearchMCategory.Name = "objbtnSearchMCategory"
        Me.objbtnSearchMCategory.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMCategory.TabIndex = 108
        '
        'lblMembersCategory
        '
        Me.lblMembersCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembersCategory.Location = New System.Drawing.Point(397, 61)
        Me.lblMembersCategory.Name = "lblMembersCategory"
        Me.lblMembersCategory.Size = New System.Drawing.Size(113, 17)
        Me.lblMembersCategory.TabIndex = 109
        Me.lblMembersCategory.Text = "Members Category"
        Me.lblMembersCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMCategory
        '
        Me.cboMCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMCategory.FormattingEnabled = True
        Me.cboMCategory.Location = New System.Drawing.Point(514, 60)
        Me.cboMCategory.Name = "cboMCategory"
        Me.cboMCategory.Size = New System.Drawing.Size(178, 21)
        Me.cboMCategory.TabIndex = 107
        '
        'radAll
        '
        Me.radAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAll.Location = New System.Drawing.Point(397, 35)
        Me.radAll.Name = "radAll"
        Me.radAll.Size = New System.Drawing.Size(45, 17)
        Me.radAll.TabIndex = 116
        Me.radAll.TabStop = True
        Me.radAll.Text = "All"
        Me.radAll.UseVisualStyleBackColor = True
        '
        'radExternal
        '
        Me.radExternal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExternal.Location = New System.Drawing.Point(317, 61)
        Me.radExternal.Name = "radExternal"
        Me.radExternal.Size = New System.Drawing.Size(75, 17)
        Me.radExternal.TabIndex = 116
        Me.radExternal.TabStop = True
        Me.radExternal.Text = "External"
        Me.radExternal.UseVisualStyleBackColor = True
        '
        'radInternal
        '
        Me.radInternal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radInternal.Location = New System.Drawing.Point(317, 35)
        Me.radInternal.Name = "radInternal"
        Me.radInternal.Size = New System.Drawing.Size(75, 17)
        Me.radInternal.TabIndex = 116
        Me.radInternal.TabStop = True
        Me.radInternal.Text = "Internal"
        Me.radInternal.UseVisualStyleBackColor = True
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(447, 35)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(61, 17)
        Me.lblName.TabIndex = 114
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(514, 33)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(178, 21)
        Me.txtName.TabIndex = 115
        '
        'objbtnSearchCommittee
        '
        Me.objbtnSearchCommittee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCommittee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCommittee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCommittee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCommittee.BorderSelected = False
        Me.objbtnSearchCommittee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCommittee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCommittee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCommittee.Location = New System.Drawing.Point(292, 33)
        Me.objbtnSearchCommittee.Name = "objbtnSearchCommittee"
        Me.objbtnSearchCommittee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCommittee.TabIndex = 113
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(292, 60)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 112
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(3, 63)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(121, 15)
        Me.lblEmployee.TabIndex = 110
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(128, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(161, 21)
        Me.cboEmployee.TabIndex = 111
        '
        'lblCommitteMaster
        '
        Me.lblCommitteMaster.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCommitteMaster.Location = New System.Drawing.Point(3, 36)
        Me.lblCommitteMaster.Name = "lblCommitteMaster"
        Me.lblCommitteMaster.Size = New System.Drawing.Size(121, 15)
        Me.lblCommitteMaster.TabIndex = 108
        Me.lblCommitteMaster.Text = "Disciplinary Committee"
        Me.lblCommitteMaster.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCommitteMaster
        '
        Me.cboCommitteMaster.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCommitteMaster.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCommitteMaster.FormattingEnabled = True
        Me.cboCommitteMaster.Location = New System.Drawing.Point(128, 33)
        Me.cboCommitteMaster.Name = "cboCommitteMaster"
        Me.cboCommitteMaster.Size = New System.Drawing.Size(161, 21)
        Me.cboCommitteMaster.TabIndex = 109
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(699, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(676, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'lvCommitteMember
        '
        Me.lvCommitteMember.BackColorOnChecked = False
        Me.lvCommitteMember.ColumnHeaders = Nothing
        Me.lvCommitteMember.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhType, Me.colhEmployee, Me.colhCompany, Me.colhDepartment, Me.colhContactNo, Me.objcolhCommitte, Me.objcolhMasterId, Me.colhMCategory, Me.colhEmail})
        Me.lvCommitteMember.CompulsoryColumns = ""
        Me.lvCommitteMember.FullRowSelect = True
        Me.lvCommitteMember.GridLines = True
        Me.lvCommitteMember.GroupingColumn = Nothing
        Me.lvCommitteMember.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvCommitteMember.HideSelection = False
        Me.lvCommitteMember.Location = New System.Drawing.Point(12, 161)
        Me.lvCommitteMember.MinColumnWidth = 50
        Me.lvCommitteMember.MultiSelect = False
        Me.lvCommitteMember.Name = "lvCommitteMember"
        Me.lvCommitteMember.OptionalColumns = ""
        Me.lvCommitteMember.ShowMoreItem = False
        Me.lvCommitteMember.ShowSaveItem = False
        Me.lvCommitteMember.ShowSelectAll = True
        Me.lvCommitteMember.ShowSizeAllColumnsToFit = True
        Me.lvCommitteMember.Size = New System.Drawing.Size(726, 248)
        Me.lvCommitteMember.Sortable = True
        Me.lvCommitteMember.TabIndex = 8
        Me.lvCommitteMember.UseCompatibleStateImageBehavior = False
        Me.lvCommitteMember.View = System.Windows.Forms.View.Details
        '
        'colhType
        '
        Me.colhType.Tag = "colhType"
        Me.colhType.Text = "Member Type"
        Me.colhType.Width = 100
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Members"
        Me.colhEmployee.Width = 180
        '
        'colhCompany
        '
        Me.colhCompany.DisplayIndex = 3
        Me.colhCompany.Tag = "colhCompany"
        Me.colhCompany.Text = "Company"
        Me.colhCompany.Width = 110
        '
        'colhDepartment
        '
        Me.colhDepartment.DisplayIndex = 4
        Me.colhDepartment.Tag = "colhDepartment"
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 110
        '
        'colhContactNo
        '
        Me.colhContactNo.DisplayIndex = 5
        Me.colhContactNo.Tag = "colhContactNo"
        Me.colhContactNo.Text = "Contact No"
        Me.colhContactNo.Width = 100
        '
        'objcolhCommitte
        '
        Me.objcolhCommitte.DisplayIndex = 6
        Me.objcolhCommitte.Tag = "objcolhCommitte"
        Me.objcolhCommitte.Text = ""
        Me.objcolhCommitte.Width = 0
        '
        'objcolhMasterId
        '
        Me.objcolhMasterId.DisplayIndex = 7
        Me.objcolhMasterId.Tag = "objcolhMasterId"
        Me.objcolhMasterId.Text = ""
        Me.objcolhMasterId.Width = 0
        '
        'colhMCategory
        '
        Me.colhMCategory.DisplayIndex = 2
        Me.colhMCategory.Tag = "colhMCategory"
        Me.colhMCategory.Text = "Member Category"
        Me.colhMCategory.Width = 120
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnAdd)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 415)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(749, 55)
        Me.objFooter.TabIndex = 7
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(537, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 123
        Me.btnEdit.Text = "&Edit/Delete"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(434, 13)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 122
        Me.btnAdd.Text = "&New"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(640, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 121
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(537, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 124
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        Me.btnDelete.Visible = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(749, 58)
        Me.eZeeHeader.TabIndex = 6
        Me.eZeeHeader.Title = "Committee Members List"
        '
        'colhEmail
        '
        Me.colhEmail.Tag = "colhEmail"
        Me.colhEmail.Text = "Email"
        '
        'frmCommitteeList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(749, 470)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCommitteeList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Committee Members List"
        Me.Panel1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvCommitteMember As eZee.Common.eZeeListView
    Friend WithEvents colhType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhCommitte As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCompany As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhContactNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMasterId As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblCommitteMaster As System.Windows.Forms.Label
    Friend WithEvents cboCommitteMaster As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchCommittee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents radAll As System.Windows.Forms.RadioButton
    Friend WithEvents radExternal As System.Windows.Forms.RadioButton
    Friend WithEvents radInternal As System.Windows.Forms.RadioButton
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnSearchMCategory As eZee.Common.eZeeGradientButton
    Friend WithEvents lblMembersCategory As System.Windows.Forms.Label
    Friend WithEvents cboMCategory As System.Windows.Forms.ComboBox
    Friend WithEvents colhMCategory As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmail As System.Windows.Forms.ColumnHeader
End Class
