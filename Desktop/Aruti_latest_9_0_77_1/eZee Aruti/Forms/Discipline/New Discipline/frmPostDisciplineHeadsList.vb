﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPostDisciplineHeadsList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPostDisciplineHeadsList"
    Private mblnCancel As Boolean = True
    Private mintDisciplineUnkid As Integer = -1
    Private objEmpED As clsEarningDeduction

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intFileUnkid As Integer) As Boolean
        Try
            mintDisciplineUnkid = intFileUnkid

            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objTranhead As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Dim dsFill As New DataSet
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsFill = objTranhead.getComboList("TranHead", True)
            dsFill = objTranhead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True)
            'Sohail (21 Aug 2015) -- End
            cboTransactionHead.ValueMember = "tranheadunkid"
            cboTransactionHead.DisplayMember = "name"
            cboTransactionHead.DataSource = dsFill.Tables("TranHead")

            dsFill = objMaster.getComboListForExemptHeadApprovalStatus("ExempApproval", True)
            With cboExempApprovStatus
                .BeginUpdate()
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = New DataView(dsFill.Tables("ExempApproval"), "Id <> " & enExempHeadApprovalStatus.All & " ", "", DataViewRowState.CurrentRows).ToTable
                If .Items.Count > 0 Then .SelectedIndex = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim lvItem As ListViewItem
        Dim dsTranHead As New DataSet
        Dim objTranHead As New clsTransactionHead
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objEmpED.GetList("List", , , , , , , , , , , , , , CInt(cboExempApprovStatus.SelectedValue), CInt(cboTransactionHead.SelectedValue), True, , , mintDisciplineUnkid)
            dsList = objEmpED.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, , , , CInt(cboExempApprovStatus.SelectedValue), CInt(cboTransactionHead.SelectedValue), , , mintDisciplineUnkid)
            'Sohail (21 Aug 2015) -- End

            RemoveHandler lvPostedHeads.SelectedIndexChanged, AddressOf lvPostedHeads_SelectedIndexChanged
            RemoveHandler lvPostedHeads.ItemChecked, AddressOf lvPostedHeads_ItemChecked
            RemoveHandler objPchkall.CheckedChanged, AddressOf objPchkall_CheckedChanged

            lvPostedHeads.Items.Clear()

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("trnheadname").ToString)
                lvItem.SubItems(colhETName.Index).Tag = dtRow.Item("tranheadunkid").ToString
                dsTranHead = objTranHead.GetList("TranHead", CInt(dtRow.Item("tranheadunkid")), , True)
                If dsTranHead.Tables("TranHead").Rows.Count > 0 Then
                    lvItem.SubItems.Add(dsTranHead.Tables("TranHead").Rows(0).Item("HeadType").ToString)
                    lvItem.SubItems.Add(dsTranHead.Tables("TranHead").Rows(0).Item("TypeOf").ToString)
                Else
                    lvItem.SubItems.Add("")
                    lvItem.SubItems.Add("")
                End If

                If CDec(dtRow.Item("amount").ToString) = 0 Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(Format(dtRow.Item("amount"), GUI.fmtCurrency))
                End If

                If CBool(dtRow.Item("isapproved")) = True Then
                    lvItem.ForeColor = Color.Blue
                    lvItem.SubItems.Add(Language.getMessage("clsDiscipline_Resolution", 1, "Approved").ToString)
                Else
                    lvItem.SubItems.Add(Language.getMessage("clsDiscipline_Resolution", 2, "Pending").ToString)
                End If

                lvItem.SubItems.Add(dtRow.Item("employeename").ToString)
                lvItem.SubItems(objcolhEmployee.Index).Tag = dtRow.Item("employeeunkid").ToString

                lvItem.Tag = dtRow.Item("edunkid").ToString
                lvPostedHeads.Items.Add(lvItem)
            Next

            lvPostedHeads.GroupingColumn = objcolhEmployee
            lvPostedHeads.DisplayGroups(True)

            If lvPostedHeads.Items.Count >= 4 Then
                colhETName.Width = 180 - 20
            Else
                colhETName.Width = 180
            End If


            If CInt(cboExempApprovStatus.SelectedValue) = enEDHeadApprovalStatus.Approved Then
                btnDisapprove.Visible = True
                btnApprove.Visible = False
            ElseIf CInt(cboExempApprovStatus.SelectedValue) = enEDHeadApprovalStatus.Pending Then
                btnApprove.Visible = True
                btnDisapprove.Visible = False
            Else
                btnDisapprove.Visible = False
                btnApprove.Visible = False
            End If

            AddHandler lvPostedHeads.ItemChecked, AddressOf lvPostedHeads_ItemChecked
            AddHandler objPchkall.CheckedChanged, AddressOf objPchkall_CheckedChanged
            AddHandler lvPostedHeads.SelectedIndexChanged, AddressOf lvPostedHeads_SelectedIndexChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteDisciplinePosting
            btnApprove.Enabled = User._Object.Privilege._AllowToApproveEarningDeduction
            btnDisapprove.Enabled = User._Object.Privilege._AllowToApproveEarningDeduction
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmPostDisciplineHeadsList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpED = New clsEarningDeduction
        Try
            Call Set_Logo(Me, gApplicationType)

            Call SetVisibility()

            Language.setLanguage(Me.Name)

            Call OtherSettings()

            Call FillCombo()

            Call Fill_List()

            lvPostedHeads.GridLines = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPostDisciplineHeadsList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmPostDisciplineHeadsList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objEmpED = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_Resolution.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_Resolution"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objSearchTransactionhead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSearchTransactionhead.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboTransactionHead.DisplayMember
                .ValueMember = cboTransactionHead.ValueMember
                .DataSource = CType(cboTransactionHead.DataSource, DataTable)
                .CodeMember = "code"
            End With
            If frm.DisplayDialog Then
                cboTransactionHead.SelectedValue = frm.SelectedValue
                cboTransactionHead.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSearchTransactionhead_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboExempApprovStatus.SelectedValue = 0
            cboTransactionHead.SelectedValue = 0
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvPostedHeads.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Earning/Deduction from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvPostedHeads.Select()
            Exit Sub
        End If
        Dim objTranHead As New clsTransactionHead
        Try

            If objEmpED.isUsed(CInt(lvPostedHeads.SelectedItems(0).SubItems(objcolhEmployee.Index).Tag), CInt(lvPostedHeads.SelectedItems(0).SubItems(colhETName.Index).Tag)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, This Earning/Deduction is in use."), enMsgBoxStyle.Information)
                Exit Try
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Earning/Deduction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim frm As New frmReasonSelection

                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objEmpED._FormName = mstrModuleName
                objEmpED._LoginEmployeeUnkid = 0
                objEmpED._ClientIP = getIP()
                objEmpED._HostName = getHostName()
                objEmpED._FromWeb = False
                objEmpED._AuditUserId = User._Object._Userunkid
objEmpED._CompanyUnkid = Company._Object._Companyunkid
                objEmpED._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objEmpED.Void(CInt(lvPostedHeads.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason)
                objEmpED.Void(CInt(lvPostedHeads.SelectedItems(0).Tag), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, FinancialYear._Object._DatabaseName)
                'Sohail (21 Aug 2015) -- End
            End If
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            objTranHead = Nothing
        End Try

    End Sub

    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnDisapprove.Click
        Dim strMsg As String = ""
        Dim blnIsApprove As Boolean
        Dim btn As Button = CType(sender, Button)
        If btn.Name = btnApprove.Name Then
            blnIsApprove = True
            strMsg = Language.getMessage(mstrModuleName, 4, "Are you sure you want to Approve selected ED Heads?")
        ElseIf btn.Name = btnDisapprove.Name Then
            blnIsApprove = False
            strMsg = Language.getMessage(mstrModuleName, 5, "Are you sure you want to Dispprove selected ED Heads?")
        End If
        If lvPostedHeads.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please Check atleast one ED Heads to Approve/Disapprove."), enMsgBoxStyle.Information)
            lvPostedHeads.Focus()
            Exit Sub
        ElseIf (eZeeMsgBox.Show(strMsg, CType(enMsgBoxStyle.YesNo + enMsgBoxStyle.Question, enMsgBoxStyle)) = Windows.Forms.DialogResult.No) Then
            Exit Sub
        End If


        Dim blnResult As Boolean
        Dim blnRefresh As Boolean = False
        Try
            For Each lvItem As ListViewItem In lvPostedHeads.CheckedItems
                With objEmpED
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    '._Edunkid = CInt(lvItem.Tag)
                    ._Edunkid(Nothing, FinancialYear._Object._DatabaseName) = CInt(lvItem.Tag)
                    'Sohail (21 Aug 2015) -- End
                    ._Isapproved = blnIsApprove
                    ._Approveruserunkid = User._Object._Userunkid
                    ._FormName = mstrModuleName
                    ._LoginEmployeeUnkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'blnResult = .Update(False)
                    blnResult = .Update(FinancialYear._Object._DatabaseName, False, Nothing, ConfigParameter._Object._CurrentDateAndTime)
                    'Sohail (21 Aug 2015) -- End
                    If blnResult = False AndAlso objEmpED._Message <> "" Then
                        eZeeMsgBox.Show(._Message, enMsgBoxStyle.Information)
                        Exit Try
                    End If
                    blnRefresh = True
                End With
            Next

            If blnRefresh = True Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Evensts "

    Private Sub objPchkall_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objPchkall.CheckedChanged
        Try
            RemoveHandler lvPostedHeads.ItemChecked, AddressOf lvPostedHeads_ItemChecked
            For Each lvItem As ListViewItem In lvPostedHeads.Items
                lvItem.Checked = CBool(objPchkall.CheckState)
            Next
            AddHandler lvPostedHeads.ItemChecked, AddressOf lvPostedHeads_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objPchkall_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvPostedHeads_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPostedHeads.ItemChecked
        Try
            RemoveHandler objPchkall.CheckedChanged, AddressOf objPchkall_CheckedChanged
            If lvPostedHeads.CheckedItems.Count <= 0 Then
                objPchkall.CheckState = CheckState.Unchecked
            ElseIf lvPostedHeads.CheckedItems.Count < lvPostedHeads.Items.Count Then
                objPchkall.CheckState = CheckState.Indeterminate
            ElseIf lvPostedHeads.CheckedItems.Count = lvPostedHeads.Items.Count Then
                objPchkall.CheckState = CheckState.Checked
            End If
            AddHandler objPchkall.CheckedChanged, AddressOf objPchkall_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPostedHeads_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvPostedHeads_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvPostedHeads.SelectedIndexChanged
        Try
            If lvPostedHeads.SelectedItems.Count > 0 Then
                If lvPostedHeads.SelectedItems(0).ForeColor = Color.Blue Then
                    btnDelete.Enabled = False
                Else
                    btnDelete.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPostedHeads_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnApprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnApprove.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDisapprove.GradientBackColor = GUI._ButttonBackColor
            Me.btnDisapprove.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblApprovalStatus.Text = Language._Object.getCaption(Me.lblApprovalStatus.Name, Me.lblApprovalStatus.Text)
            Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.Name, Me.btnApprove.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnDisapprove.Text = Language._Object.getCaption(Me.btnDisapprove.Name, Me.btnDisapprove.Text)
            Me.colhETName.Text = Language._Object.getCaption(CStr(Me.colhETName.Tag), Me.colhETName.Text)
            Me.colhEStatus.Text = Language._Object.getCaption(CStr(Me.colhEStatus.Tag), Me.colhEStatus.Text)
            Me.colhHeadType.Text = Language._Object.getCaption(CStr(Me.colhHeadType.Tag), Me.colhHeadType.Text)
            Me.colhTypeOF.Text = Language._Object.getCaption(CStr(Me.colhTypeOF.Tag), Me.colhTypeOF.Text)
            Me.colhAmount.Text = Language._Object.getCaption(CStr(Me.colhAmount.Tag), Me.colhAmount.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("clsDiscipline_Resolution", 1, "Approved")
            Language.setMessage("clsDiscipline_Resolution", 2, "Pending")
            Language.setMessage(mstrModuleName, 1, "Please select Earning/Deduction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Earning/Deduction?")
            Language.setMessage(mstrModuleName, 3, "Sorry, This Earning/Deduction is in use.")
            Language.setMessage(mstrModuleName, 4, "Are you sure you want to Approve selected ED Heads?")
            Language.setMessage(mstrModuleName, 5, "Are you sure you want to Dispprove selected ED Heads?")
            Language.setMessage(mstrModuleName, 6, "Please Check atleast one ED Heads to Approve/Disapprove.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class