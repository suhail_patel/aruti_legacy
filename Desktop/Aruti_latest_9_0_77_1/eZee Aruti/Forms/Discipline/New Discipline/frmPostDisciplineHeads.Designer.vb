﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPostDisciplineHeads
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPostDisciplineHeads))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbPostDisciplineHeads = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkOverwritePrevEDSlabHeads = New System.Windows.Forms.CheckBox
        Me.chkCopyPreviousEDSlab = New System.Windows.Forms.CheckBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTranHead = New eZee.Common.eZeeGradientButton
        Me.lblAmount = New System.Windows.Forms.Label
        Me.txtAmount = New eZee.TextBox.NumericTextBox
        Me.cboTrnHead = New System.Windows.Forms.ComboBox
        Me.lblTrnHead = New System.Windows.Forms.Label
        Me.lblTypeOf = New System.Windows.Forms.Label
        Me.cboTypeOf = New System.Windows.Forms.ComboBox
        Me.cboTrnHeadType = New System.Windows.Forms.ComboBox
        Me.lblTrnHeadType = New System.Windows.Forms.Label
        Me.lblIncident = New System.Windows.Forms.Label
        Me.txtIncident = New eZee.TextBox.AlphanumericTextBox
        Me.lblPersonalInvolved = New System.Windows.Forms.Label
        Me.txtPesonInvolved = New eZee.TextBox.AlphanumericTextBox
        Me.pnlMain.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbPostDisciplineHeads.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(424, 385)
        Me.pnlMain.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 330)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(424, 55)
        Me.objFooter.TabIndex = 87
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(212, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(315, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbPostDisciplineHeads
        '
        Me.gbPostDisciplineHeads.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbPostDisciplineHeads.BorderColor = System.Drawing.Color.Black
        Me.gbPostDisciplineHeads.Checked = False
        Me.gbPostDisciplineHeads.CollapseAllExceptThis = False
        Me.gbPostDisciplineHeads.CollapsedHoverImage = Nothing
        Me.gbPostDisciplineHeads.CollapsedNormalImage = Nothing
        Me.gbPostDisciplineHeads.CollapsedPressedImage = Nothing
        Me.gbPostDisciplineHeads.CollapseOnLoad = False
        Me.gbPostDisciplineHeads.Controls.Add(Me.chkOverwritePrevEDSlabHeads)
        Me.gbPostDisciplineHeads.Controls.Add(Me.chkCopyPreviousEDSlab)
        Me.gbPostDisciplineHeads.Controls.Add(Me.lblPeriod)
        Me.gbPostDisciplineHeads.Controls.Add(Me.cboPeriod)
        Me.gbPostDisciplineHeads.Controls.Add(Me.lblCostCenter)
        Me.gbPostDisciplineHeads.Controls.Add(Me.cboCostCenter)
        Me.gbPostDisciplineHeads.Controls.Add(Me.objbtnSearchTranHead)
        Me.gbPostDisciplineHeads.Controls.Add(Me.lblAmount)
        Me.gbPostDisciplineHeads.Controls.Add(Me.txtAmount)
        Me.gbPostDisciplineHeads.Controls.Add(Me.cboTrnHead)
        Me.gbPostDisciplineHeads.Controls.Add(Me.lblTrnHead)
        Me.gbPostDisciplineHeads.Controls.Add(Me.lblTypeOf)
        Me.gbPostDisciplineHeads.Controls.Add(Me.cboTypeOf)
        Me.gbPostDisciplineHeads.Controls.Add(Me.cboTrnHeadType)
        Me.gbPostDisciplineHeads.Controls.Add(Me.lblTrnHeadType)
        Me.gbPostDisciplineHeads.Controls.Add(Me.lblIncident)
        Me.gbPostDisciplineHeads.Controls.Add(Me.txtIncident)
        Me.gbPostDisciplineHeads.Controls.Add(Me.lblPersonalInvolved)
        Me.gbPostDisciplineHeads.Controls.Add(Me.txtPesonInvolved)
        Me.gbPostDisciplineHeads.ExpandedHoverImage = Nothing
        Me.gbPostDisciplineHeads.ExpandedNormalImage = Nothing
        Me.gbPostDisciplineHeads.ExpandedPressedImage = Nothing
        Me.gbPostDisciplineHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPostDisciplineHeads.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPostDisciplineHeads.HeaderHeight = 25
        Me.gbPostDisciplineHeads.HeaderMessage = ""
        Me.gbPostDisciplineHeads.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPostDisciplineHeads.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPostDisciplineHeads.HeightOnCollapse = 0
        Me.gbPostDisciplineHeads.LeftTextSpace = 0
        Me.gbPostDisciplineHeads.Location = New System.Drawing.Point(0, 0)
        Me.gbPostDisciplineHeads.Name = "gbPostDisciplineHeads"
        Me.gbPostDisciplineHeads.OpenHeight = 300
        Me.gbPostDisciplineHeads.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPostDisciplineHeads.ShowBorder = True
        Me.gbPostDisciplineHeads.ShowCheckBox = False
        Me.gbPostDisciplineHeads.ShowCollapseButton = False
        Me.gbPostDisciplineHeads.ShowDefaultBorderColor = True
        Me.gbPostDisciplineHeads.ShowDownButton = False
        Me.gbPostDisciplineHeads.ShowHeader = True
        Me.gbPostDisciplineHeads.Size = New System.Drawing.Size(424, 330)
        Me.gbPostDisciplineHeads.TabIndex = 1
        Me.gbPostDisciplineHeads.Temp = 0
        Me.gbPostDisciplineHeads.Text = "Post Disciplinary Heads Info"
        Me.gbPostDisciplineHeads.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkOverwritePrevEDSlabHeads
        '
        Me.chkOverwritePrevEDSlabHeads.Enabled = False
        Me.chkOverwritePrevEDSlabHeads.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkOverwritePrevEDSlabHeads.Location = New System.Drawing.Point(121, 306)
        Me.chkOverwritePrevEDSlabHeads.Name = "chkOverwritePrevEDSlabHeads"
        Me.chkOverwritePrevEDSlabHeads.Size = New System.Drawing.Size(268, 17)
        Me.chkOverwritePrevEDSlabHeads.TabIndex = 260
        Me.chkOverwritePrevEDSlabHeads.Text = "Overwrite Previous ED Slab heads"
        Me.chkOverwritePrevEDSlabHeads.UseVisualStyleBackColor = True
        '
        'chkCopyPreviousEDSlab
        '
        Me.chkCopyPreviousEDSlab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCopyPreviousEDSlab.Location = New System.Drawing.Point(121, 283)
        Me.chkCopyPreviousEDSlab.Name = "chkCopyPreviousEDSlab"
        Me.chkCopyPreviousEDSlab.Size = New System.Drawing.Size(268, 17)
        Me.chkCopyPreviousEDSlab.TabIndex = 259
        Me.chkCopyPreviousEDSlab.Text = "Copy Previous ED Slab"
        Me.chkCopyPreviousEDSlab.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 123)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(107, 15)
        Me.lblPeriod.TabIndex = 257
        Me.lblPeriod.Text = "Effective Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 215
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(121, 121)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(268, 21)
        Me.cboPeriod.TabIndex = 256
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(8, 259)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(107, 15)
        Me.lblCostCenter.TabIndex = 254
        Me.lblCostCenter.Text = "Cost Center"
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(121, 256)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(268, 21)
        Me.cboCostCenter.TabIndex = 253
        '
        'objbtnSearchTranHead
        '
        Me.objbtnSearchTranHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTranHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTranHead.BorderSelected = False
        Me.objbtnSearchTranHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTranHead.Image = CType(resources.GetObject("objbtnSearchTranHead.Image"), System.Drawing.Image)
        Me.objbtnSearchTranHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTranHead.Location = New System.Drawing.Point(395, 202)
        Me.objbtnSearchTranHead.Name = "objbtnSearchTranHead"
        Me.objbtnSearchTranHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTranHead.TabIndex = 251
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(8, 232)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(107, 15)
        Me.lblAmount.TabIndex = 250
        Me.lblAmount.Text = "Amount"
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = True
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Flags = 0
        Me.txtAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(121, 229)
        Me.txtAmount.MaxDecimalPlaces = 6
        Me.txtAmount.MaxWholeDigits = 21
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAmount.Size = New System.Drawing.Size(268, 21)
        Me.txtAmount.TabIndex = 249
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboTrnHead
        '
        Me.cboTrnHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHead.FormattingEnabled = True
        Me.cboTrnHead.Location = New System.Drawing.Point(121, 202)
        Me.cboTrnHead.Name = "cboTrnHead"
        Me.cboTrnHead.Size = New System.Drawing.Size(268, 21)
        Me.cboTrnHead.TabIndex = 247
        '
        'lblTrnHead
        '
        Me.lblTrnHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHead.Location = New System.Drawing.Point(8, 205)
        Me.lblTrnHead.Name = "lblTrnHead"
        Me.lblTrnHead.Size = New System.Drawing.Size(107, 15)
        Me.lblTrnHead.TabIndex = 248
        Me.lblTrnHead.Text = "Transaction Head"
        '
        'lblTypeOf
        '
        Me.lblTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTypeOf.Location = New System.Drawing.Point(8, 178)
        Me.lblTypeOf.Name = "lblTypeOf"
        Me.lblTypeOf.Size = New System.Drawing.Size(107, 15)
        Me.lblTypeOf.TabIndex = 246
        Me.lblTypeOf.Text = "Trans. Type Of"
        '
        'cboTypeOf
        '
        Me.cboTypeOf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTypeOf.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTypeOf.FormattingEnabled = True
        Me.cboTypeOf.Location = New System.Drawing.Point(121, 175)
        Me.cboTypeOf.Name = "cboTypeOf"
        Me.cboTypeOf.Size = New System.Drawing.Size(268, 21)
        Me.cboTypeOf.TabIndex = 245
        '
        'cboTrnHeadType
        '
        Me.cboTrnHeadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTrnHeadType.FormattingEnabled = True
        Me.cboTrnHeadType.Location = New System.Drawing.Point(121, 148)
        Me.cboTrnHeadType.Name = "cboTrnHeadType"
        Me.cboTrnHeadType.Size = New System.Drawing.Size(268, 21)
        Me.cboTrnHeadType.TabIndex = 243
        '
        'lblTrnHeadType
        '
        Me.lblTrnHeadType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrnHeadType.Location = New System.Drawing.Point(8, 151)
        Me.lblTrnHeadType.Name = "lblTrnHeadType"
        Me.lblTrnHeadType.Size = New System.Drawing.Size(107, 15)
        Me.lblTrnHeadType.TabIndex = 244
        Me.lblTrnHeadType.Text = "Trans. Head Type"
        '
        'lblIncident
        '
        Me.lblIncident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncident.Location = New System.Drawing.Point(8, 63)
        Me.lblIncident.Name = "lblIncident"
        Me.lblIncident.Size = New System.Drawing.Size(107, 15)
        Me.lblIncident.TabIndex = 241
        Me.lblIncident.Text = "Incident"
        Me.lblIncident.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIncident
        '
        Me.txtIncident.BackColor = System.Drawing.Color.White
        Me.txtIncident.Flags = 0
        Me.txtIncident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIncident.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtIncident.Location = New System.Drawing.Point(121, 60)
        Me.txtIncident.Multiline = True
        Me.txtIncident.Name = "txtIncident"
        Me.txtIncident.ReadOnly = True
        Me.txtIncident.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtIncident.Size = New System.Drawing.Size(268, 55)
        Me.txtIncident.TabIndex = 240
        '
        'lblPersonalInvolved
        '
        Me.lblPersonalInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPersonalInvolved.Location = New System.Drawing.Point(8, 36)
        Me.lblPersonalInvolved.Name = "lblPersonalInvolved"
        Me.lblPersonalInvolved.Size = New System.Drawing.Size(107, 15)
        Me.lblPersonalInvolved.TabIndex = 238
        Me.lblPersonalInvolved.Text = "Person Involved"
        Me.lblPersonalInvolved.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPesonInvolved
        '
        Me.txtPesonInvolved.BackColor = System.Drawing.SystemColors.Window
        Me.txtPesonInvolved.Flags = 0
        Me.txtPesonInvolved.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPesonInvolved.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPesonInvolved.Location = New System.Drawing.Point(121, 33)
        Me.txtPesonInvolved.Name = "txtPesonInvolved"
        Me.txtPesonInvolved.ReadOnly = True
        Me.txtPesonInvolved.Size = New System.Drawing.Size(268, 21)
        Me.txtPesonInvolved.TabIndex = 239
        '
        'frmPostDisciplineHeads
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(424, 385)
        Me.Controls.Add(Me.gbPostDisciplineHeads)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPostDisciplineHeads"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Post Disciplinary Heads"
        Me.pnlMain.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbPostDisciplineHeads.ResumeLayout(False)
        Me.gbPostDisciplineHeads.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbPostDisciplineHeads As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblIncident As System.Windows.Forms.Label
    Friend WithEvents txtIncident As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPersonalInvolved As System.Windows.Forms.Label
    Friend WithEvents txtPesonInvolved As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboTrnHeadType As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrnHeadType As System.Windows.Forms.Label
    Friend WithEvents lblTypeOf As System.Windows.Forms.Label
    Friend WithEvents cboTypeOf As System.Windows.Forms.ComboBox
    Friend WithEvents cboTrnHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrnHead As System.Windows.Forms.Label
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents objbtnSearchTranHead As eZee.Common.eZeeGradientButton
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents chkOverwritePrevEDSlabHeads As System.Windows.Forms.CheckBox
    Friend WithEvents chkCopyPreviousEDSlab As System.Windows.Forms.CheckBox
End Class
