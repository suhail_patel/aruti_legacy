﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCommittee_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCommittee_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtTran As DataTable
    Private objCommittee As clsDiscipline_Committee
    Private mintCommitteeMasterUnkid As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintCommitteeMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintCommitteeMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboCommitteMaster.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtComany.BackColor = GUI.ColorOptional
            txtPosition.BackColor = GUI.ColorOptional
            txtTrainersContactNo.BackColor = GUI.ColorOptional
            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            txtEmail.BackColor = GUI.ColorOptional
            'S.SANDEEP [24 MAY 2016] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objCMaster As New clsCommon_Master
        Dim objEMaster As New clsEmployee_Master
        Try
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE, True, "List")
            With cboCommitteMaster
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEMaster.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEMaster.GetEmployeeList("List", True)
            'End If
            dsList = objEMaster.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END


            
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 19 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMMITTEE_MEMBERS_CATEGORY, True, "List")
            With cboMCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 19 DEC 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Try
            lvInvestigatorInfo.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In mdtTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem
                    
                    If CInt(dtRow.Item("employeeunkid")) <> -1 Then
                        lvItem.Text = dtRow.Item("employee_name").ToString
                        'S.SANDEEP [ 19 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        lvItem.SubItems.Add(dtRow.Item("mcategoryname").ToString)
                        'S.SANDEEP [ 19 DEC 2012 ] -- END
                        lvItem.SubItems.Add(dtRow.Item("department").ToString)
                        lvItem.SubItems.Add(Company._Object._Name.ToString)
                        lvItem.SubItems.Add(dtRow.Item("contact").ToString)
                    Else
                        lvItem.Text = dtRow.Item("ex_name").ToString
                        'S.SANDEEP [ 19 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        lvItem.SubItems.Add(dtRow.Item("mcategoryname").ToString)
                        'S.SANDEEP [ 19 DEC 2012 ] -- END
                        lvItem.SubItems.Add(dtRow.Item("ex_department").ToString)
                        lvItem.SubItems.Add(dtRow.Item("ex_company").ToString)
                        lvItem.SubItems.Add(dtRow.Item("ex_contactno").ToString)
                    End If
                    'S.SANDEEP [24 MAY 2016] -- START
                    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                    lvItem.SubItems.Add(dtRow.Item("ex_email").ToString)
                    'S.SANDEEP [24 MAY 2016] -- END                    
                    lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    lvItem.Tag = dtRow.Item("committeetranunkid")
                    'S.SANDEEP [ 19 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(dtRow.Item("mcategoryunkid").ToString)
                    'S.SANDEEP [ 19 DEC 2012 ] -- END

                    lvInvestigatorInfo.Items.Add(lvItem)
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            radEmployee.Checked = True
            cboEmployee.SelectedValue = 0
            txtComany.Text = ""
            txtName.Text = ""
            txtPosition.Text = ""
            txtTrainersContactNo.Text = ""
            'S.SANDEEP [ 19 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboEmployee.Enabled = True : objbtnSearchEmployee.Enabled = True
            btnAddInvestigator.Enabled = True : cboMCategory.SelectedValue = 0
            'S.SANDEEP [ 19 DEC 2012 ] -- END

            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            txtEmail.Text = ""
            'S.SANDEEP [24 MAY 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboCommitteMaster.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Discipline Committee is mandatory information. Please select Discipline Committee to continue."), enMsgBoxStyle.Information)
                cboCommitteMaster.Focus()
                Return False
            End If

            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            If txtEmail.Text.Trim.Length > 0 Then
                Dim expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
                If expression.IsMatch(txtEmail.Text.Trim) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Invalid Email. Please Enter Valid Email"), enMsgBoxStyle.Information)
                    txtEmail.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP [24 MAY 2016] -- END

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub EnableDisableCommittee(ByVal blnValue As Boolean)
        Try
            cboCommitteMaster.Enabled = blnValue
            'S.SANDEEP [ 19 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objbtnAddCommittee.Enabled = blnValue : objbtnSearchCommittee.Enabled = blnValue
            'S.SANDEEP [ 19 DEC 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableDisableCommittee", mstrModuleName)
        Finally
        End Try
    End Sub
    'Anjan (17 Apr 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private Sub SetVisibility()
        Try
            btnAddInvestigator.Enabled = User._Object.Privilege._AllowtoAddDisciplineCommittee
            btnEditInvestigator.Enabled = User._Object.Privilege._AllowtoEditDisciplineCommittee
            btnDeleteInvestigator.Enabled = User._Object.Privilege._AllowtoDeleteDisciplineCommittee
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Anjan (17 Apr 2012)-End 
#End Region

#Region " Form's Events "

    Private Sub frmCommittee_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCommittee = New clsDiscipline_Committee
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            Call SetVisibility()
            'Anjan (17 Apr 2012)-End 

            SetColor()
            FillCombo()

            If menAction = enAction.EDIT_ONE Then
                'Pinkal (22-Jan-2021) -- Start
                'Enhancement NMB - Working Discipline changes required by NMB.
                'objCommittee._CommitteeMasterId(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = mintCommitteeMasterUnkid
                objCommittee._CommitteeMasterId(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._DatabaseName, ConfigParameter._Object._IsIncludeInactiveEmp) = mintCommitteeMasterUnkid
                'Pinkal (22-Jan-2021) -- End

                cboCommitteMaster.SelectedValue = mintCommitteeMasterUnkid
                cboCommitteMaster.Enabled = False
                'S.SANDEEP [ 19 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                objbtnAddCommittee.Enabled = False : objbtnSearchCommittee.Enabled = False
                'S.SANDEEP [ 19 DEC 2012 ] -- END
            End If


            radEmployee.Checked = True

            mdtTran = objCommittee._DataTable

            Call Fill_List()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommittee_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCommittee_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommittee_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCommittee_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objCommittee = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDiscipline_Committee.SetMessages()
            objfrm._Other_ModuleNames = "clsDiscipline_Committee"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchCommittee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCommittee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboCommitteMaster.DisplayMember
                .ValueMember = cboCommitteMaster.ValueMember
                .DataSource = CType(cboCommitteMaster.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboCommitteMaster.SelectedValue = frm.SelectedValue
                cboCommitteMaster.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCommittee_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboEmployee.DisplayMember
                .ValueMember = cboEmployee.ValueMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnAddInvestigator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddInvestigator.Click
        Try
            If Validation() = False Then Exit Sub
            If radEmployee.Checked = True Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information, it cannot be blank. Please select an Employee to continue."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Dim dtRow As DataRow() = mdtTran.Select("employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND AUD <> 'D' ")
                If dtRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Selected Employee is already added to the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            ElseIf radOthers.Checked = True Then
                If txtName.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Name is compulsory information, it can not be blank. Please enter Name to continue."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Dim dtRow As DataRow() = mdtTran.Select("ex_name LIKE '" & txtName.Text & "' AND AUD <> 'D' ")
                If dtRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Entered Employee is already added to the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            If txtComany.Text.Trim = Company._Object._Name Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Company cannot be same. Please enter another company name."), enMsgBoxStyle.Information)
                txtComany.Focus()
                Exit Sub
            End If

            Dim dRow As DataRow = Nothing
            dRow = mdtTran.NewRow
            dRow.Item("committeetranunkid") = -1
            dRow.Item("committeemasterunkid") = cboCommitteMaster.SelectedValue
            If radEmployee.Checked = True Then
                dRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
            ElseIf radOthers.Checked = True Then
                dRow.Item("employeeunkid") = -1
            End If
            dRow.Item("ex_name") = txtName.Text
            dRow.Item("ex_company") = txtPosition.Text
            dRow.Item("ex_department") = txtComany.Text
            dRow.Item("ex_contactno") = txtTrainersContactNo.Text
            dRow.Item("isactive") = True
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("employee_name") = cboEmployee.Text
            dRow.Item("company") = objlblCompanyValue.Text
            dRow.Item("department") = objlblDepartmentValue.Text
            dRow.Item("contact") = objlblEmployeeContactNo.Text
            'S.SANDEEP [ 19 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dRow.Item("mcategoryunkid") = CInt(cboMCategory.SelectedValue)
            If CInt(cboMCategory.SelectedValue) > 0 Then
                dRow.Item("mcategoryname") = cboMCategory.Text
            End If
            'S.SANDEEP [ 19 DEC 2012 ] -- END

            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            If radOthers.Checked = True Then
                dRow.Item("ex_email") = txtEmail.Text
            ElseIf radEmployee.Checked = True Then
                dRow.Item("ex_email") = ""
            End If
            'S.SANDEEP [24 MAY 2016] -- END

            mdtTran.Rows.Add(dRow)

            Call Fill_List()
            Call ResetValue()

            EnableDisableCommittee(False)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddInvestigator_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEditInvestigator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditInvestigator.Click
        Try
            If Validation() = False Then Exit Sub

            If lvInvestigatorInfo.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvInvestigatorInfo.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtTran.Select("GUID = '" & lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtTran.Select("committeetranunkid = '" & CInt(lvInvestigatorInfo.SelectedItems(0).Tag) & "'")
                End If

                If drTemp.Length > 0 Then
                    Dim dtRow As DataRow() = Nothing
                    If CInt(drTemp(0)("employeeunkid")) <> -1 Then
                        If CInt(lvInvestigatorInfo.SelectedItems(0).Tag) = -1 Then
                            dtRow = mdtTran.Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' AND AUD <> 'D' AND GUID <> '" & lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                        Else
                            dtRow = mdtTran.Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' AND AUD <> 'D' AND committeetranunkid <> " & CInt(lvInvestigatorInfo.SelectedItems(0).Tag))
                        End If
                    Else
                        If CInt(lvInvestigatorInfo.SelectedItems(0).Tag) = -1 Then
                            dtRow = mdtTran.Select("ex_name = '" & txtName.Text & "' AND AUD <> 'D' AND GUID <> '" & lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                    Else
                            dtRow = mdtTran.Select("ex_name = '" & txtName.Text & "' AND AUD <> 'D' AND committeetranunkid <> " & CInt(lvInvestigatorInfo.SelectedItems(0).Tag))
                        End If
                    End If
                    If dtRow.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected member is already present in the list."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    With drTemp(0)
                        .Item("committeetranunkid") = .Item("committeetranunkid")
                        .Item("committeemasterunkid") = cboCommitteMaster.SelectedValue
                        If radEmployee.Checked = True Then
                            .Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                        ElseIf radOthers.Checked = True Then
                            .Item("employeeunkid") = -1
                        End If
                        .Item("ex_name") = txtName.Text
                        .Item("ex_company") = txtPosition.Text
                        .Item("ex_department") = txtComany.Text
                        .Item("ex_contactno") = txtTrainersContactNo.Text
                        .Item("isactive") = True
                        'S.SANDEEP [ 19 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        '.Item("AUD") = "A"
                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If
                        'S.SANDEEP [ 19 DEC 2012 ] -- END
                        .Item("GUID") = Guid.NewGuid.ToString
                        .Item("employee_name") = cboEmployee.Text
                        .Item("company") = objlblCompanyValue.Text
                        .Item("department") = objlblDepartmentValue.Text
                        .Item("contact") = objlblEmployeeContactNo.Text

                        'S.SANDEEP [ 19 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        .Item("mcategoryunkid") = CInt(cboMCategory.SelectedValue)
                        If CInt(cboMCategory.SelectedValue) > 0 Then
                            .Item("mcategoryname") = cboMCategory.Text
                        Else
                            .Item("mcategoryname") = ""
                        End If
                        'S.SANDEEP [ 19 DEC 2012 ] -- END

                        'S.SANDEEP [24 MAY 2016] -- START
                        'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                        If radOthers.Checked = True Then
                            .Item("ex_email") = txtEmail.Text
                        ElseIf radEmployee.Checked = True Then
                            .Item("ex_email") = ""
                        End If
                        'S.SANDEEP [24 MAY 2016] -- END

                        .AcceptChanges()
                    End With
                End If
                Call Fill_List()
                Call ResetValue()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditInvestigator_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDeleteInvestigator_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteInvestigator.Click
        Try
            If lvInvestigatorInfo.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow() = Nothing
                If CInt(lvInvestigatorInfo.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtTran.Select("GUID = '" & lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    If objCommittee.isUsed(CInt(lvInvestigatorInfo.SelectedItems(0).Tag)) = False Then
                        drTemp = mdtTran.Select("committeetranunkid = '" & CInt(lvInvestigatorInfo.SelectedItems(0).Tag) & "'")
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, You cannot delete this member. Reason : This member is already linked with some of the resolution steps."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    Call Fill_List()
                End If
            End If
            Call ResetValue()
            If lvInvestigatorInfo.Items.Count <= 0 Then EnableDisableCommittee(True)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteInvestigator_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then Exit Sub
            If lvInvestigatorInfo.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please add atleast one member in order to save."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            objCommittee._DataTable = mdtTran

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objCommittee._FormName = mstrModuleName
            objCommittee._LoginEmployeeunkid = 0
            objCommittee._ClientIP = getIP()
            objCommittee._HostName = getHostName()
            objCommittee._FromWeb = False
            objCommittee._AuditUserId = User._Object._Userunkid
objCommittee._CompanyUnkid = Company._Object._Companyunkid
            objCommittee._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            blnFlag = objCommittee.InsertUpdateDelete_Members

            If blnFlag Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Members Successfully Saved."), enMsgBoxStyle.Information)
                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 19 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnAddMCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddMCategory.Click
        Dim frm As New frmCommonMaster
        Try
            Dim iRetVal As Integer = -1
            frm.displayDialog(iRetVal, clsCommon_Master.enCommonMaster.COMMITTEE_MEMBERS_CATEGORY, enAction.ADD_ONE)
            If iRetVal > 0 Then
                Dim objCMaster As New clsCommon_Master
                Dim dList As New DataSet
                dList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMMITTEE_MEMBERS_CATEGORY, True, "List")
                With cboMCategory
                    .DisplayMember = "name"
                    .ValueMember = "masterunkid"
                    .DataSource = dList.Tables("List")
                    .SelectedValue = iRetVal
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnAddMCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddCommittee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddCommittee.Click
        Dim frm As New frmCommonMaster
        Try
            Dim iRetVal As Integer = -1
            frm.displayDialog(iRetVal, clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE, enAction.ADD_ONE)
            If iRetVal > 0 Then
                Dim objCMaster As New clsCommon_Master
                Dim dList As New DataSet
                dList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE, True, "List")
                With cboCommitteMaster
                    .DisplayMember = "name"
                    .ValueMember = "masterunkid"
                    .DataSource = dList.Tables("List")
                    .SelectedValue = iRetVal
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnAddCommittee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchMCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMCategory.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DisplayMember = cboMCategory.DisplayMember
                .ValueMember = cboMCategory.ValueMember
                .DataSource = CType(cboMCategory.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboMCategory.SelectedValue = frm.SelectedValue
                cboMCategory.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchMCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 19 DEC 2012 ] -- END

#End Region

#Region " Controls Events "

    Private Sub radOthers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radOthers.CheckedChanged, radEmployee.CheckedChanged
        Try
            If CType(sender, RadioButton).Name.ToString.ToUpper = "RADOTHERS" AndAlso CType(sender, RadioButton).Checked = True Then
                pnlOthers.Enabled = True
                cboEmployee.Enabled = False
                objlblCompanyValue.Text = ""
                objlblDepartmentValue.Text = ""
                objlblEmployeeContactNo.Text = ""
                objbtnSearchEmployee.Enabled = False
                'S.SANDEEP [24 MAY 2016] -- START
                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                cboEmployee.SelectedValue = 0
                objlblEmployeeEmail.Text = ""
                'S.SANDEEP [24 MAY 2016] -- END

            ElseIf CType(sender, RadioButton).Name.ToString.ToUpper = "RADEMPLOYEE" AndAlso CType(sender, RadioButton).Checked = True Then
                cboEmployee.Enabled = True
                pnlOthers.Enabled = False
                objlblCompanyValue.Text = ""
                objlblDepartmentValue.Text = ""
                objlblEmployeeContactNo.Text = ""
                objbtnSearchEmployee.Enabled = True
                'S.SANDEEP [24 MAY 2016] -- START
                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                cboEmployee.SelectedValue = 0
                objlblEmployeeEmail.Text = ""
                'S.SANDEEP [24 MAY 2016] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radOthers_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                Dim objDept As New clsDepartment

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [04 JUN 2015] -- END

                objDept._Departmentunkid = objEmp._Departmentunkid
                objlblDepartmentValue.Text = objDept._Name
                objlblCompanyValue.Text = Company._Object._Name
                objlblEmployeeContactNo.Text = objEmp._Present_Tel_No
                'S.SANDEEP [24 MAY 2016] -- START
                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                objlblEmployeeEmail.Text = objEmp._Email
                'S.SANDEEP [24 MAY 2016] -- END

            Else
                objlblDepartmentValue.Text = ""
                objlblCompanyValue.Text = ""
                objlblEmployeeContactNo.Text = ""
                'S.SANDEEP [24 MAY 2016] -- START
                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                objlblEmployeeEmail.Text = ""
                'S.SANDEEP [24 MAY 2016] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvInvestigatorInfo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvInvestigatorInfo.SelectedIndexChanged
        Try
            If lvInvestigatorInfo.SelectedItems.Count > 0 Then
                cboEmployee.SelectedValue = 0

                txtName.Text = "" : txtPosition.Text = "" : txtComany.Text = "" : txtTrainersContactNo.Text = ""
                'S.SANDEEP [24 MAY 2016] -- START
                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                txtEmail.Text = ""
                'S.SANDEEP [24 MAY 2016] -- END

                'S.SANDEEP [ 19 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                cboMCategory.SelectedValue = CInt(lvInvestigatorInfo.SelectedItems(0).SubItems(objMCatId.Index).Text)
                'S.SANDEEP [ 19 DEC 2012 ] -- END

                If CInt(lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhEmpId.Index).Text) <> -1 Then
                    radEmployee.Enabled = True : radEmployee.Checked = True
                    cboEmployee.SelectedValue = CInt(lvInvestigatorInfo.SelectedItems(0).SubItems(objcolhEmpId.Index).Text)
                    radOthers.Enabled = False : pnlOthers.Enabled = False
                    'S.SANDEEP [ 19 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    cboEmployee.Enabled = False : objbtnSearchEmployee.Enabled = False : btnAddInvestigator.Enabled = False
                    'S.SANDEEP [ 19 DEC 2012 ] -- END
                Else
                    radEmployee.Enabled = False : pnlOthers.Enabled = True
                    radOthers.Enabled = True : radOthers.Checked = True
                    txtName.Text = lvInvestigatorInfo.SelectedItems(0).SubItems(colhInvestigator.Index).Text
                    txtPosition.Text = lvInvestigatorInfo.SelectedItems(0).SubItems(colhDepartment.Index).Text
                    txtComany.Text = lvInvestigatorInfo.SelectedItems(0).SubItems(colhCompany.Index).Text
                    txtTrainersContactNo.Text = lvInvestigatorInfo.SelectedItems(0).SubItems(colhContactNo.Index).Text
                    'S.SANDEEP [24 MAY 2016] -- START
                    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                    txtEmail.Text = lvInvestigatorInfo.SelectedItems(0).SubItems(colhEmail.Index).Text
                    'S.SANDEEP [24 MAY 2016] -- END

                    'S.SANDEEP [ 19 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    btnAddInvestigator.Enabled = False
                    'S.SANDEEP [ 19 DEC 2012 ] -- END
                End If
            Else
                cboEmployee.SelectedValue = 0
                radEmployee.Enabled = True : radOthers.Enabled = True
                'S.SANDEEP [ 19 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                cboEmployee.Enabled = True : objbtnSearchEmployee.Enabled = True : btnAddInvestigator.Enabled = True
                'S.SANDEEP [ 19 DEC 2012 ] -- END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvInvestigatorInfo_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbCommitteAddEdit.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCommitteAddEdit.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteInvestigator.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteInvestigator.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditInvestigator.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditInvestigator.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddInvestigator.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddInvestigator.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbCommitteAddEdit.Text = Language._Object.getCaption(Me.gbCommitteAddEdit.Name, Me.gbCommitteAddEdit.Text)
            Me.lblCommitteMaster.Text = Language._Object.getCaption(Me.lblCommitteMaster.Name, Me.lblCommitteMaster.Text)
            Me.radEmployee.Text = Language._Object.getCaption(Me.radEmployee.Name, Me.radEmployee.Text)
            Me.radOthers.Text = Language._Object.getCaption(Me.radOthers.Name, Me.radOthers.Text)
            Me.lblTrainerContactNo.Text = Language._Object.getCaption(Me.lblTrainerContactNo.Name, Me.lblTrainerContactNo.Text)
            Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
            Me.lblPosition.Text = Language._Object.getCaption(Me.lblPosition.Name, Me.lblPosition.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.colhInvestigator.Text = Language._Object.getCaption(CStr(Me.colhInvestigator.Tag), Me.colhInvestigator.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.colhCompany.Text = Language._Object.getCaption(CStr(Me.colhCompany.Tag), Me.colhCompany.Text)
            Me.colhContactNo.Text = Language._Object.getCaption(CStr(Me.colhContactNo.Tag), Me.colhContactNo.Text)
            Me.btnDeleteInvestigator.Text = Language._Object.getCaption(Me.btnDeleteInvestigator.Name, Me.btnDeleteInvestigator.Text)
            Me.btnEditInvestigator.Text = Language._Object.getCaption(Me.btnEditInvestigator.Name, Me.btnEditInvestigator.Text)
            Me.btnAddInvestigator.Text = Language._Object.getCaption(Me.btnAddInvestigator.Name, Me.btnAddInvestigator.Text)
            Me.lblEmpContact.Text = Language._Object.getCaption(Me.lblEmpContact.Name, Me.lblEmpContact.Text)
            Me.lblEmpCompany.Text = Language._Object.getCaption(Me.lblEmpCompany.Name, Me.lblEmpCompany.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.lblMembersCategory.Text = Language._Object.getCaption(Me.lblMembersCategory.Name, Me.lblMembersCategory.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.colhMCategory.Text = Language._Object.getCaption(CStr(Me.colhMCategory.Tag), Me.colhMCategory.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information, it cannot be blank. Please select an Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Selected Employee is already added to the list.")
            Language.setMessage(mstrModuleName, 3, "Name is compulsory information, it can not be blank. Please enter Name to continue.")
            Language.setMessage(mstrModuleName, 4, "Entered Employee is already added to the list.")
            Language.setMessage(mstrModuleName, 5, "Company cannot be same. Please enter another company name.")
            Language.setMessage(mstrModuleName, 6, "Selected member already peresent in the list.")
            Language.setMessage(mstrModuleName, 7, "Discipline Committee is mandatory information. Please select Discipline Committee to continue.")
            Language.setMessage(mstrModuleName, 8, "Please add atleast one member in order to save.")
            Language.setMessage(mstrModuleName, 9, "Members Successfully Saved.")
	    Language.setMessage(mstrModuleName, 10, "Sorry, You cannot delete this member. Reason : This member is already linked with some of the resolution steps.")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class