﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJobGroup_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmJobGroup_AddEdit))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbJobGroup = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblGeneralEvaluation = New System.Windows.Forms.Label
        Me.txtGeneralWeight = New eZee.TextBox.NumericTextBox
        Me.lblBalanceScoreCard = New System.Windows.Forms.Label
        Me.txtBSCWeight = New eZee.TextBox.NumericTextBox
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbJobGroup.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Controls.Add(Me.gbJobGroup)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(382, 211)
        Me.Panel1.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 156)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(382, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(170, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(273, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbJobGroup
        '
        Me.gbJobGroup.BorderColor = System.Drawing.Color.Black
        Me.gbJobGroup.Checked = False
        Me.gbJobGroup.CollapseAllExceptThis = False
        Me.gbJobGroup.CollapsedHoverImage = Nothing
        Me.gbJobGroup.CollapsedNormalImage = Nothing
        Me.gbJobGroup.CollapsedPressedImage = Nothing
        Me.gbJobGroup.CollapseOnLoad = False
        Me.gbJobGroup.Controls.Add(Me.lblGeneralEvaluation)
        Me.gbJobGroup.Controls.Add(Me.txtGeneralWeight)
        Me.gbJobGroup.Controls.Add(Me.lblBalanceScoreCard)
        Me.gbJobGroup.Controls.Add(Me.txtBSCWeight)
        Me.gbJobGroup.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbJobGroup.Controls.Add(Me.lblDescription)
        Me.gbJobGroup.Controls.Add(Me.lblName)
        Me.gbJobGroup.Controls.Add(Me.txtDescription)
        Me.gbJobGroup.Controls.Add(Me.txtName)
        Me.gbJobGroup.Controls.Add(Me.txtCode)
        Me.gbJobGroup.Controls.Add(Me.lblCode)
        Me.gbJobGroup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbJobGroup.ExpandedHoverImage = Nothing
        Me.gbJobGroup.ExpandedNormalImage = Nothing
        Me.gbJobGroup.ExpandedPressedImage = Nothing
        Me.gbJobGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbJobGroup.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbJobGroup.HeaderHeight = 25
        Me.gbJobGroup.HeaderMessage = ""
        Me.gbJobGroup.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbJobGroup.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbJobGroup.HeightOnCollapse = 0
        Me.gbJobGroup.LeftTextSpace = 0
        Me.gbJobGroup.Location = New System.Drawing.Point(0, 0)
        Me.gbJobGroup.Name = "gbJobGroup"
        Me.gbJobGroup.OpenHeight = 300
        Me.gbJobGroup.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbJobGroup.ShowBorder = True
        Me.gbJobGroup.ShowCheckBox = False
        Me.gbJobGroup.ShowCollapseButton = False
        Me.gbJobGroup.ShowDefaultBorderColor = True
        Me.gbJobGroup.ShowDownButton = False
        Me.gbJobGroup.ShowHeader = True
        Me.gbJobGroup.Size = New System.Drawing.Size(382, 211)
        Me.gbJobGroup.TabIndex = 0
        Me.gbJobGroup.Temp = 0
        Me.gbJobGroup.Text = "Job Group Info"
        Me.gbJobGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGeneralEvaluation
        '
        Me.lblGeneralEvaluation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGeneralEvaluation.Location = New System.Drawing.Point(8, 158)
        Me.lblGeneralEvaluation.Name = "lblGeneralEvaluation"
        Me.lblGeneralEvaluation.Size = New System.Drawing.Size(115, 16)
        Me.lblGeneralEvaluation.TabIndex = 7
        Me.lblGeneralEvaluation.Text = "General Evaluation %"
        Me.lblGeneralEvaluation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGeneralWeight
        '
        Me.txtGeneralWeight.AllowNegative = False
        Me.txtGeneralWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtGeneralWeight.DigitsInGroup = 0
        Me.txtGeneralWeight.Flags = 65536
        Me.txtGeneralWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGeneralWeight.Location = New System.Drawing.Point(129, 156)
        Me.txtGeneralWeight.MaxDecimalPlaces = 2
        Me.txtGeneralWeight.MaxWholeDigits = 3
        Me.txtGeneralWeight.Name = "txtGeneralWeight"
        Me.txtGeneralWeight.Prefix = ""
        Me.txtGeneralWeight.RangeMax = 1.7976931348623157E+308
        Me.txtGeneralWeight.RangeMin = -1.7976931348623157E+308
        Me.txtGeneralWeight.Size = New System.Drawing.Size(55, 21)
        Me.txtGeneralWeight.TabIndex = 8
        Me.txtGeneralWeight.Text = "0.00"
        Me.txtGeneralWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBalanceScoreCard
        '
        Me.lblBalanceScoreCard.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalanceScoreCard.Location = New System.Drawing.Point(196, 158)
        Me.lblBalanceScoreCard.Name = "lblBalanceScoreCard"
        Me.lblBalanceScoreCard.Size = New System.Drawing.Size(115, 16)
        Me.lblBalanceScoreCard.TabIndex = 9
        Me.lblBalanceScoreCard.Text = "Balance Score Card %"
        Me.lblBalanceScoreCard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBSCWeight
        '
        Me.txtBSCWeight.AllowNegative = False
        Me.txtBSCWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtBSCWeight.DigitsInGroup = 0
        Me.txtBSCWeight.Flags = 65536
        Me.txtBSCWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBSCWeight.Location = New System.Drawing.Point(317, 156)
        Me.txtBSCWeight.MaxDecimalPlaces = 2
        Me.txtBSCWeight.MaxWholeDigits = 3
        Me.txtBSCWeight.Name = "txtBSCWeight"
        Me.txtBSCWeight.Prefix = ""
        Me.txtBSCWeight.RangeMax = 1.7976931348623157E+308
        Me.txtBSCWeight.RangeMin = -1.7976931348623157E+308
        Me.txtBSCWeight.Size = New System.Drawing.Size(55, 21)
        Me.txtBSCWeight.TabIndex = 10
        Me.txtBSCWeight.Text = "0.00"
        Me.txtBSCWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(355, 60)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 4
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(8, 90)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(115, 16)
        Me.lblDescription.TabIndex = 5
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(8, 63)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(115, 16)
        Me.lblName.TabIndex = 2
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(129, 87)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(220, 63)
        Me.txtDescription.TabIndex = 6
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(129, 60)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(220, 21)
        Me.txtName.TabIndex = 3
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(129, 33)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(110, 21)
        Me.txtCode.TabIndex = 1
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 36)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(115, 16)
        Me.lblCode.TabIndex = 0
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmJobGroup_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(382, 211)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmJobGroup_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Job Group"
        Me.Panel1.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbJobGroup.ResumeLayout(False)
        Me.gbJobGroup.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents gbJobGroup As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents lblGeneralEvaluation As System.Windows.Forms.Label
    Friend WithEvents txtGeneralWeight As eZee.TextBox.NumericTextBox
    Friend WithEvents lblBalanceScoreCard As System.Windows.Forms.Label
    Friend WithEvents txtBSCWeight As eZee.TextBox.NumericTextBox
End Class
