﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmJobGroup_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmJobGroup_AddEdit"
    Private mblnCancel As Boolean = True
    Private objJobGroup As clsJobGroup
    Private menAction As enAction = enAction.ADD_ONE
    Private mintJobGroupUnkid As Integer = -1
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintJobGroupUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintJobGroupUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtBSCWeight.BackColor = GUI.ColorOptional
            txtGeneralWeight.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 23 JAN 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objJobGroup._Code
        txtName.Text = objJobGroup._Name
        txtDescription.Text = objJobGroup._Description
        'S.SANDEEP [ 23 JAN 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
        txtGeneralWeight.Decimal = objJobGroup._Weight
        If objJobGroup._Weight > 0 Then
            txtBSCWeight.Decimal = 100 - objJobGroup._Weight
        End If
        'S.SANDEEP [ 23 JAN 2012 ] -- END
    End Sub

    Private Sub SetValue()
        objJobGroup._Code = txtCode.Text
        objJobGroup._Name = txtName.Text
        objJobGroup._Description = txtDescription.Text
        'S.SANDEEP [ 23 JAN 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
        objJobGroup._Weight = txtGeneralWeight.Decimal
        'S.SANDEEP [ 23 JAN 2012 ] -- END
    End Sub

    'S.SANDEEP [ 23 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
    Public Function Validation() As Boolean
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Return False
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Job Group cannot be blank. Job Group is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Return False
            End If

            If txtGeneralWeight.Decimal > 0 AndAlso txtGeneralWeight.Decimal <> 100 Then
                If (txtGeneralWeight.Decimal + txtBSCWeight.Decimal) > 100 Or _
                   (txtGeneralWeight.Decimal + txtBSCWeight.Decimal) < 100 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, performance evaluation percentage should be between 0 and 100."), enMsgBoxStyle.Information)
                    txtGeneralWeight.Focus()
                    Return False
                End If
            ElseIf txtBSCWeight.Decimal > 0 AndAlso txtBSCWeight.Decimal <> 100 Then
                If (txtGeneralWeight.Decimal + txtBSCWeight.Decimal) > 100 Or _
                   (txtGeneralWeight.Decimal + txtBSCWeight.Decimal) < 100 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, performance evaluation percentage should be between 0 and 100."), enMsgBoxStyle.Information)
                    txtBSCWeight.Focus()
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 23 JAN 2012 ] -- END

#End Region

#Region " Form's Events "
    Private Sub frmJobGroup_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objJobGroup = Nothing
    End Sub

    Private Sub frmJobGroup_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmJobGroup_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmJobGroup_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objJobGroup = New clsJobGroup
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call setColor()

            If menAction = enAction.EDIT_ONE Then
                objJobGroup._Jobgroupunkid = mintJobGroupUnkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAirline_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsJobGroup.SetMessages()
            objfrm._Other_ModuleNames = "clsJobGroup"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
            If Validation() = False Then Exit Sub
            'S.SANDEEP [ 23 JAN 2012 ] -- END

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objJobGroup._FormName = mstrModuleName
            objJobGroup._LoginEmployeeunkid = 0
            objJobGroup._ClientIP = getIP()
            objJobGroup._HostName = getHostName()
            objJobGroup._FromWeb = False
            objJobGroup._AuditUserId = User._Object._Userunkid
objJobGroup._CompanyUnkid = Company._Object._Companyunkid
            objJobGroup._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objJobGroup.Update()
            Else
                blnFlag = objJobGroup.Insert()
            End If

            If blnFlag = False And objJobGroup._Message <> "" Then
                eZeeMsgBox.Show(objJobGroup._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objJobGroup = Nothing
                    objJobGroup = New clsJobGroup
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintJobGroupUnkid = objJobGroup._Jobgroupunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call objFrm.displayDialog(txtName.Text, objJobGroup._Name1, objJobGroup._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbJobGroup.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbJobGroup.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbJobGroup.Text = Language._Object.getCaption(Me.gbJobGroup.Name, Me.gbJobGroup.Text)
            Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblGeneralEvaluation.Text = Language._Object.getCaption(Me.lblGeneralEvaluation.Name, Me.lblGeneralEvaluation.Text)
            Me.lblBalanceScoreCard.Text = Language._Object.getCaption(Me.lblBalanceScoreCard.Name, Me.lblBalanceScoreCard.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Job Group cannot be blank. Job Group is required information.")
            Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")
            Language.setMessage(mstrModuleName, 3, "Sorry, performance evaluation percentage should be between 0 and 100.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class