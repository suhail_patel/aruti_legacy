﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmDepartment_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmDepartment_AddEdit"
    Private mblnCancel As Boolean = True
    Private objDepartment As clsDepartment
    Private menAction As enAction = enAction.ADD_ONE
    Private mintDepartmentUnkid As Integer = -1
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintDepartmentUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintDepartmentUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboDeptGroup.BackColor = GUI.ColorOptional
            cboBranch.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objDepartment._Code
        txtName.Text = objDepartment._Name
        txtDescription.Text = objDepartment._Description
        cboDeptGroup.SelectedValue = objDepartment._Deptgroupunkid
        cboBranch.SelectedValue = objDepartment._Stationunkid
    End Sub

    Private Sub SetValue()
        objDepartment._Code = txtCode.Text
        objDepartment._Name = txtName.Text
        objDepartment._Description = txtDescription.Text
        objDepartment._Deptgroupunkid = CInt(cboDeptGroup.SelectedValue)
        objDepartment._Stationunkid = CInt(cboBranch.SelectedValue)
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objDeptGroup As New clsDepartmentGroup
        Dim objStation As New clsStation
        Try
            dsCombos = objDeptGroup.getComboList("DeptGroup", True)
            With cboDeptGroup
                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("DeptGroup")
            End With
            cboDeptGroup.SelectedValue = 0

            dsCombos = objStation.getComboList("StationList", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("StationList")
            End With
            cboBranch.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddStation.Enabled = User._Object.Privilege._AddStation
            objbtnDeptGroup.Enabled = User._Object.Privilege._AddDepartmentGroup
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmDepartment_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objDepartment = Nothing
    End Sub

    Private Sub frmDepartment_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDepartment_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDepartment_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDepartment_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDepartment_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objDepartment = New clsDepartment
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call setColor()

            Call SetVisibility()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objDepartment._Departmentunkid = mintDepartmentUnkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDepartment_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDepartment.SetMessages()
            objfrm._Other_ModuleNames = "clsDepartment"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            'If CInt(cboBranch.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Branch cannot be blank. Branch is required information."), enMsgBoxStyle.Information) '?1
            '    cboBranch.Focus()
            '    Exit Sub
            'End If

            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Department cannot be blank. Department is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objDepartment._FormName = mstrModuleName
            objDepartment._LoginEmployeeunkid = 0
            objDepartment._ClientIP = getIP()
            objDepartment._HostName = getHostName()
            objDepartment._FromWeb = False
            objDepartment._AuditUserId = User._Object._Userunkid
objDepartment._CompanyUnkid = Company._Object._Companyunkid
            objDepartment._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objDepartment.Update()
            Else
                blnFlag = objDepartment.Insert()
            End If

            If blnFlag = False And objDepartment._Message <> "" Then
                eZeeMsgBox.Show(objDepartment._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objDepartment = Nothing
                    objDepartment = New clsDepartment
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintDepartmentUnkid = objDepartment._Departmentunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call objFrm.displayDialog(txtName.Text, objDepartment._Name1, objDepartment._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddStation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddStation.Click
        Try
            Dim objfrmStation_AddEdit As New frmStation_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCombos As New DataSet
            Dim objStation As New clsStation

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmStation_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmStation_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmStation_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If objfrmStation_AddEdit.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsCombos = objStation.getComboList("List", True)
                With cboBranch
                    .ValueMember = "stationunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
                cboBranch.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddStation_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnDeptGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnDeptGroup.Click
        Try
            Dim objfrmDepartmentGroup_AddEdit As New frmDepartmentGroup_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCombos As New DataSet
            Dim objDepartmentGroup As New clsDepartmentGroup

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmDepartmentGroup_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmDepartmentGroup_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmDepartmentGroup_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If objfrmDepartmentGroup_AddEdit.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsCombos = objDepartmentGroup.getComboList("List", True)
                With cboDeptGroup
                    .ValueMember = "deptgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
                cboDeptGroup.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDeptGroup_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

            Call SetLanguage()
			
			Me.gbDepartmentInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbDepartmentInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbDepartmentInfo.Text = Language._Object.getCaption(Me.gbDepartmentInfo.Name, Me.gbDepartmentInfo.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.Name, Me.lblDepartmentGroup.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Department cannot be blank. Department is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class