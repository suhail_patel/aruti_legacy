﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAllocation_Mapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAllocation_Mapping))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbAllocationMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnl2 = New System.Windows.Forms.Panel
        Me.objchkAllocation2 = New System.Windows.Forms.CheckBox
        Me.dgvAllocation2 = New System.Windows.Forms.DataGridView
        Me.objdgcolhChkAllocation2 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhAllocation2Name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAllocation2Unkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnl1 = New System.Windows.Forms.Panel
        Me.objchkAllocation1 = New System.Windows.Forms.CheckBox
        Me.dgvAllocation1 = New System.Windows.Forms.DataGridView
        Me.objdgcolhChkAllocation1 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhAllocation1Name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAllocation1Unkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lvData = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhPAllocation = New System.Windows.Forms.ColumnHeader
        Me.objcolhPAllocUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhCAllocUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhPRefUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhCRefUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhGUID = New System.Windows.Forms.ColumnHeader
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtSAllocation2 = New eZee.TextBox.AlphanumericTextBox
        Me.txtSAllocation1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblAllocation1 = New System.Windows.Forms.Label
        Me.cboAllocation2 = New System.Windows.Forms.ComboBox
        Me.cboAllocation1 = New System.Windows.Forms.ComboBox
        Me.lblAllocation2 = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhAllocationDisplay = New System.Windows.Forms.ColumnHeader
        Me.objFooter.SuspendLayout()
        Me.gbAllocationMapping.SuspendLayout()
        Me.pnl2.SuspendLayout()
        CType(Me.dgvAllocation2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl1.SuspendLayout()
        CType(Me.dgvAllocation1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 479)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(902, 55)
        Me.objFooter.TabIndex = 11
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(690, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 128
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(793, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbAllocationMapping
        '
        Me.gbAllocationMapping.BorderColor = System.Drawing.Color.Black
        Me.gbAllocationMapping.Checked = False
        Me.gbAllocationMapping.CollapseAllExceptThis = False
        Me.gbAllocationMapping.CollapsedHoverImage = Nothing
        Me.gbAllocationMapping.CollapsedNormalImage = Nothing
        Me.gbAllocationMapping.CollapsedPressedImage = Nothing
        Me.gbAllocationMapping.CollapseOnLoad = False
        Me.gbAllocationMapping.Controls.Add(Me.pnl2)
        Me.gbAllocationMapping.Controls.Add(Me.pnl1)
        Me.gbAllocationMapping.Controls.Add(Me.lvData)
        Me.gbAllocationMapping.Controls.Add(Me.btnAdd)
        Me.gbAllocationMapping.Controls.Add(Me.btnDelete)
        Me.gbAllocationMapping.Controls.Add(Me.txtSAllocation2)
        Me.gbAllocationMapping.Controls.Add(Me.txtSAllocation1)
        Me.gbAllocationMapping.Controls.Add(Me.lblAllocation1)
        Me.gbAllocationMapping.Controls.Add(Me.cboAllocation2)
        Me.gbAllocationMapping.Controls.Add(Me.cboAllocation1)
        Me.gbAllocationMapping.Controls.Add(Me.lblAllocation2)
        Me.gbAllocationMapping.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAllocationMapping.ExpandedHoverImage = Nothing
        Me.gbAllocationMapping.ExpandedNormalImage = Nothing
        Me.gbAllocationMapping.ExpandedPressedImage = Nothing
        Me.gbAllocationMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAllocationMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAllocationMapping.HeaderHeight = 25
        Me.gbAllocationMapping.HeaderMessage = ""
        Me.gbAllocationMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAllocationMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAllocationMapping.HeightOnCollapse = 0
        Me.gbAllocationMapping.LeftTextSpace = 0
        Me.gbAllocationMapping.Location = New System.Drawing.Point(0, 0)
        Me.gbAllocationMapping.Name = "gbAllocationMapping"
        Me.gbAllocationMapping.OpenHeight = 300
        Me.gbAllocationMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAllocationMapping.ShowBorder = True
        Me.gbAllocationMapping.ShowCheckBox = False
        Me.gbAllocationMapping.ShowCollapseButton = False
        Me.gbAllocationMapping.ShowDefaultBorderColor = True
        Me.gbAllocationMapping.ShowDownButton = False
        Me.gbAllocationMapping.ShowHeader = True
        Me.gbAllocationMapping.Size = New System.Drawing.Size(902, 479)
        Me.gbAllocationMapping.TabIndex = 12
        Me.gbAllocationMapping.Temp = 0
        Me.gbAllocationMapping.Text = "Allocation Mapping"
        Me.gbAllocationMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnl2
        '
        Me.pnl2.Controls.Add(Me.objchkAllocation2)
        Me.pnl2.Controls.Add(Me.dgvAllocation2)
        Me.pnl2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl2.Location = New System.Drawing.Point(259, 102)
        Me.pnl2.Name = "pnl2"
        Me.pnl2.Size = New System.Drawing.Size(241, 335)
        Me.pnl2.TabIndex = 132
        '
        'objchkAllocation2
        '
        Me.objchkAllocation2.AutoSize = True
        Me.objchkAllocation2.Location = New System.Drawing.Point(7, 5)
        Me.objchkAllocation2.Name = "objchkAllocation2"
        Me.objchkAllocation2.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllocation2.TabIndex = 133
        Me.objchkAllocation2.UseVisualStyleBackColor = True
        '
        'dgvAllocation2
        '
        Me.dgvAllocation2.AllowUserToAddRows = False
        Me.dgvAllocation2.AllowUserToDeleteRows = False
        Me.dgvAllocation2.AllowUserToResizeColumns = False
        Me.dgvAllocation2.AllowUserToResizeRows = False
        Me.dgvAllocation2.BackgroundColor = System.Drawing.Color.White
        Me.dgvAllocation2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAllocation2.ColumnHeadersHeight = 22
        Me.dgvAllocation2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAllocation2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhChkAllocation2, Me.dgcolhAllocation2Name, Me.objdgcolhAllocation2Unkid})
        Me.dgvAllocation2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAllocation2.Location = New System.Drawing.Point(0, 0)
        Me.dgvAllocation2.MultiSelect = False
        Me.dgvAllocation2.Name = "dgvAllocation2"
        Me.dgvAllocation2.RowHeadersVisible = False
        Me.dgvAllocation2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAllocation2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAllocation2.Size = New System.Drawing.Size(241, 335)
        Me.dgvAllocation2.TabIndex = 16
        '
        'objdgcolhChkAllocation2
        '
        Me.objdgcolhChkAllocation2.HeaderText = ""
        Me.objdgcolhChkAllocation2.Name = "objdgcolhChkAllocation2"
        Me.objdgcolhChkAllocation2.Width = 25
        '
        'dgcolhAllocation2Name
        '
        Me.dgcolhAllocation2Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhAllocation2Name.HeaderText = ""
        Me.dgcolhAllocation2Name.Name = "dgcolhAllocation2Name"
        Me.dgcolhAllocation2Name.ReadOnly = True
        Me.dgcolhAllocation2Name.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhAllocation2Name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhAllocation2Unkid
        '
        Me.objdgcolhAllocation2Unkid.HeaderText = "objdgcolhAllocation2Unkid"
        Me.objdgcolhAllocation2Unkid.Name = "objdgcolhAllocation2Unkid"
        Me.objdgcolhAllocation2Unkid.Visible = False
        '
        'pnl1
        '
        Me.pnl1.Controls.Add(Me.objchkAllocation1)
        Me.pnl1.Controls.Add(Me.dgvAllocation1)
        Me.pnl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl1.Location = New System.Drawing.Point(12, 102)
        Me.pnl1.Name = "pnl1"
        Me.pnl1.Size = New System.Drawing.Size(241, 335)
        Me.pnl1.TabIndex = 132
        '
        'objchkAllocation1
        '
        Me.objchkAllocation1.AutoSize = True
        Me.objchkAllocation1.Location = New System.Drawing.Point(7, 5)
        Me.objchkAllocation1.Name = "objchkAllocation1"
        Me.objchkAllocation1.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllocation1.TabIndex = 133
        Me.objchkAllocation1.UseVisualStyleBackColor = True
        '
        'dgvAllocation1
        '
        Me.dgvAllocation1.AllowUserToAddRows = False
        Me.dgvAllocation1.AllowUserToDeleteRows = False
        Me.dgvAllocation1.AllowUserToResizeColumns = False
        Me.dgvAllocation1.AllowUserToResizeRows = False
        Me.dgvAllocation1.BackgroundColor = System.Drawing.Color.White
        Me.dgvAllocation1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAllocation1.ColumnHeadersHeight = 22
        Me.dgvAllocation1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAllocation1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhChkAllocation1, Me.dgcolhAllocation1Name, Me.objdgcolhAllocation1Unkid})
        Me.dgvAllocation1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAllocation1.Location = New System.Drawing.Point(0, 0)
        Me.dgvAllocation1.MultiSelect = False
        Me.dgvAllocation1.Name = "dgvAllocation1"
        Me.dgvAllocation1.RowHeadersVisible = False
        Me.dgvAllocation1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAllocation1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAllocation1.Size = New System.Drawing.Size(241, 335)
        Me.dgvAllocation1.TabIndex = 15
        '
        'objdgcolhChkAllocation1
        '
        Me.objdgcolhChkAllocation1.HeaderText = ""
        Me.objdgcolhChkAllocation1.Name = "objdgcolhChkAllocation1"
        Me.objdgcolhChkAllocation1.Width = 25
        '
        'dgcolhAllocation1Name
        '
        Me.dgcolhAllocation1Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhAllocation1Name.HeaderText = ""
        Me.dgcolhAllocation1Name.Name = "dgcolhAllocation1Name"
        Me.dgcolhAllocation1Name.ReadOnly = True
        Me.dgcolhAllocation1Name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhAllocation1Unkid
        '
        Me.objdgcolhAllocation1Unkid.HeaderText = "objdgcolhAllocation1Unkid"
        Me.objdgcolhAllocation1Unkid.Name = "objdgcolhAllocation1Unkid"
        Me.objdgcolhAllocation1Unkid.Visible = False
        '
        'lvData
        '
        Me.lvData.BackColorOnChecked = True
        Me.lvData.ColumnHeaders = Nothing
        Me.lvData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhPAllocation, Me.colhAllocationDisplay, Me.objcolhPAllocUnkid, Me.objcolhCAllocUnkid, Me.objcolhPRefUnkid, Me.objcolhCRefUnkid, Me.objcolhGUID})
        Me.lvData.CompulsoryColumns = ""
        Me.lvData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvData.FullRowSelect = True
        Me.lvData.GridLines = True
        Me.lvData.GroupingColumn = Nothing
        Me.lvData.HideSelection = False
        Me.lvData.Location = New System.Drawing.Point(506, 48)
        Me.lvData.MinColumnWidth = 50
        Me.lvData.MultiSelect = False
        Me.lvData.Name = "lvData"
        Me.lvData.OptionalColumns = ""
        Me.lvData.ShowMoreItem = False
        Me.lvData.ShowSaveItem = False
        Me.lvData.ShowSelectAll = True
        Me.lvData.ShowSizeAllColumnsToFit = True
        Me.lvData.Size = New System.Drawing.Size(385, 425)
        Me.lvData.Sortable = True
        Me.lvData.TabIndex = 131
        Me.lvData.UseCompatibleStateImageBehavior = False
        Me.lvData.View = System.Windows.Forms.View.Details
        '
        'objcolhPAllocation
        '
        Me.objcolhPAllocation.Tag = "objcolhPAllocation"
        Me.objcolhPAllocation.Text = ""
        Me.objcolhPAllocation.Width = 0
        '
        'objcolhPAllocUnkid
        '
        Me.objcolhPAllocUnkid.Tag = "objcolhPAllocUnkid"
        Me.objcolhPAllocUnkid.Text = ""
        Me.objcolhPAllocUnkid.Width = 0
        '
        'objcolhCAllocUnkid
        '
        Me.objcolhCAllocUnkid.Tag = "objcolhCAllocUnkid"
        Me.objcolhCAllocUnkid.Text = ""
        Me.objcolhCAllocUnkid.Width = 0
        '
        'objcolhPRefUnkid
        '
        Me.objcolhPRefUnkid.Tag = "objcolhPRefUnkid"
        Me.objcolhPRefUnkid.Width = 0
        '
        'objcolhCRefUnkid
        '
        Me.objcolhCRefUnkid.Tag = "objcolhCRefUnkid"
        Me.objcolhCRefUnkid.Text = ""
        Me.objcolhCRefUnkid.Width = 0
        '
        'objcolhGUID
        '
        Me.objcolhGUID.Tag = "objcolhGUID"
        Me.objcolhGUID.Text = ""
        Me.objcolhGUID.Width = 0
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(299, 443)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 130
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(402, 443)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 129
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'txtSAllocation2
        '
        Me.txtSAllocation2.Flags = 0
        Me.txtSAllocation2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSAllocation2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSAllocation2.Location = New System.Drawing.Point(259, 75)
        Me.txtSAllocation2.Name = "txtSAllocation2"
        Me.txtSAllocation2.Size = New System.Drawing.Size(241, 21)
        Me.txtSAllocation2.TabIndex = 107
        '
        'txtSAllocation1
        '
        Me.txtSAllocation1.Flags = 0
        Me.txtSAllocation1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSAllocation1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSAllocation1.Location = New System.Drawing.Point(12, 75)
        Me.txtSAllocation1.Name = "txtSAllocation1"
        Me.txtSAllocation1.Size = New System.Drawing.Size(241, 21)
        Me.txtSAllocation1.TabIndex = 107
        '
        'lblAllocation1
        '
        Me.lblAllocation1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation1.Location = New System.Drawing.Point(12, 30)
        Me.lblAllocation1.Name = "lblAllocation1"
        Me.lblAllocation1.Size = New System.Drawing.Size(241, 15)
        Me.lblAllocation1.TabIndex = 13
        Me.lblAllocation1.Text = "Allocation1"
        Me.lblAllocation1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocation2
        '
        Me.cboAllocation2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation2.FormattingEnabled = True
        Me.cboAllocation2.Location = New System.Drawing.Point(259, 48)
        Me.cboAllocation2.Name = "cboAllocation2"
        Me.cboAllocation2.Size = New System.Drawing.Size(241, 21)
        Me.cboAllocation2.TabIndex = 14
        '
        'cboAllocation1
        '
        Me.cboAllocation1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation1.FormattingEnabled = True
        Me.cboAllocation1.Location = New System.Drawing.Point(12, 48)
        Me.cboAllocation1.Name = "cboAllocation1"
        Me.cboAllocation1.Size = New System.Drawing.Size(241, 21)
        Me.cboAllocation1.TabIndex = 14
        '
        'lblAllocation2
        '
        Me.lblAllocation2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation2.Location = New System.Drawing.Point(263, 30)
        Me.lblAllocation2.Name = "lblAllocation2"
        Me.lblAllocation2.Size = New System.Drawing.Size(237, 15)
        Me.lblAllocation2.TabIndex = 13
        Me.lblAllocation2.Text = "Allocation2"
        Me.lblAllocation2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Department Group"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Branch"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'colhAllocationDisplay
        '
        Me.colhAllocationDisplay.Tag = "colhAllocationDisplay"
        Me.colhAllocationDisplay.Text = "Allocation"
        Me.colhAllocationDisplay.Width = 380
        '
        'frmAllocation_Mapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(902, 534)
        Me.Controls.Add(Me.gbAllocationMapping)
        Me.Controls.Add(Me.objFooter)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAllocation_Mapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Allocation Mapping"
        Me.objFooter.ResumeLayout(False)
        Me.gbAllocationMapping.ResumeLayout(False)
        Me.gbAllocationMapping.PerformLayout()
        Me.pnl2.ResumeLayout(False)
        Me.pnl2.PerformLayout()
        CType(Me.dgvAllocation2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl1.ResumeLayout(False)
        Me.pnl1.PerformLayout()
        CType(Me.dgvAllocation1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbAllocationMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblAllocation2 As System.Windows.Forms.Label
    Friend WithEvents lblAllocation1 As System.Windows.Forms.Label
    Friend WithEvents cboAllocation2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboAllocation1 As System.Windows.Forms.ComboBox
    Friend WithEvents dgvAllocation2 As System.Windows.Forms.DataGridView
    Friend WithEvents dgvAllocation1 As System.Windows.Forms.DataGridView
    Friend WithEvents txtSAllocation2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSAllocation1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents lvData As eZee.Common.eZeeListView
    Friend WithEvents pnl2 As System.Windows.Forms.Panel
    Friend WithEvents pnl1 As System.Windows.Forms.Panel
    Friend WithEvents objcolhPAllocation As System.Windows.Forms.ColumnHeader
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objchkAllocation2 As System.Windows.Forms.CheckBox
    Friend WithEvents objchkAllocation1 As System.Windows.Forms.CheckBox
    Friend WithEvents objdgcolhChkAllocation2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhAllocation2Name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAllocation2Unkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhChkAllocation1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhAllocation1Name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAllocation1Unkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPAllocUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhCAllocUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPRefUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhCRefUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhAllocationDisplay As System.Windows.Forms.ColumnHeader
End Class
