﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAlloc_Mapping_List
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAlloc_Mapping_List))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchData2 = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchData1 = New eZee.Common.eZeeGradientButton
        Me.cboAllocationData2 = New System.Windows.Forms.ComboBox
        Me.objlblAlloc2 = New System.Windows.Forms.Label
        Me.cboAllocationData1 = New System.Windows.Forms.ComboBox
        Me.objlblAlloc1 = New System.Windows.Forms.Label
        Me.cboAllocation2 = New System.Windows.Forms.ComboBox
        Me.lblAllocation2 = New System.Windows.Forms.Label
        Me.cboAllocation1 = New System.Windows.Forms.ComboBox
        Me.lblAllocation1 = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objChkAll = New System.Windows.Forms.CheckBox
        Me.lvData = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhAllocationDisplay = New System.Windows.Forms.ColumnHeader
        Me.objcolhPAllocUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhCAllocUnkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhMappingTranId = New System.Windows.Forms.ColumnHeader
        Me.objcolhMappingId = New System.Windows.Forms.ColumnHeader
        Me.objcolhPAllocation = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.Panel1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.gbFilterCriteria)
        Me.Panel1.Controls.Add(Me.objChkAll)
        Me.Panel1.Controls.Add(Me.lvData)
        Me.Panel1.Controls.Add(Me.objFooter)
        Me.Panel1.Controls.Add(Me.eZeeHeader)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(693, 451)
        Me.Panel1.TabIndex = 1
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchData2)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchData1)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocationData2)
        Me.gbFilterCriteria.Controls.Add(Me.objlblAlloc2)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocationData1)
        Me.gbFilterCriteria.Controls.Add(Me.objlblAlloc1)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocation2)
        Me.gbFilterCriteria.Controls.Add(Me.lblAllocation2)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocation1)
        Me.gbFilterCriteria.Controls.Add(Me.lblAllocation1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(668, 88)
        Me.gbFilterCriteria.TabIndex = 134
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchData2
        '
        Me.objbtnSearchData2.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchData2.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchData2.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchData2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchData2.BorderSelected = False
        Me.objbtnSearchData2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchData2.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchData2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchData2.Location = New System.Drawing.Point(631, 60)
        Me.objbtnSearchData2.Name = "objbtnSearchData2"
        Me.objbtnSearchData2.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchData2.TabIndex = 150
        '
        'objbtnSearchData1
        '
        Me.objbtnSearchData1.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchData1.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchData1.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchData1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchData1.BorderSelected = False
        Me.objbtnSearchData1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchData1.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchData1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchData1.Location = New System.Drawing.Point(631, 33)
        Me.objbtnSearchData1.Name = "objbtnSearchData1"
        Me.objbtnSearchData1.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchData1.TabIndex = 101
        '
        'cboAllocationData2
        '
        Me.cboAllocationData2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocationData2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocationData2.FormattingEnabled = True
        Me.cboAllocationData2.Location = New System.Drawing.Point(414, 60)
        Me.cboAllocationData2.Name = "cboAllocationData2"
        Me.cboAllocationData2.Size = New System.Drawing.Size(211, 21)
        Me.cboAllocationData2.TabIndex = 148
        '
        'objlblAlloc2
        '
        Me.objlblAlloc2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblAlloc2.Location = New System.Drawing.Point(311, 63)
        Me.objlblAlloc2.Name = "objlblAlloc2"
        Me.objlblAlloc2.Size = New System.Drawing.Size(97, 15)
        Me.objlblAlloc2.TabIndex = 147
        Me.objlblAlloc2.Text = "#Value"
        Me.objlblAlloc2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocationData1
        '
        Me.cboAllocationData1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocationData1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocationData1.FormattingEnabled = True
        Me.cboAllocationData1.Location = New System.Drawing.Point(414, 33)
        Me.cboAllocationData1.Name = "cboAllocationData1"
        Me.cboAllocationData1.Size = New System.Drawing.Size(211, 21)
        Me.cboAllocationData1.TabIndex = 146
        '
        'objlblAlloc1
        '
        Me.objlblAlloc1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblAlloc1.Location = New System.Drawing.Point(311, 36)
        Me.objlblAlloc1.Name = "objlblAlloc1"
        Me.objlblAlloc1.Size = New System.Drawing.Size(97, 15)
        Me.objlblAlloc1.TabIndex = 145
        Me.objlblAlloc1.Text = "#Value"
        Me.objlblAlloc1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocation2
        '
        Me.cboAllocation2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation2.FormattingEnabled = True
        Me.cboAllocation2.Location = New System.Drawing.Point(89, 60)
        Me.cboAllocation2.Name = "cboAllocation2"
        Me.cboAllocation2.Size = New System.Drawing.Size(205, 21)
        Me.cboAllocation2.TabIndex = 144
        '
        'lblAllocation2
        '
        Me.lblAllocation2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation2.Location = New System.Drawing.Point(8, 63)
        Me.lblAllocation2.Name = "lblAllocation2"
        Me.lblAllocation2.Size = New System.Drawing.Size(75, 15)
        Me.lblAllocation2.TabIndex = 143
        Me.lblAllocation2.Text = "Allocation2"
        Me.lblAllocation2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocation1
        '
        Me.cboAllocation1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation1.FormattingEnabled = True
        Me.cboAllocation1.Location = New System.Drawing.Point(89, 33)
        Me.cboAllocation1.Name = "cboAllocation1"
        Me.cboAllocation1.Size = New System.Drawing.Size(205, 21)
        Me.cboAllocation1.TabIndex = 142
        '
        'lblAllocation1
        '
        Me.lblAllocation1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation1.Location = New System.Drawing.Point(8, 36)
        Me.lblAllocation1.Name = "lblAllocation1"
        Me.lblAllocation1.Size = New System.Drawing.Size(75, 15)
        Me.lblAllocation1.TabIndex = 14
        Me.lblAllocation1.Text = "Allocation1"
        Me.lblAllocation1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(643, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 141
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(618, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 140
        Me.objbtnSearch.TabStop = False
        '
        'objChkAll
        '
        Me.objChkAll.AutoSize = True
        Me.objChkAll.Location = New System.Drawing.Point(18, 163)
        Me.objChkAll.Name = "objChkAll"
        Me.objChkAll.Size = New System.Drawing.Size(15, 14)
        Me.objChkAll.TabIndex = 133
        Me.objChkAll.UseVisualStyleBackColor = True
        '
        'lvData
        '
        Me.lvData.BackColorOnChecked = False
        Me.lvData.CheckBoxes = True
        Me.lvData.ColumnHeaders = Nothing
        Me.lvData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhAllocationDisplay, Me.objcolhPAllocUnkid, Me.objcolhCAllocUnkid, Me.objcolhMappingTranId, Me.objcolhMappingId, Me.objcolhPAllocation})
        Me.lvData.CompulsoryColumns = ""
        Me.lvData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvData.FullRowSelect = True
        Me.lvData.GridLines = True
        Me.lvData.GroupingColumn = Nothing
        Me.lvData.HideSelection = False
        Me.lvData.Location = New System.Drawing.Point(12, 158)
        Me.lvData.MinColumnWidth = 50
        Me.lvData.MultiSelect = False
        Me.lvData.Name = "lvData"
        Me.lvData.OptionalColumns = ""
        Me.lvData.ShowMoreItem = False
        Me.lvData.ShowSaveItem = False
        Me.lvData.ShowSelectAll = True
        Me.lvData.ShowSizeAllColumnsToFit = True
        Me.lvData.Size = New System.Drawing.Size(668, 232)
        Me.lvData.Sortable = True
        Me.lvData.TabIndex = 132
        Me.lvData.UseCompatibleStateImageBehavior = False
        Me.lvData.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 25
        '
        'colhAllocationDisplay
        '
        Me.colhAllocationDisplay.Tag = "colhAllocationDisplay"
        Me.colhAllocationDisplay.Text = "Allocation"
        Me.colhAllocationDisplay.Width = 635
        '
        'objcolhPAllocUnkid
        '
        Me.objcolhPAllocUnkid.Tag = "objcolhPAllocUnkid"
        Me.objcolhPAllocUnkid.Text = ""
        Me.objcolhPAllocUnkid.Width = 0
        '
        'objcolhCAllocUnkid
        '
        Me.objcolhCAllocUnkid.Tag = "objcolhCAllocUnkid"
        Me.objcolhCAllocUnkid.Text = ""
        Me.objcolhCAllocUnkid.Width = 0
        '
        'objcolhMappingTranId
        '
        Me.objcolhMappingTranId.Tag = "objcolhMappingTranId"
        Me.objcolhMappingTranId.Text = ""
        Me.objcolhMappingTranId.Width = 0
        '
        'objcolhMappingId
        '
        Me.objcolhMappingId.Tag = "objcolhMappingId"
        Me.objcolhMappingId.Text = ""
        Me.objcolhMappingId.Width = 0
        '
        'objcolhPAllocation
        '
        Me.objcolhPAllocation.Tag = "objcolhPAllocation"
        Me.objcolhPAllocation.Text = ""
        Me.objcolhPAllocation.Width = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 396)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(693, 55)
        Me.objFooter.TabIndex = 6
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(481, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 122
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(378, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 120
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(584, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 119
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(693, 58)
        Me.eZeeHeader.TabIndex = 5
        Me.eZeeHeader.Title = "Mapped List"
        '
        'frmAlloc_Mapping_List
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(693, 451)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAlloc_Mapping_List"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Mapped Allocation List"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents lvData As eZee.Common.eZeeListView
    Friend WithEvents colhAllocationDisplay As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPAllocUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhCAllocUnkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMappingTranId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhMappingId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPAllocation As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents objChkAll As System.Windows.Forms.CheckBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblAllocation1 As System.Windows.Forms.Label
    Friend WithEvents cboAllocation1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllocation2 As System.Windows.Forms.Label
    Friend WithEvents cboAllocationData2 As System.Windows.Forms.ComboBox
    Friend WithEvents objlblAlloc2 As System.Windows.Forms.Label
    Friend WithEvents cboAllocationData1 As System.Windows.Forms.ComboBox
    Friend WithEvents objlblAlloc1 As System.Windows.Forms.Label
    Friend WithEvents cboAllocation2 As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchData2 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchData1 As eZee.Common.eZeeGradientButton
End Class
