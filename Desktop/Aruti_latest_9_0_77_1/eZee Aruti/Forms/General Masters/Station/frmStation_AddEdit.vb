﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmStation_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmStation_AddEdit"
    Private mblnCancel As Boolean = True
    Private objStation As clsStation
    Private menAction As enAction = enAction.ADD_ONE
    Private mintStationUnkid As Integer = -1
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintStationUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintStationUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp

            'Anjan (09 Jun 2011)-Start
            'Issue : Enhancement of FINCA requested by Rutta.
            txtDescription.BackColor = GUI.ColorOptional
            txtAddressLine1.BackColor = GUI.ColorOptional
            txtAddressLine2.BackColor = GUI.ColorOptional
            txtCompanyEmail.BackColor = GUI.ColorOptional
            txtFax.BackColor = GUI.ColorOptional
            txtCompanyEmail.BackColor = GUI.ColorOptional
            txtPhone1.BackColor = GUI.ColorOptional
            txtPhone2.BackColor = GUI.ColorOptional
            txtPhone3.BackColor = GUI.ColorOptional
            cboCity.BackColor = GUI.ColorOptional
            cboCountry.BackColor = GUI.ColorOptional
            cboState.BackColor = GUI.ColorOptional
            cboZipcode.BackColor = GUI.ColorOptional
            txtWebsite.BackColor = GUI.ColorOptional
            'Anjan (09 Jun 2011)-End 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objStation._Code
        txtName.Text = objStation._Name
        txtDescription.Text = objStation._Description

        'Anjan (09 Jun  2011)-Start
        'Issue : Enhancement of FINCA requested by Rutta.
        txtPhone1.Text = objStation._Phone1
        txtPhone2.Text = objStation._Phone2
        txtPhone3.Text = objStation._Phone3
        txtAddressLine1.Text = objStation._Address1
        txtAddressLine2.Text = objStation._Address2
        cboCountry.SelectedValue = objStation._Countryunkid
        cboState.SelectedValue = objStation._Stateunkid
        cboCity.SelectedValue = objStation._Cityunkid
        cboZipcode.SelectedValue = objStation._Postalunkid
        txtCompanyEmail.Text = objStation._Email
        txtFax.Text = objStation._Fax
        txtWebsite.Text = objStation._Website

        'Anjan (09 Jun  2011)-End 


    End Sub

    Private Sub SetValue()
        objStation._Code = txtCode.Text
        objStation._Name = txtName.Text
        objStation._Description = txtDescription.Text

        'Anjan (09 Jun 2011)-Start
        'Issue : Enhancement of FINCA requested by Rutta.
        objStation._Phone1 = txtPhone1.Text
        objStation._Phone2 = txtPhone2.Text
        objStation._Phone3 = txtPhone3.Text
        objStation._Postalunkid = CInt(cboZipcode.SelectedValue)
        objStation._Stateunkid = CInt(cboState.SelectedValue)
        objStation._Address1 = txtAddressLine1.Text
        objStation._Address2 = txtAddressLine2.Text
        objStation._Cityunkid = CInt(cboCity.SelectedValue)
        objStation._Countryunkid = CInt(cboCountry.SelectedValue)
        objStation._Email = txtCompanyEmail.Text
        objStation._Fax = txtFax.Text
        objStation._Website = txtWebsite.Text
        'Anjan (09 Jun 2011)-End 


    End Sub
    Private Sub FillCombo()
        Dim ObjMasterData As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjMasterData.getCountryList("PresentCountry", True)
            With cboCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("PresentCountry")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmStation_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objStation = Nothing
    End Sub

    Private Sub frmStation_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStation_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStation_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStation_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStation_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objStation = New clsStation
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call setColor()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objStation._Stationunkid = mintStationUnkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStation_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsStation.SetMessages()
            objfrm._Other_ModuleNames = "clsStation"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Branch cannot be blank. Branch is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objStation._FormName = mstrModuleName
            objStation._LoginEmployeeunkid = 0
            objStation._ClientIP = getIP()
            objStation._HostName = getHostName()
            objStation._FromWeb = False
            objStation._AuditUserId = User._Object._Userunkid
objStation._CompanyUnkid = Company._Object._Companyunkid
            objStation._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objStation.Update()
            Else
                blnFlag = objStation.Insert()
            End If

            If blnFlag = False And objStation._Message <> "" Then
                eZeeMsgBox.Show(objStation._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objStation = Nothing
                    objStation = New clsStation
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintStationUnkid = objStation._Stationunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call objFrm.displayDialog(txtName.Text, objStation._Name1, objStation._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged

        Try
            If CInt(cboCountry.SelectedValue) > 0 Then
                Dim objState As New clsstate_master
                Dim dsCombos As New DataSet
                dsCombos = objState.GetList("State", , True, CInt(cboCountry.SelectedValue))
                With cboState
                    .ValueMember = "stateunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("State").Copy
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    Private Sub cboCity_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCity.SelectedIndexChanged
        Try
            If CInt(cboCity.SelectedValue) > 0 Then
                Dim objZipCode As New clszipcode_master
                Dim dsCombos As New DataSet
                dsCombos = objZipCode.GetList("Zipcode", , True, CInt(cboCity.SelectedValue))
                With cboZipcode
                    .ValueMember = "zipcodeunkid"
                    .DisplayMember = "zipcode_no"
                    .DataSource = dsCombos.Tables("Zipcode").Copy
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCity_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged
        Try
            If CInt(cboState.SelectedValue) > 0 Then
                Dim objCity As New clscity_master
                Dim dsCombos As New DataSet
                dsCombos = objCity.GetList("City", , True, CInt(cboState.SelectedValue))
                With cboCity
                    .ValueMember = "cityunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("City").Copy
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboState_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbStationInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbStationInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbStationInfo.Text = Language._Object.getCaption(Me.gbStationInfo.Name, Me.gbStationInfo.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
			Me.lblPostal.Text = Language._Object.getCaption(Me.lblPostal.Name, Me.lblPostal.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
			Me.lblAddress1.Text = Language._Object.getCaption(Me.lblAddress1.Name, Me.lblAddress1.Text)
			Me.elAddressInfo.Text = Language._Object.getCaption(Me.elAddressInfo.Name, Me.elAddressInfo.Text)
			Me.lblWebsite.Text = Language._Object.getCaption(Me.lblWebsite.Name, Me.lblWebsite.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.lblFax.Text = Language._Object.getCaption(Me.lblFax.Name, Me.lblFax.Text)
			Me.lblPhone3.Text = Language._Object.getCaption(Me.lblPhone3.Name, Me.lblPhone3.Text)
			Me.lblPhone2.Text = Language._Object.getCaption(Me.lblPhone2.Name, Me.lblPhone2.Text)
			Me.lblPhone1.Text = Language._Object.getCaption(Me.lblPhone1.Name, Me.lblPhone1.Text)
			Me.elContactInfo.Text = Language._Object.getCaption(Me.elContactInfo.Name, Me.elContactInfo.Text)
			Me.lblAddress2.Text = Language._Object.getCaption(Me.lblAddress2.Name, Me.lblAddress2.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Branch cannot be blank. Branch is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class