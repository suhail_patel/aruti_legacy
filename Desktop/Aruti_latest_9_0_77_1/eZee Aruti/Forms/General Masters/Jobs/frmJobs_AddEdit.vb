﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmJobs_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmJobs_AddEdit"
    Private mblnCancel As Boolean = True
    Private objJobs As clsJobs
    Private objJobSkill As clsJob_Skill_Tran
    Private objJobQualification As clsJob_Qualification_Tran
    'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
    Private objJobLanguage As clsJob_Language_Tran
    Private mdtLangTran As DataTable
    'Sohail (18 Feb 2020) -- End
    Private mdtQTran As DataTable
    Private menAction As enAction = enAction.ADD_ONE
    Private mintJobsUnkid As Integer = -1
    Private mdtTran As DataTable
    Private intSelectedIndex As Integer = -1


    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
    Dim objKeyduties As clsJobKeyduties_Tran
    Dim mdtKeyDuties As DataTable = Nothing

    Dim objJobCompetencies As clsJobCompetencies_Tran
    Dim mdtJobCompetencies As DataTable = Nothing
    'Pinkal (03-Dec-2015) -- End


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintJobsUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintJobsUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtJobDescription.BackColor = GUI.ColorOptional
            cboGrade.BackColor = GUI.ColorOptional
            cboJobGroup.BackColor = GUI.ColorOptional
            cboSection.BackColor = GUI.ColorOptional
            cboUnit.BackColor = GUI.ColorOptional
            nudPosition.BackColor = GUI.ColorComp
            cboTeam.BackColor = GUI.ColorOptional
            cboQualificationGrp.BackColor = GUI.ColorOptional
            cboQualification.BackColor = GUI.ColorOptional
            nudJobLevel.BackColor = GUI.ColorOptional
            cboSkillCategory.BackColor = GUI.ColorOptional
            cboSkillSets.BackColor = GUI.ColorOptional
            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
            cboLanguage.BackColor = GUI.ColorOptional
            'Sohail (18 Feb 2020) -- End

            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            cboReportTo.BackColor = GUI.ColorOptional
            cboInDirectReportTo.BackColor = GUI.ColorOptional
            cboDepartment.BackColor = GUI.ColorOptional
            cboClassGroup.BackColor = GUI.ColorOptional
            nudExperienceYear.BackColor = GUI.ColorOptional
            nudExperienceMonth.BackColor = GUI.ColorOptional
            txtExperienceRemark.BackColor = GUI.ColorOptional
            txtWorkingHrs.BackColor = GUI.ColorOptional
            cboCompetenciesCategory.BackColor = GUI.ColorOptional
            cboCompetencies.BackColor = GUI.ColorOptional
            'Pinkal (03-Dec-2015) -- End
            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Pick employment type for type of job on job master.
            cboJobType.BackColor = GUI.ColorOptional
            'Sohail (18 Feb 2020) -- End

            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            cboBranch.BackColor = GUI.ColorOptional
            cboDeparmentGrp.BackColor = GUI.ColorOptional
            cboSectionGrp.BackColor = GUI.ColorOptional
            cboUnitGrp.BackColor = GUI.ColorOptional
            cboClass.BackColor = GUI.ColorOptional
            cboGradeLevel.BackColor = GUI.ColorOptional
            'Shani(18-JUN-2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objJobs._Job_Code
            txtName.Text = objJobs._Job_Name
            If objJobs._Desciription.StartsWith("{\rtf") Then
                txtJobDescription.Rtf = objJobs._Desciription
            Else
                txtJobDescription.Text = objJobs._Desciription
            End If
            cboGrade.SelectedValue = objJobs._Jobgradeunkid
            cboJobGroup.SelectedValue = objJobs._Jobgroupunkid
            cboReportTo.SelectedValue = objJobs._Report_Tounkid
            cboSection.SelectedValue = objJobs._Jobsectionunkid
            cboUnit.SelectedValue = objJobs._Jobunitunkid
            cboTeam.SelectedValue = objJobs._Teamunkid
            nudPosition.Value = objJobs._Total_Position
            If objJobs._Create_Date <> Nothing Then
                dtpCreateDate.Value = objJobs._Create_Date
            Else
                dtpCreateDate.Checked = False
            End If
            If objJobs._Terminate_Date <> Nothing Then
                dtpTerminateDate.Value = objJobs._Terminate_Date
            Else
                dtpTerminateDate.Checked = False
            End If
            nudJobLevel.Value = objJobs._Job_Level


            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            cboInDirectReportTo.SelectedValue = CInt(objJobs._InDirectReport_Tounkid)
            cboDepartment.SelectedValue = CInt(objJobs._JobDepartmentunkid)
            cboClassGroup.SelectedValue = CInt(objJobs._JobClassGroupunkid)
            nudExperienceYear.Value = objJobs._Experience_Year
            nudExperienceMonth.Value = objJobs._Experience_Month
            txtExperienceRemark.Text = objJobs._Experience_Comments.Trim
            txtWorkingHrs.Text = objJobs._WorkingHrs
            'Pinkal (03-Dec-2015) -- End

            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            cboBranch.SelectedValue = CInt(objJobs._JobBranchUnkid)
            cboDeparmentGrp.SelectedValue = CInt(objJobs._DepartmentGrpUnkId)
            cboSectionGrp.SelectedValue = CInt(objJobs._SectionGrpUnkId)
            cboUnitGrp.SelectedValue = CInt(objJobs._UnitGrpUnkId)
            cboClass.SelectedValue = CInt(objJobs._ClassUnkid)
            cboGradeLevel.SelectedValue = CInt(objJobs._GradeLevelUnkId)
            'Shani(18-JUN-2016) -- End

            'Gajanan [31-AUG-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            nudCritical.Value = CInt(objJobs._Critical)
            'Gajanan [31-AUG-2019] -- End
            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Pick employment type for type of job on job master.
            cboJobType.SelectedValue = CInt(objJobs._Jobtypeunkid)
            'Sohail (18 Feb 2020) -- End

            'Gajanan [29-Oct-2020] -- Start   
            'Enhancement:Worked On Succession Module
            chkKeyRole.Checked = CBool(objJobs._Iskeyrole)
            'Gajanan [29-Oct-2020] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objJobs._Job_Code = txtCode.Text
            objJobs._Job_Name = txtName.Text
            objJobs._Desciription = txtJobDescription.Rtf
            objJobs._Jobgradeunkid = CInt(cboGrade.SelectedValue)
            objJobs._Jobgroupunkid = CInt(cboJobGroup.SelectedValue)
            objJobs._Report_Tounkid = CInt(cboReportTo.SelectedValue)
            objJobs._Jobsectionunkid = CInt(cboSection.SelectedValue)
            objJobs._Jobunitunkid = CInt(cboUnit.SelectedValue)
            objJobs._Teamunkid = CInt(cboTeam.SelectedValue)
            objJobs._Total_Position = CInt(nudPosition.Value)
            If dtpCreateDate.Checked = True Then
                objJobs._Create_Date = dtpCreateDate.Value
            Else
                objJobs._Create_Date = Nothing
            End If
            If dtpTerminateDate.Checked = True Then
                objJobs._Terminate_Date = dtpTerminateDate.Value
            Else
                objJobs._Terminate_Date = Nothing
            End If
            objJobs._Job_Level = CInt(nudJobLevel.Value)


            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            objJobs._Userunkid = User._Object._Userunkid
            objJobs._InDirectReport_Tounkid = CInt(cboInDirectReportTo.SelectedValue)
            objJobs._JobDepartmentunkid = CInt(cboDepartment.SelectedValue)
            objJobs._JobClassGroupunkid = CInt(cboClassGroup.SelectedValue)
            objJobs._Experience_Year = CInt(nudExperienceYear.Value)
            objJobs._Experience_Month = CInt(nudExperienceMonth.Value)
            objJobs._Experience_Comments = txtExperienceRemark.Text.Trim
            objJobs._WorkingHrs = txtWorkingHrs.Text.Trim
            'Pinkal (03-Dec-2015) -- End

            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            objJobs._JobBranchUnkid = CInt(cboBranch.SelectedValue)
            objJobs._DepartmentGrpUnkId = CInt(cboDeparmentGrp.SelectedValue)
            objJobs._SectionGrpUnkId = CInt(cboSectionGrp.SelectedValue)
            objJobs._UnitGrpUnkId = CInt(cboUnitGrp.SelectedValue)
            objJobs._ClassUnkid = CInt(cboClass.SelectedValue)
            objJobs._GradeLevelUnkId = CInt(cboGradeLevel.SelectedValue)
            'Shani(18-JUN-2016) -- End

            'Gajanan [31-AUG-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            objJobs._Critical = CInt(nudCritical.Value)
            'Gajanan [31-AUG-2019] -- End

            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Pick employment type for type of job on job master.
            objJobs._Jobtypeunkid = CInt(cboJobType.SelectedValue)
            'Sohail (18 Feb 2020) -- End
            objJobs._Iskeyrole = chkKeyRole.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objJobGrp As New clsJobGroup
        Dim objGrade As New clsGrade
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objTeam As New clsTeams
        Dim objCommom As New clsCommon_Master


        'Pinkal (03-Dec-2015) -- Start
        'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
        Dim objDepartment As New clsDepartment
        Dim objClassGroup As New clsClassGroup
        Dim objMstCompetence_Category As New clsCommon_Master
        Dim objCompetencieMst As New clsassess_competencies_master
        'Pinkal (03-Dec-2015) -- End

        Dim dsList As New DataSet
        Try
            dsList = objJobGrp.getComboList("JobGrp", True)
            With cboJobGroup
                .ValueMember = "jobgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("JobGrp")
                .SelectedValue = 0
            End With

            dsList = objGrade.getComboList("Grade", True)
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Grade")
                .SelectedValue = 0
            End With

            dsList = objSection.getComboList("Section", True)
            With cboSection
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Section")
                .SelectedValue = 0
            End With

            dsList = objUnit.getComboList("Units", True)
            With cboUnit
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Units")
                .SelectedValue = 0
            End With

            dsList = objCommom.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "Category")
            With cboSkillCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Category")
                .SelectedValue = 0
            End With

            dsList = objTeam.getComboList("List", True)
            With cboTeam
                .ValueMember = "teamunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objCommom.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "QGrp")
            With cboQualificationGrp
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("QGrp")
                .SelectedValue = 0
            End With

            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
            dsList = objCommom.getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, "Lang")
            With cboLanguage
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Lang")
                .SelectedValue = 0
            End With
            'Sohail (18 Feb 2020) -- End


            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

            dsList = objJobs.getComboList("ReportTo", True)
            With cboReportTo
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("ReportTo")
            End With
            cboReportTo.SelectedValue = 0

            With cboInDirectReportTo
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("ReportTo").Copy
            End With
            cboInDirectReportTo.SelectedValue = 0

            dsList = objDepartment.getComboList("List", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboDepartment.SelectedValue = 0

            dsList = objClassGroup.getComboList("List", True)
            With cboClassGroup
                .ValueMember = "classgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboClassGroup.SelectedValue = 0


            dsList = objMstCompetence_Category.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES, True, "Competence_Category")
            With cboCompetenciesCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Competence_Category")
                .SelectedValue = 0
            End With
            cboCompetenciesCategory.SelectedValue = 0
            'Pinkal (03-Dec-2015) -- End

            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Pick employment type for type of job on job master.
            dsList = objMstCompetence_Category.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "JobType")
            With cboJobType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("JobType")
                .SelectedValue = 0
            End With
            'Sohail (18 Feb 2020) -- End

            'Shani(18-JUN-2016) -- Start
            'Enhancement : Add Allocation Column in Job master[Rutta]
            dsList = (New clsStation).getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboBranch.SelectedValue = 0

            dsList = (New clsDepartmentGroup).getComboList("List", True)
            With cboDeparmentGrp
                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboDeparmentGrp.SelectedValue = 0

            dsList = (New clsSectionGroup).getComboList("List", True)
            With cboSectionGrp
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboSectionGrp.SelectedValue = 0

            dsList = (New clsUnitGroup).getComboList("List", True)
            With cboUnitGrp
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboUnitGrp.SelectedValue = 0

            dsList = (New clsClass).getComboList("List", True)
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboClass.SelectedValue = 0

            dsList = (New clsGradeLevel).getComboList("List", True)
            With cboGradeLevel
                .ValueMember = "gradelevelunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboGradeLevel.SelectedValue = 0
            'Shani(18-JUN-2016) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsList.Dispose()
            objJobGrp = Nothing : objGrade = Nothing
            objSection = Nothing : objUnit = Nothing
            objTeam = Nothing : objCommom = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddJobGroup.Enabled = User._Object.Privilege._AddJobGroup
            objbtnAddSection.Enabled = User._Object.Privilege._AddSection
            objbtnAddSkill.Enabled = User._Object.Privilege._AddSkills
            objbtnAddSkillCategory.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddUnit.Enabled = User._Object.Privilege._AddUnit
            objbtnAddGrade.Enabled = User._Object.Privilege._AddGrade
            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
            objbtnAddLanguage.Enabled = User._Object.Privilege._AddCommonMasters
            'Sohail (18 Feb 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Job Qualification Transaction "

    Private Sub btnDeleteJobQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteJobQualification.Click
        Try
            If lvQualification.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvQualification.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtQTran.Select("GUID = '" & lvQualification.SelectedItems(0).SubItems(objcolhQGUID.Index).Text & "'")
                Else
                    drTemp = mdtQTran.Select("jobqualificationtranunkid = '" & CInt(lvQualification.SelectedItems(0).Tag) & "'")
                End If
                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    'S.SANDEEP [23-Mar-2018] -- START
                    'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
                    drTemp(0).Item("isactive") = False
                    'S.SANDEEP [23-Mar-2018] -- END
                    Call FillQualificationList()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteJobQualification_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnAddJobQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddJobQualification.Click
        Try
            If CInt(cboQualificationGrp.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Qualification Group is compulsory information. Please Select Qualification Group to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CInt(cboQualification.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Qualification is compulsory information. Please Select Qualification to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtRow As DataRow() = mdtQTran.Select("qualificationunkid = " & CInt(cboQualification.SelectedValue) & " AND AUD <> 'D' ")

            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Selected Qualification is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtQRow As DataRow
            dtQRow = mdtQTran.NewRow

            dtQRow.Item("jobqualificationtranunkid") = -1
            dtQRow.Item("jobunkid") = mintJobsUnkid
            dtQRow.Item("qualificationgroupunkid") = cboQualificationGrp.SelectedValue
            dtQRow.Item("qualificationunkid") = cboQualification.SelectedValue
            dtQRow.Item("QualificationGrp") = cboQualificationGrp.Text
            dtQRow.Item("Qualification") = cboQualification.Text
            dtQRow.Item("AUD") = "A"
            dtQRow.Item("GUID") = Guid.NewGuid.ToString
            'S.SANDEEP [23-Mar-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
            dtQRow.Item("isactive") = True
            'S.SANDEEP [23-Mar-2018] -- END

            mdtQTran.Rows.Add(dtQRow)

            Call FillQualificationList()
            Call ResetQualification()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddJobQualification_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub FillQualificationList()
        Try
            lvQualification.Items.Clear()
            Dim lvItem As ListViewItem

            For Each dtRow As DataRow In mdtQTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("QualificationGrp").ToString
                    lvItem.SubItems.Add(dtRow.Item("Qualification").ToString)
                    lvItem.SubItems.Add(dtRow.Item("qualificationunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("qualificationgroupunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("jobunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)

                    lvItem.Tag = dtRow.Item("jobqualificationtranunkid").ToString

                    lvQualification.Items.Add(lvItem)

                End If
            Next

            If lvQualification.Items.Count > 5 Then
                colhQualificationGrp.Width = 256 - 20
            Else
                colhQualificationGrp.Width = 256
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillQualificationList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetQualification()
        Try
            cboQualificationGrp.SelectedValue = 0
            cboQualification.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetQualification", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Job Skills Transactions "

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If intSelectedIndex > -1 Then
                Dim drTemp As DataRow()
                If CInt(lvJobSkill.Items(intSelectedIndex).Tag) = -1 Then
                    drTemp = mdtTran.Select("GUID = '" & lvJobSkill.Items(intSelectedIndex).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtTran.Select("jobskilltranunkid = '" & CInt(lvJobSkill.Items(intSelectedIndex).Tag) & "'")
                End If
                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    'S.SANDEEP [23-Mar-2018] -- START
                    'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
                    drTemp(0).Item("isactive") = False
                    'S.SANDEEP [23-Mar-2018] -- END
                    Call FillSkillList()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If CInt(cboSkillCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Skill Category is compulsory information. Please Select Skill Category to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CInt(cboSkillSets.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Skill is compulsory information. Please Select Skill to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtRow As DataRow() = mdtTran.Select("skillunkid = " & CInt(cboSkillSets.SelectedValue) & " AND AUD <> 'D' ")

            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Selected Skill is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtSRow As DataRow
            dtSRow = mdtTran.NewRow

            With dtSRow
                .Item("jobskilltranunkid") = -1
                .Item("jobunkid") = mintJobsUnkid
                .Item("skillcategoryunkid") = CInt(cboSkillCategory.SelectedValue)
                .Item("skillunkid") = CInt(cboSkillSets.SelectedValue)
                .Item("AUD") = "A"
                .Item("GUID") = Guid.NewGuid.ToString
                'S.SANDEEP [23-Mar-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
                .Item("isactive") = True
                'S.SANDEEP [23-Mar-2018] -- END
            End With

            mdtTran.Rows.Add(dtSRow)

            Call FillSkillList()
            Call ResetSkill()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lvJobSkill_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvJobSkill.SelectedIndexChanged, lvJobSkill.Click
        Try
            If lvJobSkill.SelectedItems.Count > 0 Then
                intSelectedIndex = CInt(lvJobSkill.SelectedItems(0).Index)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvJobSkill_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub FillSkillList()
        Try
            lvJobSkill.Items.Clear()
            Dim lvItem As ListViewItem

            For Each dtRow As DataRow In mdtTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem
                    ' Skill Category
                    Dim objComm_Mster As New clsCommon_Master
                    objComm_Mster._Masterunkid = CInt(dtRow.Item("skillcategoryunkid"))
                    lvItem.Text = objComm_Mster._Name.ToString
                    objComm_Mster = Nothing

                    ' Skill 
                    Dim objSkill As New clsskill_master
                    objSkill._Skillunkid = CInt(dtRow.Item("skillunkid"))
                    lvItem.SubItems.Add(objSkill._Skillname.ToString)
                    objSkill = Nothing

                    lvItem.SubItems.Add(dtRow.Item("skillunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("skillcategoryunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("jobunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)

                    lvItem.Tag = dtRow.Item("jobskilltranunkid")

                    lvJobSkill.Items.Add(lvItem)

                    lvItem = Nothing
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillSkillList", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetSkill()
        Try
            cboSkillCategory.SelectedValue = 0
            cboSkillSets.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetSkill", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Job Key Duties And Responsibilites"

#Region "Private Methods"

    Public Function KeyDutiesValidation() As Boolean
        Try
            If txtKeyDuties.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Key Duties and Responsibilities cannot be blank.Key Duties and Responsibilities is required information."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtKeyDuties.Select()
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "KeyDutiesValidation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Sub FillKeyDuties()
        Try
            Dim mdtTran As DataTable = New DataView(mdtKeyDuties, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable

            dgJobKeyDuties.AutoGenerateColumns = False
            dgcolhKeyDuties.DataPropertyName = "keyduties"
            objdgcolhKeyDutiesGUID.DataPropertyName = "GUID"
            objdgcolhJobKeydutiestranunkid.DataPropertyName = "jobkeydutiesunkid"

            dgJobKeyDuties.DataSource = mdtTran
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillKeyDuties", mstrModuleName)
        End Try
    End Sub

    Public Sub ClearKeyDutiesControl()
        Try
            txtKeyDuties.Text = ""
            btnKeyDutiesAdd.Visible = True
            btnKeyDutiesEdit.Visible = False
            txtKeyDuties.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearKeyDutiesControl", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button Events"

    Private Sub btnKeyDutiesAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKeyDutiesAdd.Click
        Try
            If KeyDutiesValidation() Then

                Dim drRow() As DataRow = mdtKeyDuties.Select("keyduties = '" & txtKeyDuties.Text.Trim & "' AND AUD <> 'D' ")
                If drRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "This Key Duties and Responsibilities already exists in this list.Please define new Key Duties and Responsibilities."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    txtKeyDuties.SelectAll()
                    Exit Sub
                End If

                Dim dtRow As DataRow = Nothing
                dtRow = mdtKeyDuties.NewRow
                dtRow("jobkeydutiesunkid") = -1
                'Hemant (07 June 2019) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                'dtRow("jobunkid") = -1
                dtRow("jobunkid") = mintJobsUnkid
                'Hemant (07 June 2019) -- End
                dtRow("keyduties") = txtKeyDuties.Text.Trim
                dtRow("AUD") = "A"
                dtRow("GUID") = Guid.NewGuid.ToString
                mdtKeyDuties.Rows.Add(dtRow)
                FillKeyDuties()
                ClearKeyDutiesControl()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnKeyDutiesAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnKeyDutiesEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKeyDutiesEdit.Click
        Try
            If dgJobKeyDuties.SelectedRows.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select Key Duties and Responsibilities from the list to perform further operation."), enMsgBoxStyle.Information)
                dgJobKeyDuties.Select()
                Exit Sub
            End If
            If KeyDutiesValidation() Then

                Dim drTemp As DataRow()
                If CInt(dgJobKeyDuties.SelectedRows(0).Cells(objdgcolhJobKeydutiestranunkid.Index).Value) = -1 Then
                    Dim drRow() As DataRow = mdtKeyDuties.Select("keyduties = '" & txtKeyDuties.Text.Trim & "' AND AUD <> 'D' AND GUID <> '" & dgJobKeyDuties.SelectedRows(0).Cells(objdgcolhKeyDutiesGUID.Index).Value.ToString() & "'")
                    If drRow.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "This Key Duties and Responsibilities already exists in this list.Please define new Key Duties and Responsibilities."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        txtKeyDuties.SelectAll()
                        Exit Sub
                    End If
                    drTemp = mdtKeyDuties.Select("GUID = '" & dgJobKeyDuties.SelectedRows(0).Cells(objdgcolhKeyDutiesGUID.Index).Value.ToString() & "'")
                Else
                    Dim drRow() As DataRow = mdtKeyDuties.Select("keyduties = '" & txtKeyDuties.Text.Trim & "' AND AUD <> 'D' AND jobkeydutiesunkid <> " & CInt(dgJobKeyDuties.SelectedRows(0).Cells(objdgcolhJobKeydutiestranunkid.Index).Value))
                    If drRow.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "This Key Duties and Responsibilities already exists in this list.Please define new Key Duties and Responsibilities."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        txtKeyDuties.SelectAll()
                        Exit Sub
                    End If
                    drTemp = mdtKeyDuties.Select("jobkeydutiesunkid = " & CInt(dgJobKeyDuties.SelectedRows(0).Cells(objdgcolhJobKeydutiestranunkid.Index).Value))
                End If

                If drTemp.Length > 0 Then
                    With drTemp(0)
                        .Item("jobkeydutiesunkid") = CInt(dgJobKeyDuties.SelectedRows(0).Cells(objdgcolhJobKeydutiestranunkid.Index).Value)
                        .Item("jobunkid") = mintJobsUnkid
                        .Item("keyduties") = txtKeyDuties.Text.Trim
                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If
                        .AcceptChanges()
                    End With
                End If
                FillKeyDuties()
                ClearKeyDutiesControl()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnKeyDutiesEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnKeyDutiesDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKeyDutiesDelete.Click
        Try
            If dgJobKeyDuties.SelectedRows.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please select Key Duties and Responsibilities from the list to perform further operation."), enMsgBoxStyle.Information)
                dgJobKeyDuties.Select()
                Exit Sub
            End If

            Dim drTemp As DataRow() = Nothing
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Are you sure you want to delete this Key Duties and Responsibilities?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                If CInt(dgJobKeyDuties.SelectedRows(0).Cells(objdgcolhJobKeydutiestranunkid.Index).Value) <= 0 Then
                    drTemp = mdtKeyDuties.Select("GUID = '" & dgJobKeyDuties.SelectedRows(0).Cells(objdgcolhKeyDutiesGUID.Index).Value.ToString() & "'")
                Else
                    drTemp = mdtKeyDuties.Select("jobkeydutiesunkid = " & CInt(dgJobKeyDuties.SelectedRows(0).Cells(objdgcolhJobKeydutiestranunkid.Index).Value))
                End If
                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                End If
                FillKeyDuties()
                ClearKeyDutiesControl()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnKeyDutiesDelete_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid Events"

    Private Sub dgJobKeyDuties_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgJobKeyDuties.CellContentClick, dgJobKeyDuties.CellDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhKeyDutiesEdit.Index Then
                txtKeyDuties.Text = dgJobKeyDuties.Rows(e.RowIndex).Cells(dgcolhKeyDuties.Index).Value.ToString()
                btnKeyDutiesAdd.Visible = False
                btnKeyDutiesEdit.Visible = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgJobKeyDuties_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#End Region

#Region "Job Competencies"

#Region "Private Methods"

    Public Function CompetenciesValidation() As Boolean
        Try
            If CInt(cboCompetenciesCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Competency Category is compulsory information.Please Select Competency Category."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboCompetenciesCategory.Select()
                Return False
            ElseIf CInt(cboCompetencies.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Competency is compulsory information.Please Select Competency."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboCompetencies.Select()
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CompetenciesValidation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    Public Sub FillCompetencies()
        Try
            Dim mdtTran As DataTable = New DataView(mdtJobCompetencies, "AUD <> 'D'", "competence_categoryunkid asc", DataViewRowState.CurrentRows).ToTable

            lvCompetencies.Items.Clear()
            Dim lvItem As ListViewItem = Nothing


            For Each dtRow As DataRow In mdtTran.Rows
                lvItem = New ListViewItem
                lvItem.Tag = dtRow.Item("jobcompetencetranunkid")
                lvItem.Text = dtRow.Item("competence_category").ToString
                lvItem.SubItems.Add(dtRow.Item("competency").ToString)
                lvItem.SubItems.Add(dtRow.Item("GUID").ToString)
                lvCompetencies.Items.Add(lvItem)
                lvItem = Nothing
            Next

            lvCompetencies.GridLines = False
            lvCompetencies.GroupingColumn = colhCompetenciesGroup
            lvCompetencies.DisplayGroups(True)

            If lvCompetencies.Items.Count > 7 Then
                colhCompetenciesItem.Width = 522 - 20
            Else
                colhCompetenciesItem.Width = 522
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCompetencies", mstrModuleName)
        End Try
    End Sub

    Public Sub ClearCompetenciesControl()
        Try
            cboCompetenciesCategory.SelectedValue = 0
            cboCompetencies.SelectedValue = 0
            cboCompetenciesCategory.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCompetenciesControl", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboCompetenciesCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCompetenciesCategory.SelectedIndexChanged
        Try
            Dim objCompetencieMst As New clsassess_competencies_master
            Dim objMaster As New clsMasterData
            Dim mintPeriodID As Integer = objMaster.getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime.Date, FinancialYear._Object._YearUnkid, enStatusType.Open)
            Dim dsList As DataSet = objCompetencieMst.getComboList(CInt(cboCompetenciesCategory.SelectedValue), mintPeriodID, True, "List")
            With cboCompetencies
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            cboCompetencies.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompetenciesCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button Events"

    Private Sub btnAddCompetencies_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddCompetencies.Click
        Try
            If CompetenciesValidation() Then

                Dim drRow() As DataRow = mdtJobCompetencies.Select("competence_categoryunkid = " & CInt(cboCompetenciesCategory.SelectedValue) & " AND competenciesunkid = " & CInt(cboCompetencies.SelectedValue) & " AND AUD <> 'D' ")
                If drRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "This Competency Category and Competency is already exist in this list.Please define new Competency Category or Competency."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    txtKeyDuties.SelectAll()
                    Exit Sub
                End If

                Dim dtRow As DataRow = Nothing
                dtRow = mdtJobCompetencies.NewRow
                dtRow("jobcompetencetranunkid") = -1
                'Hemant (07 June 2019) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                'dtRow("jobunkid") = -1
                dtRow("jobunkid") = mintJobsUnkid
                'Hemant (07 June 2019) -- End
                dtRow("competence_categoryunkid") = CInt(cboCompetenciesCategory.SelectedValue)
                dtRow("competence_category") = cboCompetenciesCategory.Text
                dtRow("competenciesunkid") = CInt(cboCompetencies.SelectedValue)
                dtRow("competency") = cboCompetencies.Text
                dtRow("AUD") = "A"
                dtRow("GUID") = Guid.NewGuid.ToString
                mdtJobCompetencies.Rows.Add(dtRow)
                FillCompetencies()
                ClearCompetenciesControl()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddCompetencies_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDeleteCompetencies_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteCompetencies.Click
        Try
            If lvCompetencies.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please select Competency from the list to perform further operation."), enMsgBoxStyle.Information)
                dgJobKeyDuties.Select()
                Exit Sub
            End If

            Dim drTemp As DataRow() = Nothing
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Are you sure you want to delete this Competency?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                If CInt(lvCompetencies.SelectedItems(0).Tag) <= 0 Then
                    drTemp = mdtJobCompetencies.Select("GUID = '" & lvCompetencies.SelectedItems(0).SubItems(objcolhCompetencyGUID.Index).Text.ToString() & "'")
                Else
                    drTemp = mdtJobCompetencies.Select("jobcompetencetranunkid = " & CInt(lvCompetencies.SelectedItems(0).Tag))
                End If
                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                End If
                FillCompetencies()
                ClearCompetenciesControl()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteCompetencies_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSeachCompetenciesCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSeachCompetenciesCategory.Click, objBtnSearchCompetencies.Click
        Dim frm As New frmCommonSearch
        Try
            Dim xCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSeachCompetenciesCategory.Name.ToUpper
                    xCbo = cboCompetenciesCategory
                Case objBtnSearchCompetencies.Name.ToUpper
                    xCbo = cboCompetencies
            End Select
            If xCbo.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = xCbo.ValueMember
                .DisplayMember = xCbo.DisplayMember
                .DataSource = CType(xCbo.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                xCbo.SelectedValue = frm.SelectedValue
                xCbo.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSeachCompetenciesCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


#End Region

#End Region

    'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
#Region " Job Language Transactions "

    Private Sub btnDeleteLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteLanguage.Click
        Try
            If intSelectedIndex > -1 Then
                Dim drTemp As DataRow()
                If CInt(lvJobLanguage.Items(intSelectedIndex).Tag) = -1 Then
                    drTemp = mdtLangTran.Select("GUID = '" & lvJobLanguage.Items(intSelectedIndex).SubItems(objcolhLangGUID.Index).Text & "'")
                Else
                    drTemp = mdtLangTran.Select("joblanguagetranunkid = '" & CInt(lvJobLanguage.Items(intSelectedIndex).Tag) & "'")
                End If
                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    drTemp(0).Item("isactive") = False
                    Call FillLanguageList()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAddLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddLanguage.Click
        Try
            If CInt(cboLanguage.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Language is compulsory information. Please Select Language to continue."), enMsgBoxStyle.Information)
                cboLanguage.Focus()
                Exit Sub
            End If

            Dim dtRow As DataRow() = mdtLangTran.Select("masterunkid = " & CInt(cboLanguage.SelectedValue) & " AND AUD <> 'D' ")

            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Selected Language is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtSRow As DataRow
            dtSRow = mdtLangTran.NewRow

            With dtSRow
                .Item("joblanguagetranunkid") = -1
                .Item("jobunkid") = mintJobsUnkid
                .Item("masterunkid") = CInt(cboLanguage.SelectedValue)
                .Item("languagename") = cboLanguage.Text
                .Item("AUD") = "A"
                .Item("GUID") = Guid.NewGuid.ToString
                .Item("isactive") = True
            End With

            mdtLangTran.Rows.Add(dtSRow)

            Call FillLanguageList()
            Call ResetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lvJobLanguage_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvJobLanguage.SelectedIndexChanged, lvJobLanguage.Click
        Try
            If lvJobLanguage.SelectedItems.Count > 0 Then
                intSelectedIndex = CInt(lvJobLanguage.SelectedItems(0).Index)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvJobLanguage_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub FillLanguageList()
        Try
            lvJobLanguage.Items.Clear()
            Dim lvItem As ListViewItem

            For Each dtRow As DataRow In mdtLangTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("languagename").ToString
                    lvItem.Tag = dtRow.Item("joblanguagetranunkid")

                    lvItem.SubItems.Add(dtRow.Item("masterunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("jobunkid").ToString)
                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)

                    lvJobLanguage.Items.Add(lvItem)

                    lvItem = Nothing
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLanguageList", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetLanguage()
        Try
            cboLanguage.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetLanguage", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (18 Feb 2020) -- End

#Region " Forms Events "

    Private Sub frmJobs_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objJobs = Nothing
    End Sub

    Private Sub frmJobs_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobs_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJobs_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objJobs = New clsJobs
        objJobSkill = New clsJob_Skill_Tran
        objJobQualification = New clsJob_Qualification_Tran
        'Sohail (18 Feb 2020) -- Start
        'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
        objJobLanguage = New clsJob_Language_Tran
        'Sohail (18 Feb 2020) -- End

        'Pinkal (03-Dec-2015) -- Start
        'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
        objKeyduties = New clsJobKeyduties_Tran
        objJobCompetencies = New clsJobCompetencies_Tran
        'Pinkal (03-Dec-2015) -- End

        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call setColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objJobs._Jobunkid = mintJobsUnkid
            End If

            Call GetValue()
            objJobSkill._JobUnkid = mintJobsUnkid
            mdtTran = objJobSkill._DataTable
            Call FillSkillList()

            objJobQualification._JobUnkid = mintJobsUnkid
            mdtQTran = objJobQualification._DataTable
            Call FillQualificationList()

            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
            objKeyduties._Jobunkid = mintJobsUnkid
            mdtKeyDuties = objKeyduties._mdtKeyDuties
            Call FillKeyDuties()
            btnKeyDutiesEdit.Visible = False


            objJobCompetencies._Jobunkid = mintJobsUnkid
            mdtJobCompetencies = objJobCompetencies._mdtCompetenciesTran
            Call FillCompetencies()
            'Pinkal (03-Dec-2015) -- End

            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
            objJobLanguage._JobUnkid = mintJobsUnkid
            mdtLangTran = objJobLanguage._DataTable
            Call FillLanguageList()
            'Sohail (18 Feb 2020) -- End

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmJobs_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsJobs.SetMessages()
            objfrm._Other_ModuleNames = "clsJobs"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim objSuccession As New clssucscreening_process_master
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Job Name cannot be blank. Job Name is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            If txtName.Text.Trim = cboReportTo.Text Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Job Name cannot be same as Report to. Please select other Report to for this job."), enMsgBoxStyle.Information)
                cboReportTo.Focus()
                Exit Sub
            End If

            'S.SANDEEP [05-Mar-2018] -- START
            'ISSUE/ENHANCEMENT : {#ARUTI-9}
            If dtpTerminateDate.Checked = True AndAlso dtpCreateDate.Checked = True Then
                If dtpTerminateDate.Value.Date < dtpCreateDate.Value.Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, termination date cannot be less than created date for the job. Please set correct termiation date."), enMsgBoxStyle.Information)
                    dtpTerminateDate.Focus()
                    Exit Sub
                End If
            End If
            'S.SANDEEP [05-Mar-2018] -- END



            If menAction = enAction.EDIT_ONE AndAlso chkKeyRole.Checked = False Then
                If objSuccession.IsSuccessionStarted(Nothing, mintJobsUnkid) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, you can't uncheck key role option. Succession Process is already started for this job."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objJobs._FormName = mstrModuleName
            objJobs._LoginEmployeeunkid = 0
            objJobs._ClientIP = getIP()
            objJobs._HostName = getHostName()
            objJobs._FromWeb = False
            objJobs._AuditUserId = User._Object._Userunkid
objJobs._CompanyUnkid = Company._Object._Companyunkid
            objJobs._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Pinkal (03-Dec-2015) -- Start
            'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

            'If menAction = enAction.EDIT_ONE Then
            '    blnFlag = objJobs.Update(mdtTran, mdtQTran)
            'Else
            '    blnFlag = objJobs.Insert(mdtTran, mdtQTran)
            'End If

            If menAction = enAction.EDIT_ONE Then
                'Sohail (18 Feb 2020) -- Start
                'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
                'blnFlag = objJobs.Update(mdtTran, mdtQTran, mdtKeyDuties, mdtJobCompetencies)
                blnFlag = objJobs.Update(mdtTran, mdtQTran, mdtKeyDuties, mdtJobCompetencies, mdtLangTran)
                'Sohail (18 Feb 2020) -- End
            Else
                'Sohail (18 Feb 2020) -- Start
                'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
                'blnFlag = objJobs.Insert(mdtTran, mdtQTran, mdtKeyDuties, mdtJobCompetencies)
                blnFlag = objJobs.Insert(mdtTran, mdtQTran, mdtKeyDuties, mdtJobCompetencies, mdtLangTran)
                'Sohail (18 Feb 2020) -- End

                If blnFlag Then

                    Dim StrMessage As New System.Text.StringBuilder
                    StrMessage.Append("<TR>" & vbCrLf)
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & IIf(dtpCreateDate.Checked = True, objJobs._Create_Date.ToShortDateString(), DateTime.Today.ToShortDateString()).ToString() & "</span></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & txtName.Text & "</span></TD>")

                    If cboReportTo.SelectedIndex > 0 Then
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & cboReportTo.Text & "</span></TD>")
                    Else
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '></span></TD>")
                    End If

                    If CInt(cboGrade.SelectedValue) > 0 Then
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & cboGrade.Text & "</span></TD>")
                    Else
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '></span></TD>")
                    End If

                    If CInt(cboJobGroup.SelectedValue) > 0 Then
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & cboJobGroup.Text & "</span></TD>")
                    Else
                        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '></span></TD>")
                    End If

                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & nudJobLevel.Value & "</span></TD>")
                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & nudPosition.Value & "</span></TD>")

                    'Gajanan [19-OCT-2019] -- Start    
                    'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
                    'objJobs.SendNotification(ConfigParameter._Object._NewJobAddNotificationUserIds, Company._Object._Companyunkid, mstrForm_Name, enLogin_Mode.DESKTOP, User._Object._Userunkid, User._Object._Username, StrMessage.ToString())
                    If ConfigParameter._Object._NewJobAddNotificationUserIds.Trim.Length > 0 Then
                        objJobs.SendNotification(ConfigParameter._Object._NewJobAddNotificationUserIds, Company._Object._Companyunkid, mstrForm_Name, enLogin_Mode.DESKTOP, User._Object._Userunkid, User._Object._Username, StrMessage.ToString())
                    End If
                    'Gajanan [19-OCT-2019] -- End
                   
                End If


            End If

            'Pinkal (03-Dec-2015) -- End

            If blnFlag = False And objJobs._Message <> "" Then
                eZeeMsgBox.Show(objJobs._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objJobs = Nothing
                    objJobs = New clsJobs
                    Call GetValue()
                    txtName.Focus()
                    objJobSkill._JobUnkid = mintJobsUnkid
                    mdtTran = objJobSkill._DataTable
                    Call FillSkillList()

                    objJobQualification._JobUnkid = mintJobsUnkid
                    mdtQTran = objJobQualification._DataTable
                    Call FillQualificationList()

                    'Pinkal (03-Dec-2015) -- Start
                    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.
                    objKeyduties._Jobunkid = mintJobsUnkid
                    mdtKeyDuties = objKeyduties._mdtKeyDuties
                    Call FillKeyDuties()

                    objJobCompetencies._Jobunkid = mintJobsUnkid
                    mdtJobCompetencies = objJobCompetencies._mdtCompetenciesTran
                    Call FillCompetencies()
                    'Pinkal (03-Dec-2015) -- End

                    'Sohail (18 Feb 2020) -- Start
                    'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
                    objJobLanguage._JobUnkid = mintJobsUnkid
                    mdtLangTran = objJobLanguage._DataTable
                    Call FillLanguageList()
                    'Sohail (18 Feb 2020) -- End

                    'Shani(18-JUN-2016) -- Start
                    'Enhancement : Add Allocation Column in Job master[Rutta]
                    pnlJobGrp.Enabled = False
                    pnlSection.Enabled = False
                    pnlUnit.Enabled = False
                    pnlTeam.Enabled = False
                    pnlGrades.Enabled = False
                    pnlDepartment.Enabled = False
                    pnlClassGroup.Enabled = False
                    pnlBranch.Enabled = False
                    pnlDeprtmentGrp.Enabled = False
                    pnlSectionGrp.Enabled = False
                    pnlUnitGrp.Enabled = False
                    pnlClass.Enabled = False
                    pnlGradresLevel.Enabled = False
                    'Shani(18-JUN-2016) -- End


                Else
                    mintJobsUnkid = objJobs._Jobunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            Call objFrm.displayDialog(txtName.Text, objJobs._Job_Name1, objJobs._Job_Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchSkill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSkill.Click
        Dim objfrm As New frmCommonSearch
        Try
            If CType(cboSkillSets.DataSource, DataTable) Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            With objfrm
                .ValueMember = cboSkillSets.ValueMember
                .DisplayMember = cboSkillSets.DisplayMember
                .DataSource = CType(cboSkillSets.DataSource, DataTable)
                .CodeMember = ""
            End With
            If objfrm.DisplayDialog Then
                cboSkillSets.SelectedValue = objfrm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchQualification.Click
        Dim objfrm As New frmCommonSearch
        Try
            If CType(cboQualificationGrp.DataSource, DataTable) Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            With objfrm
                .ValueMember = cboQualification.ValueMember
                .DisplayMember = cboQualification.DisplayMember
                .DataSource = CType(cboQualification.DataSource, DataTable)
                .CodeMember = ""
            End With
            If objfrm.DisplayDialog Then
                cboQualification.SelectedValue = objfrm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchQualification_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            With objfrm
                .ValueMember = cboSkillCategory.ValueMember
                .DisplayMember = cboSkillCategory.DisplayMember
                .DataSource = CType(cboSkillCategory.DataSource, DataTable)
                .CodeMember = ""
            End With
            If objfrm.DisplayDialog Then
                cboSkillCategory.SelectedValue = objfrm.SelectedValue
                cboSkillCategory.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddSkill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSkill.Click
        Dim frm As New frmSkill_AddEdit
        Dim intRefId As Integer = -1
        Dim objSkill As New clsskill_master
        Dim dsList As New DataSet
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsList = objSkill.getComboList("Skills", True, CInt(cboSkillCategory.SelectedValue))
                With cboSkillSets
                    .ValueMember = "skillunkid"
                    .DisplayMember = "NAME"
                    .DataSource = dsList.Tables("Skills")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSkill_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddSkillCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSkillCategory.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Dim objComm_Master As New clsCommon_Master
        Dim dsList As New DataSet
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.SKILL_CATEGORY, enAction.ADD_ONE)
            dsList = objComm_Master.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "Category")
            With cboSkillCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Category")
                .SelectedValue = intRefId
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSkillCategory_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddSection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSection.Click
        Dim frm As New frmSections_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objSec As New clsSections
                dsList = objSec.getComboList("Section", True)
                With cboSection
                    .ValueMember = "sectionunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Section")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objSec = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSection_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddGrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGrade.Click
        Dim frm As New frmGrade_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objGrade As New clsGrade
                dsList = objGrade.getComboList("Grade", True)
                With cboGrade
                    .ValueMember = "gradeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Grade")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objGrade = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGrade_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddUnit.Click
        Dim frm As New frmUnits_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objUnit As New clsUnits
                dsList = objUnit.getComboList("Unit", True)
                With cboUnit
                    .ValueMember = "unitunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Unit")
                End With
                dsList.Dispose()
                objUnit = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddUnit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddJobGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddJobGroup.Click
        Dim frm As New frmJobGroup_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objGroup As New clsJobGroup
                dsList = objGroup.getComboList("Group", True)
                With cboJobGroup
                    .ValueMember = "jobgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Group")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objGroup = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddJobGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddTeam_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddTeam.Click
        Dim frm As New frmTeams_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objTeams As New clsTeams
                dsList = objTeams.getComboList("Team", True)
                With cboTeam
                    .ValueMember = "teamunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Team")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objTeams = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddTeam_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnAddReminder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReminder.Click
        Try
            Dim objRemider As New frmReminder_AddEdit
            If User._Object._Isrighttoleft = True Then
                objRemider.RightToLeft = Windows.Forms.RightToLeft.Yes
                objRemider.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objRemider)
            End If
            objRemider.displayDialog(-1, enAction.ADD_ONE)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReminder_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchQGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchQGrp.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            With objfrm
                .ValueMember = cboQualificationGrp.ValueMember
                .DisplayMember = cboQualificationGrp.DisplayMember
                .DataSource = CType(cboQualificationGrp.DataSource, DataTable)
                .CodeMember = ""
            End With
            If objfrm.DisplayDialog Then
                cboQualificationGrp.SelectedValue = objfrm.SelectedValue
                cboQualificationGrp.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchQGrp_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddQGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddQGroup.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Dim objComm_Master As New clsCommon_Master
        Dim dsList As New DataSet
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, enAction.ADD_ONE)
            dsList = objComm_Master.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "QGrp")
            With cboQualificationGrp
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("QGrp")
                .SelectedValue = intRefId
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddQGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddQualification.Click
        Dim frm As New frmQualificationCourse_AddEdit
        Dim intRefId As Integer = -1
        Dim objQualification As New clsqualification_master
        Dim dsList As New DataSet
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsList = objQualification.GetComboList("Qualification", True, CInt(cboQualificationGrp.SelectedValue))
                With cboQualification
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("Qualification")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddQualification_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchJobCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJobCode.Click
        Dim frm As New frmCommonSearch
        Try
            If CType(cboReportTo.DataSource, DataTable) Is Nothing Then Exit Sub
            With frm
                .ValueMember = cboReportTo.ValueMember
                .DisplayMember = cboReportTo.DisplayMember
                .DataSource = CType(cboReportTo.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboReportTo.SelectedValue = frm.SelectedValue
                cboReportTo.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJobCode_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

    Private Sub objbtnSearchJGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJGroup.Click, _
                                                                                                             objbtnSearchSection.Click, _
                                                                                                             objbtnSearchUnit.Click, _
                                                                                                             objbtnSearchTeam.Click, _
                                                                                                             objbtnSearchGrade.Click, _
                                                                                                             objbtnSearchDepartment.Click, _
                                                                                                             objbtnSearchClassGroup.Click, _
                                                                                                             objbtnSearchDirectReportTo.Click, _
                                                                                                             objbtnSearchInDirectReportTo.Click, _
                                                                                                             objbtnSearchBranch.Click, _
                                                                                                             objbtnSearchDeparmentGrp.Click, _
                                                                                                             objbtnSearchSectionGroup.Click, _
                                                                                                             objbtnSearchUnitGrp.Click, _
                                                                                                             objbtnSearchClass.Click, _
                                                                                                             objbtnSearchGradesLevel.Click

        'Shani(18-JUN-2016) --ADD-->[jobbranchunkid,jobdepartmentgrpunkid,jobsectiongrpunkid,jobunitgrpunkid,jobclassunkid,jobgradelevelunkid]


        Dim frm As New frmCommonSearch
        Try
            Dim xCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchJGroup.Name.ToUpper
                    xCbo = cboJobGroup
                Case objbtnSearchSection.Name.ToUpper
                    xCbo = cboSection
                Case objbtnSearchUnit.Name.ToUpper
                    xCbo = cboUnit
                Case objbtnSearchTeam.Name.ToUpper
                    xCbo = cboTeam
                Case objbtnSearchGrade.Name.ToUpper
                    xCbo = cboGrade
                Case objbtnSearchDepartment.Name.ToUpper
                    xCbo = cboDepartment
                Case objbtnSearchClassGroup.Name.ToUpper
                    xCbo = cboClassGroup
                Case objbtnSearchDirectReportTo.Name.ToUpper
                    xCbo = cboReportTo
                Case objbtnSearchInDirectReportTo.Name.ToUpper
                    xCbo = cboInDirectReportTo
                    'Shani(18-JUN-2016) -- Start
                    'Enhancement : Add Allocation Column in Job master[Rutta]
                Case objbtnSearchBranch.Name.ToUpper
                    xCbo = cboBranch
                Case objbtnSearchDeparmentGrp.Name.ToUpper
                    xCbo = cboDeparmentGrp
                Case objbtnSearchSectionGroup.Name.ToUpper
                    xCbo = cboSectionGrp
                Case objbtnSearchUnitGrp.Name.ToUpper
                    xCbo = cboUnitGrp
                Case objbtnSearchClass.Name.ToUpper
                    xCbo = cboClass
                Case objbtnSearchGradesLevel.Name.ToUpper
                    xCbo = cboGradeLevel
                    'Shani(18-JUN-2016) -- End
            End Select
            If xCbo.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = xCbo.ValueMember
                .DisplayMember = xCbo.DisplayMember
                .DataSource = CType(xCbo.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                xCbo.SelectedValue = frm.SelectedValue
                xCbo.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddDepartment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddDepartment.Click
        Dim frm As New frmDepartment_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objDepartment As New clsDepartment
                dsList = objDepartment.getComboList("Department", True)
                With cboDepartment
                    .ValueMember = "departmentunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Department")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objDepartment = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddDepartment_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddClassGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddClassGroup.Click
        Dim frm As New frmClassGroup_AddEdit
        Dim intRefId As Integer = -1
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objClassGrp As New clsClassGroup
                dsList = objClassGrp.getComboList("ClassGroup", True)
                With cboClassGroup
                    .ValueMember = "classgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("ClassGroup")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objClassGrp = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddClassGroup_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (03-Dec-2015) -- End

    'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
    Private Sub objbtnSearchLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLanguage.Click
        Dim objfrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            With objfrm
                .ValueMember = cboLanguage.ValueMember
                .DisplayMember = cboLanguage.DisplayMember
                .DataSource = CType(cboLanguage.DataSource, DataTable)
                .CodeMember = ""
            End With
            If objfrm.DisplayDialog Then
                cboLanguage.SelectedValue = objfrm.SelectedValue
                cboLanguage.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddLanguage.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = 0
        Dim objComm_Master As New clsCommon_Master
        Dim dsList As New DataSet
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.LANGUAGES, enAction.ADD_ONE)
            dsList = objComm_Master.getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, "Lang")
            With cboLanguage
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Lang")
                .SelectedValue = intRefId
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddLanguage_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Feb 2020) -- End

#End Region

#Region " Combobox Events "

    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Try
            If CInt(cboGrade.SelectedValue) > 0 Then
                chkGrades.Checked = True
            Else
                chkGrades.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSection_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSection.SelectedIndexChanged
        Try
            If CInt(cboSection.SelectedValue) > 0 Then
                chkSection.Checked = True
            Else
                chkSection.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSection_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboUnit_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnit.SelectedIndexChanged
        Try
            If CInt(cboUnit.SelectedValue) > 0 Then
                chkUnit.Checked = True
            Else
                chkUnit.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUnit_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboJobGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJobGroup.SelectedIndexChanged
        Try
            If CInt(cboJobGroup.SelectedValue) > 0 Then
                chkJobGroup.Checked = True
            Else
                chkJobGroup.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJobGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTeam.SelectedIndexChanged
        Try
            If CInt(cboTeam.SelectedValue) > 0 Then
                chkTeam.Checked = True
            Else
                chkTeam.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTeam_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboSkillCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSkillCategory.SelectedIndexChanged
        Try
            If CInt(cboSkillCategory.SelectedValue) > 0 Then
                Dim objSkill As New clsskill_master
                Dim dsList As New DataSet
                dsList = objSkill.getComboList("Skills", True, CInt(cboSkillCategory.SelectedValue))
                With cboSkillSets
                    .ValueMember = "skillunkid"
                    .DisplayMember = "NAME"
                    .DataSource = dsList.Tables("Skills")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSkillCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboQualificationGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQualificationGrp.SelectedIndexChanged
        Try

            'Anjan [25 March 2016] -- Start
            'ENHANCEMENT : As Qualification search button was giving error if clicked first before setting Qualtification group.
            'If CInt(cboQualificationGrp.SelectedValue) > 0 Then
            'Anjan [25 March 2016] -- End


            Dim objQualification As New clsqualification_master
            Dim dsList As New DataSet
            dsList = objQualification.GetComboList("Qualification", True, CInt(cboQualificationGrp.SelectedValue))
            With cboQualification
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("Qualification")
                .SelectedValue = 0
            End With
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboQualificationGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

    Private Sub cboReportTo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportTo.SelectedIndexChanged
        Try

            Dim dtTable As DataTable = CType(cboReportTo.DataSource, DataTable).Copy
            If CInt(cboReportTo.SelectedValue) > 0 Then
                dtTable = New DataView(dtTable, "jobunkid <>" & CInt(cboReportTo.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
            End If
            With cboInDirectReportTo
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dtTable
            End With
            cboInDirectReportTo.SelectedValue = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportTo_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDepartment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepartment.SelectedIndexChanged
        Try
            If CInt(cboDepartment.SelectedValue) > 0 Then
                chkDepartment.Checked = True
            Else
                chkDepartment.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDepartment_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboClassGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboClassGroup.SelectedIndexChanged
        Try
            If CInt(cboClassGroup.SelectedValue) > 0 Then
                chkClassGroup.Checked = True
            Else
                chkClassGroup.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboClassGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (03-Dec-2015) -- End

    'Shani(18-JUN-2016) -- Start
    'Enhancement : Add Allocation Column in Job master[Rutta]
    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        Try
            If CInt(cboBranch.SelectedValue) > 0 Then
                chkBranch.Checked = True
            Else
                chkBranch.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBranch_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDeparmentGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDeparmentGrp.SelectedIndexChanged
        Try
            If CInt(cboDeparmentGrp.SelectedValue) > 0 Then
                chkDepartmentGroup.Checked = True
            Else
                chkDepartmentGroup.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDeparmentGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSectionGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSectionGrp.SelectedIndexChanged
        Try
            If CInt(cboSectionGrp.SelectedValue) > 0 Then
                chkSectionGroup.Checked = True
            Else
                chkSectionGroup.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSectionGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboUnitGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnitGrp.SelectedIndexChanged
        Try
            If CInt(cboUnitGrp.SelectedValue) > 0 Then
                chkUnitGroup.Checked = True
            Else
                chkUnitGroup.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUnitGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboClass_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboClass.SelectedIndexChanged
        Try
            If CInt(cboClass.SelectedValue) > 0 Then
                chkClass.Checked = True
            Else
                chkClass.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboClass_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGradeLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeLevel.SelectedIndexChanged
        Try
            If CInt(cboGradeLevel.SelectedValue) > 0 Then
                ChkGradeLevel.Checked = True
            Else
                ChkGradeLevel.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Shani(18-JUN-2016) -- End

    'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : Pick employment type for type of job on job master.
    Private Sub cboJobType_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboJobType.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboJobType.Name Then
                        .CodeMember = ""
                    Else
                        .CodeMember = "code"
                    End If
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.SelectedValue = 0
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Feb 2020) -- End

#End Region

#Region " Checkbox Events "


    'Pinkal (03-Dec-2015) -- Start
    'Enhancement - enhacement for CCBRT & SL ,requested by Andrew Muga.

    Private Sub chkJobGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkJobGroup.CheckedChanged, _
                                                                                                               chkSection.CheckedChanged, _
                                                                                                               chkUnit.CheckedChanged, _
                                                                                                               chkTeam.CheckedChanged, _
                                                                                                               chkGrades.CheckedChanged, _
                                                                                                               chkDepartment.CheckedChanged, _
                                                                                                               chkClassGroup.CheckedChanged, _
                                                                                                               chkBranch.CheckedChanged, _
                                                                                                               chkDepartmentGroup.CheckedChanged, _
                                                                                                               chkSectionGroup.CheckedChanged, _
                                                                                                               chkUnitGroup.CheckedChanged, _
                                                                                                               chkClass.CheckedChanged, _
                                                                                                               ChkGradeLevel.CheckedChanged

        'Shani(18-JUN-2016) --ADD-->[jobbranchunkid,jobdepartmentgrpunkid,jobsectiongrpunkid,jobunitgrpunkid,jobclassunkid,jobgradelevelunkid]


        Try
            Select Case CType(sender, CheckBox).Name.ToUpper
                'Shani(18-JUN-2016) -- Start
                'Enhancement : Add Allocation Column in Job master[Rutta]
                'Case chkJobGroup.Name.ToUpper
                '    pnlJobGrp.Enabled = chkJobGroup.Checked
                'Case chkSection.Name.ToUpper
                '    pnlSection.Enabled = chkSection.Checked
                'Case chkUnit.Name.ToUpper
                '    pnlUnit.Enabled = chkUnit.Checked
                'Case chkTeam.Name.ToUpper
                '    pnlTeam.Enabled = chkTeam.Checked
                'Case chkGrades.Name.ToUpper
                '    pnlGrades.Enabled = chkGrades.Checked
                'Case chkDepartment.Name.ToUpper
                '    pnlDepartment.Enabled = chkDepartment.Checked
                'Case chkClassGroup.Name.ToUpper
                '    pnlClassGroup.Enabled = chkClassGroup.Checked
                Case chkJobGroup.Name.ToUpper
                    pnlJobGrp.Enabled = chkJobGroup.Checked
                    If chkJobGroup.Checked = False Then cboJobGroup.SelectedValue = 0
                Case chkSection.Name.ToUpper
                    pnlSection.Enabled = chkSection.Checked
                    If chkSection.Checked = False Then cboSection.SelectedValue = 0
                Case chkUnit.Name.ToUpper
                    pnlUnit.Enabled = chkUnit.Checked
                    If chkUnit.Checked = False Then cboUnit.SelectedValue = 0
                Case chkTeam.Name.ToUpper
                    pnlTeam.Enabled = chkTeam.Checked
                    If chkTeam.Checked = False Then cboTeam.SelectedValue = 0
                Case chkGrades.Name.ToUpper
                    pnlGrades.Enabled = chkGrades.Checked
                    If chkGrades.Checked = False Then cboGrade.SelectedValue = 0
                Case chkDepartment.Name.ToUpper
                    pnlDepartment.Enabled = chkDepartment.Checked
                    If chkDepartment.Checked = False Then cboDepartment.SelectedValue = 0
                Case chkClassGroup.Name.ToUpper
                    pnlClassGroup.Enabled = chkClassGroup.Checked
                    If chkClassGroup.Checked = False Then cboClassGroup.SelectedValue = 0
                Case chkBranch.Name.ToUpper
                    pnlBranch.Enabled = chkBranch.Checked
                    If chkBranch.Checked = False Then cboBranch.SelectedValue = 0
                Case chkDepartmentGroup.Name.ToUpper
                    pnlDeprtmentGrp.Enabled = chkDepartmentGroup.Checked
                    If chkDepartmentGroup.Checked = False Then cboDeparmentGrp.SelectedValue = 0
                Case chkSectionGroup.Name.ToUpper
                    pnlSectionGrp.Enabled = chkSectionGroup.Checked
                    If chkSectionGroup.Checked = False Then cboSectionGrp.SelectedValue = 0
                Case chkUnitGroup.Name.ToUpper
                    pnlUnitGrp.Enabled = chkUnitGroup.Checked
                    If chkUnitGroup.Checked = False Then cboUnitGrp.SelectedValue = 0
                Case chkClass.Name.ToUpper
                    pnlClass.Enabled = chkClass.Checked
                    If chkClass.Checked = False Then cboClass.SelectedValue = 0
                Case ChkGradeLevel.Name.ToUpper
                    pnlGradresLevel.Enabled = ChkGradeLevel.Checked
                    If ChkGradeLevel.Checked = False Then cboGradeLevel.SelectedValue = 0
                    'Shani(18-JUN-2016) -- End


            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkJobGroup_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (03-Dec-2015) -- End

#End Region



    
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbJobsInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbJobsInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbJobSkill.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbJobSkill.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbJobQulification.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbJobQulification.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbKeyDutiesResponsiblities.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbKeyDutiesResponsiblities.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbCompetencies.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCompetencies.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbJobLanguage.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbJobLanguage.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteJobQualification.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteJobQualification.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddJobQualification.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddJobQualification.GradientForeColor = GUI._ButttonFontColor

            Me.btnKeyDutiesDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnKeyDutiesDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnKeyDutiesAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnKeyDutiesAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteCompetencies.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteCompetencies.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddCompetencies.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddCompetencies.GradientForeColor = GUI._ButttonFontColor

            Me.btnKeyDutiesEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnKeyDutiesEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnDeleteLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDeleteLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnAddLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.btnAddLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblReportTo.Text = Language._Object.getCaption(Me.lblReportTo.Name, Me.lblReportTo.Text)
            Me.tabpJobInfo.Text = Language._Object.getCaption(Me.tabpJobInfo.Name, Me.tabpJobInfo.Text)
            Me.gbJobsInfo.Text = Language._Object.getCaption(Me.gbJobsInfo.Name, Me.gbJobsInfo.Text)
            Me.chkTeam.Text = Language._Object.getCaption(Me.chkTeam.Name, Me.chkTeam.Text)
            Me.chkUnit.Text = Language._Object.getCaption(Me.chkUnit.Name, Me.chkUnit.Text)
            Me.elLine2.Text = Language._Object.getCaption(Me.elLine2.Name, Me.elLine2.Text)
            Me.elLine1.Text = Language._Object.getCaption(Me.elLine1.Name, Me.elLine1.Text)
            Me.chkGrades.Text = Language._Object.getCaption(Me.chkGrades.Name, Me.chkGrades.Text)
            Me.chkSection.Text = Language._Object.getCaption(Me.chkSection.Name, Me.chkSection.Text)
            Me.chkJobGroup.Text = Language._Object.getCaption(Me.chkJobGroup.Name, Me.chkJobGroup.Text)
            Me.lblJobLevel.Text = Language._Object.getCaption(Me.lblJobLevel.Name, Me.lblJobLevel.Text)
            Me.lblTerminateDate.Text = Language._Object.getCaption(Me.lblTerminateDate.Name, Me.lblTerminateDate.Text)
            Me.lblPosition.Text = Language._Object.getCaption(Me.lblPosition.Name, Me.lblPosition.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblCreateDate.Text = Language._Object.getCaption(Me.lblCreateDate.Name, Me.lblCreateDate.Text)
            Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
            Me.tabpJobSkills.Text = Language._Object.getCaption(Me.tabpJobSkills.Name, Me.tabpJobSkills.Text)
            Me.gbJobSkill.Text = Language._Object.getCaption(Me.gbJobSkill.Name, Me.gbJobSkill.Text)
            Me.colhCategory.Text = Language._Object.getCaption(CStr(Me.colhCategory.Tag), Me.colhCategory.Text)
            Me.colhSkill.Text = Language._Object.getCaption(CStr(Me.colhSkill.Tag), Me.colhSkill.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.lblSkills.Text = Language._Object.getCaption(Me.lblSkills.Name, Me.lblSkills.Text)
            Me.lblSkillCategory.Text = Language._Object.getCaption(Me.lblSkillCategory.Name, Me.lblSkillCategory.Text)
            Me.tabpQualification.Text = Language._Object.getCaption(Me.tabpQualification.Name, Me.tabpQualification.Text)
            Me.gbJobQulification.Text = Language._Object.getCaption(Me.gbJobQulification.Name, Me.gbJobQulification.Text)
            Me.colhQualificationGrp.Text = Language._Object.getCaption(CStr(Me.colhQualificationGrp.Tag), Me.colhQualificationGrp.Text)
            Me.colhQualification.Text = Language._Object.getCaption(CStr(Me.colhQualification.Tag), Me.colhQualification.Text)
            Me.btnDeleteJobQualification.Text = Language._Object.getCaption(Me.btnDeleteJobQualification.Name, Me.btnDeleteJobQualification.Text)
            Me.btnAddJobQualification.Text = Language._Object.getCaption(Me.btnAddJobQualification.Name, Me.btnAddJobQualification.Text)
            Me.lblQualification.Text = Language._Object.getCaption(Me.lblQualification.Name, Me.lblQualification.Text)
            Me.lblQualificationGroup.Text = Language._Object.getCaption(Me.lblQualificationGroup.Name, Me.lblQualificationGroup.Text)
            Me.tapbpJobDescription.Text = Language._Object.getCaption(Me.tapbpJobDescription.Name, Me.tapbpJobDescription.Text)
            Me.LblInDirectReportTo.Text = Language._Object.getCaption(Me.LblInDirectReportTo.Name, Me.LblInDirectReportTo.Text)
            Me.LblExperienceInfo.Text = Language._Object.getCaption(Me.LblExperienceInfo.Name, Me.LblExperienceInfo.Text)
            Me.LblExperienceMonth.Text = Language._Object.getCaption(Me.LblExperienceMonth.Name, Me.LblExperienceMonth.Text)
            Me.chkClassGroup.Text = Language._Object.getCaption(Me.chkClassGroup.Name, Me.chkClassGroup.Text)
            Me.chkDepartment.Text = Language._Object.getCaption(Me.chkDepartment.Name, Me.chkDepartment.Text)
            Me.tapbpKeyduties.Text = Language._Object.getCaption(Me.tapbpKeyduties.Name, Me.tapbpKeyduties.Text)
            Me.gbKeyDutiesResponsiblities.Text = Language._Object.getCaption(Me.gbKeyDutiesResponsiblities.Name, Me.gbKeyDutiesResponsiblities.Text)
            Me.btnKeyDutiesDelete.Text = Language._Object.getCaption(Me.btnKeyDutiesDelete.Name, Me.btnKeyDutiesDelete.Text)
            Me.btnKeyDutiesAdd.Text = Language._Object.getCaption(Me.btnKeyDutiesAdd.Name, Me.btnKeyDutiesAdd.Text)
            Me.tapbpCompetencies.Text = Language._Object.getCaption(Me.tapbpCompetencies.Name, Me.tapbpCompetencies.Text)
            Me.LblKeyDuties.Text = Language._Object.getCaption(Me.LblKeyDuties.Name, Me.LblKeyDuties.Text)
            Me.gbCompetencies.Text = Language._Object.getCaption(Me.gbCompetencies.Name, Me.gbCompetencies.Text)
            Me.colhCompetenciesGroup.Text = Language._Object.getCaption(CStr(Me.colhCompetenciesGroup.Tag), Me.colhCompetenciesGroup.Text)
            Me.colhCompetenciesItem.Text = Language._Object.getCaption(CStr(Me.colhCompetenciesItem.Tag), Me.colhCompetenciesItem.Text)
            Me.btnDeleteCompetencies.Text = Language._Object.getCaption(Me.btnDeleteCompetencies.Name, Me.btnDeleteCompetencies.Text)
            Me.btnAddCompetencies.Text = Language._Object.getCaption(Me.btnAddCompetencies.Name, Me.btnAddCompetencies.Text)
            Me.LblCompetenciesItem.Text = Language._Object.getCaption(Me.LblCompetenciesItem.Name, Me.LblCompetenciesItem.Text)
            Me.LblCompetenciesCategory.Text = Language._Object.getCaption(Me.LblCompetenciesCategory.Name, Me.LblCompetenciesCategory.Text)
            Me.tabpgExperienceComment.Text = Language._Object.getCaption(Me.tabpgExperienceComment.Name, Me.tabpgExperienceComment.Text)
            Me.tabpgWorkingHrs.Text = Language._Object.getCaption(Me.tabpgWorkingHrs.Name, Me.tabpgWorkingHrs.Text)
            Me.btnKeyDutiesEdit.Text = Language._Object.getCaption(Me.btnKeyDutiesEdit.Name, Me.btnKeyDutiesEdit.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.dgcolhKeyDuties.HeaderText = Language._Object.getCaption(Me.dgcolhKeyDuties.Name, Me.dgcolhKeyDuties.HeaderText)
            Me.ChkGradeLevel.Text = Language._Object.getCaption(Me.ChkGradeLevel.Name, Me.ChkGradeLevel.Text)
            Me.chkUnitGroup.Text = Language._Object.getCaption(Me.chkUnitGroup.Name, Me.chkUnitGroup.Text)
            Me.chkSectionGroup.Text = Language._Object.getCaption(Me.chkSectionGroup.Name, Me.chkSectionGroup.Text)
            Me.chkClass.Text = Language._Object.getCaption(Me.chkClass.Name, Me.chkClass.Text)
            Me.chkDepartmentGroup.Text = Language._Object.getCaption(Me.chkDepartmentGroup.Name, Me.chkDepartmentGroup.Text)
            Me.chkBranch.Text = Language._Object.getCaption(Me.chkBranch.Name, Me.chkBranch.Text)
            Me.lblcritical.Text = Language._Object.getCaption(Me.lblcritical.Name, Me.lblcritical.Text)
			Me.tabpJobLanguage.Text = Language._Object.getCaption(Me.tabpJobLanguage.Name, Me.tabpJobLanguage.Text)
			Me.gbJobLanguage.Text = Language._Object.getCaption(Me.gbJobLanguage.Name, Me.gbJobLanguage.Text)
			Me.colhLanguage.Text = Language._Object.getCaption(CStr(Me.colhLanguage.Tag), Me.colhLanguage.Text)
			Me.btnDeleteLanguage.Text = Language._Object.getCaption(Me.btnDeleteLanguage.Name, Me.btnDeleteLanguage.Text)
			Me.btnAddLanguage.Text = Language._Object.getCaption(Me.btnAddLanguage.Name, Me.btnAddLanguage.Text)
			Me.lblLanguage.Text = Language._Object.getCaption(Me.lblLanguage.Name, Me.lblLanguage.Text)
			Me.lblJobType.Text = Language._Object.getCaption(Me.lblJobType.Name, Me.lblJobType.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Job Name cannot be blank. Job Name is required information.")
            Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")
            Language.setMessage(mstrModuleName, 3, "Skill Category is compulsory information. Please Select Skill Category to continue.")
            Language.setMessage(mstrModuleName, 4, "Skill is compulsory information. Please Select Skill to continue.")
            Language.setMessage(mstrModuleName, 5, "Selected Skill is already added to the list.")
            Language.setMessage(mstrModuleName, 6, "Job Name cannot be same as Report to. Please select other Report to for this job.")
            Language.setMessage(mstrModuleName, 7, "Qualification Group is compulsory information. Please Select Qualification Group to continue.")
            Language.setMessage(mstrModuleName, 8, "Qualification is compulsory information. Please Select Qualification to continue.")
            Language.setMessage(mstrModuleName, 9, "Selected Qualification is already added to the list.")
            Language.setMessage(mstrModuleName, 10, "Key Duties and Responsibilities cannot be blank.Key Duties and Responsibilities is required information.")
            Language.setMessage(mstrModuleName, 11, "This Key Duties and Responsibilities already exists in this list.Please define new Key Duties and Responsibilities.")
            Language.setMessage(mstrModuleName, 12, "Please select Key Duties and Responsibilities from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 13, "Are you sure you want to delete this Key Duties and Responsibilities?")
            Language.setMessage(mstrModuleName, 14, "Competency Category is compulsory information.Please Select Competency Category.")
            Language.setMessage(mstrModuleName, 15, "Competency is compulsory information.Please Select Competency.")
            Language.setMessage(mstrModuleName, 16, "This Competency Category and Competency is already exist in this list.Please define new Competency Category or Competency.")
            Language.setMessage(mstrModuleName, 17, "Please select Competency from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 18, "Are you sure you want to delete this Competency?")
            Language.setMessage(mstrModuleName, 19, "Sorry, termination date cannot be less than created date for the job. Please set correct termiation date.")
			Language.setMessage(mstrModuleName, 20, "Language is compulsory information. Please Select Language to continue.")
			Language.setMessage(mstrModuleName, 21, "Selected Language is already added to the list.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

''************ SANDEEP { 14-MAR-2015 }  ********************************** CHANGES FROM RADIO TO CHECK BOX DUE TO EMPLOYEE INCREMENT AND DATES TRANSACTIION


'Public Class frmJobs_AddEdit1

'#Region " Private Variables "
'    Private ReadOnly mstrModuleName As String = "frmJobs_AddEdit"
'    Private mblnCancel As Boolean = True
'    Private objJobs As clsJobs
'    Private objJobSkill As clsJob_Skill_Tran
'    'S.SANDEEP [ 07 NOV 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private objJobQualification As clsJob_Qualification_Tran
'    Private mdtQTran As DataTable
'    'S.SANDEEP [ 07 NOV 2011 ] -- END
'    Private menAction As enAction = enAction.ADD_ONE
'    Private mintJobsUnkid As Integer = -1
'    Private mdtTran As DataTable
'    Private intSelectedIndex As Integer = -1
'#End Region

'#Region " Display Dialog "
'    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
'        Try
'            mintJobsUnkid = intUnkId
'            menAction = eAction

'            Me.ShowDialog()

'            intUnkId = mintJobsUnkid

'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try
'    End Function
'#End Region

'#Region " Private Methods "

'    Private Sub setColor()
'        Try
'            txtCode.BackColor = GUI.ColorComp
'            txtName.BackColor = GUI.ColorComp
'            txtJobDescription.BackColor = GUI.ColorOptional
'            cboGrade.BackColor = GUI.ColorOptional
'            cboJobGroup.BackColor = GUI.ColorOptional
'            'cboJobHead.BackColor = GUI.ColorOptional
'            cboReportTo.BackColor = GUI.ColorOptional
'            cboSection.BackColor = GUI.ColorOptional
'            cboUnit.BackColor = GUI.ColorOptional
'            nudPosition.BackColor = GUI.ColorComp
'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            cboTeam.BackColor = GUI.ColorOptional
'            'S.SANDEEP [ 07 NOV 2011 ] -- END

'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            cboQualificationGrp.BackColor = GUI.ColorOptional
'            cboQualification.BackColor = GUI.ColorOptional
'            'S.SANDEEP [ 07 NOV 2011 ] -- END
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetValue()
'        Try
'            txtCode.Text = objJobs._Job_Code
'            txtName.Text = objJobs._Job_Name

'            'S.SANDEEP [ 29 APR 2014 ] -- START
'            'txtJobDescription.Text = objJobs._Desciription
'            If objJobs._Desciription.StartsWith("{\rtf") Then
'                txtJobDescription.Rtf = objJobs._Desciription
'            Else
'                txtJobDescription.Text = objJobs._Desciription
'            End If
'            'S.SANDEEP [ 29 APR 2014 ] -- END

'            cboGrade.SelectedValue = objJobs._Jobgradeunkid
'            cboJobGroup.SelectedValue = objJobs._Jobgroupunkid
'            'cboJobHead.SelectedValue = objJobs._Jobheadunkid
'            cboReportTo.SelectedValue = objJobs._Report_Tounkid
'            cboSection.SelectedValue = objJobs._Jobsectionunkid
'            cboUnit.SelectedValue = objJobs._Jobunitunkid
'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            cboTeam.SelectedValue = objJobs._Teamunkid
'            'S.SANDEEP [ 07 NOV 2011 ] -- END

'            nudPosition.Value = objJobs._Total_Position
'            If objJobs._Create_Date <> Nothing Then
'                dtpCreateDate.Value = objJobs._Create_Date
'            Else
'                dtpCreateDate.Checked = False
'            End If

'            If objJobs._Terminate_Date <> Nothing Then
'                dtpTerminateDate.Value = objJobs._Terminate_Date
'            Else
'                dtpTerminateDate.Checked = False
'            End If

'            'Sandeep [ 21 Aug 2010 ] -- Start
'            nudJobLevel.Value = objJobs._Job_Level
'            'Sandeep [ 21 Aug 2010 ] -- End 


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetValue()
'        Try
'            objJobs._Job_Code = txtCode.Text
'            objJobs._Job_Name = txtName.Text
'            'S.SANDEEP [ 29 APR 2014 ] -- START
'            objJobs._Desciription = txtJobDescription.Rtf
'            'S.SANDEEP [ 29 APR 2014 ] -- END
'            objJobs._Jobgradeunkid = CInt(cboGrade.SelectedValue)
'            objJobs._Jobgroupunkid = CInt(cboJobGroup.SelectedValue)
'            'objJobs._Jobheadunkid = CInt(cboJobHead.SelectedValue)
'            objJobs._Report_Tounkid = CInt(cboReportTo.SelectedValue)
'            objJobs._Jobsectionunkid = CInt(cboSection.SelectedValue)
'            objJobs._Jobunitunkid = CInt(cboUnit.SelectedValue)
'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objJobs._Teamunkid = CInt(cboTeam.SelectedValue)
'            'S.SANDEEP [ 07 NOV 2011 ] -- END
'            objJobs._Total_Position = CInt(nudPosition.Value)
'            If dtpCreateDate.Checked = True Then
'                objJobs._Create_Date = dtpCreateDate.Value
'            Else
'                objJobs._Create_Date = Nothing
'            End If
'            If dtpTerminateDate.Checked = True Then
'                objJobs._Terminate_Date = dtpTerminateDate.Value
'            Else
'                objJobs._Terminate_Date = Nothing
'            End If

'            'Sandeep [ 21 Aug 2010 ] -- Start
'            objJobs._Job_Level = CInt(nudJobLevel.Value)
'            'Sandeep [ 21 Aug 2010 ] -- End 

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Dim objJobGrp As New clsJobGroup
'        Dim objGrade As New clsGrade
'        'Dim objJobs As New clsJobHead
'        'Dim objReportTo As New clsJobHead
'        Dim objSection As New clsSections
'        Dim objUnit As New clsUnits
'        Dim objCommom As New clsCommon_Master
'        Dim dsList As New DataSet
'        'S.SANDEEP [ 07 NOV 2011 ] -- START
'        'ENHANCEMENT : TRA CHANGES
'        Dim objTeam As New clsTeams
'        'S.SANDEEP [ 07 NOV 2011 ] -- END
'        Try

'            dsList = objJobGrp.getComboList("JobGrp", True)
'            With cboJobGroup
'                .ValueMember = "jobgroupunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("JobGrp")
'            End With
'            cboJobGroup.SelectedValue = 0

'            dsList = objGrade.getComboList("Grade", True)
'            With cboGrade
'                .ValueMember = "gradeunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("Grade")
'            End With
'            cboGrade.SelectedValue = 0

'            'dsList = objJobs.getComboList("JobHead", True)
'            'With cboJobHead
'            '    .ValueMember = "jobheadunkid"
'            '    .DisplayMember = "name"
'            '    .DataSource = dsList.Tables("JobHead")
'            'End With
'            'cboJobHead.SelectedValue = 0

'            dsList = objJobs.getComboList("ReportTo", True)
'            With cboReportTo
'                .ValueMember = "jobunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("ReportTo")
'            End With
'            cboReportTo.SelectedValue = 0

'            dsList = objSection.getComboList("Section", True)
'            With cboSection
'                .ValueMember = "sectionunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("Section")
'            End With
'            cboSection.SelectedValue = 0

'            dsList = objUnit.getComboList("Units", True)
'            With cboUnit
'                .ValueMember = "unitunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("Units")
'            End With
'            cboUnit.SelectedValue = 0

'            dsList = objCommom.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "Category")
'            With cboSkillCategory
'                .ValueMember = "masterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("Category")
'                .SelectedValue = 0
'            End With

'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            dsList = objTeam.getComboList("List", True)
'            With cboTeam
'                .ValueMember = "teamunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("List")
'                .SelectedValue = 0
'            End With
'            'S.SANDEEP [ 07 NOV 2011 ] -- END



'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            dsList = objCommom.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "QGrp")
'            With cboQualificationGrp
'                .ValueMember = "masterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("QGrp")
'                .SelectedValue = 0
'            End With
'            'S.SANDEEP [ 07 NOV 2011 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'            objJobGrp = Nothing
'            objGrade = Nothing
'            'objReportTo = Nothing
'            objSection = Nothing
'            objUnit = Nothing
'        End Try
'    End Sub

'    'S.SANDEEP [ 07 NOV 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public Sub FillQualificationList()
'        Try
'            lvQualification.Items.Clear()
'            Dim lvItem As ListViewItem

'            For Each dtRow As DataRow In mdtQTran.Rows
'                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
'                    lvItem = New ListViewItem

'                    lvItem.Text = dtRow.Item("QualificationGrp").ToString
'                    lvItem.SubItems.Add(dtRow.Item("Qualification").ToString)
'                    lvItem.SubItems.Add(dtRow.Item("qualificationunkid").ToString)
'                    lvItem.SubItems.Add(dtRow.Item("qualificationgroupunkid").ToString)
'                    lvItem.SubItems.Add(dtRow.Item("jobunkid").ToString)
'                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)

'                    lvItem.Tag = dtRow.Item("jobqualificationtranunkid").ToString

'                    lvQualification.Items.Add(lvItem)

'                End If
'            Next

'            If lvQualification.Items.Count > 5 Then
'                colhQualificationGrp.Width = 256 - 20
'            Else
'                colhQualificationGrp.Width = 256
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillQualificationList", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub ResetQualification()
'        Try
'            cboQualificationGrp.SelectedValue = 0
'            cboQualification.SelectedValue = 0
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ResetSkill", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 07 NOV 2011 ] -- END

'    Private Sub FillSkillList()
'        Try
'            lvJobSkill.Items.Clear()
'            Dim lvItem As ListViewItem

'            For Each dtRow As DataRow In mdtTran.Rows
'                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
'                    lvItem = New ListViewItem
'                    ' Skill Category
'                    Dim objComm_Mster As New clsCommon_Master
'                    objComm_Mster._Masterunkid = CInt(dtRow.Item("skillcategoryunkid"))
'                    lvItem.Text = objComm_Mster._Name.ToString
'                    objComm_Mster = Nothing

'                    ' Skill 
'                    Dim objSkill As New clsskill_master
'                    objSkill._Skillunkid = CInt(dtRow.Item("skillunkid"))
'                    lvItem.SubItems.Add(objSkill._Skillname.ToString)
'                    objSkill = Nothing

'                    lvItem.SubItems.Add(dtRow.Item("skillunkid").ToString)
'                    lvItem.SubItems.Add(dtRow.Item("skillcategoryunkid").ToString)
'                    lvItem.SubItems.Add(dtRow.Item("jobunkid").ToString)
'                    lvItem.SubItems.Add(dtRow.Item("GUID").ToString)

'                    lvItem.Tag = dtRow.Item("jobskilltranunkid")

'                    lvJobSkill.Items.Add(lvItem)

'                    lvItem = Nothing
'                End If
'            Next

'            '.Item("jobskilltranunkid") = -1
'            '.Item("jobunkid") = mintJobsUnkid
'            '.Item("skillcategoryunkid") = CInt(cboSkillCategory.SelectedValue)
'            '.Item("skillunkid") = CInt(cboSkillSets.SelectedValue)
'            '.Item("AUD") = "A"
'            '.Item("GUID") = Guid.NewGuid.ToString

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillSkillList", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub ResetSkill()
'        Try
'            cboSkillCategory.SelectedValue = 0
'            cboSkillSets.SelectedValue = 0
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ResetSkill", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetVisibility()

'        Try
'            objbtnAddJobGroup.Enabled = User._Object.Privilege._AddJobGroup
'            objbtnAddSection.Enabled = User._Object.Privilege._AddSection
'            objbtnAddSkill.Enabled = User._Object.Privilege._AddSkills
'            objbtnAddSkillCategory.Enabled = User._Object.Privilege._AddCommonMasters
'            objbtnAddUnit.Enabled = User._Object.Privilege._AddUnit
'            objbtnAddGrade.Enabled = User._Object.Privilege._AddGrade
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        End Try

'    End Sub

'#End Region

'#Region " Forms Events "

'    Private Sub frmJobs_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
'        objJobs = Nothing
'    End Sub

'    Private Sub frmJobs_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        Try
'            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
'                Call btnSave.PerformClick()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmJobs_AddEdit_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmJobs_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
'        Try
'            'If Asc(e.KeyChar) = 13 Then
'            '    Windows.Forms.SendKeys.Send("{Tab}")
'            '    e.Handled = True
'            'End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmJobs_AddEdit_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmJobs_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        objJobs = New clsJobs
'        objJobSkill = New clsJob_Skill_Tran
'        'S.SANDEEP [ 07 NOV 2011 ] -- START
'        'ENHANCEMENT : TRA CHANGES
'        objJobQualification = New clsJob_Qualification_Tran
'        'S.SANDEEP [ 07 NOV 2011 ] -- END
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            Call SetVisibility()
'            Call setColor()
'            Call FillCombo()

'            If menAction = enAction.EDIT_ONE Then
'                objJobs._Jobunkid = mintJobsUnkid
'            End If

'            Call GetValue()
'            'radJobHead.Enabled = False

'            objJobSkill._JobUnkid = mintJobsUnkid
'            mdtTran = objJobSkill._DataTable
'            Call FillSkillList()

'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objJobQualification._JobUnkid = mintJobsUnkid
'            mdtQTran = objJobQualification._DataTable
'            Call FillQualificationList()
'            'S.SANDEEP [ 07 NOV 2011 ] -- END

'            txtCode.Focus()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmJobs_AddEdit_Load", mstrModuleName)
'        End Try
'    End Sub

'    'S.SANDEEP [ 20 AUG 2011 ] -- START
'    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsJobs.SetMessages()
'            objfrm._Other_ModuleNames = "clsJobs"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub
'    'S.SANDEEP [ 20 AUG 2011 ] -- END

'#End Region

'#Region " Button's Events "

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim blnFlag As Boolean = False
'        Try
'            If Trim(txtCode.Text) = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
'                txtCode.Focus()
'                Exit Sub
'            End If

'            If Trim(txtName.Text) = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Job Name cannot be blank. Job Name is required information."), enMsgBoxStyle.Information) '?1
'                txtName.Focus()
'                Exit Sub
'            End If

'            'Sandeep [ 15 DEC 2010 ] -- Start
'            'Issue : Mr. Rutta's Comment
'            If txtName.Text.Trim = cboReportTo.Text Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Job Name cannot be same as Report to. Please select other Report to for this job."), enMsgBoxStyle.Information)
'                cboReportTo.Focus()
'                Exit Sub
'            End If
'            'Sandeep [ 15 DEC 2010 ] -- End 

'            Call SetValue()


'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If menAction = enAction.EDIT_ONE Then
'            '    blnFlag = objJobs.Update(mdtTran)
'            'Else
'            '    blnFlag = objJobs.Insert(mdtTran)
'            'End If
'            If menAction = enAction.EDIT_ONE Then
'                blnFlag = objJobs.Update(mdtTran, mdtQTran)
'            Else
'                blnFlag = objJobs.Insert(mdtTran, mdtQTran)
'            End If
'            'S.SANDEEP [ 07 NOV 2011 ] -- END


'            If blnFlag = False And objJobs._Message <> "" Then
'                eZeeMsgBox.Show(objJobs._Message, enMsgBoxStyle.Information)
'            End If

'            If blnFlag Then
'                mblnCancel = False
'                If menAction = enAction.ADD_CONTINUE Then
'                    objJobs = Nothing
'                    objJobs = New clsJobs
'                    Call GetValue()
'                    txtName.Focus()
'                    objJobSkill._JobUnkid = mintJobsUnkid
'                    mdtTran = objJobSkill._DataTable
'                    Call FillSkillList()

'                    objJobQualification._JobUnkid = mintJobsUnkid
'                    mdtQTran = objJobQualification._DataTable
'                    Call FillQualificationList()
'                Else
'                    mintJobsUnkid = objJobs._Jobunkid
'                    Me.Close()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
'        Dim objFrm As New NameLanguagePopup_Form
'        Try
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            If User._Object._Isrighttoleft = True Then
'                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objFrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objFrm)
'            End If
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            Call objFrm.displayDialog(txtName.Text, objJobs._Job_Name1, objJobs._Job_Name2)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Other Events "


'    'Sandeep [ 15 DEC 2010 ] -- Start
'    'Issue : Mr. Rutta's Comment
'    'Private Sub radJobGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radJobGroup.CheckedChanged, radSection.CheckedChanged, radGrade.CheckedChanged, radUnit.CheckedChanged
'    '    Try
'    '        If radJobGroup.Checked = True Then
'    '            cboJobGroup.Enabled = True
'    '            'cboJobGroup.SelectedValue = 0
'    '            cboGrade.Enabled = False
'    '            cboGrade.SelectedValue = 0
'    '            'cboJobHead.Enabled = False
'    '            'cboJobHead.SelectedValue = 0
'    '            cboSection.Enabled = False
'    '            cboSection.SelectedValue = 0
'    '            cboUnit.Enabled = False
'    '            cboUnit.SelectedValue = 0
'    '            'ElseIf radJobHead.Checked = True Then
'    '            'cboJobGroup.Enabled = False
'    '            'cboJobGroup.SelectedValue = 0
'    '            'cboGrade.Enabled = False
'    '            'cboGrade.SelectedValue = 0
'    '            'cboJobHead.Enabled = True
'    '            'cboJobHead.SelectedValue = 0
'    '            'cboSection.Enabled = False
'    '            'cboSection.SelectedValue = 0
'    '            'cboUnit.Enabled = False
'    '            'cboUnit.SelectedValue = 0
'    '        ElseIf radSection.Checked = True Then
'    '            cboJobGroup.Enabled = False
'    '            cboJobGroup.SelectedValue = 0
'    '            cboGrade.Enabled = False
'    '            cboGrade.SelectedValue = 0
'    '            'cboJobHead.Enabled = False
'    '            'cboJobHead.SelectedValue = 0
'    '            cboSection.Enabled = True
'    '            'cboSection.SelectedValue = 0
'    '            cboUnit.Enabled = False
'    '            cboUnit.SelectedValue = 0
'    '        ElseIf radGrade.Checked = True Then
'    '            cboJobGroup.Enabled = False
'    '            cboJobGroup.SelectedValue = 0
'    '            cboGrade.Enabled = True
'    '            'cboGrade.SelectedValue = 0
'    '            'cboJobHead.Enabled = False
'    '            'cboJobHead.SelectedValue = 0
'    '            cboSection.Enabled = False
'    '            cboSection.SelectedValue = 0
'    '            cboUnit.Enabled = False
'    '            cboUnit.SelectedValue = 0
'    '        ElseIf radUnit.Checked = True Then
'    '            cboJobGroup.Enabled = False
'    '            cboJobGroup.SelectedValue = 0
'    '            cboGrade.Enabled = False
'    '            cboGrade.SelectedValue = 0
'    '            'cboJobHead.Enabled = False
'    '            'cboJobHead.SelectedValue = 0
'    '            cboSection.Enabled = False
'    '            cboSection.SelectedValue = 0
'    '            cboUnit.Enabled = True
'    '            'cboUnit.SelectedValue = 0
'    '        End If
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "radJobGroup_CheckedChanged", mstrModuleName)
'    '    End Try
'    'End Sub
'    Private Sub radJobGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radJobGroup.CheckedChanged, radSection.CheckedChanged, radGrade.CheckedChanged, radUnit.CheckedChanged
'        Try
'            If radJobGroup.Checked = True Then
'                cboJobGroup.Enabled = True
'                cboGrade.Enabled = False
'                cboGrade.SelectedValue = 0
'                cboSection.Enabled = False
'                cboSection.SelectedValue = 0
'                cboUnit.Enabled = False
'                cboUnit.SelectedValue = 0
'                objbtnAddJobGroup.Enabled = True
'                objbtnAddUnit.Enabled = False
'                objbtnAddSection.Enabled = False
'                objbtnAddGrade.Enabled = False
'                'S.SANDEEP [ 07 NOV 2011 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                cboTeam.Enabled = False
'                objbtnAddTeam.Enabled = False
'                cboTeam.SelectedValue = 0
'                'S.SANDEEP [ 07 NOV 2011 ] -- END
'            ElseIf radSection.Checked = True Then
'                cboJobGroup.Enabled = False
'                cboJobGroup.SelectedValue = 0
'                cboGrade.Enabled = False
'                cboGrade.SelectedValue = 0
'                cboSection.Enabled = True
'                cboUnit.Enabled = False
'                cboUnit.SelectedValue = 0
'                objbtnAddJobGroup.Enabled = False
'                objbtnAddUnit.Enabled = False
'                objbtnAddSection.Enabled = True
'                objbtnAddGrade.Enabled = False
'                'S.SANDEEP [ 07 NOV 2011 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                cboTeam.Enabled = False
'                cboTeam.SelectedValue = 0
'                objbtnAddTeam.Enabled = False
'                'S.SANDEEP [ 07 NOV 2011 ] -- END
'            ElseIf radGrade.Checked = True Then
'                cboJobGroup.Enabled = False
'                cboJobGroup.SelectedValue = 0
'                cboGrade.Enabled = True
'                cboSection.Enabled = False
'                cboSection.SelectedValue = 0
'                cboUnit.Enabled = False
'                cboUnit.SelectedValue = 0
'                objbtnAddJobGroup.Enabled = False
'                objbtnAddUnit.Enabled = False
'                objbtnAddSection.Enabled = False
'                objbtnAddGrade.Enabled = True
'                'S.SANDEEP [ 07 NOV 2011 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                cboTeam.Enabled = False
'                objbtnAddTeam.Enabled = False
'                cboTeam.SelectedValue = 0
'                'S.SANDEEP [ 07 NOV 2011 ] -- END
'            ElseIf radUnit.Checked = True Then
'                cboJobGroup.Enabled = False
'                cboJobGroup.SelectedValue = 0
'                cboGrade.Enabled = False
'                cboGrade.SelectedValue = 0
'                cboSection.Enabled = False
'                cboSection.SelectedValue = 0
'                cboUnit.Enabled = True
'                objbtnAddJobGroup.Enabled = False
'                objbtnAddUnit.Enabled = True
'                objbtnAddSection.Enabled = False
'                objbtnAddGrade.Enabled = False
'                'S.SANDEEP [ 07 NOV 2011 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                cboTeam.Enabled = False
'                objbtnAddTeam.Enabled = False
'                cboTeam.SelectedValue = 0
'            ElseIf radTeam.Checked = True Then
'                cboJobGroup.Enabled = False
'                cboJobGroup.SelectedValue = 0
'                cboGrade.Enabled = False
'                cboGrade.SelectedValue = 0
'                cboSection.Enabled = False
'                cboSection.SelectedValue = 0
'                cboUnit.Enabled = False
'                objbtnAddJobGroup.Enabled = False
'                objbtnAddUnit.Enabled = False
'                objbtnAddSection.Enabled = False
'                objbtnAddGrade.Enabled = False
'                cboTeam.Enabled = True
'                objbtnAddTeam.Enabled = True
'                'S.SANDEEP [ 07 NOV 2011 ] -- END
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "radJobGroup_CheckedChanged", mstrModuleName)
'        End Try
'    End Sub
'    'Sandeep [ 15 DEC 2010 ] -- End 



'    'Private Sub objbtnAddReminder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReminder.Click
'    '    Try
'    '        Dim frm As New frmRemider_AddEdit
'    '        frm._TitleName = Language.getMessage(mstrModuleName, 6, "Job Termination Date")
'    '        frm._StartDate = dtpTerminateDate.Value
'    '        frm.displayDialog(-1, enAction.ADD_ONE)
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'    '    End Try
'    'End Sub

'    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
'        Try
'            If CInt(cboGrade.SelectedValue) > 0 Then
'                radGrade.Checked = True
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboSection_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSection.SelectedIndexChanged
'        Try
'            If CInt(cboSection.SelectedValue) > 0 Then
'                radSection.Checked = True
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboSection_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboUnit_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnit.SelectedIndexChanged
'        Try
'            If CInt(cboUnit.SelectedValue) > 0 Then
'                radUnit.Checked = True
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboUnit_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboJobGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJobGroup.SelectedIndexChanged
'        Try
'            If CInt(cboJobGroup.SelectedValue) > 0 Then
'                radJobGroup.Checked = True
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboJobGroup_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboSkillCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSkillCategory.SelectedIndexChanged
'        Try
'            If CInt(cboSkillCategory.SelectedValue) > 0 Then
'                Dim objSkill As New clsskill_master
'                Dim dsList As New DataSet
'                dsList = objSkill.getComboList("Skills", True, CInt(cboSkillCategory.SelectedValue))
'                With cboSkillSets
'                    .ValueMember = "skillunkid"
'                    .DisplayMember = "NAME"
'                    .DataSource = dsList.Tables("Skills")
'                    .SelectedValue = 0
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboSkillCategory_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchSkill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchSkill.Click
'        Dim objfrm As New frmCommonSearch
'        Try
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            With objfrm
'                .ValueMember = cboSkillSets.ValueMember
'                .DisplayMember = cboSkillSets.DisplayMember
'                .DataSource = CType(cboSkillSets.DataSource, DataTable)
'                .CodeMember = ""
'            End With
'            If objfrm.DisplayDialog Then
'                cboSkillSets.SelectedValue = objfrm.SelectedValue
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCategory.Click
'        Dim objfrm As New frmCommonSearch
'        Try
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            With objfrm
'                .ValueMember = cboSkillCategory.ValueMember
'                .DisplayMember = cboSkillCategory.DisplayMember
'                .DataSource = CType(cboSkillCategory.DataSource, DataTable)
'                .CodeMember = ""
'            End With
'            If objfrm.DisplayDialog Then
'                cboSkillCategory.SelectedValue = objfrm.SelectedValue
'                cboSkillCategory.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchCategory_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnAddSkill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSkill.Click
'        Dim frm As New frmSkill_AddEdit
'        Dim intRefId As Integer = -1
'        Dim objSkill As New clsskill_master
'        Dim dsList As New DataSet
'        Try
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
'                dsList = objSkill.getComboList("Skills", True, CInt(cboSkillCategory.SelectedValue))
'                With cboSkillSets
'                    .ValueMember = "skillunkid"
'                    .DisplayMember = "NAME"
'                    .DataSource = dsList.Tables("Skills")
'                    .SelectedValue = intRefId
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddSkill_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnAddSkillCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSkillCategory.Click
'        Dim frm As New frmCommonMaster
'        Dim intRefId As Integer = -1
'        Dim objComm_Master As New clsCommon_Master
'        Dim dsList As New DataSet
'        Try
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.SKILL_CATEGORY, enAction.ADD_ONE)
'            dsList = objComm_Master.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "Category")
'            With cboSkillCategory
'                .ValueMember = "masterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("Category")
'                .SelectedValue = intRefId
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddSkillCategory_Click", mstrModuleName)
'        End Try
'    End Sub

'    'Sandeep [ 15 DEC 2010 ] -- Start
'    'Issue : Mr. Rutta's Comment
'    Private Sub objbtnAddSection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSection.Click
'        Dim frm As New frmSections_AddEdit
'        Dim intRefId As Integer = -1
'        Try
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            frm.displayDialog(intRefId, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objSec As New clsSections
'                dsList = objSec.getComboList("Section", True)
'                With cboSection
'                    .ValueMember = "sectionunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Section")
'                    .SelectedValue = intRefId
'                End With
'                dsList.Dispose()
'                objSec = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddSection_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnAddGrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGrade.Click
'        Dim frm As New frmGrade_AddEdit
'        Dim intRefId As Integer = -1
'        Try
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            frm.displayDialog(intRefId, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objGrade As New clsGrade
'                dsList = objGrade.getComboList("Grade", True)
'                With cboGrade
'                    .ValueMember = "gradeunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Grade")
'                    .SelectedValue = intRefId
'                End With
'                dsList.Dispose()
'                objGrade = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddGrade_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnAddUnit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddUnit.Click
'        Dim frm As New frmUnits_AddEdit
'        Dim intRefId As Integer = -1
'        Try
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            frm.displayDialog(intRefId, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objUnit As New clsUnits
'                dsList = objUnit.getComboList("Unit", True)
'                With cboUnit
'                    .ValueMember = "unitunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Unit")
'                End With
'                dsList.Dispose()
'                objUnit = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddUnit_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnAddJobGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddJobGroup.Click
'        Dim frm As New frmJobGroup_AddEdit
'        Dim intRefId As Integer = -1
'        Try
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            frm.displayDialog(intRefId, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objGroup As New clsJobGroup
'                dsList = objGroup.getComboList("Group", True)
'                With cboJobGroup
'                    .ValueMember = "jobgroupunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Group")
'                    .SelectedValue = intRefId
'                End With
'                dsList.Dispose()
'                objGroup = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddJobGroup_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub
'    'Sandeep [ 15 DEC 2010 ] -- End 


'    'S.SANDEEP [ 07 NOV 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub cboTeam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTeam.SelectedIndexChanged
'        Try
'            If CInt(cboTeam.SelectedValue) > 0 Then
'                radTeam.Checked = True
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboTeam_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnAddTeam_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddTeam.Click
'        Dim frm As New frmTeams_AddEdit
'        Dim intRefId As Integer = -1
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            frm.displayDialog(intRefId, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objTeams As New clsTeams
'                dsList = objTeams.getComboList("Team", True)
'                With cboTeam
'                    .ValueMember = "teamunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Team")
'                    .SelectedValue = intRefId
'                End With
'                dsList.Dispose()
'                objTeams = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddTeam_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 07 NOV 2011 ] -- END

'    'Sandeep [ 30 September 2010 ] -- Start
'    Private Sub objbtnAddReminder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddReminder.Click
'        Try
'            Dim objRemider As New frmReminder_AddEdit
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            If User._Object._Isrighttoleft = True Then
'                objRemider.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objRemider.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objRemider)
'            End If
'            'S.SANDEEP [ 20 AUG 2011 ] -- END
'            objRemider.displayDialog(-1, enAction.ADD_ONE)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddReminder_Click", mstrModuleName)
'        End Try
'    End Sub
'    'Sandeep [ 30 September 2010 ] -- End

'    'S.SANDEEP [ 07 NOV 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub cboQualificationGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQualificationGrp.SelectedIndexChanged
'        Try
'            If CInt(cboQualificationGrp.SelectedValue) > 0 Then
'                Dim objQualification As New clsqualification_master
'                Dim dsList As New DataSet
'                dsList = objQualification.GetComboList("Qualification", True, CInt(cboQualificationGrp.SelectedValue))
'                With cboQualification
'                    .ValueMember = "Id"
'                    .DisplayMember = "Name"
'                    .DataSource = dsList.Tables("Qualification")
'                    .SelectedValue = 0
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboQualificationGrp_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchQGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchQGrp.Click
'        Dim objfrm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If
'            With objfrm
'                .ValueMember = cboQualificationGrp.ValueMember
'                .DisplayMember = cboQualificationGrp.DisplayMember
'                .DataSource = CType(cboQualificationGrp.DataSource, DataTable)
'                .CodeMember = ""
'            End With
'            If objfrm.DisplayDialog Then
'                cboQualificationGrp.SelectedValue = objfrm.SelectedValue
'                cboQualificationGrp.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchQGrp_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchQualification.Click
'        Dim objfrm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            With objfrm
'                .ValueMember = cboQualification.ValueMember
'                .DisplayMember = cboQualification.DisplayMember
'                .DataSource = CType(cboQualification.DataSource, DataTable)
'                .CodeMember = ""
'            End With
'            If objfrm.DisplayDialog Then
'                cboQualification.SelectedValue = objfrm.SelectedValue
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchQualification_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnAddQGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddQGroup.Click
'        Dim frm As New frmCommonMaster
'        Dim intRefId As Integer = -1
'        Dim objComm_Master As New clsCommon_Master
'        Dim dsList As New DataSet
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, enAction.ADD_ONE)
'            dsList = objComm_Master.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "QGrp")
'            With cboSkillCategory
'                .ValueMember = "masterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("QGrp")
'                .SelectedValue = intRefId
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddQGroup_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnAddQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddQualification.Click
'        Dim frm As New frmQualificationCourse_AddEdit
'        Dim intRefId As Integer = -1
'        Dim objQualification As New clsqualification_master
'        Dim dsList As New DataSet
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If
'            If frm.displayDialog(intRefId, enAction.ADD_ONE) Then
'                dsList = objQualification.GetComboList("Qualification", True, CInt(cboQualificationGrp.SelectedValue))
'                With cboSkillSets
'                    .ValueMember = "Id"
'                    .DisplayMember = "Name"
'                    .DataSource = dsList.Tables("Qualification")
'                    .SelectedValue = intRefId
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddQualification_Click", mstrModuleName)
'        End Try
'    End Sub
'    'S.SANDEEP [ 07 NOV 2011 ] -- END

'    'S.SANDEEP [ 27 FEB 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub objbtnSearchJobCode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJobCode.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If CType(cboReportTo.DataSource, DataTable) Is Nothing Then Exit Sub
'            With frm
'                .ValueMember = cboReportTo.ValueMember
'                .DisplayMember = cboReportTo.DisplayMember
'                .DataSource = CType(cboReportTo.DataSource, DataTable)
'            End With
'            If frm.DisplayDialog Then
'                cboReportTo.SelectedValue = frm.SelectedValue
'                cboReportTo.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchJobCode_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub
'    'S.SANDEEP [ 27 FEB 2012 ] -- END

'#End Region

'#Region " Job Skill "
'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        Try
'            If intSelectedIndex > -1 Then
'                Dim drTemp As DataRow()
'                If CInt(lvJobSkill.Items(intSelectedIndex).Tag) = -1 Then
'                    drTemp = mdtTran.Select("GUID = '" & lvJobSkill.Items(intSelectedIndex).SubItems(objcolhGUID.Index).Text & "'")
'                Else
'                    drTemp = mdtTran.Select("jobskilltranunkid = '" & CInt(lvJobSkill.Items(intSelectedIndex).Tag) & "'")
'                End If
'                If drTemp.Length > 0 Then
'                    drTemp(0).Item("AUD") = "D"
'                    Call FillSkillList()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
'        Try
'            If CInt(cboSkillCategory.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Skill Category is compulsory information. Please Select Skill Category to continue."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            If CInt(cboSkillSets.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Skill is compulsory information. Please Select Skill to continue."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            Dim dtRow As DataRow() = mdtTran.Select("skillunkid = " & CInt(cboSkillSets.SelectedValue) & " AND AUD <> 'D' ")

'            If dtRow.Length > 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Selected Skill is already added to the list."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            Dim dtSRow As DataRow
'            dtSRow = mdtTran.NewRow

'            With dtSRow
'                .Item("jobskilltranunkid") = -1
'                .Item("jobunkid") = mintJobsUnkid
'                .Item("skillcategoryunkid") = CInt(cboSkillCategory.SelectedValue)
'                .Item("skillunkid") = CInt(cboSkillSets.SelectedValue)
'                .Item("AUD") = "A"
'                .Item("GUID") = Guid.NewGuid.ToString
'            End With

'            mdtTran.Rows.Add(dtSRow)

'            Call FillSkillList()
'            Call ResetSkill()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub lvJobSkill_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvJobSkill.SelectedIndexChanged, lvJobSkill.Click
'        Try
'            If lvJobSkill.SelectedItems.Count > 0 Then
'                intSelectedIndex = CInt(lvJobSkill.SelectedItems(0).Index)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvJobSkill_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'    'S.SANDEEP [ 07 NOV 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'#Region " Job Qualification "

'    Private Sub btnDeleteJobQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteJobQualification.Click
'        Try
'            If lvQualification.SelectedItems.Count > 0 Then
'                Dim drTemp As DataRow()
'                If CInt(lvQualification.SelectedItems(0).Tag) = -1 Then
'                    drTemp = mdtQTran.Select("GUID = '" & lvQualification.SelectedItems(0).SubItems(objcolhQGUID.Index).Text & "'")
'                Else
'                    drTemp = mdtQTran.Select("jobqualificationtranunkid = '" & CInt(lvQualification.SelectedItems(0).Tag) & "'")
'                End If
'                If drTemp.Length > 0 Then
'                    drTemp(0).Item("AUD") = "D"
'                    Call FillQualificationList()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnDeleteJobQualification_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnAddJobQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddJobQualification.Click
'        Try
'            If CInt(cboQualificationGrp.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Qualification Group is compulsory information. Please Select Qualification Group to continue."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            If CInt(cboQualification.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Qualification is compulsory information. Please Select Qualification to continue."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            Dim dtRow As DataRow() = mdtQTran.Select("qualificationunkid = " & CInt(cboQualification.SelectedValue) & " AND AUD <> 'D' ")

'            If dtRow.Length > 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Selected Qualification is already added to the list."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            Dim dtQRow As DataRow
'            dtQRow = mdtQTran.NewRow

'            dtQRow.Item("jobqualificationtranunkid") = -1
'            dtQRow.Item("jobunkid") = mintJobsUnkid
'            dtQRow.Item("qualificationgroupunkid") = cboQualificationGrp.SelectedValue
'            dtQRow.Item("qualificationunkid") = cboQualification.SelectedValue
'            dtQRow.Item("QualificationGrp") = cboQualificationGrp.Text
'            dtQRow.Item("Qualification") = cboQualification.Text
'            dtQRow.Item("AUD") = "A"
'            dtQRow.Item("GUID") = Guid.NewGuid.ToString

'            mdtQTran.Rows.Add(dtQRow)

'            Call FillQualificationList()
'            Call ResetQualification()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnAddJobQualification_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region
'    'S.SANDEEP [ 07 NOV 2011 ] -- END



'End Class


''************ SANDEEP { 14-MAR-2015 }  ********************************** CHANGES FROM RADIO TO CHECK BOX DUE TO EMPLOYEE INCREMENT AND DATES TRANSACTIION