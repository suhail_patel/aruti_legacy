﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmGradeLevel_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmGradeLevel_AddEdit"
    Private mblnCancel As Boolean = True
    Private objGradeLevel As clsGradeLevel
    Private menAction As enAction = enAction.ADD_ONE
    Private mintGradeLevelUnkid As Integer = -1
    Private objGradeGroup As clsGradeGroup
    Private objGrade As clsGrade
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintGradeLevelUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintGradeLevelUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            cboGrade.BackColor = GUI.ColorComp
            cboGradeGroup.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objGradeLevel._Code
        txtName.Text = objGradeLevel._Name
        txtDescription.Text = objGradeLevel._Description

        objGrade._Gradeunkid = objGradeLevel._Gradeunkid
        objGradeGroup._Gradegroupunkid = objGrade._Gradegroupunkid
        cboGradeGroup.SelectedValue = objGradeGroup._Gradegroupunkid
        cboGrade.SelectedValue = objGradeLevel._Gradeunkid
    End Sub

    Private Sub SetValue()
        objGradeLevel._Code = txtCode.Text
        objGradeLevel._Name = txtName.Text
        objGradeLevel._Description = txtDescription.Text
        objGradeLevel._Gradeunkid = CInt(cboGrade.SelectedValue)
        'S.SANDEEP [ 14 OCT 2014 ] -- START
        objGradeLevel._GradeGroupunkid = CInt(cboGradeGroup.SelectedValue)
        'S.SANDEEP [ 14 OCT 2014 ] -- END
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Try
            dsCombos = objGradeGroup.getComboList("GradeGroup", True)
            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("GradeGroup")
            End With
            cboGradeGroup.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            objbtnAddGradeGroup.Enabled = User._Object.Privilege._AddGradeGroup
            objbtnAddGrade.Enabled = User._Object.Privilege._AddGrade
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "
    Private Sub frmDepartment_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objGradeLevel = Nothing
    End Sub

    Private Sub frmGradeLevel_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGradeLevel_AddEdit_KeyDown", mstrModuleName)
        End Try

    End Sub

    Private Sub frmGradeLevel_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGradeLevel_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDepartment_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objGradeLevel = New clsGradeLevel
        objGradeGroup = New clsGradeGroup
        objGrade = New clsGrade
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            Call setColor()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objGradeLevel._Gradelevelunkid = mintGradeLevelUnkid
                'Nilay (02-Jul-2016) -- Start
                Dim blnFlag As Boolean = False
                blnFlag = objGradeLevel.isUsed(mintGradeLevelUnkid)
                cboGradeGroup.Enabled = Not blnFlag
                objbtnAddGradeGroup.Enabled = Not blnFlag
                cboGrade.Enabled = Not blnFlag
                objbtnAddGrade.Enabled = Not blnFlag
                'Nilay (02-Jul-2016) -- End
            End If

            Call GetValue()

            cboGradeGroup.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDepartment_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsGradeLevel.SetMessages()
            objfrm._Other_ModuleNames = "clsGradeLevel"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Grade Level cannot be blank. Grade Level is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            If CInt(cboGradeGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Grade Group cannot be blank. Grade Group is required information."), enMsgBoxStyle.Information) '?1
                cboGradeGroup.Focus()
                Exit Sub
            End If

            If CInt(cboGrade.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Grade cannot be blank. Grade is required information."), enMsgBoxStyle.Information) '?1
                cboGrade.Focus()
                Exit Sub
            End If


            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objGradeLevel._FormName = mstrModuleName
            objGradeLevel._LoginEmployeeunkid = 0
            objGradeLevel._ClientIP = getIP()
            objGradeLevel._HostName = getHostName()
            objGradeLevel._FromWeb = False
            objGradeLevel._AuditUserId = User._Object._Userunkid
objGradeLevel._CompanyUnkid = Company._Object._Companyunkid
            objGradeLevel._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objGradeLevel.Update()
            Else
                blnFlag = objGradeLevel.Insert()
            End If

            If blnFlag = False And objGradeLevel._Message <> "" Then
                eZeeMsgBox.Show(objGradeLevel._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objGradeLevel = Nothing
                    objGradeLevel = New clsGradeLevel
                    Call GetValue()
                    cboGradeGroup.Select()
                Else
                    mintGradeLevelUnkid = objGradeLevel._Gradelevelunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call objFrm.displayDialog(txtName.Text, objGradeLevel._Name1, objGradeLevel._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGradeGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGradeGroup.Click
        Try
            Dim objfrmGradeGroup_AddEdit As New frmGradeGroup_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCombos As New DataSet
            Dim objGradeGroup As New clsGradeGroup
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmGradeGroup_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmGradeGroup_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmGradeGroup_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If objfrmGradeGroup_AddEdit.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsCombos = objGradeGroup.getComboList("List", True)
                With cboGradeGroup
                    .ValueMember = "gradegroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
                cboGradeGroup.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGradeGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddGrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGrade.Click
        Try
            Dim objfrmGrade_AddEdit As New frmGrade_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCombos As New DataSet
            Dim objGrade As New clsGrade
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrmGrade_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrmGrade_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrmGrade_AddEdit)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If objfrmGrade_AddEdit.displayDialog(intRefId, enAction.ADD_ONE) Then
                dsCombos = objGrade.getComboList("List", True)
                With cboGrade
                    .ValueMember = "gradeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
                cboGrade.SelectedValue = intRefId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGrade_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Other Region "
    Private Sub cboGradeGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeGroup.SelectedIndexChanged
        Try
            Dim dsCombos As New DataSet
            If CInt(cboGradeGroup.SelectedValue) > 0 Then
                dsCombos = objGrade.getComboList("GradeList", True, CInt(cboGradeGroup.SelectedValue))
                With cboGrade
                    .ValueMember = "gradeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("GradeList")
                End With
                cboGrade.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("", ex.Message, "cboGradeGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbGradeLevel.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbGradeLevel.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbGradeLevel.Text = Language._Object.getCaption(Me.gbGradeLevel.Name, Me.gbGradeLevel.Text)
			Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Grade Level cannot be blank. Grade Level is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Grade cannot be blank. Grade is required information.")
			Language.setMessage(mstrModuleName, 4, "Grade Group cannot be blank. Grade Group is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class