﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmAssignGradePriority

#Region " Private Varaibles "
    Private ReadOnly mstrModuleName As String = "frmAssignGradePriority"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private marrOldPriority() As String
    Private mdtOldPriority As DataTable
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction

            Me.ShowDialog()


            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombos As DataSet
        Dim objGradeGroup As New clsGradeGroup
        Try
            dsCombos = objGradeGroup.getComboList("GradeGroup", True)
            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("GradeGroup")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objGradeGroup = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet
        Dim strSearching As String = ""
        Dim dtDeptTable As New DataTable
        Try

            If User._Object.Privilege._AllowToViewGradeLevelList = True Then

                dgvGradePriority.AutoGenerateColumns = False
                colhPriority.DataPropertyName = "priority"

                colhGradeGroupCode.Visible = False
                colhGradeGroupName.Visible = False

                colhGradeCode.Visible = False
                colhGradeName.Visible = False

                colhGradeLevelCode.Visible = False
                colhGradeLevelName.Visible = False

                If CInt(cboGradeGroup.SelectedValue) <= 0 Then

                    btnSave.Enabled = User._Object.Privilege._EditGradeGroup

                    Dim objGradeGroup As New clsGradeGroup
                    dsList = objGradeGroup.GetList("Grade")

                    objcolhGradeGroupUnkId.DataPropertyName = "gradegroupunkid"
                    colhGradeGroupCode.DataPropertyName = "code"
                    colhGradeGroupName.DataPropertyName = "name"
                    colhGradeGroupName.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

                    objcolhGradeId.DataPropertyName = ""
                    colhGradeCode.DataPropertyName = ""
                    colhGradeName.DataPropertyName = ""

                    objcolhGradeLevelId.DataPropertyName = ""
                    colhGradeLevelCode.DataPropertyName = ""
                    colhGradeLevelName.DataPropertyName = ""

                    colhGradeGroupCode.Visible = True
                    colhGradeGroupName.Visible = True

                ElseIf CInt(cboGrade.SelectedValue) <= 0 Then

                    btnSave.Enabled = User._Object.Privilege._EditGrade

                    Dim objGrade As New clsGrade
                    dsList = objGrade.GetList("Grade", , " hrgrade_master.gradegroupunkid = " & CInt(cboGradeGroup.SelectedValue) & " ")

                    objcolhGradeGroupUnkId.DataPropertyName = "gradegroupunkid"
                    colhGradeGroupCode.DataPropertyName = ""
                    colhGradeGroupName.DataPropertyName = ""

                    objcolhGradeId.DataPropertyName = "gradeunkid"
                    colhGradeCode.DataPropertyName = "code"
                    colhGradeName.DataPropertyName = "name"
                    colhGradeName.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

                    objcolhGradeLevelId.DataPropertyName = ""
                    colhGradeLevelCode.DataPropertyName = ""
                    colhGradeLevelName.DataPropertyName = ""

                    colhGradeCode.Visible = True
                    colhGradeName.Visible = True

                Else

                    btnSave.Enabled = User._Object.Privilege._EditGradeLevel

                    Dim objGradeLevel As New clsGradeLevel
                    dsList = objGradeLevel.GetList("Grade", , " hrgradelevel_master.gradeunkid = " & CInt(cboGrade.SelectedValue) & " ")

                    objcolhGradeGroupUnkId.DataPropertyName = ""
                    colhGradeGroupCode.DataPropertyName = ""
                    colhGradeGroupName.DataPropertyName = ""

                    objcolhGradeId.DataPropertyName = "gradeunkid"
                    colhGradeCode.DataPropertyName = ""
                    colhGradeName.DataPropertyName = ""

                    objcolhGradeLevelId.DataPropertyName = "gradelevelunkid"
                    colhGradeLevelCode.DataPropertyName = "code"
                    colhGradeLevelName.DataPropertyName = "name"
                    colhGradeLevelName.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

                    colhGradeLevelCode.Visible = True
                    colhGradeLevelName.Visible = True

                End If

                dgvGradePriority.DataSource = dsList.Tables("Grade")

                marrOldPriority = (From p In CType(dgvGradePriority.DataSource, DataTable) Select (p.Item("priority").ToString)).ToArray
                mdtOldPriority = dsList.Tables("Grade").Copy

            End If


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Function IsValidated() As Boolean
        Try
            If dgvGradePriority.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, There is no transaction to Save."), enMsgBoxStyle.Information)
                Return False
            Else

                Dim arrCurrPriority() As String = (From p In CType(dgvGradePriority.DataSource, DataTable) Select (p.Item("priority").ToString)).ToArray
                If marrOldPriority.SequenceEqual(arrCurrPriority) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please change any Priority to Save."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidated", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmAssignGradePriority_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'Call SetVisibility()

            Call FillCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssignGradePriority_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssignGradePriority_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsGradeGroup.SetMessages()
            objfrm._Other_ModuleNames = "clsGradeGroup"

            clsGrade.SetMessages()
            objfrm._Other_ModuleNames = "clsGrade"

            clsGradeLevel.SetMessages()
            objfrm._Other_ModuleNames = "clsGradeLevel"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Combobox's Events "

    Private Sub cboGradeGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGradeGroup.SelectedIndexChanged
        Dim dsCombos As DataSet
        Dim objGrade As New clsGrade
        Dim strFilter As String = ""

        Try
            If CInt(cboGradeGroup.SelectedValue) <= 0 Then
                strFilter = " 1 = 2 "

            End If

            dsCombos = objGrade.getComboList("GradeList", True, CInt(cboGradeGroup.SelectedValue), strFilter)
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("GradeList")
                .SelectedValue = 0
            End With

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeGroup_SelectedIndexChanged", mstrModuleName)
        Finally
            objGrade = Nothing
        End Try
    End Sub

    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Try

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChangeds", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid's Event "

    Private Sub dgvGradePriority_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvGradePriority.CellValidating
        Try
            If e.RowIndex = -1 Then Exit Sub

            If e.ColumnIndex = colhPriority.Index Then
                Dim i As Integer = 0
                If Integer.TryParse(e.FormattedValue.ToString, i) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please enter proper Priority."), enMsgBoxStyle.Information)
                    e.Cancel = True
                ElseIf i < 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Priority should be greater than Zero."), enMsgBoxStyle.Information)
                    e.Cancel = True
                Else
                    If CInt(dgvGradePriority.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) <> CInt(e.FormattedValue) Then
                        Dim cnt As Integer = (From p In CType(dgvGradePriority.DataSource, DataTable) Where (CInt(e.FormattedValue) > 0 AndAlso CInt(p.Item("priority")) = CInt(e.FormattedValue)) Select (p)).Count
                        If cnt > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, This Priority is already assigned."), enMsgBoxStyle.Information)
                            e.Cancel = True
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvGradePriority_CellValidating", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvGradePriority_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvGradePriority.DataError

    End Sub

    Private Sub dgvGradePriority_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvGradePriority.EditingControlShowing
        Try
            If dgvGradePriority.CurrentCell.ColumnIndex = colhPriority.Index AndAlso TypeOf e.Control Is TextBox Then
                Dim tb As TextBox
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
                AddHandler tb.KeyPress, AddressOf tb_keypress
            Else
                Dim tb As TextBox
                tb = CType(e.Control, TextBox)
                RemoveHandler tb.KeyPress, AddressOf tb_keypress
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvGradePriority_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValidated() = False Then Exit Sub

            Dim dtOld As DataTable = mdtOldPriority.Copy
            dtOld.Merge(CType(dgvGradePriority.DataSource, DataTable), False)
            Dim dtDiff As DataTable
            dtDiff = dtOld.GetChanges()

            If CInt(cboGradeGroup.SelectedValue) <= 0 Then
                Dim objGradeGroup As New clsGradeGroup

                With objGradeGroup
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With
                blnFlag = objGradeGroup.UpdatePriority(dtDiff)

                If blnFlag = False And objGradeGroup._Message <> "" Then
                    eZeeMsgBox.Show(objGradeGroup._Message, enMsgBoxStyle.Information)
                End If

            ElseIf CInt(cboGrade.SelectedValue) <= 0 Then
                Dim objGrade As New clsGrade

                With objGrade
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With

                blnFlag = objGrade.UpdatePriority(dtDiff)

                If blnFlag = False And objGrade._Message <> "" Then
                    eZeeMsgBox.Show(objGrade._Message, enMsgBoxStyle.Information)
                End If

            Else
                Dim objGradeLevel As New clsGradeLevel

                With objGradeLevel
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With

                blnFlag = objGradeLevel.UpdatePriority(dtDiff)

                If blnFlag = False And objGradeLevel._Message <> "" Then
                    eZeeMsgBox.Show(objGradeLevel._Message, enMsgBoxStyle.Information)
                End If

            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    cboGradeGroup.SelectedValue = 0
                    cboGrade.SelectedValue = 0

                    Call FillList()
                Else
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboGradeGroup.SelectedValue = 0
            cboGrade.SelectedValue = 0

            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    
    

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
			Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblMessage1.Text = Language._Object.getCaption(Me.lblMessage1.Name, Me.lblMessage1.Text)
			Me.lblMessage2.Text = Language._Object.getCaption(Me.lblMessage2.Name, Me.lblMessage2.Text)
			Me.colhGradeGroupCode.HeaderText = Language._Object.getCaption(Me.colhGradeGroupCode.Name, Me.colhGradeGroupCode.HeaderText)
			Me.colhGradeGroupName.HeaderText = Language._Object.getCaption(Me.colhGradeGroupName.Name, Me.colhGradeGroupName.HeaderText)
			Me.colhGradeCode.HeaderText = Language._Object.getCaption(Me.colhGradeCode.Name, Me.colhGradeCode.HeaderText)
			Me.colhGradeName.HeaderText = Language._Object.getCaption(Me.colhGradeName.Name, Me.colhGradeName.HeaderText)
			Me.colhGradeLevelCode.HeaderText = Language._Object.getCaption(Me.colhGradeLevelCode.Name, Me.colhGradeLevelCode.HeaderText)
			Me.colhGradeLevelName.HeaderText = Language._Object.getCaption(Me.colhGradeLevelName.Name, Me.colhGradeLevelName.HeaderText)
			Me.colhPriority.HeaderText = Language._Object.getCaption(Me.colhPriority.Name, Me.colhPriority.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, There is no transaction to Save.")
			Language.setMessage(mstrModuleName, 2, "Please change any Priority to Save.")
			Language.setMessage(mstrModuleName, 3, "Please enter proper Priority.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Priority should be greater than Zero.")
			Language.setMessage(mstrModuleName, 5, "Sorry, This Priority is already assigned.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class