﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmSectionList

#Region " Private Varaibles "
    Private objSections As clsSections
    Private ReadOnly mstrModuleName As String = "frmSectionList"
#End Region

#Region " Private Function "

    'Sandeep [ 29 Oct 2010 ] -- Start
    'Privilege Changes
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AddSection
            btnEdit.Enabled = User._Object.Privilege._EditSection
            btnDelete.Enabled = User._Object.Privilege._DeleteSection
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 29 Oct 2010 ] -- End

    Private Sub FillCombo()
        Try
            Dim dsDept As New DataSet
            Dim objDepartment As New clsDepartment

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objSecGroup As New clsSectionGroup
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            dsDept = objDepartment.getComboList("Dept", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsDept.Tables("Dept")
            End With

            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsDept = objSecGroup.getComboList("List", True)
            With cboSectionGroup
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsDept.Tables("List")
            End With
            'S.SANDEEP [ 07 NOV 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsAccess As New DataSet
        Dim strSearching As String = ""
        Dim dtClassTable As New DataTable
        Try

            If User._Object.Privilege._AllowToViewSectionList = True Then    'Pinkal (09-Jul-2012) -- Start

                dsAccess = objSections.GetList("Dept")

                If CInt(cboDepartment.SelectedValue) > 0 Then
                    strSearching = "AND departmentunkid =" & CInt(cboDepartment.SelectedValue) & " "
                End If

                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CInt(cboSectionGroup.SelectedValue) > 0 Then
                    strSearching = "AND sectiongroupunkid = " & CInt(cboSectionGroup.SelectedValue) & " "
                End If
                'S.SANDEEP [ 07 NOV 2011 ] -- END

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtClassTable = New DataView(dsAccess.Tables("Dept"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtClassTable = dsAccess.Tables("Dept")
                End If


                Dim lvItem As ListViewItem

                lvSectionList.Items.Clear()
                For Each drRow As DataRow In dtClassTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("code").ToString
                    lvItem.Tag = drRow("sectionunkid")
                    lvItem.SubItems.Add(drRow("DeptName").ToString)
                    lvItem.SubItems.Add(drRow("name").ToString)
                    lvItem.SubItems.Add(drRow("description").ToString)

                    'S.SANDEEP [ 07 NOV 2011 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    lvItem.SubItems.Add(drRow("SectionGrp").ToString)
                    'S.SANDEEP [ 07 NOV 2011 ] -- END

                    lvSectionList.Items.Add(lvItem)
                Next

                If lvSectionList.Items.Count > 16 Then
                    colhDescription.Width = 200 - 18
                Else
                    colhDescription.Width = 200
                End If

            End If

            'Call setAlternateColor(lvAirline)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsAccess.Dispose()
        End Try
    End Sub

    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "

    Private Sub frmSectionList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvSectionList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSectionList_KeyUp", mstrModuleName)
        End Try

    End Sub

    Private Sub frmSectionList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSectionList_KeyPress", mstrModuleName)
        End Try


    End Sub

    Private Sub frmSectionList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSections = New clsSections
        Try
            Call Set_Logo(Me, gApplicationType)

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            'Sandeep [ 29 Oct 2010 ] -- Start
            'Privilege Changes
            Call SetVisibility()
            'Sandeep [ 29 Oct 2010 ] -- End

            Call FillCombo()

            Call fillList()

            If lvSectionList.Items.Count > 0 Then lvSectionList.Items(0).Selected = True
            lvSectionList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmClassesList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSectionList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objSections = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSections.SetMessages()
            objfrm._Other_ModuleNames = "clsSections"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvSectionList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Section from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvSectionList.Select()
            Exit Sub
        End If
        'If objSections.isUsed(CInt(lvSectionList.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Section. Reason: This Section is in use."), enMsgBoxStyle.Information) '?2
        '    lvSectionList.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvSectionList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Section?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objSections._FormName = mstrModuleName
                objSections._LoginEmployeeunkid = 0
                objSections._ClientIP = getIP()
                objSections._HostName = getHostName()
                objSections._FromWeb = False
                objSections._AuditUserId = User._Object._Userunkid
objSections._CompanyUnkid = Company._Object._Companyunkid
                objSections._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objSections.Delete(CInt(lvSectionList.SelectedItems(0).Tag))
                If objSections._Message <> "" Then
                    eZeeMsgBox.Show(objSections._Message, enMsgBoxStyle.Information)
                Else
                    lvSectionList.SelectedItems(0).Remove()
                End If

                If lvSectionList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvSectionList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvSectionList.Items.Count - 1
                    lvSectionList.Items(intSelectedIndex).Selected = True
                    lvSectionList.EnsureVisible(intSelectedIndex)
                ElseIf lvSectionList.Items.Count <> 0 Then
                    lvSectionList.Items(intSelectedIndex).Selected = True
                    lvSectionList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvSectionList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvSectionList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Section from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvSectionList.Select()
            Exit Sub
        End If
        Dim frm As New frmSections_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvSectionList.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(CInt(lvSectionList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frm = Nothing

            lvSectionList.Items(intSelectedIndex).Selected = True
            lvSectionList.EnsureVisible(intSelectedIndex)
            lvSectionList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmSections_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call fillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboDepartment.SelectedValue = 0
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboSectionGroup.SelectedValue = 0
            'S.SANDEEP [ 07 NOV 2011 ] -- END
            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.Name, Me.lblSectionGroup.Text)
            Me.colhSectionGrp.Text = Language._Object.getCaption(CStr(Me.colhSectionGrp.Tag), Me.colhSectionGrp.Text)



        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Section from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Section?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class