Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmAutoImportApplicantWizard
    Private ReadOnly mstrModuleName As String = "frmAutoImportApplicantWizard"
    Dim m_strExeFullName As String = String.Empty

#Region " Constructor "
    Public Sub New()

        Dim ObjAppSet As New clsApplicationSettings

        m_strExeFullName = IO.Path.Combine(ObjAppSet._ApplicationPath, acore32.core.AutoImportRecruitment)

        If Not System.IO.File.Exists(m_strExeFullName) Then
            Throw New System.Exception("A file (" & acore32.core.AutoImportRecruitment & ") is missing in the application directory." & _
                                           vbCrLf & "Please contact " & acore32.core.SupportTeam & " for further assistance.")
        End If

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub
#End Region

#Region " Private Function "
    Private Sub FillCombo()
        Try
            For iCnt As Integer = 1 To 31
                cboDay.Items.Add(iCnt.ToString)
            Next
            cboDay.SelectedIndex = 0

            For iCnt As Integer = 1 To 7
                cboWeekDay.Items.Add(WeekdayName(iCnt))
            Next
            cboWeekDay.SelectedIndex = 0

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function ShortPathName(ByVal Path As String) As String
        Dim sb As New System.Text.StringBuilder(1024)

        Dim tempVal As Integer = Interop.GetShortPathName(Path, sb, 1024)
        If tempVal <> 0 Then
            Dim Result As String = sb.ToString()
            Return Result
        Else
            Throw New Exception("Failed to return a short path")
        End If
    End Function
#End Region

#Region " Forms "

    Private Sub frmAutoBackupWizard_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmAutoBackupWizard_FormClosing", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAutoBackupWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call FillCombo()
            If ConfigParameter._Object._WebClientCode <= 0 OrElse ConfigParameter._Object._AuthenticationCode.Trim = "" Then
                ewWizard.NextEnabled = False
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmAutoBackupWizard_Load", mstrModuleName)
            Me.Close()
        End Try
    End Sub
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()


        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Wizard "
    Private Sub ewWizard_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles ewWizard.AfterSwitchPages
        If e.NewIndex < e.OldIndex Then Exit Sub

        Select Case e.NewIndex
            Case ewWizard.Pages.IndexOf(ewpPage1)
                ewWizard.NextEnabled = True
            Case ewWizard.Pages.IndexOf(ewpPage2)
                'ewWizard.NextEnabled = IO.Directory.Exists(txtBackupFolder.Text)
            Case ewWizard.Pages.IndexOf(ewpPage3)
                If txtUserName.Text.Trim = "" Then
                    txtUserName.Text = System.Environment.UserDomainName & "\" & System.Environment.UserName
                End If
                ewWizard.NextEnabled = (txtUserName.Text <> "") AndAlso (txtPWD.Text = txtCPWD.Text)
            Case ewWizard.Pages.IndexOf(ewpPage4)

        End Select
    End Sub

    Private Sub ewWizard_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles ewWizard.BeforeSwitchPages
        If e.NewIndex < e.OldIndex Then Exit Sub

        Select Case e.OldIndex
            Case ewWizard.Pages.IndexOf(ewpPage1)
                
            Case ewWizard.Pages.IndexOf(ewpPage2)

            Case ewWizard.Pages.IndexOf(ewpPage3)

            Case ewWizard.Pages.IndexOf(ewpPage4)

        End Select
    End Sub

    Private Sub ewWizard_Finish(ByVal sender As Object, ByVal e As System.EventArgs) Handles ewWizard.Finish
        Dim objScheduledTasks As New TaskScheduler.TaskCollection
        Try
            Dim strTaskName As String = System.IO.Path.GetFileNameWithoutExtension(m_strExeFullName) & ".job"
            Dim strExeFile As String = m_strExeFullName

            'Dim strParameter As String = """" & txtBackupFolder.Text & """"


            For Each TaskName As String In objScheduledTasks
                If strTaskName = TaskName Then
                    objScheduledTasks.Remove(strTaskName)
                    Exit For
                End If
            Next

            Dim oTask As TaskScheduler.Task
            oTask = objScheduledTasks.Add(strTaskName, "Aruti")

            With oTask

                ' Set the exe path
                .ApplicationName = strExeFile
                .Parameters = Company._Object._Code

                If rbtnMonthly.Checked Then
                    With .Triggers.Add()
                        .TriggerType = TaskScheduler.Trigger.Types.MonthlyDate
                        .Day = cboDay.SelectedIndex + 1
                        .Month = TaskScheduler.Trigger.Months.January _
                                 + TaskScheduler.Trigger.Months.February _
                                 + TaskScheduler.Trigger.Months.March _
                                 + TaskScheduler.Trigger.Months.April _
                                 + TaskScheduler.Trigger.Months.May _
                                 + TaskScheduler.Trigger.Months.June _
                                 + TaskScheduler.Trigger.Months.July _
                                 + TaskScheduler.Trigger.Months.August _
                                 + TaskScheduler.Trigger.Months.September _
                                 + TaskScheduler.Trigger.Months.October _
                                 + TaskScheduler.Trigger.Months.November _
                                 + TaskScheduler.Trigger.Months.December
                        .BeginDay = Today
                        .StartTime = dtpTime.Value
                        .Flags = 0
                    End With
                ElseIf rbtnWeekly.Checked Then
                    With .Triggers.Add()
                        .TriggerType = TaskScheduler.Trigger.Types.Weekly
                        Select Case cboWeekDay.SelectedIndex
                            Case 0
                                .DayOfTheWeek = TaskScheduler.Trigger.Days.Sunday
                            Case 1
                                .DayOfTheWeek = TaskScheduler.Trigger.Days.Monday
                            Case 2
                                .DayOfTheWeek = TaskScheduler.Trigger.Days.Tuesday
                            Case 3
                                .DayOfTheWeek = TaskScheduler.Trigger.Days.Wednesday
                            Case 4
                                .DayOfTheWeek = TaskScheduler.Trigger.Days.Thursday
                            Case 5
                                .DayOfTheWeek = TaskScheduler.Trigger.Days.Friday
                            Case 6
                                .DayOfTheWeek = TaskScheduler.Trigger.Days.Saturday
                        End Select

                        .BeginDay = Today
                        .StartTime = dtpTime.Value
                        .Flags = 0
                    End With
                Else
                    With .Triggers.Add()
                        .TriggerType = TaskScheduler.Trigger.Types.Daily
                        .BeginDay = Today
                        .StartTime = dtpTime.Value
                        .Flags = 0
                    End With
                End If

                If txtUserName.Text <> "" AndAlso txtPWD.Text <> "" Then
                    .SetAccountInfo(txtUserName.Text, txtPWD.Text)
                End If
                '.Flags = TaskScheduler.Task.TaskFlags.Disabled

                ' Save the task


                .Save()

                If chkRunNow.Checked Then
                    .Run()
                End If

            End With

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "ewWizard_Finish", mstrModuleName)
        Finally
            objScheduledTasks.Dispose()
            objScheduledTasks = Nothing
        End Try
    End Sub
#End Region

#Region " Controls "

    Private Sub txtUserName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles _
          txtUserName.TextChanged _
          , txtPWD.TextChanged _
          , txtCPWD.TextChanged

        ewWizard.NextEnabled = (txtUserName.Text <> "") AndAlso (txtPWD.Text = txtCPWD.Text)

    End Sub

    Private Sub txtBackupFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'ewWizard.NextEnabled = IO.Directory.Exists(txtBackupFolder.Text)
    End Sub

    'Private Sub objOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Using objDialog As New Windows.Forms.FolderBrowserDialog
    '            If objDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
    '                txtBackupFolder.Text = objDialog.SelectedPath
    '            End If
    '        End Using

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objOpenFile_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub rbtnDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _
            rbtnDaily.CheckedChanged _
            , rbtnWeekly.CheckedChanged _
            , rbtnMonthly.CheckedChanged

        lblWeekDay.Visible = rbtnWeekly.Checked
        cboWeekDay.Visible = rbtnWeekly.Checked

        lblDay.Visible = rbtnMonthly.Checked
        cboDay.Visible = rbtnMonthly.Checked
    End Sub

#End Region

#Region " Introp "
    Private Class Interop
        <Runtime.InteropServices.DllImport("kernel32.dll", SetLastError:=True, CharSet:=Runtime.InteropServices.CharSet.Auto)> _
        Public Shared Function GetShortPathName(ByVal longPath As String, _
        <Runtime.InteropServices.MarshalAs(Runtime.InteropServices.UnmanagedType.LPTStr)> _
        ByVal ShortPath As System.Text.StringBuilder, _
        <Runtime.InteropServices.MarshalAs(Runtime.InteropServices.UnmanagedType.U4)> _
        ByVal bufferSize As Integer) As Integer
        End Function
    End Class
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.objbuttonBack.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonBack.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonCancel.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonNext.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonNext.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.ewWizard.CancelText = Language._Object.getCaption(Me.ewWizard.Name & "_CancelText" , Me.ewWizard.CancelText)
			Me.ewWizard.NextText = Language._Object.getCaption(Me.ewWizard.Name & "_NextText" , Me.ewWizard.NextText)
			Me.ewWizard.BackText = Language._Object.getCaption(Me.ewWizard.Name & "_BackText" , Me.ewWizard.BackText)
			Me.ewWizard.FinishText = Language._Object.getCaption(Me.ewWizard.Name & "_FinishText" , Me.ewWizard.FinishText)
			Me.lblTime.Text = Language._Object.getCaption(Me.lblTime.Name, Me.lblTime.Text)
			Me.rbtnMonthly.Text = Language._Object.getCaption(Me.rbtnMonthly.Name, Me.rbtnMonthly.Text)
			Me.rbtnWeekly.Text = Language._Object.getCaption(Me.rbtnWeekly.Name, Me.rbtnWeekly.Text)
			Me.rbtnDaily.Text = Language._Object.getCaption(Me.rbtnDaily.Name, Me.rbtnDaily.Text)
			Me.lblTaskType.Text = Language._Object.getCaption(Me.lblTaskType.Name, Me.lblTaskType.Text)
			Me.lblMsg1.Text = Language._Object.getCaption(Me.lblMsg1.Name, Me.lblMsg1.Text)
			Me.lblTitle1.Text = Language._Object.getCaption(Me.lblTitle1.Name, Me.lblTitle1.Text)
			Me.lblDay.Text = Language._Object.getCaption(Me.lblDay.Name, Me.lblDay.Text)
			Me.lblWeekDay.Text = Language._Object.getCaption(Me.lblWeekDay.Name, Me.lblWeekDay.Text)
			Me.lblTitle2.Text = Language._Object.getCaption(Me.lblTitle2.Name, Me.lblTitle2.Text)
			Me.lblMsg3.Text = Language._Object.getCaption(Me.lblMsg3.Name, Me.lblMsg3.Text)
			Me.lblMsg2.Text = Language._Object.getCaption(Me.lblMsg2.Name, Me.lblMsg2.Text)
			Me.Label12.Text = Language._Object.getCaption(Me.Label12.Name, Me.Label12.Text)
			Me.lblConfirmPassword.Text = Language._Object.getCaption(Me.lblConfirmPassword.Name, Me.lblConfirmPassword.Text)
			Me.lblPassword.Text = Language._Object.getCaption(Me.lblPassword.Name, Me.lblPassword.Text)
			Me.lblUserName.Text = Language._Object.getCaption(Me.lblUserName.Name, Me.lblUserName.Text)
			Me.lblTitle3.Text = Language._Object.getCaption(Me.lblTitle3.Name, Me.lblTitle3.Text)
			Me.chkRunNow.Text = Language._Object.getCaption(Me.chkRunNow.Name, Me.chkRunNow.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class





