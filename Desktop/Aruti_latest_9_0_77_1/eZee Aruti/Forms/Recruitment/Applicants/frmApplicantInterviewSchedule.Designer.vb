﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApplicantInterviewSchedule
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApplicantInterviewSchedule))
        Me.pnlInterviewBatchProcess = New System.Windows.Forms.Panel
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.lvApplicantShortListed = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhApplicant = New System.Windows.Forms.ColumnHeader
        Me.colhEmail = New System.Windows.Forms.ColumnHeader
        Me.colhMobileNo = New System.Windows.Forms.ColumnHeader
        Me.gbBatchGenerationInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.txtBatchTime = New eZee.TextBox.AlphanumericTextBox
        Me.lblVacancyType = New System.Windows.Forms.Label
        Me.lblBatchName = New System.Windows.Forms.Label
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.txtInterviewdate = New eZee.TextBox.AlphanumericTextBox
        Me.txtLocation = New eZee.TextBox.AlphanumericTextBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.lblInterviewTime = New System.Windows.Forms.Label
        Me.lblBatchNo = New System.Windows.Forms.Label
        Me.txtBatchNo = New eZee.TextBox.AlphanumericTextBox
        Me.cboBatchName = New System.Windows.Forms.ComboBox
        Me.lblInterviewDateFrom = New System.Windows.Forms.Label
        Me.lblInterview = New System.Windows.Forms.Label
        Me.lblLocation = New System.Windows.Forms.Label
        Me.txtInterviewType = New eZee.TextBox.AlphanumericTextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlInterviewBatchProcess.SuspendLayout()
        Me.gbBatchGenerationInfo.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlInterviewBatchProcess
        '
        Me.pnlInterviewBatchProcess.Controls.Add(Me.objchkAll)
        Me.pnlInterviewBatchProcess.Controls.Add(Me.lvApplicantShortListed)
        Me.pnlInterviewBatchProcess.Controls.Add(Me.gbBatchGenerationInfo)
        Me.pnlInterviewBatchProcess.Controls.Add(Me.objFooter)
        Me.pnlInterviewBatchProcess.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlInterviewBatchProcess.Location = New System.Drawing.Point(0, 0)
        Me.pnlInterviewBatchProcess.Name = "pnlInterviewBatchProcess"
        Me.pnlInterviewBatchProcess.Size = New System.Drawing.Size(754, 436)
        Me.pnlInterviewBatchProcess.TabIndex = 0
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(20, 140)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 5
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'lvApplicantShortListed
        '
        Me.lvApplicantShortListed.BackColorOnChecked = False
        Me.lvApplicantShortListed.CheckBoxes = True
        Me.lvApplicantShortListed.ColumnHeaders = Nothing
        Me.lvApplicantShortListed.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhCheck, Me.colhCode, Me.colhApplicant, Me.colhEmail, Me.colhMobileNo})
        Me.lvApplicantShortListed.CompulsoryColumns = ""
        Me.lvApplicantShortListed.FullRowSelect = True
        Me.lvApplicantShortListed.GridLines = True
        Me.lvApplicantShortListed.GroupingColumn = Nothing
        Me.lvApplicantShortListed.HideSelection = False
        Me.lvApplicantShortListed.Location = New System.Drawing.Point(12, 136)
        Me.lvApplicantShortListed.MinColumnWidth = 50
        Me.lvApplicantShortListed.MultiSelect = False
        Me.lvApplicantShortListed.Name = "lvApplicantShortListed"
        Me.lvApplicantShortListed.OptionalColumns = ""
        Me.lvApplicantShortListed.ShowMoreItem = False
        Me.lvApplicantShortListed.ShowSaveItem = False
        Me.lvApplicantShortListed.ShowSelectAll = True
        Me.lvApplicantShortListed.ShowSizeAllColumnsToFit = True
        Me.lvApplicantShortListed.Size = New System.Drawing.Size(730, 244)
        Me.lvApplicantShortListed.Sortable = True
        Me.lvApplicantShortListed.TabIndex = 0
        Me.lvApplicantShortListed.UseCompatibleStateImageBehavior = False
        Me.lvApplicantShortListed.View = System.Windows.Forms.View.Details
        '
        'objcolhCheck
        '
        Me.objcolhCheck.Tag = "objcolhCheck"
        Me.objcolhCheck.Text = ""
        Me.objcolhCheck.Width = 25
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 120
        '
        'colhApplicant
        '
        Me.colhApplicant.Tag = "colhApplicant"
        Me.colhApplicant.Text = "Applicant"
        Me.colhApplicant.Width = 260
        '
        'colhEmail
        '
        Me.colhEmail.Tag = "colhEmail"
        Me.colhEmail.Text = "Email"
        Me.colhEmail.Width = 200
        '
        'colhMobileNo
        '
        Me.colhMobileNo.Tag = "colhMobileNo"
        Me.colhMobileNo.Text = "Mobile No."
        Me.colhMobileNo.Width = 120
        '
        'gbBatchGenerationInfo
        '
        Me.gbBatchGenerationInfo.BorderColor = System.Drawing.Color.Black
        Me.gbBatchGenerationInfo.Checked = False
        Me.gbBatchGenerationInfo.CollapseAllExceptThis = False
        Me.gbBatchGenerationInfo.CollapsedHoverImage = Nothing
        Me.gbBatchGenerationInfo.CollapsedNormalImage = Nothing
        Me.gbBatchGenerationInfo.CollapsedPressedImage = Nothing
        Me.gbBatchGenerationInfo.CollapseOnLoad = False
        Me.gbBatchGenerationInfo.Controls.Add(Me.objbtnReset)
        Me.gbBatchGenerationInfo.Controls.Add(Me.objbtnSearch)
        Me.gbBatchGenerationInfo.Controls.Add(Me.cboVacancyType)
        Me.gbBatchGenerationInfo.Controls.Add(Me.txtBatchTime)
        Me.gbBatchGenerationInfo.Controls.Add(Me.lblVacancyType)
        Me.gbBatchGenerationInfo.Controls.Add(Me.lblBatchName)
        Me.gbBatchGenerationInfo.Controls.Add(Me.cboVacancy)
        Me.gbBatchGenerationInfo.Controls.Add(Me.txtInterviewdate)
        Me.gbBatchGenerationInfo.Controls.Add(Me.txtLocation)
        Me.gbBatchGenerationInfo.Controls.Add(Me.lblJob)
        Me.gbBatchGenerationInfo.Controls.Add(Me.lblInterviewTime)
        Me.gbBatchGenerationInfo.Controls.Add(Me.lblBatchNo)
        Me.gbBatchGenerationInfo.Controls.Add(Me.txtBatchNo)
        Me.gbBatchGenerationInfo.Controls.Add(Me.cboBatchName)
        Me.gbBatchGenerationInfo.Controls.Add(Me.lblInterviewDateFrom)
        Me.gbBatchGenerationInfo.Controls.Add(Me.lblInterview)
        Me.gbBatchGenerationInfo.Controls.Add(Me.lblLocation)
        Me.gbBatchGenerationInfo.Controls.Add(Me.txtInterviewType)
        Me.gbBatchGenerationInfo.ExpandedHoverImage = Nothing
        Me.gbBatchGenerationInfo.ExpandedNormalImage = Nothing
        Me.gbBatchGenerationInfo.ExpandedPressedImage = Nothing
        Me.gbBatchGenerationInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBatchGenerationInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBatchGenerationInfo.HeaderHeight = 25
        Me.gbBatchGenerationInfo.HeaderMessage = ""
        Me.gbBatchGenerationInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBatchGenerationInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBatchGenerationInfo.HeightOnCollapse = 0
        Me.gbBatchGenerationInfo.LeftTextSpace = 0
        Me.gbBatchGenerationInfo.Location = New System.Drawing.Point(12, 12)
        Me.gbBatchGenerationInfo.Name = "gbBatchGenerationInfo"
        Me.gbBatchGenerationInfo.OpenHeight = 112
        Me.gbBatchGenerationInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBatchGenerationInfo.ShowBorder = True
        Me.gbBatchGenerationInfo.ShowCheckBox = False
        Me.gbBatchGenerationInfo.ShowCollapseButton = False
        Me.gbBatchGenerationInfo.ShowDefaultBorderColor = True
        Me.gbBatchGenerationInfo.ShowDownButton = False
        Me.gbBatchGenerationInfo.ShowHeader = True
        Me.gbBatchGenerationInfo.Size = New System.Drawing.Size(730, 118)
        Me.gbBatchGenerationInfo.TabIndex = 4
        Me.gbBatchGenerationInfo.Temp = 0
        Me.gbBatchGenerationInfo.Text = "Batch Details"
        Me.gbBatchGenerationInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(705, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 225
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(680, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 224
        Me.objbtnSearch.TabStop = False
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(100, 32)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(165, 21)
        Me.cboVacancyType.TabIndex = 6
        '
        'txtBatchTime
        '
        Me.txtBatchTime.Flags = 0
        Me.txtBatchTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchTime.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBatchTime.Location = New System.Drawing.Point(626, 86)
        Me.txtBatchTime.Name = "txtBatchTime"
        Me.txtBatchTime.ReadOnly = True
        Me.txtBatchTime.Size = New System.Drawing.Size(91, 21)
        Me.txtBatchTime.TabIndex = 223
        '
        'lblVacancyType
        '
        Me.lblVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyType.Location = New System.Drawing.Point(8, 34)
        Me.lblVacancyType.Name = "lblVacancyType"
        Me.lblVacancyType.Size = New System.Drawing.Size(86, 16)
        Me.lblVacancyType.TabIndex = 5
        Me.lblVacancyType.Text = "Vacancy Type"
        Me.lblVacancyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBatchName
        '
        Me.lblBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchName.Location = New System.Drawing.Point(8, 61)
        Me.lblBatchName.Name = "lblBatchName"
        Me.lblBatchName.Size = New System.Drawing.Size(86, 16)
        Me.lblBatchName.TabIndex = 213
        Me.lblBatchName.Text = "Batch Name"
        Me.lblBatchName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 360
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(363, 32)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(354, 21)
        Me.cboVacancy.TabIndex = 2
        '
        'txtInterviewdate
        '
        Me.txtInterviewdate.Flags = 0
        Me.txtInterviewdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterviewdate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtInterviewdate.Location = New System.Drawing.Point(626, 59)
        Me.txtInterviewdate.Name = "txtInterviewdate"
        Me.txtInterviewdate.ReadOnly = True
        Me.txtInterviewdate.Size = New System.Drawing.Size(91, 21)
        Me.txtInterviewdate.TabIndex = 222
        '
        'txtLocation
        '
        Me.txtLocation.Flags = 0
        Me.txtLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocation.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLocation.Location = New System.Drawing.Point(363, 86)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.ReadOnly = True
        Me.txtLocation.Size = New System.Drawing.Size(167, 21)
        Me.txtLocation.TabIndex = 221
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(271, 34)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(86, 16)
        Me.lblJob.TabIndex = 1
        Me.lblJob.Text = "Vacancy"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInterviewTime
        '
        Me.lblInterviewTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewTime.Location = New System.Drawing.Point(536, 88)
        Me.lblInterviewTime.Name = "lblInterviewTime"
        Me.lblInterviewTime.Size = New System.Drawing.Size(84, 16)
        Me.lblInterviewTime.TabIndex = 217
        Me.lblInterviewTime.Text = "Interview Time"
        Me.lblInterviewTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBatchNo
        '
        Me.lblBatchNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchNo.Location = New System.Drawing.Point(8, 87)
        Me.lblBatchNo.Name = "lblBatchNo"
        Me.lblBatchNo.Size = New System.Drawing.Size(86, 16)
        Me.lblBatchNo.TabIndex = 211
        Me.lblBatchNo.Text = "Batch Code"
        Me.lblBatchNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBatchNo
        '
        Me.txtBatchNo.Flags = 0
        Me.txtBatchNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBatchNo.Location = New System.Drawing.Point(100, 86)
        Me.txtBatchNo.Name = "txtBatchNo"
        Me.txtBatchNo.ReadOnly = True
        Me.txtBatchNo.Size = New System.Drawing.Size(165, 21)
        Me.txtBatchNo.TabIndex = 210
        '
        'cboBatchName
        '
        Me.cboBatchName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBatchName.DropDownWidth = 270
        Me.cboBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBatchName.FormattingEnabled = True
        Me.cboBatchName.Location = New System.Drawing.Point(100, 59)
        Me.cboBatchName.Name = "cboBatchName"
        Me.cboBatchName.Size = New System.Drawing.Size(165, 21)
        Me.cboBatchName.TabIndex = 219
        '
        'lblInterviewDateFrom
        '
        Me.lblInterviewDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewDateFrom.Location = New System.Drawing.Point(536, 61)
        Me.lblInterviewDateFrom.Name = "lblInterviewDateFrom"
        Me.lblInterviewDateFrom.Size = New System.Drawing.Size(84, 16)
        Me.lblInterviewDateFrom.TabIndex = 215
        Me.lblInterviewDateFrom.Text = "Interview Date"
        Me.lblInterviewDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInterview
        '
        Me.lblInterview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterview.Location = New System.Drawing.Point(271, 61)
        Me.lblInterview.Name = "lblInterview"
        Me.lblInterview.Size = New System.Drawing.Size(86, 16)
        Me.lblInterview.TabIndex = 218
        Me.lblInterview.Text = "Interview Type"
        Me.lblInterview.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLocation
        '
        Me.lblLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocation.Location = New System.Drawing.Point(271, 88)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(86, 16)
        Me.lblLocation.TabIndex = 220
        Me.lblLocation.Text = "Location"
        Me.lblLocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInterviewType
        '
        Me.txtInterviewType.Flags = 0
        Me.txtInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterviewType.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtInterviewType.Location = New System.Drawing.Point(363, 59)
        Me.txtInterviewType.Name = "txtInterviewType"
        Me.txtInterviewType.ReadOnly = True
        Me.txtInterviewType.Size = New System.Drawing.Size(167, 21)
        Me.txtInterviewType.TabIndex = 212
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 386)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(754, 50)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(542, 10)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 128
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(645, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmApplicantInterviewSchedule
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(754, 436)
        Me.Controls.Add(Me.pnlInterviewBatchProcess)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApplicantInterviewSchedule"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Applicant Batch Schedule"
        Me.pnlInterviewBatchProcess.ResumeLayout(False)
        Me.pnlInterviewBatchProcess.PerformLayout()
        Me.gbBatchGenerationInfo.ResumeLayout(False)
        Me.gbBatchGenerationInfo.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlInterviewBatchProcess As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbBatchGenerationInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents lvApplicantShortListed As eZee.Common.eZeeListView
    Friend WithEvents lblBatchName As System.Windows.Forms.Label
    Friend WithEvents txtInterviewType As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBatchNo As System.Windows.Forms.Label
    Friend WithEvents txtBatchNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblInterviewTime As System.Windows.Forms.Label
    Friend WithEvents lblInterviewDateFrom As System.Windows.Forms.Label
    Friend WithEvents lblInterview As System.Windows.Forms.Label
    Friend WithEvents txtLocation As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblLocation As System.Windows.Forms.Label
    Friend WithEvents cboBatchName As System.Windows.Forms.ComboBox
    Friend WithEvents txtInterviewdate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtBatchTime As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancyType As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objcolhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhApplicant As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmail As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMobileNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
End Class
