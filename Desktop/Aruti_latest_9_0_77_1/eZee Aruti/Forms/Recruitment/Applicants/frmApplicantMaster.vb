﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

Public Class frmApplicantMaster

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmApplicantMaster"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objApplicant As clsApplicant_master
    Private mintApplicantunkid As Integer
    Private mdtSkillTran As DataTable
    Private mintSkillTypeId As Integer = -1
    Private mdtQualificationTran As DataTable
    Private mintQualificationTypeId As Integer = -1
    Private mdtJobHistory As DataTable
    Private mintJobHistoryTypeId As Integer = -1
    Private objSkill_Tran As clsApplicantSkill_tran
    Private objQualification_Tran As clsApplicantQualification_tran
    Private objJobHistory_Tran As clsJobhistory


    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes
    Private mdtReferenceTran As DataTable
    Private mintReferencesTypeId As Integer = -1
    Private objReference_Tran As clsrcapp_reference_Tran
    'Pinkal (06-Feb-2012) -- End

    'S.SANDEEP [ 06 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintVacancyId As Integer = -1
    'S.SANDEEP [ 06 NOV 2012 ] -- END
    'Sohail (25 Sep 2020) -- Start
    'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
    Private mintEmployeeunkid As Integer = 0
    'Sohail (25 Sep 2020) -- End

#End Region

#Region " Display Dialog "

    'S.SANDEEP [ 06 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function displayDialog(ByRef intUnkId As Integer, ByVal enAction As enAction) As Boolean
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal enAction As enAction, Optional ByVal intMappedVacancy As Integer = -1) As Boolean
        'S.SANDEEP [ 06 NOV 2012 ] -- END


        Try
            mintApplicantunkid = intUnkId
            menAction = enAction

            'S.SANDEEP [ 06 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mintVacancyId = intMappedVacancy
            'S.SANDEEP [ 06 NOV 2012 ] -- END

            Me.ShowDialog()

            intUnkId = mintApplicantunkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub SetColor()

        Try
            txtAddress1.BackColor = GUI.ColorOptional
            txtAddress2.BackColor = GUI.ColorOptional
            txtAlternativeNo.BackColor = GUI.ColorOptional
            txtApplcantFirstName.BackColor = GUI.ColorComp
            txtApplicantOtherName.BackColor = GUI.ColorComp
            txtApplicantSurname.BackColor = GUI.ColorComp
            'Sandeep [ 09 Oct 2010 ] -- Start
            'txtAward.BackColor = GUI.ColorOptional
            'Sandeep [ 09 Oct 2010 ] -- End 
            txtReferenceNo.BackColor = GUI.ColorComp
            txtCompanyName.BackColor = GUI.ColorComp
            'Sandeep [ 09 Oct 2010 ] -- Start
            'txtContactNo.BackColor = GUI.ColorOptional
            'txtContactPerson.BackColor = GUI.ColorOptional
            'Sandeep [ 09 Oct 2010 ] -- End 
            txtPermAddress1.BackColor = GUI.ColorOptional
            txtPermAddress2.BackColor = GUI.ColorOptional
            txtPermAltNo.BackColor = GUI.ColorOptional
            txtPermEstate.BackColor = GUI.ColorOptional
            txtPermFax.BackColor = GUI.ColorOptional
            txtPermMobile.BackColor = GUI.ColorOptional
            txtPermPlotNo.BackColor = GUI.ColorOptional
            txtPermProvince.BackColor = GUI.ColorOptional
            txtPermRoad.BackColor = GUI.ColorOptional
            txtPermTelNo.BackColor = GUI.ColorOptional
            txtEmployerName.BackColor = GUI.ColorComp
            txtApplicantCode.BackColor = GUI.ColorComp
            txtEmail.BackColor = GUI.ColorOptional
            txtEstate.BackColor = GUI.ColorOptional
            txtQualificationRemark.BackColor = GUI.ColorOptional
            txtFax.BackColor = GUI.ColorOptional
            'Sandeep [ 09 Oct 2010 ] -- Start
            'txtInstituteAddress.BackColor = GUI.ColorOptional
            'txtInstitution.BackColor = GUI.ColorComp
            'Sandeep [ 09 Oct 2010 ] -- End 
            txtLeavingReason.BackColor = GUI.ColorOptional
            txtMobile.BackColor = GUI.ColorOptional
            txtOfficePhone.BackColor = GUI.ColorOptional
            txtPlotNo.BackColor = GUI.ColorOptional
            txtProvince.BackColor = GUI.ColorOptional
            txtTelNo.BackColor = GUI.ColorOptional
            txtRemark.BackColor = GUI.ColorOptional
            txtRoad.BackColor = GUI.ColorOptional

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'cboVacancy.BackColor = GUI.ColorComp
            txtVacancy.BackColor = GUI.ColorComp
            'S.SANDEEP [ 28 FEB 2012 ] -- END
            cboCountry.BackColor = GUI.ColorOptional


            'Pinkal (1-Sep-2014) -- Start
            'Enhancement - TRA GENDER COMPULOSRY IN EMPLOYEE AND APPLICANT MASTER AS WELL AS IMPORTATION AND PUT APPOINTMENT CHECK BOX ON EMPLOYEE MASTER
            'cboGender.BackColor = GUI.ColorOptional
            'Pinkal (1-Sep-2014) -- End


            cboPermCountry.BackColor = GUI.ColorOptional
            cboPermPostCode.BackColor = GUI.ColorOptional
            cboPermState.BackColor = GUI.ColorOptional
            cboLanguage1.BackColor = GUI.ColorOptional
            cboLanguage2.BackColor = GUI.ColorOptional
            cboLanguage3.BackColor = GUI.ColorOptional
            cboLanguage4.BackColor = GUI.ColorOptional
            cboMaritalStatus.BackColor = GUI.ColorOptional
            cboNationality.BackColor = GUI.ColorOptional
            cboPostCode.BackColor = GUI.ColorOptional
            cboPostTown.BackColor = GUI.ColorOptional
            'Sohail (11 Sep 2010) -- Start
            'cboSkillCategory.BackColor = GUI.ColorOptional
            'cboSkill.BackColor = GUI.ColorOptional
            cboSkillCategory.BackColor = GUI.ColorComp
            cboSkill.BackColor = GUI.ColorComp
            'Sohail (11 Sep 2010) -- End
            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            cboSkillExpertise.BackColor = GUI.ColorOptional
            'Sohail (27 Apr 2017) -- End

            cboState.BackColor = GUI.ColorOptional
            cboTitle.BackColor = GUI.ColorOptional
            cboQualificationGroup.BackColor = GUI.ColorOptional
            cboQualification.BackColor = GUI.ColorOptional

            'Sohail (11 Sep 2010) -- Start
            cboPermPostTown.BackColor = GUI.ColorOptional
            txtDesignation.BackColor = GUI.ColorOptional
            txtResponsibility.BackColor = GUI.ColorOptional
            'Sohail (11 Sep 2010) -- End

            'Sandeep [ 09 Oct 2010 ] -- Start
            cboInstitution.BackColor = GUI.ColorComp
            'Sandeep [ 09 Oct 2010 ] -- End 

            'Anjan (21 Jul 2011)-Start
            'Issue : for web extra information.
            txtOtherSkill.BackColor = GUI.ColorOptional
            txtOtherQualification.BackColor = GUI.ColorOptional
            'Anjan (21 Jul 2011)-End 

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            cboResultCode.BackColor = GUI.ColorOptional
            nudGPA.BackColor = GUI.ColorOptional
            'Pinkal (12-Oct-2011) -- End

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtEmployeeCode.BackColor = GUI.ColorComp
            cboVacancyType.BackColor = GUI.ColorComp
            'S.SANDEEP [ 25 DEC 2011 ] -- END



            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            txtOtherSkillCategory.BackColor = GUI.ColorComp
            txtOtherSkill.BackColor = GUI.ColorComp

            txtOtherQualificationGrp.BackColor = GUI.ColorComp
            txtOtherQualification.BackColor = GUI.ColorComp
            txtOtherResultCode.BackColor = GUI.ColorComp
            txtOtherInstitute.BackColor = GUI.ColorComp

            txtReferenceName.BackColor = GUI.ColorComp
            txtRefPosition.BackColor = GUI.ColorOptional
            txtRefAddress.BackColor = GUI.ColorOptional
            cboRefCountry.BackColor = GUI.ColorOptional
            cboRefState.BackColor = GUI.ColorOptional
            cboReftown.BackColor = GUI.ColorOptional
            cboRefType.BackColor = GUI.ColorOptional
            txtRefTelNo.BackColor = GUI.ColorOptional
            txtRefMobileNo.BackColor = GUI.ColorOptional
            txtRefEmail.BackColor = GUI.ColorOptional
            cboRefGender.BackColor = GUI.ColorOptional

            'Pinkal (06-Feb-2012) -- End

            'Sohail (30 May 2012) -- Start
            'TRA - ENHANCEMENT
            txtMemberships.BackColor = GUI.ColorOptional
            txtAchievements.BackColor = GUI.ColorOptional
            txtJournalsResearchPapers.BackColor = GUI.ColorOptional
            'Sohail (30 May 2012) -- End


            'Pinkal (1-Sep-2014) -- Start
            'Enhancement - TRA GENDER COMPULOSRY IN EMPLOYEE AND APPLICANT MASTER AS WELL AS IMPORTATION AND PUT APPOINTMENT CHECK BOX ON EMPLOYEE MASTER
            cboGender.BackColor = GUI.ColorComp
            'Pinkal (1-Sep-2014) -- End

            'Sohail (22 Apr 2015) -- Start
            'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            txtCertiNo.BackColor = GUI.ColorOptional
            txtJobAchievement.BackColor = GUI.ColorOptional
            'Sohail (22 Apr 2015) -- End

            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            txtMotherTongue.BackColor = GUI.ColorOptional
            txtImpairment.BackColor = GUI.ColorOptional
            txtCurrentSalary.BackColor = GUI.ColorOptional
            txtExpectedSalary.BackColor = GUI.ColorOptional
            txtExpectedBenefit.BackColor = GUI.ColorOptional
            txtNoticePeriodDays.BackColor = GUI.ColorOptional
            'Sohail (27 Apr 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try

    End Sub

    Private Sub SetValue()

        Try
            objApplicant._Applicant_Code = txtApplicantCode.Text

            objApplicant._Email = txtEmail.Text
            objApplicant._Firstname = txtApplcantFirstName.Text
            objApplicant._Surname = txtApplicantSurname.Text
            objApplicant._Othername = txtApplicantOtherName.Text
            objApplicant._Titleunkid = CInt(cboTitle.SelectedValue)
            'Sandeep [ 27 NOV 2010 ] -- Start
            'objApplicant._Gender = cboGender.Text
            objApplicant._Gender = CInt(cboGender.SelectedValue)
            'Sandeep [ 27 NOV 2010 ] -- End


            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objApplicant._Vacancyunkid = CInt(cboVacancy.SelectedValue)
            'S.SANDEEP [ 28 FEB 2012 ] -- END



            objApplicant._ImagePath = imgImageControl._FileName
            imgImageControl.SaveImage()

            'Anjan (21 Jul 2011)-Start
            'Issue : for web extra information.
            objApplicant._OtherSkills = txtOtherSkill.Text
            objApplicant._OtherQualifications = txtOtherQualification.Text
            'Anjan (21 Jul 2011)-End 

            'Personal Start
            objApplicant._Present_Address1 = txtAddress1.Text
            objApplicant._Present_Address2 = txtAddress2.Text
            objApplicant._Present_Alternateno = txtAlternativeNo.Text
            objApplicant._Present_Countryunkid = CInt(cboCountry.SelectedValue)
            objApplicant._Present_Estate = txtEstate.Text
            objApplicant._Present_Fax = txtFax.Text
            objApplicant._Present_Mobileno = txtMobile.Text
            objApplicant._Present_Plotno = txtPlotNo.Text
            objApplicant._Present_Post_Townunkid = CInt(cboPostTown.SelectedValue)
            objApplicant._Present_Province = txtProvince.Text
            objApplicant._Present_Road = txtRoad.Text
            objApplicant._Present_Stateunkid = CInt(cboState.SelectedValue)
            objApplicant._Present_Tel_No = txtTelNo.Text
            objApplicant._Present_ZipCode = CInt(cboPostCode.SelectedValue)

            objApplicant._Perm_Address1 = txtPermAddress1.Text
            objApplicant._Perm_Address2 = txtPermAddress2.Text
            objApplicant._Perm_Alternateno = txtPermAltNo.Text
            objApplicant._Perm_Countryunkid = CInt(cboPermCountry.SelectedValue)
            objApplicant._Perm_Estate = txtPermEstate.Text
            objApplicant._Perm_Fax = txtPermFax.Text
            objApplicant._Perm_Mobileno = txtPermMobile.Text
            objApplicant._Perm_Plotno = txtPermPlotNo.Text
            objApplicant._Perm_Post_Townunkid = CInt(cboPermPostTown.SelectedValue)
            objApplicant._Perm_Province = txtPermProvince.Text
            objApplicant._Perm_Road = txtPermRoad.Text
            objApplicant._Perm_Stateunkid = CInt(cboPermState.SelectedValue)
            objApplicant._Perm_Tel_No = txtPermTelNo.Text
            objApplicant._Perm_ZipCode = CInt(cboPermPostCode.SelectedValue)
            'Personal End

            'Additional Start
            If dtpAnniversaryDate.Checked = True Then
                objApplicant._Anniversary_Date = dtpAnniversaryDate.Value
            Else
                objApplicant._Anniversary_Date = Nothing
            End If
            If dtpBirthdate.Checked = True Then
                objApplicant._Birth_Date = dtpBirthdate.Value
            Else
                objApplicant._Birth_Date = Nothing
            End If
            objApplicant._Marital_Statusunkid = CInt(cboMaritalStatus.SelectedValue)
            objApplicant._Nationality = CInt(cboNationality.SelectedValue)
            objApplicant._Language1unkid = CInt(cboLanguage1.SelectedValue)
            objApplicant._Language2unkid = CInt(cboLanguage2.SelectedValue)
            objApplicant._Language3unkid = CInt(cboLanguage3.SelectedValue)
            objApplicant._Language4unkid = CInt(cboLanguage4.SelectedValue)
            'Additional End


            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objApplicant._Employeecode = txtEmployeeCode.Text
            'S.SANDEEP [ 25 DEC 2011 ] -- END


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            objApplicant._Userunkid = User._Object._Userunkid
            'Pinkal (06-Feb-2012) -- End

            'Sohail (30 May 2012) -- Start
            'TRA - ENHANCEMENT
            objApplicant._Memberships = txtMemberships.Text.Trim
            objApplicant._Achievements = txtAchievements.Text.Trim
            objApplicant._JournalsResearchPapers = txtJournalsResearchPapers.Text.Trim
            'Sohail (30 May 2012) -- End

            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            objApplicant._MotherTongue = txtMotherTongue.Text.Trim
            objApplicant._IsImpaired = chkImpaired.Checked
            objApplicant._Impairment = txtImpairment.Text.Trim
            objApplicant._CurrentSalary = txtCurrentSalary.Decimal
            objApplicant._ExpectedSalary = txtExpectedSalary.Decimal
            objApplicant._ExpectedBenefits = txtExpectedBenefit.Text.Trim
            objApplicant._WillingToRelocate = chkWillingToRelocate.Checked
            objApplicant._WillingToTravel = chkWillingToTravel.Checked
            objApplicant._NoticePeriodDays = CInt(txtNoticePeriodDays.Decimal)
            'Sohail (27 Apr 2017) -- End

            'Sohail (05 Dec 2016) -- Start
            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
            If menAction <> enAction.EDIT_ONE Then
                objApplicant._UserId = Guid.NewGuid.ToString
            End If
            'Sohail (05 Dec 2016) -- End

            'Hemant (09 July 2018) -- Start
            'Enhancement : RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)
            objApplicant._CurrentSalaryCurrencyId = CInt(cboCurrentSalaryCurrency.SelectedValue)
            objApplicant._ExpectedSalaryCurrencyId = CInt(cboExpectedSalaryCurrency.SelectedValue)
            'Hemant (09 July 2018) -- End

            'Sohail (09 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
            objApplicant._IsFromOnline = False
            'Sohail (09 Oct 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Try
            If objApplicant._Employeeunkid <= 0 Then 'Sohail (25 Sep 2020)
            txtApplicantCode.Text = objApplicant._Applicant_Code
            txtEmail.Text = objApplicant._Email
            txtApplcantFirstName.Text = objApplicant._Firstname
            txtApplicantSurname.Text = objApplicant._Surname
            txtApplicantOtherName.Text = objApplicant._Othername
            cboTitle.SelectedValue = objApplicant._Titleunkid
            'Sandeep [ 27 NOV 2010 ] -- Start
            'cboGender.Text = objApplicant._Gender
            cboGender.SelectedValue = objApplicant._Gender
            'Sandeep [ 27 NOV 2010 ] -- End
            If objApplicant._ImagePath <> "" Then
                'imgImageControl._FileName = imgImageControl._FileName & "\" & objApplicant._ImagePath
                imgImageControl._Image = Image.FromFile(ConfigParameter._Object._PhotoPath & "\" & objApplicant._ImagePath)
            End If

            'Anjan (21 Jul 2011)-Start
            'Issue : for web extra information.
            txtOtherSkill.Text = objApplicant._OtherSkills
            txtOtherQualification.Text = objApplicant._OtherQualifications
            'Anjan (21 Jul 2011)-End 


            'Personal Start
            txtAddress1.Text = objApplicant._Present_Address1
            txtAddress2.Text = objApplicant._Present_Address2
            txtAlternativeNo.Text = objApplicant._Present_Alternateno
            cboCountry.SelectedValue = objApplicant._Present_Countryunkid
            txtEstate.Text = objApplicant._Present_Estate
            txtFax.Text = objApplicant._Present_Fax
            txtMobile.Text = objApplicant._Present_Mobileno
            txtPlotNo.Text = objApplicant._Present_Plotno
            cboPostTown.SelectedValue = objApplicant._Present_Post_Townunkid
            txtProvince.Text = objApplicant._Present_Province
            txtRoad.Text = objApplicant._Present_Road
            cboState.SelectedValue = objApplicant._Present_Stateunkid
            txtTelNo.Text = objApplicant._Present_Tel_No
            cboPostCode.SelectedValue = objApplicant._Present_ZipCode

            txtPermAddress1.Text = objApplicant._Perm_Address1
            txtPermAddress2.Text = objApplicant._Perm_Address2
            txtPermAltNo.Text = objApplicant._Perm_Alternateno
            cboPermCountry.SelectedValue = objApplicant._Perm_Countryunkid
            txtPermEstate.Text = objApplicant._Perm_Estate
            txtPermFax.Text = objApplicant._Perm_Fax
            txtPermMobile.Text = objApplicant._Perm_Mobileno
            txtPermPlotNo.Text = objApplicant._Perm_Plotno
            cboPermPostTown.SelectedValue = objApplicant._Perm_Post_Townunkid
            txtPermProvince.Text = objApplicant._Perm_Province
            txtPermRoad.Text = objApplicant._Perm_Road
            cboPermState.SelectedValue = objApplicant._Perm_Stateunkid
            txtPermTelNo.Text = objApplicant._Perm_Tel_No
            cboPermPostCode.SelectedValue = objApplicant._Perm_ZipCode
            'Personal End

            'Additional Start
            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            'If objApplicant._Anniversary_Date = Nothing Then
            If objApplicant._Anniversary_Date = Nothing OrElse objApplicant._Anniversary_Date = CDate("01/01/1900") Then
                'Sohail (08 Oct 2018) -- End
                dtpAnniversaryDate.Checked = False
            Else
                dtpAnniversaryDate.Checked = True
                dtpAnniversaryDate.Value = objApplicant._Anniversary_Date
            End If
            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            'If objApplicant._Birth_Date = Nothing Then
            If objApplicant._Birth_Date = Nothing OrElse objApplicant._Birth_Date = CDate("01/01/1900") Then
                'Sohail (08 Oct 2018) -- End
                dtpBirthdate.Checked = False
            Else
                dtpBirthdate.Checked = True
                dtpBirthdate.Value = objApplicant._Birth_Date
            End If
            cboMaritalStatus.SelectedValue = objApplicant._Marital_Statusunkid
            cboNationality.SelectedValue = objApplicant._Nationality
            cboLanguage1.SelectedValue = objApplicant._Language1unkid
            cboLanguage2.SelectedValue = objApplicant._Language2unkid
            cboLanguage3.SelectedValue = objApplicant._Language3unkid
            cboLanguage4.SelectedValue = objApplicant._Language4unkid
            'Additional End

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtEmployeeCode.Text = objApplicant._Employeecode

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If objApplicant._Vacancyunkid > 0 Then
            '    Dim objVac As New clsVacancy
            '    objVac._Vacancyunkid = objApplicant._Vacancyunkid
            '    Select Case CBool(objVac._Is_External_Vacancy)
            '        Case True
            '            cboVacancyType.SelectedValue = enVacancyType.EXTERNAL_VACANCY
            '        Case False
            '            cboVacancyType.SelectedValue = enVacancyType.INTERNAL_VACANCY
            '    End Select
            '    objVac = Nothing
            'Else
            '    cboVacancyType.SelectedValue = 0
            'End If
            Dim intVacancyId As Integer = -1

            'S.SANDEEP [ 06 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'txtVacancy.Text = clsApplicant_Vacancy_Mapping.GetLatestVacancy(objApplicant._Applicantunkid, intVacancyId)
            txtVacancy.Text = clsApplicant_Vacancy_Mapping.GetLatestVacancy(objApplicant._Applicantunkid, intVacancyId, mintVacancyId)
            'S.SANDEEP [ 06 NOV 2012 ] -- END
                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                'If intVacancyId > 0 Then
                'Dim objVac As New clsVacancy
                'objVac._Vacancyunkid = intVacancyId
                'Select Case CBool(objVac._Is_External_Vacancy)
                '    Case True
                '        cboVacancyType.SelectedValue = enVacancyType.EXTERNAL_VACANCY
                '    Case False
                '        cboVacancyType.SelectedValue = enVacancyType.INTERNAL_VACANCY
                'End Select
                'objVac = Nothing
                'ElseIf intVacancyId <= 0 AndAlso objApplicant._Employeecode.Trim.Length > 0 Then
                'cboVacancyType.SelectedValue = enVacancyType.INTERNAL_VACANCY
                'Else
                'cboVacancyType.SelectedValue = 0
                'End If
                If objApplicant._Employeeunkid > 0 Then
                cboVacancyType.SelectedValue = enVacancyType.INTERNAL_VACANCY
                    If menAction = enAction.EDIT_ONE Then
                        cboVacancyType.Enabled = False
                        txtEmployeeCode.Enabled = False
                    End If
            Else
                    cboVacancyType.SelectedValue = enVacancyType.EXTERNAL_VACANCY
            End If
                'Sohail (25 Sep 2020) -- End
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'Sohail (30 May 2012) -- Start
            'TRA - ENHANCEMENT
            txtMemberships.Text = objApplicant._Memberships.Replace(vbLf, vbCrLf)
            txtAchievements.Text = objApplicant._Achievements.Replace(vbLf, vbCrLf)
            txtJournalsResearchPapers.Text = objApplicant._JournalsResearchPapers.Replace(vbLf, vbCrLf)
            'Sohail (30 May 2012) -- End

            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            txtMotherTongue.Text = objApplicant._MotherTongue
            chkImpaired.Checked = objApplicant._IsImpaired
            txtImpairment.Text = objApplicant._Impairment
            txtCurrentSalary.Text = Format(objApplicant._CurrentSalary, GUI.fmtCurrency)
            txtExpectedSalary.Text = Format(objApplicant._ExpectedSalary, GUI.fmtCurrency)
            txtExpectedBenefit.Text = objApplicant._ExpectedBenefits
            chkWillingToRelocate.Checked = objApplicant._WillingToRelocate
            chkWillingToTravel.Checked = objApplicant._WillingToTravel
            txtNoticePeriodDays.Text = CInt(objApplicant._NoticePeriodDays).ToString
            'Sohail (27 Apr 2017) -- End

            'Hemant (09 July 2018) -- Start
            'Enhancement : RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)

            cboCurrentSalaryCurrency.SelectedValue = objApplicant._CurrentSalaryCurrencyId
            cboExpectedSalaryCurrency.SelectedValue = objApplicant._ExpectedSalaryCurrencyId
            'Hemant (09 July 2018) -- End

                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            Else
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(ConfigParameter._Object._CurrentDateAndTime) = objApplicant._Employeeunkid

                txtApplicantCode.Text = objApplicant._Applicant_Code
                txtEmail.Text = objEmp._Email
                txtApplcantFirstName.Text = objEmp._Firstname
                txtApplicantSurname.Text = objEmp._Surname
                txtApplicantOtherName.Text = objEmp._Othername
                cboTitle.SelectedValue = objEmp._Titalunkid
                cboGender.SelectedValue = objEmp._Gender

                If ConfigParameter._Object._IsImgInDataBase Then
                    imgImageControl._Image = Nothing
                    If objEmp._Photo IsNot Nothing Then
                        Dim ms As New System.IO.MemoryStream(objEmp._Photo, 0, objEmp._Photo.Length)
                        imgImageControl._Image = Image.FromStream(ms)
                    End If
                Else
                    If objEmp._ImagePath <> "" Then
                        imgImageControl._FileName = objEmp._ImagePath
                        Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                        If blnIsIISInstalled Then
                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL + "/UploadImage/" + StrEmployeeProfileFolder
                            Dim strError As String = ""
                            Dim strLocalpath As String = ""

                            Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(strPath, objEmp._ImagePath, StrEmployeeProfileFolder, strError, ConfigParameter._Object._ArutiSelfServiceURL)

                            If imagebyte IsNot Nothing Then
                                strLocalpath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & objEmp._ImagePath
                                Dim ms As New MemoryStream(imagebyte)
                                Dim fs As New FileStream(strLocalpath, FileMode.Create)
                                ms.WriteTo(fs)
                                ms.Close()
                                fs.Close()
                                fs.Dispose()
                            Else
                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            End If

                            If System.IO.File.Exists(strLocalpath) Then
                                imgImageControl._Image = Image.FromFile(strLocalpath)
                                imgImageControl._OriginalFilePath = strLocalpath
                                'mstrOrigincalFilepath = strLocalpath
                            End If
                        Else
                            If ConfigParameter._Object._PhotoPath.Trim.Length > 0 Then
                                If System.IO.File.Exists(ConfigParameter._Object._PhotoPath & "\" & objEmp._ImagePath) Then
                                    imgImageControl._Image = Image.FromFile(ConfigParameter._Object._PhotoPath & "\" & objEmp._ImagePath)
                                End If
                            End If
                        End If
                    End If
                End If

                txtOtherSkill.Text = objApplicant._OtherSkills
                txtOtherQualification.Text = objApplicant._OtherQualifications

                'Personal Start
                txtAddress1.Text = objEmp._Present_Address1
                txtAddress2.Text = objEmp._Present_Address2
                txtAlternativeNo.Text = objEmp._Present_Alternateno
                cboCountry.SelectedValue = objEmp._Present_Countryunkid
                txtEstate.Text = objEmp._Present_Estate
                txtFax.Text = objEmp._Present_Fax
                txtMobile.Text = objEmp._Present_Mobile
                txtPlotNo.Text = objEmp._Present_Plotno
                cboPostTown.SelectedValue = objEmp._Present_Post_Townunkid
                txtProvince.Text = objEmp._Present_Provicnce
                txtRoad.Text = objEmp._Present_Road
                cboState.SelectedValue = objEmp._Present_Stateunkid
                txtTelNo.Text = objEmp._Present_Tel_No
                cboPostCode.SelectedValue = objEmp._Present_Postcodeunkid

                txtPermAddress1.Text = objEmp._Domicile_Address1
                txtPermAddress2.Text = objEmp._Domicile_Address2
                txtPermAltNo.Text = objEmp._Domicile_Alternateno
                cboPermCountry.SelectedValue = objEmp._Domicile_Countryunkid
                txtPermEstate.Text = objEmp._Domicile_Estate
                txtPermFax.Text = objEmp._Domicile_Fax
                txtPermMobile.Text = objEmp._Domicile_Mobile
                txtPermPlotNo.Text = objEmp._Domicile_Plotno
                cboPermPostTown.SelectedValue = objEmp._Domicile_Post_Townunkid
                txtPermProvince.Text = objEmp._Domicile_Provicnce
                txtPermRoad.Text = objEmp._Domicile_Road
                cboPermState.SelectedValue = objEmp._Domicile_Stateunkid
                txtPermTelNo.Text = objEmp._Domicile_Tel_No
                cboPermPostCode.SelectedValue = objEmp._Domicile_Postcodeunkid
                'Personal End

                'Additional Start
                If objEmp._Anniversary_Date = Nothing OrElse objEmp._Anniversary_Date = CDate("01/01/1900") Then
                    dtpAnniversaryDate.Checked = False
                Else
                    dtpAnniversaryDate.Checked = True
                    dtpAnniversaryDate.Value = objEmp._Anniversary_Date
                End If

                If objEmp._Birthdate = Nothing OrElse objEmp._Birthdate = CDate("01/01/1900") Then
                    dtpBirthdate.Checked = False
                Else
                    dtpBirthdate.Checked = True
                    dtpBirthdate.Value = objEmp._Birthdate
                End If
                cboMaritalStatus.SelectedValue = objEmp._Maritalstatusunkid
                cboNationality.SelectedValue = objEmp._Nationalityunkid
                cboLanguage1.SelectedValue = objEmp._Language1unkid
                cboLanguage2.SelectedValue = objEmp._Language2unkid
                cboLanguage3.SelectedValue = objEmp._Language3unkid
                cboLanguage4.SelectedValue = objEmp._Language4unkid
                'Additional End

                txtEmployeeCode.Text = objEmp._Employeecode

                Dim intVacancyId As Integer = -1

                txtVacancy.Text = clsApplicant_Vacancy_Mapping.GetLatestVacancy(objApplicant._Applicantunkid, intVacancyId, mintVacancyId)

                If objApplicant._Employeeunkid > 0 Then
                    cboVacancyType.SelectedValue = enVacancyType.INTERNAL_VACANCY
                    If menAction = enAction.EDIT_ONE Then
                        cboVacancyType.Enabled = False
                        txtEmployeeCode.Enabled = False
                    End If
                Else
                    cboVacancyType.SelectedValue = enVacancyType.EXTERNAL_VACANCY
                End If

                txtMemberships.Text = objApplicant._Memberships.Replace(vbLf, vbCrLf)
                txtAchievements.Text = objApplicant._Achievements.Replace(vbLf, vbCrLf)
                txtJournalsResearchPapers.Text = objApplicant._JournalsResearchPapers.Replace(vbLf, vbCrLf)

                txtMotherTongue.Text = objApplicant._MotherTongue
                chkImpaired.Checked = objApplicant._IsImpaired
                txtImpairment.Text = objApplicant._Impairment

                dsList = objMaster.Get_Current_Scale("Salary", objApplicant._Employeeunkid, DateTime.Today)
                If dsList.Tables(0).Rows.Count > 0 Then
                    txtCurrentSalary.Text = Format(CDec(dsList.Tables(0).Rows(0).Item("newscale")), GUI.fmtCurrency)
                Else
                    txtCurrentSalary.Text = Format(objApplicant._CurrentSalary, GUI.fmtCurrency)
                End If

                txtExpectedSalary.Text = Format(objApplicant._ExpectedSalary, GUI.fmtCurrency)
                txtExpectedBenefit.Text = objApplicant._ExpectedBenefits
                chkWillingToRelocate.Checked = objApplicant._WillingToRelocate
                chkWillingToTravel.Checked = objApplicant._WillingToTravel
                txtNoticePeriodDays.Text = CInt(objApplicant._NoticePeriodDays).ToString

                cboCurrentSalaryCurrency.SelectedValue = objApplicant._CurrentSalaryCurrencyId
                cboExpectedSalaryCurrency.SelectedValue = objApplicant._ExpectedSalaryCurrencyId

                objEmp = Nothing
            End If
            'Sohail (25 Sep 2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
        Finally
            objMaster = Nothing
            'Sohail (25 Sep 2020) -- End
        End Try

    End Sub

    Private Sub FillCombo()

        Dim dsCombos As New DataSet

        Dim objCommon As New clsCommon_Master
        Dim objMaster As New clsMasterData
        Dim objReason As New clsAction_Reason
        Dim objCity As New clscity_master
        Dim objState As New clsstate_master
        Dim objZipCode As New clszipcode_master
        Dim objVacancy As New clsVacancy
        'Sandeep [ 09 Oct 2010 ] -- Start
        Dim objInstitute As New clsinstitute_master
        'Sandeep [ 09 Oct 2010 ] -- End 
        'Sohail (27 Apr 2017) -- Start
        'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
        Dim objSkillExp As New clsSkillExpertise_master
        'Sohail (27 Apr 2017) -- End

        Try


            dsCombos = objMaster.getCountryList("Nationality", True)
            With cboNationality
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("Nationality")
            End With
            dsCombos = objMaster.getGenderList("Gender", True)
            With cboGender
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Gender")
            End With

            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            With cboRefGender
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Gender").Copy
            End With
            'Pinkal (06-Feb-2012) -- End


            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "SkillCategory")
            With cboSkillCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("SkillCategory")
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "QualificationCategory")
            With cboQualificationGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("QualificationCategory")
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, "Lang1")
            With cboLanguage1
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Lang1")
            End With
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, "Lang2")
            With cboLanguage2
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Lang2")
            End With
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, "Lang3")
            With cboLanguage3
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Lang3")
            End With
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, "Lang4")
            With cboLanguage4
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Lang4")
            End With
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, True, "MaritalStatus")
            With cboMaritalStatus
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("MaritalStatus")
            End With

            dsCombos = objMaster.getCountryList("PresentCountry", True)
            With cboCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("PresentCountry").Copy
            End With
            With cboPermCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("PresentCountry").Copy
            End With


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            With cboRefCountry
                .ValueMember = "countryunkid"
                .DisplayMember = "country_name"
                .DataSource = dsCombos.Tables("PresentCountry")
            End With
            'Pinkal (06-Feb-2012) -- End



            dsCombos = objState.GetList("State", , True)
            With cboState
                .ValueMember = "stateunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("State").Copy
            End With
            With cboPermState
                .ValueMember = "stateunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("State").Copy
            End With

            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            With cboRefState
                .ValueMember = "stateunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("State").Copy
            End With
            'Pinkal (06-Feb-2012) -- End


            dsCombos = objCity.GetList("City", , True)
            With cboPostTown
                .ValueMember = "cityunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("City").Copy
            End With
            With cboPermPostTown
                .ValueMember = "cityunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("City").Copy
            End With

            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            With cboReftown
                .ValueMember = "cityunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("City").Copy
            End With
            'Pinkal (06-Feb-2012) -- End


            dsCombos = objZipCode.GetList("Zipcode", , True)
            With cboPostCode
                .ValueMember = "zipcodeunkid"
                .DisplayMember = "zipcode_no"
                .DataSource = dsCombos.Tables("Zipcode").Copy
            End With
            With cboPermPostCode
                .ValueMember = "zipcodeunkid"
                .DisplayMember = "zipcode_no"
                .DataSource = dsCombos.Tables("Zipcode").Copy
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.TITLE, True, "Title")
            With cboTitle
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Title")
            End With

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsCombos = objVacancy.getComboList(True, "List")
            'With cboVacancy
            '    .ValueMember = "id"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("List")
            '    .SelectedValue = 0
            'End With
            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            'dsCombos = objVacancy.getVacancyType()
            dsCombos = objVacancy.getVacancyType(False)
            'Sohail (25 Sep 2020) -- End
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'Sandeep [ 09 Oct 2010 ] -- Start
            'S.SANDEEP [ 03 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsCombos = objInstitute.getListForCombo(False, "List", True)
            dsCombos = objInstitute.getListForCombo(False, "List", True, , , , True)
            'S.SANDEEP [ 03 MAY 2012 ] -- END
            With cboInstitution
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            'Sandeep [ 09 Oct 2010 ] -- End 





            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation", -1, True)
            With cboRefType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Relation")
            End With
            'Pinkal (06-Feb-2012) -- End

            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            dsCombos = objSkillExp.getListForCombo("Expertise", True)
            With cboSkillExpertise
                .ValueMember = "skillexpertiseunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Expertise")
                .SelectedValue = 0
            End With
            'Sohail (27 Apr 2017) -- End

            'Hemant (07 July 2018) -- Start
            ' Enhancement - RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)
            dsCombos = objMaster.getCountryList("Nationality", True)
            With cboCurrentSalaryCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_country"
                .DataSource = dsCombos.Tables("Nationality").Copy
            End With

            With cboExpectedSalaryCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_country"
                .DataSource = dsCombos.Tables("Nationality").Copy
            End With
            'Hemant (07 July 2018) -- Start

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub

    'Additional Info -Start

    Private Sub FillApplicantSkillList()
        Dim objCommonMaster As New clsCommon_Master
        Dim objSkillMaster As New clsskill_master
        Dim objSExpertise As clsSkillExpertise_master 'Sohail (27 Apr 2017)
        Try
            lvApplicantSkill.Items.Clear()
            Dim lvApplicantSkillItems As ListViewItem

            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            objSExpertise = New clsSkillExpertise_master
            Dim dsSExpert As DataSet = objSExpertise.getListForCombo("List", False)
            'Sohail (25 Sep 2020) -- End

            For Each dtRow As DataRow In mdtSkillTran.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvApplicantSkillItems = New ListViewItem


                    'Pinkal (06-Feb-2012) -- Start
                    'Enhancement : TRA Changes
                    If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
                    If CInt(dtRow.Item("skillcategoryunkid").ToString) > 0 Then
                        objCommonMaster._Masterunkid = CInt(dtRow.Item("skillcategoryunkid").ToString)
                        lvApplicantSkillItems.Text = objCommonMaster._Name
                    Else
                        lvApplicantSkillItems.Text = dtRow.Item("other_skillcategory").ToString
                        lvApplicantSkillItems.ForeColor = Color.Blue
                    End If

                    If CInt(dtRow.Item("skillunkid").ToString) > 0 Then
                        objSkillMaster._Skillunkid = CInt(dtRow.Item("skillunkid").ToString)
                        lvApplicantSkillItems.SubItems.Add(objSkillMaster._Skillname)
                    Else
                        lvApplicantSkillItems.SubItems.Add(dtRow.Item("other_skill").ToString)
                    End If
                    'Pinkal (06-Feb-2012) -- End
                        'Sohail (25 Sep 2020) -- Start
                        'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    Else
                        If CInt(dtRow.Item("CatId").ToString) > 0 Then
                            lvApplicantSkillItems.Text = dtRow.Item("Category").ToString
                        Else
                            lvApplicantSkillItems.Text = dtRow.Item("other_skillcategory").ToString
                            lvApplicantSkillItems.ForeColor = Color.Blue
                        End If

                        If CInt(dtRow.Item("SkillId").ToString) > 0 Then
                            lvApplicantSkillItems.SubItems.Add(dtRow.Item("SkillName").ToString)
                        Else
                            lvApplicantSkillItems.SubItems.Add(dtRow.Item("other_skill").ToString)
                        End If
                    End If
                    'Sohail (25 Sep 2020) -- End


                    'Sohail (27 Apr 2017) -- Start
                    'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                    'Sohail (25 Sep 2020) -- Start
                    'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    'objSExpertise = New clsSkillExpertise_master
                    'objSExpertise._Skillexpertiseunkid = CInt(dtRow.Item("skillexpertiseunkid"))
                    'lvApplicantSkillItems.SubItems.Add(objSExpertise._Name)
                    Dim dr() As DataRow = dsSExpert.Tables(0).Select("skillexpertiseunkid = " & CInt(dtRow.Item("skillexpertiseunkid")) & " ")
                    If dr.Length > 0 Then
                        lvApplicantSkillItems.SubItems.Add(dr(0).Item("Name").ToString)
                    Else
                        lvApplicantSkillItems.SubItems.Add("")
                    End If
                    'Sohail (25 Sep 2020) -- End
                    lvApplicantSkillItems.SubItems(colhSkillExpertise.Index).Tag = CInt(dtRow.Item("skillexpertiseunkid"))
                    'Sohail (27 Apr 2017) -- End

                    If mintEmployeeunkid <= 0 Then 'Sohail (25 Jul 2020)
                    lvApplicantSkillItems.SubItems.Add(dtRow.Item("remark").ToString)
                    lvApplicantSkillItems.SubItems.Add(dtRow.Item("GUID").ToString)
                    lvApplicantSkillItems.SubItems.Add(dtRow.Item("skillcategoryunkid").ToString)
                    lvApplicantSkillItems.SubItems.Add(dtRow.Item("skillunkid").ToString)
                    lvApplicantSkillItems.Tag = dtRow.Item("skilltranunkid")
                        'Sohail (25 Sep 2020) -- Start
                        'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    Else
                        lvApplicantSkillItems.SubItems.Add(dtRow.Item("Description").ToString) 'remark
                        lvApplicantSkillItems.SubItems.Add(dtRow.Item("GUID").ToString) 'GUID
                        lvApplicantSkillItems.SubItems.Add(dtRow.Item("CatId").ToString) 'skillcategoryunkid
                        lvApplicantSkillItems.SubItems.Add(dtRow.Item("SkillId").ToString) 'skillunkid
                        lvApplicantSkillItems.Tag = dtRow.Item("SkillTranId")
                    End If
                    'Sohail (25 Sep 2020) -- End
                    lvApplicantSkill.Items.Add(lvApplicantSkillItems)
                    lvApplicantSkillItems = Nothing

                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillApplicantSkillList", mstrModuleName)
        Finally
            objCommonMaster = Nothing
            objSkillMaster = Nothing
        End Try
    End Sub

    Private Sub ResetApplicantSkill()

        Try
            cboSkill.SelectedValue = 0
            cboSkillCategory.SelectedValue = 0
            txtRemark.Text = ""


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            txtOtherSkillCategory.Text = ""
            txtOtherSkill.Text = ""
            pnlOtherSkill.Visible = False
            lnkOtherSkill.Enabled = True
            'Pinkal (06-Feb-2012) -- End
            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            cboSkillExpertise.SelectedValue = 0
            'Sohail (27 Apr 2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetApplicantSkill", mstrModuleName)
        End Try


    End Sub
    'Additional Info -End

    'Qualification Details -Start
    Private Sub FillApplicantQualificationList()
        'Sandeep [ 09 Oct 2010 ] -- Start
        'Dim objCommonMaster As New clsCommon_Master
        'Dim objQualificationMaster As New clsqualification_master
        'Sandeep [ 09 Oct 2010 ] -- End 
        Try


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            'mdtQualificationTran = New DataView(mdtQualificationTran, "", "award_end_date DESC", DataViewRowState.CurrentRows).ToTable()
            If ConfigParameter._Object._ApplicantQualificationSortByInRecruitment = enApplicantQualificationSortBy.HighestQualification Then
                If mintEmployeeunkid <= 0 Then 'Sohail (25 Jul 2020)
                mdtQualificationTran = New DataView(mdtQualificationTran, "", "ishighestqualification DESC, award_end_date_string DESC", DataViewRowState.CurrentRows).ToTable()
                    'Sohail (25 Sep 2020) -- Start
                    'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            Else
                    mdtQualificationTran = New DataView(mdtQualificationTran, "", "ishighestqualification DESC, EnDate DESC", DataViewRowState.CurrentRows).ToTable()
                End If
                'Sohail (25 Sep 2020) -- End
            Else
                    If mintEmployeeunkid <= 0 Then 'Sohail (25 Jul 2020)
                mdtQualificationTran = New DataView(mdtQualificationTran, "", "award_end_date_string DESC", DataViewRowState.CurrentRows).ToTable()
                    'Sohail (25 Sep 2020) -- Start
                    'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                Else
                    mdtQualificationTran = New DataView(mdtQualificationTran, "", "EnDate DESC", DataViewRowState.CurrentRows).ToTable()
                End If
                'Sohail (25 Sep 2020) -- End
            End If
            'Sohail (27 Apr 2017) -- End
            'Pinkal (25-APR-2012) -- End

            lvQualificationList.Items.Clear()
            Dim lvQualificationItems As ListViewItem

            For Each dtrow As DataRow In mdtQualificationTran.Rows
                If CStr(IIf(IsDBNull(dtrow.Item("AUD")), "A", dtrow.Item("AUD"))) <> "D" Then
                    lvQualificationItems = New ListViewItem


                    'Pinkal (06-Feb-2012) -- Start
                    'Enhancement : TRA Changes
                    If mintEmployeeunkid <= 0 Then 'Sohail (25 Jul 2020)
                    If CInt(dtrow.Item("qualificationgroupunkid").ToString) > 0 Then
                        'Sohail (19 Mar 2020) -- Start
                        'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
                        'Dim objCommonMaster As New clsCommon_Master
                        'objCommonMaster._Masterunkid = CInt(dtrow.Item("qualificationgroupunkid").ToString)
                        'lvQualificationItems.Text = objCommonMaster._Name
                        'objCommonMaster = Nothing
                        lvQualificationItems.Text = dtrow.Item("qualificationgrp").ToString
                        'Sohail (19 Mar 2020) -- End
                    Else
                        lvQualificationItems.Text = dtrow.Item("other_qualificationgrp").ToString
                        'lvQualificationItems.ForeColor = Color.Blue 'Sohail (19 Mar 2020)
                    End If

                    If CInt(dtrow.Item("qualificationunkid").ToString) > 0 Then
                        'Sohail (19 Mar 2020) -- Start
                        'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
                        'Dim objQualificationMaster As New clsqualification_master
                        'objQualificationMaster._Qualificationunkid = CInt(dtrow.Item("qualificationunkid").ToString)
                        'lvQualificationItems.SubItems.Add(objQualificationMaster._Qualificationname)
                        'objQualificationMaster = Nothing
                        lvQualificationItems.SubItems.Add(dtrow.Item("qualification").ToString)
                        'Sohail (19 Mar 2020) -- End
                    Else
                        lvQualificationItems.SubItems.Add(dtrow.Item("other_qualification").ToString)

                    End If
                        'Sohail (25 Sep 2020) -- Start
                        'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    Else
                        lvQualificationItems.Text = dtrow.Item("QGrp").ToString
                        lvQualificationItems.SubItems.Add(dtrow.Item("Qualify").ToString)
                    End If
                    'Sohail (25 Sep 2020) -- End
                    'Pinkal (06-Feb-2012) -- End

                    If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
                    lvQualificationItems.SubItems.Add(dtrow.Item("reference_no").ToString)
                        'Sohail (25 Sep 2020) -- Start
                        'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    Else
                        lvQualificationItems.SubItems.Add(dtrow.Item("RefNo").ToString)
                    End If
                    'Sohail (25 Sep 2020) -- End

                    If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
                    If IsDBNull(dtrow.Item("award_start_date")) Then
                        lvQualificationItems.SubItems.Add("")
                    Else
                        lvQualificationItems.SubItems.Add(CDate(dtrow.Item("award_start_date").ToString).ToShortDateString)
                    End If
                    If IsDBNull(dtrow.Item("award_end_date")) Then
                        lvQualificationItems.SubItems.Add("")
                    Else
                        lvQualificationItems.SubItems.Add(CDate(dtrow.Item("award_end_date").ToString).ToShortDateString)
                    End If
                        'Sohail (25 Sep 2020) -- Start
                        'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    Else
                        If IsDBNull(dtrow.Item("StDate")) Then
                            lvQualificationItems.SubItems.Add("")
                        Else
                            lvQualificationItems.SubItems.Add(CDate(eZeeDate.convertDate(dtrow.Item("StDate").ToString)).ToShortDateString)
                        End If
                        If IsDBNull(dtrow.Item("EnDate")) Then
                            lvQualificationItems.SubItems.Add("")
                        Else
                            lvQualificationItems.SubItems.Add(CDate(eZeeDate.convertDate(dtrow.Item("EnDate").ToString)).ToShortDateString)
                        End If
                    End If
                    'Sohail (25 Sep 2020) -- End

                    'Pinkal (06-Feb-2012) -- Start
                    'Enhancement : TRA Changes
                    If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
                    If CInt(dtrow.Item("instituteunkid")) > 0 Then
                        'Sohail (19 Mar 2020) -- Start
                        'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
                        'Dim objInstitute As New clsinstitute_master
                        'objInstitute._Instituteunkid = CInt(dtrow.Item("instituteunkid"))
                        'lvQualificationItems.SubItems.Add(objInstitute._Institute_Name)
                        'objInstitute = Nothing
                        lvQualificationItems.SubItems.Add(dtrow.Item("institute").ToString)
                        'Sohail (19 Mar 2020) -- End
                    Else
                        lvQualificationItems.SubItems.Add(dtrow.Item("other_institute").ToString)
                    End If
                        'Sohail (25 Sep 2020) -- Start
                        'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    Else
                        lvQualificationItems.SubItems.Add(dtrow.Item("Institute").ToString)
                    End If
                    'Sohail (25 Sep 2020) -- End
                    'Pinkal (06-Feb-2012) -- End

                    lvQualificationItems.SubItems.Add(dtrow.Item("remark").ToString)

                    If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
                    lvQualificationItems.SubItems.Add(dtrow.Item("qualificationgroupunkid").ToString)
                    lvQualificationItems.SubItems.Add(dtrow.Item("qualificationunkid").ToString)
                    lvQualificationItems.SubItems.Add(dtrow.Item("transaction_date").ToString)
                    lvQualificationItems.SubItems.Add(dtrow.Item("instituteunkid").ToString)
                        'Sohail (25 Sep 2020) -- Start
                        'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    Else
                        lvQualificationItems.SubItems.Add(dtrow.Item("QGrpId").ToString)
                        lvQualificationItems.SubItems.Add(dtrow.Item("QualifyId").ToString)
                        lvQualificationItems.SubItems.Add(DateAndTime.Now.Date.ToString)
                        lvQualificationItems.SubItems.Add(dtrow.Item("InstituteId").ToString)
                    End If
                    'Sohail (25 Sep 2020) -- End
                    lvQualificationItems.SubItems.Add(dtrow.Item("GUID").ToString)



                    'Pinkal (06-Feb-2012) -- Start
                    'Enhancement : TRA Changes
                    'Sohail (19 Mar 2020) -- Start
                    'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
                    'If CInt(dtrow.Item("resultunkid")) > 0 Then
                    '    lvQualificationItems.SubItems.Add(dtrow.Item("resultunkid").ToString)
                    'Else
                    '    lvQualificationItems.SubItems.Add(dtrow.Item("other_resultcode").ToString)
                    'End If
                    'Sohail (25 Sep 2020) -- Start
                    'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    'lvQualificationItems.SubItems.Add(dtrow.Item("resultunkid").ToString)
                    If CInt(dtrow.Item("resultunkid")) > 0 Then
                        lvQualificationItems.SubItems.Add(dtrow.Item("resultcode").ToString)
                        lvQualificationItems.SubItems(objcolhResultCodeID.Index).Tag = CInt(dtrow.Item("resultunkid").ToString)
                    Else
                        lvQualificationItems.SubItems.Add(dtrow.Item("other_resultcode").ToString)
                        lvQualificationItems.SubItems(objcolhResultCodeID.Index).Tag = 0
                    End If
                    'Sohail (25 Sep 2020) -- End
                    'Sohail (19 Mar 2020) -- End

                    'Pinkal (06-Feb-2012) -- End

                    lvQualificationItems.SubItems.Add(dtrow.Item("gpacode").ToString)

                    'Sohail (22 Apr 2015) -- Start
                    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
                    lvQualificationItems.SubItems.Add(dtrow.Item("certificateno").ToString)
                    'Sohail (22 Apr 2015) -- End

                    'Sohail (27 Apr 2017) -- Start
                    'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                    lvQualificationItems.SubItems.Add(dtrow.Item("ishighestqualification").ToString)
                    'Sohail (27 Apr 2017) -- End

                    If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
                        lvQualificationItems.Tag = dtrow.Item("qualificationtranunkid")
                        'Sohail (25 Sep 2020) -- Start
                        'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    Else
                        lvQualificationItems.Tag = dtrow.Item("QulifyTranId")
                    End If
                    'Sohail (25 Sep 2020) -- End


                    lvQualificationList.Items.Add(lvQualificationItems)

                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillApplicantQualificationList", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetApplicantQualification()

        Try

            'Sandeep [ 09 Oct 2010 ] -- Start
            'cboQualification.SelectedValue = 0
            'cboQualificationGroup.SelectedValue = 0
            'txtAward.Text = ""
            'txtReferenceNo.Text = ""
            'txtRemark.Text = ""
            'txtInstituteAddress.Text = ""
            'txtInstitution.Text = ""
            'txtContactNo.Text = ""
            'txtContactPerson.Text = ""
            'txtQualificationRemark.Text = ""
            'dtpQualifyDate.Value = Now.Date
            'dtpQualifyDate.Checked = False

            cboQualificationGroup.SelectedValue = 0
            cboQualification.SelectedValue = 0
            txtReferenceNo.Text = ""
            cboInstitution.SelectedValue = 0
            dtpQStartDate.Checked = False
            dtpQEndDate.Checked = False
            txtQualificationRemark.Text = ""
            'Sandeep [ 09 Oct 2010 ] -- End 



            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            cboResultCode.SelectedValue = 0
            nudGPA.Value = 0
            'Pinkal (12-Oct-2011) -- End


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            txtOtherQualificationGrp.Text = ""
            txtOtherQualification.Text = ""
            txtOtherResultCode.Text = ""
            txtOtherInstitute.Text = ""
            pnlOtherQualification.Visible = False
            lnkOtherQualification.Enabled = True
            'Pinkal (06-Feb-2012) -- End
            txtCertiNo.Text = "" 'Sohail (22 Apr 2015)
            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            chkHighestqualification.Checked = False
            'Sohail (27 Apr 2017) -- End

            'Sohail (19 Mar 2020) -- Start
            'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
            chkqualificationgroup.Checked = False
            chkqualificationAward.Checked = False
            chkResultCode.Checked = False
            chkInstitute.Checked = False
            'Sohail (19 Mar 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetApplicantQualification", mstrModuleName)
        End Try
    End Sub
    'Qualification Details -End

    'Job History -Start
    Private Sub FillApplicantsJobHistoryList()

        Try


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
            mdtJobHistory = New DataView(mdtJobHistory, "", "joiningdate DESC", DataViewRowState.CurrentRows).ToTable()
                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            Else
                mdtJobHistory = New DataView(mdtJobHistory, "", "StartDate DESC", DataViewRowState.CurrentRows).ToTable()
            End If
            'Sohail (25 Sep 2020) -- End
            'Pinkal (25-APR-2012) -- End


            lvJobHistory.Items.Clear()
            Dim lvJobHistoryItem As ListViewItem

            For Each dtRow As DataRow In mdtJobHistory.Rows
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvJobHistoryItem = New ListViewItem
                    If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
                    lvJobHistoryItem.Text = dtRow.Item("employername").ToString
                    lvJobHistoryItem.SubItems.Add(dtRow.Item("companyname").ToString)
                    lvJobHistoryItem.SubItems.Add(dtRow.Item("officephone").ToString)
                    lvJobHistoryItem.SubItems.Add(dtRow.Item("designation").ToString)
                    lvJobHistoryItem.SubItems.Add(dtRow.Item("responsibility").ToString)
                    'Sohail (11 Sep 2010) -- Start
                    'Issue : conversion from string "" to date is not valid
                    'lvJobHistoryItem.SubItems.Add(CDate(dtRow.Item("joiningdate").ToString).ToShortDateString)
                    'lvJobHistoryItem.SubItems.Add(CDate(dtRow.Item("terminationdate").ToString).ToShortDateString)
                    If dtRow.Item("joiningdate").ToString = Nothing Then
                        lvJobHistoryItem.SubItems.Add("")
                    Else
                        lvJobHistoryItem.SubItems.Add(CDate(dtRow.Item("joiningdate").ToString).ToShortDateString)
                    End If
                    If dtRow.Item("terminationdate").ToString = Nothing Then
                        lvJobHistoryItem.SubItems.Add("")
                    Else
                        lvJobHistoryItem.SubItems.Add(CDate(dtRow.Item("terminationdate").ToString).ToShortDateString)
                    End If
                    'Sohail (11 Sep 2010) -- End
                    lvJobHistoryItem.SubItems.Add(dtRow.Item("leavingreason").ToString)
                    lvJobHistoryItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    'Sohail (22 Apr 2015) -- Start
                    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
                    lvJobHistoryItem.SubItems.Add(dtRow.Item("achievements").ToString)
                    'Sohail (22 Apr 2015) -- End
                    lvJobHistoryItem.Tag = dtRow.Item("jobhistorytranunkid")
                        'Sohail (25 Sep 2020) -- Start
                        'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    Else
                        lvJobHistoryItem.Text = dtRow.Item("contact_person").ToString
                        lvJobHistoryItem.SubItems.Add(dtRow.Item("Company").ToString)
                        lvJobHistoryItem.SubItems.Add(dtRow.Item("contact_no").ToString)
                        lvJobHistoryItem.SubItems.Add(dtRow.Item("Jobs").ToString)
                        lvJobHistoryItem.SubItems.Add(dtRow.Item("remark").ToString)
                        If IsDBNull(dtRow.Item("StartDate")) = True Then
                            lvJobHistoryItem.SubItems.Add("")
                        Else
                            lvJobHistoryItem.SubItems.Add(CDate(eZeeDate.convertDate(dtRow.Item("StartDate").ToString)).ToShortDateString)
                        End If
                        If IsDBNull(dtRow.Item("EndDate")) = True Then
                            lvJobHistoryItem.SubItems.Add("")
                        Else
                            lvJobHistoryItem.SubItems.Add(CDate(eZeeDate.convertDate(dtRow.Item("EndDate").ToString)).ToShortDateString)
                        End If
                        lvJobHistoryItem.SubItems.Add(dtRow.Item("leave_reason").ToString)
                        lvJobHistoryItem.SubItems.Add(dtRow.Item("GUID").ToString)
                        lvJobHistoryItem.SubItems.Add("") 'achievements
                        lvJobHistoryItem.Tag = dtRow.Item("ExpId")
                    End If
                    'Sohail (25 Sep 2020) -- End
                    lvJobHistory.Items.Add(lvJobHistoryItem)
                    lvJobHistoryItem = Nothing
                End If

            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillApplicantsJobHistoryList", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetJobHistory()

        Try
            txtLeavingReason.Text = ""
            txtEmployerName.Text = ""
            txtCompanyName.Text = ""
            txtOfficePhone.Text = ""
            txtDesignation.Text = ""
            txtResponsibility.Text = ""
            txtLeavingReason.Text = ""

            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            dtpJoinDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpJoinDate.Checked = False
            dtpTerminationDate.Checked = False
            'Pinkal (25-APR-2012) -- End
            txtJobAchievement.Text = "" 'Sohail (22 Apr 2015)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetJobHistory", mstrModuleName)
        End Try

    End Sub
    'Job History -End

    Private Function isValid() As Boolean

        Try

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If CInt(cboVacancyType.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Vacancy Type cannot be blank. Vacancy Type is compulsory information."), enMsgBoxStyle.Information)
            '    cboVacancyType.Focus()
            '    Return False
            'End If
            'S.SANDEEP [ 25 DEC 2011 ] -- END


            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If CInt(cboVacancy.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Vacancy cannot be blank. Vacancy is compulsory information."), enMsgBoxStyle.Information)
            '    cboVacancy.Focus()
            '    Return False
            'End If
            'S.SANDEEP [ 28 FEB 2012 ] -- END


            'Sandeep [ 21 Aug 2010 ] -- Start
            'If txtApplicantCode.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Applicant Code cannot be blank. Applicant Code is compulsory information."), enMsgBoxStyle.Information)
            '    txtApplicantCode.Focus()
            '    Return False
            'End If
            If ConfigParameter._Object._ApplicantCodeNotype = 0 Then
                If txtApplicantCode.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Applicant Code cannot be blank. Applicant Code is compulsory information."), enMsgBoxStyle.Information)
                    txtApplicantCode.Focus()
                    Return False
                End If
            End If
            'Sandeep [ 21 Aug 2010 ] -- End 

            If txtApplcantFirstName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Firstname cannot be blank. Firstname is compulsory information."), enMsgBoxStyle.Information)
                txtApplcantFirstName.Focus()
                Return False
            End If

            'Sohail (11 Sep 2010) -- Start
            'If txtApplicantSurname.Text = "" Then
            If txtApplicantSurname.Text.Trim = "" Then
                'Sohail (11 Sep 2010) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Surname cannot be blank. Surname is compulsory information."), enMsgBoxStyle.Information)
                txtApplicantSurname.Focus()
                Return False
            End If

            If txtEmail.Text.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Email is mandatory information. Please enter email to continue."), enMsgBoxStyle.Information)
                txtEmail.Focus()
                Return False
            End If

            'S.SANDEEP [ 11 MAR 2014 ] -- START
            'Dim strExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
            Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
            'S.SANDEEP [ 11 MAR 2014 ] -- END

            If txtEmail.Text.Length > 0 Then
                If Expression.IsMatch(txtEmail.Text.Trim) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Invalid Email.Please Enter Valid Email"), enMsgBoxStyle.Information)
                    txtEmail.Focus()
                    Return False
                End If
            End If

            If dtpBirthdate.Checked = True And dtpAnniversaryDate.Checked = True Then
                If dtpAnniversaryDate.Value <= dtpBirthdate.Value Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Married Date should be greater than Birth Date."), enMsgBoxStyle.Information)
                    If tabcApplicantInformation.SelectedIndex <> 1 Then tabcApplicantInformation.SelectedIndex = 1 'Sohail (11 Sep 2010)
                    dtpAnniversaryDate.Focus()
                    Return False
                End If
            End If

            'Sohail (11 Sep 2010) -- Start
            'Issue : This validation should be on add/edit entry level (on add, edit click)
            'If dtpTerminationDate.Checked = True And dtpJoinDate.Checked = True Then
            '    If dtpTerminationDate.Value < dtpJoinDate.Value Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Termination should be greater than Join Date."), enMsgBoxStyle.Information)
            '        dtpTerminationDate.Focus()
            '        Return False
            '    End If
            'End If
            'Sohail (11 Sep 2010) -- End

            'Sohail (11 Sep 2010) -- Start
            If dtpBirthdate.Checked = True AndAlso dtpBirthdate.Value > DateTime.Today.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Birth Date should be less than current Date."), enMsgBoxStyle.Information)
                If tabcApplicantInformation.SelectedIndex <> 1 Then tabcApplicantInformation.SelectedIndex = 1
                dtpBirthdate.Focus()
                Return False
            ElseIf dtpAnniversaryDate.Checked = True AndAlso dtpAnniversaryDate.Value >= DateTime.Today.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Married Date should be less than current Date."), enMsgBoxStyle.Information)
                If tabcApplicantInformation.SelectedIndex <> 1 Then tabcApplicantInformation.SelectedIndex = 1
                dtpAnniversaryDate.Focus()
                Return False
            End If
            'Sohail (11 Sep 2010) -- End

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If CInt(cboVacancyType.SelectedValue) = enVacancyType.INTERNAL_VACANCY Then
                If txtEmployeeCode.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Employee Code cannot be blank. Please enter Employee Code to continue."), enMsgBoxStyle.Information)
                    txtEmployeeCode.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP [ 25 DEC 2011 ] -- END


            'Anjan (06 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            If CInt(cboVacancyType.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Applicant Type cannot be blank. Applicant Type is compulsory information."), enMsgBoxStyle.Information)
                cboVacancyType.Focus()
                Return False
            End If
            'Anjan (06 May 2012)-End 



            'Pinkal (1-Sep-2014) -- Start
            'Enhancement - TRA GENDER COMPULOSRY IN EMPLOYEE AND APPLICANT MASTER AS WELL AS IMPORTATION AND PUT APPOINTMENT CHECK BOX ON EMPLOYEE MASTER
            If CInt(cboGender.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 43, "Gender is mandatory information.Please select Gender."), enMsgBoxStyle.Information)
                cboGender.Focus()
                Return False
            End If
            'Pinkal (1-Sep-2014) -- End

            'Hemant (09 July 2018) -- Start
            'Enhancement : RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)
            If CInt(cboCurrentSalaryCurrency.SelectedValue) = 0 And CDec(txtCurrentSalary.Text) > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 43, "Please Select Currency of Current Salary."), enMsgBoxStyle.Information)
                cboCurrentSalaryCurrency.Focus()
                Return False
            End If

            If CInt(cboExpectedSalaryCurrency.SelectedValue) = 0 And CDec(txtExpectedSalary.Text) > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 43, "Please Select Currency of Expected Salary."), enMsgBoxStyle.Information)
                cboExpectedSalaryCurrency.Focus()
                Return False
            End If
            'Hemant (09 July 2018) -- End

            'Sohail (04 Jul 2019) -- Start
            'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (Setting on aruti configuration to set Curriculam Vitae and Cover Letter attachment mandatory).
            'If ConfigParameter._Object._OneQualificationMandatoryInRecruitment = True AndAlso lvQualificationList.Items.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 44, "Please add atleast one qualification."), enMsgBoxStyle.Information)
            '    tabcApplicantInformation.SelectTab(tabpEducationalInfo)
            '    Return False
            'End If
            'Sohail (04 Jul 2019) -- End


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isValid", mstrModuleName)
        End Try

    End Function


    'Sandeep [ 21 Aug 2010 ] -- Start
    Private Sub SetVisibility()
        Try
            If ConfigParameter._Object._ApplicantCodeNotype = 1 Then
                txtApplicantCode.Enabled = False
            Else
                txtApplicantCode.Enabled = True
            End If
            objbtnAddGroup.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddInstitution.Enabled = User._Object.Privilege._AddTrainingInstitute
            objbtnAddQulification.Enabled = User._Object.Privilege._AddQualification_Course
            objbtnAddSkill.Enabled = User._Object.Privilege._AddSkills
            objbtnAddSkillCategory.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddVacancy.Enabled = User._Object.Privilege._AllowtoAddVacancy


            'Anjan (09 May 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            If menAction = enAction.EDIT_ONE Then
                'Sohail (27 Apr 2017) -- Start
                'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                'If objApplicant._Referenceno <> "" Then
                'Sohail (09 Oct 2018) -- Start
                'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                'If objApplicant._UserId <> "" Then
                If objApplicant._UserId <> "" AndAlso objApplicant._IsFromOnline = True Then
                    'Sohail (09 Oct 2018) -- End
                    'Sohail (27 Apr 2017) -- End
                    btnAdd.Enabled = False
                    btnDelete.Enabled = False
                    btnEdit.Enabled = False
                    btnJobHistoryAdd.Enabled = False
                    btnJobHistoryDelete.Enabled = False
                    btnJobHistoryEdit.Enabled = False
                    btnQualifyAdd.Enabled = False
                    btnQualifyEdit.Enabled = False
                    btnQualifyDelete.Enabled = False
                    btnRefAdd.Enabled = False
                    btnRefDelete.Enabled = False
                    btnRefEdit.Enabled = False
                    btnSaveInfo.Visible = False
                    'Sohail (25 Sep 2020) -- Start
                    'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                ElseIf objApplicant._UserId <> "" AndAlso CInt(objApplicant._Employeeunkid) > 0 Then
                    btnAdd.Enabled = False
                    btnDelete.Enabled = False
                    btnEdit.Enabled = False
                    btnJobHistoryAdd.Enabled = False
                    btnJobHistoryDelete.Enabled = False
                    btnJobHistoryEdit.Enabled = False
                    btnQualifyAdd.Enabled = False
                    btnQualifyEdit.Enabled = False
                    btnQualifyDelete.Enabled = False
                    btnRefAdd.Enabled = False
                    btnRefDelete.Enabled = False
                    btnRefEdit.Enabled = False

                    Dim lst As IEnumerable(Of Control) = pnlPersonalInfoinner.Controls.OfType(Of Control)().Where(Function(x) Not TypeOf (x) Is Label)
                    For Each ctrl In lst
                        ctrl.Enabled = False
                    Next

                    lst = gbApplicantInfo.Controls.OfType(Of Control)().Where(Function(x) Not (TypeOf (x) Is Label OrElse TypeOf (x) Is TabControl))
                    For Each ctrl In lst
                        ctrl.Enabled = False
                    Next

                    lst = gbOtherInfo.Controls.OfType(Of Control)().Where(Function(x) Not TypeOf (x) Is Label)
                    For Each ctrl In lst
                        ctrl.Enabled = False
                    Next

                    txtCurrentSalary.Enabled = False
                    cboCurrentSalaryCurrency.Enabled = False
                    lnkCopyAddress.Enabled = False
                    'Sohail (25 Sep 2020) -- End
                End If
            End If
            'Anjan (09 May 2012)-End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 21 Aug 2010 ] -- End 


    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub FillApplicantReferencesList()
        Try
            lvReferences.Items.Clear()
            Dim lvItem As ListViewItem = Nothing
            For Each dtRow As DataRow In mdtReferenceTran.Rows
                If dtRow("AUD").ToString() <> "D" Then
                    lvItem = New ListViewItem
                    If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
                    lvItem.Text = dtRow("name").ToString()
                    lvItem.Tag = CInt(dtRow("referencetranunkid").ToString())
                    lvItem.SubItems.Add(dtRow("position").ToString())
                    lvItem.SubItems.Add(dtRow("gender").ToString())
                    lvItem.SubItems.Add(dtRow("relation").ToString())
                    lvItem.SubItems.Add(dtRow("email").ToString())
                    lvItem.SubItems.Add(dtRow("address").ToString())
                    lvItem.SubItems.Add(dtRow("countryunkid").ToString())
                    lvItem.SubItems.Add(dtRow("stateunkid").ToString())
                    lvItem.SubItems.Add(dtRow("cityunkid").ToString())
                    lvItem.SubItems.Add(dtRow("relationunkid").ToString())
                    lvItem.SubItems.Add(dtRow("telephone_no").ToString())
                    lvItem.SubItems.Add(dtRow("mobile_no").ToString())
                    lvItem.SubItems.Add(dtRow("genderunkid").ToString())
                        'Sohail (25 Sep 2020) -- Start
                        'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    Else
                        lvItem.Text = dtRow("RefName").ToString()
                        lvItem.Tag = CInt(dtRow("RefreeTranId").ToString())
                        lvItem.SubItems.Add(dtRow("ref_position").ToString())
                        lvItem.SubItems.Add(dtRow("gender").ToString())
                        lvItem.SubItems.Add(dtRow("relation").ToString())
                        lvItem.SubItems.Add(dtRow("email").ToString())
                        lvItem.SubItems.Add(dtRow("address").ToString())
                        lvItem.SubItems.Add(dtRow("CountryId").ToString())
                        lvItem.SubItems.Add(dtRow("stateunkid").ToString())
                        lvItem.SubItems.Add(dtRow("cityunkid").ToString())
                        lvItem.SubItems.Add(dtRow("relationunkid").ToString())
                        lvItem.SubItems.Add(dtRow("telephone_no").ToString())
                        lvItem.SubItems.Add(dtRow("mobile_no").ToString())
                        lvItem.SubItems.Add(dtRow("genderunkid").ToString())
                    End If
                    'Sohail (25 Sep 2020) -- End
                    lvItem.SubItems.Add(dtRow("GUID").ToString())
                    lvReferences.Items.Add(lvItem)
                End If
            Next

            If lvReferences.Items.Count > 10 Then
                colhRefEmail.Width = 150 - 18
            Else
                colhRefEmail.Width = 150
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillApplicantReferencesList", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetApplicantReferences()
        Try
            txtReferenceName.Text = ""
            txtRefAddress.Text = ""
            txtRefPosition.Text = ""
            txtRefTelNo.Text = ""
            txtRefEmail.Text = ""
            txtRefMobileNo.Text = ""
            cboRefCountry.SelectedIndex = 0
            cboRefState.SelectedIndex = 0
            cboReftown.SelectedIndex = 0
            cboRefType.SelectedIndex = 0
            cboRefGender.SelectedIndex = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetApplicantReferences", mstrModuleName)
        End Try
    End Sub

    'Pinkal (06-Feb-2012) -- End


#End Region

#Region " Skill Tab "
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click


        'Pinkal (06-Feb-2012) -- Start
        'Enhancement : TRA Changes

        If pnlOtherSkill.Visible Then

            If txtOtherSkillCategory.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "Other Skill Category can not be blank. Other Skill Category is Required information."), enMsgBoxStyle.Information)
                txtOtherSkillCategory.Select()
                Exit Sub
            End If

            If txtOtherSkill.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "Other Skill can not be blank. Other Skill is Required information."), enMsgBoxStyle.Information)
                txtOtherSkill.Select()
                Exit Sub
            End If

        Else

            If CInt(cboSkillCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Skill Category is compulsory information, it can not be blank. Please select Skill Category to continue."), enMsgBoxStyle.Information)
                cboSkillCategory.Focus()
                Exit Sub
            End If

            If CInt(cboSkill.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Skill is compulsory information, it can not be blank. Please select Skill to continue."), enMsgBoxStyle.Information)
                cboSkill.Focus() 'Sohail (11 Sep 2010)
                Exit Sub
            End If

        End If


        Dim dtRow As DataRow() = Nothing

        If pnlOtherSkill.Visible Then
            dtRow = mdtSkillTran.Select("other_skill = '" & txtOtherSkillCategory.Text.Trim & "' AND AUD <> 'D' ")
        Else
            dtRow = mdtSkillTran.Select("skillunkid = " & CInt(cboSkill.SelectedValue) & " AND AUD <> 'D' ")
        End If


        'Pinkal (06-Feb-2012) -- End

        If dtRow.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Selected Skill is already added to the list."), enMsgBoxStyle.Information)
            cboSkill.Focus() 'Sohail (11 Sep 2010)
            Exit Sub
        End If
        Try
            Dim dtIdRow As DataRow
            dtIdRow = mdtSkillTran.NewRow

            dtIdRow.Item("skilltranunkid") = -1
            dtIdRow.Item("applicantunkid") = mintApplicantunkid
            dtIdRow.Item("skillcategoryunkid") = CInt(cboSkillCategory.SelectedValue)
            dtIdRow.Item("skillunkid") = CInt(cboSkill.SelectedValue)
            dtIdRow.Item("remark") = txtRemark.Text


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            dtIdRow.Item("other_skillcategory") = txtOtherSkillCategory.Text.Trim
            dtIdRow.Item("other_skill") = txtOtherSkill.Text.Trim
            'Pinkal (06-Feb-2012) -- End

            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            dtIdRow.Item("skillexpertiseunkid") = CInt(cboSkillExpertise.SelectedValue)
            'Sohail (27 Apr 2017) -- End

            dtIdRow.Item("GUID") = Guid.NewGuid().ToString
            dtIdRow.Item("AUD") = "A"

            mdtSkillTran.Rows.Add(dtIdRow)

            Call FillApplicantSkillList()
            Call ResetApplicantSkill()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        If lvApplicantSkill.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please select Applicant Skill from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApplicantSkill.Select()
            Exit Sub
        End If


        'Pinkal (06-Feb-2012) -- Start
        'Enhancement : TRA Changes
        If pnlOtherSkill.Visible Then

            If txtOtherSkillCategory.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "Other Skill Category can not be blank. Other Skill Category is Required information."), enMsgBoxStyle.Information)
                txtOtherSkillCategory.Select()
                Exit Sub
            End If

            If txtOtherSkill.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "Other Skill can not be blank. Other Skill is Required information."), enMsgBoxStyle.Information)
                txtOtherSkill.Select()
                Exit Sub
            End If

        Else

            If CInt(cboSkillCategory.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Skill Category is compulsory information, it can not be blank. Please select Skill Category to continue."), enMsgBoxStyle.Information)
                cboSkillCategory.Focus()
                Exit Sub
            End If
            If CInt(cboSkill.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Skill is compulsory information, it can not be blank. Please select Skill to continue."), enMsgBoxStyle.Information)
                cboSkill.Focus()
                Exit Sub
            End If

        End If

        'Pinkal (06-Feb-2012) -- End
        Try
            'Sohail (11 Sep 2010) -- Start
            'If lvApplicantSkill.Items.Count > 0 Then
            If lvApplicantSkill.SelectedItems.Count > 0 Then
                'Sohail (11 Sep 2010) -- End
                If mintSkillTypeId > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvApplicantSkill.Items(mintSkillTypeId).Tag) = -1 Then
                        drTemp = mdtSkillTran.Select("GUID = '" & lvApplicantSkill.Items(mintSkillTypeId).SubItems(objcolhSkillGUID.Index).Text & "'")

                    Else

                        'Pinkal (06-Feb-2012) -- Start
                        'Enhancement : TRA Changes

                        If pnlOtherSkill.Visible Then
                            drTemp = mdtSkillTran.Select("other_skill = '" & lvApplicantSkill.Items(mintSkillTypeId).SubItems(colhSkillName.Index).Text & "'")
                        Else
                            drTemp = mdtSkillTran.Select("skilltranunkid = " & CInt(lvApplicantSkill.Items(mintSkillTypeId).Tag) & "")
                        End If
                        'Pinkal (06-Feb-2012) -- End

                    End If

                    If drTemp.Length > 0 Then
                        With drTemp(0)
                            .Item("skilltranunkid") = lvApplicantSkill.Items(mintSkillTypeId).Tag
                            .Item("applicantunkid") = mintApplicantunkid
                            .Item("skillcategoryunkid") = CInt(cboSkillCategory.SelectedValue)
                            .Item("skillunkid") = CInt(cboSkill.SelectedValue)
                            .Item("remark") = txtRemark.Text

                            'Pinkal (06-Feb-2012) -- Start
                            'Enhancement : TRA Changes
                            .Item("other_skillcategory") = txtOtherSkillCategory.Text.Trim
                            .Item("other_skill") = txtOtherSkill.Text.Trim
                            'Pinkal (06-Feb-2012) -- End

                            'Sohail (27 Apr 2017) -- Start
                            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                            .Item("skillexpertiseunkid") = CInt(cboSkillExpertise.SelectedValue)
                            'Sohail (27 Apr 2017) -- End


                            .Item("GUID") = Guid.NewGuid().ToString
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            .AcceptChanges()
                        End With
                        Call FillApplicantSkillList()
                    End If
                End If
                Call ResetApplicantSkill()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Sohail (11 Sep 2010) -- Start
        If lvApplicantSkill.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please select Applicant Skill from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvApplicantSkill.Select()
            Exit Sub
        End If
        'Sohail (11 Sep 2010) -- End
        Try
            If lvApplicantSkill.SelectedItems.Count > 0 Then
                If mintSkillTypeId > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvApplicantSkill.Items(mintSkillTypeId).Tag) = -1 Then
                        drTemp = mdtSkillTran.Select("GUID = '" & lvApplicantSkill.Items(mintSkillTypeId).SubItems(objcolhSkillGUID.Index).Text & "'")

                    Else

                        'Pinkal (06-Feb-2012) -- Start
                        'Enhancement : TRA Changes

                        If pnlOtherSkill.Visible Then
                            drTemp = mdtSkillTran.Select("other_skill = '" & lvApplicantSkill.Items(mintSkillTypeId).SubItems(colhSkillName.Index).Text & "'")
                        Else
                            drTemp = mdtSkillTran.Select("skilltranunkid = " & CInt(lvApplicantSkill.Items(mintSkillTypeId).Tag) & "")
                        End If
                        'Pinkal (06-Feb-2012) -- End

                    End If
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        Call FillApplicantSkillList()
                    End If
                End If
            End If
            Call ResetApplicantSkill()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try

    End Sub

    'Sohail (22 Apr 2015) -- Start
    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    'Private Sub lvApplicantSkill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvApplicantSkill.Click
    Private Sub lvApplicantSkill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvApplicantSkill.SelectedIndexChanged
        'Sohail (22 Apr 2015) -- End

        Try
            'Sohail (22 Apr 2015) -- Start
            'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            'If lvApplicantSkill.Items.Count > 0 Then
            If lvApplicantSkill.SelectedItems.Count > 0 Then
                'Sohail (22 Apr 2015) -- End
                mintSkillTypeId = lvApplicantSkill.SelectedItems(0).Index

                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes

                If CInt(lvApplicantSkill.SelectedItems(0).SubItems(objcolhSkillCategoryunkid.Index).Text) <= 0 AndAlso CInt(lvApplicantSkill.SelectedItems(0).SubItems(objcolhSkillunkid.Index).Text) <= 0 Then
                    pnlOtherSkill.Visible = True
                    lnkOtherSkill.Enabled = True
                    txtOtherSkillCategory.Text = lvApplicantSkill.SelectedItems(0).SubItems(colhSkillCategory.Index).Text.Trim
                    txtOtherSkill.Text = lvApplicantSkill.SelectedItems(0).SubItems(colhSkillName.Index).Text.Trim
                Else
                    pnlOtherSkill.Visible = False
                    lnkOtherSkill.Enabled = False
                    cboSkillCategory.SelectedValue = CInt(lvApplicantSkill.SelectedItems(0).SubItems(objcolhSkillCategoryunkid.Index).Text)
                    cboSkill.SelectedValue = CInt(lvApplicantSkill.SelectedItems(0).SubItems(objcolhSkillunkid.Index).Text)
                End If

                'Pinkal (06-Feb-2012) -- End

                'Sohail (27 Apr 2017) -- Start
                'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                cboSkillExpertise.SelectedValue = CInt(lvApplicantSkill.SelectedItems(0).SubItems(colhSkillExpertise.Index).Tag)
                'Sohail (27 Apr 2017) -- End

                txtRemark.Text = lvApplicantSkill.SelectedItems(0).SubItems(colhSkillRemark.Index).Text
                'Sohail (22 Apr 2015) -- Start
                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            Else
                Call ResetApplicantSkill()
                'Sohail (22 Apr 2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvApplicantSkill_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub lnkOtherSkill_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkOtherSkill.LinkClicked
        Try
            If pnlOtherSkill.Visible = False Then
                pnlOtherSkill.Visible = True
                'Sohail (22 Apr 2015) -- Start
                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            Else
                pnlOtherSkill.Visible = False
                'Sohail (22 Apr 2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkOtherSkill_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Qualification Tab "

    'Sohail (19 Mar 2020) -- Start
    'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
    Private Function IsValidQualification() As Boolean
        Try
            If chkqualificationgroup.Checked = True AndAlso txtOtherQualificationGrp.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Other Qualification Group can not be blank. Other Qualification Group is required information."), enMsgBoxStyle.Information)
                txtOtherQualificationGrp.Select()
                Exit Function
            End If
            
            If chkqualificationAward.Checked = True AndAlso txtOtherQualification.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Other Qualification can not be blank. Other Qualification is required information."), enMsgBoxStyle.Information)
                txtOtherQualification.Select()
                Exit Function
            End If

            If chkResultCode.Checked = True AndAlso txtOtherResultCode.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, " Other Result Code can not be blank. Other Result Code  is required information."), enMsgBoxStyle.Information)
                txtOtherResultCode.Select()
                Exit Function
            End If

            'If chkInstitute.Checked = True AndAlso txtOtherInstitute.Text.Trim.Length = 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Other Institute can not be blank. Other Institute is required information."), enMsgBoxStyle.Information)
            '    txtOtherInstitute.Select()
            '    Exit Function
            'End If

            If chkqualificationgroup.Checked = False AndAlso CInt(cboQualificationGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Qualification Group is compulsory information, it can not be blank. Please select Qualification Group to continue."), enMsgBoxStyle.Information)
                cboQualificationGroup.Focus()
                Exit Function
            End If
            
            If chkqualificationAward.Checked = False AndAlso CInt(cboQualification.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Qualification is compulsory information, it can not be blank. Please select Qualification to continue."), enMsgBoxStyle.Information)
                cboQualification.Focus()
                Exit Function
            End If
          
            If chkResultCode.Checked = False AndAlso CInt(cboResultCode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Result Code is compulsory information, it can not be blank. Please select Result Code to continue."), enMsgBoxStyle.Information)
                cboResultCode.Focus()
                Exit Function
            End If

            'If chkInstitute.Checked = False AndAlso CInt(cboInstitution.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Institute is compulsory information. Please select Institute to continue."), enMsgBoxStyle.Information)
            '    cboInstitution.Focus()
            '    Exit Function
            'End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidQualification", mstrModuleName)
        End Try
    End Function
    'Sohail (19 Mar 2020) -- End

    Private Sub btnQualifyAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQualifyAdd.Click


        'Pinkal (06-Feb-2012) -- Start
        'Enhancement : TRA Changes

        'Sohail (19 Mar 2020) -- Start
        'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
        'If pnlOtherQualification.Visible Then

        '    If txtOtherQualificationGrp.Text.Trim.Length = 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Other Qualification Group can not be blank. Other Qualification Group is required information."), enMsgBoxStyle.Information)
        '        txtOtherQualificationGrp.Select()
        '        Exit Sub
        '    End If
        '    If txtOtherQualification.Text.Trim.Length = 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Other Qualification can not be blank. Other Qualification is required information."), enMsgBoxStyle.Information)
        '        txtOtherQualification.Select()
        '        Exit Sub
        '    End If

        '    'Anjan (02 May 2012)-Start
        '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        '    'If txtOtherResultCode.Text.Trim.Length = 0 Then
        '    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, " Other Result Code can not be blank. Other Result Code  is required information."), enMsgBoxStyle.Information)
        '    '    txtOtherResultCode.Select()
        '    '    Exit Sub
        '    'End If
        '    'Anjan (02 May 2012)-End 


        '    If txtOtherInstitute.Text.Trim.Length = 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Other Institute can not be blank. Other Institute is required information."), enMsgBoxStyle.Information)
        '        txtOtherInstitute.Select()
        '        Exit Sub
        '    End If

        'Else

        '    If CInt(cboQualificationGroup.SelectedValue) <= 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Qualification Group is compulsory information, it can not be blank. Please select Qualification Group to continue."), enMsgBoxStyle.Information)
        '        cboQualificationGroup.Focus()
        '        Exit Sub
        '    End If
        '    If CInt(cboQualification.SelectedValue) <= 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Qualification is compulsory information, it can not be blank. Please select Qualification to continue."), enMsgBoxStyle.Information)
        '        cboQualification.Focus() 'Sohail (11 Sep 2010)
        '        Exit Sub
        '    End If
        '    If CInt(cboResultCode.SelectedValue) <= 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Result Code is compulsory information, it can not be blank. Please select Result Code to continue."), enMsgBoxStyle.Information)
        '        cboResultCode.Focus() 'Sohail (11 Sep 2010)
        '        Exit Sub
            'End If

        '    'Anjan (17 Apr 2012)-Start
        '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        '    'If CInt(cboInstitution.SelectedValue) <= 0 Then
        '    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Institute is compulsory information. Please select Institute to continue."), enMsgBoxStyle.Information)
        '    '    cboInstitution.Focus()
        '    '    Exit Sub
        '    'End If
        '    'Anjan (17 Apr 2012)-End 



        'End If
        If IsValidQualification() = False Then Exit Sub
        'Sohail (19 Mar 2020) -- End
        'Pinkal (06-Feb-2012) -- End


        'Sandeep [ 09 Oct 2010 ] -- Start
        ''Sohail (11 Sep 2010) -- Start
        'If dtpQualifyDate.Checked = True AndAlso dtpQualifyDate.Value > DateTime.Today.Date Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 137, "Qualify date should be less than current date."), enMsgBoxStyle.Information)
        '    dtpQualifyDate.Focus()
        '    Exit Sub
        'End If
        ''Sohail (11 Sep 2010) -- End
        If dtpQEndDate.Checked = True AndAlso dtpQStartDate.Checked = True Then
            If dtpQEndDate.Value.Date < dtpQStartDate.Value.Date Then 'Anjan (02 May 2012) on andrew sir request - equal to check is removed as qualification can be for one day training of short courses.
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "End date cannot be less than to start date."), enMsgBoxStyle.Information)
                dtpQEndDate.Focus()
                Exit Sub
            End If
        End If


        'Pinkal (06-Feb-2012) -- Start
        'Enhancement : TRA Changes

        Dim dtRow As DataRow() = Nothing

        If pnlOtherQualification.Visible Then
            dtRow = mdtQualificationTran.Select("other_qualification = '" & txtOtherQualification.Text.Trim & "' AND AUD <> 'D' ")
        Else
            dtRow = mdtQualificationTran.Select("qualificationunkid = " & CInt(cboQualification.SelectedValue) & " AND AUD <> 'D' ")
        End If

        'Pinkal (06-Feb-2012) -- End

        If dtRow.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Selected Qualification is already added to the list."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            Dim dtIdRow As DataRow
            dtIdRow = mdtQualificationTran.NewRow


            'Sandeep [ 09 Oct 2010 ] -- Start
            'dtIdRow.Item("qualificationtranunkid") = -1
            'dtIdRow.Item("applicantunkid") = mintApplicantunkid
            'dtIdRow.Item("qualificationgroupunkid") = CInt(cboQualificationGroup.SelectedValue)
            'dtIdRow.Item("qualificationunkid") = CInt(cboQualification.SelectedValue)
            'If dtpQualifyDate.Checked = True Then
            '    dtIdRow.Item("qualificationdate") = dtpQualifyDate.Value
            'Else
            '    dtIdRow.Item("qualificationdate") = DBNull.Value
            'End If

            'dtIdRow.Item("award") = txtAward.Text
            'dtIdRow.Item("referenceno") = txtReferenceNo.Text
            'dtIdRow.Item("institution_name") = txtInstitution.Text
            'dtIdRow.Item("address") = txtInstituteAddress.Text
            'dtIdRow.Item("contactperson") = txtContactPerson.Text
            'dtIdRow.Item("contactno") = txtContactNo.Text
            'dtIdRow.Item("remark") = txtQualificationRemark.Text
            'dtIdRow.Item("GUID") = Guid.NewGuid().ToString
            'dtIdRow.Item("AUD") = "A"

            dtIdRow.Item("qualificationtranunkid") = -1
            dtIdRow.Item("applicantunkid") = mintApplicantunkid
            dtIdRow.Item("qualificationgroupunkid") = CInt(cboQualificationGroup.SelectedValue)
            dtIdRow.Item("qualificationunkid") = CInt(cboQualification.SelectedValue)



            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            'dtIdRow.Item("transaction_date") = dtpQualifyDate.Value
            dtIdRow.Item("transaction_date") = ConfigParameter._Object._CurrentDateAndTime
            'Pinkal (06-Feb-2012) -- End


            dtIdRow.Item("reference_no") = txtReferenceNo.Text
            If dtpQStartDate.Checked = True Then
                dtIdRow.Item("award_start_date") = dtpQStartDate.Value
            Else
                dtIdRow.Item("award_start_date") = DBNull.Value
            End If
            If dtpQEndDate.Checked = True Then
                dtIdRow.Item("award_end_date") = dtpQEndDate.Value
                'Sohail (27 Apr 2017) -- Start
                'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                dtIdRow.Item("award_end_date_string") = eZeeDate.convertDate(dtpQEndDate.Value).ToString
                'Sohail (27 Apr 2017) -- End
            Else
                dtIdRow.Item("award_end_date") = DBNull.Value
                'Sohail (27 Apr 2017) -- Start
                'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                dtIdRow.Item("award_end_date_string") = "88880101"
                'Sohail (27 Apr 2017) -- End
            End If
            dtIdRow.Item("instituteunkid") = CInt(cboInstitution.SelectedValue)

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            dtIdRow.Item("resultunkid") = CInt(cboResultCode.SelectedValue)
            dtIdRow.Item("gpacode") = nudGPA.Value
            'Pinkal (12-Oct-2011) -- End


            dtIdRow.Item("remark") = txtQualificationRemark.Text


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            dtIdRow.Item("other_qualificationgrp") = txtOtherQualificationGrp.Text.Trim
            dtIdRow.Item("other_qualification") = txtOtherQualification.Text.Trim
            dtIdRow.Item("other_institute") = txtOtherInstitute.Text.Trim
            dtIdRow.Item("other_resultcode") = txtOtherResultCode.Text.Trim
            'Pinkal (06-Feb-2012) -- End

            'Sohail (19 Mar 2020) -- Start
            'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
            dtIdRow.Item("qualificationgrp") = cboQualificationGroup.Text
            dtIdRow.Item("qualification") = cboQualification.Text
            dtIdRow.Item("institute") = cboInstitution.Text
            dtIdRow.Item("resultcode") = cboResultCode.Text
            'Sohail (19 Mar 2020) -- End

            dtIdRow.Item("certificateno") = txtCertiNo.Text.Trim 'Sohail (22 Apr 2015)

            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            dtIdRow.Item("ishighestqualification") = CBool(chkHighestqualification.Checked)
            'Sohail (27 Apr 2017) -- End

            dtIdRow.Item("AUD") = "A"
            dtIdRow.Item("GUID") = Guid.NewGuid().ToString
            'Sandeep [ 09 Oct 2010 ] -- End 


            mdtQualificationTran.Rows.Add(dtIdRow)

            Call FillApplicantQualificationList()
            Call ResetApplicantQualification()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnQualifyAdd_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnQualifyEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQualifyEdit.Click


        If lvQualificationList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Please select Qualification Detail from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvQualificationList.Select()
            Exit Sub
        End If


        'Pinkal (06-Feb-2012) -- Start
        'Enhancement : TRA Changes

        'Sohail (19 Mar 2020) -- Start
        'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
        'If pnlOtherQualification.Visible Then

        '    If txtOtherQualificationGrp.Text.Trim.Length = 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Other Qualification Group can not be blank. Other Qualification Group is required information."), enMsgBoxStyle.Information)
        '        txtOtherQualificationGrp.Select()
        '        Exit Sub
        '    End If
        '    If txtOtherQualification.Text.Trim.Length = 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Other Qualification can not be blank. Other Qualification is required information."), enMsgBoxStyle.Information)
        '        txtOtherQualification.Select()
        '        Exit Sub
        '    End If
        '    If txtOtherResultCode.Text.Trim.Length = 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, " Other Result Code can not be blank. Other Result Code  is required information."), enMsgBoxStyle.Information)
        '        txtOtherResultCode.Select()
        '        Exit Sub
        '    End If
        '    If txtOtherInstitute.Text.Trim.Length = 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Other Institute can not be blank. Other Institute is required information."), enMsgBoxStyle.Information)
        '        txtOtherInstitute.Select()
        '        Exit Sub
        '    End If
        'Else
        '    If CInt(cboQualificationGroup.SelectedValue) <= 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Qualification Group is compulsory information, it can not be blank. Please select Qualification Group to continue."), enMsgBoxStyle.Information)
        '        cboQualificationGroup.Focus()
        '        Exit Sub
        '    End If
        '    If CInt(cboQualification.SelectedValue) <= 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Qualification is compulsory information, it can not be blank. Please select Qualification to continue."), enMsgBoxStyle.Information)
        '        cboQualification.Focus()
        '        Exit Sub
        '    End If
        '    If CInt(cboResultCode.SelectedValue) <= 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Result Code is compulsory information, it can not be blank. Please select Result Code to continue."), enMsgBoxStyle.Information)
        '        cboResultCode.Focus() 'Sohail (11 Sep 2010)
        '        Exit Sub
        '    End If
        '    If CInt(cboInstitution.SelectedValue) <= 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Institute is compulsory information. Please select Institute to continue."), enMsgBoxStyle.Information)
        '        cboInstitution.Focus()
        '        Exit Sub
        '    End If

        'End If
        If IsValidQualification() = False Then Exit Sub
        'Sohail (19 Mar 2020) -- End
        'Pinkal (06-Feb-2012) -- End

        'Sandeep [ 09 Oct 2010 ] -- Start
        'If dtpQualifyDate.Checked = True AndAlso dtpQualifyDate.Value > DateTime.Today.Date Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 137, "Qualify date should be less than current date."), enMsgBoxStyle.Information)
        '    dtpQualifyDate.Focus()
        '    Exit Sub
        'End If


        If dtpQEndDate.Checked = True AndAlso dtpQStartDate.Checked = True Then
            If dtpQEndDate.Value.Date <= dtpQStartDate.Value.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "End date cannot be less or equal to start date."), enMsgBoxStyle.Information)
                dtpQEndDate.Focus()
                Exit Sub
            End If
        End If


        Try
            'Sohail (11 Sep 2010) -- Start
            'If lvQualificationList.Items.Count > 0 Then
            If lvQualificationList.SelectedItems.Count > 0 Then
                'Sohail (11 Sep 2010) -- End
                If mintQualificationTypeId > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvQualificationList.Items(mintQualificationTypeId).Tag) = -1 Then
                        drTemp = mdtQualificationTran.Select("GUID = '" & lvQualificationList.Items(mintQualificationTypeId).SubItems(objcolhQualifyGUID.Index).Text & "'")

                    Else


                        'Pinkal (06-Feb-2012) -- Start
                        'Enhancement : TRA Changes

                        If pnlOtherQualification.Visible Then
                            drTemp = mdtQualificationTran.Select("other_qualification = '" & lvQualificationList.Items(mintQualificationTypeId).SubItems(colhQualificationName.Index).Text & "'")
                        Else
                            drTemp = mdtQualificationTran.Select("qualificationtranunkid = " & CInt(lvQualificationList.Items(mintQualificationTypeId).Tag) & "")
                        End If


                        'Pinkal (06-Feb-2012) -- End

                    End If
                    If drTemp.Length > 0 Then
                        With drTemp(0)

                            'Sandeep [ 09 Oct 2010 ] -- Start
                            '.Item("qualificationtranunkid") = lvQualificationList.Items(mintQualificationTypeId).Tag
                            '.Item("applicantunkid") = mintApplicantunkid
                            '.Item("qualificationgroupunkid") = CInt(cboQualificationGroup.SelectedValue)
                            '.Item("qualificationunkid") = CInt(cboQualification.SelectedValue)
                            'If dtpQualifyDate.Checked = True Then
                            '    .Item("qualificationdate") = dtpQualifyDate.Value
                            'Else
                            '    .Item("qualificationdate") = DBNull.Value
                            'End If

                            '.Item("award") = txtAward.Text
                            '.Item("referenceno") = txtReferenceNo.Text
                            '.Item("institution_name") = txtInstitution.Text
                            '.Item("address") = txtInstituteAddress.Text
                            '.Item("contactperson") = txtContactPerson.Text
                            '.Item("contactno") = txtContactNo.Text
                            '.Item("remark") = txtQualificationRemark.Text
                            '.Item("contactperson") = txtContactPerson.Text
                            '.Item("GUID") = Guid.NewGuid().ToString

                            .Item("qualificationtranunkid") = lvQualificationList.Items(mintQualificationTypeId).Tag
                            .Item("applicantunkid") = mintApplicantunkid
                            .Item("qualificationgroupunkid") = CInt(cboQualificationGroup.SelectedValue)
                            .Item("qualificationunkid") = CInt(cboQualification.SelectedValue)

                            'Pinkal (06-Feb-2012) -- Start
                            'Enhancement : TRA Changes
                            '.Item("transaction_date") = dtpQualifyDate.Value
                            .Item("transaction_date") = ConfigParameter._Object._CurrentDateAndTime
                            'Pinkal (06-Feb-2012) -- End


                            .Item("reference_no") = txtReferenceNo.Text
                            If dtpQStartDate.Checked = True Then
                                .Item("award_start_date") = dtpQStartDate.Value
                            Else
                                .Item("award_start_date") = DBNull.Value
                            End If
                            If dtpQEndDate.Checked = True Then
                                .Item("award_end_date") = dtpQEndDate.Value
                                'Sohail (27 Apr 2017) -- Start
                                'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                                .Item("award_end_date_string") = eZeeDate.convertDate(dtpQEndDate.Value).ToString
                                'Sohail (27 Apr 2017) -- End
                            Else
                                .Item("award_end_date") = DBNull.Value
                                'Sohail (27 Apr 2017) -- Start
                                'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                                .Item("award_end_date_string") = "88880101"
                                'Sohail (27 Apr 2017) -- End
                            End If
                            .Item("instituteunkid") = CInt(cboInstitution.SelectedValue)


                            'Pinkal (12-Oct-2011) -- Start
                            'Enhancement : TRA Changes
                            .Item("resultunkid") = CInt(cboResultCode.SelectedValue)
                            .Item("gpacode") = nudGPA.Value
                            'Pinkal (12-Oct-2011) -- End


                            .Item("remark") = txtQualificationRemark.Text



                            'Pinkal (06-Feb-2012) -- Start
                            'Enhancement : TRA Changes
                            .Item("other_qualificationgrp") = txtOtherQualificationGrp.Text.Trim
                            .Item("other_qualification") = txtOtherQualification.Text.Trim
                            .Item("other_institute") = txtOtherInstitute.Text.Trim
                            .Item("other_resultcode") = txtOtherResultCode.Text.Trim
                            'Pinkal (06-Feb-2012) -- End

                            'Sohail (19 Mar 2020) -- Start
                            'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
                            .Item("qualificationgrp") = cboQualificationGroup.Text
                            .Item("qualification") = cboQualification.Text
                            .Item("institute") = cboInstitution.Text
                            .Item("resultcode") = cboResultCode.Text
                            'Sohail (19 Mar 2020) -- End

                            .Item("certificateno") = txtCertiNo.Text.Trim 'Sohail (22 Apr 2015)

                            'Sohail (27 Apr 2017) -- Start
                            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                            .Item("ishighestqualification") = CBool(chkHighestqualification.Checked)
                            'Sohail (27 Apr 2017) -- End

                            .Item("GUID") = Guid.NewGuid().ToString
                            'Sandeep [ 09 Oct 2010 ] -- End 

                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            .AcceptChanges()

                        End With
                        Call FillApplicantQualificationList()
                    End If
                End If
                Call ResetApplicantQualification()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnQualifyEdit_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnQualifyDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQualifyDelete.Click
        'Sohail (11 Sep 2010) -- Start
        If lvQualificationList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Please select Qualification Detail from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvQualificationList.Select()
            Exit Sub
        End If
        'Sohail (11 Sep 2010) -- End
        Try
            If lvQualificationList.SelectedItems.Count > 0 Then
                If mintQualificationTypeId > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvQualificationList.Items(mintQualificationTypeId).Tag) = -1 Then
                        drTemp = mdtQualificationTran.Select("GUID = '" & lvQualificationList.Items(mintQualificationTypeId).SubItems(objcolhQualifyGUID.Index).Text & "'")
                    Else

                        'Pinkal (06-Feb-2012) -- Start
                        'Enhancement : TRA Changes

                        If pnlOtherQualification.Visible Then
                            drTemp = mdtQualificationTran.Select("other_qualification = '" & lvQualificationList.Items(mintQualificationTypeId).SubItems(colhQualificationName.Index).Text & "'")
                        Else
                            drTemp = mdtQualificationTran.Select("qualificationtranunkid = " & CInt(lvQualificationList.Items(mintQualificationTypeId).Tag) & "")
                        End If

                        'Pinkal (06-Feb-2012) -- End

                    End If
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        Call FillApplicantQualificationList()
                    End If
                End If
            End If
            Call ResetApplicantQualification()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    'Sohail (22 Apr 2015) -- Start
    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    'Private Sub lvQualificationList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvQualificationList.Click
    Private Sub lvQualificationList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvQualificationList.SelectedIndexChanged
        'Sohail (22 Apr 2015) -- End

        Try
            'Sohail (22 Apr 2015) -- Start
            'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            'If lvQualificationList.Items.Count > 0 Then
            If lvQualificationList.SelectedItems.Count > 0 Then
                'Sohail (22 Apr 2015) -- End
                mintQualificationTypeId = lvQualificationList.SelectedItems(0).Index

                'Sandeep [ 09 Oct 2010 ] -- Start
                'cboQualificationGroup.SelectedValue = CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhQualifyGroupunkid.Index).Text)
                'cboQualification.SelectedValue = CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhQualifyunkid.Index).Text)
                ''Sohail (11 Sep 2010) -- Start
                ''txtAward.Text = lvQualificationList.SelectedItems(0).SubItems(colhQualification.Index).Text
                'txtAward.Text = lvQualificationList.SelectedItems(0).SubItems(colhAward.Index).Text
                ''Sohail (11 Sep 2010) -- End
                'txtReferenceNo.Text = lvQualificationList.SelectedItems(0).SubItems(colhRefNo.Index).Text
                'If lvQualificationList.SelectedItems(0).SubItems(colhQualifyDate.Index).Text = "" Then
                '    dtpQualifyDate.Value = Now.Date
                '    dtpQualifyDate.Checked = False
                'Else
                '    dtpQualifyDate.Checked = True
                '    dtpQualifyDate.Value = CDate(lvQualificationList.SelectedItems(0).SubItems(colhQualifyDate.Index).Text)
                'End If

                'txtInstituteAddress.Text = lvQualificationList.SelectedItems(0).SubItems(colhAddress.Index).Text
                'txtInstitution.Text = lvQualificationList.SelectedItems(0).SubItems(colhInstitution.Index).Text
                'txtQualificationRemark.Text = lvQualificationList.SelectedItems(0).SubItems(colhRemark.Index).Text
                'txtContactNo.Text = lvQualificationList.SelectedItems(0).SubItems(colhContactNo.Index).Text
                'txtContactPerson.Text = lvQualificationList.SelectedItems(0).SubItems(colhContactPerson.Index).Text



                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes

                'Sohail (19 Mar 2020) -- Start
                'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
                'If CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhQualifyGroupunkid.Index).Text) <= 0 AndAlso CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhQualifyunkid.Index).Text) <= 0 AndAlso _
                '    CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhInstituteId.Index).Text) <= 0 Then
                '    pnlOtherQualification.Visible = True
                '    lnkOtherQualification.Enabled = True
                '    txtOtherQualificationGrp.Text = lvQualificationList.SelectedItems(0).Text.Trim
                '    txtOtherQualification.Text = lvQualificationList.SelectedItems(0).SubItems(colhQualificationName.Index).Text.Trim
                '    txtOtherInstitute.Text = lvQualificationList.SelectedItems(0).SubItems(colhInstitute.Index).Text
                '    txtOtherResultCode.Text = lvQualificationList.SelectedItems(0).SubItems(objcolhResultCodeID.Index).Text.Trim
                '    cboQualificationGroup.SelectedValue = 0
                '    cboQualification.SelectedValue = 0
                '    cboInstitution.SelectedValue = 0
                '    cboResultCode.SelectedValue = 0
                'Else
                '    pnlOtherQualification.Visible = False
                '    lnkOtherQualification.Enabled = False
                '    cboQualificationGroup.SelectedValue = CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhQualifyGroupunkid.Index).Text)
                '    cboQualification.SelectedValue = CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhQualifyunkid.Index).Text)
                '    txtReferenceNo.Text = CStr(lvQualificationList.SelectedItems(0).SubItems(colhRefNo.Index).Text)
                '    cboInstitution.SelectedValue = CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhInstituteId.Index).Text)
                '    cboResultCode.SelectedValue = CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhResultCodeID.Index).Text)
                '    txtOtherQualificationGrp.Text = ""
                '    txtOtherQualification.Text = ""
                '    txtOtherResultCode.Text = ""
                '    txtOtherInstitute.Text = ""
                'End If
                If CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhQualifyGroupunkid.Index).Text) <= 0 Then
                    txtOtherQualificationGrp.Text = lvQualificationList.SelectedItems(0).Text.Trim
                    cboQualificationGroup.SelectedValue = 0
                    chkqualificationgroup.Checked = True
                Else
                    cboQualificationGroup.SelectedValue = CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhQualifyGroupunkid.Index).Text)
                    txtOtherQualificationGrp.Text = ""
                    chkqualificationgroup.Checked = False
                End If
                If CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhQualifyunkid.Index).Text) <= 0 Then
                    txtOtherQualification.Text = lvQualificationList.SelectedItems(0).SubItems(colhQualificationName.Index).Text.Trim
                    cboQualification.SelectedValue = 0
                    chkqualificationAward.Checked = True
                Else
                    cboQualification.SelectedValue = CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhQualifyunkid.Index).Text)
                    txtOtherQualification.Text = ""
                    chkqualificationAward.Checked = False
                End If
                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                'If CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhResultCodeID.Index).Text) <= 0 Then
                If CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhResultCodeID.Index).Tag) <= 0 Then
                    'Sohail (25 Sep 2020) -- End
                    txtOtherResultCode.Text = lvQualificationList.SelectedItems(0).SubItems(objcolhResultCodeID.Index).Text.Trim
                    cboResultCode.SelectedValue = 0
                    chkResultCode.Checked = True
                Else
                    'Sohail (25 Sep 2020) -- Start
                    'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    'cboResultCode.SelectedValue = CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhResultCodeID.Index).Text)
                    cboResultCode.SelectedValue = CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhResultCodeID.Index).Tag)
                    'Sohail (25 Sep 2020) -- End
                    txtOtherResultCode.Text = ""
                    chkResultCode.Checked = False
                End If
                If CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhInstituteId.Index).Text) <= 0 Then
                    txtOtherInstitute.Text = lvQualificationList.SelectedItems(0).SubItems(colhInstitute.Index).Text
                    cboInstitution.SelectedValue = 0
                    chkInstitute.Checked = True
                Else
                    cboInstitution.SelectedValue = CInt(lvQualificationList.SelectedItems(0).SubItems(objcolhInstituteId.Index).Text)
                    txtOtherInstitute.Text = ""
                    chkInstitute.Checked = False
                End If
                'Sohail (19 Mar 2020) -- End

                dtpQualifyDate.Value = CDate(lvQualificationList.SelectedItems(0).SubItems(objcolhTransdate.Index).Text)
                If lvQualificationList.SelectedItems(0).SubItems(colhStartDate.Index).Text <> "" Then
                    dtpQStartDate.Value = CDate(lvQualificationList.SelectedItems(0).SubItems(colhStartDate.Index).Text)
                    dtpQStartDate.Checked = True
                Else
                    dtpQStartDate.Checked = False
                End If
                If lvQualificationList.SelectedItems(0).SubItems(colhEndDate.Index).Text <> "" Then
                    dtpQEndDate.Value = CDate(lvQualificationList.SelectedItems(0).SubItems(colhEndDate.Index).Text)
                    dtpQEndDate.Checked = True
                Else
                    dtpQEndDate.Checked = False
                End If
                txtQualificationRemark.Text = CStr(lvQualificationList.SelectedItems(0).SubItems(colhRemark.Index).Text)
                nudGPA.Value = CDec(lvQualificationList.SelectedItems(0).SubItems(objColhgpacode.Index).Text)
                'Sohail (27 Apr 2017) -- Start
                'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
                chkHighestqualification.Checked = CBool(lvQualificationList.SelectedItems(0).SubItems(objcolhHighestQualification.Index).Text)
                'Sohail (27 Apr 2017) -- End

                'Sohail (22 Apr 2015) -- Start
                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            Else
                Call ResetApplicantQualification()
                'Sohail (22 Apr 2015) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvQualificationList_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkOtherQualification_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkOtherQualification.LinkClicked
        Try
            If pnlOtherQualification.Visible = False Then
                pnlOtherQualification.Visible = True
                'Sohail (22 Apr 2015) -- Start
                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            Else
                pnlOtherQualification.Visible = False
                'Sohail (22 Apr 2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkOtherQualification_LinkClicked", mstrModuleName)
        End Try
    End Sub

    'Sohail (19 Mar 2020) -- Start
    'NMB Enhancement # : Separate other option for qualification group, qualification, result and institute for applicant in recruitment.
    Private Sub chkqualificationgroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkqualificationgroup.CheckedChanged
        Try
            cboQualificationGroup.Visible = Not chkqualificationgroup.Checked
            txtOtherQualificationGrp.Visible = chkqualificationgroup.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkqualificationgroup_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkqualificationAward_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkqualificationAward.CheckedChanged
        Try
            cboQualification.Visible = Not chkqualificationAward.Checked
            txtOtherQualification.Visible = chkqualificationAward.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkqualificationAward_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkResultCode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkResultCode.CheckedChanged
        Try
            cboResultCode.Visible = Not chkResultCode.Checked
            txtOtherResultCode.Visible = chkResultCode.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkResultCode_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkInstitute_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInstitute.CheckedChanged
        Try
            cboInstitution.Visible = Not chkInstitute.Checked
            txtOtherInstitute.Visible = chkInstitute.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkInstitute_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (19 Mar 2020) -- End

#End Region

#Region " Job History Tab "

    Private Sub btnJobHistoryAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnJobHistoryAdd.Click
        If txtCompanyName.Text.Trim = "" Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Company Name is compulsory information, it can not be blank. Please give Company Name to continue."), enMsgBoxStyle.Information)
            txtCompanyName.Focus() 'Sohail (11 Sep 2010) 
            Exit Sub

            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes

        ElseIf dtpJoinDate.Checked = False Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 42, "Joining Date is compulsory information.Please Check Joining Date."), enMsgBoxStyle.Information)
            dtpJoinDate.Select()
            Exit Sub

            'Pinkal (25-APR-2012) -- End

        End If
        'Sohail (11 Sep 2010) -- Start
        If dtpJoinDate.Checked = True AndAlso dtpJoinDate.Value > DateTime.Today.Date Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Joining Date should be less than current Date."), enMsgBoxStyle.Information)
            dtpJoinDate.Focus()
            Exit Sub
            ''ElseIf dtpTerminationDate.Checked = True AndAlso dtpTerminationDate.Value > DateTime.Today.Date Then
            ''    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Termination Date should be less than current Date."), enMsgBoxStyle.Information)
            ''    dtpTerminationDate.Focus()
            ''    Exit Sub
        ElseIf dtpTerminationDate.Checked = True AndAlso dtpJoinDate.Checked = True Then
            If dtpTerminationDate.Value < dtpJoinDate.Value Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Termination should be greater than Join Date."), enMsgBoxStyle.Information)
                dtpTerminationDate.Focus()
                Exit Sub
            End If
        End If
        'Sohail (11 Sep 2010) -- End

        'Pinkal (25-APR-2012) -- Start
        'Enhancement : TRA Changes
        'Dim dtRow As DataRow() = mdtJobHistory.Select("companyname = '" & txtCompanyName.Text & "'" & " AND AUD <> 'D' ")
        Dim dtRow As DataRow() = mdtJobHistory.Select("companyname = '" & txtCompanyName.Text.Trim & "'" & " AND AUD <> 'D' AND joiningdate ='" & dtpJoinDate.Value.Date & "'")
        'Pinkal (25-APR-2012) -- End

        If dtRow.Length > 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Selected company is already added to the list."), enMsgBoxStyle.Information)
            Exit Sub
        End If
        Try
            Dim dtIdRow As DataRow
            dtIdRow = mdtJobHistory.NewRow

            dtIdRow.Item("jobhistorytranunkid") = -1
            dtIdRow.Item("applicantunkid") = mintApplicantunkid
            dtIdRow.Item("employername") = txtEmployerName.Text
            dtIdRow.Item("companyname") = txtCompanyName.Text
            If dtpJoinDate.Checked = True Then
                'Pinkal (25-APR-2012) -- Start
                'Enhancement : TRA Changes
                'dtIdRow.Item("joiningdate") = dtpJoinDate.Value
                dtIdRow.Item("joiningdate") = dtpJoinDate.Value.Date
                'Pinkal (25-APR-2012) -- End
            Else
                dtIdRow.Item("joiningdate") = DBNull.Value
            End If
            If dtpTerminationDate.Checked = True Then
                'Pinkal (25-APR-2012) -- Start
                'Enhancement : TRA Changes
                'dtIdRow.Item("terminationdate") = dtpTerminationDate.Value
                dtIdRow.Item("terminationdate") = dtpTerminationDate.Value.Date
                'Pinkal (25-APR-2012) -- End
            Else
                dtIdRow.Item("terminationdate") = DBNull.Value
            End If
            dtIdRow.Item("designation") = txtDesignation.Text
            dtIdRow.Item("responsibility") = txtResponsibility.Text
            dtIdRow.Item("officephone") = txtOfficePhone.Text
            dtIdRow.Item("leavingreason") = txtLeavingReason.Text
            dtIdRow.Item("achievements") = txtJobAchievement.Text.Trim 'Sohail (22 Apr 2015)
            dtIdRow.Item("GUID") = Guid.NewGuid().ToString
            dtIdRow.Item("AUD") = "A"

            mdtJobHistory.Rows.Add(dtIdRow)

            Call FillApplicantsJobHistoryList()
            Call ResetJobHistory()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnJobHistoryAdd_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnJobHistoryEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnJobHistoryEdit.Click
        'Sohail (11 Sep 2010) -- Start
        If lvJobHistory.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Please select Job History from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvJobHistory.Select()
            Exit Sub
        End If
        If txtCompanyName.Text.Trim = "" Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Company Name is compulsory information, it can not be blank. Please give Company Name to continue."), enMsgBoxStyle.Information)
            txtCompanyName.Focus()
            Exit Sub

            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes

        ElseIf dtpJoinDate.Checked = False Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 42, "Joining Date is compulsory information.Please Check Joining Date."), enMsgBoxStyle.Information)
            dtpJoinDate.Select()
            Exit Sub

            'Pinkal (25-APR-2012) -- End

        End If
        If dtpJoinDate.Checked = True AndAlso dtpJoinDate.Value > DateTime.Today.Date Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Joining Date should be less than current Date."), enMsgBoxStyle.Information)
            dtpJoinDate.Focus()
            Exit Sub
        ElseIf dtpTerminationDate.Checked = True AndAlso dtpTerminationDate.Value > DateTime.Today.Date Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Termination Date should be less than current Date."), enMsgBoxStyle.Information)
            dtpTerminationDate.Focus()
            Exit Sub
        ElseIf dtpTerminationDate.Checked = True AndAlso dtpJoinDate.Checked = True Then
            If dtpTerminationDate.Value < dtpJoinDate.Value Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Termination should be greater than Join Date."), enMsgBoxStyle.Information)
                dtpTerminationDate.Focus()
                Exit Sub
            End If
        End If
        'Sohail (11 Sep 2010) -- End
        Try
            If lvJobHistory.SelectedItems.Count > 0 Then
                If mintJobHistoryTypeId > -1 Then


                    Dim drRow As DataRow()
                    If CInt(lvJobHistory.Items(mintJobHistoryTypeId).Tag) = -1 Then
                        drRow = mdtJobHistory.Select("companyname = '" & txtCompanyName.Text.Trim & "'" & " AND AUD <> 'D' AND joiningdate ='" & dtpJoinDate.Value.Date & "' AND GUID <> '" & lvJobHistory.Items(mintJobHistoryTypeId).SubItems(objcolhJobhistoryGUID.Index).Text & "'")
                        If drRow.Length > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Selected company is already added to the list."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    Else
                        drRow = mdtJobHistory.Select("jobhistorytranunkid <> " & CInt(lvJobHistory.Items(mintJobHistoryTypeId).Tag) & " AND companyname = '" & txtCompanyName.Text.Trim & "'" & " AND AUD <> 'D' AND joiningdate ='" & dtpJoinDate.Value.Date & "'")
                        If drRow.Length > 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Selected company is already added to the list."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If


                    Dim drTemp As DataRow()
                    If CInt(lvJobHistory.Items(mintJobHistoryTypeId).Tag) = -1 Then
                        drTemp = mdtJobHistory.Select("GUID = '" & lvJobHistory.Items(mintJobHistoryTypeId).SubItems(objcolhJobhistoryGUID.Index).Text & "'")

                    Else
                        'Sohail (11 Sep 2010) -- Start
                        'drTemp = mdtJobHistory.Select("jobhistorytranunkid = '" & CInt(lvJobHistory.Items(mintJobHistoryTypeId).Tag) & "'")
                        drTemp = mdtJobHistory.Select("jobhistorytranunkid = " & CInt(lvJobHistory.Items(mintJobHistoryTypeId).Tag) & "")
                        'Sohail (11 Sep 2010) -- End
                    End If

                    If drTemp.Length > 0 Then
                        With drTemp(0)
                            .Item("jobhistorytranunkid") = lvJobHistory.Items(mintJobHistoryTypeId).Tag
                            .Item("applicantunkid") = mintApplicantunkid
                            .Item("employername") = txtEmployerName.Text
                            .Item("companyname") = txtCompanyName.Text
                            If dtpJoinDate.Checked = True Then
                                .Item("joiningdate") = dtpJoinDate.Value
                            Else
                                .Item("joiningdate") = DBNull.Value
                            End If

                            If dtpTerminationDate.Checked = True Then
                                .Item("terminationdate") = dtpTerminationDate.Value
                            Else
                                .Item("terminationdate") = DBNull.Value
                            End If

                            .Item("designation") = txtDesignation.Text
                            .Item("responsibility") = txtResponsibility.Text
                            .Item("officephone") = txtOfficePhone.Text
                            .Item("leavingreason") = txtLeavingReason.Text
                            .Item("achievements") = txtJobAchievement.Text.Trim 'Sohail (22 Apr 2015)
                            .Item("GUID") = Guid.NewGuid().ToString
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            .AcceptChanges()
                        End With
                        Call FillApplicantsJobHistoryList()
                    End If
                End If
                Call ResetJobHistory()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnJobHistoryEdit_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnJobHistoryDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnJobHistoryDelete.Click
        'Sohail (11 Sep 2010) -- Start
        If lvJobHistory.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Please select Job History from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvJobHistory.Select()
            Exit Sub
        End If
        'Sohail (11 Sep 2010) -- End
        Try
            If lvJobHistory.SelectedItems.Count > 0 Then
                If mintJobHistoryTypeId > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvJobHistory.Items(mintJobHistoryTypeId).Tag) = -1 Then
                        drTemp = mdtJobHistory.Select("GUID = '" & lvJobHistory.Items(mintJobHistoryTypeId).SubItems(objcolhJobhistoryGUID.Index).Text & "'")
                    Else
                        'Sohail (11 Sep 2010) -- Start
                        'drTemp = mdtJobHistory.Select("jobhistorytranunkid = '" & CInt(lvJobHistory.Items(mintJobHistoryTypeId).Tag) & "'")
                        drTemp = mdtJobHistory.Select("jobhistorytranunkid = " & CInt(lvJobHistory.Items(mintJobHistoryTypeId).Tag) & "")
                        'Sohail (11 Sep 2010) -- End
                    End If

                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        Call FillApplicantsJobHistoryList()
                    End If
                End If
            End If
            Call ResetJobHistory()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnJobHistoryDelete_Click", mstrModuleName)
        End Try

    End Sub

    'Sohail (22 Apr 2015) -- Start
    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    'Private Sub lvJobHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvJobHistory.Click
    Private Sub lvJobHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvJobHistory.SelectedIndexChanged
        'Sohail (22 Apr 2015) -- End

        Try
            'Sohail (22 Apr 2015) -- Start
            'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            'If lvJobHistory.Items.Count > 0 Then
            If lvJobHistory.SelectedItems.Count > 0 Then
                'Sohail (22 Apr 2015) -- End
                mintJobHistoryTypeId = lvJobHistory.SelectedItems(0).Index
                txtEmployerName.Text = lvJobHistory.SelectedItems(0).SubItems(colhEmployer.Index).Text
                txtCompanyName.Text = lvJobHistory.SelectedItems(0).SubItems(colhCompanyName.Index).Text
                txtOfficePhone.Text = lvJobHistory.SelectedItems(0).SubItems(colhOfficePhone.Index).Text
                txtDesignation.Text = lvJobHistory.SelectedItems(0).SubItems(colhDesignation.Index).Text
                txtResponsibility.Text = lvJobHistory.SelectedItems(0).SubItems(colhResponsibility.Index).Text
                txtLeavingReason.Text = lvJobHistory.SelectedItems(0).SubItems(colhLeavingReason.Index).Text
                txtJobAchievement.Text = lvJobHistory.SelectedItems(0).SubItems(objcolhJobAchievement.Index).Text 'Sohail (22 Apr 2015)
                If lvJobHistory.SelectedItems(0).SubItems(colhJoinDate.Index).Text = "" Then
                    dtpJoinDate.Value = Now.Date
                    dtpJoinDate.Checked = False
                Else
                    dtpJoinDate.Checked = True
                    dtpJoinDate.Value = CDate(lvJobHistory.SelectedItems(0).SubItems(colhJoinDate.Index).Text)
                End If

                If lvJobHistory.SelectedItems(0).SubItems(colhTermDate.Index).Text = "" Then
                    dtpTerminationDate.Value = Now.Date
                    dtpTerminationDate.Checked = False
                Else
                    dtpTerminationDate.Checked = True
                    dtpTerminationDate.Value = CDate(lvJobHistory.SelectedItems(0).SubItems(colhTermDate.Index).Text)
                End If

                'Sohail (22 Apr 2015) -- Start
                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            Else
                Call ResetJobHistory()
                'Sohail (22 Apr 2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvJobHistory_Click", mstrModuleName)
        End Try

    End Sub
#End Region

#Region "References Tab"

    Private Sub cboRefCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRefCountry.SelectedIndexChanged
        Try
            If CInt(cboRefCountry.SelectedValue) > 0 Then
                Dim objState As New clsstate_master
                Dim dsList As New DataSet

                dsList = objState.GetList("State", , True, CInt(cboRefCountry.SelectedValue))
                With cboRefState
                    .ValueMember = "stateunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("State")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboRefCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboRefState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRefState.SelectedIndexChanged
        Try
            If CInt(cboRefState.SelectedValue) > 0 Then
                Dim objCity As New clscity_master
                Dim dslist As New DataSet
                dslist = objCity.GetList("City", , True, CInt(cboRefState.SelectedValue))
                With cboReftown
                    .ValueMember = "cityunkid"
                    .DisplayMember = "name"
                    .DataSource = dslist.Tables("City")
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboRefState_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub btnRefAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefAdd.Click
        Try
            'S.SANDEEP [ 11 MAR 2014 ] -- START
            'Dim strExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            'S.SANDEEP [ 11 MAR 2014 ] -- END
            If txtReferenceName.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Name cannot be blank.Name is required information."), enMsgBoxStyle.Information)
                txtReferenceName.Focus()
                Exit Sub
            End If
            If txtRefEmail.Text.Length > 0 Then
                'S.SANDEEP [ 11 MAR 2014 ] -- START
                'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
                Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
                'S.SANDEEP [ 11 MAR 2014 ] -- END
                If Expression.IsMatch(txtRefEmail.Text.Trim) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Invalid Email.Please Enter Valid Email"), enMsgBoxStyle.Information)
                    txtRefEmail.Focus()
                    Exit Sub
                End If
            End If

            Dim dtRow As DataRow() = mdtReferenceTran.Select("name = '" & txtReferenceName.Text.Trim & "'" & " AND AUD <> 'D' ")

            If dtRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "This name is already added to the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dtIdRow As DataRow
            dtIdRow = mdtReferenceTran.NewRow

            dtIdRow.Item("referencetranunkid") = -1
            dtIdRow.Item("applicantunkid") = mintApplicantunkid
            dtIdRow.Item("name") = txtReferenceName.Text.Trim
            dtIdRow.Item("position") = txtRefPosition.Text.Trim
            dtIdRow.Item("address") = txtRefAddress.Text.Trim
            dtIdRow.Item("countryunkid") = CInt(cboRefCountry.SelectedValue)
            dtIdRow.Item("stateunkid") = CInt(cboRefState.SelectedValue)
            dtIdRow.Item("cityunkid") = CInt(cboReftown.SelectedValue)
            dtIdRow.Item("email") = txtRefEmail.Text.Trim
            dtIdRow.Item("genderunkid") = CInt(cboRefGender.SelectedValue)
            If CInt(cboRefGender.SelectedValue) > 0 Then
                dtIdRow.Item("gender") = cboRefGender.Text.Trim
            Else
                dtIdRow.Item("gender") = ""
            End If
            dtIdRow.Item("telephone_no") = txtRefTelNo.Text.Trim
            dtIdRow.Item("mobile_no") = txtRefMobileNo.Text.Trim
            dtIdRow.Item("relationunkid") = CInt(cboRefType.SelectedValue)

            If CInt(cboRefType.SelectedValue) > 0 Then
                dtIdRow.Item("relation") = cboRefType.Text
            Else
                dtIdRow.Item("relation") = ""
            End If
            dtIdRow.Item("GUID") = Guid.NewGuid().ToString
            dtIdRow.Item("AUD") = "A"

            mdtReferenceTran.Rows.Add(dtIdRow)

            Call FillApplicantReferencesList()
            Call ResetApplicantReferences()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnRefAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnRefEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefEdit.Click
        Try
            'S.SANDEEP [ 11 MAR 2014 ] -- START
            'Dim strExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            'S.SANDEEP [ 11 MAR 2014 ] -- END
            If txtReferenceName.Text.Trim.Length = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Name cannot be blank.Name is required information."), enMsgBoxStyle.Information)
                txtReferenceName.Focus()
                Exit Sub
            End If
            If txtRefEmail.Text.Length > 0 Then
                'S.SANDEEP [ 11 MAR 2014 ] -- START
                'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
                Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
                'S.SANDEEP [ 11 MAR 2014 ] -- END
                If Expression.IsMatch(txtRefEmail.Text.Trim) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Invalid Email.Please Enter Valid Email"), enMsgBoxStyle.Information)
                    txtRefEmail.Focus()
                    Exit Sub
                End If
            End If

            If lvReferences.SelectedItems.Count > 0 Then

                If mintReferencesTypeId > -1 Then

                    Dim drTemp As DataRow()
                    If CInt(lvReferences.Items(mintReferencesTypeId).Tag) = -1 Then
                        drTemp = mdtReferenceTran.Select("GUID = '" & lvReferences.Items(mintReferencesTypeId).SubItems(objColhRefGUID.Index).Text & "'")

                    Else
                        drTemp = mdtReferenceTran.Select("referencetranunkid = " & CInt(lvReferences.Items(mintReferencesTypeId).Tag) & "")
                    End If

                    If drTemp.Length > 0 Then

                        With drTemp(0)
                            .Item("referencetranunkid") = lvReferences.Items(mintReferencesTypeId).Tag
                            .Item("applicantunkid") = mintApplicantunkid
                            .Item("name") = txtReferenceName.Text.Trim
                            .Item("position") = txtRefPosition.Text.Trim
                            .Item("address") = txtRefAddress.Text.Trim
                            .Item("countryunkid") = CInt(cboRefCountry.SelectedValue)
                            .Item("stateunkid") = CInt(cboRefState.SelectedValue)
                            .Item("cityunkid") = CInt(cboReftown.SelectedValue)
                            .Item("email") = txtRefEmail.Text.Trim
                            .Item("genderunkid") = CInt(cboRefGender.SelectedValue)
                            If CInt(cboRefGender.SelectedValue) > 0 Then
                                .Item("gender") = cboRefGender.Text.Trim
                            Else
                                .Item("gender") = ""
                            End If
                            .Item("telephone_no") = txtRefTelNo.Text.Trim
                            .Item("mobile_no") = txtRefMobileNo.Text.Trim
                            .Item("relationunkid") = CInt(cboRefType.SelectedValue)
                            If CInt(cboRefType.SelectedValue) > 0 Then
                                .Item("relation") = cboRefType.Text
                            Else
                                .Item("relation") = ""
                            End If
                            .Item("GUID") = Guid.NewGuid().ToString
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                                .Item("AUD") = "U"
                            End If
                            .AcceptChanges()
                        End With
                        Call FillApplicantReferencesList()
                    End If
                End If
                ResetApplicantReferences()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnRefEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnRefDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefDelete.Click
        If lvReferences.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Please select Reference from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvJobHistory.Select()
            Exit Sub
        End If
        Try
            If lvReferences.SelectedItems.Count > 0 Then
                If mintReferencesTypeId > -1 Then
                    Dim drTemp As DataRow()
                    If CInt(lvReferences.Items(mintReferencesTypeId).Tag) = -1 Then
                        drTemp = mdtReferenceTran.Select("GUID = '" & lvReferences.Items(mintReferencesTypeId).SubItems(objColhRefGUID.Index).Text & "'")
                    Else
                        drTemp = mdtReferenceTran.Select("referencetranunkid = " & CInt(lvReferences.Items(mintReferencesTypeId).Tag) & "")
                    End If

                    If drTemp.Length > 0 Then

                        'If menAction = enAction.EDIT_ONE Then
                        '    Dim frm As New frmReasonSelection
                        '    Dim mstrVoidReason As String = String.Empty
                        '    frm.displayDialog(enVoidCategoryType.APPLICANT, mstrVoidReason)
                        '    If mstrVoidReason.Length <= 0 Then
                        '        Exit Sub
                        '    Else
                        '        drTemp(0)("AUD") = "D"
                        '        drTemp(0)("isvoid") = True
                        '        drTemp(0)("voiduserunkid") = User._Object._Userunkid
                        '        drTemp(0)("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        '        drTemp(0)("voidreason") = mstrVoidReason
                        '    End If
                        '    frm = Nothing
                        'Else
                        drTemp(0).Item("AUD") = "D"
                    End If

                    Call FillApplicantReferencesList()

                End If
            End If

            Call ResetApplicantReferences()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnJobHistoryDelete_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (22 Apr 2015) -- Start
    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    'Private Sub lvReferences_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvReferences.Click
    Private Sub lvReferences_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvReferences.SelectedIndexChanged
        'Sohail (22 Apr 2015) -- End
        Try
            If lvReferences.SelectedItems.Count > 0 Then
                mintReferencesTypeId = lvReferences.SelectedItems(0).Index
                txtReferenceName.Text = lvReferences.SelectedItems(0).SubItems(colhRefName.Index).Text
                txtRefPosition.Text = lvReferences.SelectedItems(0).SubItems(colhRefPosition.Index).Text
                txtRefAddress.Text = lvReferences.SelectedItems(0).SubItems(objcolhRefAddress.Index).Text
                cboRefCountry.SelectedValue = lvReferences.SelectedItems(0).SubItems(objcolhRefcountryId.Index).Text
                cboRefState.SelectedValue = lvReferences.SelectedItems(0).SubItems(objcolhRefStateId.Index).Text
                cboReftown.SelectedValue = lvReferences.SelectedItems(0).SubItems(objColhRefTownId.Index).Text
                cboRefType.SelectedValue = lvReferences.SelectedItems(0).SubItems(objColhRefTypeId.Index).Text
                cboRefGender.SelectedValue = lvReferences.SelectedItems(0).SubItems(objColhRefGenderId.Index).Text
                txtRefTelNo.Text = lvReferences.SelectedItems(0).SubItems(objcolhRefTelNo.Index).Text
                txtRefMobileNo.Text = lvReferences.SelectedItems(0).SubItems(objColhRefMobileNo.Index).Text
                txtRefEmail.Text = lvReferences.SelectedItems(0).SubItems(colhRefEmail.Index).Text
                'Sohail (22 Apr 2015) -- Start
                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            Else
                Call ResetApplicantReferences()
                'Sohail (22 Apr 2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvReferences_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmApplicantMaster_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objApplicant = Nothing
    End Sub

    Private Sub frmApplicantMaster_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSaveInfo.PerformClick()
        End If
    End Sub

    Private Sub frmApplicantMaster_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmApplicantsMaster_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objApplicant = New clsApplicant_master
        objSkill_Tran = New clsApplicantSkill_tran
        objQualification_Tran = New clsApplicantQualification_tran
        objJobHistory_Tran = New clsJobhistory
        'Sohail (25 Sep 2020) -- Start
        'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
        Dim objEmpSkill As clsEmployee_Skill_Tran
        Dim objEmpQuali As clsEmp_Qualification_Tran
        Dim objEmpJob As clsJobExperience_tran
        Dim objEmpRefree As clsEmployee_Refree_tran
        Dim dsList As DataSet
        'Sohail (25 Sep 2020) -- End


        'Pinkal (06-Feb-2012) -- Start
        'Enhancement : TRA Changes
        objReference_Tran = New clsrcapp_reference_Tran
        'Pinkal (06-Feb-2012) -- End


        'Call Language.setLanguage(Me.Name)
        'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

        Try
            Call Set_Logo(Me, gApplicationType)
            'Sohail (26 May 2021) -- Start
            'Issue :  : Languange issue on applicant master screen in desktop.
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Sohail (26 May 2021) -- End

            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objApplicant._Applicantunkid = mintApplicantunkid
                'Sohail (05 Dec 2016) -- Start
                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                txtEmail.ReadOnly = True
                'Sohail (05 Dec 2016) -- End
                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                mintEmployeeunkid = objApplicant._Employeeunkid
                'Sohail (25 Sep 2020) -- End

            End If


            'Anjan (22 Feb 2011)-Start
            imgImageControl._FilePath = ConfigParameter._Object._PhotoPath
            'Anjan (22 Feb 2011)-End



            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            txtEmployeeCode.Enabled = False
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            Call GetValue()


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            'If menAction = enAction.EDIT_ONE Then
            '    If objApplicant._OtherSkills.Length > 0 Then
            '        txtOtherSkill.Visible = True
            '        lblOtherSkill.Visible = True
            '    Else
            '        lblOtherSkill.Visible = False
            '        txtOtherSkill.Visible = False
            '    End If

            '    If objApplicant._OtherQualifications.Length > 0 Then
            '        txtOtherQualification.Visible = True
            '        lblOtherQualification.Visible = True
            '    Else
            '        lblOtherQualification.Visible = False
            '        txtOtherQualification.Visible = False
            '    End If



            'Else
            '    lblOtherSkill.Visible = False
            '    txtOtherSkill.Visible = False

            '    lblOtherQualification.Visible = False
            '    txtOtherQualification.Visible = False
            'End If

            'Pinkal (06-Feb-2012) -- End 
            If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
            objSkill_Tran._ApplicantUnkid = mintApplicantunkid
            mdtSkillTran = objSkill_Tran._DataList
                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            Else
                objEmpSkill = New clsEmployee_Skill_Tran
                dsList = objEmpSkill.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, "List", True, " hremp_app_skills_tran.emp_app_unkid = " & mintEmployeeunkid & " ")
                mdtSkillTran = dsList.Tables(0)
            End If
            'Sohail (25 Sep 2020) -- End
            Call FillApplicantSkillList()

            'Sohail (27 Apr 2017) -- Start
            'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
            'objQualification_Tran._ApplicantUnkid = mintApplicantunkid
            If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
            objQualification_Tran._ApplicantUnkid(ConfigParameter._Object._ApplicantQualificationSortByInRecruitment) = mintApplicantunkid
            'Sohail (27 Apr 2017) -- End
            mdtQualificationTran = objQualification_Tran._DataList
                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            Else
                objEmpQuali = New clsEmp_Qualification_Tran
                dsList = objEmpQuali.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, "List", True, "hremp_qualification_tran.employeeunkid = " & mintEmployeeunkid & " ")
                mdtQualificationTran = dsList.Tables(0)
            End If
            'Sohail (25 Sep 2020) -- End
            Call FillApplicantQualificationList()

            If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
            objJobHistory_Tran._Applicantunkid = mintApplicantunkid
            mdtJobHistory = objJobHistory_Tran._DataList
                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            Else
                objEmpJob = New clsJobExperience_tran
                dsList = objEmpJob.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, "List", True, "hremp_experience_tran.employeeunkid = " & mintEmployeeunkid & " ")
                mdtJobHistory = dsList.Tables(0)
            End If
            'Sohail (25 Sep 2020) -- End
            Call FillApplicantsJobHistoryList()



            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes 
            If mintEmployeeunkid <= 0 Then 'Sohail (25 Sep 2020)
            objReference_Tran._Applicantunkid = mintApplicantunkid
            mdtReferenceTran = objReference_Tran._dtReferences
                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            Else
                objEmpRefree = New clsEmployee_Refree_tran
                dsList = objEmpRefree.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, DateAndTime.Now.Date, DateAndTime.Now.Date, ConfigParameter._Object._UserAccessModeSetting, True, True, "List", True, True, "hremployee_referee_tran.employeeunkid = " & mintEmployeeunkid & " ")
                mdtReferenceTran = dsList.Tables(0)
            End If
            'Sohail (25 Sep 2020) -- End
            FillApplicantReferencesList()
            'Pinkal (06-Feb-2012) -- End

            txtApplicantCode.Focus()
            'Sandeep [ 21 Aug 2010 ] -- Start
            Call SetVisibility()
            'Sandeep [ 21 Aug 2010 ] -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApplicantsMaster_Load", mstrModuleName)
            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
        Finally
            objEmpSkill = Nothing
            objEmpQuali = Nothing
            objEmpJob = Nothing
            objEmpRefree = Nothing
            'Sohail (25 Sep 2020) -- End
        End Try

    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsApplicant_master.SetMessages()
            objfrm._Other_ModuleNames = "clsApplicant_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region


#Region " Button's Event "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnSaveInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveInfo.Click
        Dim blnFlag As Boolean = False
        Try
            If isValid() = False Then
                Exit Sub
            End If

            Call SetValue()


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            'If menAction = enAction.EDIT_ONE Then
            '    blnFlag = objApplicant.Update(mdtSkillTran, mdtQualificationTran, mdtJobHistory)
            'Else
            '    blnFlag = objApplicant.Insert(mdtSkillTran, mdtQualificationTran, mdtJobHistory)
            'End If

 With objApplicant
                ._FormName = mstrModuleName
                ._LoginEmployeeUnkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objApplicant.Update(mdtSkillTran, mdtQualificationTran, mdtJobHistory, mdtReferenceTran)
            Else

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objApplicant.Insert(mdtSkillTran, mdtQualificationTran, mdtJobHistory, mdtReferenceTran)
                blnFlag = objApplicant.Insert(ConfigParameter._Object._ApplicantCodeNotype, ConfigParameter._Object._ApplicantCodePrifix, mdtSkillTran, mdtQualificationTran, mdtJobHistory, mdtReferenceTran)
                'Shani(24-Aug-2015) -- End

            End If

            'Pinkal (06-Feb-2012) -- End

            If blnFlag = False And objApplicant._Message <> "" Then
                eZeeMsgBox.Show(objApplicant._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objApplicant = Nothing
                    objApplicant = New clsApplicant_master
                    Call GetValue()
                    txtApplicantCode.Focus()
                Else

                    mintApplicantunkid = objApplicant._Applicantunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveInfo_Click", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "ComboBox Event"

    Private Sub cboQualification_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQualification.SelectedIndexChanged
        Try

            If cboQualification.SelectedIndex < 0 Then Exit Sub

            Dim objQualification As New clsqualification_master
            Dim dsList As DataSet = objQualification.GetResultCodeFromQualification(cboQualification.SelectedValue.ToString(), True)
            cboResultCode.DisplayMember = "resultname"
            cboResultCode.ValueMember = "resultunkid"
            cboResultCode.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboQualification_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Control Events "

    Private Sub cboQualificationGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboQualificationGroup.SelectedIndexChanged

        Try
            'If CInt(cboQualificationGroup.SelectedValue) > 0 Then  'Sohail (11 Sep 2010) (to set selectedvalue = 0)
            Dim objQualification As New clsqualification_master
            Dim dsList As New DataSet

            'Sohail (20 Mar 2020) -- Start
            'NMB Enhancement # : System should allow multiple selection of qualification groups with AND condition and return the correct results when filter is applied.
            '2 employees, one with advance diploma and bachelor degree, and one with advance diploma only. then created filter, advance diploma AND bachelor degree AND gender (male). both were pulled despite one not having bachelor degree.
            'dsList = objQualification.GetComboList("List", True, CInt(cboQualificationGroup.SelectedValue))
            If CInt(cboQualificationGroup.SelectedValue) > 0 Then
            dsList = objQualification.GetComboList("List", True, CInt(cboQualificationGroup.SelectedValue))
            Else
                dsList = objQualification.GetComboList("List", True, -1)
            End If
            'Sohail (20 Mar 2020) -- End
            With cboQualification
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0 'Sohail (11 Sep 2010)
            End With

            'End If 'Sohail (11 Sep 2010)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboQualificationGroup_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub cboPermCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPermCountry.SelectedIndexChanged
        Try
            If CInt(cboPermCountry.SelectedValue) > 0 Then
                Dim objState As New clsstate_master
                Dim dsList As New DataSet

                dsList = objState.GetList("State", , True, CInt(cboPermCountry.SelectedValue))
                With cboPermState
                    .ValueMember = "stateunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("State")
                End With
                If menAction = enAction.EDIT_ONE Then
                    cboPermState.SelectedValue = objApplicant._Perm_Stateunkid
                Else
                    cboPermState.SelectedValue = 0
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPermCountry_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub
    Private Sub cboPermState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPermState.SelectedIndexChanged

        Try
            If CInt(cboPermState.SelectedValue) > 0 Then
                Dim objCity As New clscity_master
                Dim dslist As New DataSet
                dslist = objCity.GetList("City", , True, CInt(cboPermState.SelectedValue))
                With cboPermPostTown
                    .ValueMember = "cityunkid"
                    .DisplayMember = "name"
                    .DataSource = dslist.Tables("City")
                End With
                If menAction = enAction.EDIT_ONE Then
                    cboPermPostTown.SelectedValue = objApplicant._Perm_Post_Townunkid
                Else
                    cboPermPostTown.SelectedValue = 0
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPermState_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPermPostTown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPermPostTown.SelectedIndexChanged

        Try
            If CInt(cboPermPostTown.SelectedValue) > 0 Then
                Dim objZipcode As New clszipcode_master
                Dim dsList As New DataSet
                dsList = objZipcode.GetList("Zipcode", , True, CInt(cboPermPostTown.SelectedValue))
                With cboPermPostCode
                    .ValueMember = "zipcodeunkid"
                    .DisplayMember = "zipcode_no"
                    .DataSource = dsList.Tables("Zipcode")
                End With
                If menAction = enAction.EDIT_ONE Then
                    cboPermPostCode.SelectedValue = objApplicant._Perm_ZipCode
                Else
                    cboPermPostCode.SelectedValue = 0
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged

        Try
            If CInt(cboCountry.SelectedValue) > 0 Then
                Dim objState As New clsstate_master
                Dim dsList As New DataSet

                dsList = objState.GetList("State", , True, CInt(cboCountry.SelectedValue))
                With cboState
                    .ValueMember = "stateunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("State")
                End With
                If menAction = enAction.EDIT_ONE Then
                    cboState.SelectedValue = objApplicant._Present_Stateunkid
                Else
                    cboState.SelectedValue = 0
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged

        Try
            If CInt(cboState.SelectedValue) > 0 Then
                Dim objCity As New clscity_master
                Dim dslist As New DataSet
                dslist = objCity.GetList("City", , True, CInt(cboState.SelectedValue))
                With cboPostTown
                    .ValueMember = "cityunkid"
                    .DisplayMember = "name"
                    .DataSource = dslist.Tables("City")
                End With
                If menAction = enAction.EDIT_ONE Then
                    cboPostTown.SelectedValue = objApplicant._Present_Post_Townunkid
                Else
                    cboPostTown.SelectedValue = 0
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboState_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    Private Sub cboPostTown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPostTown.SelectedIndexChanged
        Try
            If CInt(cboPostTown.SelectedValue) > 0 Then
                Dim objZipcode As New clszipcode_master
                Dim dsList As New DataSet
                dsList = objZipcode.GetList("Zipcode", , True, CInt(cboPostTown.SelectedValue))
                With cboPostCode
                    .ValueMember = "zipcodeunkid"
                    .DisplayMember = "zipcode_no"
                    .DataSource = dsList.Tables("Zipcode")
                End With
                If menAction = enAction.EDIT_ONE Then
                    cboPostCode.SelectedValue = objApplicant._Present_ZipCode
                Else
                    cboPostCode.SelectedValue = 0
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPostTown_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub


    Private Sub cboSkillCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSkillCategory.SelectedIndexChanged

        Try
            'If CInt(cboSkillCategory.SelectedValue) > 0 Then   'Sohail (11 Sep 2010) 'To set selected value=0 if no skill category is selected.
            Dim objSkill As New clsskill_master
            Dim dsList As New DataSet

            dsList = objSkill.getComboList("List", True, CInt(cboSkillCategory.SelectedValue))
            With cboSkill
                .ValueMember = "skillunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0 'Sohail (11 Sep 2010)
            End With

            'End If  'Sohail (11 Sep 2010)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSkillCategory_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub dtpJoinDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpJoinDate.ValueChanged

        Try
            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes

            'If dtpJoinDate.Checked = True Then
            '    dtpTerminationDate.Checked = True
            'Else
            '    dtpTerminationDate.Checked = False
            'End If

            'Pinkal (25-APR-2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpJoinDate_ValueChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub lnkCopyAddress_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkCopyAddress.LinkClicked

        Try
            txtPermAddress1.Text = txtAddress1.Text
            txtPermAddress2.Text = txtAddress2.Text
            txtPermAltNo.Text = txtAlternativeNo.Text
            txtPermEstate.Text = txtEstate.Text
            txtPermFax.Text = txtFax.Text
            txtPermMobile.Text = txtMobile.Text
            txtPermPlotNo.Text = txtPlotNo.Text
            txtPermProvince.Text = txtProvince.Text
            txtPermRoad.Text = txtRoad.Text
            txtPermTelNo.Text = txtTelNo.Text
            cboPermCountry.SelectedValue = cboCountry.SelectedValue
            cboPermState.SelectedValue = cboState.SelectedValue
            cboPermPostTown.SelectedValue = cboPostTown.SelectedValue
            cboPermPostCode.SelectedValue = cboPostCode.SelectedValue



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkCopyAddress_LinkClicked", mstrModuleName)
        End Try

    End Sub

    'Sandeep [ 09 Oct 2010 ] -- Start
    Private Sub objbtnAddQulification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddQulification.Click
        Dim frm As New frmQualificationCourse_AddEdit
        Dim dsList As New DataSet
        Dim intQualifyId As Integer = -1
        Dim objQualify As New clsqualification_master
        Try
            If frm.displayDialog(intQualifyId, enAction.ADD_ONE) Then
                dsList = objQualify.GetComboList("Qualify", True, CInt(cboQualificationGroup.SelectedValue))
                With cboQualification
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("Qualify")
                    .SelectedValue = intQualifyId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddQulification_Click", mstrModuleName)
        Finally
            frm = Nothing
            dsList = Nothing
            objQualify = Nothing
        End Try
    End Sub

    Private Sub objbtnAddGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddGroup.Click
        Dim frm As New frmCommonMaster
        Dim dsList As New DataSet
        Dim intGroupId As Integer = -1
        Dim objGroup As New clsCommon_Master
        Try
            frm.displayDialog(intGroupId, clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, enAction.ADD_ONE)
            If intGroupId > -1 Then
                dsList = objGroup.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "Group")
                With cboQualificationGroup
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Group")
                    .SelectedValue = intGroupId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddGroup_Click", mstrModuleName)
        Finally
            frm = Nothing
            dsList = Nothing
            objGroup = Nothing
        End Try
    End Sub

    Private Sub objbtnAddInstitution_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddInstitution.Click
        Dim intRefId As Integer = -1
        Dim frm As New frmTraningInstitutes_AddEdit
        Try
            frm.displayDialog(intRefId, enAction.ADD_ONE, False)
            If intRefId > -1 Then
                Dim objInstitute As New clsinstitute_master
                Dim dsList As New DataSet
                'S.SANDEEP [ 03 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsList = objInstitute.getListForCombo(False, "List", True)
                dsList = objInstitute.getListForCombo(False, "List", True, , , , True)
                'S.SANDEEP [ 03 MAY 2012 ] -- END
                With cboInstitution
                    .ValueMember = "instituteunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 09 Oct 2010 ] -- End 

    Private Sub cboLanguage1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLanguage1.SelectedIndexChanged


        Try
            If CInt(cboLanguage1.SelectedValue) > 0 Then
                If CInt(cboLanguage1.SelectedValue) = CInt(cboLanguage2.SelectedValue) Or CInt(cboLanguage1.SelectedValue) = CInt(cboLanguage3.SelectedValue) Or CInt(cboLanguage1.SelectedValue) = CInt(cboLanguage4.SelectedValue) Then
                    eZeeMsgBox.Show("Language1 cannot be same as other languages. Please select other language ", enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLanguage1_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub cboLanguage2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLanguage2.SelectedIndexChanged


        Try
            If CInt(cboLanguage2.SelectedValue) > 0 Then
                If CInt(cboLanguage2.SelectedValue) = CInt(cboLanguage1.SelectedValue) Or CInt(cboLanguage2.SelectedValue) = CInt(cboLanguage3.SelectedValue) Or CInt(cboLanguage2.SelectedValue) = CInt(cboLanguage4.SelectedValue) Then
                    eZeeMsgBox.Show("Language2 cannot be same as other languages. Please select other language ", enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLanguage2_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub cboLanguage3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLanguage3.SelectedIndexChanged


        Try
            If CInt(cboLanguage3.SelectedValue) > 0 Then
                If CInt(cboLanguage3.SelectedValue) = CInt(cboLanguage1.SelectedValue) Or CInt(cboLanguage3.SelectedValue) = CInt(cboLanguage2.SelectedValue) Or CInt(cboLanguage3.SelectedValue) = CInt(cboLanguage4.SelectedValue) Then
                    eZeeMsgBox.Show("Language3 cannot be same as other languages. Please select other language ", enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLanguage3_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub cboLanguage4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLanguage4.SelectedIndexChanged

        Try
            If CInt(cboLanguage4.SelectedValue) > 0 Then
                If CInt(cboLanguage4.SelectedValue) = CInt(cboLanguage1.SelectedValue) Or CInt(cboLanguage4.SelectedValue) = CInt(cboLanguage2.SelectedValue) Or CInt(cboLanguage4.SelectedValue) = CInt(cboLanguage3.SelectedValue) Then
                    eZeeMsgBox.Show("Language4 cannot be same as other languages. Please select other language ", enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLanguage4_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

    'Sandeep [ 17 DEC 2010 ] -- Start
    Private Sub objbtnAddVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddVacancy.Click
        'Dim frm As New frmVacancy_AddEdit
        'Dim intRefId As Integer = -1
        'Try
        '    frm.displayDialog(intRefId, enAction.ADD_ONE)
        '    If intRefId > 0 Then
        '        Dim dsList As New DataSet
        '        Dim objVac As New clsVacancy
        '        dsList = objVac.getComboList(True, "Vac")
        '        With cboVacancy
        '            .ValueMember = "id"
        '            .DisplayMember = "name"
        '            .DataSource = dsList.Tables("Vac")
        '            .SelectedValue = intRefId
        '        End With
        '        dsList.Dispose()
        '        objVac = Nothing
        '    End If
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "objbtnAddVacancy_Click", mstrModuleName)
        'Finally
        '    If frm IsNot Nothing Then frm.Dispose()
        'End Try
    End Sub

    Private Sub objbtnAddSkillCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSkillCategory.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.SKILL_CATEGORY, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "SkillCat")
                With cboSkillCategory
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("SkillCat")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSkillCategory_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddSkill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSkill.Click
        Dim frm As New frmSkill_AddEdit
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objSkill As New clsskill_master
                dsList = objSkill.getComboList("Skill", True, CInt(cboSkillCategory.SelectedValue))
                With cboSkill
                    .ValueMember = "skillunkid"
                    .DisplayMember = "NAME"
                    .DataSource = dsList.Tables("Skill")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objSkill = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSkill_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sandeep [ 17 DEC 2010 ] -- End 

    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            If CInt(cboVacancyType.SelectedValue) >= 0 Then
                'S.SANDEEP [ 28 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim dsVacList As New DataSet
                'Dim objVac As New clsVacancy
                'dsVacList = objVac.getComboList(True, "List", , CInt(cboVacancyType.SelectedValue))
                'With cboVacancy
                '    .ValueMember = "id"
                '    .DisplayMember = "name"
                '    .DataSource = dsVacList.Tables("List")
                '    If menAction = enAction.EDIT_ONE Then
                '        cboVacancy.SelectedValue = objApplicant._Vacancyunkid
                '    Else
                '        .SelectedValue = 0
                '    End If
                'End With
                'S.SANDEEP [ 28 FEB 2012 ] -- END

                Select Case CInt(cboVacancyType.SelectedValue)
                    Case enVacancyType.EXTERNAL_VACANCY
                        txtEmployeeCode.Enabled = False
                    Case enVacancyType.INTERNAL_VACANCY
                        txtEmployeeCode.Enabled = True
                End Select

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END
    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    'Pinkal (12-Oct-2011) -- End

    'Sohail (27 Apr 2017) -- Start
    'TRA Enhancement - 66.1 - Online Recruitment Redesign changes for tra security issues.
    Private Sub chkImpaired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkImpaired.CheckedChanged
        Try
            If chkImpaired.Checked = True Then
                txtImpairment.Enabled = True
            Else
                txtImpairment.Enabled = False
                txtImpairment.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkImpaired_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (27 Apr 2017) -- End

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
			Call SetLanguage()

            Me.gbApplicantInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbApplicantInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSkillInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSkillInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbOtherInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbOtherInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbPrsonalInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPrsonalInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbQualificationInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbQualificationInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbJobHistory.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbJobHistory.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbReferences.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbReferences.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbOtherDetails.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbOtherDetails.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSaveInfo.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveInfo.GradientForeColor = GUI._ButttonFontColor

            Me.btnQualifyDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnQualifyDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnQualifyEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnQualifyEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnQualifyAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnQualifyAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnJobHistoryDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnJobHistoryDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnJobHistoryEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnJobHistoryEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnJobHistoryAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnJobHistoryAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnRefDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnRefDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnRefEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnRefEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnRefAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnRefAdd.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSaveInfo.Text = Language._Object.getCaption(Me.btnSaveInfo.Name, Me.btnSaveInfo.Text)
            Me.gbApplicantInfo.Text = Language._Object.getCaption(Me.gbApplicantInfo.Name, Me.gbApplicantInfo.Text)
            Me.tabpPersonalInfo.Text = Language._Object.getCaption(Me.tabpPersonalInfo.Name, Me.tabpPersonalInfo.Text)
            Me.tabpEducationalInfo.Text = Language._Object.getCaption(Me.tabpEducationalInfo.Name, Me.tabpEducationalInfo.Text)
            Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.Name, Me.lblGender.Text)
            Me.tabpEmployment.Text = Language._Object.getCaption(Me.tabpEmployment.Name, Me.tabpEmployment.Text)
            Me.tabpAdditionalInfo.Text = Language._Object.getCaption(Me.tabpAdditionalInfo.Name, Me.tabpAdditionalInfo.Text)
            Me.lblOthername.Text = Language._Object.getCaption(Me.lblOthername.Name, Me.lblOthername.Text)
            Me.lblApplicantFirstname.Text = Language._Object.getCaption(Me.lblApplicantFirstname.Name, Me.lblApplicantFirstname.Text)
            Me.lblSurname.Text = Language._Object.getCaption(Me.lblSurname.Name, Me.lblSurname.Text)
            Me.imgImageControl.Text = Language._Object.getCaption(Me.imgImageControl.Name, Me.imgImageControl.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.lblApplicantCode.Text = Language._Object.getCaption(Me.lblApplicantCode.Name, Me.lblApplicantCode.Text)
            Me.gbSkillInfo.Text = Language._Object.getCaption(Me.gbSkillInfo.Name, Me.gbSkillInfo.Text)
            Me.gbOtherInfo.Text = Language._Object.getCaption(Me.gbOtherInfo.Name, Me.gbOtherInfo.Text)
            Me.lblBirtdate.Text = Language._Object.getCaption(Me.lblBirtdate.Name, Me.lblBirtdate.Text)
            Me.lblMaritalStatus.Text = Language._Object.getCaption(Me.lblMaritalStatus.Name, Me.lblMaritalStatus.Text)
            Me.lblMaritalDate.Text = Language._Object.getCaption(Me.lblMaritalDate.Name, Me.lblMaritalDate.Text)
            Me.gbPrsonalInfo.Text = Language._Object.getCaption(Me.gbPrsonalInfo.Name, Me.gbPrsonalInfo.Text)
            Me.gbQualificationInfo.Text = Language._Object.getCaption(Me.gbQualificationInfo.Name, Me.gbQualificationInfo.Text)
            Me.gbJobHistory.Text = Language._Object.getCaption(Me.gbJobHistory.Name, Me.gbJobHistory.Text)
            Me.lblPermPostCountry.Text = Language._Object.getCaption(Me.lblPermPostCountry.Name, Me.lblPermPostCountry.Text)
            Me.lblPermPostCode.Text = Language._Object.getCaption(Me.lblPermPostCode.Name, Me.lblPermPostCode.Text)
            Me.lblPostcode.Text = Language._Object.getCaption(Me.lblPostcode.Name, Me.lblPostcode.Text)
            Me.lblPermState.Text = Language._Object.getCaption(Me.lblPermState.Name, Me.lblPermState.Text)
            Me.lblPostCountry.Text = Language._Object.getCaption(Me.lblPostCountry.Name, Me.lblPostCountry.Text)
            Me.lnkCopyAddress.Text = Language._Object.getCaption(Me.lnkCopyAddress.Name, Me.lnkCopyAddress.Text)
            Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
            Me.lblPermFax.Text = Language._Object.getCaption(Me.lblPermFax.Name, Me.lblPermFax.Text)
            Me.lblPermAltNo.Text = Language._Object.getCaption(Me.lblPermAltNo.Name, Me.lblPermAltNo.Text)
            Me.lblPermTelNo.Text = Language._Object.getCaption(Me.lblPermTelNo.Name, Me.lblPermTelNo.Text)
            Me.lblPermMobileNo.Text = Language._Object.getCaption(Me.lblPermMobileNo.Name, Me.lblPermMobileNo.Text)
            Me.lblPermPlotNo.Text = Language._Object.getCaption(Me.lblPermPlotNo.Name, Me.lblPermPlotNo.Text)
            Me.lblPermProvince.Text = Language._Object.getCaption(Me.lblPermProvince.Name, Me.lblPermProvince.Text)
            Me.lblPermRoad.Text = Language._Object.getCaption(Me.lblPermRoad.Name, Me.lblPermRoad.Text)
            Me.lblPermEstate.Text = Language._Object.getCaption(Me.lblPermEstate.Name, Me.lblPermEstate.Text)
            Me.lblPermPostTown.Text = Language._Object.getCaption(Me.lblPermPostTown.Name, Me.lblPermPostTown.Text)
            Me.lblPermAddress.Text = Language._Object.getCaption(Me.lblPermAddress.Name, Me.lblPermAddress.Text)
            Me.lnPermanentAddress.Text = Language._Object.getCaption(Me.lnPermanentAddress.Name, Me.lnPermanentAddress.Text)
            Me.lblProvince.Text = Language._Object.getCaption(Me.lblProvince.Name, Me.lblProvince.Text)
            Me.lblPostTown.Text = Language._Object.getCaption(Me.lblPostTown.Name, Me.lblPostTown.Text)
            Me.lblRoad.Text = Language._Object.getCaption(Me.lblRoad.Name, Me.lblRoad.Text)
            Me.lblFax.Text = Language._Object.getCaption(Me.lblFax.Name, Me.lblFax.Text)
            Me.lblEstate.Text = Language._Object.getCaption(Me.lblEstate.Name, Me.lblEstate.Text)
            Me.lblAlternativeNo.Text = Language._Object.getCaption(Me.lblAlternativeNo.Name, Me.lblAlternativeNo.Text)
            Me.lblPloteNo.Text = Language._Object.getCaption(Me.lblPloteNo.Name, Me.lblPloteNo.Text)
            Me.lblMobile.Text = Language._Object.getCaption(Me.lblMobile.Name, Me.lblMobile.Text)
            Me.lblTelNo.Text = Language._Object.getCaption(Me.lblTelNo.Name, Me.lblTelNo.Text)
            Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.Name, Me.lblAddress.Text)
            Me.lnPresentAddress.Text = Language._Object.getCaption(Me.lnPresentAddress.Name, Me.lnPresentAddress.Text)
            Me.lblNationality.Text = Language._Object.getCaption(Me.lblNationality.Name, Me.lblNationality.Text)
            Me.lblLanguage1.Text = Language._Object.getCaption(Me.lblLanguage1.Name, Me.lblLanguage1.Text)
            Me.lblLanguage2.Text = Language._Object.getCaption(Me.lblLanguage2.Name, Me.lblLanguage2.Text)
            Me.lblLanguage4.Text = Language._Object.getCaption(Me.lblLanguage4.Name, Me.lblLanguage4.Text)
            Me.lblLanguage3.Text = Language._Object.getCaption(Me.lblLanguage3.Name, Me.lblLanguage3.Text)
            Me.colhSkillCategory.Text = Language._Object.getCaption(CStr(Me.colhSkillCategory.Tag), Me.colhSkillCategory.Text)
            Me.colhSkillName.Text = Language._Object.getCaption(CStr(Me.colhSkillName.Tag), Me.colhSkillName.Text)
            Me.colhSkillRemark.Text = Language._Object.getCaption(CStr(Me.colhSkillRemark.Tag), Me.colhSkillRemark.Text)
            Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
            Me.lblSkillName.Text = Language._Object.getCaption(Me.lblSkillName.Name, Me.lblSkillName.Text)
            Me.lblSkillCategory.Text = Language._Object.getCaption(Me.lblSkillCategory.Name, Me.lblSkillCategory.Text)
            Me.colhQualifyGroupName.Text = Language._Object.getCaption(CStr(Me.colhQualifyGroupName.Tag), Me.colhQualifyGroupName.Text)
            Me.colhQualificationName.Text = Language._Object.getCaption(CStr(Me.colhQualificationName.Tag), Me.colhQualificationName.Text)
            Me.colhRefNo.Text = Language._Object.getCaption(CStr(Me.colhRefNo.Tag), Me.colhRefNo.Text)
            Me.colhStartDate.Text = Language._Object.getCaption(CStr(Me.colhStartDate.Tag), Me.colhStartDate.Text)
            Me.colhEndDate.Text = Language._Object.getCaption(CStr(Me.colhEndDate.Tag), Me.colhEndDate.Text)
            Me.colhInstitute.Text = Language._Object.getCaption(CStr(Me.colhInstitute.Tag), Me.colhInstitute.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblInstitution.Text = Language._Object.getCaption(Me.lblInstitution.Name, Me.lblInstitution.Text)
            Me.btnQualifyDelete.Text = Language._Object.getCaption(Me.btnQualifyDelete.Name, Me.btnQualifyDelete.Text)
            Me.btnQualifyEdit.Text = Language._Object.getCaption(Me.btnQualifyEdit.Name, Me.btnQualifyEdit.Text)
            Me.btnQualifyAdd.Text = Language._Object.getCaption(Me.btnQualifyAdd.Name, Me.btnQualifyAdd.Text)
            Me.lblQualification.Text = Language._Object.getCaption(Me.lblQualification.Name, Me.lblQualification.Text)
            Me.lblQualificationGroup.Text = Language._Object.getCaption(Me.lblQualificationGroup.Name, Me.lblQualificationGroup.Text)
            Me.lblReferenceNo.Text = Language._Object.getCaption(Me.lblReferenceNo.Name, Me.lblReferenceNo.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.lblAwardDate.Text = Language._Object.getCaption(Me.lblAwardDate.Name, Me.lblAwardDate.Text)
            Me.btnJobHistoryDelete.Text = Language._Object.getCaption(Me.btnJobHistoryDelete.Name, Me.btnJobHistoryDelete.Text)
            Me.btnJobHistoryEdit.Text = Language._Object.getCaption(Me.btnJobHistoryEdit.Name, Me.btnJobHistoryEdit.Text)
            Me.btnJobHistoryAdd.Text = Language._Object.getCaption(Me.btnJobHistoryAdd.Name, Me.btnJobHistoryAdd.Text)
            Me.colhEmployer.Text = Language._Object.getCaption(CStr(Me.colhEmployer.Tag), Me.colhEmployer.Text)
            Me.colhCompanyName.Text = Language._Object.getCaption(CStr(Me.colhCompanyName.Tag), Me.colhCompanyName.Text)
            Me.colhOfficePhone.Text = Language._Object.getCaption(CStr(Me.colhOfficePhone.Tag), Me.colhOfficePhone.Text)
            Me.colhDesignation.Text = Language._Object.getCaption(CStr(Me.colhDesignation.Tag), Me.colhDesignation.Text)
            Me.colhResponsibility.Text = Language._Object.getCaption(CStr(Me.colhResponsibility.Tag), Me.colhResponsibility.Text)
            Me.colhJoinDate.Text = Language._Object.getCaption(CStr(Me.colhJoinDate.Tag), Me.colhJoinDate.Text)
            Me.colhTermDate.Text = Language._Object.getCaption(CStr(Me.colhTermDate.Tag), Me.colhTermDate.Text)
            Me.colhLeavingReason.Text = Language._Object.getCaption(CStr(Me.colhLeavingReason.Tag), Me.colhLeavingReason.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.lblTermDate.Text = Language._Object.getCaption(Me.lblTermDate.Name, Me.lblTermDate.Text)
            Me.lblOrgDescription.Text = Language._Object.getCaption(Me.lblOrgDescription.Name, Me.lblOrgDescription.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.Name, Me.lblReason.Text)
            Me.lblResponsibility.Text = Language._Object.getCaption(Me.lblResponsibility.Name, Me.lblResponsibility.Text)
            Me.lblOfficePhone.Text = Language._Object.getCaption(Me.lblOfficePhone.Name, Me.lblOfficePhone.Text)
            Me.lblJoinDate.Text = Language._Object.getCaption(Me.lblJoinDate.Name, Me.lblJoinDate.Text)
            Me.lblDesignation.Text = Language._Object.getCaption(Me.lblDesignation.Name, Me.lblDesignation.Text)
            Me.lblEmployer.Text = Language._Object.getCaption(Me.lblEmployer.Name, Me.lblEmployer.Text)
            Me.lblOtherSkill.Text = Language._Object.getCaption(Me.lblOtherSkill.Name, Me.lblOtherSkill.Text)
            Me.lblOtherQualification.Text = Language._Object.getCaption(Me.lblOtherQualification.Name, Me.lblOtherQualification.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.lblGPA.Text = Language._Object.getCaption(Me.lblGPA.Name, Me.lblGPA.Text)
            Me.lblResult.Text = Language._Object.getCaption(Me.lblResult.Name, Me.lblResult.Text)
            Me.lblVacancyType.Text = Language._Object.getCaption(Me.lblVacancyType.Name, Me.lblVacancyType.Text)
            Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
            Me.tabpReferences.Text = Language._Object.getCaption(Me.tabpReferences.Name, Me.tabpReferences.Text)
            Me.gbReferences.Text = Language._Object.getCaption(Me.gbReferences.Name, Me.gbReferences.Text)
            Me.lblReferenceName.Text = Language._Object.getCaption(Me.lblReferenceName.Name, Me.lblReferenceName.Text)
            Me.lblRefAddress.Text = Language._Object.getCaption(Me.lblRefAddress.Name, Me.lblRefAddress.Text)
            Me.lblPosition.Text = Language._Object.getCaption(Me.lblPosition.Name, Me.lblPosition.Text)
            Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
            Me.lblRefTown.Text = Language._Object.getCaption(Me.lblRefTown.Name, Me.lblRefTown.Text)
            Me.lblRefState.Text = Language._Object.getCaption(Me.lblRefState.Name, Me.lblRefState.Text)
            Me.lblRefEmail.Text = Language._Object.getCaption(Me.lblRefEmail.Name, Me.lblRefEmail.Text)
            Me.lblRefMobileNo.Text = Language._Object.getCaption(Me.lblRefMobileNo.Name, Me.lblRefMobileNo.Text)
            Me.lblRefTelPhNo.Text = Language._Object.getCaption(Me.lblRefTelPhNo.Name, Me.lblRefTelPhNo.Text)
            Me.ColumnHeader1.Text = Language._Object.getCaption(CStr(Me.ColumnHeader1.Tag), Me.ColumnHeader1.Text)
            Me.ColumnHeader2.Text = Language._Object.getCaption(CStr(Me.ColumnHeader2.Tag), Me.ColumnHeader2.Text)
            Me.ColumnHeader3.Text = Language._Object.getCaption(CStr(Me.ColumnHeader3.Tag), Me.ColumnHeader3.Text)
            Me.ColumnHeader4.Text = Language._Object.getCaption(CStr(Me.ColumnHeader4.Tag), Me.ColumnHeader4.Text)
            Me.ColumnHeader5.Text = Language._Object.getCaption(CStr(Me.ColumnHeader5.Tag), Me.ColumnHeader5.Text)
            Me.ColumnHeader6.Text = Language._Object.getCaption(CStr(Me.ColumnHeader6.Tag), Me.ColumnHeader6.Text)
            Me.ColumnHeader7.Text = Language._Object.getCaption(CStr(Me.ColumnHeader7.Tag), Me.ColumnHeader7.Text)
            Me.ColumnHeader8.Text = Language._Object.getCaption(CStr(Me.ColumnHeader8.Tag), Me.ColumnHeader8.Text)
            Me.ColumnHeader9.Text = Language._Object.getCaption(CStr(Me.ColumnHeader9.Tag), Me.ColumnHeader9.Text)
            Me.lblRefGender.Text = Language._Object.getCaption(Me.lblRefGender.Name, Me.lblRefGender.Text)
            Me.lblRefType.Text = Language._Object.getCaption(Me.lblRefType.Name, Me.lblRefType.Text)
            Me.btnRefDelete.Text = Language._Object.getCaption(Me.btnRefDelete.Name, Me.btnRefDelete.Text)
            Me.btnRefEdit.Text = Language._Object.getCaption(Me.btnRefEdit.Name, Me.btnRefEdit.Text)
            Me.colhRefName.Text = Language._Object.getCaption(CStr(Me.colhRefName.Tag), Me.colhRefName.Text)
            Me.colhRefPosition.Text = Language._Object.getCaption(CStr(Me.colhRefPosition.Tag), Me.colhRefPosition.Text)
            Me.colhRefGender.Text = Language._Object.getCaption(CStr(Me.colhRefGender.Tag), Me.colhRefGender.Text)
            Me.colhRefType.Text = Language._Object.getCaption(CStr(Me.colhRefType.Tag), Me.colhRefType.Text)
            Me.colhRefEmail.Text = Language._Object.getCaption(CStr(Me.colhRefEmail.Tag), Me.colhRefEmail.Text)
            Me.lblOtherInstitution.Text = Language._Object.getCaption(Me.lblOtherInstitution.Name, Me.lblOtherInstitution.Text)
            Me.lblOtherResultCode.Text = Language._Object.getCaption(Me.lblOtherResultCode.Name, Me.lblOtherResultCode.Text)
            Me.lblOtherQualificationGrp.Text = Language._Object.getCaption(Me.lblOtherQualificationGrp.Name, Me.lblOtherQualificationGrp.Text)
            Me.lnkOtherQualification.Text = Language._Object.getCaption(Me.lnkOtherQualification.Name, Me.lnkOtherQualification.Text)
            Me.lblOtherSkillCategory.Text = Language._Object.getCaption(Me.lblOtherSkillCategory.Name, Me.lblOtherSkillCategory.Text)
            Me.lnkOtherSkill.Text = Language._Object.getCaption(Me.lnkOtherSkill.Name, Me.lnkOtherSkill.Text)
            Me.btnRefAdd.Text = Language._Object.getCaption(Me.btnRefAdd.Name, Me.btnRefAdd.Text)
            Me.tabpOtherdetails.Text = Language._Object.getCaption(Me.tabpOtherdetails.Name, Me.tabpOtherdetails.Text)
            Me.gbOtherDetails.Text = Language._Object.getCaption(Me.gbOtherDetails.Name, Me.gbOtherDetails.Text)
            Me.lblAchievements.Text = Language._Object.getCaption(Me.lblAchievements.Name, Me.lblAchievements.Text)
            Me.lblMemberships.Text = Language._Object.getCaption(Me.lblMemberships.Name, Me.lblMemberships.Text)
            Me.lblJournalsResearchPapers.Text = Language._Object.getCaption(Me.lblJournalsResearchPapers.Name, Me.lblJournalsResearchPapers.Text)
            Me.lblCertiNo.Text = Language._Object.getCaption(Me.lblCertiNo.Name, Me.lblCertiNo.Text)
            Me.lblJobAchievement.Text = Language._Object.getCaption(Me.lblJobAchievement.Name, Me.lblJobAchievement.Text)
			Me.lblSkillExpertise.Text = Language._Object.getCaption(Me.lblSkillExpertise.Name, Me.lblSkillExpertise.Text)
			Me.colhSkillExpertise.Text = Language._Object.getCaption(CStr(Me.colhSkillExpertise.Tag), Me.colhSkillExpertise.Text)
			Me.chkHighestqualification.Text = Language._Object.getCaption(Me.chkHighestqualification.Name, Me.chkHighestqualification.Text)
			Me.lblMotherTongue.Text = Language._Object.getCaption(Me.lblMotherTongue.Name, Me.lblMotherTongue.Text)
			Me.lblCurrentSalary.Text = Language._Object.getCaption(Me.lblCurrentSalary.Name, Me.lblCurrentSalary.Text)
			Me.lblExpectedSalary.Text = Language._Object.getCaption(Me.lblExpectedSalary.Name, Me.lblExpectedSalary.Text)
			Me.lblNoticePeriodDays.Text = Language._Object.getCaption(Me.lblNoticePeriodDays.Name, Me.lblNoticePeriodDays.Text)
			Me.chkWillingToTravel.Text = Language._Object.getCaption(Me.chkWillingToTravel.Name, Me.chkWillingToTravel.Text)
			Me.chkWillingToRelocate.Text = Language._Object.getCaption(Me.chkWillingToRelocate.Name, Me.chkWillingToRelocate.Text)
			Me.chkImpaired.Text = Language._Object.getCaption(Me.chkImpaired.Name, Me.chkImpaired.Text)
			Me.lblExpectedBenefit.Text = Language._Object.getCaption(Me.lblExpectedBenefit.Name, Me.lblExpectedBenefit.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Applicant Code cannot be blank. Applicant Code is compulsory information.")
            Language.setMessage(mstrModuleName, 2, "Firstname cannot be blank. Firstname is compulsory information.")
            Language.setMessage(mstrModuleName, 3, "Surname cannot be blank. Surname is compulsory information.")
            Language.setMessage(mstrModuleName, 4, "Invalid Email.Please Enter Valid Email")
            Language.setMessage(mstrModuleName, 5, "Applicant Type cannot be blank. Applicant Type is compulsory information.")
            Language.setMessage(mstrModuleName, 6, "Skill is compulsory information, it can not be blank. Please select Skill to continue.")
            Language.setMessage(mstrModuleName, 7, "Selected Skill is already added to the list.")
            Language.setMessage(mstrModuleName, 8, "Skill is compulsory information, it can not be blank. Please select Skill to continue.")
            Language.setMessage(mstrModuleName, 9, "Qualification is compulsory information, it can not be blank. Please select Qualification to continue.")
            Language.setMessage(mstrModuleName, 11, "Selected Qualification is already added to the list.")
            Language.setMessage(mstrModuleName, 12, "Company Name is compulsory information, it can not be blank. Please give Company Name to continue.")
            Language.setMessage(mstrModuleName, 14, "Selected company is already added to the list.")
            Language.setMessage(mstrModuleName, 15, "Married Date should be greater than Birth Date.")
            Language.setMessage(mstrModuleName, 16, "Termination should be greater than Join Date.")
            Language.setMessage(mstrModuleName, 17, "Skill Category is compulsory information, it can not be blank. Please select Skill Category to continue.")
            Language.setMessage(mstrModuleName, 18, "Please select Applicant Skill from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 19, "Birth Date should be less than current Date.")
            Language.setMessage(mstrModuleName, 20, "Married Date should be less than current Date.")
            Language.setMessage(mstrModuleName, 21, "Qualification Group is compulsory information, it can not be blank. Please select Qualification Group to continue.")
            Language.setMessage(mstrModuleName, 22, "Please select Qualification Detail from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 24, "End date cannot be less than to start date.")
            Language.setMessage(mstrModuleName, 25, "Please select Job History from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 26, "Joining Date should be less than current Date.")
            Language.setMessage(mstrModuleName, 27, "Termination Date should be less than current Date.")
            Language.setMessage(mstrModuleName, 28, "Institute is compulsory information. Please select Institute to continue.")
            Language.setMessage(mstrModuleName, 29, "Email is mandatory information. Please enter email to continue.")
            Language.setMessage(mstrModuleName, 30, "Employee Code cannot be blank. Please enter Employee Code to continue.")
            Language.setMessage(mstrModuleName, 31, "End date cannot be less or equal to start date.")
            Language.setMessage(mstrModuleName, 32, "Result Code is compulsory information, it can not be blank. Please select Result Code to continue.")
            Language.setMessage(mstrModuleName, 33, "Name cannot be blank.Name is required information.")
            Language.setMessage(mstrModuleName, 34, "This name is already added to the list.")
            Language.setMessage(mstrModuleName, 35, "Please select Reference from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 36, "Other Qualification Group can not be blank. Other Qualification Group is required information.")
            Language.setMessage(mstrModuleName, 37, "Other Qualification can not be blank. Other Qualification is required information.")
            Language.setMessage(mstrModuleName, 38, " Other Result Code can not be blank. Other Result Code  is required information.")
            Language.setMessage(mstrModuleName, 39, "Other Institute can not be blank. Other Institute is required information.")
            Language.setMessage(mstrModuleName, 40, "Other Skill Category can not be blank. Other Skill Category is Required information.")
            Language.setMessage(mstrModuleName, 41, "Other Skill can not be blank. Other Skill is Required information.")
            Language.setMessage(mstrModuleName, 42, "Joining Date is compulsory information.Please Check Joining Date.")
            Language.setMessage(mstrModuleName, 43, "Gender is mandatory information.Please select Gender.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

    
End Class