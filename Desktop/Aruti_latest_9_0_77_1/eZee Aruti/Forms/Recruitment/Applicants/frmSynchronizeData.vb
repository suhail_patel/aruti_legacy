﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmSynchronizeData

#Region " Private Variable(s) "

    Private mstrModuleName As String = String.Empty
    Private mblnIsImport As Boolean = False
    Private objApplicant As clsApplicant_master

    'Sohail (01 Nov 2012) -- Start
    'TRA - ENHANCEMENT
    Private mblnStopWebImport As Boolean = False
    'Sohail (01 Nov 2012) -- End
#End Region

#Region " Constructor "

    Public Sub New(ByVal blnIsImport As Boolean)
        InitializeComponent()
        mblnIsImport = blnIsImport
        If mblnIsImport = True Then
            mstrModuleName = "frmWebImportData"
            Me.Name = "frmWebImportData"
            Me.Text = Language.getMessage(mstrModuleName, 2, "Import data from Web")
        Else

            mstrModuleName = "frmWebExportData"
            Me.Name = "frmWebExportData"
            Me.Text = Language.getMessage(mstrModuleName, 1, "Export data to Web")
        End If
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub DoExportImport()
        Try

            'Sohail (05 Jun 2020) -- Start
            'NMB Enhancement # : Upon successful application of a vacancy for internal vacancies, system to copy the reporting-to on the reverse email sent back to the employee.
            objApplicant._CompanyCode = Company._Object._Code
            objApplicant._CompanyName = Company._Object._Name
            objApplicant._DatabaseName = FinancialYear._Object._DatabaseName
            objApplicant._LoginEmployeeUnkid = enLogin_Mode.DESKTOP
            'Sohail (05 Jun 2020) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objApplicant.Synchronize_Data(mblnIsImport, objbgwExport_Import) Then 'Sohail (09 Feb 2012)

            'Nilay (13 Apr 2017) -- Start
            'Enhancements: Settings for mandatory options in online recruitment
            'If objApplicant.Synchronize_Data(mblnIsImport, ConfigParameter._Object._ApplicantCodeNotype, _
            '                                ConfigParameter._Object._ApplicantCodePrifix, _
            '                                ConfigParameter._Object._DatabaseServer, _
            '                                ConfigParameter._Object._DatabaseServerSetting, _
            '                                ConfigParameter._Object._AuthenticationCode, _
            '                                ConfigParameter._Object._DatabaseName, _
            '                                ConfigParameter._Object._DatabaseUserName, _
            '                                ConfigParameter._Object._DatabaseUserPassword, _
            '                                ConfigParameter._Object._DatabaseOwner, _
            '                                ConfigParameter._Object._WebClientCode, _
            '                                Company._Object._ConfigDatabaseName, _
            '                                ConfigParameter._Object._IsImgInDataBase, _
            '                                ConfigParameter._Object._PhotoPath, _
            '                                ConfigParameter._Object._Document_Path, _
            '                                User._Object._Userunkid, _
            '                                ConfigParameter._Object._ArutiSelfServiceURL, _
            '                                ConfigParameter._Object._ApplicantDeclaration, _
            '                                ConfigParameter._Object._TimeZoneMinuteDifference, _
            '                                ConfigParameter._Object._QualificationCertificateAttachmentMandatory, _
            '                                ConfigParameter._Object._CurrentDateAndTime, _
            '                                Company._Object._Code, _
            '                                Company._Object._Companyunkid, _
            '                                ConfigParameter._Object._CompanyDateFormat, _
            '                                ConfigParameter._Object._CompanyDateSeparator, _
            '                                ConfigParameter._Object._AdminEmail, _
            '                                ConfigParameter._Object._MiddleNameMandatoryInRecruitment, _
            '                                ConfigParameter._Object._GenderMandatoryInRecruitment, _
            '                                ConfigParameter._Object._BirthDateMandatoryInRecruitment, _
            '                                ConfigParameter._Object._Address1MandatoryInRecruitment, _
            '                                ConfigParameter._Object._Address2MandatoryInRecruitment, _
            '                                ConfigParameter._Object._CountryMandatoryInRecruitment, _
            '                                ConfigParameter._Object._StateMandatoryInRecruitment, _
            '                                ConfigParameter._Object._CityMandatoryInRecruitment, _
            '                                ConfigParameter._Object._PostCodeMandatoryInRecruitment, _
            '                                ConfigParameter._Object._MaritalStatusMandatoryInRecruitment, _
            '                                ConfigParameter._Object._Language1MandatoryInRecruitment, _
            '                                ConfigParameter._Object._NationalityMandatoryInRecruitment, _
            '                                ConfigParameter._Object._OneSkillMandatoryInRecruitment, _
            '                                ConfigParameter._Object._OneQualificationMandatoryInRecruitment, _
            '                                ConfigParameter._Object._OneJobExperienceMandatoryInRecruitment, _
            '                                ConfigParameter._Object._OneReferenceMandatoryInRecruitment, _
            '                                objbgwExport_Import _
            '                                ) Then

'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objApplicant._FormName = mstrModuleName
            objApplicant._LoginEmployeeUnkid = 0
            objApplicant._ClientIP = getIP()
            objApplicant._HostName = getHostName()
            objApplicant._FromWeb = False
            objApplicant._AuditUserId = User._Object._Userunkid
objApplicant._CompanyUnkid = Company._Object._Companyunkid
            objApplicant._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objApplicant.Synchronize_Data(mblnIsImport, ConfigParameter._Object._ApplicantCodeNotype, ConfigParameter._Object._ApplicantCodePrifix, _
                                             ConfigParameter._Object._DatabaseServer, ConfigParameter._Object._DatabaseServerSetting, _
                                             ConfigParameter._Object._AuthenticationCode, ConfigParameter._Object._DatabaseName, _
                                             ConfigParameter._Object._DatabaseUserName, ConfigParameter._Object._DatabaseUserPassword, _
                                             ConfigParameter._Object._DatabaseOwner, ConfigParameter._Object._WebClientCode, _
                                             Company._Object._ConfigDatabaseName, ConfigParameter._Object._IsImgInDataBase, _
                                             ConfigParameter._Object._PhotoPath, ConfigParameter._Object._Document_Path, User._Object._Userunkid, _
                                             ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._ApplicantDeclaration, _
                                             ConfigParameter._Object._TimeZoneMinuteDifference, ConfigParameter._Object._QualificationCertificateAttachmentMandatory, _
                                             ConfigParameter._Object._CurrentDateAndTime, Company._Object._Code, Company._Object._Companyunkid, _
                                             ConfigParameter._Object._CompanyDateFormat, ConfigParameter._Object._CompanyDateSeparator, ConfigParameter._Object._AdminEmail, _
                                             ConfigParameter._Object._MiddleNameMandatoryInRecruitment, ConfigParameter._Object._GenderMandatoryInRecruitment, _
                                             ConfigParameter._Object._BirthDateMandatoryInRecruitment, ConfigParameter._Object._Address1MandatoryInRecruitment, _
                                             ConfigParameter._Object._Address2MandatoryInRecruitment, ConfigParameter._Object._CountryMandatoryInRecruitment, _
                                             ConfigParameter._Object._StateMandatoryInRecruitment, ConfigParameter._Object._CityMandatoryInRecruitment, _
                                             ConfigParameter._Object._PostCodeMandatoryInRecruitment, ConfigParameter._Object._MaritalStatusMandatoryInRecruitment, _
                                             ConfigParameter._Object._Language1MandatoryInRecruitment, ConfigParameter._Object._NationalityMandatoryInRecruitment, _
                                             ConfigParameter._Object._OneSkillMandatoryInRecruitment, ConfigParameter._Object._OneQualificationMandatoryInRecruitment, _
                                             ConfigParameter._Object._OneJobExperienceMandatoryInRecruitment, ConfigParameter._Object._OneReferenceMandatoryInRecruitment, _
                                             ConfigParameter._Object._ApplicantQualificationSortByInRecruitment, ConfigParameter._Object._RegionMandatoryInRecruitment, _
                                             ConfigParameter._Object._HighestQualificationMandatoryInRecruitment, ConfigParameter._Object._EmployerMandatoryInRecruitment, _
                                             ConfigParameter._Object._MotherTongueMandatoryInRecruitment, ConfigParameter._Object._CurrentSalaryMandatoryInRecruitment, _
                                             ConfigParameter._Object._ExpectedSalaryMandatoryInRecruitment, ConfigParameter._Object._ExpectedBenefitsMandatoryInRecruitment, _
                                             ConfigParameter._Object._NoticePeriodMandatoryInRecruitment, ConfigParameter._Object._EarliestPossibleStartDateMandatoryInRecruitment, _
                                             ConfigParameter._Object._CommentsMandatoryInRecruitment, ConfigParameter._Object._VacancyFoundOutFromMandatoryInRecruitment, _
                                             ConfigParameter._Object._CompanyNameJobHistoryMandatoryInRecruitment, ConfigParameter._Object._PositionHeldMandatoryInRecruitment, _
                                             ConfigParameter._Object._SendJobConfirmationEmail, ConfigParameter._Object._isVacancyAlert, ConfigParameter._Object._MaxVacancyAlert, _
                                             ConfigParameter._Object._AttachApplicantCV, chkSendApplicantJobAlerts.Checked, ConfigParameter._Object._RecruitmentWebServiceLink, objbgwExport_Import, _
                                             ConfigParameter._Object._OneCurriculamVitaeMandatoryInRecruitment, ConfigParameter._Object._OneCoverLetterMandatoryInRecruitment, , , _
                                             ConfigParameter._Object._JobConfirmationTemplateId, ConfigParameter._Object._JobAlertTemplateId, ConfigParameter._Object._InternalJobAlertTemplateId, ConfigParameter._Object._ShowArutisignatureinemailnotification _
                                             ) Then
                'Hemant (30 Oct 2019) -- [ConfigParameter._Object._JobConfirmationTemplateId, ConfigParameter._Object._JobAlertTemplateId, ConfigParameter._Object._InternalJobAlertTemplateId]
                'Sohail (04 Jul 2019) - [blnOneCurriculamVitaeMandatory, blnOneCoverLetterMandatory]
                'Sohail (16 Nov 2018) - [RecruitmentWebServiceLink]
                'Pinkal (18-May-2018) -- 'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.[ConfigParameter._Object._AttachApplicantCV]


                'Pinkal (12-Feb-2018) -- 'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.[ ConfigParameter._Object._SendJobConfirmationEmail, ConfigParameter._Object._isVacancyAlert, ConfigParameter._Object._MaxVacancyAlert,chkSendApplicantJobAlerts.Checked]

                'Sohail (24 Jul 2017) - [_CompanyNameJobHistoryMandatoryInRecruitment, _PositionHeldMandatoryInRecruitment]
                'Nilay (13 Apr 2017) -- End

                'Sohail (05 Dec 2016) - [_CompanyDateFormat, _CompanyDateSeparator]
                'Shani(24-Aug-2015) -- End

                objeZeeWait.Active = False
                eZeeMsgBox.Show(objApplicant._Message, enMsgBoxStyle.Information)
                objeZeeWait.Visible = False
            Else
                objeZeeWait.Active = False
                eZeeMsgBox.Show(objApplicant._Message, enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoExportImport", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetControlVisibility()
        Try
            'Me.Hide()
            'Me.WindowState = FormWindowState.Minimized

            Me.ControlBox = False
            objeZeeWait.Visible = True
            objeZeeWait.Active = True
            btnStart.Enabled = False
            'Sohail (01 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            If mblnIsImport = True Then
                btnClose.Enabled = True
            Else
                btnClose.Enabled = False
            End If
            mblnStopWebImport = False
            'Sohail (01 Nov 2012) -- End
            Application.DoEvents()
            objeZeeWait.Refresh()

            'ntfSyncData.Visible = True
            'ntfSyncData.ShowBalloonTip(1000, ntfSyncData.BalloonTipTitle, ntfSyncData.BalloonTipText, ToolTipIcon.Info)

            'mblnIsWebProcess = True

            'Anjan (26 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            'Enhancement : for count of applicant on import data
            GintTotalApplicantToImport = 0
            'Anjan (26 Jan 2012)-End 



            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            objbgwExport_Import.RunWorkerAsync()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetControlVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Private Sub frmSynchronizeData_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objApplicant = New clsApplicant_master
        Try
            CheckForIllegalCrossThreadCalls = False
            Set_Logo(Me, gApplicationType)
            objeZeeWait.Visible = False
            objbgwExport_Import.WorkerReportsProgress = True 'Sohail (09 Feb 2012)
            'Sohail (01 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'btnClose.Visible = False
            'Sohail (01 Nov 2012) -- End

            If mblnIsImport = True Then
                objlblCaption.Text = Language.getMessage(mstrModuleName, 3, "You are about to Import data from Web,this process will take some time depending on Internet bandwidth. Do you want to continue?")
                objntfSyncData.BalloonTipTitle = Language.getMessage(mstrModuleName, 9, "Import Data")
                objntfSyncData.BalloonTipText = Language.getMessage(mstrModuleName, 10, "Importing Data From Web.")

                'Pinkal (18-May-2018) -- Start
                'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.
                chkSendApplicantJobAlerts.Checked = False
                chkSendApplicantJobAlerts.Visible = False
                'Pinkal (18-May-2018) -- End

            Else
                objlblCaption.Text = Language.getMessage(mstrModuleName, 4, "You are about to Export data to Web,this process will take some time depending on Internet bandwidth. Do you want to continue?")
                objntfSyncData.BalloonTipTitle = Language.getMessage(mstrModuleName, 11, "Export Data")
                objntfSyncData.BalloonTipText = Language.getMessage(mstrModuleName, 12, "Exporting Data to Web.")

                'Pinkal (18-May-2018) -- Start
                'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.
            chkSendApplicantJobAlerts.Visible = ConfigParameter._Object._isVacancyAlert
            chkSendApplicantJobAlerts.Checked = ConfigParameter._Object._isVacancyAlert
                'Pinkal (18-May-2018) -- End
            End If



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSynchronizeData_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsApplicant_master.SetMessages()
            objfrm._Other_ModuleNames = "clsApplicant_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If objeZeeWait.Active = True Then
                If mblnIsImport = True Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You are in middle of Importing data from web. If you close this window. You will not be able to Import the data.") & vbCrLf & _
                                                        Language.getMessage(mstrModuleName, 6, "Are you sure you want to continue?"), enMsgBoxStyle.Question + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then
                        objbgwExport_Import.CancelAsync()
                        'Sohail (01 Nov 2012) -- Start
                        'TRA - ENHANCEMENT
                        'Me.Close()
                        objlblCaption.Text = Language.getMessage(mstrModuleName, 13, "Please wait for a while until the import process for current applicant is completed.")
                        btnClose.Enabled = False
                        mblnStopWebImport = True
                        'Sohail (01 Nov 2012) -- End
                    End If
                Else
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You are in middle of Exporting data from web. If you close this window. You will not be able to Export the data.") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 8, "Are you sure you want to continue?"), enMsgBoxStyle.Question + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.Yes Then
                        objbgwExport_Import.CancelAsync()
                        Me.Close()
                    End If
                End If
            Else
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStart.Click
        Try
            Call SetControlVisibility()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnStart_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Control's Event(s) "

    Private Sub bgwExport_Import_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgwExport_Import.DoWork
        Try
            Call DoExportImport()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgwExport_Import_DoWork", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub bgwExport_Import_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgwExport_Import.ProgressChanged
        Application.DoEvents()
        objeZeeWait.Refresh()
        'lblProgress.Text = e.ProgressPercentage.ToString & " %" 'Sohail (09 Feb 2012)
        If GintTotalApplicantToImport > 0 Then
            lblProgress.Text = "[ " & e.ProgressPercentage.ToString & " / " & GintTotalApplicantToImport.ToString & " ]"
        End If

    End Sub

    Private Sub bgwExport_Import_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgwExport_Import.RunWorkerCompleted
        Try
            objntfSyncData.Visible = False
            lblProgress.Text = "" 'Sohail (09 Feb 2012)
            btnStart.Visible = False : btnClose.Visible = True : Me.ControlBox = True : objeZeeWait.Visible = False
            btnClose.Enabled = True 'Sohail (01 Nov 2012)

            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgwExport_Import_RunWorkerCompleted", mstrModuleName)
        Finally

        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnStart.GradientBackColor = GUI._ButttonBackColor
            Me.btnStart.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnStart.Text = Language._Object.getCaption(Me.btnStart.Name, Me.btnStart.Text)
            Me.lblProgress.Text = Language._Object.getCaption(Me.lblProgress.Name, Me.lblProgress.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Export data to Web")
            Language.setMessage(mstrModuleName, 2, "Import data from Web")
            Language.setMessage(mstrModuleName, 3, "You are about to Import data from Web,this process will take some time depending on Internet bandwidth. Do you want to continue?")
            Language.setMessage(mstrModuleName, 4, "You are about to Export data to Web,this process will take some time depending on Internet bandwidth. Do you want to continue?")
            Language.setMessage(mstrModuleName, 5, "You are in middle of Importing data from web. If you close this window. You will not be able to Import the data.")
            Language.setMessage(mstrModuleName, 6, "Are you sure you want to continue?")
            Language.setMessage(mstrModuleName, 7, "You are in middle of Exporting data from web. If you close this window. You will not be able to Export the data.")
            Language.setMessage(mstrModuleName, 8, "Are you sure you want to continue?")
            Language.setMessage(mstrModuleName, 9, "Import Data")
            Language.setMessage(mstrModuleName, 10, "Importing Data From Web.")
            Language.setMessage(mstrModuleName, 11, "Export Data")
            Language.setMessage(mstrModuleName, 12, "Exporting Data to Web.")
            Language.setMessage(mstrModuleName, 13, "Please wait for a while until the import process for current applicant is completed.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class