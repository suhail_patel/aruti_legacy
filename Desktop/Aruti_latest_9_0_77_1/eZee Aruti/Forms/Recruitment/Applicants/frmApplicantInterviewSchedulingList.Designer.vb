﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApplicantInterviewSchedulingList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApplicantInterviewSchedulingList))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.rbtnSplitButton = New eZee.Common.eZeeSplitButton
        Me.mnuEvaluation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuAnalyse = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuChangeBatch = New System.Windows.Forms.ToolStripMenuItem
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchApplicant = New eZee.Common.eZeeGradientButton
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.lblVacancyType = New System.Windows.Forms.Label
        Me.cboBatchName = New System.Windows.Forms.ComboBox
        Me.txtBatchCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblBatchName = New System.Windows.Forms.Label
        Me.lblBatchCode = New System.Windows.Forms.Label
        Me.cboResultGroup = New System.Windows.Forms.ComboBox
        Me.lblResultGroup = New System.Windows.Forms.Label
        Me.objStLine = New eZee.Common.eZeeStraightLine
        Me.dtpInterviewDateFrom = New System.Windows.Forms.DateTimePicker
        Me.lblInterviewFrom = New System.Windows.Forms.Label
        Me.txtLocation = New eZee.TextBox.AlphanumericTextBox
        Me.lblEnrollmentDateTo = New System.Windows.Forms.Label
        Me.radShowAll = New System.Windows.Forms.RadioButton
        Me.lblLocation = New System.Windows.Forms.Label
        Me.radCancelled = New System.Windows.Forms.RadioButton
        Me.dtpInterviewTo = New System.Windows.Forms.DateTimePicker
        Me.radScheduled = New System.Windows.Forms.RadioButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboInterviewType = New System.Windows.Forms.ComboBox
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.cboApplicant = New System.Windows.Forms.ComboBox
        Me.lblInterviewType = New System.Windows.Forms.Label
        Me.lblVacancy = New System.Windows.Forms.Label
        Me.lblApplicant = New System.Windows.Forms.Label
        Me.pnlEnrollmentAndCancellationList = New System.Windows.Forms.Panel
        Me.lvAppInterviewScheduling = New eZee.Common.eZeeListView(Me.components)
        Me.colhBatchName = New System.Windows.Forms.ColumnHeader
        Me.colhApplicant = New System.Windows.Forms.ColumnHeader
        Me.colhVacancy = New System.Windows.Forms.ColumnHeader
        Me.colhInterviewType = New System.Windows.Forms.ColumnHeader
        Me.colhInterviewDate = New System.Windows.Forms.ColumnHeader
        Me.colhLocation = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsVoid = New System.Windows.Forms.ColumnHeader
        Me.objcolhIsCancel = New System.Windows.Forms.ColumnHeader
        Me.objcolhGrp = New System.Windows.Forms.ColumnHeader
        Me.colhInterviewer = New System.Windows.Forms.ColumnHeader
        Me.colhScore = New System.Windows.Forms.ColumnHeader
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter.SuspendLayout()
        Me.mnuEvaluation.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlEnrollmentAndCancellationList.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.rbtnSplitButton)
        Me.objFooter.Controls.Add(Me.btnCancel)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 440)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(790, 55)
        Me.objFooter.TabIndex = 0
        '
        'rbtnSplitButton
        '
        Me.rbtnSplitButton.BorderColor = System.Drawing.Color.Black
        Me.rbtnSplitButton.ContextMenuStrip = Me.mnuEvaluation
        Me.rbtnSplitButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnSplitButton.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.rbtnSplitButton.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.rbtnSplitButton.Location = New System.Drawing.Point(12, 13)
        Me.rbtnSplitButton.Name = "rbtnSplitButton"
        Me.rbtnSplitButton.ShowDefaultBorderColor = True
        Me.rbtnSplitButton.Size = New System.Drawing.Size(110, 30)
        Me.rbtnSplitButton.SplitButtonMenu = Me.mnuEvaluation
        Me.rbtnSplitButton.TabIndex = 0
        Me.rbtnSplitButton.Text = "Operations"
        '
        'mnuEvaluation
        '
        Me.mnuEvaluation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAnalyse, Me.mnuChangeBatch})
        Me.mnuEvaluation.Name = "mnuEvaluation"
        Me.mnuEvaluation.Size = New System.Drawing.Size(149, 48)
        '
        'mnuAnalyse
        '
        Me.mnuAnalyse.Name = "mnuAnalyse"
        Me.mnuAnalyse.Size = New System.Drawing.Size(148, 22)
        Me.mnuAnalyse.Text = "Analyse"
        '
        'mnuChangeBatch
        '
        Me.mnuChangeBatch.Name = "mnuChangeBatch"
        Me.mnuChangeBatch.Size = New System.Drawing.Size(148, 22)
        Me.mnuChangeBatch.Text = "Change Batch"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(372, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Ca&ncel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(475, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 2
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(681, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(578, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 3
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchApplicant)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.cboBatchName)
        Me.gbFilterCriteria.Controls.Add(Me.txtBatchCode)
        Me.gbFilterCriteria.Controls.Add(Me.lblBatchName)
        Me.gbFilterCriteria.Controls.Add(Me.lblBatchCode)
        Me.gbFilterCriteria.Controls.Add(Me.cboResultGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblResultGroup)
        Me.gbFilterCriteria.Controls.Add(Me.objStLine)
        Me.gbFilterCriteria.Controls.Add(Me.dtpInterviewDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblInterviewFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtLocation)
        Me.gbFilterCriteria.Controls.Add(Me.lblEnrollmentDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.radShowAll)
        Me.gbFilterCriteria.Controls.Add(Me.lblLocation)
        Me.gbFilterCriteria.Controls.Add(Me.radCancelled)
        Me.gbFilterCriteria.Controls.Add(Me.dtpInterviewTo)
        Me.gbFilterCriteria.Controls.Add(Me.radScheduled)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboInterviewType)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.cboApplicant)
        Me.gbFilterCriteria.Controls.Add(Me.lblInterviewType)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.lblApplicant)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 121
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(767, 142)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(450, 60)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 95
        '
        'objbtnSearchApplicant
        '
        Me.objbtnSearchApplicant.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchApplicant.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchApplicant.BorderSelected = False
        Me.objbtnSearchApplicant.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchApplicant.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchApplicant.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchApplicant.Location = New System.Drawing.Point(215, 33)
        Me.objbtnSearchApplicant.Name = "objbtnSearchApplicant"
        Me.objbtnSearchApplicant.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchApplicant.TabIndex = 94
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(88, 60)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(121, 21)
        Me.cboVacancyType.TabIndex = 7
        '
        'lblVacancyType
        '
        Me.lblVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyType.Location = New System.Drawing.Point(8, 63)
        Me.lblVacancyType.Name = "lblVacancyType"
        Me.lblVacancyType.Size = New System.Drawing.Size(75, 15)
        Me.lblVacancyType.TabIndex = 6
        Me.lblVacancyType.Text = "Vacancy Type"
        Me.lblVacancyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBatchName
        '
        Me.cboBatchName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBatchName.DropDownWidth = 300
        Me.cboBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBatchName.FormattingEnabled = True
        Me.cboBatchName.Location = New System.Drawing.Point(546, 33)
        Me.cboBatchName.Name = "cboBatchName"
        Me.cboBatchName.Size = New System.Drawing.Size(121, 21)
        Me.cboBatchName.TabIndex = 5
        '
        'txtBatchCode
        '
        Me.txtBatchCode.Flags = 0
        Me.txtBatchCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBatchCode.Location = New System.Drawing.Point(326, 33)
        Me.txtBatchCode.Name = "txtBatchCode"
        Me.txtBatchCode.Size = New System.Drawing.Size(121, 21)
        Me.txtBatchCode.TabIndex = 3
        '
        'lblBatchName
        '
        Me.lblBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchName.Location = New System.Drawing.Point(472, 35)
        Me.lblBatchName.Name = "lblBatchName"
        Me.lblBatchName.Size = New System.Drawing.Size(69, 16)
        Me.lblBatchName.TabIndex = 4
        Me.lblBatchName.Text = "Batch Name"
        Me.lblBatchName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBatchCode
        '
        Me.lblBatchCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchCode.Location = New System.Drawing.Point(238, 35)
        Me.lblBatchCode.Name = "lblBatchCode"
        Me.lblBatchCode.Size = New System.Drawing.Size(82, 16)
        Me.lblBatchCode.TabIndex = 2
        Me.lblBatchCode.Text = "Batch Code"
        Me.lblBatchCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboResultGroup
        '
        Me.cboResultGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultGroup.DropDownWidth = 150
        Me.cboResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultGroup.FormattingEnabled = True
        Me.cboResultGroup.Location = New System.Drawing.Point(546, 60)
        Me.cboResultGroup.Name = "cboResultGroup"
        Me.cboResultGroup.Size = New System.Drawing.Size(121, 21)
        Me.cboResultGroup.TabIndex = 11
        '
        'lblResultGroup
        '
        Me.lblResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultGroup.Location = New System.Drawing.Point(472, 62)
        Me.lblResultGroup.Name = "lblResultGroup"
        Me.lblResultGroup.Size = New System.Drawing.Size(70, 16)
        Me.lblResultGroup.TabIndex = 10
        Me.lblResultGroup.Text = "Result Group"
        Me.lblResultGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objStLine
        '
        Me.objStLine.BackColor = System.Drawing.Color.Transparent
        Me.objStLine.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine.Location = New System.Drawing.Point(670, 36)
        Me.objStLine.Name = "objStLine"
        Me.objStLine.Size = New System.Drawing.Size(10, 70)
        Me.objStLine.TabIndex = 20
        '
        'dtpInterviewDateFrom
        '
        Me.dtpInterviewDateFrom.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpInterviewDateFrom.Checked = False
        Me.dtpInterviewDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpInterviewDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpInterviewDateFrom.Location = New System.Drawing.Point(326, 87)
        Me.dtpInterviewDateFrom.Name = "dtpInterviewDateFrom"
        Me.dtpInterviewDateFrom.ShowCheckBox = True
        Me.dtpInterviewDateFrom.Size = New System.Drawing.Size(121, 21)
        Me.dtpInterviewDateFrom.TabIndex = 15
        '
        'lblInterviewFrom
        '
        Me.lblInterviewFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewFrom.Location = New System.Drawing.Point(238, 90)
        Me.lblInterviewFrom.Name = "lblInterviewFrom"
        Me.lblInterviewFrom.Size = New System.Drawing.Size(82, 15)
        Me.lblInterviewFrom.TabIndex = 14
        Me.lblInterviewFrom.Text = "Interview Date"
        '
        'txtLocation
        '
        Me.txtLocation.Flags = 0
        Me.txtLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocation.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLocation.Location = New System.Drawing.Point(88, 114)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(579, 21)
        Me.txtLocation.TabIndex = 19
        '
        'lblEnrollmentDateTo
        '
        Me.lblEnrollmentDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnrollmentDateTo.Location = New System.Drawing.Point(472, 90)
        Me.lblEnrollmentDateTo.Name = "lblEnrollmentDateTo"
        Me.lblEnrollmentDateTo.Size = New System.Drawing.Size(69, 15)
        Me.lblEnrollmentDateTo.TabIndex = 16
        Me.lblEnrollmentDateTo.Text = "To"
        '
        'radShowAll
        '
        Me.radShowAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radShowAll.Location = New System.Drawing.Point(683, 89)
        Me.radShowAll.Name = "radShowAll"
        Me.radShowAll.Size = New System.Drawing.Size(77, 17)
        Me.radShowAll.TabIndex = 23
        Me.radShowAll.TabStop = True
        Me.radShowAll.Text = "Show All"
        Me.radShowAll.UseVisualStyleBackColor = True
        '
        'lblLocation
        '
        Me.lblLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocation.Location = New System.Drawing.Point(8, 114)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(75, 15)
        Me.lblLocation.TabIndex = 18
        Me.lblLocation.Text = "Location"
        '
        'radCancelled
        '
        Me.radCancelled.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCancelled.Location = New System.Drawing.Point(683, 62)
        Me.radCancelled.Name = "radCancelled"
        Me.radCancelled.Size = New System.Drawing.Size(77, 17)
        Me.radCancelled.TabIndex = 22
        Me.radCancelled.TabStop = True
        Me.radCancelled.Text = "Cancelled"
        Me.radCancelled.UseVisualStyleBackColor = True
        '
        'dtpInterviewTo
        '
        Me.dtpInterviewTo.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpInterviewTo.Checked = False
        Me.dtpInterviewTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpInterviewTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpInterviewTo.Location = New System.Drawing.Point(546, 87)
        Me.dtpInterviewTo.Name = "dtpInterviewTo"
        Me.dtpInterviewTo.ShowCheckBox = True
        Me.dtpInterviewTo.Size = New System.Drawing.Size(121, 21)
        Me.dtpInterviewTo.TabIndex = 17
        '
        'radScheduled
        '
        Me.radScheduled.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radScheduled.Location = New System.Drawing.Point(683, 35)
        Me.radScheduled.Name = "radScheduled"
        Me.radScheduled.Size = New System.Drawing.Size(77, 17)
        Me.radScheduled.TabIndex = 21
        Me.radScheduled.TabStop = True
        Me.radScheduled.Text = "Scheduled"
        Me.radScheduled.UseVisualStyleBackColor = True
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(740, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 4
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(717, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 3
        Me.objbtnSearch.TabStop = False
        '
        'cboInterviewType
        '
        Me.cboInterviewType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterviewType.DropDownWidth = 360
        Me.cboInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterviewType.FormattingEnabled = True
        Me.cboInterviewType.Location = New System.Drawing.Point(88, 87)
        Me.cboInterviewType.Name = "cboInterviewType"
        Me.cboInterviewType.Size = New System.Drawing.Size(121, 21)
        Me.cboInterviewType.TabIndex = 13
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 360
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(326, 60)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(121, 21)
        Me.cboVacancy.TabIndex = 9
        '
        'cboApplicant
        '
        Me.cboApplicant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApplicant.DropDownWidth = 360
        Me.cboApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApplicant.FormattingEnabled = True
        Me.cboApplicant.Location = New System.Drawing.Point(88, 33)
        Me.cboApplicant.Name = "cboApplicant"
        Me.cboApplicant.Size = New System.Drawing.Size(121, 21)
        Me.cboApplicant.TabIndex = 1
        '
        'lblInterviewType
        '
        Me.lblInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewType.Location = New System.Drawing.Point(8, 90)
        Me.lblInterviewType.Name = "lblInterviewType"
        Me.lblInterviewType.Size = New System.Drawing.Size(75, 15)
        Me.lblInterviewType.TabIndex = 12
        Me.lblInterviewType.Text = "Int. Type"
        '
        'lblVacancy
        '
        Me.lblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancy.Location = New System.Drawing.Point(238, 63)
        Me.lblVacancy.Name = "lblVacancy"
        Me.lblVacancy.Size = New System.Drawing.Size(64, 15)
        Me.lblVacancy.TabIndex = 8
        Me.lblVacancy.Text = "Vacancy"
        '
        'lblApplicant
        '
        Me.lblApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicant.Location = New System.Drawing.Point(8, 36)
        Me.lblApplicant.Name = "lblApplicant"
        Me.lblApplicant.Size = New System.Drawing.Size(75, 15)
        Me.lblApplicant.TabIndex = 0
        Me.lblApplicant.Text = "Applicant"
        '
        'pnlEnrollmentAndCancellationList
        '
        Me.pnlEnrollmentAndCancellationList.Controls.Add(Me.lvAppInterviewScheduling)
        Me.pnlEnrollmentAndCancellationList.Controls.Add(Me.eZeeHeader)
        Me.pnlEnrollmentAndCancellationList.Controls.Add(Me.gbFilterCriteria)
        Me.pnlEnrollmentAndCancellationList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEnrollmentAndCancellationList.Location = New System.Drawing.Point(0, 0)
        Me.pnlEnrollmentAndCancellationList.Name = "pnlEnrollmentAndCancellationList"
        Me.pnlEnrollmentAndCancellationList.Size = New System.Drawing.Size(790, 495)
        Me.pnlEnrollmentAndCancellationList.TabIndex = 0
        '
        'lvAppInterviewScheduling
        '
        Me.lvAppInterviewScheduling.BackColorOnChecked = True
        Me.lvAppInterviewScheduling.ColumnHeaders = Nothing
        Me.lvAppInterviewScheduling.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhBatchName, Me.colhApplicant, Me.colhVacancy, Me.colhInterviewType, Me.colhInterviewDate, Me.colhLocation, Me.colhDescription, Me.objcolhIsVoid, Me.objcolhIsCancel, Me.objcolhGrp, Me.colhInterviewer, Me.colhScore})
        Me.lvAppInterviewScheduling.CompulsoryColumns = ""
        Me.lvAppInterviewScheduling.FullRowSelect = True
        Me.lvAppInterviewScheduling.GridLines = True
        Me.lvAppInterviewScheduling.GroupingColumn = Nothing
        Me.lvAppInterviewScheduling.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvAppInterviewScheduling.HideSelection = False
        Me.lvAppInterviewScheduling.Location = New System.Drawing.Point(12, 212)
        Me.lvAppInterviewScheduling.MinColumnWidth = 50
        Me.lvAppInterviewScheduling.MultiSelect = False
        Me.lvAppInterviewScheduling.Name = "lvAppInterviewScheduling"
        Me.lvAppInterviewScheduling.OptionalColumns = ""
        Me.lvAppInterviewScheduling.ShowMoreItem = False
        Me.lvAppInterviewScheduling.ShowSaveItem = False
        Me.lvAppInterviewScheduling.ShowSelectAll = True
        Me.lvAppInterviewScheduling.ShowSizeAllColumnsToFit = True
        Me.lvAppInterviewScheduling.Size = New System.Drawing.Size(767, 224)
        Me.lvAppInterviewScheduling.Sortable = True
        Me.lvAppInterviewScheduling.TabIndex = 2
        Me.lvAppInterviewScheduling.UseCompatibleStateImageBehavior = False
        Me.lvAppInterviewScheduling.View = System.Windows.Forms.View.Details
        '
        'colhBatchName
        '
        Me.colhBatchName.Text = "Batch Name"
        Me.colhBatchName.Width = 0
        '
        'colhApplicant
        '
        Me.colhApplicant.Text = "Applicant"
        Me.colhApplicant.Width = 0
        '
        'colhVacancy
        '
        Me.colhVacancy.Text = "Vacancy"
        Me.colhVacancy.Width = 155
        '
        'colhInterviewType
        '
        Me.colhInterviewType.Text = "Interview Type"
        Me.colhInterviewType.Width = 100
        '
        'colhInterviewDate
        '
        Me.colhInterviewDate.Text = "Interview Date"
        Me.colhInterviewDate.Width = 90
        '
        'colhLocation
        '
        Me.colhLocation.Text = "Location"
        Me.colhLocation.Width = 80
        '
        'colhDescription
        '
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 120
        '
        'objcolhIsVoid
        '
        Me.objcolhIsVoid.Width = 0
        '
        'objcolhIsCancel
        '
        Me.objcolhIsCancel.Width = 0
        '
        'objcolhGrp
        '
        Me.objcolhGrp.Tag = "objcolhGrp"
        Me.objcolhGrp.Text = ""
        Me.objcolhGrp.Width = 0
        '
        'colhInterviewer
        '
        Me.colhInterviewer.Tag = "colhInterviewer"
        Me.colhInterviewer.Text = "Interviewer"
        Me.colhInterviewer.Width = 115
        '
        'colhScore
        '
        Me.colhScore.Tag = "colhScore"
        Me.colhScore.Text = "Score"
        Me.colhScore.Width = 100
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(790, 58)
        Me.eZeeHeader.TabIndex = 0
        Me.eZeeHeader.Title = "Applicant Interview Scheduling List"
        '
        'frmApplicantInterviewSchedulingList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(790, 495)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlEnrollmentAndCancellationList)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApplicantInterviewSchedulingList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Applicant Interview Scheduling List"
        Me.objFooter.ResumeLayout(False)
        Me.mnuEvaluation.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlEnrollmentAndCancellationList.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents dtpInterviewTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpInterviewDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboInterviewType As System.Windows.Forms.ComboBox
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents cboApplicant As System.Windows.Forms.ComboBox
    Friend WithEvents lblEnrollmentDateTo As System.Windows.Forms.Label
    Friend WithEvents lblInterviewFrom As System.Windows.Forms.Label
    Friend WithEvents lblInterviewType As System.Windows.Forms.Label
    Friend WithEvents lblVacancy As System.Windows.Forms.Label
    Friend WithEvents lblApplicant As System.Windows.Forms.Label
    Friend WithEvents pnlEnrollmentAndCancellationList As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents radCancelled As System.Windows.Forms.RadioButton
    Friend WithEvents radScheduled As System.Windows.Forms.RadioButton
    Friend WithEvents lblLocation As System.Windows.Forms.Label
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents radShowAll As System.Windows.Forms.RadioButton
    Friend WithEvents mnuEvaluation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuAnalyse As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtLocation As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objStLine As eZee.Common.eZeeStraightLine
    Friend WithEvents cboResultGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblResultGroup As System.Windows.Forms.Label
    Friend WithEvents lvAppInterviewScheduling As eZee.Common.eZeeListView
    Friend WithEvents colhApplicant As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhVacancy As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInterviewType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInterviewDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLocation As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtBatchCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBatchName As System.Windows.Forms.Label
    Friend WithEvents lblBatchCode As System.Windows.Forms.Label
    Friend WithEvents colhBatchName As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboBatchName As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objcolhIsVoid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIsCancel As System.Windows.Forms.ColumnHeader
    Friend WithEvents rbtnSplitButton As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuChangeBatch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancyType As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchApplicant As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents objcolhGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInterviewer As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhScore As System.Windows.Forms.ColumnHeader
End Class
