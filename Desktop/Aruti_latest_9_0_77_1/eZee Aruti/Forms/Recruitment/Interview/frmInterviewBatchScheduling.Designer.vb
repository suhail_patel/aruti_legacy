﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInterviewBatchScheduling
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInterviewBatchScheduling))
        Me.pnlScheduleInterview = New System.Windows.Forms.Panel
        Me.tabcBatchSchdule = New System.Windows.Forms.TabControl
        Me.tabpBatchScheduleInfo = New System.Windows.Forms.TabPage
        Me.gbScheduleInterview = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.lblVacancyType = New System.Windows.Forms.Label
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.elBatchInformation = New eZee.Common.eZeeLine
        Me.elVacancyInfo = New eZee.Common.eZeeLine
        Me.objbtnAddRGroup = New eZee.Common.eZeeGradientButton
        Me.tabcRemarks = New System.Windows.Forms.TabControl
        Me.tabpDescription = New System.Windows.Forms.TabPage
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.tabpCancelRemark = New System.Windows.Forms.TabPage
        Me.txtCancelRemark = New eZee.TextBox.AlphanumericTextBox
        Me.txtBatchCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtBatchName = New eZee.TextBox.AlphanumericTextBox
        Me.cboResultGroup = New System.Windows.Forms.ComboBox
        Me.lblResultGroup = New System.Windows.Forms.Label
        Me.lblBatchName = New System.Windows.Forms.Label
        Me.lblBatchCode = New System.Windows.Forms.Label
        Me.txtLocation = New eZee.TextBox.AlphanumericTextBox
        Me.lblLocation = New System.Windows.Forms.Label
        Me.dtInterviewTime = New System.Windows.Forms.DateTimePicker
        Me.lblInterviewDateFrom = New System.Windows.Forms.Label
        Me.dtInterviewDate = New System.Windows.Forms.DateTimePicker
        Me.cboInterviewType = New System.Windows.Forms.ComboBox
        Me.lblInterview = New System.Windows.Forms.Label
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.tabpInterview = New System.Windows.Forms.TabPage
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lvInterviewer = New System.Windows.Forms.ListView
        Me.colhLevel = New System.Windows.Forms.ColumnHeader
        Me.colhEmpName = New System.Windows.Forms.ColumnHeader
        Me.colhCompany = New System.Windows.Forms.ColumnHeader
        Me.colhDepartment = New System.Windows.Forms.ColumnHeader
        Me.colhContactNo = New System.Windows.Forms.ColumnHeader
        Me.colhInterviewType = New System.Windows.Forms.ColumnHeader
        Me.objcolhInterviewerunkid = New System.Windows.Forms.ColumnHeader
        Me.objcolhIntGUID = New System.Windows.Forms.ColumnHeader
        Me.objcolhInterviewtypeunkid = New System.Windows.Forms.ColumnHeader
        Me.gbInterviewer = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboiInterViewType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblLevelInfo = New System.Windows.Forms.Label
        Me.nudLevel = New System.Windows.Forms.NumericUpDown
        Me.lblLevel = New System.Windows.Forms.Label
        Me.EZeeLine3 = New eZee.Common.eZeeLine
        Me.objbtnAddInterviewType = New eZee.Common.eZeeGradientButton
        Me.lblInterViewType = New System.Windows.Forms.Label
        Me.pnlOthers = New System.Windows.Forms.Panel
        Me.lblTrainerContactNo = New System.Windows.Forms.Label
        Me.txtOtherContactNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblOtherDepartment = New System.Windows.Forms.Label
        Me.txtOtherCompany = New eZee.TextBox.AlphanumericTextBox
        Me.txtOtherDepartment = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtOtherInterviewerName = New eZee.TextBox.AlphanumericTextBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.radEmployee = New System.Windows.Forms.RadioButton
        Me.objColon3 = New System.Windows.Forms.Label
        Me.lblEmployeeDepartment = New System.Windows.Forms.Label
        Me.radOthers = New System.Windows.Forms.RadioButton
        Me.objColon2 = New System.Windows.Forms.Label
        Me.objColon1 = New System.Windows.Forms.Label
        Me.lblEmpContact = New System.Windows.Forms.Label
        Me.lblEmpCompany = New System.Windows.Forms.Label
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.lblEmployeeContactNo = New System.Windows.Forms.Label
        Me.lblEmployeeCompany = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.chkIsClosed = New System.Windows.Forms.CheckBox
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnActivate = New eZee.Common.eZeeLightButton(Me.components)
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.AlphanumericTextBox2 = New eZee.TextBox.AlphanumericTextBox
        Me.AlphanumericTextBox1 = New eZee.TextBox.AlphanumericTextBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.pnlScheduleInterview.SuspendLayout()
        Me.tabcBatchSchdule.SuspendLayout()
        Me.tabpBatchScheduleInfo.SuspendLayout()
        Me.gbScheduleInterview.SuspendLayout()
        Me.tabcRemarks.SuspendLayout()
        Me.tabpDescription.SuspendLayout()
        Me.tabpCancelRemark.SuspendLayout()
        Me.tabpInterview.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.gbInterviewer.SuspendLayout()
        CType(Me.nudLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOthers.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlScheduleInterview
        '
        Me.pnlScheduleInterview.Controls.Add(Me.tabcBatchSchdule)
        Me.pnlScheduleInterview.Controls.Add(Me.EZeeFooter1)
        Me.pnlScheduleInterview.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlScheduleInterview.Location = New System.Drawing.Point(0, 0)
        Me.pnlScheduleInterview.Name = "pnlScheduleInterview"
        Me.pnlScheduleInterview.Size = New System.Drawing.Size(853, 441)
        Me.pnlScheduleInterview.TabIndex = 0
        '
        'tabcBatchSchdule
        '
        Me.tabcBatchSchdule.Controls.Add(Me.tabpBatchScheduleInfo)
        Me.tabcBatchSchdule.Controls.Add(Me.tabpInterview)
        Me.tabcBatchSchdule.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcBatchSchdule.Location = New System.Drawing.Point(3, 3)
        Me.tabcBatchSchdule.Name = "tabcBatchSchdule"
        Me.tabcBatchSchdule.SelectedIndex = 0
        Me.tabcBatchSchdule.Size = New System.Drawing.Size(849, 385)
        Me.tabcBatchSchdule.TabIndex = 20
        '
        'tabpBatchScheduleInfo
        '
        Me.tabpBatchScheduleInfo.Controls.Add(Me.gbScheduleInterview)
        Me.tabpBatchScheduleInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabpBatchScheduleInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpBatchScheduleInfo.Name = "tabpBatchScheduleInfo"
        Me.tabpBatchScheduleInfo.Size = New System.Drawing.Size(841, 359)
        Me.tabpBatchScheduleInfo.TabIndex = 0
        Me.tabpBatchScheduleInfo.Text = "Batch Schedule Info"
        Me.tabpBatchScheduleInfo.UseVisualStyleBackColor = True
        '
        'gbScheduleInterview
        '
        Me.gbScheduleInterview.BorderColor = System.Drawing.Color.Black
        Me.gbScheduleInterview.Checked = False
        Me.gbScheduleInterview.CollapseAllExceptThis = False
        Me.gbScheduleInterview.CollapsedHoverImage = Nothing
        Me.gbScheduleInterview.CollapsedNormalImage = Nothing
        Me.gbScheduleInterview.CollapsedPressedImage = Nothing
        Me.gbScheduleInterview.CollapseOnLoad = False
        Me.gbScheduleInterview.Controls.Add(Me.cboVacancyType)
        Me.gbScheduleInterview.Controls.Add(Me.lblVacancyType)
        Me.gbScheduleInterview.Controls.Add(Me.objlblCaption)
        Me.gbScheduleInterview.Controls.Add(Me.elBatchInformation)
        Me.gbScheduleInterview.Controls.Add(Me.elVacancyInfo)
        Me.gbScheduleInterview.Controls.Add(Me.objbtnAddRGroup)
        Me.gbScheduleInterview.Controls.Add(Me.tabcRemarks)
        Me.gbScheduleInterview.Controls.Add(Me.txtBatchCode)
        Me.gbScheduleInterview.Controls.Add(Me.txtBatchName)
        Me.gbScheduleInterview.Controls.Add(Me.cboResultGroup)
        Me.gbScheduleInterview.Controls.Add(Me.lblResultGroup)
        Me.gbScheduleInterview.Controls.Add(Me.lblBatchName)
        Me.gbScheduleInterview.Controls.Add(Me.lblBatchCode)
        Me.gbScheduleInterview.Controls.Add(Me.txtLocation)
        Me.gbScheduleInterview.Controls.Add(Me.lblLocation)
        Me.gbScheduleInterview.Controls.Add(Me.dtInterviewTime)
        Me.gbScheduleInterview.Controls.Add(Me.lblInterviewDateFrom)
        Me.gbScheduleInterview.Controls.Add(Me.dtInterviewDate)
        Me.gbScheduleInterview.Controls.Add(Me.cboInterviewType)
        Me.gbScheduleInterview.Controls.Add(Me.lblInterview)
        Me.gbScheduleInterview.Controls.Add(Me.cboVacancy)
        Me.gbScheduleInterview.Controls.Add(Me.lblJob)
        Me.gbScheduleInterview.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbScheduleInterview.ExpandedHoverImage = Nothing
        Me.gbScheduleInterview.ExpandedNormalImage = Nothing
        Me.gbScheduleInterview.ExpandedPressedImage = Nothing
        Me.gbScheduleInterview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbScheduleInterview.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbScheduleInterview.HeaderHeight = 25
        Me.gbScheduleInterview.HeaderMessage = ""
        Me.gbScheduleInterview.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbScheduleInterview.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbScheduleInterview.HeightOnCollapse = 0
        Me.gbScheduleInterview.LeftTextSpace = 0
        Me.gbScheduleInterview.Location = New System.Drawing.Point(0, 0)
        Me.gbScheduleInterview.Name = "gbScheduleInterview"
        Me.gbScheduleInterview.OpenHeight = 174
        Me.gbScheduleInterview.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbScheduleInterview.ShowBorder = True
        Me.gbScheduleInterview.ShowCheckBox = False
        Me.gbScheduleInterview.ShowCollapseButton = False
        Me.gbScheduleInterview.ShowDefaultBorderColor = True
        Me.gbScheduleInterview.ShowDownButton = False
        Me.gbScheduleInterview.ShowHeader = True
        Me.gbScheduleInterview.Size = New System.Drawing.Size(841, 359)
        Me.gbScheduleInterview.TabIndex = 3
        Me.gbScheduleInterview.Temp = 0
        Me.gbScheduleInterview.Text = "Batch Creation"
        Me.gbScheduleInterview.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(126, 60)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(149, 21)
        Me.cboVacancyType.TabIndex = 225
        '
        'lblVacancyType
        '
        Me.lblVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancyType.Location = New System.Drawing.Point(31, 62)
        Me.lblVacancyType.Name = "lblVacancyType"
        Me.lblVacancyType.Size = New System.Drawing.Size(89, 16)
        Me.lblVacancyType.TabIndex = 224
        Me.lblVacancyType.Text = "Vacancy Type"
        Me.lblVacancyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(129, 118)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(384, 17)
        Me.objlblCaption.TabIndex = 222
        Me.objlblCaption.Text = "#Vacancy Information"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elBatchInformation
        '
        Me.elBatchInformation.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elBatchInformation.Location = New System.Drawing.Point(9, 139)
        Me.elBatchInformation.Name = "elBatchInformation"
        Me.elBatchInformation.Size = New System.Drawing.Size(266, 17)
        Me.elBatchInformation.TabIndex = 3
        Me.elBatchInformation.Text = "Batch Information"
        Me.elBatchInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elVacancyInfo
        '
        Me.elVacancyInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elVacancyInfo.Location = New System.Drawing.Point(8, 36)
        Me.elVacancyInfo.Name = "elVacancyInfo"
        Me.elVacancyInfo.Size = New System.Drawing.Size(264, 17)
        Me.elVacancyInfo.TabIndex = 0
        Me.elVacancyInfo.Text = "Vacancy Information"
        Me.elVacancyInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddRGroup
        '
        Me.objbtnAddRGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddRGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddRGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddRGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddRGroup.BorderSelected = False
        Me.objbtnAddRGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddRGroup.Image = CType(resources.GetObject("objbtnAddRGroup.Image"), System.Drawing.Image)
        Me.objbtnAddRGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddRGroup.Location = New System.Drawing.Point(645, 216)
        Me.objbtnAddRGroup.Name = "objbtnAddRGroup"
        Me.objbtnAddRGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddRGroup.TabIndex = 17
        '
        'tabcRemarks
        '
        Me.tabcRemarks.Controls.Add(Me.tabpDescription)
        Me.tabcRemarks.Controls.Add(Me.tabpCancelRemark)
        Me.tabcRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcRemarks.Location = New System.Drawing.Point(12, 243)
        Me.tabcRemarks.Name = "tabcRemarks"
        Me.tabcRemarks.SelectedIndex = 0
        Me.tabcRemarks.Size = New System.Drawing.Size(654, 115)
        Me.tabcRemarks.TabIndex = 19
        '
        'tabpDescription
        '
        Me.tabpDescription.Controls.Add(Me.txtDescription)
        Me.tabpDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabpDescription.Location = New System.Drawing.Point(4, 22)
        Me.tabpDescription.Name = "tabpDescription"
        Me.tabpDescription.Size = New System.Drawing.Size(646, 89)
        Me.tabpDescription.TabIndex = 0
        Me.tabpDescription.Text = "Description"
        Me.tabpDescription.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(0, 0)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(646, 89)
        Me.txtDescription.TabIndex = 0
        '
        'tabpCancelRemark
        '
        Me.tabpCancelRemark.Controls.Add(Me.txtCancelRemark)
        Me.tabpCancelRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabpCancelRemark.Location = New System.Drawing.Point(4, 22)
        Me.tabpCancelRemark.Name = "tabpCancelRemark"
        Me.tabpCancelRemark.Size = New System.Drawing.Size(520, 89)
        Me.tabpCancelRemark.TabIndex = 1
        Me.tabpCancelRemark.Text = "Cancel Remark"
        Me.tabpCancelRemark.UseVisualStyleBackColor = True
        '
        'txtCancelRemark
        '
        Me.txtCancelRemark.BackColor = System.Drawing.SystemColors.Window
        Me.txtCancelRemark.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCancelRemark.Flags = 0
        Me.txtCancelRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCancelRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCancelRemark.Location = New System.Drawing.Point(0, 0)
        Me.txtCancelRemark.Multiline = True
        Me.txtCancelRemark.Name = "txtCancelRemark"
        Me.txtCancelRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCancelRemark.Size = New System.Drawing.Size(520, 89)
        Me.txtCancelRemark.TabIndex = 215
        '
        'txtBatchCode
        '
        Me.txtBatchCode.Flags = 0
        Me.txtBatchCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBatchCode.Location = New System.Drawing.Point(126, 162)
        Me.txtBatchCode.Name = "txtBatchCode"
        Me.txtBatchCode.Size = New System.Drawing.Size(149, 21)
        Me.txtBatchCode.TabIndex = 5
        '
        'txtBatchName
        '
        Me.txtBatchName.Flags = 0
        Me.txtBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBatchName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBatchName.Location = New System.Drawing.Point(126, 189)
        Me.txtBatchName.Name = "txtBatchName"
        Me.txtBatchName.Size = New System.Drawing.Size(149, 21)
        Me.txtBatchName.TabIndex = 7
        '
        'cboResultGroup
        '
        Me.cboResultGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultGroup.FormattingEnabled = True
        Me.cboResultGroup.Location = New System.Drawing.Point(496, 216)
        Me.cboResultGroup.Name = "cboResultGroup"
        Me.cboResultGroup.Size = New System.Drawing.Size(143, 21)
        Me.cboResultGroup.TabIndex = 16
        '
        'lblResultGroup
        '
        Me.lblResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultGroup.Location = New System.Drawing.Point(407, 218)
        Me.lblResultGroup.Name = "lblResultGroup"
        Me.lblResultGroup.Size = New System.Drawing.Size(83, 16)
        Me.lblResultGroup.TabIndex = 15
        Me.lblResultGroup.Text = "Result Group"
        Me.lblResultGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBatchName
        '
        Me.lblBatchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchName.Location = New System.Drawing.Point(31, 191)
        Me.lblBatchName.Name = "lblBatchName"
        Me.lblBatchName.Size = New System.Drawing.Size(89, 16)
        Me.lblBatchName.TabIndex = 6
        Me.lblBatchName.Text = "Batch Name"
        Me.lblBatchName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBatchCode
        '
        Me.lblBatchCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBatchCode.Location = New System.Drawing.Point(31, 164)
        Me.lblBatchCode.Name = "lblBatchCode"
        Me.lblBatchCode.Size = New System.Drawing.Size(89, 16)
        Me.lblBatchCode.TabIndex = 4
        Me.lblBatchCode.Text = "Batch Code"
        Me.lblBatchCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLocation
        '
        Me.txtLocation.Flags = 0
        Me.txtLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLocation.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLocation.Location = New System.Drawing.Point(126, 216)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(149, 21)
        Me.txtLocation.TabIndex = 9
        '
        'lblLocation
        '
        Me.lblLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocation.Location = New System.Drawing.Point(31, 218)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(89, 16)
        Me.lblLocation.TabIndex = 8
        Me.lblLocation.Text = "Location"
        Me.lblLocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtInterviewTime
        '
        Me.dtInterviewTime.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtInterviewTime.CustomFormat = "HH:mm"
        Me.dtInterviewTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtInterviewTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtInterviewTime.Location = New System.Drawing.Point(607, 162)
        Me.dtInterviewTime.Name = "dtInterviewTime"
        Me.dtInterviewTime.ShowUpDown = True
        Me.dtInterviewTime.Size = New System.Drawing.Size(55, 21)
        Me.dtInterviewTime.TabIndex = 12
        '
        'lblInterviewDateFrom
        '
        Me.lblInterviewDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterviewDateFrom.Location = New System.Drawing.Point(407, 164)
        Me.lblInterviewDateFrom.Name = "lblInterviewDateFrom"
        Me.lblInterviewDateFrom.Size = New System.Drawing.Size(83, 16)
        Me.lblInterviewDateFrom.TabIndex = 10
        Me.lblInterviewDateFrom.Text = "Interview Date"
        Me.lblInterviewDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtInterviewDate
        '
        Me.dtInterviewDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtInterviewDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtInterviewDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtInterviewDate.Location = New System.Drawing.Point(496, 162)
        Me.dtInterviewDate.Name = "dtInterviewDate"
        Me.dtInterviewDate.Size = New System.Drawing.Size(105, 21)
        Me.dtInterviewDate.TabIndex = 11
        '
        'cboInterviewType
        '
        Me.cboInterviewType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterviewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterviewType.FormattingEnabled = True
        Me.cboInterviewType.Location = New System.Drawing.Point(496, 189)
        Me.cboInterviewType.Name = "cboInterviewType"
        Me.cboInterviewType.Size = New System.Drawing.Size(166, 21)
        Me.cboInterviewType.TabIndex = 14
        '
        'lblInterview
        '
        Me.lblInterview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterview.Location = New System.Drawing.Point(407, 191)
        Me.lblInterview.Name = "lblInterview"
        Me.lblInterview.Size = New System.Drawing.Size(83, 16)
        Me.lblInterview.TabIndex = 13
        Me.lblInterview.Text = "Interview Type"
        Me.lblInterview.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(126, 87)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(387, 21)
        Me.cboVacancy.TabIndex = 2
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(31, 89)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(89, 16)
        Me.lblJob.TabIndex = 1
        Me.lblJob.Text = "Job/Vacancy"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabpInterview
        '
        Me.tabpInterview.Controls.Add(Me.btnAdd)
        Me.tabpInterview.Controls.Add(Me.btnEdit)
        Me.tabpInterview.Controls.Add(Me.btnDelete)
        Me.tabpInterview.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.tabpInterview.Controls.Add(Me.gbInterviewer)
        Me.tabpInterview.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabpInterview.Location = New System.Drawing.Point(4, 22)
        Me.tabpInterview.Name = "tabpInterview"
        Me.tabpInterview.Size = New System.Drawing.Size(841, 359)
        Me.tabpInterview.TabIndex = 1
        Me.tabpInterview.Text = "Interview"
        Me.tabpInterview.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(305, 321)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnAdd.TabIndex = 218
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(408, 321)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 219
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(728, 321)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 220
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lvInterviewer)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(302, 3)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 308
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(532, 310)
        Me.EZeeCollapsibleContainer1.TabIndex = 217
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Interviewers Detail"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvInterviewer
        '
        Me.lvInterviewer.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLevel, Me.colhEmpName, Me.colhCompany, Me.colhDepartment, Me.colhContactNo, Me.colhInterviewType, Me.objcolhInterviewerunkid, Me.objcolhIntGUID, Me.objcolhInterviewtypeunkid})
        Me.lvInterviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvInterviewer.FullRowSelect = True
        Me.lvInterviewer.GridLines = True
        Me.lvInterviewer.Location = New System.Drawing.Point(2, 26)
        Me.lvInterviewer.Name = "lvInterviewer"
        Me.lvInterviewer.Size = New System.Drawing.Size(524, 279)
        Me.lvInterviewer.TabIndex = 0
        Me.lvInterviewer.UseCompatibleStateImageBehavior = False
        Me.lvInterviewer.View = System.Windows.Forms.View.Details
        '
        'colhLevel
        '
        Me.colhLevel.Text = "Level"
        Me.colhLevel.Width = 45
        '
        'colhEmpName
        '
        Me.colhEmpName.Text = "Emp. Name"
        Me.colhEmpName.Width = 110
        '
        'colhCompany
        '
        Me.colhCompany.Text = "Company"
        Me.colhCompany.Width = 90
        '
        'colhDepartment
        '
        Me.colhDepartment.Text = "Department"
        Me.colhDepartment.Width = 90
        '
        'colhContactNo
        '
        Me.colhContactNo.Text = "Contact No."
        Me.colhContactNo.Width = 90
        '
        'colhInterviewType
        '
        Me.colhInterviewType.Text = "Interview Type"
        Me.colhInterviewType.Width = 90
        '
        'objcolhInterviewerunkid
        '
        Me.objcolhInterviewerunkid.Text = "Interviewerunkid"
        Me.objcolhInterviewerunkid.Width = 0
        '
        'objcolhIntGUID
        '
        Me.objcolhIntGUID.Text = "objcolhIntGUID"
        Me.objcolhIntGUID.Width = 0
        '
        'objcolhInterviewtypeunkid
        '
        Me.objcolhInterviewtypeunkid.Text = "objcolhInterviewtypeunkid"
        Me.objcolhInterviewtypeunkid.Width = 0
        '
        'gbInterviewer
        '
        Me.gbInterviewer.BorderColor = System.Drawing.Color.Black
        Me.gbInterviewer.Checked = False
        Me.gbInterviewer.CollapseAllExceptThis = False
        Me.gbInterviewer.CollapsedHoverImage = Nothing
        Me.gbInterviewer.CollapsedNormalImage = Nothing
        Me.gbInterviewer.CollapsedPressedImage = Nothing
        Me.gbInterviewer.CollapseOnLoad = False
        Me.gbInterviewer.Controls.Add(Me.cboiInterViewType)
        Me.gbInterviewer.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbInterviewer.Controls.Add(Me.lblLevelInfo)
        Me.gbInterviewer.Controls.Add(Me.nudLevel)
        Me.gbInterviewer.Controls.Add(Me.lblLevel)
        Me.gbInterviewer.Controls.Add(Me.EZeeLine3)
        Me.gbInterviewer.Controls.Add(Me.objbtnAddInterviewType)
        Me.gbInterviewer.Controls.Add(Me.lblInterViewType)
        Me.gbInterviewer.Controls.Add(Me.pnlOthers)
        Me.gbInterviewer.Controls.Add(Me.cboEmployee)
        Me.gbInterviewer.Controls.Add(Me.radEmployee)
        Me.gbInterviewer.Controls.Add(Me.objColon3)
        Me.gbInterviewer.Controls.Add(Me.lblEmployeeDepartment)
        Me.gbInterviewer.Controls.Add(Me.radOthers)
        Me.gbInterviewer.Controls.Add(Me.objColon2)
        Me.gbInterviewer.Controls.Add(Me.objColon1)
        Me.gbInterviewer.Controls.Add(Me.lblEmpContact)
        Me.gbInterviewer.Controls.Add(Me.lblEmpCompany)
        Me.gbInterviewer.Controls.Add(Me.lblDepartment)
        Me.gbInterviewer.Controls.Add(Me.lblEmployeeContactNo)
        Me.gbInterviewer.Controls.Add(Me.lblEmployeeCompany)
        Me.gbInterviewer.ExpandedHoverImage = Nothing
        Me.gbInterviewer.ExpandedNormalImage = Nothing
        Me.gbInterviewer.ExpandedPressedImage = Nothing
        Me.gbInterviewer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInterviewer.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInterviewer.HeaderHeight = 25
        Me.gbInterviewer.HeaderMessage = ""
        Me.gbInterviewer.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbInterviewer.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInterviewer.HeightOnCollapse = 0
        Me.gbInterviewer.LeftTextSpace = 0
        Me.gbInterviewer.Location = New System.Drawing.Point(3, 3)
        Me.gbInterviewer.Name = "gbInterviewer"
        Me.gbInterviewer.OpenHeight = 308
        Me.gbInterviewer.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInterviewer.ShowBorder = True
        Me.gbInterviewer.ShowCheckBox = False
        Me.gbInterviewer.ShowCollapseButton = False
        Me.gbInterviewer.ShowDefaultBorderColor = True
        Me.gbInterviewer.ShowDownButton = False
        Me.gbInterviewer.ShowHeader = True
        Me.gbInterviewer.Size = New System.Drawing.Size(295, 348)
        Me.gbInterviewer.TabIndex = 216
        Me.gbInterviewer.Temp = 0
        Me.gbInterviewer.Text = "Interview Info"
        Me.gbInterviewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboiInterViewType
        '
        Me.cboiInterViewType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboiInterViewType.DropDownWidth = 200
        Me.cboiInterViewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboiInterViewType.FormattingEnabled = True
        Me.cboiInterViewType.Location = New System.Drawing.Point(116, 30)
        Me.cboiInterViewType.Name = "cboiInterViewType"
        Me.cboiInterViewType.Size = New System.Drawing.Size(148, 21)
        Me.cboiInterViewType.TabIndex = 434
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(269, 103)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 432
        '
        'lblLevelInfo
        '
        Me.lblLevelInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevelInfo.Location = New System.Drawing.Point(162, 65)
        Me.lblLevelInfo.Name = "lblLevelInfo"
        Me.lblLevelInfo.Size = New System.Drawing.Size(120, 13)
        Me.lblLevelInfo.TabIndex = 214
        '
        'nudLevel
        '
        Me.nudLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudLevel.Location = New System.Drawing.Point(115, 61)
        Me.nudLevel.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.nudLevel.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudLevel.Name = "nudLevel"
        Me.nudLevel.Size = New System.Drawing.Size(40, 21)
        Me.nudLevel.TabIndex = 1
        Me.nudLevel.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblLevel
        '
        Me.lblLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLevel.Location = New System.Drawing.Point(11, 65)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(92, 13)
        Me.lblLevel.TabIndex = 212
        Me.lblLevel.Text = "Level"
        '
        'EZeeLine3
        '
        Me.EZeeLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine3.Location = New System.Drawing.Point(8, 92)
        Me.EZeeLine3.Name = "EZeeLine3"
        Me.EZeeLine3.Size = New System.Drawing.Size(274, 6)
        Me.EZeeLine3.TabIndex = 211
        '
        'objbtnAddInterviewType
        '
        Me.objbtnAddInterviewType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddInterviewType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddInterviewType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddInterviewType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddInterviewType.BorderSelected = False
        Me.objbtnAddInterviewType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddInterviewType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddInterviewType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddInterviewType.Location = New System.Drawing.Point(269, 33)
        Me.objbtnAddInterviewType.Name = "objbtnAddInterviewType"
        Me.objbtnAddInterviewType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddInterviewType.TabIndex = 208
        '
        'lblInterViewType
        '
        Me.lblInterViewType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterViewType.Location = New System.Drawing.Point(8, 36)
        Me.lblInterViewType.Name = "lblInterViewType"
        Me.lblInterViewType.Size = New System.Drawing.Size(95, 15)
        Me.lblInterViewType.TabIndex = 206
        Me.lblInterViewType.Text = "Interview Type"
        Me.lblInterViewType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlOthers
        '
        Me.pnlOthers.Controls.Add(Me.lblTrainerContactNo)
        Me.pnlOthers.Controls.Add(Me.txtOtherContactNo)
        Me.pnlOthers.Controls.Add(Me.lblCompany)
        Me.pnlOthers.Controls.Add(Me.lblOtherDepartment)
        Me.pnlOthers.Controls.Add(Me.txtOtherCompany)
        Me.pnlOthers.Controls.Add(Me.txtOtherDepartment)
        Me.pnlOthers.Controls.Add(Me.lblName)
        Me.pnlOthers.Controls.Add(Me.txtOtherInterviewerName)
        Me.pnlOthers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlOthers.Location = New System.Drawing.Point(11, 222)
        Me.pnlOthers.Name = "pnlOthers"
        Me.pnlOthers.Size = New System.Drawing.Size(277, 109)
        Me.pnlOthers.TabIndex = 199
        '
        'lblTrainerContactNo
        '
        Me.lblTrainerContactNo.Location = New System.Drawing.Point(15, 90)
        Me.lblTrainerContactNo.Name = "lblTrainerContactNo"
        Me.lblTrainerContactNo.Size = New System.Drawing.Size(85, 15)
        Me.lblTrainerContactNo.TabIndex = 160
        Me.lblTrainerContactNo.Text = "Contact No"
        Me.lblTrainerContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherContactNo
        '
        Me.txtOtherContactNo.Flags = 0
        Me.txtOtherContactNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherContactNo.Location = New System.Drawing.Point(105, 84)
        Me.txtOtherContactNo.Name = "txtOtherContactNo"
        Me.txtOtherContactNo.Size = New System.Drawing.Size(148, 21)
        Me.txtOtherContactNo.TabIndex = 3
        '
        'lblCompany
        '
        Me.lblCompany.Location = New System.Drawing.Point(15, 63)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(85, 15)
        Me.lblCompany.TabIndex = 158
        Me.lblCompany.Text = "Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOtherDepartment
        '
        Me.lblOtherDepartment.Location = New System.Drawing.Point(15, 36)
        Me.lblOtherDepartment.Name = "lblOtherDepartment"
        Me.lblOtherDepartment.Size = New System.Drawing.Size(85, 15)
        Me.lblOtherDepartment.TabIndex = 157
        Me.lblOtherDepartment.Text = "Department"
        Me.lblOtherDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherCompany
        '
        Me.txtOtherCompany.Flags = 0
        Me.txtOtherCompany.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherCompany.Location = New System.Drawing.Point(105, 57)
        Me.txtOtherCompany.Name = "txtOtherCompany"
        Me.txtOtherCompany.Size = New System.Drawing.Size(148, 21)
        Me.txtOtherCompany.TabIndex = 2
        '
        'txtOtherDepartment
        '
        Me.txtOtherDepartment.Flags = 0
        Me.txtOtherDepartment.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherDepartment.Location = New System.Drawing.Point(105, 30)
        Me.txtOtherDepartment.Name = "txtOtherDepartment"
        Me.txtOtherDepartment.Size = New System.Drawing.Size(148, 21)
        Me.txtOtherDepartment.TabIndex = 1
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(15, 6)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(85, 15)
        Me.lblName.TabIndex = 154
        Me.lblName.Text = "Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOtherInterviewerName
        '
        Me.txtOtherInterviewerName.Flags = 0
        Me.txtOtherInterviewerName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOtherInterviewerName.Location = New System.Drawing.Point(105, 3)
        Me.txtOtherInterviewerName.Name = "txtOtherInterviewerName"
        Me.txtOtherInterviewerName.Size = New System.Drawing.Size(148, 21)
        Me.txtOtherInterviewerName.TabIndex = 0
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 270
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(115, 103)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(148, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'radEmployee
        '
        Me.radEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEmployee.Location = New System.Drawing.Point(11, 105)
        Me.radEmployee.Name = "radEmployee"
        Me.radEmployee.Size = New System.Drawing.Size(95, 17)
        Me.radEmployee.TabIndex = 181
        Me.radEmployee.TabStop = True
        Me.radEmployee.Text = "Employee"
        Me.radEmployee.UseVisualStyleBackColor = True
        '
        'objColon3
        '
        Me.objColon3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon3.Location = New System.Drawing.Point(107, 174)
        Me.objColon3.Name = "objColon3"
        Me.objColon3.Size = New System.Drawing.Size(8, 15)
        Me.objColon3.TabIndex = 192
        Me.objColon3.Text = ":"
        Me.objColon3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployeeDepartment
        '
        Me.lblEmployeeDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeDepartment.Location = New System.Drawing.Point(121, 132)
        Me.lblEmployeeDepartment.Name = "lblEmployeeDepartment"
        Me.lblEmployeeDepartment.Size = New System.Drawing.Size(136, 15)
        Me.lblEmployeeDepartment.TabIndex = 184
        Me.lblEmployeeDepartment.Text = "#DepartmentName"
        Me.lblEmployeeDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radOthers
        '
        Me.radOthers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radOthers.Location = New System.Drawing.Point(11, 199)
        Me.radOthers.Name = "radOthers"
        Me.radOthers.Size = New System.Drawing.Size(76, 17)
        Me.radOthers.TabIndex = 198
        Me.radOthers.TabStop = True
        Me.radOthers.Text = "Other"
        Me.radOthers.UseVisualStyleBackColor = True
        '
        'objColon2
        '
        Me.objColon2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon2.Location = New System.Drawing.Point(107, 152)
        Me.objColon2.Name = "objColon2"
        Me.objColon2.Size = New System.Drawing.Size(8, 15)
        Me.objColon2.TabIndex = 191
        Me.objColon2.Text = ":"
        Me.objColon2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objColon1
        '
        Me.objColon1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objColon1.Location = New System.Drawing.Point(107, 132)
        Me.objColon1.Name = "objColon1"
        Me.objColon1.Size = New System.Drawing.Size(8, 15)
        Me.objColon1.TabIndex = 190
        Me.objColon1.Text = ":"
        Me.objColon1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpContact
        '
        Me.lblEmpContact.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpContact.Location = New System.Drawing.Point(34, 174)
        Me.lblEmpContact.Name = "lblEmpContact"
        Me.lblEmpContact.Size = New System.Drawing.Size(72, 15)
        Me.lblEmpContact.TabIndex = 189
        Me.lblEmpContact.Text = "Contact No"
        Me.lblEmpContact.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpCompany
        '
        Me.lblEmpCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpCompany.Location = New System.Drawing.Point(34, 152)
        Me.lblEmpCompany.Name = "lblEmpCompany"
        Me.lblEmpCompany.Size = New System.Drawing.Size(72, 15)
        Me.lblEmpCompany.TabIndex = 188
        Me.lblEmpCompany.Text = "Company"
        Me.lblEmpCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(34, 132)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(72, 15)
        Me.lblDepartment.TabIndex = 187
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployeeContactNo
        '
        Me.lblEmployeeContactNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeContactNo.Location = New System.Drawing.Point(121, 174)
        Me.lblEmployeeContactNo.Name = "lblEmployeeContactNo"
        Me.lblEmployeeContactNo.Size = New System.Drawing.Size(136, 15)
        Me.lblEmployeeContactNo.TabIndex = 186
        Me.lblEmployeeContactNo.Text = "#Contact No"
        Me.lblEmployeeContactNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployeeCompany
        '
        Me.lblEmployeeCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCompany.Location = New System.Drawing.Point(121, 152)
        Me.lblEmployeeCompany.Name = "lblEmployeeCompany"
        Me.lblEmployeeCompany.Size = New System.Drawing.Size(136, 15)
        Me.lblEmployeeCompany.TabIndex = 185
        Me.lblEmployeeCompany.Text = "#Company"
        Me.lblEmployeeCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.chkIsClosed)
        Me.EZeeFooter1.Controls.Add(Me.btnCancel)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnActivate)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 391)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(853, 50)
        Me.EZeeFooter1.TabIndex = 1
        '
        'chkIsClosed
        '
        Me.chkIsClosed.AutoSize = True
        Me.chkIsClosed.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsClosed.Location = New System.Drawing.Point(15, 20)
        Me.chkIsClosed.Name = "chkIsClosed"
        Me.chkIsClosed.Size = New System.Drawing.Size(98, 17)
        Me.chkIsClosed.TabIndex = 4
        Me.chkIsClosed.Text = "Mark as Closed"
        Me.chkIsClosed.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(538, 10)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(97, 30)
        Me.btnCancel.TabIndex = 0
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(744, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(641, 11)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnActivate
        '
        Me.btnActivate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnActivate.BackColor = System.Drawing.Color.White
        Me.btnActivate.BackgroundImage = CType(resources.GetObject("btnActivate.BackgroundImage"), System.Drawing.Image)
        Me.btnActivate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnActivate.BorderColor = System.Drawing.Color.Empty
        Me.btnActivate.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnActivate.FlatAppearance.BorderSize = 0
        Me.btnActivate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActivate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActivate.ForeColor = System.Drawing.Color.Black
        Me.btnActivate.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnActivate.GradientForeColor = System.Drawing.Color.Black
        Me.btnActivate.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnActivate.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnActivate.Location = New System.Drawing.Point(641, 11)
        Me.btnActivate.Name = "btnActivate"
        Me.btnActivate.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnActivate.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnActivate.Size = New System.Drawing.Size(97, 30)
        Me.btnActivate.TabIndex = 3
        Me.btnActivate.Text = "&Activate"
        Me.btnActivate.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(200, 100)
        Me.TabControl1.TabIndex = 0
        '
        'AlphanumericTextBox2
        '
        Me.AlphanumericTextBox2.BackColor = System.Drawing.SystemColors.Window
        Me.AlphanumericTextBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.AlphanumericTextBox2.Flags = 0
        Me.AlphanumericTextBox2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AlphanumericTextBox2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.AlphanumericTextBox2.Location = New System.Drawing.Point(0, 0)
        Me.AlphanumericTextBox2.Multiline = True
        Me.AlphanumericTextBox2.Name = "AlphanumericTextBox2"
        Me.AlphanumericTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.AlphanumericTextBox2.Size = New System.Drawing.Size(520, 89)
        Me.AlphanumericTextBox2.TabIndex = 215
        '
        'AlphanumericTextBox1
        '
        Me.AlphanumericTextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.AlphanumericTextBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.AlphanumericTextBox1.Flags = 0
        Me.AlphanumericTextBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AlphanumericTextBox1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.AlphanumericTextBox1.Location = New System.Drawing.Point(0, 0)
        Me.AlphanumericTextBox1.Multiline = True
        Me.AlphanumericTextBox1.Name = "AlphanumericTextBox1"
        Me.AlphanumericTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.AlphanumericTextBox1.Size = New System.Drawing.Size(520, 89)
        Me.AlphanumericTextBox1.TabIndex = 0
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(370, 189)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(166, 21)
        Me.ComboBox1.TabIndex = 14
        '
        'frmInterviewBatchScheduling
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(853, 441)
        Me.Controls.Add(Me.pnlScheduleInterview)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInterviewBatchScheduling"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Batch Schedule "
        Me.pnlScheduleInterview.ResumeLayout(False)
        Me.tabcBatchSchdule.ResumeLayout(False)
        Me.tabpBatchScheduleInfo.ResumeLayout(False)
        Me.gbScheduleInterview.ResumeLayout(False)
        Me.gbScheduleInterview.PerformLayout()
        Me.tabcRemarks.ResumeLayout(False)
        Me.tabpDescription.ResumeLayout(False)
        Me.tabpDescription.PerformLayout()
        Me.tabpCancelRemark.ResumeLayout(False)
        Me.tabpCancelRemark.PerformLayout()
        Me.tabpInterview.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.gbInterviewer.ResumeLayout(False)
        CType(Me.nudLevel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOthers.ResumeLayout(False)
        Me.pnlOthers.PerformLayout()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.EZeeFooter1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlScheduleInterview As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents chkIsClosed As System.Windows.Forms.CheckBox
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnActivate As eZee.Common.eZeeLightButton
    Friend WithEvents tabcBatchSchdule As System.Windows.Forms.TabControl
    Friend WithEvents tabpBatchScheduleInfo As System.Windows.Forms.TabPage
    Friend WithEvents tabpInterview As System.Windows.Forms.TabPage
    Friend WithEvents gbInterviewer As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblLevelInfo As System.Windows.Forms.Label
    Friend WithEvents nudLevel As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents EZeeLine3 As eZee.Common.eZeeLine
    Friend WithEvents objbtnAddInterviewType As eZee.Common.eZeeGradientButton
    Friend WithEvents lblInterViewType As System.Windows.Forms.Label
    Friend WithEvents pnlOthers As System.Windows.Forms.Panel
    Friend WithEvents lblTrainerContactNo As System.Windows.Forms.Label
    Friend WithEvents txtOtherContactNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents lblOtherDepartment As System.Windows.Forms.Label
    Friend WithEvents txtOtherCompany As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOtherDepartment As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtOtherInterviewerName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents radEmployee As System.Windows.Forms.RadioButton
    Friend WithEvents objColon3 As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeDepartment As System.Windows.Forms.Label
    Friend WithEvents radOthers As System.Windows.Forms.RadioButton
    Friend WithEvents objColon2 As System.Windows.Forms.Label
    Friend WithEvents objColon1 As System.Windows.Forms.Label
    Friend WithEvents lblEmpContact As System.Windows.Forms.Label
    Friend WithEvents lblEmpCompany As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeContactNo As System.Windows.Forms.Label
    Friend WithEvents lblEmployeeCompany As System.Windows.Forms.Label
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lvInterviewer As System.Windows.Forms.ListView
    Friend WithEvents colhLevel As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmpName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCompany As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDepartment As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhContactNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInterviewType As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhInterviewerunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhIntGUID As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhInterviewtypeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents AlphanumericTextBox1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents AlphanumericTextBox2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents gbScheduleInterview As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancyType As System.Windows.Forms.Label
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents elBatchInformation As eZee.Common.eZeeLine
    Friend WithEvents elVacancyInfo As eZee.Common.eZeeLine
    Friend WithEvents objbtnAddRGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents tabcRemarks As System.Windows.Forms.TabControl
    Friend WithEvents tabpDescription As System.Windows.Forms.TabPage
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents tabpCancelRemark As System.Windows.Forms.TabPage
    Friend WithEvents txtCancelRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtBatchCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtBatchName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboResultGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblResultGroup As System.Windows.Forms.Label
    Friend WithEvents lblBatchName As System.Windows.Forms.Label
    Friend WithEvents lblBatchCode As System.Windows.Forms.Label
    Friend WithEvents txtLocation As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblLocation As System.Windows.Forms.Label
    Friend WithEvents dtInterviewTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblInterviewDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtInterviewDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboInterviewType As System.Windows.Forms.ComboBox
    Friend WithEvents lblInterview As System.Windows.Forms.Label
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboiInterViewType As System.Windows.Forms.ComboBox
End Class
