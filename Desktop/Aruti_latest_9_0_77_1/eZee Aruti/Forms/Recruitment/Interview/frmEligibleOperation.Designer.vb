﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEligibleOperation
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEligibleOperation))
        Me.gbEvaluationInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.eslLine3 = New eZee.Common.eZeeStraightLine
        Me.eslLine2 = New eZee.Common.eZeeStraightLine
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.radNotEligible = New System.Windows.Forms.RadioButton
        Me.radEligible = New System.Windows.Forms.RadioButton
        Me.eslLine1 = New eZee.Common.eZeeStraightLine
        Me.dtpFinalDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemark = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbEvaluationInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbEvaluationInfo
        '
        Me.gbEvaluationInfo.BorderColor = System.Drawing.Color.Black
        Me.gbEvaluationInfo.Checked = False
        Me.gbEvaluationInfo.CollapseAllExceptThis = False
        Me.gbEvaluationInfo.CollapsedHoverImage = Nothing
        Me.gbEvaluationInfo.CollapsedNormalImage = Nothing
        Me.gbEvaluationInfo.CollapsedPressedImage = Nothing
        Me.gbEvaluationInfo.CollapseOnLoad = False
        Me.gbEvaluationInfo.Controls.Add(Me.eslLine3)
        Me.gbEvaluationInfo.Controls.Add(Me.eslLine2)
        Me.gbEvaluationInfo.Controls.Add(Me.objlblCaption)
        Me.gbEvaluationInfo.Controls.Add(Me.radNotEligible)
        Me.gbEvaluationInfo.Controls.Add(Me.radEligible)
        Me.gbEvaluationInfo.Controls.Add(Me.eslLine1)
        Me.gbEvaluationInfo.Controls.Add(Me.dtpFinalDate)
        Me.gbEvaluationInfo.Controls.Add(Me.lblDate)
        Me.gbEvaluationInfo.Controls.Add(Me.txtRemark)
        Me.gbEvaluationInfo.Controls.Add(Me.lblRemark)
        Me.gbEvaluationInfo.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbEvaluationInfo.ExpandedHoverImage = Nothing
        Me.gbEvaluationInfo.ExpandedNormalImage = Nothing
        Me.gbEvaluationInfo.ExpandedPressedImage = Nothing
        Me.gbEvaluationInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEvaluationInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEvaluationInfo.HeaderHeight = 25
        Me.gbEvaluationInfo.HeaderMessage = ""
        Me.gbEvaluationInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEvaluationInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEvaluationInfo.HeightOnCollapse = 0
        Me.gbEvaluationInfo.LeftTextSpace = 0
        Me.gbEvaluationInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbEvaluationInfo.Name = "gbEvaluationInfo"
        Me.gbEvaluationInfo.OpenHeight = 68
        Me.gbEvaluationInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEvaluationInfo.ShowBorder = True
        Me.gbEvaluationInfo.ShowCheckBox = False
        Me.gbEvaluationInfo.ShowCollapseButton = False
        Me.gbEvaluationInfo.ShowDefaultBorderColor = True
        Me.gbEvaluationInfo.ShowDownButton = False
        Me.gbEvaluationInfo.ShowHeader = True
        Me.gbEvaluationInfo.Size = New System.Drawing.Size(414, 252)
        Me.gbEvaluationInfo.TabIndex = 4
        Me.gbEvaluationInfo.Temp = 0
        Me.gbEvaluationInfo.Text = "Eligible/Not Eligible"
        Me.gbEvaluationInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eslLine3
        '
        Me.eslLine3.BackColor = System.Drawing.Color.Transparent
        Me.eslLine3.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.eslLine3.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.eslLine3.Location = New System.Drawing.Point(242, 80)
        Me.eslLine3.Name = "eslLine3"
        Me.eslLine3.Size = New System.Drawing.Size(8, 39)
        Me.eslLine3.TabIndex = 197
        '
        'eslLine2
        '
        Me.eslLine2.BackColor = System.Drawing.Color.Transparent
        Me.eslLine2.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.eslLine2.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.eslLine2.Location = New System.Drawing.Point(1, 125)
        Me.eslLine2.Name = "eslLine2"
        Me.eslLine2.Size = New System.Drawing.Size(413, 17)
        Me.eslLine2.TabIndex = 196
        '
        'objlblCaption
        '
        Me.objlblCaption.Location = New System.Drawing.Point(12, 39)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(390, 15)
        Me.objlblCaption.TabIndex = 195
        Me.objlblCaption.Text = "#Value"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radNotEligible
        '
        Me.radNotEligible.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radNotEligible.Location = New System.Drawing.Point(15, 103)
        Me.radNotEligible.Name = "radNotEligible"
        Me.radNotEligible.Size = New System.Drawing.Size(226, 17)
        Me.radNotEligible.TabIndex = 190
        Me.radNotEligible.TabStop = True
        Me.radNotEligible.Text = "Set As Not Eligible"
        Me.radNotEligible.UseVisualStyleBackColor = True
        '
        'radEligible
        '
        Me.radEligible.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radEligible.Location = New System.Drawing.Point(15, 80)
        Me.radEligible.Name = "radEligible"
        Me.radEligible.Size = New System.Drawing.Size(226, 17)
        Me.radEligible.TabIndex = 190
        Me.radEligible.TabStop = True
        Me.radEligible.Text = "Set As Eligible"
        Me.radEligible.UseVisualStyleBackColor = True
        '
        'eslLine1
        '
        Me.eslLine1.BackColor = System.Drawing.Color.Transparent
        Me.eslLine1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.eslLine1.LineType = eZee.Common.StraightLineTypes.Horizontal
        Me.eslLine1.Location = New System.Drawing.Point(1, 57)
        Me.eslLine1.Name = "eslLine1"
        Me.eslLine1.Size = New System.Drawing.Size(413, 17)
        Me.eslLine1.TabIndex = 193
        '
        'dtpFinalDate
        '
        Me.dtpFinalDate.Checked = False
        Me.dtpFinalDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFinalDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFinalDate.Location = New System.Drawing.Point(306, 80)
        Me.dtpFinalDate.Name = "dtpFinalDate"
        Me.dtpFinalDate.Size = New System.Drawing.Size(99, 21)
        Me.dtpFinalDate.TabIndex = 9
        '
        'lblDate
        '
        Me.lblDate.BackColor = System.Drawing.Color.Transparent
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(254, 83)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(46, 15)
        Me.lblDate.TabIndex = 8
        Me.lblDate.Text = "Date"
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtRemark.Location = New System.Drawing.Point(15, 166)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(387, 76)
        Me.txtRemark.TabIndex = 13
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(12, 148)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(75, 15)
        Me.lblRemark.TabIndex = 12
        Me.lblRemark.Text = "Remark"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 256)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(414, 55)
        Me.objFooter.TabIndex = 10
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(202, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 128
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(305, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmEligibleOperation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(414, 311)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbEvaluationInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEligibleOperation"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Set Eligible/Not Eligible"
        Me.gbEvaluationInfo.ResumeLayout(False)
        Me.gbEvaluationInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbEvaluationInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents eslLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents radNotEligible As System.Windows.Forms.RadioButton
    Friend WithEvents radEligible As System.Windows.Forms.RadioButton
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents dtpFinalDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents eslLine3 As eZee.Common.eZeeStraightLine
    Friend WithEvents eslLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
End Class
