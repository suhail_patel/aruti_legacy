﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmInterviewBatchScheduleList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmInterviewBatchScheduleList"
    Private objBatchScheduleData As clsBatchSchedule
    Private objCommonMaster As clsCommon_Master

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As New DataSet


        Try
            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True, "InterviewType")
            With cboInterviewType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("InterviewType")
                .SelectedValue = 0
            End With

            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            objBatchScheduleData = New clsBatchSchedule

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objBatchScheduleData.getListForCombo("List", True)
            dsList = objBatchScheduleData.getListForCombo(FinancialYear._Object._Database_Start_Date, "List", True)
            'Shani(24-Aug-2015) -- End

            With cboBatchName
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'Anjan (02 Mar 2012)-End 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombos", mstrModuleName)
        End Try

    End Sub


    Private Sub FillList()
        Dim dsList As New DataSet
        Dim strSearching As String = ""
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem

        Try

            If User._Object.Privilege._AllowToViewBatchSchedulingList = True Then                'Pinkal (02-Jul-2012) -- Start

                dsList = objBatchScheduleData.GetList("List")

                lvBatchList.GridLines = False

                If CInt(cboInterviewType.SelectedValue) > 0 Then
                    strSearching &= " AND interviewtypeunkid = " & CInt(cboInterviewType.SelectedValue) & " "
                End If


                'Anjan (02 Mar 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                'If txtBatchName.Text.Trim <> "" Then
                'strSearching &= " AND batchname  LIKE '%" & txtBatchName.Text & "%'" & " "
                'End If
                If CInt(cboBatchName.SelectedValue) > 0 Then
                    strSearching &= " AND batchscheduleunkid = " & CInt(cboBatchName.SelectedValue) & "  "
                End If
                'Anjan (02 Mar 2012)-End 



                If txtBatchNo.Text.Trim <> "" Then
                    strSearching &= " AND batchcode LIKE  '%" & txtBatchNo.Text & "%'" & " "
                End If

                If txtLocation.Text.Trim <> "" Then
                    strSearching &= " AND location LIKE '%" & txtLocation.Text & "%'" & " "
                End If

                If txtDescription.Text.Trim <> "" Then
                    strSearching &= " AND description LIKE '%" & txtDescription.Text & "%'" & " "
                End If

                If txtCancelRemark.Text.Trim <> "" Then
                    strSearching &= " AND cancel_remark '%" & txtCancelRemark.Text & "%'" & " "
                End If

                If dtpInterviewDate.Checked = True Then
                    strSearching &= " AND interviewdate = " & eZeeDate.convertDate(dtpInterviewDate.Value) & " "
                End If

                If dtpCancelDate.Checked = True Then
                    strSearching &= " AND cancel_date = " & eZeeDate.convertDate(dtpCancelDate.Value) & " "
                End If

                If radCancelled.Checked = True Then
                    strSearching &= " AND iscancel = 1 " & " "
                End If

                If radEnrolled.Checked = True Then
                    strSearching &= " AND iscancel = 0 AND isclosed=0 AND isvoid=0 " & " "
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(4)
                    dtTable = New DataView(dsList.Tables("List"), strSearching, "job", DataViewRowState.CurrentRows).ToTable

                Else
                    dtTable = New DataView(dsList.Tables("List"), "", "job", DataViewRowState.CurrentRows).ToTable
                End If

                lvBatchList.Items.Clear()


                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("batchcode").ToString
                    lvItem.SubItems.Add(dtRow.Item("batchname").ToString)
                    If dtRow.Item("Odate").ToString.Trim.Length > 0 AndAlso dtRow.Item("Cdate").ToString.Trim.Length > 0 Then
                        lvItem.SubItems.Add(dtRow.Item("job").ToString & " ( " & eZeeDate.convertDate(dtRow.Item("Odate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dtRow.Item("Cdate").ToString).ToShortDateString & " ) ")
                    Else
                        lvItem.SubItems.Add(dtRow.Item("job").ToString)
                    End If
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("interviewdate").ToString).ToShortDateString)
                    lvItem.SubItems.Add(dtRow.Item("interviewtime").ToString)
                    lvItem.SubItems.Add(dtRow.Item("resultcode").ToString)
                    lvItem.SubItems.Add(dtRow.Item("description").ToString)
                    lvItem.SubItems.Add(dtRow.Item("iscancel").ToString)
                    lvItem.SubItems.Add(dtRow.Item("isclosed").ToString)
                    lvItem.SubItems.Add(dtRow.Item("isvoid").ToString)
                    lvItem.Tag = dtRow.Item("batchscheduleunkid")
                    If CBool(dtRow.Item("iscancel")) = True Then
                        lvItem.ForeColor = Color.Purple
                    ElseIf CBool(dtRow.Item("isclosed")) Then
                        lvItem.ForeColor = Color.Blue
                    ElseIf CBool(dtRow.Item("isvoid")) Then
                        lvItem.ForeColor = Color.Red
                    End If
                    lvBatchList.Items.Add(lvItem)

                    lvBatchList.GroupingColumn = colhVacancy
                    lvBatchList.DisplayGroups(True)

                    If lvBatchList.Items.Count > 11 Then
                        colhDescription.Width = 195 - 18
                    Else
                        colhDescription.Width = 195
                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try


    End Sub
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddBatch
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditBatch
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteBatch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Buttons "
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmInterviewBatchScheduling
        Try
            frm.btnActivate.Visible = False
            frm.btnCancel.Visible = False
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvBatchList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information)
            lvBatchList.Select()
            Exit Sub
        End If

        If CBool(lvBatchList.SelectedItems(0).SubItems(objcolhIsCancel.Index).Text) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected Batch cannot be edited.Reason:Selected Batch is Cancelled."), enMsgBoxStyle.Information)
            lvBatchList.Select()
            Exit Sub
        End If
        If CBool(lvBatchList.SelectedItems(0).SubItems(objcolhIsClose.Index).Text) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Selected Batch cannot be edited.Reason:Selected Batch is Closed."), enMsgBoxStyle.Information)
            lvBatchList.Select()
            Exit Sub
        End If
        If CBool(lvBatchList.SelectedItems(0).SubItems(objcolhIsVoid.Index).Text) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Selected Batch cannot be edited.Reason:Selected Batch is deleted."), enMsgBoxStyle.Information)
            lvBatchList.Select()
            Exit Sub
        End If

        Dim frm As New frmInterviewBatchScheduling
        Try

            frm.btnCancel.Visible = True
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBatchList.SelectedItems(0).Index

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If frm.displayDialog(CInt(lvBatchList.SelectedItems(0).Tag), enAction.EDIT_ONE) = True Then
                'Hemant (07 Oct 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
                'Call FillList()
                'Hemant (07 Oct 2019) -- End
            End If

            frm = Nothing
            If lvBatchList.SelectedItems.Count > 0 Then
                lvBatchList.Items(intSelectedIndex).Selected = True
                lvBatchList.EnsureVisible(intSelectedIndex)
                lvBatchList.Select()
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvBatchList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information)
            lvBatchList.Select()
            Exit Sub
        End If
        If CBool(lvBatchList.SelectedItems(0).SubItems(objcolhIsCancel.Index).Text) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Selected Batch cannot be Deleted.Reason:Selected transaction is Cancelled."), enMsgBoxStyle.Information)
            lvBatchList.Select()
            Exit Sub
        End If
        If CBool(lvBatchList.SelectedItems(0).SubItems(objcolhIsClose.Index).Text) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Selected Batch cannot be Deleted.Reason:Selected transaction is Closed."), enMsgBoxStyle.Information)
            lvBatchList.Select()
            Exit Sub
        End If

        If CBool(lvBatchList.SelectedItems(0).SubItems(objcolhIsVoid.Index).Text) = True Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selected Batch cannot be Deleted.Reason:Selected transaction is deleted."), enMsgBoxStyle.Information)
            lvBatchList.Select()
            Exit Sub
        End If

        Dim frm As New frmInterviewBatchScheduling
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvBatchList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure you want to delete this transaction ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objBatchScheduleData._Isvoid = True

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objBatchScheduleData._Voiduserunkid = 1
                'objBatchScheduleData._Voidreason = "Void Reason"
                'objBatchScheduleData._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))

                Dim vfrm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                vfrm.displayDialog(enVoidCategoryType.INTERVIEW, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objBatchScheduleData._Voidreason = mstrVoidReason
                End If
                vfrm = Nothing
                objBatchScheduleData._Voiduserunkid = User._Object._Userunkid
                objBatchScheduleData._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End 

'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objBatchScheduleData._FormName = mstrModuleName
                objBatchScheduleData._LoginEmployeeunkid = 0
                objBatchScheduleData._ClientIP = getIP()
                objBatchScheduleData._HostName = getHostName()
                objBatchScheduleData._FromWeb = False
                objBatchScheduleData._AuditUserId = User._Object._Userunkid
objBatchScheduleData._CompanyUnkid = Company._Object._Companyunkid
                objBatchScheduleData._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objBatchScheduleData.Delete(CInt(lvBatchList.SelectedItems(0).Tag))
                'Sandeep [ 09 Oct 2010 ] -- Start
                'lvBatchList.SelectedItems(0).Remove()
                If objBatchScheduleData._Message <> "" Then
                    eZeeMsgBox.Show(objBatchScheduleData._Message, enMsgBoxStyle.Information)
                Else
                    lvBatchList.SelectedItems(0).Remove()
                End If
                'Sandeep [ 09 Oct 2010 ] -- End 

                If lvBatchList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvBatchList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvBatchList.Items.Count - 1
                    lvBatchList.Items(intSelectedIndex).Selected = True
                    lvBatchList.EnsureVisible(intSelectedIndex)
                ElseIf lvBatchList.Items.Count <> 0 Then
                    lvBatchList.Items(intSelectedIndex).Selected = True
                    lvBatchList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvBatchList.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try

    End Sub
    'Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If lvBatchList.SelectedItems.Count < 1 Then
    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation on it."), enMsgBoxStyle.Information)
    '        lvBatchList.Select()
    '        Exit Sub
    '    End If
    '    Dim frm As New frmInterviewBatchScheduling
    '    Try

    '        If frm.displayDialog(-1, enAction.ADD_ONE) Then
    '            Call FillList()
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
    '    End Try

    'End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
    Private Sub btnClose_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles btnClose.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub btnClose_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles btnClose.KeyUp
        If e.KeyCode = Keys.Delete And lvBatchList.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub


#End Region

#Region " Form Events "


    Private Sub frmInterviewBatchScheduleList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objBatchScheduleData = New clsBatchSchedule
        objCommonMaster = New clsCommon_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            radEnrolled.Checked = True
            Call SetVisibility()

            Call FillCombo()
            Call FillList()

            If lvBatchList.Items.Count > 0 Then lvBatchList.Items(0).Selected = True
            lvBatchList.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmInterviewBatchScheduleList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmInterviewBatchScheduleList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objBatchScheduleData = Nothing
        objCommonMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsBatchSchedule.SetMessages()
            objfrm._Other_ModuleNames = "clsBatchSchedule"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region



    'Private Sub radCancelled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radCancelled.CheckedChanged
    '    Try
    '        btnActivate.Enabled = True
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "radCancelled_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub radEnrolled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radEnrolled.CheckedChanged
    '    Try
    '        btnActivate.Enabled = False
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "radEnrolled_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub radShowAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radShowAll.CheckedChanged
    '    Try
    '        btnActivate.Enabled = True
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "radShowAll_CheckedChanged", mstrModuleName)
    '    End Try
    'End Sub


    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click

        Try

            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'txtBatchName.Text = ""
            cboBatchName.SelectedValue = 0
            'Anjan (02 Mar 2012)-End 


            txtBatchNo.Text = ""
            txtCancelRemark.Text = ""
            txtDescription.Text = ""
            txtLocation.Text = ""
            dtpCancelDate.Value = Today.Date
            dtpInterviewDate.Value = Today.Date
            dtpCancelDate.Checked = False
            dtpInterviewDate.Checked = False
            cboInterviewType.SelectedValue = 0
            radEnrolled.Checked = True
            radCancelled.Checked = False
            radShowAll.Checked = False
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click

        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub radCancelled_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radCancelled.CheckedChanged

        Try
            If radCancelled.Checked = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radCancelled_CheckedChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub radEnrolled_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEnrolled.CheckedChanged

        Try
            If radEnrolled.Checked = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radEnrolled_CheckedChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub radShowAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radShowAll.CheckedChanged

        Try
            If radShowAll.Checked = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblBatchName.Text = Language._Object.getCaption(Me.lblBatchName.Name, Me.lblBatchName.Text)
            Me.lblBatchNo.Text = Language._Object.getCaption(Me.lblBatchNo.Name, Me.lblBatchNo.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.colhBatchNo.Text = Language._Object.getCaption(CStr(Me.colhBatchNo.Tag), Me.colhBatchNo.Text)
            Me.colhBatchName.Text = Language._Object.getCaption(CStr(Me.colhBatchName.Tag), Me.colhBatchName.Text)
            Me.colhVacancy.Text = Language._Object.getCaption(CStr(Me.colhVacancy.Tag), Me.colhVacancy.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblInterviewDate.Text = Language._Object.getCaption(Me.lblInterviewDate.Name, Me.lblInterviewDate.Text)
            Me.lblInterviewType.Text = Language._Object.getCaption(Me.lblInterviewType.Name, Me.lblInterviewType.Text)
            Me.lblLocation.Text = Language._Object.getCaption(Me.lblLocation.Name, Me.lblLocation.Text)
            Me.radShowAll.Text = Language._Object.getCaption(Me.radShowAll.Name, Me.radShowAll.Text)
            Me.radCancelled.Text = Language._Object.getCaption(Me.radCancelled.Name, Me.radCancelled.Text)
            Me.radEnrolled.Text = Language._Object.getCaption(Me.radEnrolled.Name, Me.radEnrolled.Text)
            Me.lblCancelDate.Text = Language._Object.getCaption(Me.lblCancelDate.Name, Me.lblCancelDate.Text)
            Me.lblCancelRemark.Text = Language._Object.getCaption(Me.lblCancelRemark.Name, Me.lblCancelRemark.Text)
            Me.colhInterviewDate.Text = Language._Object.getCaption(CStr(Me.colhInterviewDate.Tag), Me.colhInterviewDate.Text)
            Me.colhInterviewTime.Text = Language._Object.getCaption(CStr(Me.colhInterviewTime.Tag), Me.colhInterviewTime.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.colhResultCode.Text = Language._Object.getCaption(CStr(Me.colhResultCode.Tag), Me.colhResultCode.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Selected Batch cannot be Deleted.Reason:Selected transaction is Cancelled.")
            Language.setMessage(mstrModuleName, 3, "Selected Batch cannot be Deleted.Reason:Selected transaction is Closed.")
            Language.setMessage(mstrModuleName, 4, "Selected Batch cannot be Deleted.Reason:Selected transaction is deleted.")
            Language.setMessage(mstrModuleName, 5, "Are you sure you want to delete this transaction ?")
            Language.setMessage(mstrModuleName, 6, "Selected Batch cannot be edited.Reason:Selected Batch is Cancelled.")
            Language.setMessage(mstrModuleName, 7, "Selected Batch cannot be edited.Reason:Selected Batch is Closed.")
            Language.setMessage(mstrModuleName, 8, "Selected Batch cannot be edited.Reason:Selected Batch is deleted.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class