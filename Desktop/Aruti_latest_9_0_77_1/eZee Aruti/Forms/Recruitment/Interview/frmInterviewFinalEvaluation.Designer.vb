﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInterviewFinalEvaluation
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInterviewFinalEvaluation))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.eslLine3 = New eZee.Common.eZeeStraightLine
        Me.dtpFinalDate = New System.Windows.Forms.DateTimePicker
        Me.lblDate = New System.Windows.Forms.Label
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.eLine1 = New eZee.Common.eZeeLine
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDisapprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhInterviewType_AD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAnalysisDate_AD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReviewer_AD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhScore_AD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRemark_AD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAppBatchTranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAnalysisId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhVacancyId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApplicantId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.eslLine3)
        Me.pnlMainInfo.Controls.Add(Me.dtpFinalDate)
        Me.pnlMainInfo.Controls.Add(Me.lblDate)
        Me.pnlMainInfo.Controls.Add(Me.txtRemark)
        Me.pnlMainInfo.Controls.Add(Me.eLine1)
        Me.pnlMainInfo.Controls.Add(Me.dgvData)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(532, 360)
        Me.pnlMainInfo.TabIndex = 0
        '
        'eslLine3
        '
        Me.eslLine3.BackColor = System.Drawing.Color.Transparent
        Me.eslLine3.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.eslLine3.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.eslLine3.Location = New System.Drawing.Point(382, 197)
        Me.eslLine3.Name = "eslLine3"
        Me.eslLine3.Size = New System.Drawing.Size(5, 21)
        Me.eslLine3.TabIndex = 198
        '
        'dtpFinalDate
        '
        Me.dtpFinalDate.Checked = False
        Me.dtpFinalDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFinalDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFinalDate.Location = New System.Drawing.Point(442, 197)
        Me.dtpFinalDate.Name = "dtpFinalDate"
        Me.dtpFinalDate.Size = New System.Drawing.Size(87, 21)
        Me.dtpFinalDate.TabIndex = 46
        '
        'lblDate
        '
        Me.lblDate.BackColor = System.Drawing.Color.Transparent
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(392, 200)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(46, 15)
        Me.lblDate.TabIndex = 45
        Me.lblDate.Text = "Date"
        '
        'txtRemark
        '
        Me.txtRemark.Location = New System.Drawing.Point(3, 224)
        Me.txtRemark.MaxLength = 2147483647
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(526, 76)
        Me.txtRemark.TabIndex = 44
        '
        'eLine1
        '
        Me.eLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.eLine1.Location = New System.Drawing.Point(4, 199)
        Me.eLine1.Name = "eLine1"
        Me.eLine1.Size = New System.Drawing.Size(366, 17)
        Me.eLine1.TabIndex = 43
        Me.eLine1.Text = "Remark"
        Me.eLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollapse, Me.dgcolhInterviewType_AD, Me.dgcolhAnalysisDate_AD, Me.dgcolhReviewer_AD, Me.dgcolhScore_AD, Me.dgcolhRemark_AD, Me.objdgcolhAppBatchTranId, Me.objdgcolhAnalysisId, Me.objdgcolhVacancyId, Me.objdgcolhApplicantId, Me.objdgcolhIsGrp, Me.objdgcolhGrpId})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.ReadOnly = True
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(532, 192)
        Me.dgvData.TabIndex = 42
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnApprove)
        Me.objFooter.Controls.Add(Me.btnDisapprove)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 305)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(532, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(215, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(98, 30)
        Me.btnApprove.TabIndex = 128
        Me.btnApprove.Text = "&Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        '
        'btnDisapprove
        '
        Me.btnDisapprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDisapprove.BackColor = System.Drawing.Color.White
        Me.btnDisapprove.BackgroundImage = CType(resources.GetObject("btnDisapprove.BackgroundImage"), System.Drawing.Image)
        Me.btnDisapprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDisapprove.BorderColor = System.Drawing.Color.Empty
        Me.btnDisapprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDisapprove.FlatAppearance.BorderSize = 0
        Me.btnDisapprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDisapprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisapprove.ForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDisapprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDisapprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.Location = New System.Drawing.Point(319, 13)
        Me.btnDisapprove.Name = "btnDisapprove"
        Me.btnDisapprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDisapprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.Size = New System.Drawing.Size(98, 30)
        Me.btnDisapprove.TabIndex = 128
        Me.btnDisapprove.Text = "&Disapprove"
        Me.btnDisapprove.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(423, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Interview Type"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "AnalysisDate"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Reviewer"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Score"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Remark"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 120
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhAppBatchTranId"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhAnalysisId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhVacancyId"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhApplicantId"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhGrpId"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'objdgcolhCollapse
        '
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.ReadOnly = True
        Me.objdgcolhCollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCollapse.Width = 25
        '
        'dgcolhInterviewType_AD
        '
        Me.dgcolhInterviewType_AD.HeaderText = "Interview Type"
        Me.dgcolhInterviewType_AD.Name = "dgcolhInterviewType_AD"
        Me.dgcolhInterviewType_AD.ReadOnly = True
        Me.dgcolhInterviewType_AD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhInterviewType_AD.Width = 350
        '
        'dgcolhAnalysisDate_AD
        '
        Me.dgcolhAnalysisDate_AD.HeaderText = "Analysis Date"
        Me.dgcolhAnalysisDate_AD.Name = "dgcolhAnalysisDate_AD"
        Me.dgcolhAnalysisDate_AD.ReadOnly = True
        Me.dgcolhAnalysisDate_AD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAnalysisDate_AD.Visible = False
        '
        'dgcolhReviewer_AD
        '
        Me.dgcolhReviewer_AD.HeaderText = "Reviewer"
        Me.dgcolhReviewer_AD.Name = "dgcolhReviewer_AD"
        Me.dgcolhReviewer_AD.ReadOnly = True
        Me.dgcolhReviewer_AD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhReviewer_AD.Width = 150
        '
        'dgcolhScore_AD
        '
        Me.dgcolhScore_AD.HeaderText = "Score"
        Me.dgcolhScore_AD.Name = "dgcolhScore_AD"
        Me.dgcolhScore_AD.ReadOnly = True
        Me.dgcolhScore_AD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhScore_AD.Width = 80
        '
        'dgcolhRemark_AD
        '
        Me.dgcolhRemark_AD.HeaderText = "Remark"
        Me.dgcolhRemark_AD.Name = "dgcolhRemark_AD"
        Me.dgcolhRemark_AD.ReadOnly = True
        Me.dgcolhRemark_AD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRemark_AD.Width = 120
        '
        'objdgcolhAppBatchTranId
        '
        Me.objdgcolhAppBatchTranId.HeaderText = "objdgcolhAppBatchTranId"
        Me.objdgcolhAppBatchTranId.Name = "objdgcolhAppBatchTranId"
        Me.objdgcolhAppBatchTranId.ReadOnly = True
        Me.objdgcolhAppBatchTranId.Visible = False
        '
        'objdgcolhAnalysisId
        '
        Me.objdgcolhAnalysisId.HeaderText = "objdgcolhAnalysisId"
        Me.objdgcolhAnalysisId.Name = "objdgcolhAnalysisId"
        Me.objdgcolhAnalysisId.ReadOnly = True
        Me.objdgcolhAnalysisId.Visible = False
        '
        'objdgcolhVacancyId
        '
        Me.objdgcolhVacancyId.HeaderText = "objdgcolhVacancyId"
        Me.objdgcolhVacancyId.Name = "objdgcolhVacancyId"
        Me.objdgcolhVacancyId.ReadOnly = True
        Me.objdgcolhVacancyId.Visible = False
        '
        'objdgcolhApplicantId
        '
        Me.objdgcolhApplicantId.HeaderText = "objdgcolhApplicantId"
        Me.objdgcolhApplicantId.Name = "objdgcolhApplicantId"
        Me.objdgcolhApplicantId.ReadOnly = True
        Me.objdgcolhApplicantId.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.ReadOnly = True
        Me.objdgcolhGrpId.Visible = False
        '
        'frmInterviewFinalEvaluation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(532, 360)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmInterviewFinalEvaluation"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Approve / Disapprove Applicant Eligibility"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDisapprove As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents eLine1 As eZee.Common.eZeeLine
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dtpFinalDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents eslLine3 As eZee.Common.eZeeStraightLine
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhInterviewType_AD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAnalysisDate_AD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReviewer_AD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhScore_AD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRemark_AD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAppBatchTranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAnalysisId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhVacancyId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApplicantId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
