﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmInterviewAnalysisList

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmInterviewAnalysisList"
    Private objInterviewAnalysisMaster As clsInterviewAnalysis_master
    Private objInterviewAnalysisTran As clsInteviewAnalysis_tran

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage
    Private dtTable As DataTable
    Private mblnIsSuccess As Boolean = False
    'S.SANDEEP [ 14 May 2013 ] -- END

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objApplicant As New clsApplicant_master
        Dim objInterviewType As New clsCommon_Master
        'S.SANDEEP [ 28 FEB 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim objVac As New clsVacancy
        'S.SANDEEP [ 28 FEB 2012 ] -- END
        Dim dsList As New DataSet
        Try
            dsList = objApplicant.GetApplicantList("List", True)
            With cboApplicant
                .ValueMember = "applicantunkid"
                .DisplayMember = "applicantname"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With


            dsList = objInterviewType.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True)
            With cboInterviewType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsList = objVac.getVacancyType
            With cboVacancyType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 28 FEB 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objApplicant = Nothing
            objInterviewType = Nothing
            dsList = Nothing
        End Try
    End Sub

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Private Sub FillList()
        Dim StrSearching As String = String.Empty
        Dim dtList As New DataTable
        Try
            If User._Object.Privilege._AllowtoViewInterviewAnalysisList = False Then Exit Sub

            dtList = objInterviewAnalysisMaster.Get_DataTable(CInt(cboVacancy.SelectedValue), "List")

            If CInt(cboApplicant.SelectedValue) > 0 Then
                StrSearching &= "AND applicantunkid = " & CInt(cboApplicant.SelectedValue) & " "
            End If

            If dtpAnalysisDateFrom.Checked = True And dtpAnalysisDateTo.Checked = True Then
                StrSearching &= "AND analysisdate >= '" & eZeeDate.convertDate(dtpAnalysisDateFrom.Value) & "' AND analysisdate <= '" & eZeeDate.convertDate(dtpAnalysisDateTo.Value) & "'" & " "
            End If

            If CInt(cboInterviewType.SelectedValue) > 0 Then
                StrSearching &= "AND interviewtypeunkid = " & CInt(cboInterviewType.SelectedValue) & " "
            End If

            If CInt(cboVacancy.SelectedValue) > 0 Then
                StrSearching &= "AND vacancyunkid = '" & CInt(cboVacancy.SelectedValue) & "' "
            End If

            If CInt(cboBatch.SelectedValue) > 0 Then
                StrSearching &= "AND batchscheduleunkid = '" & CInt(cboBatch.SelectedValue) & "' "
            End If

            If CInt(cboResult.SelectedValue) > 0 Then
                StrSearching &= "AND resultcodeunkid = '" & CInt(cboResult.SelectedValue) & "' "
            End If

            If chkEligible.Checked = True And chkNotEligible.Checked = True Then
                StrSearching &= ""
            ElseIf chkEligible.Checked = True Then
                StrSearching &= "AND iseligible = 1 "
            ElseIf chkNotEligible.Checked = True Then
                StrSearching &= "AND iseligible = 0 "
            ElseIf chkEligible.Checked = False And chkNotEligible.Checked = False Then
                StrSearching &= ""
            End If

            'S.SANDEEP [ 09 OCT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrSearching &= "OR is_grp = True"
            'S.SANDEEP [ 09 OCT 2013 ] -- END

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dtList, StrSearching, "grp_id,sort_id,score DESC", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dtList, "", "grp_id,sort_id,score DESC", DataViewRowState.CurrentRows).ToTable
            End If


            'S.SANDEEP [ 09 OCT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If dtTable.Rows.Count > 0 Then
                Dim itmp() As DataRow = dtTable.Select("is_grp = True")
                If itmp.Length <= 0 Then
                    Dim iGrp = From p In dtTable.AsEnumerable() Where p.Field(Of Integer)("grp_id") > 0 Select p
                    Dim sValue As String = String.Empty
                    Dim selectedTags As List(Of Integer) = iGrp.Select(Function(x) x.Field(Of Integer)("grp_id")).Distinct.ToList
                    Dim result() As String = selectedTags.Select(Function(x) x.ToString()).ToArray()
                    sValue = String.Join(", ", result)
                    Dim dtmp() As DataRow = dtList.Select("grp_id IN(" & sValue & ") AND is_grp = True")
                    If dtmp.Length > 0 Then
                        For iRow As Integer = 0 To dtmp.Length - 1
                            dtTable.ImportRow(dtmp(iRow))
                        Next
                        dtTable = New DataView(dtTable, "", "grp_id,sort_id,score DESC", DataViewRowState.CurrentRows).ToTable
                        dtTable.AcceptChanges()
                    End If
                End If
            End If

            'S.SANDEEP [ 09 OCT 2013 ] -- END



            dgvData.AutoGenerateColumns = False

            objdgcolhAnalysisId.DataPropertyName = "analysisunkid"
            objdgcolhAppBatchTranId.DataPropertyName = "appbatchscheduletranunkid"
            objdgcolhApplicantId.DataPropertyName = "applicantunkid"
            objdgcolhCheck.DataPropertyName = "ischeck"
            objdgcolhGrpId.DataPropertyName = "grp_id"
            objdgcolhSortId.DataPropertyName = "sort_id"
            objdgcolhVacancyId.DataPropertyName = "vacancyunkid"
            dgcolhAnalysisDate.DataPropertyName = "analysis_date"
            dgcolhInterviewType.DataPropertyName = "interview_type"
            dgcolhRemark.DataPropertyName = "remark"
            dgcolhReviewer.DataPropertyName = "reviewer"
            dgcolhScore.DataPropertyName = "score"
            objdgcolhIsGrp.DataPropertyName = "is_grp"

            dgvData.DataSource = dtTable

            Call SetGridColor()
            Call SetCollapseValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    'Private Sub FillList()
    '    Dim dsList As New DataSet
    '    Dim StrSearching As String = String.Empty
    '    Dim dtTable As New DataTable
    '    Dim lvItem As ListViewItem
    '    Try
    '        'Anjan (25 Oct 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS on Andrew's Request
    '        If User._Object.Privilege._AllowtoViewInterviewAnalysisList = False Then Exit Sub
    '        'Anjan (25 Oct 2012)-End 


    '        dsList = objInterviewAnalysisMaster.GetList("AnalysisData")



    '        If CInt(cboApplicant.SelectedValue) > 0 Then
    '            StrSearching &= "AND applicantunkid = " & CInt(cboApplicant.SelectedValue) & " "
    '        End If

    '        If dtpAnalysisDateFrom.Checked = True And dtpAnalysisDateTo.Checked = True Then
    '            StrSearching &= "AND analysisdate >= '" & eZeeDate.convertDate(dtpAnalysisDateFrom.Value) & "' AND analysisdate <= '" & eZeeDate.convertDate(dtpAnalysisDateTo.Value) & "'" & " "
    '        End If


    '        If CInt(cboInterviewType.SelectedValue) > 0 Then
    '            StrSearching &= "AND interviewtypeunkid = " & CInt(cboInterviewType.SelectedValue) & " "
    '        End If


    '        If chkComplete.Checked = True And chkIncomplete.Checked = True Then
    '            StrSearching &= ""
    '        ElseIf chkComplete.Checked = True Then
    '            StrSearching &= "AND iscomplete = 1 "
    '        ElseIf chkIncomplete.Checked = True Then
    '            StrSearching &= "AND iscomplete = 0 "
    '        ElseIf chkComplete.Checked = False And chkIncomplete.Checked = False Then
    '            StrSearching &= ""
    '        End If

    '        'S.SANDEEP [ 28 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        If CInt(cboVacancy.SelectedValue) > 0 Then
    '            StrSearching &= "AND vacancyunkid = '" & CInt(cboVacancy.SelectedValue) & "' "
    '        End If

    '        If CInt(cboBatch.SelectedValue) > 0 Then
    '            StrSearching &= "AND batchscheduleunkid = '" & CInt(cboBatch.SelectedValue) & "' "
    '        End If

    '        If CInt(cboResult.SelectedValue) > 0 Then
    '            StrSearching &= "AND resultcodeunkid = '" & CInt(cboResult.SelectedValue) & "' "
    '        End If
    '        'S.SANDEEP [ 28 FEB 2012 ] -- END



    '        'Pinkal (12-Mar-2013) -- Start
    '        'Enhancement : TRA Changes

    '        If chkEligible.Checked = True And chkNotEligible.Checked = True Then
    '            StrSearching &= ""
    '        ElseIf chkEligible.Checked = True Then
    '            StrSearching &= "AND iseligible = 1 "
    '        ElseIf chkNotEligible.Checked = True Then
    '            StrSearching &= "AND iseligible = 0 "
    '        ElseIf chkEligible.Checked = False And chkNotEligible.Checked = False Then
    '            StrSearching &= ""
    '        End If

    '        'Pinkal (12-Mar-2013) -- End



    '        If StrSearching.Length > 0 Then
    '            StrSearching = StrSearching.Substring(3)
    '            dtTable = New DataView(dsList.Tables("AnalysisData"), StrSearching, "Applicant", DataViewRowState.CurrentRows).ToTable
    '        Else
    '            dtTable = New DataView(dsList.Tables("AnalysisData"), "", "Applicant", DataViewRowState.CurrentRows).ToTable
    '        End If

    '        lvAnalysisList.Items.Clear()

    '        For Each dtRow As DataRow In dtTable.Rows
    '            lvItem = New ListViewItem

    '            If CBool(dtRow.Item("iscomplete")) = True Then
    '                lvItem.ForeColor = Color.Blue
    '            End If

    '            'S.SANDEEP [ 14 May 2013 ] -- START
    '            'ENHANCEMENT : TRA ENHANCEMENT
    '            'lvItem.Text = dtRow.Item("Applicant").ToString
    '            lvItem.Text = ""
    '            lvItem.SubItems.Add(dtRow.Item("interviewtype").ToString)
    '            'S.SANDEEP [ 14 May 2013 ] -- END
    '            lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("analysisdate").ToString).ToShortDateString & " " & dtRow.Item("analysistime"))
    '            If CBool(dtRow.Item("iscomplete")) = True Then
    '                lvItem.SubItems.Add(chkComplete.Text)
    '            Else
    '                lvItem.SubItems.Add(chkIncomplete.Text)
    '            End If
    '            lvItem.SubItems.Add(dtRow.Item("reviewer").ToString)

    '            lvItem.SubItems.Add(dtRow.Item("score").ToString)
    '            lvItem.SubItems.Add(dtRow.Item("appbatchscheduletranunkid").ToString)
    '            lvItem.SubItems.Add(dtRow.Item("analysistranunkid").ToString)
    '            lvItem.SubItems.Add(dtRow.Item("iseligible").ToString)
    '            lvItem.Tag = dtRow.Item("analysisunkid")

    '            If CBool(dtRow.Item("iscomplete")) = True Then
    '                If CInt(dtRow.Item("interviewer_level")) = CInt(dtTable.Compute("MAX(interviewer_level)", " vacancyunkid =" & CInt(dtRow.Item("vacancyunkid")))) Then
    '                    'S.SANDEEP [ 25 DEC 2011 ] -- START
    '                    'ENHANCEMENT : TRA CHANGES
    '                    'If CInt(dtRow.Item("analysistranunkid")) = CInt(dtTable.Compute("MAX(analysistranunkid)", " vacancyunkid = " & CInt(dtRow.Item("vacancyunkid")))) Then
    '                    '    lvItem.ForeColor = Color.Magenta
    '                    'End If

    '                    If CInt(dtRow.Item("analysistranunkid")) = CInt(dtTable.Compute("MAX(analysistranunkid)", " vacancyunkid ='" & CInt(dtRow.Item("vacancyunkid")) & "' AND applicantunkid = '" & CInt(dtRow.Item("applicantunkid")) & "'")) Then
    '    lvItem.ForeColor = Color.Magenta
    'End If
    '                    'S.SANDEEP [ 25 DEC 2011 ] -- END
    '                End If
    '            End If


    '            'Pinkal (07-Jan-2012) -- Start
    '            'Enhancement : TRA Changes
    '            lvItem.SubItems.Add(dtRow.Item("vacancyunkid").ToString)
    '            'Pinkal (07-Jan-2012) -- End

    '            'S.SANDEEP [ 28 FEB 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            lvItem.SubItems.Add(dtRow.Item("remark").ToString)
    '            'S.SANDEEP [ 28 FEB 2012 ] -- END


    '            'S.SANDEEP [ 14 May 2013 ] -- START
    '            'ENHANCEMENT : TRA ENHANCEMENT
    '            lvItem.SubItems.Add(dtRow.Item("Applicant").ToString)
    '            'S.SANDEEP [ 14 May 2013 ] -- END

    '            lvAnalysisList.Items.Add(lvItem)

    '            lvItem = Nothing
    '        Next

    '        lvAnalysisList.GroupingColumn = colhApplicant
    '        lvAnalysisList.DisplayGroups(True)



    '        'S.SANDEEP [ 25 DEC 2011 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If lvAnalysisList.Items.Count > 5 Then
    '        '    colhScore.Width = 123 - 20
    '        'Else
    '        '    colhScore.Width = 123
    '        'End If
    'If lvAnalysisList.Items.Count > 5 Then
    '            colhReviewer.Width = 245 - 20
    'Else
    '            colhReviewer.Width = 245
    'End If
    '        'S.SANDEEP [ 25 DEC 2011 ] -- END


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray
            Dim pCell As New clsMergeCell

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvData.Rows(i).DefaultCellStyle = dgvcsHeader
                    'If CInt(dgvData.Rows(i).Cells(objdgcolhStatusId.Index).Value) = 1 Then dgvData.Rows(i).Cells(objdgcolhIsCheck.Index).ReadOnly = True
                Else
                    pCell.MakeMerge(dgvData, dgvData.Rows(i).Index, 0, 1, Color.White, Color.White, , , picStayView.Image)
                    dgvData.Rows(i).ReadOnly = True
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvData.Font.FontFamily, 13, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            'S.SANDEEP [08-DEC-2017] -- START
            'ISSUE/ENHANCEMENT : CurrencyManager ERROR
            Dim objCMManager As CurrencyManager = CType(BindingContext(dgvData.DataSource), CurrencyManager)
            'S.SANDEEP [08-DEC-2017] -- END

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                        dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                    End If
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
                Else
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
                    'S.SANDEEP [08-DEC-2017] -- START
                    'ISSUE/ENHANCEMENT : CurrencyManager ERROR
                    'dgvData.Rows(i).Visible = False
                    objCMManager.SuspendBinding()
                    dgvData.Rows(i).Visible = False
                    objCMManager.ResumeBinding()
                    'S.SANDEEP [08-DEC-2017] -- END
                    dgvData.Rows(i).Cells(objdgcolhCheck.Index).ToolTipText = ""
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Set_Notification_Approvals(ByVal uName As String, ByVal iCount As Integer, ByVal iUserId As Integer) As String 'S.SANDEEP [ 31 DEC 2013 ] -- START {iUserId} -- END
        'Private Function Set_Notification_Approvals(ByVal uName As String, ByVal iCount As Integer) As String
        Dim StrMessage As New System.Text.StringBuilder
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("Dear <b>" & uName & "</b></span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 22, "Dear") & " <b>" & getTitleCase(uName) & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to notify you that after the interview process, <b>" & iCount.ToString & "</b> applicant(s).has/have been set as eligible for appointment in the vacancy of <b>" & cboVacancy.Text & "</b>. </span></p>")
            'Sohail (13 May 2020) -- Start
            'NMB Issue # : The notification sent out to approver to approve the eligible applicant is not proper. It has concatenated the messages incorrectly. I have tried changing on language but its not getting changed properly.
            'StrMessage.Append(Language.getMessage(mstrModuleName, 21, "This is to notify you that after the interview process,") & " <b>" & iCount.ToString & "</b> " & Language.getMessage(mstrModuleName, 22, "applicant(s).has/have been set as eligible for appointment in the vacancy of") & " <b>" & cboVacancy.Text & "</b>. </span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 21, "This is to notify you that after the interview process,") & " <b>" & iCount.ToString & "</b> " & Language.getMessage(mstrModuleName, 20, "applicant(s).has/have been set as eligible for appointment in the vacancy of") & " <b>" & cboVacancy.Text & "</b>. </span></p>")
            'Sohail (13 May 2020) -- End
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;which is an <b>" & cboVacancyType.Text & "</b> vacancy.</span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 23, "which is an") & " <b>" & cboVacancyType.Text & "</b> " & Language.getMessage(mstrModuleName, 24, "vacancy.") & "</span></p>")
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            'S.SANDEEP [ 31 DEC 2013 ] -- START
            'StrMessage.Append("Please login to Aruti Recruitment and <b> Approve/Disapprove </b>  the Applicant(s) set as eligible before they  get posted to <b>" & Company._Object._Name & "</b> Employee Master.</span></p>")
            Dim iDataValue As String = Company._Object._Companyunkid & "|" & iUserId & "|" & cboVacancyType.SelectedValue & "|" & cboVacancy.SelectedValue & "|" & _
                                  cboBatch.SelectedValue & "|" & cboResult.SelectedValue & "|" & cboApplicant.SelectedValue & "|" & cboInterviewType.SelectedValue & "|" & _
                                  IIf(dtpAnalysisDateFrom.Checked = True, dtpAnalysisDateFrom.Value.Date, "") & "|" & IIf(dtpAnalysisDateTo.Checked = True, dtpAnalysisDateTo.Value.Date, "") & "|" & _
                                  IIf(chkEligible.Checked = True, 1, 0) & "|" & IIf(chkNotEligible.Checked = True, 1, 0)

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language


            'StrMessage.Append("Please login to Aruti Recruitment or click below link <BR> " & _
            '                  ConfigParameter._Object._ArutiSelfServiceURL & "/Recruitment/wPgInterviewAnalysisList.aspx?" & _
            '                  System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iDataValue)) & _
            '                  "<BR> to <b> Approve/Disapprove </b>  the Applicant(s) set as eligible before they  get posted to <b>" & Company._Object._Name & "</b> Employee Master.</span></p>")


            StrMessage.Append(Language.getMessage(mstrModuleName, 25, "Please login to Aruti Recruitment or click below link") & "<BR> " & _
                              ConfigParameter._Object._ArutiSelfServiceURL & "/Recruitment/wPgInterviewAnalysisList.aspx?" & _
                              System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iDataValue)) & _
                          "<BR> " & Language.getMessage(mstrModuleName, 26, "to") & " <b>" & Language.getMessage(mstrModuleName, 27, "Approve/Disapprove") & " </b>" & Language.getMessage(mstrModuleName, 28, "the Applicant(s) set as eligible before they  get posted to") & " <b>" & Company._Object._Name & "</b>" & Language.getMessage(mstrModuleName, 29, "Employee Master.") & "</span></p>")
            'Gajanan [27-Mar-2019] -- End

            'S.SANDEEP [ 31 DEC 2013 ] -- END

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End



            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Notification_Approvals", mstrModuleName)
            Return ""
        End Try
        Return StrMessage.ToString
    End Function

    Private Sub Send_Notification(ByVal iCount As Integer)
        Try
            Dim dicUnQEmail As New Dictionary(Of String, String)
            Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
            Dim dUList As New DataSet : Dim dtULst As DataTable = Nothing
            dUList = objUsr.Get_UserBy_PrivilegeId(808)
            dtULst = dUList.Tables(0)
            dUList = objUsr.Get_UserBy_PrivilegeId(809)
            dtULst.Merge(dUList.Tables(0), True)
            If dtULst.Rows.Count > 0 Then
                For Each dRow As DataRow In dtULst.Rows
                    If dicUnQEmail.ContainsKey(CStr(dRow.Item("UEmail"))) = True Then Continue For
                    dicUnQEmail.Add(CStr(dRow.Item("UEmail")), CStr(dRow.Item("UEmail")))
                    Dim objSMail As New clsSendMail
                    'S.SANDEEP [ 31 DEC 2013 ] -- START
                    'StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), iCount)
                    StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), iCount, CInt(dRow.Item("UId")))
                    'S.SANDEEP [ 31 DEC 2013 ] -- END
                    objSMail._ToEmail = CStr(dRow.Item("UEmail"))
                    objSMail._Subject = Language.getMessage(mstrModuleName, 10, "Eligible applicant approval")
                    objSMail._Message = StrMessage
                    ''objSMail._FormName = "" 'Please pass Form Name for WEB
                    'objSMail._LoginEmployeeunkid = -1
                    With objSMail
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    'Sohail (17 Dec 2014) -- End                    
                    objSMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSMail._UserUnkid = User._Object._Userunkid
                    objSMail._SenderAddress = User._Object._Email
                    objSMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT
                    'S.SANDEEP [ 28 JAN 2014 ] -- END
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objSMail.SendMail() : objSMail = Nothing
                    objSMail.SendMail(Company._Object._Companyunkid) : objSMail = Nothing
                    'Sohail (30 Nov 2017) -- End
                    mblnIsSuccess = True
                Next
            Else
                mblnIsSuccess = False
            End If
        Catch ex As Exception
            mblnIsSuccess = False
            DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 14 May 2013 ] -- END

    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisbility()
        Try
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditInterviewAnalysis
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteInterviewAnalysis
            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'btnFinalEvaluation.Enabled = User._Object.Privilege._AllowtoAddInterviewFinalEvaluation
            mnuMarkAs.Enabled = User._Object.Privilege._AllowtoAddInterviewFinalEvaluation
            mnuSubmitForApproval.Enabled = User._Object.Privilege._AllowtoSubmitApplicantEligibilityForApproval
            'S.SANDEEP [ 14 May 2013 ] -- END

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges in payroll module.
            mnuAllEligibleApplicants.Enabled = User._Object.Privilege._AllowToViewEligibleApplicants
            mnuEligibleApplicantsPending.Enabled = User._Object.Privilege._AllowToViewEligibleApplicants
            mnuEligibleApplicantsApproved.Enabled = User._Object.Privilege._AllowToViewEligibleApplicants
            mnuEligibleApplicantsNotApproved.Enabled = User._Object.Privilege._AllowToViewEligibleApplicants
            mnuEligible.Enabled = User._Object.Privilege._AllowToPerformEligibleOperation
            mnuNotEligible.Enabled = User._Object.Privilege._AllowToPerformNotEligibleOperation
            'Varsha Rana (17-Oct-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisbility", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 25 DEC 2011 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmInterviewAnalysisList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvAnalysisList.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmInterviewAnalysisList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmInterviewAnalysisList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objInterviewAnalysisMaster = New clsInterviewAnalysis_master
        objInterviewAnalysisTran = New clsInteviewAnalysis_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            'Sohail (05 Jun 2020) -- Start
            'NMB Enhancement # : On interview analysis list screen, after changing the below captions on language, they are not reflecting.
            Call OtherSettings()
            'Sohail (05 Jun 2020) -- End
            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()

            lvAnalysisList.GridLines = False
            chkIncomplete.Checked = True

            Call FillCombo()

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'Call FillList()
            'S.SANDEEP [ 14 May 2013 ] -- END



            If lvAnalysisList.Items.Count > 0 Then lvAnalysisList.Items(0).Selected = True
            lvAnalysisList.Select()


            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisbility()
            'S.SANDEEP [ 25 DEC 2011 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmInterviewAnalysisList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmInterviewAnalysisList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objInterviewAnalysisMaster = Nothing
        objInterviewAnalysisTran = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsInterviewAnalysis_master.SetMessages()
            objfrm._Other_ModuleNames = "clsInterviewAnalysis_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    '    Dim objfrm As New frmLanguage
    '    Try
    '        If User._Object._RightToLeft = True Then
    '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objfrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objfrm)
    '        End If

    '        Call SetMessages()

    '        clsTraining_Analysis_Master.SetMessages()
    '        objfrm._Other_ModuleNames = "clsTraining_Analysis_Master"
    '        objfrm.displayDialog(Me)

    '        Call SetLanguage()

    '    Catch ex As System.Exception
    '        Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
    '    Finally
    '        objfrm.Dispose()
    '        objfrm = Nothing
    '    End Try
    'End Sub
#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            If CInt(cboVacancyType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Vacancy Type is mandatory information. Please select Vacancy Type."), enMsgBoxStyle.Information)
                cboVacancyType.Focus()
                Exit Sub
            End If

            If CInt(cboVacancy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Vacancy is mandatory information. Please select Vacancy."), enMsgBoxStyle.Information)
                cboVacancy.Focus()
                Exit Sub
            End If
            'S.SANDEEP [ 14 May 2013 ] -- END
            Call FillList()
            lvAnalysisList.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try

            cboApplicant.SelectedValue = 0
            cboInterviewType.SelectedValue = 0
            chkComplete.Checked = False
            chkIncomplete.Checked = False
            dtpAnalysisDateFrom.Value = Today.Date
            dtpAnalysisDateTo.Value = Today.Date
            dtpAnalysisDateFrom.Checked = False
            dtpAnalysisDateTo.Checked = False
            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboVacancyType.SelectedValue = 0
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'Call FillList()
            dgvData.DataSource = Nothing
            'S.SANDEEP [ 14 May 2013 ] -- END

            lvAnalysisList.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApplicant.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboApplicant.ValueMember
                .DisplayMember = cboApplicant.DisplayMember
                .DataSource = CType(cboApplicant.DataSource, DataTable)
                .CodeMember = "applicant_code"
            End With

            If objFrm.DisplayDialog Then
                cboApplicant.SelectedValue = objFrm.SelectedValue
                cboApplicant.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)

            End If

            With frm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = CType(cboVacancy.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboVacancy.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If dtTable Is Nothing Then Exit Sub
            If dgvData.SelectedRows.Count > 0 Then
                If dgvData.Rows(dgvData.SelectedRows(0).Index).Cells(objdgcolhIsGrp.Index).Value = False Then
                    If dtTable.Rows(dgvData.SelectedRows(0).Index).Item("issent") = True Or dtTable.Rows(dgvData.SelectedRows(0).Index).Item("statustypid") <> enShortListing_Status.SC_PENDING Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, You cannot edit selected applicant. Reason : Approval Notification already sent or applicant is approved."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    Dim frm As New frmInterviewAnalysis_AddEdit
                    If frm.displayDialog(dtTable.Rows(dgvData.SelectedRows(0).Index).Item("analysisunkid"), enAction.EDIT_ONE, dtTable.Rows(dgvData.SelectedRows(0).Index).Item("appbatchscheduletranunkid")) = True Then
                        Call FillList()
                    End If
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If dtTable Is Nothing Then Exit Sub

            Dim dRow As DataRow() = dtTable.Select("ischeck = True AND is_grp = True")
            If dRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please tick atleast one transaction from the list to perform further operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete selected transaction(s)?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.INTERVIEW, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If
                frm = Nothing
                Dim blnFlag As Boolean = False
                For i As Integer = 0 To dRow.Length - 1
                    If dRow(i).Item("issent") = True Or dRow(i).Item("statustypid") <> enShortListing_Status.SC_PENDING Then
                        If blnFlag = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot delete some of the selected applicant(s). Reason : Approval Notification already sent or applicant(s). are approved."), enMsgBoxStyle.Information)
                            blnFlag = True : Continue For
                        End If
                    Else
                        For Each sKeyId As String In dRow(i).Item("a_ids").ToString.Replace("'", "").Split(CChar(","))
                            objInterviewAnalysisMaster._Isvoid = True
                            objInterviewAnalysisMaster._Voidatetime = ConfigParameter._Object._CurrentDateAndTime
                            objInterviewAnalysisMaster._Voidreason = mstrVoidReason
                            objInterviewAnalysisMaster._Voiduserunkid = User._Object._Userunkid

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objInterviewAnalysisMaster._FormName = mstrModuleName
                            objInterviewAnalysisMaster._LoginEmployeeunkid = 0
                            objInterviewAnalysisMaster._ClientIP = getIP()
                            objInterviewAnalysisMaster._HostName = getHostName()
                            objInterviewAnalysisMaster._FromWeb = False
                            objInterviewAnalysisMaster._AuditUserId = User._Object._Userunkid
objInterviewAnalysisMaster._CompanyUnkid = Company._Object._Companyunkid
                            objInterviewAnalysisMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END

                            If objInterviewAnalysisMaster.Delete(CInt(sKeyId)) = False Then
                                Exit Sub
                            End If
                        Next
                    End If
                Next
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Contols Event(s) "

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy
            Dim dsVac As New DataSet

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsVac = objVacancy.getComboList(True, "List", , CInt(cboVacancyType.SelectedValue))
            dsVac = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", , CInt(cboVacancyType.SelectedValue))
            'Shani(24-Aug-2015) -- End

            With cboVacancy
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsVac.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancyType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboVacancy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancy.SelectedIndexChanged
        Try
            If CInt(cboVacancy.SelectedValue) > 0 Then
                Dim dsBatch As New DataSet
                Dim objBatch As New clsBatchSchedule

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsBatch = objBatch.getListForCombo("List", True)
                dsBatch = objBatch.getListForCombo(FinancialYear._Object._Database_Start_Date, "List", True)
                'Shani(24-Aug-2015) -- End

                Dim dtFilter = New DataView(dsBatch.Tables(0), "vacancyid IN(0," & CInt(cboVacancy.SelectedValue) & ")", "", DataViewRowState.CurrentRows).ToTable
                With cboBatch
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dtFilter
                    .SelectedValue = 0
                End With
            Else
                cboBatch.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboVacancy_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboBatch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBatch.SelectedIndexChanged
        Try
            If CInt(cboBatch.SelectedValue) > 0 Then
                Dim dsResult As New DataSet
                Dim objBatch As New clsBatchSchedule
                Dim objResult As New clsresult_master
                objBatch._Batchscheduleunkid = CInt(cboBatch.SelectedValue)
                dsResult = objResult.getComboList("List", True, objBatch._Resultgroupunkid)
                objBatch = Nothing
                With cboResult
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsResult.Tables("List")
                    .SelectedValue = 0
                End With
            Else
                cboResult.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBatch_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dtpAnalysisDateFrom_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpAnalysisDateFrom.ValueChanged

        Try
            If dtpAnalysisDateFrom.Checked = True Then
                dtpAnalysisDateTo.Checked = True
                dtpAnalysisDateTo.Value = dtpAnalysisDateFrom.Value
            ElseIf dtpAnalysisDateFrom.Checked = False Then
                dtpAnalysisDateTo.Checked = False
                dtpAnalysisDateTo.Value = dtpAnalysisDateFrom.Value
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpAnalysisDateFrom_ValueChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub dtpAnalysisDateTo_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpAnalysisDateTo.ValueChanged

        Try
            If dtpAnalysisDateTo.Checked = True Then
                dtpAnalysisDateFrom.Checked = True
                'dtpAnalysisDateTo.Value = dtpAnalysisDateFrom.Value
            ElseIf dtpAnalysisDateTo.Checked = False Then
                dtpAnalysisDateFrom.Checked = False
                'dtpAnalysisDateTo.Value = dtpAnalysisDateFrom.Value
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpAnalysisDateTo_ValueChanged", mstrModuleName)
        End Try

    End Sub

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Private Sub mnuMarkAs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMarkAs.Click
        Try
            If dtTable Is Nothing Then Exit Sub

            Dim dRow As DataRow() = dtTable.Select("ischeck = True AND is_grp = True")
            If dRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please tick atleast one transaction from the list to perform further operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If dRow.Length > 0 Then
                Dim sMsg As String = String.Empty
                For i As Integer = 0 To dRow.Length - 1
                    sMsg = ""

                    If dRow(i).Item("ischeck") = True AndAlso dRow(i).Item("is_grp") = True AndAlso dRow(i).Item("iscomplete") = True AndAlso dRow(i).Item("statustypid") = enShortListing_Status.SC_APPROVED Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot perform the selected operation. Reason : Selected applicant(s) has already been set as eligible/not eligible and approved."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    If objInterviewAnalysisMaster.Is_Analysis_Done(dRow(i).Item("vacancyunkid"), dRow(i).Item("applicantunkid"), sMsg) = False Then
                        If sMsg.Trim.Length > 0 Then
                            eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                        End If
                        Exit Sub
                    End If
                Next
            End If

            Dim frm As New frmEligibleOperation
            If frm.displayDialog(dRow) = True Then
                eZeeMsgBox.Show("Selected Operation applied sucessfully.", enMsgBoxStyle.Information)
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuMarkAs_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuSubmitForApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSubmitForApproval.Click
        Try
            If dtTable Is Nothing Then Exit Sub

            Dim iCount As Integer = 0
            Dim sAnalysisIds As String = ""

            Dim dRow As DataRow() = dtTable.Select("ischeck = True AND is_grp = True")
            If dRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please tick atleast one transaction from the list to perform further operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim blnDisplay As Boolean = False
            If dRow.Length > 0 Then
                For i As Integer = 0 To dRow.Length - 1
                    If dRow(i).Item("issent") = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot do submit for approval operation again. Reason : Notification is already been sent."), enMsgBoxStyle.Information)
                        Exit Sub
                    ElseIf dRow(i).Item("iscomplete") = False Then
                        Dim sMsg As String = ""
                        If objInterviewAnalysisMaster.Is_Analysis_Done(dRow(i).Item("vacancyunkid"), dRow(i).Item("applicantunkid"), sMsg) = False Then
                            If sMsg.Trim.Length > 0 Then
                                eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                            End If
                            Exit Sub
                        End If
                    ElseIf dRow(i).Item("iscomplete") = True AndAlso dRow(i).Item("statustypid") = enShortListing_Status.SC_APPROVED Then
                        If blnDisplay = False Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry,You cannot submit already approved applicant(s) for approval.These applicant(s) will be discarded." & vbCrLf & "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                blnDisplay = True : GoTo chk
                            Else
                                Exit Sub
                            End If
                        Else
chk:                        If blnDisplay = True Then
                                dRow(i).Item("ischeck") = False
                            End If
                        End If
                        'ElseIf dRow(i).Item("iscomplete") = True AndAlso dRow(i).Item("E_StatusId") = 1 Then
                    ElseIf dRow(i).Item("iscomplete") = True AndAlso dRow(i).Item("E_StatusId") >= 0 Then
                        iCount += 1
                        sAnalysisIds &= "," & dRow(i).Item("a_ids").ToString
                    End If
                Next
                If sAnalysisIds.Trim.Length > 0 Then sAnalysisIds = Mid(sAnalysisIds, 2)
                Me.Cursor = Cursors.WaitCursor
                Dim blnMsg As Boolean = False
                If iCount > 0 Then
                    Call Send_Notification(iCount)
                    If mblnIsSuccess = True Then
                        If sAnalysisIds.Trim.Length > 0 Then
                            For Each sKeyId As String In sAnalysisIds.ToString.Replace("'", "").Split(CChar(","))
                                objInterviewAnalysisMaster._Analysisunkid = CInt(sKeyId)
                                objInterviewAnalysisMaster._Issent = True

                                'S.SANDEEP [28-May-2018] -- START
                                'ISSUE/ENHANCEMENT : {Audit Trails} 
                                objInterviewAnalysisMaster._FormName = mstrModuleName
                                objInterviewAnalysisMaster._LoginEmployeeunkid = 0
                                objInterviewAnalysisMaster._ClientIP = getIP()
                                objInterviewAnalysisMaster._HostName = getHostName()
                                objInterviewAnalysisMaster._FromWeb = False
                                objInterviewAnalysisMaster._AuditUserId = User._Object._Userunkid
objInterviewAnalysisMaster._CompanyUnkid = Company._Object._Companyunkid
                                objInterviewAnalysisMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                'S.SANDEEP [28-May-2018] -- END

                                blnMsg = objInterviewAnalysisMaster.Update()
                            Next
                        End If
                    End If
                End If
                If blnMsg = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Notification Sent Successfully."), enMsgBoxStyle.Information)
                    Call FillList()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuSubmitForApproval_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub mnuApproveDisapprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuApproveDisapprove.Click
        'Dim frm As New frmInterviewFinalEvaluation
        'Try
        '    If dtTable Is Nothing Then Exit Sub

        '    Dim sAnalysisIds As String = ""

        '    Dim dRow As DataRow() = dtTable.Select("ischeck = True AND is_grp = True")
        '    If dRow.Length <= 0 Then
        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please tick atleast one transaction from the list to perform further operation on it."), enMsgBoxStyle.Information)
        '        Exit Sub
        '    End If

        '    Dim blnDisplay As Boolean = False
        '    If dRow.Length > 0 Then
        '        For i As Integer = 0 To dRow.Length - 1
        '            If dRow(i).Item("issent") = False Then
        '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, You cannot do approve/disapprove operation. Reason : Notification is still pending to send for some applicant(s)."), enMsgBoxStyle.Information)
        '                Exit Sub
        '            ElseIf dRow(i).Item("iscomplete") = False Then
        '                Dim sMsg As String = ""
        '                If objInterviewAnalysisMaster.Is_Analysis_Done(dRow(i).Item("vacancyunkid"), dRow(i).Item("applicantunkid"), sMsg) = False Then
        '                    If sMsg.Trim.Length > 0 Then
        '                        eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
        '                    End If
        '                    Exit Sub
        '                End If
        '                'ElseIf dRow(i).Item("iscomplete") = True AndAlso dRow(i).Item("E_StatusId") = 1 Then
        '            ElseIf dRow(i).Item("iscomplete") = True AndAlso dRow(i).Item("E_StatusId") >= 0 Then
        '                sAnalysisIds &= "," & dRow(i).Item("a_ids").ToString
        '            End If
        '        Next
        '    End If
        '    If sAnalysisIds.Trim.Length > 0 Then
        '        sAnalysisIds = Mid(sAnalysisIds, 2)
        '        If frm.displayDialog(sAnalysisIds, CInt(cboVacancy.SelectedValue)) Then
        '            Call FillList()
        '        End If
        '    End If
        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "mnuApproveDisapprove_Click", mstrModuleName)
        'Finally
        '    If frm IsNot Nothing Then frm.Dispose()
        'End Try
    End Sub

    'Pinkal (12-Jun-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub mnuAllEligibleApplicants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAllEligibleApplicants.Click
        Try
            If CInt(cboVacancy.SelectedIndex) > 0 Then
                Dim objShortListApplicantReport As New ArutiReports.clsShortListedApplicantReport(User._Object._Languageunkid, Company._Object._Companyunkid)
                objShortListApplicantReport._VacancyId = CInt(cboVacancy.SelectedValue)
                objShortListApplicantReport._VacancyName = cboVacancy.Text

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objShortListApplicantReport.generateReport(1, enPrintAction.Preview)
                objShortListApplicantReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                                              ConfigParameter._Object._ExportReportPath, _
                                                              ConfigParameter._Object._OpenAfterExport, _
                                                              1, enPrintAction.Preview)
                'Shani(24-Aug-2015) -- End

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please Select Vacancy in order to preview All Eligible Applicants."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuAllEligibleApplicants_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEligibleApplicantsPending_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEligibleApplicantsPending.Click
        Try
            If CInt(cboVacancy.SelectedIndex) > 0 Then
                Dim objShortListApplicantReport As New ArutiReports.clsShortListedApplicantReport(User._Object._Languageunkid, Company._Object._Companyunkid)
                objShortListApplicantReport._VacancyId = CInt(cboVacancy.SelectedValue)
                objShortListApplicantReport._VacancyName = cboVacancy.Text
                objShortListApplicantReport._FinalShortListStatusId = enShortListing_Status.SC_PENDING
                objShortListApplicantReport._FinalShortListStatus = Language.getMessage("clsMasterData", 759, "Pending")

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objShortListApplicantReport.generateReport(1, enPrintAction.Preview)
                objShortListApplicantReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                                              ConfigParameter._Object._ExportReportPath, _
                                                              ConfigParameter._Object._OpenAfterExport, _
                                                              1, enPrintAction.Preview)
                'Shani(24-Aug-2015) -- End

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please Select Vacancy in order to preview Pending Eligible Applicants."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEligibleApplicantsApproved_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEligibleApplicantsApproved_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEligibleApplicantsApproved.Click
        Try
            If CInt(cboVacancy.SelectedIndex) > 0 Then
                Dim objShortListApplicantReport As New ArutiReports.clsShortListedApplicantReport(User._Object._Languageunkid, Company._Object._Companyunkid)
                objShortListApplicantReport._VacancyId = CInt(cboVacancy.SelectedValue)
                objShortListApplicantReport._VacancyName = cboVacancy.Text
                objShortListApplicantReport._FinalShortListStatusId = enShortListing_Status.SC_APPROVED
                objShortListApplicantReport._FinalShortListStatus = Language.getMessage("clsMasterData", 760, "Approved")

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objShortListApplicantReport.generateReport(1, enPrintAction.Preview)
                objShortListApplicantReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                                              ConfigParameter._Object._ExportReportPath, _
                                                              ConfigParameter._Object._OpenAfterExport, _
                                                              1, enPrintAction.Preview)
                'Shani(24-Aug-2015) -- End

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please Select Vacancy in order to preview Approved Eligible Applicants."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEligibleApplicantsApproved_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuEligibleApplicantsNotApproved_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEligibleApplicantsNotApproved.Click
        Try
            If CInt(cboVacancy.SelectedIndex) > 0 Then
                Dim objShortListApplicantReport As New ArutiReports.clsShortListedApplicantReport(User._Object._Languageunkid, Company._Object._Companyunkid)
                objShortListApplicantReport._VacancyId = CInt(cboVacancy.SelectedValue)
                objShortListApplicantReport._VacancyName = cboVacancy.Text
                objShortListApplicantReport._FinalShortListStatusId = enShortListing_Status.SC_REJECT
                objShortListApplicantReport._FinalShortListStatus = Language.getMessage("clsMasterData", 761, "Disapproved")

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS(20151103)
                'objShortListApplicantReport.generateReport(1, enPrintAction.Preview)
                objShortListApplicantReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                                              ConfigParameter._Object._ExportReportPath, _
                                                              ConfigParameter._Object._OpenAfterExport, _
                                                              1, enPrintAction.Preview)
                'Shani(24-Aug-2015) -- End

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please Select Vacancy in order to preview Disapproved Eligible Applicants."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEligibleApplicantsNotApproved_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (12-Jun-2013) -- End

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvData.Rows(i).Visible = False Then
                                    dgvData.Rows(i).Visible = True
                                Else
                                    dgvData.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 14 May 2013 ] -- END

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            If dtTable IsNot Nothing Then
                For Each dRow As DataRow In dtTable.Rows
                    dRow.Item("ischeck") = CBool(objchkAll.Checked)
                Next
                ' dtTable.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 09 OCT 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub

    Private Sub mnuEligible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEligible.Click
        Dim frm As New frmInterviewFinalEvaluation
        Try
            If dtTable Is Nothing Then Exit Sub
            Dim sAnalysisIds As String = ""
            Dim dRow As DataRow() = dtTable.Select("ischeck = True AND is_grp = True AND E_StatusId = 1")
            If dRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please tick atleast one Eligible transaction from the list to perform further operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim blnDisplay As Boolean = False
            If dRow.Length > 0 Then
                For i As Integer = 0 To dRow.Length - 1
                    If dRow(i).Item("issent") = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, You cannot do approve/disapprove operation. Reason : Notification is still pending to send for some applicant(s)."), enMsgBoxStyle.Information)
                        Exit Sub
                    ElseIf dRow(i).Item("iscomplete") = False Then
                        Dim sMsg As String = ""
                        If objInterviewAnalysisMaster.Is_Analysis_Done(dRow(i).Item("vacancyunkid"), dRow(i).Item("applicantunkid"), sMsg) = False Then
                            If sMsg.Trim.Length > 0 Then
                                eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                            End If
                            Exit Sub
                        End If
                    ElseIf dRow(i).Item("iscomplete") = True AndAlso dRow(i).Item("E_StatusId") >= 0 Then
                        sAnalysisIds &= "," & dRow(i).Item("a_ids").ToString
                    End If
                Next
            End If
            If sAnalysisIds.Trim.Length > 0 Then
                sAnalysisIds = Mid(sAnalysisIds, 2)
                If frm.displayDialog(sAnalysisIds, CInt(cboVacancy.SelectedValue)) Then
                    Call FillList()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuEligible_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuNotEligible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNotEligible.Click
        Try
            Dim frm As New frmInterviewFinalEvaluation
            Try
                If dtTable Is Nothing Then Exit Sub
                Dim sAnalysisIds As String = ""
                Dim dRow As DataRow() = dtTable.Select("ischeck = True AND is_grp = True AND E_StatusId = 0")
                If dRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Please tick atleast one Not Eligible transaction from the list to perform further operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Dim blnDisplay As Boolean = False
                If dRow.Length > 0 Then
                    For i As Integer = 0 To dRow.Length - 1
                        If dRow(i).Item("issent") = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, You cannot do approve/disapprove operation. Reason : Notification is still pending to send for some applicant(s)."), enMsgBoxStyle.Information)
                            Exit Sub
                        ElseIf dRow(i).Item("iscomplete") = False Then
                            Dim sMsg As String = ""
                            If objInterviewAnalysisMaster.Is_Analysis_Done(dRow(i).Item("vacancyunkid"), dRow(i).Item("applicantunkid"), sMsg) = False Then
                                If sMsg.Trim.Length > 0 Then
                                    eZeeMsgBox.Show(sMsg, enMsgBoxStyle.Information)
                                End If
                                Exit Sub
                            End If
                        ElseIf dRow(i).Item("iscomplete") = True AndAlso dRow(i).Item("E_StatusId") >= 0 Then
                            sAnalysisIds &= "," & dRow(i).Item("a_ids").ToString
                        End If
                    Next
                End If
                If sAnalysisIds.Trim.Length > 0 Then
                    sAnalysisIds = Mid(sAnalysisIds, 2)
                    If frm.displayDialog(sAnalysisIds, CInt(cboVacancy.SelectedValue)) Then
                        Call FillList()
                    End If
                End If
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "mnuEligible_Click", mstrModuleName)
            Finally
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuNotEligible_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 09 OCT 2013 ] -- END
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
			
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


			 
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnFinalEvaluation.GradientBackColor = GUI._ButttonBackColor
            Me.btnFinalEvaluation.GradientForeColor = GUI._ButttonFontColor

            Me.btnEligible.GradientBackColor = GUI._ButttonBackColor
            Me.btnEligible.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblApplicant.Text = Language._Object.getCaption(Me.lblApplicant.Name, Me.lblApplicant.Text)
            Me.lblInterviewType.Text = Language._Object.getCaption(Me.lblInterviewType.Name, Me.lblInterviewType.Text)
            Me.lbAnalysisDateTo.Text = Language._Object.getCaption(Me.lbAnalysisDateTo.Name, Me.lbAnalysisDateTo.Text)
            Me.lblAnalysisDateFrom.Text = Language._Object.getCaption(Me.lblAnalysisDateFrom.Name, Me.lblAnalysisDateFrom.Text)
            Me.btnFinalEvaluation.Text = Language._Object.getCaption(Me.btnFinalEvaluation.Name, Me.btnFinalEvaluation.Text)
            Me.btnEligible.Text = Language._Object.getCaption(Me.btnEligible.Name, Me.btnEligible.Text)
            Me.chkIncomplete.Text = Language._Object.getCaption(Me.chkIncomplete.Name, Me.chkIncomplete.Text)
            Me.chkComplete.Text = Language._Object.getCaption(Me.chkComplete.Name, Me.chkComplete.Text)
            Me.colhApplicant.Text = Language._Object.getCaption(CStr(Me.colhApplicant.Tag), Me.colhApplicant.Text)
            Me.colhInterviewType.Text = Language._Object.getCaption(CStr(Me.colhInterviewType.Tag), Me.colhInterviewType.Text)
            Me.colhAnalysisDate.Text = Language._Object.getCaption(CStr(Me.colhAnalysisDate.Tag), Me.colhAnalysisDate.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
            Me.colhReviewer.Text = Language._Object.getCaption(CStr(Me.colhReviewer.Tag), Me.colhReviewer.Text)
            Me.colhScore.Text = Language._Object.getCaption(CStr(Me.colhScore.Tag), Me.colhScore.Text)
            Me.lblColor.Text = Language._Object.getCaption(Me.lblColor.Name, Me.lblColor.Text)
            Me.lblVacanyType.Text = Language._Object.getCaption(Me.lblVacanyType.Name, Me.lblVacanyType.Text)
            Me.lblVacancy.Text = Language._Object.getCaption(Me.lblVacancy.Name, Me.lblVacancy.Text)
            Me.lblBatch.Text = Language._Object.getCaption(Me.lblBatch.Name, Me.lblBatch.Text)
            Me.lblResult.Text = Language._Object.getCaption(Me.lblResult.Name, Me.lblResult.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
            Me.chkNotEligible.Text = Language._Object.getCaption(Me.chkNotEligible.Name, Me.chkNotEligible.Text)
            Me.chkEligible.Text = Language._Object.getCaption(Me.chkEligible.Name, Me.chkEligible.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuSubmitForApproval.Text = Language._Object.getCaption(Me.mnuSubmitForApproval.Name, Me.mnuSubmitForApproval.Text)
            Me.mnuMarkAs.Text = Language._Object.getCaption(Me.mnuMarkAs.Name, Me.mnuMarkAs.Text)
            Me.mnuApproveDisapprove.Text = Language._Object.getCaption(Me.mnuApproveDisapprove.Name, Me.mnuApproveDisapprove.Text)
            Me.mnuViewApplicants.Text = Language._Object.getCaption(Me.mnuViewApplicants.Name, Me.mnuViewApplicants.Text)
            Me.mnuAllEligibleApplicants.Text = Language._Object.getCaption(Me.mnuAllEligibleApplicants.Name, Me.mnuAllEligibleApplicants.Text)
            Me.mnuEligibleApplicantsApproved.Text = Language._Object.getCaption(Me.mnuEligibleApplicantsApproved.Name, Me.mnuEligibleApplicantsApproved.Text)
            Me.mnuEligibleApplicantsNotApproved.Text = Language._Object.getCaption(Me.mnuEligibleApplicantsNotApproved.Name, Me.mnuEligibleApplicantsNotApproved.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.mnuEligibleApplicantsPending.Text = Language._Object.getCaption(Me.mnuEligibleApplicantsPending.Name, Me.mnuEligibleApplicantsPending.Text)
            Me.mnuEligible.Text = Language._Object.getCaption(Me.mnuEligible.Name, Me.mnuEligible.Text)
            Me.mnuNotEligible.Text = Language._Object.getCaption(Me.mnuNotEligible.Name, Me.mnuNotEligible.Text)
            Me.dgcolhInterviewType.HeaderText = Language._Object.getCaption(Me.dgcolhInterviewType.Name, Me.dgcolhInterviewType.HeaderText)
            Me.dgcolhAnalysisDate.HeaderText = Language._Object.getCaption(Me.dgcolhAnalysisDate.Name, Me.dgcolhAnalysisDate.HeaderText)
            Me.dgcolhReviewer.HeaderText = Language._Object.getCaption(Me.dgcolhReviewer.Name, Me.dgcolhReviewer.HeaderText)
            Me.dgcolhScore.HeaderText = Language._Object.getCaption(Me.dgcolhScore.Name, Me.dgcolhScore.HeaderText)
            Me.dgcolhRemark.HeaderText = Language._Object.getCaption(Me.dgcolhRemark.Name, Me.dgcolhRemark.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete selected transaction(s)?")
            Language.setMessage(mstrModuleName, 3, "Vacancy Type is mandatory information. Please select Vacancy Type.")
            Language.setMessage(mstrModuleName, 4, "Vacancy is mandatory information. Please select Vacancy.")
            Language.setMessage(mstrModuleName, 5, "Please tick atleast one transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 6, "Sorry, you cannot perform the selected operation. Reason : Selected applicant(s) has already been set as eligible/not eligible and approved.")
            Language.setMessage(mstrModuleName, 7, "Sorry, You cannot delete some of the selected applicant(s). Reason : Approval Notification already sent or applicant(s). are approved.")
            Language.setMessage(mstrModuleName, 8, "Sorry, You cannot edit selected applicant. Reason : Approval Notification already sent or applicant is approved.")
            Language.setMessage(mstrModuleName, 9, "Sorry, You cannot do submit for approval operation again. Reason : Notification is already been sent.")
            Language.setMessage(mstrModuleName, 10, "Eligible applicant approval")
            Language.setMessage(mstrModuleName, 11, "Notification Sent Successfully.")
            Language.setMessage(mstrModuleName, 12, "Please Select Vacancy in order to preview All Eligible Applicants.")
            Language.setMessage(mstrModuleName, 13, "Please Select Vacancy in order to preview Pending Eligible Applicants.")
            Language.setMessage(mstrModuleName, 14, "Please Select Vacancy in order to preview Approved Eligible Applicants.")
            Language.setMessage(mstrModuleName, 15, "Please Select Vacancy in order to preview Disapproved Eligible Applicants.")
            Language.setMessage(mstrModuleName, 16, "Sorry, You cannot do approve/disapprove operation. Reason : Notification is still pending to send for some applicant(s).")
            Language.setMessage(mstrModuleName, 17, "Sorry,You cannot submit already approved applicant(s) for approval.These applicant(s) will be discarded." & vbCrLf & "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 18, "Please tick atleast one Eligible transaction from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 19, "Please tick atleast one Not Eligible transaction from the list to perform further operation.")
			Language.setMessage(mstrModuleName, 20, "applicant(s).has/have been set as eligible for appointment in the vacancy of")
			Language.setMessage(mstrModuleName, 21, "This is to notify you that after the interview process,")
			Language.setMessage(mstrModuleName, 22, "Dear")
			Language.setMessage(mstrModuleName, 23, "which is an")
			Language.setMessage(mstrModuleName, 24, "vacancy.")
			Language.setMessage(mstrModuleName, 25, "Please login to Aruti Recruitment or click below link")
			Language.setMessage(mstrModuleName, 26, "to")
			Language.setMessage(mstrModuleName, 27, "Approve/Disapprove")
			Language.setMessage(mstrModuleName, 28, "the Applicant(s) set as eligible before they  get posted to")
			Language.setMessage(mstrModuleName, 29, "Employee Master.")
			Language.setMessage("clsMasterData", 759, "Pending")
			Language.setMessage("clsMasterData", 760, "Approved")
			Language.setMessage("clsMasterData", 761, "Disapproved")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Private Sub btnFinalEvaluation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinalEvaluation.Click

'    If lvAnalysisList.SelectedItems.Count < 1 Then
'        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation on it."), enMsgBoxStyle.Information)
'        lvAnalysisList.Select()
'        Exit Sub
'    End If

'    If lvAnalysisList.SelectedItems(0).ForeColor = Color.Red Then
'        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot Void already voided transaction."), enMsgBoxStyle.Information) '?1
'        lvAnalysisList.Select()
'        Exit Sub
'    End If


'    'If lvAnalysisList.SelectedItems(0).ForeColor = Color.Blue Or lvAnalysisList.SelectedItems(0).ForeColor = Color.Magenta Then
'    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot Analyze the completed Analysis."), enMsgBoxStyle.Information)
'    '    lvAnalysisList.Select()
'    '    Exit Sub
'    'End If


'    Try

'        Dim frm As New frmInterviewFinalEvaluation
'        'Pinkal (07-Jan-2012) -- Start
'        'Enhancement : TRA Changes
'        'frm.displayDialog(CInt(lvAnalysisList.SelectedItems(0).Tag), enAction.EDIT_ONE, CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhAppbatchscheduletranunkid.Index).Text))


'        'S.SANDEEP [ 09 MAR 2013 ] -- START
'        'ENHANCEMENT : TRA CHANGES
'        'frm.displayDialog(CInt(lvAnalysisList.SelectedItems(0).Tag), enAction.EDIT_ONE, CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhAppbatchscheduletranunkid.Index).Text), _
'        '                  CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhVacancyunkid.Index).Text))
'        If lvAnalysisList.SelectedItems(0).ForeColor = Color.Blue Or lvAnalysisList.SelectedItems(0).ForeColor = Color.Magenta Then
'            frm.displayDialog(CInt(lvAnalysisList.SelectedItems(0).Tag), enAction.EDIT_ONE, CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhAppbatchscheduletranunkid.Index).Text), _
'                          CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhVacancyunkid.Index).Text), True)
'        Else
'            frm.displayDialog(CInt(lvAnalysisList.SelectedItems(0).Tag), enAction.EDIT_ONE, CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhAppbatchscheduletranunkid.Index).Text), _
'                              CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhVacancyunkid.Index).Text), False)
'        End If
'        'S.SANDEEP [ 09 MAR 2013 ] -- END



'        'Pinkal (07-Jan-2012) -- End

'        Call FillList()

'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "btnFinalEvaluation_Click", mstrModuleName)
'    End Try
'End Sub

'Private Sub btnEligible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEligible.Click

'    If lvAnalysisList.SelectedItems.Count < 1 Then
'        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation on it."), enMsgBoxStyle.Information)
'        lvAnalysisList.Select()
'        Exit Sub
'    End If

'    If lvAnalysisList.SelectedItems(0).SubItems(objcolhiseligible.Index).Text = True Then
'        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "This transaction is already marked as Eligible."), enMsgBoxStyle.Information)
'        lvAnalysisList.Select()
'        Exit Sub
'    End If


'    'Sandeep [ 09 Oct 2010 ] -- Start
'    Dim blnFlag As Boolean = False
'    'Sandeep [ 09 Oct 2010 ] -- End 


'    Try
'        objInterviewAnalysisMaster._Analysisunkid = CInt(lvAnalysisList.SelectedItems(0).Tag)


'        'Sandeep [ 09 Oct 2010 ] -- Start
'        'If objInterviewAnalysisMaster._Iscomplete = False Then
'        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1111, "You cannot mark this transaction as Eligible. Reason: This transaction not complete."), enMsgBoxStyle.Information)
'        '    lvAnalysisList.Select()
'        '    Exit Try
'        'Else
'        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1112, "This transaction has already been marked as Eligible."), enMsgBoxStyle.Information)
'        '    lvAnalysisList.Select()
'        '    Exit Try
'        'End If
'        If objInterviewAnalysisMaster._Iscomplete = False Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "You cannot mark this transaction as Eligible. Reason: This transaction not complete."), enMsgBoxStyle.Information)
'            lvAnalysisList.Select()
'            Exit Try
'        ElseIf objInterviewAnalysisMaster._IsEligible = True Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "This transaction has already been marked as Eligible."), enMsgBoxStyle.Information)
'            lvAnalysisList.Select()
'            Exit Try
'        End If
'        'Sandeep [ 09 Oct 2010 ] -- End 

'        objInterviewAnalysisMaster._IsEligible = True

'        'Sandeep [ 09 Oct 2010 ] -- Start
'        blnFlag = objInterviewAnalysisMaster.MarkEligible_Status(CInt(lvAnalysisList.SelectedItems(0).Tag))
'        If blnFlag = True Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Selected transaction has been successfully marked as Eligible."), enMsgBoxStyle.Information)
'            'Call btnClose_Click(sender, e)
'        End If
'        'Sandeep [ 09 Oct 2010 ] -- End 

'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "btnEligible_Click", mstrModuleName)
'    End Try

'End Sub
'Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
'    Dim frm As New frmTraningAnalysis_AddEdit
'    Try
'        'If User._Object._RightToLeft = True Then
'        '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'        '    frm.RightToLeftLayout = True
'        '    Call Language.ctlRightToLeftlayOut(frm)
'        'End If
'        If frm.displayDialog(-1, enAction.ADD_ONE) Then
'            Call FillList()
'        End If
'    Catch ex As Exception
'        Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
'    Finally
'        If frm IsNot Nothing Then frm.Dispose()
'    End Try
'End Sub
'Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'    If lvAnalysisList.SelectedItems.Count < 1 Then
'        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'        lvAnalysisList.Select()
'        Exit Sub
'    End If
'    'If objAnalysis.isUsed(CInt(lvAnalysisList.SelectedItems(0).Tag)) Then
'    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Training Schedule. Reason: This Training Schedule is in use."), enMsgBoxStyle.Information) '?2
'    '    lvAnalysisList.Select()
'    '    Exit Sub
'    'End If


'    If lvAnalysisList.SelectedItems(0).ForeColor = Color.Red Then
'        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot Void already voided transaction."), enMsgBoxStyle.Information) '?1
'        lvAnalysisList.Select()
'        Exit Sub
'    End If

'    If lvAnalysisList.SelectedItems(0).ForeColor = Color.Purple Then
'        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot Void cancelled transaction."), enMsgBoxStyle.Information) '?1
'        lvAnalysisList.Select()
'        Exit Sub
'    End If

'    If lvAnalysisList.SelectedItems(0).ForeColor = Color.Blue Or lvAnalysisList.SelectedItems(0).ForeColor = Color.Magenta Then
'        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot Delete the completed Analysis."), enMsgBoxStyle.Information)
'        lvAnalysisList.Select()
'        Exit Sub
'    End If

'    Try
'        Dim intSelectedIndex As Integer
'        intSelectedIndex = lvAnalysisList.SelectedItems(0).Index

'        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this transaction?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

'            objInterviewAnalysisMaster._Isvoid = True
'            'Sandeep [ 16 Oct 2010 ] -- Start
'            'objInterviewAnalysisMaster._Voidreason = "TESTING"
'            'objInterviewAnalysisMaster._Voidatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
'            Dim frm As New frmReasonSelection
'            Dim mstrVoidReason As String = String.Empty
'            frm.displayDialog(enVoidCategoryType.INTERVIEW, mstrVoidReason)
'            If mstrVoidReason.Length <= 0 Then
'                Exit Sub
'            Else
'                objInterviewAnalysisMaster._Voidreason = mstrVoidReason
'            End If
'            frm = Nothing
'            objInterviewAnalysisMaster._Voidatetime = ConfigParameter._Object._CurrentDateAndTime
'            'Sandeep [ 16 Oct 2010 ] -- End 
'            objInterviewAnalysisMaster._Voiduserunkid = User._Object._Userunkid

'            'Sandeep [ 16 Oct 2010 ] -- Start
'            'objInterviewAnalysisTran.Void_SingleAnalysis(CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhanalysistranunkid.Index).Text), True, _
'            '                                                 1, CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt")), "TESTING")
'            objInterviewAnalysisTran.Void_SingleAnalysis(CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhanalysistranunkid.Index).Text), _
'                                                                             True, _
'                                                                             User._Object._Userunkid, _
'                                                                             ConfigParameter._Object._CurrentDateAndTime, _
'                                                                             mstrVoidReason)
'            'Sandeep [ 16 Oct 2010 ] -- End 
'            lvAnalysisList.SelectedItems(0).Remove()

'            If lvAnalysisList.Items.Count <= 0 Then
'                Exit Try
'            Else
'                Call FillList()
'            End If

'            If lvAnalysisList.Items.Count = intSelectedIndex Then
'                intSelectedIndex = lvAnalysisList.Items.Count - 1
'                lvAnalysisList.Items(intSelectedIndex).Selected = True
'                lvAnalysisList.EnsureVisible(intSelectedIndex)
'            ElseIf lvAnalysisList.Items.Count <> 0 Then
'                lvAnalysisList.Items(intSelectedIndex).Selected = True
'                lvAnalysisList.EnsureVisible(intSelectedIndex)
'            End If
'        End If
'        lvAnalysisList.Select()
'    Catch ex As Exception
'        Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'    End Try
'End Sub

'Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
'    If lvAnalysisList.SelectedItems.Count < 1 Then
'        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select transaction from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'        lvAnalysisList.Select()
'        Exit Sub
'    End If

'    If lvAnalysisList.SelectedItems(0).ForeColor = Color.Red Then
'        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot edit Void transaction."), enMsgBoxStyle.Information) '?1
'        lvAnalysisList.Select()
'        Exit Sub
'    End If

'    If lvAnalysisList.SelectedItems(0).ForeColor = Color.Blue Or lvAnalysisList.SelectedItems(0).ForeColor = Color.Magenta Then
'        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, You cannot Edit the completed Analysis."), enMsgBoxStyle.Information)
'        lvAnalysisList.Select()
'        Exit Sub
'    End If

'    Dim frm As New frmInterviewAnalysis_AddEdit
'    Try
'        Dim intSelectedIndex As Integer
'        intSelectedIndex = lvAnalysisList.SelectedItems(0).Index

'        'If User._Object._RightToLeft = True Then
'        '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'        '    frm.RightToLeftLayout = True
'        '    Call Language.ctlRightToLeftlayOut(frm)
'        'End If

'        If frm.displayDialog(CInt(lvAnalysisList.SelectedItems(0).Tag), enAction.EDIT_ONE, CInt(lvAnalysisList.SelectedItems(0).SubItems(objcolhAppbatchscheduletranunkid.Index).Text)) Then
'            Call FillList()
'        End If
'        frm = Nothing

'        If lvAnalysisList.Items.Count > 0 Then
'            lvAnalysisList.Items(intSelectedIndex).Selected = True
'            lvAnalysisList.EnsureVisible(intSelectedIndex)
'            lvAnalysisList.Select()
'        End If
'    Catch ex As Exception
'        Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'    Finally
'        If frm IsNot Nothing Then frm.Dispose()
'    End Try
'End Sub