﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmVacancy_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmVacancy_AddEdit"
    Private menAction As enAction = enAction.ADD_ONE
    Private mintVacancyunkid As Integer = 0
    Private objVacancyData As clsVacancy
    Private mblnCancel As Boolean = True

    'Interviewer Declaration
    Private mdtInterviewerTran As DataTable
    Private mintInterviewerIndex As Integer
    Private objInterviewerTran As clsinterviewer_tran
    Private objEmployeeData As clsEmployee_Master
    Private objDepartmentData As clsDepartment
    Private objInterviewType As clsCommon_Master

    'Advertise Declaration 
    Private objAdvertiseTran As clsAdvertise_tran
    Private objAdvertise_Master As clsAdvertise_master
    Private mintAdvertiseIndex As Integer
    Private mdtAdvertiseTran As DataTable
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean

        Try
            mintVacancyunkid = intUnkId
            menAction = eAction
            Me.ShowDialog()
            intUnkId = mintVacancyunkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try

    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtPayRangeFrom.BackColor = GUI.ColorOptional
            txtPayRangeTo.BackColor = GUI.ColorOptional
            nudPosition.BackColor = GUI.ColorComp
            cboDepartmentGrp.BackColor = GUI.ColorOptional
            cboJobGroup.BackColor = GUI.ColorOptional
            cboEmploymentType.BackColor = GUI.ColorComp
            cboJob.BackColor = GUI.ColorComp
            cboSections.BackColor = GUI.ColorOptional
            cboShiftType.BackColor = GUI.ColorComp
            cboUnits.BackColor = GUI.ColorOptional
            cboStation.BackColor = GUI.ColorOptional
            cboVacancy.BackColor = GUI.ColorComp
            cboJobDepartment.BackColor = GUI.ColorComp
            cboPayType.BackColor = GUI.ColorOptional
            txtWorkingExp.BackColor = GUI.ColorOptional
            txtResponsibilitiesDuties.BackColor = GUI.ColorOptional
            txtRemark.BackColor = GUI.ColorOptional
            'Hemant (13 Sep 2021) -- Start
            'NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.
            txtOtherWorkingExperience.BackColor = GUI.ColorOptional
            'Hemant (13 Sep 2021) -- End


            '*** Interview Tab
            cboInterviewType.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            nudLevel.BackColor = GUI.ColorComp
            txtOtherInterviewerName.BackColor = GUI.ColorComp
            txtOtherDepartment.BackColor = GUI.ColorOptional
            txtOtherCompany.BackColor = GUI.ColorOptional
            txtOtherContactNo.BackColor = GUI.ColorOptional

            '*** Advertisement Tab
            cboAdvertiseCategory.BackColor = GUI.ColorComp
            cboAdvertiser.BackColor = GUI.ColorComp
            txtCosting.BackColor = GUI.ColorOptional
            txtBudget.BackColor = GUI.ColorOptional
            txtNotes.BackColor = GUI.ColorOptional
            txtDescription.BackColor = GUI.ColorOptional

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            cboGradeGroup.BackColor = GUI.ColorOptional
            cboGrade.BackColor = GUI.ColorOptional
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            'Hemant (13 Sep 2021) -- Start
            'NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.
            '*** Other Parameters Tab
            txtOtherJobQualificationDesc.BackColor = GUI.ColorOptional
            txtOtherJobSkillsDesc.BackColor = GUI.ColorOptional
            txtOtherJobLanguageDesc.BackColor = GUI.ColorOptional
            'Hemant (13 Sep 2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'ALLOCATION ORDER DO NOT REMOVE IT
            cboStation.SelectedValue = objVacancyData._Stationunkid
            cboDepartmentGrp.SelectedValue = objVacancyData._Deptgroupunkid
            cboJobDepartment.SelectedValue = objVacancyData._Departmentunkid
            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
            'cboSections.SelectedValue = objVacancyData._Sectionunkid
            'cboJobGroup.SelectedValue = objVacancyData._Jobgroupunkid
            'cboJob.SelectedValue = objVacancyData._Jobunkid
            'cboUnits.SelectedValue = objVacancyData._Unitunkid
            RemoveHandler cboSections.SelectedIndexChanged, AddressOf cboSections_SelectedIndexChanged
            cboSections.SelectedValue = objVacancyData._Sectionunkid
            AddHandler cboSections.SelectedIndexChanged, AddressOf cboSections_SelectedIndexChanged
            RemoveHandler cboJobGroup.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            cboJobGroup.SelectedValue = objVacancyData._Jobgroupunkid
            AddHandler cboJobGroup.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            RemoveHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
            cboJob.SelectedValue = objVacancyData._Jobunkid
            AddHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
            RemoveHandler cboUnits.SelectedIndexChanged, AddressOf cboUnits_SelectedIndexChanged
            cboUnits.SelectedValue = objVacancyData._Unitunkid
            AddHandler cboUnits.SelectedIndexChanged, AddressOf cboUnits_SelectedIndexChanged
            'Hemant (03 Sep 2019) -- End
            'cboDepartmentGrp.SelectedValue = objVacancyData._Deptgroupunkid
            'cboJobGroup.SelectedValue = objVacancyData._Jobgroupunkid
            cboEmploymentType.SelectedValue = objVacancyData._Employeementtypeunkid
            'cboJob.SelectedValue = objVacancyData._Jobunkid
            cboPayType.SelectedValue = objVacancyData._Paytypeunkid
            'cboSections.SelectedValue = objVacancyData._Sectionunkid
            cboShiftType.SelectedValue = objVacancyData._Shifttypeunkid
            'cboUnits.SelectedValue = objVacancyData._Unitunkid
            'cboStation.SelectedValue = objVacancyData._Stationunkid
            'cboJobDepartment.SelectedValue = objVacancyData._Departmentunkid

            If objVacancyData._Noofposition = 0 Then
                nudPosition.Value = nudPosition.Minimum
            Else
                nudPosition.Value = objVacancyData._Noofposition
            End If

            If objVacancyData._Pay_From > 0 Then
                txtPayRangeFrom.Text = CStr(objVacancyData._Pay_From)
            End If
            If objVacancyData._Pay_To > 0 Then
                txtPayRangeTo.Text = CStr(objVacancyData._Pay_To)
            End If

            txtRemark.Text = objVacancyData._Remark
            txtWorkingExp.Text = CStr(objVacancyData._Experience)
            txtResponsibilitiesDuties.Text = objVacancyData._Responsibilities_Duties

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'txtVacancyTitle.Text = objVacancyData._Vacancytitle
            cboVacancy.SelectedValue = objVacancyData._Vacancy_MasterUnkid
            'S.SANDEEP [ 28 FEB 2012 ] -- END
            cboVacancy.Tag = objVacancyData._Vacancy_MasterUnkid 'Sohail (28 May 2014)

            If objVacancyData._Openingdate = Nothing Then
                dtOpeningDate.Value = Now.Date
            Else
                dtOpeningDate.Value = objVacancyData._Openingdate
            End If

            If objVacancyData._Closingdate = Nothing Then
                dtClosingDate.Value = Now.Date
            Else
                dtClosingDate.Value = objVacancyData._Closingdate
            End If
            If objVacancyData._Interview_Startdate = Nothing Then
                dtInterviewStartDate.Value = Now.Date
            Else
                dtInterviewStartDate.Value = objVacancyData._Interview_Startdate
            End If

            If objVacancyData._Interview_Closedate = Nothing Then
                dtInterviewClosingDate.Value = Now.Date
            Else
                dtInterviewClosingDate.Value = objVacancyData._Interview_Closedate
            End If

            chkIsExternalVacancy.Checked = objVacancyData._Is_External_Vacancy

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            chkExportToWeb.Checked = objVacancyData._Iswebexport
            cboGradeGroup.SelectedValue = objVacancyData._Gradegroupunkid
            cboGrade.SelectedValue = objVacancyData._Gradeunkid
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
            RemoveHandler cboSectionGroup.SelectedIndexChanged, AddressOf cboSectionGroup_SelectedIndexChanged
            cboSectionGroup.SelectedValue = objVacancyData._SectionGrpUnkId
            AddHandler cboSectionGroup.SelectedIndexChanged, AddressOf cboSectionGroup_SelectedIndexChanged
            RemoveHandler cboUnitGroup.SelectedIndexChanged, AddressOf cboUnitGroup_SelectedIndexChanged
            cboUnitGroup.SelectedValue = objVacancyData._UnitGrpUnkId
            AddHandler cboUnitGroup.SelectedIndexChanged, AddressOf cboUnitGroup_SelectedIndexChanged
            RemoveHandler cboTeams.SelectedIndexChanged, AddressOf cboTeams_SelectedIndexChanged
            cboTeams.SelectedValue = objVacancyData._Teamunkid
            AddHandler cboTeams.SelectedIndexChanged, AddressOf cboTeams_SelectedIndexChanged
            cboCostCenter.SelectedValue = objVacancyData._Costcenterunkid
            RemoveHandler cboClassGroup.SelectedIndexChanged, AddressOf cboClassGroup_SelectedIndexChanged
            cboClassGroup.SelectedValue = objVacancyData._ClassGroupunkid
            AddHandler cboClassGroup.SelectedIndexChanged, AddressOf cboClassGroup_SelectedIndexChanged
            RemoveHandler cboClass.SelectedIndexChanged, AddressOf cboClass_SelectedIndexChanged
            cboClass.SelectedValue = objVacancyData._ClassUnkid
            AddHandler cboClass.SelectedIndexChanged, AddressOf cboClass_SelectedIndexChanged
            cboLevel.SelectedValue = objVacancyData._GradeLevelUnkId
            'Hemant (03 Sep 2019) -- End
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # : They want option to bolden the content of the advert as per the needs of the advertised position..
            objtlbbtnSkillBold.Checked = objVacancyData._Isskillbold
            objtlbbtnSkillItalic.Checked = objVacancyData._Isskillitalic
            objtlbbtnQualiBold.Checked = objVacancyData._Isqualibold
            objtlbbtnQualiItalic.Checked = objVacancyData._Isqualiitalic
            objtlbbtnExperienceBold.Checked = objVacancyData._Isexpbold
            objtlbbtnExperienceItalic.Checked = objVacancyData._Isexpitalic
            'Sohail (27 Sep 2019) -- End
            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously.
            chkIsBothInternalExternal.Checked = objVacancyData._Isbothintext
            'Sohail (18 Feb 2020) -- End
            'Hemant (13 Sep 2021) -- Start
            'NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.
            'Hemant (23 Sep 2021) -- Start
            'If objVacancyData._Other_Experience.StartsWith("{\rtf") Then
            '    txtOtherWorkingExperience.Rtf = objVacancyData._Other_Experience
            'Else
            '    txtOtherWorkingExperience.Text = objVacancyData._Other_Experience
            'End If
            'If objVacancyData._Other_Qualification.StartsWith("{\rtf") Then
            '    txtOtherJobQualificationDesc.Rtf = objVacancyData._Other_Qualification
            'Else
            '    txtOtherJobQualificationDesc.Text = objVacancyData._Other_Qualification
            'End If
            'If objVacancyData._Other_Skill.StartsWith("{\rtf") Then
            '    txtOtherJobSkillsDesc.Rtf = objVacancyData._Other_Skill
            'Else
            '    txtOtherJobSkillsDesc.Text = objVacancyData._Other_Skill
            'End If
            'If objVacancyData._Other_Language.StartsWith("{\rtf") Then
            '    txtOtherJobLanguageDesc.Rtf = objVacancyData._Other_Language
            'Else
            '    txtOtherJobLanguageDesc.Text = objVacancyData._Other_Language
            'End If            
                txtOtherWorkingExperience.Text = objVacancyData._Other_Experience
                txtOtherJobQualificationDesc.Text = objVacancyData._Other_Qualification
                txtOtherJobSkillsDesc.Text = objVacancyData._Other_Skill
                txtOtherJobLanguageDesc.Text = objVacancyData._Other_Language
            'Hemant (23 Sep 2021) -- End
            'Hemant (13 Sep 2021) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objVacancyData._Vacancytitle = txtVacancyTitle.Text
            objVacancyData._Vacancy_MasterUnkid = CInt(cboVacancy.SelectedValue)
            'S.SANDEEP [ 28 FEB 2012 ] -- END
            objVacancyData._Deptgroupunkid = CInt(cboDepartmentGrp.SelectedValue)
            objVacancyData._Jobgroupunkid = CInt(cboJobGroup.SelectedValue)
            objVacancyData._Employeementtypeunkid = CInt(cboEmploymentType.SelectedValue)
            objVacancyData._Jobunkid = CInt(cboJob.SelectedValue)
            objVacancyData._Paytypeunkid = CInt(cboPayType.SelectedValue)
            objVacancyData._Sectionunkid = CInt(cboSections.SelectedValue)
            objVacancyData._Shifttypeunkid = CInt(cboShiftType.SelectedValue)
            objVacancyData._Unitunkid = CInt(cboUnits.SelectedValue)
            objVacancyData._Stationunkid = CInt(cboStation.SelectedValue)
            objVacancyData._Departmentunkid = CInt(cboJobDepartment.SelectedValue)

            objVacancyData._Noofposition = CInt(nudPosition.Value)

            If txtPayRangeFrom.Text.Trim.Length > 0 Then
                objVacancyData._Pay_From = CDec(txtPayRangeFrom.Text)
            Else
                objVacancyData._Pay_From = 0
            End If
            If txtPayRangeTo.Text.Trim.Length > 0 Then
                objVacancyData._Pay_To = CDec(txtPayRangeTo.Text)
            Else
                objVacancyData._Pay_To = 0
            End If

            objVacancyData._Remark = txtRemark.Text
            objVacancyData._Experience = CInt(txtWorkingExp.Text)
            objVacancyData._Responsibilities_Duties = txtResponsibilitiesDuties.Text

            objVacancyData._Openingdate = dtOpeningDate.Value
            objVacancyData._Closingdate = dtClosingDate.Value
            objVacancyData._Interview_Startdate = dtInterviewStartDate.Value
            objVacancyData._Interview_Closedate = dtInterviewClosingDate.Value

            objVacancyData._Is_External_Vacancy = chkIsExternalVacancy.Checked

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objVacancyData._Iswebexport = chkExportToWeb.Checked
            objVacancyData._Gradegroupunkid = CInt(cboGradeGroup.SelectedValue)
            objVacancyData._Gradeunkid = CInt(cboGrade.SelectedValue)
            'S.SANDEEP [ 28 FEB 2012 ] -- END


            'Anjan (08 Nov 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            objVacancyData._Userunkid = User._Object._Userunkid
            'Anjan (08 Nov 2012)-End 

            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
            objVacancyData._SectionGrpUnkId = CInt(cboSectionGroup.SelectedValue)
            objVacancyData._UnitGrpUnkId = CInt(cboUnitGroup.SelectedValue)
            objVacancyData._Teamunkid = CInt(cboTeams.SelectedValue)
            objVacancyData._Costcenterunkid = CInt(cboCostCenter.SelectedValue)
            objVacancyData._ClassGroupunkid = CInt(cboClassGroup.SelectedValue)
            objVacancyData._ClassUnkid = CInt(cboClass.SelectedValue)
            objVacancyData._GradeLevelUnkId = CInt(cboLevel.SelectedValue)
            'Hemant (03 Sep 2019) -- End
            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # : They want option to bolden the content of the advert as per the needs of the advertised position..
            objVacancyData._Isskillbold = objtlbbtnSkillBold.Checked
            objVacancyData._Isskillitalic = objtlbbtnSkillItalic.Checked
            objVacancyData._Isqualibold = objtlbbtnQualiBold.Checked
            objVacancyData._Isqualiitalic = objtlbbtnQualiItalic.Checked
            objVacancyData._Isexpbold = objtlbbtnExperienceBold.Checked
            objVacancyData._Isexpitalic = objtlbbtnExperienceItalic.Checked
            'Sohail (27 Sep 2019) -- End
            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : On vacancy master, it should be possible to publish a vacancy as an internal and external vacancy simultaneously.
            objVacancyData._Isbothintext = chkIsBothInternalExternal.Checked
            'Sohail (18 Feb 2020) -- End
            'Hemant (13 Sep 2021) -- Start
            'NMB Enhancement : Adding New Fields in Vacancy Master required by NMB.
            'Hemant (23 Sep 2021) -- Start
            'objVacancyData._Other_Experience = txtOtherWorkingExperience.Rtf
            'objVacancyData._Other_Qualification = txtOtherJobQualificationDesc.Rtf
            'objVacancyData._Other_Skill = txtOtherJobSkillsDesc.Rtf
            'objVacancyData._Other_Language = txtOtherJobLanguageDesc.Rtf
            objVacancyData._Other_Experience = txtOtherWorkingExperience.Text
            objVacancyData._Other_Qualification = txtOtherJobQualificationDesc.Text
            objVacancyData._Other_Skill = txtOtherJobSkillsDesc.Text
            objVacancyData._Other_Language = txtOtherJobLanguageDesc.Text
            'Hemant (23 Sep 2021) -- End
            'Hemant (13 Sep 2021) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    'Vacancy Tab Start
    Private Sub VacancyFillCombo()
        Dim dsCombos As New DataSet
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objGradeGrp As New clsGradeGroup
        Dim objEmployee As New clsEmployee_Master
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objCommon As New clsCommon_Master
        ' Dim objShift As New clsCommon_Master
        Dim objMaster As New clsMasterData

        Dim objTranHead As New clsTransactionHead
        'Hemant (03 Sep 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Dim objCostCenter As New clscostcenter_master
        'Hemant (03 Sep 2019) -- End


        Try
            dsCombos = objStation.getComboList("Station", True)
            With cboStation
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Station")
            End With
            dsCombos = objDeptGrp.getComboList("DeptGrp", True)
            With cboDepartmentGrp
                .ValueMember = "deptgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("DeptGrp")
            End With
            dsCombos = objDepartment.getComboList("Department", True)
            With cboJobDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Department")
            End With
            dsCombos = objSection.getComboList("Section", True)
            With cboSections
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Section")
            End With
            dsCombos = objUnit.getComboList("Unit", True)
            With cboUnits
                .ValueMember = "unitunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Unit")
            End With
            dsCombos = objJobGrp.getComboList("JobGrp", True)
            With cboJobGroup
                .ValueMember = "jobgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("JobGrp")
            End With
            dsCombos = objJob.getComboList("Job", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Job")
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EmplType")
            With cboEmploymentType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("EmplType")
            End With
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.PAY_TYPE, True, "PayType")
            With cboPayType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("PayType")
            End With
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "ShiftType")
            With cboShiftType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("ShiftType")
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.VACANCY_MASTER, True, "Vacancy")
            With cboVacancy
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Vacancy")
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dsCombos = objGradeGrp.getComboList("List", True)
            With cboGradeGroup
                .ValueMember = "gradegroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
            dsCombos = objSectionGrp.getComboList("List", True)
            With cboSectionGroup
                .ValueMember = "sectiongroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = objUnitGroup.getComboList("List", True)
            With cboUnitGroup
                .ValueMember = "unitgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = objTeam.getComboList("List", True)
            With cboTeams
                .ValueMember = "teamunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With

            dsCombos = objClassGrp.getComboList("ClassGrp", True)
            With cboClassGroup
                .ValueMember = "classgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("ClassGrp")
            End With

            dsCombos = objClass.getComboList("Class", True)
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Class")
            End With

            dsCombos = objCostCenter.getComboList("CostCenter", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsCombos.Tables("CostCenter")
            End With
            'Hemant (03 Sep 2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub
    'Vacancy Tab End

    'Interviewers Tab Start
    Private Sub InterviewerFillCombo()
        Dim dsCombos As New DataSet
        objInterviewType = New clsCommon_Master
        objEmployeeData = New clsEmployee_Master
        Try
            dsCombos = objInterviewType.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True, "InterviewType")
            With cboInterviewType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("InterviewType")
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If menAction = enAction.EDIT_ONE Then
            '    dsCombos = objEmployeeData.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmployeeData.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If

            dsCombos = objEmployeeData.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                       User._Object._Userunkid, _
                                                       FinancialYear._Object._YearUnkid, _
                                                       Company._Object._Companyunkid, _
                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                       eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                       ConfigParameter._Object._UserAccessModeSetting, _
                                                       True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END



            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Employee")
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InterviewerFillCombo", mstrModuleName)
        End Try

    End Sub

    Private Sub FillInterviewersList()
        objEmployeeData = New clsEmployee_Master
        objDepartmentData = New clsDepartment
        Try
            lvInterviewer.Items.Clear()

            Dim lvInterviewerList As ListViewItem
            For Each dtRow As DataRow In mdtInterviewerTran.Rows
                objEmployeeData._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(dtRow.Item("interviewerunkid"))
                objDepartmentData._Departmentunkid = objEmployeeData._Departmentunkid
                objInterviewType._Masterunkid = CInt(dtRow.Item("interviewtypeunkid"))
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvInterviewerList = New ListViewItem
                    lvInterviewerList.Text = dtRow.Item("interviewer_level").ToString
                    If CInt(dtRow.Item("interviewerunkid")) > 0 Then
                        lvInterviewerList.SubItems.Add(objEmployeeData._Firstname + " " + objEmployeeData._Surname)
                        lvInterviewerList.SubItems.Add(Company._Object._Name)
                        lvInterviewerList.SubItems.Add(objDepartmentData._Name)
                        lvInterviewerList.SubItems.Add(objEmployeeData._Present_Tel_No)
                    Else
                        lvInterviewerList.SubItems.Add(dtRow.Item("otherinterviewer_name").ToString)
                        lvInterviewerList.SubItems.Add(dtRow.Item("othercompany").ToString)
                        lvInterviewerList.SubItems.Add(dtRow.Item("otherdepartment").ToString)
                        lvInterviewerList.SubItems.Add(dtRow.Item("othercontact_no").ToString)
                    End If

                    lvInterviewerList.SubItems.Add(objInterviewType._Name)
                    lvInterviewerList.SubItems.Add(dtRow.Item("interviewerunkid").ToString)
                    lvInterviewerList.SubItems.Add(dtRow.Item("GUID").ToString)
                    lvInterviewerList.SubItems.Add(dtRow.Item("interviewtypeunkid").ToString)
                    lvInterviewerList.Tag = dtRow.Item("interviewertranunkid")
                    lvInterviewer.Items.Add(lvInterviewerList)

                    lvInterviewerList = Nothing
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillInterviewersList", mstrModuleName)
        End Try

    End Sub

    Private Sub ResetInterviewer()

        Try
            cboInterviewType.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            lblEmployeeCompany.Text = ""
            lblEmployeeContactNo.Text = ""
            lblEmployeeDepartment.Text = ""
            nudLevel.Value = 1
            txtOtherCompany.Text = ""
            txtOtherContactNo.Text = ""
            txtOtherDepartment.Text = ""
            txtOtherInterviewerName.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub
    'Interviewers Tab End

    'Advertise Tab Start 
    Private Sub AdvertiseFillCombo()
        Dim dsCombo As New DataSet
        Dim objAdvertiseCategory As New clsCommon_Master
        Try
            dsCombo = objAdvertiseCategory.getComboList(clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY, True, "AdvertiseCategory")
            With cboAdvertiseCategory
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("AdvertiseCategory")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AdvertiseFillCombo", mstrModuleName)
        Finally
            objAdvertiseCategory = Nothing
        End Try

    End Sub

    Private Sub FillAdvertiseList()
        objAdvertise_Master = New clsAdvertise_master
        Try
            lvAdvertisement.Items.Clear()

            Dim lvAdvertiseItem As ListViewItem

            For Each dtRow As DataRow In mdtAdvertiseTran.Rows
                objAdvertise_Master._Advertiseunkid = CInt(dtRow.Item("advertiserunkid"))
                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
                    lvAdvertiseItem = New ListViewItem
                    lvAdvertiseItem.Text = objAdvertise_Master._Company
                    lvAdvertiseItem.SubItems.Add(dtRow.Item("costing").ToString)
                    lvAdvertiseItem.SubItems.Add(dtRow.Item("budget").ToString)
                    lvAdvertiseItem.SubItems.Add(dtRow.Item("description").ToString)
                    lvAdvertiseItem.SubItems.Add(dtRow.Item("notes").ToString)
                    lvAdvertiseItem.SubItems.Add(dtRow.Item("advertiserunkid").ToString)
                    lvAdvertiseItem.SubItems.Add(dtRow.Item("advertisecategoryunkid").ToString)
                    lvAdvertiseItem.Tag = dtRow.Item("advertisetranunkid")
                    'Sohail (11 Sep 2010) -- Start
                    lvAdvertiseItem.SubItems.Add(dtRow.Item("GUID").ToString)
                    'Sohail (11 Sep 2010) -- End
                    lvAdvertisement.Items.Add(lvAdvertiseItem)
                    lvAdvertiseItem = Nothing

                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillAdvertiseList", mstrModuleName)
        End Try

    End Sub

    Private Sub ResetAdvertise()

        Try
            txtBudget.Text = ""
            txtCosting.Text = ""
            txtDescription.Text = ""
            txtNotes.Text = ""
            cboAdvertiseCategory.SelectedValue = 0
            cboAdvertiser.SelectedValue = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetAdvertise", mstrModuleName)
        End Try

    End Sub
    'Advertise Tab End

    Private Sub SetVisibility()
        Try
            objbtnAddInterviewType.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddJob.Enabled = User._Object.Privilege._AddJob
            objbtnAddJobDept.Enabled = User._Object.Privilege._AddDepartment
            objbtnAddPayType.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddQualification.Enabled = User._Object.Privilege._AddQualification_Course
            objbtnAddEmployment.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddShiftType.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAddSkills.Enabled = User._Object.Privilege._AddSkills
            objbtnAdvertiseCategory.Enabled = User._Object.Privilege._AddCommonMasters
            objbtnAdvertiser.Enabled = User._Object.Privilege._AddAdvertise
            'Nilay (20 Mar 2017) -- Start
            objbtnAddVacancy.Enabled = User._Object.Privilege._AddCommonMasters
            'Nilay (20 Mar 2017) -- End
            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
            objbtnAddLanguage.Enabled = User._Object.Privilege._AddCommonMasters
            'Sohail (18 Feb 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Other Events "

    ' Vacancy Tab Start
    Private Sub cboJob_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboJob.SelectedValueChanged
        'Removed : , objbtnAddSkills.Click, objbtnAddQualification.Click, objbtnAddLanguage.Click
        'Sohail (18 Feb 2020) - [objbtnAddLanguage]
        Dim objSkill_Tran As New clsJob_Skill_Tran
        Dim objJobQualification As New clsJob_Qualification_Tran
        Dim objSkill_master As New clsskill_master
        'Sohail (18 Feb 2020) -- Start
        'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
        Dim objLanguage_Tran As New clsJob_Language_Tran
        'Sohail (18 Feb 2020) -- End
        Dim lvItem As ListViewItem
        Try
            lvJobSkill.Items.Clear()

            objSkill_Tran._JobUnkid = CInt(cboJob.SelectedValue)
            For Each dtRow As DataRow In objSkill_Tran._DataTable.Rows
                lvItem = New ListViewItem
                objSkill_master._Skillunkid = CInt(dtRow.Item("skillunkid"))
                lvItem.Text = objSkill_master._Skillname
                lvJobSkill.Items.Add(lvItem)
            Next

            If lvJobSkill.Items.Count > 3 Then
                objColhJobSkills.Width = 200 - 18
            Else
                objColhJobSkills.Width = 200
            End If

            lvJobQualification.Items.Clear()
            objJobQualification._JobUnkid = CInt(cboJob.SelectedValue)
            For Each dtRow As DataRow In objJobQualification._DataTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = dtRow.Item("Qualification").ToString
                lvJobQualification.Items.Add(lvItem)
            Next

            If lvJobQualification.Items.Count > 3 Then
                objColhJobQualification.Width = 200 - 18
            Else
                objColhJobQualification.Width = 200
            End If

            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
            lvJobLanguage.Items.Clear()
            objLanguage_Tran._JobUnkid = CInt(cboJob.SelectedValue)
            For Each dtRow As DataRow In objLanguage_Tran._DataTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = dtRow.Item("languagename").ToString
                lvJobLanguage.Items.Add(lvItem)
            Next

            If lvJobLanguage.Items.Count > 1 Then
                lvJobLanguage.Width = 195 - 18
            Else
                lvJobLanguage.Width = 195
            End If
            'Sohail (18 Feb 2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJob_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub
    ' Vacancy Tab End

    'Interviewers Tab Start
    Private Sub lvtInterviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvInterviewer.Click

        Try
            If lvInterviewer.SelectedItems.Count > 0 Then
                mintInterviewerIndex = lvInterviewer.SelectedItems(0).Index

                nudLevel.Value = CDec(lvInterviewer.SelectedItems(0).SubItems(colhLevel.Index).Text)

                If CDec(lvInterviewer.SelectedItems(0).SubItems(objcolhInterviewerunkid.Index).Text) <> -1 Then
                    radEmployee.Checked = True
                    radOthers.Checked = False

                    cboEmployee.SelectedValue = lvInterviewer.SelectedItems(0).SubItems(objcolhInterviewerunkid.Index).Text
                    lblEmployeeCompany.Text = lvInterviewer.SelectedItems(0).SubItems(colhCompany.Index).Text
                    lblEmployeeDepartment.Text = lvInterviewer.SelectedItems(0).SubItems(colhDepartment.Index).Text
                    lblEmployeeContactNo.Text = lvInterviewer.SelectedItems(0).SubItems(colhContactNo.Index).Text
                Else
                    radEmployee.Checked = False
                    radOthers.Checked = True
                    txtOtherCompany.Text = lvInterviewer.SelectedItems(0).SubItems(colhCompany.Index).Text
                    txtOtherContactNo.Text = lvInterviewer.SelectedItems(0).SubItems(colhContactNo.Index).Text
                    txtOtherDepartment.Text = lvInterviewer.SelectedItems(0).SubItems(colhDepartment.Index).Text
                    txtOtherInterviewerName.Text = lvInterviewer.SelectedItems(0).SubItems(colhEmpName.Index).Text

                End If
                cboInterviewType.SelectedValue = lvInterviewer.SelectedItems(0).SubItems(objcolhInterviewtypeunkid.Index).Text
                'Sohail (11 Sep 2010) -- Start
            Else
                mintInterviewerIndex = -1
                'Sohail (11 Sep 2010) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvtInterviewer_Click", mstrModuleName)
        End Try

    End Sub
    'Interviewers Tab End

    'Advertisement Tab Start
    Private Sub lvAdvertisement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvAdvertisement.Click

        Try
            If lvAdvertisement.SelectedItems.Count > 0 Then
                mintAdvertiseIndex = lvAdvertisement.SelectedItems(0).Index

                cboAdvertiseCategory.SelectedValue = lvAdvertisement.SelectedItems(0).SubItems(objcolhCategoryunkid.Index).Text
                cboAdvertiser.SelectedValue = lvAdvertisement.SelectedItems(0).SubItems(objcolhAdvertiseunkid.Index).Text
                txtBudget.Text = lvAdvertisement.SelectedItems(0).SubItems(colhBudget.Index).Text
                txtCosting.Text = lvAdvertisement.SelectedItems(0).SubItems(colhCosting.Index).Text
                txtDescription.Text = lvAdvertisement.SelectedItems(0).SubItems(colhDescription.Index).Text
                txtNotes.Text = lvAdvertisement.SelectedItems(0).SubItems(colhNotes.Index).Text

                'Sohail (11 Sep 2010) -- Start
            Else
                mintAdvertiseIndex = -1
                'Sohail (11 Sep 2010) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lblAdvertiser_Click", mstrModuleName)
        End Try
    End Sub
    'Advertisement Tab End

#End Region

#Region " Buttons "

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        'objVacancyData = New clsVacancy

        If dtInterviewStartDate.Value < dtOpeningDate.Value Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Interview start date cannot be less than vacancy Start date"), enMsgBoxStyle.Information)
            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab  'Sohail (11 Sep 2010)
            dtInterviewStartDate.Focus()
            Exit Sub
        End If
        'Anjan (04 Jan 2012)-Start
        'Issue : Interview date can be greater then vacancy dates - Mr.Andrew Suggesstion.
        'If dtInterviewStartDate.Value > dtClosingDate.Value Then
        '    'Sohail (11 Sep 2010) -- Start
        '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Interview start date cannot be greater than vacancy Start date"), enMsgBoxStyle.Information)
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Interview start date cannot be greater than Vacancy Close date"), enMsgBoxStyle.Information)
        '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
        '    'Sohail (11 Sep 2010) -- End
        '    dtInterviewStartDate.Focus()
        '    Exit Sub
        'End If
        'Anjan (04 Jan 2012)-End



        If dtInterviewClosingDate.Value < dtOpeningDate.Value Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Interview close date cannot be less than vacancy Start date."), enMsgBoxStyle.Information) 'Sohail (11 Sep 2010)
            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab  'Sohail (11 Sep 2010)
            dtInterviewClosingDate.Focus()
            Exit Sub
        End If
        'Anjan (04 Jan 2012)-Start
        'Issue : Interview date can be greater then vacancy dates - Mr.Andrew Suggesstion.
        'If dtInterviewClosingDate.Value > dtClosingDate.Value Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 120, "Interview close date cannot be greater than vacancy close date"), enMsgBoxStyle.Information) 'Sohail (11 Sep 2010)
        '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab  'Sohail (11 Sep 2010)
        '    dtInterviewClosingDate.Focus()
        '    Exit Sub
        'End If
        'Anjan (04 Jan 2012)-End

        'Sohail (11 Sep 2010) -- Start


        'S.SANDEEP [ 28 FEB 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If txtVacancyTitle.Text.Trim = "" Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 121, "Sorry, Vacancy Title cannot be blank. Vacancy Title is mandatory information."), enMsgBoxStyle.Information)
        '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
        '    txtVacancyTitle.Focus()
        '    Exit Sub
        If CInt(cboVacancy.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Vacancy Title is mandatory information. Please select Vacancy Title to continue."), enMsgBoxStyle.Information)
            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
            cboVacancy.Focus()
            Exit Sub
            'S.SANDEEP [ 28 FEB 2012 ] -- END
        ElseIf CInt(cboJobDepartment.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please select Job Department. Job Department is mandatory information."), enMsgBoxStyle.Information)
            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
            cboJobDepartment.Focus()
            Exit Sub
        ElseIf CInt(cboJob.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please select Job. Job is mandatory information."), enMsgBoxStyle.Information)
            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
            cboJob.Focus()
            Exit Sub
        ElseIf CInt(cboEmploymentType.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Please select Employment Type. Employment Type is mandatory information."), enMsgBoxStyle.Information)
            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
            cboEmploymentType.Focus()
            Exit Sub

            'Anjan (30 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'ElseIf dtOpeningDate.Value < DateTime.Today.Date AndAlso menAction <> enAction.EDIT_ONE Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 125, "Sorry, Vacancy start date cannot be less than current date."), enMsgBoxStyle.Information)
            '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
            '    dtOpeningDate.Focus()
            '    Exit Sub
            'ElseIf dtClosingDate.Value < dtOpeningDate.Value Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 126, "Sorry, Vacancy close date cannot be less than current date."), enMsgBoxStyle.Information)
            '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
            '    dtClosingDate.Focus()
            '    Exit Sub
            'Anjan (30 Apr 2012)-End 


        ElseIf dtInterviewClosingDate.Value < dtInterviewStartDate.Value Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Interview close date cannot be less than Interview start date."), enMsgBoxStyle.Information)
            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
            dtInterviewClosingDate.Focus()
            Exit Sub
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'ElseIf CInt(cboPayType.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 128, "Please select Pay Type. Pay Type is mandatory information."), enMsgBoxStyle.Information)
            '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
            '    cboPayType.Focus()
            '    Exit Sub
            'S.SANDEEP [ 07 NOV 2011 ] -- END        
        ElseIf CInt(cboShiftType.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Please select Shift Type. Shift Type is mandatory information."), enMsgBoxStyle.Information)
            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
            cboShiftType.Focus()
            Exit Sub
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'ElseIf txtPayRangeFrom.Decimal <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 130, "Sorry, Pay Range From should be greater than zero."), enMsgBoxStyle.Information)
            '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
            '    txtPayRangeFrom.Focus()
            '    Exit Sub
            'ElseIf txtPayRangeTo.Decimal < txtPayRangeFrom.Decimal Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 131, "Sorry, Pay Range To should be greater than Pay Range From."), enMsgBoxStyle.Information)
            '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
            '    txtPayRangeTo.Focus()
            '    Exit Sub
            'S.SANDEEP [ 07 NOV 2011 ] -- END
            'Hemant (27 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
            'ElseIf lvInterviewer.Items.Count = 0 Then
        ElseIf tabcVacancy.TabPages.Contains(tabpInterview) = True AndAlso lvInterviewer.Items.Count = 0 Then
            'Hemant (27 Sep 2019) -- End
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Please add atleast one Interview Detail."), enMsgBoxStyle.Information)
            If tabcVacancy.SelectedIndex <> 1 Then tabcVacancy.SelectedIndex = 1 'Interview Tab
            cboInterviewType.Focus()
            Exit Sub
        End If
        'Sohail (11 Sep 2010) -- End



        Try
            Call SetValue()
            With objVacancyData
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With
            If menAction = enAction.EDIT_ONE Then
                'Sohail (28 May 2014) -- Start
                'Enhancement - Staff Requisition.
                'blnFlag = objVacancyData.Update(mdtInterviewerTran, mdtAdvertiseTran)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objVacancyData.Update(CInt(cboVacancy.Tag), mdtInterviewerTran, mdtAdvertiseTran)
                blnFlag = objVacancyData.Update(CInt(cboVacancy.Tag), ConfigParameter._Object._CurrentDateAndTime, mdtInterviewerTran, mdtAdvertiseTran)
                'Sohail (21 Aug 2015) -- End
                'Sohail (28 May 2014) -- End
            Else
                blnFlag = objVacancyData.Insert(ConfigParameter._Object._CurrentDateAndTime, mdtInterviewerTran, mdtAdvertiseTran)
                'Hemant (13 Sep 2019) -- [ConfigParameter._Object._CurrentDateAndTime]
            End If

            If blnFlag = False And objVacancyData._Message <> "" Then
                eZeeMsgBox.Show(objVacancyData._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objVacancyData = Nothing
                    objVacancyData = New clsVacancy
                    Call GetValue()
                Else
                    mintVacancyunkid = objVacancyData._Vacancyunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnAddPayType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddPayType.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.PAY_TYPE, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.PAY_TYPE, True, "PAYTYP")
                With cboPayType
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("PAYTYP")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddPayType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddJobDept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddJobDept.Click
        Dim frm As New frmDepartment_AddEdit
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objDept As New clsDepartment
                dsList = objDept.getComboList("Dept", True)
                With cboJobDepartment
                    .ValueMember = "departmentunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Dept")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objDept = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddJobDept_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddJob.Click
        Dim frm As New frmJobs_AddEdit
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objJob As New clsJobs
                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue))
                With cboJob
                    .ValueMember = "jobunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Job")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objJob = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddJob_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddEmployment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddEmployment.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EMPLTYP")
                With cboEmploymentType
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("EMPLTYP")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddEmployment_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnAddShiftType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddShiftType.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.SHIFT_TYPE, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "SFTTYP")
                With cboShiftType
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("SFTTYP")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddShiftType_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub cboSections_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSections.SelectedIndexChanged
        Try
            If CInt(cboSections.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objJob As New clsJobs
                'Hemant (03 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
                'dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue))
                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue), _
                                             CInt(cboGrade.SelectedValue), CInt(cboTeams.SelectedValue), , , CInt(cboJobDepartment.SelectedValue), _
                                             CInt(cboClassGroup.SelectedValue), CInt(cboStation.SelectedValue), CInt(cboDepartmentGrp.SelectedValue), _
                                             CInt(cboSectionGroup.SelectedValue), CInt(cboUnitGroup.SelectedValue), CInt(cboClass.SelectedValue))
                RemoveHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                'Hemant (03 Sep 2019) -- End

                With cboJob
                    .ValueMember = "jobunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Job")
                    If menAction = enAction.EDIT_ONE Then
                        .SelectedValue = objVacancyData._Jobunkid
                    Else
                        .SelectedValue = 0
                    End If
                End With
                AddHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged  'Hemant (03 Sep 2019) 
                dsList.Dispose()
                objJob = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSections_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboJobGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJobGroup.SelectedIndexChanged
        Try
            If CInt(cboJobGroup.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objJob As New clsJobs
                'Hemant (03 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
                'dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue))
                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue), _
                                             CInt(cboGrade.SelectedValue), CInt(cboTeams.SelectedValue), , , CInt(cboJobDepartment.SelectedValue), _
                                             CInt(cboClassGroup.SelectedValue), CInt(cboStation.SelectedValue), CInt(cboDepartmentGrp.SelectedValue), _
                                             CInt(cboSectionGroup.SelectedValue), CInt(cboUnitGroup.SelectedValue), CInt(cboClass.SelectedValue))
                RemoveHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                'Hemant (03 Sep 2019) -- End

                With cboJob
                    .ValueMember = "jobunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Job")
                    If menAction = enAction.EDIT_ONE Then
                        .SelectedValue = objVacancyData._Jobunkid
                    Else
                        .SelectedValue = 0
                    End If
                End With
                AddHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged  'Hemant (03 Sep 2019)
                dsList.Dispose()
                objJob = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJobGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboUnits_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnits.SelectedIndexChanged
        Try
            If CInt(cboUnits.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objJob As New clsJobs
                'Hemant (03 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
                'dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue))
                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue), _
                                             CInt(cboGrade.SelectedValue), CInt(cboTeams.SelectedValue), , , CInt(cboJobDepartment.SelectedValue), _
                                             CInt(cboClassGroup.SelectedValue), CInt(cboStation.SelectedValue), CInt(cboDepartmentGrp.SelectedValue), _
                                             CInt(cboSectionGroup.SelectedValue), CInt(cboUnitGroup.SelectedValue), CInt(cboClass.SelectedValue))
                RemoveHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                'Hemant (03 Sep 2019) -- End
                With cboJob
                    .ValueMember = "jobunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Job")
                    If menAction = enAction.EDIT_ONE Then
                        .SelectedValue = objVacancyData._Jobunkid
                    Else
                        .SelectedValue = 0
                    End If
                End With
                AddHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged  'Hemant (03 Sep 2019)
                dsList.Dispose()
                objJob = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUnits_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchJobs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJobs.Click

        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboJob.ValueMember
                .DisplayMember = cboJob.DisplayMember
                .CodeMember = ""
                .DataSource = CType(cboJob.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboJob.SelectedValue = frm.SelectedValue
                cboJob.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJobs_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchVacancy.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboVacancy.ValueMember
                .DisplayMember = cboVacancy.DisplayMember
                .DataSource = CType(cboVacancy.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboVacancy.SelectedValue = frm.SelectedValue
                cboVacancy.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Nilay (20 Mar 2017) -- Start
    Private Sub objbtnAddVacancy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddVacancy.Click
        Dim frm As New frmCommonMaster
        Dim intRefId As Integer = -1
        Try
            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.VACANCY_MASTER, enAction.ADD_ONE)
            If intRefId > 0 Then
                Dim dsList As New DataSet
                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.VACANCY_MASTER, True, "Vacancy")
                With cboVacancy
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Vacancy")
                    .SelectedValue = intRefId
                End With
                dsList.Dispose()
                objCMaster = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddVacancy_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Nilay (20 Mar 2017) -- End


#End Region

#Region " Form Events "
    'Sohail (11 Sep 2010) -- Start
    Private Sub frmVacancy_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                Case Keys.S
                    If e.Control = True Then
                        Call btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancy_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 Sep 2010) -- End

    Private Sub frmVacancy_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        radEmployee.Checked = True
        objVacancyData = New clsVacancy
        objInterviewerTran = New clsinterviewer_tran
        objAdvertiseTran = New clsAdvertise_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            'Sohail (11 Sep 2010) -- Start
            Call SetColor()
            'Sohail (11 Sep 2010) -- End
            'Hemant (06 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Hemant (06 Aug 2019) -- End
            If menAction = enAction.EDIT_ONE Then
                objVacancyData._Vacancyunkid = mintVacancyunkid

            End If
            Call VacancyFillCombo()
            Call InterviewerFillCombo()
            Call AdvertiseFillCombo()
            lblLevelInfo.Text = Language.getMessage(mstrModuleName, 13, "Level 1 means lower Level.")
            'Hemant (27 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 12 : On batch scheduling, system should have a functionality to indicate the interview panel members)
            tabcVacancy.TabPages.Remove(tabpInterview)
            'Hemant (27 Sep 2019) -- End

            GetValue()

            objInterviewerTran._VacancyUnkid = mintVacancyunkid
            mdtInterviewerTran = objInterviewerTran._DataTable
            Call FillInterviewersList()

            objAdvertiseTran._VacancyUnkid = mintVacancyunkid
            mdtAdvertiseTran = objAdvertiseTran._DataTable
            Call FillAdvertiseList()

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 25 DEC 2011 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmVacancy_AddEdit_Load", mstrModuleName)
        End Try


    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsVacancy.SetMessages()
            objfrm._Other_ModuleNames = "clsVacancy"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Tab Buttons "

#Region " Vacancy "
    Private Sub objbtnAddSkills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSkills.Click
        'Sohail (11 Sep 2010) -- Start
        If CInt(cboJob.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Please select Job."), enMsgBoxStyle.Information)
            cboJob.Focus()
            Exit Sub
        End If
        'Sohail (11 Sep 2010) -- End
        Try
            Dim objfrmJob As New frmJobs_AddEdit
            objfrmJob.tabcJobInfo.SelectTab(objfrmJob.tabpJobSkills) 'Sohail (18 Feb 2020)
            objfrmJob.displayDialog(CInt(cboJob.SelectedValue), enAction.EDIT_ONE)
            Call cboJob_SelectedValueChanged(cboJob, New System.EventArgs) 'Sohail (18 Feb 2020)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddSkills_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub objbtnAddQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddQualification.Click
        Try
            If CInt(cboJob.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Please select Job."), enMsgBoxStyle.Information)
                cboJob.Focus()
                Exit Sub
            End If
            Dim objfrmJob As New frmJobs_AddEdit
            objfrmJob.tabcJobInfo.SelectTab(objfrmJob.tabpQualification) 'Sohail (18 Feb 2020)
            objfrmJob.displayDialog(CInt(cboJob.SelectedValue), enAction.EDIT_ONE)
            Call cboJob_SelectedValueChanged(cboJob, New System.EventArgs) 'Sohail (18 Feb 2020)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddQualification_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 07 NOV 2011 ] -- END

    'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : Add a tab to define the language(s) attached to the job on job master.
    Private Sub objbtnAddLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddLanguage.Click
        If CInt(cboJob.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Please select Job."), enMsgBoxStyle.Information)
            cboJob.Focus()
            Exit Sub
        End If
        Try
            Dim objfrmJob As New frmJobs_AddEdit
            objfrmJob.tabcJobInfo.SelectTab(objfrmJob.tabpJobLanguage)
            objfrmJob.displayDialog(CInt(cboJob.SelectedValue), enAction.EDIT_ONE)
            Call cboJob_SelectedValueChanged(cboJob, New System.EventArgs)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddLanguage_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Feb 2020) -- End

#End Region

#Region " Interview "

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        Try
            If radEmployee.Checked = True Then
                'Sohail (11 Sep 2010) -- Start
                'Dim dtRow As DataRow() = mdtInterviewerTran.Select("interviewerunkid = '" & CInt(cboEmployee.SelectedValue) & "' AND AUD <> 'D' ")
                Dim dtRow As DataRow() = mdtInterviewerTran.Select("interviewerunkid = " & CInt(cboEmployee.SelectedValue) & " AND AUD <> 'D' ")
                'Sohail (11 Sep 2010) -- End

                If dtRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Selected Interviewer is already added to the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

            ElseIf radOthers.Checked = True Then
                Dim dtRow As DataRow() = mdtInterviewerTran.Select("otherinterviewer_name LIKE '" & txtOtherInterviewerName.Text & "%' AND AUD <> 'D' ")

                If dtRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Selected Interviewer is already added to the list."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

            End If

            If txtOtherCompany.Text = Company._Object._Name Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Company cannot be same. Please give another company name."), enMsgBoxStyle.Information)
                txtOtherCompany.Focus()
                Exit Sub
            End If

            'Sohail (11 Sep 2010) -- Start
            If CInt(cboInterviewType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Interview Type."), enMsgBoxStyle.Information)
                cboInterviewType.Focus()
                Exit Sub
            ElseIf radEmployee.Checked = True AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Focus()
                Exit Sub
            ElseIf radOthers.Checked = True AndAlso txtOtherInterviewerName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Enter Interviewer Name."), enMsgBoxStyle.Information)
                txtOtherInterviewerName.Focus()
                Exit Sub
            End If

            'Dim dtLevelRow As DataRow() = mdtInterviewerTran.Select("interviewer_level = '" & CDec(nudLevel.Value) & "' AND AUD <> 'D' ")
            Dim dtLevelRow As DataRow() = mdtInterviewerTran.Select("interviewer_level = " & CDec(nudLevel.Value) & " AND AUD <> 'D' ")
            'Sohail (11 Sep 2010) -- End
            If dtLevelRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected Level is already assigned to Interviewer. Please assign new level."), enMsgBoxStyle.Information)
                nudLevel.Focus()
                Exit Sub
            End If

            Dim dtInterviewerRow As DataRow
            dtInterviewerRow = mdtInterviewerTran.NewRow

            dtInterviewerRow.Item("interviewertranunkid") = -1
            dtInterviewerRow.Item("vacancyunkid") = mintVacancyunkid
            dtInterviewerRow.Item("interviewer_level") = nudLevel.Value
            If radEmployee.Checked = True Then
                dtInterviewerRow.Item("interviewerunkid") = CInt(cboEmployee.SelectedValue)
                dtInterviewerRow.Item("otherinterviewer_name") = ""
                'Sohail (11 Sep 2010) -- Start
                dtInterviewerRow.Item("othercompany") = ""
                dtInterviewerRow.Item("otherdepartment") = ""
                dtInterviewerRow.Item("othercontact_no") = ""
                'Sohail (11 Sep 2010) -- End
            Else
                dtInterviewerRow.Item("interviewerunkid") = CInt(-1)
                dtInterviewerRow.Item("otherinterviewer_name") = txtOtherInterviewerName.Text
                'Sohail (11 Sep 2010) -- Start
                dtInterviewerRow.Item("othercompany") = txtOtherCompany.Text
                dtInterviewerRow.Item("otherdepartment") = txtOtherDepartment.Text
                dtInterviewerRow.Item("othercontact_no") = txtOtherContactNo.Text
            End If
            'dtInterviewerRow.Item("othercompany") = txtOtherCompany.Text
            'dtInterviewerRow.Item("otherdepartment") = txtOtherDepartment.Text
            'dtInterviewerRow.Item("othercontact_no") = txtOtherContactNo.Text
            'Sohail (11 Sep 2010) -- End
            dtInterviewerRow.Item("interviewtypeunkid") = CInt(cboInterviewType.SelectedValue)
            dtInterviewerRow.Item("userunkid") = User._Object._Userunkid
            dtInterviewerRow.Item("isvoid") = False
            dtInterviewerRow.Item("voiduserunkid") = -1
            'dtInterviewerRow.Item("voiddatetime") = DBNull
            dtInterviewerRow.Item("voidreason") = ""
            dtInterviewerRow.Item("GUID") = Guid.NewGuid().ToString
            dtInterviewerRow.Item("AUD") = "A"

            mdtInterviewerTran.Rows.Add(dtInterviewerRow)

            Call FillInterviewersList()
            Call ResetInterviewer()
            cboInterviewType.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        'Sohail (11 Sep 2010) -- Start
        If lvInterviewer.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Interviewer from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvInterviewer.Select()
            Exit Sub
        End If
        If CInt(cboInterviewType.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Interview Type."), enMsgBoxStyle.Information)
            cboInterviewType.Focus()
            Exit Sub
        ElseIf radEmployee.Checked = True AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Employee."), enMsgBoxStyle.Information)
            cboEmployee.Focus()
            Exit Sub
        ElseIf radOthers.Checked = True AndAlso txtOtherInterviewerName.Text.Trim = "" Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Enter Interviewer Name."), enMsgBoxStyle.Information)
            txtOtherInterviewerName.Focus()
            Exit Sub
        End If
        'Sohail (11 Sep 2010) -- End
        Try


            If lvInterviewer.SelectedItems.Count > 0 Then
                If mintInterviewerIndex > -1 Then
                    Dim drtemp As DataRow()
                    If CInt(lvInterviewer.Items(mintInterviewerIndex).Tag) = -1 Then
                        drtemp = mdtInterviewerTran.Select("GUID ='" & lvInterviewer.Items(mintInterviewerIndex).SubItems(objcolhIntGUID.Index).Text & "'")
                    Else
                        'Sohail (11 Sep 2010) -- Start
                        'drtemp = mdtInterviewerTran.Select("interviewertranunkid ='" & CInt(lvInterviewer.SelectedItems(0).Tag) & "'")
                        drtemp = mdtInterviewerTran.Select("interviewertranunkid = " & CInt(lvInterviewer.SelectedItems(0).Tag) & "")
                        'Sohail (11 Sep 2010) -- End
                    End If

                    'Sohail (11 Sep 2010) -- Start
                    'Dim dtLevelRow As DataRow() = mdtInterviewerTran.Select("interviewer_level = '" & CInt(nudLevel.Value) & "' AND AUD <> 'D'")
                    Dim dtLevelRow As DataRow() = mdtInterviewerTran.Select("interviewer_level = " & CInt(nudLevel.Value) & " AND GUID <> '" & lvInterviewer.Items(mintInterviewerIndex).SubItems(objcolhIntGUID.Index).Text & "' AND AUD <> 'D'")
                    'Sohail (11 Sep 2010) -- End

                    If dtLevelRow.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selected Level is already assigned to Interviewer. Please assign new level."), enMsgBoxStyle.Information)
                        nudLevel.Focus()
                        Exit Sub
                    End If

                    If drtemp.Length > 0 Then
                        With drtemp(0)
                            .Item("interviewertranunkid") = lvInterviewer.Items(mintInterviewerIndex).Tag
                            .Item("vacancyunkid") = mintVacancyunkid
                            .Item("interviewer_level") = nudLevel.Value
                            .Item("interviewtypeunkid") = CInt(cboInterviewType.SelectedValue)
                            If radEmployee.Checked = True Then
                                .Item("interviewerunkid") = CInt(cboEmployee.SelectedValue)
                            Else
                                .Item("interviewerunkid") = CInt(-1)
                                .Item("otherinterviewer_name") = txtOtherInterviewerName.Text
                                .Item("othercompany") = txtOtherCompany.Text
                                .Item("otherdepartment") = txtOtherDepartment.Text
                                .Item("othercontact_no") = txtOtherContactNo.Text
                            End If
                            .Item("userunkid") = 1
                            .Item("GUID") = Guid.NewGuid().ToString
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")) = "" Then
                                .Item("AUD") = "U"
                            End If
                            .AcceptChanges()
                        End With
                        Call FillInterviewersList()
                    End If

                End If
                Call ResetInterviewer()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Sohail (11 Sep 2010) -- Start
        If lvInterviewer.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Interviewer from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvInterviewer.Select()
            Exit Sub
        End If
        'Sohail (11 Sep 2010) -- End
        Try

            If mintInterviewerIndex > -1 Then
                'Sohail (11 Sep 2010) -- Start
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Are you sure you want to delete this Interviewer?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                End If
                'Sohail (11 Sep 2010) -- End

                Dim drTemp As DataRow()
                If CInt(lvInterviewer.Items(mintInterviewerIndex).Tag) = -1 Then
                    drTemp = mdtInterviewerTran.Select("GUID = '" & lvInterviewer.Items(mintInterviewerIndex).SubItems(objcolhIntGUID.Index).Text & "'")
                Else
                    'Sohail (11 Sep 2010) -- Start
                    'drTemp = mdtInterviewerTran.Select("interviewertranunkid ='" & CInt(lvInterviewer.SelectedItems(0).Tag) & "'")
                    drTemp = mdtInterviewerTran.Select("interviewertranunkid = " & CInt(lvInterviewer.SelectedItems(0).Tag) & "")
                    'Sohail (11 Sep 2010) -- End
                End If

                If drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    Call FillInterviewersList()
                    Call ResetInterviewer()
                End If
            End If
            If lvInterviewer.Items.Count > 11 Then
                colhEmpName.Width = 110 - 18
            Else
                colhEmpName.Width = 110
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnAddInterviewType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddInterviewType.Click
        Dim objfrm As New frmCommonMaster
        Dim objCommonMaster As New clsCommon_Master
        Dim dsList As DataSet
        Dim intRefId As Integer = -1
        Try

            objfrm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, enAction.ADD_ONE)

            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True, "InterviewType", -1)
            With cboInterviewType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("InterviewType")
                .SelectedValue = intRefId
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddInterviewType_Click", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Advertisement "

    Private Sub btnAdAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdAdd.Click
        'Sohail (11 Sep 2010) -- Start
        If CInt(cboAdvertiseCategory.SelectedValue) <= 0 Then
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 139, "Please select Advertise Category. Advertise Category is mandatory information."), enMsgBoxStyle.Information)
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select Cost Category. Cost Category is mandatory information."), enMsgBoxStyle.Information)
            'S.SANDEEP [ 25 DEC 2011 ] -- END
            cboAdvertiseCategory.Focus()
            Exit Sub
        ElseIf CInt(cboAdvertiser.SelectedValue) <= 0 Then
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 140, "Please select Advertiser. Advertiser is mandatory information."), enMsgBoxStyle.Information)
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Cost. Cost is mandatory information."), enMsgBoxStyle.Information)
            'S.SANDEEP [ 25 DEC 2011 ] -- END
            cboAdvertiser.Focus()
            Exit Sub
        End If

        Dim drRow As DataRow() = mdtAdvertiseTran.Select("advertiserunkid = " & CInt(cboAdvertiser.SelectedValue) & " AND AUD <> 'D' ")
        If drRow.Length > 0 Then
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 143, "Selected Advertiser is already assigned. Please assign new Advertiser."), enMsgBoxStyle.Information)
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Selected Cost is already assigned. Please assign new Cost."), enMsgBoxStyle.Information)
            'S.SANDEEP [ 25 DEC 2011 ] -- END
            cboAdvertiser.Focus()
            Exit Sub
        End If
        'Sohail (11 Sep 2010) -- End
        Try

            Dim dtAdvertiseRow As DataRow

            dtAdvertiseRow = mdtAdvertiseTran.NewRow

            dtAdvertiseRow.Item("advertisetranunkid") = -1
            dtAdvertiseRow.Item("vacancyunkid") = mintVacancyunkid


            dtAdvertiseRow.Item("advertisecategoryunkid") = CInt(cboAdvertiseCategory.SelectedValue)

            dtAdvertiseRow.Item("advertiserunkid") = CInt(cboAdvertiser.SelectedValue)
            'Sohail (11 Sep 2010) -- Start (errro occurs if txtcost OR txtbudget is blank
            'dtAdvertiseRow.Item("costing") = txtCosting.Text
            'dtAdvertiseRow.Item("budget") = txtBudget.Text

            'Anjan (11 May 2011)-Start
            'dtAdvertiseRow.Item("costing") = txtCosting.Decimal
            'dtAdvertiseRow.Item("budget") = txtBudget.Decimal
            dtAdvertiseRow.Item("costing") = txtCosting.Decimal
            dtAdvertiseRow.Item("budget") = txtBudget.Decimal
            'Anjan (11 May 2011)-End 



            'Sohail (11 Sep 2010) -- End
            dtAdvertiseRow.Item("description") = txtDescription.Text
            dtAdvertiseRow.Item("notes") = txtNotes.Text
            dtAdvertiseRow.Item("userunkid") = 1
            dtAdvertiseRow.Item("isvoid") = False
            dtAdvertiseRow.Item("userunkid") = User._Object._Userunkid
            dtAdvertiseRow.Item("voiduserunkid") = -1
            'dtInterviewerRow.Item("voiddatetime") = DBNull
            dtAdvertiseRow.Item("voidreason") = ""
            dtAdvertiseRow.Item("GUID") = Guid.NewGuid().ToString
            dtAdvertiseRow.Item("AUD") = "A"

            mdtAdvertiseTran.Rows.Add(dtAdvertiseRow)

            Call FillAdvertiseList()
            Call ResetAdvertise()
            cboAdvertiseCategory.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdAdd_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnAdEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdEdit.Click
        'Sohail (11 Sep 2010) -- Start
        If lvAdvertisement.SelectedItems.Count < 1 Then
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 141, "Please select Advertisement from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please select Costing from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            'S.SANDEEP [ 25 DEC 2011 ] -- END
            lvAdvertisement.Select()
            Exit Sub
        End If
        If CInt(cboAdvertiseCategory.SelectedValue) <= 0 Then
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 139, "Please select Advertise Category. Advertise Category is mandatory information."), enMsgBoxStyle.Information)
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select Cost Category. Cost Category is mandatory information."), enMsgBoxStyle.Information)
            'S.SANDEEP [ 25 DEC 2011 ] -- END
            cboAdvertiseCategory.Focus()
            Exit Sub
        ElseIf CInt(cboAdvertiser.SelectedValue) <= 0 Then
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 140, "Please select Advertiser. Advertiser is mandatory information."), enMsgBoxStyle.Information)
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select Cost. Cost is mandatory information."), enMsgBoxStyle.Information)
            'S.SANDEEP [ 25 DEC 2011 ] -- END
            cboAdvertiser.Focus()
            Exit Sub
        End If
        'Sohail (11 Sep 2010) -- End
        Try

            If lvAdvertisement.SelectedItems.Count > 0 Then
                If mintAdvertiseIndex > -1 Then
                    Dim drTemp As DataRow()

                    Dim drRow As DataRow() = mdtAdvertiseTran.Select("advertiserunkid = " & CInt(cboAdvertiser.SelectedValue) & " AND GUID <> '" & lvAdvertisement.Items(mintAdvertiseIndex).SubItems(objcolhADGUID.Index).Text & "' AND AUD <> 'D' ")
                    If drRow.Length > 0 Then
                        'S.SANDEEP [ 25 DEC 2011 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 143, "Selected Advertiser is already assigned. Please assign new Advertiser."), enMsgBoxStyle.Information)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Selected Cost is already assigned. Please assign new Cost."), enMsgBoxStyle.Information)
                        'S.SANDEEP [ 25 DEC 2011 ] -- END
                        cboAdvertiser.Focus()
                        Exit Sub
                    End If

                    If CInt(lvAdvertisement.Items(mintAdvertiseIndex).Tag) = -1 Then
                        drTemp = mdtAdvertiseTran.Select("GUID ='" & lvAdvertisement.Items(mintAdvertiseIndex).SubItems(objcolhADGUID.Index).Text & "'")
                    Else
                        'Sohail (11 Sep 2010) -- Start
                        'drTemp = mdtAdvertiseTran.Select("advertisetranunkid ='" & CInt(lvAdvertisement.SelectedItems(0).Tag) & "'")
                        drTemp = mdtAdvertiseTran.Select("advertisetranunkid = " & CInt(lvAdvertisement.SelectedItems(0).Tag) & "")
                        'Sohail (11 Sep 2010) -- End
                    End If
                    If drTemp.Length > 0 Then
                        With drTemp(0)
                            .Item("advertisetranunkid") = lvAdvertisement.Items(mintAdvertiseIndex).Tag
                            .Item("vacancyunkid") = mintVacancyunkid
                            .Item("advertiserunkid") = CInt(cboAdvertiser.SelectedValue)
                            .Item("advertisecategoryunkid") = CInt(cboAdvertiseCategory.SelectedValue)
                            'Sohail (11 Sep 2010) -- Start (errro occurs if txtcost OR txtbudget is blank
                            '.Item("costing") = txtCosting.Text
                            '.Item("budget") = txtBudget.Text

                            'Anjan (11 May 2011)-Start
                            '.Item("costing") = txtCosting.Decimal
                            '.Item("budget") = txtBudget.Decimal
                            .Item("costing") = txtCosting.Decimal
                            .Item("budget") = txtBudget.Decimal
                            'Anjan (11 May 2011)-End 


                            'Sohail (11 Sep 2010) -- End
                            .Item("description") = txtDescription.Text
                            .Item("notes") = txtNotes.Text

                            .Item("userunkid") = User._Object._Userunkid
                            .Item("GUID") = Guid.NewGuid().ToString
                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")) = "" Then
                                .Item("AUD") = "U"
                            End If
                            .AcceptChanges()
                        End With
                        Call FillAdvertiseList()
                    End If
                End If
                Call ResetAdvertise()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    Private Sub btnAdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdDelete.Click
        'Sohail (11 Sep 2010) -- Start
        If lvAdvertisement.SelectedItems.Count < 1 Then
            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 141, "Please select Advertisement from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Please select Costing from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            'S.SANDEEP [ 25 DEC 2011 ] -- END
            lvAdvertisement.Select()
            Exit Sub
        End If
        'Sohail (11 Sep 2010) -- End
        Try

            If mintAdvertiseIndex > -1 Then
                'Sohail (11 Sep 2010) -- Start

                'S.SANDEEP [ 25 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 142, "Are you sure you want to delete this Advertisement detail?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Are you sure you want to delete this Costing detail?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    'S.SANDEEP [ 25 DEC 2011 ] -- END
                    Exit Sub
                End If
                'Sohail (11 Sep 2010) -- End

                Dim drtemp As DataRow()
                If CInt(lvAdvertisement.Items(mintAdvertiseIndex).Tag) = -1 Then
                    drtemp = mdtAdvertiseTran.Select("GUID = '" & lvAdvertisement.Items(mintAdvertiseIndex).SubItems(objcolhADGUID.Index).Text & "'")
                Else
                    'Sohail (11 Sep 2010) -- Start
                    'drTemp = mdtAdvertiseTran.Select("advertisetranunkid ='" & CInt(lvAdvertisement.SelectedItems(0).Tag) & "'")
                    drtemp = mdtAdvertiseTran.Select("advertisetranunkid = " & CInt(lvAdvertisement.SelectedItems(0).Tag) & "")
                    'Sohail (11 Sep 2010) -- End
                End If

                If drtemp.Length > 0 Then
                    drtemp(0).Item("AUD") = "D"
                    Call FillAdvertiseList()
                    Call ResetAdvertise()
                End If
            End If
            If lvAdvertisement.Items.Count > 11 Then
                colhAdvertiserName.Width = 110 - 18
            Else
                colhAdvertiserName.Width = 110
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnAdvertiseCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvertiseCategory.Click
        Dim objfrm As New frmCommonMaster
        Dim objCommonMaster As New clsCommon_Master
        Dim dsList As DataSet
        Dim intRefId As Integer = -1

        Try
            objfrm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY, enAction.ADD_ONE)
            If intRefId > 0 Then
                dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY, True, "AdvertiseCategory")
                With cboAdvertiseCategory
                    .ValueMember = "masterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("AdvertiseCategory")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvertiseCategory_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnAdvertiser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvertiser.Click
        Dim objfrmAdvertiser As New frmAdvertise_AddEdit
        Dim objAdvertiser As New clsAdvertise_master
        Dim dsList As DataSet
        Dim intRefid As Integer = -1
        Try
            objfrmAdvertiser.displayDialog(intRefid, enAction.ADD_ONE)
            If intRefid > 0 Then
                dsList = objAdvertiser.getComboList(True, "Advertiser")
                With cboAdvertiser
                    .ValueMember = "advertiseunkid"
                    .DisplayMember = "company"
                    .DataSource = dsList.Tables("Advertiser")
                    .SelectedValue = intRefid
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvertiser_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#End Region

#Region " Tab Other Control Events "

    'Vacancy Start
    Private Sub radEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEmployee.CheckedChanged, radOthers.CheckedChanged
        If radEmployee.Checked = True Then
            pnlOthers.Visible = False
        ElseIf radOthers.Checked = True Then
            pnlOthers.Visible = True
        End If
    End Sub
    'Vacancny End

    ' Advertise Start
    Private Sub cboAdvertiseCategory_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAdvertiseCategory.SelectedValueChanged
        Dim dsCombo As New DataSet
        Dim objadvertise As New clsAdvertise_master

        Try
            'If CInt(cboAdvertiseCategory.SelectedValue) > 0 Then 'Sohail (11 Sep 2010) 'To set selected value=0 if no adv. category is selected.
            dsCombo = objadvertise.getComboList(True, "Advertiser", CInt(cboAdvertiseCategory.SelectedValue))
            With cboAdvertiser
                .ValueMember = "advertiseunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Advertiser")
                .SelectedValue = 0 'Sohail (11 Sep 2010)
            End With
            'End If 'Sohail (11 Sep 2010)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAdvertiseCategory_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub
    'Advertise  End

    'S.SANDEEP [ 21 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboStation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStation.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.DEPARTMENT_GROUP)) = True Then
                Dim objDeptGrp As New clsDepartmentGroup
                Dim dsCombos As New DataSet

                dsCombos = objDeptGrp.getComboList(CInt(cboStation.SelectedValue), "DeptGrp", True)
                With cboDepartmentGrp
                    .ValueMember = "deptgroupunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("DeptGrp")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboStation_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDepartmentGrp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepartmentGrp.SelectedIndexChanged
        Try
            If ConfigParameter._Object._IsAllocation_Hierarchy_Set = True AndAlso ConfigParameter._Object._Allocation_Hierarchy.Contains(CStr(enAllocation.DEPARTMENT)) = True Then
                Dim objDepartment As New clsDepartment
                Dim dsCombos As New DataSet

                dsCombos = objDepartment.getComboList(CInt(cboDepartmentGrp.SelectedValue), "Department", True)
                With cboJobDepartment
                    .ValueMember = "departmentunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Department")
                End With
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboDepartmentGrp_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 21 SEP 2012 ] -- END

#End Region

#Region " ComboBox's Events "

    Private Sub cboEmployee_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedValueChanged
        Dim objEmp As New clsEmployee_Master
        Dim objDept As New clsDepartment
        Try
            objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)
            objDept._Departmentunkid = objEmp._Departmentunkid
            lblEmployeeDepartment.Text = objDept._Name
            lblEmployeeCompany.Text = Company._Object._Name
            lblEmployeeContactNo.Text = objEmployeeData._Present_Tel_No
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedValueChanged", mstrModuleName)
        Finally
            objEmp = Nothing
            objDept = Nothing
        End Try
    End Sub

    'S.SANDEEP [ 28 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub cboGradeGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGradeGroup.SelectedIndexChanged
        Try
            If CInt(cboGradeGroup.SelectedValue) > 0 Then
                Dim dsGrade As New DataSet
                Dim objGrade As New clsGrade
                dsGrade = objGrade.getComboList("List", True, CInt(cboGradeGroup.SelectedValue))
                With cboGrade
                    .ValueMember = "gradeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsGrade.Tables("List")
                    .SelectedValue = 0
                End With
            Else
                cboGrade.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGradeGroup_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSeachGradeGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSeachGradeGroup.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboGradeGroup.ValueMember
                .DisplayMember = cboGradeGroup.DisplayMember
                .CodeMember = ""
                .DataSource = CType(cboGradeGroup.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboGradeGroup.SelectedValue = frm.SelectedValue
                cboGradeGroup.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSeachGradeGroup_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSeachGrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSeachGrade.Click
        Dim frm As New frmCommonSearch
        Try
            If cboGrade.DataSource Is Nothing Then Exit Sub

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboGrade.ValueMember
                .DisplayMember = cboGrade.DisplayMember
                .CodeMember = ""
                .DataSource = CType(cboGrade.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboGrade.SelectedValue = frm.SelectedValue
                cboGrade.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSeachGrade_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 28 FEB 2012 ] -- END

    'Hemant (03 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 22 : On vacancy master screen, provide all allocations – TC005).
    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objGradeLevel As New clsGradeLevel
        Try
            If CInt(cboGrade.SelectedValue) > 0 Then
                dsCombos = objGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
                With cboLevel
                    .ValueMember = "gradelevelunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("GradeLevel")
                End With
                
            Else
                cboLevel.DataSource = Nothing
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboJob_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJob.SelectedIndexChanged
        Dim objJobs As New clsJobs
        Dim objGrade As New clsGrade
        Try
            If CInt(cboJob.SelectedValue) > 0 Then
                objJobs._Jobunkid = CInt(cboJob.SelectedValue)
                cboStation.SelectedValue = objJobs._JobBranchUnkid
                cboDepartmentGrp.SelectedValue = objJobs._DepartmentGrpUnkId
                cboJobDepartment.SelectedValue = objJobs._JobDepartmentunkid
                'Hemant (24 Sep 2019) -- Start
                'cboSectionGroup.SelectedValue = objJobs._SectionGrpUnkId
                RemoveHandler cboSectionGroup.SelectedIndexChanged, AddressOf cboSectionGroup_SelectedIndexChanged
                cboSectionGroup.SelectedValue = objJobs._SectionGrpUnkId
                AddHandler cboSectionGroup.SelectedIndexChanged, AddressOf cboSectionGroup_SelectedIndexChanged
                'Hemant (24 Sep 2019) -- End
                RemoveHandler cboSections.SelectedIndexChanged, AddressOf cboSections_SelectedIndexChanged
                cboSections.SelectedValue = objJobs._Jobsectionunkid
                AddHandler cboSections.SelectedIndexChanged, AddressOf cboSections_SelectedIndexChanged
                'Hemant (24 Sep 2019) -- Start
                'cboUnitGroup.SelectedValue = objJobs._UnitGrpUnkId
                RemoveHandler cboUnitGroup.SelectedIndexChanged, AddressOf cboUnitGroup_SelectedIndexChanged
                cboUnitGroup.SelectedValue = objJobs._UnitGrpUnkId
                AddHandler cboUnitGroup.SelectedIndexChanged, AddressOf cboUnitGroup_SelectedIndexChanged
                'Hemant (24 Sep 2019) -- End
                RemoveHandler cboUnits.SelectedIndexChanged, AddressOf cboUnits_SelectedIndexChanged
                cboUnits.SelectedValue = objJobs._Jobunitunkid
                AddHandler cboUnits.SelectedIndexChanged, AddressOf cboUnits_SelectedIndexChanged
                'Hemant (24 Sep 2019) -- Start
                'cboTeams.SelectedValue = objJobs._Teamunkid
                'cboClassGroup.SelectedValue = objJobs._JobClassGroupunkid
                'cboClass.SelectedValue = objJobs._ClassUnkid
                RemoveHandler cboTeams.SelectedIndexChanged, AddressOf cboTeams_SelectedIndexChanged
                cboTeams.SelectedValue = objJobs._Teamunkid
                AddHandler cboTeams.SelectedIndexChanged, AddressOf cboTeams_SelectedIndexChanged
                RemoveHandler cboClassGroup.SelectedIndexChanged, AddressOf cboClassGroup_SelectedIndexChanged
                cboClassGroup.SelectedValue = objJobs._JobClassGroupunkid
                AddHandler cboClassGroup.SelectedIndexChanged, AddressOf cboClassGroup_SelectedIndexChanged
                RemoveHandler cboClass.SelectedIndexChanged, AddressOf cboClass_SelectedIndexChanged
                cboClass.SelectedValue = objJobs._ClassUnkid
                AddHandler cboClass.SelectedIndexChanged, AddressOf cboClass_SelectedIndexChanged
                'Hemant (24 Sep 2019) -- End
                objGrade._Gradeunkid = objJobs._Jobgradeunkid
                cboGradeGroup.SelectedValue = objGrade._Gradegroupunkid
                cboGrade.SelectedValue = objJobs._Jobgradeunkid
                RemoveHandler cboJobGroup.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
                cboJobGroup.SelectedValue = objJobs._Jobgroupunkid
                AddHandler cboJobGroup.SelectedIndexChanged, AddressOf cboJobGroup_SelectedIndexChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJob_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSectionGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSectionGroup.SelectedIndexChanged
        Try
            If CInt(cboSectionGroup.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objJob As New clsJobs
                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue), _
                                             CInt(cboGrade.SelectedValue), CInt(cboTeams.SelectedValue), , , CInt(cboJobDepartment.SelectedValue), _
                                             CInt(cboClassGroup.SelectedValue), CInt(cboStation.SelectedValue), CInt(cboDepartmentGrp.SelectedValue), _
                                             CInt(cboSectionGroup.SelectedValue), CInt(cboUnitGroup.SelectedValue), CInt(cboClass.SelectedValue))
                RemoveHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                With cboJob
                    .ValueMember = "jobunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Job")
                    If menAction = enAction.EDIT_ONE Then
                        .SelectedValue = objVacancyData._Jobunkid
                    Else
                        .SelectedValue = 0
                    End If
                End With
                AddHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                dsList.Dispose()
                objJob = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSectionGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboUnitGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnitGroup.SelectedIndexChanged
        Try
            If CInt(cboUnitGroup.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objJob As New clsJobs
                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue), _
                                             CInt(cboGrade.SelectedValue), CInt(cboTeams.SelectedValue), , , CInt(cboJobDepartment.SelectedValue), _
                                             CInt(cboClassGroup.SelectedValue), CInt(cboStation.SelectedValue), CInt(cboDepartmentGrp.SelectedValue), _
                                             CInt(cboSectionGroup.SelectedValue), CInt(cboUnitGroup.SelectedValue), CInt(cboClass.SelectedValue))
                RemoveHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                With cboJob
                    .ValueMember = "jobunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Job")
                    If menAction = enAction.EDIT_ONE Then
                        .SelectedValue = objVacancyData._Jobunkid
                    Else
                        .SelectedValue = 0
                    End If
                End With
                AddHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                dsList.Dispose()
                objJob = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUnitGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTeams_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTeams.SelectedIndexChanged
        Try
            If CInt(cboTeams.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objJob As New clsJobs
                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue), _
                                             CInt(cboGrade.SelectedValue), CInt(cboTeams.SelectedValue), , , CInt(cboJobDepartment.SelectedValue), _
                                             CInt(cboClassGroup.SelectedValue), CInt(cboStation.SelectedValue), CInt(cboDepartmentGrp.SelectedValue), _
                                             CInt(cboSectionGroup.SelectedValue), CInt(cboUnitGroup.SelectedValue), CInt(cboClass.SelectedValue))
                RemoveHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                With cboJob
                    .ValueMember = "jobunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Job")
                    If menAction = enAction.EDIT_ONE Then
                        .SelectedValue = objVacancyData._Jobunkid
                    Else
                        .SelectedValue = 0
                    End If
                End With
                AddHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                dsList.Dispose()
                objJob = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTeams_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboClassGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboClassGroup.SelectedIndexChanged
        Try
            If CInt(cboClassGroup.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objJob As New clsJobs
                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue), _
                                             CInt(cboGrade.SelectedValue), CInt(cboTeams.SelectedValue), , , CInt(cboJobDepartment.SelectedValue), _
                                             CInt(cboClassGroup.SelectedValue), CInt(cboStation.SelectedValue), CInt(cboDepartmentGrp.SelectedValue), _
                                             CInt(cboSectionGroup.SelectedValue), CInt(cboUnitGroup.SelectedValue), CInt(cboClass.SelectedValue))
                RemoveHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                With cboJob
                    .ValueMember = "jobunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Job")
                    If menAction = enAction.EDIT_ONE Then
                        .SelectedValue = objVacancyData._Jobunkid
                    Else
                        .SelectedValue = 0
                    End If
                End With
                AddHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                dsList.Dispose()
                objJob = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboClassGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboClass_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboClass.SelectedIndexChanged
        Try
            If CInt(cboClass.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objJob As New clsJobs
                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue), _
                                             CInt(cboGrade.SelectedValue), CInt(cboTeams.SelectedValue), , , CInt(cboJobDepartment.SelectedValue), _
                                             CInt(cboClassGroup.SelectedValue), CInt(cboStation.SelectedValue), CInt(cboDepartmentGrp.SelectedValue), _
                                             CInt(cboSectionGroup.SelectedValue), CInt(cboUnitGroup.SelectedValue), CInt(cboClass.SelectedValue))
                RemoveHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                With cboJob
                    .ValueMember = "jobunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Job")
                    If menAction = enAction.EDIT_ONE Then
                        .SelectedValue = objVacancyData._Jobunkid
                    Else
                        .SelectedValue = 0
                    End If
                End With
                AddHandler cboJob.SelectedIndexChanged, AddressOf cboJob_SelectedIndexChanged
                dsList.Dispose()
                objJob = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboClass_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (03 Sep 2019) -- End

#End Region


    'Anjan (17 Apr 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dsList As DataSet
        Try

            objEmployeeData = New clsEmployee_Master
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployeeData.GetEmployeeList("List", False)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployeeData.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployeeData.GetEmployeeList("List", True)
            'End If

            dsList = objEmployeeData.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END


            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                objfrm.DataSource = dsList.Tables("List")
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployeeData = Nothing

        End Try
    End Sub
    'Anjan (17 Apr 2012)-End 



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbSkill.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSkill.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbJob.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbJob.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbInterviewer.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInterviewer.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbAdvertisementAnalysis.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAdvertisementAnalysis.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbJobQualification.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbJobQualification.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.gbLanguage.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLanguage.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdAdd.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.tabpJobInfo.Text = Language._Object.getCaption(Me.tabpJobInfo.Name, Me.tabpJobInfo.Text)
            Me.gbSkill.Text = Language._Object.getCaption(Me.gbSkill.Name, Me.gbSkill.Text)
            Me.gbJob.Text = Language._Object.getCaption(Me.gbJob.Name, Me.gbJob.Text)
            Me.EZeeLine1.Text = Language._Object.getCaption(Me.EZeeLine1.Name, Me.EZeeLine1.Text)
            Me.lblShiftType.Text = Language._Object.getCaption(Me.lblShiftType.Name, Me.lblShiftType.Text)
            Me.lblEmploymentType.Text = Language._Object.getCaption(Me.lblEmploymentType.Name, Me.lblEmploymentType.Text)
            Me.lblPayRangeTo.Text = Language._Object.getCaption(Me.lblPayRangeTo.Name, Me.lblPayRangeTo.Text)
            Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.Name, Me.lblJobGroup.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblPayRangeFrom.Text = Language._Object.getCaption(Me.lblPayRangeFrom.Name, Me.lblPayRangeFrom.Text)
            Me.lblWorkExp.Text = Language._Object.getCaption(Me.lblWorkExp.Name, Me.lblWorkExp.Text)
            Me.lblPayType.Text = Language._Object.getCaption(Me.lblPayType.Name, Me.lblPayType.Text)
            Me.lblInterviewCloseDate.Text = Language._Object.getCaption(Me.lblInterviewCloseDate.Name, Me.lblInterviewCloseDate.Text)
            Me.lblInterviewStartDate.Text = Language._Object.getCaption(Me.lblInterviewStartDate.Name, Me.lblInterviewStartDate.Text)
            Me.lblClosingDate.Text = Language._Object.getCaption(Me.lblClosingDate.Name, Me.lblClosingDate.Text)
            Me.lblJobOpenDate.Text = Language._Object.getCaption(Me.lblJobOpenDate.Name, Me.lblJobOpenDate.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.Name, Me.lblDepartmentGroup.Text)
            Me.lblUnits.Text = Language._Object.getCaption(Me.lblUnits.Name, Me.lblUnits.Text)
            Me.lblNoofPosition.Text = Language._Object.getCaption(Me.lblNoofPosition.Name, Me.lblNoofPosition.Text)
            Me.lblJobDept.Text = Language._Object.getCaption(Me.lblJobDept.Name, Me.lblJobDept.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.tabpInterview.Text = Language._Object.getCaption(Me.tabpInterview.Name, Me.tabpInterview.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.colhEmpName.Text = Language._Object.getCaption(CStr(Me.colhEmpName.Tag), Me.colhEmpName.Text)
            Me.colhCompany.Text = Language._Object.getCaption(CStr(Me.colhCompany.Tag), Me.colhCompany.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.colhContactNo.Text = Language._Object.getCaption(CStr(Me.colhContactNo.Tag), Me.colhContactNo.Text)
            Me.colhInterviewType.Text = Language._Object.getCaption(CStr(Me.colhInterviewType.Tag), Me.colhInterviewType.Text)
            Me.gbInterviewer.Text = Language._Object.getCaption(Me.gbInterviewer.Name, Me.gbInterviewer.Text)
            Me.lblInterViewType.Text = Language._Object.getCaption(Me.lblInterViewType.Name, Me.lblInterViewType.Text)
            Me.lblTrainerContactNo.Text = Language._Object.getCaption(Me.lblTrainerContactNo.Name, Me.lblTrainerContactNo.Text)
            Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
            Me.lblOtherDepartment.Text = Language._Object.getCaption(Me.lblOtherDepartment.Name, Me.lblOtherDepartment.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.radEmployee.Text = Language._Object.getCaption(Me.radEmployee.Name, Me.radEmployee.Text)
            Me.lblEmployeeDepartment.Text = Language._Object.getCaption(Me.lblEmployeeDepartment.Name, Me.lblEmployeeDepartment.Text)
            Me.radOthers.Text = Language._Object.getCaption(Me.radOthers.Name, Me.radOthers.Text)
            Me.lblEmpContact.Text = Language._Object.getCaption(Me.lblEmpContact.Name, Me.lblEmpContact.Text)
            Me.lblEmpCompany.Text = Language._Object.getCaption(Me.lblEmpCompany.Name, Me.lblEmpCompany.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblEmployeeContactNo.Text = Language._Object.getCaption(Me.lblEmployeeContactNo.Name, Me.lblEmployeeContactNo.Text)
            Me.lblEmployeeCompany.Text = Language._Object.getCaption(Me.lblEmployeeCompany.Name, Me.lblEmployeeCompany.Text)
            Me.tabpAdvertise.Text = Language._Object.getCaption(Me.tabpAdvertise.Name, Me.tabpAdvertise.Text)
            Me.gbAdvertisementAnalysis.Text = Language._Object.getCaption(Me.gbAdvertisementAnalysis.Name, Me.gbAdvertisementAnalysis.Text)
            Me.lblNotes.Text = Language._Object.getCaption(Me.lblNotes.Name, Me.lblNotes.Text)
            Me.colhAdvertiserName.Text = Language._Object.getCaption(CStr(Me.colhAdvertiserName.Tag), Me.colhAdvertiserName.Text)
            Me.colhCosting.Text = Language._Object.getCaption(CStr(Me.colhCosting.Tag), Me.colhCosting.Text)
            Me.colhBudget.Text = Language._Object.getCaption(CStr(Me.colhBudget.Tag), Me.colhBudget.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)
            Me.colhNotes.Text = Language._Object.getCaption(CStr(Me.colhNotes.Tag), Me.colhNotes.Text)
            Me.lblAgency.Text = Language._Object.getCaption(Me.lblAgency.Name, Me.lblAgency.Text)
            Me.lblBudget.Text = Language._Object.getCaption(Me.lblBudget.Name, Me.lblBudget.Text)
            Me.lblCosting.Text = Language._Object.getCaption(Me.lblCosting.Name, Me.lblCosting.Text)
            Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
            Me.lblAdvertiser.Text = Language._Object.getCaption(Me.lblAdvertiser.Name, Me.lblAdvertiser.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.btnAdDelete.Text = Language._Object.getCaption(Me.btnAdDelete.Name, Me.btnAdDelete.Text)
            Me.btnAdEdit.Text = Language._Object.getCaption(Me.btnAdEdit.Name, Me.btnAdEdit.Text)
            Me.btnAdAdd.Text = Language._Object.getCaption(Me.btnAdAdd.Name, Me.btnAdAdd.Text)
            Me.lblVacancyTitle.Text = Language._Object.getCaption(Me.lblVacancyTitle.Name, Me.lblVacancyTitle.Text)
            Me.EZeeLine3.Text = Language._Object.getCaption(Me.EZeeLine3.Name, Me.EZeeLine3.Text)
            Me.colhLevel.Text = Language._Object.getCaption(CStr(Me.colhLevel.Tag), Me.colhLevel.Text)
            Me.lblLevelInfo.Text = Language._Object.getCaption(Me.lblLevelInfo.Name, Me.lblLevelInfo.Text)
            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
            Me.lblMonths.Text = Language._Object.getCaption(Me.lblMonths.Name, Me.lblMonths.Text)
            Me.gbJobQualification.Text = Language._Object.getCaption(Me.gbJobQualification.Name, Me.gbJobQualification.Text)
            Me.chkIsExternalVacancy.Text = Language._Object.getCaption(Me.chkIsExternalVacancy.Name, Me.chkIsExternalVacancy.Text)
            Me.chkExportToWeb.Text = Language._Object.getCaption(Me.chkExportToWeb.Name, Me.chkExportToWeb.Text)
            Me.tabpDuties.Text = Language._Object.getCaption(Me.tabpDuties.Name, Me.tabpDuties.Text)
            Me.tabpRemark.Text = Language._Object.getCaption(Me.tabpRemark.Name, Me.tabpRemark.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.Name, Me.lblGradeGroup.Text)
            Me.tabpOtherInfo.Text = Language._Object.getCaption(Me.tabpOtherInfo.Name, Me.tabpOtherInfo.Text)
			Me.lblTeam.Text = Language._Object.getCaption(Me.lblTeam.Name, Me.lblTeam.Text)
			Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.Name, Me.lblUnitGroup.Text)
			Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.Name, Me.lblSectionGroup.Text)
			Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
			Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
			Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.Name, Me.lblClassGroup.Text)
			Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
			Me.lblRemarkBold.Text = Language._Object.getCaption(Me.lblRemarkBold.Name, Me.lblRemarkBold.Text)
			Me.lblRemarkItalic.Text = Language._Object.getCaption(Me.lblRemarkItalic.Name, Me.lblRemarkItalic.Text)
			Me.chkIsBothInternalExternal.Text = Language._Object.getCaption(Me.chkIsBothInternalExternal.Name, Me.chkIsBothInternalExternal.Text)
			Me.gbLanguage.Text = Language._Object.getCaption(Me.gbLanguage.Name, Me.gbLanguage.Text)
            'Hemant (13 Sep 2021) -- Start
            'ENHANCEMENT : OLD-446 - New Report - Training Feedback Form.
            Me.tabpOtherParameters.Text = Language._Object.getCaption(Me.tabpOtherParameters.Name, Me.tabpOtherParameters.Text)
            Me.lblOtherWorkingExperience.Text = Language._Object.getCaption(Me.lblOtherWorkingExperience.Name, Me.lblOtherWorkingExperience.Text)
            Me.tabpOtherJobQualification.Text = Language._Object.getCaption(Me.tabpOtherJobQualification.Name, Me.tabpOtherJobQualification.Text)
            Me.tabpOtherJobSkills.Text = Language._Object.getCaption(Me.tabpOtherJobSkills.Name, Me.tabpOtherJobSkills.Text)
            Me.tabpOtherJobLanguage.Text = Language._Object.getCaption(Me.tabpOtherJobLanguage.Name, Me.tabpOtherJobLanguage.Text)
            'Hemant (13 Sep 2021) -- End
        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Selected Interviewer is already added to the list.")
            Language.setMessage(mstrModuleName, 2, "Company cannot be same. Please give another company name.")
            Language.setMessage(mstrModuleName, 3, "Please select Interview Type.")
            Language.setMessage(mstrModuleName, 4, "Please select Employee.")
            Language.setMessage(mstrModuleName, 5, "Please Enter Interviewer Name.")
            Language.setMessage(mstrModuleName, 6, "Selected Level is already assigned to Interviewer. Please assign new level.")
            Language.setMessage(mstrModuleName, 7, "Please select Interviewer from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 8, "Are you sure you want to delete this Interviewer?")
            Language.setMessage(mstrModuleName, 9, "Please select Cost Category. Cost Category is mandatory information.")
            Language.setMessage(mstrModuleName, 10, "Please select Cost. Cost is mandatory information.")
            Language.setMessage(mstrModuleName, 11, "Selected Cost is already assigned. Please assign new Cost.")
            Language.setMessage(mstrModuleName, 12, "Interview start date cannot be less than vacancy Start date")
            Language.setMessage(mstrModuleName, 13, "Level 1 means lower Level.")
            Language.setMessage(mstrModuleName, 14, "Interview close date cannot be less than vacancy Start date.")
            Language.setMessage(mstrModuleName, 15, "Sorry, Vacancy Title is mandatory information. Please select Vacancy Title to continue.")
            Language.setMessage(mstrModuleName, 16, "Please select Job Department. Job Department is mandatory information.")
            Language.setMessage(mstrModuleName, 17, "Please select Job. Job is mandatory information.")
            Language.setMessage(mstrModuleName, 18, "Please select Employment Type. Employment Type is mandatory information.")
            Language.setMessage(mstrModuleName, 19, "Sorry, Interview close date cannot be less than Interview start date.")
            Language.setMessage(mstrModuleName, 20, "Please select Shift Type. Shift Type is mandatory information.")
            Language.setMessage(mstrModuleName, 21, "Please add atleast one Interview Detail.")
            Language.setMessage(mstrModuleName, 22, "Please select Job.")
            Language.setMessage(mstrModuleName, 23, "Please select Costing from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 24, "Are you sure you want to delete this Costing detail?")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
   
    
   
End Class

'Public Class frmVacancy_AddEdit1

'#Region " Private Variables "
'    Private ReadOnly mstrModuleName As String = "frmVacancy_AddEdit"
'    Private menAction As enAction = enAction.ADD_ONE
'    Private mintVacancyunkid As Integer = 0
'    Private objVacancyData As clsVacancy
'    Private mblnCancel As Boolean = True

'    'Interviewer Declaration
'    Private mdtInterviewerTran As DataTable
'    Private mintInterviewerIndex As Integer
'    Private objInterviewerTran As clsinterviewer_tran
'    Private objEmployeeData As clsEmployee_Master
'    Private objDepartmentData As clsDepartment
'    Private objInterviewType As clsCommon_Master

'    'Advertise Declaration 
'    Private objAdvertiseTran As clsAdvertise_tran
'    Private objAdvertise_Master As clsAdvertise_master
'    Private mintAdvertiseIndex As Integer
'    Private mdtAdvertiseTran As DataTable
'#End Region

'#Region " Display Dialog "
'    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean

'        Try

'            mintVacancyunkid = intUnkId

'            menAction = eAction

'            Me.ShowDialog()
'            intUnkId = mintVacancyunkid

'            Return Not mblnCancel

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        End Try

'    End Function
'#End Region

'#Region " Private Methods "
'    Private Sub SetColor()
'        Try
'            '*** Vacancy Info Tab 'Sohail (11 Sep 2010)
'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'txtPayRangeFrom.BackColor = GUI.ColorComp
'            'txtPayRangeTo.BackColor = GUI.ColorComp
'            txtPayRangeFrom.BackColor = GUI.ColorOptional
'            txtPayRangeTo.BackColor = GUI.ColorOptional
'            'S.SANDEEP [ 07 NOV 2011 ] -- END
'            nudPosition.BackColor = GUI.ColorComp
'            cboDepartmentGrp.BackColor = GUI.ColorOptional
'            'Sohail (11 Sep 2010) -- Start
'            'cboJobGroup.BackColor = GUI.ColorComp
'            cboJobGroup.BackColor = GUI.ColorOptional
'            'Sohail (11 Sep 2010) -- End
'            cboEmploymentType.BackColor = GUI.ColorComp
'            cboJob.BackColor = GUI.ColorComp
'            'Sohail (11 Sep 2010) -- Start
'            'cboSections.BackColor = GUI.ColorComp
'            cboSections.BackColor = GUI.ColorOptional
'            'Sohail (11 Sep 2010) -- End
'            cboShiftType.BackColor = GUI.ColorComp
'            'Sohail (11 Sep 2010) -- Start
'            'cboUnits.BackColor = GUI.ColorComp
'            'cboStation.BackColor = GUI.ColorComp
'            cboUnits.BackColor = GUI.ColorOptional
'            cboStation.BackColor = GUI.ColorOptional
'            'Sohail (11 Sep 2010) -- End

'            'Sohail (11 Sep 2010) -- Start
'            txtVacancyTitle.BackColor = GUI.ColorComp
'            cboJobDepartment.BackColor = GUI.ColorComp
'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'cboPayType.BackColor = GUI.ColorComp
'            cboPayType.BackColor = GUI.ColorOptional
'            'S.SANDEEP [ 07 NOV 2011 ] -- END
'            txtWorkingExp.BackColor = GUI.ColorOptional
'            txtResponsibilitiesDuties.BackColor = GUI.ColorOptional
'            txtRemark.BackColor = GUI.ColorOptional

'            '*** Interview Tab
'            cboInterviewType.BackColor = GUI.ColorComp
'            cboEmployee.BackColor = GUI.ColorComp
'            nudLevel.BackColor = GUI.ColorComp
'            txtOtherInterviewerName.BackColor = GUI.ColorComp
'            txtOtherDepartment.BackColor = GUI.ColorOptional
'            txtOtherCompany.BackColor = GUI.ColorOptional
'            txtOtherContactNo.BackColor = GUI.ColorOptional

'            '*** Advertisement Tab
'            cboAdvertiseCategory.BackColor = GUI.ColorComp
'            cboAdvertiser.BackColor = GUI.ColorComp
'            txtCosting.BackColor = GUI.ColorOptional
'            txtBudget.BackColor = GUI.ColorOptional
'            txtNotes.BackColor = GUI.ColorOptional
'            txtDescription.BackColor = GUI.ColorOptional
'            'Sohail (11 Sep 2010) -- End
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub GetValue()
'        Try

'            cboDepartmentGrp.SelectedValue = objVacancyData._Deptgroupunkid
'            cboJobGroup.SelectedValue = objVacancyData._Jobgroupunkid
'            cboEmploymentType.SelectedValue = objVacancyData._Employeementtypeunkid
'            cboJob.SelectedValue = objVacancyData._Jobunkid
'            cboPayType.SelectedValue = objVacancyData._Paytypeunkid
'            cboSections.SelectedValue = objVacancyData._Sectionunkid
'            cboShiftType.SelectedValue = objVacancyData._Shifttypeunkid
'            cboUnits.SelectedValue = objVacancyData._Unitunkid
'            cboStation.SelectedValue = objVacancyData._Stationunkid
'            cboJobDepartment.SelectedValue = objVacancyData._Departmentunkid

'            If objVacancyData._Noofposition = 0 Then
'                nudPosition.Value = nudPosition.Minimum
'            Else
'                nudPosition.Value = objVacancyData._Noofposition
'            End If

'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'txtPayRangeFrom.Text = CStr(objVacancyData._Pay_From)
'            'txtPayRangeTo.Text = CStr(objVacancyData._Pay_To)
'            If objVacancyData._Pay_From > 0 Then
'                txtPayRangeFrom.Text = CStr(objVacancyData._Pay_From)
'            End If
'            If objVacancyData._Pay_To > 0 Then
'                txtPayRangeTo.Text = CStr(objVacancyData._Pay_To)
'            End If
'            'S.SANDEEP [ 07 NOV 2011 ] -- END

'            txtRemark.Text = objVacancyData._Remark
'            txtWorkingExp.Text = CStr(objVacancyData._Experience)
'            txtResponsibilitiesDuties.Text = objVacancyData._Responsibilities_Duties
'            txtVacancyTitle.Text = objVacancyData._Vacancytitle

'            If objVacancyData._Openingdate = Nothing Then
'                dtOpeningDate.Value = Now.Date
'            Else
'                dtOpeningDate.Value = objVacancyData._Openingdate
'            End If

'            If objVacancyData._Closingdate = Nothing Then
'                dtClosingDate.Value = Now.Date
'            Else
'                dtClosingDate.Value = objVacancyData._Closingdate
'            End If
'            If objVacancyData._Interview_Startdate = Nothing Then
'                dtInterviewStartDate.Value = Now.Date
'            Else
'                dtInterviewStartDate.Value = objVacancyData._Interview_Startdate
'            End If

'            If objVacancyData._Interview_Closedate = Nothing Then
'                dtInterviewClosingDate.Value = Now.Date
'            Else
'                dtInterviewClosingDate.Value = objVacancyData._Interview_Closedate
'            End If

'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            chkIsExternalVacancy.Checked = objVacancyData._Is_External_Vacancy
'            'S.SANDEEP [ 25 DEC 2011 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetValue()
'        Try
'            objVacancyData._Vacancytitle = txtVacancyTitle.Text
'            objVacancyData._Deptgroupunkid = CInt(cboDepartmentGrp.SelectedValue)
'            objVacancyData._Jobgroupunkid = CInt(cboJobGroup.SelectedValue)
'            objVacancyData._Employeementtypeunkid = CInt(cboEmploymentType.SelectedValue)
'            objVacancyData._Jobunkid = CInt(cboJob.SelectedValue)
'            objVacancyData._Paytypeunkid = CInt(cboPayType.SelectedValue)
'            objVacancyData._Sectionunkid = CInt(cboSections.SelectedValue)
'            objVacancyData._Shifttypeunkid = CInt(cboShiftType.SelectedValue)
'            objVacancyData._Unitunkid = CInt(cboUnits.SelectedValue)
'            objVacancyData._Stationunkid = CInt(cboStation.SelectedValue)
'            objVacancyData._Departmentunkid = CInt(cboJobDepartment.SelectedValue)

'            objVacancyData._Noofposition = CInt(nudPosition.Value)
'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'objVacancyData._Pay_From = CDec(txtPayRangeFrom.Text)
'            'objVacancyData._Pay_To = CDec(txtPayRangeTo.Text)
'            If txtPayRangeFrom.Text.Trim.Length > 0 Then
'                objVacancyData._Pay_From = CDec(txtPayRangeFrom.Text)
'            Else
'                objVacancyData._Pay_From = 0
'            End If
'            If txtPayRangeTo.Text.Trim.Length > 0 Then
'                objVacancyData._Pay_To = CDec(txtPayRangeTo.Text)
'            Else
'                objVacancyData._Pay_To = 0
'            End If
'            'S.SANDEEP [ 07 NOV 2011 ] -- END
'            objVacancyData._Remark = txtRemark.Text
'            objVacancyData._Experience = CInt(txtWorkingExp.Text)
'            objVacancyData._Responsibilities_Duties = txtResponsibilitiesDuties.Text

'            objVacancyData._Openingdate = dtOpeningDate.Value
'            objVacancyData._Closingdate = dtClosingDate.Value
'            objVacancyData._Interview_Startdate = dtInterviewStartDate.Value
'            objVacancyData._Interview_Closedate = dtInterviewClosingDate.Value

'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objVacancyData._Is_External_Vacancy = chkIsExternalVacancy.Checked
'            'S.SANDEEP [ 25 DEC 2011 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
'        End Try
'    End Sub

'    'Vacancy Tab Start
'    Private Sub VacancyFillCombo()
'        Dim dsCombos As New DataSet
'        Dim objStation As New clsStation
'        Dim objDeptGrp As New clsDepartmentGroup
'        Dim objDepartment As New clsDepartment
'        Dim objSection As New clsSections
'        Dim objUnit As New clsUnits
'        Dim objJobGrp As New clsJobGroup
'        Dim objJob As New clsJobs
'        Dim objGradeGrp As New clsGradeGroup
'        Dim objEmployee As New clsEmployee_Master
'        Dim objClassGrp As New clsClassGroup
'        Dim objClass As New clsClass
'        Dim objCommon As New clsCommon_Master
'        ' Dim objShift As New clsCommon_Master
'        Dim objMaster As New clsMasterData

'        Dim objTranHead As New clsTransactionHead


'        Try
'            dsCombos = objStation.getComboList("Station", True)
'            With cboStation
'                .ValueMember = "stationunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("Station")
'            End With
'            dsCombos = objDeptGrp.getComboList("DeptGrp", True)
'            With cboDepartmentGrp
'                .ValueMember = "deptgroupunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("DeptGrp")
'            End With
'            dsCombos = objDepartment.getComboList("Department", True)
'            With cboJobDepartment
'                .ValueMember = "departmentunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("Department")
'            End With
'            dsCombos = objSection.getComboList("Section", True)
'            With cboSections
'                .ValueMember = "sectionunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("Section")
'            End With
'            dsCombos = objUnit.getComboList("Unit", True)
'            With cboUnits
'                .ValueMember = "unitunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("Unit")
'            End With
'            dsCombos = objJobGrp.getComboList("JobGrp", True)
'            With cboJobGroup
'                .ValueMember = "jobgroupunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("JobGrp")
'            End With
'            dsCombos = objJob.getComboList("Job", True)
'            With cboJob
'                .ValueMember = "jobunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("Job")
'            End With

'            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EmplType")
'            With cboEmploymentType
'                .ValueMember = "masterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("EmplType")
'            End With
'            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.PAY_TYPE, True, "PayType")
'            With cboPayType
'                .ValueMember = "masterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("PayType")
'            End With
'            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "ShiftType")
'            With cboShiftType
'                .ValueMember = "masterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("ShiftType")
'            End With


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub
'    'Vacancy Tab End

'    'Interviewers Tab Start
'    Private Sub InterviewerFillCombo()
'        Dim dsCombos As New DataSet
'        objInterviewType = New clsCommon_Master
'        objEmployeeData = New clsEmployee_Master
'        Try
'            dsCombos = objInterviewType.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True, "InterviewType")
'            With cboInterviewType
'                .ValueMember = "masterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("InterviewType")
'            End With
'            'Sohail (06 Jan 2012) -- Start
'            'TRA - ENHANCEMENT
'            'dsCombos = objEmployeeData.GetEmployeeList("Employee", True, True)
'            If menAction = enAction.EDIT_ONE Then
'                dsCombos = objEmployeeData.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
'            Else
'                dsCombos = objEmployeeData.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
'            End If
'            'Sohail (06 Jan 2012) -- End
'            With cboEmployee
'                .ValueMember = "employeeunkid"
'                .DisplayMember = "employeename"
'                .DataSource = dsCombos.Tables("Employee")
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "InterviewerFillCombo", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub FillInterviewersList()
'        objEmployeeData = New clsEmployee_Master
'        objDepartmentData = New clsDepartment
'        Try
'            lvInterviewer.Items.Clear()

'            Dim lvInterviewerList As ListViewItem
'            For Each dtRow As DataRow In mdtInterviewerTran.Rows
'                objEmployeeData._Employeeunkid = CInt(dtRow.Item("interviewerunkid"))
'                objDepartmentData._Departmentunkid = objEmployeeData._Departmentunkid
'                objInterviewType._Masterunkid = CInt(dtRow.Item("interviewtypeunkid"))
'                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
'                    lvInterviewerList = New ListViewItem
'                    lvInterviewerList.Text = dtRow.Item("interviewer_level").ToString
'                    If CInt(dtRow.Item("interviewerunkid")) > 0 Then
'                        lvInterviewerList.SubItems.Add(objEmployeeData._Firstname + " " + objEmployeeData._Surname)
'                        lvInterviewerList.SubItems.Add(Company._Object._Name)
'                        lvInterviewerList.SubItems.Add(objDepartmentData._Name)
'                        lvInterviewerList.SubItems.Add(objEmployeeData._Present_Tel_No)

'                    Else
'                        lvInterviewerList.SubItems.Add(dtRow.Item("otherinterviewer_name").ToString)
'                        lvInterviewerList.SubItems.Add(dtRow.Item("othercompany").ToString)
'                        lvInterviewerList.SubItems.Add(dtRow.Item("otherdepartment").ToString)
'                        lvInterviewerList.SubItems.Add(dtRow.Item("othercontact_no").ToString)
'                    End If

'                    lvInterviewerList.SubItems.Add(objInterviewType._Name)
'                    lvInterviewerList.SubItems.Add(dtRow.Item("interviewerunkid").ToString)
'                    lvInterviewerList.SubItems.Add(dtRow.Item("GUID").ToString)
'                    lvInterviewerList.SubItems.Add(dtRow.Item("interviewtypeunkid").ToString)
'                    lvInterviewerList.Tag = dtRow.Item("interviewertranunkid")
'                    lvInterviewer.Items.Add(lvInterviewerList)

'                    lvInterviewerList = Nothing

'                End If


'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillInterviewersList", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub ResetInterviewer()

'        Try

'            cboInterviewType.SelectedValue = 0
'            cboEmployee.SelectedValue = 0
'            lblEmployeeCompany.Text = ""
'            lblEmployeeContactNo.Text = ""
'            lblEmployeeDepartment.Text = ""
'            nudLevel.Value = 1

'            txtOtherCompany.Text = ""
'            txtOtherContactNo.Text = ""
'            txtOtherDepartment.Text = ""
'            txtOtherInterviewerName.Text = ""

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try

'    End Sub

'    'Interviewers Tab End

'    'Advertise Tab Start 
'    Private Sub AdvertiseFillCombo()
'        Dim dsCombo As New DataSet
'        Dim objAdvertiseCategory As New clsCommon_Master
'        'Dim objadvertise As New clsAdvertise_master    'Sohail (11 Sep 2010)
'        Try
'            dsCombo = objAdvertiseCategory.getComboList(clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY, True, "AdvertiseCategory")
'            With cboAdvertiseCategory
'                .ValueMember = "masterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombo.Tables("AdvertiseCategory")
'                .SelectedValue = 0 'Sohail (11 Sep 2010)
'            End With
'            'Sohail (11 Sep 2010) -- Start
'            'dsCombo = objadvertise.getComboList(True, "Advertiser")
'            'With cboAdvertiser
'            '    .ValueMember = "advertiseunkid"
'            '    .DisplayMember = "name"
'            '    .DataSource = dsCombo.Tables("Advertiser")
'            '    .SelectedValue = 0
'            'End With
'            'Sohail (11 Sep 2010) -- End
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "AdvertiseFillCombo", mstrModuleName)
'        Finally
'            objAdvertiseCategory = Nothing 'Sohail (11 Sep 2010)
'        End Try

'    End Sub

'    Private Sub FillAdvertiseList()
'        objAdvertise_Master = New clsAdvertise_master
'        Try
'            lvAdvertisement.Items.Clear()

'            Dim lvAdvertiseItem As ListViewItem

'            For Each dtRow As DataRow In mdtAdvertiseTran.Rows
'                objAdvertise_Master._Advertiseunkid = CInt(dtRow.Item("advertiserunkid"))
'                If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
'                    lvAdvertiseItem = New ListViewItem
'                    lvAdvertiseItem.Text = objAdvertise_Master._Company
'                    lvAdvertiseItem.SubItems.Add(dtRow.Item("costing").ToString)
'                    lvAdvertiseItem.SubItems.Add(dtRow.Item("budget").ToString)
'                    lvAdvertiseItem.SubItems.Add(dtRow.Item("description").ToString)
'                    lvAdvertiseItem.SubItems.Add(dtRow.Item("notes").ToString)
'                    lvAdvertiseItem.SubItems.Add(dtRow.Item("advertiserunkid").ToString)
'                    lvAdvertiseItem.SubItems.Add(dtRow.Item("advertisecategoryunkid").ToString)
'                    lvAdvertiseItem.Tag = dtRow.Item("advertisetranunkid")
'                    'Sohail (11 Sep 2010) -- Start
'                    lvAdvertiseItem.SubItems.Add(dtRow.Item("GUID").ToString)
'                    'Sohail (11 Sep 2010) -- End
'                    lvAdvertisement.Items.Add(lvAdvertiseItem)
'                    lvAdvertiseItem = Nothing

'                End If
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillAdvertiseList", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub ResetAdvertise()

'        Try
'            txtBudget.Text = ""
'            txtCosting.Text = ""
'            txtDescription.Text = ""
'            txtNotes.Text = ""
'            cboAdvertiseCategory.SelectedValue = 0
'            cboAdvertiser.SelectedValue = 0

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ResetAdvertise", mstrModuleName)
'        End Try

'    End Sub
'    'Advertise Tab End


'#Region " Other Events "

'    ' Vacancy Tab Start
'    Private Sub cboJob_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboJob.SelectedValueChanged, objbtnAddSkills.Click, objbtnAddQualification.Click
'        Dim objSkill_Tran As New clsJob_Skill_Tran
'        'S.SANDEEP [ 07 NOV 2011 ] -- START
'        'ENHANCEMENT : TRA CHANGES
'        Dim objJobQualification As New clsJob_Qualification_Tran
'        'S.SANDEEP [ 07 NOV 2011 ] -- END

'        Dim objSkill_master As New clsskill_master
'        Dim lvItem As ListViewItem
'        Try
'            lvJobSkill.Items.Clear()

'            objSkill_Tran._JobUnkid = CInt(cboJob.SelectedValue)
'            For Each dtRow As DataRow In objSkill_Tran._DataTable.Rows
'                lvItem = New ListViewItem
'                objSkill_master._Skillunkid = CInt(dtRow.Item("skillunkid"))
'                lvItem.Text = objSkill_master._Skillname
'                lvJobSkill.Items.Add(lvItem)
'            Next

'            If lvJobSkill.Items.Count > 3 Then
'                objColhJobSkills.Width = 200 - 18
'            Else
'                objColhJobSkills.Width = 200
'            End If

'            lvJobQualification.Items.Clear()
'            objJobQualification._JobUnkid = CInt(cboJob.SelectedValue)
'            For Each dtRow As DataRow In objJobQualification._DataTable.Rows
'                lvItem = New ListViewItem
'                lvItem.Text = dtRow.Item("Qualification").ToString
'                lvJobQualification.Items.Add(lvItem)
'            Next

'            If lvJobQualification.Items.Count > 3 Then
'                objColhJobQualification.Width = 200 - 18
'            Else
'                objColhJobQualification.Width = 200
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboJob_SelectedValueChanged", mstrModuleName)
'        End Try
'    End Sub



'    ' Vacancy Tab End

'    'Interviewers Tab Start
'    Private Sub lvtInterviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvInterviewer.Click

'        Try
'            If lvInterviewer.SelectedItems.Count > 0 Then
'                mintInterviewerIndex = lvInterviewer.SelectedItems(0).Index

'                nudLevel.Value = CDec(lvInterviewer.SelectedItems(0).SubItems(colhLevel.Index).Text)

'                If CDec(lvInterviewer.SelectedItems(0).SubItems(objcolhInterviewerunkid.Index).Text) <> -1 Then
'                    radEmployee.Checked = True
'                    radOthers.Checked = False

'                    cboEmployee.SelectedValue = lvInterviewer.SelectedItems(0).SubItems(objcolhInterviewerunkid.Index).Text
'                    lblEmployeeCompany.Text = lvInterviewer.SelectedItems(0).SubItems(colhCompany.Index).Text
'                    lblEmployeeDepartment.Text = lvInterviewer.SelectedItems(0).SubItems(colhDepartment.Index).Text
'                    lblEmployeeContactNo.Text = lvInterviewer.SelectedItems(0).SubItems(colhContactNo.Index).Text
'                Else
'                    radEmployee.Checked = False
'                    radOthers.Checked = True
'                    txtOtherCompany.Text = lvInterviewer.SelectedItems(0).SubItems(colhCompany.Index).Text
'                    txtOtherContactNo.Text = lvInterviewer.SelectedItems(0).SubItems(colhContactNo.Index).Text
'                    txtOtherDepartment.Text = lvInterviewer.SelectedItems(0).SubItems(colhDepartment.Index).Text
'                    txtOtherInterviewerName.Text = lvInterviewer.SelectedItems(0).SubItems(colhEmpName.Index).Text

'                End If
'                cboInterviewType.SelectedValue = lvInterviewer.SelectedItems(0).SubItems(objcolhInterviewtypeunkid.Index).Text
'                'Sohail (11 Sep 2010) -- Start
'            Else
'                mintInterviewerIndex = -1
'                'Sohail (11 Sep 2010) -- End
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvtInterviewer_Click", mstrModuleName)
'        End Try

'    End Sub
'    'Interviewers Tab End

'    'Advertisement Tab Start
'    Private Sub lvAdvertisement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvAdvertisement.Click

'        Try
'            If lvAdvertisement.SelectedItems.Count > 0 Then
'                mintAdvertiseIndex = lvAdvertisement.SelectedItems(0).Index

'                cboAdvertiseCategory.SelectedValue = lvAdvertisement.SelectedItems(0).SubItems(objcolhCategoryunkid.Index).Text
'                cboAdvertiser.SelectedValue = lvAdvertisement.SelectedItems(0).SubItems(objcolhAdvertiseunkid.Index).Text
'                txtBudget.Text = lvAdvertisement.SelectedItems(0).SubItems(colhBudget.Index).Text
'                txtCosting.Text = lvAdvertisement.SelectedItems(0).SubItems(colhCosting.Index).Text
'                txtDescription.Text = lvAdvertisement.SelectedItems(0).SubItems(colhDescription.Index).Text
'                txtNotes.Text = lvAdvertisement.SelectedItems(0).SubItems(colhNotes.Index).Text

'                'Sohail (11 Sep 2010) -- Start
'            Else
'                mintAdvertiseIndex = -1
'                'Sohail (11 Sep 2010) -- End
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lblAdvertiser_Click", mstrModuleName)
'        End Try
'    End Sub
'    'Advertisement Tab End

'#End Region


'    'S.SANDEEP [ 25 DEC 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub SetVisibility()
'        Try
'            objbtnAddInterviewType.Enabled = User._Object.Privilege._AddCommonMasters
'            objbtnAddJob.Enabled = User._Object.Privilege._AddJob
'            objbtnAddJobDept.Enabled = User._Object.Privilege._AddDepartment
'            objbtnAddPayType.Enabled = User._Object.Privilege._AddCommonMasters
'            objbtnAddQualification.Enabled = User._Object.Privilege._AddQualification_Course
'            objbtnAddEmployment.Enabled = User._Object.Privilege._AddCommonMasters
'            objbtnAddShiftType.Enabled = User._Object.Privilege._AddCommonMasters
'            objbtnAddSkills.Enabled = User._Object.Privilege._AddSkills
'            objbtnAdvertiseCategory.Enabled = User._Object.Privilege._AddCommonMasters
'            objbtnAdvertiser.Enabled = User._Object.Privilege._AddAdvertise
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 25 DEC 2011 ] -- END

'#End Region

'#Region " Buttons "
'    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim blnFlag As Boolean = False
'        'objVacancyData = New clsVacancy

'        If dtInterviewStartDate.Value < dtOpeningDate.Value Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Interview start date cannot be less than vacancy Start date"), enMsgBoxStyle.Information)
'            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab  'Sohail (11 Sep 2010)
'            dtInterviewStartDate.Focus()
'            Exit Sub
'        End If
'        'Anjan (04 Jan 2012)-Start
'        'Issue : Interview date can be greater then vacancy dates - Mr.Andrew Suggesstion.
'        'If dtInterviewStartDate.Value > dtClosingDate.Value Then
'        '    'Sohail (11 Sep 2010) -- Start
'        '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Interview start date cannot be greater than vacancy Start date"), enMsgBoxStyle.Information)
'        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Interview start date cannot be greater than Vacancy Close date"), enMsgBoxStyle.Information)
'        '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
'        '    'Sohail (11 Sep 2010) -- End
'        '    dtInterviewStartDate.Focus()
'        '    Exit Sub
'        'End If
'        'Anjan (04 Jan 2012)-End



'        If dtInterviewClosingDate.Value < dtOpeningDate.Value Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 119, "Interview close date cannot be less than vacancy Start date"), enMsgBoxStyle.Information) 'Sohail (11 Sep 2010)
'            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab  'Sohail (11 Sep 2010)
'            dtInterviewClosingDate.Focus()
'            Exit Sub
'        End If
'        'Anjan (04 Jan 2012)-Start
'        'Issue : Interview date can be greater then vacancy dates - Mr.Andrew Suggesstion.
'        'If dtInterviewClosingDate.Value > dtClosingDate.Value Then
'        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 120, "Interview close date cannot be greater than vacancy close date"), enMsgBoxStyle.Information) 'Sohail (11 Sep 2010)
'        '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab  'Sohail (11 Sep 2010)
'        '    dtInterviewClosingDate.Focus()
'        '    Exit Sub
'        'End If
'        'Anjan (04 Jan 2012)-End

'        'Sohail (11 Sep 2010) -- Start
'        If txtVacancyTitle.Text.Trim = "" Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 121, "Sorry, Vacancy Title cannot be blank. Vacancy Title is mandatory information."), enMsgBoxStyle.Information)
'            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
'            txtVacancyTitle.Focus()
'            Exit Sub
'        ElseIf CInt(cboJobDepartment.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 122, "Please select Job Department. Job Department is mandatory information."), enMsgBoxStyle.Information)
'            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
'            cboJobDepartment.Focus()
'            Exit Sub
'        ElseIf CInt(cboJob.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 123, "Please select Job. Job is mandatory information."), enMsgBoxStyle.Information)
'            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
'            cboJob.Focus()
'            Exit Sub
'        ElseIf CInt(cboEmploymentType.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 124, "Please select Employment Type. Employment Type is mandatory information."), enMsgBoxStyle.Information)
'            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
'            cboEmploymentType.Focus()
'            Exit Sub
'        ElseIf dtOpeningDate.Value < DateTime.Today.Date AndAlso menAction <> enAction.EDIT_ONE Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 125, "Sorry, Vacancy start date cannot be less than current date."), enMsgBoxStyle.Information)
'            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
'            dtOpeningDate.Focus()
'            Exit Sub
'        ElseIf dtClosingDate.Value < dtOpeningDate.Value Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 126, "Sorry, Vacancy close date cannot be less than current date."), enMsgBoxStyle.Information)
'            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
'            dtClosingDate.Focus()
'            Exit Sub
'        ElseIf dtInterviewClosingDate.Value < dtInterviewStartDate.Value Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 127, "Sorry, Interview close date cannot be less than Interview start date."), enMsgBoxStyle.Information)
'            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
'            dtInterviewClosingDate.Focus()
'            Exit Sub
'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'ElseIf CInt(cboPayType.SelectedValue) <= 0 Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 128, "Please select Pay Type. Pay Type is mandatory information."), enMsgBoxStyle.Information)
'            '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
'            '    cboPayType.Focus()
'            '    Exit Sub
'            'S.SANDEEP [ 07 NOV 2011 ] -- END        
'        ElseIf CInt(cboShiftType.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 129, "Please select Shift Type. Shift Type is mandatory information."), enMsgBoxStyle.Information)
'            If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
'            cboShiftType.Focus()
'            Exit Sub
'            'S.SANDEEP [ 07 NOV 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'ElseIf txtPayRangeFrom.Decimal <= 0 Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 130, "Sorry, Pay Range From should be greater than zero."), enMsgBoxStyle.Information)
'            '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
'            '    txtPayRangeFrom.Focus()
'            '    Exit Sub
'            'ElseIf txtPayRangeTo.Decimal < txtPayRangeFrom.Decimal Then
'            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 131, "Sorry, Pay Range To should be greater than Pay Range From."), enMsgBoxStyle.Information)
'            '    If tabcVacancy.SelectedIndex <> 0 Then tabcVacancy.SelectedIndex = 0 'Vacancy Info Tab
'            '    txtPayRangeTo.Focus()
'            '    Exit Sub
'            'S.SANDEEP [ 07 NOV 2011 ] -- END
'        ElseIf lvInterviewer.Items.Count = 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 132, "Please add atleast one Interview Detail."), enMsgBoxStyle.Information)
'            If tabcVacancy.SelectedIndex <> 1 Then tabcVacancy.SelectedIndex = 1 'Interview Tab
'            cboInterviewType.Focus()
'            Exit Sub
'        End If
'        'Sohail (11 Sep 2010) -- End



'        Try
'            Call SetValue()

'            If menAction = enAction.EDIT_ONE Then
'                blnFlag = objVacancyData.Update(mdtInterviewerTran, mdtAdvertiseTran)
'            Else
'                blnFlag = objVacancyData.Insert(mdtInterviewerTran, mdtAdvertiseTran)
'            End If

'            If blnFlag = False And objVacancyData._Message <> "" Then
'                eZeeMsgBox.Show(objVacancyData._Message, enMsgBoxStyle.Information)
'            End If

'            If blnFlag Then
'                mblnCancel = False
'                If menAction = enAction.ADD_CONTINUE Then
'                    objVacancyData = Nothing
'                    objVacancyData = New clsVacancy
'                    Call GetValue()
'                Else
'                    mintVacancyunkid = objVacancyData._Vacancyunkid
'                    Me.Close()
'                End If
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Me.Close()
'    End Sub
'#End Region

'#Region " Form Events "
'    'Sohail (11 Sep 2010) -- Start
'    Private Sub frmVacancy_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
'        Try
'            Select Case e.KeyCode
'                Case Keys.Return
'                    SendKeys.Send("{TAB}")
'                Case Keys.S
'                    If e.Control = True Then
'                        Call btnSave.PerformClick()
'                    End If
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmVacancy_AddEdit_KeyDown", mstrModuleName)
'        End Try
'    End Sub
'    'Sohail (11 Sep 2010) -- End

'    Private Sub frmVacancy_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        radEmployee.Checked = True
'        objVacancyData = New clsVacancy
'        objInterviewerTran = New clsinterviewer_tran
'        objAdvertiseTran = New clsAdvertise_tran
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            'Sohail (11 Sep 2010) -- Start
'            Call SetColor()
'            'Sohail (11 Sep 2010) -- End
'            If menAction = enAction.EDIT_ONE Then
'                objVacancyData._Vacancyunkid = mintVacancyunkid

'            End If
'            Call VacancyFillCombo()
'            Call InterviewerFillCombo()
'            Call AdvertiseFillCombo()
'            lblLevelInfo.Text = Language.getMessage(mstrModuleName, 111, "Level 1 means lower Level.")

'            GetValue()

'            objInterviewerTran._VacancyUnkid = mintVacancyunkid
'            mdtInterviewerTran = objInterviewerTran._DataTable
'            Call FillInterviewersList()

'            objAdvertiseTran._VacancyUnkid = mintVacancyunkid
'            mdtAdvertiseTran = objAdvertiseTran._DataTable
'            Call FillAdvertiseList()

'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            Call SetVisibility()
'            'S.SANDEEP [ 25 DEC 2011 ] -- END

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmVacancy_AddEdit_Load", mstrModuleName)
'        End Try


'    End Sub
'#End Region

'#Region " Tab Buttons "

'#Region " Vacancy "
'    Private Sub objbtnAddSkills_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddSkills.Click
'        'Sohail (11 Sep 2010) -- Start
'        If CInt(cboJob.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 138, "Please select Job."), enMsgBoxStyle.Information)
'            cboJob.Focus()
'            Exit Sub
'        End If
'        'Sohail (11 Sep 2010) -- End
'        Try
'            Dim objfrmJob As New frmJobs_AddEdit
'            objfrmJob.displayDialog(CInt(cboJob.SelectedValue), enAction.EDIT_ONE)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddSkills_Click", mstrModuleName)
'        End Try
'    End Sub

'    'S.SANDEEP [ 07 NOV 2011 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub objbtnAddQualification_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddQualification.Click
'        Try
'            If CInt(cboJob.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 138, "Please select Job."), enMsgBoxStyle.Information)
'                cboJob.Focus()
'                Exit Sub
'            End If
'            Dim objfrmJob As New frmJobs_AddEdit
'            objfrmJob.displayDialog(CInt(cboJob.SelectedValue), enAction.EDIT_ONE)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddQualification_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 07 NOV 2011 ] -- END

'#End Region

'#Region " Interview "

'    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

'        Try
'            If radEmployee.Checked = True Then
'                'Sohail (11 Sep 2010) -- Start
'                'Dim dtRow As DataRow() = mdtInterviewerTran.Select("interviewerunkid = '" & CInt(cboEmployee.SelectedValue) & "' AND AUD <> 'D' ")
'                Dim dtRow As DataRow() = mdtInterviewerTran.Select("interviewerunkid = " & CInt(cboEmployee.SelectedValue) & " AND AUD <> 'D' ")
'                'Sohail (11 Sep 2010) -- End

'                If dtRow.Length > 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Selected Interviewer is already added to the list."), enMsgBoxStyle.Information)
'                    Exit Sub
'                End If

'            ElseIf radOthers.Checked = True Then
'                Dim dtRow As DataRow() = mdtInterviewerTran.Select("otherinterviewer_name LIKE '" & txtOtherInterviewerName.Text & "%' AND AUD <> 'D' ")

'                If dtRow.Length > 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 113, "Selected Interviewer is already added to the list."), enMsgBoxStyle.Information)
'                    Exit Sub
'                End If

'            End If

'            If txtOtherCompany.Text = Company._Object._Name Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 114, "Company cannot be same. Please give another company name."), enMsgBoxStyle.Information)
'                txtOtherCompany.Focus()
'                Exit Sub
'            End If

'            'Sohail (11 Sep 2010) -- Start
'            If CInt(cboInterviewType.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 133, "Please select Interview Type."), enMsgBoxStyle.Information)
'                cboInterviewType.Focus()
'                Exit Sub
'            ElseIf radEmployee.Checked = True AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 134, "Please select Employee."), enMsgBoxStyle.Information)
'                cboEmployee.Focus()
'                Exit Sub
'            ElseIf radOthers.Checked = True AndAlso txtOtherInterviewerName.Text.Trim = "" Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 135, "Please Enter Interviewer Name."), enMsgBoxStyle.Information)
'                txtOtherInterviewerName.Focus()
'                Exit Sub
'            End If

'            'Dim dtLevelRow As DataRow() = mdtInterviewerTran.Select("interviewer_level = '" & CDec(nudLevel.Value) & "' AND AUD <> 'D' ")
'            Dim dtLevelRow As DataRow() = mdtInterviewerTran.Select("interviewer_level = " & CDec(nudLevel.Value) & " AND AUD <> 'D' ")
'            'Sohail (11 Sep 2010) -- End
'            If dtLevelRow.Length > 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 117, "Selected Level is already assigned to Interviewer. Please assign new level."), enMsgBoxStyle.Information)
'                nudLevel.Focus()
'                Exit Sub
'            End If

'            Dim dtInterviewerRow As DataRow
'            dtInterviewerRow = mdtInterviewerTran.NewRow

'            dtInterviewerRow.Item("interviewertranunkid") = -1
'            dtInterviewerRow.Item("vacancyunkid") = mintVacancyunkid
'            dtInterviewerRow.Item("interviewer_level") = nudLevel.Value
'            If radEmployee.Checked = True Then
'                dtInterviewerRow.Item("interviewerunkid") = CInt(cboEmployee.SelectedValue)
'                dtInterviewerRow.Item("otherinterviewer_name") = ""
'                'Sohail (11 Sep 2010) -- Start
'                dtInterviewerRow.Item("othercompany") = ""
'                dtInterviewerRow.Item("otherdepartment") = ""
'                dtInterviewerRow.Item("othercontact_no") = ""
'                'Sohail (11 Sep 2010) -- End
'            Else
'                dtInterviewerRow.Item("interviewerunkid") = CInt(-1)
'                dtInterviewerRow.Item("otherinterviewer_name") = txtOtherInterviewerName.Text
'                'Sohail (11 Sep 2010) -- Start
'                dtInterviewerRow.Item("othercompany") = txtOtherCompany.Text
'                dtInterviewerRow.Item("otherdepartment") = txtOtherDepartment.Text
'                dtInterviewerRow.Item("othercontact_no") = txtOtherContactNo.Text
'            End If
'            'dtInterviewerRow.Item("othercompany") = txtOtherCompany.Text
'            'dtInterviewerRow.Item("otherdepartment") = txtOtherDepartment.Text
'            'dtInterviewerRow.Item("othercontact_no") = txtOtherContactNo.Text
'            'Sohail (11 Sep 2010) -- End
'            dtInterviewerRow.Item("interviewtypeunkid") = CInt(cboInterviewType.SelectedValue)
'            dtInterviewerRow.Item("userunkid") = User._Object._Userunkid
'            dtInterviewerRow.Item("isvoid") = False
'            dtInterviewerRow.Item("voiduserunkid") = -1
'            'dtInterviewerRow.Item("voiddatetime") = DBNull
'            dtInterviewerRow.Item("voidreason") = ""
'            dtInterviewerRow.Item("GUID") = Guid.NewGuid().ToString
'            dtInterviewerRow.Item("AUD") = "A"

'            mdtInterviewerTran.Rows.Add(dtInterviewerRow)

'            Call FillInterviewersList()
'            Call ResetInterviewer()
'            cboInterviewType.Focus()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
'        'Sohail (11 Sep 2010) -- Start
'        If lvInterviewer.SelectedItems.Count < 1 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 136, "Please select Interviewer from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            lvInterviewer.Select()
'            Exit Sub
'        End If
'        If CInt(cboInterviewType.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 133, "Please select Interview Type."), enMsgBoxStyle.Information)
'            cboInterviewType.Focus()
'            Exit Sub
'        ElseIf radEmployee.Checked = True AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 134, "Please select Employee."), enMsgBoxStyle.Information)
'            cboEmployee.Focus()
'            Exit Sub
'        ElseIf radOthers.Checked = True AndAlso txtOtherInterviewerName.Text.Trim = "" Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 135, "Please Enter Interviewer Name."), enMsgBoxStyle.Information)
'            txtOtherInterviewerName.Focus()
'            Exit Sub
'        End If
'        'Sohail (11 Sep 2010) -- End
'        Try


'            If lvInterviewer.SelectedItems.Count > 0 Then
'                If mintInterviewerIndex > -1 Then
'                    Dim drtemp As DataRow()
'                    If CInt(lvInterviewer.Items(mintInterviewerIndex).Tag) = -1 Then
'                        drtemp = mdtInterviewerTran.Select("GUID ='" & lvInterviewer.Items(mintInterviewerIndex).SubItems(objcolhIntGUID.Index).Text & "'")
'                    Else
'                        'Sohail (11 Sep 2010) -- Start
'                        'drtemp = mdtInterviewerTran.Select("interviewertranunkid ='" & CInt(lvInterviewer.SelectedItems(0).Tag) & "'")
'                        drtemp = mdtInterviewerTran.Select("interviewertranunkid = " & CInt(lvInterviewer.SelectedItems(0).Tag) & "")
'                        'Sohail (11 Sep 2010) -- End
'                    End If

'                    'Sohail (11 Sep 2010) -- Start
'                    'Dim dtLevelRow As DataRow() = mdtInterviewerTran.Select("interviewer_level = '" & CInt(nudLevel.Value) & "' AND AUD <> 'D'")
'                    Dim dtLevelRow As DataRow() = mdtInterviewerTran.Select("interviewer_level = " & CInt(nudLevel.Value) & " AND GUID <> '" & lvInterviewer.Items(mintInterviewerIndex).SubItems(objcolhIntGUID.Index).Text & "' AND AUD <> 'D'")
'                    'Sohail (11 Sep 2010) -- End

'                    If dtLevelRow.Length > 0 Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 117, "Selected Level is already assigned to Interviewer. Please assign new level."), enMsgBoxStyle.Information)
'                        nudLevel.Focus()
'                        Exit Sub
'                    End If

'                    If drtemp.Length > 0 Then
'                        With drtemp(0)
'                            .Item("interviewertranunkid") = lvInterviewer.Items(mintInterviewerIndex).Tag
'                            .Item("vacancyunkid") = mintVacancyunkid
'                            .Item("interviewer_level") = nudLevel.Value
'                            .Item("interviewtypeunkid") = CInt(cboInterviewType.SelectedValue)
'                            If radEmployee.Checked = True Then
'                                .Item("interviewerunkid") = CInt(cboEmployee.SelectedValue)
'                            Else
'                                .Item("interviewerunkid") = CInt(-1)
'                                .Item("otherinterviewer_name") = txtOtherInterviewerName.Text
'                                .Item("othercompany") = txtOtherCompany.Text
'                                .Item("otherdepartment") = txtOtherDepartment.Text
'                                .Item("othercontact_no") = txtOtherContactNo.Text
'                            End If
'                            .Item("userunkid") = 1
'                            .Item("GUID") = Guid.NewGuid().ToString
'                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")) = "" Then
'                                .Item("AUD") = "U"
'                            End If
'                            .AcceptChanges()
'                        End With
'                        Call FillInterviewersList()
'                    End If

'                End If
'                Call ResetInterviewer()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        'Sohail (11 Sep 2010) -- Start
'        If lvInterviewer.SelectedItems.Count < 1 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 136, "Please select Interviewer from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            lvInterviewer.Select()
'            Exit Sub
'        End If
'        'Sohail (11 Sep 2010) -- End
'        Try

'            If mintInterviewerIndex > -1 Then
'                'Sohail (11 Sep 2010) -- Start
'                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 137, "Are you sure you want to delete this Interviewer?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
'                    Exit Sub
'                End If
'                'Sohail (11 Sep 2010) -- End

'                Dim drTemp As DataRow()
'                If CInt(lvInterviewer.Items(mintInterviewerIndex).Tag) = -1 Then
'                    drTemp = mdtInterviewerTran.Select("GUID = '" & lvInterviewer.Items(mintInterviewerIndex).SubItems(objcolhIntGUID.Index).Text & "'")
'                Else
'                    'Sohail (11 Sep 2010) -- Start
'                    'drTemp = mdtInterviewerTran.Select("interviewertranunkid ='" & CInt(lvInterviewer.SelectedItems(0).Tag) & "'")
'                    drTemp = mdtInterviewerTran.Select("interviewertranunkid = " & CInt(lvInterviewer.SelectedItems(0).Tag) & "")
'                    'Sohail (11 Sep 2010) -- End
'                End If

'                If drTemp.Length > 0 Then
'                    drTemp(0).Item("AUD") = "D"
'                    Call FillInterviewersList()
'                    Call ResetInterviewer()
'                End If
'            End If
'            If lvInterviewer.Items.Count > 11 Then
'                colhEmpName.Width = 110 - 18
'            Else
'                colhEmpName.Width = 110
'            End If


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub objbtnAddInterviewType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddInterviewType.Click
'        Dim objfrm As New frmCommonMaster
'        Dim objCommonMaster As New clsCommon_Master
'        Dim dsList As DataSet
'        Dim intRefId As Integer = -1
'        Try

'            objfrm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, enAction.ADD_ONE)

'            dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.INTERVIEW_TYPE, True, "InterviewType", -1)
'            With cboInterviewType
'                .ValueMember = "masterunkid"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("InterviewType")
'                .SelectedValue = intRefId
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddInterviewType_Click", mstrModuleName)
'        End Try

'    End Sub

'#End Region

'#Region " Advertisement "

'    Private Sub btnAdAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdAdd.Click
'        'Sohail (11 Sep 2010) -- Start
'        If CInt(cboAdvertiseCategory.SelectedValue) <= 0 Then
'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 139, "Please select Advertise Category. Advertise Category is mandatory information."), enMsgBoxStyle.Information)
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 139, "Please select Cost Category. Cost Category is mandatory information."), enMsgBoxStyle.Information)
'            'S.SANDEEP [ 25 DEC 2011 ] -- END
'            cboAdvertiseCategory.Focus()
'            Exit Sub
'        ElseIf CInt(cboAdvertiser.SelectedValue) <= 0 Then
'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 140, "Please select Advertiser. Advertiser is mandatory information."), enMsgBoxStyle.Information)
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 140, "Please select Cost. Cost is mandatory information."), enMsgBoxStyle.Information)
'            'S.SANDEEP [ 25 DEC 2011 ] -- END
'            cboAdvertiser.Focus()
'            Exit Sub
'        End If

'        Dim drRow As DataRow() = mdtAdvertiseTran.Select("advertiserunkid = " & CInt(cboAdvertiser.SelectedValue) & " AND AUD <> 'D' ")
'        If drRow.Length > 0 Then
'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 143, "Selected Advertiser is already assigned. Please assign new Advertiser."), enMsgBoxStyle.Information)
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 143, "Selected Cost is already assigned. Please assign new Cost."), enMsgBoxStyle.Information)
'            'S.SANDEEP [ 25 DEC 2011 ] -- END
'            cboAdvertiser.Focus()
'            Exit Sub
'        End If
'        'Sohail (11 Sep 2010) -- End
'        Try

'            Dim dtAdvertiseRow As DataRow

'            dtAdvertiseRow = mdtAdvertiseTran.NewRow

'            dtAdvertiseRow.Item("advertisetranunkid") = -1
'            dtAdvertiseRow.Item("vacancyunkid") = mintVacancyunkid


'            dtAdvertiseRow.Item("advertisecategoryunkid") = CInt(cboAdvertiseCategory.SelectedValue)

'            dtAdvertiseRow.Item("advertiserunkid") = CInt(cboAdvertiser.SelectedValue)
'            'Sohail (11 Sep 2010) -- Start (errro occurs if txtcost OR txtbudget is blank
'            'dtAdvertiseRow.Item("costing") = txtCosting.Text
'            'dtAdvertiseRow.Item("budget") = txtBudget.Text

'            'Anjan (11 May 2011)-Start
'            'dtAdvertiseRow.Item("costing") = txtCosting.Decimal
'            'dtAdvertiseRow.Item("budget") = txtBudget.Decimal
'            dtAdvertiseRow.Item("costing") = txtCosting.Decimal
'            dtAdvertiseRow.Item("budget") = txtBudget.Decimal
'            'Anjan (11 May 2011)-End 



'            'Sohail (11 Sep 2010) -- End
'            dtAdvertiseRow.Item("description") = txtDescription.Text
'            dtAdvertiseRow.Item("notes") = txtNotes.Text
'            dtAdvertiseRow.Item("userunkid") = 1
'            dtAdvertiseRow.Item("isvoid") = False
'            dtAdvertiseRow.Item("userunkid") = User._Object._Userunkid
'            dtAdvertiseRow.Item("voiduserunkid") = -1
'            'dtInterviewerRow.Item("voiddatetime") = DBNull
'            dtAdvertiseRow.Item("voidreason") = ""
'            dtAdvertiseRow.Item("GUID") = Guid.NewGuid().ToString
'            dtAdvertiseRow.Item("AUD") = "A"

'            mdtAdvertiseTran.Rows.Add(dtAdvertiseRow)

'            Call FillAdvertiseList()
'            Call ResetAdvertise()
'            cboAdvertiseCategory.Focus()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnAdAdd_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub btnAdEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdEdit.Click
'        'Sohail (11 Sep 2010) -- Start
'        If lvAdvertisement.SelectedItems.Count < 1 Then
'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 141, "Please select Advertisement from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 141, "Please select Costing from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            'S.SANDEEP [ 25 DEC 2011 ] -- END
'            lvAdvertisement.Select()
'            Exit Sub
'        End If
'        If CInt(cboAdvertiseCategory.SelectedValue) <= 0 Then
'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 139, "Please select Advertise Category. Advertise Category is mandatory information."), enMsgBoxStyle.Information)
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 139, "Please select Cost Category. Cost Category is mandatory information."), enMsgBoxStyle.Information)
'            'S.SANDEEP [ 25 DEC 2011 ] -- END
'            cboAdvertiseCategory.Focus()
'            Exit Sub
'        ElseIf CInt(cboAdvertiser.SelectedValue) <= 0 Then
'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 140, "Please select Advertiser. Advertiser is mandatory information."), enMsgBoxStyle.Information)
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 140, "Please select Cost. Cost is mandatory information."), enMsgBoxStyle.Information)
'            'S.SANDEEP [ 25 DEC 2011 ] -- END
'            cboAdvertiser.Focus()
'            Exit Sub
'        End If
'        'Sohail (11 Sep 2010) -- End
'        Try

'            If lvAdvertisement.SelectedItems.Count > 0 Then
'                If mintAdvertiseIndex > -1 Then
'                    Dim drTemp As DataRow()

'                    Dim drRow As DataRow() = mdtAdvertiseTran.Select("advertiserunkid = " & CInt(cboAdvertiser.SelectedValue) & " AND GUID <> '" & lvAdvertisement.Items(mintAdvertiseIndex).SubItems(objcolhADGUID.Index).Text & "' AND AUD <> 'D' ")
'                    If drRow.Length > 0 Then
'                        'S.SANDEEP [ 25 DEC 2011 ] -- START
'                        'ENHANCEMENT : TRA CHANGES
'                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 143, "Selected Advertiser is already assigned. Please assign new Advertiser."), enMsgBoxStyle.Information)
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 143, "Selected Cost is already assigned. Please assign new Cost."), enMsgBoxStyle.Information)
'                        'S.SANDEEP [ 25 DEC 2011 ] -- END
'                        cboAdvertiser.Focus()
'                        Exit Sub
'                    End If

'                    If CInt(lvAdvertisement.Items(mintAdvertiseIndex).Tag) = -1 Then
'                        drTemp = mdtAdvertiseTran.Select("GUID ='" & lvAdvertisement.Items(mintAdvertiseIndex).SubItems(objcolhADGUID.Index).Text & "'")
'                    Else
'                        'Sohail (11 Sep 2010) -- Start
'                        'drTemp = mdtAdvertiseTran.Select("advertisetranunkid ='" & CInt(lvAdvertisement.SelectedItems(0).Tag) & "'")
'                        drTemp = mdtAdvertiseTran.Select("advertisetranunkid = " & CInt(lvAdvertisement.SelectedItems(0).Tag) & "")
'                        'Sohail (11 Sep 2010) -- End
'                    End If
'                    If drTemp.Length > 0 Then
'                        With drTemp(0)
'                            .Item("advertisetranunkid") = lvAdvertisement.Items(mintAdvertiseIndex).Tag
'                            .Item("vacancyunkid") = mintVacancyunkid
'                            .Item("advertiserunkid") = CInt(cboAdvertiser.SelectedValue)
'                            .Item("advertisecategoryunkid") = CInt(cboAdvertiseCategory.SelectedValue)
'                            'Sohail (11 Sep 2010) -- Start (errro occurs if txtcost OR txtbudget is blank
'                            '.Item("costing") = txtCosting.Text
'                            '.Item("budget") = txtBudget.Text

'                            'Anjan (11 May 2011)-Start
'                            '.Item("costing") = txtCosting.Decimal
'                            '.Item("budget") = txtBudget.Decimal
'                            .Item("costing") = txtCosting.Decimal
'                            .Item("budget") = txtBudget.Decimal
'                            'Anjan (11 May 2011)-End 


'                            'Sohail (11 Sep 2010) -- End
'                            .Item("description") = txtDescription.Text
'                            .Item("notes") = txtNotes.Text

'                            .Item("userunkid") = User._Object._Userunkid
'                            .Item("GUID") = Guid.NewGuid().ToString
'                            If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")) = "" Then
'                                .Item("AUD") = "U"
'                            End If
'                            .AcceptChanges()
'                        End With
'                        Call FillAdvertiseList()
'                    End If
'                End If
'                Call ResetAdvertise()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub btnAdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdDelete.Click
'        'Sohail (11 Sep 2010) -- Start
'        If lvAdvertisement.SelectedItems.Count < 1 Then
'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 141, "Please select Advertisement from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 141, "Please select Costing from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            'S.SANDEEP [ 25 DEC 2011 ] -- END
'            lvAdvertisement.Select()
'            Exit Sub
'        End If
'        'Sohail (11 Sep 2010) -- End
'        Try

'            If mintAdvertiseIndex > -1 Then
'                'Sohail (11 Sep 2010) -- Start

'                'S.SANDEEP [ 25 DEC 2011 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 142, "Are you sure you want to delete this Advertisement detail?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
'                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 142, "Are you sure you want to delete this Costing detail?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
'                    'S.SANDEEP [ 25 DEC 2011 ] -- END
'                    Exit Sub
'                End If
'                'Sohail (11 Sep 2010) -- End

'                Dim drtemp As DataRow()
'                If CInt(lvAdvertisement.Items(mintAdvertiseIndex).Tag) = -1 Then
'                    drtemp = mdtAdvertiseTran.Select("GUID = '" & lvAdvertisement.Items(mintAdvertiseIndex).SubItems(objcolhADGUID.Index).Text & "'")
'                Else
'                    'Sohail (11 Sep 2010) -- Start
'                    'drTemp = mdtAdvertiseTran.Select("advertisetranunkid ='" & CInt(lvAdvertisement.SelectedItems(0).Tag) & "'")
'                    drtemp = mdtAdvertiseTran.Select("advertisetranunkid = " & CInt(lvAdvertisement.SelectedItems(0).Tag) & "")
'                    'Sohail (11 Sep 2010) -- End
'                End If

'                If drtemp.Length > 0 Then
'                    drtemp(0).Item("AUD") = "D"
'                    Call FillAdvertiseList()
'                    Call ResetAdvertise()
'                End If
'            End If
'            If lvAdvertisement.Items.Count > 11 Then
'                colhAdvertiserName.Width = 110 - 18
'            Else
'                colhAdvertiserName.Width = 110
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub objbtnAdvertiseCategory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvertiseCategory.Click
'        Dim objfrm As New frmCommonMaster
'        Dim objCommonMaster As New clsCommon_Master
'        Dim dsList As DataSet
'        Dim intRefId As Integer = -1

'        Try


'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'objfrm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY, enAction.ADD_ONE)
'            'dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY, True, "AdvertiseCategory")

'            'With cboAdvertiseCategory
'            '    .ValueMember = "masterunkid"
'            '    .DisplayMember = "name"
'            '    .DataSource = dsList.Tables("AdvertiseCategory")
'            '    .SelectedValue = intRefId
'            'End With
'            objfrm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ADVERTISE_CATEGORY, True, "AdvertiseCategory")
'                With cboAdvertiseCategory
'                    .ValueMember = "masterunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("AdvertiseCategory")
'                    .SelectedValue = intRefId
'                End With
'            End If
'            'S.SANDEEP [ 25 DEC 2011 ] -- END


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAdvertiseCategory_Click", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub objbtnAdvertiser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvertiser.Click
'        Dim objfrmAdvertiser As New frmAdvertise_AddEdit
'        Dim objAdvertiser As New clsAdvertise_master
'        Dim dsList As DataSet
'        Dim intRefid As Integer = -1
'        Try
'            'S.SANDEEP [ 25 DEC 2011 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'objfrmAdvertiser.displayDialog(intRefid, enAction.ADD_ONE)
'            'dsList = objAdvertiser.getComboList(True, "Advertiser")

'            'With cboAdvertiser
'            '    .ValueMember = "advertiseunkid"
'            '    .DisplayMember = "company"
'            '    .DataSource = dsList.Tables("Advertiser")
'            '    .SelectedValue = intRefid
'            'End With
'            objfrmAdvertiser.displayDialog(intRefid, enAction.ADD_ONE)
'            If intRefid > 0 Then
'                dsList = objAdvertiser.getComboList(True, "Advertiser")
'                With cboAdvertiser
'                    .ValueMember = "advertiseunkid"
'                    .DisplayMember = "company"
'                    .DataSource = dsList.Tables("Advertiser")
'                    .SelectedValue = intRefid
'                End With
'            End If
'            'S.SANDEEP [ 25 DEC 2011 ] -- END
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAdvertiser_Click", mstrModuleName)
'        End Try

'    End Sub
'#End Region

'#End Region

'#Region " Tab Other Control Events "
'    'Vacancy Start
'    Private Sub radEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radEmployee.CheckedChanged, radOthers.CheckedChanged
'        If radEmployee.Checked = True Then
'            pnlOthers.Visible = False
'        ElseIf radOthers.Checked = True Then
'            pnlOthers.Visible = True
'        End If
'    End Sub

'    'Vacancny End


'    ' Advertise Start
'    Private Sub cboAdvertiseCategory_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAdvertiseCategory.SelectedValueChanged
'        Dim dsCombo As New DataSet
'        Dim objadvertise As New clsAdvertise_master

'        Try
'            'If CInt(cboAdvertiseCategory.SelectedValue) > 0 Then 'Sohail (11 Sep 2010) 'To set selected value=0 if no adv. category is selected.
'            dsCombo = objadvertise.getComboList(True, "Advertiser", CInt(cboAdvertiseCategory.SelectedValue))
'            With cboAdvertiser
'                .ValueMember = "advertiseunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombo.Tables("Advertiser")
'                .SelectedValue = 0 'Sohail (11 Sep 2010)
'            End With
'            'End If 'Sohail (11 Sep 2010)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboAdvertiseCategory_SelectedValueChanged", mstrModuleName)
'        End Try
'    End Sub
'    'Advertise  End



'#End Region

'    'Sohail (11 Sep 2010) -- Start
'#Region " ComboBox's Events "
'    Private Sub cboEmployee_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedValueChanged
'        Dim objEmp As New clsEmployee_Master
'        Dim objDept As New clsDepartment
'        Try
'            objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
'            objDept._Departmentunkid = objEmp._Departmentunkid
'            lblEmployeeDepartment.Text = objDept._Name

'            'Sandeep [ 01 FEB 2011 ] -- START
'            'lblEmployeeCompany.Text = "1" ' 
'            lblEmployeeCompany.Text = Company._Object._Name
'            'Sandeep [ 01 FEB 2011 ] -- END 

'            lblEmployeeContactNo.Text = objEmployeeData._Present_Tel_No
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedValueChanged", mstrModuleName)
'        Finally
'            objEmp = Nothing
'            objDept = Nothing
'        End Try
'    End Sub
'#End Region
'    'Sohail (11 Sep 2010) -- End


'    'Sandeep [ 17 DEC 2010 ] -- Start
'    Private Sub objbtnAddPayType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddPayType.Click
'        Dim frm As New frmCommonMaster
'        Dim intRefId As Integer = -1
'        Try
'            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.PAY_TYPE, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objCMaster As New clsCommon_Master
'                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.PAY_TYPE, True, "PAYTYP")
'                With cboPayType
'                    .ValueMember = "masterunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("PAYTYP")
'                    .SelectedValue = intRefId
'                End With
'                dsList.Dispose()
'                objCMaster = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddPayType_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnAddJobDept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddJobDept.Click
'        Dim frm As New frmDepartment_AddEdit
'        Dim intRefId As Integer = -1
'        Try
'            frm.displayDialog(intRefId, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objDept As New clsDepartment
'                dsList = objDept.getComboList("Dept", True)
'                With cboJobDepartment
'                    .ValueMember = "departmentunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Dept")
'                    .SelectedValue = intRefId
'                End With
'                dsList.Dispose()
'                objDept = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddJobDept_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnAddJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddJob.Click
'        Dim frm As New frmJobs_AddEdit
'        Dim intRefId As Integer = -1
'        Try
'            frm.displayDialog(intRefId, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objJob As New clsJobs
'                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue))
'                With cboJob
'                    .ValueMember = "jobunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Job")
'                    .SelectedValue = intRefId
'                End With
'                dsList.Dispose()
'                objJob = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddJob_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnAddEmployment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddEmployment.Click
'        Dim frm As New frmCommonMaster
'        Dim intRefId As Integer = -1
'        Try
'            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objCMaster As New clsCommon_Master
'                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EMPLTYP")
'                With cboEmploymentType
'                    .ValueMember = "masterunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("EMPLTYP")
'                    .SelectedValue = intRefId
'                End With
'                dsList.Dispose()
'                objCMaster = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddEmployment_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnAddShiftType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddShiftType.Click
'        Dim frm As New frmCommonMaster
'        Dim intRefId As Integer = -1
'        Try
'            frm.displayDialog(intRefId, clsCommon_Master.enCommonMaster.SHIFT_TYPE, enAction.ADD_ONE)
'            If intRefId > 0 Then
'                Dim dsList As New DataSet
'                Dim objCMaster As New clsCommon_Master
'                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "SFTTYP")
'                With cboShiftType
'                    .ValueMember = "masterunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("SFTTYP")
'                    .SelectedValue = intRefId
'                End With
'                dsList.Dispose()
'                objCMaster = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnAddShiftType_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub cboSections_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSections.SelectedIndexChanged
'        Try
'            If CInt(cboSections.SelectedValue) > 0 Then
'                Dim dsList As New DataSet
'                Dim objJob As New clsJobs
'                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue))
'                With cboJob
'                    .ValueMember = "jobunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Job")
'                    If menAction = enAction.EDIT_ONE Then
'                        .SelectedValue = objVacancyData._Jobunkid
'                    Else
'                        .SelectedValue = 0
'                    End If
'                End With
'                dsList.Dispose()
'                objJob = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboSections_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboJobGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJobGroup.SelectedIndexChanged
'        Try
'            If CInt(cboJobGroup.SelectedValue) > 0 Then
'                Dim dsList As New DataSet
'                Dim objJob As New clsJobs
'                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue))
'                With cboJob
'                    .ValueMember = "jobunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Job")
'                    If menAction = enAction.EDIT_ONE Then
'                        .SelectedValue = objVacancyData._Jobunkid
'                    Else
'                        .SelectedValue = 0
'                    End If
'                End With
'                dsList.Dispose()
'                objJob = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboJobGroup_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub cboUnits_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnits.SelectedIndexChanged
'        Try
'            If CInt(cboUnits.SelectedValue) > 0 Then
'                Dim dsList As New DataSet
'                Dim objJob As New clsJobs
'                dsList = objJob.getComboList("Job", True, CInt(cboJobGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboSections.SelectedValue))
'                With cboJob
'                    .ValueMember = "jobunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("Job")
'                    If menAction = enAction.EDIT_ONE Then
'                        .SelectedValue = objVacancyData._Jobunkid
'                    Else
'                        .SelectedValue = 0
'                    End If
'                End With
'                dsList.Dispose()
'                objJob = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboUnits_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub
'    'Sandeep [ 17 DEC 2010 ] -- End 

'    Private Sub objbtnSearchApplicant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApplicant.Click

'        Dim frm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            With frm
'                .ValueMember = cboJob.ValueMember
'                .DisplayMember = cboJob.DisplayMember
'                .CodeMember = ""
'                .DataSource = CType(cboJob.DataSource, DataTable)
'            End With

'            If frm.DisplayDialog Then
'                cboJob.SelectedValue = frm.SelectedValue
'                cboJob.Focus()
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchApplicant_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'End Class