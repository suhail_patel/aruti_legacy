﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStaffRequisitionApproverMapping_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStaffRequisitionApproverMapping_AddEdit))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbApproverMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.tbJob = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchJob = New eZee.TextBox.AlphanumericTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objSelectAllJob = New System.Windows.Forms.CheckBox
        Me.dgvJob = New System.Windows.Forms.DataGridView
        Me.objdgcolhischeck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhJobId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblSearch = New System.Windows.Forms.Label
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.pnlAllocationList = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvAllocationList = New System.Windows.Forms.ListView
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.cboAllocation = New System.Windows.Forms.ComboBox
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.objbtnSearchUser = New eZee.Common.eZeeGradientButton
        Me.cboApproverUser = New System.Windows.Forms.ComboBox
        Me.objbtnSearchLevel = New eZee.Common.eZeeGradientButton
        Me.lblApprLevel = New System.Windows.Forms.Label
        Me.cboLevel = New System.Windows.Forms.ComboBox
        Me.lblApprUser = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbApproverMapping.SuspendLayout()
        Me.tbJob.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvJob, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAllocationList.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.gbApproverMapping)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(617, 378)
        Me.pnlMain.TabIndex = 1
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 323)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(617, 55)
        Me.objFooter.TabIndex = 12
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(405, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(508, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbApproverMapping
        '
        Me.gbApproverMapping.BorderColor = System.Drawing.Color.Black
        Me.gbApproverMapping.Checked = False
        Me.gbApproverMapping.CollapseAllExceptThis = False
        Me.gbApproverMapping.CollapsedHoverImage = Nothing
        Me.gbApproverMapping.CollapsedNormalImage = Nothing
        Me.gbApproverMapping.CollapsedPressedImage = Nothing
        Me.gbApproverMapping.CollapseOnLoad = False
        Me.gbApproverMapping.Controls.Add(Me.tbJob)
        Me.gbApproverMapping.Controls.Add(Me.lblSearch)
        Me.gbApproverMapping.Controls.Add(Me.txtSearch)
        Me.gbApproverMapping.Controls.Add(Me.pnlAllocationList)
        Me.gbApproverMapping.Controls.Add(Me.cboAllocation)
        Me.gbApproverMapping.Controls.Add(Me.lblAllocation)
        Me.gbApproverMapping.Controls.Add(Me.objbtnSearchUser)
        Me.gbApproverMapping.Controls.Add(Me.cboApproverUser)
        Me.gbApproverMapping.Controls.Add(Me.objbtnSearchLevel)
        Me.gbApproverMapping.Controls.Add(Me.lblApprLevel)
        Me.gbApproverMapping.Controls.Add(Me.cboLevel)
        Me.gbApproverMapping.Controls.Add(Me.lblApprUser)
        Me.gbApproverMapping.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbApproverMapping.ExpandedHoverImage = Nothing
        Me.gbApproverMapping.ExpandedNormalImage = Nothing
        Me.gbApproverMapping.ExpandedPressedImage = Nothing
        Me.gbApproverMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApproverMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApproverMapping.HeaderHeight = 25
        Me.gbApproverMapping.HeaderMessage = ""
        Me.gbApproverMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbApproverMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApproverMapping.HeightOnCollapse = 0
        Me.gbApproverMapping.LeftTextSpace = 0
        Me.gbApproverMapping.Location = New System.Drawing.Point(0, 0)
        Me.gbApproverMapping.Name = "gbApproverMapping"
        Me.gbApproverMapping.OpenHeight = 300
        Me.gbApproverMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApproverMapping.ShowBorder = True
        Me.gbApproverMapping.ShowCheckBox = False
        Me.gbApproverMapping.ShowCollapseButton = False
        Me.gbApproverMapping.ShowDefaultBorderColor = True
        Me.gbApproverMapping.ShowDownButton = False
        Me.gbApproverMapping.ShowHeader = True
        Me.gbApproverMapping.Size = New System.Drawing.Size(617, 378)
        Me.gbApproverMapping.TabIndex = 0
        Me.gbApproverMapping.Temp = 0
        Me.gbApproverMapping.Text = "Staff Requisition Approver Mapping"
        Me.gbApproverMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tbJob
        '
        Me.tbJob.ColumnCount = 1
        Me.tbJob.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tbJob.Controls.Add(Me.txtSearchJob, 0, 0)
        Me.tbJob.Controls.Add(Me.Panel1, 0, 1)
        Me.tbJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbJob.Location = New System.Drawing.Point(12, 120)
        Me.tbJob.Name = "tbJob"
        Me.tbJob.RowCount = 2
        Me.tbJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tbJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tbJob.Size = New System.Drawing.Size(335, 194)
        Me.tbJob.TabIndex = 109
        '
        'txtSearchJob
        '
        Me.txtSearchJob.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchJob.Flags = 0
        Me.txtSearchJob.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchJob.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchJob.Name = "txtSearchJob"
        Me.txtSearchJob.Size = New System.Drawing.Size(329, 21)
        Me.txtSearchJob.TabIndex = 106
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objSelectAllJob)
        Me.Panel1.Controls.Add(Me.dgvJob)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 29)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(329, 162)
        Me.Panel1.TabIndex = 107
        '
        'objSelectAllJob
        '
        Me.objSelectAllJob.AutoSize = True
        Me.objSelectAllJob.Location = New System.Drawing.Point(7, 5)
        Me.objSelectAllJob.Name = "objSelectAllJob"
        Me.objSelectAllJob.Size = New System.Drawing.Size(15, 14)
        Me.objSelectAllJob.TabIndex = 104
        Me.objSelectAllJob.UseVisualStyleBackColor = True
        '
        'dgvJob
        '
        Me.dgvJob.AllowUserToAddRows = False
        Me.dgvJob.AllowUserToDeleteRows = False
        Me.dgvJob.AllowUserToResizeColumns = False
        Me.dgvJob.AllowUserToResizeRows = False
        Me.dgvJob.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvJob.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvJob.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvJob.ColumnHeadersHeight = 21
        Me.dgvJob.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvJob.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhischeck, Me.dgcolhJob, Me.objdgcolhJobId})
        Me.dgvJob.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvJob.Location = New System.Drawing.Point(0, 0)
        Me.dgvJob.MultiSelect = False
        Me.dgvJob.Name = "dgvJob"
        Me.dgvJob.RowHeadersVisible = False
        Me.dgvJob.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvJob.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvJob.Size = New System.Drawing.Size(329, 162)
        Me.dgvJob.TabIndex = 105
        '
        'objdgcolhischeck
        '
        Me.objdgcolhischeck.HeaderText = ""
        Me.objdgcolhischeck.Name = "objdgcolhischeck"
        Me.objdgcolhischeck.Width = 25
        '
        'dgcolhJob
        '
        Me.dgcolhJob.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhJob.HeaderText = "Job"
        Me.dgcolhJob.Name = "dgcolhJob"
        Me.dgcolhJob.ReadOnly = True
        Me.dgcolhJob.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhJob.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhJobId
        '
        Me.objdgcolhJobId.HeaderText = "JobId"
        Me.objdgcolhJobId.Name = "objdgcolhJobId"
        Me.objdgcolhJobId.Visible = False
        '
        'lblSearch
        '
        Me.lblSearch.BackColor = System.Drawing.Color.Transparent
        Me.lblSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearch.Location = New System.Drawing.Point(353, 296)
        Me.lblSearch.Name = "lblSearch"
        Me.lblSearch.Size = New System.Drawing.Size(60, 13)
        Me.lblSearch.TabIndex = 15
        Me.lblSearch.Text = "Search"
        '
        'txtSearch
        '
        Me.txtSearch.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(419, 293)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(175, 21)
        Me.txtSearch.TabIndex = 14
        '
        'pnlAllocationList
        '
        Me.pnlAllocationList.Controls.Add(Me.objchkSelectAll)
        Me.pnlAllocationList.Controls.Add(Me.lvAllocationList)
        Me.pnlAllocationList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlAllocationList.Location = New System.Drawing.Point(356, 36)
        Me.pnlAllocationList.Name = "pnlAllocationList"
        Me.pnlAllocationList.Size = New System.Drawing.Size(238, 251)
        Me.pnlAllocationList.TabIndex = 10
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 18
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lvAllocationList
        '
        Me.lvAllocationList.CheckBoxes = True
        Me.lvAllocationList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhName})
        Me.lvAllocationList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvAllocationList.FullRowSelect = True
        Me.lvAllocationList.GridLines = True
        Me.lvAllocationList.HideSelection = False
        Me.lvAllocationList.Location = New System.Drawing.Point(0, 0)
        Me.lvAllocationList.Name = "lvAllocationList"
        Me.lvAllocationList.Size = New System.Drawing.Size(238, 251)
        Me.lvAllocationList.TabIndex = 0
        Me.lvAllocationList.UseCompatibleStateImageBehavior = False
        Me.lvAllocationList.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 30
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 200
        '
        'cboAllocation
        '
        Me.cboAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation.DropDownWidth = 300
        Me.cboAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation.FormattingEnabled = True
        Me.cboAllocation.Location = New System.Drawing.Point(161, 89)
        Me.cboAllocation.Name = "cboAllocation"
        Me.cboAllocation.Size = New System.Drawing.Size(143, 21)
        Me.cboAllocation.TabIndex = 9
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(9, 90)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(146, 16)
        Me.lblAllocation.TabIndex = 8
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchUser
        '
        Me.objbtnSearchUser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUser.BorderSelected = False
        Me.objbtnSearchUser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUser.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUser.Location = New System.Drawing.Point(310, 62)
        Me.objbtnSearchUser.Name = "objbtnSearchUser"
        Me.objbtnSearchUser.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUser.TabIndex = 6
        '
        'cboApproverUser
        '
        Me.cboApproverUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApproverUser.DropDownWidth = 300
        Me.cboApproverUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApproverUser.FormattingEnabled = True
        Me.cboApproverUser.Location = New System.Drawing.Point(161, 62)
        Me.cboApproverUser.Name = "cboApproverUser"
        Me.cboApproverUser.Size = New System.Drawing.Size(143, 21)
        Me.cboApproverUser.TabIndex = 5
        '
        'objbtnSearchLevel
        '
        Me.objbtnSearchLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLevel.BorderSelected = False
        Me.objbtnSearchLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLevel.Location = New System.Drawing.Point(310, 35)
        Me.objbtnSearchLevel.Name = "objbtnSearchLevel"
        Me.objbtnSearchLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLevel.TabIndex = 4
        '
        'lblApprLevel
        '
        Me.lblApprLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprLevel.Location = New System.Drawing.Point(8, 36)
        Me.lblApprLevel.Name = "lblApprLevel"
        Me.lblApprLevel.Size = New System.Drawing.Size(147, 16)
        Me.lblApprLevel.TabIndex = 1
        Me.lblApprLevel.Text = "Approver Level"
        Me.lblApprLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLevel
        '
        Me.cboLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLevel.DropDownWidth = 300
        Me.cboLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLevel.FormattingEnabled = True
        Me.cboLevel.Location = New System.Drawing.Point(161, 35)
        Me.cboLevel.Name = "cboLevel"
        Me.cboLevel.Size = New System.Drawing.Size(143, 21)
        Me.cboLevel.TabIndex = 2
        '
        'lblApprUser
        '
        Me.lblApprUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprUser.Location = New System.Drawing.Point(9, 63)
        Me.lblApprUser.Name = "lblApprUser"
        Me.lblApprUser.Size = New System.Drawing.Size(146, 16)
        Me.lblApprUser.TabIndex = 1
        Me.lblApprUser.Text = "Staff Requisition Approver"
        Me.lblApprUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Job"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "JobId"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'frmStaffRequisitionApproverMapping_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 378)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStaffRequisitionApproverMapping_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Staff Requisition Approver Mapping"
        Me.pnlMain.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbApproverMapping.ResumeLayout(False)
        Me.gbApproverMapping.PerformLayout()
        Me.tbJob.ResumeLayout(False)
        Me.tbJob.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvJob, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAllocationList.ResumeLayout(False)
        Me.pnlAllocationList.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbApproverMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchUser As eZee.Common.eZeeGradientButton
    Friend WithEvents cboApproverUser As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents lblApprLevel As System.Windows.Forms.Label
    Friend WithEvents cboLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprUser As System.Windows.Forms.Label
    Friend WithEvents cboAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents pnlAllocationList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents lvAllocationList As System.Windows.Forms.ListView
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Private WithEvents lblSearch As System.Windows.Forms.Label
    Private WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents tbJob As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchJob As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objSelectAllJob As System.Windows.Forms.CheckBox
    Friend WithEvents dgvJob As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhischeck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhJobId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
