﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System.IO
Imports ArutiReports

Public Class frmStaffRequisition_AddEdit

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmStaffRequisition_AddEdit"
    Private mblnCancel As Boolean = True
    Private trd As Thread
    Private objStaffReq As clsStaffrequisition_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintStaffRequisitionUnkid As Integer = -1
    Private mdicAllocaions As New Dictionary(Of Integer, String)
    'Sohail (18 Mar 2015) -- Start
    'Enhancement - Allow more than one employee to be replaced in staff requisition.
    Private mdtOldTable As DataTable = Nothing
    Private mdtNewTable As DataTable = Nothing
    Private mdtList As DataTable = Nothing
    Private mdtView As DataView = Nothing
    Private marrOldEmp As New ArrayList
    'Sohail (18 Mar 2015) -- End
    'Hemant (13 Sep 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 10 : When filing the additional staff reason and date to start work, the grade level populated earlier is getting reset. Forcing user to select it again. – TC001. The grade level and the employee name selected and the name of the allocation are getting reset when filling out the additional staff reason textbox.)
    Private marrSelectedEmployeeList As New ArrayList
    Private mblnIsSelectedEmployeeListChanged As Boolean = False
    'Hemant (13 Sep 2019) -- End


    'Gajanan [6-NOV-2019] -- Start    
    'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
    Private mdtStaffRequisitionDocument As DataTable
    Private objDocument As clsScan_Attach_Documents
    Private mstrFolderName As String = ""
    'Gajanan [6-NOV-2019] -- End
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintStaffRequisitionUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintStaffRequisitionUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtFormNo.BackColor = GUI.ColorComp
            cboAllocation.BackColor = GUI.ColorComp
            cboStatus.BackColor = GUI.ColorComp
            'cboEmployee.BackColor = GUI.ColorComp 'Sohail (18 Mar 2015)
            cboJob.BackColor = GUI.ColorComp
            cboName.BackColor = GUI.ColorComp
            cboClassGroup.BackColor = GUI.ColorOptional
            cboClass.BackColor = GUI.ColorOptional
            txtAddStaffReason.BackColor = GUI.ColorOptional
            txtJobDesc.BackColor = GUI.ColorOptional
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            cboGrade.BackColor = GUI.ColorComp
            cboGradeLevel.BackColor = GUI.ColorComp
            cboEmploymentType.BackColor = GUI.ColorComp
            'Sohail (12 Oct 2018) -- End
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            cboJobReportTo.BackColor = GUI.ColorComp
            cboJobAdvert.BackColor = GUI.ColorComp
            'Sohail (29 Sep 2021) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            objlblPlanned.Text = Format(CInt(objStaffReq._Approved_Headcount), "0")
            objlblAvailable.Text = Format(CInt(objStaffReq._Actual_Headcount), "0")
            objlblVariation.Text = Format(CInt(objStaffReq._Approved_Headcount) - CInt(objStaffReq._Actual_Headcount), "0")
            'Sohail (12 Oct 2018) -- End
            txtFormNo.Text = objStaffReq._Formno
            cboStatus.SelectedValue = objStaffReq._Staffrequisitiontypeid
            cboAllocation.SelectedValue = objStaffReq._Staffrequisitionbyid
            cboAllocation_SelectedIndexChanged(Nothing, Nothing) 'Hemant (02 Jul 2019)
            cboName.SelectedValue = objStaffReq._Allocationunkid
            cboClassGroup.SelectedValue = objStaffReq._Classgroupunkid
            cboClass.SelectedValue = objStaffReq._Classunkid
            'cboEmployee.SelectedValue = objStaffReq._Employeeunkid 'Sohail (18 Mar 2015)
            cboLeavingReason.SelectedValue = objStaffReq._Actionreasonunkid
            txtAddStaffReason.Text = objStaffReq._Additionalstaffreason
            cboJob.SelectedValue = objStaffReq._Jobunkid
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
            'txtJobDesc.Text = objStaffReq._Jobdescrription
            If objStaffReq._Jobdescrription.StartsWith("{\rtf") Then
                txtJobDesc.Rtf = objStaffReq._Jobdescrription
            Else
                txtJobDesc.Text = objStaffReq._Jobdescrription
            End If
            cboGradeLevel.SelectedValue = objStaffReq._Gradelevelunkid
            cboEmploymentType.SelectedValue = objStaffReq._Employmenttypeunkid
            nudContractDuration.Value = objStaffReq._Contract_Duration_Months
            'Sohail (12 Oct 2018) -- End
            If menAction = enAction.EDIT_ONE Then
                cboAllocation.SelectedValue = objStaffReq._Staffrequisitionbyid
                dtpWorkStartDate.Value = objStaffReq._Workstartdate

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                nudPosition.Value = objStaffReq._Noofposition
                'Shani(24-Aug-2015) -- End

            End If

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            If objStaffReq._Gradeunkid > 0 Then
                cboGrade.SelectedValue = objStaffReq._Gradeunkid
            End If
            If objStaffReq._Gradelevelunkid > 0 Then
                cboGradeLevel.SelectedValue = objStaffReq._Gradelevelunkid
            End If
            'Sohail (12 Oct 2018) -- End
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            cboJobReportTo.SelectedValue = objStaffReq._Job_report_tounkid
            cboJobAdvert.SelectedValue = objStaffReq._Jobadvertid
            'Sohail (29 Sep 2021) -- End


            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
            mdtStaffRequisitionDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.STAFF_REQUISITION, mintStaffRequisitionUnkid, ConfigParameter._Object._Document_Path)
            If menAction <> enAction.EDIT_ONE Then
                If mdtStaffRequisitionDocument IsNot Nothing Then mdtStaffRequisitionDocument.Rows.Clear()
            End If
            'Gajanan [6-NOV-2019] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objStaffReq._Formno = txtFormNo.Text.Trim
            objStaffReq._Staffrequisitiontypeid = CInt(cboStatus.SelectedValue)
            objStaffReq._Staffrequisitionbyid = CInt(cboAllocation.SelectedValue)
            objStaffReq._Allocationunkid = CInt(cboName.SelectedValue)
            objStaffReq._Classgroupunkid = CInt(cboClassGroup.SelectedValue)
            objStaffReq._Classunkid = CInt(cboClass.SelectedValue)
            'objStaffReq._Employeeunkid = CInt(cboEmployee.SelectedValue) 'Sohail (18 Mar 2015)
            objStaffReq._Actionreasonunkid = CInt(cboLeavingReason.SelectedValue)
            objStaffReq._Additionalstaffreason = txtAddStaffReason.Text.Trim
            objStaffReq._Jobunkid = CInt(cboJob.SelectedValue)
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
            'objStaffReq._Jobdescrription = txtJobDesc.Text.Trim
            objStaffReq._Jobdescrription = txtJobDesc.Rtf.Trim
            objStaffReq._Gradeunkid = CInt(cboGrade.SelectedValue)
            objStaffReq._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
            objStaffReq._Employmenttypeunkid = CInt(cboEmploymentType.SelectedValue)
            objStaffReq._Contract_Duration_Months = nudContractDuration.Value
            objStaffReq._Approved_Headcount = CInt(objlblPlanned.Text)
            objStaffReq._Actual_Headcount = CInt(objlblAvailable.Text)
            If mintStaffRequisitionUnkid <= 0 Then
                objStaffReq._Requisition_Date = DateAndTime.Today.Date
            End If
            'Sohail (12 Oct 2018) -- End
            objStaffReq._Workstartdate = dtpWorkStartDate.Value
            objStaffReq._Noofposition = CInt(nudPosition.Value)
            objStaffReq._Userunkid = User._Object._Userunkid
            objStaffReq._CompanyUnkid = ConfigParameter._Object._Companyunkid 'Hemant (17 Jul 2019)
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            objStaffReq._Job_report_tounkid = CInt(cboJobReportTo.SelectedValue)
            objStaffReq._Jobadvertid = CInt(cboJobAdvert.SelectedValue)
            'Sohail (29 Sep 2021) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Combo()
        Dim objMaster As New clsMasterData
        'Dim objEmp As New clsEmployee_Master 'Sohail (18 Mar 2015)
        Dim objJob As New clsJobs
        Dim objClassGroup As New clsClassGroup
        Dim objReason As New clsAction_Reason
        'Sohail (12 Oct 2018) -- Start
        'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
        Dim objGrade As New clsGrade
        Dim objCommon As New clsCommon_Master
        'Sohail (12 Oct 2018) -- End
        Dim dsList As New DataSet
        Dim strFilter As String = ""
        Dim objStaffRequisition_approver_mapping As New clsStaffRequisition_approver_mapping 'Hemant (02 Jul 2019)
        Try

            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            'dsList = objMaster.GetEAllocation_Notification("List")
            dsList = objStaffRequisition_approver_mapping.GetOnlyMappedAllocatonList("List")
            'Hemant (29 Jul 2019) -- Start
            'ISSUE :  Error Occurs - "cboAllocation_SelectedIndexChanged:-Conversion from string "" to type Interger is not valid."  
            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList = objMaster.GetEAllocation_Notification("List")
            End If
            'Hemant (29 Jul 2019) -- End


            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
            Dim objCMaster As New clsCommon_Master
            'Gajanan [6-NOV-2019] -- End


            'Hemant (02 Jul 2019) -- End
            With cboAllocation
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedIndex = 0
            End With
            mdicAllocaions = (From p In dsList.Tables("List").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)
            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            dsList = objStaffRequisition_approver_mapping.GetLastMappedAllocatonRow("List")
            If (dsList.Tables(0).Rows.Count > 0) Then
                Dim intLastMappedAllocatonId = CInt(dsList.Tables(0).Rows(0).Item("allocationid").ToString)
                cboAllocation.SelectedValue = intLastMappedAllocatonId
                objStaffReq._Staffrequisitionbyid = intLastMappedAllocatonId
            End If
            'Hemant (02 Jul 2019) -- End

            dsList = objMaster.GetListForStaffRequisitionStatus(True, "Status")
            With cboStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Status")
                .SelectedValue = 0
            End With

            dsList = objReason.getComboList("Reason", True, False)
            With cboLeavingReason
                .ValueMember = "actionreasonunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Reason")
                .SelectedValue = 0
            End With

            'strFilter = " ( ISNULL(CONVERT(CHAR(8),termination_from_date,112),'" & eZeeDate.convertDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).AddDays(1)) & "') <= '" & ConfigParameter._Object._EmployeeAsOnDate & "' " & _
            '            "   OR ISNULL(CONVERT(CHAR(8),termination_to_date,112),'" & eZeeDate.convertDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).AddDays(1)) & "') <= '" & ConfigParameter._Object._EmployeeAsOnDate & "' " & _
            '            "   OR ISNULL(CONVERT(CHAR(8), empl_enddate,112), '" & eZeeDate.convertDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).AddDays(1)) & "') <= '" & ConfigParameter._Object._EmployeeAsOnDate & "' ) "

            'Sohail (18 Mar 2015) -- Start
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            'dsList = objEmp.GetList("Emp", True, False, , , , , , strFilter)
            'Dim dR As DataRow = dsList.Tables("Emp").NewRow
            'dR.Item("employeeunkid") = 0
            'dR.Item("name") = Language.getMessage(mstrModuleName, 1, " Select")
            'dsList.Tables("Emp").Rows.InsertAt(dR, 0)
            'With cboEmployee
            '    .ValueMember = "employeeunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("Emp")
            '    .SelectedValue = 0
            'End With
            'Sohail (18 Mar 2015) -- End


            dsList = objJob.getComboList("Job", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Job")
                .SelectedValue = 0
            End With

            dsList = objClassGroup.getComboList("ClassGroup", True)
            With cboClassGroup
                .ValueMember = "classgroupunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("ClassGroup")
                .SelectedValue = 0
            End With

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            dsList = objGrade.getComboList("Grade", True)
            With cboGrade
                .ValueMember = "gradeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Grade")
                .SelectedValue = 0
            End With

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "EmpType")
            With cboEmploymentType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("EmpType")
                .SelectedValue = 0
            End With
            'Sohail (12 Oct 2018) -- End


            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With
            'Gajanan [6-NOV-2019] -- End

            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            dsList = objMaster.getJobAdvert("List")
            With cboJobAdvert
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (29 Sep 2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsList.Dispose() : objMaster = Nothing : objJob = Nothing : objClassGroup = Nothing
        End Try
    End Sub

    'Sohail (18 Mar 2015) -- Start
    'Enhancement - Allow more than one employee to be replaced in staff requisition.
    Private Sub CreateTable()
        Try
            mdtList = New DataTable

            mdtList.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtList.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtList.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : For employees that have been transferred and their jobs have changed, provide option to see them on under the old job during replacement staff requisition.
            mdtList.Columns.Add("job_name", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (29 Sep 2021) -- End
            mdtList.Columns.Add("grade", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("gradelevel", System.Type.GetType("System.String")).DefaultValue = ""
            mdtList.Columns.Add("actionreasonunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtList.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateTable", mstrModuleName)
        End Try
    End Sub
    Private Sub FillList()
        Dim objEmp As New clsEmployee_Master
        Dim dsList As DataSet
        Dim strFilter As String = ""
        Dim objStaffEmp As New clsStaffrequisition_emp_tran
        Try

            mdtList.Rows.Clear()
            Cursor.Current = Cursors.WaitCursor 'Sohail (12 Oct 2018)

            objcolhCheckAll.DataPropertyName = "IsChecked"
            objcolhEmpID.DataPropertyName = "employeeunkid"
            colhEmpCode.DataPropertyName = "employeecode"
            colhEmpName.DataPropertyName = "name"
            colhGrade.DataPropertyName = "grade"
            colhSalaryBand.DataPropertyName = "gradelevel"
            objcolhActionReasonId.DataPropertyName = "actionreasonunkid"
            objcolhAUD.DataPropertyName = "AUD"
            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : For employees that have been transferred and their jobs have changed, provide option to see them on under the old job during replacement staff requisition.
            colhJob.DataPropertyName = "job_name"
            'Sohail (29 Sep 2021) -- End

            objStaffEmp._Staffrequisitiontranunkid = mintStaffRequisitionUnkid
            mdtOldTable = objStaffEmp._Datasource
            marrOldEmp.AddRange((From p In mdtOldTable Select (p.Item("employeeunkid").ToString)).ToArray)
            'Hemant (06 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            Dim strOldEmpIDs As String = String.Join(",", (From p In mdtOldTable Select (p.Item("employeeunkid").ToString)).ToArray())
            'Hemant (06 Aug 2019) -- End

            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 10 : When filing the additional staff reason and date to start work, the grade level populated earlier is getting reset. Forcing user to select it again. – TC001. The grade level and the employee name selected and the name of the allocation are getting reset when filling out the additional staff reason textbox.)
            If mblnIsSelectedEmployeeListChanged = False Then
                marrSelectedEmployeeList.AddRange((From p In mdtOldTable Select (p.Item("employeeunkid").ToString)).ToArray)
            End If
            'Hemant (13 Sep 2019) -- End

            If CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.ADDITIONAL OrElse CInt(cboJob.SelectedValue) <= 0 OrElse CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.ADDITIONAL Then Exit Try

            'strFilter = " ( ISNULL(CONVERT(CHAR(8),termination_from_date,112),'" & eZeeDate.convertDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).AddDays(1)) & "') <= '" & ConfigParameter._Object._EmployeeAsOnDate & "' " & _
            '            "   OR ISNULL(CONVERT(CHAR(8),termination_to_date,112),'" & eZeeDate.convertDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).AddDays(1)) & "') <= '" & ConfigParameter._Object._EmployeeAsOnDate & "' " & _
            '            "   OR ISNULL(CONVERT(CHAR(8), empl_enddate,112), '" & eZeeDate.convertDate(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).AddDays(1)) & "') <= '" & ConfigParameter._Object._EmployeeAsOnDate & "' ) "


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CInt(cboJob.SelectedValue) > 0 Then
            '    If strFilter.Trim = "" Then
            '        strFilter = " hremployee_master.jobunkid = " & CInt(cboJob.SelectedValue) & " "
            '    Else
            '        strFilter &= " AND hremployee_master.jobunkid = " & CInt(cboJob.SelectedValue) & " "
            '    End If
            'End If

            'dsList = objEmp.GetList("Emp", True, False, , , , , , strFilter)

            If CInt(cboJob.SelectedValue) > 0 Then
                'Sohail (29 Sep 2021) -- Start
                'NMB Enhancement : OLD-486 : For employees that have been transferred and their jobs have changed, provide option to see them on under the old job during replacement staff requisition.
                'If strFilter.Trim = "" Then
                '    strFilter = " ERECAT.jobunkid = " & CInt(cboJob.SelectedValue) & " "
                'Else
                '    strFilter &= " AND ERECAT.jobunkid = " & CInt(cboJob.SelectedValue) & " "
                'End If
                strFilter &= " AND (ERECAT.jobunkid = " & CInt(cboJob.SelectedValue) & " OR PrevRecat.jobunkid = " & CInt(cboJob.SelectedValue) & " ) "
                'Sohail (29 Sep 2021) -- End
            End If

            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : For employees that have been transferred and their jobs have changed, provide option to see them on under the old job during replacement staff requisition.
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(4)
            'Sohail (29 Sep 2021) -- End

            dsList = objEmp.GetList(FinancialYear._Object._DatabaseName, _
                                    User._Object._Userunkid, _
                                    FinancialYear._Object._YearUnkid, _
                                    Company._Object._Companyunkid, _
                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                    True, _
                                    "Emp", _
                                    ConfigParameter._Object._ShowFirstAppointmentDate, , , strFilter, blnIncludePreviousJob:=True)
            'Sohail (29 Sep 2021) - [blnIncludePreviousJob]

            'Hemant (06 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            Dim strFilterRows As String = String.Empty

            If chkShowInActiveEmployee.Checked = True Then
                strFilterRows &= "  ISNULL(termination_from_date,'') <> " & "''"
            Else
                strFilterRows &= "  ISNULL(termination_from_date,'') = " & "''"
            End If

            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : For employees that have been transferred and their jobs have changed, provide option to see them on under the old job during replacement staff requisition.
            If CInt(cboJob.SelectedValue) > 0 AndAlso chkShowPreviousJobEmp.Checked = True Then
                strFilterRows &= " AND Prevjobunkid = " & CInt(cboJob.SelectedValue) & " "
            ElseIf CInt(cboJob.SelectedValue) > 0 AndAlso chkShowPreviousJobEmp.Checked = False Then
                strFilterRows &= " AND Jobunkid = " & CInt(cboJob.SelectedValue) & " "
            End If
            'Sohail (29 Sep 2021) -- End

            If strOldEmpIDs.Trim.Length > 0 Then
                If strFilterRows.Trim.Length > 0 Then
                    strFilterRows = "( " & strFilterRows & " ) " 'Sohail (29 Sep 2021)
                    strFilterRows &= "OR employeeunkid in ( " & strOldEmpIDs & " ) "
                End If
            End If
            'Hemant (06 Aug 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- END

            Dim dRow As DataRow = Nothing
            'Hemant (06 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            'For Each dtRow As DataRow In dsList.Tables("Emp").Rows
            For Each dtRow As DataRow In dsList.Tables("Emp").Select(strFilterRows)
                'Hemant (06 Aug 2019) -- End
                dRow = mdtList.NewRow

                dRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                dRow.Item("employeecode") = dtRow.Item("employeecode").ToString
                dRow.Item("name") = dtRow.Item("name").ToString
                dRow.Item("grade") = dtRow.Item("grade").ToString
                dRow.Item("gradelevel") = dtRow.Item("gradelevel").ToString
                dRow.Item("actionreasonunkid") = CInt(dtRow.Item("actionreasonunkid"))
                'Sohail (29 Sep 2021) -- Start
                'NMB Enhancement : OLD-486 : For employees that have been transferred and their jobs have changed, provide option to see them on under the old job during replacement staff requisition.
                dRow.Item("job_name") = dtRow.Item("job_name").ToString
                'Sohail (29 Sep 2021) -- End

                'Hemant (13 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 10 : When filing the additional staff reason and date to start work, the grade level populated earlier is getting reset. Forcing user to select it again. – TC001. The grade level and the employee name selected and the name of the allocation are getting reset when filling out the additional staff reason textbox.)
                'If marrOldEmp.Contains(dtRow.Item("employeeunkid").ToString) = True Then
                If (marrSelectedEmployeeList.ToArray.Distinct).ToArray.Contains(dtRow.Item("employeeunkid").ToString) = True Then
                    'Hemant (13 Sep 2019) -- End
                    dRow.Item("IsChecked") = True
                Else
                    dRow.Item("IsChecked") = False
                End If

                mdtList.Rows.Add(dRow)

            Next


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            mdtView = mdtList.DefaultView
            dgvEmployeeList.DataSource = mdtView
            dgvEmployeeList.Refresh()

            objEmp = Nothing
            Cursor.Current = Cursors.Default 'Sohail (12 Oct 2018)
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            For Each dView As DataRowView In mdtView
                RemoveHandler dgvEmployeeList.CellContentClick, AddressOf dgvEmployeeList_CellContentClick
                dView.Item("IsChecked") = blnCheckAll
                dView.EndEdit()
                AddHandler dgvEmployeeList.CellContentClick, AddressOf dgvEmployeeList_CellContentClick
            Next
            mdtView.ToTable.AcceptChanges()
            'Sohail (15 Oct 2021) -- Start
            'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
            'nudPosition.Value = mdtView.ToTable.Select("IsChecked = 1 ").Length
            nudPosition.Value = mdtView.Table.Select("IsChecked = 1 ").Length
            'Sohail (15 Oct 2021) -- End
            'Hemant (13 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 10 : When filing the additional staff reason and date to start work, the grade level populated earlier is getting reset. Forcing user to select it again. – TC001. The grade level and the employee name selected and the name of the allocation are getting reset when filling out the additional staff reason textbox.)
            marrSelectedEmployeeList.Clear()
            'Sohail (15 Oct 2021) -- Start
            'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
            'marrSelectedEmployeeList.AddRange((From p In mdtView.ToTable.Select("IsChecked = 1") Select (p.Item("employeeunkid").ToString)).ToArray)
            marrSelectedEmployeeList.AddRange((From p In mdtView.Table.Select("IsChecked = 1") Select (p.Item("employeeunkid").ToString)).ToArray)
            'Sohail (15 Oct 2021) -- End
            mblnIsSelectedEmployeeListChanged = True
            'Hemant (13 Sep 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Mar 2015) -- End

    Private Function IsValid() As Boolean
        Try
            If ConfigParameter._Object._StaffReqFormNoType = 0 AndAlso txtFormNo.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry! Staff Requisition Form No. cannot be blank. Staff Requisition Form No. is mandatory information."), enMsgBoxStyle.Information)
                txtFormNo.Focus()
                Return False
            ElseIf CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Requisition Type."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False
            ElseIf CInt(cboAllocation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Requisition By."), enMsgBoxStyle.Information)
                cboAllocation.Focus()
                Return False
            ElseIf CInt(cboName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select") & " " & lblName.Text, enMsgBoxStyle.Information)
                cboName.Focus()
                Return False
            ElseIf CInt(cboJob.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Job Title."), enMsgBoxStyle.Information)
                cboJob.Focus()
                Return False
                'Sohail (29 Sep 2021) -- Start
                'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            ElseIf CInt(cboJobReportTo.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Please select Job Reporting Line."), enMsgBoxStyle.Information)
                cboJobReportTo.Focus()
                Return False
            ElseIf CInt(cboJobAdvert.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "Please select Job Advert."), enMsgBoxStyle.Information)
                cboJobAdvert.Focus()
                Return False
                'Sohail (29 Sep 2021) -- End
                'Sohail (18 Mar 2015) -- Start
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
                'ElseIf CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select Employee to be Replaced."), enMsgBoxStyle.Information)
                '    cboEmployee.Focus()
                '    Return False
                'ElseIf CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT AndAlso CInt(cboLeavingReason.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Leaving Reason."), enMsgBoxStyle.Information)
                '    cboLeavingReason.Focus()
                '    Return False
                'Sohail (15 Oct 2021) -- Start
                'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
                'ElseIf (CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT OrElse CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT_AND_ADDITIONAL) AndAlso mdtView.ToTable.Select("IsChecked = 1 ").Length <= 0 Then
            ElseIf (CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT OrElse CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT_AND_ADDITIONAL) AndAlso mdtView.Table.Select("IsChecked = 1 ").Length <= 0 Then
                'Sohail (15 Oct 2021) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select atleast one Employee to be Replaced."), enMsgBoxStyle.Information)
                dgvEmployeeList.Focus()
                Return False
            ElseIf (CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT OrElse CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT_AND_ADDITIONAL) AndAlso CInt(cboLeavingReason.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select Leaving Reason."), enMsgBoxStyle.Information)
                cboLeavingReason.Focus()
                Return False
                'Sohail (18 Mar 2015) -- End

                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            ElseIf CInt(cboGrade.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Please select Grade."), enMsgBoxStyle.Information)
                cboGrade.Focus()
                Return False

            ElseIf CInt(cboGradeLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Please select Grade Level."), enMsgBoxStyle.Information)
                cboGradeLevel.Focus()
                Return False

            ElseIf CInt(cboEmploymentType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Please select Employment Type."), enMsgBoxStyle.Information)
                cboEmploymentType.Focus()
                Return False
                'Sohail (12 Oct 2018) -- End

                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-516 - They want below fields made mandatory on staff requisition form ( Reporting Line, Advertising internal only or internal and external in parallel, Number of positions, Contract period.
            ElseIf nudContractDuration.Enabled = True AndAlso nudContractDuration.Value <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "Sorry, No. of Contract Duration should be greater than Zero."), enMsgBoxStyle.Information)
                nudContractDuration.Focus()
                Return False
                'Hemant (01 Nov 2021) -- End

            ElseIf dtpWorkStartDate.Value.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Date to Start Work should be greather than current date."), enMsgBoxStyle.Information)
                dtpWorkStartDate.Focus()
                Return False
                'Sohail (18 Mar 2015) -- Start
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
            ElseIf nudPosition.Value <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, No. of Position should be greater than Zero."), enMsgBoxStyle.Information)
                nudPosition.Focus()
                Return False
                'Sohail (18 Mar 2015) -- End
            End If

            'Sohail (16 Dec 2021) -- Start
            'Enhancement : : NMB - Allow to create staff requisition if class is not same.
            Dim objGrpMast As New clsGroup_Master
            objGrpMast._Groupunkid = 1
            If objGrpMast._Groupname.ToString.ToUpper = "NMB PLC" Then
                If CInt(cboClassGroup.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Please select Class Group."), enMsgBoxStyle.Information)
                    cboClassGroup.Focus()
                    Return False
                ElseIf CInt(cboClass.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Please select Class."), enMsgBoxStyle.Information)
                    cboClass.Focus()
                    Return False
                End If
            End If
            objGrpMast = Nothing
            'Sohail (16 Dec 2021) -- End

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - System not to allow raising a staff requisition on a position if manpower planning on that position is not planned in 75.1.
            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(During staff requisition, remove the condition that requires manpower planning must be done and provide a configurable option that can be turned and off depending on the needs).
            If ConfigParameter._Object._EnforceManpowerPlanning = True Then


                'Gajanan [16-NOV-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                Dim intCheckedCount As Integer = 0
                If mdtView IsNot Nothing Then
                    'Sohail (15 Oct 2021) -- Start
                    'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
                    'intCheckedCount = mdtView.ToTable.Select("IsChecked = 1 ").Length
                    intCheckedCount = mdtView.Table.Select("IsChecked = 1 ").Length
                    'Sohail (15 Oct 2021) -- End
                End If
                'Gajanan [16-NOV-2019] -- End   


            'Hemant (03 Sep 2019) -- End

                'Gajanan [16-NOV-2019] -- Start 
                'If (CInt(objlblVariation.Text) + mdtView.ToTable.Select("IsChecked = 1 ").Length) < CInt(nudPosition.Value) Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry, No. of position should not be greater than Manpower variations.") & " [" & (CInt(objlblVariation.Text) + mdtView.ToTable.Select("IsChecked = 1 ").Length) & "]. " & Language.getMessage(mstrModuleName, 28, "Please add vacancies on Job master screen."), enMsgBoxStyle.Information)
                '    Return False
                'End If

                If (CInt(objlblVariation.Text) + intCheckedCount) < CInt(nudPosition.Value) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry, No. of position should not be greater than Manpower variations.") & " [" & (CInt(objlblVariation.Text) + intCheckedCount) & "]. " & Language.getMessage(mstrModuleName, 28, "Please add vacancies on Job master screen."), enMsgBoxStyle.Information)
                    Return False
                End If

                'Gajanan [16-NOV-2019] -- End   


            End If
            'Hemant (03 Sep 2019)

            If mintStaffRequisitionUnkid > 0 AndAlso objStaffReq.isUsed(mintStaffRequisitionUnkid) = True Then
                Dim objStaffReqApp As New clsStaffrequisition_approval_Tran
                Dim ds As DataSet = objStaffReqApp.GetList("List", User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, , mintStaffRequisitionUnkid.ToString(), , , , , True, , True, CInt(enApprovalStatus.PENDING))

                If ds.Tables(0).Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, Staff Requisition approval process is in progress and it is not approved by previous level user yet."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'Sohail (12 Oct 2018) -- End

            If gobjEmailList.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sending Email(s) process is in progress from other module. Please wait."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._FormName = obj._FormName
                    objSendMail._ClientIP = getIP()
                    objSendMail._HostName = getHostName()
                    objSendMail._FromWeb = False
                    objSendMail._AuditUserId = User._Object._Userunkid
objSendMail._CompanyUnkid = Company._Object._Companyunkid
                    objSendMail._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    objSendMail._LoginEmployeeunkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception

                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub


    'Gajanan [6-NOV-2019] -- Start    
    'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtStaffRequisitionDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtStaffRequisitionDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = -1
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Staff_Requisition
                dRow("scanattachrefid") = enScanAttactRefId.STAFF_REQUISITION
                dRow("form_name") = mstrForm_Name
                dRow("userunkid") = User._Object._Userunkid
                dRow("transactionunkid") = mintStaffRequisitionUnkid
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = DateTime.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            mdtStaffRequisitionDocument.Rows.Add(dRow)
            Call FillStaffRequisitionAttachment()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddDocumentAttachment", mstrModuleName)
        End Try
    End Sub

    Private Sub FillStaffRequisitionAttachment()
        Dim dtView As DataView
        Try
            If mdtStaffRequisitionDocument Is Nothing Then Exit Sub

            dtView = New DataView(mdtStaffRequisitionDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvStaffRequisitionAttachment.AutoGenerateColumns = False
            colhName.DataPropertyName = "filename"
            colhSize.DataPropertyName = "filesize"
            objcolhGUID.DataPropertyName = "GUID"
            objcolhScanUnkId.DataPropertyName = "scanattachtranunkid"
            dgvStaffRequisitionAttachment.DataSource = dtView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillStaffRequisitionAttachment", mstrModuleName)
        End Try
    End Sub
    'Gajanan [6-NOV-2019] -- End
#End Region

#Region " Form's Event "

    Private Sub frmStaffRequisition_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        objStaffReq = New clsStaffrequisition_Tran

        'Gajanan [6-NOV-2019] -- Start    
        'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
        objDocument = New clsScan_Attach_Documents
        'Gajanan [6-NOV-2019] -- End

        Try
            Call Set_Logo(Me, gApplicationType)

            Call CreateTable() 'Sohail (18 Mar 2015)
            Call Fill_Combo()

            If ConfigParameter._Object._StaffReqFormNoType = 1 Then
                txtFormNo.Enabled = False
            Else
                txtFormNo.Enabled = True
            End If

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call setColor()
            If menAction = enAction.EDIT_ONE Then
                objStaffReq._Staffrequisitiontranunkid = mintStaffRequisitionUnkid
                'Sohail (18 Mar 2015) -- Start
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
                cboStatus.Enabled = False
                cboJob.Enabled = False
                'Sohail (18 Mar 2015) -- End
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                objbtnSearchJobTitle.Enabled = False
                'Sohail (12 Oct 2018) -- End
            End If
            'Hemant (17 Oct 2019) -- Start
            lnkViewJobDescription.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.JobReport, User._Object._Userunkid, Company._Object._Companyunkid)
            'Hemant (17 Oct 2019) -- End
            GetValue()


            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
            Call FillStaffRequisitionAttachment()
            mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.STAFF_REQUISITION).Tables(0).Rows(0)("Name").ToString
            'Gajanan [6-NOV-2019] -- End

            cboStatus.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffRequisition_AddEdit_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub frmStaffRequisition_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            ElseIf e.KeyCode = Keys.Return Then
                SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffRequisition_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStaffRequisition_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objStaffReq = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsStaffrequisition_Tran.SetMessages()
            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 15 : Provide all recruitment notifications on language. They keep on changing on the contents they want.)
            'objfrm._Other_ModuleNames = "clsStaffrequisition_Tran"
            clsStaffrequisition_approval_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsStaffrequisition_Tran,clsStaffrequisition_approval_Tran"
            'Hemant (15 Nov 2019) -- End
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Try


            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
            'If ConfigParameter._Object._StaffRequisitionDocsAttachmentMandatory Then
            '    If mdtStaffRequisitionDocument Is Nothing OrElse mdtStaffRequisitionDocument.Select("AUD <> 'D'").Length <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), enMsgBoxStyle.Information)
            '        Exit Sub
            '    End If
            'End If


            'Gajanan [15-NOV-2019] -- Start   
            'Enhancement:Worked On Staff Requisition New Attachment Mandatory Option FOR NMB   


            If (ConfigParameter._Object._AdditionalStaffRequisitionDocsAttachmentMandatory AndAlso CType(cboStatus.SelectedValue, enStaffRequisition_Status) = enStaffRequisition_Status.ADDITIONAL) OrElse _
               (ConfigParameter._Object._AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory AndAlso CType(cboStatus.SelectedValue, enStaffRequisition_Status) = enStaffRequisition_Status.REPLACEMENT_AND_ADDITIONAL) OrElse _
               (ConfigParameter._Object._ReplacementStaffRequisitionDocsAttachmentMandatory AndAlso CType(cboStatus.SelectedValue, enStaffRequisition_Status) = enStaffRequisition_Status.REPLACEMENT) Then

                If mdtStaffRequisitionDocument Is Nothing OrElse mdtStaffRequisitionDocument.Select("AUD <> 'D'").Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), enMsgBoxStyle.Information)
                    Exit Sub
                End If

            End If



            'Gajanan [15-NOV-2019] -- End


            'Gajanan [6-NOV-2019] -- End

            Call SetValue()

            'Sohail (18 Mar 2015) -- Start
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            mdtNewTable = mdtOldTable.Clone

            Dim marrNewEmp As New ArrayList
            'Sohail (15 Oct 2021) -- Start
            'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
            'marrNewEmp.AddRange((From p In mdtView.ToTable.Select("IsChecked = 1 ").AsEnumerable Select (p.Item("employeeunkid").ToString)).ToArray)
            marrNewEmp.AddRange((From p In mdtView.Table.Select("IsChecked = 1 ").AsEnumerable Select (p.Item("employeeunkid").ToString)).ToArray)
            'Sohail (15 Oct 2021) -- End

            Dim add() As String = (From p In marrNewEmp Where (marrOldEmp.Contains(p.ToString) = False) Select (p.ToString)).ToArray
            Dim del() As String = (From p In marrOldEmp Where (marrNewEmp.Contains(p.ToString) = False) Select (p.ToString)).ToArray

            Dim dr_Row As List(Of DataRow) = Nothing
            Dim dRow As DataRow

            dr_Row = (From p In mdtOldTable Where (del.Contains(p.Item("employeeunkid").ToString) = True)).ToList

            If dr_Row IsNot Nothing Then
                For Each row As DataRow In dr_Row
                    dRow = mdtNewTable.NewRow

                    dRow.ItemArray = row.ItemArray
                    dRow.Item("AUD") = "D"

                    mdtNewTable.Rows.Add(dRow)
                Next
            End If

            'Sohail (15 Oct 2021) -- Start
            'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
            'Dim dr As List(Of DataRow) = (From p In mdtView.ToTable.Select("IsChecked = 1 ").AsEnumerable Where (add.Contains(p.Item("employeeunkid").ToString) = True)).ToList
            Dim dr As List(Of DataRow) = (From p In mdtView.Table.Select("IsChecked = 1 ").AsEnumerable Where (add.Contains(p.Item("employeeunkid").ToString) = True)).ToList
            'Sohail (15 Oct 2021) -- End

            If dr IsNot Nothing Then
                For Each dtRow In dr
                    dRow = mdtNewTable.NewRow

                    dRow.Item("IsChecked") = True
                    dRow.Item("staffrequisitionemptranunkid") = -1
                    dRow.Item("staffrequisitiontranunkid") = mintStaffRequisitionUnkid
                    dRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                    dRow.Item("userunkid") = User._Object._Userunkid
                    dRow.Item("AUD") = "A"

                    mdtNewTable.Rows.Add(dRow)
                Next
            End If
            'Sohail (18 Mar 2015) -- End

'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objStaffReq._FormName = mstrModuleName
            objStaffReq._LoginEmployeeUnkid = 0
            objStaffReq._ClientIP = getIP()
            objStaffReq._HostName = getHostName()
            objStaffReq._FromWeb = False
            objStaffReq._AuditUserId = User._Object._Userunkid
objStaffReq._CompanyUnkid = Company._Object._Companyunkid
            objStaffReq._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END



            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
            If mdtStaffRequisitionDocument.Rows.Count > 0 Then

                Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                Dim strError As String = ""

                For Each drdoc_Row As DataRow In mdtStaffRequisitionDocument.Rows
                    If drdoc_Row("AUD").ToString = "A" AndAlso drdoc_Row("localpath").ToString <> "" Then
                        Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(drdoc_Row("localpath")))
                        If blnIsIISInstalled Then
                            If clsFileUploadDownload.UploadFile(CStr(drdoc_Row("localpath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            Else
                                Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                drdoc_Row("fileuniquename") = strFileName
                                If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                    strPath += "/"
                                End If
                                strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                                drdoc_Row("filepath") = strPath
                                drdoc_Row.AcceptChanges()
                            End If
                        Else
                            If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                    Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                                End If
                                File.Copy(CStr(drdoc_Row("localpath")), strDocLocalPath, True)
                                drdoc_Row("fileuniquename") = strFileName
                                drdoc_Row("filepath") = strDocLocalPath
                                drdoc_Row.AcceptChanges()
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    ElseIf drdoc_Row("AUD").ToString = "D" AndAlso drdoc_Row("fileuniquename").ToString <> "" Then

                        Dim strFileName As String = drdoc_Row("fileuniquename").ToString
                        If blnIsIISInstalled Then
                            If clsFileUploadDownload.DeleteFile(drdoc_Row("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        Else
                            If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If File.Exists(strDocLocalPath) Then
                                    File.Delete(strDocLocalPath)
                                End If
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If
            'Gajanan [6-NOV-2019] -- End

            If menAction = enAction.EDIT_ONE Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objStaffReq.Update(, mdtNewTable)


                'Gajanan [6-NOV-2019] -- Start    
                'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
                'blnFlag = objStaffReq.Update(ConfigParameter._Object._CurrentDateAndTime, , mdtNewTable)
                blnFlag = objStaffReq.Update(ConfigParameter._Object._CurrentDateAndTime, , mdtNewTable, mdtStaffRequisitionDocument)
                'Gajanan [6-NOV-2019] -- End
                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objStaffReq.Insert(mdtNewTable)

                'Gajanan [6-NOV-2019] -- Start    
                'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   

                'blnFlag = objStaffReq.Insert(ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._StaffReqFormNoType, ConfigParameter._Object._StaffReqFormNoPrefix, mdtNewTable)
                blnFlag = objStaffReq.Insert(ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._StaffReqFormNoType, ConfigParameter._Object._StaffReqFormNoPrefix, mdtNewTable, mdtStaffRequisitionDocument)
                'Sohail (21 Aug 2015) -- End

                'Gajanan [6-NOV-2019] -- End

            End If

            If blnFlag = False And objStaffReq._Message <> "" Then
                eZeeMsgBox.Show(objStaffReq._Message, enMsgBoxStyle.Information)
            Else
                If menAction <> enAction.EDIT_ONE Then
                    Dim objStaffReqApproval As New clsStaffrequisition_approval_Tran
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objStaffReqApproval.SendMailToApprover(enApprovalStatus.PENDING, User._Object._Userunkid, CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), cboJob.Text, 0, objStaffReq._Staffrequisitiontranunkid, txtJobDesc.Text.Trim, , enLogin_Mode.DESKTOP, 0)
                    'Sohail (13 Jun 2016) -- Start
                    'Enhancement - 61.1 - User access on On Staff Requisition Final Approved, All approvers are receiving Notifications even if department/branch not in his/her user access.
                    'objStaffReqApproval.SendMailToApprover(enApprovalStatus.PENDING, User._Object._Userunkid, CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), cboJob.Text, 0, objStaffReq._Staffrequisitiontranunkid, txtJobDesc.Text.Trim, Company._Object._Companyunkid, ConfigParameter._Object._IsArutiDemo, ConfigParameter._Object._ArutiSelfServiceURL, enLogin_Mode.DESKTOP, 0)

With objStaffReqApproval
                        ._FormName = mstrModuleName
                        ._LoginEmployeeUnkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With

                    'Sohail (11 Aug 2021) -- Start
                    'NMB Enhancement :  : The staff requisition requests are sent to all level 1 approvers (respective cheifs) despite them not having access to the respective function.
                    'objStaffReqApproval.SendMailToApprover(enApprovalStatus.PENDING, User._Object._Userunkid, CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), cboJob.Text, 0, objStaffReq._Staffrequisitiontranunkid, txtJobDesc.Text.Trim, Company._Object._Companyunkid, ConfigParameter._Object._IsArutiDemo, ConfigParameter._Object._ArutiSelfServiceURL, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, enLogin_Mode.DESKTOP, 0)
                    objStaffReqApproval.SendMailToApprover(enApprovalStatus.PENDING, User._Object._Userunkid, CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), cboJob.Text, 0, objStaffReq._Staffrequisitiontranunkid, txtJobDesc.Text.Trim, Company._Object._Companyunkid, ConfigParameter._Object._IsArutiDemo, ConfigParameter._Object._ArutiSelfServiceURL, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, FinancialYear._Object._DatabaseName, ConfigParameter._Object._EmployeeAsOnDate, enLogin_Mode.DESKTOP, 0)
                    'Sohail (11 Aug 2021) -- End
                    'Sohail (13 Jun 2016) -- End
                    'Sohail (21 Aug 2015) -- End

                    trd = New Thread(AddressOf Send_Notification)
                    trd.IsBackground = True
                    trd.Start()

                End If
            End If

            If blnFlag Then
                mblnCancel = False
                'Hemant (06 Aug 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Staff Requisition Saved Successfully."), enMsgBoxStyle.Information)
                'Hemant (06 Aug 2019) -- End
                If menAction = enAction.ADD_CONTINUE Then
                    objStaffReq = Nothing
                    objStaffReq = New clsStaffrequisition_Tran
                    Call GetValue()
                    cboStatus.Focus()
                Else
                    mintStaffRequisitionUnkid = objStaffReq._Staffrequisitiontranunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


    Private Sub objbtnLeavingReason_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLeavingReason.Click
        Dim frm As New frmAction_Reason_AddEdit
        Dim intRefId As Integer = -1
        Try
            If frm.displayDialog(intRefId, enAction.ADD_ONE, False) Then
                Dim dsList As New DataSet
                Dim objReason As New clsAction_Reason
                dsList = objReason.getComboList("Reason", True, False)
                With cboLeavingReason
                    .ValueMember = "actionreasonunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("Reason")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddReason_Click", mstrModuleName)
        End Try
    End Sub

    'Nilay (20 Mar 2017) -- Start
    Private Sub objbtnSearchClassGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchClassGroup.Click
        Try
            Dim objFrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboClassGroup.ValueMember
                .DisplayMember = cboClassGroup.DisplayMember
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                '.CodeMember = "code"
                .CodeMember = "code"
                'Sohail (12 Oct 2018) -- End
                .DataSource = CType(cboClassGroup.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboClassGroup.SelectedValue = objFrm.SelectedValue
            End If
            cboClassGroup.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchClassGroup_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchClass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchClass.Click
        Try
            Dim objFrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboClass.ValueMember
                .DisplayMember = cboClass.DisplayMember
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                '.CodeMember = "code"
                .CodeMember = "code"
                'Sohail (12 Oct 2018) -- End
                .DataSource = CType(cboClass.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboClass.SelectedValue = objFrm.SelectedValue
            End If
            cboClass.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchClass_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchJobTitle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJobTitle.Click
        Try
            Dim objFrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboJob.ValueMember
                .DisplayMember = cboJob.DisplayMember
                '.CodeMember = "code"
                .DataSource = CType(cboJob.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboJob.SelectedValue = objFrm.SelectedValue
            End If
            cboJob.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJobTitle_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (20 Mar 2017) -- End

    'Sohail (29 Sep 2021) -- Start
    'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
    Private Sub objbtnSearchJobReportingTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJobReportingTo.Click
        Try
            Dim objFrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboJobReportTo.ValueMember
                .DisplayMember = cboJobReportTo.DisplayMember
                '.CodeMember = "code"
                .DataSource = CType(cboJobReportTo.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboJobReportTo.SelectedValue = objFrm.SelectedValue
            End If
            cboJobReportTo.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJobReportingTo_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Sep 2021) -- End

    'Sohail (12 Oct 2018) -- Start
    'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
    Private Sub objbtnSearchGrade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGrade.Click
        Try
            Dim objFrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboGrade.ValueMember
                .DisplayMember = cboGrade.DisplayMember
                '.CodeMember = "code"
                .DataSource = CType(cboGrade.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboGrade.SelectedValue = objFrm.SelectedValue
            End If
            cboGrade.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGrade_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchGradeLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchGradeLevel.Click
        Try
            Dim objFrm As New frmCommonSearch

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboGradeLevel.ValueMember
                .DisplayMember = cboGradeLevel.DisplayMember
                '.CodeMember = "code"
                .DataSource = CType(cboGradeLevel.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboGradeLevel.SelectedValue = objFrm.SelectedValue
            End If
            cboGradeLevel.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchGradeLevel_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Oct 2018) -- End

    'Gajanan [6-NOV-2019] -- Start    
    'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
    Private Sub btnAddAttachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAttachment.Click
        Try
            If IsValid() = False Then Exit Try

            If CInt(cboDocumentType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Document Type is compulsory information. Please select Document Type to continue."), enMsgBoxStyle.Information)
                cboDocumentType.Focus()
                Exit Sub
            End If

            If (ofdAttachment.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                Dim f As New System.IO.FileInfo(ofdAttachment.FileName)
                Call AddDocumentAttachment(f, ofdAttachment.FileName.ToString)
                f = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddAttachment_Click :", mstrModuleName)
        End Try
    End Sub
    'Gajanan [6-NOV-2019] -- End
#End Region

#Region " Combobox's Events "

    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Dim dsCombos As DataSet = Nothing
        Dim strName As String = ""

        Try

            Select Case CInt(cboAllocation.SelectedValue)

                Case enAllocation.BRANCH
                    dsCombos = objStation.getComboList("Station", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 11, "Branch")
                Case enAllocation.DEPARTMENT_GROUP
                    dsCombos = objDeptGrp.getComboList("DeptGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 12, "Department Group")
                Case enAllocation.DEPARTMENT
                    dsCombos = objDepartment.getComboList("Department", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 13, "Department")
                Case enAllocation.SECTION_GROUP
                    dsCombos = objSectionGrp.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 14, "Section Group")
                Case enAllocation.SECTION
                    dsCombos = objSection.getComboList("Section", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 15, "Section")
                Case enAllocation.UNIT_GROUP
                    dsCombos = objUnitGroup.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 16, "Unit Group")
                Case enAllocation.UNIT
                    dsCombos = objUnit.getComboList("Unit", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 17, "Unit")
                Case enAllocation.TEAM
                    dsCombos = objTeam.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 18, "Team")
                Case enAllocation.JOB_GROUP
                    dsCombos = objJobGrp.getComboList("JobGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 19, "Job Group")
                Case enAllocation.JOBS
                    dsCombos = objJob.getComboList("Job", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 20, "Jobs")
                Case enAllocation.CLASS_GROUP
                    dsCombos = objClassGrp.getComboList("ClassGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 21, "Class Group")
                Case enAllocation.CLASSES
                    dsCombos = objClass.getComboList("Class", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 22, "Classes")

            End Select

            lblName.Text = strName
            If dsCombos IsNot Nothing Then

                With cboName
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables(0)
                    .SelectedValue = 0
                End With
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocation_SelectedIndexChanged", mstrModuleName)
        Finally
            objStation = Nothing
            objDeptGrp = Nothing
            objDepartment = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objClassGrp = Nothing
            objClass = Nothing
            objSectionGrp = Nothing
            objUnitGroup = Nothing
            objTeam = Nothing
            dsCombos = Nothing
        End Try
    End Sub

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.ADDITIONAL Then
                txtAddStaffReason.Enabled = True
                'Sohail (18 Mar 2015) -- Start
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
                'cboEmployee.SelectedValue = 0
                'cboEmployee.Enabled = False
                'objbtnSearchEmp.Enabled = False
                'objtlpEmployee.Visible = False
                nudPosition.Enabled = True
                dgvEmployeeList.Enabled = False
                If objchkSelectAll.CheckState <> CheckState.Unchecked Then objchkSelectAll.Checked = False
                objchkSelectAll.Enabled = False
                cboLeavingReason.SelectedValue = 0
                cboLeavingReason.Enabled = False
                objbtnLeavingReason.Enabled = False
                'Sohail (18 Mar 2015) -- End
            Else
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) - Reason for requesting staff. The reason section should be active regardless of the staff requisition type selected in 75.1.
                'txtAddStaffReason.Text = ""
                'txtAddStaffReason.Enabled = False
                txtAddStaffReason.Enabled = True
                'Sohail (12 Oct 2018) -- End
                'Sohail (18 Mar 2015) -- Start
                'Enhancement - Allow more than one employee to be replaced in staff requisition.
                'cboEmployee.Enabled = True
                'objtlpEmployee.Visible = True
                'objbtnSearchEmp.Enabled = True
                If CInt(cboStatus.SelectedValue) = enStaffRequisition_Status.REPLACEMENT_AND_ADDITIONAL Then
                    nudPosition.Enabled = True
                Else
                    nudPosition.Enabled = False
                End If
                dgvEmployeeList.Enabled = True
                objchkSelectAll.Enabled = True
                cboLeavingReason.Enabled = True
                objbtnLeavingReason.Enabled = True
                'Sohail (18 Mar 2015) -- End
            End If
            Call FillList() 'Sohail (18 Mar 2015)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (18 Mar 2015) -- Start
    'Enhancement - Allow more than one employee to be replaced in staff requisition.
    'Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If CInt(cboEmployee.SelectedValue) > 0 Then
    '            objlblGrade.Text = CType(cboEmployee.SelectedItem, DataRowView).Item("grade").ToString
    '            objlblSalaryBand.Text = CType(cboEmployee.SelectedItem, DataRowView).Item("gradelevel").ToString
    '            cboLeavingReason.SelectedValue = CType(cboEmployee.SelectedItem, DataRowView).Item("actionreasonunkid").ToString
    '            cboLeavingReason.Enabled = True
    '            objbtnLeavingReason.Enabled = True
    '        Else
    '            objlblGrade.Text = ""
    '            objlblSalaryBand.Text = ""
    '            cboLeavingReason.SelectedValue = 0
    '            cboLeavingReason.Enabled = False
    '            objbtnLeavingReason.Enabled = False
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Sohail (18 Mar 2015) -- End

    Private Sub cboJob_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboJob.SelectedIndexChanged, dtpWorkStartDate.ValueChanged
        'Sohail (12 Oct 2018) - [dtpWorkStartDate.ValueChanged]
        Dim objJob As New clsJobs
        Dim dsList As DataSet
        Try

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
            'objlblPlanned.Text = Format(0, "0")
            'objlblAvailable.Text = Format(0, "0")
            'objlblVariation.Text = Format(0, "0")
            If mintStaffRequisitionUnkid <= 0 Then 'Add New Mode
                txtJobDesc.Rtf = ""
            End If
            cboGrade.Enabled = True
            objbtnSearchGrade.Enabled = True
            'Sohail (12 Oct 2018) -- End
            If CInt(cboJob.SelectedValue) > 0 Then
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
                'dsList = objJob.getHeadCount("Count", , CInt(cboJob.SelectedValue))
                dsList = objJob.getHeadCount(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtpWorkStartDate.Value.Date, dtpWorkStartDate.Value.Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "Count", , CInt(cboJob.SelectedValue))
                'Sohail (12 Oct 2018) -- End
                If dsList.Tables("Count").Rows.Count > 0 Then
                    'Sohail (12 Oct 2018) -- Start
                    'NMB Enhancement - Ref. # :  - On JD section on staff requisition screen, system to pick JD populated on Job master for that particular vacancy being requested for in 75.1.
                    'objlblPlanned.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("PLANNED")), "0")
                    'objlblAvailable.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("AVAILABLE")), "0")
                    'objlblVariation.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("VARIATION")), "0")
                    If mintStaffRequisitionUnkid <= 0 Then 'Add New Mode
                        objlblPlanned.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("PLANNED")), "0")
                        objlblAvailable.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("AVAILABLE")), "0")
                        objlblVariation.Text = Format(CDec(dsList.Tables("Count").Rows(0).Item("VARIATION")), "0")
                        'Hemant (06 Aug 2019) -- Start
                        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                        If txtJobDesc.Text.Trim.Length <= 0 Then
                            'Hemant (06 Aug 2019) -- End
                        If dsList.Tables("Count").Rows(0).Item("desciription").ToString.StartsWith("{\rtf") = True Then
                            txtJobDesc.Rtf = dsList.Tables("Count").Rows(0).Item("desciription").ToString
                        Else
                            txtJobDesc.Text = dsList.Tables("Count").Rows(0).Item("desciription").ToString
                        End If
                        End If 'Hemant (06 Aug 2019) 
                    End If
                    'Hemant (06 Aug 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                    If CInt(cboGrade.SelectedValue) <= 0 Then
                        'Hemant (06 Aug 2019) -- End
                    cboGrade.SelectedValue = CInt(dsList.Tables("Count").Rows(0).Item("jobgradeunkid"))
                    End If 'Hemant (06 Aug 2019)

                    If CInt(cboGrade.SelectedValue) > 0 Then
                        cboGrade.Enabled = False
                        objbtnSearchGrade.Enabled = False
                    End If
                    'Sohail (12 Oct 2018) -- End
                End If
            End If

            'Sohail (29 Sep 2021) -- Start
            'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
            Dim intDReportTo As Integer = 0
            If CInt(cboJob.SelectedValue) > 0 Then
                objJob._Jobunkid = CInt(cboJob.SelectedValue)
                intDReportTo = objJob._Report_Tounkid
            End If
            dsList = objJob.getComboList("Job", True)
            With cboJobReportTo
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = intDReportTo
            End With
            'Sohail (29 Sep 2021) -- End

            'Sohail (18 Mar 2015) -- Start
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            Call FillList()
            'Sohail (18 Mar 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJob_SelectedIndexChanged", mstrModuleName)
        Finally
            objJob = Nothing
        End Try
    End Sub

    Private Sub cboClassGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboClassGroup.SelectedIndexChanged
        Dim objClass As New clsClass
        Dim dsCombo As DataSet
        Try
            dsCombo = objClass.getComboList("Class", True, CInt(cboClassGroup.SelectedValue))
            With cboClass
                .ValueMember = "classesunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Class")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboClassGroup_SelectedIndexChanged", mstrModuleName)
        Finally
            objClass = Nothing
        End Try
    End Sub

    'Sohail (12 Oct 2018) -- Start
    'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
    Private Sub cboGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Dim objGradeLevel As New clsGradeLevel
        Dim dsList As DataSet
        Try
            dsList = objGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
            With cboGradeLevel
                .ValueMember = "gradelevelunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("GradeLevel")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGrade_SelectedIndexChanged", mstrModuleName)
        Finally
            objGradeLevel = Nothing
        End Try
    End Sub
    'Sohail (12 Oct 2018) -- End

    'Hemant (15 Nov 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 1 : During staff requisition, when employment type is selected as Permanent. The text box on contact duration should be greyed out completely and should not allow user to increment the contact duration. It should be greyed out completely.)
    Private Sub cboEmploymentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmploymentType.SelectedIndexChanged
        Try
            If cboEmploymentType.Text.ToString.ToUpper.Contains("PERMANENT") Then
                nudContractDuration.Value = 0
                nudContractDuration.Enabled = False
            Else
                nudContractDuration.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmploymentType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (15 Nov 2019) -- End

#End Region

    'Sohail (18 Mar 2015) -- Start
    'Enhancement - Allow more than one employee to be replaced in staff requisition.
#Region " Listview's Events "

    Private Sub dgvEmployeeList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmployeeList.CellContentClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objcolhCheckAll.Index Then

                If dgvEmployeeList.IsCurrentCellDirty Then
                    dgvEmployeeList.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    'Sohail (15 Oct 2021) -- Start
                    'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
                    'mdtView.ToTable.AcceptChanges()
                    mdtView.Table.AcceptChanges()
                    'Sohail (15 Oct 2021) -- End
                End If

                'Sohail (15 Oct 2021) -- Start
                'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
                'Dim drRow As DataRow() = mdtView.ToTable.Select("IsChecked = 1")
                Dim drRow As DataRow() = mdtView.Table.Select("IsChecked = 1")
                'Sohail (15 Oct 2021) -- End

                'Hemant (13 Sep 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 10 : When filing the additional staff reason and date to start work, the grade level populated earlier is getting reset. Forcing user to select it again. – TC001. The grade level and the employee name selected and the name of the allocation are getting reset when filling out the additional staff reason textbox.)
                marrSelectedEmployeeList.Clear()
                'Sohail (15 Oct 2021) -- Start
                'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
                'marrSelectedEmployeeList.AddRange((From p In mdtView.ToTable.Select("IsChecked = 1") Select (p.Item("employeeunkid").ToString)).ToArray)
                marrSelectedEmployeeList.AddRange((From p In mdtView.Table.Select("IsChecked = 1") Select (p.Item("employeeunkid").ToString)).ToArray)
                'Sohail (15 Oct 2021) -- End
                mblnIsSelectedEmployeeListChanged = True
                'Hemant (13 Sep 2019) -- End

                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

                If drRow.Length <= 0 Then
                    objchkSelectAll.CheckState = CheckState.Unchecked
                ElseIf drRow.Length < dgvEmployeeList.Rows.Count Then
                    objchkSelectAll.CheckState = CheckState.Indeterminate
                ElseIf drRow.Length = dgvEmployeeList.Rows.Count Then
                    objchkSelectAll.CheckState = CheckState.Checked
                End If

                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            End If
            'Sohail (15 Oct 2021) -- Start
            'NMB Issue :  : Employee is not getting saved when employee is searched with non-existent employee in staff requisition in self service.
            'nudPosition.Value = mdtView.ToTable.Select("IsChecked = 1").Length
            nudPosition.Value = mdtView.Table.Select("IsChecked = 1").Length
            'Sohail (15 Oct 2021) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmployeeList_CellContentClick", mstrModuleName)
        End Try
    End Sub


    Private Sub dgvStaffRequisitionAttachment_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvStaffRequisitionAttachment.CellContentClick
        Try
            Cursor.Current = Cursors.WaitCursor
            If e.RowIndex >= 0 Then
                Dim xrow() As DataRow

                If CInt(dgvStaffRequisitionAttachment.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) > 0 Then
                    xrow = mdtStaffRequisitionDocument.Select("scanattachtranunkid = " & CInt(dgvStaffRequisitionAttachment.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) & "")
                Else
                    xrow = mdtStaffRequisitionDocument.Select("GUID = '" & dgvStaffRequisitionAttachment.Rows(e.RowIndex).Cells(objcolhGUID.Index).Value.ToString & "'")
                End If

                If e.ColumnIndex = objcohDelete.Index Then
                    If xrow.Length > 0 Then
                        If MessageBox.Show(Language.getMessage(mstrModuleName, 36, "Are you sure, you want to delete this attachment?"), "Aruti", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                            xrow(0).Item("AUD") = "D"
                            Call FillStaffRequisitionAttachment()
                        End If
                    End If
                ElseIf e.ColumnIndex = objcolhDownload.Index Then
                    Dim xPath As String = ""

                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("file_path").ToString
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If

                    Dim finfo As New IO.FileInfo(xPath)

                    sfdAttachment.Filter = "File Format (" & finfo.Extension & ")|*" & finfo.Extension & ""
                    sfdAttachment.FileName = xrow(0).Item("filename").ToString

                    If sfdAttachment.ShowDialog = Windows.Forms.DialogResult.OK Then

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            Dim strError As String = ""
                            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                            If blnIsIISInstalled Then

                                Dim filebytes As Byte() = clsFileUploadDownload.DownloadFile(xrow(0).Item("filepath").ToString, xrow(0).Item("fileuniquename").ToString, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)

                                If strError <> "" Then
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.OkOnly)
                                    Exit Sub
                                End If
                                If filebytes IsNot Nothing Then
                                    Dim ms As New MemoryStream(filebytes)
                                    Dim fs As New FileStream(sfdAttachment.FileName, FileMode.Create)
                                    ms.WriteTo(fs)

                                    ms.Close()
                                    fs.Close()
                                    fs.Dispose()
                                Else
                                    If System.IO.File.Exists(xrow(0).Item("filepath").ToString) Then
                                        xPath = xrow(0).Item("filepath").ToString

                                        Dim strFileUniqueName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & "_" & DateTime.Now.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(CStr(xrow(0).Item("filepath").ToString))

                                        If clsFileUploadDownload.UploadFile(xrow(0).Item("filepath").ToString, mstrFolderName, strFileUniqueName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then

                                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                            Exit Try
                                        Else
                                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                                strPath += "/"
                                            End If
                                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileUniqueName
                                            xrow(0).Item("AUD") = "U"
                                            xrow(0).Item("fileuniquename") = strFileUniqueName
                                            xrow(0).Item("filepath") = strPath
                                            xrow(0).Item("filesize") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).Item("filesize_kb") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).AcceptChanges()
                                            Dim objScanAttach As New clsScan_Attach_Documents
                                            objScanAttach._Datatable = mdtStaffRequisitionDocument
                                            objScanAttach.InsertUpdateDelete_Documents()
                                            If objDocument._Message <> "" Then
                                                objScanAttach = Nothing
                                                eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
                                                Exit Try
                                            End If
                                            objScanAttach = Nothing
                                            xrow(0).Item("AUD") = ""
                                            xrow(0).AcceptChanges()
                                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                                        End If
                                    Else
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "sorry, file you are trying to access does not exists on Aruti self service application folder."), enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                End If
                            Else
                                IO.File.Copy(xPath, sfdAttachment.FileName, True)
                            End If
                        Else
                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvQualification_CellContentClick :", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Dim decTotAmt As Decimal = 0

        Try
            Call CheckAllEmployee(objchkSelectAll.Checked)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Hemant (06 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
    Private Sub chkShowInActiveEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowInActiveEmployee.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowInActiveEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (06 Aug 2019) -- End

    'Sohail (29 Sep 2021) -- Start
    'NMB Enhancement : OLD-486 : Provide a field for user to select the reporting title of the job being requested for; Provide a selection field for user to select whether the job requested for will be advertised Internally, Externally or Both Internally and Externally.
    Private Sub chkShowPreviousJobEmp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowPreviousJobEmp.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkShowPreviousJobEmp_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Sep 2021) -- End

#End Region

#Region " Textbox Events "
    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Dim strSearch As String = ""
        Try
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = colhEmpCode.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            colhEmpName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            colhGrade.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            colhSalaryBand.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' "
            End If
            mdtView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (18 Mar 2015) -- End

#Region " Other Controls Events "
    'Hemant (06 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
    Private Sub lnkViewJobDescription_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewJobDescription.Click
        Try
            'Sohail (13 May 2020) -- Start
            'NMB Issue # : On the shortlisting screen,  when you do multiple shortlists and reset the screen then do final save and export, the shortlists that were reset without saving are appearing on the shortlisting report exported.
            Dim frm As New ArutiReports.frmJobReport
            'frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
            'frm.MaximizeBox = True
            'frm.MaximizeBox = False
            'frm.StartPosition = FormStartPosition.CenterParent
            'frm.WindowState = FormWindowState.Maximized
            ''Hemant (22 Aug 2019) -- Start
            ''ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            ''frm.ShowDialog()
            'frm.DisplayDialog(CInt(cboJob.SelectedValue))
            ''Hemant (22 Aug 2019) -- End
            If CInt(cboJob.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, "Please Select Job to view this report."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            Dim objJob As New clsJobReport(User._Object._Languageunkid, Company._Object._Companyunkid)

            objJob.SetDefaultValue()

            objJob._JobId = CInt(cboJob.SelectedValue)
            objJob._JobName = cboJob.SelectedText

            objJob._EmployeeAsOnDate = ConfigParameter._Object._EmployeeAsOnDate
            objJob._ReportId = CInt(enJob_Report_Template.Job_Listing_Report)

            objJob.generateReportNew(FinancialYear._Object._DatabaseName, _
                                    User._Object._Userunkid, _
                                    FinancialYear._Object._YearUnkid, _
                                    Company._Object._Companyunkid, _
                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                    ConfigParameter._Object._ExportReportPath, _
                                    ConfigParameter._Object._OpenAfterExport, _
                                    0, enPrintAction.Preview, enExportAction.None)
            'Sohail (13 May 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkViewJobDescription_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (06 Aug 2019) -- End

    'Hemant (01 Nov 2021) -- Start
    'ENHANCEMENT : OLD-515) - Provide quick link on staff requisition report on request (Add/Edit and Approval pages of staff requisition) Similar to the way we have provide an icon.
    Private Sub lnkViewStaffRequisitionFormReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewStaffRequisitionFormReport.Click
        Try
            Dim frm As New ArutiReports.frmStaffRequisitionFormReport
            frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
            frm.MaximizeBox = True
            frm.MaximizeBox = False
            frm.StartPosition = FormStartPosition.CenterParent
            frm.WindowState = FormWindowState.Maximized
            frm._StaffRequisitionTranunkid = mintStaffRequisitionUnkid
            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkViewStaffRequisitionFormReport_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (01 Nov 2021) -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbApproverMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbApproverMapping.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnAddAttachment.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddAttachment.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbApproverMapping.Text = Language._Object.getCaption(Me.gbApproverMapping.Name, Me.gbApproverMapping.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.Name, Me.lblClass.Text)
			Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.Name, Me.lblClassGroup.Text)
			Me.lblWorkStartDate.Text = Language._Object.getCaption(Me.lblWorkStartDate.Name, Me.lblWorkStartDate.Text)
			Me.lblLeavingReason.Text = Language._Object.getCaption(Me.lblLeavingReason.Name, Me.lblLeavingReason.Text)
			Me.lblAddStaffReason.Text = Language._Object.getCaption(Me.lblAddStaffReason.Name, Me.lblAddStaffReason.Text)
			Me.lblFormNo.Text = Language._Object.getCaption(Me.lblFormNo.Name, Me.lblFormNo.Text)
			Me.lblNoofPosition.Text = Language._Object.getCaption(Me.lblNoofPosition.Name, Me.lblNoofPosition.Text)
			Me.lblAvailable.Text = Language._Object.getCaption(Me.lblAvailable.Name, Me.lblAvailable.Text)
			Me.lblVariation.Text = Language._Object.getCaption(Me.lblVariation.Name, Me.lblVariation.Text)
			Me.lblPlanned.Text = Language._Object.getCaption(Me.lblPlanned.Name, Me.lblPlanned.Text)
			Me.lblJobDesc.Text = Language._Object.getCaption(Me.lblJobDesc.Name, Me.lblJobDesc.Text)
			Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
			Me.lblSearchEmp.Text = Language._Object.getCaption(Me.lblSearchEmp.Name, Me.lblSearchEmp.Text)
			Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.Name, Me.lblGradeLevel.Text)
			Me.lblEmploymentType.Text = Language._Object.getCaption(Me.lblEmploymentType.Name, Me.lblEmploymentType.Text)
			Me.lblContractDuration.Text = Language._Object.getCaption(Me.lblContractDuration.Name, Me.lblContractDuration.Text)
			Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.Name, Me.lblGrade.Text)
            Me.chkShowInActiveEmployee.Text = Language._Object.getCaption(Me.chkShowInActiveEmployee.Name, Me.chkShowInActiveEmployee.Text)
            Me.lnkViewJobDescription.Text = Language._Object.getCaption(Me.lnkViewJobDescription.Name, Me.lnkViewJobDescription.Text)
            Me.btnAddAttachment.Text = Language._Object.getCaption(Me.btnAddAttachment.Name, Me.btnAddAttachment.Text)
            Me.lblDocumentType.Text = Language._Object.getCaption(Me.lblDocumentType.Name, Me.lblDocumentType.Text)
            Me.colhName.HeaderText = Language._Object.getCaption(Me.colhName.Name, Me.colhName.HeaderText)
            Me.colhSize.HeaderText = Language._Object.getCaption(Me.colhSize.Name, Me.colhSize.HeaderText)
			Me.lblJobReportTo.Text = Language._Object.getCaption(Me.lblJobReportTo.Name, Me.lblJobReportTo.Text)
			Me.lblJobAdvert.Text = Language._Object.getCaption(Me.lblJobAdvert.Name, Me.lblJobAdvert.Text)
			Me.chkShowPreviousJobEmp.Text = Language._Object.getCaption(Me.chkShowPreviousJobEmp.Name, Me.chkShowPreviousJobEmp.Text)
			Me.colhEmpCode.HeaderText = Language._Object.getCaption(Me.colhEmpCode.Name, Me.colhEmpCode.HeaderText)
			Me.colhEmpName.HeaderText = Language._Object.getCaption(Me.colhEmpName.Name, Me.colhEmpName.HeaderText)
			Me.colhJob.HeaderText = Language._Object.getCaption(Me.colhJob.Name, Me.colhJob.HeaderText)
			Me.colhGrade.HeaderText = Language._Object.getCaption(Me.colhGrade.Name, Me.colhGrade.HeaderText)
			Me.colhSalaryBand.HeaderText = Language._Object.getCaption(Me.colhSalaryBand.Name, Me.colhSalaryBand.HeaderText)
            Me.lnkViewStaffRequisitionFormReport.Text = Language._Object.getCaption(Me.lnkViewStaffRequisitionFormReport.Name, Me.lnkViewStaffRequisitionFormReport.Text)

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "Sorry! Staff Requisition Form No. cannot be blank. Staff Requisition Form No. is mandatory information.")
			Language.setMessage(mstrModuleName, 3, "Please select Requisition Type.")
			Language.setMessage(mstrModuleName, 4, "Please select Requisition By.")
			Language.setMessage(mstrModuleName, 5, "Please select")
			Language.setMessage(mstrModuleName, 6, "Please select atleast one Employee to be Replaced.")
			Language.setMessage(mstrModuleName, 7, "Please select Leaving Reason.")
			Language.setMessage(mstrModuleName, 8, "Please select Job Title.")
			Language.setMessage(mstrModuleName, 9, "Sorry, Date to Start Work should be greather than current date.")
			Language.setMessage(mstrModuleName, 10, "Sending Email(s) process is in progress from other module. Please wait.")
			Language.setMessage(mstrModuleName, 11, "Branch")
			Language.setMessage(mstrModuleName, 12, "Department Group")
			Language.setMessage(mstrModuleName, 13, "Department")
			Language.setMessage(mstrModuleName, 14, "Section Group")
			Language.setMessage(mstrModuleName, 15, "Section")
			Language.setMessage(mstrModuleName, 16, "Unit Group")
			Language.setMessage(mstrModuleName, 17, "Unit")
			Language.setMessage(mstrModuleName, 18, "Team")
			Language.setMessage(mstrModuleName, 19, "Job Group")
			Language.setMessage(mstrModuleName, 20, "Jobs")
			Language.setMessage(mstrModuleName, 21, "Class Group")
			Language.setMessage(mstrModuleName, 22, "Classes")
			Language.setMessage(mstrModuleName, 23, "Sorry, No. of Position should be greater than Zero.")
			Language.setMessage(mstrModuleName, 24, "Please select Grade.")
			Language.setMessage(mstrModuleName, 25, "Please select Grade Level.")
			Language.setMessage(mstrModuleName, 26, "Please select Employment Type.")
			Language.setMessage(mstrModuleName, 27, "Sorry, No. of position should not be greater than Manpower variations.")
			Language.setMessage(mstrModuleName, 28, "Please add vacancies on Job master screen.")
			Language.setMessage(mstrModuleName, 29, "Sorry, Staff Requisition approval process is in progress and it is not approved by previous level user yet.")
            Language.setMessage(mstrModuleName, 30, "Staff Requisition Saved Successfully.")
            Language.setMessage(mstrModuleName, 32, "Selected information is already present for particular employee.")
            Language.setMessage(mstrModuleName, 33, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation.")
            Language.setMessage(mstrModuleName, 34, "Document Type is compulsory information. Please select Document Type to continue.")
			Language.setMessage(mstrModuleName, 35, "Configuration Path does not Exist.")
			Language.setMessage(mstrModuleName, 36, "Are you sure, you want to delete this attachment?")
			Language.setMessage(mstrModuleName, 37, "sorry, file you are trying to access does not exists on Aruti self service application folder.")
			Language.setMessage(mstrModuleName, 38, "Please Select Job to view this report.")
			Language.setMessage(mstrModuleName, 39, "Please select Job Reporting Line.")
			Language.setMessage(mstrModuleName, 40, "Please select Job Advert.")
            Language.setMessage(mstrModuleName, 41, "Sorry, No. of Contract Duration should be greater than Zero.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class