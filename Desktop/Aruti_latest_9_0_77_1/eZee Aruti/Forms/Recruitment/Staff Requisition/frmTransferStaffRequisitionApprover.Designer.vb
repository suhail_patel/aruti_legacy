﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTransferStaffRequisitionApprover
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTransferStaffRequisitionApprover))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAssignJob = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnUnAssignJob = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.txtSearchNewJob = New eZee.TextBox.AlphanumericTextBox
        Me.objchkNewJobSelect = New System.Windows.Forms.CheckBox
        Me.dgNewJob = New System.Windows.Forms.DataGridView
        Me.objcolhNewJobCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhNewJobName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.dgAssignedJob = New System.Windows.Forms.DataGridView
        Me.colhAssignedJobName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchAssignedJob = New eZee.TextBox.AlphanumericTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkOldJobSelect = New System.Windows.Forms.CheckBox
        Me.dgOldJob = New System.Windows.Forms.DataGridView
        Me.objcolhOldJobCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhOldJobName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchOldJob = New eZee.TextBox.AlphanumericTextBox
        Me.lblNewAllocation = New System.Windows.Forms.Label
        Me.cboNewAllocation = New System.Windows.Forms.ComboBox
        Me.lblOldAllocation = New System.Windows.Forms.Label
        Me.cboOldAllocation = New System.Windows.Forms.ComboBox
        Me.txtSearchOldAllocation = New eZee.TextBox.AlphanumericTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.objbtnSearchOldLevel = New eZee.Common.eZeeGradientButton
        Me.cboOldLevel = New System.Windows.Forms.ComboBox
        Me.objbtnSearchOldApprover = New eZee.Common.eZeeGradientButton
        Me.pnlOldEmp = New System.Windows.Forms.Panel
        Me.objchkOldAllocationSelect = New System.Windows.Forms.CheckBox
        Me.dgOldAllocation = New System.Windows.Forms.DataGridView
        Me.objcolhOldAllocationCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhOldAllocationName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlNewEmp = New System.Windows.Forms.Panel
        Me.tabNewApprover = New System.Windows.Forms.TabControl
        Me.tbpMigratedEmployee = New System.Windows.Forms.TabPage
        Me.txtSearchNewAllocation = New eZee.TextBox.AlphanumericTextBox
        Me.objchkNewAllocationSelect = New System.Windows.Forms.CheckBox
        Me.dgNewAllocation = New System.Windows.Forms.DataGridView
        Me.objcolhNewAllocationCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhNewAllocationName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tbpAssignEmployee = New System.Windows.Forms.TabPage
        Me.dgAssignedAllocation = New System.Windows.Forms.DataGridView
        Me.colhAssignedAllocationName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchAssignedAllocation = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnUnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblNewLevel = New System.Windows.Forms.Label
        Me.lblNewApprover = New System.Windows.Forms.Label
        Me.lblCaption1 = New System.Windows.Forms.Label
        Me.objbtnSearchNewApprover = New eZee.Common.eZeeGradientButton
        Me.cboOldApprover = New System.Windows.Forms.ComboBox
        Me.objbtnSearchNewLevel = New eZee.Common.eZeeGradientButton
        Me.cboNewApprover = New System.Windows.Forms.ComboBox
        Me.cboNewLevel = New System.Windows.Forms.ComboBox
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbInfo.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgNewJob, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgAssignedJob, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.dgOldJob, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOldEmp.SuspendLayout()
        CType(Me.dgOldAllocation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlNewEmp.SuspendLayout()
        Me.tabNewApprover.SuspendLayout()
        Me.tbpMigratedEmployee.SuspendLayout()
        CType(Me.dgNewAllocation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpAssignEmployee.SuspendLayout()
        CType(Me.dgAssignedAllocation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbInfo)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(763, 654)
        Me.pnlMain.TabIndex = 1
        '
        'gbInfo
        '
        Me.gbInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInfo.Checked = False
        Me.gbInfo.CollapseAllExceptThis = False
        Me.gbInfo.CollapsedHoverImage = Nothing
        Me.gbInfo.CollapsedNormalImage = Nothing
        Me.gbInfo.CollapsedPressedImage = Nothing
        Me.gbInfo.CollapseOnLoad = False
        Me.gbInfo.Controls.Add(Me.objbtnAssignJob)
        Me.gbInfo.Controls.Add(Me.objbtnUnAssignJob)
        Me.gbInfo.Controls.Add(Me.Panel2)
        Me.gbInfo.Controls.Add(Me.Panel1)
        Me.gbInfo.Controls.Add(Me.txtSearchOldJob)
        Me.gbInfo.Controls.Add(Me.lblNewAllocation)
        Me.gbInfo.Controls.Add(Me.cboNewAllocation)
        Me.gbInfo.Controls.Add(Me.lblOldAllocation)
        Me.gbInfo.Controls.Add(Me.cboOldAllocation)
        Me.gbInfo.Controls.Add(Me.txtSearchOldAllocation)
        Me.gbInfo.Controls.Add(Me.Label1)
        Me.gbInfo.Controls.Add(Me.objbtnSearchOldLevel)
        Me.gbInfo.Controls.Add(Me.cboOldLevel)
        Me.gbInfo.Controls.Add(Me.objbtnSearchOldApprover)
        Me.gbInfo.Controls.Add(Me.pnlOldEmp)
        Me.gbInfo.Controls.Add(Me.pnlNewEmp)
        Me.gbInfo.Controls.Add(Me.objbtnAssign)
        Me.gbInfo.Controls.Add(Me.objbtnUnAssign)
        Me.gbInfo.Controls.Add(Me.lblNewLevel)
        Me.gbInfo.Controls.Add(Me.lblNewApprover)
        Me.gbInfo.Controls.Add(Me.lblCaption1)
        Me.gbInfo.Controls.Add(Me.objbtnSearchNewApprover)
        Me.gbInfo.Controls.Add(Me.cboOldApprover)
        Me.gbInfo.Controls.Add(Me.objbtnSearchNewLevel)
        Me.gbInfo.Controls.Add(Me.cboNewApprover)
        Me.gbInfo.Controls.Add(Me.cboNewLevel)
        Me.gbInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbInfo.ExpandedHoverImage = Nothing
        Me.gbInfo.ExpandedNormalImage = Nothing
        Me.gbInfo.ExpandedPressedImage = Nothing
        Me.gbInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInfo.HeaderHeight = 25
        Me.gbInfo.HeaderMessage = ""
        Me.gbInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInfo.HeightOnCollapse = 0
        Me.gbInfo.LeftTextSpace = 0
        Me.gbInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbInfo.Name = "gbInfo"
        Me.gbInfo.OpenHeight = 300
        Me.gbInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInfo.ShowBorder = True
        Me.gbInfo.ShowCheckBox = False
        Me.gbInfo.ShowCollapseButton = False
        Me.gbInfo.ShowDefaultBorderColor = True
        Me.gbInfo.ShowDownButton = False
        Me.gbInfo.ShowHeader = True
        Me.gbInfo.Size = New System.Drawing.Size(763, 599)
        Me.gbInfo.TabIndex = 0
        Me.gbInfo.Temp = 0
        Me.gbInfo.Text = "Approver Information"
        Me.gbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAssignJob
        '
        Me.objbtnAssignJob.BackColor = System.Drawing.Color.White
        Me.objbtnAssignJob.BackgroundImage = CType(resources.GetObject("objbtnAssignJob.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAssignJob.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAssignJob.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAssignJob.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAssignJob.FlatAppearance.BorderSize = 0
        Me.objbtnAssignJob.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAssignJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAssignJob.ForeColor = System.Drawing.Color.Black
        Me.objbtnAssignJob.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAssignJob.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssignJob.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssignJob.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssignJob.Image = Global.Aruti.Main.My.Resources.Resources.right_arrow_16
        Me.objbtnAssignJob.Location = New System.Drawing.Point(345, 457)
        Me.objbtnAssignJob.Name = "objbtnAssignJob"
        Me.objbtnAssignJob.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssignJob.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssignJob.Size = New System.Drawing.Size(42, 40)
        Me.objbtnAssignJob.TabIndex = 251
        Me.objbtnAssignJob.UseVisualStyleBackColor = True
        '
        'objbtnUnAssignJob
        '
        Me.objbtnUnAssignJob.BackColor = System.Drawing.Color.White
        Me.objbtnUnAssignJob.BackgroundImage = CType(resources.GetObject("objbtnUnAssignJob.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUnAssignJob.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUnAssignJob.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUnAssignJob.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUnAssignJob.FlatAppearance.BorderSize = 0
        Me.objbtnUnAssignJob.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUnAssignJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnUnAssignJob.ForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssignJob.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUnAssignJob.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssignJob.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnAssignJob.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssignJob.Image = Global.Aruti.Main.My.Resources.Resources.left_arrow_16
        Me.objbtnUnAssignJob.Location = New System.Drawing.Point(345, 503)
        Me.objbtnUnAssignJob.Name = "objbtnUnAssignJob"
        Me.objbtnUnAssignJob.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnAssignJob.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssignJob.Size = New System.Drawing.Size(42, 40)
        Me.objbtnUnAssignJob.TabIndex = 252
        Me.objbtnUnAssignJob.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.TabControl1)
        Me.Panel2.Location = New System.Drawing.Point(411, 380)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(316, 209)
        Me.Panel2.TabIndex = 109
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(3, 6)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(310, 202)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.txtSearchNewJob)
        Me.TabPage1.Controls.Add(Me.objchkNewJobSelect)
        Me.TabPage1.Controls.Add(Me.dgNewJob)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(302, 176)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Transfer"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'txtSearchNewJob
        '
        Me.txtSearchNewJob.Flags = 0
        Me.txtSearchNewJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchNewJob.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchNewJob.Location = New System.Drawing.Point(1, 3)
        Me.txtSearchNewJob.Name = "txtSearchNewJob"
        Me.txtSearchNewJob.Size = New System.Drawing.Size(297, 21)
        Me.txtSearchNewJob.TabIndex = 126
        '
        'objchkNewJobSelect
        '
        Me.objchkNewJobSelect.AutoSize = True
        Me.objchkNewJobSelect.Location = New System.Drawing.Point(8, 32)
        Me.objchkNewJobSelect.Name = "objchkNewJobSelect"
        Me.objchkNewJobSelect.Size = New System.Drawing.Size(15, 14)
        Me.objchkNewJobSelect.TabIndex = 119
        Me.objchkNewJobSelect.UseVisualStyleBackColor = True
        '
        'dgNewJob
        '
        Me.dgNewJob.AllowUserToAddRows = False
        Me.dgNewJob.AllowUserToDeleteRows = False
        Me.dgNewJob.AllowUserToResizeColumns = False
        Me.dgNewJob.AllowUserToResizeRows = False
        Me.dgNewJob.BackgroundColor = System.Drawing.Color.White
        Me.dgNewJob.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgNewJob.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhNewJobCheck, Me.colhNewJobName})
        Me.dgNewJob.Location = New System.Drawing.Point(1, 26)
        Me.dgNewJob.Name = "dgNewJob"
        Me.dgNewJob.RowHeadersVisible = False
        Me.dgNewJob.Size = New System.Drawing.Size(297, 150)
        Me.dgNewJob.TabIndex = 120
        '
        'objcolhNewJobCheck
        '
        Me.objcolhNewJobCheck.HeaderText = ""
        Me.objcolhNewJobCheck.Name = "objcolhNewJobCheck"
        Me.objcolhNewJobCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhNewJobCheck.Width = 25
        '
        'colhNewJobName
        '
        Me.colhNewJobName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhNewJobName.FillWeight = 54.65117!
        Me.colhNewJobName.HeaderText = "Job"
        Me.colhNewJobName.Name = "colhNewJobName"
        Me.colhNewJobName.ReadOnly = True
        Me.colhNewJobName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhNewJobName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.dgAssignedJob)
        Me.TabPage2.Controls.Add(Me.txtSearchAssignedJob)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(302, 176)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Assigned"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'dgAssignedJob
        '
        Me.dgAssignedJob.AllowUserToAddRows = False
        Me.dgAssignedJob.AllowUserToDeleteRows = False
        Me.dgAssignedJob.AllowUserToResizeColumns = False
        Me.dgAssignedJob.AllowUserToResizeRows = False
        Me.dgAssignedJob.BackgroundColor = System.Drawing.Color.White
        Me.dgAssignedJob.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgAssignedJob.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhAssignedJobName})
        Me.dgAssignedJob.Location = New System.Drawing.Point(1, 26)
        Me.dgAssignedJob.Name = "dgAssignedJob"
        Me.dgAssignedJob.RowHeadersVisible = False
        Me.dgAssignedJob.Size = New System.Drawing.Size(298, 152)
        Me.dgAssignedJob.TabIndex = 128
        '
        'colhAssignedJobName
        '
        Me.colhAssignedJobName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhAssignedJobName.FillWeight = 54.65117!
        Me.colhAssignedJobName.HeaderText = "Job"
        Me.colhAssignedJobName.Name = "colhAssignedJobName"
        Me.colhAssignedJobName.ReadOnly = True
        Me.colhAssignedJobName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhAssignedJobName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'txtSearchAssignedJob
        '
        Me.txtSearchAssignedJob.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchAssignedJob.Flags = 0
        Me.txtSearchAssignedJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchAssignedJob.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchAssignedJob.Location = New System.Drawing.Point(1, 3)
        Me.txtSearchAssignedJob.Name = "txtSearchAssignedJob"
        Me.txtSearchAssignedJob.Size = New System.Drawing.Size(297, 21)
        Me.txtSearchAssignedJob.TabIndex = 127
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkOldJobSelect)
        Me.Panel1.Controls.Add(Me.dgOldJob)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(9, 405)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(314, 184)
        Me.Panel1.TabIndex = 120
        '
        'objchkOldJobSelect
        '
        Me.objchkOldJobSelect.AutoSize = True
        Me.objchkOldJobSelect.Location = New System.Drawing.Point(7, 6)
        Me.objchkOldJobSelect.Name = "objchkOldJobSelect"
        Me.objchkOldJobSelect.Size = New System.Drawing.Size(15, 14)
        Me.objchkOldJobSelect.TabIndex = 118
        Me.objchkOldJobSelect.UseVisualStyleBackColor = True
        '
        'dgOldJob
        '
        Me.dgOldJob.AllowUserToAddRows = False
        Me.dgOldJob.AllowUserToDeleteRows = False
        Me.dgOldJob.AllowUserToResizeColumns = False
        Me.dgOldJob.AllowUserToResizeRows = False
        Me.dgOldJob.BackgroundColor = System.Drawing.Color.White
        Me.dgOldJob.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgOldJob.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhOldJobCheck, Me.colhOldJobName})
        Me.dgOldJob.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgOldJob.Location = New System.Drawing.Point(0, 0)
        Me.dgOldJob.Name = "dgOldJob"
        Me.dgOldJob.RowHeadersVisible = False
        Me.dgOldJob.Size = New System.Drawing.Size(314, 184)
        Me.dgOldJob.TabIndex = 119
        '
        'objcolhOldJobCheck
        '
        Me.objcolhOldJobCheck.HeaderText = ""
        Me.objcolhOldJobCheck.Name = "objcolhOldJobCheck"
        Me.objcolhOldJobCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhOldJobCheck.Width = 25
        '
        'colhOldJobName
        '
        Me.colhOldJobName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhOldJobName.FillWeight = 54.65117!
        Me.colhOldJobName.HeaderText = "Job"
        Me.colhOldJobName.Name = "colhOldJobName"
        Me.colhOldJobName.ReadOnly = True
        Me.colhOldJobName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhOldJobName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'txtSearchOldJob
        '
        Me.txtSearchOldJob.Flags = 0
        Me.txtSearchOldJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchOldJob.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchOldJob.Location = New System.Drawing.Point(9, 380)
        Me.txtSearchOldJob.Name = "txtSearchOldJob"
        Me.txtSearchOldJob.Size = New System.Drawing.Size(314, 21)
        Me.txtSearchOldJob.TabIndex = 249
        '
        'lblNewAllocation
        '
        Me.lblNewAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewAllocation.Location = New System.Drawing.Point(408, 91)
        Me.lblNewAllocation.Name = "lblNewAllocation"
        Me.lblNewAllocation.Size = New System.Drawing.Size(80, 15)
        Me.lblNewAllocation.TabIndex = 247
        Me.lblNewAllocation.Text = "Allocation"
        Me.lblNewAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNewAllocation
        '
        Me.cboNewAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNewAllocation.DropDownWidth = 300
        Me.cboNewAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNewAllocation.FormattingEnabled = True
        Me.cboNewAllocation.Location = New System.Drawing.Point(491, 88)
        Me.cboNewAllocation.Name = "cboNewAllocation"
        Me.cboNewAllocation.Size = New System.Drawing.Size(236, 21)
        Me.cboNewAllocation.TabIndex = 7
        '
        'lblOldAllocation
        '
        Me.lblOldAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOldAllocation.Location = New System.Drawing.Point(6, 90)
        Me.lblOldAllocation.Name = "lblOldAllocation"
        Me.lblOldAllocation.Size = New System.Drawing.Size(80, 15)
        Me.lblOldAllocation.TabIndex = 245
        Me.lblOldAllocation.Text = "Allocation"
        Me.lblOldAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOldAllocation
        '
        Me.cboOldAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOldAllocation.DropDownWidth = 300
        Me.cboOldAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOldAllocation.FormattingEnabled = True
        Me.cboOldAllocation.Location = New System.Drawing.Point(87, 87)
        Me.cboOldAllocation.Name = "cboOldAllocation"
        Me.cboOldAllocation.Size = New System.Drawing.Size(236, 21)
        Me.cboOldAllocation.TabIndex = 3
        '
        'txtSearchOldAllocation
        '
        Me.txtSearchOldAllocation.Flags = 0
        Me.txtSearchOldAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchOldAllocation.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchOldAllocation.Location = New System.Drawing.Point(9, 122)
        Me.txtSearchOldAllocation.Name = "txtSearchOldAllocation"
        Me.txtSearchOldAllocation.Size = New System.Drawing.Size(314, 21)
        Me.txtSearchOldAllocation.TabIndex = 124
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(4, 63)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 15)
        Me.Label1.TabIndex = 122
        Me.Label1.Text = "Level"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchOldLevel
        '
        Me.objbtnSearchOldLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOldLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOldLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOldLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOldLevel.BorderSelected = False
        Me.objbtnSearchOldLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOldLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchOldLevel.Image = CType(resources.GetObject("objbtnSearchOldLevel.Image"), System.Drawing.Image)
        Me.objbtnSearchOldLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOldLevel.Location = New System.Drawing.Point(331, 60)
        Me.objbtnSearchOldLevel.Name = "objbtnSearchOldLevel"
        Me.objbtnSearchOldLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOldLevel.TabIndex = 121
        '
        'cboOldLevel
        '
        Me.cboOldLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOldLevel.DropDownWidth = 300
        Me.cboOldLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOldLevel.FormattingEnabled = True
        Me.cboOldLevel.Location = New System.Drawing.Point(87, 60)
        Me.cboOldLevel.Name = "cboOldLevel"
        Me.cboOldLevel.Size = New System.Drawing.Size(236, 21)
        Me.cboOldLevel.TabIndex = 2
        '
        'objbtnSearchOldApprover
        '
        Me.objbtnSearchOldApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOldApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOldApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOldApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOldApprover.BorderSelected = False
        Me.objbtnSearchOldApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOldApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchOldApprover.Image = CType(resources.GetObject("objbtnSearchOldApprover.Image"), System.Drawing.Image)
        Me.objbtnSearchOldApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOldApprover.Location = New System.Drawing.Point(331, 33)
        Me.objbtnSearchOldApprover.Name = "objbtnSearchOldApprover"
        Me.objbtnSearchOldApprover.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOldApprover.TabIndex = 118
        '
        'pnlOldEmp
        '
        Me.pnlOldEmp.Controls.Add(Me.objchkOldAllocationSelect)
        Me.pnlOldEmp.Controls.Add(Me.dgOldAllocation)
        Me.pnlOldEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlOldEmp.Location = New System.Drawing.Point(9, 146)
        Me.pnlOldEmp.Name = "pnlOldEmp"
        Me.pnlOldEmp.Size = New System.Drawing.Size(314, 220)
        Me.pnlOldEmp.TabIndex = 108
        '
        'objchkOldAllocationSelect
        '
        Me.objchkOldAllocationSelect.AutoSize = True
        Me.objchkOldAllocationSelect.Location = New System.Drawing.Point(7, 6)
        Me.objchkOldAllocationSelect.Name = "objchkOldAllocationSelect"
        Me.objchkOldAllocationSelect.Size = New System.Drawing.Size(15, 14)
        Me.objchkOldAllocationSelect.TabIndex = 118
        Me.objchkOldAllocationSelect.UseVisualStyleBackColor = True
        '
        'dgOldAllocation
        '
        Me.dgOldAllocation.AllowUserToAddRows = False
        Me.dgOldAllocation.AllowUserToDeleteRows = False
        Me.dgOldAllocation.AllowUserToResizeColumns = False
        Me.dgOldAllocation.AllowUserToResizeRows = False
        Me.dgOldAllocation.BackgroundColor = System.Drawing.Color.White
        Me.dgOldAllocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgOldAllocation.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhOldAllocationCheck, Me.colhOldAllocationName})
        Me.dgOldAllocation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgOldAllocation.Location = New System.Drawing.Point(0, 0)
        Me.dgOldAllocation.Name = "dgOldAllocation"
        Me.dgOldAllocation.RowHeadersVisible = False
        Me.dgOldAllocation.Size = New System.Drawing.Size(314, 220)
        Me.dgOldAllocation.TabIndex = 119
        '
        'objcolhOldAllocationCheck
        '
        Me.objcolhOldAllocationCheck.HeaderText = ""
        Me.objcolhOldAllocationCheck.Name = "objcolhOldAllocationCheck"
        Me.objcolhOldAllocationCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhOldAllocationCheck.Width = 25
        '
        'colhOldAllocationName
        '
        Me.colhOldAllocationName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhOldAllocationName.FillWeight = 54.65117!
        Me.colhOldAllocationName.HeaderText = "Name"
        Me.colhOldAllocationName.Name = "colhOldAllocationName"
        Me.colhOldAllocationName.ReadOnly = True
        Me.colhOldAllocationName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhOldAllocationName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'pnlNewEmp
        '
        Me.pnlNewEmp.Controls.Add(Me.tabNewApprover)
        Me.pnlNewEmp.Location = New System.Drawing.Point(411, 122)
        Me.pnlNewEmp.Name = "pnlNewEmp"
        Me.pnlNewEmp.Size = New System.Drawing.Size(316, 244)
        Me.pnlNewEmp.TabIndex = 108
        '
        'tabNewApprover
        '
        Me.tabNewApprover.Controls.Add(Me.tbpMigratedEmployee)
        Me.tabNewApprover.Controls.Add(Me.tbpAssignEmployee)
        Me.tabNewApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabNewApprover.Location = New System.Drawing.Point(3, 6)
        Me.tabNewApprover.Name = "tabNewApprover"
        Me.tabNewApprover.SelectedIndex = 0
        Me.tabNewApprover.Size = New System.Drawing.Size(310, 235)
        Me.tabNewApprover.TabIndex = 0
        '
        'tbpMigratedEmployee
        '
        Me.tbpMigratedEmployee.Controls.Add(Me.txtSearchNewAllocation)
        Me.tbpMigratedEmployee.Controls.Add(Me.objchkNewAllocationSelect)
        Me.tbpMigratedEmployee.Controls.Add(Me.dgNewAllocation)
        Me.tbpMigratedEmployee.Location = New System.Drawing.Point(4, 22)
        Me.tbpMigratedEmployee.Name = "tbpMigratedEmployee"
        Me.tbpMigratedEmployee.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpMigratedEmployee.Size = New System.Drawing.Size(302, 209)
        Me.tbpMigratedEmployee.TabIndex = 0
        Me.tbpMigratedEmployee.Text = "Transfer"
        Me.tbpMigratedEmployee.UseVisualStyleBackColor = True
        '
        'txtSearchNewAllocation
        '
        Me.txtSearchNewAllocation.Flags = 0
        Me.txtSearchNewAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchNewAllocation.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchNewAllocation.Location = New System.Drawing.Point(1, 3)
        Me.txtSearchNewAllocation.Name = "txtSearchNewAllocation"
        Me.txtSearchNewAllocation.Size = New System.Drawing.Size(297, 21)
        Me.txtSearchNewAllocation.TabIndex = 126
        '
        'objchkNewAllocationSelect
        '
        Me.objchkNewAllocationSelect.AutoSize = True
        Me.objchkNewAllocationSelect.Location = New System.Drawing.Point(8, 32)
        Me.objchkNewAllocationSelect.Name = "objchkNewAllocationSelect"
        Me.objchkNewAllocationSelect.Size = New System.Drawing.Size(15, 14)
        Me.objchkNewAllocationSelect.TabIndex = 119
        Me.objchkNewAllocationSelect.UseVisualStyleBackColor = True
        '
        'dgNewAllocation
        '
        Me.dgNewAllocation.AllowUserToAddRows = False
        Me.dgNewAllocation.AllowUserToDeleteRows = False
        Me.dgNewAllocation.AllowUserToResizeColumns = False
        Me.dgNewAllocation.AllowUserToResizeRows = False
        Me.dgNewAllocation.BackgroundColor = System.Drawing.Color.White
        Me.dgNewAllocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgNewAllocation.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhNewAllocationCheck, Me.colhNewAllocationName})
        Me.dgNewAllocation.Location = New System.Drawing.Point(1, 26)
        Me.dgNewAllocation.Name = "dgNewAllocation"
        Me.dgNewAllocation.RowHeadersVisible = False
        Me.dgNewAllocation.Size = New System.Drawing.Size(297, 183)
        Me.dgNewAllocation.TabIndex = 120
        '
        'objcolhNewAllocationCheck
        '
        Me.objcolhNewAllocationCheck.HeaderText = ""
        Me.objcolhNewAllocationCheck.Name = "objcolhNewAllocationCheck"
        Me.objcolhNewAllocationCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhNewAllocationCheck.Width = 25
        '
        'colhNewAllocationName
        '
        Me.colhNewAllocationName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhNewAllocationName.FillWeight = 54.65117!
        Me.colhNewAllocationName.HeaderText = "Name"
        Me.colhNewAllocationName.Name = "colhNewAllocationName"
        Me.colhNewAllocationName.ReadOnly = True
        Me.colhNewAllocationName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhNewAllocationName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'tbpAssignEmployee
        '
        Me.tbpAssignEmployee.Controls.Add(Me.dgAssignedAllocation)
        Me.tbpAssignEmployee.Controls.Add(Me.txtSearchAssignedAllocation)
        Me.tbpAssignEmployee.Location = New System.Drawing.Point(4, 22)
        Me.tbpAssignEmployee.Name = "tbpAssignEmployee"
        Me.tbpAssignEmployee.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpAssignEmployee.Size = New System.Drawing.Size(302, 209)
        Me.tbpAssignEmployee.TabIndex = 1
        Me.tbpAssignEmployee.Text = "Assigned"
        Me.tbpAssignEmployee.UseVisualStyleBackColor = True
        '
        'dgAssignedAllocation
        '
        Me.dgAssignedAllocation.AllowUserToAddRows = False
        Me.dgAssignedAllocation.AllowUserToDeleteRows = False
        Me.dgAssignedAllocation.AllowUserToResizeColumns = False
        Me.dgAssignedAllocation.AllowUserToResizeRows = False
        Me.dgAssignedAllocation.BackgroundColor = System.Drawing.Color.White
        Me.dgAssignedAllocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgAssignedAllocation.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhAssignedAllocationName})
        Me.dgAssignedAllocation.Location = New System.Drawing.Point(1, 26)
        Me.dgAssignedAllocation.Name = "dgAssignedAllocation"
        Me.dgAssignedAllocation.RowHeadersVisible = False
        Me.dgAssignedAllocation.Size = New System.Drawing.Size(301, 183)
        Me.dgAssignedAllocation.TabIndex = 128
        '
        'colhAssignedAllocationName
        '
        Me.colhAssignedAllocationName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhAssignedAllocationName.FillWeight = 54.65117!
        Me.colhAssignedAllocationName.HeaderText = "Name"
        Me.colhAssignedAllocationName.Name = "colhAssignedAllocationName"
        Me.colhAssignedAllocationName.ReadOnly = True
        Me.colhAssignedAllocationName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhAssignedAllocationName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'txtSearchAssignedAllocation
        '
        Me.txtSearchAssignedAllocation.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchAssignedAllocation.Flags = 0
        Me.txtSearchAssignedAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchAssignedAllocation.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchAssignedAllocation.Location = New System.Drawing.Point(1, 3)
        Me.txtSearchAssignedAllocation.Name = "txtSearchAssignedAllocation"
        Me.txtSearchAssignedAllocation.Size = New System.Drawing.Size(297, 21)
        Me.txtSearchAssignedAllocation.TabIndex = 127
        '
        'objbtnAssign
        '
        Me.objbtnAssign.BackColor = System.Drawing.Color.White
        Me.objbtnAssign.BackgroundImage = CType(resources.GetObject("objbtnAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAssign.FlatAppearance.BorderSize = 0
        Me.objbtnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.Image = Global.Aruti.Main.My.Resources.Resources.right_arrow_16
        Me.objbtnAssign.Location = New System.Drawing.Point(345, 230)
        Me.objbtnAssign.Name = "objbtnAssign"
        Me.objbtnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnAssign.TabIndex = 115
        Me.objbtnAssign.UseVisualStyleBackColor = True
        '
        'objbtnUnAssign
        '
        Me.objbtnUnAssign.BackColor = System.Drawing.Color.White
        Me.objbtnUnAssign.BackgroundImage = CType(resources.GetObject("objbtnUnAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnUnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnUnAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnUnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnUnAssign.FlatAppearance.BorderSize = 0
        Me.objbtnUnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnUnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnUnAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnUnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssign.Image = Global.Aruti.Main.My.Resources.Resources.left_arrow_16
        Me.objbtnUnAssign.Location = New System.Drawing.Point(345, 276)
        Me.objbtnUnAssign.Name = "objbtnUnAssign"
        Me.objbtnUnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnUnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnUnAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnUnAssign.TabIndex = 116
        Me.objbtnUnAssign.UseVisualStyleBackColor = True
        '
        'lblNewLevel
        '
        Me.lblNewLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewLevel.Location = New System.Drawing.Point(408, 63)
        Me.lblNewLevel.Name = "lblNewLevel"
        Me.lblNewLevel.Size = New System.Drawing.Size(77, 15)
        Me.lblNewLevel.TabIndex = 114
        Me.lblNewLevel.Text = "Level"
        Me.lblNewLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblNewApprover
        '
        Me.lblNewApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNewApprover.Location = New System.Drawing.Point(408, 36)
        Me.lblNewApprover.Name = "lblNewApprover"
        Me.lblNewApprover.Size = New System.Drawing.Size(77, 15)
        Me.lblNewApprover.TabIndex = 113
        Me.lblNewApprover.Text = "To Approver"
        Me.lblNewApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaption1
        '
        Me.lblCaption1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption1.Location = New System.Drawing.Point(4, 36)
        Me.lblCaption1.Name = "lblCaption1"
        Me.lblCaption1.Size = New System.Drawing.Size(80, 15)
        Me.lblCaption1.TabIndex = 110
        Me.lblCaption1.Text = "From Approver"
        Me.lblCaption1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchNewApprover
        '
        Me.objbtnSearchNewApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchNewApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchNewApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchNewApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchNewApprover.BorderSelected = False
        Me.objbtnSearchNewApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchNewApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchNewApprover.Image = CType(resources.GetObject("objbtnSearchNewApprover.Image"), System.Drawing.Image)
        Me.objbtnSearchNewApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchNewApprover.Location = New System.Drawing.Point(733, 32)
        Me.objbtnSearchNewApprover.Name = "objbtnSearchNewApprover"
        Me.objbtnSearchNewApprover.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchNewApprover.TabIndex = 9
        '
        'cboOldApprover
        '
        Me.cboOldApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOldApprover.DropDownWidth = 300
        Me.cboOldApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOldApprover.FormattingEnabled = True
        Me.cboOldApprover.Location = New System.Drawing.Point(87, 33)
        Me.cboOldApprover.Name = "cboOldApprover"
        Me.cboOldApprover.Size = New System.Drawing.Size(238, 21)
        Me.cboOldApprover.TabIndex = 1
        '
        'objbtnSearchNewLevel
        '
        Me.objbtnSearchNewLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchNewLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchNewLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchNewLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchNewLevel.BorderSelected = False
        Me.objbtnSearchNewLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchNewLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearchNewLevel.Image = CType(resources.GetObject("objbtnSearchNewLevel.Image"), System.Drawing.Image)
        Me.objbtnSearchNewLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchNewLevel.Location = New System.Drawing.Point(733, 59)
        Me.objbtnSearchNewLevel.Name = "objbtnSearchNewLevel"
        Me.objbtnSearchNewLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchNewLevel.TabIndex = 8
        '
        'cboNewApprover
        '
        Me.cboNewApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNewApprover.DropDownWidth = 300
        Me.cboNewApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNewApprover.FormattingEnabled = True
        Me.cboNewApprover.Location = New System.Drawing.Point(491, 33)
        Me.cboNewApprover.Name = "cboNewApprover"
        Me.cboNewApprover.Size = New System.Drawing.Size(236, 21)
        Me.cboNewApprover.TabIndex = 5
        '
        'cboNewLevel
        '
        Me.cboNewLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNewLevel.DropDownWidth = 300
        Me.cboNewLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNewLevel.FormattingEnabled = True
        Me.cboNewLevel.Location = New System.Drawing.Point(491, 60)
        Me.cboNewLevel.Name = "cboNewLevel"
        Me.cboNewLevel.Size = New System.Drawing.Size(236, 21)
        Me.cboNewLevel.TabIndex = 6
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 599)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(763, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(553, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(96, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(655, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(96, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        '
        'frmTransferStaffRequisitionApprover
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(763, 654)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTransferStaffRequisitionApprover"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Transfer Staff Requisition Approver"
        Me.pnlMain.ResumeLayout(False)
        Me.gbInfo.ResumeLayout(False)
        Me.gbInfo.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgNewJob, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgAssignedJob, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgOldJob, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOldEmp.ResumeLayout(False)
        Me.pnlOldEmp.PerformLayout()
        CType(Me.dgOldAllocation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlNewEmp.ResumeLayout(False)
        Me.tabNewApprover.ResumeLayout(False)
        Me.tbpMigratedEmployee.ResumeLayout(False)
        Me.tbpMigratedEmployee.PerformLayout()
        CType(Me.dgNewAllocation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpAssignEmployee.ResumeLayout(False)
        Me.tbpAssignEmployee.PerformLayout()
        CType(Me.dgAssignedAllocation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents tabNewApprover As System.Windows.Forms.TabControl
    Friend WithEvents tbpMigratedEmployee As System.Windows.Forms.TabPage
    Friend WithEvents txtSearchNewAllocation As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objchkNewCheck As System.Windows.Forms.CheckBox
    Friend WithEvents dgNewAllocation As System.Windows.Forms.DataGridView
    Friend WithEvents tbpAssignEmployee As System.Windows.Forms.TabPage
    Friend WithEvents dgAssignedAllocation As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearchAssignedAllocation As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblNewAllocation As System.Windows.Forms.Label
    Friend WithEvents txtSearchOldAllocation As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchOldLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOldLevel As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchOldApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlOldEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkOldAllocationSelect As System.Windows.Forms.CheckBox
    Friend WithEvents dgOldAllocation As System.Windows.Forms.DataGridView
    Friend WithEvents pnlNewEmp As System.Windows.Forms.Panel
    Friend WithEvents objbtnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnUnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents lblNewLevel As System.Windows.Forms.Label
    Friend WithEvents lblNewApprover As System.Windows.Forms.Label
    Friend WithEvents lblCaption1 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchNewApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOldApprover As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchNewLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents cboNewApprover As System.Windows.Forms.ComboBox
    Friend WithEvents cboNewLevel As System.Windows.Forms.ComboBox
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblOldAllocation As System.Windows.Forms.Label
    Friend WithEvents cboOldAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents cboNewAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents colhAssignedAllocationName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents objchkOldJobSelect As System.Windows.Forms.CheckBox
    Friend WithEvents dgOldJob As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearchOldJob As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents txtSearchNewJob As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objchkNewJobSelect As System.Windows.Forms.CheckBox
    Friend WithEvents dgNewJob As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents dgAssignedJob As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearchAssignedJob As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents colhAssignedJobName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhOldJobCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhOldJobName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhOldAllocationCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhOldAllocationName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhNewAllocationCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhNewAllocationName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objchkNewAllocationSelect As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnAssignJob As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnUnAssignJob As eZee.Common.eZeeLightButton
    Friend WithEvents objcolhNewJobCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhNewJobName As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
