﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStaffRequisition_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStaffRequisition_AddEdit))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbApproverMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowPreviousJobEmp = New System.Windows.Forms.CheckBox
        Me.lblJobAdvert = New System.Windows.Forms.Label
        Me.cboJobAdvert = New System.Windows.Forms.ComboBox
        Me.objbtnSearchJobReportingTo = New eZee.Common.eZeeGradientButton
        Me.cboJobReportTo = New System.Windows.Forms.ComboBox
        Me.lblJobReportTo = New System.Windows.Forms.Label
        Me.lnkViewJobDescription = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchGrade = New eZee.Common.eZeeGradientButton
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvStaffRequisitionAttachment = New System.Windows.Forms.DataGridView
        Me.objcohDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSize = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDownload = New System.Windows.Forms.DataGridViewLinkColumn
        Me.objcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhScanUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblGrade = New System.Windows.Forms.Label
        Me.cboDocumentType = New System.Windows.Forms.ComboBox
        Me.btnAddAttachment = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkShowInActiveEmployee = New System.Windows.Forms.CheckBox
        Me.lblDocumentType = New System.Windows.Forms.Label
        Me.cboGrade = New System.Windows.Forms.ComboBox
        Me.objbtnSearchGradeLevel = New eZee.Common.eZeeGradientButton
        Me.lblGradeLevel = New System.Windows.Forms.Label
        Me.cboGradeLevel = New System.Windows.Forms.ComboBox
        Me.txtJobDesc = New System.Windows.Forms.RichTextBox
        Me.lblEmploymentType = New System.Windows.Forms.Label
        Me.lblContractDuration = New System.Windows.Forms.Label
        Me.nudContractDuration = New System.Windows.Forms.NumericUpDown
        Me.cboEmploymentType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchClass = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchJobTitle = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchClassGroup = New eZee.Common.eZeeGradientButton
        Me.lblSearchEmp = New System.Windows.Forms.Label
        Me.objtlpJob = New System.Windows.Forms.TableLayoutPanel
        Me.objlblVariation = New System.Windows.Forms.Label
        Me.objlblAvailable = New System.Windows.Forms.Label
        Me.lblAvailable = New System.Windows.Forms.Label
        Me.lblVariation = New System.Windows.Forms.Label
        Me.objlblPlanned = New System.Windows.Forms.Label
        Me.lblPlanned = New System.Windows.Forms.Label
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.lblJobDesc = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.txtFormNo = New System.Windows.Forms.TextBox
        Me.lblFormNo = New System.Windows.Forms.Label
        Me.lblClass = New System.Windows.Forms.Label
        Me.nudPosition = New System.Windows.Forms.NumericUpDown
        Me.lblNoofPosition = New System.Windows.Forms.Label
        Me.lblLeavingReason = New System.Windows.Forms.Label
        Me.cboClass = New System.Windows.Forms.ComboBox
        Me.objbtnLeavingReason = New eZee.Common.eZeeGradientButton
        Me.txtAddStaffReason = New System.Windows.Forms.TextBox
        Me.lblAddStaffReason = New System.Windows.Forms.Label
        Me.cboLeavingReason = New System.Windows.Forms.ComboBox
        Me.lblClassGroup = New System.Windows.Forms.Label
        Me.cboClassGroup = New System.Windows.Forms.ComboBox
        Me.dtpWorkStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblWorkStartDate = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.cboName = New System.Windows.Forms.ComboBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.cboAllocation = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.pnlEmployee = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvEmployeeList = New System.Windows.Forms.DataGridView
        Me.objcolhCheckAll = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objcolhEmpID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmpName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGrade = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSalaryBand = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhActionReasonId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhAUD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.sfdAttachment = New System.Windows.Forms.SaveFileDialog
        Me.ofdAttachment = New System.Windows.Forms.OpenFileDialog
        Me.lnkViewStaffRequisitionFormReport = New System.Windows.Forms.LinkLabel
        Me.pnlMain.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbApproverMapping.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvStaffRequisitionAttachment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudContractDuration, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objtlpJob.SuspendLayout()
        CType(Me.nudPosition, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlEmployee.SuspendLayout()
        CType(Me.dgvEmployeeList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.gbApproverMapping)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(994, 507)
        Me.pnlMain.TabIndex = 2
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.lnkViewStaffRequisitionFormReport)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 452)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(994, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(782, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(885, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbApproverMapping
        '
        Me.gbApproverMapping.BorderColor = System.Drawing.Color.Black
        Me.gbApproverMapping.Checked = False
        Me.gbApproverMapping.CollapseAllExceptThis = False
        Me.gbApproverMapping.CollapsedHoverImage = Nothing
        Me.gbApproverMapping.CollapsedNormalImage = Nothing
        Me.gbApproverMapping.CollapsedPressedImage = Nothing
        Me.gbApproverMapping.CollapseOnLoad = False
        Me.gbApproverMapping.Controls.Add(Me.chkShowPreviousJobEmp)
        Me.gbApproverMapping.Controls.Add(Me.lblJobAdvert)
        Me.gbApproverMapping.Controls.Add(Me.cboJobAdvert)
        Me.gbApproverMapping.Controls.Add(Me.objbtnSearchJobReportingTo)
        Me.gbApproverMapping.Controls.Add(Me.cboJobReportTo)
        Me.gbApproverMapping.Controls.Add(Me.lblJobReportTo)
        Me.gbApproverMapping.Controls.Add(Me.lnkViewJobDescription)
        Me.gbApproverMapping.Controls.Add(Me.objbtnSearchGrade)
        Me.gbApproverMapping.Controls.Add(Me.Panel1)
        Me.gbApproverMapping.Controls.Add(Me.lblGrade)
        Me.gbApproverMapping.Controls.Add(Me.cboDocumentType)
        Me.gbApproverMapping.Controls.Add(Me.btnAddAttachment)
        Me.gbApproverMapping.Controls.Add(Me.chkShowInActiveEmployee)
        Me.gbApproverMapping.Controls.Add(Me.lblDocumentType)
        Me.gbApproverMapping.Controls.Add(Me.cboGrade)
        Me.gbApproverMapping.Controls.Add(Me.objbtnSearchGradeLevel)
        Me.gbApproverMapping.Controls.Add(Me.lblGradeLevel)
        Me.gbApproverMapping.Controls.Add(Me.cboGradeLevel)
        Me.gbApproverMapping.Controls.Add(Me.txtJobDesc)
        Me.gbApproverMapping.Controls.Add(Me.lblEmploymentType)
        Me.gbApproverMapping.Controls.Add(Me.lblContractDuration)
        Me.gbApproverMapping.Controls.Add(Me.nudContractDuration)
        Me.gbApproverMapping.Controls.Add(Me.cboEmploymentType)
        Me.gbApproverMapping.Controls.Add(Me.objbtnSearchClass)
        Me.gbApproverMapping.Controls.Add(Me.objbtnSearchJobTitle)
        Me.gbApproverMapping.Controls.Add(Me.objbtnSearchClassGroup)
        Me.gbApproverMapping.Controls.Add(Me.lblSearchEmp)
        Me.gbApproverMapping.Controls.Add(Me.objtlpJob)
        Me.gbApproverMapping.Controls.Add(Me.txtSearchEmp)
        Me.gbApproverMapping.Controls.Add(Me.lblJobDesc)
        Me.gbApproverMapping.Controls.Add(Me.cboJob)
        Me.gbApproverMapping.Controls.Add(Me.lblJob)
        Me.gbApproverMapping.Controls.Add(Me.txtFormNo)
        Me.gbApproverMapping.Controls.Add(Me.lblFormNo)
        Me.gbApproverMapping.Controls.Add(Me.lblClass)
        Me.gbApproverMapping.Controls.Add(Me.nudPosition)
        Me.gbApproverMapping.Controls.Add(Me.lblNoofPosition)
        Me.gbApproverMapping.Controls.Add(Me.lblLeavingReason)
        Me.gbApproverMapping.Controls.Add(Me.cboClass)
        Me.gbApproverMapping.Controls.Add(Me.objbtnLeavingReason)
        Me.gbApproverMapping.Controls.Add(Me.txtAddStaffReason)
        Me.gbApproverMapping.Controls.Add(Me.lblAddStaffReason)
        Me.gbApproverMapping.Controls.Add(Me.cboLeavingReason)
        Me.gbApproverMapping.Controls.Add(Me.lblClassGroup)
        Me.gbApproverMapping.Controls.Add(Me.cboClassGroup)
        Me.gbApproverMapping.Controls.Add(Me.dtpWorkStartDate)
        Me.gbApproverMapping.Controls.Add(Me.lblWorkStartDate)
        Me.gbApproverMapping.Controls.Add(Me.lblName)
        Me.gbApproverMapping.Controls.Add(Me.cboName)
        Me.gbApproverMapping.Controls.Add(Me.cboStatus)
        Me.gbApproverMapping.Controls.Add(Me.lblAllocation)
        Me.gbApproverMapping.Controls.Add(Me.cboAllocation)
        Me.gbApproverMapping.Controls.Add(Me.lblStatus)
        Me.gbApproverMapping.Controls.Add(Me.pnlEmployee)
        Me.gbApproverMapping.Controls.Add(Me.lblEmployee)
        Me.gbApproverMapping.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbApproverMapping.ExpandedHoverImage = Nothing
        Me.gbApproverMapping.ExpandedNormalImage = Nothing
        Me.gbApproverMapping.ExpandedPressedImage = Nothing
        Me.gbApproverMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbApproverMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbApproverMapping.HeaderHeight = 25
        Me.gbApproverMapping.HeaderMessage = ""
        Me.gbApproverMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbApproverMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbApproverMapping.HeightOnCollapse = 0
        Me.gbApproverMapping.LeftTextSpace = 0
        Me.gbApproverMapping.Location = New System.Drawing.Point(0, 0)
        Me.gbApproverMapping.Name = "gbApproverMapping"
        Me.gbApproverMapping.OpenHeight = 300
        Me.gbApproverMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbApproverMapping.ShowBorder = True
        Me.gbApproverMapping.ShowCheckBox = False
        Me.gbApproverMapping.ShowCollapseButton = False
        Me.gbApproverMapping.ShowDefaultBorderColor = True
        Me.gbApproverMapping.ShowDownButton = False
        Me.gbApproverMapping.ShowHeader = True
        Me.gbApproverMapping.Size = New System.Drawing.Size(994, 507)
        Me.gbApproverMapping.TabIndex = 0
        Me.gbApproverMapping.Temp = 0
        Me.gbApproverMapping.Text = "Staff Requisition Add/Edit"
        Me.gbApproverMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowPreviousJobEmp
        '
        Me.chkShowPreviousJobEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowPreviousJobEmp.Location = New System.Drawing.Point(11, 369)
        Me.chkShowPreviousJobEmp.Name = "chkShowPreviousJobEmp"
        Me.chkShowPreviousJobEmp.Size = New System.Drawing.Size(104, 35)
        Me.chkShowPreviousJobEmp.TabIndex = 331
        Me.chkShowPreviousJobEmp.Text = "Show Previous Job"
        Me.chkShowPreviousJobEmp.UseVisualStyleBackColor = True
        '
        'lblJobAdvert
        '
        Me.lblJobAdvert.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobAdvert.Location = New System.Drawing.Point(320, 203)
        Me.lblJobAdvert.Name = "lblJobAdvert"
        Me.lblJobAdvert.Size = New System.Drawing.Size(70, 16)
        Me.lblJobAdvert.TabIndex = 329
        Me.lblJobAdvert.Text = "Job Advert"
        Me.lblJobAdvert.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJobAdvert
        '
        Me.cboJobAdvert.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJobAdvert.DropDownWidth = 300
        Me.cboJobAdvert.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobAdvert.FormattingEnabled = True
        Me.cboJobAdvert.Location = New System.Drawing.Point(417, 202)
        Me.cboJobAdvert.Name = "cboJobAdvert"
        Me.cboJobAdvert.Size = New System.Drawing.Size(131, 21)
        Me.cboJobAdvert.TabIndex = 328
        '
        'objbtnSearchJobReportingTo
        '
        Me.objbtnSearchJobReportingTo.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJobReportingTo.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobReportingTo.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobReportingTo.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJobReportingTo.BorderSelected = False
        Me.objbtnSearchJobReportingTo.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJobReportingTo.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJobReportingTo.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJobReportingTo.Location = New System.Drawing.Point(290, 201)
        Me.objbtnSearchJobReportingTo.Name = "objbtnSearchJobReportingTo"
        Me.objbtnSearchJobReportingTo.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJobReportingTo.TabIndex = 327
        '
        'cboJobReportTo
        '
        Me.cboJobReportTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJobReportTo.DropDownWidth = 300
        Me.cboJobReportTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJobReportTo.FormattingEnabled = True
        Me.cboJobReportTo.Location = New System.Drawing.Point(121, 201)
        Me.cboJobReportTo.Name = "cboJobReportTo"
        Me.cboJobReportTo.Size = New System.Drawing.Size(163, 21)
        Me.cboJobReportTo.TabIndex = 325
        '
        'lblJobReportTo
        '
        Me.lblJobReportTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobReportTo.Location = New System.Drawing.Point(8, 202)
        Me.lblJobReportTo.Name = "lblJobReportTo"
        Me.lblJobReportTo.Size = New System.Drawing.Size(107, 16)
        Me.lblJobReportTo.TabIndex = 326
        Me.lblJobReportTo.Text = "Job Reporting Line"
        Me.lblJobReportTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkViewJobDescription
        '
        Me.lnkViewJobDescription.BackColor = System.Drawing.Color.Transparent
        Me.lnkViewJobDescription.Location = New System.Drawing.Point(118, 185)
        Me.lnkViewJobDescription.Name = "lnkViewJobDescription"
        Me.lnkViewJobDescription.Size = New System.Drawing.Size(230, 13)
        Me.lnkViewJobDescription.TabIndex = 318
        Me.lnkViewJobDescription.TabStop = True
        Me.lnkViewJobDescription.Text = "View Job Description"
        '
        'objbtnSearchGrade
        '
        Me.objbtnSearchGrade.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGrade.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGrade.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGrade.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGrade.BorderSelected = False
        Me.objbtnSearchGrade.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGrade.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGrade.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGrade.Location = New System.Drawing.Point(290, 228)
        Me.objbtnSearchGrade.Name = "objbtnSearchGrade"
        Me.objbtnSearchGrade.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGrade.TabIndex = 98
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.dgvStaffRequisitionAttachment)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(584, 233)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(398, 212)
        Me.Panel1.TabIndex = 323
        '
        'dgvStaffRequisitionAttachment
        '
        Me.dgvStaffRequisitionAttachment.AllowUserToAddRows = False
        Me.dgvStaffRequisitionAttachment.AllowUserToDeleteRows = False
        Me.dgvStaffRequisitionAttachment.AllowUserToResizeRows = False
        Me.dgvStaffRequisitionAttachment.BackgroundColor = System.Drawing.Color.White
        Me.dgvStaffRequisitionAttachment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvStaffRequisitionAttachment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvStaffRequisitionAttachment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcohDelete, Me.colhName, Me.colhSize, Me.objcolhDownload, Me.objcolhGUID, Me.objcolhScanUnkId})
        Me.dgvStaffRequisitionAttachment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvStaffRequisitionAttachment.Location = New System.Drawing.Point(0, 0)
        Me.dgvStaffRequisitionAttachment.Name = "dgvStaffRequisitionAttachment"
        Me.dgvStaffRequisitionAttachment.RowHeadersVisible = False
        Me.dgvStaffRequisitionAttachment.Size = New System.Drawing.Size(398, 212)
        Me.dgvStaffRequisitionAttachment.TabIndex = 228
        '
        'objcohDelete
        '
        Me.objcohDelete.HeaderText = ""
        Me.objcohDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objcohDelete.Name = "objcohDelete"
        Me.objcohDelete.ReadOnly = True
        Me.objcohDelete.Width = 25
        '
        'colhName
        '
        Me.colhName.HeaderText = "File Name"
        Me.colhName.Name = "colhName"
        Me.colhName.ReadOnly = True
        Me.colhName.Width = 150
        '
        'colhSize
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhSize.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhSize.HeaderText = "File Size"
        Me.colhSize.Name = "colhSize"
        Me.colhSize.ReadOnly = True
        Me.colhSize.Width = 120
        '
        'objcolhDownload
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        Me.objcolhDownload.DefaultCellStyle = DataGridViewCellStyle2
        Me.objcolhDownload.HeaderText = "Download"
        Me.objcolhDownload.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objcolhDownload.Name = "objcolhDownload"
        Me.objcolhDownload.ReadOnly = True
        Me.objcolhDownload.Text = "Download"
        Me.objcolhDownload.UseColumnTextForLinkValue = True
        '
        'objcolhGUID
        '
        Me.objcolhGUID.HeaderText = "objcolhGUID"
        Me.objcolhGUID.Name = "objcolhGUID"
        Me.objcolhGUID.Visible = False
        '
        'objcolhScanUnkId
        '
        Me.objcolhScanUnkId.HeaderText = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Name = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Visible = False
        '
        'lblGrade
        '
        Me.lblGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrade.Location = New System.Drawing.Point(11, 230)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(104, 16)
        Me.lblGrade.TabIndex = 97
        Me.lblGrade.Text = "Grade"
        Me.lblGrade.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDocumentType
        '
        Me.cboDocumentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDocumentType.FormattingEnabled = True
        Me.cboDocumentType.Location = New System.Drawing.Point(694, 203)
        Me.cboDocumentType.Name = "cboDocumentType"
        Me.cboDocumentType.Size = New System.Drawing.Size(184, 21)
        Me.cboDocumentType.TabIndex = 321
        '
        'btnAddAttachment
        '
        Me.btnAddAttachment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddAttachment.BackColor = System.Drawing.Color.White
        Me.btnAddAttachment.BackgroundImage = CType(resources.GetObject("btnAddAttachment.BackgroundImage"), System.Drawing.Image)
        Me.btnAddAttachment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddAttachment.BorderColor = System.Drawing.Color.Empty
        Me.btnAddAttachment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddAttachment.FlatAppearance.BorderSize = 0
        Me.btnAddAttachment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddAttachment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddAttachment.ForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddAttachment.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddAttachment.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.Location = New System.Drawing.Point(884, 196)
        Me.btnAddAttachment.Name = "btnAddAttachment"
        Me.btnAddAttachment.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddAttachment.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.Size = New System.Drawing.Size(98, 29)
        Me.btnAddAttachment.TabIndex = 320
        Me.btnAddAttachment.Text = "&Browse"
        Me.btnAddAttachment.UseVisualStyleBackColor = True
        '
        'chkShowInActiveEmployee
        '
        Me.chkShowInActiveEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowInActiveEmployee.Location = New System.Drawing.Point(11, 328)
        Me.chkShowInActiveEmployee.Name = "chkShowInActiveEmployee"
        Me.chkShowInActiveEmployee.Size = New System.Drawing.Size(104, 35)
        Me.chkShowInActiveEmployee.TabIndex = 111
        Me.chkShowInActiveEmployee.Text = "Show Inactive Employee"
        Me.chkShowInActiveEmployee.UseVisualStyleBackColor = True
        '
        'lblDocumentType
        '
        Me.lblDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocumentType.Location = New System.Drawing.Point(581, 203)
        Me.lblDocumentType.Name = "lblDocumentType"
        Me.lblDocumentType.Size = New System.Drawing.Size(107, 16)
        Me.lblDocumentType.TabIndex = 322
        Me.lblDocumentType.Text = "Document Type"
        Me.lblDocumentType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrade
        '
        Me.cboGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrade.DropDownWidth = 300
        Me.cboGrade.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrade.FormattingEnabled = True
        Me.cboGrade.Location = New System.Drawing.Point(121, 228)
        Me.cboGrade.Name = "cboGrade"
        Me.cboGrade.Size = New System.Drawing.Size(163, 21)
        Me.cboGrade.TabIndex = 8
        '
        'objbtnSearchGradeLevel
        '
        Me.objbtnSearchGradeLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchGradeLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchGradeLevel.BorderSelected = False
        Me.objbtnSearchGradeLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchGradeLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchGradeLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchGradeLevel.Location = New System.Drawing.Point(554, 229)
        Me.objbtnSearchGradeLevel.Name = "objbtnSearchGradeLevel"
        Me.objbtnSearchGradeLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchGradeLevel.TabIndex = 95
        '
        'lblGradeLevel
        '
        Me.lblGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradeLevel.Location = New System.Drawing.Point(320, 230)
        Me.lblGradeLevel.Name = "lblGradeLevel"
        Me.lblGradeLevel.Size = New System.Drawing.Size(70, 16)
        Me.lblGradeLevel.TabIndex = 89
        Me.lblGradeLevel.Text = "Grade Level"
        Me.lblGradeLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGradeLevel
        '
        Me.cboGradeLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGradeLevel.DropDownWidth = 300
        Me.cboGradeLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGradeLevel.FormattingEnabled = True
        Me.cboGradeLevel.Location = New System.Drawing.Point(417, 229)
        Me.cboGradeLevel.Name = "cboGradeLevel"
        Me.cboGradeLevel.Size = New System.Drawing.Size(131, 21)
        Me.cboGradeLevel.TabIndex = 9
        '
        'txtJobDesc
        '
        Me.txtJobDesc.AcceptsTab = True
        Me.txtJobDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtJobDesc.Location = New System.Drawing.Point(121, 142)
        Me.txtJobDesc.Name = "txtJobDesc"
        Me.txtJobDesc.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth
        Me.txtJobDesc.Size = New System.Drawing.Size(151, 40)
        Me.txtJobDesc.TabIndex = 7
        Me.txtJobDesc.Text = ""
        '
        'lblEmploymentType
        '
        Me.lblEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmploymentType.Location = New System.Drawing.Point(581, 61)
        Me.lblEmploymentType.Name = "lblEmploymentType"
        Me.lblEmploymentType.Size = New System.Drawing.Size(107, 16)
        Me.lblEmploymentType.TabIndex = 91
        Me.lblEmploymentType.Text = "Employment Type"
        Me.lblEmploymentType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblContractDuration
        '
        Me.lblContractDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContractDuration.Location = New System.Drawing.Point(581, 90)
        Me.lblContractDuration.Name = "lblContractDuration"
        Me.lblContractDuration.Size = New System.Drawing.Size(107, 27)
        Me.lblContractDuration.TabIndex = 93
        Me.lblContractDuration.Text = "Contract Duration (months) if Employment type is Contract"
        Me.lblContractDuration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudContractDuration
        '
        Me.nudContractDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudContractDuration.Location = New System.Drawing.Point(694, 95)
        Me.nudContractDuration.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.nudContractDuration.Name = "nudContractDuration"
        Me.nudContractDuration.Size = New System.Drawing.Size(112, 21)
        Me.nudContractDuration.TabIndex = 13
        '
        'cboEmploymentType
        '
        Me.cboEmploymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmploymentType.DropDownWidth = 300
        Me.cboEmploymentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmploymentType.FormattingEnabled = True
        Me.cboEmploymentType.Location = New System.Drawing.Point(694, 59)
        Me.cboEmploymentType.Name = "cboEmploymentType"
        Me.cboEmploymentType.Size = New System.Drawing.Size(167, 21)
        Me.cboEmploymentType.TabIndex = 12
        '
        'objbtnSearchClass
        '
        Me.objbtnSearchClass.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchClass.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchClass.BorderSelected = False
        Me.objbtnSearchClass.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchClass.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchClass.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchClass.Location = New System.Drawing.Point(552, 88)
        Me.objbtnSearchClass.Name = "objbtnSearchClass"
        Me.objbtnSearchClass.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchClass.TabIndex = 85
        '
        'objbtnSearchJobTitle
        '
        Me.objbtnSearchJobTitle.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchJobTitle.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobTitle.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchJobTitle.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchJobTitle.BorderSelected = False
        Me.objbtnSearchJobTitle.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchJobTitle.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchJobTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchJobTitle.Location = New System.Drawing.Point(278, 115)
        Me.objbtnSearchJobTitle.Name = "objbtnSearchJobTitle"
        Me.objbtnSearchJobTitle.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchJobTitle.TabIndex = 84
        '
        'objbtnSearchClassGroup
        '
        Me.objbtnSearchClassGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchClassGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchClassGroup.BorderSelected = False
        Me.objbtnSearchClassGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchClassGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchClassGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchClassGroup.Location = New System.Drawing.Point(278, 88)
        Me.objbtnSearchClassGroup.Name = "objbtnSearchClassGroup"
        Me.objbtnSearchClassGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchClassGroup.TabIndex = 83
        '
        'lblSearchEmp
        '
        Me.lblSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.lblSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearchEmp.Location = New System.Drawing.Point(8, 259)
        Me.lblSearchEmp.Name = "lblSearchEmp"
        Me.lblSearchEmp.Size = New System.Drawing.Size(107, 13)
        Me.lblSearchEmp.TabIndex = 80
        Me.lblSearchEmp.Text = "Search Employee"
        '
        'objtlpJob
        '
        Me.objtlpJob.BackColor = System.Drawing.SystemColors.Control
        Me.objtlpJob.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.objtlpJob.ColumnCount = 2
        Me.objtlpJob.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.0!))
        Me.objtlpJob.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.0!))
        Me.objtlpJob.Controls.Add(Me.objlblVariation, 1, 2)
        Me.objtlpJob.Controls.Add(Me.objlblAvailable, 1, 1)
        Me.objtlpJob.Controls.Add(Me.lblAvailable, 0, 1)
        Me.objtlpJob.Controls.Add(Me.lblVariation, 0, 2)
        Me.objtlpJob.Controls.Add(Me.objlblPlanned, 1, 0)
        Me.objtlpJob.Controls.Add(Me.lblPlanned, 0, 0)
        Me.objtlpJob.Location = New System.Drawing.Point(303, 114)
        Me.objtlpJob.Name = "objtlpJob"
        Me.objtlpJob.RowCount = 3
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtlpJob.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.objtlpJob.Size = New System.Drawing.Size(243, 68)
        Me.objtlpJob.TabIndex = 75
        '
        'objlblVariation
        '
        Me.objlblVariation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblVariation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblVariation.Location = New System.Drawing.Point(161, 43)
        Me.objlblVariation.Name = "objlblVariation"
        Me.objlblVariation.Size = New System.Drawing.Size(78, 24)
        Me.objlblVariation.TabIndex = 39
        Me.objlblVariation.Text = "0"
        Me.objlblVariation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblAvailable
        '
        Me.objlblAvailable.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblAvailable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblAvailable.Location = New System.Drawing.Point(161, 22)
        Me.objlblAvailable.Name = "objlblAvailable"
        Me.objlblAvailable.Size = New System.Drawing.Size(78, 20)
        Me.objlblAvailable.TabIndex = 39
        Me.objlblAvailable.Text = "0"
        Me.objlblAvailable.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAvailable
        '
        Me.lblAvailable.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblAvailable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAvailable.Location = New System.Drawing.Point(4, 22)
        Me.lblAvailable.Name = "lblAvailable"
        Me.lblAvailable.Size = New System.Drawing.Size(150, 20)
        Me.lblAvailable.TabIndex = 25
        Me.lblAvailable.Text = "Available Head Counts"
        Me.lblAvailable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVariation
        '
        Me.lblVariation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblVariation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVariation.Location = New System.Drawing.Point(4, 43)
        Me.lblVariation.Name = "lblVariation"
        Me.lblVariation.Size = New System.Drawing.Size(150, 24)
        Me.lblVariation.TabIndex = 4
        Me.lblVariation.Text = "Variations"
        Me.lblVariation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblPlanned
        '
        Me.objlblPlanned.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblPlanned.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblPlanned.Location = New System.Drawing.Point(161, 1)
        Me.objlblPlanned.Name = "objlblPlanned"
        Me.objlblPlanned.Size = New System.Drawing.Size(78, 20)
        Me.objlblPlanned.TabIndex = 3
        Me.objlblPlanned.Text = "0"
        Me.objlblPlanned.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPlanned
        '
        Me.lblPlanned.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblPlanned.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlanned.Location = New System.Drawing.Point(4, 1)
        Me.lblPlanned.Name = "lblPlanned"
        Me.lblPlanned.Size = New System.Drawing.Size(150, 20)
        Me.lblPlanned.TabIndex = 2
        Me.lblPlanned.Text = "Planned Head Counts"
        Me.lblPlanned.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchEmp.Location = New System.Drawing.Point(121, 255)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(452, 21)
        Me.txtSearchEmp.TabIndex = 10
        '
        'lblJobDesc
        '
        Me.lblJobDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJobDesc.Location = New System.Drawing.Point(8, 142)
        Me.lblJobDesc.Name = "lblJobDesc"
        Me.lblJobDesc.Size = New System.Drawing.Size(107, 40)
        Me.lblJobDesc.TabIndex = 74
        Me.lblJobDesc.Text = "Job Description && Indicators/Remarks"
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 300
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(121, 115)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(151, 21)
        Me.cboJob.TabIndex = 6
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(8, 116)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(107, 16)
        Me.lblJob.TabIndex = 73
        Me.lblJob.Text = "Job Title"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFormNo
        '
        Me.txtFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFormNo.Location = New System.Drawing.Point(121, 34)
        Me.txtFormNo.MaxLength = 255
        Me.txtFormNo.Name = "txtFormNo"
        Me.txtFormNo.Size = New System.Drawing.Size(151, 21)
        Me.txtFormNo.TabIndex = 0
        '
        'lblFormNo
        '
        Me.lblFormNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormNo.Location = New System.Drawing.Point(8, 36)
        Me.lblFormNo.Name = "lblFormNo"
        Me.lblFormNo.Size = New System.Drawing.Size(107, 16)
        Me.lblFormNo.TabIndex = 53
        Me.lblFormNo.Text = "Form No."
        Me.lblFormNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblClass
        '
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.Location = New System.Drawing.Point(300, 90)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(94, 16)
        Me.lblClass.TabIndex = 33
        Me.lblClass.Text = "Class"
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudPosition
        '
        Me.nudPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPosition.Location = New System.Drawing.Point(694, 171)
        Me.nudPosition.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.nudPosition.Name = "nudPosition"
        Me.nudPosition.Size = New System.Drawing.Size(58, 21)
        Me.nudPosition.TabIndex = 15
        Me.nudPosition.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblNoofPosition
        '
        Me.lblNoofPosition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoofPosition.Location = New System.Drawing.Point(581, 173)
        Me.lblNoofPosition.Name = "lblNoofPosition"
        Me.lblNoofPosition.Size = New System.Drawing.Size(107, 16)
        Me.lblNoofPosition.TabIndex = 69
        Me.lblNoofPosition.Text = "No. of Position"
        Me.lblNoofPosition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLeavingReason
        '
        Me.lblLeavingReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeavingReason.Location = New System.Drawing.Point(581, 34)
        Me.lblLeavingReason.Name = "lblLeavingReason"
        Me.lblLeavingReason.Size = New System.Drawing.Size(107, 16)
        Me.lblLeavingReason.TabIndex = 46
        Me.lblLeavingReason.Text = "Leaving Reason"
        Me.lblLeavingReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClass
        '
        Me.cboClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClass.DropDownWidth = 300
        Me.cboClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClass.FormattingEnabled = True
        Me.cboClass.Location = New System.Drawing.Point(400, 88)
        Me.cboClass.Name = "cboClass"
        Me.cboClass.Size = New System.Drawing.Size(146, 21)
        Me.cboClass.TabIndex = 5
        '
        'objbtnLeavingReason
        '
        Me.objbtnLeavingReason.BackColor = System.Drawing.Color.Transparent
        Me.objbtnLeavingReason.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnLeavingReason.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnLeavingReason.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnLeavingReason.BorderSelected = False
        Me.objbtnLeavingReason.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnLeavingReason.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnLeavingReason.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnLeavingReason.Location = New System.Drawing.Point(867, 32)
        Me.objbtnLeavingReason.Name = "objbtnLeavingReason"
        Me.objbtnLeavingReason.Size = New System.Drawing.Size(21, 21)
        Me.objbtnLeavingReason.TabIndex = 51
        '
        'txtAddStaffReason
        '
        Me.txtAddStaffReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddStaffReason.Location = New System.Drawing.Point(694, 125)
        Me.txtAddStaffReason.Multiline = True
        Me.txtAddStaffReason.Name = "txtAddStaffReason"
        Me.txtAddStaffReason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAddStaffReason.Size = New System.Drawing.Size(288, 40)
        Me.txtAddStaffReason.TabIndex = 14
        '
        'lblAddStaffReason
        '
        Me.lblAddStaffReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddStaffReason.Location = New System.Drawing.Point(581, 125)
        Me.lblAddStaffReason.Name = "lblAddStaffReason"
        Me.lblAddStaffReason.Size = New System.Drawing.Size(107, 40)
        Me.lblAddStaffReason.TabIndex = 48
        Me.lblAddStaffReason.Text = "Additional Staff Reason"
        '
        'cboLeavingReason
        '
        Me.cboLeavingReason.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLeavingReason.DropDownWidth = 300
        Me.cboLeavingReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLeavingReason.FormattingEnabled = True
        Me.cboLeavingReason.Location = New System.Drawing.Point(694, 32)
        Me.cboLeavingReason.Name = "cboLeavingReason"
        Me.cboLeavingReason.Size = New System.Drawing.Size(167, 21)
        Me.cboLeavingReason.TabIndex = 11
        '
        'lblClassGroup
        '
        Me.lblClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassGroup.Location = New System.Drawing.Point(8, 90)
        Me.lblClassGroup.Name = "lblClassGroup"
        Me.lblClassGroup.Size = New System.Drawing.Size(107, 16)
        Me.lblClassGroup.TabIndex = 31
        Me.lblClassGroup.Text = "Class Group"
        Me.lblClassGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboClassGroup
        '
        Me.cboClassGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboClassGroup.DropDownWidth = 300
        Me.cboClassGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClassGroup.FormattingEnabled = True
        Me.cboClassGroup.Location = New System.Drawing.Point(121, 88)
        Me.cboClassGroup.Name = "cboClassGroup"
        Me.cboClassGroup.Size = New System.Drawing.Size(151, 21)
        Me.cboClassGroup.TabIndex = 4
        '
        'dtpWorkStartDate
        '
        Me.dtpWorkStartDate.Checked = False
        Me.dtpWorkStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpWorkStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpWorkStartDate.Location = New System.Drawing.Point(867, 168)
        Me.dtpWorkStartDate.Name = "dtpWorkStartDate"
        Me.dtpWorkStartDate.Size = New System.Drawing.Size(115, 21)
        Me.dtpWorkStartDate.TabIndex = 16
        '
        'lblWorkStartDate
        '
        Me.lblWorkStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWorkStartDate.Location = New System.Drawing.Point(758, 171)
        Me.lblWorkStartDate.Name = "lblWorkStartDate"
        Me.lblWorkStartDate.Size = New System.Drawing.Size(103, 18)
        Me.lblWorkStartDate.TabIndex = 40
        Me.lblWorkStartDate.Text = "Date to Start Work"
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(300, 62)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(94, 16)
        Me.lblName.TabIndex = 29
        Me.lblName.Text = "Name"
        '
        'cboName
        '
        Me.cboName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboName.DropDownWidth = 300
        Me.cboName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboName.FormattingEnabled = True
        Me.cboName.Location = New System.Drawing.Point(400, 60)
        Me.cboName.Name = "cboName"
        Me.cboName.Size = New System.Drawing.Size(146, 21)
        Me.cboName.TabIndex = 3
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 300
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(400, 34)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(146, 21)
        Me.cboStatus.TabIndex = 1
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(8, 62)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(107, 16)
        Me.lblAllocation.TabIndex = 17
        Me.lblAllocation.Text = "Requisition By"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocation
        '
        Me.cboAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation.DropDownWidth = 300
        Me.cboAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation.FormattingEnabled = True
        Me.cboAllocation.Location = New System.Drawing.Point(121, 60)
        Me.cboAllocation.Name = "cboAllocation"
        Me.cboAllocation.Size = New System.Drawing.Size(151, 21)
        Me.cboAllocation.TabIndex = 2
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(300, 36)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(94, 16)
        Me.lblStatus.TabIndex = 8
        Me.lblStatus.Text = "Requisition Type"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlEmployee
        '
        Me.pnlEmployee.Controls.Add(Me.objchkSelectAll)
        Me.pnlEmployee.Controls.Add(Me.dgvEmployeeList)
        Me.pnlEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlEmployee.Location = New System.Drawing.Point(121, 282)
        Me.pnlEmployee.Name = "pnlEmployee"
        Me.pnlEmployee.Size = New System.Drawing.Size(452, 163)
        Me.pnlEmployee.TabIndex = 81
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 6)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 77
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvEmployeeList
        '
        Me.dgvEmployeeList.AllowUserToAddRows = False
        Me.dgvEmployeeList.AllowUserToDeleteRows = False
        Me.dgvEmployeeList.BackgroundColor = System.Drawing.Color.White
        Me.dgvEmployeeList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEmployeeList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhCheckAll, Me.objcolhEmpID, Me.colhEmpCode, Me.colhEmpName, Me.colhJob, Me.colhGrade, Me.colhSalaryBand, Me.objcolhActionReasonId, Me.objcolhAUD})
        Me.dgvEmployeeList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEmployeeList.Location = New System.Drawing.Point(0, 0)
        Me.dgvEmployeeList.MultiSelect = False
        Me.dgvEmployeeList.Name = "dgvEmployeeList"
        Me.dgvEmployeeList.RowHeadersVisible = False
        Me.dgvEmployeeList.Size = New System.Drawing.Size(452, 163)
        Me.dgvEmployeeList.TabIndex = 0
        '
        'objcolhCheckAll
        '
        Me.objcolhCheckAll.HeaderText = ""
        Me.objcolhCheckAll.Name = "objcolhCheckAll"
        Me.objcolhCheckAll.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhCheckAll.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhCheckAll.Width = 30
        '
        'objcolhEmpID
        '
        Me.objcolhEmpID.HeaderText = ""
        Me.objcolhEmpID.Name = "objcolhEmpID"
        Me.objcolhEmpID.ReadOnly = True
        Me.objcolhEmpID.Visible = False
        Me.objcolhEmpID.Width = 5
        '
        'colhEmpCode
        '
        Me.colhEmpCode.HeaderText = "Code"
        Me.colhEmpCode.Name = "colhEmpCode"
        Me.colhEmpCode.ReadOnly = True
        Me.colhEmpCode.Width = 80
        '
        'colhEmpName
        '
        Me.colhEmpName.HeaderText = "Employee Name"
        Me.colhEmpName.Name = "colhEmpName"
        Me.colhEmpName.ReadOnly = True
        Me.colhEmpName.Width = 120
        '
        'colhJob
        '
        Me.colhJob.HeaderText = "Job"
        Me.colhJob.Name = "colhJob"
        Me.colhJob.ReadOnly = True
        '
        'colhGrade
        '
        Me.colhGrade.HeaderText = "Grade"
        Me.colhGrade.Name = "colhGrade"
        Me.colhGrade.ReadOnly = True
        Me.colhGrade.Width = 80
        '
        'colhSalaryBand
        '
        Me.colhSalaryBand.HeaderText = "Salary Band"
        Me.colhSalaryBand.Name = "colhSalaryBand"
        Me.colhSalaryBand.ReadOnly = True
        Me.colhSalaryBand.Width = 80
        '
        'objcolhActionReasonId
        '
        Me.objcolhActionReasonId.HeaderText = ""
        Me.objcolhActionReasonId.Name = "objcolhActionReasonId"
        Me.objcolhActionReasonId.ReadOnly = True
        Me.objcolhActionReasonId.Visible = False
        Me.objcolhActionReasonId.Width = 5
        '
        'objcolhAUD
        '
        Me.objcolhAUD.HeaderText = "AUD"
        Me.objcolhAUD.Name = "objcolhAUD"
        Me.objcolhAUD.ReadOnly = True
        Me.objcolhAUD.Visible = False
        Me.objcolhAUD.Width = 5
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 286)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(107, 28)
        Me.lblEmployee.TabIndex = 19
        Me.lblEmployee.Text = "Employee to be replaced"
        Me.lblEmployee.Visible = False
        '
        'ofdAttachment
        '
        Me.ofdAttachment.FileName = "OpenFileDialog1"
        '
        'lnkViewStaffRequisitionFormReport
        '
        Me.lnkViewStaffRequisitionFormReport.BackColor = System.Drawing.Color.Transparent
        Me.lnkViewStaffRequisitionFormReport.Location = New System.Drawing.Point(12, 22)
        Me.lnkViewStaffRequisitionFormReport.Name = "lnkViewStaffRequisitionFormReport"
        Me.lnkViewStaffRequisitionFormReport.Size = New System.Drawing.Size(230, 13)
        Me.lnkViewStaffRequisitionFormReport.TabIndex = 319
        Me.lnkViewStaffRequisitionFormReport.TabStop = True
        Me.lnkViewStaffRequisitionFormReport.Text = "View Staff Requisition Form Report"
        '
        'frmStaffRequisition_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(994, 507)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStaffRequisition_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Staff Requisition Add/Edit"
        Me.pnlMain.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.gbApproverMapping.ResumeLayout(False)
        Me.gbApproverMapping.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvStaffRequisitionAttachment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudContractDuration, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objtlpJob.ResumeLayout(False)
        CType(Me.nudPosition, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlEmployee.ResumeLayout(False)
        Me.pnlEmployee.PerformLayout()
        CType(Me.dgvEmployeeList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbApproverMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents cboName As System.Windows.Forms.ComboBox
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents cboClass As System.Windows.Forms.ComboBox
    Friend WithEvents lblClassGroup As System.Windows.Forms.Label
    Friend WithEvents cboClassGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblWorkStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpWorkStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblLeavingReason As System.Windows.Forms.Label
    Friend WithEvents cboLeavingReason As System.Windows.Forms.ComboBox
    Friend WithEvents txtAddStaffReason As System.Windows.Forms.TextBox
    Friend WithEvents lblAddStaffReason As System.Windows.Forms.Label
    Friend WithEvents objbtnLeavingReason As eZee.Common.eZeeGradientButton
    Friend WithEvents lblFormNo As System.Windows.Forms.Label
    Friend WithEvents txtFormNo As System.Windows.Forms.TextBox
    Friend WithEvents nudPosition As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblNoofPosition As System.Windows.Forms.Label
    Friend WithEvents objtlpJob As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents objlblVariation As System.Windows.Forms.Label
    Friend WithEvents objlblAvailable As System.Windows.Forms.Label
    Friend WithEvents lblAvailable As System.Windows.Forms.Label
    Friend WithEvents lblVariation As System.Windows.Forms.Label
    Friend WithEvents objlblPlanned As System.Windows.Forms.Label
    Friend WithEvents lblPlanned As System.Windows.Forms.Label
    Friend WithEvents lblJobDesc As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Private WithEvents lblSearchEmp As System.Windows.Forms.Label
    Private WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents pnlEmployee As System.Windows.Forms.Panel
    Friend WithEvents dgvEmployeeList As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnSearchClass As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchJobTitle As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchClassGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents txtJobDesc As System.Windows.Forms.RichTextBox
    Friend WithEvents lblGradeLevel As System.Windows.Forms.Label
    Friend WithEvents cboGradeLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmploymentType As System.Windows.Forms.Label
    Friend WithEvents cboEmploymentType As System.Windows.Forms.ComboBox
    Friend WithEvents nudContractDuration As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblContractDuration As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchGradeLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchGrade As eZee.Common.eZeeGradientButton
    Friend WithEvents lblGrade As System.Windows.Forms.Label
    Friend WithEvents cboGrade As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowInActiveEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents lnkViewJobDescription As System.Windows.Forms.LinkLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvStaffRequisitionAttachment As System.Windows.Forms.DataGridView
    Friend WithEvents cboDocumentType As System.Windows.Forms.ComboBox
    Friend WithEvents btnAddAttachment As eZee.Common.eZeeLightButton
    Friend WithEvents lblDocumentType As System.Windows.Forms.Label
    Friend WithEvents objcohDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSize As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDownload As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents objcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhScanUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sfdAttachment As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ofdAttachment As System.Windows.Forms.OpenFileDialog
    Friend WithEvents objbtnSearchJobReportingTo As eZee.Common.eZeeGradientButton
    Friend WithEvents cboJobReportTo As System.Windows.Forms.ComboBox
    Friend WithEvents lblJobReportTo As System.Windows.Forms.Label
    Friend WithEvents lblJobAdvert As System.Windows.Forms.Label
    Friend WithEvents cboJobAdvert As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowPreviousJobEmp As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhCheckAll As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objcolhEmpID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmpName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGrade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSalaryBand As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhActionReasonId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhAUD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkViewStaffRequisitionFormReport As System.Windows.Forms.LinkLabel
End Class
