﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmApproveDisapproveStaffRequisitionList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmApproveDisapproveStaffRequisitionList"
    Private objStaffReqApproval As clsStaffrequisition_approval_Tran
    Private mdicAllocaions As New Dictionary(Of Integer, String)
    Private mdicApprovalStatus As New Dictionary(Of Integer, String)

    Private mintCurrLevelPriority As Integer = -1
    Private mintLowerLevelPriority As Integer = -99 'Keep -99
    Private mintMinPriority As Integer = -1
    Private mintMaxPriority As Integer = -1
    Private mintLowerPriorityForFirstLevel As Integer = -1
    Private mintCurrLevelID As Integer = -1

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboAllocation.BackColor = GUI.ColorComp
            cboName.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Try

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
            'dsCombo = objMaster.GetEAllocation_Notification("List")
            dsCombo = objMaster.GetEAllocation_Notification("List", , , True)
            'Sohail (12 Oct 2018) -- End
            With cboAllocation
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedIndex = 0
            End With
            mdicAllocaions = (From p In dsCombo.Tables("List").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            dsCombo = objMaster.getApprovalStatus("ApprovalStatus", True, , True, True, True, True, True)
            mdicApprovalStatus = (From p In dsCombo.Tables("ApprovalStatus").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
            With cboStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("ApprovalStatus")
                .SelectedValue = CInt(enApprovalStatus.PENDING)
            End With
            'Sohail (12 Oct 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objApproverMap As New clsStaffRequisition_approver_mapping
        Dim objApproverLevel As New clsStaffRequisitionApproverlevel_master
        Dim dsList As DataSet
        Dim dsCombo As DataSet
        Dim intCurrPayUnkID As Integer = 0
        Dim intPrevPayUnkID As Integer = 0
        Dim intCurrPriority As Integer = 0
        Dim intPrevPriority As Integer = 0
        Dim intCurrUserID As Integer = 0
        Dim intPrevUserID As Integer = 0
        Dim lvItem As New ListViewItem
        Try

            lvStaffReqApprovalList.Items.Clear()

            If User._Object.Privilege._AllowToViewStaffRequisitionApprovals = False Then Exit Try

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
            'If CInt(cboAllocation.SelectedValue) <= 0 OrElse CInt(cboName.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select") & " " & lblName.Text)
            '    cboName.Focus()
            '    Exit Try
            'End If

            'dsCombo = objApproverMap.GetList("ApproverLevel", User._Object._Userunkid, CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue))
            'If dsCombo.Tables("ApproverLevel").Rows.Count > 0 Then
            '    mintCurrLevelPriority = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("priority"))
            '    mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority, CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue))
            '    mintMinPriority = objApproverLevel.GetMinPriority(CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue))
            '    mintMaxPriority = objApproverLevel.GetMaxPriority(CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue))
            '    mintLowerPriorityForFirstLevel = objApproverLevel.GetLowerLevelPriority(mintMinPriority, CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue))

            '    If mintCurrLevelPriority >= 0 Then
            '        mintCurrLevelID = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("levelunkid"))
            '    End If
            'Else
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You do not have Approval Permission for the selected") & " " & lblName.Text)
            '    cboName.Focus()
            '    Exit Try
            'End If

            'If mintLowerLevelPriority >= 0 Then
            '    'dsList = objStaffReqApproval.GetList("List", , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), User._Object._Userunkid, , , True, " (rcstaffrequisition_approval_tran.priority = " & mintLowerLevelPriority & "  OR rcstaffrequisition_approval_tran.statusunkid NOT IN ( " & enApprovalStatus.PENDING & " ) ) ")
            '    'Sohail (21 Aug 2015) -- Start
            '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '    'dsList = objStaffReqApproval.GetList("List", , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), User._Object._Userunkid, , , True, " rcstaffrequisition_approval_tran.priority <= " & mintCurrLevelPriority & " ")
            '    dsList = objStaffReqApproval.GetList("List", User._Object._Userunkid, , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), , , True, " rcstaffrequisition_approval_tran.priority <= " & mintCurrLevelPriority & " ")
            '    'Sohail (21 Aug 2015) -- End
            'Else
            '    'Sohail (21 Aug 2015) -- Start
            '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '    'dsList = objStaffReqApproval.GetList("List", , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), User._Object._Userunkid, , , True, " rcstaffrequisition_approval_tran.priority <= " & mintCurrLevelPriority & " ")
            '    dsList = objStaffReqApproval.GetList("List", User._Object._Userunkid, , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), , , True, " rcstaffrequisition_approval_tran.priority <= " & mintCurrLevelPriority & " ")
            '    'Sohail (21 Aug 2015) -- End
            'End If
            'Hemant (22 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            'dsList = objStaffReqApproval.GetList("List", User._Object._Userunkid, , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), , , True, "", chkMyApprovals.Checked, CInt(cboStatus.SelectedValue))
            'Sohail (15 Oct 2021) -- Start
            'NMB Enhancement :  : Show staff requisition only if they are pending at logged in user level if my approval is ticked on staff requisition approval screen.
            'dsList = objStaffReqApproval.GetList("List", User._Object._Userunkid, , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), , , True, "", False, CInt(cboStatus.SelectedValue), chkMyApprovals.Checked)


            'Pinkal (16-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            'dsList = objStaffReqApproval.GetList("List", User._Object._Userunkid, , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), , , True, "", chkMyApprovals.Checked, CInt(cboStatus.SelectedValue), chkMyApprovals.Checked)
            dsList = objStaffReqApproval.GetList("List", User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), , , True, "", chkMyApprovals.Checked, CInt(cboStatus.SelectedValue), chkMyApprovals.Checked)
            'Pinkal (16-Nov-2021) -- End


            'Sohail (15 Oct 2021) -- End
            'Hemant (22 Aug 2019) -- End
            'Sohail (12 Oct 2018) -- End

            For Each dtRow As DataRow In dsList.Tables("List").Rows

                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("formno").ToString
                lvItem.Tag = CInt(dtRow.Item("staffrequisitionapprovaltranunkid"))

                lvItem.SubItems.Add(mdicAllocaions.Item(CInt(dtRow.Item("staffrequisitionbyid"))))
                lvItem.SubItems(colhStaffReqBy.Index).Tag = CInt(dtRow.Item("staffrequisitionbyid"))

                Select Case CInt(dtRow.Item("staffrequisitionbyid"))

                    Case enAllocation.BRANCH
                        lvItem.SubItems.Add(dtRow.Item("Branch").ToString)

                    Case enAllocation.DEPARTMENT_GROUP
                        lvItem.SubItems.Add(dtRow.Item("DepartmentGroup").ToString)

                    Case enAllocation.DEPARTMENT
                        lvItem.SubItems.Add(dtRow.Item("Department").ToString)

                    Case enAllocation.SECTION_GROUP
                        lvItem.SubItems.Add(dtRow.Item("SectionGroup").ToString)

                    Case enAllocation.SECTION
                        lvItem.SubItems.Add(dtRow.Item("Section").ToString)

                    Case enAllocation.UNIT_GROUP
                        lvItem.SubItems.Add(dtRow.Item("UnitGroup").ToString)

                    Case enAllocation.UNIT
                        lvItem.SubItems.Add(dtRow.Item("Unit").ToString)

                    Case enAllocation.TEAM
                        lvItem.SubItems.Add(dtRow.Item("Team").ToString)

                    Case enAllocation.JOB_GROUP
                        lvItem.SubItems.Add(dtRow.Item("JobGroup").ToString)

                    Case enAllocation.JOBS
                        lvItem.SubItems.Add(dtRow.Item("Job").ToString)

                    Case enAllocation.CLASS_GROUP
                        lvItem.SubItems.Add(dtRow.Item("ClassGroup").ToString)

                    Case enAllocation.CLASSES
                        lvItem.SubItems.Add(dtRow.Item("Class").ToString)

                End Select
                lvItem.SubItems(colhName.Index).Tag = CInt(dtRow.Item("allocationunkid"))

                lvItem.SubItems.Add(dtRow.Item("JobTitle").ToString)
                lvItem.SubItems(colhJobTitle.Index).Tag = CInt(dtRow.Item("jobunkid"))

                lvItem.SubItems.Add(dtRow.Item("levelname").ToString)
                lvItem.SubItems(colhLevel.Index).Tag = CInt(dtRow.Item("levelunkid"))

                lvItem.SubItems.Add(dtRow.Item("username").ToString)
                lvItem.SubItems(colhApprover.Index).Tag = CInt(dtRow.Item("userunkid"))

                lvItem.SubItems.Add(dtRow.Item("priority").ToString)

                If dtRow.Item("approval_date").ToString = "19000101" Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("approval_date").ToString).ToShortDateString)
                End If

                If CInt(dtRow.Item("form_statusunkid")) = enApprovalStatus.PUBLISHED Then
                    lvItem.SubItems.Add(mdicApprovalStatus.Item(CInt(dtRow.Item("form_statusunkid"))).ToString)
                ElseIf CInt(dtRow.Item("form_statusunkid")) = enApprovalStatus.APPROVED Then
                    lvItem.SubItems.Add(Language.getMessage(mstrModuleName, 3, "Final") & " " & mdicApprovalStatus.Item(CInt(dtRow.Item("statusunkid"))).ToString)
                Else
                    lvItem.SubItems.Add(mdicApprovalStatus.Item(CInt(dtRow.Item("statusunkid"))).ToString)
                End If
                lvItem.SubItems(colhStatus.Index).Tag = CInt(dtRow.Item("statusunkid"))

                lvItem.SubItems.Add(dtRow.Item("remarks").ToString)

                lvItem.SubItems.Add(dtRow.Item("form_statusunkid").ToString)
                lvItem.SubItems(objcolhFormStatus.Index).Tag = CInt(dtRow.Item("form_statusunkid"))

                lvItem.SubItems.Add(dtRow.Item("staffrequisitiontranunkid").ToString)

                lvStaffReqApprovalList.Items.Add(lvItem)

            Next

            lvStaffReqApprovalList.GroupingColumn = colhFormNo
            lvStaffReqApprovalList.DisplayGroups(True)
            lvStaffReqApprovalList.GridLines = False

            If lvStaffReqApprovalList.Items.Count > 4 Then
                colhApprover.Width = 120 - 18
            Else
                colhApprover.Width = 120
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            Call objbtnSearch.ShowResult(CStr(lvStaffReqApprovalList.Items.Count))
            objApproverMap = Nothing
            objApproverLevel = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnChangeStatus.Enabled = User._Object.Privilege._AllowToApproveStaffRequisition

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Function IsValid() As Boolean
        Dim objApproverMap As New clsStaffRequisition_approver_mapping
        Dim objApproverLevel As New clsStaffRequisitionApproverlevel_master
        Dim objApproval As New clsStaffrequisition_approval_Tran
        Dim mintLowerLevelPriority As Integer = -1
        Dim dsCombo As DataSet
        Try
            If lvStaffReqApprovalList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Staff Requisition Form Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvStaffReqApprovalList.Select()
                Exit Function
            ElseIf CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(objcolhFormStatus.Index).Tag) = enApprovalStatus.REJECTED OrElse CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.REJECTED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Rejected."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(objcolhFormStatus.Index).Tag) = enApprovalStatus.CANCELLED OrElse CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.CANCELLED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Cancelled."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(objcolhFormStatus.Index).Tag) = enApprovalStatus.PUBLISHED OrElse CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.PUBLISHED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Published."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(objcolhFormStatus.Index).Tag) = enApprovalStatus.APPROVED AndAlso User._Object.Privilege._AllowToCancelStaffRequisition = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "You can't Cancel this Staff Requisition detail. Reason: This Staff Requisition is already Final Approved and you don't have permission to Cancel Staff Requisition."), enMsgBoxStyle.Information)
                Exit Function
                'ElseIf CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.APPROVED AndAlso CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(objcolhFormStatus.Index).Tag) = enApprovalStatus.PENDING Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Approved."), enMsgBoxStyle.Information)
                '    Exit Function
            ElseIf CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(objcolhFormStatus.Index).Tag) = enApprovalStatus.PENDING AndAlso CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.APPROVED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Approved."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf User._Object._Userunkid <> CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhApprover.Index).Tag) AndAlso CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.PENDING Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "You can't Edit this Staff Requisition detail. Reason: You are logged in as another user."), enMsgBoxStyle.Information)
                Exit Function
            ElseIf CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhStatus.Index).Tag) = enApprovalStatus.PENDING AndAlso User._Object.Privilege._AllowToApproveStaffRequisition = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "You can't Edit this Staff Requisition detail. Reason: You don't have permission to Approve Staff Requisition."), enMsgBoxStyle.Information)
                Exit Function
            End If

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
            'dsCombo = objApproverMap.GetList("CurrPriority", User._Object._Userunkid, CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhStaffReqBy.Index).Tag), CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhName.Index).Tag))
            'If dsCombo.Tables("CurrPriority").Rows.Count > 0 Then
            '    mintCurrLevelPriority = CInt(dsCombo.Tables("CurrPriority").Rows(0).Item("priority"))
            'End If
            'If mintCurrLevelPriority > CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhPriority.Index).Text) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "You can't Edit this Staff Requisition detail. Reason: You are logged in into another user login."), enMsgBoxStyle.Information)
            '    Exit Function
            'End If
            'mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority, CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhStaffReqBy.Index).Tag), CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhName.Index).Tag))
            'If mintLowerLevelPriority >= 0 Then
            '    'Sohail (21 Aug 2015) -- Start
            '    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            '    'dsCombo = objApproval.GetList("List", , lvStaffReqApprovalList.SelectedItems(0).SubItems(objcolhStaffReqTranUnkId.Index).Text, , , , , mintLowerLevelPriority)
            '    dsCombo = objApproval.GetList("List", User._Object._Userunkid, , lvStaffReqApprovalList.SelectedItems(0).SubItems(objcolhStaffReqTranUnkId.Index).Text, , , , mintLowerLevelPriority)
            '    'Sohail (21 Aug 2015) -- End

            '    If dsCombo.Tables("List").Rows.Count > 0 Then
            '        If CInt(dsCombo.Tables("List").Rows(0).Item("statusunkid")) = 1 Then
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition approval is still pending for lower level."), enMsgBoxStyle.Information)
            '            Exit Function
            '        End If
            '    End If
            'End If

            'Pinkal (16-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            'dsCombo = objApproval.GetList("List", User._Object._Userunkid, , lvStaffReqApprovalList.SelectedItems(0).SubItems(objcolhStaffReqTranUnkId.Index).Text, , , , mintLowerLevelPriority, True, "", True, CInt(enApprovalStatus.PENDING))
            dsCombo = objApproval.GetList("List", User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate, , lvStaffReqApprovalList.SelectedItems(0).SubItems(objcolhStaffReqTranUnkId.Index).Text, , , , mintLowerLevelPriority, True, "", True, CInt(enApprovalStatus.PENDING))
            'Pinkal (16-Nov-2021)-- End

            If dsCombo.Tables("List").Rows.Count <= 0 Then
                If Not (CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(objcolhFormStatus.Index).Tag) = enApprovalStatus.APPROVED AndAlso User._Object.Privilege._AllowToCancelStaffRequisition = True) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition approval is still pending for lower level."), enMsgBoxStyle.Information)
                    Exit Function
                End If
            End If
            'Sohail (12 Oct 2018) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
            objApproverMap = Nothing
            objApproverLevel = Nothing
            objApproval = Nothing
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmApproveDisapproveStaffRequisitionList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objStaffReqApproval = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveStaffRequisitionList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapproveStaffRequisitionList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                Case Keys.Escape
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveStaffRequisitionList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApproveDisapproveStaffRequisitionList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objStaffReqApproval = New clsStaffrequisition_approval_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            'Hemant (06 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            chkMyApprovals.Checked = True
            'Hemant (06 Aug 2019) -- End
            lvStaffReqApprovalList.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApproveDisapproveStaffRequisitionList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsStaffrequisition_approval_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsStaffrequisition_approval_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Combobox's Events "
    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Dim dsCombos As DataSet = Nothing
        Dim strName As String = ""

        Try

            Select Case CInt(cboAllocation.SelectedValue)

                Case enAllocation.BRANCH
                    dsCombos = objStation.getComboList("Station", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 14, "Branch")
                Case enAllocation.DEPARTMENT_GROUP
                    dsCombos = objDeptGrp.getComboList("DeptGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 15, "Department Group")
                Case enAllocation.DEPARTMENT
                    dsCombos = objDepartment.getComboList("Department", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 16, "Department")
                Case enAllocation.SECTION_GROUP
                    dsCombos = objSectionGrp.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 17, "Section Group")
                Case enAllocation.SECTION
                    dsCombos = objSection.getComboList("Section", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 18, "Section")
                Case enAllocation.UNIT_GROUP
                    dsCombos = objUnitGroup.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 19, "Unit Group")
                Case enAllocation.UNIT
                    dsCombos = objUnit.getComboList("Unit", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 20, "Unit")
                Case enAllocation.TEAM
                    dsCombos = objTeam.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 21, "Team")
                Case enAllocation.JOB_GROUP
                    dsCombos = objJobGrp.getComboList("JobGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 22, "Job Group")
                Case enAllocation.JOBS
                    dsCombos = objJob.getComboList("Job", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 23, "Jobs")
                Case enAllocation.CLASS_GROUP
                    dsCombos = objClassGrp.getComboList("ClassGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 24, "Class Group")
                Case enAllocation.CLASSES
                    dsCombos = objClass.getComboList("Class", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 25, "Classes")

                    'Sohail (12 Oct 2018) -- Start
                    'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
                Case Else
                    dsCombos = Nothing
                    'Sohail (12 Oct 2018) -- End

            End Select

            lblName.Text = strName
            If dsCombos IsNot Nothing Then

                With cboName
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables(0)
                    .SelectedValue = 0
                End With
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
            Else
                cboName.DataSource = Nothing
                cboName.Items.Clear()
                'Sohail (12 Oct 2018) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocation_SelectedIndexChanged", mstrModuleName)
        Finally
            objStation = Nothing
            objDeptGrp = Nothing
            objDepartment = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objClassGrp = Nothing
            objClass = Nothing
            objSectionGrp = Nothing
            objUnitGroup = Nothing
            objTeam = Nothing
            dsCombos = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnChangeStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
        Try
            If IsValid() = False Then Exit Try

            Dim objFrm As New frmApproveDisapproveStaffRequisition
            If objFrm.DisplayDialog(enAction.ADD_ONE, CInt(lvStaffReqApprovalList.SelectedItems(0).Tag), CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(objcolhStaffReqTranUnkId.Index).Text), CInt(lvStaffReqApprovalList.SelectedItems(0).SubItems(colhStatus.Index).Tag)) = True Then
                Call FillList()
            End If
            objFrm = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnChangeStatus_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Control's Events "
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboAllocation.SelectedIndex = 0
            cboStatus.SelectedValue = CInt(enApprovalStatus.PENDING) 'Sohail (12 Oct 2018)
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnChangeStatus.GradientBackColor = GUI._ButttonBackColor 
			Me.btnChangeStatus.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.colhLevel.Text = Language._Object.getCaption(CStr(Me.colhLevel.Tag), Me.colhLevel.Text)
			Me.colhApprover.Text = Language._Object.getCaption(CStr(Me.colhApprover.Tag), Me.colhApprover.Text)
			Me.colhPriority.Text = Language._Object.getCaption(CStr(Me.colhPriority.Tag), Me.colhPriority.Text)
			Me.colhApprovalDate.Text = Language._Object.getCaption(CStr(Me.colhApprovalDate.Tag), Me.colhApprovalDate.Text)
			Me.colhRemarks.Text = Language._Object.getCaption(CStr(Me.colhRemarks.Tag), Me.colhRemarks.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnChangeStatus.Text = Language._Object.getCaption(Me.btnChangeStatus.Name, Me.btnChangeStatus.Text)
			Me.colhFormNo.Text = Language._Object.getCaption(CStr(Me.colhFormNo.Tag), Me.colhFormNo.Text)
			Me.colhStaffReqBy.Text = Language._Object.getCaption(CStr(Me.colhStaffReqBy.Tag), Me.colhStaffReqBy.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.colhJobTitle.Text = Language._Object.getCaption(CStr(Me.colhJobTitle.Tag), Me.colhJobTitle.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.chkMyApprovals.Text = Language._Object.getCaption(Me.chkMyApprovals.Name, Me.chkMyApprovals.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 3, "Final")
			Language.setMessage(mstrModuleName, 4, "Please select Staff Requisition Form Approver from the list to perform further operation.")
			Language.setMessage(mstrModuleName, 5, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Rejected.")
			Language.setMessage(mstrModuleName, 6, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Cancelled.")
			Language.setMessage(mstrModuleName, 7, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Published.")
			Language.setMessage(mstrModuleName, 8, "You can't Cancel this Staff Requisition detail. Reason: This Staff Requisition is already Final Approved and you don't have permission to Cancel Staff Requisition.")
			Language.setMessage(mstrModuleName, 9, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Approved.")
			Language.setMessage(mstrModuleName, 10, "You can't Edit this Staff Requisition detail. Reason: You are logged in as another user.")
			Language.setMessage(mstrModuleName, 11, "You can't Edit this Staff Requisition detail. Reason: You don't have permission to Approve Staff Requisition.")
			Language.setMessage(mstrModuleName, 13, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition approval is still pending for lower level.")
			Language.setMessage(mstrModuleName, 14, "Branch")
			Language.setMessage(mstrModuleName, 15, "Department Group")
			Language.setMessage(mstrModuleName, 16, "Department")
			Language.setMessage(mstrModuleName, 17, "Section Group")
			Language.setMessage(mstrModuleName, 18, "Section")
			Language.setMessage(mstrModuleName, 19, "Unit Group")
			Language.setMessage(mstrModuleName, 20, "Unit")
			Language.setMessage(mstrModuleName, 21, "Team")
			Language.setMessage(mstrModuleName, 22, "Job Group")
			Language.setMessage(mstrModuleName, 23, "Jobs")
			Language.setMessage(mstrModuleName, 24, "Class Group")
			Language.setMessage(mstrModuleName, 25, "Classes")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class