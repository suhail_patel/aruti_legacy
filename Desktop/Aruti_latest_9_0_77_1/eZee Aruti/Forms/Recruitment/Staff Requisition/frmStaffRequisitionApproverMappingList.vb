﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmStaffRequisitionApproverMappingList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmStaffRequisitionApproverMappingList"
    Private objStaffRequisitionApprover As clsStaffRequisition_approver_mapping
    Private mdicAllocaions As New Dictionary(Of Integer, String)

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboApproverUser.BackColor = GUI.ColorOptional
            cboLevel.BackColor = GUI.ColorOptional
            cboAllocation.BackColor = GUI.ColorOptional
            cboName.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub


    Private Sub Fill_Combo()
        Dim objUsr As New clsUserAddEdit
        Dim objLevel As New clsStaffRequisitionApproverlevel_master
        Dim objOption As New clsPassowdOptions
        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objLevel.getListForCombo("List", True)
            With cboLevel
                .ValueMember = "levelunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            Dim intPrivilegeId As Integer = 790
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'Select Case objOption._UserLogingModeId
            '    Case enAuthenticationMode.BASIC_AUTHENTICATION
            '        If objOption._IsEmployeeAsUser Then
            '            dsList = objUsr.getComboList("List", True, False, True, Company._Object._Companyunkid, intPrivilegeId, FinancialYear._Object._YearUnkid)
            '        Else
            '            dsList = objUsr.getComboList("List", True, False, , Company._Object._Companyunkid, intPrivilegeId)
            '        End If
            '    Case enAuthenticationMode.AD_BASIC_AUTHENTICATION, enAuthenticationMode.AD_SSO_AUTHENTICATION
            '        dsList = objUsr.getComboList("List", True, True, , Company._Object._Companyunkid, intPrivilegeId)
            'End Select

            'Nilay (01-Mar-2016) -- Start
            'dsList = objUsr.getNewComboList("List", , True, Company._Object._Companyunkid, intPrivilegeId)
            dsList = objUsr.getNewComboList("List", , True, Company._Object._Companyunkid, intPrivilegeId.ToString(), FinancialYear._Object._YearUnkid, False)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            With cboApproverUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
            End With

            dsList = objMaster.GetEAllocation_Notification("List")
            With cboAllocation
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedIndex = 0
            End With
            mdicAllocaions = (From p In dsList.Tables("List").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
            dsList.Dispose() : objUsr = Nothing : objLevel = Nothing : objOption = Nothing
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim StrSearch As String = String.Empty
        Dim lvItem As ListViewItem
        Try

            lvMappingList.Items.Clear()

            If User._Object.Privilege._AllowToViewStaffRequisitionApproverMappingList = False Then Exit Try

            If CInt(cboLevel.SelectedValue) > 0 Then
                StrSearch &= "AND rcstaffrequisition_approver_mapping.levelunkid = " & CInt(cboLevel.SelectedValue) & " "
            End If

            If CInt(cboApproverUser.SelectedValue) > 0 Then
                StrSearch &= "AND rcstaffrequisition_approver_mapping.userapproverunkid = " & CInt(cboApproverUser.SelectedValue) & " "
            End If

            If CInt(cboAllocation.SelectedValue) > 0 Then
                StrSearch &= "AND rcstaffrequisition_approver_mapping.allocationid = " & CInt(cboAllocation.SelectedValue) & " "
            End If

            If CInt(cboName.SelectedValue) > 0 Then
                StrSearch &= "AND rcstaffrequisition_approver_mapping.allocationunkid = " & CInt(cboName.SelectedValue) & " "
            End If

            If StrSearch.Trim.Length > 0 Then StrSearch = StrSearch.Substring(3)

            dsList = objStaffRequisitionApprover.GetList("List", , , , StrSearch, "rcstaffrequisitionlevel_master.levelname")



            For Each dtRow As DataRow In dsList.Tables("List").Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("approver_level").ToString
                lvItem.Tag = dtRow.Item("StaffRequisitionapproverunkid")

                lvItem.SubItems.Add(dtRow.Item("approver").ToString)
                lvItem.SubItems(colhStaffRequisitionApprover.Index).Tag = CInt(dtRow.Item("userapproverunkid"))


                lvItem.SubItems.Add(mdicAllocaions.Item(CInt(dtRow.Item("allocationid"))))
                lvItem.SubItems(colhAllocation.Index).Tag = CInt(dtRow.Item("allocationid"))

                Select Case CInt(dtRow.Item("allocationid"))

                    Case enAllocation.BRANCH
                        lvItem.SubItems.Add(dtRow.Item("Branch").ToString)

                    Case enAllocation.DEPARTMENT_GROUP
                        lvItem.SubItems.Add(dtRow.Item("DepartmentGroup").ToString)

                    Case enAllocation.DEPARTMENT
                        lvItem.SubItems.Add(dtRow.Item("Department").ToString)

                    Case enAllocation.SECTION_GROUP
                        lvItem.SubItems.Add(dtRow.Item("SectionGroup").ToString)

                    Case enAllocation.SECTION
                        lvItem.SubItems.Add(dtRow.Item("Section").ToString)

                    Case enAllocation.UNIT_GROUP
                        lvItem.SubItems.Add(dtRow.Item("UnitGroup").ToString)

                    Case enAllocation.UNIT
                        lvItem.SubItems.Add(dtRow.Item("Unit").ToString)

                    Case enAllocation.TEAM
                        lvItem.SubItems.Add(dtRow.Item("Team").ToString)

                    Case enAllocation.JOB_GROUP
                        lvItem.SubItems.Add(dtRow.Item("JobGroup").ToString)

                    Case enAllocation.JOBS
                        lvItem.SubItems.Add(dtRow.Item("Job").ToString)

                    Case enAllocation.CLASS_GROUP
                        lvItem.SubItems.Add(dtRow.Item("ClassGroup").ToString)

                    Case enAllocation.CLASSES
                        lvItem.SubItems.Add(dtRow.Item("Class").ToString)

                End Select
                lvItem.SubItems(colhName.Index).Tag = CInt(dtRow.Item("allocationunkid"))


                lvMappingList.Items.Add(lvItem)
            Next

            lvMappingList.GridLines = False
            lvMappingList.GroupingColumn = objcolhLevel
            lvMappingList.DisplayGroups(True)

            If lvMappingList.Items.Count > 3 Then
                colhStaffRequisitionApprover.Width = 200 - 18
            Else
                colhStaffRequisitionApprover.Width = 200
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddStaffRequisitionApproverMapping
            btnEdit.Enabled = User._Object.Privilege._AllowToEditStaffRequisitionApproverMapping
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteStaffRequisitionApproverMapping
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmStaffRequisitionApproverMappingList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objStaffRequisitionApprover = New clsStaffRequisition_approver_mapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Call SetColor()

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()

            Call Fill_Combo()

            If lvMappingList.Items.Count > 0 Then lvMappingList.Items(0).Selected = True
            lvMappingList.Select()
            lvMappingList.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffRequisitionApproverMappingList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStaffRequisitionApproverMappingList_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmStaffRequisitionApproverMappingList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStaffRequisitionApproverMappingList_Closed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Close()
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsStaffRequisition_approver_mapping.SetMessages()
            objfrm._Other_ModuleNames = "clsStaffRequisition_approver_mapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim frm As New frmStaffRequisitionApproverMapping_AddEdit
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvMappingList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvMappingList.Select()
            Exit Sub
        End If
        Dim frm As New frmStaffRequisitionApproverMapping_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvMappingList.SelectedItems(0).Index
            If frm.displayDialog(CInt(lvMappingList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If
            frm = Nothing
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvMappingList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation."), enMsgBoxStyle.Information)
            lvMappingList.Select()
            Exit Sub
        End If

        Dim objMaster As New clsMasterData
        Try
            If objStaffRequisitionApprover.isUsed(CInt(lvMappingList.SelectedItems(0).SubItems(colhStaffRequisitionApprover.Index).Tag), CInt(lvMappingList.SelectedItems(0).SubItems(colhAllocation.Index).Tag), CInt(lvMappingList.SelectedItems(0).SubItems(colhName.Index).Tag)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver. Reason: This Approver is in use."), enMsgBoxStyle.Information)
                lvMappingList.Select()
                Exit Try
            End If

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvMappingList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.STAFF_REQUISITION, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                End If
                objStaffRequisitionApprover._Isvoid = True
                objStaffRequisitionApprover._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objStaffRequisitionApprover._Voiduserunkid = User._Object._Userunkid
                objStaffRequisitionApprover._Voidreason = mstrVoidReason


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objStaffRequisitionApprover._FormName = mstrModuleName
                objStaffRequisitionApprover._LoginEmployeeunkid = 0
                objStaffRequisitionApprover._ClientIP = getIP()
                objStaffRequisitionApprover._HostName = getHostName()
                objStaffRequisitionApprover._FromWeb = False
                objStaffRequisitionApprover._AuditUserId = User._Object._Userunkid
objStaffRequisitionApprover._CompanyUnkid = Company._Object._Companyunkid
                objStaffRequisitionApprover._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'objStaffRequisitionApprover.Delete(CInt(lvMappingList.SelectedItems(0).Tag))
                
'Pinkal (02-Nov-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                'objStaffRequisitionApprover.Delete(CInt(lvMappingList.SelectedItems(0).Tag))

                objStaffRequisitionApprover._WebClientIP = getIP()
                objStaffRequisitionApprover._WebHostName = getHostName()
                objStaffRequisitionApprover._WebFormName = mstrModuleName
                objStaffRequisitionApprover._IsFromWeb = False

                objStaffRequisitionApprover.Delete(CInt(lvMappingList.SelectedItems(0).Tag), CInt(lvMappingList.SelectedItems(0).SubItems(colhAllocation.Index).Tag))

                'Pinkal (02-Nov-2021) -- End

                lvMappingList.SelectedItems(0).Remove()

                If lvMappingList.Items.Count <= 0 Then
                    Exit Try
                End If
            End If
            lvMappingList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            objMaster = Nothing
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            lvMappingList.Items.Clear()
            cboApproverUser.SelectedValue = 0
            cboLevel.SelectedValue = 0
            lvMappingList.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If


            With objFrm
                .ValueMember = cboApproverUser.ValueMember
                .DisplayMember = cboApproverUser.DisplayMember
                .CodeMember = "code"
                .DataSource = CType(cboApproverUser.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboApproverUser.SelectedValue = objFrm.SelectedValue
            End If
            cboApproverUser.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchLevel.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If


            With objFrm
                .ValueMember = cboLevel.ValueMember
                .CodeMember = cboLevel.DisplayMember
                .DisplayMember = "Display"
                .DataSource = CType(cboApproverUser.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboApproverUser.SelectedValue = objFrm.SelectedValue
            End If
            cboApproverUser.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchLevel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Dim dsCombos As DataSet = Nothing
        Dim strName As String = ""

        Try

            Select Case CInt(cboAllocation.SelectedValue)

                Case enAllocation.BRANCH
                    dsCombos = objStation.getComboList("Station", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 4, "Branch")
                Case enAllocation.DEPARTMENT_GROUP
                    dsCombos = objDeptGrp.getComboList("DeptGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 5, "Department Group")
                Case enAllocation.DEPARTMENT
                    dsCombos = objDepartment.getComboList("Department", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 6, "Department")
                Case enAllocation.SECTION_GROUP
                    dsCombos = objSectionGrp.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 7, "Section Group")
                Case enAllocation.SECTION
                    dsCombos = objSection.getComboList("Section", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 8, "Section")
                Case enAllocation.UNIT_GROUP
                    dsCombos = objUnitGroup.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 9, "Unit Group")
                Case enAllocation.UNIT
                    dsCombos = objUnit.getComboList("Unit", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 10, "Unit")
                Case enAllocation.TEAM
                    dsCombos = objTeam.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 11, "Team")
                Case enAllocation.JOB_GROUP
                    dsCombos = objJobGrp.getComboList("JobGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 12, "Job Group")
                Case enAllocation.JOBS
                    dsCombos = objJob.getComboList("Job", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 13, "Jobs")
                Case enAllocation.CLASS_GROUP
                    dsCombos = objClassGrp.getComboList("ClassGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 14, "Class Group")
                Case enAllocation.CLASSES
                    dsCombos = objClass.getComboList("Class", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 15, "Classes")

            End Select

            lblName.Text = strName
            If dsCombos IsNot Nothing Then

                With cboName
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables(0)
                    .SelectedValue = 0
                End With
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocation_SelectedIndexChanged", mstrModuleName)
        Finally
            objStation = Nothing
            objDeptGrp = Nothing
            objDepartment = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objClassGrp = Nothing
            objClass = Nothing
            objSectionGrp = Nothing
            objUnitGroup = Nothing
            objTeam = Nothing
            dsCombos = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.colhStaffRequisitionApprover.Text = Language._Object.getCaption(CStr(Me.colhStaffRequisitionApprover.Tag), Me.colhStaffRequisitionApprover.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblApprUser.Text = Language._Object.getCaption(Me.lblApprUser.Name, Me.lblApprUser.Text)
            Me.lblApprLevel.Text = Language._Object.getCaption(Me.lblApprLevel.Name, Me.lblApprLevel.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
            Me.colhAllocation.Text = Language._Object.getCaption(CStr(Me.colhAllocation.Tag), Me.colhAllocation.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Approver from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver. Reason: This Approver is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Approver?")
            Language.setMessage(mstrModuleName, 4, "Branch")
            Language.setMessage(mstrModuleName, 5, "Department Group")
            Language.setMessage(mstrModuleName, 6, "Department")
            Language.setMessage(mstrModuleName, 7, "Section Group")
            Language.setMessage(mstrModuleName, 8, "Section")
            Language.setMessage(mstrModuleName, 9, "Unit Group")
            Language.setMessage(mstrModuleName, 10, "Unit")
            Language.setMessage(mstrModuleName, 11, "Team")
            Language.setMessage(mstrModuleName, 12, "Job Group")
            Language.setMessage(mstrModuleName, 13, "Jobs")
            Language.setMessage(mstrModuleName, 14, "Class Group")
            Language.setMessage(mstrModuleName, 15, "Classes")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class