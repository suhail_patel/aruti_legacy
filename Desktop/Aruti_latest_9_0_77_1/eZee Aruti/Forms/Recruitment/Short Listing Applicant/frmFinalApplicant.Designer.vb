﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFinalApplicant
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFinalApplicant))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.txtSearchFinalShortListed = New eZee.TextBox.AlphanumericTextBox
        Me.txtSearchShortListed = New eZee.TextBox.AlphanumericTextBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.LblStatus = New System.Windows.Forms.Label
        Me.objchkFinalSelectAll = New System.Windows.Forms.CheckBox
        Me.btndown = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnUp = New eZee.Common.eZeeLightButton(Me.components)
        Me.objCheckAll = New System.Windows.Forms.CheckBox
        Me.lnkAddtoExistingList = New System.Windows.Forms.LinkLabel
        Me.dgFinalApplicant = New System.Windows.Forms.DataGridView
        Me.objcolhFinalSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgShortListApplicant = New System.Windows.Forms.DataGridView
        Me.objcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.lblFinalApplicant = New System.Windows.Forms.Label
        Me.lblShortListApplicant = New System.Windows.Forms.Label
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboVacancy = New System.Windows.Forms.ComboBox
        Me.objbtnSearchVacancyType = New eZee.Common.eZeeGradientButton
        Me.cboVacancyType = New System.Windows.Forms.ComboBox
        Me.objbtnSearchVacancy = New eZee.Common.eZeeGradientButton
        Me.txtClosingDate = New eZee.TextBox.AlphanumericTextBox
        Me.LblVacancyType = New System.Windows.Forms.Label
        Me.txtOpeningDate = New eZee.TextBox.AlphanumericTextBox
        Me.cboRefNo = New System.Windows.Forms.ComboBox
        Me.objbtnSearchRefNo = New eZee.Common.eZeeGradientButton
        Me.lblRefNo = New System.Windows.Forms.Label
        Me.lblEnddate = New System.Windows.Forms.Label
        Me.lblVacancy = New System.Windows.Forms.Label
        Me.lblStartdate = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDisapprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.rbtnSplitButton = New eZee.Common.eZeeSplitButton
        Me.mnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuViewApplicantCV = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuViewShortListedApplicantCV = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuViewFinalShortListedApplicantCV = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuViewShortListedApplicant = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFinalShortlistedApplicants = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuViewPendingApplicants = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuViewApprovedApplicants = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuViewDisapprovedApplicants = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSubmitForApproval = New System.Windows.Forms.ToolStripMenuItem
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.txtRemarks = New System.Windows.Forms.RichTextBox
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFinalApplicantName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFinalGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFinalBirthdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFinalPhone = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFinalEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhdgStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhdgFinalApplicantID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhFinalApprovalSubmit = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhApplicant = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhGender = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhBirthdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPhone = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmail = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objColhShortListId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objColhShortListApplicantId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        CType(Me.dgFinalApplicant, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgShortListApplicant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.mnuOperation.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.txtSearchFinalShortListed)
        Me.pnlMain.Controls.Add(Me.txtSearchShortListed)
        Me.pnlMain.Controls.Add(Me.cboStatus)
        Me.pnlMain.Controls.Add(Me.LblStatus)
        Me.pnlMain.Controls.Add(Me.objchkFinalSelectAll)
        Me.pnlMain.Controls.Add(Me.btndown)
        Me.pnlMain.Controls.Add(Me.btnUp)
        Me.pnlMain.Controls.Add(Me.objCheckAll)
        Me.pnlMain.Controls.Add(Me.lnkAddtoExistingList)
        Me.pnlMain.Controls.Add(Me.dgFinalApplicant)
        Me.pnlMain.Controls.Add(Me.dgShortListApplicant)
        Me.pnlMain.Controls.Add(Me.lblFinalApplicant)
        Me.pnlMain.Controls.Add(Me.lblShortListApplicant)
        Me.pnlMain.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(775, 586)
        Me.pnlMain.TabIndex = 0
        '
        'txtSearchFinalShortListed
        '
        Me.txtSearchFinalShortListed.Flags = 0
        Me.txtSearchFinalShortListed.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchFinalShortListed.Location = New System.Drawing.Point(10, 331)
        Me.txtSearchFinalShortListed.Name = "txtSearchFinalShortListed"
        Me.txtSearchFinalShortListed.Size = New System.Drawing.Size(755, 21)
        Me.txtSearchFinalShortListed.TabIndex = 467
        '
        'txtSearchShortListed
        '
        Me.txtSearchShortListed.Flags = 0
        Me.txtSearchShortListed.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchShortListed.Location = New System.Drawing.Point(157, 106)
        Me.txtSearchShortListed.Name = "txtSearchShortListed"
        Me.txtSearchShortListed.Size = New System.Drawing.Size(608, 21)
        Me.txtSearchShortListed.TabIndex = 466
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 175
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(168, 301)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(149, 21)
        Me.cboStatus.TabIndex = 465
        '
        'LblStatus
        '
        Me.LblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblStatus.Location = New System.Drawing.Point(117, 303)
        Me.LblStatus.Name = "LblStatus"
        Me.LblStatus.Size = New System.Drawing.Size(44, 17)
        Me.LblStatus.TabIndex = 465
        Me.LblStatus.Text = "Status"
        Me.LblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objchkFinalSelectAll
        '
        Me.objchkFinalSelectAll.AutoSize = True
        Me.objchkFinalSelectAll.Location = New System.Drawing.Point(17, 359)
        Me.objchkFinalSelectAll.Name = "objchkFinalSelectAll"
        Me.objchkFinalSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkFinalSelectAll.TabIndex = 457
        Me.objchkFinalSelectAll.UseVisualStyleBackColor = True
        '
        'btndown
        '
        Me.btndown.BackColor = System.Drawing.Color.White
        Me.btndown.BackgroundImage = CType(resources.GetObject("btndown.BackgroundImage"), System.Drawing.Image)
        Me.btndown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btndown.BorderColor = System.Drawing.Color.Empty
        Me.btndown.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btndown.FlatAppearance.BorderSize = 0
        Me.btndown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btndown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndown.ForeColor = System.Drawing.Color.Black
        Me.btndown.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btndown.GradientForeColor = System.Drawing.Color.Black
        Me.btndown.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btndown.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btndown.Image = Global.Aruti.Main.My.Resources.Resources.MoveDown_16
        Me.btndown.Location = New System.Drawing.Point(343, 297)
        Me.btndown.Name = "btndown"
        Me.btndown.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btndown.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btndown.Size = New System.Drawing.Size(39, 28)
        Me.btndown.TabIndex = 456
        Me.btndown.UseVisualStyleBackColor = True
        '
        'btnUp
        '
        Me.btnUp.BackColor = System.Drawing.Color.White
        Me.btnUp.BackgroundImage = CType(resources.GetObject("btnUp.BackgroundImage"), System.Drawing.Image)
        Me.btnUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUp.BorderColor = System.Drawing.Color.Empty
        Me.btnUp.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnUp.FlatAppearance.BorderSize = 0
        Me.btnUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUp.ForeColor = System.Drawing.Color.Black
        Me.btnUp.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUp.GradientForeColor = System.Drawing.Color.Black
        Me.btnUp.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUp.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUp.Image = Global.Aruti.Main.My.Resources.Resources.MoveUp_16
        Me.btnUp.Location = New System.Drawing.Point(388, 297)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUp.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUp.Size = New System.Drawing.Size(39, 28)
        Me.btnUp.TabIndex = 455
        Me.btnUp.UseVisualStyleBackColor = True
        '
        'objCheckAll
        '
        Me.objCheckAll.AutoSize = True
        Me.objCheckAll.Location = New System.Drawing.Point(18, 138)
        Me.objCheckAll.Name = "objCheckAll"
        Me.objCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.objCheckAll.TabIndex = 454
        Me.objCheckAll.UseVisualStyleBackColor = True
        '
        'lnkAddtoExistingList
        '
        Me.lnkAddtoExistingList.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAddtoExistingList.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAddtoExistingList.Location = New System.Drawing.Point(621, 303)
        Me.lnkAddtoExistingList.Name = "lnkAddtoExistingList"
        Me.lnkAddtoExistingList.Size = New System.Drawing.Size(142, 17)
        Me.lnkAddtoExistingList.TabIndex = 453
        Me.lnkAddtoExistingList.TabStop = True
        Me.lnkAddtoExistingList.Text = "Add to Final Shortlisted List"
        Me.lnkAddtoExistingList.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgFinalApplicant
        '
        Me.dgFinalApplicant.AllowUserToAddRows = False
        Me.dgFinalApplicant.AllowUserToDeleteRows = False
        Me.dgFinalApplicant.AllowUserToResizeColumns = False
        Me.dgFinalApplicant.AllowUserToResizeRows = False
        Me.dgFinalApplicant.BackgroundColor = System.Drawing.Color.White
        Me.dgFinalApplicant.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgFinalApplicant.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgFinalApplicant.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhFinalSelect, Me.colhFinalApplicantName, Me.colhFinalGender, Me.colhFinalBirthdate, Me.colhFinalPhone, Me.colhFinalEmail, Me.colhdgStatus, Me.objcolhdgFinalApplicantID, Me.colhFinalApprovalSubmit})
        Me.dgFinalApplicant.Location = New System.Drawing.Point(10, 353)
        Me.dgFinalApplicant.Name = "dgFinalApplicant"
        Me.dgFinalApplicant.RowHeadersVisible = False
        Me.dgFinalApplicant.RowHeadersWidth = 25
        Me.dgFinalApplicant.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgFinalApplicant.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgFinalApplicant.Size = New System.Drawing.Size(755, 172)
        Me.dgFinalApplicant.TabIndex = 452
        '
        'objcolhFinalSelect
        '
        Me.objcolhFinalSelect.Frozen = True
        Me.objcolhFinalSelect.HeaderText = ""
        Me.objcolhFinalSelect.Name = "objcolhFinalSelect"
        Me.objcolhFinalSelect.Width = 25
        '
        'dgShortListApplicant
        '
        Me.dgShortListApplicant.AllowUserToAddRows = False
        Me.dgShortListApplicant.AllowUserToDeleteRows = False
        Me.dgShortListApplicant.AllowUserToResizeColumns = False
        Me.dgShortListApplicant.AllowUserToResizeRows = False
        Me.dgShortListApplicant.BackgroundColor = System.Drawing.Color.White
        Me.dgShortListApplicant.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgShortListApplicant.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgShortListApplicant.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhSelect, Me.colhApplicant, Me.colhGender, Me.colhBirthdate, Me.colhPhone, Me.colhEmail, Me.objColhShortListId, Me.objColhShortListApplicantId})
        Me.dgShortListApplicant.Location = New System.Drawing.Point(10, 133)
        Me.dgShortListApplicant.MultiSelect = False
        Me.dgShortListApplicant.Name = "dgShortListApplicant"
        Me.dgShortListApplicant.RowHeadersVisible = False
        Me.dgShortListApplicant.RowHeadersWidth = 25
        Me.dgShortListApplicant.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgShortListApplicant.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgShortListApplicant.Size = New System.Drawing.Size(755, 158)
        Me.dgShortListApplicant.TabIndex = 451
        '
        'objcolhSelect
        '
        Me.objcolhSelect.Frozen = True
        Me.objcolhSelect.HeaderText = ""
        Me.objcolhSelect.Name = "objcolhSelect"
        Me.objcolhSelect.Width = 25
        '
        'lblFinalApplicant
        '
        Me.lblFinalApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinalApplicant.Location = New System.Drawing.Point(12, 303)
        Me.lblFinalApplicant.Name = "lblFinalApplicant"
        Me.lblFinalApplicant.Size = New System.Drawing.Size(92, 17)
        Me.lblFinalApplicant.TabIndex = 449
        Me.lblFinalApplicant.Text = "Final Applicant"
        Me.lblFinalApplicant.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblShortListApplicant
        '
        Me.lblShortListApplicant.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShortListApplicant.Location = New System.Drawing.Point(12, 108)
        Me.lblShortListApplicant.Name = "lblShortListApplicant"
        Me.lblShortListApplicant.Size = New System.Drawing.Size(139, 16)
        Me.lblShortListApplicant.TabIndex = 448
        Me.lblShortListApplicant.Text = "Short List Applicant"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.cboVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.txtClosingDate)
        Me.gbFilterCriteria.Controls.Add(Me.LblVacancyType)
        Me.gbFilterCriteria.Controls.Add(Me.txtOpeningDate)
        Me.gbFilterCriteria.Controls.Add(Me.cboRefNo)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchRefNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblRefNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblEnddate)
        Me.gbFilterCriteria.Controls.Add(Me.lblVacancy)
        Me.gbFilterCriteria.Controls.Add(Me.lblStartdate)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(7, 8)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(756, 92)
        Me.gbFilterCriteria.TabIndex = 447
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(732, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 454
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(708, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 453
        Me.objbtnSearch.TabStop = False
        '
        'cboVacancy
        '
        Me.cboVacancy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancy.DropDownWidth = 450
        Me.cboVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancy.FormattingEnabled = True
        Me.cboVacancy.Location = New System.Drawing.Point(428, 32)
        Me.cboVacancy.Name = "cboVacancy"
        Me.cboVacancy.Size = New System.Drawing.Size(286, 21)
        Me.cboVacancy.TabIndex = 460
        '
        'objbtnSearchVacancyType
        '
        Me.objbtnSearchVacancyType.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancyType.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancyType.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancyType.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancyType.BorderSelected = False
        Me.objbtnSearchVacancyType.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancyType.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancyType.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancyType.Location = New System.Drawing.Point(289, 32)
        Me.objbtnSearchVacancyType.Name = "objbtnSearchVacancyType"
        Me.objbtnSearchVacancyType.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancyType.TabIndex = 463
        '
        'cboVacancyType
        '
        Me.cboVacancyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVacancyType.DropDownWidth = 350
        Me.cboVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVacancyType.FormattingEnabled = True
        Me.cboVacancyType.Location = New System.Drawing.Point(103, 32)
        Me.cboVacancyType.Name = "cboVacancyType"
        Me.cboVacancyType.Size = New System.Drawing.Size(180, 21)
        Me.cboVacancyType.TabIndex = 462
        '
        'objbtnSearchVacancy
        '
        Me.objbtnSearchVacancy.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchVacancy.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchVacancy.BorderSelected = False
        Me.objbtnSearchVacancy.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchVacancy.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchVacancy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchVacancy.Location = New System.Drawing.Point(720, 32)
        Me.objbtnSearchVacancy.Name = "objbtnSearchVacancy"
        Me.objbtnSearchVacancy.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchVacancy.TabIndex = 461
        '
        'txtClosingDate
        '
        Me.txtClosingDate.BackColor = System.Drawing.Color.White
        Me.txtClosingDate.Flags = 0
        Me.txtClosingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClosingDate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtClosingDate.Location = New System.Drawing.Point(630, 59)
        Me.txtClosingDate.Name = "txtClosingDate"
        Me.txtClosingDate.ReadOnly = True
        Me.txtClosingDate.Size = New System.Drawing.Size(84, 21)
        Me.txtClosingDate.TabIndex = 457
        '
        'LblVacancyType
        '
        Me.LblVacancyType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVacancyType.Location = New System.Drawing.Point(6, 34)
        Me.LblVacancyType.Name = "LblVacancyType"
        Me.LblVacancyType.Size = New System.Drawing.Size(91, 17)
        Me.LblVacancyType.TabIndex = 459
        Me.LblVacancyType.Text = "Vacancy Type"
        Me.LblVacancyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOpeningDate
        '
        Me.txtOpeningDate.BackColor = System.Drawing.Color.White
        Me.txtOpeningDate.Flags = 0
        Me.txtOpeningDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOpeningDate.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtOpeningDate.Location = New System.Drawing.Point(428, 59)
        Me.txtOpeningDate.Name = "txtOpeningDate"
        Me.txtOpeningDate.ReadOnly = True
        Me.txtOpeningDate.Size = New System.Drawing.Size(84, 21)
        Me.txtOpeningDate.TabIndex = 456
        '
        'cboRefNo
        '
        Me.cboRefNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRefNo.FormattingEnabled = True
        Me.cboRefNo.Location = New System.Drawing.Point(103, 59)
        Me.cboRefNo.Name = "cboRefNo"
        Me.cboRefNo.Size = New System.Drawing.Size(180, 21)
        Me.cboRefNo.TabIndex = 438
        '
        'objbtnSearchRefNo
        '
        Me.objbtnSearchRefNo.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchRefNo.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchRefNo.BorderSelected = False
        Me.objbtnSearchRefNo.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchRefNo.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchRefNo.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchRefNo.Location = New System.Drawing.Point(289, 59)
        Me.objbtnSearchRefNo.Name = "objbtnSearchRefNo"
        Me.objbtnSearchRefNo.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchRefNo.TabIndex = 435
        '
        'lblRefNo
        '
        Me.lblRefNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRefNo.Location = New System.Drawing.Point(6, 61)
        Me.lblRefNo.Name = "lblRefNo"
        Me.lblRefNo.Size = New System.Drawing.Size(91, 17)
        Me.lblRefNo.TabIndex = 434
        Me.lblRefNo.Text = "Reference No"
        Me.lblRefNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEnddate
        '
        Me.lblEnddate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnddate.Location = New System.Drawing.Point(518, 61)
        Me.lblEnddate.Name = "lblEnddate"
        Me.lblEnddate.Size = New System.Drawing.Size(106, 17)
        Me.lblEnddate.TabIndex = 441
        Me.lblEnddate.Text = "Vacancy End date"
        Me.lblEnddate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVacancy
        '
        Me.lblVacancy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVacancy.Location = New System.Drawing.Point(319, 34)
        Me.lblVacancy.Name = "lblVacancy"
        Me.lblVacancy.Size = New System.Drawing.Size(103, 17)
        Me.lblVacancy.TabIndex = 436
        Me.lblVacancy.Text = "Vacancy"
        Me.lblVacancy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartdate
        '
        Me.lblStartdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartdate.Location = New System.Drawing.Point(316, 61)
        Me.lblStartdate.Name = "lblStartdate"
        Me.lblStartdate.Size = New System.Drawing.Size(106, 17)
        Me.lblStartdate.TabIndex = 439
        Me.lblStartdate.Text = "Vacancy Start date"
        Me.lblStartdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.txtRemarks)
        Me.EZeeFooter1.Controls.Add(Me.lblRemarks)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Controls.Add(Me.btnApprove)
        Me.EZeeFooter1.Controls.Add(Me.btnDisapprove)
        Me.EZeeFooter1.Controls.Add(Me.rbtnSplitButton)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 531)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(775, 55)
        Me.EZeeFooter1.TabIndex = 446
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(580, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 447
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnApprove
        '
        Me.btnApprove.BackColor = System.Drawing.Color.White
        Me.btnApprove.BackgroundImage = CType(resources.GetObject("btnApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnApprove.FlatAppearance.BorderSize = 0
        Me.btnApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.Black
        Me.btnApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Location = New System.Drawing.Point(484, 13)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApprove.Size = New System.Drawing.Size(90, 30)
        Me.btnApprove.TabIndex = 451
        Me.btnApprove.Text = "Approve"
        Me.btnApprove.UseVisualStyleBackColor = True
        '
        'btnDisapprove
        '
        Me.btnDisapprove.BackColor = System.Drawing.Color.White
        Me.btnDisapprove.BackgroundImage = CType(resources.GetObject("btnDisapprove.BackgroundImage"), System.Drawing.Image)
        Me.btnDisapprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDisapprove.BorderColor = System.Drawing.Color.Empty
        Me.btnDisapprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDisapprove.FlatAppearance.BorderSize = 0
        Me.btnDisapprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDisapprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisapprove.ForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDisapprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDisapprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.Location = New System.Drawing.Point(580, 13)
        Me.btnDisapprove.Name = "btnDisapprove"
        Me.btnDisapprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDisapprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDisapprove.Size = New System.Drawing.Size(90, 30)
        Me.btnDisapprove.TabIndex = 450
        Me.btnDisapprove.Text = "Disapprove"
        Me.btnDisapprove.UseVisualStyleBackColor = True
        '
        'rbtnSplitButton
        '
        Me.rbtnSplitButton.BorderColor = System.Drawing.Color.Black
        Me.rbtnSplitButton.ContextMenuStrip = Me.mnuOperation
        Me.rbtnSplitButton.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtnSplitButton.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.rbtnSplitButton.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.rbtnSplitButton.Location = New System.Drawing.Point(10, 13)
        Me.rbtnSplitButton.Name = "rbtnSplitButton"
        Me.rbtnSplitButton.ShowDefaultBorderColor = True
        Me.rbtnSplitButton.Size = New System.Drawing.Size(110, 30)
        Me.rbtnSplitButton.SplitButtonMenu = Me.mnuOperation
        Me.rbtnSplitButton.TabIndex = 449
        Me.rbtnSplitButton.Text = "Operations"
        '
        'mnuOperation
        '
        Me.mnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuViewApplicantCV, Me.mnuViewShortListedApplicant, Me.mnuFinalShortlistedApplicants, Me.mnuViewPendingApplicants, Me.mnuViewApprovedApplicants, Me.mnuViewDisapprovedApplicants, Me.mnuSubmitForApproval})
        Me.mnuOperation.Name = "mnuEvaluation"
        Me.mnuOperation.Size = New System.Drawing.Size(246, 158)
        '
        'mnuViewApplicantCV
        '
        Me.mnuViewApplicantCV.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuViewShortListedApplicantCV, Me.mnuViewFinalShortListedApplicantCV})
        Me.mnuViewApplicantCV.Name = "mnuViewApplicantCV"
        Me.mnuViewApplicantCV.Size = New System.Drawing.Size(245, 22)
        Me.mnuViewApplicantCV.Text = "View Applicant CV "
        '
        'mnuViewShortListedApplicantCV
        '
        Me.mnuViewShortListedApplicantCV.Name = "mnuViewShortListedApplicantCV"
        Me.mnuViewShortListedApplicantCV.Size = New System.Drawing.Size(261, 22)
        Me.mnuViewShortListedApplicantCV.Text = "View ShortListed Applicant CV"
        '
        'mnuViewFinalShortListedApplicantCV
        '
        Me.mnuViewFinalShortListedApplicantCV.Name = "mnuViewFinalShortListedApplicantCV"
        Me.mnuViewFinalShortListedApplicantCV.Size = New System.Drawing.Size(261, 22)
        Me.mnuViewFinalShortListedApplicantCV.Text = "View Final ShortListed Applicant CV"
        '
        'mnuViewShortListedApplicant
        '
        Me.mnuViewShortListedApplicant.Name = "mnuViewShortListedApplicant"
        Me.mnuViewShortListedApplicant.Size = New System.Drawing.Size(245, 22)
        Me.mnuViewShortListedApplicant.Text = "View Shortlisted Applicants"
        '
        'mnuFinalShortlistedApplicants
        '
        Me.mnuFinalShortlistedApplicants.Name = "mnuFinalShortlistedApplicants"
        Me.mnuFinalShortlistedApplicants.Size = New System.Drawing.Size(245, 22)
        Me.mnuFinalShortlistedApplicants.Text = "View Final Shortlisted Applicants"
        '
        'mnuViewPendingApplicants
        '
        Me.mnuViewPendingApplicants.Name = "mnuViewPendingApplicants"
        Me.mnuViewPendingApplicants.Size = New System.Drawing.Size(245, 22)
        Me.mnuViewPendingApplicants.Text = "View Pending Applicants"
        '
        'mnuViewApprovedApplicants
        '
        Me.mnuViewApprovedApplicants.Name = "mnuViewApprovedApplicants"
        Me.mnuViewApprovedApplicants.Size = New System.Drawing.Size(245, 22)
        Me.mnuViewApprovedApplicants.Text = "View Approved Applicants"
        '
        'mnuViewDisapprovedApplicants
        '
        Me.mnuViewDisapprovedApplicants.Name = "mnuViewDisapprovedApplicants"
        Me.mnuViewDisapprovedApplicants.Size = New System.Drawing.Size(245, 22)
        Me.mnuViewDisapprovedApplicants.Text = "View Disapproved Applicants"
        '
        'mnuSubmitForApproval
        '
        Me.mnuSubmitForApproval.Name = "mnuSubmitForApproval"
        Me.mnuSubmitForApproval.Size = New System.Drawing.Size(245, 22)
        Me.mnuSubmitForApproval.Text = "Submit for Approval"
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(675, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 448
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(159, 15)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(78, 16)
        Me.lblRemarks.TabIndex = 467
        Me.lblRemarks.Text = "Remarks"
        '
        'txtRemarks
        '
        Me.txtRemarks.AcceptsTab = True
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.Location = New System.Drawing.Point(246, 9)
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth
        Me.txtRemarks.Size = New System.Drawing.Size(223, 37)
        Me.txtRemarks.TabIndex = 468
        Me.txtRemarks.Text = ""
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Applicant Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Applicant Name"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Gender"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 90
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "BirthDate"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 95
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Phone"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 126
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn7.HeaderText = "Applicant Code"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn8.HeaderText = "Applicant Name"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn9.HeaderText = "Gender"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "BirthDate"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Visible = False
        Me.DataGridViewTextBoxColumn10.Width = 200
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Phone"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Visible = False
        Me.DataGridViewTextBoxColumn11.Width = 300
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Visible = False
        Me.DataGridViewTextBoxColumn12.Width = 126
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Visible = False
        Me.DataGridViewTextBoxColumn13.Width = 126
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "ShortListIsGrp"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = ""
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'colhFinalApplicantName
        '
        Me.colhFinalApplicantName.HeaderText = "Applicant"
        Me.colhFinalApplicantName.Name = "colhFinalApplicantName"
        Me.colhFinalApplicantName.ReadOnly = True
        Me.colhFinalApplicantName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFinalApplicantName.Width = 201
        '
        'colhFinalGender
        '
        Me.colhFinalGender.HeaderText = "Gender"
        Me.colhFinalGender.Name = "colhFinalGender"
        Me.colhFinalGender.ReadOnly = True
        Me.colhFinalGender.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFinalGender.Width = 80
        '
        'colhFinalBirthdate
        '
        Me.colhFinalBirthdate.HeaderText = "BirthDate"
        Me.colhFinalBirthdate.Name = "colhFinalBirthdate"
        Me.colhFinalBirthdate.ReadOnly = True
        Me.colhFinalBirthdate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFinalBirthdate.Width = 90
        '
        'colhFinalPhone
        '
        Me.colhFinalPhone.HeaderText = "Phone"
        Me.colhFinalPhone.Name = "colhFinalPhone"
        Me.colhFinalPhone.ReadOnly = True
        Me.colhFinalPhone.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFinalPhone.Width = 95
        '
        'colhFinalEmail
        '
        Me.colhFinalEmail.HeaderText = "Email"
        Me.colhFinalEmail.Name = "colhFinalEmail"
        Me.colhFinalEmail.ReadOnly = True
        Me.colhFinalEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFinalEmail.Width = 126
        '
        'colhdgStatus
        '
        Me.colhdgStatus.HeaderText = "Status"
        Me.colhdgStatus.Name = "colhdgStatus"
        Me.colhdgStatus.ReadOnly = True
        Me.colhdgStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objcolhdgFinalApplicantID
        '
        Me.objcolhdgFinalApplicantID.HeaderText = "FinalApplicantID"
        Me.objcolhdgFinalApplicantID.Name = "objcolhdgFinalApplicantID"
        Me.objcolhdgFinalApplicantID.ReadOnly = True
        Me.objcolhdgFinalApplicantID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhdgFinalApplicantID.Visible = False
        '
        'colhFinalApprovalSubmit
        '
        Me.colhFinalApprovalSubmit.HeaderText = "Approval Submitted"
        Me.colhFinalApprovalSubmit.Name = "colhFinalApprovalSubmit"
        Me.colhFinalApprovalSubmit.ReadOnly = True
        Me.colhFinalApprovalSubmit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhFinalApprovalSubmit.Width = 120
        '
        'colhApplicant
        '
        Me.colhApplicant.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhApplicant.HeaderText = "Applicant"
        Me.colhApplicant.Name = "colhApplicant"
        Me.colhApplicant.ReadOnly = True
        Me.colhApplicant.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhGender
        '
        Me.colhGender.HeaderText = "Gender"
        Me.colhGender.Name = "colhGender"
        Me.colhGender.ReadOnly = True
        Me.colhGender.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhBirthdate
        '
        Me.colhBirthdate.HeaderText = "BirthDate"
        Me.colhBirthdate.Name = "colhBirthdate"
        Me.colhBirthdate.ReadOnly = True
        Me.colhBirthdate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhPhone
        '
        Me.colhPhone.HeaderText = "Phone"
        Me.colhPhone.Name = "colhPhone"
        Me.colhPhone.ReadOnly = True
        Me.colhPhone.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colhEmail
        '
        Me.colhEmail.HeaderText = "Email"
        Me.colhEmail.Name = "colhEmail"
        Me.colhEmail.ReadOnly = True
        Me.colhEmail.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colhEmail.Width = 126
        '
        'objColhShortListId
        '
        Me.objColhShortListId.HeaderText = "objColhShortListId"
        Me.objColhShortListId.Name = "objColhShortListId"
        Me.objColhShortListId.ReadOnly = True
        Me.objColhShortListId.Visible = False
        '
        'objColhShortListApplicantId
        '
        Me.objColhShortListApplicantId.HeaderText = "objColhShortListApplicantId"
        Me.objColhShortListApplicantId.Name = "objColhShortListApplicantId"
        Me.objColhShortListApplicantId.ReadOnly = True
        Me.objColhShortListApplicantId.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "objColhShortListApplicantId"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Visible = False
        Me.DataGridViewTextBoxColumn16.Width = 25
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "objColhShortListId"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn17.Visible = False
        Me.DataGridViewTextBoxColumn17.Width = 200
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "objColhShortListApplicantId"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn18.Visible = False
        Me.DataGridViewTextBoxColumn18.Width = 300
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "Gender"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "BirthDate"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "Phone"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "Email"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "ShortListIsGrp"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        Me.DataGridViewTextBoxColumn23.Visible = False
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.HeaderText = "objColhShortListId"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        Me.DataGridViewTextBoxColumn24.Visible = False
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.HeaderText = "objColhShortListApplicantId"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        Me.DataGridViewTextBoxColumn25.Visible = False
        '
        'frmFinalApplicant
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(775, 586)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFinalApplicant"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Final Applicant"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        CType(Me.dgFinalApplicant, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgShortListApplicant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.mnuOperation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchRefNo As eZee.Common.eZeeGradientButton
    Friend WithEvents cboRefNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblVacancy As System.Windows.Forms.Label
    Friend WithEvents lblRefNo As System.Windows.Forms.Label
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dgShortListApplicant As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgFinalApplicant As System.Windows.Forms.DataGridView
    Friend WithEvents lblFinalApplicant As System.Windows.Forms.Label
    Friend WithEvents lblShortListApplicant As System.Windows.Forms.Label
    Friend WithEvents lnkAddtoExistingList As System.Windows.Forms.LinkLabel
    Friend WithEvents objCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchVacancyType As eZee.Common.eZeeGradientButton
    Friend WithEvents cboVacancyType As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchVacancy As eZee.Common.eZeeGradientButton
    Friend WithEvents cboVacancy As System.Windows.Forms.ComboBox
    Friend WithEvents LblVacancyType As System.Windows.Forms.Label
    Friend WithEvents txtClosingDate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtOpeningDate As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEnddate As System.Windows.Forms.Label
    Friend WithEvents lblStartdate As System.Windows.Forms.Label
    Friend WithEvents rbtnSplitButton As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuViewApplicantCV As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewShortListedApplicant As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFinalShortlistedApplicants As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewPendingApplicants As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewApprovedApplicants As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSubmitForApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btndown As eZee.Common.eZeeLightButton
    Friend WithEvents btnUp As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objchkFinalSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents btnApprove As eZee.Common.eZeeLightButton
    Friend WithEvents btnDisapprove As eZee.Common.eZeeLightButton
    Friend WithEvents mnuViewShortListedApplicantCV As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewFinalShortListedApplicantCV As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuViewDisapprovedApplicants As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents LblStatus As System.Windows.Forms.Label
    Friend WithEvents objcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhApplicant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhBirthdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPhone As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objColhShortListId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objColhShortListApplicantId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtSearchFinalShortListed As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSearchShortListed As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objcolhFinalSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhFinalApplicantName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFinalGender As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFinalBirthdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFinalPhone As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFinalEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhdgStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhdgFinalApplicantID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhFinalApprovalSubmit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As System.Windows.Forms.RichTextBox
End Class
