﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmAccountConfigurationList

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmAccountConfigurationList"

    Private objAccountConfig As clsAccountConfiguration
    'Sohail (03 Jul 2020) -- Start
    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
    Private mdtPeriod_startdate As Date
    Private mdtPeriod_enddate As Date
    'Sohail (03 Jul 2020) -- End
#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            cboTransactionType.BackColor = GUI.ColorOptional
            cboTrnHead.BackColor = GUI.ColorOptional
            cboAccountName.BackColor = GUI.ColorOptional
            cboAccountGroup.BackColor = GUI.ColorOptional
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            cboPeriod.BackColor = GUI.ColorComp
            'Sohail (03 Jul 2020) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim objAccount As New clsAccount_master
        'Dim objTransactionHead As New clsTransactionHead 'Sohail (02 Aug 2017)
        'Sohail (03 Jul 2020) -- Start
        'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
        Dim objPeriod As New clscommom_period_Tran
        Dim mintFirstPeriodID As Integer = 0
        'Sohail (03 Jul 2020) -- End
        Dim dsCombo As DataSet
        Try
            dsCombo = objMaster.getComboListJVTransactionType("TranType")
            With cboTransactionType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("TranType")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            'Sohail (21 Mar 2014) -- Start
            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
            'dsCombo = objTransactionHead.getComboList("TranHead", True)
            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'dsCombo = objTransactionHead.getComboList("TranHead", True, , , , True, , , , True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objTransactionHead.getComboList("TranHead", True, , , , True, , , , True, True)
            'Sohail (18 Apr 2016) -- Start
            'Enhancement - 59.1 - NET PAY ROUNDING ADJUSTMENT head for the difference of actual Net Pay and Rounded Net Pay for HERON Portico.
            'dsCombo = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , , True, , , True, True)
            'dsCombo = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "TranHead", True, , , , True, , , True, True, True) 'Sohail (02 Aug 2017)
            'Sohail (18 Apr 2016) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (17 Sep 2014) -- End
            'Sohail (21 Mar 2014) -- End
            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            'With cboTrnHead
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombo.Tables("TranHead")
            '    If .Items.Count > 0 Then .SelectedValue = 0
            'End With
            'Sohail (02 Aug 2017) -- End

            dsCombo = objAccount.getComboList("Accounts")
            With cboAccountName
                .ValueMember = "accountunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Accounts")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            dsCombo = objMaster.getComboListAccountGroup("AccountGroup")
            With cboAccountGroup
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("AccountGroup")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            mintFirstPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open, False, True)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, 0)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = mintFirstPeriodID
            End With
            'Sohail (03 Jul 2020) -- End


            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            dsCombo = objMaster.getComboListTranHeadActiveInActive("ActiveInactive", False)
            With cboActiveInactive
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsCombo.Tables("ActiveInactive")
                .SelectedIndex = 0
            End With
            'Gajanan [24-Aug-2020] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMaster = Nothing
            objAccount = Nothing
            'objTransactionHead = Nothing 'Sohail (02 Aug 2017)
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            objPeriod = Nothing
            'Sohail (03 Jul 2020) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet
        Dim intAcGrpSelValue As Integer
        Dim intAcTrnTypeSelValue As Integer
        Try

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            'If User._Object.Privilege._AllowToViewCompanyAccountConfigurationList = False Then Exit Sub 'Sohail (02 Aug 2017)
            'Anjan (25 Oct 2012)-End 

            lvTrnHeadAccount.Items.Clear()
            If User._Object.Privilege._AllowToViewCompanyAccountConfigurationList = False Then Exit Try 'Sohail (02 Aug 2017)
            objchkSelectAll.Checked = False 'Sohail (30 Aug 2013)

            Dim lvItem As ListViewItem

            intAcGrpSelValue = CInt(cboAccountGroup.SelectedValue)
            intAcTrnTypeSelValue = CInt(cboTransactionType.SelectedValue)
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            'dsList = objAccountConfig.GetList("AccountConfig", True, CInt(cboTransactionType.SelectedValue), CInt(cboTrnHead.SelectedValue), CInt(cboAccountName.SelectedValue), intAcGrpSelValue)
            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            'dsList = objAccountConfig.GetList("AccountConfig", True, CInt(cboTransactionType.SelectedValue), CInt(cboTrnHead.SelectedValue), CInt(cboAccountName.SelectedValue), intAcGrpSelValue, mdtPeriod_enddate)
            dsList = objAccountConfig.GetList("AccountConfig", True, CInt(cboTransactionType.SelectedValue), CInt(cboTrnHead.SelectedValue), CInt(cboAccountName.SelectedValue), intAcGrpSelValue, mdtPeriod_enddate, "", CInt(cboActiveInactive.SelectedValue))
            'Gajanan [24-Aug-2020] -- End

            'Sohail (03 Jul 2020) -- End

            For Each dsRow As DataRow In dsList.Tables("AccountConfig").Rows
                lvItem = New ListViewItem

                'Sohail (30 Aug 2013) -- Start
                'TRA - ENHANCEMENT
                'lvItem.Text = dsRow.Item("accountconfigunkid").ToString
                lvItem.Text = ""
                'Sohail (30 Aug 2013) -- End
                lvItem.Tag = CInt(dsRow.Item("accountconfigunkid").ToString)

                'Sohail (06 Aug 2016) -- Start
                'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
                'cboTransactionType.SelectedValue = CInt(dsRow.Item("transactiontype_id").ToString)
                'lvItem.SubItems.Add(cboTransactionType.Text)
                lvItem.SubItems.Add(dsRow.Item("transactiontype_name").ToString)
                'Sohail (06 Aug 2016) -- End
                lvItem.SubItems(colhTransactionType.Index).Tag = CInt(dsRow.Item("transactiontype_id").ToString) 'Sohail (25 Jul 2020)

                lvItem.SubItems.Add(dsRow.Item("trnheadname").ToString)
                lvItem.SubItems(colhTrnHead.Index).Tag = CInt(dsRow.Item("tranheadunkid").ToString)

                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                lvItem.SubItems.Add(dsRow.Item("period_name").ToString)
                lvItem.SubItems(colhPeriod.Index).Tag = CInt(dsRow.Item("periodunkid").ToString)
                'Sohail (03 Jul 2020) -- End

                'Sohail (08 Nov 2011) -- Start
                lvItem.SubItems.Add(dsRow.Item("account_code").ToString)
                'Sohail (08 Nov 2011) -- End

                lvItem.SubItems.Add(dsRow.Item("account_name").ToString)
                lvItem.SubItems(colhAccountName.Index).Tag = CInt(dsRow.Item("accountunkid").ToString)

                cboAccountGroup.SelectedValue = CInt(dsRow.Item("accountgroup_id").ToString)
                lvItem.SubItems.Add(cboAccountGroup.Text)

                lvItem.SubItems.Add(dsRow.Item("shortname").ToString) 'Sohail (13 Mar 2013)
                lvItem.SubItems.Add(dsRow.Item("shortname2").ToString) 'Sohail (08 Jul 2017)
                'Sohail (03 Mar 2020) -- Start
                'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
                lvItem.SubItems.Add(dsRow.Item("shortname3").ToString)
                'Sohail (03 Mar 2020) -- End


                'Gajanan [24-Aug-2020] -- Start
                'NMB Enhancement : Allow to set account configuration mapping 
                'inactive for closed period to allow to map head on other account 
                'configuration screen from new period
                lvItem.SubItems.Add(CBool(dsRow.Item("isinactive")).ToString)
                'Gajanan [24-Aug-2020] -- End


                RemoveHandler lvTrnHeadAccount.ItemChecked, AddressOf lvTrnHeadAccount_ItemChecked 'Sohail (30 Aug 2013)
                lvTrnHeadAccount.Items.Add(lvItem)
                AddHandler lvTrnHeadAccount.ItemChecked, AddressOf lvTrnHeadAccount_ItemChecked 'Sohail (30 Aug 2013)
            Next
            cboAccountGroup.SelectedValue = intAcGrpSelValue
            cboTransactionType.SelectedValue = intAcTrnTypeSelValue

            'Sohail (08 Jul 2017) -- Start
            'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
            'If lvTrnHeadAccount.Items.Count > 15 Then
            If lvTrnHeadAccount.Items.Count > 14 Then
                'Sohail (08 Jul 2017) -- End
                colhTrnHead.Width = 170 - 18
            Else
                colhTrnHead.Width = 170
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
        Finally
            objbtnSearch.ShowResult(lvTrnHeadAccount.Items.Count.ToString())
            'Sohail (02 Aug 2017) -- End
        End Try
    End Sub

    'Anjan (25 Oct 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew's Request
    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AllowToAddCompanyConfig
            btnEdit.Enabled = User._Object.Privilege._AllowToEditCompanyConfig
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteCompanyConfig
            btnSetinactive.Enabled = User._Object.Privilege._AllowToInactiveCompanyAccountConfiguration 'Sohail (25 Jul 2020)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
    'Anjan (25 Oct 2012)-End 

    'Sohail (30 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Private Sub CheckAll(ByVal blnCheckAll As Boolean)
        Try
            For Each lvItem As ListViewItem In lvTrnHeadAccount.Items
                RemoveHandler lvTrnHeadAccount.ItemChecked, AddressOf lvTrnHeadAccount_ItemChecked
                lvItem.Checked = blnCheckAll
                AddHandler lvTrnHeadAccount.ItemChecked, AddressOf lvTrnHeadAccount_ItemChecked
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAll", mstrModuleName)
        End Try
    End Sub
    'Sohail (30 Aug 2013) -- End
#End Region

#Region " Form's Events "

    Private Sub frmAccountConfigurationList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            objAccountConfig = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAccountConfigurationList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAccountConfigurationList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Escape
                    btnClose.PerformClick()
                Case Keys.Delete
                    If lvTrnHeadAccount.Focus = True Then btnDelete.PerformClick()
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAccountConfigurationList_KeyDown", mstrModuleName)
        End Try
    End Sub
    Private Sub frmAccountConfigurationList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAccountConfig = New clsAccountConfiguration
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 


            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            Call SetVisibility()
            'Anjan (25 Oct 2012)-End 


            Call SetColor()
            Call FillCombo()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAccountConfigurationList_Load", mstrModuleName)
        End Try
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAccountConfiguration.SetMessages()
            objfrm._Other_ModuleNames = "clsAccountConfiguration"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

    'Sohail (02 Aug 2017) -- Start
    'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
#Region " Combobox Events "

    Private Sub cboTransactionType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTransactionType.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try

            cboTrnHead.DataSource = Nothing
            Select Case CInt(cboTransactionType.SelectedValue)
                Case enJVTransactionType.TRANSACTION_HEAD, 0
                    Dim objTransactionHead As New clsTransactionHead
                    dsCombo = objTransactionHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, , , , True, , , True, True, True)
                    cboTrnHead.ValueMember = "tranheadunkid"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")

                Case enJVTransactionType.LOAN
                    Dim objLoan As New clsLoan_Scheme
                    'Sohail (02 Apr 2018) -- Start
                    'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
                    'dsCombo = objLoan.getComboList(True, "List")
                    dsCombo = objLoan.getComboList(True, "List", -1, "", False, True)
                    'Sohail (02 Apr 2018) -- End
                    cboTrnHead.ValueMember = "loanschemeunkid"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")

                Case enJVTransactionType.SAVINGS
                    Dim objSaving As New clsSavingScheme
                    dsCombo = objSaving.getComboList(True, "List")
                    cboTrnHead.ValueMember = "savingschemeunkid"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")

                Case enJVTransactionType.PAY_PER_ACTIVITY
                    Dim objActivity As New clsActivity_Master
                    dsCombo = objActivity.getComboList("List", True)
                    cboTrnHead.ValueMember = "activityunkid"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")

                Case enJVTransactionType.CR_EXPENSE
                    Dim objExpense As New clsExpense_Master
                    dsCombo = objExpense.getComboList(0, True, "List")
                    cboTrnHead.ValueMember = "Id"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")

                Case enJVTransactionType.BANK
                    Dim objBankBranch As New clsbankbranch_master
                    dsCombo = objBankBranch.getListForCombo("List", True)
                    cboTrnHead.ValueMember = "branchunkid"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")

                    'Sohail (03 Jan 2019) -- Start
                    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
                Case enJVTransactionType.COMPANY_BANK
                    Dim objBankBranch As New clsCompany_Bank_tran
                    'Sohail (21 May 2020) -- Start
                    'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
                    'dsCombo = objBankBranch.GetComboList(Company._Object._Companyunkid, 2, "List", "")
                    dsCombo = objBankBranch.GetComboList(Company._Object._Companyunkid, 4, "List", "")
                    'Sohail (21 May 2020) -- End
                    cboTrnHead.ValueMember = "Id"
                    cboTrnHead.DisplayMember = "Name"
                    cboTrnHead.DataSource = dsCombo.Tables("List")
                    'Sohail (03 Jan 2019) -- End

                Case Else

                  
            End Select

            If cboTrnHead.Items.Count > 0 Then cboTrnHead.SelectedValue = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTransactionType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (03 Jul 2020) -- Start
    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriod_startdate = objPeriod._Start_Date
                mdtPeriod_enddate = objPeriod._End_Date
            Else
                mdtPeriod_startdate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriod_enddate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (03 Jul 2020) -- End


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Private Sub cboActiveInactive_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboActiveInactive.SelectedIndexChanged
        Try

            If CInt(cboActiveInactive.SelectedValue) = enTranHeadActiveInActive.ACTIVE Then
                btnSetinactive.Visible = True
            Else
                btnSetinactive.Visible = False
            End If

            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboActiveInactive_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Gajanan [24-Aug-2020] -- End

#End Region
    'Sohail (02 Aug 2017) -- End

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objFrm As New frmAccountConfiguration
            If objFrm.displayDialog(-1, enAction.ADD_CONTINUE) = True Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lvTrnHeadAccount.DoubleClick
        Try
            If lvTrnHeadAccount.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Account Configuration from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvTrnHeadAccount.Select()
                Exit Sub
            End If
            Dim objFrm As New frmAccountConfiguration
            Try
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvTrnHeadAccount.SelectedItems(0).Index
                If objFrm.displayDialog(CInt(lvTrnHeadAccount.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call FillList()
                End If
                objFrm = Nothing

                lvTrnHeadAccount.Items(intSelectedIndex).Selected = True
                lvTrnHeadAccount.EnsureVisible(intSelectedIndex)
                lvTrnHeadAccount.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objFrm IsNot Nothing Then objFrm.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Sohail (30 Aug 2013) -- Start
        'TRA - ENHANCEMENT
        'If lvTrnHeadAccount.SelectedItems.Count < 1 Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Account Configuration from the list to perform further operation."), enMsgBoxStyle.Information) '?1
        '    lvTrnHeadAccount.Select()
        '    Exit Sub
        'End If
        If lvTrnHeadAccount.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please tick atleast one Account Configuration from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvTrnHeadAccount.Select()
            Exit Sub
        End If
        Dim strIDs As String = ""
        Dim allEmp As List(Of String) = (From lv In lvTrnHeadAccount.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToList
        strIDs = String.Join(",", allEmp.ToArray)
        'Sohail (30 Aug 2013) -- End
        'If objAccountConfig.isUsed(CInt(lvTrnHeadAccount.SelectedItems(0).Tag)) Then '<TODO make query in isused method>
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Account Configuration. Reason: This Account Configuration is in use."), enMsgBoxStyle.Information) '?2
        '    lvState.Select()
        '    Exit Sub
        'End If
        Try
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            Dim objPeriod As New clscommom_period_Tran
            Dim arrDistPeriod() As String = (From lv In lvTrnHeadAccount.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(colhPeriod.Index).Tag.ToString)).Distinct().ToArray
            For Each strPeriodID As String In arrDistPeriod
                objPeriod = New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(strPeriodID)
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Some of the periods of selected transactions are already closed."), enMsgBoxStyle.Information)
                    lvTrnHeadAccount.Select()
                    Exit Sub
                End If
            Next
            'Sohail (03 Jul 2020) -- End

            'Sohail (30 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'Dim intSelectedIndex As Integer
            'intSelectedIndex = lvTrnHeadAccount.SelectedItems(0).Index

            'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Account Configuration?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
            '    objAccountConfig.Delete(CInt(lvTrnHeadAccount.SelectedItems(0).Tag))
            '    lvTrnHeadAccount.SelectedItems(0).Remove()

            '    If lvTrnHeadAccount.Items.Count <= 0 Then
            '        Exit Try
            '    End If

            '    If lvTrnHeadAccount.Items.Count = intSelectedIndex Then
            '        intSelectedIndex = lvTrnHeadAccount.Items.Count - 1
            '        lvTrnHeadAccount.Items(intSelectedIndex).Selected = True
            '        lvTrnHeadAccount.EnsureVisible(intSelectedIndex)
            '    ElseIf lvTrnHeadAccount.Items.Count <> 0 Then
            '        lvTrnHeadAccount.Items(intSelectedIndex).Selected = True
            '        lvTrnHeadAccount.EnsureVisible(intSelectedIndex)
            '    End If
            'End If
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete ticked Account Configurations?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Cursor.Current = Cursors.WaitCursor

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objAccountConfig._FormName = mstrModuleName
                objAccountConfig._LoginEmployeeunkid = 0
                objAccountConfig._ClientIP = getIP()
                objAccountConfig._HostName = getHostName()
                objAccountConfig._FromWeb = False
                objAccountConfig._AuditUserId = User._Object._Userunkid
objAccountConfig._CompanyUnkid = Company._Object._Companyunkid
                objAccountConfig._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objAccountConfig.VoidAll(strIDs) = True Then
                    Call FillList()
                End If
            End If
            'Sohail (30 Aug 2013) -- End
            lvTrnHeadAccount.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
            'Sohail (30 Aug 2013) -- Start
            'TRA - ENHANCEMENT
        Finally
            Cursor.Current = Cursors.Default
            'Sohail (30 Aug 2013) -- End
        End Try
    End Sub


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Private Sub btnSetinactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetinactive.Click
        If lvTrnHeadAccount.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please tick atleast one Account Configuration from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvTrnHeadAccount.Select()
            Exit Sub
        End If
        Dim strIDs As String = ""
        Dim allEmp As List(Of String) = (From lv In lvTrnHeadAccount.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToList
        strIDs = String.Join(",", allEmp.ToArray)

        Try
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, selected period is already closed."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Sub
            End If
            objPeriod = Nothing

            Dim arrDistPeriod() As String = (From lv In lvTrnHeadAccount.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(colhPeriod.Index).Tag.ToString)).Distinct().ToArray
            For Each strPeriodID As String In arrDistPeriod
                objPeriod = New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(strPeriodID)
                If objPeriod._Statusid = enStatusType.Open Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Some of the periods of selected transactions are already opened."), enMsgBoxStyle.Information)
                    lvTrnHeadAccount.Select()
                    Exit Sub
                End If
                objPeriod = Nothing
            Next

            For Each lvItem As ListViewItem In lvTrnHeadAccount.CheckedItems
                If objAccountConfig.isExist(CInt(lvItem.SubItems(colhTransactionType.Index).Tag), CInt(lvItem.SubItems(colhTrnHead.Index).Tag), CInt(cboPeriod.SelectedValue)) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, #headname# is already mapped in selected period #periodname#.").Replace("#headname#", lvItem.SubItems(colhTransactionType.Index).Text & " : " & lvItem.SubItems(colhTrnHead.Index).Text).Replace("#periodname#", cboPeriod.Text), enMsgBoxStyle.Information)
                    lvTrnHeadAccount.Select()
                    Exit Sub
                End If
            Next

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to inactive ticked Account Configurations from #periodname#?").Replace("#periodname#", cboPeriod.Text), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Cursor.Current = Cursors.WaitCursor
                If objAccountConfig.InactiveAll(strIDs, CInt(cboPeriod.SelectedValue)) = False Then
                    If objAccountConfig._Message.Length > 0 Then
                        eZeeMsgBox.Show(objAccountConfig._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Else
                    Call FillList()
                End If
            End If
            lvTrnHeadAccount.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnSetinactive_Click", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try

    End Sub
    'Gajanan [24-Aug-2020] -- End
#End Region

    'Sohail (30 Aug 2013) -- Start
    'TRA - ENHANCEMENT
#Region " Listview's Events "
    Private Sub lvTrnHeadAccount_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvTrnHeadAccount.ItemChecked

        Try
            If lvTrnHeadAccount.CheckedItems.Count <= 0 Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Unchecked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvTrnHeadAccount.CheckedItems.Count < lvTrnHeadAccount.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Indeterminate
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            ElseIf lvTrnHeadAccount.CheckedItems.Count = lvTrnHeadAccount.Items.Count Then
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
                objchkSelectAll.CheckState = CheckState.Checked
                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTrnHeadAccount_ItemChecked", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            Call CheckAll(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (30 Aug 2013) -- End

#Region " Other Control's Events "
    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboTransactionType.SelectedValue = 0
            cboTrnHead.SelectedValue = 0
            cboAccountName.SelectedValue = 0
            cboAccountGroup.SelectedValue = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        'Sohail (21 Mar 2014) -- Start
        'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
        'Dim objTranHead As New clsTransactionHead
        'Dim dsList As DataSet
        'Sohail (21 Mar 2014) -- End
        If cboTrnHead.DataSource Is Nothing Then Exit Sub 'Sohail (02 Aug 2017)
        Dim objfrm As New frmCommonSearch

        Try
            'If User._Object._RightToLeft = True Then
            '    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    objfrm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(objfrm)
            'End If
            'dsList = objTranHead.getComboList("TranHead", False) 'Sohail (21 Mar 2014)
            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            'With cboTrnHead
            '    objfrm.ValueMember = "tranheadunkid"
            '    objfrm.DisplayMember = "Name"
            '    objfrm.CodeMember = "code"
            '    'Sohail (21 Mar 2014) -- Start
            '    'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
            '    'objfrm.DataSource = dsList.Tables("TranHead")
            '    objfrm.DataSource = CType(cboTrnHead.DataSource, DataTable)
            '    'Sohail (21 Mar 2014) -- End
            '    If objfrm.DisplayDialog Then
            '        .SelectedValue = objfrm.SelectedValue
            '    End If
            '    .Focus()
            'End With
            With cboTrnHead
                objfrm.ValueMember = cboTrnHead.ValueMember
                objfrm.DisplayMember = cboTrnHead.DisplayMember
                objfrm.CodeMember = "code"
                objfrm.DataSource = CType(cboTrnHead.DataSource, DataTable)
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
            'Sohail (02 Aug 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    'Sohail (08 Nov 2011) -- Start
    Private Sub objbtnSearchAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAccount.Click
        Dim objfrm As New frmCommonSearch
        Dim objAccount As New clsAccount_master
        Dim dsList As DataSet
        Try
            'If User._Object._RightToLeft = True Then
            '    objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    objfrm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(objfrm)
            'End If
            dsList = objAccount.GetList("AccountList")
            With cboAccountName
                objfrm.DataSource = dsList.Tables("AccountList")
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = "account_name"
                objfrm.CodeMember = "account_code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAccount_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objAccount = Nothing
        End Try
    End Sub
    'Sohail (08 Nov 2011) -- End



    'Anjan [20 Feb 2014] -- Start
    'ENHANCEMENT : Requested by Rutta for comment 336#
    Private Sub objbtnSearchTransactionType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTransactionType.Click
        Dim objMaster As New clsMasterData
        Dim objfrm As New frmCommonSearch
        Dim dsList As DataSet
        Try
            dsList = objMaster.getComboListJVTransactionType("HeadType")
            With cboTransactionType
                objfrm.ValueMember = "Id"
                objfrm.DisplayMember = "Name"
                objfrm.CodeMember = "Name"
                objfrm.DataSource = dsList.Tables("HeadType")
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTrantype_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan [20 Feb 2014 ] -- End


#End Region

#Region " Message "
    '1, "Please select Account Configuration from the list to perform further operation on it."
    '2, "Sorry, You cannot delete this Account Configuration. Reason: This Account Configuration is in use."
    '3, "Are you sure you want to delete this Account Configuration?"
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbTranAccountList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbTranAccountList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


			
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSetinactive.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSetinactive.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbTranAccountList.Text = Language._Object.getCaption(Me.gbTranAccountList.Name, Me.gbTranAccountList.Text)
            Me.colhID.Text = Language._Object.getCaption(CStr(Me.colhID.Tag), Me.colhID.Text)
            Me.colhTrnHead.Text = Language._Object.getCaption(CStr(Me.colhTrnHead.Tag), Me.colhTrnHead.Text)
            Me.colhAccountName.Text = Language._Object.getCaption(CStr(Me.colhAccountName.Tag), Me.colhAccountName.Text)
            Me.colhAccGroup.Text = Language._Object.getCaption(CStr(Me.colhAccGroup.Tag), Me.colhAccGroup.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblAccuntName.Text = Language._Object.getCaption(Me.lblAccuntName.Name, Me.lblAccuntName.Text)
            Me.lblTrnHead.Text = Language._Object.getCaption(Me.lblTrnHead.Name, Me.lblTrnHead.Text)
            Me.lblAccountGroup.Text = Language._Object.getCaption(Me.lblAccountGroup.Name, Me.lblAccountGroup.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhTransactionType.Text = Language._Object.getCaption(CStr(Me.colhTransactionType.Tag), Me.colhTransactionType.Text)
            Me.lblTransactionType.Text = Language._Object.getCaption(Me.lblTransactionType.Name, Me.lblTransactionType.Text)
            Me.colhAccountCode.Text = Language._Object.getCaption(CStr(Me.colhAccountCode.Tag), Me.colhAccountCode.Text)
            Me.colhShortname.Text = Language._Object.getCaption(CStr(Me.colhShortname.Tag), Me.colhShortname.Text)
			Me.colhShortname2.Text = Language._Object.getCaption(CStr(Me.colhShortname2.Tag), Me.colhShortname2.Text)
            Me.colhShortname3.Text = Language._Object.getCaption(CStr(Me.colhShortname3.Tag), Me.colhShortname3.Text)
			Me.EZeeGradientButton2.Text = Language._Object.getCaption(Me.EZeeGradientButton2.Name, Me.EZeeGradientButton2.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
			Me.btnSetinactive.Text = Language._Object.getCaption(Me.btnSetinactive.Name, Me.btnSetinactive.Text)
			Me.lblactiveinactive.Text = Language._Object.getCaption(Me.lblactiveinactive.Name, Me.lblactiveinactive.Text)
			Me.colhactiveinactive.Text = Language._Object.getCaption(CStr(Me.colhactiveinactive.Tag), Me.colhactiveinactive.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Account Configuration from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete ticked Account Configurations?")
            Language.setMessage(mstrModuleName, 3, "Please tick atleast one Account Configuration from the list to perform further operation.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Some of the periods of selected transactions are already closed.")
			Language.setMessage(mstrModuleName, 5, "Sorry, Some of the periods of selected transactions are already opened.")
			Language.setMessage(mstrModuleName, 6, "Are you sure you want to inactive ticked Account Configurations from #periodname#?")
			Language.setMessage(mstrModuleName, 7, "Sorry, selected period is already closed.")
			Language.setMessage(mstrModuleName, 8, "Sorry, #headname# is already mapped in selected period #periodname#.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class