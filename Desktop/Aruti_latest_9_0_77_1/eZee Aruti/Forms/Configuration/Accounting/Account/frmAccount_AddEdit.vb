﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmAccount_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmAccount_AddEdit"
    Private mblnCancel As Boolean = True
    Private mintAccountUnkid As Integer = -1
    Private menAction As enAction = enAction.ADD_ONE

    Private objAccount As clsAccount_master
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, _
                              ByVal pAction As enAction) As Boolean

        Try

            mintAccountUnkid = intUnkId
            menAction = pAction

            Me.ShowDialog()

            intUnkId = mintAccountUnkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "
    Private Sub SetColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            cboAccountGroup.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            With objAccount
                txtCode.Text = ._Account_Code
                txtName.Text = ._Account_Name
                cboAccountGroup.SelectedValue = ._Accountgroup_Id
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            With objAccount
                ._Account_Code = txtCode.Text.Trim
                ._Account_Name = txtName.Text.Trim
                ._Accountgroup_Id = CInt(cboAccountGroup.SelectedValue)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Try
            dsCombo = objMaster.getComboListAccountGroup("AccountGroup")
            With cboAccountGroup
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("AccountGroup")
                If .Items.Count > 0 Then .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmAccount_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objAccount = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAccount_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAccount_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Return
                    SendKeys.Send("{TAB}")
                Case Keys.S
                    If e.Control = True Then
                        btnSave.PerformClick()
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAccount_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub
    Private Sub frmAccount_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAccount = New clsAccount_master
        Try
            Call Set_Logo(Me, gApplicationType)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            Call SetColor()
            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objAccount._Accountunkid = mintAccountUnkid
                cboAccountGroup.Enabled = False
            End If
            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAccount_AddEdit_Load", mstrModuleName)
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAccount_master.SetMessages()
            objfrm._Other_ModuleNames = "clsAccount_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " ComboBox's Events "

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If txtCode.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Account Code can not be blank. Account Code is mandatory information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Try
            ElseIf txtName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Account Name can not be blank. Account Name is mandatory information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Exit Try
            ElseIf CInt(cboAccountGroup.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Account Group."), enMsgBoxStyle.Information)
                cboAccountGroup.Focus()
                Exit Try
            End If
            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objAccount._FormName = mstrModuleName
            objAccount._LoginEmployeeunkid = 0
            objAccount._ClientIP = getIP()
            objAccount._HostName = getHostName()
            objAccount._FromWeb = False
            objAccount._AuditUserId = User._Object._Userunkid
objAccount._CompanyUnkid = Company._Object._Companyunkid
            objAccount._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAccount.Update()
            Else
                blnFlag = objAccount.Insert()
            End If

            If blnFlag = False And objAccount._Message <> "" Then
                eZeeMsgBox.Show(objAccount._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objAccount = Nothing
                    objAccount = New clsAccount_master
                    Call GetValue()
                    txtCode.Select()
                Else
                    mintAccountUnkid = objAccount._Accountunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'Anjan (02 Sep 2011)-End 

            Call objFrm.displayDialog(txtName.Text, objAccount._Account_Name1, objAccount._Account_Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Message "
    '1, "Sorry, Account Code can not be blank. Account Code is mandatory information."
    '2, "Sorry, Account Name can not be blank. Account Name is mandatory information."
    '3, "Please select Account Group."
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.gbAccountInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAccountInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbAccountInfo.Text = Language._Object.getCaption(Me.gbAccountInfo.Name, Me.gbAccountInfo.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblAccountGroup.Text = Language._Object.getCaption(Me.lblAccountGroup.Name, Me.lblAccountGroup.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Account Code can not be blank. Account Code is mandatory information.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Account Name can not be blank. Account Name is mandatory information.")
			Language.setMessage(mstrModuleName, 3, "Please select Account Group.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class