﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmEmployeeSavings_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEmployeeSavings_AddEdit"

    Private mblnCancel As Boolean = True

    Private objEmployeeSaving As clsSaving_Tran

    Private menAction As enAction = enAction.ADD_ONE
    Private mintSavingtranunkid As Integer = -1
    Private objEmployeeData As clsEmployee_Master
    Private objSavingScheme As clsSavingScheme
    Private objPeriodData As clscommom_period_Tran
    Private mintMinLoan_Tenure As Integer = -1
    Private mdecMinContribution As Decimal = 0 'Sohail (11 May 2011)
    Private mdecMaturityAmount As Decimal = 0 'Sohail (11 May 2011)
    Private mblnFormload As Boolean = False
    Dim objInterestRate As clsInterestRate_tran

    Private mdtPayPeriodStartDate As Date = Nothing
    Private mdtPayPeriodEndDate As Date = Nothing
    'Shani [ 12 JAN 2015 ] -- START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.

    Private mdtContribution As DataTable = Nothing
    Private mdtIntRate As DataTable = Nothing
    Private drowcontribution() As DataRow
    Private dRowIntRate() As DataRow
    Private objSavingContribution As New clsSaving_contribution_tran
    Private objSavingInterestRate As New clsSaving_interest_rate_tran
    'Shani [ 12 JAN 2015 ] -- END


    'SHANI [09 MAR 2015]-START
    'Enhancement - Add Currency Field.
    Private mstrBaseCurrSign As String
    Private mintBaseCurrId As Integer
    Private mintPaidCurrId As Integer
    Private mdecBaseExRate As Decimal
    Private mdecPaidExRate As Decimal
    'SHANI [09 MAR 2015]--END 
    Private mintBaseCountryId As Integer = 0 'Sohail (09 Mar 2015)
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintSavingtranunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintSavingtranunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtVoucherNo.BackColor = GUI.ColorComp
            'NudSavingPeriod.BackColor = GUI.ColorComp
            'txtMaturityAmt.BackColor = GUI.ColorOptional
            txtMonthlyContribution.BackColor = GUI.ColorComp
            cboEmpName.BackColor = GUI.ColorComp
            cboPayPeriod.BackColor = GUI.ColorComp
            cboSavingScheme.BackColor = GUI.ColorComp
            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            txtOpeningContribution.BackColor = GUI.ColorOptional
            txtOpeningBalance.BackColor = GUI.ColorOptional
            'Sohail (12 Dec 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objExchangeRate As New clsExchangeRate
        objExchangeRate._ExchangeRateunkid = 1

        txtVoucherNo.Text = objEmployeeSaving._Voucherno
        'Sohail (12 Dec 2015) -- Start
        'Enhancement - Provide Deposit feaure in Employee Saving.
        cboPayPeriod.SelectedValue = objEmployeeSaving._Payperiodunkid
        'Sohail (12 Dec 2015) -- End
        If objEmployeeSaving._Effectivedate = Nothing Then
            dtpDate.Value = Now
        Else
            dtpDate.Value = objEmployeeSaving._Effectivedate
        End If

        'Sohail (12 Dec 2015) -- Start
        'Enhancement - Provide Deposit feaure in Employee Saving.
        'cboPayPeriod.SelectedValue = objEmployeeSaving._Payperiodunkid
        'Sohail (12 Dec 2015) -- End
        cboEmpName.SelectedValue = objEmployeeSaving._Employeeunkid

        cboSavingScheme.SelectedValue = objEmployeeSaving._Savingschemeunkid

        'If objEmployeeSaving._Savingperiods = 0 Then
        '    NudSavingPeriod.Value = 1
        'Else
        '    NudSavingPeriod.Value = objEmployeeSaving._Savingperiods
        'End If

        txtMonthlyContribution.Text = CStr(Format(objEmployeeSaving._Contribution, objExchangeRate._fmtCurrency))
        'If objEmployeeSaving._Maturitydate = Nothing Then
        '    dtpMaturity.Value = Now
        'Else
        '    dtpMaturity.Value = objEmployeeSaving._Maturitydate
        'End If

        'txtMaturityAmt.Text = CStr(Format(objEmployeeSaving._Maturity_Amount, objExchangeRate._fmtCurrency))
        objEmployeeSaving._Savingstatus = objEmployeeSaving._Savingstatus
        'txtMaturityAmt.Text = CStr(objEmployeeSaving._Maturity_Amount)

        'Anjan (26 Jan 2012)-Start
        'ENHANCEMENT : TRA COMMENTS

        If objEmployeeSaving._Stopdate = Nothing Then
            dtpStopDate.Value = ConfigParameter._Object._CurrentDateAndTime
            dtpStopDate.Checked = False
        Else
            dtpStopDate.Value = objEmployeeSaving._Stopdate
            dtpStopDate.Checked = True
        End If
        'Anjan (26 Jan 2012)-End 


        'Sohail (09 Mar 2015) -- Start
        'Enhancement - Provide Multi Currency on Saving scheme contribution.
        If menAction = enAction.EDIT_ONE Then
            cboCurrency.SelectedValue = objEmployeeSaving._Countryunkid
        Else
            cboCurrency.SelectedValue = mintBaseCountryId
        End If
        'Sohail (09 Mar 2015) -- End

        'Sohail (12 Dec 2015) -- Start
        'Enhancement - Provide Deposit feaure in Employee Saving.
        txtOpeningContribution.Text = Format(objEmployeeSaving._Bf_Amount, GUI.fmtCurrency)
        txtOpeningBalance.Text = Format(objEmployeeSaving._Bf_Balance, GUI.fmtCurrency)
        'Sohail (12 Dec 2015) -- End

        objExchangeRate = Nothing
    End Sub

    Private Sub SetValue()
        objEmployeeSaving._Voucherno = txtVoucherNo.Text
        objEmployeeSaving._Effectivedate = dtpDate.Value
        objEmployeeSaving._Payperiodunkid = CInt(cboPayPeriod.SelectedValue)
        objEmployeeSaving._Employeeunkid = CInt(cboEmpName.SelectedValue)
        objEmployeeSaving._Savingschemeunkid = CInt(cboSavingScheme.SelectedValue)
        'objEmployeeSaving._Savingperiods = CInt(NudSavingPeriod.Value)
        objEmployeeSaving._Contribution = txtMonthlyContribution.Decimal 'Sohail (11 May 2011)
        'objEmployeeSaving._Maturitydate = dtpMaturity.Value
        'objEmployeeSaving._Maturity_Amount = cdec(txtMaturityAmt.Text)
        If mintSavingtranunkid = -1 Then
            objEmployeeSaving._Savingstatus = 1 ' In progress
        Else
            objEmployeeSaving._Savingstatus = objEmployeeSaving._Savingstatus
        End If

        'objEmployeeSaving._Maturity_Amount = mdblMaturityAmount

        'Anjan (26 Jan 2012)-Start
        'ENHANCEMENT : TRA COMMENTS
        If dtpStopDate.Checked = True Then
            objEmployeeSaving._Stopdate = dtpStopDate.Value
        Else
            objEmployeeSaving._Stopdate = Nothing
        End If
        objEmployeeSaving._Userunkid = User._Object._Userunkid
        'Anjan (26 Jan 2012)-End 

        'SHANI [09 MAR 2015]-START
        'Enhancement - Add Currency Field.
        objEmployeeSaving._Countryunkid = CInt(cboCurrency.SelectedValue)
        'SHANI [09 MAR 2015]--END
        'Sohail (12 Dec 2015) -- Start
        'Enhancement - Provide Deposit feaure in Employee Saving.
        If menAction <> enAction.EDIT_ONE Then
            objEmployeeSaving._Total_Contribution = txtOpeningContribution.Decimal
            If txtOpeningBalance.Decimal < txtOpeningContribution.Decimal Then
                txtOpeningBalance.Decimal = txtOpeningContribution.Decimal
            End If
            objEmployeeSaving._Balance_Amount = txtOpeningBalance.Decimal
            objEmployeeSaving._Bf_Amount = txtOpeningContribution.Decimal
            objEmployeeSaving._Bf_Balance = txtOpeningBalance.Decimal
        End If
        'Sohail (12 Dec 2015) -- End
    End Sub

    Private Sub FillCombo()
        Dim dsList As DataSet
        'objEmployeeData = New clsEmployee_Master 'Sohail (06 Jan 2012)
        objPeriodData = New clscommom_period_Tran
        objSavingScheme = New clsSavingScheme
        'SHANI [09 MAR 2015]-START
        'Enhancement - Add Currency Field.
        Dim objExRate As New clsExchangeRate
        'SHANI [09 MAR 2015]--END 
        Try
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployeeData.GetEmployeeList("List", True)
            'cboEmpName.ValueMember = "employeeunkid"
            'cboEmpName.DisplayMember = "employeename"
            'cboEmpName.DataSource = dsList.Tables("List")
            'cboEmpName.SelectedValue = 0
            'Sohail (06 Jan 2012) -- End

            'Sohail (27 Jul 2010) -- Start
            'dsList = objPeriodData.getListForCombo(enModuleRefenence.Payroll, 1, "List", True)
            If menAction = enAction.EDIT_ONE Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, 0, "List", True)
                dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
                dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
                'Sohail (21 Aug 2015) -- End
            End If


            'Sohail (27 Jul 2010) -- End
            'cboPayPeriod.ValueMember = "periodunkid"
            'cboPayPeriod.DisplayMember = "name"
            'cboPayPeriod.DataSource = dsList.Tables("List").Copy()
            'cboPayPeriod.SelectedValue = 0


            'Shani [ 12 JAN 2015 ] -- START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            cboPayPeriod.ValueMember = "periodunkid"
            cboPayPeriod.DisplayMember = "name"
            cboPayPeriod.DataSource = dsList.Tables("List").Copy()
            cboPayPeriod.SelectedValue = 0

            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # : Do not Show closed period on employee saving add edit screen.
            dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (06 Dec 2019) -- End

            cboContPeriod.ValueMember = "periodunkid"
            cboContPeriod.DisplayMember = "name"
            cboContPeriod.DataSource = dsList.Tables("List").Copy()
            cboContPeriod.SelectedValue = 0

            cboRatePeriod.ValueMember = "periodunkid"
            cboRatePeriod.DisplayMember = "name"
            cboRatePeriod.DataSource = dsList.Tables("List").Copy()
            cboRatePeriod.SelectedValue = 0
            'Shani [ 12 JAN 2015 ] -- END


            dsList = objSavingScheme.getComboList(True, "List")
            cboSavingScheme.ValueMember = "savingschemeunkid"
            cboSavingScheme.DisplayMember = "name"
            cboSavingScheme.DataSource = dsList.Tables("List")
            cboSavingScheme.SelectedValue = 0


            'SHANI [09 MAR 2015]-START
            'Enhancement - Add Currency Field.
            dsList = objExRate.getComboList("ExRate", True)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    mintBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid")) 'Sohail (09 Mar 2015)
                End If

                With cboCurrency
                    .ValueMember = "countryunkid"
                    .DisplayMember = "currency_sign"
                    .DataSource = dsList.Tables("ExRate")
                    .SelectedValue = 0
                End With
            End If
            'SHANI [09 MAR 2015]--END 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub
    'Shani [ 12 JAN 2015 ] -- START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    'Private Function IsValid() As Boolean
    '    Try

    '        'Sandeep [ 16 Oct 2010 ] -- Start
    '        'Issue : Auto No. Generation
    '        'If Trim(txtVoucherNo.Text) = "" Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Voucher No. cannot be blank. Voucher No. is required information."), enMsgBoxStyle.Information) '?1
    '        '    txtVoucherNo.Focus()
    '        '    Return False
    '        'End If
    '        If ConfigParameter._Object._SavingsVocNoType = 0 Then
    'If Trim(txtVoucherNo.Text) = "" Then
    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Voucher No. cannot be blank. Voucher No. is required information."), enMsgBoxStyle.Information) '?1
    '    txtVoucherNo.Focus()
    '    Return False
    'End If
    '        End If
    '        'Sandeep [ 16 Oct 2010 ] -- End 

    '        If CInt(cboPayPeriod.SelectedValue) <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Pay Period cannot be blank. Pay Period is required information."), enMsgBoxStyle.Information) '?1
    '            cboPayPeriod.Focus()
    '            Return False
    '        End If

    '        If CInt(cboEmpName.SelectedValue) <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Name cannot be blank. Employee Name is required information."), enMsgBoxStyle.Information) '?1
    '            cboEmpName.Focus()
    '            Return False
    '        End If

    '        If CInt(cboSavingScheme.SelectedValue) <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Saving Scheme cannot be blank. Saving Scheme is required information."), enMsgBoxStyle.Information) '?1
    '            cboSavingScheme.Focus()
    '            Return False
    '        End If


    '        If Trim(txtMonthlyContribution.Text) = "" Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Contribution cannot be blank. Contribution is required information."), enMsgBoxStyle.Information) '?1
    '            txtMonthlyContribution.Focus()
    '            Return False
    '        End If


    '        'If dtpMaturity.Value <= dtpDate.Value Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Maturity date cannot be less than or equal to effective date"), enMsgBoxStyle.Information) '?1
    '        '    dtpMaturity.Focus()
    '        '    Return False
    '        'End If

    '        'If CInt(NudSavingPeriod.Value) < mintMinLoan_Tenure Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Saving Scheme tenure cannot be less than minimum tenure of this scheme."), enMsgBoxStyle.Information) '?1
    '        '    NudSavingPeriod.Focus()
    '        '    Return False
    '        'End If

    '        If CDec(txtMonthlyContribution.Text) < mdecMinContribution Then 'Sohail (11 May 2011)
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Minimum Contribution of employee cannot be less than Minimum Contribution of this scheme."), enMsgBoxStyle.Information) '?1
    '            txtMonthlyContribution.Focus()
    '            Return False
    '        End If

    '        'Sohail (26 Jul 2010) -- Start
    '        If dtpDate.Value > mdtPayPeriodEndDate Or dtpDate.Value < mdtPayPeriodStartDate Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Date should be in between ") & mdtPayPeriodStartDate & _
    '                            Language.getMessage(mstrModuleName, 9, " And ") & mdtPayPeriodEndDate, enMsgBoxStyle.Information)
    '            dtpDate.Focus()
    '            Return False
    '        End If
    '        'Sohail (26 Jul 2010) -- End

    '        Dim objScheme As New clsSavingScheme
    '        objScheme._Savingschemeunkid = CInt(cboSavingScheme.SelectedValue)

    '        If txtMonthlyContribution.Decimal < objScheme._Mincontribution Then 'Sohail (11 May 2011)
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Monthly contribution cannot be less than Minimum contribution. Which is set to ") & " " & objScheme._Mincontribution, enMsgBoxStyle.Information)
    '            txtMonthlyContribution.Focus()
    '            Return False
    '        End If


    '        'Anjan (27 Sep 2010)-Start
    '        'Issue : Ruttas suggesstion not necessary
    '        ' objEmployeeData._Employeeunkid = CInt(cboEmpName.SelectedValue)
    '        'If txtMonthlyContribution.Decimal >= objEmployeeData._Scale Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Monthly contribution cannot be greater than equal to Employee Scale. Which is set to " & " " & objEmployeeData._Scale), enMsgBoxStyle.Information)
    '        '    txtMonthlyContribution.Focus()
    '        '    Return False
    '        'End If
    '        'Anjan (27 Sep 2010)-End

    '        'Anjan (26 Jan 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS


    '        If dtpStopDate.Value.Date < FinancialYear._Object._Database_Start_Date Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set this Stop date. Reason this date is less than current financial year."), enMsgBoxStyle.Information)
    '            Return False
    '        End If

    '        Dim objMasterData As New clsMasterData
    '        Dim intPeriodId As Integer
    '        intPeriodId = objMasterData.getCurrentPeriodID(enModuleReference.Payroll, dtpStopDate.Value.Date)
    '        If intPeriodId > 0 Then
    '            Dim objPeriod As New clscommom_period_Tran
    '            objPeriod._Periodunkid = intPeriodId
    '            If objPeriod._Statusid = enStatusType.Close Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set this Stop date. Reason the period is already closed. Please set new Stop date."), enMsgBoxStyle.Information)
    '                Return False
    '            End If
    '            objPeriod = Nothing
    '        End If
    '        objMasterData = Nothing


    '        'Anjan (26 Jan 2012)-End 

    '        objScheme = Nothing

    '        Return True

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
    '    End Try
    'End Function
    Private Function IsValid() As Boolean
        Try

            If ConfigParameter._Object._SavingsVocNoType = 0 Then
                If Trim(txtVoucherNo.Text) = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Voucher No. cannot be blank. Voucher No. is required information."), enMsgBoxStyle.Information) '?1
                    txtVoucherNo.Focus()
                    Return False
                End If
            End If

            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Pay Period cannot be blank. Pay Period is required information."), enMsgBoxStyle.Information) '?1
                cboPayPeriod.Focus()
                Return False
            End If

            If CInt(cboEmpName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Name cannot be blank. Employee Name is required information."), enMsgBoxStyle.Information) '?1
                cboEmpName.Focus()
                Return False
            End If

            If CInt(cboSavingScheme.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Saving Scheme cannot be blank. Saving Scheme is required information."), enMsgBoxStyle.Information) '?1
                cboSavingScheme.Focus()
                Return False
            End If

            If dtpDate.Value > mdtPayPeriodEndDate Or dtpDate.Value < mdtPayPeriodStartDate Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Date should be between ") & mdtPayPeriodStartDate & _
                                Language.getMessage(mstrModuleName, 9, " And ") & mdtPayPeriodEndDate, enMsgBoxStyle.Information)
                dtpDate.Focus()
                Return False
            End If

            If dtpStopDate.Value.Date < FinancialYear._Object._Database_Start_Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set this Stop date. Reason this date is less than current financial year."), enMsgBoxStyle.Information)
                Return False
            End If

            Dim objMasterData As New clsMasterData
            Dim intPeriodId As Integer

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intPeriodId = objMasterData.getCurrentPeriodID(enModuleReference.Payroll, dtpStopDate.Value.Date)
            intPeriodId = objMasterData.getCurrentPeriodID(enModuleReference.Payroll, dtpStopDate.Value.Date, FinancialYear._Object._YearUnkid)
            'S.SANDEEP [04 JUN 2015] -- END

            If intPeriodId > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = intPeriodId
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodId
                'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set this Stop date. Reason the period is already closed. Please set new Stop date."), enMsgBoxStyle.Information)
                    Return False
                End If
                objPeriod = Nothing
            End If
            objMasterData = Nothing

            'SHANI [09 MAR 2015]-START
            'Enhancement - Add Currency Field.
            If CInt(cboCurrency.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Currency cannot be blank. Currency is required information."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Return False
            End If
            'SHANI [09 MAR 2015]--END 

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            'If txtOpeningBalance.Decimal < txtOpeningContribution.Decimal Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, Opening Balance (Opening Contribution + Opening Interest Amount) cannot be less than Opening Contribution."), enMsgBoxStyle.Information)
            '    txtOpeningBalance.Focus()
            '    Return False
            'End If
            'Sohail (12 Dec 2015) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function
    'Shani [ 12 JAN 2015 ] -- END
    Private Sub SetVisibility()
        Try
            'Sandeep [ 16 Oct 2010 ] -- Start
            'Issue : Auto No. Generation
            If ConfigParameter._Object._SavingsVocNoType = 1 Then
                txtVoucherNo.Enabled = False
            Else
                txtVoucherNo.Enabled = True
            End If
            'Sandeep [ 16 Oct 2010 ] -- End 

            If menAction = enAction.EDIT_ONE Then
                cboSavingScheme.Enabled = False
                dtpDate.Enabled = False
                cboPayPeriod.Enabled = False
                cboEmpName.Enabled = False
                objbtnSearchEmployee.Enabled = False
                objSavingSchemeSearch.Enabled = False 'Sohail (07 Jul 2014)
                'SHANI [09 MAR 2015]-START
                'Enhancement - Add Currency Field.
                cboCurrency.Enabled = False
                'SHANI [09 MAR 2015]--END
            Else
                cboSavingScheme.Enabled = True
                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                dtpDate.Enabled = True
                'SHANI [12 JAN 2015]--END 
                cboPayPeriod.Enabled = True
                cboEmpName.Enabled = True
                objbtnSearchEmployee.Enabled = True
                objSavingSchemeSearch.Enabled = True 'Sohail (07 Jul 2014)
                'SHANI [09 MAR 2015]-START
                'Enhancement - Add Currency Field.
                cboCurrency.Enabled = True
                'SHANI [09 MAR 2015]--END
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    'Sohail (06 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate)

            'S.SANDEEP [18 JAN 2016] -- START
            'Issue : TRA Employee Saving Issue : Due to Access
            'dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                                      User._Object._Userunkid, _
            '                                      FinancialYear._Object._YearUnkid, _
            '                                      Company._Object._Companyunkid, _
            '                                      mdtPayPeriodStartDate, _
            '                                      mdtPayPeriodEndDate, _
            '                                      ConfigParameter._Object._UserAccessModeSetting, True, _
            '                                      ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            Dim blnAppyAccessFilter As Boolean = True
            If menAction = enAction.EDIT_ONE Then
                blnAppyAccessFilter = False
            End If
            dsCombo = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                  User._Object._Userunkid, _
                                                  FinancialYear._Object._YearUnkid, _
                                                  Company._Object._Companyunkid, _
                                                  mdtPayPeriodStartDate, _
                                                  mdtPayPeriodEndDate, _
                                                  ConfigParameter._Object._UserAccessModeSetting, True, _
                                                  ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True, , , , , , , , , , , , , , , , , blnAppyAccessFilter)
            'S.SANDEEP [18 JAN 2016] -- START



            'Shani(24-Aug-2015) -- End
            With cboEmpName
                .BeginUpdate()
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Employee")
                .SelectedValue = 0
                .EndUpdate()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try

    End Sub
    'Sohail (06 Jan 2012) -- End

    'Shani [ 12 JAN 2015 ] -- START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    Private Sub Fill_Contribution_Data()
        Try
            Dim dvTran As DataView = mdtContribution.DefaultView
            dvTran.RowFilter = "AUD <> 'D'"
            dvTran.Sort = "contributiondate DESC"
            lvContributionData.Items.Clear()
            For Each Item As DataRowView In dvTran
                Dim lvItem As New ListViewItem
                lvItem.Text = Item("period_name").ToString
                lvItem.SubItems.Add(Format(Item("contribution"), GUI.fmtCurrency))
                lvItem.SubItems.Add(Item("savingcontributiontranunkid").ToString)
                lvItem.SubItems.Add(Item("GUID").ToString)
                lvContributionData.Items.Add(lvItem)
            Next
            If lvContributionData.Items.Count > 0 Then
                cboContPeriod.Enabled = True
                cboContPeriod.SelectedValue = 0
            Else
                cboContPeriod.Enabled = False
                cboContPeriod.SelectedValue = cboPayPeriod.SelectedValue
            End If

            If lvContributionData.Items.Count > 5 Then
                colhContribution.Width = 120 - 18
            Else
                colhContribution.Width = 120
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Contribution_Data", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_IntRate_Date()
        Try
            Dim dvTran As DataView = mdtIntRate.DefaultView
            dvTran.RowFilter = "AUD <> 'D'"
            dvTran.Sort = "effectivedate DESC"
            lvInterestRateData.Items.Clear()
            For Each Item As DataRowView In dvTran
                Dim lvItem As New ListViewItem
                lvItem.Text = Item("period_name").ToString
                lvItem.SubItems.Add(CDate(Item("effectivedate")).ToShortDateString)
                lvItem.SubItems.Add(Format(Item("interest_rate"), GUI.fmtCurrency))
                lvItem.SubItems.Add(Item("savinginterestratetranunkid").ToString)
                lvItem.SubItems.Add(Item("GUID").ToString)
                lvInterestRateData.Items.Add(lvItem)
            Next
            If lvInterestRateData.Items.Count > 0 Then
                cboRatePeriod.Enabled = True
                cboRatePeriod.SelectedValue = 0
            Else
                cboRatePeriod.Enabled = False
                cboRatePeriod.SelectedValue = cboPayPeriod.SelectedValue
            End If

            If lvInterestRateData.Items.Count > 5 Then
                colhEffectiveDate.Width = 100 - 18
            Else
                colhEffectiveDate.Width = 100
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_IntRate_Date", mstrModuleName)
        End Try
    End Sub

    Private Sub Control_Enabled_Disbled(ByVal blnEnable As Boolean)
        Try
            cboEmpName.Enabled = blnEnable
            cboSavingScheme.Enabled = blnEnable
            cboPayPeriod.Enabled = blnEnable
            dtpDate.Enabled = blnEnable
            objSavingSchemeSearch.Enabled = blnEnable
            objbtnSearchEmployee.Enabled = blnEnable

            'SHANI [09 MAR 2015]-START
            'Enhancement - Add Currency Field.
            cboCurrency.Enabled = blnEnable
            'SHANI [09 MAR 2015]--END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Control_Enabled_Disbled", mstrModuleName)
        End Try
    End Sub
    'Shani [ 12 JAN 2015 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmEmployeeSaving_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objEmployeeSaving = Nothing
        objInterestRate = Nothing
    End Sub

    Private Sub frmEmployeeSaving_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmEmployeeSaving_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmEmployeeSaving_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmployeeSaving = New clsSaving_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()
            Call setColor()

            If menAction = enAction.EDIT_ONE Then
                objEmployeeSaving._Savingtranunkid = mintSavingtranunkid
                'Sohail (12 Dec 2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                txtOpeningContribution.Enabled = False
                txtOpeningBalance.Enabled = False
            Else
                txtOpeningContribution.Enabled = True
                txtOpeningBalance.Enabled = True
                'Sohail (12 Dec 2015) -- End
            End If
            Call FillCombo()
            Call GetValue()
            'Shani [ 12 JAN 2015 ] -- START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            objSavingContribution._Savingtranunkid = mintSavingtranunkid
            mdtContribution = objSavingContribution._DataTable

            objSavingInterestRate._Savingtranunkid = mintSavingtranunkid
            mdtIntRate = objSavingInterestRate._DataTable

            Call Fill_Contribution_Data()
            Call Fill_IntRate_Date()
            'NudSavingPeriod.Minimum = 1
            'Shani [ 12 JAN 2015 ] -- END
            txtVoucherNo.Focus()
            mblnFormload = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeSaving_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsSaving_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsSaving_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValid() = False Then Exit Sub

            'Shani [ 12 JAN 2015 ] -- START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            If mdtContribution.Select("AUD <> 'D'").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Please add atleast one Contribution."), enMsgBoxStyle.Information)
                txtMonthlyContribution.Focus()
                Exit Sub
            ElseIf mdtContribution.Select("periodunkid = '" & CInt(cboPayPeriod.SelectedValue) & "' AND AUD <> 'D'").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Plase add atleast one contribution for effective period."), enMsgBoxStyle.Information)
                cboContPeriod.SelectedValue = cboPayPeriod.SelectedValue
                txtMonthlyContribution.Focus()
                Exit Sub
                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            Else
                If menAction <> enAction.EDIT_ONE Then
                    Dim objTnALeaveTran As New clsTnALeaveTran
                    If objTnALeaveTran.IsPayrollProcessDone(CInt(cboPayPeriod.SelectedValue), cboEmpName.SelectedValue.ToString, eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Date, enModuleReference.Payroll) = True Then
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, You can not Save this Saving Scheme. Reason : Process Payroll is already done for last date of selected Period."), enMsgBoxStyle.Information)
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, You can not Save this Saving Scheme. Reason : Process Payroll is already done for last date of selected Period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 31, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                        Exit Try
                    End If
                End If
                'Sohail (12 Jan 2015) -- End
            End If

            If mdtIntRate.Select("AUD <> 'D'").Length <= 0 Then
                cboRatePeriod.SelectedValue = cboPayPeriod.SelectedValue
                txtRate.Text = CStr(txtRate.Tag) : Call btnRateAdd_Click(btnRateAdd, Nothing)
            ElseIf mdtIntRate.Select("periodunkid = '" & CInt(cboPayPeriod.SelectedValue) & "' AND AUD <> 'D'").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Plase add atleast one Interest Rate for effective period."), enMsgBoxStyle.Information)
                cboRatePeriod.SelectedValue = cboPayPeriod.SelectedValue
                txtRate.Focus()
                Exit Sub
            End If
            'Shani [ 12 JAN 2015 ] -- END

            Call SetValue()

            With objEmployeeSaving
                ._FormName = mstrModuleName
                ._LoginEmployeeUnkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With

            If menAction = enAction.EDIT_ONE Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objEmployeeSaving.Update(, mdtContribution, mdtIntRate)
                'Sohail (12 Dec 2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                If mdtContribution.Rows.Count > 0 Then
                    objEmployeeSaving._Contribution = CDec(mdtContribution.Rows(0).Item("contribution"))
                End If
                If mdtIntRate.Rows.Count > 0 Then
                    objEmployeeSaving._Interest_Rate = CDec(mdtIntRate.Rows(0).Item("interest_rate"))
                End If
                'Sohail (12 Dec 2015) -- End
                blnFlag = objEmployeeSaving.Update(ConfigParameter._Object._CurrentDateAndTime, , mdtContribution, mdtIntRate)
                'Shani(24-Aug-2015) -- End

            Else

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objEmployeeSaving.Insert(, , mdtContribution, mdtIntRate)
                'Sohail (12 Dec 2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                If mdtContribution.Rows.Count > 0 Then
                    objEmployeeSaving._Contribution = CDec(mdtContribution.Rows(0).Item("contribution"))
                End If
                If mdtIntRate.Rows.Count > 0 Then
                    objEmployeeSaving._Interest_Rate = CDec(mdtIntRate.Rows(0).Item("interest_rate"))
                End If
                'Sohail (12 Dec 2015) -- End
                blnFlag = objEmployeeSaving.Insert(ConfigParameter._Object._CurrentDateAndTime, , , mdtContribution, mdtIntRate)
                'Shani(24-Aug-2015) -- End

            End If

            If blnFlag = False And objEmployeeSaving._Message <> "" Then
                eZeeMsgBox.Show(objEmployeeSaving._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objEmployeeSaving = Nothing
                    objEmployeeSaving = New clsSaving_Tran
                    Call GetValue()
                    txtVoucherNo.Focus()
                    'SHANI [12 JAN 2015]-START
                    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                    Call Control_Enabled_Disbled(True)
                    mdtContribution.Rows.Clear()
                    mdtIntRate.Rows.Clear()
                    Call Fill_Contribution_Data()
                    Call Fill_IntRate_Date()
                    'SHANI [12 JAN 2015]--END 

                Else
                    mintSavingtranunkid = objEmployeeSaving._Savingtranunkid
                    Me.Close()


                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    'Shani [ 12 JAN 2015 ] -- START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    Private Sub btnContributionAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContributionAdd.Click
        Try
            If IsValid() = False Then Exit Sub
            'SHANI [09 MAR 2015]-START
            'Enhancement - Add Currency Field.
            'Call Control_Enabled_Disbled(False)
            'SHANI [09 MAR 2015]--END

            If CInt(cboContPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Period cannot be blank. Period is required information."), enMsgBoxStyle.Information) '?1
                cboContPeriod.Focus()
                Exit Sub
            ElseIf txtMonthlyContribution.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Contribution should be greater than zero."), enMsgBoxStyle.Information) '?1
                txtMonthlyContribution.Focus()
                Exit Sub
            ElseIf CDec(txtMonthlyContribution.Decimal) < mdecMinContribution Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Minimum Contribution of employee cannot be less than Minimum Contribution of this scheme."), enMsgBoxStyle.Information) '?1
                txtMonthlyContribution.Focus()
                Exit Sub
            End If

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboContPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboContPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), enMsgBoxStyle.Information)
                Exit Sub
            ElseIf mdtContribution.Select("periodunkid=" & CInt(cboContPeriod.SelectedValue) & " AND AUD <> 'D'").Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, Contribution for the selected period is already given."), enMsgBoxStyle.Information) '?1
                cboContPeriod.Focus()
                Exit Sub
            ElseIf CDate(mdtPayPeriodEndDate) > CDate(objPeriod._End_Date) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, Contribution period should not be less than effective period."), enMsgBoxStyle.Information) '?1
                cboContPeriod.Focus()
                Exit Sub
            End If

            'Sohail (12 Jan 2015) -- Start
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            Dim objTnALeaveTran As New clsTnALeaveTran
            If objTnALeaveTran.IsPayrollProcessDone(CInt(cboContPeriod.SelectedValue), cboEmpName.SelectedValue.ToString, eZeeDate.convertDate(CType(cboContPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Date, enModuleReference.Payroll) = True Then
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, You can not Add / Edit / Delete this Contribution. Reason : Process Payroll is already done for last date of selected period."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, You can not Add / Edit / Delete this Contribution. Reason : Process Payroll is already done for last date of selected period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 31, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                Exit Try
            End If
            'Sohail (12 Jan 2015) -- End

            'SHANI [09 MAR 2015]-START
            'Enhancement - Add Currency Field.
            Call Control_Enabled_Disbled(False)
            'SHANI [09 MAR 2015]--END

            Dim dRow As DataRow = mdtContribution.NewRow
            dRow.Item("savingcontributiontranunkid") = -1
            dRow.Item("savingtranunkid") = mintSavingtranunkid
            dRow.Item("period_name") = cboContPeriod.Text
            dRow.Item("periodunkid") = CInt(cboContPeriod.SelectedValue)
            dRow.Item("contribution") = txtMonthlyContribution.Decimal
            dRow.Item("contributiondate") = CStr(CDate(objPeriod._End_Date))
            dRow.Item("userunkid") = User._Object._Userunkid
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            mdtContribution.Rows.Add(dRow) : Call Fill_Contribution_Data()
            cboContPeriod.SelectedValue = 0 : txtMonthlyContribution.Decimal = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnContributionAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnContributionEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContributionEdit.Click
        Try
            If txtMonthlyContribution.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Contribution should be greater than zero."), enMsgBoxStyle.Information) '?1
                txtMonthlyContribution.Focus()
                Exit Sub
            ElseIf CDec(txtMonthlyContribution.Decimal) < mdecMinContribution Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, Contribution should not be less than Minimum Contribution of selected scheme."), enMsgBoxStyle.Information) '?1
                txtMonthlyContribution.Focus()
                Exit Sub
            End If
            If drowcontribution IsNot Nothing Then
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(drowcontribution(0).Item("periodunkid"))
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(drowcontribution(0).Item("periodunkid"))
                'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, Period is already closed. Please select open Period."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Dim objTnaLeaveTran As New clsTnALeaveTran
                If objTnaLeaveTran.IsPayrollProcessDone(CInt(cboContPeriod.SelectedValue), CStr(cboEmpName.SelectedValue), objPeriod._End_Date, enModuleReference.Payroll) Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, You can not Add / Edit / Delete this Contribution. Reason : Process Payroll is already done for last date of selected period."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, You can not Add / Edit / Delete this Contribution. Reason : Process Payroll is already done for last date of selected period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 31, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    Exit Sub
                End If

                drowcontribution(0).Item("savingcontributiontranunkid") = drowcontribution(0).Item("savingcontributiontranunkid")
                drowcontribution(0).Item("savingtranunkid") = mintSavingtranunkid
                drowcontribution(0).Item("period_name") = cboContPeriod.Text
                drowcontribution(0).Item("periodunkid") = CInt(cboContPeriod.SelectedValue)
                drowcontribution(0).Item("contribution") = txtMonthlyContribution.Decimal
                drowcontribution(0).Item("userunkid") = User._Object._Userunkid
                drowcontribution(0).Item("isvoid") = False
                drowcontribution(0).Item("voiduserunkid") = -1
                drowcontribution(0).Item("voiddatetime") = DBNull.Value
                drowcontribution(0).Item("voidreason") = ""
                If drowcontribution(0).Item("AUD").ToString.Trim = "" Then
                    drowcontribution(0).Item("AUD") = "U"
                End If
                drowcontribution(0).Item("GUID") = Guid.NewGuid.ToString

                mdtContribution.AcceptChanges() : Call Fill_Contribution_Data()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnContributionEdit_Click", mstrModuleName)
        Finally
            cboContPeriod.SelectedValue = 0 : txtMonthlyContribution.Decimal = 0
            btnContributionEdit.Visible = False : btnContributionAdd.Visible = True
            cboContPeriod.Enabled = True : drowcontribution = Nothing
        End Try

    End Sub

    Private Sub btnRateAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRateAdd.Click
        Try

            If IsValid() = False Then Exit Sub
            Call Control_Enabled_Disbled(False)

            If CInt(cboRatePeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Period cannot be blank. Period is required information."), enMsgBoxStyle.Information) '?1
                cboRatePeriod.Focus()
                Exit Sub
            ElseIf txtRate.Decimal < 0 OrElse txtRate.Decimal > 100 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Interest Rate should be between 0 and 100."), enMsgBoxStyle.Information) '?1
                txtRate.Focus()
                Exit Sub
            ElseIf CDate(mdtPayPeriodEndDate) > CDate(dtpRateDate.MaxDate) Then '<TODO put same validation in SelfService>
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry, Interest Rate period should not be less than effective period."), enMsgBoxStyle.Information) '?1
                cboRatePeriod.Focus()
                Exit Sub
            End If

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboRatePeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboRatePeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            If objPeriod._Statusid = enStatusType.Close Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), enMsgBoxStyle.Information)
                Exit Sub
            ElseIf mdtIntRate.Select("effectivedate ='" & CDate(dtpRateDate.Value) & "' AND AUD <> 'D'").Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, you cannot add same Date Interest Rate again in the below list."), enMsgBoxStyle.Information) '?1
                cboRatePeriod.Focus()
                Exit Sub
            End If

            Dim dRow As DataRow = mdtIntRate.NewRow
            dRow.Item("savinginterestratetranunkid") = -1
            dRow.Item("savingtranunkid") = mintSavingtranunkid
            dRow.Item("period_name") = cboRatePeriod.Text
            dRow.Item("periodunkid") = CInt(cboRatePeriod.SelectedValue)
            dRow.Item("effectivedate") = CDate(dtpRateDate.Value)
            dRow.Item("interest_rate") = txtRate.Decimal
            dRow.Item("userunkid") = User._Object._Userunkid
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            mdtIntRate.Rows.Add(dRow) : Call Fill_IntRate_Date()
            cboRatePeriod.SelectedValue = 0 : txtRate.Decimal = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnRateAdd_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnRateEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRateEdit.Click
        Try
            If txtRate.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Interest Rate cannot be blank. Interest Rate is required information."), enMsgBoxStyle.Information) '?1
                txtRate.Focus()
                Exit Sub
            ElseIf txtRate.Decimal < 0 OrElse txtRate.Decimal > 100 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Interest Rate should be between 0 and 100."), enMsgBoxStyle.Information) '?1
                txtRate.Focus()
                Exit Sub
            End If

            If dRowIntRate IsNot Nothing Then

                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(dRowIntRate(0).Item("periodunkid"))
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dRowIntRate(0).Item("periodunkid"))
                'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), enMsgBoxStyle.Information)
                    dRowIntRate = Nothing
                    Exit Sub
                End If

                'Dim objTnaLeaveTran As New clsTnALeaveTran
                'If objTnaLeaveTran.IsPayrollProcessDone(CInt(dRowIntRate(0).Item("periodunkid")), CStr(cboEmpName.SelectedValue), objPeriod._End_Date) Then
                '    eZeeMsgBox.Show("Sorry,PayRoll Process is DONE. can not Edit/Delete Record ", enMsgBoxStyle.Information)
                '    dRowIntRate = Nothing
                '    Exit Sub
                'End If

                dRowIntRate(0).Item("savinginterestratetranunkid") = dRowIntRate(0).Item("savinginterestratetranunkid")
                dRowIntRate(0).Item("savingtranunkid") = mintSavingtranunkid
                dRowIntRate(0).Item("period_name") = cboRatePeriod.Text
                dRowIntRate(0).Item("periodunkid") = CInt(cboRatePeriod.SelectedValue)
                dRowIntRate(0).Item("interest_rate") = txtRate.Decimal
                dRowIntRate(0).Item("effectivedate") = CDate(dtpRateDate.Value.Date).Date
                dRowIntRate(0).Item("userunkid") = User._Object._Userunkid
                dRowIntRate(0).Item("isvoid") = False
                dRowIntRate(0).Item("voiduserunkid") = -1
                dRowIntRate(0).Item("voiddatetime") = DBNull.Value
                dRowIntRate(0).Item("voidreason") = ""
                If dRowIntRate(0).Item("AUD").ToString.Trim = "" Then
                    dRowIntRate(0).Item("AUD") = "U"
                End If
                dRowIntRate(0).Item("GUID") = Guid.NewGuid.ToString
                mdtIntRate.AcceptChanges() : Call Fill_IntRate_Date()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnRateEdit_Click", mstrModuleName)
        Finally
            cboRatePeriod.SelectedValue = 0 : txtRate.Decimal = 0 : cboRatePeriod.SelectedValue = 0
            btnRateEdit.Visible = False : btnRateAdd.Visible = True
            cboRatePeriod.Enabled = True : dRowIntRate = Nothing
        End Try
    End Sub

    Private Sub btnContDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContDelete.Click
        Try
            If drowcontribution IsNot Nothing Then

                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboContPeriod.SelectedValue)
                'Sohail (06 Dec 2019) -- Start
                'NMB UAT Enhancement # : Do not Show closed period on employee saving add edit screen.
                'objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboContPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(drowcontribution(0).Item("periodunkid"))
                'Sohail (06 Dec 2019) -- End
                'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), enMsgBoxStyle.Information)
                    drowcontribution = Nothing
                    Exit Sub
                End If
                Dim objTnaLeaveTran As New clsTnALeaveTran
                If objTnaLeaveTran.IsPayrollProcessDone(CInt(cboContPeriod.SelectedValue), CStr(cboEmpName.SelectedValue), objPeriod._End_Date, enModuleReference.Payroll) Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, You can not Add / Edit / Delete this Contribution. Reason : Process Payroll is already done for last date of selected period."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, You can not Add / Edit / Delete this Contribution. Reason : Process Payroll is already done for last date of selected period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 31, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    Exit Sub
                End If
                'Sohail (12 Jan 2015) -- End

                If CInt(drowcontribution(0).Item("savingcontributiontranunkid")) > 0 Then
                    Dim iVoidReason As String = String.Empty
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    frm.displayDialog(enVoidCategoryType.SAVINGS, iVoidReason)
                    If iVoidReason.Trim.Length <= 0 Then Exit Sub
                    drowcontribution(0).Item("isvoid") = True
                    drowcontribution(0).Item("voiduserunkid") = User._Object._Userunkid
                    drowcontribution(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    drowcontribution(0).Item("voidreason") = iVoidReason
                    drowcontribution(0).Item("AUD") = "D"
                Else
                    drowcontribution(0).Item("AUD") = "D"
                End If
                mdtContribution.AcceptChanges()
                Call Fill_Contribution_Data()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnContDelete_Click", mstrModuleName)
        Finally
            cboContPeriod.SelectedValue = 0 : txtMonthlyContribution.Decimal = 0
            btnContributionEdit.Visible = False : btnContributionAdd.Visible = True
            cboContPeriod.Enabled = True : drowcontribution = Nothing
        End Try
    End Sub

    Private Sub btnRateDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRateDelete.Click
        Try
            If dRowIntRate IsNot Nothing Then

                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = CInt(cboRatePeriod.SelectedValue)
                'Sohail (06 Dec 2019) -- Start
                'NMB UAT Enhancement # : Do not Show closed period on employee saving add edit screen.
                'objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboContPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(dRowIntRate(0).Item("periodunkid"))
                'Sohail (06 Dec 2019) -- End
                'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = enStatusType.Close Then
                    eZeeMsgBox.Show("Sorry, Period is already closed. Please select open Period.", enMsgBoxStyle.Information)
                    Exit Sub
                End If
                Dim objTnaLeaveTran As New clsTnALeaveTran
                If objTnaLeaveTran.IsPayrollProcessDone(CInt(cboRatePeriod.SelectedValue), CStr(cboEmpName.SelectedValue), objPeriod._End_Date) Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, You can not Add / Edit / Delete this Rate. Reason : Process Payroll is already done for last date of selected period."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, You can not Add / Edit / Delete this Rate. Reason : Process Payroll is already done for last date of selected period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 31, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    Exit Sub
                End If
                'Sohail (12 Jan 2015) -- End

                If CInt(dRowIntRate(0).Item("savinginterestratetranunkid")) > 0 Then
                    Dim iVoidReason As String = String.Empty
                    Dim frm As New frmReasonSelection
                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    frm.displayDialog(enVoidCategoryType.SAVINGS, iVoidReason)
                    If iVoidReason.Trim.Length <= 0 Then Exit Sub
                    dRowIntRate(0).Item("isvoid") = True
                    dRowIntRate(0).Item("voiduserunkid") = User._Object._Userunkid
                    dRowIntRate(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    dRowIntRate(0).Item("voidreason") = iVoidReason
                    dRowIntRate(0).Item("AUD") = "D"
                Else
                    dRowIntRate(0).Item("AUD") = "D"
                End If
                mdtIntRate.AcceptChanges()
                Call Fill_IntRate_Date()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnContDelete_Click", mstrModuleName)
        Finally
            cboRatePeriod.SelectedValue = 0 : txtRate.Decimal = 0
            btnRateEdit.Visible = False : btnRateAdd.Visible = True
            cboRatePeriod.Enabled = True : dRowIntRate = Nothing
        End Try
    End Sub
    'Shani [ 12 JAN 2015 ] -- END

#End Region

#Region " ComboBox's Events "
    Private Sub cboSavingScheme_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSavingScheme.SelectedIndexChanged
        Try
            objSavingScheme._Savingschemeunkid = CInt(cboSavingScheme.SelectedValue)
            'mintMinLoan_Tenure = objSavingScheme._Mintenure
            mdecMinContribution = objSavingScheme._Mincontribution
            'Shani [ 12 JAN 2015 ] -- START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            txtMonthlyContribution.Text = Format(objSavingScheme._DefualtContribution, GUI.fmtCurrency)
            txtRate.Text = Format(objSavingScheme._DefualtIntRate, GUI.fmtCurrency)
            txtRate.Tag = Format(objSavingScheme._DefualtIntRate, GUI.fmtCurrency)
            'Shani [ 12 JAN 2015 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSavingScheme_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub

    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date


            'Shani [ 12 JAN 2015 ] -- START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                'Sohail (12 Dec 2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                'dtpDate.Value = mdtPayPeriodEndDate.Date
                dtpDate.Value = mdtPayPeriodStartDate.Date
                'Sohail (12 Dec 2015) -- End
                cboContPeriod.SelectedValue = cboPayPeriod.SelectedValue
                cboRatePeriod.SelectedValue = cboPayPeriod.SelectedValue
                cboContPeriod.Enabled = False
                cboRatePeriod.Enabled = False
            End If
            'Shani [ 12 JAN 2015 ] -- END

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            Call FillEmployeeCombo()
            'Sohail (06 Jan 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    'Shani [ 12 JAN 2015 ] -- START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.

    Private Sub cboRatePeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRatePeriod.SelectedIndexChanged
        Try
            Dim objperiod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objperiod._Periodunkid = CInt(cboRatePeriod.SelectedValue)
            objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboRatePeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End

            dtpRateDate.MinDate = CDate(eZeeDate.convertDate("17530101")).Date
            dtpRateDate.MaxDate = CDate(eZeeDate.convertDate("99981231")).Date

            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            'dtpRateDate.MaxDate = objperiod._End_Date
            'dtpRateDate.MinDate = objperiod._Start_Date
            'dtpRateDate.Value = objperiod._End_Date
            If CInt(cboRatePeriod.SelectedValue) > 0 Then
                dtpRateDate.MaxDate = objperiod._End_Date
                dtpRateDate.MinDate = objperiod._Start_Date
                dtpRateDate.Value = objperiod._Start_Date
            End If
            'Sohail (12 Dec 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    'Shani [ 12 JAN 2015 ] -- END


    'SHANI [09 MAR 2015]-START
    'Enhancement - Add Currency Field.

    Private Sub cboCurrency_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedValueChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExRate.Text = ""
            If CInt(cboCurrency.SelectedValue) <= 0 Then Exit Sub
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpDate.Value, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

    'SHANI [09 MAR 2015]--END 

#End Region

#Region " Textbox's Events "
    Private Sub txtMonthlyContribution_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMonthlyContribution.LostFocus

        Try
            'Call MaturityAmount_Calculation()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtMonthlyContribution_LostFocus", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Other Control's Events "

    'Sohail (24 Jul 2010) - Start
    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        'Sohail (06 Jan 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objEmployee As New clsEmployee_Master
        'Dim dsList As DataSet
        'Sohail (06 Jan 2012) -- End
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("EmployeeList")
            'Sohail (06 Jan 2012) -- End
            With cboEmpName
                'Sohail (06 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'objfrm.DataSource = dsList.Tables("EmployeeList")
                objfrm.DataSource = CType(cboEmpName.DataSource, DataTable)
                'Sohail (06 Jan 2012) -- End
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            'objEmployee = Nothing 'Sohail (06 Jan 2012
        End Try
    End Sub
    'Sohail (24 Jul 2010) - End
    'Anjan (30 Jan 2012)-Start
    'ENHANCEMENT : TRA COMMENTS
    Private Sub objSavingSchemeSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSavingSchemeSearch.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            With cboSavingScheme
                objFrm.DataSource = CType(cboSavingScheme.DataSource, DataTable)
                objFrm.ValueMember = .ValueMember
                objFrm.DisplayMember = .DisplayMember
                objFrm.CodeMember = "code"
            End With
            If objFrm.DisplayDialog Then
                cboSavingScheme.SelectedValue = objFrm.SelectedValue
                cboSavingScheme.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objSavingSchemeSearch_Click", mstrModuleName)
        End Try
    End Sub
    'Anjan (30 Jan 2012)-End 

    'Shani [ 12 JAN 2015 ] -- START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    Private Sub lvContributionData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvContributionData.SelectedIndexChanged
        Dim mintItemIndex As Integer = -1
        Try
            If lvContributionData.SelectedItems.Count > 0 Then
                mintItemIndex = lvContributionData.SelectedItems(0).Index
                If CInt(lvContributionData.Items(mintItemIndex).SubItems(objColhContMasterId.Index).Text) > 0 Then
                    drowcontribution = mdtContribution.Select("savingcontributiontranunkid = '" & CStr(lvContributionData.Items(mintItemIndex).SubItems(objColhContMasterId.Index).Text) & "'")
                ElseIf CStr(lvContributionData.Items(mintItemIndex).SubItems(objcolhContGuid.Index).Text) <> "" Then
                    drowcontribution = mdtContribution.Select("GUID = '" & CStr(lvContributionData.Items(mintItemIndex).SubItems(objcolhContGuid.Index).Text) & "'")
                End If
                If drowcontribution.Length > 0 Then
                    cboContPeriod.SelectedValue = drowcontribution(0).Item("periodunkid")
                    txtMonthlyContribution.Text = Format(drowcontribution(0).Item("contribution"), GUI.fmtCurrency)
                    cboContPeriod.Enabled = False : btnContributionAdd.Visible = False : btnContributionEdit.Visible = True
                End If

            Else
                cboContPeriod.SelectedValue = 0 : txtMonthlyContribution.Decimal = 0
                btnContributionEdit.Visible = False : btnContributionAdd.Visible = True
                cboContPeriod.Enabled = True : drowcontribution = Nothing
            End If
        Catch ex As Exception

            DisplayError.Show("-1", ex.Message, "lvContributionData_SelectedIndexChanged", mstrModuleName)

        End Try
    End Sub

    Private Sub lvInterestRateData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvInterestRateData.SelectedIndexChanged
        Dim mintItemIndex As Integer = -1
        Try
            If lvInterestRateData.SelectedItems.Count > 0 Then
                mintItemIndex = lvInterestRateData.SelectedItems(0).Index
                If mintItemIndex > -1 Then
                    If CInt(lvInterestRateData.Items(mintItemIndex).SubItems(objcolhRateMasterid.Index).Text) > 0 Then
                        dRowIntRate = mdtIntRate.Select("savinginterestratetranunkid = '" & CStr(lvInterestRateData.Items(mintItemIndex).SubItems(objcolhRateMasterid.Index).Text) & "'")
                    ElseIf CStr(lvInterestRateData.Items(mintItemIndex).SubItems(objcolhRateGuid.Index).Text) <> "" Then
                        dRowIntRate = mdtIntRate.Select("GUID = '" & CStr(lvInterestRateData.Items(mintItemIndex).SubItems(objcolhRateGuid.Index).Text) & "'")
                    End If

                    If dRowIntRate.Length > 0 Then
                        cboRatePeriod.SelectedValue = dRowIntRate(0).Item("periodunkid")
                        txtRate.Text = Format(dRowIntRate(0).Item("interest_rate"), GUI.fmtCurrency)
                        dtpRateDate.Value = CDate(dRowIntRate(0).Item("effectivedate"))
                        cboRatePeriod.Enabled = False : btnRateAdd.Visible = False : btnRateEdit.Visible = True
                    End If
                End If
            Else
                cboRatePeriod.SelectedValue = 0 : txtRate.Decimal = 0
                btnRateEdit.Visible = False : btnRateAdd.Visible = True
                cboRatePeriod.Enabled = True : dRowIntRate = Nothing
            End If
        Catch ex As Exception

            DisplayError.Show("-1", ex.Message, "lvInterestRateData_SelectedIndexChanged", mstrModuleName)

        End Try
    End Sub
    'Shani [ 12 JAN 2015 ] -- END

    'Sohail (12 Dec 2015) -- Start
    'Enhancement - Provide Deposit feaure in Employee Saving.
    Private Sub dtpDate_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDate.Validated
        Try
            dtpRateDate.Value = dtpDate.Value
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpDate_Validated", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Dec 2015) -- End

#End Region

    'Private Sub MaturityAmount_Calculation()
    '    Dim interest As Double = 0
    '    Dim cnt As Integer = 1
    '    Dim principal As Double = 0
    '    Dim rate As Double = 0
    '    Dim tenure As Integer = 0
    '    Dim months As Integer = 0
    '    Dim dblYearsUpto As Double = 0
    '    Dim intprincipal As Double = 0
    '    objInterestRate = New clsInterestRate_tran

    '    Try
    '        intprincipal = cdec(txtMonthlyContribution.Text) * 12

    '        dblYearsUpto = cdec(NudSavingPeriod.Value / 12)

    '        objInterestRate.GetInterestRate(CInt(cboSavingScheme.SelectedValue), dblYearsUpto)

    '        rate = cdec(objInterestRate._Rate)

    '        tenure = CInt(NudSavingPeriod.Value)

    '        While tenure > 0
    '            If tenure > 12 Then
    '                months = 12
    '            Else
    '                months = tenure
    '            End If

    '            principal = intprincipal * cnt ' cnt will be mutiplied with principal amount of 12 months 
    '            interest = (principal * rate * months) / (100 * 12) + interest
    '            tenure = tenure - 12

    '            cnt += 1

    '        End While
    '        If NudSavingPeriod.Value > 12 Then
    '            mdblMaturityAmount = principal + interest
    '        Else
    '            mdblMaturityAmount = (cdec(txtMonthlyContribution.Text) * months) + interest
    '        End If

    '        txtMaturityAmt.Text = CStr(mdblMaturityAmount)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    End Try

    'End Sub

    'Private Sub txtMonthlyContribution_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMonthlyContribution.LostFocus

    '    Try
    '        Call MaturityAmount_Calculation()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtMonthlyContribution_LostFocus", mstrModuleName)
    '    End Try

    'End Sub


    'Private Sub NumericUpDown_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NudSavingPeriod.ValueChanged

    '    Try
    '        If mblnFormload = True Then
    '            Dim dtdate As DateTime = dtpDate.Value
    '            dtdate = dtdate.AddMonths(CInt(NudSavingPeriod.Value))
    '            dtpMaturity.Value = dtdate

    '            Call MaturityAmount_Calculation()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "NumericUpDown_ValueChanged", mstrModuleName)
    '    End Try
    'End Sub
#Region " Code before redesign Saving on 12 Jan 2015 "
    'Public Class frmEmployeeSavings_AddEdit

    '#Region " Private Variables "
    '    Private ReadOnly mstrModuleName As String = "frmEmployeeSavings_AddEdit"

    '    Private mblnCancel As Boolean = True

    '    Private objEmployeeSaving As clsSaving_Tran

    '    Private menAction As enAction = enAction.ADD_ONE
    '    Private mintSavingtranunkid As Integer = -1
    '    Private objEmployeeData As clsEmployee_Master
    '    Private objSavingScheme As clsSavingScheme
    '    Private objPeriodData As clscommom_period_Tran
    '    Private mintMinLoan_Tenure As Integer = -1
    '    Private mdecMinContribution As Decimal = 0 'Sohail (11 May 2011)
    '    Private mdecMaturityAmount As Decimal = 0 'Sohail (11 May 2011)
    '    Private mblnFormload As Boolean = False
    '    Dim objInterestRate As clsInterestRate_tran

    '    Private mdtPayPeriodStartDate As Date = Nothing
    '    Private mdtPayPeriodEndDate As Date = Nothing
    '#End Region

    '#Region " Display Dialog "
    '    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
    '        Try
    '            mintSavingtranunkid = intUnkId
    '            menAction = eAction

    '            Me.ShowDialog()

    '            intUnkId = mintSavingtranunkid

    '            Return Not mblnCancel
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
    '        End Try
    '    End Function
    '#End Region

    '#Region " Private Methods "
    '    Private Sub setColor()
    '        Try
    '            txtVoucherNo.BackColor = GUI.ColorComp
    '            'NudSavingPeriod.BackColor = GUI.ColorComp
    '            'txtMaturityAmt.BackColor = GUI.ColorOptional
    '            txtMonthlyContribution.BackColor = GUI.ColorComp
    '            cboEmpName.BackColor = GUI.ColorComp
    '            cboPayPeriod.BackColor = GUI.ColorComp
    '            cboSavingScheme.BackColor = GUI.ColorComp


    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub GetValue()
    '        Dim objExchangeRate As New clsExchangeRate
    '        objExchangeRate._ExchangeRateunkid = 1

    '        txtVoucherNo.Text = objEmployeeSaving._Voucherno
    '        If objEmployeeSaving._Effectivedate = Nothing Then
    '            dtpDate.Value = Now
    '        Else
    '            dtpDate.Value = objEmployeeSaving._Effectivedate
    '        End If

    '        cboPayPeriod.SelectedValue = objEmployeeSaving._Payperiodunkid
    '        cboEmpName.SelectedValue = objEmployeeSaving._Employeeunkid

    '        cboSavingScheme.SelectedValue = objEmployeeSaving._Savingschemeunkid

    '        'If objEmployeeSaving._Savingperiods = 0 Then
    '        '    NudSavingPeriod.Value = 1
    '        'Else
    '        '    NudSavingPeriod.Value = objEmployeeSaving._Savingperiods
    '        'End If

    '        txtMonthlyContribution.Text = CStr(Format(objEmployeeSaving._Contribution, objExchangeRate._fmtCurrency))
    '        'If objEmployeeSaving._Maturitydate = Nothing Then
    '        '    dtpMaturity.Value = Now
    '        'Else
    '        '    dtpMaturity.Value = objEmployeeSaving._Maturitydate
    '        'End If

    '        'txtMaturityAmt.Text = CStr(Format(objEmployeeSaving._Maturity_Amount, objExchangeRate._fmtCurrency))
    '        objEmployeeSaving._Savingstatus = objEmployeeSaving._Savingstatus
    '        'txtMaturityAmt.Text = CStr(objEmployeeSaving._Maturity_Amount)

    '        'Anjan (26 Jan 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS

    '        If objEmployeeSaving._Stopdate = Nothing Then
    '            dtpStopDate.Value = ConfigParameter._Object._CurrentDateAndTime
    '            dtpStopDate.Checked = False
    '        Else
    '            dtpStopDate.Value = objEmployeeSaving._Stopdate
    '            dtpStopDate.Checked = True
    '        End If
    '        'Anjan (26 Jan 2012)-End 

    '        objExchangeRate = Nothing
    '    End Sub

    '    Private Sub SetValue()
    '        objEmployeeSaving._Voucherno = txtVoucherNo.Text
    '        objEmployeeSaving._Effectivedate = dtpDate.Value
    '        objEmployeeSaving._Payperiodunkid = CInt(cboPayPeriod.SelectedValue)
    '        objEmployeeSaving._Employeeunkid = CInt(cboEmpName.SelectedValue)
    '        objEmployeeSaving._Savingschemeunkid = CInt(cboSavingScheme.SelectedValue)
    '        'objEmployeeSaving._Savingperiods = CInt(NudSavingPeriod.Value)
    '        objEmployeeSaving._Contribution = txtMonthlyContribution.Decimal 'Sohail (11 May 2011)
    '        'objEmployeeSaving._Maturitydate = dtpMaturity.Value
    '        'objEmployeeSaving._Maturity_Amount = cdec(txtMaturityAmt.Text)
    '        If mintSavingtranunkid = -1 Then
    '            objEmployeeSaving._Savingstatus = 1 ' In progress
    '        Else
    '            objEmployeeSaving._Savingstatus = objEmployeeSaving._Savingstatus
    '        End If

    '        'objEmployeeSaving._Maturity_Amount = mdblMaturityAmount

    '        'Anjan (26 Jan 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS
    '        If dtpStopDate.Checked = True Then
    '            objEmployeeSaving._Stopdate = dtpStopDate.Value
    '        Else
    '            objEmployeeSaving._Stopdate = Nothing
    '        End If
    '        objEmployeeSaving._Userunkid = User._Object._Userunkid
    '        'Anjan (26 Jan 2012)-End 

    '    End Sub
    '    Private Sub FillCombo()
    '        Dim dsList As DataSet
    '        'objEmployeeData = New clsEmployee_Master 'Sohail (06 Jan 2012)
    '        objPeriodData = New clscommom_period_Tran
    '        objSavingScheme = New clsSavingScheme
    '        Try
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'dsList = objEmployeeData.GetEmployeeList("List", True)
    '            'cboEmpName.ValueMember = "employeeunkid"
    '            'cboEmpName.DisplayMember = "employeename"
    '            'cboEmpName.DataSource = dsList.Tables("List")
    '            'cboEmpName.SelectedValue = 0
    '            'Sohail (06 Jan 2012) -- End

    '            'Sohail (27 Jul 2010) -- Start
    '            'dsList = objPeriodData.getListForCombo(enModuleRefenence.Payroll, 1, "List", True)
    '            If menAction = enAction.EDIT_ONE Then
    '                dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, 0, "List", True)
    '            Else
    '                dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
    '            End If


    '            'Sohail (27 Jul 2010) -- End
    '            cboPayPeriod.ValueMember = "periodunkid"
    '            cboPayPeriod.DisplayMember = "name"
    '            cboPayPeriod.DataSource = dsList.Tables("List")
    '            cboPayPeriod.SelectedValue = 0

    '            dsList = objSavingScheme.getComboList(True, "List")
    '            cboSavingScheme.ValueMember = "savingschemeunkid"
    '            cboSavingScheme.DisplayMember = "name"
    '            cboSavingScheme.DataSource = dsList.Tables("List")
    '            cboSavingScheme.SelectedValue = 0

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
    '        End Try

    '    End Sub

    '    Private Function IsValid() As Boolean
    '        Try

    '            'Sandeep [ 16 Oct 2010 ] -- Start
    '            'Issue : Auto No. Generation
    '            'If Trim(txtVoucherNo.Text) = "" Then
    '            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Voucher No. cannot be blank. Voucher No. is required information."), enMsgBoxStyle.Information) '?1
    '            '    txtVoucherNo.Focus()
    '            '    Return False
    '            'End If
    '            If ConfigParameter._Object._SavingsVocNoType = 0 Then
    '                If Trim(txtVoucherNo.Text) = "" Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Voucher No. cannot be blank. Voucher No. is required information."), enMsgBoxStyle.Information) '?1
    '                    txtVoucherNo.Focus()
    '                    Return False
    '                End If
    '            End If
    '            'Sandeep [ 16 Oct 2010 ] -- End 

    '            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Pay Period cannot be blank. Pay Period is required information."), enMsgBoxStyle.Information) '?1
    '                cboPayPeriod.Focus()
    '                Return False
    '            End If

    '            If CInt(cboEmpName.SelectedValue) <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Name cannot be blank. Employee Name is required information."), enMsgBoxStyle.Information) '?1
    '                cboEmpName.Focus()
    '                Return False
    '            End If

    '            If CInt(cboSavingScheme.SelectedValue) <= 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Saving Scheme cannot be blank. Saving Scheme is required information."), enMsgBoxStyle.Information) '?1
    '                cboSavingScheme.Focus()
    '                Return False
    '            End If


    '            If Trim(txtMonthlyContribution.Text) = "" Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Contribution cannot be blank. Contribution is required information."), enMsgBoxStyle.Information) '?1
    '                txtMonthlyContribution.Focus()
    '                Return False
    '            End If


    '            'If dtpMaturity.Value <= dtpDate.Value Then
    '            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Maturity date cannot be less than or equal to effective date"), enMsgBoxStyle.Information) '?1
    '            '    dtpMaturity.Focus()
    '            '    Return False
    '            'End If

    '            'If CInt(NudSavingPeriod.Value) < mintMinLoan_Tenure Then
    '            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Saving Scheme tenure cannot be less than minimum tenure of this scheme."), enMsgBoxStyle.Information) '?1
    '            '    NudSavingPeriod.Focus()
    '            '    Return False
    '            'End If

    '            If CDec(txtMonthlyContribution.Text) < mdecMinContribution Then 'Sohail (11 May 2011)
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Minimum Contribution of employee cannot be less than Minimum Contribution of this scheme."), enMsgBoxStyle.Information) '?1
    '                txtMonthlyContribution.Focus()
    '                Return False
    '            End If

    '            'Sohail (26 Jul 2010) -- Start
    '            If dtpDate.Value > mdtPayPeriodEndDate Or dtpDate.Value < mdtPayPeriodStartDate Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Date should be in between ") & mdtPayPeriodStartDate & _
    '                                Language.getMessage(mstrModuleName, 9, " And ") & mdtPayPeriodEndDate, enMsgBoxStyle.Information)
    '                dtpDate.Focus()
    '                Return False
    '            End If
    '            'Sohail (26 Jul 2010) -- End

    '            Dim objScheme As New clsSavingScheme
    '            objScheme._Savingschemeunkid = CInt(cboSavingScheme.SelectedValue)

    '            If txtMonthlyContribution.Decimal < objScheme._Mincontribution Then 'Sohail (11 May 2011)
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Monthly contribution cannot be less than Minimum contribution. Which is set to ") & " " & objScheme._Mincontribution, enMsgBoxStyle.Information)
    '                txtMonthlyContribution.Focus()
    '                Return False
    '            End If


    '            'Anjan (27 Sep 2010)-Start
    '            'Issue : Ruttas suggesstion not necessary
    '            ' objEmployeeData._Employeeunkid = CInt(cboEmpName.SelectedValue)
    '            'If txtMonthlyContribution.Decimal >= objEmployeeData._Scale Then
    '            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Monthly contribution cannot be greater than equal to Employee Scale. Which is set to " & " " & objEmployeeData._Scale), enMsgBoxStyle.Information)
    '            '    txtMonthlyContribution.Focus()
    '            '    Return False
    '            'End If
    '            'Anjan (27 Sep 2010)-End

    '            'Anjan (26 Jan 2012)-Start
    '            'ENHANCEMENT : TRA COMMENTS


    '            If dtpStopDate.Value.Date < FinancialYear._Object._Database_Start_Date Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set this Stop date. Reason this date is less than current financial year."), enMsgBoxStyle.Information)
    '                Return False
    '            End If

    '            Dim objMasterData As New clsMasterData
    '            Dim intPeriodId As Integer
    '            intPeriodId = objMasterData.getCurrentPeriodID(enModuleReference.Payroll, dtpStopDate.Value.Date)
    '            If intPeriodId > 0 Then
    '                Dim objPeriod As New clscommom_period_Tran
    '                objPeriod._Periodunkid = intPeriodId
    '                If objPeriod._Statusid = enStatusType.Close Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set this Stop date. Reason the period is already closed. Please set new Stop date."), enMsgBoxStyle.Information)
    '                    Return False
    '                End If
    '                objPeriod = Nothing
    '            End If
    '            objMasterData = Nothing


    '            'Anjan (26 Jan 2012)-End 

    '            objScheme = Nothing

    '            Return True

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
    '        End Try
    '    End Function

    '    Private Sub SetVisibility()
    '        Try
    '            'Sandeep [ 16 Oct 2010 ] -- Start
    '            'Issue : Auto No. Generation
    '            If ConfigParameter._Object._SavingsVocNoType = 1 Then
    '                txtVoucherNo.Enabled = False
    '            Else
    '                txtVoucherNo.Enabled = True
    '            End If
    '            'Sandeep [ 16 Oct 2010 ] -- End 

    '            If menAction = enAction.EDIT_ONE Then
    '                cboSavingScheme.Enabled = False
    '                dtpDate.Enabled = False
    '                cboPayPeriod.Enabled = False
    '                cboEmpName.Enabled = False
    '                objbtnSearchEmployee.Enabled = False
    '                objSavingSchemeSearch.Enabled = False 'Sohail (07 Jul 2014)
    '            Else
    '                cboSavingScheme.Enabled = True
    '                dtpDate.Enabled = True
    '                cboPayPeriod.Enabled = True
    '                cboEmpName.Enabled = True
    '                objbtnSearchEmployee.Enabled = True
    '                objSavingSchemeSearch.Enabled = True 'Sohail (07 Jul 2014)
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
    '        End Try
    '    End Sub

    '    'Sohail (06 Jan 2012) -- Start
    '    'TRA - ENHANCEMENT
    '    Private Sub FillEmployeeCombo()
    '        Dim objEmployee As New clsEmployee_Master
    '        Dim dsCombo As DataSet
    '        Try
    '            dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , mdtPayPeriodStartDate, mdtPayPeriodEndDate)
    '            With cboEmpName
    '                .BeginUpdate()
    '                .ValueMember = "employeeunkid"
    '                .DisplayMember = "employeename"
    '                .DataSource = dsCombo.Tables("Employee")
    '                .SelectedValue = 0
    '                .EndUpdate()
    '            End With
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "FillEmployeeCombo", mstrModuleName)
    '        Finally
    '            objEmployee = Nothing
    '        End Try

    '    End Sub
    '    'Sohail (06 Jan 2012) -- End
    '#End Region

    '#Region " Form's Events "

    '    Private Sub frmEmployeeSaving_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    '        objEmployeeSaving = Nothing
    '        objInterestRate = Nothing
    '    End Sub

    '    Private Sub frmEmployeeSaving_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
    '        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
    '            Call btnSave.PerformClick()
    '        End If
    '    End Sub

    '    Private Sub frmEmployeeSaving_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
    '        If Asc(e.KeyChar) = 13 Then
    '            Windows.Forms.SendKeys.Send("{Tab}")
    '            e.Handled = True
    '        End If
    '    End Sub

    '    Private Sub frmEmployeeSaving_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '        objEmployeeSaving = New clsSaving_Tran
    '        Try
    '            Call Set_Logo(Me, gApplicationType)
    '            'S.SANDEEP [ 20 AUG 2011 ] -- START
    '            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    '            Language.setLanguage(Me.Name)
    '            Call OtherSettings()
    '            'S.SANDEEP [ 20 AUG 2011 ] -- END
    '            Call SetVisibility()
    '            Call setColor()

    '            If menAction = enAction.EDIT_ONE Then
    '                objEmployeeSaving._Savingtranunkid = mintSavingtranunkid
    '            End If
    '            Call FillCombo()
    '            Call GetValue()

    '            'NudSavingPeriod.Minimum = 1

    '            txtVoucherNo.Focus()
    '            mblnFormload = True
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "frmEmployeeSaving_AddEdit_Load", mstrModuleName)
    '        End Try
    '    End Sub

    '    'S.SANDEEP [ 20 AUG 2011 ] -- START
    '    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    '    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    '        Dim objfrm As New frmLanguage
    '        Try
    '            If User._Object._Isrighttoleft = True Then
    '                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                objfrm.RightToLeftLayout = True
    '                Call Language.ctlRightToLeftlayOut(objfrm)
    '            End If

    '            Call SetMessages()

    '            clsSaving_Tran.SetMessages()
    '            objfrm._Other_ModuleNames = "clsSaving_Tran"
    '            objfrm.displayDialog(Me)

    '            Call SetLanguage()

    '        Catch ex As System.Exception
    '            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
    '        Finally
    '            objfrm.Dispose()
    '            objfrm = Nothing
    '        End Try
    '    End Sub
    '    'S.SANDEEP [ 20 AUG 2011 ] -- END
    '#End Region

    '#Region " Button's Events "

    '    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '        Dim blnFlag As Boolean = False
    '        Try

    '            If IsValid() = False Then Exit Sub

    '            Call SetValue()

    '            If menAction = enAction.EDIT_ONE Then
    '                blnFlag = objEmployeeSaving.Update()
    '            Else
    '                blnFlag = objEmployeeSaving.Insert()
    '            End If

    '            If blnFlag = False And objEmployeeSaving._Message <> "" Then
    '                eZeeMsgBox.Show(objEmployeeSaving._Message, enMsgBoxStyle.Information)
    '            End If

    '            If blnFlag Then
    '                mblnCancel = False
    '                If menAction = enAction.ADD_CONTINUE Then
    '                    objEmployeeSaving = Nothing
    '                    objEmployeeSaving = New clsSaving_Tran
    '                    Call GetValue()
    '                    txtVoucherNo.Focus()
    '                Else
    '                    mintSavingtranunkid = objEmployeeSaving._Savingtranunkid
    '                    Me.Close()


    '                End If
    '            End If
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
    '        End Try
    '    End Sub

    '    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '        Try
    '            Me.Close()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
    '        End Try
    '    End Sub
    '#End Region

    '#Region " ComboBox's Events "
    '    Private Sub cboSavingScheme_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSavingScheme.SelectionChangeCommitted
    '        Try
    '            objSavingScheme._Savingschemeunkid = CInt(cboSavingScheme.SelectedValue)
    '            'mintMinLoan_Tenure = objSavingScheme._Mintenure
    '            mdecMinContribution = objSavingScheme._Mincontribution
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "cboSavingScheme_SelectionChangeCommitted", mstrModuleName)
    '        End Try

    '    End Sub

    '    Private Sub cboPayPeriod_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedValueChanged
    '        Dim objPeriod As New clscommom_period_Tran
    '        Try
    '            objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
    '            mdtPayPeriodStartDate = objPeriod._Start_Date
    '            mdtPayPeriodEndDate = objPeriod._End_Date

    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            Call FillEmployeeCombo()
    '            'Sohail (06 Jan 2012) -- End
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '        Finally
    '            objPeriod = Nothing
    '        End Try
    '    End Sub
    '#End Region

    '#Region " Textbox's Events "
    '    Private Sub txtMonthlyContribution_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMonthlyContribution.LostFocus

    '        Try
    '            'Call MaturityAmount_Calculation()
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "txtMonthlyContribution_LostFocus", mstrModuleName)
    '        End Try

    '    End Sub
    '#End Region

    '#Region " Other Control's Events "

    '    'Sohail (24 Jul 2010) - Start
    '    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
    '        Dim objfrm As New frmCommonSearch
    '        'Sohail (06 Jan 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'Dim objEmployee As New clsEmployee_Master
    '        'Dim dsList As DataSet
    '        'Sohail (06 Jan 2012) -- End
    '        Try
    '            'S.SANDEEP [ 20 AUG 2011 ] -- START
    '            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    '            If User._Object._Isrighttoleft = True Then
    '                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                objfrm.RightToLeftLayout = True
    '                Call Language.ctlRightToLeftlayOut(objfrm)
    '            End If
    '            'S.SANDEEP [ 20 AUG 2011 ] -- END
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'dsList = objEmployee.GetEmployeeList("EmployeeList")
    '            'Sohail (06 Jan 2012) -- End
    '            With cboEmpName
    '                'Sohail (06 Jan 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'objfrm.DataSource = dsList.Tables("EmployeeList")
    '                objfrm.DataSource = CType(cboEmpName.DataSource, DataTable)
    '                'Sohail (06 Jan 2012) -- End
    '                objfrm.ValueMember = .ValueMember
    '                objfrm.DisplayMember = .DisplayMember
    '                objfrm.CodeMember = "employeecode"
    '                If objfrm.DisplayDialog Then
    '                    .SelectedValue = objfrm.SelectedValue
    '                End If
    '                .Focus()
    '            End With
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
    '        Finally
    '            objfrm = Nothing
    '            'objEmployee = Nothing 'Sohail (06 Jan 2012
    '        End Try
    '    End Sub
    '    'Sohail (24 Jul 2010) - End
    '    'Anjan (30 Jan 2012)-Start
    '    'ENHANCEMENT : TRA COMMENTS
    '    Private Sub objSavingSchemeSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objSavingSchemeSearch.Click
    '        Dim objFrm As New frmCommonSearch
    '        Try
    '            If User._Object._Isrighttoleft = True Then
    '                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                objFrm.RightToLeftLayout = True
    '                Call Language.ctlRightToLeftlayOut(objFrm)
    '            End If
    '            With cboSavingScheme
    '                objFrm.DataSource = CType(cboSavingScheme.DataSource, DataTable)
    '                objFrm.ValueMember = .ValueMember
    '                objFrm.DisplayMember = .DisplayMember
    '                objFrm.CodeMember = "code"
    '            End With
    '            If objFrm.DisplayDialog Then
    '                cboSavingScheme.SelectedValue = objFrm.SelectedValue
    '                cboSavingScheme.Focus()
    '            End If

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "objSavingSchemeSearch_Click", mstrModuleName)
    '        End Try
    '    End Sub
    '    'Anjan (30 Jan 2012)-End 

    '#End Region

    'Private Function IsValid() As Boolean
    '    Try

    '        'Sandeep [ 16 Oct 2010 ] -- Start
    '        'Issue : Auto No. Generation
    '        'If Trim(txtVoucherNo.Text) = "" Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Voucher No. cannot be blank. Voucher No. is required information."), enMsgBoxStyle.Information) '?1
    '        '    txtVoucherNo.Focus()
    '        '    Return False
    '        'End If
    '        If ConfigParameter._Object._SavingsVocNoType = 0 Then
    '            If Trim(txtVoucherNo.Text) = "" Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Voucher No. cannot be blank. Voucher No. is required information."), enMsgBoxStyle.Information) '?1
    '                txtVoucherNo.Focus()
    '                Return False
    '            End If
    '        End If
    '        'Sandeep [ 16 Oct 2010 ] -- End 

    '        If CInt(cboPayPeriod.SelectedValue) <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Pay Period cannot be blank. Pay Period is required information."), enMsgBoxStyle.Information) '?1
    '            cboPayPeriod.Focus()
    '            Return False
    '        End If

    '        If CInt(cboEmpName.SelectedValue) <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Name cannot be blank. Employee Name is required information."), enMsgBoxStyle.Information) '?1
    '            cboEmpName.Focus()
    '            Return False
    '        End If

    '        If CInt(cboSavingScheme.SelectedValue) <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Saving Scheme cannot be blank. Saving Scheme is required information."), enMsgBoxStyle.Information) '?1
    '            cboSavingScheme.Focus()
    '            Return False
    '        End If


    '        'Shani [ 12 JAN 2015 ] -- START
    '        'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    '        'If Trim(txtContribution.Text) = "" Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Contribution cannot be blank. Contribution is required information."), enMsgBoxStyle.Information) '?1
    '        '    txtContribution.Focus()
    '        '    Return False
    '        'End If
    '        'Shani [ 12 JAN 2015 ] -- END




    '        'If dtpMaturity.Value <= dtpDate.Value Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Maturity date cannot be less than or equal to effective date"), enMsgBoxStyle.Information) '?1
    '        '    dtpMaturity.Focus()
    '        '    Return False
    '        'End If

    '        'If CInt(NudSavingPeriod.Value) < mintMinLoan_Tenure Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Saving Scheme tenure cannot be less than minimum tenure of this scheme."), enMsgBoxStyle.Information) '?1
    '        '    NudSavingPeriod.Focus()
    '        '    Return False
    '        'End If


    '        'Shani [ 12 JAN 2015 ] -- START
    '        '
    '        'If CDec(txtContribution.Text) < mdecMinContribution Then 'Sohail (11 May 2011)
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Minimum Contribution of employee cannot be less than Minimum Contribution of this scheme."), enMsgBoxStyle.Information) '?1
    '        '    txtContribution.Focus()
    '        '    Return False
    '        'End If
    '        'Shani [ 12 JAN 2015 ] -- END



    '        'Sohail (26 Jul 2010) -- Start
    '        If dtpDate.Value > mdtPayPeriodEndDate Or dtpDate.Value < mdtPayPeriodStartDate Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Date should be in between ") & mdtPayPeriodStartDate & _
    '                            Language.getMessage(mstrModuleName, 9, " And ") & mdtPayPeriodEndDate, enMsgBoxStyle.Information)
    '            dtpDate.Focus()
    '            Return False
    '        End If
    '        'Sohail (26 Jul 2010) -- End


    '        'Shani [ 12 JAN 2015 ] -- START
    '        '
    '        'Dim objScheme As New clsSavingScheme
    '        'objScheme._Savingschemeunkid = CInt(cboSavingScheme.SelectedValue)

    '        'If txtContribution.Decimal < objScheme._Mincontribution Then 'Sohail (11 May 2011)
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Monthly contribution cannot be less than Minimum contribution. Which is set to ") & " " & objScheme._Mincontribution, enMsgBoxStyle.Information)
    '        '    txtContribution.Focus()
    '        '    Return False
    '        'End If
    '        'Shani [ 12 JAN 2015 ] -- END




    '        'Anjan (27 Sep 2010)-Start
    '        'Issue : Ruttas suggesstion not necessary
    '        ' objEmployeeData._Employeeunkid = CInt(cboEmpName.SelectedValue)
    '        'If txtMonthlyContribution.Decimal >= objEmployeeData._Scale Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Monthly contribution cannot be greater than equal to Employee Scale. Which is set to " & " " & objEmployeeData._Scale), enMsgBoxStyle.Information)
    '        '    txtMonthlyContribution.Focus()
    '        '    Return False
    '        'End If
    '        'Anjan (27 Sep 2010)-End

    '        'Anjan (26 Jan 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS


    '        If dtpStopDate.Value.Date < FinancialYear._Object._Database_Start_Date Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set this Stop date. Reason this date is less than current financial year."), enMsgBoxStyle.Information)
    '            Return False
    '        End If

    '        Dim objMasterData As New clsMasterData
    '        Dim intPeriodId As Integer
    '        intPeriodId = objMasterData.getCurrentPeriodID(enModuleReference.Payroll, dtpStopDate.Value.Date)
    '        If intPeriodId > 0 Then
    '            Dim objPeriod As New clscommom_period_Tran
    '            objPeriod._Periodunkid = intPeriodId
    '            If objPeriod._Statusid = enStatusType.Close Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set this Stop date. Reason the period is already closed. Please set new Stop date."), enMsgBoxStyle.Information)
    '                Return False
    '            End If
    '            objPeriod = Nothing
    '        End If
    '        objMasterData = Nothing


    '        'Anjan (26 Jan 2012)-End 

    '        objScheme = Nothing

    '        Return True

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
    '    End Try
    'End Function
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbEmpSavingsInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmpSavingsInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeContributionInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeContributionInformation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmpRateInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmpRateInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnContributionAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnContributionAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnRateAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnRateAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnContributionEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnContributionEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnRateEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnRateEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnContDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnContDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnRateDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnRateDelete.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbEmpSavingsInfo.Text = Language._Object.getCaption(Me.gbEmpSavingsInfo.Name, Me.gbEmpSavingsInfo.Text)
            Me.lblContribution.Text = Language._Object.getCaption(Me.lblContribution.Name, Me.lblContribution.Text)
            Me.lblSavingScheme.Text = Language._Object.getCaption(Me.lblSavingScheme.Name, Me.lblSavingScheme.Text)
            Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.Name, Me.lblDate.Text)
            Me.lblVoucherNo.Text = Language._Object.getCaption(Me.lblVoucherNo.Name, Me.lblVoucherNo.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.lblStopDate.Text = Language._Object.getCaption(Me.lblStopDate.Name, Me.lblStopDate.Text)
            Me.gbEmployeeContributionInformation.Text = Language._Object.getCaption(Me.gbEmployeeContributionInformation.Name, Me.gbEmployeeContributionInformation.Text)
            Me.lblContPeriod.Text = Language._Object.getCaption(Me.lblContPeriod.Name, Me.lblContPeriod.Text)
            Me.gbEmpRateInfo.Text = Language._Object.getCaption(Me.gbEmpRateInfo.Name, Me.gbEmpRateInfo.Text)
            Me.lblRatePeriod.Text = Language._Object.getCaption(Me.lblRatePeriod.Name, Me.lblRatePeriod.Text)
            Me.lblRate.Text = Language._Object.getCaption(Me.lblRate.Name, Me.lblRate.Text)
            Me.btnContributionAdd.Text = Language._Object.getCaption(Me.btnContributionAdd.Name, Me.btnContributionAdd.Text)
            Me.btnRateAdd.Text = Language._Object.getCaption(Me.btnRateAdd.Name, Me.btnRateAdd.Text)
            Me.lblRateDate.Text = Language._Object.getCaption(Me.lblRateDate.Name, Me.lblRateDate.Text)
            Me.lnEmpRateInfo.Text = Language._Object.getCaption(Me.lnEmpRateInfo.Name, Me.lnEmpRateInfo.Text)
            Me.btnContributionEdit.Text = Language._Object.getCaption(Me.btnContributionEdit.Name, Me.btnContributionEdit.Text)
            Me.btnRateEdit.Text = Language._Object.getCaption(Me.btnRateEdit.Name, Me.btnRateEdit.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.colhEffectiveDate.Text = Language._Object.getCaption(CStr(Me.colhEffectiveDate.Tag), Me.colhEffectiveDate.Text)
            Me.colhInterestRate.Text = Language._Object.getCaption(CStr(Me.colhInterestRate.Tag), Me.colhInterestRate.Text)
            Me.btnContDelete.Text = Language._Object.getCaption(Me.btnContDelete.Name, Me.btnContDelete.Text)
            Me.btnRateDelete.Text = Language._Object.getCaption(Me.btnRateDelete.Name, Me.btnRateDelete.Text)
            Me.colhContPeriod.Text = Language._Object.getCaption(CStr(Me.colhContPeriod.Tag), Me.colhContPeriod.Text)
            Me.colhContribution.Text = Language._Object.getCaption(CStr(Me.colhContribution.Tag), Me.colhContribution.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.lblOpeningContribution.Text = Language._Object.getCaption(Me.lblOpeningContribution.Name, Me.lblOpeningContribution.Text)
            Me.lblOpeningBalance.Text = Language._Object.getCaption(Me.lblOpeningBalance.Name, Me.lblOpeningBalance.Text)
            Me.lblWithOpeningInterest.Text = Language._Object.getCaption(Me.lblWithOpeningInterest.Name, Me.lblWithOpeningInterest.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Voucher No. cannot be blank. Voucher No. is required information.")
            Language.setMessage(mstrModuleName, 2, "Pay Period cannot be blank. Pay Period is required information.")
            Language.setMessage(mstrModuleName, 3, "Employee Name cannot be blank. Employee Name is required information.")
            Language.setMessage(mstrModuleName, 4, "Saving Scheme cannot be blank. Saving Scheme is required information.")
            Language.setMessage(mstrModuleName, 7, "Date should be between")
            Language.setMessage(mstrModuleName, 9, " And")
            Language.setMessage(mstrModuleName, 10, "Sorry, you cannot set this Stop date. Reason this date is less than current financial year.")
            Language.setMessage(mstrModuleName, 11, "Sorry, you cannot set this Stop date. Reason the period is already closed. Please set new Stop date.")
            Language.setMessage(mstrModuleName, 12, "Please add atleast one Contribution.")
            Language.setMessage(mstrModuleName, 13, "Plase add atleast one contribution for effective period.")
            Language.setMessage(mstrModuleName, 14, "Sorry, You can not Save this Saving Scheme. Reason : Process Payroll is already done for last date of selected Period.")
            Language.setMessage(mstrModuleName, 15, "Plase add atleast one Interest Rate for effective period.")
            Language.setMessage(mstrModuleName, 16, "Period cannot be blank. Period is required information.")
            Language.setMessage(mstrModuleName, 17, "Sorry, Contribution should be greater than zero.")
            Language.setMessage(mstrModuleName, 18, "Minimum Contribution of employee cannot be less than Minimum Contribution of this scheme.")
            Language.setMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period.")
            Language.setMessage(mstrModuleName, 20, "Sorry, Contribution for the selected period is already given.")
            Language.setMessage(mstrModuleName, 21, "Sorry, Contribution period should not be less than effective period.")
            Language.setMessage(mstrModuleName, 22, "Sorry, You can not Add / Edit / Delete this Contribution. Reason : Process Payroll is already done for last date of selected period.")
            Language.setMessage(mstrModuleName, 23, "Sorry, You can not Add / Edit / Delete this Rate. Reason : Process Payroll is already done for last date of selected period.")
            Language.setMessage(mstrModuleName, 24, "Sorry, Contribution should not be less than Minimum Contribution of selected scheme.")
            Language.setMessage(mstrModuleName, 25, "Sorry, Period is already closed. Please select open Period.")
            Language.setMessage(mstrModuleName, 26, "Interest Rate should be between 0 and 100.")
            Language.setMessage(mstrModuleName, 27, "Sorry, Interest Rate period should not be less than effective period.")
            Language.setMessage(mstrModuleName, 28, "Sorry, you cannot add same Date Interest Rate again in the below list.")
            Language.setMessage(mstrModuleName, 29, "Interest Rate cannot be blank. Interest Rate is required information.")
            Language.setMessage(mstrModuleName, 30, "Currency cannot be blank. Currency is required information.")
			Language.setMessage(mstrModuleName, 31, "Do you want to void Payroll?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class



