﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Public Class frmRedemption_AddEdit
    Private ReadOnly mstrModuleName As String = "frmRedemption_AddEdit"

    Private mblnCancel As Boolean = True
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbRedemptionInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbRedemptionInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbRedemptionInfo.Text = Language._Object.getCaption(Me.gbRedemptionInfo.Name, Me.gbRedemptionInfo.Text)
			Me.lblRedempCharges.Text = Language._Object.getCaption(Me.lblRedempCharges.Name, Me.lblRedempCharges.Text)
			Me.lblRedempWithinYear.Text = Language._Object.getCaption(Me.lblRedempWithinYear.Name, Me.lblRedempWithinYear.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class