﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmployeeSavings_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmployeeSavings_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbEmployeeContributionInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnContDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnl_dgvContribution = New System.Windows.Forms.Panel
        Me.lvContributionData = New System.Windows.Forms.ListView
        Me.colhContPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhContribution = New System.Windows.Forms.ColumnHeader
        Me.objColhContMasterId = New System.Windows.Forms.ColumnHeader
        Me.objcolhContGuid = New System.Windows.Forms.ColumnHeader
        Me.btnContributionAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.cboContPeriod = New System.Windows.Forms.ComboBox
        Me.lblContPeriod = New System.Windows.Forms.Label
        Me.txtMonthlyContribution = New eZee.TextBox.NumericTextBox
        Me.lblContribution = New System.Windows.Forms.Label
        Me.btnContributionEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbEmpSavingsInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtOpeningContribution = New eZee.TextBox.NumericTextBox
        Me.lblOpeningContribution = New System.Windows.Forms.Label
        Me.objlblExRate = New System.Windows.Forms.Label
        Me.lblStopDate = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.objSavingSchemeSearch = New eZee.Common.eZeeGradientButton
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.dtpStopDate = New System.Windows.Forms.DateTimePicker
        Me.cboSavingScheme = New System.Windows.Forms.ComboBox
        Me.lblDate = New System.Windows.Forms.Label
        Me.cboEmpName = New System.Windows.Forms.ComboBox
        Me.txtVoucherNo = New eZee.TextBox.AlphanumericTextBox
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblSavingScheme = New System.Windows.Forms.Label
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.lblVoucherNo = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbEmpRateInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnRateDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnl_dgvIntRate = New System.Windows.Forms.Panel
        Me.lvInterestRateData = New System.Windows.Forms.ListView
        Me.colhPeriod = New System.Windows.Forms.ColumnHeader
        Me.colhEffectiveDate = New System.Windows.Forms.ColumnHeader
        Me.colhInterestRate = New System.Windows.Forms.ColumnHeader
        Me.objcolhRateMasterid = New System.Windows.Forms.ColumnHeader
        Me.objcolhRateGuid = New System.Windows.Forms.ColumnHeader
        Me.lnEmpRateInfo = New eZee.Common.eZeeLine
        Me.dtpRateDate = New System.Windows.Forms.DateTimePicker
        Me.lblRateDate = New System.Windows.Forms.Label
        Me.btnRateAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboRatePeriod = New System.Windows.Forms.ComboBox
        Me.lblRatePeriod = New System.Windows.Forms.Label
        Me.txtRate = New eZee.TextBox.NumericTextBox
        Me.lblRate = New System.Windows.Forms.Label
        Me.btnRateEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblOpeningBalance = New System.Windows.Forms.Label
        Me.txtOpeningBalance = New eZee.TextBox.NumericTextBox
        Me.lblWithOpeningInterest = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.gbEmployeeContributionInformation.SuspendLayout()
        Me.pnl_dgvContribution.SuspendLayout()
        Me.gbEmpSavingsInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.gbEmpRateInfo.SuspendLayout()
        Me.pnl_dgvIntRate.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbEmployeeContributionInformation)
        Me.pnlMainInfo.Controls.Add(Me.gbEmpSavingsInfo)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbEmpRateInfo)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(713, 570)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbEmployeeContributionInformation
        '
        Me.gbEmployeeContributionInformation.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeContributionInformation.Checked = False
        Me.gbEmployeeContributionInformation.CollapseAllExceptThis = False
        Me.gbEmployeeContributionInformation.CollapsedHoverImage = Nothing
        Me.gbEmployeeContributionInformation.CollapsedNormalImage = Nothing
        Me.gbEmployeeContributionInformation.CollapsedPressedImage = Nothing
        Me.gbEmployeeContributionInformation.CollapseOnLoad = False
        Me.gbEmployeeContributionInformation.Controls.Add(Me.btnContDelete)
        Me.gbEmployeeContributionInformation.Controls.Add(Me.pnl_dgvContribution)
        Me.gbEmployeeContributionInformation.Controls.Add(Me.btnContributionAdd)
        Me.gbEmployeeContributionInformation.Controls.Add(Me.objLine1)
        Me.gbEmployeeContributionInformation.Controls.Add(Me.cboContPeriod)
        Me.gbEmployeeContributionInformation.Controls.Add(Me.lblContPeriod)
        Me.gbEmployeeContributionInformation.Controls.Add(Me.txtMonthlyContribution)
        Me.gbEmployeeContributionInformation.Controls.Add(Me.lblContribution)
        Me.gbEmployeeContributionInformation.Controls.Add(Me.btnContributionEdit)
        Me.gbEmployeeContributionInformation.ExpandedHoverImage = Nothing
        Me.gbEmployeeContributionInformation.ExpandedNormalImage = Nothing
        Me.gbEmployeeContributionInformation.ExpandedPressedImage = Nothing
        Me.gbEmployeeContributionInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeContributionInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeContributionInformation.HeaderHeight = 25
        Me.gbEmployeeContributionInformation.HeaderMessage = ""
        Me.gbEmployeeContributionInformation.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmployeeContributionInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeContributionInformation.HeightOnCollapse = 0
        Me.gbEmployeeContributionInformation.LeftTextSpace = 0
        Me.gbEmployeeContributionInformation.Location = New System.Drawing.Point(411, 64)
        Me.gbEmployeeContributionInformation.Name = "gbEmployeeContributionInformation"
        Me.gbEmployeeContributionInformation.OpenHeight = 300
        Me.gbEmployeeContributionInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeContributionInformation.ShowBorder = True
        Me.gbEmployeeContributionInformation.ShowCheckBox = False
        Me.gbEmployeeContributionInformation.ShowCollapseButton = False
        Me.gbEmployeeContributionInformation.ShowDefaultBorderColor = True
        Me.gbEmployeeContributionInformation.ShowDownButton = False
        Me.gbEmployeeContributionInformation.ShowHeader = True
        Me.gbEmployeeContributionInformation.Size = New System.Drawing.Size(290, 296)
        Me.gbEmployeeContributionInformation.TabIndex = 1
        Me.gbEmployeeContributionInformation.Temp = 0
        Me.gbEmployeeContributionInformation.Text = "Employee Contribution Information"
        Me.gbEmployeeContributionInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnContDelete
        '
        Me.btnContDelete.BackColor = System.Drawing.Color.White
        Me.btnContDelete.BackgroundImage = CType(resources.GetObject("btnContDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnContDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnContDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnContDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnContDelete.FlatAppearance.BorderSize = 0
        Me.btnContDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnContDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnContDelete.ForeColor = System.Drawing.Color.Black
        Me.btnContDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnContDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnContDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnContDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnContDelete.Location = New System.Drawing.Point(183, 87)
        Me.btnContDelete.Name = "btnContDelete"
        Me.btnContDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnContDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnContDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnContDelete.TabIndex = 7
        Me.btnContDelete.Text = "Delete"
        Me.btnContDelete.UseVisualStyleBackColor = True
        '
        'pnl_dgvContribution
        '
        Me.pnl_dgvContribution.Controls.Add(Me.lvContributionData)
        Me.pnl_dgvContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl_dgvContribution.Location = New System.Drawing.Point(10, 133)
        Me.pnl_dgvContribution.Name = "pnl_dgvContribution"
        Me.pnl_dgvContribution.Size = New System.Drawing.Size(270, 145)
        Me.pnl_dgvContribution.TabIndex = 5
        '
        'lvContributionData
        '
        Me.lvContributionData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhContPeriod, Me.colhContribution, Me.objColhContMasterId, Me.objcolhContGuid})
        Me.lvContributionData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvContributionData.FullRowSelect = True
        Me.lvContributionData.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvContributionData.Location = New System.Drawing.Point(0, 0)
        Me.lvContributionData.Name = "lvContributionData"
        Me.lvContributionData.Size = New System.Drawing.Size(270, 145)
        Me.lvContributionData.TabIndex = 0
        Me.lvContributionData.UseCompatibleStateImageBehavior = False
        Me.lvContributionData.View = System.Windows.Forms.View.Details
        '
        'colhContPeriod
        '
        Me.colhContPeriod.Tag = "colhContPeriod"
        Me.colhContPeriod.Text = "Period"
        Me.colhContPeriod.Width = 140
        '
        'colhContribution
        '
        Me.colhContribution.Tag = "colhContribution"
        Me.colhContribution.Text = "Contribution"
        Me.colhContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhContribution.Width = 120
        '
        'objColhContMasterId
        '
        Me.objColhContMasterId.Tag = "objColhContMasterId"
        Me.objColhContMasterId.Text = ""
        Me.objColhContMasterId.Width = 0
        '
        'objcolhContGuid
        '
        Me.objcolhContGuid.Tag = "objcolhContGuid"
        Me.objcolhContGuid.Text = ""
        Me.objcolhContGuid.Width = 0
        '
        'btnContributionAdd
        '
        Me.btnContributionAdd.BackColor = System.Drawing.Color.White
        Me.btnContributionAdd.BackgroundImage = CType(resources.GetObject("btnContributionAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnContributionAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnContributionAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnContributionAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnContributionAdd.FlatAppearance.BorderSize = 0
        Me.btnContributionAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnContributionAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnContributionAdd.ForeColor = System.Drawing.Color.Black
        Me.btnContributionAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnContributionAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnContributionAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnContributionAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnContributionAdd.Location = New System.Drawing.Point(80, 87)
        Me.btnContributionAdd.Name = "btnContributionAdd"
        Me.btnContributionAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnContributionAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnContributionAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnContributionAdd.TabIndex = 2
        Me.btnContributionAdd.Text = "Add"
        Me.btnContributionAdd.UseVisualStyleBackColor = True
        '
        'objLine1
        '
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(7, 120)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(273, 10)
        Me.objLine1.TabIndex = 0
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboContPeriod
        '
        Me.cboContPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboContPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboContPeriod.FormattingEnabled = True
        Me.cboContPeriod.Location = New System.Drawing.Point(112, 33)
        Me.cboContPeriod.Name = "cboContPeriod"
        Me.cboContPeriod.Size = New System.Drawing.Size(112, 21)
        Me.cboContPeriod.TabIndex = 0
        '
        'lblContPeriod
        '
        Me.lblContPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContPeriod.Location = New System.Drawing.Point(7, 35)
        Me.lblContPeriod.Name = "lblContPeriod"
        Me.lblContPeriod.Size = New System.Drawing.Size(99, 16)
        Me.lblContPeriod.TabIndex = 0
        Me.lblContPeriod.Text = "Pay Period"
        Me.lblContPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMonthlyContribution
        '
        Me.txtMonthlyContribution.AllowNegative = True
        Me.txtMonthlyContribution.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMonthlyContribution.DigitsInGroup = 0
        Me.txtMonthlyContribution.Flags = 0
        Me.txtMonthlyContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMonthlyContribution.Location = New System.Drawing.Point(112, 60)
        Me.txtMonthlyContribution.MaxDecimalPlaces = 6
        Me.txtMonthlyContribution.MaxWholeDigits = 21
        Me.txtMonthlyContribution.Name = "txtMonthlyContribution"
        Me.txtMonthlyContribution.Prefix = ""
        Me.txtMonthlyContribution.RangeMax = 1.7976931348623157E+308
        Me.txtMonthlyContribution.RangeMin = -1.7976931348623157E+308
        Me.txtMonthlyContribution.Size = New System.Drawing.Size(112, 21)
        Me.txtMonthlyContribution.TabIndex = 1
        Me.txtMonthlyContribution.Text = "0"
        Me.txtMonthlyContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblContribution
        '
        Me.lblContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContribution.Location = New System.Drawing.Point(7, 62)
        Me.lblContribution.Name = "lblContribution"
        Me.lblContribution.Size = New System.Drawing.Size(99, 16)
        Me.lblContribution.TabIndex = 0
        Me.lblContribution.Text = "Contribution"
        Me.lblContribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnContributionEdit
        '
        Me.btnContributionEdit.BackColor = System.Drawing.Color.White
        Me.btnContributionEdit.BackgroundImage = CType(resources.GetObject("btnContributionEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnContributionEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnContributionEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnContributionEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnContributionEdit.FlatAppearance.BorderSize = 0
        Me.btnContributionEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnContributionEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnContributionEdit.ForeColor = System.Drawing.Color.Black
        Me.btnContributionEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnContributionEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnContributionEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnContributionEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnContributionEdit.Location = New System.Drawing.Point(80, 87)
        Me.btnContributionEdit.Name = "btnContributionEdit"
        Me.btnContributionEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnContributionEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnContributionEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnContributionEdit.TabIndex = 3
        Me.btnContributionEdit.Text = "Edit"
        Me.btnContributionEdit.UseVisualStyleBackColor = True
        '
        'gbEmpSavingsInfo
        '
        Me.gbEmpSavingsInfo.BorderColor = System.Drawing.Color.Black
        Me.gbEmpSavingsInfo.Checked = False
        Me.gbEmpSavingsInfo.CollapseAllExceptThis = False
        Me.gbEmpSavingsInfo.CollapsedHoverImage = Nothing
        Me.gbEmpSavingsInfo.CollapsedNormalImage = Nothing
        Me.gbEmpSavingsInfo.CollapsedPressedImage = Nothing
        Me.gbEmpSavingsInfo.CollapseOnLoad = False
        Me.gbEmpSavingsInfo.Controls.Add(Me.lblWithOpeningInterest)
        Me.gbEmpSavingsInfo.Controls.Add(Me.txtOpeningBalance)
        Me.gbEmpSavingsInfo.Controls.Add(Me.lblOpeningBalance)
        Me.gbEmpSavingsInfo.Controls.Add(Me.txtOpeningContribution)
        Me.gbEmpSavingsInfo.Controls.Add(Me.lblOpeningContribution)
        Me.gbEmpSavingsInfo.Controls.Add(Me.objlblExRate)
        Me.gbEmpSavingsInfo.Controls.Add(Me.lblStopDate)
        Me.gbEmpSavingsInfo.Controls.Add(Me.cboCurrency)
        Me.gbEmpSavingsInfo.Controls.Add(Me.lblCurrency)
        Me.gbEmpSavingsInfo.Controls.Add(Me.objSavingSchemeSearch)
        Me.gbEmpSavingsInfo.Controls.Add(Me.lblPayPeriod)
        Me.gbEmpSavingsInfo.Controls.Add(Me.cboPayPeriod)
        Me.gbEmpSavingsInfo.Controls.Add(Me.dtpStopDate)
        Me.gbEmpSavingsInfo.Controls.Add(Me.cboSavingScheme)
        Me.gbEmpSavingsInfo.Controls.Add(Me.lblDate)
        Me.gbEmpSavingsInfo.Controls.Add(Me.cboEmpName)
        Me.gbEmpSavingsInfo.Controls.Add(Me.txtVoucherNo)
        Me.gbEmpSavingsInfo.Controls.Add(Me.dtpDate)
        Me.gbEmpSavingsInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbEmpSavingsInfo.Controls.Add(Me.lblSavingScheme)
        Me.gbEmpSavingsInfo.Controls.Add(Me.lblEmpName)
        Me.gbEmpSavingsInfo.Controls.Add(Me.lblVoucherNo)
        Me.gbEmpSavingsInfo.ExpandedHoverImage = Nothing
        Me.gbEmpSavingsInfo.ExpandedNormalImage = Nothing
        Me.gbEmpSavingsInfo.ExpandedPressedImage = Nothing
        Me.gbEmpSavingsInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmpSavingsInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmpSavingsInfo.HeaderHeight = 25
        Me.gbEmpSavingsInfo.HeaderMessage = ""
        Me.gbEmpSavingsInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmpSavingsInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmpSavingsInfo.HeightOnCollapse = 0
        Me.gbEmpSavingsInfo.LeftTextSpace = 0
        Me.gbEmpSavingsInfo.Location = New System.Drawing.Point(12, 64)
        Me.gbEmpSavingsInfo.Name = "gbEmpSavingsInfo"
        Me.gbEmpSavingsInfo.OpenHeight = 300
        Me.gbEmpSavingsInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmpSavingsInfo.ShowBorder = True
        Me.gbEmpSavingsInfo.ShowCheckBox = False
        Me.gbEmpSavingsInfo.ShowCollapseButton = False
        Me.gbEmpSavingsInfo.ShowDefaultBorderColor = True
        Me.gbEmpSavingsInfo.ShowDownButton = False
        Me.gbEmpSavingsInfo.ShowHeader = True
        Me.gbEmpSavingsInfo.Size = New System.Drawing.Size(393, 296)
        Me.gbEmpSavingsInfo.TabIndex = 0
        Me.gbEmpSavingsInfo.Temp = 0
        Me.gbEmpSavingsInfo.Text = "Employee Savings Information"
        Me.gbEmpSavingsInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOpeningContribution
        '
        Me.txtOpeningContribution.AllowNegative = True
        Me.txtOpeningContribution.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOpeningContribution.DigitsInGroup = 0
        Me.txtOpeningContribution.Flags = 0
        Me.txtOpeningContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOpeningContribution.Location = New System.Drawing.Point(132, 240)
        Me.txtOpeningContribution.MaxDecimalPlaces = 6
        Me.txtOpeningContribution.MaxWholeDigits = 21
        Me.txtOpeningContribution.Name = "txtOpeningContribution"
        Me.txtOpeningContribution.Prefix = ""
        Me.txtOpeningContribution.RangeMax = 1.7976931348623157E+308
        Me.txtOpeningContribution.RangeMin = -1.7976931348623157E+308
        Me.txtOpeningContribution.Size = New System.Drawing.Size(112, 21)
        Me.txtOpeningContribution.TabIndex = 7
        Me.txtOpeningContribution.Text = "0"
        Me.txtOpeningContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOpeningContribution
        '
        Me.lblOpeningContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpeningContribution.Location = New System.Drawing.Point(7, 242)
        Me.lblOpeningContribution.Name = "lblOpeningContribution"
        Me.lblOpeningContribution.Size = New System.Drawing.Size(119, 16)
        Me.lblOpeningContribution.TabIndex = 210
        Me.lblOpeningContribution.Text = "Opening Total Contrib."
        Me.lblOpeningContribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblExRate
        '
        Me.objlblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExRate.Location = New System.Drawing.Point(132, 219)
        Me.objlblExRate.Name = "objlblExRate"
        Me.objlblExRate.Size = New System.Drawing.Size(230, 18)
        Me.objlblExRate.TabIndex = 208
        Me.objlblExRate.Text = "1"
        Me.objlblExRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStopDate
        '
        Me.lblStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStopDate.Location = New System.Drawing.Point(7, 117)
        Me.lblStopDate.Name = "lblStopDate"
        Me.lblStopDate.Size = New System.Drawing.Size(99, 15)
        Me.lblStopDate.TabIndex = 9
        Me.lblStopDate.Text = "Stop Date"
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(132, 195)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(112, 21)
        Me.cboCurrency.TabIndex = 6
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(7, 197)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(119, 16)
        Me.lblCurrency.TabIndex = 207
        Me.lblCurrency.Text = "Contribution Currency"
        Me.lblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objSavingSchemeSearch
        '
        Me.objSavingSchemeSearch.BackColor = System.Drawing.Color.Transparent
        Me.objSavingSchemeSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objSavingSchemeSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objSavingSchemeSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objSavingSchemeSearch.BorderSelected = False
        Me.objSavingSchemeSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objSavingSchemeSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objSavingSchemeSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objSavingSchemeSearch.Location = New System.Drawing.Point(367, 168)
        Me.objSavingSchemeSearch.Name = "objSavingSchemeSearch"
        Me.objSavingSchemeSearch.Size = New System.Drawing.Size(21, 21)
        Me.objSavingSchemeSearch.TabIndex = 0
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(7, 62)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(99, 16)
        Me.lblPayPeriod.TabIndex = 7
        Me.lblPayPeriod.Text = "Pay Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Enabled = False
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(132, 60)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(112, 21)
        Me.cboPayPeriod.TabIndex = 1
        '
        'dtpStopDate
        '
        Me.dtpStopDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Checked = False
        Me.dtpStopDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStopDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStopDate.Location = New System.Drawing.Point(132, 114)
        Me.dtpStopDate.Name = "dtpStopDate"
        Me.dtpStopDate.ShowCheckBox = True
        Me.dtpStopDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpStopDate.TabIndex = 3
        '
        'cboSavingScheme
        '
        Me.cboSavingScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSavingScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSavingScheme.FormattingEnabled = True
        Me.cboSavingScheme.Items.AddRange(New Object() {"Select"})
        Me.cboSavingScheme.Location = New System.Drawing.Point(132, 168)
        Me.cboSavingScheme.Name = "cboSavingScheme"
        Me.cboSavingScheme.Size = New System.Drawing.Size(230, 21)
        Me.cboSavingScheme.TabIndex = 5
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(7, 90)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(99, 15)
        Me.lblDate.TabIndex = 8
        Me.lblDate.Text = "Start Date"
        '
        'cboEmpName
        '
        Me.cboEmpName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpName.FormattingEnabled = True
        Me.cboEmpName.Items.AddRange(New Object() {"Select"})
        Me.cboEmpName.Location = New System.Drawing.Point(132, 141)
        Me.cboEmpName.Name = "cboEmpName"
        Me.cboEmpName.Size = New System.Drawing.Size(230, 21)
        Me.cboEmpName.TabIndex = 4
        '
        'txtVoucherNo
        '
        Me.txtVoucherNo.Flags = 0
        Me.txtVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVoucherNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVoucherNo.Location = New System.Drawing.Point(132, 33)
        Me.txtVoucherNo.Name = "txtVoucherNo"
        Me.txtVoucherNo.Size = New System.Drawing.Size(112, 21)
        Me.txtVoucherNo.TabIndex = 0
        '
        'dtpDate
        '
        Me.dtpDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Checked = False
        Me.dtpDate.Enabled = False
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(132, 87)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpDate.TabIndex = 2
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(368, 141)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 10
        '
        'lblSavingScheme
        '
        Me.lblSavingScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSavingScheme.Location = New System.Drawing.Point(7, 171)
        Me.lblSavingScheme.Name = "lblSavingScheme"
        Me.lblSavingScheme.Size = New System.Drawing.Size(119, 15)
        Me.lblSavingScheme.TabIndex = 11
        Me.lblSavingScheme.Text = "Saving Scheme"
        '
        'lblEmpName
        '
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(7, 144)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(99, 15)
        Me.lblEmpName.TabIndex = 10
        Me.lblEmpName.Text = "Employee Name"
        '
        'lblVoucherNo
        '
        Me.lblVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVoucherNo.Location = New System.Drawing.Point(7, 36)
        Me.lblVoucherNo.Name = "lblVoucherNo"
        Me.lblVoucherNo.Size = New System.Drawing.Size(99, 15)
        Me.lblVoucherNo.TabIndex = 6
        Me.lblVoucherNo.Text = "Voucher No."
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(713, 58)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Add / Edit Employee Savings"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 515)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(713, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(501, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(604, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbEmpRateInfo
        '
        Me.gbEmpRateInfo.BorderColor = System.Drawing.Color.Black
        Me.gbEmpRateInfo.Checked = False
        Me.gbEmpRateInfo.CollapseAllExceptThis = False
        Me.gbEmpRateInfo.CollapsedHoverImage = Nothing
        Me.gbEmpRateInfo.CollapsedNormalImage = Nothing
        Me.gbEmpRateInfo.CollapsedPressedImage = Nothing
        Me.gbEmpRateInfo.CollapseOnLoad = False
        Me.gbEmpRateInfo.Controls.Add(Me.btnRateDelete)
        Me.gbEmpRateInfo.Controls.Add(Me.pnl_dgvIntRate)
        Me.gbEmpRateInfo.Controls.Add(Me.lnEmpRateInfo)
        Me.gbEmpRateInfo.Controls.Add(Me.dtpRateDate)
        Me.gbEmpRateInfo.Controls.Add(Me.lblRateDate)
        Me.gbEmpRateInfo.Controls.Add(Me.btnRateAdd)
        Me.gbEmpRateInfo.Controls.Add(Me.cboRatePeriod)
        Me.gbEmpRateInfo.Controls.Add(Me.lblRatePeriod)
        Me.gbEmpRateInfo.Controls.Add(Me.txtRate)
        Me.gbEmpRateInfo.Controls.Add(Me.lblRate)
        Me.gbEmpRateInfo.Controls.Add(Me.btnRateEdit)
        Me.gbEmpRateInfo.ExpandedHoverImage = Nothing
        Me.gbEmpRateInfo.ExpandedNormalImage = Nothing
        Me.gbEmpRateInfo.ExpandedPressedImage = Nothing
        Me.gbEmpRateInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmpRateInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmpRateInfo.HeaderHeight = 25
        Me.gbEmpRateInfo.HeaderMessage = ""
        Me.gbEmpRateInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbEmpRateInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmpRateInfo.HeightOnCollapse = 0
        Me.gbEmpRateInfo.LeftTextSpace = 0
        Me.gbEmpRateInfo.Location = New System.Drawing.Point(12, 362)
        Me.gbEmpRateInfo.Name = "gbEmpRateInfo"
        Me.gbEmpRateInfo.OpenHeight = 300
        Me.gbEmpRateInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmpRateInfo.ShowBorder = True
        Me.gbEmpRateInfo.ShowCheckBox = False
        Me.gbEmpRateInfo.ShowCollapseButton = False
        Me.gbEmpRateInfo.ShowDefaultBorderColor = True
        Me.gbEmpRateInfo.ShowDownButton = False
        Me.gbEmpRateInfo.ShowHeader = True
        Me.gbEmpRateInfo.Size = New System.Drawing.Size(689, 148)
        Me.gbEmpRateInfo.TabIndex = 2
        Me.gbEmpRateInfo.Temp = 0
        Me.gbEmpRateInfo.Text = "Employee Interest Rate Information"
        Me.gbEmpRateInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRateDelete
        '
        Me.btnRateDelete.BackColor = System.Drawing.Color.White
        Me.btnRateDelete.BackgroundImage = CType(resources.GetObject("btnRateDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnRateDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRateDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnRateDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRateDelete.FlatAppearance.BorderSize = 0
        Me.btnRateDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRateDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRateDelete.ForeColor = System.Drawing.Color.Black
        Me.btnRateDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRateDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnRateDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRateDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRateDelete.Location = New System.Drawing.Point(203, 112)
        Me.btnRateDelete.Name = "btnRateDelete"
        Me.btnRateDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRateDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRateDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnRateDelete.TabIndex = 39
        Me.btnRateDelete.Text = "Delete"
        Me.btnRateDelete.UseVisualStyleBackColor = True
        '
        'pnl_dgvIntRate
        '
        Me.pnl_dgvIntRate.Controls.Add(Me.lvInterestRateData)
        Me.pnl_dgvIntRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnl_dgvIntRate.Location = New System.Drawing.Point(347, 31)
        Me.pnl_dgvIntRate.Name = "pnl_dgvIntRate"
        Me.pnl_dgvIntRate.Size = New System.Drawing.Size(332, 108)
        Me.pnl_dgvIntRate.TabIndex = 37
        '
        'lvInterestRateData
        '
        Me.lvInterestRateData.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhPeriod, Me.colhEffectiveDate, Me.colhInterestRate, Me.objcolhRateMasterid, Me.objcolhRateGuid})
        Me.lvInterestRateData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvInterestRateData.FullRowSelect = True
        Me.lvInterestRateData.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvInterestRateData.Location = New System.Drawing.Point(0, 0)
        Me.lvInterestRateData.Name = "lvInterestRateData"
        Me.lvInterestRateData.Size = New System.Drawing.Size(332, 108)
        Me.lvInterestRateData.TabIndex = 22
        Me.lvInterestRateData.UseCompatibleStateImageBehavior = False
        Me.lvInterestRateData.View = System.Windows.Forms.View.Details
        '
        'colhPeriod
        '
        Me.colhPeriod.Tag = "colhRatePeriod"
        Me.colhPeriod.Text = "Period"
        Me.colhPeriod.Width = 136
        '
        'colhEffectiveDate
        '
        Me.colhEffectiveDate.Tag = "colhRateEffectiveDate"
        Me.colhEffectiveDate.Text = "Effective Date"
        Me.colhEffectiveDate.Width = 100
        '
        'colhInterestRate
        '
        Me.colhInterestRate.Tag = "colhInterestRate"
        Me.colhInterestRate.Text = "Rate %"
        Me.colhInterestRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhInterestRate.Width = 80
        '
        'objcolhRateMasterid
        '
        Me.objcolhRateMasterid.Tag = "objcolhRateMasterid"
        Me.objcolhRateMasterid.Text = ""
        Me.objcolhRateMasterid.Width = 0
        '
        'objcolhRateGuid
        '
        Me.objcolhRateGuid.Tag = "objcolhRateGuid"
        Me.objcolhRateGuid.Text = ""
        Me.objcolhRateGuid.Width = 0
        '
        'lnEmpRateInfo
        '
        Me.lnEmpRateInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmpRateInfo.Location = New System.Drawing.Point(11, 142)
        Me.lnEmpRateInfo.Name = "lnEmpRateInfo"
        Me.lnEmpRateInfo.Size = New System.Drawing.Size(370, 10)
        Me.lnEmpRateInfo.TabIndex = 7
        Me.lnEmpRateInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lnEmpRateInfo.Visible = False
        '
        'dtpRateDate
        '
        Me.dtpRateDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpRateDate.Checked = False
        Me.dtpRateDate.Enabled = False
        Me.dtpRateDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpRateDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpRateDate.Location = New System.Drawing.Point(132, 58)
        Me.dtpRateDate.Name = "dtpRateDate"
        Me.dtpRateDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpRateDate.TabIndex = 1
        '
        'lblRateDate
        '
        Me.lblRateDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRateDate.Location = New System.Drawing.Point(27, 61)
        Me.lblRateDate.Name = "lblRateDate"
        Me.lblRateDate.Size = New System.Drawing.Size(99, 15)
        Me.lblRateDate.TabIndex = 5
        Me.lblRateDate.Text = "Start Date"
        '
        'btnRateAdd
        '
        Me.btnRateAdd.BackColor = System.Drawing.Color.White
        Me.btnRateAdd.BackgroundImage = CType(resources.GetObject("btnRateAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnRateAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRateAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnRateAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRateAdd.FlatAppearance.BorderSize = 0
        Me.btnRateAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRateAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRateAdd.ForeColor = System.Drawing.Color.Black
        Me.btnRateAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRateAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnRateAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRateAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRateAdd.Location = New System.Drawing.Point(100, 112)
        Me.btnRateAdd.Name = "btnRateAdd"
        Me.btnRateAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRateAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRateAdd.Size = New System.Drawing.Size(97, 30)
        Me.btnRateAdd.TabIndex = 3
        Me.btnRateAdd.Text = "Add"
        Me.btnRateAdd.UseVisualStyleBackColor = True
        '
        'cboRatePeriod
        '
        Me.cboRatePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRatePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRatePeriod.FormattingEnabled = True
        Me.cboRatePeriod.Location = New System.Drawing.Point(132, 31)
        Me.cboRatePeriod.Name = "cboRatePeriod"
        Me.cboRatePeriod.Size = New System.Drawing.Size(112, 21)
        Me.cboRatePeriod.TabIndex = 0
        '
        'lblRatePeriod
        '
        Me.lblRatePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRatePeriod.Location = New System.Drawing.Point(27, 33)
        Me.lblRatePeriod.Name = "lblRatePeriod"
        Me.lblRatePeriod.Size = New System.Drawing.Size(99, 16)
        Me.lblRatePeriod.TabIndex = 4
        Me.lblRatePeriod.Text = "Pay Period"
        Me.lblRatePeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRate
        '
        Me.txtRate.AllowNegative = True
        Me.txtRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtRate.DigitsInGroup = 0
        Me.txtRate.Flags = 0
        Me.txtRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRate.Location = New System.Drawing.Point(132, 85)
        Me.txtRate.MaxDecimalPlaces = 6
        Me.txtRate.MaxWholeDigits = 21
        Me.txtRate.Name = "txtRate"
        Me.txtRate.Prefix = ""
        Me.txtRate.RangeMax = 100
        Me.txtRate.RangeMin = 0
        Me.txtRate.Size = New System.Drawing.Size(112, 21)
        Me.txtRate.TabIndex = 2
        Me.txtRate.Text = "0"
        Me.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblRate
        '
        Me.lblRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRate.Location = New System.Drawing.Point(27, 87)
        Me.lblRate.Name = "lblRate"
        Me.lblRate.Size = New System.Drawing.Size(99, 16)
        Me.lblRate.TabIndex = 6
        Me.lblRate.Text = "Interest Rate"
        Me.lblRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnRateEdit
        '
        Me.btnRateEdit.BackColor = System.Drawing.Color.White
        Me.btnRateEdit.BackgroundImage = CType(resources.GetObject("btnRateEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnRateEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRateEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnRateEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnRateEdit.FlatAppearance.BorderSize = 0
        Me.btnRateEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRateEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRateEdit.ForeColor = System.Drawing.Color.Black
        Me.btnRateEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRateEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnRateEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRateEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRateEdit.Location = New System.Drawing.Point(100, 112)
        Me.btnRateEdit.Name = "btnRateEdit"
        Me.btnRateEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRateEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRateEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnRateEdit.TabIndex = 35
        Me.btnRateEdit.Text = "Edit"
        Me.btnRateEdit.UseVisualStyleBackColor = True
        '
        'lblOpeningBalance
        '
        Me.lblOpeningBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpeningBalance.Location = New System.Drawing.Point(7, 269)
        Me.lblOpeningBalance.Name = "lblOpeningBalance"
        Me.lblOpeningBalance.Size = New System.Drawing.Size(119, 16)
        Me.lblOpeningBalance.TabIndex = 212
        Me.lblOpeningBalance.Text = "Opening Balance"
        Me.lblOpeningBalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblOpeningBalance.Visible = False
        '
        'txtOpeningBalance
        '
        Me.txtOpeningBalance.AllowNegative = True
        Me.txtOpeningBalance.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOpeningBalance.DigitsInGroup = 0
        Me.txtOpeningBalance.Flags = 0
        Me.txtOpeningBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOpeningBalance.Location = New System.Drawing.Point(132, 267)
        Me.txtOpeningBalance.MaxDecimalPlaces = 6
        Me.txtOpeningBalance.MaxWholeDigits = 21
        Me.txtOpeningBalance.Name = "txtOpeningBalance"
        Me.txtOpeningBalance.Prefix = ""
        Me.txtOpeningBalance.RangeMax = 1.7976931348623157E+308
        Me.txtOpeningBalance.RangeMin = -1.7976931348623157E+308
        Me.txtOpeningBalance.Size = New System.Drawing.Size(112, 21)
        Me.txtOpeningBalance.TabIndex = 8
        Me.txtOpeningBalance.Text = "0"
        Me.txtOpeningBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOpeningBalance.Visible = False
        '
        'lblWithOpeningInterest
        '
        Me.lblWithOpeningInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWithOpeningInterest.Location = New System.Drawing.Point(250, 269)
        Me.lblWithOpeningInterest.Name = "lblWithOpeningInterest"
        Me.lblWithOpeningInterest.Size = New System.Drawing.Size(137, 16)
        Me.lblWithOpeningInterest.TabIndex = 213
        Me.lblWithOpeningInterest.Text = "(With Opening Interest)"
        Me.lblWithOpeningInterest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblWithOpeningInterest.Visible = False
        '
        'frmEmployeeSavings_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(713, 570)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmployeeSavings_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Employee Savings"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbEmployeeContributionInformation.ResumeLayout(False)
        Me.gbEmployeeContributionInformation.PerformLayout()
        Me.pnl_dgvContribution.ResumeLayout(False)
        Me.gbEmpSavingsInfo.ResumeLayout(False)
        Me.gbEmpSavingsInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.gbEmpRateInfo.ResumeLayout(False)
        Me.gbEmpRateInfo.PerformLayout()
        Me.pnl_dgvIntRate.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbEmpSavingsInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblContribution As System.Windows.Forms.Label
    Friend WithEvents txtMonthlyContribution As eZee.TextBox.NumericTextBox
    Friend WithEvents cboSavingScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblSavingScheme As System.Windows.Forms.Label
    Friend WithEvents cboEmpName As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents txtVoucherNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblVoucherNo As System.Windows.Forms.Label
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents dtpStopDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblStopDate As System.Windows.Forms.Label
    Friend WithEvents objSavingSchemeSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents gbEmployeeContributionInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboContPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblContPeriod As System.Windows.Forms.Label
    Friend WithEvents gbEmpRateInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboRatePeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblRatePeriod As System.Windows.Forms.Label
    Friend WithEvents txtRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblRate As System.Windows.Forms.Label
    Friend WithEvents btnContributionAdd As eZee.Common.eZeeLightButton
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents btnRateAdd As eZee.Common.eZeeLightButton
    Friend WithEvents dtpRateDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblRateDate As System.Windows.Forms.Label
    Friend WithEvents lnEmpRateInfo As eZee.Common.eZeeLine
    Friend WithEvents btnContributionEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnRateEdit As eZee.Common.eZeeLightButton
    Friend WithEvents pnl_dgvContribution As System.Windows.Forms.Panel
    Friend WithEvents pnl_dgvIntRate As System.Windows.Forms.Panel
    Friend WithEvents lvInterestRateData As System.Windows.Forms.ListView
    Friend WithEvents colhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEffectiveDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInterestRate As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnContDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnRateDelete As eZee.Common.eZeeLightButton
    Friend WithEvents objcolhRateMasterid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhRateGuid As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvContributionData As System.Windows.Forms.ListView
    Friend WithEvents colhContPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhContribution As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhContMasterId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhContGuid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objlblExRate As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents txtOpeningContribution As eZee.TextBox.NumericTextBox
    Friend WithEvents lblOpeningContribution As System.Windows.Forms.Label
    Friend WithEvents txtOpeningBalance As eZee.TextBox.NumericTextBox
    Friend WithEvents lblOpeningBalance As System.Windows.Forms.Label
    Friend WithEvents lblWithOpeningInterest As System.Windows.Forms.Label
End Class
