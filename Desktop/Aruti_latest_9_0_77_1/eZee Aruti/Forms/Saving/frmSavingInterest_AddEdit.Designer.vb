﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSavingInterest_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSavingInterest_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbRedemptionInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtInterestRate = New eZee.TextBox.NumericTextBox
        Me.lblInterestRate = New System.Windows.Forms.Label
        Me.txtInterestForYear = New eZee.TextBox.NumericTextBox
        Me.lblInterestForYear = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbRedemptionInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbRedemptionInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(293, 164)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbRedemptionInfo
        '
        Me.gbRedemptionInfo.BorderColor = System.Drawing.Color.Black
        Me.gbRedemptionInfo.Checked = False
        Me.gbRedemptionInfo.CollapseAllExceptThis = False
        Me.gbRedemptionInfo.CollapsedHoverImage = Nothing
        Me.gbRedemptionInfo.CollapsedNormalImage = Nothing
        Me.gbRedemptionInfo.CollapsedPressedImage = Nothing
        Me.gbRedemptionInfo.CollapseOnLoad = False
        Me.gbRedemptionInfo.Controls.Add(Me.txtInterestRate)
        Me.gbRedemptionInfo.Controls.Add(Me.lblInterestRate)
        Me.gbRedemptionInfo.Controls.Add(Me.txtInterestForYear)
        Me.gbRedemptionInfo.Controls.Add(Me.lblInterestForYear)
        Me.gbRedemptionInfo.ExpandedHoverImage = Nothing
        Me.gbRedemptionInfo.ExpandedNormalImage = Nothing
        Me.gbRedemptionInfo.ExpandedPressedImage = Nothing
        Me.gbRedemptionInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRedemptionInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRedemptionInfo.HeaderHeight = 25
        Me.gbRedemptionInfo.HeightOnCollapse = 0
        Me.gbRedemptionInfo.LeftTextSpace = 0
        Me.gbRedemptionInfo.Location = New System.Drawing.Point(12, 6)
        Me.gbRedemptionInfo.Name = "gbRedemptionInfo"
        Me.gbRedemptionInfo.OpenHeight = 300
        Me.gbRedemptionInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRedemptionInfo.ShowBorder = True
        Me.gbRedemptionInfo.ShowCheckBox = False
        Me.gbRedemptionInfo.ShowCollapseButton = False
        Me.gbRedemptionInfo.ShowDefaultBorderColor = True
        Me.gbRedemptionInfo.ShowDownButton = False
        Me.gbRedemptionInfo.ShowHeader = True
        Me.gbRedemptionInfo.Size = New System.Drawing.Size(269, 91)
        Me.gbRedemptionInfo.TabIndex = 0
        Me.gbRedemptionInfo.Temp = 0
        Me.gbRedemptionInfo.Text = "Add Saving Scheme Interest"
        Me.gbRedemptionInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInterestRate
        '
        Me.txtInterestRate.AllowNegative = True
        Me.txtInterestRate.DigitsInGroup = 0
        Me.txtInterestRate.Flags = 0
        Me.txtInterestRate.Location = New System.Drawing.Point(162, 61)
        Me.txtInterestRate.MaxDecimalPlaces = 6
        Me.txtInterestRate.MaxWholeDigits = 21
        Me.txtInterestRate.Name = "txtInterestRate"
        Me.txtInterestRate.Prefix = ""
        Me.txtInterestRate.RangeMax = 1.7976931348623157E+308
        Me.txtInterestRate.RangeMin = -1.7976931348623157E+308
        Me.txtInterestRate.Size = New System.Drawing.Size(90, 21)
        Me.txtInterestRate.TabIndex = 1
        Me.txtInterestRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblInterestRate
        '
        Me.lblInterestRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestRate.Location = New System.Drawing.Point(8, 63)
        Me.lblInterestRate.Name = "lblInterestRate"
        Me.lblInterestRate.Size = New System.Drawing.Size(148, 16)
        Me.lblInterestRate.TabIndex = 9
        Me.lblInterestRate.Text = "Interest Rate (%)"
        Me.lblInterestRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInterestForYear
        '
        Me.txtInterestForYear.AllowNegative = True
        Me.txtInterestForYear.DigitsInGroup = 0
        Me.txtInterestForYear.Flags = 0
        Me.txtInterestForYear.Location = New System.Drawing.Point(162, 34)
        Me.txtInterestForYear.MaxDecimalPlaces = 6
        Me.txtInterestForYear.MaxWholeDigits = 21
        Me.txtInterestForYear.Name = "txtInterestForYear"
        Me.txtInterestForYear.Prefix = ""
        Me.txtInterestForYear.RangeMax = 1.7976931348623157E+308
        Me.txtInterestForYear.RangeMin = -1.7976931348623157E+308
        Me.txtInterestForYear.Size = New System.Drawing.Size(90, 21)
        Me.txtInterestForYear.TabIndex = 0
        Me.txtInterestForYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblInterestForYear
        '
        Me.lblInterestForYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestForYear.Location = New System.Drawing.Point(8, 36)
        Me.lblInterestForYear.Name = "lblInterestForYear"
        Me.lblInterestForYear.Size = New System.Drawing.Size(148, 16)
        Me.lblInterestForYear.TabIndex = 2
        Me.lblInterestForYear.Text = "Interest For Year"
        Me.lblInterestForYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 109)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(293, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(81, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(184, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmSavingInterest_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(293, 164)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSavingInterest_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add Saving Scheme Interest"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbRedemptionInfo.ResumeLayout(False)
        Me.gbRedemptionInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbRedemptionInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtInterestRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblInterestRate As System.Windows.Forms.Label
    Friend WithEvents txtInterestForYear As eZee.TextBox.NumericTextBox
    Friend WithEvents lblInterestForYear As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
End Class
