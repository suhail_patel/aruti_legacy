﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalAssignLoanAdvance
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalAssignLoanAdvance))
        Me.pnlMail = New System.Windows.Forms.Panel
        Me.gbLoanInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtInstallmentAmt = New eZee.TextBox.NumericTextBox
        Me.lblEMIAmount = New System.Windows.Forms.Label
        Me.lblLoanSchedule = New System.Windows.Forms.Label
        Me.cboLoanScheduleBy = New System.Windows.Forms.ComboBox
        Me.lnInstallmentInfo = New eZee.Common.eZeeLine
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.radReduceBalInterest = New System.Windows.Forms.RadioButton
        Me.radCompoundInterest = New System.Windows.Forms.RadioButton
        Me.radSimpleInterest = New System.Windows.Forms.RadioButton
        Me.lnInterestCalcType = New eZee.Common.eZeeLine
        Me.lblDuration = New System.Windows.Forms.Label
        Me.nudDuration = New System.Windows.Forms.NumericUpDown
        Me.txtLoanInterest = New eZee.TextBox.NumericTextBox
        Me.lblLoanInterest = New System.Windows.Forms.Label
        Me.txtInstallmentNo = New eZee.TextBox.NumericTextBox
        Me.lblNoOfInstallment = New System.Windows.Forms.Label
        Me.gbBasicInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtPrefix = New System.Windows.Forms.TextBox
        Me.nudManualNo = New System.Windows.Forms.NumericUpDown
        Me.lblVoucherNo = New System.Windows.Forms.Label
        Me.lblPurpose = New System.Windows.Forms.Label
        Me.txtPurpose = New eZee.TextBox.AlphanumericTextBox
        Me.cboDeductionPeriod = New System.Windows.Forms.ComboBox
        Me.lblDeductionPeriod = New System.Windows.Forms.Label
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.dgvDataList = New System.Windows.Forms.DataGridView
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboLoan = New System.Windows.Forms.ComboBox
        Me.cboApprover = New System.Windows.Forms.ComboBox
        Me.lblApprover = New System.Windows.Forms.Label
        Me.lblLoan = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboMode = New System.Windows.Forms.ComboBox
        Me.lblMode = New System.Windows.Forms.Label
        Me.cboSection = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsEx = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPendingunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApprId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMail.SuspendLayout()
        Me.gbLoanInfo.SuspendLayout()
        CType(Me.nudDuration, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbBasicInfo.SuspendLayout()
        CType(Me.nudManualNo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        CType(Me.dgvDataList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMail
        '
        Me.pnlMail.Controls.Add(Me.gbLoanInfo)
        Me.pnlMail.Controls.Add(Me.gbBasicInfo)
        Me.pnlMail.Controls.Add(Me.EZeeFooter1)
        Me.pnlMail.Controls.Add(Me.dgvDataList)
        Me.pnlMail.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMail.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMail.Location = New System.Drawing.Point(0, 0)
        Me.pnlMail.Name = "pnlMail"
        Me.pnlMail.Size = New System.Drawing.Size(894, 462)
        Me.pnlMail.TabIndex = 0
        '
        'gbLoanInfo
        '
        Me.gbLoanInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLoanInfo.Checked = False
        Me.gbLoanInfo.CollapseAllExceptThis = False
        Me.gbLoanInfo.CollapsedHoverImage = Nothing
        Me.gbLoanInfo.CollapsedNormalImage = Nothing
        Me.gbLoanInfo.CollapsedPressedImage = Nothing
        Me.gbLoanInfo.CollapseOnLoad = False
        Me.gbLoanInfo.Controls.Add(Me.txtInstallmentAmt)
        Me.gbLoanInfo.Controls.Add(Me.lblEMIAmount)
        Me.gbLoanInfo.Controls.Add(Me.lblLoanSchedule)
        Me.gbLoanInfo.Controls.Add(Me.cboLoanScheduleBy)
        Me.gbLoanInfo.Controls.Add(Me.lnInstallmentInfo)
        Me.gbLoanInfo.Controls.Add(Me.EZeeStraightLine2)
        Me.gbLoanInfo.Controls.Add(Me.radReduceBalInterest)
        Me.gbLoanInfo.Controls.Add(Me.radCompoundInterest)
        Me.gbLoanInfo.Controls.Add(Me.radSimpleInterest)
        Me.gbLoanInfo.Controls.Add(Me.lnInterestCalcType)
        Me.gbLoanInfo.Controls.Add(Me.lblDuration)
        Me.gbLoanInfo.Controls.Add(Me.nudDuration)
        Me.gbLoanInfo.Controls.Add(Me.txtLoanInterest)
        Me.gbLoanInfo.Controls.Add(Me.lblLoanInterest)
        Me.gbLoanInfo.Controls.Add(Me.txtInstallmentNo)
        Me.gbLoanInfo.Controls.Add(Me.lblNoOfInstallment)
        Me.gbLoanInfo.ExpandedHoverImage = Nothing
        Me.gbLoanInfo.ExpandedNormalImage = Nothing
        Me.gbLoanInfo.ExpandedPressedImage = Nothing
        Me.gbLoanInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLoanInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLoanInfo.HeaderHeight = 25
        Me.gbLoanInfo.HeaderMessage = ""
        Me.gbLoanInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbLoanInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLoanInfo.HeightOnCollapse = 0
        Me.gbLoanInfo.LeftTextSpace = 0
        Me.gbLoanInfo.Location = New System.Drawing.Point(440, 260)
        Me.gbLoanInfo.Name = "gbLoanInfo"
        Me.gbLoanInfo.OpenHeight = 300
        Me.gbLoanInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLoanInfo.ShowBorder = True
        Me.gbLoanInfo.ShowCheckBox = False
        Me.gbLoanInfo.ShowCollapseButton = False
        Me.gbLoanInfo.ShowDefaultBorderColor = True
        Me.gbLoanInfo.ShowDownButton = False
        Me.gbLoanInfo.ShowHeader = True
        Me.gbLoanInfo.Size = New System.Drawing.Size(451, 148)
        Me.gbLoanInfo.TabIndex = 3
        Me.gbLoanInfo.Temp = 0
        Me.gbLoanInfo.Text = "Loan Info."
        Me.gbLoanInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInstallmentAmt
        '
        Me.txtInstallmentAmt.AllowNegative = True
        Me.txtInstallmentAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInstallmentAmt.DigitsInGroup = 0
        Me.txtInstallmentAmt.Flags = 0
        Me.txtInstallmentAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInstallmentAmt.Location = New System.Drawing.Point(296, 106)
        Me.txtInstallmentAmt.MaxDecimalPlaces = 6
        Me.txtInstallmentAmt.MaxWholeDigits = 21
        Me.txtInstallmentAmt.Name = "txtInstallmentAmt"
        Me.txtInstallmentAmt.Prefix = ""
        Me.txtInstallmentAmt.RangeMax = 1.7976931348623157E+308
        Me.txtInstallmentAmt.RangeMin = -1.7976931348623157E+308
        Me.txtInstallmentAmt.Size = New System.Drawing.Size(132, 21)
        Me.txtInstallmentAmt.TabIndex = 13
        Me.txtInstallmentAmt.Text = "0"
        Me.txtInstallmentAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblEMIAmount
        '
        Me.lblEMIAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIAmount.Location = New System.Drawing.Point(196, 108)
        Me.lblEMIAmount.Name = "lblEMIAmount"
        Me.lblEMIAmount.Size = New System.Drawing.Size(96, 15)
        Me.lblEMIAmount.TabIndex = 12
        Me.lblEMIAmount.Text = "Amount"
        Me.lblEMIAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanSchedule
        '
        Me.lblLoanSchedule.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanSchedule.Location = New System.Drawing.Point(196, 82)
        Me.lblLoanSchedule.Name = "lblLoanSchedule"
        Me.lblLoanSchedule.Size = New System.Drawing.Size(96, 15)
        Me.lblLoanSchedule.TabIndex = 10
        Me.lblLoanSchedule.Text = "Loan Schedule By"
        Me.lblLoanSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanScheduleBy
        '
        Me.cboLoanScheduleBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanScheduleBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheduleBy.FormattingEnabled = True
        Me.cboLoanScheduleBy.Items.AddRange(New Object() {"Weekly", "Fortnightly", "Monthly", "Half Yearly", "Yearly"})
        Me.cboLoanScheduleBy.Location = New System.Drawing.Point(296, 79)
        Me.cboLoanScheduleBy.Name = "cboLoanScheduleBy"
        Me.cboLoanScheduleBy.Size = New System.Drawing.Size(132, 21)
        Me.cboLoanScheduleBy.TabIndex = 11
        '
        'lnInstallmentInfo
        '
        Me.lnInstallmentInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnInstallmentInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnInstallmentInfo.Location = New System.Drawing.Point(193, 60)
        Me.lnInstallmentInfo.Name = "lnInstallmentInfo"
        Me.lnInstallmentInfo.Size = New System.Drawing.Size(235, 17)
        Me.lnInstallmentInfo.TabIndex = 9
        Me.lnInstallmentInfo.Text = "Monthly Installment Information"
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(175, 61)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(12, 81)
        Me.EZeeStraightLine2.TabIndex = 8
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'radReduceBalInterest
        '
        Me.radReduceBalInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radReduceBalInterest.Location = New System.Drawing.Point(25, 125)
        Me.radReduceBalInterest.Name = "radReduceBalInterest"
        Me.radReduceBalInterest.Size = New System.Drawing.Size(144, 17)
        Me.radReduceBalInterest.TabIndex = 7
        Me.radReduceBalInterest.Text = "Reducing Balance"
        Me.radReduceBalInterest.UseVisualStyleBackColor = True
        '
        'radCompoundInterest
        '
        Me.radCompoundInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCompoundInterest.Location = New System.Drawing.Point(25, 102)
        Me.radCompoundInterest.Name = "radCompoundInterest"
        Me.radCompoundInterest.Size = New System.Drawing.Size(144, 17)
        Me.radCompoundInterest.TabIndex = 6
        Me.radCompoundInterest.Text = "Compound"
        Me.radCompoundInterest.UseVisualStyleBackColor = True
        '
        'radSimpleInterest
        '
        Me.radSimpleInterest.Checked = True
        Me.radSimpleInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radSimpleInterest.Location = New System.Drawing.Point(25, 80)
        Me.radSimpleInterest.Name = "radSimpleInterest"
        Me.radSimpleInterest.Size = New System.Drawing.Size(144, 17)
        Me.radSimpleInterest.TabIndex = 5
        Me.radSimpleInterest.TabStop = True
        Me.radSimpleInterest.Text = "Simple"
        Me.radSimpleInterest.UseVisualStyleBackColor = True
        '
        'lnInterestCalcType
        '
        Me.lnInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnInterestCalcType.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnInterestCalcType.Location = New System.Drawing.Point(8, 60)
        Me.lnInterestCalcType.Name = "lnInterestCalcType"
        Me.lnInterestCalcType.Size = New System.Drawing.Size(161, 17)
        Me.lnInterestCalcType.TabIndex = 4
        Me.lnInterestCalcType.Text = "Interest Calc. Type"
        '
        'lblDuration
        '
        Me.lblDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDuration.Location = New System.Drawing.Point(235, 36)
        Me.lblDuration.Name = "lblDuration"
        Me.lblDuration.Size = New System.Drawing.Size(113, 15)
        Me.lblDuration.TabIndex = 2
        Me.lblDuration.Text = "Duration (In Months)"
        '
        'nudDuration
        '
        Me.nudDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDuration.Location = New System.Drawing.Point(354, 33)
        Me.nudDuration.Maximum = New Decimal(New Integer() {9999999, 0, 0, 0})
        Me.nudDuration.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudDuration.Name = "nudDuration"
        Me.nudDuration.Size = New System.Drawing.Size(74, 21)
        Me.nudDuration.TabIndex = 3
        Me.nudDuration.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtLoanInterest
        '
        Me.txtLoanInterest.AllowNegative = True
        Me.txtLoanInterest.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanInterest.DigitsInGroup = 0
        Me.txtLoanInterest.Flags = 0
        Me.txtLoanInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanInterest.Location = New System.Drawing.Point(101, 33)
        Me.txtLoanInterest.MaxDecimalPlaces = 6
        Me.txtLoanInterest.MaxWholeDigits = 21
        Me.txtLoanInterest.Name = "txtLoanInterest"
        Me.txtLoanInterest.Prefix = ""
        Me.txtLoanInterest.RangeMax = 1.7976931348623157E+308
        Me.txtLoanInterest.RangeMin = -1.7976931348623157E+308
        Me.txtLoanInterest.Size = New System.Drawing.Size(68, 21)
        Me.txtLoanInterest.TabIndex = 1
        Me.txtLoanInterest.Text = "0"
        Me.txtLoanInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLoanInterest
        '
        Me.lblLoanInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanInterest.Location = New System.Drawing.Point(24, 36)
        Me.lblLoanInterest.Name = "lblLoanInterest"
        Me.lblLoanInterest.Size = New System.Drawing.Size(74, 15)
        Me.lblLoanInterest.TabIndex = 0
        Me.lblLoanInterest.Text = "Interest (%)"
        Me.lblLoanInterest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInstallmentNo
        '
        Me.txtInstallmentNo.AllowNegative = True
        Me.txtInstallmentNo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInstallmentNo.DigitsInGroup = 0
        Me.txtInstallmentNo.Flags = 0
        Me.txtInstallmentNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInstallmentNo.Location = New System.Drawing.Point(296, 106)
        Me.txtInstallmentNo.MaxDecimalPlaces = 6
        Me.txtInstallmentNo.MaxWholeDigits = 21
        Me.txtInstallmentNo.Name = "txtInstallmentNo"
        Me.txtInstallmentNo.Prefix = ""
        Me.txtInstallmentNo.RangeMax = 1.7976931348623157E+308
        Me.txtInstallmentNo.RangeMin = -1.7976931348623157E+308
        Me.txtInstallmentNo.Size = New System.Drawing.Size(132, 21)
        Me.txtInstallmentNo.TabIndex = 15
        Me.txtInstallmentNo.Text = "0"
        Me.txtInstallmentNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNoOfInstallment
        '
        Me.lblNoOfInstallment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfInstallment.Location = New System.Drawing.Point(207, 108)
        Me.lblNoOfInstallment.Name = "lblNoOfInstallment"
        Me.lblNoOfInstallment.Size = New System.Drawing.Size(83, 15)
        Me.lblNoOfInstallment.TabIndex = 14
        Me.lblNoOfInstallment.Text = "Installments"
        Me.lblNoOfInstallment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbBasicInfo
        '
        Me.gbBasicInfo.BorderColor = System.Drawing.Color.Black
        Me.gbBasicInfo.Checked = False
        Me.gbBasicInfo.CollapseAllExceptThis = False
        Me.gbBasicInfo.CollapsedHoverImage = Nothing
        Me.gbBasicInfo.CollapsedNormalImage = Nothing
        Me.gbBasicInfo.CollapsedPressedImage = Nothing
        Me.gbBasicInfo.CollapseOnLoad = False
        Me.gbBasicInfo.Controls.Add(Me.txtPrefix)
        Me.gbBasicInfo.Controls.Add(Me.nudManualNo)
        Me.gbBasicInfo.Controls.Add(Me.lblVoucherNo)
        Me.gbBasicInfo.Controls.Add(Me.lblPurpose)
        Me.gbBasicInfo.Controls.Add(Me.txtPurpose)
        Me.gbBasicInfo.Controls.Add(Me.cboDeductionPeriod)
        Me.gbBasicInfo.Controls.Add(Me.lblDeductionPeriod)
        Me.gbBasicInfo.Controls.Add(Me.dtpDate)
        Me.gbBasicInfo.Controls.Add(Me.lblEffectiveDate)
        Me.gbBasicInfo.Controls.Add(Me.cboPayPeriod)
        Me.gbBasicInfo.Controls.Add(Me.lblPeriod)
        Me.gbBasicInfo.ExpandedHoverImage = Nothing
        Me.gbBasicInfo.ExpandedNormalImage = Nothing
        Me.gbBasicInfo.ExpandedPressedImage = Nothing
        Me.gbBasicInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicInfo.HeaderHeight = 25
        Me.gbBasicInfo.HeaderMessage = ""
        Me.gbBasicInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbBasicInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicInfo.HeightOnCollapse = 0
        Me.gbBasicInfo.LeftTextSpace = 0
        Me.gbBasicInfo.Location = New System.Drawing.Point(3, 260)
        Me.gbBasicInfo.Name = "gbBasicInfo"
        Me.gbBasicInfo.OpenHeight = 300
        Me.gbBasicInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicInfo.ShowBorder = True
        Me.gbBasicInfo.ShowCheckBox = False
        Me.gbBasicInfo.ShowCollapseButton = False
        Me.gbBasicInfo.ShowDefaultBorderColor = True
        Me.gbBasicInfo.ShowDownButton = False
        Me.gbBasicInfo.ShowHeader = True
        Me.gbBasicInfo.Size = New System.Drawing.Size(431, 148)
        Me.gbBasicInfo.TabIndex = 2
        Me.gbBasicInfo.Temp = 0
        Me.gbBasicInfo.Text = "Basic Info."
        Me.gbBasicInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPrefix
        '
        Me.txtPrefix.Enabled = False
        Me.txtPrefix.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrefix.Location = New System.Drawing.Point(86, 34)
        Me.txtPrefix.MaxLength = 6
        Me.txtPrefix.Name = "txtPrefix"
        Me.txtPrefix.ReadOnly = True
        Me.txtPrefix.Size = New System.Drawing.Size(47, 21)
        Me.txtPrefix.TabIndex = 14
        Me.txtPrefix.Text = "ISGLA_"
        Me.txtPrefix.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'nudManualNo
        '
        Me.nudManualNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudManualNo.Location = New System.Drawing.Point(141, 34)
        Me.nudManualNo.Maximum = New Decimal(New Integer() {9999999, 0, 0, 0})
        Me.nudManualNo.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudManualNo.Name = "nudManualNo"
        Me.nudManualNo.Size = New System.Drawing.Size(67, 21)
        Me.nudManualNo.TabIndex = 13
        Me.nudManualNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudManualNo.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblVoucherNo
        '
        Me.lblVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVoucherNo.Location = New System.Drawing.Point(8, 36)
        Me.lblVoucherNo.Name = "lblVoucherNo"
        Me.lblVoucherNo.Size = New System.Drawing.Size(73, 16)
        Me.lblVoucherNo.TabIndex = 12
        Me.lblVoucherNo.Text = "Start Voc #"
        '
        'lblPurpose
        '
        Me.lblPurpose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPurpose.Location = New System.Drawing.Point(212, 37)
        Me.lblPurpose.Name = "lblPurpose"
        Me.lblPurpose.Size = New System.Drawing.Size(208, 16)
        Me.lblPurpose.TabIndex = 8
        Me.lblPurpose.Text = "Purpose"
        '
        'txtPurpose
        '
        Me.txtPurpose.Flags = 0
        Me.txtPurpose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPurpose.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPurpose.Location = New System.Drawing.Point(214, 61)
        Me.txtPurpose.Multiline = True
        Me.txtPurpose.Name = "txtPurpose"
        Me.txtPurpose.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtPurpose.Size = New System.Drawing.Size(208, 75)
        Me.txtPurpose.TabIndex = 9
        '
        'cboDeductionPeriod
        '
        Me.cboDeductionPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeductionPeriod.FormattingEnabled = True
        Me.cboDeductionPeriod.Location = New System.Drawing.Point(86, 115)
        Me.cboDeductionPeriod.Name = "cboDeductionPeriod"
        Me.cboDeductionPeriod.Size = New System.Drawing.Size(120, 21)
        Me.cboDeductionPeriod.TabIndex = 7
        '
        'lblDeductionPeriod
        '
        Me.lblDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeductionPeriod.Location = New System.Drawing.Point(8, 117)
        Me.lblDeductionPeriod.Name = "lblDeductionPeriod"
        Me.lblDeductionPeriod.Size = New System.Drawing.Size(77, 16)
        Me.lblDeductionPeriod.TabIndex = 6
        Me.lblDeductionPeriod.Text = "Deduction"
        Me.lblDeductionPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpDate
        '
        Me.dtpDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Checked = False
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(86, 61)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(120, 21)
        Me.dtpDate.TabIndex = 3
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(8, 63)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(73, 16)
        Me.lblEffectiveDate.TabIndex = 2
        Me.lblEffectiveDate.Text = "Date"
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(86, 88)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(120, 21)
        Me.cboPayPeriod.TabIndex = 5
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 90)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(77, 16)
        Me.lblPeriod.TabIndex = 4
        Me.lblPeriod.Text = "Assign"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnAssign)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 412)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(894, 50)
        Me.EZeeFooter1.TabIndex = 4
        '
        'btnAssign
        '
        Me.btnAssign.BackColor = System.Drawing.Color.White
        Me.btnAssign.BackgroundImage = CType(resources.GetObject("btnAssign.BackgroundImage"), System.Drawing.Image)
        Me.btnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAssign.BorderColor = System.Drawing.Color.Empty
        Me.btnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAssign.FlatAppearance.BorderSize = 0
        Me.btnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAssign.ForeColor = System.Drawing.Color.Black
        Me.btnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Location = New System.Drawing.Point(674, 11)
        Me.btnAssign.Name = "btnAssign"
        Me.btnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAssign.Size = New System.Drawing.Size(101, 30)
        Me.btnAssign.TabIndex = 0
        Me.btnAssign.Text = "&Assign"
        Me.btnAssign.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(781, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(101, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgvDataList
        '
        Me.dgvDataList.AllowUserToAddRows = False
        Me.dgvDataList.AllowUserToDeleteRows = False
        Me.dgvDataList.AllowUserToResizeColumns = False
        Me.dgvDataList.AllowUserToResizeRows = False
        Me.dgvDataList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvDataList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvDataList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvDataList.ColumnHeadersHeight = 21
        Me.dgvDataList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvDataList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollapse, Me.objdgcolhSelect, Me.dgcolhCode, Me.dgcolhEmployee, Me.dgcolhApprover, Me.dgcolhAmount, Me.objdgcolhIsGrp, Me.objdgcolhIsEx, Me.objdgcolhPendingunkid, Me.objdgcolhGrpId, Me.objdgcolhApprId, Me.objdgcolhEmpId})
        Me.dgvDataList.Location = New System.Drawing.Point(228, 3)
        Me.dgvDataList.Name = "dgvDataList"
        Me.dgvDataList.RowHeadersVisible = False
        Me.dgvDataList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvDataList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDataList.Size = New System.Drawing.Size(663, 252)
        Me.dgvDataList.TabIndex = 1
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboLoan)
        Me.gbFilterCriteria.Controls.Add(Me.cboApprover)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprover)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoan)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboMode)
        Me.gbFilterCriteria.Controls.Add(Me.lblMode)
        Me.gbFilterCriteria.Controls.Add(Me.cboSection)
        Me.gbFilterCriteria.Controls.Add(Me.lblJob)
        Me.gbFilterCriteria.Controls.Add(Me.cboJob)
        Me.gbFilterCriteria.Controls.Add(Me.lblSection)
        Me.gbFilterCriteria.Controls.Add(Me.cboBranch)
        Me.gbFilterCriteria.Controls.Add(Me.lblDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.cboDepartment)
        Me.gbFilterCriteria.Controls.Add(Me.lblBranch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(3, 3)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(219, 252)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoan
        '
        Me.cboLoan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoan.DropDownWidth = 190
        Me.cboLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoan.FormattingEnabled = True
        Me.cboLoan.Location = New System.Drawing.Point(86, 60)
        Me.cboLoan.Name = "cboLoan"
        Me.cboLoan.Size = New System.Drawing.Size(120, 21)
        Me.cboLoan.TabIndex = 3
        '
        'cboApprover
        '
        Me.cboApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprover.DropDownWidth = 190
        Me.cboApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprover.FormattingEnabled = True
        Me.cboApprover.Location = New System.Drawing.Point(86, 222)
        Me.cboApprover.Name = "cboApprover"
        Me.cboApprover.Size = New System.Drawing.Size(120, 21)
        Me.cboApprover.TabIndex = 15
        '
        'lblApprover
        '
        Me.lblApprover.BackColor = System.Drawing.Color.Transparent
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(8, 225)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(73, 15)
        Me.lblApprover.TabIndex = 14
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoan
        '
        Me.lblLoan.BackColor = System.Drawing.Color.Transparent
        Me.lblLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoan.Location = New System.Drawing.Point(8, 63)
        Me.lblLoan.Name = "lblLoan"
        Me.lblLoan.Size = New System.Drawing.Size(73, 15)
        Me.lblLoan.TabIndex = 2
        Me.lblLoan.Text = "Loan"
        Me.lblLoan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 190
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(86, 195)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(120, 21)
        Me.cboEmployee.TabIndex = 13
        '
        'lblEmployee
        '
        Me.lblEmployee.BackColor = System.Drawing.Color.Transparent
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 198)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(73, 15)
        Me.lblEmployee.TabIndex = 12
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMode
        '
        Me.cboMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMode.DropDownWidth = 190
        Me.cboMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMode.FormattingEnabled = True
        Me.cboMode.Location = New System.Drawing.Point(86, 33)
        Me.cboMode.Name = "cboMode"
        Me.cboMode.Size = New System.Drawing.Size(120, 21)
        Me.cboMode.TabIndex = 1
        '
        'lblMode
        '
        Me.lblMode.BackColor = System.Drawing.Color.Transparent
        Me.lblMode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMode.Location = New System.Drawing.Point(8, 36)
        Me.lblMode.Name = "lblMode"
        Me.lblMode.Size = New System.Drawing.Size(73, 15)
        Me.lblMode.TabIndex = 0
        Me.lblMode.Text = "Mode"
        Me.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSection
        '
        Me.cboSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSection.DropDownWidth = 190
        Me.cboSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSection.FormattingEnabled = True
        Me.cboSection.Location = New System.Drawing.Point(86, 141)
        Me.cboSection.Name = "cboSection"
        Me.cboSection.Size = New System.Drawing.Size(120, 21)
        Me.cboSection.TabIndex = 9
        '
        'lblJob
        '
        Me.lblJob.BackColor = System.Drawing.Color.Transparent
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(8, 171)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(73, 15)
        Me.lblJob.TabIndex = 10
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 190
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(86, 168)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(120, 21)
        Me.cboJob.TabIndex = 11
        '
        'lblSection
        '
        Me.lblSection.BackColor = System.Drawing.Color.Transparent
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(8, 144)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(73, 15)
        Me.lblSection.TabIndex = 8
        Me.lblSection.Text = "Section"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.DropDownWidth = 190
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(86, 87)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(120, 21)
        Me.cboBranch.TabIndex = 5
        '
        'lblDepartment
        '
        Me.lblDepartment.BackColor = System.Drawing.Color.Transparent
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 117)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(73, 15)
        Me.lblDepartment.TabIndex = 6
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.DropDownWidth = 190
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(86, 114)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(120, 21)
        Me.cboDepartment.TabIndex = 7
        '
        'lblBranch
        '
        Me.lblBranch.BackColor = System.Drawing.Color.Transparent
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 90)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(73, 15)
        Me.lblBranch.TabIndex = 4
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objdgcolhCollapse
        '
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.ReadOnly = True
        Me.objdgcolhCollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCollapse.Width = 25
        '
        'objdgcolhSelect
        '
        Me.objdgcolhSelect.HeaderText = ""
        Me.objdgcolhSelect.Name = "objdgcolhSelect"
        Me.objdgcolhSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhSelect.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhSelect.Width = 25
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn1.Width = 85
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Approver"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn4.Width = 120
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn5.Visible = False
        Me.DataGridViewTextBoxColumn5.Width = 120
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhIsEx"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhPendingunkid"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhGrpId"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhGrpId"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhApprId"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'dgcolhCode
        '
        Me.dgcolhCode.HeaderText = "Code"
        Me.dgcolhCode.Name = "dgcolhCode"
        Me.dgcolhCode.ReadOnly = True
        Me.dgcolhCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.dgcolhCode.Width = 120
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'dgcolhApprover
        '
        Me.dgcolhApprover.HeaderText = "Approver"
        Me.dgcolhApprover.Name = "dgcolhApprover"
        Me.dgcolhApprover.ReadOnly = True
        Me.dgcolhApprover.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.dgcolhApprover.Width = 130
        '
        'dgcolhAmount
        '
        Me.dgcolhAmount.HeaderText = "Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        Me.dgcolhAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.dgcolhAmount.Width = 120
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.ReadOnly = True
        Me.objdgcolhIsGrp.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhIsGrp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhIsEx
        '
        Me.objdgcolhIsEx.HeaderText = "objdgcolhIsEx"
        Me.objdgcolhIsEx.Name = "objdgcolhIsEx"
        Me.objdgcolhIsEx.ReadOnly = True
        Me.objdgcolhIsEx.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhIsEx.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhIsEx.Visible = False
        '
        'objdgcolhPendingunkid
        '
        Me.objdgcolhPendingunkid.HeaderText = "objdgcolhPendingunkid"
        Me.objdgcolhPendingunkid.Name = "objdgcolhPendingunkid"
        Me.objdgcolhPendingunkid.ReadOnly = True
        Me.objdgcolhPendingunkid.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhPendingunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhPendingunkid.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.ReadOnly = True
        Me.objdgcolhGrpId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhGrpId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhGrpId.Visible = False
        '
        'objdgcolhApprId
        '
        Me.objdgcolhApprId.HeaderText = "objdgcolhApprId"
        Me.objdgcolhApprId.Name = "objdgcolhApprId"
        Me.objdgcolhApprId.ReadOnly = True
        Me.objdgcolhApprId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhApprId.Visible = False
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.ReadOnly = True
        Me.objdgcolhEmpId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEmpId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhEmpId.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'frmGlobalAssignLoanAdvance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 462)
        Me.Controls.Add(Me.pnlMail)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalAssignLoanAdvance"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Assigned Loan / Advance"
        Me.pnlMail.ResumeLayout(False)
        Me.gbLoanInfo.ResumeLayout(False)
        Me.gbLoanInfo.PerformLayout()
        CType(Me.nudDuration, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbBasicInfo.ResumeLayout(False)
        Me.gbBasicInfo.PerformLayout()
        CType(Me.nudManualNo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        CType(Me.dgvDataList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMail As System.Windows.Forms.Panel
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboSection As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents cboLoan As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoan As System.Windows.Forms.Label
    Friend WithEvents cboMode As System.Windows.Forms.ComboBox
    Friend WithEvents lblMode As System.Windows.Forms.Label
    Friend WithEvents cboApprover As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents dgvDataList As System.Windows.Forms.DataGridView
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbLoanInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbBasicInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents cboDeductionPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblDeductionPeriod As System.Windows.Forms.Label
    Friend WithEvents lblPurpose As System.Windows.Forms.Label
    Friend WithEvents txtPurpose As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtLoanInterest As eZee.TextBox.NumericTextBox
    Friend WithEvents lblLoanInterest As System.Windows.Forms.Label
    Friend WithEvents lblDuration As System.Windows.Forms.Label
    Friend WithEvents nudDuration As System.Windows.Forms.NumericUpDown
    Friend WithEvents radReduceBalInterest As System.Windows.Forms.RadioButton
    Friend WithEvents radCompoundInterest As System.Windows.Forms.RadioButton
    Friend WithEvents radSimpleInterest As System.Windows.Forms.RadioButton
    Friend WithEvents lnInterestCalcType As eZee.Common.eZeeLine
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents lnInstallmentInfo As eZee.Common.eZeeLine
    Friend WithEvents lblLoanSchedule As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheduleBy As System.Windows.Forms.ComboBox
    Friend WithEvents txtInstallmentAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEMIAmount As System.Windows.Forms.Label
    Friend WithEvents txtInstallmentNo As eZee.TextBox.NumericTextBox
    Friend WithEvents lblNoOfInstallment As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblVoucherNo As System.Windows.Forms.Label
    Friend WithEvents nudManualNo As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtPrefix As System.Windows.Forms.TextBox
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsEx As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPendingunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApprId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
