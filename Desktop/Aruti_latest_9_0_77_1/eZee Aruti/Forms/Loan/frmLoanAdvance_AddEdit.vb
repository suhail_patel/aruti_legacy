﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Math

Public Class frmLoanAdvance_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmLoanAdvance_AddEdit"
    Private mblnCancel As Boolean = True
    Private objLoan_Advance As clsLoan_Advance
    Private menAction As enAction = enAction.ADD_ONE
    Private mintLoanAdvanceUnkid As Integer = -1
    Private mdecInterest_Rate As Decimal = 0 'Sohail (11 May 2011)
    Private mdecInterest_Amount As Decimal = 0 'Sohail (11 May 2011)
    Private mdecNetAmount As Decimal = 0 'Sohail (11 May 2011)
    Private mdecPrincipleAmount As Decimal = 0 'Sohail (11 May 2011)
    Private mdecDuration As Decimal = 0 'Sohail (11 May 2011)
    Private mintEmployeeSelectedId As Integer = -1
    Private mintApproverSeletedId As Integer = -1
    Private objProcessPendigLoanid As clsProcess_pending_loan

Private mintEmpLoyeeid As Integer = -1
    Private mintLoanSchemeId As Integer = -1
    Private mblnIsItLoan As Boolean = False
    Private mintApproverId As Integer = -1
    Private mdecApprovedAmount As Decimal = 0 'Sohail (11 May 2011)
    Private mblnIsFromAssign As Boolean = False
    Private mintProcessPendingLoanUnkid As Integer = -1


    'Sandeep [ 21 Aug 2010 ] -- Start
    Private mblnIsExternalEntity As Boolean = False
    'Sandeep [ 21 Aug 2010 ] -- End 


    'Sohail (04 Aug 2010) -- Start
    Private mdtPayPeriodStartDate As Date = Nothing
    Private mdtPayPeriodEndDate As Date = Nothing
    'Sohail (04 Aug 2010) -- End


    'S.SANDEEP [ 06 SEP 2011 ] -- START
    'ENHANCEMENT : MAKING COMMON FUNCTION
    Private blnIsFormLoad As Boolean = False
    'S.SANDEEP [ 06 SEP 2011 ] -- END 

#End Region

#Region " Properties "
    Public Property _Employeeid() As Integer
        Get
            Return mintEmpLoyeeid
        End Get
        Set(ByVal value As Integer)
            mintEmpLoyeeid = value
        End Set
    End Property

    Public Property _LoanSchemeId() As Integer
        Get
            Return mintLoanSchemeId
        End Get
        Set(ByVal value As Integer)
            mintLoanSchemeId = Value
        End Set
    End Property

    Public Property _IsItLoan() As Boolean
        Get
            Return mblnIsItLoan
        End Get
        Set(ByVal value As Boolean)
            mblnIsItLoan = Value
        End Set
    End Property

    Public Property _ApproverId() As Integer
        Get
            Return mintApproverId
        End Get
        Set(ByVal value As Integer)
            mintApproverId = value
        End Set
    End Property

    Public Property _ApprovedAmount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecApprovedAmount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecApprovedAmount = value
        End Set
    End Property

    Public Property _IsFromAssign() As Boolean
        Get
            Return mblnIsFromAssign
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromAssign = Value
        End Set
    End Property

    Public Property _IsExternalEntity() As Boolean
        Get
            Return mblnIsExternalEntity
        End Get
        Set(ByVal value As Boolean)
            mblnIsExternalEntity = value
        End Set
    End Property
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intPendingLoanUnkid As Integer = -1) As Boolean
        Try
            mintLoanAdvanceUnkid = intUnkId
            menAction = eAction
            mintProcessPendingLoanUnkid = intPendingLoanUnkid
            Me.ShowDialog()

            intUnkId = mintLoanAdvanceUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtAdvanceAmt.BackColor = GUI.ColorOptional
            txtPurpose.BackColor = GUI.ColorComp
            'txtEMIDatesAmt.BackColor = GUI.ColorOptional
            'txtEMIDays.BackColor = GUI.ColorOptional
            'txtEMIDaysAmt.BackColor = GUI.ColorOptional
            txtEMIMonths.BackColor = GUI.ColorOptional
            txtEMIMonthsAmt.BackColor = GUI.ColorOptional
            txtHistoryBalanceAmt.BackColor = GUI.ColorOptional
            txtHistoryAmount.BackColor = GUI.ColorOptional
            txtHistoryNetAmt.BackColor = GUI.ColorOptional
            txtHistoryRepaymentAmt.BackColor = GUI.ColorOptional
            txtInstallmentAmt.BackColor = GUI.ColorComp
            txtInterestAmt.BackColor = GUI.ColorComp
            txtLoanInterest.BackColor = GUI.ColorComp
            txtLoanAmt.BackColor = GUI.ColorComp
            txtNetAmount.BackColor = GUI.ColorComp
            txtNoOfEMI.BackColor = GUI.ColorOptional
            txtVoucherNo.BackColor = GUI.ColorComp
            txtApprovedBy.BackColor = GUI.ColorComp
            cboEmpName.BackColor = GUI.ColorComp
            cboPayPeriod.BackColor = GUI.ColorComp
            cboLoanScheduleBy.BackColor = GUI.ColorComp
            cboLoanScheme.BackColor = GUI.ColorComp
            nudDuration.BackColor = GUI.ColorComp
            cboDeductionPeriod.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objLoan_Advance._Isloan = gbLoanInfo.Checked
            objLoan_Advance._Advance_Amount = txtAdvanceAmt.Decimal 'Sohail (11 May 2011)
            objLoan_Advance._Approverunkid = CInt(txtApprovedBy.Tag)

            'Sandeep [ 21 Aug 2010 ] -- Start
            'objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount
            'Sandeep [ 02 Oct 2010 ] -- Start
            'If mblnIsExternalEntity = True Then
            '    objLoan_Advance._Balance_Amount = cdec(txtNetAmount.Text)
            'Else
            '    objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount
            'End If

            If mblnIsExternalEntity = True Then
                If mblnIsItLoan = True Then
                    objLoan_Advance._Balance_Amount = txtNetAmount.Decimal 'Sohail (11 May 2011)
                Else
                    objLoan_Advance._Balance_Amount = txtAdvanceAmt.Decimal 'Sohail (11 May 2011)
                End If
            Else
                objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount
            End If
            'Sandeep [ 02 Oct 2010 ] -- End 
            'Sandeep [ 21 Aug 2010 ] -- End 


            If gbLoanInfo.Checked Then
                If radSimpleInterest.Checked Then
                    objLoan_Advance._Calctype_Id = enLoanCalcId.Simple_Interest '1
                ElseIf radCompoundInterest.Checked Then
                    objLoan_Advance._Calctype_Id = enLoanCalcId.Compound_Interest   '2
                ElseIf radReduceBalInterest.Checked Then
                    objLoan_Advance._Calctype_Id = enLoanCalcId.Reducing_Amount '3
                End If
            Else
                objLoan_Advance._Calctype_Id = 0
            End If
            objLoan_Advance._Effective_Date = dtpDate.Value
            If CInt(cboLoanScheduleBy.SelectedValue) > 0 Then
                Select Case CInt(cboLoanScheduleBy.SelectedValue)
                    Case 1  'Amounts
                        objLoan_Advance._Emi_Amount = txtInstallmentAmt.Decimal 'Sohail (11 May 2011)
                        objLoan_Advance._Emi_Tenure = CInt(txtNoOfEMI.Text)
                    Case 2  'Months
                        objLoan_Advance._Emi_Amount = txtEMIMonthsAmt.Decimal 'Sohail (11 May 2011)
                        objLoan_Advance._Emi_Tenure = CInt(txtEMIMonths.Text)
                        'Case 3  'Days   
                        'objLoan_Advance._Emi_Amount = cdec(txtEMIDatesAmt.Text)
                        'objLoan_Advance._Emi_Tenure = CInt(txtEMIDays.Text)
                        'Case 4  'Dates
                        'objLoan_Advance._Emi_Amount = cdec(txtEMIDatesAmt.Text)
                        'objLoan_Advance._Emi_Tenure = 0
                        'objLoan_Advance._Emi_Start_Date = dtpEMIStart.Value
                        'objLoan_Advance._Emi_End_Date = dtpEMIEnd.Value
                End Select
            End If
            objLoan_Advance._Employeeunkid = CInt(cboEmpName.SelectedValue)
            objLoan_Advance._Interest_Amount = txtInterestAmt.Decimal 'Sohail (11 May 2011)
            objLoan_Advance._Interest_Rate = txtLoanInterest.Decimal 'Sohail (11 May 2011)
            objLoan_Advance._Isbrought_Forward = False
            objLoan_Advance._Loan_Amount = txtLoanAmt.Decimal 'Sohail (11 May 2011)
            objLoan_Advance._Loan_Duration = CInt(nudDuration.Value)
            objLoan_Advance._Loan_Purpose = txtPurpose.Text
            If gbLoanInfo.Checked = True Then
                objLoan_Advance._Loanscheduleunkid = CInt(cboLoanScheduleBy.SelectedValue)
                objLoan_Advance._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                objLoan_Advance._Net_Amount = txtNetAmount.Decimal 'Sohail (11 May 2011)
                'S.SANDEEP [ 18 JAN 2014 ] -- START
                If menAction <> enAction.EDIT_ONE Then
                    objLoan_Advance._LoanBF_Amount = txtNetAmount.Decimal
                End If
                'S.SANDEEP [ 18 JAN 2014 ] -- END
            Else
                objLoan_Advance._Loanscheduleunkid = 0
                objLoan_Advance._Loanschemeunkid = 0
                objLoan_Advance._Net_Amount = 0
            End If
            objLoan_Advance._Loanvoucher_No = txtVoucherNo.Text
            objLoan_Advance._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            If mintLoanAdvanceUnkid = -1 Then
                objLoan_Advance._Userunkid = User._Object._Userunkid
                objLoan_Advance._Voiduserunkid = -1
                objLoan_Advance._Voiddatetime = Nothing
                objLoan_Advance._Isvoid = False
            Else
                objLoan_Advance._Userunkid = objLoan_Advance._Userunkid
                objLoan_Advance._Voiduserunkid = objLoan_Advance._Voiduserunkid
                objLoan_Advance._Voiddatetime = objLoan_Advance._Voiddatetime
                objLoan_Advance._Isvoid = objLoan_Advance._Isvoid
            End If
            If mintLoanAdvanceUnkid = -1 Then
                objLoan_Advance._LoanStatus = 1
            Else
                objLoan_Advance._LoanStatus = objLoan_Advance._LoanStatus
            End If

            'Sandeep [ 21 Aug 2010 ] -- Start
            objLoan_Advance._Deductionperiodunkid = CInt(cboDeductionPeriod.SelectedValue)
            'Sandeep [ 21 Aug 2010 ] -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try

            Dim objApprove As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objApprove._Employeeunkid = objLoan_Advance._Approverunkid
            objApprove._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLoan_Advance._Approverunkid
            'S.SANDEEP [04 JUN 2015] -- END

            txtApprovedBy.Text = objApprove._Firstname & " " & objApprove._Othername & " " & objApprove._Surname
            txtApprovedBy.Tag = objLoan_Advance._Approverunkid
            objApprove = Nothing
            Select Case objLoan_Advance._Calctype_Id
                Case 1
                    radSimpleInterest.Checked = True
                Case 2
                    radCompoundInterest.Checked = True
                Case 3
                    radReduceBalInterest.Checked = True
            End Select
            If Not (objLoan_Advance._Effective_Date = Nothing) Then
                dtpDate.Value = objLoan_Advance._Effective_Date
            End If
            cboEmpName.SelectedValue = objLoan_Advance._Employeeunkid
            txtInterestAmt.Text = Format(objLoan_Advance._Interest_Amount, GUI.fmtCurrency) 'Sohail (11 May 2011)
            txtLoanInterest.Text = Format(objLoan_Advance._Interest_Rate, GUI.fmtCurrency) 'Sohail (11 May 2011)
            gbLoanInfo.Checked = objLoan_Advance._Isloan
            'chkIncludeOvertimeHours.Checked = objLoan_Advance._Isovertimehour
            'chkIncludePaidLeaveHours.Checked = objLoan_Advance._Ispaidleavehour
            'chkIncludeUnpaidLeaveHours.Checked = objLoan_Advance._Isunpaidleavehour
            txtLoanAmt.Text = Format(objLoan_Advance._Loan_Amount, GUI.fmtCurrency) 'Sohail (11 May 2011)
            txtAdvanceAmt.Text = Format(objLoan_Advance._Advance_Amount, GUI.fmtCurrency) 'Sohail (11 May 2011)
            nudDuration.Value = CDec(IIf(objLoan_Advance._Loan_Duration = 0, 1, objLoan_Advance._Loan_Duration))
            txtPurpose.Text = objLoan_Advance._Loan_Purpose
            cboLoanScheme.SelectedValue = objLoan_Advance._Loanschemeunkid
            txtVoucherNo.Text = objLoan_Advance._Loanvoucher_No
            txtNetAmount.Text = Format(objLoan_Advance._Net_Amount, GUI.fmtCurrency) 'Sohail (11 May 2011)
            cboPayPeriod.SelectedValue = objLoan_Advance._Periodunkid
            objLoan_Advance._Userunkid = objLoan_Advance._Userunkid
            objLoan_Advance._Voiduserunkid = -1
            objLoan_Advance._Voiddatetime = Nothing
            objLoan_Advance._LoanStatus = objLoan_Advance._LoanStatus
            cboLoanScheduleBy.SelectedValue = objLoan_Advance._Loanscheduleunkid
            If CInt(cboLoanScheduleBy.SelectedValue) > 0 Then
                Select Case CInt(cboLoanScheduleBy.SelectedValue)
                    Case 1  'Amounts
                        txtInstallmentAmt.Text = Format(objLoan_Advance._Emi_Amount, GUI.fmtCurrency) 'Anjan (07 May 2012)
                        txtNoOfEMI.Text = CStr(objLoan_Advance._Emi_Tenure)
                    Case 2  'Months
                        txtEMIMonthsAmt.Text = Format(objLoan_Advance._Emi_Amount, GUI.fmtCurrency) 'Sohail (11 May 2011)
                        txtEMIMonths.Text = Format(objLoan_Advance._Emi_Tenure, GUI.fmtCurrency) 'Sohail (11 May 2011)
                        'Case 3  'Days   
                        'txtEMIDatesAmt.Text = CStr(objLoan_Advance._Emi_Amount)
                        'txtEMIDays.Text = CStr(objLoan_Advance._Emi_Tenure)
                        'Case 4  'Dates
                        'txtEMIDatesAmt.Text = CStr(objLoan_Advance._Emi_Amount)
                        'If objLoan_Advance._Emi_Start_Date = Nothing Then
                        'dtpEMIStart.Checked = False
                        'Else
                        'dtpEMIStart.Value = objLoan_Advance._Emi_Start_Date
                        'End If
                        'If objLoan_Advance._Emi_End_Date = Nothing Then
                        'dtpEMIEnd.Checked = False
                        'Else
                        'dtpEMIEnd.Value = objLoan_Advance._Emi_End_Date
                        'End If
                End Select
            End If
            'Sandeep [ 21 Aug 2010 ] -- Start
            cboDeductionPeriod.SelectedValue = objLoan_Advance._Deductionperiodunkid
            'Sandeep [ 21 Aug 2010 ] -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objApprover As New clsLoan_Approver
        Dim objMasterData As New clsMasterData
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objPeriod As New clscommom_period_Tran
        Try

            'dsCombos = objApprover.GetList("Approver", True, True)
            'With cboApprovedBy
            '    .ValueMember = "approverunkid"
            '    .DisplayMember = "approvername"
            '    .DataSource = dsCombos.Tables("Approver")
            '    .SelectedValue = 0
            'End With

            'Dim strString As String = ""
            'strString = RemoveApprover(dsCombos)
            'Dim dtTable As DataTable

            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmployee.GetEmployeeList("Employee", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, True)
            'End If
            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (23 Nov 2012) -- End
            'dtTable = New DataView(dsCombos.Tables("Employee"), strString, "", DataViewRowState.CurrentRows).ToTable

            With cboEmpName
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Employee")
                .SelectedValue = 0
            End With

            dsCombos = objMasterData.GetLoanScheduling("LoanScheduling")
            With cboLoanScheduleBy
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("LoanScheduling")
                .SelectedValue = 0
            End With

            dsCombos = objLoanScheme.getComboList(True, "LoanScheme")
            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("LoanScheme")
                .SelectedValue = 0
            End With

            'Sandeep ( 18 JAN 2011 ) -- START
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 1, "PeriodList", True, enStatusType.Open)
            If menAction = enAction.EDIT_ONE Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "PeriodList", True)
                dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PeriodList", True)
                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "PeriodList", True, enStatusType.Open)
                dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "PeriodList", True, enStatusType.Open)
                'Sohail (21 Aug 2015) -- End
            End If
            'Sandeep ( 18 JAN 2011 ) -- END 
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("PeriodList").Copy
                .SelectedValue = 0
            End With

            With cboDeductionPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("PeriodList")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub



    'S.SANDEEP [ 06 SEP 2011 ] -- START
    'ENHANCEMENT : MAKING COMMON FUNCTION
    Private Sub Calculate_Loan_Amount()
        Try
            If radSimpleInterest.Checked Then
                mdecNetAmount = 0 : mdecInterest_Amount = 0
                objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Simple_Interest, txtLoanAmt.Decimal, CInt(nudDuration.Value), txtLoanInterest.Decimal, mdecNetAmount, mdecInterest_Amount)
                txtInterestAmt.Decimal = mdecInterest_Amount
                txtNetAmount.Decimal = mdecNetAmount
            ElseIf radCompoundInterest.Checked Then
                mdecNetAmount = 0 : mdecInterest_Amount = 0
                objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Compound_Interest, txtLoanAmt.Decimal, CInt(nudDuration.Value), txtLoanInterest.Decimal, mdecNetAmount, mdecInterest_Amount)
                txtInterestAmt.Decimal = mdecInterest_Amount
                txtNetAmount.Decimal = mdecNetAmount
            ElseIf radReduceBalInterest.Checked Then
                mdecNetAmount = 0 : mdecInterest_Amount = 0 : Dim mdecEMIAmt As Decimal = 0
                objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Reducing_Amount, txtLoanAmt.Decimal, CInt(nudDuration.Value), txtLoanInterest.Decimal, mdecNetAmount, mdecInterest_Amount, mdecEMIAmt)
            txtInterestAmt.Decimal = mdecInterest_Amount
            RemoveHandler txtNetAmount.TextChanged, AddressOf txtNetAmount_TextChanged
                txtNetAmount.Decimal = mdecNetAmount
            AddHandler txtNetAmount.TextChanged, AddressOf txtNetAmount_TextChanged
            If radReduceBalInterest.Checked Then
                cboLoanScheduleBy.SelectedValue = 1
                cboLoanScheduleBy.Enabled = False
                txtInstallmentAmt.Text = CStr(mdecEMIAmt)
            End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 06 SEP 2011 ] -- END 


    'Private Sub Calculate_Summary()
    '    Try
    '        mdecPrincipleAmount = txtLoanAmt.Decimal
    '        mdecDuration = nudDuration.Value
    '        mdecInterest_Rate = txtLoanInterest.Decimal

    '        'Sandeep [ 14 Oct 2010 ] -- Start
    '        'Reducing Balance Changes
    '        Dim mdecEMIAmt As Decimal = 0 'Sohail (11 May 2011)
    '        'Sandeep [ 14 Oct 2010 ] -- End 

    '        If radSimpleInterest.Checked Then

    '            'Sandeep [ 01 MARCH 2011 ] -- Start
    '            cboLoanScheduleBy.Enabled = True
    '            'Sandeep [ 01 MARCH 2011 ] -- End 

    '            'Sandeep [ 14 Oct 2010 ] -- Start
    '            'mdblInterest_Amount = (mdblPrincipleAmount * mdblInterest_Rate * (mdblDuration / 12)) / 100
    '            If mdecDuration > 12 Then
    '                mdecInterest_Amount = (mdecPrincipleAmount * mdecInterest_Rate * (mdecDuration / 12)) / 100
    '            Else

    '                'Sandeep [ 25 MARCH 2011 ] -- Start
    '                'Issue : Period Opening Balance
    '                'mdblInterest_Amount = (mdblPrincipleAmount * mdblInterest_Rate * mdblDuration) / 100
    '                mdecInterest_Amount = ((mdecPrincipleAmount * mdecInterest_Rate * mdecDuration) / 100) / 12
    '                'Sandeep [ 25 MARCH 2011 ] -- End 
    '            End If
    '            'Sandeep [ 14 Oct 2010 ] -- End 

    '        ElseIf radCompoundInterest.Checked Then

    '            'Sandeep [ 01 MARCH 2011 ] -- Start
    '            cboLoanScheduleBy.Enabled = True
    '            'Sandeep [ 01 MARCH 2011 ] -- End 

    '            'Sandeep [ 14 Oct 2010 ] -- Start
    '            'mdblInterest_Amount = mdblPrincipleAmount * Pow((1 + (mdblInterest_Rate / 100)), (mdblDuration / 12))
    '            If mdecDuration > 12 Then
    '                mdecInterest_Amount = mdecPrincipleAmount * CDec(Pow((1 + (mdecInterest_Rate / 100)), (mdecDuration / 12))) 'Sohail (11 May 2011)
    '            Else
    '                mdecInterest_Amount = mdecPrincipleAmount * CDec(Pow((1 + (mdecInterest_Rate / 100)), mdecDuration)) 'Sohail (11 May 2011)
    '            End If
    '            'Sandeep [ 14 Oct 2010 ] -- End 

    '        ElseIf radReduceBalInterest.Checked Then
    '            'Sandeep [ 14 Oct 2010 ] -- Start
    '            'Reducing Balance Changes
    '            Dim mdecMonthlyRate As Decimal = 0 'Sohail (11 May 2011)
    '            Dim mdecTotalRate As Decimal = 0 'Sohail (11 May 2011)
    '            Dim mdecNewReduceAmt As Decimal = 0 'Sohail (11 May 2011)
    '            mdecInterest_Amount = 0
    '            mdecMonthlyRate = ((mdecInterest_Rate / 12) / 100) + 1
    '            mdecTotalRate = CDec(Pow(mdecMonthlyRate, mdecDuration)) - 1 'Sohail (11 May 2011)
    '            'Sohail (11 May 2011) -- Start
    '            If mdecTotalRate > 0 Then
    '                mdecTotalRate = (mdecMonthlyRate - 1) / mdecTotalRate
    '            Else
    '                mdecTotalRate = 0
    '            End If
    '            'Sohail (11 May 2011) -- End
    '            mdecTotalRate = mdecTotalRate + (mdecMonthlyRate - 1)
    '            mdecEMIAmt = mdecPrincipleAmount * mdecTotalRate

    '            For i As Integer = 1 To CInt(mdecDuration)
    '                If i = 1 Then
    '                    mdecInterest_Amount += mdecPrincipleAmount * (mdecMonthlyRate - 1)
    '                    mdecNewReduceAmt = (mdecPrincipleAmount - (mdecEMIAmt - (mdecPrincipleAmount * (mdecMonthlyRate - 1))))
    '                Else
    '                    mdecInterest_Amount += mdecNewReduceAmt * (mdecMonthlyRate - 1)
    '                    mdecNewReduceAmt = (mdecNewReduceAmt - (mdecEMIAmt - (mdecNewReduceAmt * (mdecMonthlyRate - 1))))
    '                End If
    '            Next
    '            'Sandeep [ 14 Oct 2010 ] -- End 
    '        End If
    '        mdecNetAmount = mdecInterest_Amount + mdecPrincipleAmount
    '        txtInterestAmt.Decimal = mdecInterest_Amount

    '        'Sandeep [ 15 Oct 2010 ] -- Start
    '        'txtNetAmount.Decimal = mdblNetAmount
    '        RemoveHandler txtNetAmount.TextChanged, AddressOf txtNetAmount_TextChanged
    '        txtNetAmount.Decimal = mdecNetAmount 'Sohail (11 May 2011)
    '        AddHandler txtNetAmount.TextChanged, AddressOf txtNetAmount_TextChanged
    '        'Sandeep [ 15 Oct 2010 ] -- End 

    '        'Sandeep [ 14 Oct 2010 ] -- Start
    '        'Reducing Balance Changes
    '        If radReduceBalInterest.Checked Then
    '            cboLoanScheduleBy.SelectedValue = 1
    '            cboLoanScheduleBy.Enabled = False
    '            txtInstallmentAmt.Text = CStr(mdecEMIAmt)
    '        End If
    '        'Sandeep [ 14 Oct 2010 ] -- End 
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Calculate_Summary", mstrModuleName) 'Sohail (11 May 2011)
    '    End Try
    'End Sub



    'S.SANDEEP [ 06 SEP 2011 ] -- START
    'ENHANCEMENT : MAKING COMMON FUNCTION
    Private Sub Calculate_Loan_EMI()
        Dim mdecInstallmentAmount As Decimal = 0
        Dim mintTotalEMI As Integer = 0
            Select Case CInt(cboLoanScheduleBy.SelectedValue)
            Case 1
                objLoan_Advance.Calculate_LoanEMI(CInt(cboLoanScheduleBy.SelectedValue), txtNetAmount.Decimal, txtInstallmentAmt.Decimal, mintTotalEMI)
                txtNoOfEMI.Text = mintTotalEMI.ToString
            Case 2
                objLoan_Advance.Calculate_LoanEMI(CInt(cboLoanScheduleBy.SelectedValue), txtNetAmount.Decimal, mdecInstallmentAmount, txtEMIMonths.Int)
                'S.SANDEEP [ 24 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'txtEMIMonthsAmt.Decimal = mdecInstallmentAmount
                txtEMIMonthsAmt.Decimal = CDec(Format(mdecInstallmentAmount, GUI.fmtCurrency))
                'S.SANDEEP [ 24 MARCH 2012 ] -- END
            End Select
    End Sub

    'Private Sub Calculate_EMI()
    '    Try
    '        Dim mdecInstallmentAmount As Decimal = 0 'Sohail (11 May 2011)
    '        Dim mdecTotalEMI As Decimal = 0 'Sohail (11 May 2011)
    '        Dim mdecNetAmount As Decimal = 0 'Sohail (11 May 2011)
    '        Select Case CInt(cboLoanScheduleBy.SelectedValue)
    '            Case 1  'AMOUNT
    '                mdecNetAmount = txtNetAmount.Decimal 'Sohail (11 May 2011)
    '                mdecInstallmentAmount = txtInstallmentAmt.Decimal 'Sohail (11 May 2011)
    '                'Sohail (11 May 2011) -- Start
    '                If mdecInstallmentAmount > 0 Then
    '                    mdecTotalEMI = CDec(IIf(mdecInstallmentAmount = 0, 0, mdecNetAmount / mdecInstallmentAmount))
    '                Else
    '                    mdecTotalEMI = 0
    '                    txtNoOfEMI.Text = mdecTotalEMI.ToString
    '                    Exit Try
    '                End If
    '                'Sohail (11 May 2011) -- End
    '                'Sandeep [ 16 Oct 2010 ] -- Start
    '                'If (mdblNetAmount / mdblInstallmentAmount) > mdblTotalEMI Then
    '                If CDec(IIf(mdecInstallmentAmount = 0, 0, mdecNetAmount / mdecInstallmentAmount)) > mdecTotalEMI Then 'Sohail (11 May 2011)
    '                    'Sandeep [ 16 Oct 2010 ] -- End 
    '                    mdecTotalEMI += 1
    '                End If
    '                txtNoOfEMI.Decimal = mdecTotalEMI  'Sohail (11 May 2011)
    '            Case 2  'MONTHS
    '                mdecNetAmount = txtNetAmount.Decimal 'Sohail (11 May 2011)
    '                mdecTotalEMI = txtEMIMonths.Decimal 'Sohail (11 May 2011)
    '                'Sohail (11 May 2011) -- Start
    '                If mdecTotalEMI > 0 Then
    '                    mdecInstallmentAmount = CDec(IIf(mdecTotalEMI = 0, 0, mdecNetAmount / mdecTotalEMI))
    '                Else
    '                    mdecInstallmentAmount = 0
    '                End If
    '                'Sohail (11 May 2011) -- End
    '                txtEMIMonthsAmt.Decimal = mdecInstallmentAmount 'Sohail (11 May 2011)
    '        End Select
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Calculate_EMI", mstrModuleName)
    '    End Try
    'End Sub

    'S.SANDEEP [ 06 SEP 2011 ] -- END 

    Private Sub FillHistoryList()
        Dim dsHistory As New DataSet
        Dim decLnAmount As Decimal = 0 'Sohail (11 May 2011)
        Dim decLnNetAmount As Decimal = 0 'Sohail (11 May 2011)
        Dim decLnPiadAmount As Decimal = 0 'Sohail (11 May 2011)
        Dim decLnBalance As Decimal = 0 'Sohail (11 May 2011)
        Try
            dsHistory = objLoan_Advance.GetLoanAdvance_History_List(mintLoanAdvanceUnkid)

            lvLoanHistory.Items.Clear()

            Dim lvItem As ListViewItem

            For Each dtRow As DataRow In dsHistory.Tables(0).Rows
                lvItem = New ListViewItem

                lvItem.Text = dtRow.Item("lnScheme").ToString
                lvItem.SubItems.Add(dtRow.Item("lnVocNo").ToString)
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("lnDate").ToString).ToShortDateString)
                lvItem.SubItems.Add(Format(dtRow.Item("lnAmount"), GUI.fmtCurrency))
                lvItem.SubItems.Add(Format(dtRow.Item("lnNetAmount"), GUI.fmtCurrency))
                If CDec(dtRow.Item("lnBalance")) <= 0 Then 'Sohail (11 May 2011)
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(Format(dtRow.Item("lnPaid"), GUI.fmtCurrency))
                End If
                If CDec(dtRow.Item("lnBalance")) <= 0 Then 'Sohail (11 May 2011)
                    lvItem.SubItems.Add(Format(dtRow.Item("lnPaid"), GUI.fmtCurrency))
                Else
                    lvItem.SubItems.Add(Format(dtRow.Item("lnBalance"), GUI.fmtCurrency))
                End If
                lvItem.SubItems.Add(dtRow.Item("lnStatus").ToString)

                lvItem.Tag = dtRow.Item("lnUnkid").ToString

                decLnAmount += CDec(dtRow.Item("lnAmount")) 'Sohail (11 May 2011)
                decLnNetAmount += CDec(dtRow.Item("lnNetAmount")) 'Sohail (11 May 2011)
                If CDec(dtRow.Item("lnBalance")) <= 0 Then 'Sohail (11 May 2011)
                    decLnPiadAmount += 0
                Else
                    decLnPiadAmount += CDec(dtRow.Item("lnPaid")) 'Sohail (11 May 2011)
                End If

                If CDec(dtRow.Item("lnBalance")) <= 0 Then 'Sohail (11 May 2011)
                    decLnBalance += CDec(dtRow.Item("lnPaid")) 'Sohail (11 May 2011)
                Else
                    decLnBalance += CDec(dtRow.Item("lnBalance")) 'Sohail (11 May 2011)
                End If

                lvLoanHistory.Items.Add(lvItem)

                lvItem = Nothing
            Next

            txtHistoryAmount.Text = Format(decLnAmount, GUI.fmtCurrency) 'Sohail (11 May 2011)
            txtHistoryNetAmt.Text = Format(decLnNetAmount, GUI.fmtCurrency) 'Sohail (11 May 2011)
            txtHistoryRepaymentAmt.Text = Format(decLnPiadAmount, GUI.fmtCurrency) 'Sohail (11 May 2011)
            txtHistoryBalanceAmt.Text = Format(decLnBalance, GUI.fmtCurrency) 'Sohail (11 May 2011)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillHistoryList", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try


            'Sandeep [ 16 Oct 2010 ] -- Start
            'Issue : Auto No. Generation
            'If txtVoucherNo.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Voucher No. cannot be blank. Voucher No. is compulsory information."), enMsgBoxStyle.Information)
            '    txtVoucherNo.Focus()
            '    Return False
            'End If
            If ConfigParameter._Object._LoanVocNoType = 0 Then
                If txtVoucherNo.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Voucher No. cannot be blank. Voucher No. is compulsory information."), enMsgBoxStyle.Information)
                    txtVoucherNo.Focus()
                    Return False
                End If
            End If
            'Sandeep [ 16 Oct 2010 ] -- End 

            If CInt(cboEmpName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmpName.Focus()
                Return False
            End If

            'If CInt(cboApprovedBy.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Approver is compulsory information. Please select Approver to continue."), enMsgBoxStyle.Information)
            '    cboApprovedBy.Focus()
            '    Return False
            'End If

            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Pay Period is compulsory information. Please select Pay Period."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Return False
            End If


            If gbLoanInfo.Checked = True Then
                If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), enMsgBoxStyle.Information)
                    cboLoanScheme.Focus()
                    Return False
                End If

                If txtLoanAmt.Decimal = 0 Then 'Sohail (11 May 2011)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Loan Amount cannot be blank. Loan Amount is compulsory information."), enMsgBoxStyle.Information)
                    txtLoanAmt.Focus()
                    Return False
                End If

                'If cdec(txtLoanInterest.Text) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Loan Interest cannot be blank. Loan Interest is compulsory information."), enMsgBoxStyle.Information)
                '    txtLoanInterest.Focus()
                '    Return False
                'End If

                'Vimal (28-Aug-2010)-- Start

                If Trim(txtLoanInterest.Text) = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Loan Interest cannot be blank. Loan Interest is compulsory information."), enMsgBoxStyle.Information)
                   txtLoanInterest.Focus()
                    Return False
                ElseIf CDbl(Trim(txtLoanInterest.Text)) > 100 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Loan Interest cannot be greater than 100. Please provide proper Loan Interest."), enMsgBoxStyle.Information)
                    txtLoanInterest.Focus()
                    Return False
                End If

                'Vimal (28-Aug-2010)-- End


                If CInt(cboLoanScheduleBy.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Loan Schedule is compulsory information. Please select Loan Schedule to continue."), enMsgBoxStyle.Information)
                    cboLoanScheduleBy.Focus()
                    Return False
                End If

                Select Case CInt(cboLoanScheduleBy.SelectedValue)
                    Case 1  'AMOUNT
                        If txtInstallmentAmt.Decimal = 0 Then 'Sohail (11 May 2011)
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Installment Amount cannot be blank. Installment Amount is compulsory information."), enMsgBoxStyle.Information)
                            txtInstallmentAmt.Focus()
                            Return False
                        End If
                    Case 2  'PERIOD WISE
                        If txtEMIMonths.Decimal = 0 Then 'Sohail (11 May 2011)
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "No. of Installments cannot be blank. No. of Installments is compulsory information."), enMsgBoxStyle.Information)
                            txtEMIMonths.Focus()
                            Return False
                        End If
                End Select
            End If

            'Sandeep [ 25 MARCH 2011 ] -- Start
            'Issue : Period Opening Balance
            Select Case CInt(cboLoanScheduleBy.SelectedValue)
                Case 1    'AMOUNT
                    If txtNoOfEMI.Decimal > nudDuration.Value Then 'Sohail (11 May 2011)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "The Number of Installments cannot be greater than the loan duration."), enMsgBoxStyle.Information)
                        txtInstallmentAmt.Focus()
                        Return False
                    End If
                Case 2    'PERIODWISE
                    If txtEMIMonths.Decimal > nudDuration.Value Then 'Sohail (11 May 2011)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "The Number of Installments cannot be greater than the loan duration."), enMsgBoxStyle.Information)
                        txtEMIMonths.Focus()
                        Return False
                    End If
            End Select

            If menAction = enAction.ADD_ONE Then
10:             Select Case CInt(cboLoanScheduleBy.SelectedValue)
                    Case 1    'AMOUNT
                        If txtInstallmentAmt.Decimal > txtNetAmount.Decimal Then 'Sohail (11 May 2011)
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "The Installments Amount cannot be greater than the Net Amount."), enMsgBoxStyle.Information)
                            txtInstallmentAmt.Focus()
                            Return False
                        End If
                    Case 2    'PERIODWISE
                        If txtEMIMonthsAmt.Decimal > txtNetAmount.Decimal Then 'Sohail (11 May 2011)
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "The Installments Amount cannot be greater than the Net Amount."), enMsgBoxStyle.Information)
                            txtEMIMonths.Focus()
                            Return False
                        End If
                End Select
            ElseIf menAction = enAction.EDIT_ONE Then
                'Anjan (05 May 2011)-Start
                'Issue : Rutta wanted to increase / decrease EMI tenure or amount for loan scheme with ZERO interest rate.
                If objLoan_Advance._Interest_Amount > 0 Then
                    Select Case CInt(cboLoanScheduleBy.SelectedValue)
                        Case 1    'AMOUNT
                            If objLoan_Advance._Balance_Amount > 0 Then
                                If txtInstallmentAmt.Decimal > objLoan_Advance._Balance_Amount Then 'Sohail (11 May 2011)
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "The Installments Amount cannot be greater than the Balance Amount."), enMsgBoxStyle.Information)
                                    txtInstallmentAmt.Focus()
                                    Return False
                                End If
                            Else
                                GoTo 10
                            End If
                        Case 2    'PERIODWISE
                            If objLoan_Advance._Balance_Amount > 0 Then
                                If txtEMIMonthsAmt.Decimal > objLoan_Advance._Balance_Amount Then 'Sohail (11 May 2011)
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "The Installments Amount cannot be greater than the Balance Amount."), enMsgBoxStyle.Information)
                                    txtEMIMonths.Focus()
                                    Return False
                                End If
                            Else
                                GoTo 10
                            End If
                    End Select
                End If
                'Anjan (05 May 2011)-End
            End If

            'Sandeep [ 25 MARCH 2011 ] -- End 

            If gbAdvanceAmountInfo.Checked = True Then
                If txtAdvanceAmt.Decimal = 0 Then 'Sohail (11 May 2011)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Advance Amount cannot be blank. Advance Amount is compulsory information."), enMsgBoxStyle.Information)
                    txtAdvanceAmt.Focus()
                    Return False
                End If
            End If

            If CInt(txtApprovedBy.Tag) = mintEmployeeSelectedId Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Approver cannot approve Loan/Advance to him/herself. Please select different Approver/Employee."), enMsgBoxStyle.Information)
                cboEmpName.Focus()
                Return False
            End If

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End

            If dtpDate.Value.Date > objPeriod._End_Date Or dtpDate.Value.Date < objPeriod._Start_Date Then
                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Effective date should be in between " & objPeriod._Start_Date & " And " & objPeriod._End_Date & "."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Effective date should be in between ") & _
                                objPeriod._Start_Date & Language.getMessage(mstrModuleName, 18, " And ") & _
                                objPeriod._End_Date, enMsgBoxStyle.Information)
                'S.SANDEEP [ 20 AUG 2011 ] -- END 
                dtpDate.Focus()
                Return False
            End If

            If CInt(cboDeductionPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Deduction period is compulsory information. Please select Deduction period to continue."), enMsgBoxStyle.Information)
                cboDeductionPeriod.Focus()
                Return False
            End If


            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                Dim objDPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objDPeriod._Periodunkid = CInt(cboDeductionPeriod.SelectedValue)
                objDPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboDeductionPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End

                If objDPeriod._Start_Date < objPeriod._Start_Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Deduction period cannot be less then the Loan Period."), enMsgBoxStyle.Information)
                    cboDeductionPeriod.Focus()
                    Return False
                End If

                objDPeriod = Nothing
            End If

            objPeriod = Nothing

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            Dim objSalInc As New clsMasterData
            Dim dsCurrSal As DataSet = objSalInc.Get_Current_Scale("Sal", CInt(cboEmpName.SelectedValue), ConfigParameter._Object._CurrentDateAndTime.Date)
            If dsCurrSal.Tables("Sal").Rows.Count > 0 Then
                Select Case CInt(cboLoanScheduleBy.SelectedValue)
                    Case 1    'AMOUNT
                        If CDec(dsCurrSal.Tables("Sal").Rows(0)("newscale")) > 0 Then
                            If txtInstallmentAmt.Decimal > CDec(dsCurrSal.Tables("Sal").Rows(0)("newscale")) Then
                                'S.SANDEEP [ 03 OCT 2014 ] -- START
                                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, Installment amount cannot be greater then current scale of employee."), enMsgBoxStyle.Information)
                                'Return False 
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Installment amount is greater then current scale of employee.") & vbCrLf & _
                                                   Language.getMessage(mstrModuleName, 22, "Do you still want to assign this installment amount?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Return False
                            End If
                                'S.SANDEEP [ 03 OCT 2014 ] -- END
                            End If
                        End If
                    Case 2    'PERIODWISE
                        If CDec(dsCurrSal.Tables("Sal").Rows(0)("newscale")) > 0 Then
                            If txtEMIMonthsAmt.Decimal > CDec(dsCurrSal.Tables("Sal").Rows(0)("newscale")) Then
                                'S.SANDEEP [ 03 OCT 2014 ] -- START
                                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, Installment amount cannot be greater then current scale of employee."), enMsgBoxStyle.Information)
                                'Return False
                                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Installment amount is greater then current scale of employee.") & vbCrLf & _
                                                   Language.getMessage(mstrModuleName, 22, "Do you still want to assign this installment amount?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Return False
                            End If
                                'S.SANDEEP [ 03 OCT 2014 ] -- END
                            End If
                        End If
                End Select
            End If
            objSalInc = Nothing : dsCurrSal.Dispose()
            'S.SANDEEP [ 12 OCT 2011 ] -- END 


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function

    Private Sub FillInfo()
        Try
            cboEmpName.SelectedValue = mintEmpLoyeeid
            cboLoanScheme.SelectedValue = mintLoanSchemeId
            If mblnIsItLoan = True Then
                txtLoanAmt.Decimal = mdecApprovedAmount 'Sohail (11 May 2011)
                gbLoanInfo.Checked = True
            Else
                txtAdvanceAmt.Decimal = mdecApprovedAmount 'Sohail (11 May 2011)
                gbAdvanceAmountInfo.Checked = True
            End If
            Dim objApprove As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objApprove._Employeeunkid = mintApproverId
            objApprove._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = mintApproverId
            'S.SANDEEP [04 JUN 2015] -- END

            txtApprovedBy.Text = objApprove._Firstname & " " & objApprove._Othername & " " & objApprove._Surname
            txtApprovedBy.Tag = mintApproverId
            objApprove = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillInfo", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 16 Oct 2010 ] -- Start
    'Issue : Auto No. Generation
    Private Sub SetProcessVisibility()
        Try
            If ConfigParameter._Object._LoanVocNoType = 1 Then
                txtVoucherNo.Enabled = False
            Else
                txtVoucherNo.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetProcessVisibility", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 16 Oct 2010 ] -- End 


    'Private Function RemoveApprover(ByVal dsList As DataSet) As String
    '    Dim strQryId As String = String.Empty
    '    Try

    '        For Each drRow As DataRow In dsList.Tables(0).Rows
    '            If CInt(drRow.Item("approverunkid")) = 0 Then Continue For

    '            If strQryId.Length <= 0 Then
    '                strQryId = "employeeunkid NOT IN ( " & drRow.Item("approverunkid").ToString
    '            Else
    '                strQryId &= "," & drRow.Item("approverunkid").ToString
    '            End If
    '        Next
    '        strQryId &= " )"

    '        Return strQryId

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "RemoveApprover", mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    'Private Function RemoveApprover(ByVal dsList As DataSet) As String
    '    Dim strQryId As String = String.Empty
    '    Try

    '        For Each drRow As DataRow In dsList.Tables(0).Rows
    '            If CInt(drRow.Item("approverunkid")) = 0 Then Continue For

    '            If strQryId.Length <= 0 Then
    '                strQryId = "employeeunkid NOT IN ( " & drRow.Item("approverunkid").ToString
    '            Else
    '                strQryId &= "," & drRow.Item("approverunkid").ToString
    '            End If
    '        Next
    '        strQryId &= " )"

    '        Return strQryId

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "RemoveApprover", mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

#End Region

#Region " Form's Events "
    Private Sub frmLoanAdvance_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objLoan_Advance = Nothing
    End Sub

    Private Sub frmLoanAdvance_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmLoanAdvance_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmLoanAdvance_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLoan_Advance = New clsLoan_Advance
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetColor()

            'Sandeep [ 16 Oct 2010 ] -- Start
            'Issue : Auto No. Generation
            Call SetProcessVisibility()
            'Sandeep [ 16 Oct 2010 ] -- End 

            Call FillCombo()

            If menAction = enAction.EDIT_ONE Then
                objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceUnkid
                'Sandeep ( 18 JAN 2011 ) -- START
                If objLoan_Advance._Isloan = True Then
                    gbAdvanceAmountInfo.Enabled = False
                    gbLoanInfo.Enabled = True
                    cboPayPeriod.Enabled = False
                    cboDeductionPeriod.Enabled = False
                    cboLoanScheduleBy.Enabled = False
                    txtLoanInterest.Enabled = False
                    nudDuration.Enabled = False
                    radCompoundInterest.Enabled = False
                    radReduceBalInterest.Enabled = False
                    radSimpleInterest.Enabled = False
                Else
                    gbAdvanceAmountInfo.Enabled = True
                    gbLoanInfo.Enabled = False
                End If
                'Sandeep ( 18 JAN 2011 ) -- END
            End If

            Call GetValue()

            Call FillHistoryList()

            If mblnIsFromAssign = True Then
                Call FillInfo()
                objLoan_Advance._Processpendingloanunkid = mintProcessPendingLoanUnkid
            End If

            txtVoucherNo.Focus()

            blnIsFormLoad = True

            'S.SANDEEP [ 24 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mblnIsFromAssign = True Then
                Call nudDuration_ValueChanged(sender, e)
            End If
            'S.SANDEEP [ 24 MARCH 2012 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvance_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoan_Advance.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Advance"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            'If Trim(txtVoucherNo.Text) = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Voucher No. cannot be blank. Voucher No. is required information."), enMsgBoxStyle.Information) '?1
            '    txtVoucherNo.Focus()
            '    Exit Sub
            'End If

            'If CInt(cboPayPeriod.SelectedValue) > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Pay Period is compulsory infomation. Please select Pay Period to continue."), enMsgBoxStyle.Information)
            '    cboPayPeriod.Focus()
            '    Exit Sub
            'End If

            'If CInt(cboEmpName.SelectedValue) > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee is compulsory infomation. Please select Employee to continue."), enMsgBoxStyle.Information)
            '    cboEmpName.Focus()
            '    Exit Sub
            'End If

            'If CInt(cboApprovedBy.SelectedValue) > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Approver is compulsory infomation. Please select Approver to continue."), enMsgBoxStyle.Information)
            '    cboApprovedBy.Focus()
            '    Exit Sub
            'End If

            If IsValidate() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objLoan_Advance.Update()
            Else
                blnFlag = objLoan_Advance.Insert()
            End If

            If blnFlag = False And objLoan_Advance._Message <> "" Then
                eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objLoan_Advance = Nothing
                    objLoan_Advance = New clsLoan_Advance
                    Call GetValue()
                    txtVoucherNo.Focus()
                Else
                    mintLoanAdvanceUnkid = objLoan_Advance._Loanadvancetranunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Controls "

    Private Sub cboScheduleBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoanScheduleBy.SelectedIndexChanged
        gbEMIAmount.Visible = False
        gbEMIMonth.Visible = False
        'gbEMIDays.Visible = False
        'gbEMIDates.Visible = False
        Select Case CInt(cboLoanScheduleBy.SelectedValue)
            Case 1
                gbEMIAmount.Visible = True
                gbEMIAmount.BringToFront()
            Case 2
                gbEMIMonth.Visible = True
                gbEMIMonth.BringToFront()
                txtInstallmentAmt.Decimal = 0 'Sohail (11 May 2011)
                txtNoOfEMI.Decimal = 0 'Sohail (11 May 2011)
                'Case 3
                '    gbEMIDays.Visible = True
                '    gbEMIDays.BringToFront()
                'Case 4
                '    gbEMIDates.Visible = True
                '    gbEMIDates.BringToFront()
        End Select
    End Sub

    Private Sub gbAdvanceAmountInfo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbAdvanceAmountInfo.CheckedChanged
        gbLoanInfo.Checked = Not gbAdvanceAmountInfo.Checked
    End Sub

    Private Sub gbLoanInfo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbLoanInfo.CheckedChanged
        gbAdvanceAmountInfo.Checked = Not gbLoanInfo.Checked
    End Sub

    Private Sub txtLoanInterest_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanInterest.TextChanged
        Try
            If blnIsFormLoad = False Then Exit Sub
            Call Calculate_Loan_Amount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtLoanInterest_TextChanged", mstrModuleName)
        End Try
    End Sub

    'Private Sub txtLoanAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanAmt.TextChanged
    '    Try
    '        'Call Calculate_Summary()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtLoanAmt_TextChanged", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub txtInstallmentAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            'Call Calculate_EMI()
            If blnIsFormLoad = False Then Exit Sub
            Call Calculate_Loan_EMI()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtInstallmentAmt_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtEMIMonths_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEMIMonths.TextChanged
        Try
            'Sandeep [ 14 Oct 2010 ] -- Start
            'Reducing Balance Changes
            If txtInstallmentAmt.Text = "" Then
                Exit Sub
            ElseIf txtInstallmentAmt.Text = "." Then
                Exit Sub
            End If
            'Sandeep [ 14 Oct 2010 ] -- End 

            'Call Calculate_EMI()
            If blnIsFormLoad = False Then Exit Sub
            Call Calculate_Loan_EMI()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtEMIMonths_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radCompoundInterest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radCompoundInterest.CheckedChanged
        Try
            If blnIsFormLoad = False Then Exit Sub
            If radCompoundInterest.Checked Then
                Call Calculate_Loan_Amount()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radCompoundInterest_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub nudDuration_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudDuration.ValueChanged
        Try
            'Call Calculate_Summary()
            If blnIsFormLoad = False Then Exit Sub
            Call Calculate_Loan_Amount()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudDuration_ValueChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radReduceBalInterest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radReduceBalInterest.CheckedChanged
        'Sandeep [ 14 Oct 2010 ] -- Start
        'Reducing Balance Changes
        Try
            If radReduceBalInterest.Checked = True Then
                Call Calculate_Loan_Amount()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radReduceBalInterest_CheckedChanged", mstrModuleName)
        End Try
        'Sandeep [ 14 Oct 2010 ] -- End 
    End Sub

    Private Sub cboEmpName_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpName.SelectedIndexChanged
        Try
            mintEmployeeSelectedId = CInt(cboEmpName.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpName_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub

    'Private Sub cboApprovedBy_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        mintApproverSeletedId = CInt(cboApprovedBy.SelectedValue)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboApprovedBy_SelectionChangeCommitted", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub objbtnSearchLoanApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim objFrm As New frmCommonSearch
    '    Try
    '        With objFrm
    '            .ValueMember = cboApprovedBy.ValueMember
    '            .DisplayMember = cboApprovedBy.DisplayMember
    '            .DataSource = CType(cboApprovedBy.DataSource, DataTable)
    '            .CodeMember = "employeecode"
    '        End With

    '        If objFrm.DisplayDialog Then
    '            cboApprovedBy.SelectedValue = objFrm.SelectedValue
    '            cboApprovedBy.Focus()
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnSearchLoanApprover_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                .ValueMember = cboEmpName.ValueMember
                .DisplayMember = cboEmpName.DisplayMember
                .DataSource = CType(cboEmpName.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmpName.SelectedValue = objFrm.SelectedValue
                cboEmpName.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddLoanScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddLoanScheme.Click
        Dim frm As New frmLoanScheme_AddEdit
        Dim intLoan_SchemeId As Integer = -1
        Try
            If frm.displayDialog(intLoan_SchemeId, enAction.ADD_ONE) Then
                Dim dsCombo As New DataSet
                Dim objLoan As New clsLoan_Scheme
                dsCombo = objLoan.getComboList(True, "LoanScheme")
                With cboLoanScheme
                    .ValueMember = "loanschemeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables("LoanScheme")
                    .SelectedValue = intLoan_SchemeId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddLoanScheme_Click", mstrModuleName)
        End Try
    End Sub


    'S.SANDEEP [ 06 SEP 2011 ] -- START
    'ENHANCEMENT : MAKING COMMON FUNCTION
    Private Sub radSimpleInterest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radSimpleInterest.CheckedChanged
        Try
            If blnIsFormLoad = False Then Exit Sub
            If radSimpleInterest.Checked Then
                Call Calculate_Loan_Amount()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radSimpleInterest_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 06 SEP 2011 ] -- END 

#End Region

    'Sohail (04 Aug 2010) -- Start
#Region " ComboBox's Events "
    Private Sub cboPayPeriod_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedValueChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            mdtPayPeriodStartDate = objPeriod._Start_Date
            mdtPayPeriodEndDate = objPeriod._End_Date

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedValueChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
        End Try
    End Sub
#End Region
    'Sohail (04 Aug 2010) -- End

    'Sandeep [ 14 Oct 2010 ] -- Start
    'Reducing Balance Changes
    Private Sub txtNetAmount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNetAmount.TextChanged
        Try
            'Call Calculate_EMI()
            If blnIsFormLoad = False Then Exit Sub
            Call Calculate_Loan_EMI()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 14 Oct 2010 ] -- End 


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbLoanInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLoanInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbAdvanceAmountInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAdvanceAmountInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbLoanAdvanceInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLoanAdvanceInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbLoanAdvanceHistory.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLoanAdvanceHistory.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.tabpLoanAdvance.Text = Language._Object.getCaption(Me.tabpLoanAdvance.Name, Me.tabpLoanAdvance.Text)
			Me.gbLoanInfo.Text = Language._Object.getCaption(Me.gbLoanInfo.Name, Me.gbLoanInfo.Text)
			Me.gbEMIMonth.Text = Language._Object.getCaption(Me.gbEMIMonth.Name, Me.gbEMIMonth.Text)
			Me.lblEMIMonthsAmt.Text = Language._Object.getCaption(Me.lblEMIMonthsAmt.Name, Me.lblEMIMonthsAmt.Text)
			Me.lblNoOfInstallment.Text = Language._Object.getCaption(Me.lblNoOfInstallment.Name, Me.lblNoOfInstallment.Text)
			Me.gbEMIAmount.Text = Language._Object.getCaption(Me.gbEMIAmount.Name, Me.gbEMIAmount.Text)
			Me.lblNoOfInstallments.Text = Language._Object.getCaption(Me.lblNoOfInstallments.Name, Me.lblNoOfInstallments.Text)
			Me.lblEMIAmount.Text = Language._Object.getCaption(Me.lblEMIAmount.Name, Me.lblEMIAmount.Text)
			Me.radReduceBalInterest.Text = Language._Object.getCaption(Me.radReduceBalInterest.Name, Me.radReduceBalInterest.Text)
			Me.radCompoundInterest.Text = Language._Object.getCaption(Me.radCompoundInterest.Name, Me.radCompoundInterest.Text)
			Me.radSimpleInterest.Text = Language._Object.getCaption(Me.radSimpleInterest.Name, Me.radSimpleInterest.Text)
			Me.lnInterestCalcType.Text = Language._Object.getCaption(Me.lnInterestCalcType.Name, Me.lnInterestCalcType.Text)
			Me.lblLoanSchedule.Text = Language._Object.getCaption(Me.lblLoanSchedule.Name, Me.lblLoanSchedule.Text)
			Me.lnInstallmentInfo.Text = Language._Object.getCaption(Me.lnInstallmentInfo.Name, Me.lnInstallmentInfo.Text)
			Me.lblInterestAmt.Text = Language._Object.getCaption(Me.lblInterestAmt.Name, Me.lblInterestAmt.Text)
			Me.lblLoanInterest.Text = Language._Object.getCaption(Me.lblLoanInterest.Name, Me.lblLoanInterest.Text)
			Me.lblLoanAmt.Text = Language._Object.getCaption(Me.lblLoanAmt.Name, Me.lblLoanAmt.Text)
			Me.lblNetAmount.Text = Language._Object.getCaption(Me.lblNetAmount.Name, Me.lblNetAmount.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.gbAdvanceAmountInfo.Text = Language._Object.getCaption(Me.gbAdvanceAmountInfo.Name, Me.gbAdvanceAmountInfo.Text)
			Me.lblAdvanceAmt.Text = Language._Object.getCaption(Me.lblAdvanceAmt.Name, Me.lblAdvanceAmt.Text)
			Me.gbLoanAdvanceInfo.Text = Language._Object.getCaption(Me.gbLoanAdvanceInfo.Name, Me.gbLoanAdvanceInfo.Text)
			Me.lblPurpose.Text = Language._Object.getCaption(Me.lblPurpose.Name, Me.lblPurpose.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblApprovedBy.Text = Language._Object.getCaption(Me.lblApprovedBy.Name, Me.lblApprovedBy.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
			Me.lblVoucherNo.Text = Language._Object.getCaption(Me.lblVoucherNo.Name, Me.lblVoucherNo.Text)
			Me.tabpHostory.Text = Language._Object.getCaption(Me.tabpHostory.Name, Me.tabpHostory.Text)
			Me.gbLoanAdvanceHistory.Text = Language._Object.getCaption(Me.gbLoanAdvanceHistory.Name, Me.gbLoanAdvanceHistory.Text)
			Me.colhLoanScheme.Text = Language._Object.getCaption(CStr(Me.colhLoanScheme.Tag), Me.colhLoanScheme.Text)
			Me.colhVoucherNo.Text = Language._Object.getCaption(CStr(Me.colhVoucherNo.Tag), Me.colhVoucherNo.Text)
			Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
			Me.colhNetAmount.Text = Language._Object.getCaption(CStr(Me.colhNetAmount.Tag), Me.colhNetAmount.Text)
			Me.colhRepayment.Text = Language._Object.getCaption(CStr(Me.colhRepayment.Tag), Me.colhRepayment.Text)
			Me.colhBalanceAmt.Text = Language._Object.getCaption(CStr(Me.colhBalanceAmt.Tag), Me.colhBalanceAmt.Text)
			Me.lblHistoryBalanceAmt.Text = Language._Object.getCaption(Me.lblHistoryBalanceAmt.Name, Me.lblHistoryBalanceAmt.Text)
			Me.lblHistoryNetAMount.Text = Language._Object.getCaption(Me.lblHistoryNetAMount.Name, Me.lblHistoryNetAMount.Text)
			Me.lblHistoryRepaymentAmt.Text = Language._Object.getCaption(Me.lblHistoryRepaymentAmt.Name, Me.lblHistoryRepaymentAmt.Text)
			Me.lblHistoryAmount.Text = Language._Object.getCaption(Me.lblHistoryAmount.Name, Me.lblHistoryAmount.Text)
			Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
			Me.lblDuration.Text = Language._Object.getCaption(Me.lblDuration.Name, Me.lblDuration.Text)
			Me.lblDeductionPeriod.Text = Language._Object.getCaption(Me.lblDeductionPeriod.Name, Me.lblDeductionPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Voucher No. cannot be blank. Voucher No. is compulsory information.")
			Language.setMessage(mstrModuleName, 2, "Employee is compulsory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 3, "Pay Period is compulsory information. Please select Pay Period.")
			Language.setMessage(mstrModuleName, 4, "Loan Scheme is compulsory information. Please select Loan Scheme to continue.")
			Language.setMessage(mstrModuleName, 5, "Loan Amount cannot be blank. Loan Amount is compulsory information.")
			Language.setMessage(mstrModuleName, 6, "Loan Interest cannot be blank. Loan Interest is compulsory information.")
			Language.setMessage(mstrModuleName, 7, "Loan Schedule is compulsory information. Please select Loan Schedule to continue.")
			Language.setMessage(mstrModuleName, 8, "Installment Amount cannot be blank. Installment Amount is compulsory information.")
			Language.setMessage(mstrModuleName, 9, "No. of Installments cannot be blank. No. of Installments is compulsory information.")
			Language.setMessage(mstrModuleName, 10, "The Number of Installments cannot be greater than the loan duration.")
			Language.setMessage(mstrModuleName, 11, "The Installments Amount cannot be greater than the Net Amount.")
			Language.setMessage(mstrModuleName, 12, "The Installments Amount cannot be greater than the Balance Amount.")
			Language.setMessage(mstrModuleName, 13, "Advance Amount cannot be blank. Advance Amount is compulsory information.")
			Language.setMessage(mstrModuleName, 14, "Approver cannot approve Loan/Advance to him/herself. Please select different Approver/Employee.")
			Language.setMessage(mstrModuleName, 15, "Effective date should be in between")
			Language.setMessage(mstrModuleName, 16, "Deduction period is compulsory information. Please select Deduction period to continue.")
			Language.setMessage(mstrModuleName, 17, "Deduction period cannot be less then the Loan Period.")
			Language.setMessage(mstrModuleName, 18, " And")
			Language.setMessage(mstrModuleName, 19, "Loan Interest cannot be greater than 100. Please provide proper Loan Interest.")
			Language.setMessage(mstrModuleName, 21, "Installment amount is greater then current scale of employee.")
			Language.setMessage(mstrModuleName, 22, "Do you still want to assign this installment amount?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class