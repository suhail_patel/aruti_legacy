﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGlobalLoanStatus_Change

#Region " Private Varaibles "

    Private ReadOnly mstrModuleName As String = "frmGlobalLoanStatus_Change"
    Private objLoan_Advance As clsLoan_Advance
    Private objStatustran As clsLoan_Status_tran

#End Region

#Region " Private Function "

    Private Sub Fill_Combo()
        Dim dsCombos As New DataSet
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Try

            dsCombos = objLoanScheme.getComboList(True, "LoanScheme")
            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("LoanScheme")
                .SelectedValue = 0
            End With

            dsCombos = objMaster.GetLoan_Saving_Status("LoanStatus")
            Dim dTab1 As DataTable = New DataView(dsCombos.Tables("LoanStatus"), "Id IN(0,1,2)", "", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dTab1
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = 0
            End With

            With cboLoanAdvance
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Loan"))
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Advance"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Dim dsLoanAdvance As New DataSet
        Dim dtTable As New DataTable
        Dim StrSearching As String = String.Empty
        Try
            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                dsLoanAdvance = objLoan_Advance.GetList("Loan", , , CInt(cboStatus.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            Else
                dsLoanAdvance = objLoan_Advance.GetList("Loan")
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                StrSearching &= "AND loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " "
            End If

            If CInt(cboLoanAdvance.SelectedIndex) > 0 Then
                StrSearching &= "AND Loan_AdvanceUnkid = " & CInt(cboLoanAdvance.SelectedIndex) & " "
            End If

            If dtpDateFrom.Checked = True AndAlso dtpToDate.Checked = True Then
                StrSearching &= "AND effective_date >= '" & eZeeDate.convertDate(dtpDateFrom.Value.Date) & "' AND effective_date <= '" & eZeeDate.convertDate(dtpDateFrom.Value.Date) & "' "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsLoanAdvance.Tables("Loan"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsLoanAdvance.Tables("Loan")
            End If

            lvLoanAdvance.Items.Clear()

            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("VocNo").ToString)
                lvItem.SubItems.Add(dtRow.Item("empcode").ToString)
                lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)
                lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("effective_date").ToString).ToShortDateString)
                lvItem.SubItems.Add(dtRow.Item("LoanScheme").ToString)
                lvItem.SubItems.Add(dtRow.Item("Loan_Advance").ToString)
                lvItem.SubItems.Add(dtRow.Item("PeriodName").ToString)
                lvItem.SubItems.Add(dtRow.Item("isloan").ToString)
                lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)

                If CBool(dtRow.Item("isbrought_forward")) = True Then lvItem.ForeColor = Color.Gray

                lvItem.Tag = dtRow.Item("loanadvancetranunkid").ToString

                lvLoanAdvance.Items.Add(lvItem)
            Next

            If lvLoanAdvance.Items.Count > 16 Then
                colhEmployee.Width = 220 - 20
            Else
                colhEmployee.Width = 220
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objStatustran._Isvoid = False
            objStatustran._Loanadvancetranunkid = objLoan_Advance._Loanadvancetranunkid
            objStatustran._Remark = txtRemarks.Text
            objStatustran._Settle_Amount = 0
            objStatustran._Staus_Date = ConfigParameter._Object._CurrentDateAndTime
            objStatustran._Voiddatetime = Nothing
            objStatustran._Voiduserunkid = -1
            objStatustran._Statusunkid = CInt(cboOperation.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " From's Events "

    Private Sub frmGlobalLoanStatus_Change_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLoan_Advance = New clsLoan_Advance
        objStatustran = New clsLoan_Status_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Fill_Combo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalLoanStatus_Change_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoan_Advance.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Advance"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnflag As Boolean = False
        Try
            If CInt(cboOperation.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Change Status is mandatory information. Please select Change Status to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            If CInt(cboOperation.SelectedValue) = 3 Then
                If txtRemarks.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Remark is mandatory information. Please enter remark to continue."), enMsgBoxStyle.Information)
                    txtRemarks.Focus()
                    Exit Sub
                End If
            End If

            For Each iChkItem As ListViewItem In lvLoanAdvance.CheckedItems
                objLoan_Advance._Loanadvancetranunkid = CInt(iChkItem.Tag)
                If objStatustran IsNot Nothing Then
                    objStatustran = Nothing
                    objStatustran = New clsLoan_Status_tran
                End If
                Call SetValue()
                blnflag = objStatustran.Insert
                If blnflag = True Then
                    objLoan_Advance._LoanStatus = CInt(cboOperation.SelectedValue)
                    objLoan_Advance.Update()
                Else
                    iChkItem.ForeColor = Color.Red
                End If
            Next
            If blnflag = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Change Status process done successfully."), enMsgBoxStyle.Information)
                Call FillList()
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Change Status for some of the loan/advance did not changed and highlighted in red color."), enMsgBoxStyle.Information)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboLoanAdvance.SelectedIndex = 0
            cboLoanScheme.SelectedValue = 0
            cboPayPeriod.SelectedValue = 0
            cboStatus.SelectedValue = 0
            lvLoanAdvance.Items.Clear()
            Call objbtnReset.ShowResult(CStr(lvLoanAdvance.Items.Count))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Loan status is mandatory information. Please select loan status in order to continue."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Exit Sub
            End If
            Call FillList()
            Call objbtnSearch.ShowResult(CStr(lvLoanAdvance.Items.Count))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboStatus.SelectedValue) > 0 Then
                Dim objMaster As New clsMasterData
                Dim dsCombos As New DataSet
                dsCombos = objMaster.GetLoan_Saving_Status("LoanStatus")
                Dim dTab1 As DataTable = New DataView(dsCombos.Tables("LoanStatus"), "Id IN(0,1,2,3) AND Id <> '" & CInt(cboStatus.SelectedValue) & "'", "", DataViewRowState.CurrentRows).ToTable
                With cboOperation
                    .ValueMember = "Id"
                    .DisplayMember = "NAME"
                    .DataSource = dTab1
                    .SelectedValue = 0
                End With
                objMaster = Nothing
            Else
                cboOperation.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvLoanAdvance.ItemChecked, AddressOf lvLoanAdvance_ItemChecked
            For Each lItem As ListViewItem In lvLoanAdvance.Items
                lItem.Checked = objchkAll.Checked
            Next
            AddHandler lvLoanAdvance.ItemChecked, AddressOf lvLoanAdvance_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvLoanAdvance_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvLoanAdvance.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            If lvLoanAdvance.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvLoanAdvance.CheckedItems.Count < lvLoanAdvance.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvLoanAdvance.CheckedItems.Count = lvLoanAdvance.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If
            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvLoanAdvance_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboOperation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOperation.SelectedIndexChanged
        Try
            Select Case CInt(cboOperation.SelectedValue)
                Case 3
                    txtRemarks.Text = "" : txtRemarks.Enabled = True
                Case Else
                    txtRemarks.Text = "" : txtRemarks.Enabled = False
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOperation_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbOperation.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbOperation.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.Name, Me.lblLoanAdvance.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
			Me.lblFrmDate.Text = Language._Object.getCaption(Me.lblFrmDate.Name, Me.lblFrmDate.Text)
			Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbOperation.Text = Language._Object.getCaption(Me.gbOperation.Name, Me.gbOperation.Text)
			Me.lblOperation.Text = Language._Object.getCaption(Me.lblOperation.Name, Me.lblOperation.Text)
			Me.colhVocNo.Text = Language._Object.getCaption(CStr(Me.colhVocNo.Tag), Me.colhVocNo.Text)
			Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
			Me.colhLoanScheme.Text = Language._Object.getCaption(CStr(Me.colhLoanScheme.Tag), Me.colhLoanScheme.Text)
			Me.colhLoan_Advance.Text = Language._Object.getCaption(CStr(Me.colhLoan_Advance.Tag), Me.colhLoan_Advance.Text)
			Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
			Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Loan status is mandatory information. Please select loan status in order to continue.")
			Language.setMessage(mstrModuleName, 2, "Select")
			Language.setMessage(mstrModuleName, 3, "Loan")
			Language.setMessage(mstrModuleName, 4, "Advance")
			Language.setMessage(mstrModuleName, 5, "Change Status is mandatory information. Please select Change Status to continue.")
			Language.setMessage(mstrModuleName, 6, "Remark is mandatory information. Please enter remark to continue.")
			Language.setMessage(mstrModuleName, 7, "Change Status process done successfully.")
			Language.setMessage(mstrModuleName, 8, "Change Status for some of the loan/advance did not changed and highlighted in red color.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class