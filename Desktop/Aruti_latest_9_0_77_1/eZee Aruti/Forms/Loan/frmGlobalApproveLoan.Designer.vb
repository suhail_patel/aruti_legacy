﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalApproveLoan
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalApproveLoan))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboApprover = New System.Windows.Forms.ComboBox
        Me.lblApprover = New System.Windows.Forms.Label
        Me.gbFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboSection = New System.Windows.Forms.ComboBox
        Me.lblSection = New System.Windows.Forms.Label
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblJob = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.cboDepartment = New System.Windows.Forms.ComboBox
        Me.lblDepartment = New System.Windows.Forms.Label
        Me.gbPending = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radAll = New System.Windows.Forms.RadioButton
        Me.radAdvance = New System.Windows.Forms.RadioButton
        Me.radLoan = New System.Windows.Forms.RadioButton
        Me.pnlGrid = New System.Windows.Forms.Panel
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAppNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPendingUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.gbInfo.SuspendLayout()
        Me.gbFilter.SuspendLayout()
        Me.gbPending.SuspendLayout()
        Me.pnlGrid.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbInfo)
        Me.pnlMain.Controls.Add(Me.gbFilter)
        Me.pnlMain.Controls.Add(Me.gbPending)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(751, 423)
        Me.pnlMain.TabIndex = 0
        '
        'gbInfo
        '
        Me.gbInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInfo.Checked = False
        Me.gbInfo.CollapseAllExceptThis = False
        Me.gbInfo.CollapsedHoverImage = Nothing
        Me.gbInfo.CollapsedNormalImage = Nothing
        Me.gbInfo.CollapsedPressedImage = Nothing
        Me.gbInfo.CollapseOnLoad = False
        Me.gbInfo.Controls.Add(Me.cboStatus)
        Me.gbInfo.Controls.Add(Me.lblStatus)
        Me.gbInfo.Controls.Add(Me.cboApprover)
        Me.gbInfo.Controls.Add(Me.lblApprover)
        Me.gbInfo.ExpandedHoverImage = Nothing
        Me.gbInfo.ExpandedNormalImage = Nothing
        Me.gbInfo.ExpandedPressedImage = Nothing
        Me.gbInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInfo.HeaderHeight = 25
        Me.gbInfo.HeaderMessage = ""
        Me.gbInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInfo.HeightOnCollapse = 0
        Me.gbInfo.LeftTextSpace = 0
        Me.gbInfo.Location = New System.Drawing.Point(480, 3)
        Me.gbInfo.Name = "gbInfo"
        Me.gbInfo.OpenHeight = 300
        Me.gbInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInfo.ShowBorder = True
        Me.gbInfo.ShowCheckBox = False
        Me.gbInfo.ShowCollapseButton = False
        Me.gbInfo.ShowDefaultBorderColor = True
        Me.gbInfo.ShowDownButton = False
        Me.gbInfo.ShowHeader = True
        Me.gbInfo.Size = New System.Drawing.Size(268, 88)
        Me.gbInfo.TabIndex = 4
        Me.gbInfo.Temp = 0
        Me.gbInfo.Text = "Information"
        Me.gbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(83, 60)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(173, 21)
        Me.cboStatus.TabIndex = 8
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(8, 63)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(69, 15)
        Me.lblStatus.TabIndex = 7
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApprover
        '
        Me.cboApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprover.FormattingEnabled = True
        Me.cboApprover.Location = New System.Drawing.Point(83, 33)
        Me.cboApprover.Name = "cboApprover"
        Me.cboApprover.Size = New System.Drawing.Size(173, 21)
        Me.cboApprover.TabIndex = 5
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(8, 36)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(69, 15)
        Me.lblApprover.TabIndex = 3
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbFilter
        '
        Me.gbFilter.BorderColor = System.Drawing.Color.Black
        Me.gbFilter.Checked = False
        Me.gbFilter.CollapseAllExceptThis = False
        Me.gbFilter.CollapsedHoverImage = Nothing
        Me.gbFilter.CollapsedNormalImage = Nothing
        Me.gbFilter.CollapsedPressedImage = Nothing
        Me.gbFilter.CollapseOnLoad = False
        Me.gbFilter.Controls.Add(Me.cboSection)
        Me.gbFilter.Controls.Add(Me.lblSection)
        Me.gbFilter.Controls.Add(Me.cboJob)
        Me.gbFilter.Controls.Add(Me.lblJob)
        Me.gbFilter.Controls.Add(Me.cboBranch)
        Me.gbFilter.Controls.Add(Me.lblBranch)
        Me.gbFilter.Controls.Add(Me.cboDepartment)
        Me.gbFilter.Controls.Add(Me.lblDepartment)
        Me.gbFilter.ExpandedHoverImage = Nothing
        Me.gbFilter.ExpandedNormalImage = Nothing
        Me.gbFilter.ExpandedPressedImage = Nothing
        Me.gbFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilter.HeaderHeight = 25
        Me.gbFilter.HeaderMessage = ""
        Me.gbFilter.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilter.HeightOnCollapse = 0
        Me.gbFilter.LeftTextSpace = 0
        Me.gbFilter.Location = New System.Drawing.Point(3, 3)
        Me.gbFilter.Name = "gbFilter"
        Me.gbFilter.OpenHeight = 300
        Me.gbFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilter.ShowBorder = True
        Me.gbFilter.ShowCheckBox = False
        Me.gbFilter.ShowCollapseButton = False
        Me.gbFilter.ShowDefaultBorderColor = True
        Me.gbFilter.ShowDownButton = False
        Me.gbFilter.ShowHeader = True
        Me.gbFilter.Size = New System.Drawing.Size(471, 88)
        Me.gbFilter.TabIndex = 3
        Me.gbFilter.Temp = 0
        Me.gbFilter.Text = "Filter Criteria"
        Me.gbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSection
        '
        Me.cboSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSection.DropDownWidth = 190
        Me.cboSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSection.FormattingEnabled = True
        Me.cboSection.Location = New System.Drawing.Point(313, 32)
        Me.cboSection.Name = "cboSection"
        Me.cboSection.Size = New System.Drawing.Size(146, 21)
        Me.cboSection.TabIndex = 12
        '
        'lblSection
        '
        Me.lblSection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSection.Location = New System.Drawing.Point(240, 35)
        Me.lblSection.Name = "lblSection"
        Me.lblSection.Size = New System.Drawing.Size(67, 15)
        Me.lblSection.TabIndex = 11
        Me.lblSection.Text = "Section"
        Me.lblSection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 190
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(313, 59)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(146, 21)
        Me.cboJob.TabIndex = 10
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(240, 62)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(67, 15)
        Me.lblJob.TabIndex = 9
        Me.lblJob.Text = "Job"
        Me.lblJob.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.DropDownWidth = 190
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(80, 33)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(146, 21)
        Me.cboBranch.TabIndex = 8
        '
        'lblBranch
        '
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 36)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(66, 15)
        Me.lblBranch.TabIndex = 7
        Me.lblBranch.Text = "Branch"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDepartment
        '
        Me.cboDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartment.DropDownWidth = 190
        Me.cboDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDepartment.FormattingEnabled = True
        Me.cboDepartment.Location = New System.Drawing.Point(80, 60)
        Me.cboDepartment.Name = "cboDepartment"
        Me.cboDepartment.Size = New System.Drawing.Size(146, 21)
        Me.cboDepartment.TabIndex = 6
        '
        'lblDepartment
        '
        Me.lblDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDepartment.Location = New System.Drawing.Point(8, 63)
        Me.lblDepartment.Name = "lblDepartment"
        Me.lblDepartment.Size = New System.Drawing.Size(66, 15)
        Me.lblDepartment.TabIndex = 4
        Me.lblDepartment.Text = "Department"
        Me.lblDepartment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbPending
        '
        Me.gbPending.BorderColor = System.Drawing.Color.Black
        Me.gbPending.Checked = False
        Me.gbPending.CollapseAllExceptThis = False
        Me.gbPending.CollapsedHoverImage = Nothing
        Me.gbPending.CollapsedNormalImage = Nothing
        Me.gbPending.CollapsedPressedImage = Nothing
        Me.gbPending.CollapseOnLoad = False
        Me.gbPending.Controls.Add(Me.radAll)
        Me.gbPending.Controls.Add(Me.radAdvance)
        Me.gbPending.Controls.Add(Me.radLoan)
        Me.gbPending.Controls.Add(Me.pnlGrid)
        Me.gbPending.ExpandedHoverImage = Nothing
        Me.gbPending.ExpandedNormalImage = Nothing
        Me.gbPending.ExpandedPressedImage = Nothing
        Me.gbPending.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPending.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPending.HeaderHeight = 25
        Me.gbPending.HeaderMessage = ""
        Me.gbPending.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPending.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPending.HeightOnCollapse = 0
        Me.gbPending.LeftTextSpace = 0
        Me.gbPending.Location = New System.Drawing.Point(3, 97)
        Me.gbPending.Name = "gbPending"
        Me.gbPending.OpenHeight = 300
        Me.gbPending.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPending.ShowBorder = True
        Me.gbPending.ShowCheckBox = False
        Me.gbPending.ShowCollapseButton = False
        Me.gbPending.ShowDefaultBorderColor = True
        Me.gbPending.ShowDownButton = False
        Me.gbPending.ShowHeader = True
        Me.gbPending.Size = New System.Drawing.Size(745, 268)
        Me.gbPending.TabIndex = 2
        Me.gbPending.Temp = 0
        Me.gbPending.Text = "Pending Loan/Advance Application(s)."
        Me.gbPending.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radAll
        '
        Me.radAll.BackColor = System.Drawing.Color.Transparent
        Me.radAll.Location = New System.Drawing.Point(651, 4)
        Me.radAll.Name = "radAll"
        Me.radAll.Size = New System.Drawing.Size(90, 17)
        Me.radAll.TabIndex = 5
        Me.radAll.TabStop = True
        Me.radAll.Text = "All"
        Me.radAll.UseVisualStyleBackColor = False
        '
        'radAdvance
        '
        Me.radAdvance.BackColor = System.Drawing.Color.Transparent
        Me.radAdvance.Location = New System.Drawing.Point(555, 4)
        Me.radAdvance.Name = "radAdvance"
        Me.radAdvance.Size = New System.Drawing.Size(90, 17)
        Me.radAdvance.TabIndex = 4
        Me.radAdvance.TabStop = True
        Me.radAdvance.Text = "Advance"
        Me.radAdvance.UseVisualStyleBackColor = False
        '
        'radLoan
        '
        Me.radLoan.BackColor = System.Drawing.Color.Transparent
        Me.radLoan.Location = New System.Drawing.Point(459, 4)
        Me.radLoan.Name = "radLoan"
        Me.radLoan.Size = New System.Drawing.Size(90, 17)
        Me.radLoan.TabIndex = 4
        Me.radLoan.TabStop = True
        Me.radLoan.Text = "Loan"
        Me.radLoan.UseVisualStyleBackColor = False
        '
        'pnlGrid
        '
        Me.pnlGrid.Controls.Add(Me.dgvData)
        Me.pnlGrid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlGrid.Location = New System.Drawing.Point(2, 26)
        Me.pnlGrid.Name = "pnlGrid"
        Me.pnlGrid.Size = New System.Drawing.Size(740, 240)
        Me.pnlGrid.TabIndex = 2
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeight = 22
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollapse, Me.objdgcolhCheck, Me.dgcolhEcode, Me.dgcolhEName, Me.dgcolhAppNo, Me.dgcolhRAmount, Me.dgcolhAAmount, Me.objcolhPendingUnkid, Me.objdgcolhIsGrp, Me.objdgcolhGrpId})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(740, 240)
        Me.dgvData.TabIndex = 1
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 368)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(751, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(553, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(649, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 85
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Mode"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn4.HeaderText = "Application No"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 80
        '
        'DataGridViewTextBoxColumn5
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn5.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 110
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn6.HeaderText = "Approved Amount"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        Me.DataGridViewTextBoxColumn6.Width = 110
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objcolhPendingUnkid"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhGrpId"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'objdgcolhCollapse
        '
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.ReadOnly = True
        Me.objdgcolhCollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhCollapse.Width = 25
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 125
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAppNo
        '
        Me.dgcolhAppNo.HeaderText = "Application No"
        Me.dgcolhAppNo.Name = "dgcolhAppNo"
        Me.dgcolhAppNo.ReadOnly = True
        Me.dgcolhAppNo.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhAppNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAppNo.Width = 80
        '
        'dgcolhRAmount
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhRAmount.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhRAmount.HeaderText = "Amount"
        Me.dgcolhRAmount.Name = "dgcolhRAmount"
        Me.dgcolhRAmount.ReadOnly = True
        Me.dgcolhRAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhRAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAAmount
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhAAmount.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhAAmount.HeaderText = "Approved Amount"
        Me.dgcolhAAmount.Name = "dgcolhAAmount"
        Me.dgcolhAAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhAAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAAmount.Width = 110
        '
        'objcolhPendingUnkid
        '
        Me.objcolhPendingUnkid.HeaderText = "objcolhPendingUnkid"
        Me.objcolhPendingUnkid.Name = "objcolhPendingUnkid"
        Me.objcolhPendingUnkid.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhPendingUnkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhPendingUnkid.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Visible = False
        '
        'frmGlobalApproveLoan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(751, 423)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalApproveLoan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Approve Loan"
        Me.pnlMain.ResumeLayout(False)
        Me.gbInfo.ResumeLayout(False)
        Me.gbFilter.ResumeLayout(False)
        Me.gbPending.ResumeLayout(False)
        Me.pnlGrid.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbPending As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents gbFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboApprover As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents lblDepartment As System.Windows.Forms.Label
    Friend WithEvents cboSection As System.Windows.Forms.ComboBox
    Friend WithEvents lblSection As System.Windows.Forms.Label
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents lblBranch As System.Windows.Forms.Label
    Friend WithEvents cboDepartment As System.Windows.Forms.ComboBox
    Friend WithEvents pnlGrid As System.Windows.Forms.Panel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents radAdvance As System.Windows.Forms.RadioButton
    Friend WithEvents radLoan As System.Windows.Forms.RadioButton
    Friend WithEvents radAll As System.Windows.Forms.RadioButton
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAppNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPendingUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
