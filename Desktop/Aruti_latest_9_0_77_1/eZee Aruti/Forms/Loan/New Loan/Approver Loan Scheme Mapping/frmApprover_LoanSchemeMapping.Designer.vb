﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApprover_LoanSchemeMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmApprover_LoanSchemeMapping))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.pnlLeaveType = New System.Windows.Forms.Panel
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgLoanScheme = New System.Windows.Forms.DataGridView
        Me.objdgcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhSchemeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLoanScheme = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhLoanSchemeMappingunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbLeavedetail = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objlblApprover = New System.Windows.Forms.Label
        Me.lblApprover = New System.Windows.Forms.Label
        Me.objlblApproverLevel = New System.Windows.Forms.Label
        Me.lblApproverLevel = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOK = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtSearchScheme = New eZee.TextBox.AlphanumericTextBox
        Me.pnlMain.SuspendLayout()
        Me.pnlLeaveType.SuspendLayout()
        CType(Me.dgLoanScheme, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbLeavedetail.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.txtSearchScheme)
        Me.pnlMain.Controls.Add(Me.pnlLeaveType)
        Me.pnlMain.Controls.Add(Me.gbLeavedetail)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(403, 426)
        Me.pnlMain.TabIndex = 0
        '
        'pnlLeaveType
        '
        Me.pnlLeaveType.Controls.Add(Me.chkSelectAll)
        Me.pnlLeaveType.Controls.Add(Me.dgLoanScheme)
        Me.pnlLeaveType.Location = New System.Drawing.Point(9, 129)
        Me.pnlLeaveType.Name = "pnlLeaveType"
        Me.pnlLeaveType.Size = New System.Drawing.Size(385, 236)
        Me.pnlLeaveType.TabIndex = 12
        '
        'chkSelectAll
        '
        Me.chkSelectAll.Location = New System.Drawing.Point(7, 6)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(13, 14)
        Me.chkSelectAll.TabIndex = 284
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'dgLoanScheme
        '
        Me.dgLoanScheme.AllowUserToAddRows = False
        Me.dgLoanScheme.AllowUserToDeleteRows = False
        Me.dgLoanScheme.AllowUserToResizeRows = False
        Me.dgLoanScheme.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgLoanScheme.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgLoanScheme.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhSelect, Me.dgcolhSchemeCode, Me.dgcolhLoanScheme, Me.objdgcolhLoanSchemeMappingunkid})
        Me.dgLoanScheme.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgLoanScheme.Location = New System.Drawing.Point(0, 0)
        Me.dgLoanScheme.Name = "dgLoanScheme"
        Me.dgLoanScheme.RowHeadersVisible = False
        Me.dgLoanScheme.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgLoanScheme.Size = New System.Drawing.Size(385, 236)
        Me.dgLoanScheme.TabIndex = 0
        '
        'objdgcolhSelect
        '
        Me.objdgcolhSelect.HeaderText = ""
        Me.objdgcolhSelect.Name = "objdgcolhSelect"
        Me.objdgcolhSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhSelect.Width = 25
        '
        'dgcolhSchemeCode
        '
        Me.dgcolhSchemeCode.HeaderText = "Loan Scheme Code"
        Me.dgcolhSchemeCode.Name = "dgcolhSchemeCode"
        Me.dgcolhSchemeCode.ReadOnly = True
        Me.dgcolhSchemeCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhSchemeCode.Width = 150
        '
        'dgcolhLoanScheme
        '
        Me.dgcolhLoanScheme.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhLoanScheme.HeaderText = "Loan Scheme Name"
        Me.dgcolhLoanScheme.Name = "dgcolhLoanScheme"
        Me.dgcolhLoanScheme.ReadOnly = True
        Me.dgcolhLoanScheme.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhLoanSchemeMappingunkid
        '
        Me.objdgcolhLoanSchemeMappingunkid.HeaderText = ""
        Me.objdgcolhLoanSchemeMappingunkid.Name = "objdgcolhLoanSchemeMappingunkid"
        Me.objdgcolhLoanSchemeMappingunkid.ReadOnly = True
        Me.objdgcolhLoanSchemeMappingunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhLoanSchemeMappingunkid.Visible = False
        '
        'gbLeavedetail
        '
        Me.gbLeavedetail.BorderColor = System.Drawing.Color.Black
        Me.gbLeavedetail.Checked = False
        Me.gbLeavedetail.CollapseAllExceptThis = False
        Me.gbLeavedetail.CollapsedHoverImage = Nothing
        Me.gbLeavedetail.CollapsedNormalImage = Nothing
        Me.gbLeavedetail.CollapsedPressedImage = Nothing
        Me.gbLeavedetail.CollapseOnLoad = False
        Me.gbLeavedetail.Controls.Add(Me.objlblApprover)
        Me.gbLeavedetail.Controls.Add(Me.lblApprover)
        Me.gbLeavedetail.Controls.Add(Me.objlblApproverLevel)
        Me.gbLeavedetail.Controls.Add(Me.lblApproverLevel)
        Me.gbLeavedetail.ExpandedHoverImage = Nothing
        Me.gbLeavedetail.ExpandedNormalImage = Nothing
        Me.gbLeavedetail.ExpandedPressedImage = Nothing
        Me.gbLeavedetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLeavedetail.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLeavedetail.HeaderHeight = 25
        Me.gbLeavedetail.HeaderMessage = ""
        Me.gbLeavedetail.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLeavedetail.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLeavedetail.HeightOnCollapse = 0
        Me.gbLeavedetail.LeftTextSpace = 0
        Me.gbLeavedetail.Location = New System.Drawing.Point(9, 8)
        Me.gbLeavedetail.Name = "gbLeavedetail"
        Me.gbLeavedetail.OpenHeight = 300
        Me.gbLeavedetail.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLeavedetail.ShowBorder = True
        Me.gbLeavedetail.ShowCheckBox = False
        Me.gbLeavedetail.ShowCollapseButton = False
        Me.gbLeavedetail.ShowDefaultBorderColor = True
        Me.gbLeavedetail.ShowDownButton = False
        Me.gbLeavedetail.ShowHeader = True
        Me.gbLeavedetail.Size = New System.Drawing.Size(385, 88)
        Me.gbLeavedetail.TabIndex = 11
        Me.gbLeavedetail.Temp = 0
        Me.gbLeavedetail.Text = "Approver Detail"
        Me.gbLeavedetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblApprover
        '
        Me.objlblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblApprover.Location = New System.Drawing.Point(119, 37)
        Me.objlblApprover.Name = "objlblApprover"
        Me.objlblApprover.Size = New System.Drawing.Size(246, 14)
        Me.objlblApprover.TabIndex = 282
        Me.objlblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(8, 37)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(104, 14)
        Me.lblApprover.TabIndex = 281
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblApproverLevel
        '
        Me.objlblApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblApproverLevel.Location = New System.Drawing.Point(119, 63)
        Me.objlblApproverLevel.Name = "objlblApproverLevel"
        Me.objlblApproverLevel.Size = New System.Drawing.Size(246, 14)
        Me.objlblApproverLevel.TabIndex = 280
        Me.objlblApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApproverLevel
        '
        Me.lblApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproverLevel.Location = New System.Drawing.Point(8, 63)
        Me.lblApproverLevel.Name = "lblApproverLevel"
        Me.lblApproverLevel.Size = New System.Drawing.Size(104, 14)
        Me.lblApproverLevel.TabIndex = 279
        Me.lblApproverLevel.Text = "Approver Level"
        Me.lblApproverLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOK)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 371)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(403, 55)
        Me.objFooter.TabIndex = 10
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.BackColor = System.Drawing.Color.White
        Me.btnOK.BackgroundImage = CType(resources.GetObject("btnOK.BackgroundImage"), System.Drawing.Image)
        Me.btnOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOK.BorderColor = System.Drawing.Color.Empty
        Me.btnOK.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOK.FlatAppearance.BorderSize = 0
        Me.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOK.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.ForeColor = System.Drawing.Color.Black
        Me.btnOK.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOK.GradientForeColor = System.Drawing.Color.Black
        Me.btnOK.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOK.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOK.Location = New System.Drawing.Point(190, 13)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOK.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOK.Size = New System.Drawing.Size(97, 30)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "&Save"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(293, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtSearchScheme
        '
        Me.txtSearchScheme.Flags = 0
        Me.txtSearchScheme.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchScheme.Location = New System.Drawing.Point(9, 102)
        Me.txtSearchScheme.Name = "txtSearchScheme"
        Me.txtSearchScheme.Size = New System.Drawing.Size(385, 21)
        Me.txtSearchScheme.TabIndex = 107
        '
        'frmApprover_LoanSchemeMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(403, 426)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmApprover_LoanSchemeMapping"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Loan Scheme Mapping"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.pnlLeaveType.ResumeLayout(False)
        CType(Me.dgLoanScheme, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbLeavedetail.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOK As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbLeavedetail As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objlblApprover As System.Windows.Forms.Label
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents objlblApproverLevel As System.Windows.Forms.Label
    Friend WithEvents lblApproverLevel As System.Windows.Forms.Label
    Friend WithEvents pnlLeaveType As System.Windows.Forms.Panel
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgLoanScheme As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhSchemeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoanScheme As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhLoanSchemeMappingunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtSearchScheme As eZee.TextBox.AlphanumericTextBox
End Class
