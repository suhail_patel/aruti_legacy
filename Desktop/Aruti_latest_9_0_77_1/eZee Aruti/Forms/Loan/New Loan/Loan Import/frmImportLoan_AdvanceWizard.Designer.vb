﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportLoan_AdvanceWizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportLoan_AdvanceWizard))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.eZeeWizImportLoan = New eZee.Common.eZeeWizard
        Me.WizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlLoanMapping = New System.Windows.Forms.Panel
        Me.objlblASign18 = New System.Windows.Forms.Label
        Me.cboMappedHead = New System.Windows.Forms.ComboBox
        Me.lblMappedHead = New System.Windows.Forms.Label
        Me.lnkAutoMap = New System.Windows.Forms.LinkLabel
        Me.objlblASign17 = New System.Windows.Forms.Label
        Me.cboInterestCalcType = New System.Windows.Forms.ComboBox
        Me.lblInterestCalcType = New System.Windows.Forms.Label
        Me.cboApplicationNo = New System.Windows.Forms.ComboBox
        Me.lblAppNo = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.objlblASign16 = New System.Windows.Forms.Label
        Me.objlblASign15 = New System.Windows.Forms.Label
        Me.objlblASign14 = New System.Windows.Forms.Label
        Me.objlblASign13 = New System.Windows.Forms.Label
        Me.objlblASign12 = New System.Windows.Forms.Label
        Me.objlblASign11 = New System.Windows.Forms.Label
        Me.objlblASign10 = New System.Windows.Forms.Label
        Me.objlblASign9 = New System.Windows.Forms.Label
        Me.cboApproverLevel = New System.Windows.Forms.ComboBox
        Me.lvlLoanApproverName = New System.Windows.Forms.Label
        Me.cboApproverName = New System.Windows.Forms.ComboBox
        Me.lblLoanApproverName = New System.Windows.Forms.Label
        Me.cboApproverCode = New System.Windows.Forms.ComboBox
        Me.lblLoanApproverCode = New System.Windows.Forms.Label
        Me.cboInstallmentAmount = New System.Windows.Forms.ComboBox
        Me.lblLoanInstallmentAmt = New System.Windows.Forms.Label
        Me.cboCurrncy = New System.Windows.Forms.ComboBox
        Me.lblLoanCurrncy = New System.Windows.Forms.Label
        Me.cboLoanCalType = New System.Windows.Forms.ComboBox
        Me.lblLoanCalType = New System.Windows.Forms.Label
        Me.cboNoOfInstallment = New System.Windows.Forms.ComboBox
        Me.lblExternalEntity = New System.Windows.Forms.Label
        Me.cboExternalEntity = New System.Windows.Forms.ComboBox
        Me.lblDurationMonth = New System.Windows.Forms.Label
        Me.cboInterestRate = New System.Windows.Forms.ComboBox
        Me.lblLoanInterestRate = New System.Windows.Forms.Label
        Me.objlblASign8 = New System.Windows.Forms.Label
        Me.cboLoanAmount = New System.Windows.Forms.ComboBox
        Me.lblLoanAmount = New System.Windows.Forms.Label
        Me.objlblASign7 = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.objlblASign6 = New System.Windows.Forms.Label
        Me.cboDeductionPeriod = New System.Windows.Forms.ComboBox
        Me.lblLoanDeductionPeriod = New System.Windows.Forms.Label
        Me.objlblASign5 = New System.Windows.Forms.Label
        Me.cboAssignmentPeriod = New System.Windows.Forms.ComboBox
        Me.lblLoanAssignedPeriod = New System.Windows.Forms.Label
        Me.objlblASign4 = New System.Windows.Forms.Label
        Me.cboLoanDate = New System.Windows.Forms.ComboBox
        Me.lblLoanDate = New System.Windows.Forms.Label
        Me.objlblASign3 = New System.Windows.Forms.Label
        Me.cboLoanVocNo = New System.Windows.Forms.ComboBox
        Me.lblLoanVocNo = New System.Windows.Forms.Label
        Me.objlblASign2 = New System.Windows.Forms.Label
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.cboLoanSurname = New System.Windows.Forms.ComboBox
        Me.lblLoanFirstname = New System.Windows.Forms.Label
        Me.cboLoanFirstname = New System.Windows.Forms.ComboBox
        Me.lblLoanEmpCode = New System.Windows.Forms.Label
        Me.lblLoanSurname = New System.Windows.Forms.Label
        Me.objlblASign1 = New System.Windows.Forms.Label
        Me.cboAdvanceAmount = New System.Windows.Forms.ComboBox
        Me.lblAdvanceAmount = New System.Windows.Forms.Label
        Me.lblLoanPrincipalAmt = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.WizPageSelectFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.objlblNote1 = New System.Windows.Forms.Label
        Me.lnkGenerateFile = New System.Windows.Forms.LinkLabel
        Me.objlblNote = New System.Windows.Forms.Label
        Me.lblImportation = New System.Windows.Forms.Label
        Me.radAdvance = New System.Windows.Forms.RadioButton
        Me.radLoan = New System.Windows.Forms.RadioButton
        Me.elImportationType = New eZee.Common.eZeeLine
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.WizPageImporting = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhlogindate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.objbuttonBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblLoanAccNumber = New System.Windows.Forms.Label
        Me.cboLoanAccountNo = New System.Windows.Forms.ComboBox
        Me.objlblASign19 = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.eZeeWizImportLoan.SuspendLayout()
        Me.WizPageMapping.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.pnlLoanMapping.SuspendLayout()
        Me.WizPageSelectFile.SuspendLayout()
        Me.WizPageImporting.SuspendLayout()
        Me.cmsFilter.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.eZeeWizImportLoan)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(784, 433)
        Me.pnlMainInfo.TabIndex = 0
        '
        'eZeeWizImportLoan
        '
        Me.eZeeWizImportLoan.Controls.Add(Me.WizPageMapping)
        Me.eZeeWizImportLoan.Controls.Add(Me.WizPageSelectFile)
        Me.eZeeWizImportLoan.Controls.Add(Me.WizPageImporting)
        Me.eZeeWizImportLoan.Dock = System.Windows.Forms.DockStyle.Fill
        Me.eZeeWizImportLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeWizImportLoan.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.eZeeWizImportLoan.Location = New System.Drawing.Point(0, 0)
        Me.eZeeWizImportLoan.Name = "eZeeWizImportLoan"
        Me.eZeeWizImportLoan.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.WizPageSelectFile, Me.WizPageMapping, Me.WizPageImporting})
        Me.eZeeWizImportLoan.SaveEnabled = True
        Me.eZeeWizImportLoan.SaveText = "Save && Finish"
        Me.eZeeWizImportLoan.SaveVisible = False
        Me.eZeeWizImportLoan.SetSaveIndexBeforeFinishIndex = False
        Me.eZeeWizImportLoan.Size = New System.Drawing.Size(784, 433)
        Me.eZeeWizImportLoan.TabIndex = 3
        Me.eZeeWizImportLoan.WelcomeImage = Nothing
        '
        'WizPageMapping
        '
        Me.WizPageMapping.Controls.Add(Me.gbFieldMapping)
        Me.WizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.WizPageMapping.Name = "WizPageMapping"
        Me.WizPageMapping.Size = New System.Drawing.Size(784, 385)
        Me.WizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageMapping.TabIndex = 8
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.pnlLoanMapping)
        Me.gbFieldMapping.Controls.Add(Me.lblCaption)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(165, 0)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(619, 384)
        Me.gbFieldMapping.TabIndex = 0
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLoanMapping
        '
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign19)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanAccountNo)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanAccNumber)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign18)
        Me.pnlLoanMapping.Controls.Add(Me.cboMappedHead)
        Me.pnlLoanMapping.Controls.Add(Me.lblMappedHead)
        Me.pnlLoanMapping.Controls.Add(Me.lnkAutoMap)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign17)
        Me.pnlLoanMapping.Controls.Add(Me.cboInterestCalcType)
        Me.pnlLoanMapping.Controls.Add(Me.lblInterestCalcType)
        Me.pnlLoanMapping.Controls.Add(Me.cboApplicationNo)
        Me.pnlLoanMapping.Controls.Add(Me.lblAppNo)
        Me.pnlLoanMapping.Controls.Add(Me.Label11)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign16)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign15)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign14)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign13)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign12)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign11)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign10)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign9)
        Me.pnlLoanMapping.Controls.Add(Me.cboApproverLevel)
        Me.pnlLoanMapping.Controls.Add(Me.lvlLoanApproverName)
        Me.pnlLoanMapping.Controls.Add(Me.cboApproverName)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanApproverName)
        Me.pnlLoanMapping.Controls.Add(Me.cboApproverCode)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanApproverCode)
        Me.pnlLoanMapping.Controls.Add(Me.cboInstallmentAmount)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanInstallmentAmt)
        Me.pnlLoanMapping.Controls.Add(Me.cboCurrncy)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanCurrncy)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanCalType)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanCalType)
        Me.pnlLoanMapping.Controls.Add(Me.cboNoOfInstallment)
        Me.pnlLoanMapping.Controls.Add(Me.lblExternalEntity)
        Me.pnlLoanMapping.Controls.Add(Me.cboExternalEntity)
        Me.pnlLoanMapping.Controls.Add(Me.lblDurationMonth)
        Me.pnlLoanMapping.Controls.Add(Me.cboInterestRate)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanInterestRate)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign8)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanAmount)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanAmount)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign7)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanScheme)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanScheme)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign6)
        Me.pnlLoanMapping.Controls.Add(Me.cboDeductionPeriod)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanDeductionPeriod)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign5)
        Me.pnlLoanMapping.Controls.Add(Me.cboAssignmentPeriod)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanAssignedPeriod)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign4)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanDate)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanDate)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign3)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanVocNo)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanVocNo)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign2)
        Me.pnlLoanMapping.Controls.Add(Me.cboEmployeeCode)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanSurname)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanFirstname)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanFirstname)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanEmpCode)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanSurname)
        Me.pnlLoanMapping.Controls.Add(Me.objlblASign1)
        Me.pnlLoanMapping.Controls.Add(Me.cboAdvanceAmount)
        Me.pnlLoanMapping.Controls.Add(Me.lblAdvanceAmount)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanPrincipalAmt)
        Me.pnlLoanMapping.Location = New System.Drawing.Point(2, 26)
        Me.pnlLoanMapping.Name = "pnlLoanMapping"
        Me.pnlLoanMapping.Size = New System.Drawing.Size(615, 356)
        Me.pnlLoanMapping.TabIndex = 1
        '
        'objlblASign18
        '
        Me.objlblASign18.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign18.ForeColor = System.Drawing.Color.Red
        Me.objlblASign18.Location = New System.Drawing.Point(306, 261)
        Me.objlblASign18.Name = "objlblASign18"
        Me.objlblASign18.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign18.TabIndex = 62
        Me.objlblASign18.Text = "*"
        Me.objlblASign18.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboMappedHead
        '
        Me.cboMappedHead.DropDownWidth = 190
        Me.cboMappedHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMappedHead.FormattingEnabled = True
        Me.cboMappedHead.Location = New System.Drawing.Point(429, 259)
        Me.cboMappedHead.Name = "cboMappedHead"
        Me.cboMappedHead.Size = New System.Drawing.Size(166, 21)
        Me.cboMappedHead.TabIndex = 64
        '
        'lblMappedHead
        '
        Me.lblMappedHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMappedHead.Location = New System.Drawing.Point(324, 261)
        Me.lblMappedHead.Name = "lblMappedHead"
        Me.lblMappedHead.Size = New System.Drawing.Size(99, 17)
        Me.lblMappedHead.TabIndex = 63
        Me.lblMappedHead.Text = "Mapped Head"
        Me.lblMappedHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAutoMap
        '
        Me.lnkAutoMap.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAutoMap.Location = New System.Drawing.Point(429, 319)
        Me.lnkAutoMap.Name = "lnkAutoMap"
        Me.lnkAutoMap.Size = New System.Drawing.Size(166, 20)
        Me.lnkAutoMap.TabIndex = 60
        Me.lnkAutoMap.TabStop = True
        Me.lnkAutoMap.Text = "Auto Map"
        Me.lnkAutoMap.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblASign17
        '
        Me.objlblASign17.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign17.ForeColor = System.Drawing.Color.Red
        Me.objlblASign17.Location = New System.Drawing.Point(306, 234)
        Me.objlblASign17.Name = "objlblASign17"
        Me.objlblASign17.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign17.TabIndex = 57
        Me.objlblASign17.Text = "*"
        Me.objlblASign17.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboInterestCalcType
        '
        Me.cboInterestCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterestCalcType.DropDownWidth = 190
        Me.cboInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterestCalcType.FormattingEnabled = True
        Me.cboInterestCalcType.Location = New System.Drawing.Point(429, 232)
        Me.cboInterestCalcType.Name = "cboInterestCalcType"
        Me.cboInterestCalcType.Size = New System.Drawing.Size(166, 21)
        Me.cboInterestCalcType.TabIndex = 59
        '
        'lblInterestCalcType
        '
        Me.lblInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestCalcType.Location = New System.Drawing.Point(324, 234)
        Me.lblInterestCalcType.Name = "lblInterestCalcType"
        Me.lblInterestCalcType.Size = New System.Drawing.Size(99, 17)
        Me.lblInterestCalcType.TabIndex = 58
        Me.lblInterestCalcType.Text = "Intrst. Calc. Type"
        Me.lblInterestCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApplicationNo
        '
        Me.cboApplicationNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApplicationNo.DropDownWidth = 190
        Me.cboApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApplicationNo.FormattingEnabled = True
        Me.cboApplicationNo.Location = New System.Drawing.Point(122, 124)
        Me.cboApplicationNo.Name = "cboApplicationNo"
        Me.cboApplicationNo.Size = New System.Drawing.Size(166, 21)
        Me.cboApplicationNo.TabIndex = 12
        '
        'lblAppNo
        '
        Me.lblAppNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppNo.Location = New System.Drawing.Point(25, 126)
        Me.lblAppNo.Name = "lblAppNo"
        Me.lblAppNo.Size = New System.Drawing.Size(91, 17)
        Me.lblAppNo.TabIndex = 11
        Me.lblAppNo.Text = "Application No"
        Me.lblAppNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Red
        Me.Label11.Location = New System.Drawing.Point(9, 126)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(10, 17)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "*"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign16
        '
        Me.objlblASign16.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign16.ForeColor = System.Drawing.Color.Red
        Me.objlblASign16.Location = New System.Drawing.Point(306, 180)
        Me.objlblASign16.Name = "objlblASign16"
        Me.objlblASign16.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign16.TabIndex = 54
        Me.objlblASign16.Text = "*"
        Me.objlblASign16.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign15
        '
        Me.objlblASign15.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign15.ForeColor = System.Drawing.Color.Red
        Me.objlblASign15.Location = New System.Drawing.Point(306, 153)
        Me.objlblASign15.Name = "objlblASign15"
        Me.objlblASign15.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign15.TabIndex = 51
        Me.objlblASign15.Text = "*"
        Me.objlblASign15.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign14
        '
        Me.objlblASign14.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign14.ForeColor = System.Drawing.Color.Red
        Me.objlblASign14.Location = New System.Drawing.Point(306, 126)
        Me.objlblASign14.Name = "objlblASign14"
        Me.objlblASign14.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign14.TabIndex = 48
        Me.objlblASign14.Text = "*"
        Me.objlblASign14.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign13
        '
        Me.objlblASign13.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign13.ForeColor = System.Drawing.Color.Red
        Me.objlblASign13.Location = New System.Drawing.Point(306, 99)
        Me.objlblASign13.Name = "objlblASign13"
        Me.objlblASign13.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign13.TabIndex = 45
        Me.objlblASign13.Text = "*"
        Me.objlblASign13.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign12
        '
        Me.objlblASign12.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign12.ForeColor = System.Drawing.Color.Red
        Me.objlblASign12.Location = New System.Drawing.Point(306, 288)
        Me.objlblASign12.Name = "objlblASign12"
        Me.objlblASign12.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign12.TabIndex = 42
        Me.objlblASign12.Text = "*"
        Me.objlblASign12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign11
        '
        Me.objlblASign11.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign11.ForeColor = System.Drawing.Color.Red
        Me.objlblASign11.Location = New System.Drawing.Point(306, 207)
        Me.objlblASign11.Name = "objlblASign11"
        Me.objlblASign11.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign11.TabIndex = 39
        Me.objlblASign11.Text = "*"
        Me.objlblASign11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign10
        '
        Me.objlblASign10.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign10.ForeColor = System.Drawing.Color.Red
        Me.objlblASign10.Location = New System.Drawing.Point(306, 74)
        Me.objlblASign10.Name = "objlblASign10"
        Me.objlblASign10.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign10.TabIndex = 36
        Me.objlblASign10.Text = "*"
        Me.objlblASign10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'objlblASign9
        '
        Me.objlblASign9.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign9.ForeColor = System.Drawing.Color.Red
        Me.objlblASign9.Location = New System.Drawing.Point(306, 47)
        Me.objlblASign9.Name = "objlblASign9"
        Me.objlblASign9.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign9.TabIndex = 33
        Me.objlblASign9.Text = "*"
        Me.objlblASign9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboApproverLevel
        '
        Me.cboApproverLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApproverLevel.DropDownWidth = 190
        Me.cboApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApproverLevel.FormattingEnabled = True
        Me.cboApproverLevel.Location = New System.Drawing.Point(429, 178)
        Me.cboApproverLevel.Name = "cboApproverLevel"
        Me.cboApproverLevel.Size = New System.Drawing.Size(166, 21)
        Me.cboApproverLevel.TabIndex = 56
        '
        'lvlLoanApproverName
        '
        Me.lvlLoanApproverName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvlLoanApproverName.Location = New System.Drawing.Point(324, 180)
        Me.lvlLoanApproverName.Name = "lvlLoanApproverName"
        Me.lvlLoanApproverName.Size = New System.Drawing.Size(99, 17)
        Me.lvlLoanApproverName.TabIndex = 55
        Me.lvlLoanApproverName.Text = "Approver Level"
        Me.lvlLoanApproverName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApproverName
        '
        Me.cboApproverName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApproverName.DropDownWidth = 190
        Me.cboApproverName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApproverName.FormattingEnabled = True
        Me.cboApproverName.Location = New System.Drawing.Point(429, 151)
        Me.cboApproverName.Name = "cboApproverName"
        Me.cboApproverName.Size = New System.Drawing.Size(166, 21)
        Me.cboApproverName.TabIndex = 53
        '
        'lblLoanApproverName
        '
        Me.lblLoanApproverName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanApproverName.Location = New System.Drawing.Point(324, 153)
        Me.lblLoanApproverName.Name = "lblLoanApproverName"
        Me.lblLoanApproverName.Size = New System.Drawing.Size(99, 17)
        Me.lblLoanApproverName.TabIndex = 52
        Me.lblLoanApproverName.Text = "Approver Name"
        Me.lblLoanApproverName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApproverCode
        '
        Me.cboApproverCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApproverCode.DropDownWidth = 190
        Me.cboApproverCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApproverCode.FormattingEnabled = True
        Me.cboApproverCode.Location = New System.Drawing.Point(429, 124)
        Me.cboApproverCode.Name = "cboApproverCode"
        Me.cboApproverCode.Size = New System.Drawing.Size(166, 21)
        Me.cboApproverCode.TabIndex = 50
        '
        'lblLoanApproverCode
        '
        Me.lblLoanApproverCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanApproverCode.Location = New System.Drawing.Point(324, 126)
        Me.lblLoanApproverCode.Name = "lblLoanApproverCode"
        Me.lblLoanApproverCode.Size = New System.Drawing.Size(99, 17)
        Me.lblLoanApproverCode.TabIndex = 49
        Me.lblLoanApproverCode.Text = "Approver Code"
        Me.lblLoanApproverCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInstallmentAmount
        '
        Me.cboInstallmentAmount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInstallmentAmount.DropDownWidth = 190
        Me.cboInstallmentAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInstallmentAmount.FormattingEnabled = True
        Me.cboInstallmentAmount.Location = New System.Drawing.Point(429, 97)
        Me.cboInstallmentAmount.Name = "cboInstallmentAmount"
        Me.cboInstallmentAmount.Size = New System.Drawing.Size(166, 21)
        Me.cboInstallmentAmount.TabIndex = 47
        '
        'lblLoanInstallmentAmt
        '
        Me.lblLoanInstallmentAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanInstallmentAmt.Location = New System.Drawing.Point(324, 99)
        Me.lblLoanInstallmentAmt.Name = "lblLoanInstallmentAmt"
        Me.lblLoanInstallmentAmt.Size = New System.Drawing.Size(99, 17)
        Me.lblLoanInstallmentAmt.TabIndex = 46
        Me.lblLoanInstallmentAmt.Text = "Installment Amt."
        Me.lblLoanInstallmentAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrncy
        '
        Me.cboCurrncy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrncy.DropDownWidth = 190
        Me.cboCurrncy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrncy.FormattingEnabled = True
        Me.cboCurrncy.Location = New System.Drawing.Point(429, 286)
        Me.cboCurrncy.Name = "cboCurrncy"
        Me.cboCurrncy.Size = New System.Drawing.Size(166, 21)
        Me.cboCurrncy.TabIndex = 44
        '
        'lblLoanCurrncy
        '
        Me.lblLoanCurrncy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanCurrncy.Location = New System.Drawing.Point(324, 288)
        Me.lblLoanCurrncy.Name = "lblLoanCurrncy"
        Me.lblLoanCurrncy.Size = New System.Drawing.Size(99, 17)
        Me.lblLoanCurrncy.TabIndex = 43
        Me.lblLoanCurrncy.Text = "Currency"
        Me.lblLoanCurrncy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanCalType
        '
        Me.cboLoanCalType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanCalType.DropDownWidth = 190
        Me.cboLoanCalType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanCalType.FormattingEnabled = True
        Me.cboLoanCalType.Location = New System.Drawing.Point(429, 205)
        Me.cboLoanCalType.Name = "cboLoanCalType"
        Me.cboLoanCalType.Size = New System.Drawing.Size(166, 21)
        Me.cboLoanCalType.TabIndex = 41
        '
        'lblLoanCalType
        '
        Me.lblLoanCalType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanCalType.Location = New System.Drawing.Point(324, 207)
        Me.lblLoanCalType.Name = "lblLoanCalType"
        Me.lblLoanCalType.Size = New System.Drawing.Size(99, 17)
        Me.lblLoanCalType.TabIndex = 40
        Me.lblLoanCalType.Text = "Loan Calc. Type"
        Me.lblLoanCalType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNoOfInstallment
        '
        Me.cboNoOfInstallment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNoOfInstallment.DropDownWidth = 190
        Me.cboNoOfInstallment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNoOfInstallment.FormattingEnabled = True
        Me.cboNoOfInstallment.Location = New System.Drawing.Point(429, 44)
        Me.cboNoOfInstallment.Name = "cboNoOfInstallment"
        Me.cboNoOfInstallment.Size = New System.Drawing.Size(166, 21)
        Me.cboNoOfInstallment.TabIndex = 35
        '
        'lblExternalEntity
        '
        Me.lblExternalEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExternalEntity.Location = New System.Drawing.Point(324, 73)
        Me.lblExternalEntity.Name = "lblExternalEntity"
        Me.lblExternalEntity.Size = New System.Drawing.Size(99, 17)
        Me.lblExternalEntity.TabIndex = 37
        Me.lblExternalEntity.Text = "External Entity"
        Me.lblExternalEntity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboExternalEntity
        '
        Me.cboExternalEntity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExternalEntity.DropDownWidth = 190
        Me.cboExternalEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExternalEntity.FormattingEnabled = True
        Me.cboExternalEntity.Location = New System.Drawing.Point(429, 71)
        Me.cboExternalEntity.Name = "cboExternalEntity"
        Me.cboExternalEntity.Size = New System.Drawing.Size(166, 21)
        Me.cboExternalEntity.TabIndex = 38
        '
        'lblDurationMonth
        '
        Me.lblDurationMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDurationMonth.Location = New System.Drawing.Point(324, 45)
        Me.lblDurationMonth.Name = "lblDurationMonth"
        Me.lblDurationMonth.Size = New System.Drawing.Size(99, 17)
        Me.lblDurationMonth.TabIndex = 34
        Me.lblDurationMonth.Text = "No Of Installment"
        Me.lblDurationMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInterestRate
        '
        Me.cboInterestRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterestRate.DropDownWidth = 190
        Me.cboInterestRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterestRate.FormattingEnabled = True
        Me.cboInterestRate.Location = New System.Drawing.Point(429, 17)
        Me.cboInterestRate.Name = "cboInterestRate"
        Me.cboInterestRate.Size = New System.Drawing.Size(166, 21)
        Me.cboInterestRate.TabIndex = 32
        '
        'lblLoanInterestRate
        '
        Me.lblLoanInterestRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanInterestRate.Location = New System.Drawing.Point(324, 19)
        Me.lblLoanInterestRate.Name = "lblLoanInterestRate"
        Me.lblLoanInterestRate.Size = New System.Drawing.Size(99, 17)
        Me.lblLoanInterestRate.TabIndex = 31
        Me.lblLoanInterestRate.Text = "Interest Rate"
        Me.lblLoanInterestRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign8
        '
        Me.objlblASign8.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign8.ForeColor = System.Drawing.Color.Red
        Me.objlblASign8.Location = New System.Drawing.Point(306, 20)
        Me.objlblASign8.Name = "objlblASign8"
        Me.objlblASign8.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign8.TabIndex = 30
        Me.objlblASign8.Text = "*"
        Me.objlblASign8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboLoanAmount
        '
        Me.cboLoanAmount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanAmount.DropDownWidth = 190
        Me.cboLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanAmount.FormattingEnabled = True
        Me.cboLoanAmount.Location = New System.Drawing.Point(122, 259)
        Me.cboLoanAmount.Name = "cboLoanAmount"
        Me.cboLoanAmount.Size = New System.Drawing.Size(166, 21)
        Me.cboLoanAmount.TabIndex = 27
        '
        'lblLoanAmount
        '
        Me.lblLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAmount.Location = New System.Drawing.Point(25, 261)
        Me.lblLoanAmount.Name = "lblLoanAmount"
        Me.lblLoanAmount.Size = New System.Drawing.Size(91, 17)
        Me.lblLoanAmount.TabIndex = 26
        Me.lblLoanAmount.Text = "Loan Amount"
        Me.lblLoanAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign7
        '
        Me.objlblASign7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign7.ForeColor = System.Drawing.Color.Red
        Me.objlblASign7.Location = New System.Drawing.Point(9, 261)
        Me.objlblASign7.Name = "objlblASign7"
        Me.objlblASign7.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign7.TabIndex = 25
        Me.objlblASign7.Text = "*"
        Me.objlblASign7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanScheme.DropDownWidth = 190
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Location = New System.Drawing.Point(122, 232)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(166, 21)
        Me.cboLoanScheme.TabIndex = 24
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(25, 234)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(91, 17)
        Me.lblLoanScheme.TabIndex = 23
        Me.lblLoanScheme.Text = "Loan Scheme"
        Me.lblLoanScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign6
        '
        Me.objlblASign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign6.ForeColor = System.Drawing.Color.Red
        Me.objlblASign6.Location = New System.Drawing.Point(9, 234)
        Me.objlblASign6.Name = "objlblASign6"
        Me.objlblASign6.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign6.TabIndex = 22
        Me.objlblASign6.Text = "*"
        Me.objlblASign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboDeductionPeriod
        '
        Me.cboDeductionPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeductionPeriod.DropDownWidth = 190
        Me.cboDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeductionPeriod.FormattingEnabled = True
        Me.cboDeductionPeriod.Location = New System.Drawing.Point(122, 205)
        Me.cboDeductionPeriod.Name = "cboDeductionPeriod"
        Me.cboDeductionPeriod.Size = New System.Drawing.Size(166, 21)
        Me.cboDeductionPeriod.TabIndex = 21
        '
        'lblLoanDeductionPeriod
        '
        Me.lblLoanDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanDeductionPeriod.Location = New System.Drawing.Point(25, 207)
        Me.lblLoanDeductionPeriod.Name = "lblLoanDeductionPeriod"
        Me.lblLoanDeductionPeriod.Size = New System.Drawing.Size(91, 17)
        Me.lblLoanDeductionPeriod.TabIndex = 20
        Me.lblLoanDeductionPeriod.Text = "Deduction Period"
        Me.lblLoanDeductionPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign5
        '
        Me.objlblASign5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign5.ForeColor = System.Drawing.Color.Red
        Me.objlblASign5.Location = New System.Drawing.Point(9, 207)
        Me.objlblASign5.Name = "objlblASign5"
        Me.objlblASign5.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign5.TabIndex = 19
        Me.objlblASign5.Text = "*"
        Me.objlblASign5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboAssignmentPeriod
        '
        Me.cboAssignmentPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAssignmentPeriod.DropDownWidth = 190
        Me.cboAssignmentPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAssignmentPeriod.FormattingEnabled = True
        Me.cboAssignmentPeriod.Location = New System.Drawing.Point(122, 178)
        Me.cboAssignmentPeriod.Name = "cboAssignmentPeriod"
        Me.cboAssignmentPeriod.Size = New System.Drawing.Size(166, 21)
        Me.cboAssignmentPeriod.TabIndex = 18
        '
        'lblLoanAssignedPeriod
        '
        Me.lblLoanAssignedPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAssignedPeriod.Location = New System.Drawing.Point(25, 180)
        Me.lblLoanAssignedPeriod.Name = "lblLoanAssignedPeriod"
        Me.lblLoanAssignedPeriod.Size = New System.Drawing.Size(91, 17)
        Me.lblLoanAssignedPeriod.TabIndex = 17
        Me.lblLoanAssignedPeriod.Text = "Assigned Period"
        Me.lblLoanAssignedPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign4
        '
        Me.objlblASign4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign4.ForeColor = System.Drawing.Color.Red
        Me.objlblASign4.Location = New System.Drawing.Point(9, 180)
        Me.objlblASign4.Name = "objlblASign4"
        Me.objlblASign4.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign4.TabIndex = 16
        Me.objlblASign4.Text = "*"
        Me.objlblASign4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboLoanDate
        '
        Me.cboLoanDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanDate.DropDownWidth = 190
        Me.cboLoanDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanDate.FormattingEnabled = True
        Me.cboLoanDate.Location = New System.Drawing.Point(122, 151)
        Me.cboLoanDate.Name = "cboLoanDate"
        Me.cboLoanDate.Size = New System.Drawing.Size(166, 21)
        Me.cboLoanDate.TabIndex = 15
        '
        'lblLoanDate
        '
        Me.lblLoanDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanDate.Location = New System.Drawing.Point(25, 153)
        Me.lblLoanDate.Name = "lblLoanDate"
        Me.lblLoanDate.Size = New System.Drawing.Size(91, 17)
        Me.lblLoanDate.TabIndex = 14
        Me.lblLoanDate.Text = "Loan  Date"
        Me.lblLoanDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign3
        '
        Me.objlblASign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign3.ForeColor = System.Drawing.Color.Red
        Me.objlblASign3.Location = New System.Drawing.Point(9, 153)
        Me.objlblASign3.Name = "objlblASign3"
        Me.objlblASign3.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign3.TabIndex = 13
        Me.objlblASign3.Text = "*"
        Me.objlblASign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboLoanVocNo
        '
        Me.cboLoanVocNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanVocNo.DropDownWidth = 190
        Me.cboLoanVocNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanVocNo.FormattingEnabled = True
        Me.cboLoanVocNo.Location = New System.Drawing.Point(122, 97)
        Me.cboLoanVocNo.Name = "cboLoanVocNo"
        Me.cboLoanVocNo.Size = New System.Drawing.Size(166, 21)
        Me.cboLoanVocNo.TabIndex = 9
        '
        'lblLoanVocNo
        '
        Me.lblLoanVocNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanVocNo.Location = New System.Drawing.Point(25, 99)
        Me.lblLoanVocNo.Name = "lblLoanVocNo"
        Me.lblLoanVocNo.Size = New System.Drawing.Size(91, 17)
        Me.lblLoanVocNo.TabIndex = 8
        Me.lblLoanVocNo.Text = "Loan Voc. No"
        Me.lblLoanVocNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign2
        '
        Me.objlblASign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign2.ForeColor = System.Drawing.Color.Red
        Me.objlblASign2.Location = New System.Drawing.Point(9, 99)
        Me.objlblASign2.Name = "objlblASign2"
        Me.objlblASign2.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign2.TabIndex = 7
        Me.objlblASign2.Text = "*"
        Me.objlblASign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.DropDownWidth = 190
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(122, 16)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(166, 21)
        Me.cboEmployeeCode.TabIndex = 2
        '
        'cboLoanSurname
        '
        Me.cboLoanSurname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanSurname.DropDownWidth = 190
        Me.cboLoanSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanSurname.FormattingEnabled = True
        Me.cboLoanSurname.Location = New System.Drawing.Point(122, 70)
        Me.cboLoanSurname.Name = "cboLoanSurname"
        Me.cboLoanSurname.Size = New System.Drawing.Size(166, 21)
        Me.cboLoanSurname.TabIndex = 6
        '
        'lblLoanFirstname
        '
        Me.lblLoanFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanFirstname.Location = New System.Drawing.Point(25, 45)
        Me.lblLoanFirstname.Name = "lblLoanFirstname"
        Me.lblLoanFirstname.Size = New System.Drawing.Size(91, 17)
        Me.lblLoanFirstname.TabIndex = 3
        Me.lblLoanFirstname.Text = "Firstname"
        Me.lblLoanFirstname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanFirstname
        '
        Me.cboLoanFirstname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanFirstname.DropDownWidth = 190
        Me.cboLoanFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanFirstname.FormattingEnabled = True
        Me.cboLoanFirstname.Location = New System.Drawing.Point(122, 43)
        Me.cboLoanFirstname.Name = "cboLoanFirstname"
        Me.cboLoanFirstname.Size = New System.Drawing.Size(166, 21)
        Me.cboLoanFirstname.TabIndex = 4
        '
        'lblLoanEmpCode
        '
        Me.lblLoanEmpCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanEmpCode.Location = New System.Drawing.Point(25, 18)
        Me.lblLoanEmpCode.Name = "lblLoanEmpCode"
        Me.lblLoanEmpCode.Size = New System.Drawing.Size(91, 17)
        Me.lblLoanEmpCode.TabIndex = 1
        Me.lblLoanEmpCode.Text = "Employee Code"
        Me.lblLoanEmpCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanSurname
        '
        Me.lblLoanSurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanSurname.Location = New System.Drawing.Point(25, 72)
        Me.lblLoanSurname.Name = "lblLoanSurname"
        Me.lblLoanSurname.Size = New System.Drawing.Size(91, 17)
        Me.lblLoanSurname.TabIndex = 5
        Me.lblLoanSurname.Text = "Surname"
        Me.lblLoanSurname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign1
        '
        Me.objlblASign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign1.ForeColor = System.Drawing.Color.Red
        Me.objlblASign1.Location = New System.Drawing.Point(9, 18)
        Me.objlblASign1.Name = "objlblASign1"
        Me.objlblASign1.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign1.TabIndex = 0
        Me.objlblASign1.Text = "*"
        Me.objlblASign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboAdvanceAmount
        '
        Me.cboAdvanceAmount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAdvanceAmount.DropDownWidth = 190
        Me.cboAdvanceAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAdvanceAmount.FormattingEnabled = True
        Me.cboAdvanceAmount.Location = New System.Drawing.Point(122, 259)
        Me.cboAdvanceAmount.Name = "cboAdvanceAmount"
        Me.cboAdvanceAmount.Size = New System.Drawing.Size(166, 21)
        Me.cboAdvanceAmount.TabIndex = 29
        '
        'lblAdvanceAmount
        '
        Me.lblAdvanceAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdvanceAmount.Location = New System.Drawing.Point(25, 263)
        Me.lblAdvanceAmount.Name = "lblAdvanceAmount"
        Me.lblAdvanceAmount.Size = New System.Drawing.Size(91, 17)
        Me.lblAdvanceAmount.TabIndex = 28
        Me.lblAdvanceAmount.Text = "Advance Amount"
        Me.lblAdvanceAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanPrincipalAmt
        '
        Me.lblLoanPrincipalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanPrincipalAmt.Location = New System.Drawing.Point(324, 100)
        Me.lblLoanPrincipalAmt.Name = "lblLoanPrincipalAmt"
        Me.lblLoanPrincipalAmt.Size = New System.Drawing.Size(99, 17)
        Me.lblLoanPrincipalAmt.TabIndex = 61
        Me.lblLoanPrincipalAmt.Text = "Principal Amt."
        Me.lblLoanPrincipalAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(331, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(284, 19)
        Me.lblCaption.TabIndex = 323
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'WizPageSelectFile
        '
        Me.WizPageSelectFile.Controls.Add(Me.objlblNote1)
        Me.WizPageSelectFile.Controls.Add(Me.lnkGenerateFile)
        Me.WizPageSelectFile.Controls.Add(Me.objlblNote)
        Me.WizPageSelectFile.Controls.Add(Me.lblImportation)
        Me.WizPageSelectFile.Controls.Add(Me.radAdvance)
        Me.WizPageSelectFile.Controls.Add(Me.radLoan)
        Me.WizPageSelectFile.Controls.Add(Me.elImportationType)
        Me.WizPageSelectFile.Controls.Add(Me.lblMessage)
        Me.WizPageSelectFile.Controls.Add(Me.lblTitle)
        Me.WizPageSelectFile.Controls.Add(Me.btnOpenFile)
        Me.WizPageSelectFile.Controls.Add(Me.txtFilePath)
        Me.WizPageSelectFile.Controls.Add(Me.lblSelectfile)
        Me.WizPageSelectFile.Location = New System.Drawing.Point(0, 0)
        Me.WizPageSelectFile.Name = "WizPageSelectFile"
        Me.WizPageSelectFile.Size = New System.Drawing.Size(784, 385)
        Me.WizPageSelectFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageSelectFile.TabIndex = 7
        '
        'objlblNote1
        '
        Me.objlblNote1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblNote1.ForeColor = System.Drawing.Color.Maroon
        Me.objlblNote1.Location = New System.Drawing.Point(186, 288)
        Me.objlblNote1.Name = "objlblNote1"
        Me.objlblNote1.Size = New System.Drawing.Size(586, 40)
        Me.objlblNote1.TabIndex = 328
        Me.objlblNote1.Text = "Note 1"
        '
        'lnkGenerateFile
        '
        Me.lnkGenerateFile.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkGenerateFile.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkGenerateFile.Location = New System.Drawing.Point(572, 342)
        Me.lnkGenerateFile.Name = "lnkGenerateFile"
        Me.lnkGenerateFile.Size = New System.Drawing.Size(183, 23)
        Me.lnkGenerateFile.TabIndex = 327
        Me.lnkGenerateFile.TabStop = True
        Me.lnkGenerateFile.Text = "Get File Format"
        Me.lnkGenerateFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblNote
        '
        Me.objlblNote.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblNote.ForeColor = System.Drawing.Color.Maroon
        Me.objlblNote.Location = New System.Drawing.Point(186, 240)
        Me.objlblNote.Name = "objlblNote"
        Me.objlblNote.Size = New System.Drawing.Size(586, 40)
        Me.objlblNote.TabIndex = 326
        '
        'lblImportation
        '
        Me.lblImportation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImportation.Location = New System.Drawing.Point(206, 203)
        Me.lblImportation.Name = "lblImportation"
        Me.lblImportation.Size = New System.Drawing.Size(172, 15)
        Me.lblImportation.TabIndex = 325
        Me.lblImportation.Text = "Select Importation Type"
        Me.lblImportation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radAdvance
        '
        Me.radAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAdvance.Location = New System.Drawing.Point(495, 202)
        Me.radAdvance.Name = "radAdvance"
        Me.radAdvance.Size = New System.Drawing.Size(100, 17)
        Me.radAdvance.TabIndex = 324
        Me.radAdvance.Text = "Advance"
        Me.radAdvance.UseVisualStyleBackColor = True
        '
        'radLoan
        '
        Me.radLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLoan.Location = New System.Drawing.Point(384, 202)
        Me.radLoan.Name = "radLoan"
        Me.radLoan.Size = New System.Drawing.Size(100, 17)
        Me.radLoan.TabIndex = 323
        Me.radLoan.Text = "Loan"
        Me.radLoan.UseVisualStyleBackColor = True
        '
        'elImportationType
        '
        Me.elImportationType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elImportationType.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elImportationType.Location = New System.Drawing.Point(189, 164)
        Me.elImportationType.Name = "elImportationType"
        Me.elImportationType.Size = New System.Drawing.Size(566, 17)
        Me.elImportationType.TabIndex = 322
        Me.elImportationType.Text = "Importation Type"
        Me.elImportationType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(189, 49)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(426, 34)
        Me.lblMessage.TabIndex = 18
        Me.lblMessage.Text = "This wizard will import Employee 'Loan/Advance' records made from other system."
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(188, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(427, 23)
        Me.lblTitle.TabIndex = 17
        Me.lblTitle.Text = "Employee Loan/Advance Import Wizard"
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(732, 112)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(23, 20)
        Me.btnOpenFile.TabIndex = 16
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(189, 112)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(537, 21)
        Me.txtFilePath.TabIndex = 15
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(189, 83)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(228, 20)
        Me.lblSelectfile.TabIndex = 14
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'WizPageImporting
        '
        Me.WizPageImporting.Controls.Add(Me.btnFilter)
        Me.WizPageImporting.Controls.Add(Me.dgData)
        Me.WizPageImporting.Controls.Add(Me.pnlInfo)
        Me.WizPageImporting.Controls.Add(Me.objbuttonBack)
        Me.WizPageImporting.Controls.Add(Me.objbuttonCancel)
        Me.WizPageImporting.Controls.Add(Me.objbuttonNext)
        Me.WizPageImporting.Location = New System.Drawing.Point(0, 0)
        Me.WizPageImporting.Name = "WizPageImporting"
        Me.WizPageImporting.Size = New System.Drawing.Size(784, 385)
        Me.WizPageImporting.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.WizPageImporting.TabIndex = 9
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 349)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(107, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 23
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(200, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(199, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(199, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(199, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.colhlogindate, Me.colhStatus, Me.colhMessage, Me.objcolhstatus, Me.objcolhDate})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(751, 274)
        Me.dgData.TabIndex = 19
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 30
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhlogindate
        '
        Me.colhlogindate.HeaderText = "Date"
        Me.colhlogindate.Name = "colhlogindate"
        Me.colhlogindate.ReadOnly = True
        Me.colhlogindate.Visible = False
        Me.colhlogindate.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 250
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'objcolhDate
        '
        Me.objcolhDate.HeaderText = "objcolhDate"
        Me.objcolhDate.Name = "objcolhDate"
        Me.objcolhDate.ReadOnly = True
        Me.objcolhDate.Visible = False
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(751, 51)
        Me.pnlInfo.TabIndex = 2
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(629, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(515, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(629, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(561, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(674, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(515, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(674, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(561, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbuttonBack
        '
        Me.objbuttonBack.BackColor = System.Drawing.Color.White
        Me.objbuttonBack.BackgroundImage = CType(resources.GetObject("objbuttonBack.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonBack.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonBack.Enabled = False
        Me.objbuttonBack.FlatAppearance.BorderSize = 0
        Me.objbuttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonBack.ForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonBack.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Location = New System.Drawing.Point(246, 265)
        Me.objbuttonBack.Name = "objbuttonBack"
        Me.objbuttonBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonBack.TabIndex = 6
        Me.objbuttonBack.Text = "Back"
        Me.objbuttonBack.UseVisualStyleBackColor = False
        '
        'objbuttonCancel
        '
        Me.objbuttonCancel.BackColor = System.Drawing.Color.White
        Me.objbuttonCancel.BackgroundImage = CType(resources.GetObject("objbuttonCancel.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonCancel.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.objbuttonCancel.FlatAppearance.BorderSize = 0
        Me.objbuttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonCancel.ForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonCancel.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Location = New System.Drawing.Point(414, 265)
        Me.objbuttonCancel.Name = "objbuttonCancel"
        Me.objbuttonCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonCancel.TabIndex = 5
        Me.objbuttonCancel.Text = "Cancel"
        Me.objbuttonCancel.UseVisualStyleBackColor = False
        '
        'objbuttonNext
        '
        Me.objbuttonNext.BackColor = System.Drawing.Color.White
        Me.objbuttonNext.BackgroundImage = CType(resources.GetObject("objbuttonNext.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonNext.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonNext.FlatAppearance.BorderSize = 0
        Me.objbuttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonNext.ForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Location = New System.Drawing.Point(330, 265)
        Me.objbuttonNext.Name = "objbuttonNext"
        Me.objbuttonNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonNext.TabIndex = 4
        Me.objbuttonNext.Text = "Next"
        Me.objbuttonNext.UseVisualStyleBackColor = False
        '
        'lblLoanAccNumber
        '
        Me.lblLoanAccNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAccNumber.Location = New System.Drawing.Point(25, 290)
        Me.lblLoanAccNumber.Name = "lblLoanAccNumber"
        Me.lblLoanAccNumber.Size = New System.Drawing.Size(91, 17)
        Me.lblLoanAccNumber.TabIndex = 344
        Me.lblLoanAccNumber.Text = "Loan Account  No"
        '
        'cboLoanAccountNo
        '
        Me.cboLoanAccountNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanAccountNo.DropDownWidth = 190
        Me.cboLoanAccountNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanAccountNo.FormattingEnabled = True
        Me.cboLoanAccountNo.Location = New System.Drawing.Point(122, 286)
        Me.cboLoanAccountNo.Name = "cboLoanAccountNo"
        Me.cboLoanAccountNo.Size = New System.Drawing.Size(166, 21)
        Me.cboLoanAccountNo.TabIndex = 345
        '
        'objlblASign19
        '
        Me.objlblASign19.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign19.ForeColor = System.Drawing.Color.Red
        Me.objlblASign19.Location = New System.Drawing.Point(9, 288)
        Me.objlblASign19.Name = "objlblASign19"
        Me.objlblASign19.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign19.TabIndex = 346
        Me.objlblASign19.Text = "*"
        Me.objlblASign19.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'frmImportLoan_AdvanceWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 433)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportLoan_AdvanceWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Loan/Advance Wizard"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.eZeeWizImportLoan.ResumeLayout(False)
        Me.WizPageMapping.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.pnlLoanMapping.ResumeLayout(False)
        Me.WizPageSelectFile.ResumeLayout(False)
        Me.WizPageSelectFile.PerformLayout()
        Me.WizPageImporting.ResumeLayout(False)
        Me.cmsFilter.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeWizImportLoan As eZee.Common.eZeeWizard
    Friend WithEvents WizPageSelectFile As eZee.Common.eZeeWizardPage
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents WizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents WizPageImporting As eZee.Common.eZeeWizardPage
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents objbuttonBack As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonNext As eZee.Common.eZeeLightButton
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents pnlLoanMapping As System.Windows.Forms.Panel
    Friend WithEvents cboInterestRate As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanInterestRate As System.Windows.Forms.Label
    Friend WithEvents objlblASign8 As System.Windows.Forms.Label
    Friend WithEvents cboLoanAmount As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanAmount As System.Windows.Forms.Label
    Friend WithEvents objlblASign7 As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents objlblASign6 As System.Windows.Forms.Label
    Friend WithEvents cboDeductionPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanDeductionPeriod As System.Windows.Forms.Label
    Friend WithEvents objlblASign5 As System.Windows.Forms.Label
    Friend WithEvents cboAssignmentPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanAssignedPeriod As System.Windows.Forms.Label
    Friend WithEvents objlblASign4 As System.Windows.Forms.Label
    Friend WithEvents cboLoanDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanDate As System.Windows.Forms.Label
    Friend WithEvents objlblASign3 As System.Windows.Forms.Label
    Friend WithEvents cboLoanVocNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanVocNo As System.Windows.Forms.Label
    Friend WithEvents objlblASign2 As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboLoanSurname As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanFirstname As System.Windows.Forms.Label
    Friend WithEvents cboLoanFirstname As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanEmpCode As System.Windows.Forms.Label
    Friend WithEvents lblLoanSurname As System.Windows.Forms.Label
    Friend WithEvents objlblASign1 As System.Windows.Forms.Label
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhlogindate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblImportation As System.Windows.Forms.Label
    Friend WithEvents radAdvance As System.Windows.Forms.RadioButton
    Friend WithEvents radLoan As System.Windows.Forms.RadioButton
    Friend WithEvents elImportationType As eZee.Common.eZeeLine
    Friend WithEvents cboApproverLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lvlLoanApproverName As System.Windows.Forms.Label
    Friend WithEvents cboApproverName As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanApproverName As System.Windows.Forms.Label
    Friend WithEvents cboApproverCode As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanApproverCode As System.Windows.Forms.Label
    Friend WithEvents cboInstallmentAmount As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanInstallmentAmt As System.Windows.Forms.Label
    Friend WithEvents cboCurrncy As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanCurrncy As System.Windows.Forms.Label
    Friend WithEvents cboLoanCalType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanCalType As System.Windows.Forms.Label
    Friend WithEvents cboNoOfInstallment As System.Windows.Forms.ComboBox
    Friend WithEvents lblExternalEntity As System.Windows.Forms.Label
    Friend WithEvents cboExternalEntity As System.Windows.Forms.ComboBox
    Friend WithEvents lblDurationMonth As System.Windows.Forms.Label
    Friend WithEvents objlblASign16 As System.Windows.Forms.Label
    Friend WithEvents objlblASign15 As System.Windows.Forms.Label
    Friend WithEvents objlblASign14 As System.Windows.Forms.Label
    Friend WithEvents objlblASign13 As System.Windows.Forms.Label
    Friend WithEvents objlblASign12 As System.Windows.Forms.Label
    Friend WithEvents objlblASign11 As System.Windows.Forms.Label
    Friend WithEvents objlblASign10 As System.Windows.Forms.Label
    Friend WithEvents objlblASign9 As System.Windows.Forms.Label
    Friend WithEvents cboApplicationNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblAppNo As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboAdvanceAmount As System.Windows.Forms.ComboBox
    Friend WithEvents lblAdvanceAmount As System.Windows.Forms.Label
    Friend WithEvents lnkGenerateFile As System.Windows.Forms.LinkLabel
    Friend WithEvents objlblNote As System.Windows.Forms.Label
    Friend WithEvents objlblASign17 As System.Windows.Forms.Label
    Friend WithEvents cboInterestCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblInterestCalcType As System.Windows.Forms.Label
    Friend WithEvents lnkAutoMap As System.Windows.Forms.LinkLabel
    Friend WithEvents lblLoanPrincipalAmt As System.Windows.Forms.Label
    Friend WithEvents objlblNote1 As System.Windows.Forms.Label
    Friend WithEvents objlblASign18 As System.Windows.Forms.Label
    Friend WithEvents cboMappedHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblMappedHead As System.Windows.Forms.Label
    Friend WithEvents lblLoanAccNumber As System.Windows.Forms.Label
    Friend WithEvents cboLoanAccountNo As System.Windows.Forms.ComboBox
    Friend WithEvents objlblASign19 As System.Windows.Forms.Label
End Class
