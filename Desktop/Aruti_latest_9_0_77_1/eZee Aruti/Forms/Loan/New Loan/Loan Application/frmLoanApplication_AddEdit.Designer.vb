﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanApplication_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanApplication_AddEdit))
        Me.gbProcessPendingLoanInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtLoanAccNumber = New eZee.TextBox.AlphanumericTextBox
        Me.lblLoanAccNumber = New System.Windows.Forms.Label
        Me.radAdvance = New System.Windows.Forms.RadioButton
        Me.radLoan = New System.Windows.Forms.RadioButton
        Me.lblLoanAdvance = New System.Windows.Forms.Label
        Me.objpnlLoanProjection = New System.Windows.Forms.Panel
        Me.cboLoanCalcType = New System.Windows.Forms.ComboBox
        Me.pnlProjectedAmt = New System.Windows.Forms.Panel
        Me.lblInterestAmt = New System.Windows.Forms.Label
        Me.txtInterestAmt = New eZee.TextBox.NumericTextBox
        Me.lblNetAmount = New System.Windows.Forms.Label
        Me.txtNetAmount = New eZee.TextBox.NumericTextBox
        Me.cboInterestCalcType = New System.Windows.Forms.ComboBox
        Me.lnProjectedAmount = New eZee.Common.eZeeLine
        Me.lblInterestCalcType = New System.Windows.Forms.Label
        Me.pnlInformation = New System.Windows.Forms.Panel
        Me.lblIntAmt = New System.Windows.Forms.Label
        Me.lblPrincipalAmt = New System.Windows.Forms.Label
        Me.txtIntAmt = New eZee.TextBox.NumericTextBox
        Me.txtPrincipalAmt = New eZee.TextBox.NumericTextBox
        Me.txtLoanRate = New eZee.TextBox.NumericTextBox
        Me.lblLoanInterest = New System.Windows.Forms.Label
        Me.lblEMIInstallments = New System.Windows.Forms.Label
        Me.nudDuration = New System.Windows.Forms.NumericUpDown
        Me.lblEMIAmount = New System.Windows.Forms.Label
        Me.txtInstallmentAmt = New eZee.TextBox.NumericTextBox
        Me.elProjectedINSTLAmt = New eZee.Common.eZeeLine
        Me.lblLoanCalcType = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeStraightLine
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.cboDeductionPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchDeductionPeriod = New eZee.Common.eZeeGradientButton
        Me.lblDeductionPeriod = New System.Windows.Forms.Label
        Me.txtExternalEntity = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmpRemark = New System.Windows.Forms.Label
        Me.txtEmployeeRemark = New eZee.TextBox.AlphanumericTextBox
        Me.chkExternalEntity = New System.Windows.Forms.CheckBox
        Me.txtApplicationNo = New eZee.TextBox.AlphanumericTextBox
        Me.dtpApplicationDate = New System.Windows.Forms.DateTimePicker
        Me.lblApplicationDate = New System.Windows.Forms.Label
        Me.lblApplicationNo = New System.Windows.Forms.Label
        Me.objbtnAddLoanScheme = New eZee.Common.eZeeGradientButton
        Me.lblAmt = New System.Windows.Forms.Label
        Me.txtLoanAmt = New eZee.TextBox.NumericTextBox
        Me.cboEmpName = New System.Windows.Forms.ComboBox
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.objLoanSchemeSearch = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objvalPeriodDuration = New System.Windows.Forms.Label
        Me.nudDurationInMths = New System.Windows.Forms.NumericUpDown
        Me.lblDuration = New System.Windows.Forms.Label
        Me.txtEMIInstallments = New eZee.TextBox.NumericTextBox
        Me.lblEMIInstallments1 = New System.Windows.Forms.Label
        Me.txtInstallmentAmt1 = New eZee.TextBox.NumericTextBox
        Me.lblEMIAmount1 = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblExRate = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbProcessPendingLoanInfo.SuspendLayout()
        Me.objpnlLoanProjection.SuspendLayout()
        Me.pnlProjectedAmt.SuspendLayout()
        Me.pnlInformation.SuspendLayout()
        CType(Me.nudDuration, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDurationInMths, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbProcessPendingLoanInfo
        '
        Me.gbProcessPendingLoanInfo.BorderColor = System.Drawing.Color.Black
        Me.gbProcessPendingLoanInfo.Checked = False
        Me.gbProcessPendingLoanInfo.CollapseAllExceptThis = False
        Me.gbProcessPendingLoanInfo.CollapsedHoverImage = Nothing
        Me.gbProcessPendingLoanInfo.CollapsedNormalImage = Nothing
        Me.gbProcessPendingLoanInfo.CollapsedPressedImage = Nothing
        Me.gbProcessPendingLoanInfo.CollapseOnLoad = False
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtLoanAccNumber)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblLoanAccNumber)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.radAdvance)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.radLoan)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblLoanAdvance)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.objpnlLoanProjection)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.objLine1)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblLoanScheme)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.cboLoanScheme)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.cboCurrency)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.cboDeductionPeriod)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.objbtnSearchDeductionPeriod)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblDeductionPeriod)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtExternalEntity)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblEmpRemark)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtEmployeeRemark)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.chkExternalEntity)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtApplicationNo)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.dtpApplicationDate)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblApplicationDate)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblApplicationNo)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.objbtnAddLoanScheme)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblAmt)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtLoanAmt)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.cboEmpName)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblEmpName)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.objLoanSchemeSearch)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbProcessPendingLoanInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbProcessPendingLoanInfo.ExpandedHoverImage = Nothing
        Me.gbProcessPendingLoanInfo.ExpandedNormalImage = Nothing
        Me.gbProcessPendingLoanInfo.ExpandedPressedImage = Nothing
        Me.gbProcessPendingLoanInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbProcessPendingLoanInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbProcessPendingLoanInfo.HeaderHeight = 25
        Me.gbProcessPendingLoanInfo.HeaderMessage = ""
        Me.gbProcessPendingLoanInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbProcessPendingLoanInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbProcessPendingLoanInfo.HeightOnCollapse = 0
        Me.gbProcessPendingLoanInfo.LeftTextSpace = 0
        Me.gbProcessPendingLoanInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbProcessPendingLoanInfo.Name = "gbProcessPendingLoanInfo"
        Me.gbProcessPendingLoanInfo.OpenHeight = 300
        Me.gbProcessPendingLoanInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbProcessPendingLoanInfo.ShowBorder = True
        Me.gbProcessPendingLoanInfo.ShowCheckBox = False
        Me.gbProcessPendingLoanInfo.ShowCollapseButton = False
        Me.gbProcessPendingLoanInfo.ShowDefaultBorderColor = True
        Me.gbProcessPendingLoanInfo.ShowDownButton = False
        Me.gbProcessPendingLoanInfo.ShowHeader = True
        Me.gbProcessPendingLoanInfo.Size = New System.Drawing.Size(824, 358)
        Me.gbProcessPendingLoanInfo.TabIndex = 0
        Me.gbProcessPendingLoanInfo.Temp = 0
        Me.gbProcessPendingLoanInfo.Text = "Loan Application Info"
        Me.gbProcessPendingLoanInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLoanAccNumber
        '
        Me.txtLoanAccNumber.Flags = 0
        Me.txtLoanAccNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanAccNumber.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLoanAccNumber.Location = New System.Drawing.Point(132, 194)
        Me.txtLoanAccNumber.Name = "txtLoanAccNumber"
        Me.txtLoanAccNumber.Size = New System.Drawing.Size(288, 21)
        Me.txtLoanAccNumber.TabIndex = 344
        '
        'lblLoanAccNumber
        '
        Me.lblLoanAccNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAccNumber.Location = New System.Drawing.Point(12, 195)
        Me.lblLoanAccNumber.Name = "lblLoanAccNumber"
        Me.lblLoanAccNumber.Size = New System.Drawing.Size(115, 17)
        Me.lblLoanAccNumber.TabIndex = 343
        Me.lblLoanAccNumber.Text = "Loan Account  Number"
        '
        'radAdvance
        '
        Me.radAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAdvance.Location = New System.Drawing.Point(233, 88)
        Me.radAdvance.Name = "radAdvance"
        Me.radAdvance.Size = New System.Drawing.Size(95, 17)
        Me.radAdvance.TabIndex = 3
        Me.radAdvance.Text = "Advance"
        Me.radAdvance.UseVisualStyleBackColor = True
        '
        'radLoan
        '
        Me.radLoan.Checked = True
        Me.radLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLoan.Location = New System.Drawing.Point(132, 88)
        Me.radLoan.Name = "radLoan"
        Me.radLoan.Size = New System.Drawing.Size(95, 17)
        Me.radLoan.TabIndex = 2
        Me.radLoan.TabStop = True
        Me.radLoan.Text = "Loan"
        Me.radLoan.UseVisualStyleBackColor = True
        '
        'lblLoanAdvance
        '
        Me.lblLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAdvance.Location = New System.Drawing.Point(12, 88)
        Me.lblLoanAdvance.Name = "lblLoanAdvance"
        Me.lblLoanAdvance.Size = New System.Drawing.Size(113, 17)
        Me.lblLoanAdvance.TabIndex = 341
        Me.lblLoanAdvance.Text = "Select"
        '
        'objpnlLoanProjection
        '
        Me.objpnlLoanProjection.Controls.Add(Me.cboLoanCalcType)
        Me.objpnlLoanProjection.Controls.Add(Me.pnlProjectedAmt)
        Me.objpnlLoanProjection.Controls.Add(Me.cboInterestCalcType)
        Me.objpnlLoanProjection.Controls.Add(Me.lnProjectedAmount)
        Me.objpnlLoanProjection.Controls.Add(Me.lblInterestCalcType)
        Me.objpnlLoanProjection.Controls.Add(Me.pnlInformation)
        Me.objpnlLoanProjection.Controls.Add(Me.elProjectedINSTLAmt)
        Me.objpnlLoanProjection.Controls.Add(Me.lblLoanCalcType)
        Me.objpnlLoanProjection.Location = New System.Drawing.Point(457, 26)
        Me.objpnlLoanProjection.Name = "objpnlLoanProjection"
        Me.objpnlLoanProjection.Size = New System.Drawing.Size(365, 269)
        Me.objpnlLoanProjection.TabIndex = 9
        '
        'cboLoanCalcType
        '
        Me.cboLoanCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboLoanCalcType.DropDownWidth = 250
        Me.cboLoanCalcType.Enabled = False
        Me.cboLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanCalcType.FormattingEnabled = True
        Me.cboLoanCalcType.Location = New System.Drawing.Point(109, 8)
        Me.cboLoanCalcType.Name = "cboLoanCalcType"
        Me.cboLoanCalcType.Size = New System.Drawing.Size(241, 21)
        Me.cboLoanCalcType.TabIndex = 0
        '
        'pnlProjectedAmt
        '
        Me.pnlProjectedAmt.Controls.Add(Me.lblInterestAmt)
        Me.pnlProjectedAmt.Controls.Add(Me.txtInterestAmt)
        Me.pnlProjectedAmt.Controls.Add(Me.lblNetAmount)
        Me.pnlProjectedAmt.Controls.Add(Me.txtNetAmount)
        Me.pnlProjectedAmt.Location = New System.Drawing.Point(9, 94)
        Me.pnlProjectedAmt.Name = "pnlProjectedAmt"
        Me.pnlProjectedAmt.Size = New System.Drawing.Size(352, 53)
        Me.pnlProjectedAmt.TabIndex = 33
        '
        'lblInterestAmt
        '
        Me.lblInterestAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestAmt.Location = New System.Drawing.Point(5, 5)
        Me.lblInterestAmt.Name = "lblInterestAmt"
        Me.lblInterestAmt.Size = New System.Drawing.Size(92, 16)
        Me.lblInterestAmt.TabIndex = 0
        Me.lblInterestAmt.Text = "Interest Amount"
        Me.lblInterestAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInterestAmt
        '
        Me.txtInterestAmt.AllowNegative = True
        Me.txtInterestAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInterestAmt.DigitsInGroup = 3
        Me.txtInterestAmt.Flags = 0
        Me.txtInterestAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterestAmt.Location = New System.Drawing.Point(103, 3)
        Me.txtInterestAmt.MaxDecimalPlaces = 6
        Me.txtInterestAmt.MaxWholeDigits = 21
        Me.txtInterestAmt.Name = "txtInterestAmt"
        Me.txtInterestAmt.Prefix = ""
        Me.txtInterestAmt.RangeMax = 1.7976931348623157E+308
        Me.txtInterestAmt.RangeMin = -1.7976931348623157E+308
        Me.txtInterestAmt.ReadOnly = True
        Me.txtInterestAmt.Size = New System.Drawing.Size(238, 21)
        Me.txtInterestAmt.TabIndex = 0
        Me.txtInterestAmt.Text = "0"
        Me.txtInterestAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNetAmount
        '
        Me.lblNetAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetAmount.Location = New System.Drawing.Point(5, 32)
        Me.lblNetAmount.Name = "lblNetAmount"
        Me.lblNetAmount.Size = New System.Drawing.Size(92, 17)
        Me.lblNetAmount.TabIndex = 2
        Me.lblNetAmount.Text = "Net Amount"
        '
        'txtNetAmount
        '
        Me.txtNetAmount.AllowNegative = True
        Me.txtNetAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNetAmount.DigitsInGroup = 3
        Me.txtNetAmount.Flags = 0
        Me.txtNetAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNetAmount.Location = New System.Drawing.Point(103, 30)
        Me.txtNetAmount.MaxDecimalPlaces = 6
        Me.txtNetAmount.MaxWholeDigits = 21
        Me.txtNetAmount.Name = "txtNetAmount"
        Me.txtNetAmount.Prefix = ""
        Me.txtNetAmount.RangeMax = 1.7976931348623157E+308
        Me.txtNetAmount.RangeMin = -1.7976931348623157E+308
        Me.txtNetAmount.ReadOnly = True
        Me.txtNetAmount.Size = New System.Drawing.Size(238, 21)
        Me.txtNetAmount.TabIndex = 1
        Me.txtNetAmount.Text = "0"
        Me.txtNetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboInterestCalcType
        '
        Me.cboInterestCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboInterestCalcType.Enabled = False
        Me.cboInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterestCalcType.FormattingEnabled = True
        Me.cboInterestCalcType.Location = New System.Drawing.Point(109, 35)
        Me.cboInterestCalcType.Name = "cboInterestCalcType"
        Me.cboInterestCalcType.Size = New System.Drawing.Size(241, 21)
        Me.cboInterestCalcType.TabIndex = 1
        '
        'lnProjectedAmount
        '
        Me.lnProjectedAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnProjectedAmount.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnProjectedAmount.Location = New System.Drawing.Point(7, 71)
        Me.lnProjectedAmount.Name = "lnProjectedAmount"
        Me.lnProjectedAmount.Size = New System.Drawing.Size(343, 17)
        Me.lnProjectedAmount.TabIndex = 32
        Me.lnProjectedAmount.Text = "Projected Amount"
        '
        'lblInterestCalcType
        '
        Me.lblInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestCalcType.Location = New System.Drawing.Point(7, 37)
        Me.lblInterestCalcType.Name = "lblInterestCalcType"
        Me.lblInterestCalcType.Size = New System.Drawing.Size(96, 17)
        Me.lblInterestCalcType.TabIndex = 334
        Me.lblInterestCalcType.Text = "Int. Calc. Type"
        Me.lblInterestCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlInformation
        '
        Me.pnlInformation.Controls.Add(Me.lblIntAmt)
        Me.pnlInformation.Controls.Add(Me.lblPrincipalAmt)
        Me.pnlInformation.Controls.Add(Me.txtIntAmt)
        Me.pnlInformation.Controls.Add(Me.txtPrincipalAmt)
        Me.pnlInformation.Controls.Add(Me.txtLoanRate)
        Me.pnlInformation.Controls.Add(Me.lblLoanInterest)
        Me.pnlInformation.Controls.Add(Me.lblEMIInstallments)
        Me.pnlInformation.Controls.Add(Me.nudDuration)
        Me.pnlInformation.Controls.Add(Me.lblEMIAmount)
        Me.pnlInformation.Controls.Add(Me.txtInstallmentAmt)
        Me.pnlInformation.Location = New System.Drawing.Point(9, 183)
        Me.pnlInformation.Name = "pnlInformation"
        Me.pnlInformation.Size = New System.Drawing.Size(352, 79)
        Me.pnlInformation.TabIndex = 2
        '
        'lblIntAmt
        '
        Me.lblIntAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIntAmt.Location = New System.Drawing.Point(194, 32)
        Me.lblIntAmt.Name = "lblIntAmt"
        Me.lblIntAmt.Size = New System.Drawing.Size(90, 15)
        Me.lblIntAmt.TabIndex = 28
        Me.lblIntAmt.Text = "Interest Amt."
        Me.lblIntAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPrincipalAmt
        '
        Me.lblPrincipalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrincipalAmt.Location = New System.Drawing.Point(3, 32)
        Me.lblPrincipalAmt.Name = "lblPrincipalAmt"
        Me.lblPrincipalAmt.Size = New System.Drawing.Size(92, 17)
        Me.lblPrincipalAmt.TabIndex = 27
        Me.lblPrincipalAmt.Text = "Principal Amt."
        Me.lblPrincipalAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIntAmt
        '
        Me.txtIntAmt.AllowNegative = True
        Me.txtIntAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtIntAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtIntAmt.DigitsInGroup = 3
        Me.txtIntAmt.Flags = 0
        Me.txtIntAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIntAmt.Location = New System.Drawing.Point(290, 29)
        Me.txtIntAmt.MaxDecimalPlaces = 6
        Me.txtIntAmt.MaxWholeDigits = 21
        Me.txtIntAmt.Name = "txtIntAmt"
        Me.txtIntAmt.Prefix = ""
        Me.txtIntAmt.RangeMax = 1.7976931348623157E+308
        Me.txtIntAmt.RangeMin = -1.7976931348623157E+308
        Me.txtIntAmt.ReadOnly = True
        Me.txtIntAmt.Size = New System.Drawing.Size(51, 21)
        Me.txtIntAmt.TabIndex = 2
        Me.txtIntAmt.Text = "0"
        Me.txtIntAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrincipalAmt
        '
        Me.txtPrincipalAmt.AllowNegative = True
        Me.txtPrincipalAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtPrincipalAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPrincipalAmt.DigitsInGroup = 3
        Me.txtPrincipalAmt.Flags = 0
        Me.txtPrincipalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrincipalAmt.Location = New System.Drawing.Point(103, 29)
        Me.txtPrincipalAmt.MaxDecimalPlaces = 6
        Me.txtPrincipalAmt.MaxWholeDigits = 21
        Me.txtPrincipalAmt.Name = "txtPrincipalAmt"
        Me.txtPrincipalAmt.Prefix = ""
        Me.txtPrincipalAmt.RangeMax = 1.7976931348623157E+308
        Me.txtPrincipalAmt.RangeMin = -1.7976931348623157E+308
        Me.txtPrincipalAmt.ReadOnly = True
        Me.txtPrincipalAmt.Size = New System.Drawing.Size(85, 21)
        Me.txtPrincipalAmt.TabIndex = 3
        Me.txtPrincipalAmt.Text = "0"
        Me.txtPrincipalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLoanRate
        '
        Me.txtLoanRate.AllowNegative = True
        Me.txtLoanRate.BackColor = System.Drawing.SystemColors.Window
        Me.txtLoanRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanRate.DigitsInGroup = 0
        Me.txtLoanRate.Flags = 0
        Me.txtLoanRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanRate.Location = New System.Drawing.Point(103, 2)
        Me.txtLoanRate.MaxDecimalPlaces = 6
        Me.txtLoanRate.MaxWholeDigits = 21
        Me.txtLoanRate.Name = "txtLoanRate"
        Me.txtLoanRate.Prefix = ""
        Me.txtLoanRate.RangeMax = 1.7976931348623157E+308
        Me.txtLoanRate.RangeMin = -1.7976931348623157E+308
        Me.txtLoanRate.ReadOnly = True
        Me.txtLoanRate.Size = New System.Drawing.Size(48, 21)
        Me.txtLoanRate.TabIndex = 0
        Me.txtLoanRate.Text = "0"
        Me.txtLoanRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLoanInterest
        '
        Me.lblLoanInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanInterest.Location = New System.Drawing.Point(3, 5)
        Me.lblLoanInterest.Name = "lblLoanInterest"
        Me.lblLoanInterest.Size = New System.Drawing.Size(94, 17)
        Me.lblLoanInterest.TabIndex = 17
        Me.lblLoanInterest.Text = "Rate (%)"
        Me.lblLoanInterest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEMIInstallments
        '
        Me.lblEMIInstallments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIInstallments.Location = New System.Drawing.Point(192, 4)
        Me.lblEMIInstallments.Name = "lblEMIInstallments"
        Me.lblEMIInstallments.Size = New System.Drawing.Size(92, 15)
        Me.lblEMIInstallments.TabIndex = 19
        Me.lblEMIInstallments.Text = "INSTL(In Months)"
        '
        'nudDuration
        '
        Me.nudDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDuration.Location = New System.Drawing.Point(290, 2)
        Me.nudDuration.Maximum = New Decimal(New Integer() {-402653185, -1613725636, 54210108, 0})
        Me.nudDuration.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudDuration.Name = "nudDuration"
        Me.nudDuration.Size = New System.Drawing.Size(51, 21)
        Me.nudDuration.TabIndex = 1
        Me.nudDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudDuration.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblEMIAmount
        '
        Me.lblEMIAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIAmount.Location = New System.Drawing.Point(3, 59)
        Me.lblEMIAmount.Name = "lblEMIAmount"
        Me.lblEMIAmount.Size = New System.Drawing.Size(92, 17)
        Me.lblEMIAmount.TabIndex = 23
        Me.lblEMIAmount.Text = "INSTL Amt."
        Me.lblEMIAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInstallmentAmt
        '
        Me.txtInstallmentAmt.AllowNegative = True
        Me.txtInstallmentAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtInstallmentAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInstallmentAmt.DigitsInGroup = 3
        Me.txtInstallmentAmt.Flags = 0
        Me.txtInstallmentAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInstallmentAmt.Location = New System.Drawing.Point(103, 56)
        Me.txtInstallmentAmt.MaxDecimalPlaces = 6
        Me.txtInstallmentAmt.MaxWholeDigits = 21
        Me.txtInstallmentAmt.Name = "txtInstallmentAmt"
        Me.txtInstallmentAmt.Prefix = ""
        Me.txtInstallmentAmt.RangeMax = 1.7976931348623157E+308
        Me.txtInstallmentAmt.RangeMin = -1.7976931348623157E+308
        Me.txtInstallmentAmt.Size = New System.Drawing.Size(238, 21)
        Me.txtInstallmentAmt.TabIndex = 4
        Me.txtInstallmentAmt.Text = "0"
        Me.txtInstallmentAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'elProjectedINSTLAmt
        '
        Me.elProjectedINSTLAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elProjectedINSTLAmt.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elProjectedINSTLAmt.Location = New System.Drawing.Point(7, 158)
        Me.elProjectedINSTLAmt.Name = "elProjectedINSTLAmt"
        Me.elProjectedINSTLAmt.Size = New System.Drawing.Size(343, 17)
        Me.elProjectedINSTLAmt.TabIndex = 34
        Me.elProjectedINSTLAmt.Text = "Projected Installment Amount (First Installment)"
        '
        'lblLoanCalcType
        '
        Me.lblLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanCalcType.Location = New System.Drawing.Point(7, 10)
        Me.lblLoanCalcType.Name = "lblLoanCalcType"
        Me.lblLoanCalcType.Size = New System.Drawing.Size(96, 17)
        Me.lblLoanCalcType.TabIndex = 332
        Me.lblLoanCalcType.Text = "Loan Calc. Type"
        Me.lblLoanCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLine1
        '
        Me.objLine1.BackColor = System.Drawing.Color.Transparent
        Me.objLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objLine1.Location = New System.Drawing.Point(453, 24)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(5, 272)
        Me.objLine1.TabIndex = 337
        Me.objLine1.Text = "EZeeStraightLine2"
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(12, 140)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(114, 17)
        Me.lblLoanScheme.TabIndex = 12
        Me.lblLoanScheme.Text = "Loan Scheme"
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Items.AddRange(New Object() {"Select"})
        Me.cboLoanScheme.Location = New System.Drawing.Point(132, 138)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(288, 21)
        Me.cboLoanScheme.TabIndex = 5
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(132, 165)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(58, 21)
        Me.cboCurrency.TabIndex = 6
        '
        'cboDeductionPeriod
        '
        Me.cboDeductionPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeductionPeriod.FormattingEnabled = True
        Me.cboDeductionPeriod.Location = New System.Drawing.Point(132, 111)
        Me.cboDeductionPeriod.Name = "cboDeductionPeriod"
        Me.cboDeductionPeriod.Size = New System.Drawing.Size(288, 21)
        Me.cboDeductionPeriod.TabIndex = 4
        '
        'objbtnSearchDeductionPeriod
        '
        Me.objbtnSearchDeductionPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDeductionPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeductionPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeductionPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDeductionPeriod.BorderSelected = False
        Me.objbtnSearchDeductionPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDeductionPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDeductionPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDeductionPeriod.Location = New System.Drawing.Point(426, 111)
        Me.objbtnSearchDeductionPeriod.Name = "objbtnSearchDeductionPeriod"
        Me.objbtnSearchDeductionPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDeductionPeriod.TabIndex = 23
        '
        'lblDeductionPeriod
        '
        Me.lblDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeductionPeriod.Location = New System.Drawing.Point(12, 113)
        Me.lblDeductionPeriod.Name = "lblDeductionPeriod"
        Me.lblDeductionPeriod.Size = New System.Drawing.Size(114, 17)
        Me.lblDeductionPeriod.TabIndex = 21
        Me.lblDeductionPeriod.Text = "Deduction Period"
        Me.lblDeductionPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExternalEntity
        '
        Me.txtExternalEntity.Flags = 0
        Me.txtExternalEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExternalEntity.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtExternalEntity.Location = New System.Drawing.Point(155, 138)
        Me.txtExternalEntity.Name = "txtExternalEntity"
        Me.txtExternalEntity.Size = New System.Drawing.Size(102, 21)
        Me.txtExternalEntity.TabIndex = 11
        '
        'lblEmpRemark
        '
        Me.lblEmpRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpRemark.Location = New System.Drawing.Point(11, 227)
        Me.lblEmpRemark.Name = "lblEmpRemark"
        Me.lblEmpRemark.Size = New System.Drawing.Size(114, 17)
        Me.lblEmpRemark.TabIndex = 29
        Me.lblEmpRemark.Text = "Employee Remarks"
        '
        'txtEmployeeRemark
        '
        Me.txtEmployeeRemark.Flags = 0
        Me.txtEmployeeRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(124)}
        Me.txtEmployeeRemark.Location = New System.Drawing.Point(132, 222)
        Me.txtEmployeeRemark.Multiline = True
        Me.txtEmployeeRemark.Name = "txtEmployeeRemark"
        Me.txtEmployeeRemark.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtEmployeeRemark.Size = New System.Drawing.Size(288, 73)
        Me.txtEmployeeRemark.TabIndex = 8
        '
        'chkExternalEntity
        '
        Me.chkExternalEntity.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkExternalEntity.Checked = True
        Me.chkExternalEntity.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkExternalEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExternalEntity.Location = New System.Drawing.Point(12, 140)
        Me.chkExternalEntity.Name = "chkExternalEntity"
        Me.chkExternalEntity.Size = New System.Drawing.Size(114, 17)
        Me.chkExternalEntity.TabIndex = 10
        Me.chkExternalEntity.Text = "Is External Entity"
        Me.chkExternalEntity.UseVisualStyleBackColor = True
        '
        'txtApplicationNo
        '
        Me.txtApplicationNo.Flags = 0
        Me.txtApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicationNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplicationNo.Location = New System.Drawing.Point(132, 34)
        Me.txtApplicationNo.Name = "txtApplicationNo"
        Me.txtApplicationNo.Size = New System.Drawing.Size(125, 21)
        Me.txtApplicationNo.TabIndex = 0
        '
        'dtpApplicationDate
        '
        Me.dtpApplicationDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplicationDate.Checked = False
        Me.dtpApplicationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplicationDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApplicationDate.Location = New System.Drawing.Point(319, 34)
        Me.dtpApplicationDate.Name = "dtpApplicationDate"
        Me.dtpApplicationDate.Size = New System.Drawing.Size(101, 21)
        Me.dtpApplicationDate.TabIndex = 3
        '
        'lblApplicationDate
        '
        Me.lblApplicationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicationDate.Location = New System.Drawing.Point(263, 37)
        Me.lblApplicationDate.Name = "lblApplicationDate"
        Me.lblApplicationDate.Size = New System.Drawing.Size(50, 15)
        Me.lblApplicationDate.TabIndex = 2
        Me.lblApplicationDate.Text = "Date"
        '
        'lblApplicationNo
        '
        Me.lblApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicationNo.Location = New System.Drawing.Point(12, 36)
        Me.lblApplicationNo.Name = "lblApplicationNo"
        Me.lblApplicationNo.Size = New System.Drawing.Size(114, 17)
        Me.lblApplicationNo.TabIndex = 0
        Me.lblApplicationNo.Text = "Application No."
        '
        'objbtnAddLoanScheme
        '
        Me.objbtnAddLoanScheme.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddLoanScheme.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddLoanScheme.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddLoanScheme.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddLoanScheme.BorderSelected = False
        Me.objbtnAddLoanScheme.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddLoanScheme.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddLoanScheme.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddLoanScheme.Location = New System.Drawing.Point(427, 138)
        Me.objbtnAddLoanScheme.Name = "objbtnAddLoanScheme"
        Me.objbtnAddLoanScheme.Size = New System.Drawing.Size(20, 21)
        Me.objbtnAddLoanScheme.TabIndex = 15
        '
        'lblAmt
        '
        Me.lblAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmt.Location = New System.Drawing.Point(12, 166)
        Me.lblAmt.Name = "lblAmt"
        Me.lblAmt.Size = New System.Drawing.Size(114, 17)
        Me.lblAmt.TabIndex = 16
        Me.lblAmt.Text = "Amount"
        Me.lblAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLoanAmt
        '
        Me.txtLoanAmt.AllowNegative = False
        Me.txtLoanAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanAmt.DigitsInGroup = 3
        Me.txtLoanAmt.Flags = 65536
        Me.txtLoanAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanAmt.Location = New System.Drawing.Point(196, 165)
        Me.txtLoanAmt.MaxDecimalPlaces = 6
        Me.txtLoanAmt.MaxWholeDigits = 21
        Me.txtLoanAmt.Name = "txtLoanAmt"
        Me.txtLoanAmt.Prefix = ""
        Me.txtLoanAmt.RangeMax = 1.7976931348623157E+308
        Me.txtLoanAmt.RangeMin = -1.7976931348623157E+308
        Me.txtLoanAmt.Size = New System.Drawing.Size(224, 21)
        Me.txtLoanAmt.TabIndex = 7
        Me.txtLoanAmt.Text = "0"
        Me.txtLoanAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboEmpName
        '
        Me.cboEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpName.FormattingEnabled = True
        Me.cboEmpName.Items.AddRange(New Object() {"Select"})
        Me.cboEmpName.Location = New System.Drawing.Point(132, 61)
        Me.cboEmpName.Name = "cboEmpName"
        Me.cboEmpName.Size = New System.Drawing.Size(288, 21)
        Me.cboEmpName.TabIndex = 1
        '
        'lblEmpName
        '
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(12, 63)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(114, 17)
        Me.lblEmpName.TabIndex = 4
        Me.lblEmpName.Text = "Employee"
        '
        'objLoanSchemeSearch
        '
        Me.objLoanSchemeSearch.BackColor = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objLoanSchemeSearch.BorderSelected = False
        Me.objLoanSchemeSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objLoanSchemeSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objLoanSchemeSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objLoanSchemeSearch.Location = New System.Drawing.Point(427, 138)
        Me.objLoanSchemeSearch.Name = "objLoanSchemeSearch"
        Me.objLoanSchemeSearch.Size = New System.Drawing.Size(20, 21)
        Me.objLoanSchemeSearch.TabIndex = 14
        Me.objLoanSchemeSearch.Visible = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(426, 61)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 6
        Me.objbtnSearchEmployee.Visible = False
        '
        'objvalPeriodDuration
        '
        Me.objvalPeriodDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objvalPeriodDuration.Location = New System.Drawing.Point(9, 8)
        Me.objvalPeriodDuration.Name = "objvalPeriodDuration"
        Me.objvalPeriodDuration.Size = New System.Drawing.Size(597, 18)
        Me.objvalPeriodDuration.TabIndex = 24
        Me.objvalPeriodDuration.Text = "#periodDuration"
        '
        'nudDurationInMths
        '
        Me.nudDurationInMths.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDurationInMths.Location = New System.Drawing.Point(646, 19)
        Me.nudDurationInMths.Name = "nudDurationInMths"
        Me.nudDurationInMths.Size = New System.Drawing.Size(22, 21)
        Me.nudDurationInMths.TabIndex = 20
        Me.nudDurationInMths.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudDurationInMths.Visible = False
        '
        'lblDuration
        '
        Me.lblDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDuration.Location = New System.Drawing.Point(646, 19)
        Me.lblDuration.Name = "lblDuration"
        Me.lblDuration.Size = New System.Drawing.Size(22, 21)
        Me.lblDuration.TabIndex = 19
        Me.lblDuration.Text = "Duration In Months"
        Me.lblDuration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblDuration.Visible = False
        '
        'txtEMIInstallments
        '
        Me.txtEMIInstallments.AllowNegative = False
        Me.txtEMIInstallments.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtEMIInstallments.DigitsInGroup = 0
        Me.txtEMIInstallments.Flags = 65536
        Me.txtEMIInstallments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEMIInstallments.Location = New System.Drawing.Point(646, 19)
        Me.txtEMIInstallments.MaxDecimalPlaces = 0
        Me.txtEMIInstallments.MaxWholeDigits = 4
        Me.txtEMIInstallments.Name = "txtEMIInstallments"
        Me.txtEMIInstallments.Prefix = ""
        Me.txtEMIInstallments.RangeMax = 1.7976931348623157E+308
        Me.txtEMIInstallments.RangeMin = -1.7976931348623157E+308
        Me.txtEMIInstallments.Size = New System.Drawing.Size(22, 21)
        Me.txtEMIInstallments.TabIndex = 28
        Me.txtEMIInstallments.Text = "0"
        Me.txtEMIInstallments.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtEMIInstallments.Visible = False
        '
        'lblEMIInstallments1
        '
        Me.lblEMIInstallments1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIInstallments1.Location = New System.Drawing.Point(646, 19)
        Me.lblEMIInstallments1.Name = "lblEMIInstallments1"
        Me.lblEMIInstallments1.Size = New System.Drawing.Size(22, 21)
        Me.lblEMIInstallments1.TabIndex = 27
        Me.lblEMIInstallments1.Text = "No. of Installments"
        Me.lblEMIInstallments1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEMIInstallments1.Visible = False
        '
        'txtInstallmentAmt1
        '
        Me.txtInstallmentAmt1.AllowNegative = False
        Me.txtInstallmentAmt1.BackColor = System.Drawing.SystemColors.Window
        Me.txtInstallmentAmt1.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInstallmentAmt1.DigitsInGroup = 0
        Me.txtInstallmentAmt1.Flags = 65536
        Me.txtInstallmentAmt1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInstallmentAmt1.Location = New System.Drawing.Point(646, 19)
        Me.txtInstallmentAmt1.MaxDecimalPlaces = 6
        Me.txtInstallmentAmt1.MaxWholeDigits = 21
        Me.txtInstallmentAmt1.Name = "txtInstallmentAmt1"
        Me.txtInstallmentAmt1.Prefix = ""
        Me.txtInstallmentAmt1.RangeMax = 1.7976931348623157E+308
        Me.txtInstallmentAmt1.RangeMin = -1.7976931348623157E+308
        Me.txtInstallmentAmt1.Size = New System.Drawing.Size(22, 21)
        Me.txtInstallmentAmt1.TabIndex = 0
        Me.txtInstallmentAmt1.Text = "0"
        Me.txtInstallmentAmt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtInstallmentAmt1.Visible = False
        '
        'lblEMIAmount1
        '
        Me.lblEMIAmount1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIAmount1.Location = New System.Drawing.Point(646, 19)
        Me.lblEMIAmount1.Name = "lblEMIAmount1"
        Me.lblEMIAmount1.Size = New System.Drawing.Size(22, 21)
        Me.lblEMIAmount1.TabIndex = 25
        Me.lblEMIAmount1.Text = "Installment Amt."
        Me.lblEMIAmount1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblEMIAmount1.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblExRate)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.txtInstallmentAmt1)
        Me.objFooter.Controls.Add(Me.lblEMIAmount1)
        Me.objFooter.Controls.Add(Me.lblEMIInstallments1)
        Me.objFooter.Controls.Add(Me.objvalPeriodDuration)
        Me.objFooter.Controls.Add(Me.txtEMIInstallments)
        Me.objFooter.Controls.Add(Me.nudDurationInMths)
        Me.objFooter.Controls.Add(Me.lblDuration)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 303)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(824, 55)
        Me.objFooter.TabIndex = 1
        '
        'objlblExRate
        '
        Me.objlblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExRate.Location = New System.Drawing.Point(9, 30)
        Me.objlblExRate.Name = "objlblExRate"
        Me.objlblExRate.Size = New System.Drawing.Size(597, 18)
        Me.objlblExRate.TabIndex = 29
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(612, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(715, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmLoanApplication_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(824, 358)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbProcessPendingLoanInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanApplication_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add / Edit Loan Application"
        Me.gbProcessPendingLoanInfo.ResumeLayout(False)
        Me.gbProcessPendingLoanInfo.PerformLayout()
        Me.objpnlLoanProjection.ResumeLayout(False)
        Me.pnlProjectedAmt.ResumeLayout(False)
        Me.pnlProjectedAmt.PerformLayout()
        Me.pnlInformation.ResumeLayout(False)
        Me.pnlInformation.PerformLayout()
        CType(Me.nudDuration, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDurationInMths, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.objFooter.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbProcessPendingLoanInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboEmpName As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddLoanScheme As eZee.Common.eZeeGradientButton
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents lblAmt As System.Windows.Forms.Label
    Friend WithEvents txtLoanAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents dtpApplicationDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtApplicationNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblApplicationDate As System.Windows.Forms.Label
    Friend WithEvents lblApplicationNo As System.Windows.Forms.Label
    Friend WithEvents chkExternalEntity As System.Windows.Forms.CheckBox
    Friend WithEvents txtExternalEntity As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objLoanSchemeSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmpRemark As System.Windows.Forms.Label
    Friend WithEvents txtEmployeeRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboDeductionPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblDeductionPeriod As System.Windows.Forms.Label
    Friend WithEvents txtEMIInstallments As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEMIInstallments1 As System.Windows.Forms.Label
    Friend WithEvents txtInstallmentAmt1 As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEMIAmount1 As System.Windows.Forms.Label
    Friend WithEvents nudDurationInMths As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblDuration As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchDeductionPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents objvalPeriodDuration As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents elProjectedINSTLAmt As eZee.Common.eZeeLine
    Friend WithEvents pnlInformation As System.Windows.Forms.Panel
    Friend WithEvents lblIntAmt As System.Windows.Forms.Label
    Friend WithEvents lblPrincipalAmt As System.Windows.Forms.Label
    Friend WithEvents txtIntAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents txtPrincipalAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents txtLoanRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblLoanInterest As System.Windows.Forms.Label
    Friend WithEvents lblEMIInstallments As System.Windows.Forms.Label
    Friend WithEvents nudDuration As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblEMIAmount As System.Windows.Forms.Label
    Friend WithEvents txtInstallmentAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lnProjectedAmount As eZee.Common.eZeeLine
    Friend WithEvents pnlProjectedAmt As System.Windows.Forms.Panel
    Friend WithEvents lblInterestAmt As System.Windows.Forms.Label
    Friend WithEvents txtInterestAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblNetAmount As System.Windows.Forms.Label
    Friend WithEvents txtNetAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents cboInterestCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblInterestCalcType As System.Windows.Forms.Label
    Friend WithEvents cboLoanCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanCalcType As System.Windows.Forms.Label
    Friend WithEvents objLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objpnlLoanProjection As System.Windows.Forms.Panel
    Friend WithEvents objlblExRate As System.Windows.Forms.Label
    Friend WithEvents radAdvance As System.Windows.Forms.RadioButton
    Friend WithEvents radLoan As System.Windows.Forms.RadioButton
    Friend WithEvents lblLoanAdvance As System.Windows.Forms.Label
    Friend WithEvents lblLoanAccNumber As System.Windows.Forms.Label
    Friend WithEvents txtLoanAccNumber As eZee.TextBox.AlphanumericTextBox
End Class
