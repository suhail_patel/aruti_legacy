﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Math
Imports System.Threading

#End Region

Public Class frmLoanApplication_AddEdit

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmLoanApplication_AddEdit"
    Private objProcesspendingloan As clsProcess_pending_loan
    'Nilay (08-Dec-2016) -- Start
    'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
    'Private objStatusTran As clsProcess_pending_loan
    'Nilay (08-Dec-2016) -- End
    Private mintProcessPendingLoanUnkid As Integer = -1
    Private mintEmployeeSelectedId As Integer = -1
    Private mintApproverSelectedId As Integer = -1
    Private mdecLoanAmount As Decimal = 0
    Private mdecApprovedAmount As Decimal = 0
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    
    Private mstrStatusData As String = String.Empty
    Private mintLoanAdvaceunkid As Integer
    Private mdecInterestAmount As Decimal
    Private mdecNetAmount As Decimal
    Private mblnIsAdvance As Boolean
    Private mdecOldAmount As Decimal = 0
    Private mintNewStatusId As Integer = 0
    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private mstrSearchText As String = String.Empty
    'Nilay (27-Oct-2016) -- End

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 52
    Private mdecMaxLoanAmountDefined As Decimal = 0
    'S.SANDEEP [20-SEP-2017] -- END

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
    Private mintLastCalcTypeId As Integer = 0
    Private mdecInstallmentAmount As Decimal = 0
    Private mintNoOfInstallment As Integer = 1
    'S.SANDEEP [20-SEP-2017] -- END

    'Sohail (22 Sep 2017) -- Start
    'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    Private mdecEMI_NetPayPercentage As Decimal = 0
    Private mstrBaseCurrSign As String
    Private mintBaseCurrId As Integer
    Private mintPaidCurrId As Integer
    Private mdecBaseExRate As Decimal
    Private mdecPaidExRate As Decimal
    'Sohail (22 Sep 2017) -- End
    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
    Private mintTranheadUnkid As Integer = 0
    'Sohail (11 Apr 2018) -- End
    'Hemant (30 Aug 2019) -- Start
    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
    Private trd As Thread
    Private objEmailList As New List(Of clsEmailCollection)
    'Hemant (30 Aug 2019) -- End

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intNewStatusId As Integer = 0) As Boolean
        Try
            mintProcessPendingLoanUnkid = intUnkId
            menAction = eAction
            mintNewStatusId = intNewStatusId

            Me.ShowDialog()

            intUnkId = mintProcessPendingLoanUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region "Private Methods"

    Private Sub SetColor()
        Try
            cboEmpName.BackColor = GUI.ColorComp
            cboLoanScheme.BackColor = GUI.ColorComp
            cboDeductionPeriod.BackColor = GUI.ColorComp
            cboCurrency.BackColor = GUI.ColorComp
            txtApplicationNo.BackColor = GUI.ColorComp
            txtExternalEntity.BackColor = GUI.ColorComp
            txtLoanAmt.BackColor = GUI.ColorComp
            nudDurationInMths.BackColor = GUI.ColorComp
            txtInstallmentAmt.BackColor = GUI.ColorComp
            txtEMIInstallments.BackColor = GUI.ColorComp
            txtEmployeeRemark.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Public Sub SetValue()
        Try
            If radLoan.Checked Then
                objProcesspendingloan._Isloan = True
            Else
                objProcesspendingloan._Isloan = False
            End If
            objProcesspendingloan._Application_No = txtApplicationNo.Text
            objProcesspendingloan._Application_Date = dtpApplicationDate.Value
            objProcesspendingloan._Loan_Amount = txtLoanAmt.Decimal
            objProcesspendingloan._Employeeunkid = CInt(cboEmpName.SelectedValue)
            objProcesspendingloan._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            objProcesspendingloan._Isexternal_Entity = CBool(chkExternalEntity.CheckState)
            objProcesspendingloan._External_Entity_Name = txtExternalEntity.Text
            objProcesspendingloan._Countryunkid = CInt(cboCurrency.SelectedValue)
            objProcesspendingloan._DeductionPeriodunkid = CInt(cboDeductionPeriod.SelectedValue)
            'Nilay (21-Oct-2015) -- Start
            'ENHANCEMENT : NEW LOAN Given By Rutta
            'objProcesspendingloan._DurationInMonths = CInt(nudDurationInMths.Value)
            'Nilay (21-Oct-2015) -- End

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            'objProcesspendingloan._InstallmentAmount = CDec(txtInstallmentAmt.Tag)
            'objProcesspendingloan._NoOfInstallment = CInt(txtEMIInstallments.Decimal)

            objProcesspendingloan._InstallmentAmount = CDec(txtPrincipalAmt.Decimal)
            objProcesspendingloan._NoOfInstallment = CInt(nudDuration.Value)
            'S.SANDEEP [20-SEP-2017] -- END
            objProcesspendingloan._Loan_Statusunkid = 1 'Pending
            objProcesspendingloan._Emp_Remark = txtEmployeeRemark.Text
            objProcesspendingloan._Userunkid = User._Object._Userunkid
            objProcesspendingloan._Isvoid = False
            objProcesspendingloan._Voiddatetime = Nothing
            objProcesspendingloan._Voiduserunkid = -1
            objProcesspendingloan._IsLoanApprover_ForLoanScheme = ConfigParameter._Object._IsLoanApprover_ForLoanScheme

            'Shani(26-Nov-2015) -- Start
            'ENHANCEMENT : Add Loan Import Form
            objProcesspendingloan._IsImportedLoan = False
            'Shani(26-Nov-2015) -- End

            'Hemant (02 Jan 2019) -- Start
            'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
            objProcesspendingloan._Loan_Account_No = txtLoanAccNumber.Text
            'Hemant (02 Jan 2019) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try

    End Sub

    Public Sub GetValue()

        Try
            If menAction = enAction.EDIT_ONE Then
                If objProcesspendingloan._Isloan = True Then
                    radLoan.Checked = True
                Else
                    radAdvance.Checked = True
                End If
                chkExternalEntity.Checked = objProcesspendingloan._Isexternal_Entity
                txtExternalEntity.Text = objProcesspendingloan._External_Entity_Name
            Else
                radLoan.Checked = True
                chkExternalEntity.CheckState = CheckState.Checked
            End If
            If Not (objProcesspendingloan._Application_Date = Nothing) Then
                dtpApplicationDate.Value = objProcesspendingloan._Application_Date
            End If
            txtApplicationNo.Text = objProcesspendingloan._Application_No
            cboEmpName.SelectedValue = CStr(objProcesspendingloan._Employeeunkid)

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            RemoveHandler cboLoanCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
            txtLoanAmt.Text = Format(objProcesspendingloan._Loan_Amount, GUI.fmtCurrency)
            cboLoanScheme.SelectedValue = CInt(objProcesspendingloan._Loanschemeunkid)

            'Varsha (11 Jan 2018) -- Start
            'Issue : In Edit mode Installment amount is not get from database and when we will Change the no. of installment months then it will not modified.
            'Sohail (09 Feb 2018) -- Start
            'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
            'If menAction = enAction.EDIT_ONE AndAlso CInt(cboLoanCalcType.SelectedValue) = CInt(enLoanCalcId.No_Interest) Then
            '    nudDuration.Enabled = False
            'Else
            '    nudDuration.Enabled = True
            'End If
            'Sohail (09 Feb 2018) -- End
            'Varsha (11 Jan 2018) -- End

            AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            AddHandler cboLoanCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
            'S.SANDEEP [20-SEP-2017] -- START


            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            cboDeductionPeriod.SelectedValue = objProcesspendingloan._DeductionPeriodunkid
            RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            txtPrincipalAmt.Text = Format(objProcesspendingloan._InstallmentAmount, GUI.fmtCurrency)
            AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            'Sohail (09 Feb 2018) -- Start
            'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
            'nudDuration.Value = CDec(IIf(objProcesspendingloan._NoOfInstallment <= 0, 1, objProcesspendingloan._NoOfInstallment))
            RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
            nudDuration.Value = CDec(IIf(objProcesspendingloan._NoOfInstallment <= 0, 1, objProcesspendingloan._NoOfInstallment))
            AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
            'Sohail (09 Feb 2018) -- End
            'S.SANDEEP [20-SEP-2017] -- END

            objProcesspendingloan._Isvoid = objProcesspendingloan._Isvoid
            objProcesspendingloan._Voiddatetime = objProcesspendingloan._Voiddatetime
            objProcesspendingloan._Voiduserunkid = objProcesspendingloan._Voiduserunkid
            txtEmployeeRemark.Text = objProcesspendingloan._Emp_Remark
            cboCurrency.SelectedValue = objProcesspendingloan._Countryunkid

            'Hemant (02 Jan 2019) -- Start
            'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
            txtLoanAccNumber.Text = objProcesspendingloan._Loan_Account_No
            'Hemant (02 Jan 2019) -- End

            'Nilay (21-Oct-2015) -- Start
            'ENHANCEMENT : NEW LOAN Given By Rutta
            'nudDurationInMths.Value = objProcesspendingloan._DurationInMonths
            'Nilay (21-Oct-2015) -- End

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            'cboDeductionPeriod.SelectedValue = objProcesspendingloan._DeductionPeriodunkid

            'Varsha (11 Jan 2018) -- Start
            'Issue : In Edit mode Installment amount is not get from database and when we will Change the no. of installment months then it will not modified.
            'txtInstallmentAmt.Text = Format(objProcesspendingloan._InstallmentAmount, GUI.fmtCurrency)
            'Sohail (09 Feb 2018) -- Start
            'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
            'txtInstallmentAmt.Text = Format(objProcesspendingloan._InstallmentAmount, GUI.fmtCurrency)
            RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            txtInstallmentAmt.Text = Format(objProcesspendingloan._InstallmentAmount, GUI.fmtCurrency)
            AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            Call nudDuration_ValueChanged(nudDuration, New System.EventArgs)
            'Sohail (09 Feb 2018) -- End
            'Varsha (11 Jan 2018) -- End
            'txtEMIInstallments.Decimal = objProcesspendingloan._NoOfInstallment
            'S.SANDEEP [20-SEP-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Public Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objMasterData As New clsMasterData
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        'Dim objPeriod As New clscommom_period_Tran 'Sohail (16 May 2018)
        'S.SANDEEP [20-SEP-2017] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 50
        Dim objLoan_Advance As New clsLoan_Advance
        'S.SANDEEP [20-SEP-2017] -- END
        Try
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If menAction = enAction.EDIT_ONE Then
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If

            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, _
                                                   ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                   "Employee", True)
          
            'Nilay (10-Oct-2015) -- End

            With cboEmpName
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Employee")
                .SelectedValue = 0
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboEmpName)
            'Nilay (27-Oct-2016) -- End

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsCombos = objLoanScheme.getComboList(True, "LoanScheme")
            dsCombos = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End
            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("LoanScheme")
                .SelectedValue = 0
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboLoanScheme)
            'Nilay (27-Oct-2016) -- End

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            'Sohail (16 May 2018) -- Start
            'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, _
            '                                     FinancialYear._Object._YearUnkid, _
            '                                     FinancialYear._Object._DatabaseName, _
            '                                     FinancialYear._Object._Database_Start_Date, _
            '                                     "List", True, enStatusType.Open)
            ''Nilay (10-Oct-2015) -- End


            'With cboDeductionPeriod
            '    .ValueMember = "periodunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("List")
            '    .SelectedValue = 0
            'End With
            'Sohail (16 May 2018) -- End

            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency", True)
            cboCurrency.ValueMember = "countryunkid"
            cboCurrency.DisplayMember = "currency_sign"
            cboCurrency.DataSource = dsCombos.Tables("Currency")


            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            dsCombos = objLoan_Advance.GetLoanCalculationTypeList("List", True)
            With cboLoanCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dsCombos = objLoan_Advance.GetLoan_Interest_Calculation_Type("List", True)
            With cboInterestCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [20-SEP-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try

    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(cboEmpName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmpName.Focus()
                Return False
            End If

            If radLoan.Checked Then
                If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), enMsgBoxStyle.Information)
                    cboLoanScheme.Focus()
                    Return False
                End If

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 57
                Dim objLA As New clsLoan_Advance
                If objLA.GetExistLoan(CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), True) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you can not apply loan for the selected scheme: Reason, The selected loan scheme is already assigned to you and having (In-progress or On hold) status. Instead of applying new loan, You can add top-up amount from Loan Advance List-> Operation -> Other Loan Operations."), enMsgBoxStyle.Information)
                    objLA = Nothing
                    Return False
                End If
                objLA = Nothing
                'S.SANDEEP [20-SEP-2017] -- END

            End If

            If txtLoanAmt.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Loan/Advance Amount cannot be blank. Loan/Advance Amount is compulsory information."), enMsgBoxStyle.Information)
                txtLoanAmt.Focus()
                Return False
            End If

            If CInt(cboCurrency.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Currency is compulsory information.Please select currency."), enMsgBoxStyle.Information)
                cboCurrency.Select()
                Return False
            End If

            If ConfigParameter._Object._LoanApplicationNoType = 0 Then
                If txtApplicationNo.Text = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Application No cannot be blank. Application No is compulsory information."), enMsgBoxStyle.Information)
                    txtApplicationNo.Focus()
                    Return False
                End If
            End If

            If chkExternalEntity.CheckState = CheckState.Checked Then
                If txtExternalEntity.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "External Entity cannot be blank. External Entity is compulsory information."), enMsgBoxStyle.Information)
                    txtExternalEntity.Focus()
                    Return False
                End If
            End If

            If dtpApplicationDate.Value.Date > FinancialYear._Object._Database_End_Date Or dtpApplicationDate.Value.Date < FinancialYear._Object._Database_Start_Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Application date should be between current financial year."), enMsgBoxStyle.Information)
                dtpApplicationDate.Focus()
                Return False
            End If

            If CInt(cboDeductionPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Deduction Period is compulsory information.Please select deduction period."), enMsgBoxStyle.Information)
                cboDeductionPeriod.Focus()
                Return False
            End If

            If radAdvance.Checked = False Then
                'Nilay (21-Oct-2015) -- Start
                'ENHANCEMENT : NEW LOAN Given By Rutta
                'If nudDurationInMths.Value <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Duration in Months cannot be 0.Please define duration in months greater than 0."), enMsgBoxStyle.Information)
                '    nudDurationInMths.Focus()
                '    Return False
                'End If
                'Nilay (21-Oct-2015) -- End

                If txtInstallmentAmt.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Installment Amount cannot be 0.Please define installment amount greater than 0."), enMsgBoxStyle.Information)
                    txtInstallmentAmt.Focus()
                    Return False
                End If

                'Sohail (09 Feb 2018) -- Start
                'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
                If CInt(cboLoanCalcType.SelectedValue) = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI) Then
                    If txtPrincipalAmt.Decimal <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Principal Amount cannot be 0. Please enter Principal amount."), enMsgBoxStyle.Information)
                        txtPrincipalAmt.Focus()
                        Return False
                    End If
                End If
                'Sohail (09 Feb 2018) -- End

                'Nilay (15-Dec-2015) -- Start
                'Hemant (28 Aug 2018) -- Start
                'Enhancement issue # 0002478 : System not allowing to have deduction amount > principal amount for only one Installment
                'If txtInstallmentAmt.Decimal > txtLoanAmt.Decimal Then
                If txtInstallmentAmt.Decimal > txtLoanAmt.Decimal AndAlso nudDuration.Value > 1 Then
                    'Hemant (28 Aug 2018) -- Enhancement
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Installment Amount cannot be greater than Loan Amount."), enMsgBoxStyle.Information)
                    txtInstallmentAmt.Focus()
                    Exit Function
                End If
                'Nilay (15-Dec-2015) -- End

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                'If txtEMIInstallments.Decimal <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "No of Installment cannot be 0.Please define No of Installment greater than 0."), enMsgBoxStyle.Information)
                '    txtInstallmentAmt1.Focus()
                '    Return False
                'End If
                'S.SANDEEP [20-SEP-2017] -- END

                'Sandeep [08 September 2015] -- Start
                'ENHANCEMENT : rutta's comment remmoved checkpoint for decimal in installment.
                'If Format(txtLoanAmt.Decimal, GUI.fmtCurrency) <> Format((txtEMIInstallments.Decimal * CDec(txtInstallmentAmt.Tag)), GUI.fmtCurrency) Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "You are changing loan amount.We recommanded you that you have to change No of installments or Installment amount."), enMsgBoxStyle.Information)
                '    txtEMIInstallments.Focus()
                '    Return False
                'End If
                'Sandeep [08 September 2015] -- End


                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 52
                If mdecMaxLoanAmountDefined > 0 Then
                    If txtLoanAmt.Decimal > mdecMaxLoanAmountDefined Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you can not apply for this loan scheme: Reason, amount applied is exceeding the max amount") & " [" & Format(mdecMaxLoanAmountDefined, GUI.fmtCurrency) & "] " & Language.getMessage(mstrModuleName, 20, " set for selected scheme."), enMsgBoxStyle.Information)
                        txtLoanAmt.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP [20-SEP-2017] -- END

                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                If mdecEMI_NetPayPercentage > 0 Then
                    'Sohail (11 Apr 2018) -- Start
                    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                    'Dim objTnALeave As New clsTnALeaveTran
                    'Dim ds As DataSet = objTnALeave.GetPreviousNetPay(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, True, mdtPeriodStart.AddDays(-1), "List", If(CInt(cboEmpName.SelectedValue) > 0, cboEmpName.SelectedValue.ToString(), ""), "")
                    Dim ds As DataSet = Nothing
                    'Sohail (06 Jul 2018) -- Start
                    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                    Dim dsMapped As DataSet = Nothing
                    'Sohail (06 Jul 2018) -- End
                    'If mintTranheadUnkid <= 0 Then 'Sohail (06 Jul 2018)
                    Dim objTnALeave As New clsTnALeaveTran
                        ds = objTnALeave.GetPreviousNetPay(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, True, mdtPeriodStart.AddDays(-1), "List", If(CInt(cboEmpName.SelectedValue) > 0, cboEmpName.SelectedValue.ToString(), ""), "")
                    'Else 'Sohail (06 Jul 2018)
                    If mintTranheadUnkid > 0 Then 'Sohail (06 Jul 2018)
                        Dim objMaster As New clsMasterData
                        Dim intPrevPeriod As Integer = objMaster.getCurrentPeriodID(CInt(enModuleReference.Payroll), mdtPeriodStart.AddDays(-1), 0, 0, True, False, Nothing, False)
                        Dim strFilter As String = ""
                        Dim dtStart As Date = mdtPeriodStart
                        Dim dtEnd As Date = mdtPeriodEnd
                        Dim strDBName As String = FinancialYear._Object._DatabaseName
                        Dim intYearId As Integer = FinancialYear._Object._YearUnkid
                        If intPrevPeriod <= 0 Then
                            strFilter = " 1 = 2 "
                        Else
                            Dim objPeriod As New clscommom_period_Tran
                            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriod
                            dtStart = objPeriod._Start_Date
                            dtEnd = objPeriod._End_Date
                            If intYearId <> objPeriod._Yearunkid Then
                                intYearId = objPeriod._Yearunkid
                                strDBName = objPeriod.GetDatabaseName(objPeriod._Yearunkid, Company._Object._Companyunkid)
                            End If
                            strFilter = " prpayrollprocess_tran.tranheadunkid = " & mintTranheadUnkid & " "
                        End If
                        Dim objProcess As New clsPayrollProcessTran
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        'ds = objProcess.GetList(strDBName, User._Object._Userunkid, intYearId, Company._Object._Companyunkid, dtStart, dtEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", CInt(cboEmpName.SelectedValue), intPrevPeriod, , , strFilter)
                        dsMapped = objProcess.GetList(strDBName, User._Object._Userunkid, intYearId, Company._Object._Companyunkid, dtStart, dtEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", CInt(cboEmpName.SelectedValue), intPrevPeriod, , , strFilter)
                        'Sohail (06 Jul 2018) -- End
                    End If
                    'Sohail (11 Apr 2018) -- End
                    If ds.Tables(0).Rows.Count <= 0 Then
                        'Sohail (11 Apr 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, you can not apply for this loan scheme: Reason, There is no previous salary history for this employee."), enMsgBoxStyle.Information)
                        'If mintTranheadUnkid <= 0 Then 'Sohail (06 Jul 2018)
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, you can not apply for this loan scheme: Reason, There is no previous salary history for this employee."), enMsgBoxStyle.Information)
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        'Else
                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, you can not apply for this loan scheme: Reason, Either There is no previous salary history for this employee for the net pay head set on loan scheme master screen."), enMsgBoxStyle.Information)
                        'End If
                        'Sohail (06 Jul 2018) -- End
                        'Sohail (11 Apr 2018) -- End
                        txtLoanAmt.Focus()
                        Return False
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                    ElseIf mintTranheadUnkid > 0 AndAlso dsMapped.Tables(0).Rows.Count <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, you can not apply for this loan scheme: Reason, Either There is no previous salary history for this employee for the net pay head set on loan scheme master screen."), enMsgBoxStyle.Information)
                        txtLoanAmt.Focus()
                        Return False
                        'Sohail (06 Jul 2018) -- End
                    Else
                        Dim decAmt As Decimal = txtInstallmentAmt.Decimal
                        If mdecBaseExRate <> 0 Then
                            decAmt = decAmt * mdecBaseExRate / mdecPaidExRate
                        Else
                            decAmt = decAmt * mdecPaidExRate
                        End If
                        'Sohail (11 Apr 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                        'If decAmt > ((CDec(ds.Tables(0).Rows(0)("total_amount")) * mdecEMI_NetPayPercentage) / 100) Then
                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the") & " [" & Format(mdecEMI_NetPayPercentage, "00.00") & "%] " & Language.getMessage(mstrModuleName, 24, "of pervious net pay.") & " " & Format(CDec(ds.Tables(0).Rows(0)("total_amount")), GUI.fmtCurrency), enMsgBoxStyle.Information)
                        Dim decPrevNetPay As Decimal = 0
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        Dim decPrevActualNetPay As Decimal = 0
                        Dim decOtherLoanEMI As Decimal = 0
                        'Sohail (06 Jul 2018) -- End
                        If mintTranheadUnkid <= 0 Then
                            decPrevNetPay = CDec(ds.Tables(0).Rows(0)("total_amount"))
                        Else
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                            'decPrevNetPay = CDec(ds.Tables(0).Rows(0)("amountPaidCurrency"))
                            decPrevNetPay = CDec(dsMapped.Tables(0).Rows(0)("amountPaidCurrency"))
                            'Sohail (06 Jul 2018) -- End
                        End If
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        'Dim decCheckPoint As Decimal = decPrevNetPay - decAmt
                        decPrevActualNetPay = CDec(ds.Tables(0).Rows(0)("total_amount"))

                        Dim objLoanAdvance As New clsLoan_Advance
                        Dim dsLoanCalc As DataSet = objLoanAdvance.Calculate_LoanBalanceInfo(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                  mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, _
                                                  "Loan", mdtPeriodEnd.AddDays(1), cboEmpName.SelectedValue.ToString(), 0, _
                                                  mdtPeriodStart, True, , , True, True, , True, True, , , , 0, , , , , Nothing)
                        Dim dtTable As DataTable = New DataView(dsLoanCalc.Tables(0), "statusunkid IN (" & CInt(enLoanStatus.IN_PROGRESS) & ", " & CInt(enLoanStatus.ON_HOLD) & ") AND transactionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " ", "", DataViewRowState.CurrentRows).ToTable
                        For Each dtRow As DataRow In dtTable.Rows

                            If CInt(dtRow.Item("statusunkid").ToString) = CInt(enLoanStatus.ON_HOLD) AndAlso CBool(dtRow.Item("iscalculateinterest").ToString) = False Then
                                decOtherLoanEMI += 0
                            ElseIf CInt(dtRow.Item("statusunkid").ToString) = CInt(enLoanStatus.ON_HOLD) AndAlso CBool(dtRow.Item("iscalculateinterest").ToString) = True Then
                                decOtherLoanEMI += CDec(dtRow.Item("TotalMonthlyInterestPaidCurrency").ToString) + CDec(dtRow.Item("TotInterestAmountPaidCurrency").ToString)
                            Else
                                decOtherLoanEMI += CDec(dtRow.Item("TotalMonthlyDeductionPaidCurrency").ToString) + CDec(dtRow.Item("TotPMTAmountPaidCurrency").ToString)
                            End If
                        Next
                        Dim dsLoan As DataSet = objProcesspendingloan.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "Loan", 0, "lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmpName.SelectedValue) & " AND lnloan_process_pending_loan.deductionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " AND lnloan_process_pending_loan.loan_statusunkid IN ( " & CInt(enLoanApplicationStatus.PENDING) & ", " & CInt(enLoanApplicationStatus.APPROVED) & " ) ")
                        For Each dtRow As DataRow In dsLoan.Tables(0).Rows
                            If CBool(dtRow.Item("isloan")) = True Then
                                Dim dtEnd_Date As Date = DateAdd(DateInterval.Month, CInt(dtRow.Item("noofinstallment")), mdtPeriodStart).AddDays(-1)
                                Dim decIntAmt As Decimal
                                Dim decEMI As Decimal
                                Dim decTotIntAmt As Decimal
                                Dim decTotEMI As Decimal
                                objLoanAdvance.Calulate_Projected_Loan_Balance(CDec(dtRow.Item("Amount")), CInt(DateDiff(DateInterval.Day, mdtPeriodStart, dtEnd_Date.Date)), CDec(dtRow.Item("interest_rate")), CType(dtRow.Item("loancalctype_id"), enLoanCalcId), CType(dtRow.Item("interest_calctype_id"), enLoanInterestCalcType), CInt(dtRow.Item("noofinstallment")), CInt(DateDiff(DateInterval.Day, mdtPeriodStart.Date, mdtPeriodEnd.Date.AddDays(1))), CDec(dtRow.Item("installmentamt")), decIntAmt, decEMI, decTotIntAmt, decTotEMI)
                                decOtherLoanEMI += decEMI
                            Else
                                decOtherLoanEMI += CDec(dtRow.Item("amount"))
                            End If
                            
                        Next
                        'Sohail (19 Mar 2020) -- Start
                        'IHI Issue # : Check point explained by Rutta by giving example in xls file attached in Ref # 220 : Tracking ID	: ARUTI-99 on PACRA request was not working as per the percentage set for "Advance amount should not exceed of previous Basic Salary" option as it is allowing to apply loan / advance when less % is set and it is not allowing when high % is set on configuration.
                        'Dim decCheckPoint As Decimal = decPrevActualNetPay - decAmt - decOtherLoanEMI
                        Dim decCheckPoint As Decimal = decAmt + decOtherLoanEMI
                        'Sohail (19 Mar 2020) -- End
                        'Sohail (06 Jul 2018) -- End
                        'Sohail (19 Mar 2020) -- Start
                        'IHI Issue # : Check point explained by Rutta by giving example in xls file attached in Ref # 220 : Tracking ID	: ARUTI-99 on PACRA request was not working as per the percentage set for "Advance amount should not exceed of previous Basic Salary" option as it is allowing to apply loan / advance when less % is set and it is not allowing when high % is set on configuration.
                        'If decPrevNetPay <= 0 OrElse ((decPrevNetPay * mdecEMI_NetPayPercentage) / 100) > decCheckPoint Then
                        If decPrevNetPay <= 0 OrElse ((decPrevNetPay * mdecEMI_NetPayPercentage) / 100) < decCheckPoint Then
                            'Sohail (19 Mar 2020) -- End
                            If mintTranheadUnkid <= 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the") & " [" & Format(mdecEMI_NetPayPercentage, "00.00") & "%] " & Language.getMessage(mstrModuleName, 24, "of previous net pay.") & " " & Format(decPrevNetPay, GUI.fmtCurrency), enMsgBoxStyle.Information)
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the") & " [" & Format(mdecEMI_NetPayPercentage, "00.00") & "%] " & Language.getMessage(mstrModuleName, 31, "of previous net pay head") & " " & Format(decPrevNetPay, GUI.fmtCurrency), enMsgBoxStyle.Information)
                            End If
                            'Sohail (11 Apr 2018) -- End
                            txtLoanAmt.Focus()
                            Return False
                        End If
                    End If
                End If
                'Sohail (22 Sep 2017) -- End

                'Sohail (17 Dec 2019) -- Start
                'Mukuba University # 0004115: Option for EOC to affect number of installments on loans.
                If CInt(nudDuration.Value) > 1 Then
                    Dim dtLoanEnd As Date = mdtPeriodStart.AddMonths(CInt(nudDuration.Value)).AddDays(-1)
                    Dim objEmpDates As New clsemployee_dates_tran
                    Dim ds As DataSet = objEmpDates.GetEmployeeEndDates(dtLoanEnd, cboEmpName.SelectedValue.ToString)
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, mdtPeriodStart, eZeeDate.convertDate(ds.Tables(0).Rows(0).Item("finalenddate").ToString).AddDays(1)))
                        If CInt(nudDuration.Value) > intEmpTenure Then
                            'Hemant (24 Nov 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-495 - Add an option that will allow users to import/add loan without considering the employee tenure(EOC).
                            If ConfigParameter._Object._SkipEOCValidationOnLoanTenure = False Then
                                'Hemant (24 Nov 2021) -- End
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 41, "Sorry, Loan tenure should not be greater than employee tenure month") & " [" & intEmpTenure & "].", enMsgBoxStyle.Information)
                            If nudDuration.Enabled = True Then nudDuration.Focus()
                            Return False
                            End If 'Hemant (24 Nov 2021)
                        End If
                    End If
                End If
                'Sohail (17 Dec 2019) -- End

                'Sohail (16 May 2018) -- Start
                'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
            Else
                If ConfigParameter._Object._Advance_NetPayPercentage > 0 Then
                    Dim ds As DataSet = Nothing
                    'Sohail (06 Jul 2018) -- Start
                    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                    Dim dsMapped As DataSet = Nothing
                    'Sohail (06 Jul 2018) -- End
                    'If ConfigParameter._Object._Advance_NetPayTranheadUnkid <= 0 Then 'Sohail (06 Jul 2018)
                        Dim objTnALeave As New clsTnALeaveTran
                        ds = objTnALeave.GetPreviousNetPay(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, True, mdtPeriodStart.AddDays(-1), "List", If(CInt(cboEmpName.SelectedValue) > 0, cboEmpName.SelectedValue.ToString(), ""), "")
                    'Else 'Sohail (06 Jul 2018)
                    If ConfigParameter._Object._Advance_NetPayTranheadUnkid > 0 Then 'Sohail (06 Jul 2018)
                        Dim objMaster As New clsMasterData
                        Dim intPrevPeriod As Integer = objMaster.getCurrentPeriodID(CInt(enModuleReference.Payroll), mdtPeriodStart.AddDays(-1), 0, 0, True, False, Nothing, False)
                        Dim strFilter As String = ""
                        Dim dtStart As Date = mdtPeriodStart
                        Dim dtEnd As Date = mdtPeriodEnd
                        Dim strDBName As String = FinancialYear._Object._DatabaseName
                        Dim intYearId As Integer = FinancialYear._Object._YearUnkid
                        If intPrevPeriod <= 0 Then
                            strFilter = " 1 = 2 "
                        Else
                            Dim objPeriod As New clscommom_period_Tran
                            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriod
                            dtStart = objPeriod._Start_Date
                            dtEnd = objPeriod._End_Date
                            If intYearId <> objPeriod._Yearunkid Then
                                intYearId = objPeriod._Yearunkid
                                strDBName = objPeriod.GetDatabaseName(objPeriod._Yearunkid, Company._Object._Companyunkid)
                            End If
                            strFilter = " prpayrollprocess_tran.tranheadunkid = " & mintTranheadUnkid & " "
                        End If
                        Dim objProcess As New clsPayrollProcessTran
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        'ds = objProcess.GetList(strDBName, User._Object._Userunkid, intYearId, Company._Object._Companyunkid, dtStart, dtEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", CInt(cboEmpName.SelectedValue), intPrevPeriod, , , strFilter)
                        dsMapped = objProcess.GetList(strDBName, User._Object._Userunkid, intYearId, Company._Object._Companyunkid, dtStart, dtEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", CInt(cboEmpName.SelectedValue), intPrevPeriod, , , strFilter)
                        'Sohail (06 Jul 2018) -- End
                    End If

                    If ds.Tables(0).Rows.Count <= 0 Then
                        'If ConfigParameter._Object._Advance_NetPayTranheadUnkid <= 0 Then 'Sohail (06 Jul 2018)
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, you can not apply for this Advance: Reason, There is no previous salary history for this employee."), enMsgBoxStyle.Information)
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        'Else
                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Sorry, you can not apply for this Advance: Reason, Either There is no previous salary history for this employee for the net pay head set on Configuration -> Option screen."), enMsgBoxStyle.Information)
                        'End If
                        'Sohail (06 Jul 2018) -- End
                        txtLoanAmt.Focus()
                        Return False
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                    ElseIf ConfigParameter._Object._Advance_NetPayTranheadUnkid > 0 AndAlso dsMapped.Tables(0).Rows.Count <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Sorry, you can not apply for this Advance: Reason, Either There is no previous salary history for this employee for the net pay head set on Configuration -> Option screen."), enMsgBoxStyle.Information)
                        txtLoanAmt.Focus()
                        Return False
                        'Sohail (06 Jul 2018) -- End
                    Else
                        Dim decAmt As Decimal = txtLoanAmt.Decimal
                        If mdecBaseExRate <> 0 Then
                            decAmt = decAmt * mdecBaseExRate / mdecPaidExRate
                        Else
                            decAmt = decAmt * mdecPaidExRate
                        End If

                        Dim decPrevNetPay As Decimal = 0
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        Dim decPrevActualNetPay As Decimal = 0
                        Dim decOtherLoanEMI As Decimal = 0
                        'Sohail (06 Jul 2018) -- End
                        If ConfigParameter._Object._Advance_NetPayTranheadUnkid <= 0 Then
                            'Gajanan (23-May-2018) -- Start
                            'CCK Enhancement - Ref # 180 - On Advances Application, provide configuration where we can be able to prevent Advance application after configured days (set on configuration) in each period.
                            'decPrevNetPay = CDec(ds.Tables(0).Rows(0)("total_amount"))
                            decPrevNetPay = CDec(ds.Tables(0).Rows(0)("basicsalary"))
                            'Gajanan (23-May-2018) -- End

                        Else
                            'Sohail (06 Jul 2018) -- Start
                            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                            'decPrevNetPay = CDec(ds.Tables(0).Rows(0)("amountPaidCurrency"))
                            decPrevNetPay = CDec(dsMapped.Tables(0).Rows(0)("amountPaidCurrency"))
                            'Sohail (06 Jul 2018) -- End
                        End If
                        'Sohail (06 Jul 2018) -- Start
                        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved (Pick Actual Net for Loan Check point and deduct current month other loans EMI from check point as well) in 72.1.
                        'Dim decCheckPoint As Decimal = decPrevNetPay - decAmt
                        decPrevActualNetPay = CDec(ds.Tables(0).Rows(0)("basicsalary"))

                        Dim objLoanAdvance As New clsLoan_Advance
                        Dim dsLoanCalc As DataSet = objLoanAdvance.Calculate_LoanBalanceInfo(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                  mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, _
                                                  "Loan", mdtPeriodEnd.AddDays(1), cboEmpName.SelectedValue.ToString(), 0, _
                                                  mdtPeriodStart, True, , , True, True, , True, True, , , , 0, , , , , Nothing)
                        Dim dtTable As DataTable = New DataView(dsLoanCalc.Tables(0), "statusunkid IN (" & CInt(enLoanStatus.IN_PROGRESS) & ", " & CInt(enLoanStatus.ON_HOLD) & ") AND transactionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " ", "", DataViewRowState.CurrentRows).ToTable
                        For Each dtRow As DataRow In dtTable.Rows

                            If CInt(dtRow.Item("statusunkid").ToString) = CInt(enLoanStatus.ON_HOLD) AndAlso CBool(dtRow.Item("iscalculateinterest").ToString) = False Then
                                decOtherLoanEMI += 0
                            ElseIf CInt(dtRow.Item("statusunkid").ToString) = CInt(enLoanStatus.ON_HOLD) AndAlso CBool(dtRow.Item("iscalculateinterest").ToString) = True Then
                                decOtherLoanEMI += CDec(dtRow.Item("TotalMonthlyInterestPaidCurrency").ToString) + CDec(dtRow.Item("TotInterestAmountPaidCurrency").ToString)
                            Else
                                decOtherLoanEMI += CDec(dtRow.Item("TotalMonthlyDeductionPaidCurrency").ToString) + CDec(dtRow.Item("TotPMTAmountPaidCurrency").ToString)
                            End If
                        Next
                        Dim dsLoan As DataSet = objProcesspendingloan.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "Loan", 0, "lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmpName.SelectedValue) & " AND lnloan_process_pending_loan.deductionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " AND lnloan_process_pending_loan.loan_statusunkid IN ( " & CInt(enLoanApplicationStatus.PENDING) & ", " & CInt(enLoanApplicationStatus.APPROVED) & " ) ")
                        For Each dtRow As DataRow In dsLoan.Tables(0).Rows
                            If CBool(dtRow.Item("isloan")) = True Then
                                Dim dtEnd_Date As Date = DateAdd(DateInterval.Month, CInt(dtRow.Item("noofinstallment")), mdtPeriodStart).AddDays(-1)
                                Dim decIntAmt As Decimal
                                Dim decEMI As Decimal
                                Dim decTotIntAmt As Decimal
                                Dim decTotEMI As Decimal
                                objLoanAdvance.Calulate_Projected_Loan_Balance(CDec(dtRow.Item("Amount")), CInt(DateDiff(DateInterval.Day, mdtPeriodStart, dtEnd_Date.Date)), CDec(dtRow.Item("interest_rate")), CType(dtRow.Item("loancalctype_id"), enLoanCalcId), CType(dtRow.Item("interest_calctype_id"), enLoanInterestCalcType), CInt(dtRow.Item("noofinstallment")), CInt(DateDiff(DateInterval.Day, mdtPeriodStart.Date, mdtPeriodEnd.Date.AddDays(1))), CDec(dtRow.Item("installmentamt")), decIntAmt, decEMI, decTotIntAmt, decTotEMI)
                                decOtherLoanEMI += decEMI
                            Else
                                decOtherLoanEMI += CDec(dtRow.Item("amount"))
                        End If

                        Next
                        'Sohail (19 Mar 2020) -- Start
                        'IHI Issue # : Check point explained by Rutta by giving example in xls file attached in Ref # 220 : Tracking ID	: ARUTI-99 on PACRA request was not working as per the percentage set for "Advance amount should not exceed of previous Basic Salary" option as it is allowing to apply loan / advance when less % is set and it is not allowing when high % is set on configuration.
                        'Dim decCheckPoint As Decimal = decPrevActualNetPay - decAmt - decOtherLoanEMI
                        Dim decCheckPoint As Decimal = decAmt + decOtherLoanEMI
                        'Sohail (19 Mar 2020) -- End
                        'Sohail (06 Jul 2018) -- End
                        'Sohail (19 Mar 2020) -- Start
                        'IHI Issue # : Check point explained by Rutta by giving example in xls file attached in Ref # 220 : Tracking ID	: ARUTI-99 on PACRA request was not working as per the percentage set for "Advance amount should not exceed of previous Basic Salary" option as it is allowing to apply loan / advance when less % is set and it is not allowing when high % is set on configuration.
                        'If decPrevNetPay <= 0 OrElse ((decPrevNetPay * ConfigParameter._Object._Advance_NetPayPercentage) / 100) > decCheckPoint Then
                        If decPrevNetPay <= 0 OrElse ((decPrevNetPay * ConfigParameter._Object._Advance_NetPayPercentage) / 100) < decCheckPoint Then
                            'Sohail (19 Mar 2020) -- End
                            If ConfigParameter._Object._Advance_NetPayTranheadUnkid <= 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Sorry, you can not apply for this Advance: Reason, Advance amount applied is exceeding the") & " [" & Format(ConfigParameter._Object._Advance_NetPayPercentage, "00.00") & "%] " & Language.getMessage(mstrModuleName, 24, "of previous net pay.") & " " & Format(decPrevNetPay, GUI.fmtCurrency), enMsgBoxStyle.Information)
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, "Sorry, you can not apply for this Advance: Reason, Advance amount applied is exceeding the") & " [" & Format(ConfigParameter._Object._Advance_NetPayPercentage, "00.00") & "%] " & Language.getMessage(mstrModuleName, 31, "of previous net pay head") & " " & Format(decPrevNetPay, GUI.fmtCurrency), enMsgBoxStyle.Information)
                            End If
                            txtLoanAmt.Focus()
                            Return False
                        End If
                    End If

                    Dim objLA As New clsLoan_Advance
                    If objLA.GetExistLoan(0, CInt(cboEmpName.SelectedValue), False) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Sorry, you can not apply for Advance: Reason, The Advance is already assigned to you and having (In-progress or On hold) status."), enMsgBoxStyle.Information)
                        objLA = Nothing
                        Return False
                    End If
                    objLA = Nothing
                End If
                'Sohail (16 May 2018) -- End


                'Gajanan (23-May-2018) -- Start
                'CCK Enhancement - Ref # 180 - On Advances Application, provide configuration where we can be able to prevent Advance application after configured days (set on configuration) in each period.
                If ConfigParameter._Object._Advance_DontAllowAfterDays > 0 Then
                    If mdtPeriodStart.AddDays(ConfigParameter._Object._Advance_DontAllowAfterDays) <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "Sorry, you can not apply for Advance: Reason, Days to Advance Has Been Exceeded."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If
                'Gajanan (23-May-2018) -- End
            End If

            Dim objLoanApprover As New clsLoanApprover_master
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dtList As DataTable = objLoanApprover.GetEmployeeApprover(CInt(cboEmpName.SelectedValue))
            Dim dtList As DataTable = objLoanApprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, _
                                                                          User._Object._Userunkid, _
                                                                          FinancialYear._Object._YearUnkid, _
                                                                          Company._Object._Companyunkid, _
                                                                          CInt(cboEmpName.SelectedValue), _
                                                                          ConfigParameter._Object._IsLoanApprover_ForLoanScheme)
            'Nilay (10-Oct-2015) -- End

            If dtList.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please Assign Approver(s) to this employee."), enMsgBoxStyle.Information)
                Return False
            End If

            If ConfigParameter._Object._IsLoanApprover_ForLoanScheme Then
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dtList = objLoanApprover.GetEmployeeApprover(CInt(cboEmpName.SelectedValue), _
                '                                            ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                '                                            CInt(cboLoanScheme.SelectedValue))
                dtList = objLoanApprover.GetEmployeeApprover(FinancialYear._Object._DatabaseName, _
                                                             User._Object._Userunkid, _
                                                             FinancialYear._Object._YearUnkid, _
                                                             Company._Object._Companyunkid, _
                                                             CInt(cboEmpName.SelectedValue), _
                                                             ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                             CInt(cboLoanScheme.SelectedValue))
                'Nilay (10-Oct-2015) -- End


                If dtList.Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please Map this Loan scheme(s) to this employee's Approver(s)."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If


            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            Dim objLoanScheme As New clsLoan_Scheme
            objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(nudDuration.Value) > objLoanScheme._MaxNoOfInstallment Then
                'Sohail (11 Apr 2018) -- Start
                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Installment months cannot be greater than " & objLoanScheme._MaxNoOfInstallment & " for " & objLoanScheme._Name & " Scheme."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Installment months cannot be greater than") & " " & objLoanScheme._MaxNoOfInstallment & Language.getMessage(mstrModuleName, 33, "for") & " " & objLoanScheme._Name & Language.getMessage(mstrModuleName, 34, "Scheme."), enMsgBoxStyle.Information)
                'Sohail (11 Apr 2018) -- End
                nudDuration.Focus()
                Exit Function
            End If
            'Varsha (25 Nov 2017) -- End


            dtList.Rows.Clear()
            dtList = Nothing
            objLoanApprover = Nothing

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function

    Private Sub SetVisibility()
        Try
            If ConfigParameter._Object._LoanApplicationNoType = 1 Then
                txtApplicationNo.Enabled = False
                txtApplicationNo.Text = ConfigParameter._Object._LoanApplicationPrifix & ConfigParameter._Object._NextLoanApplicationNo
            Else
                txtApplicationNo.Enabled = True
            End If
            If mintProcessPendingLoanUnkid = -1 Then
            Else
                objbtnAddLoanScheme.Enabled = User._Object.Privilege._AddLoanScheme
            End If

            If radLoan.Checked = True Then
            ElseIf radAdvance.Checked = True Then
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Function IsAmount_Changed(ByVal decNewAmount As Decimal) As Boolean
        Try
            If mdecOldAmount <> decNewAmount Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception

        End Try
    End Function

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 18, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Nilay (27-Oct-2016) -- End

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
    Private Sub Calculate_Projected_Loan_Balance(ByVal eLnCalcType As enLoanCalcId _
                                                 , ByVal eIntRateCalcTypeID As enLoanInterestCalcType _
                                                 , Optional ByVal blnIsDurationChange As Boolean = False _
                                                 )
        Try
            Dim objLoan_Advance As New clsLoan_Advance
            Dim decInstallmentAmount As Decimal = 0
            Dim decInstrAmount As Decimal = 0
            Dim decTotInstallmentAmount As Decimal = 0
            Dim decTotIntrstAmount As Decimal = 0
            Dim dtEndDate As Date = DateAdd(DateInterval.Month, nudDuration.Value, Now.Date)
            Dim dtFPeriodStart As Date = dtEndDate
            Dim dtFPeriodEnd As Date = dtEndDate
            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboDeductionPeriod.SelectedValue)
                dtFPeriodStart = objPeriod._Start_Date
                dtFPeriodEnd = objPeriod._End_Date
            End If
            objLoan_Advance.Calulate_Projected_Loan_Balance(txtLoanAmt.Decimal _
                                                            , CInt(DateDiff(DateInterval.Day, Now.Date, dtEndDate.Date)) _
                                                            , txtLoanRate.Decimal _
                                                            , eLnCalcType _
                                                            , eIntRateCalcTypeID _
                                                            , CInt(nudDuration.Value) _
                                                            , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
                                                            , Convert.ToDecimal(IIf(eLnCalcType = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, txtPrincipalAmt.Decimal, txtInstallmentAmt.Decimal)) _
                                                            , decInstrAmount _
                                                            , decInstallmentAmount _
                                                            , decTotIntrstAmount _
                                                            , decTotInstallmentAmount _
                                                            )
            txtInterestAmt.Tag = decTotIntrstAmount
            txtInterestAmt.Decimal = CDec(Format(decTotIntrstAmount, GUI.fmtCurrency))
            RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            txtInstallmentAmt.Tag = decInstallmentAmount
            txtInstallmentAmt.Decimal = CDec(Format(decInstallmentAmount, GUI.fmtCurrency))
            AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            txtPrincipalAmt.Text = Format(decInstallmentAmount - decInstrAmount, GUI.fmtCurrency)
            AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            txtIntAmt.Text = Format(decInstrAmount, GUI.fmtCurrency)
            txtNetAmount.Tag = decTotInstallmentAmount + decTotIntrstAmount
            txtNetAmount.Decimal = CDec(Format(decTotInstallmentAmount + decTotIntrstAmount, GUI.fmtCurrency))
            objLoan_Advance = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Calculate_Projected_Loan_Balance", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [20-SEP-2017] -- END

    'Sohail (16 May 2018) -- Start
    'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
    Private Sub FillPeriod(ByVal blnOnlyFirstPeriod As Boolean)
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombos As DataSet
        Dim mdtTable As DataTable
        Try


            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 FinancialYear._Object._DatabaseName, _
                                                 FinancialYear._Object._Database_Start_Date, _
                                                 "List", True, enStatusType.Open)

            If blnOnlyFirstPeriod = True Then
                'Sohail (19 Jun 2020) -- Start
                'Ifakara enhancement : 0004734 : Show next open period on salary advance dropdown when system date is of next period.
                'Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
                'If intFirstPeriodId > 0 Then
                '    mdtTable = New DataView(dsCombos.Tables("List"), "periodunkid IN (0, " & intFirstPeriodId & " )", "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    mdtTable = New DataView(dsCombos.Tables("List"), "1=2", "", DataViewRowState.CurrentRows).ToTable
                'End If
                'Sohail (15 Jul 2020) -- Start
                'IHI Issue # : Current period not coming in period list on loan application screen.
                'mdtTable = New DataView(dsCombos.Tables("List"), "end_date <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' ", "", DataViewRowState.CurrentRows).ToTable
                mdtTable = New DataView(dsCombos.Tables("List"), "start_date <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' ", "", DataViewRowState.CurrentRows).ToTable
                'Sohail (15 Jul 2020) -- End
                'Sohail (19 Jun 2020) -- End
            Else
                mdtTable = New DataView(dsCombos.Tables("List")).ToTable
            End If


            With cboDeductionPeriod
                .DataSource = Nothing
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = mdtTable
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPeriod", mstrModuleName)
        Finally
            objMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub
    'Sohail (16 May 2018) -- End

    'Hemant (30 Aug 2019) -- Start
    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
    Private Sub Send_Notification()
        Try
            If objEmailList.Count > 0 Then
                RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                clsApplicationIdleTimer.LastIdealTime.Stop()
                Dim objSendMail As New clsSendMail
                For Each obj In objEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._FormName
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendMail._UserUnkid = User._Object._Userunkid
                    objSendMail._SenderAddress = User._Object._Email
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT

                    Try
                        objSendMail.SendMail(Company._Object._Companyunkid)
                    Catch ex As Exception

                    End Try
                Next
                objEmailList.Clear()
                AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                clsApplicationIdleTimer.LastIdealTime.Start()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If objEmailList.Count > 0 Then
                objEmailList.Clear()
            End If
        End Try
    End Sub
    'Hemant (30 Aug 2019) -- End

#End Region

#Region " Form's Events "

    Private Sub frmLoanApplication_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objProcesspendingloan = Nothing
    End Sub

    Private Sub frmLoanApplication_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.Control = True And e.KeyCode = Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmLoanApplication_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmLoanApplication_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objProcesspendingloan = New clsProcess_pending_loan

        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            Call FillCombo()
            'Sohail (16 May 2018) -- Start
            'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
            Call FillPeriod(False)
            'Sohail (16 May 2018) -- End
            Call SetVisibility()

            If menAction <> enAction.EDIT_ONE Then
                dtpApplicationDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
                If ConfigParameter._Object._CurrentDateAndTime.Date > FinancialYear._Object._Database_End_Date.Date Then
                    dtpApplicationDate.MaxDate = FinancialYear._Object._Database_End_Date.Date
                End If
            End If

            If menAction = enAction.EDIT_ONE Then
                objProcesspendingloan._Processpendingloanunkid = mintProcessPendingLoanUnkid
                'If objProcesspendingloan._Loan_Statusunkid = 2 Then
                '    radLoan.Enabled = False
                '    radAdvance.Enabled = False
                'End If
                radLoan.Enabled = False
                radAdvance.Enabled = False
                'Nilay (05-May-2016) -- Start
                cboEmpName.Enabled = False
                'Nilay (05-May-2016) -- End

                'Nilay (23-Aug-2016) -- Start
                'Enhancement - Create New Loan Notification 
                objbtnSearchEmployee.Enabled = False
                'Nilay (23-Aug-2016) -- End
            End If

            Call GetValue()
            cboEmpName.Focus()

            Call chkExternalEntity_CheckedChanged(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanApplication_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsProcess_pending_loan.SetMessages()
            objfrm._Other_ModuleNames = "clsProcess_pending_loan"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Textbox Events"

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
    'Nilay (05-May-2016) -- Start
    'Private Sub txtLoanAmt_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLoanAmt.Validated
    '    Try
    '        RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
    '        RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
    '        If txtLoanAmt.Decimal > 0 AndAlso txtInstallmentAmt.Decimal > 0 Then
    '            txtEMIInstallments.Decimal = CInt(Math.Ceiling(txtLoanAmt.Decimal / txtInstallmentAmt.Decimal))
    '        End If
    '        AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
    '        AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtLoanAmt_Validated", mstrModuleName)
    '    End Try
    'End Sub
    'Nilay (05-May-2016) -- End


    'Private Sub txtInstallmentAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
    '    Try
    '        RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
    '        If txtLoanAmt.Decimal > 0 AndAlso txtInstallmentAmt.Decimal > 0 Then
    '            'Nilay (18-Nov-2015) -- Start
    '            'txtEMIInstallments.Decimal = CInt(txtLoanAmt.Decimal / txtInstallmentAmt.Decimal)
    '            'Nilay (15-Dec-2015) -- Start
    '            'If txtInstallmentAmt.Decimal > txtLoanAmt.Decimal Then
    '            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Installment Amount cannot be greater than Loan Amount."), enMsgBoxStyle.Information)
    '            'Else
    '            'txtEMIInstallments.Decimal = CInt(txtLoanAmt.Decimal / txtInstallmentAmt.Decimal)
    '            'End If
    '            txtEMIInstallments.Decimal = CInt(Math.Ceiling(txtLoanAmt.Decimal / txtInstallmentAmt.Decimal))
    '            'Nilay (15-Dec-2015) -- End
    '            'Nilay (18-Nov-2015) -- End
    '        Else
    '            txtEMIInstallments.Decimal = 0
    '        End If
    '        txtInstallmentAmt.Tag = CDec(txtInstallmentAmt.Decimal)
    '        AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtInstallmentAmt_TextChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub txtEMIInstallments_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEMIInstallments.TextChanged
    '    Try
    '        RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
    '        If txtLoanAmt.Decimal > 0 AndAlso txtEMIInstallments.Decimal > 0 Then
    '            txtInstallmentAmt.Tag = CDec(txtLoanAmt.Decimal / txtEMIInstallments.Decimal)
    '            txtInstallmentAmt.Text = Format(CDec(txtLoanAmt.Decimal / txtEMIInstallments.Decimal), GUI.fmtCurrency)
    '        Else
    '            txtInstallmentAmt.Tag = txtLoanAmt.Decimal
    '            txtInstallmentAmt.Text = Format(txtLoanAmt.Decimal, GUI.fmtCurrency)
    '        End If
    '        AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtEMIInstallments_TextChanged", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub txtPrincipalAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPrincipalAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                'Sohail (09 Feb 2018) -- Start
                'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
                Dim decValue As Decimal = 0
                decValue = txtLoanAmt.Decimal / txtPrincipalAmt.Decimal
                RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                nudDuration.Value = CDec(IIf(decValue <= 0, 1, CDec(Math.Ceiling(decValue))))
                AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                'Sohail (09 Feb 2018) -- End
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest_With_Mapped_Head, enIntCalcType)
                'Sohail (29 Apr 2019) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtPrincipalAmt_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtInstallmentAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                'Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest, enIntCalcType)

                If txtInstallmentAmt.Decimal > 0 Then
                    RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                    Dim dblvalue As Double = 0
                    dblvalue = txtLoanAmt.Decimal / txtInstallmentAmt.Decimal
                    'S.SANDEEP [28-SEP-2017] -- START
                    'ISSUE/ENHANCEMENT : LEFT OVER CHANGES
                    'nudDuration.Value = CDec(Math.Ceiling(dblvalue))
                    nudDuration.Value = CDec(IIf(dblvalue <= 0, 1, CDec(Math.Ceiling(dblvalue))))
                    'S.SANDEEP [28-SEP-2017] -- END

                    AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                    'S.SANDEEP [28-SEP-2017] -- START
                    'ISSUE/ENHANCEMENT : LEFT OVER CHANGES
                    RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtPrincipalAmt.Text = Format(txtInstallmentAmt.Decimal, GUI.fmtCurrency)
                    AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtIntAmt.Text = Format(0, GUI.fmtCurrency)
                    txtNetAmount.Tag = txtInstallmentAmt.Decimal
                    txtNetAmount.Decimal = CDec(Format(txtInstallmentAmt.Decimal, GUI.fmtCurrency))
                    'S.SANDEEP [28-SEP-2017] -- END
                End If

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then

                If txtInstallmentAmt.Decimal > 0 Then
                    RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                    Dim dblvalue As Double = 0
                    dblvalue = txtLoanAmt.Decimal / txtInstallmentAmt.Decimal
                    nudDuration.Value = CDec(IIf(dblvalue <= 0, 1, CDec(Math.Ceiling(dblvalue))))

                    AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                    RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtPrincipalAmt.Text = Format(txtInstallmentAmt.Decimal, GUI.fmtCurrency)
                    AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtIntAmt.Text = Format(0, GUI.fmtCurrency)
                    txtNetAmount.Tag = txtInstallmentAmt.Decimal
                    txtNetAmount.Decimal = CDec(Format(txtInstallmentAmt.Decimal, GUI.fmtCurrency))
                End If
                'Sohail (29 Apr 2019) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtInstallmentAmt_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtLoanAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                'Sohail (09 Feb 2018) -- Start
                'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
                RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                Dim dblvalue As Double = 0
                If txtPrincipalAmt.Decimal > 0 Then
                    dblvalue = txtLoanAmt.Decimal / txtPrincipalAmt.Decimal
                End If
                nudDuration.Value = CDec(IIf(dblvalue <= 0, 1, CDec(Math.Ceiling(dblvalue))))
                AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                'Sohail (09 Feb 2018) -- End
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                'Sohail (09 Feb 2018) -- Start
                'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                Call nudDuration_ValueChanged(nudDuration, New System.EventArgs)
                'Sohail (09 Feb 2018) -- End

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Call nudDuration_ValueChanged(nudDuration, New System.EventArgs)
                'Sohail (29 Apr 2019) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtLoanAmt_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtLoanRate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanRate.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtLoanRate_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [20-SEP-2017] -- END


#End Region

#Region "Radio Button's Event"

    Private Sub radLoan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radLoan.CheckedChanged
        'S.SANDEEP [20-SEP-2017] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 50
        'If radLoan.Checked Then
        '    cboLoanScheme.Enabled = True
        '    objbtnAddLoanScheme.Enabled = True
        '    objLoanSchemeSearch.Enabled = True
        '    nudDurationInMths.Enabled = True
        '    txtInstallmentAmt.Enabled = True
        '    txtEMIInstallments.Enabled = True
        'Else
        '    txtEMIInstallments.Enabled = False
        '    txtInstallmentAmt1.Enabled = False
        '    nudDurationInMths.Enabled = False
        '    cboLoanScheme.Enabled = False
        '    objbtnAddLoanScheme.Enabled = False
        '    objLoanSchemeSearch.Enabled = False
        '    cboLoanScheme.SelectedValue = 0
        '    'Nilay (20-Sept-2016) -- Start
        '    'Enhancement : Cancel feature for approved but not assigned loan application
        '    txtInstallmentAmt1.Decimal = 0
        '    txtEMIInstallments.Decimal = 0
        '    'Nilay (20-Sept-2016) -- End
        'End If
        If radLoan.Checked Then
            cboLoanScheme.Enabled = True
            objbtnAddLoanScheme.Enabled = True
            'Hemant (02 Jan 2019) -- Start
            'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
            txtLoanAccNumber.Enabled = True
            'Hemant (02 Jan 2019) -- End
            'Sohail (16 May 2018) -- Start
            'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
            Call FillPeriod(False)
            'Sohail (16 May 2018) -- End
        Else
            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                cboLoanScheme.SelectedValue = 0
                nudDuration.Value = 1
                txtPrincipalAmt.Decimal = 0
                txtLoanAmt.Decimal = 0
            End If

            'Gajanan (23-May-2018) -- Start
            'Issue -(RefNo:180) on advance Loan Data is Not Clear So On Advance Loan Data Also Saved
            cboLoanScheme.Enabled = False
            objbtnAddLoanScheme.Enabled = False
            txtInterestAmt.Decimal = 0
            txtNetAmount.Decimal = 0
            txtLoanRate.Decimal = 0
            nudDuration.Value = 1
            txtPrincipalAmt.Decimal = 0
            txtIntAmt.Decimal = 0
            txtInstallmentAmt.Decimal = 0
            'Gajanan (23-May-2018) -- End
            'Hemant (02 Jan 2019) -- Start
            'Enhancement - On loan application screen there should be a field to set employee's loan account number, the account number should also be displayed on loan report in 76.1.
            txtLoanAccNumber.Text = ""
            txtLoanAccNumber.Enabled = False
            'Hemant (02 Jan 2019) -- End
            'Sohail (16 May 2018) -- Start
            'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
            Call FillPeriod(True)
            'Sohail (16 May 2018) -- End
        End If

        'S.SANDEEP [20-SEP-2017] -- END
    End Sub


#End Region

#Region "Checkbox Event"

    Private Sub chkExternalEntity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExternalEntity.CheckedChanged
        Try
            If chkExternalEntity.CheckState = CheckState.Checked Then
                txtExternalEntity.Enabled = True
            Else
                txtExternalEntity.Text = ""
                txtExternalEntity.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkExternalEntity_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboDeductionPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDeductionPeriod.SelectedIndexChanged
        Try
            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objPeriod._Periodunkid = CInt(cboDeductionPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboDeductionPeriod.SelectedValue)
                'Nilay (10-Oct-2015) -- End

                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
                'Sohail (22 Sep 2017) -- End

                objvalPeriodDuration.Text = Language.getMessage(mstrModuleName, 13, "Period Duration From :") & " " & objPeriod._Start_Date.Date & " " & Language.getMessage(mstrModuleName, 14, "To") & " " & objPeriod._End_Date.Date
                objPeriod = Nothing
            Else
                objvalPeriodDuration.Text = ""
                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                mdtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString())
                mdtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString())
                'Sohail (22 Sep 2017) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDeductionPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private Sub cboEmpName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmpName.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmpName.ValueMember
                    .DisplayMember = cboEmpName.DisplayMember
                    .DataSource = CType(cboEmpName.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmpName.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmpName.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpName_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmpName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpName.SelectedIndexChanged
        Try
            If CInt(cboEmpName.SelectedValue) < 0 Then Call SetDefaultSearchText(cboEmpName)
            'Nilay (16-Nov-2016) -- Start
            If CInt(cboEmpName.SelectedValue) > 0 Then
                With cboEmpName
                    .ForeColor = Color.Black
                    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                    .SelectionStart = cboEmpName.Text.Length
                    .SelectionLength = 0
                End With
            End If
            'Nilay (16-Nov-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpName_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmpName_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpName.GotFocus
        Try
            With cboEmpName
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpName_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmpName_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpName.Leave
        Try
            If CInt(cboEmpName.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboEmpName)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpName_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboLoanScheme.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboLoanScheme.ValueMember
                    .DisplayMember = cboLoanScheme.DisplayMember
                    .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                    .CodeMember = "Code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboLoanScheme.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboLoanScheme.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged
        Try
            'Nilay (16-Nov-2016) -- Start
            'If CInt(cboLoanScheme.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboLoanScheme)
            If CInt(cboLoanScheme.SelectedValue) < 0 Then Call SetDefaultSearchText(cboLoanScheme)
            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                With cboLoanScheme
                    .ForeColor = Color.Black
                    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                    .SelectionStart = cboLoanScheme.Text.Length
                    .SelectionLength = 0
                End With
            End If
            'Nilay (16-Nov-2016) -- End

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 53
            txtExternalEntity.Text = cboLoanScheme.Text
            'S.SANDEEP [20-SEP-2017] -- END

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 52
            If cboLoanScheme.SelectedValue IsNot Nothing AndAlso CInt(cboLoanScheme.SelectedValue) > 0 Then
                Dim objLScheme As New clsLoan_Scheme
                objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                mdecMaxLoanAmountDefined = objLScheme._MaxLoanAmountLimit

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                txtLoanRate.Text = CStr(objLScheme._InterestRate)
                cboLoanCalcType.SelectedValue = objLScheme._LoanCalcTypeId
                cboInterestCalcType.SelectedValue = objLScheme._InterestCalctypeId
                objpnlLoanProjection.Enabled = True
                'S.SANDEEP [20-SEP-2017] -- END

                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                mdecEMI_NetPayPercentage = objLScheme._EMI_NetPayPercentage
                'Sohail (22 Sep 2017) -- End
                'Sohail (11 Apr 2018) -- Start
                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                mintTranheadUnkid = objLScheme._TranheadUnkid
                'Sohail (11 Apr 2018) -- End

                objLScheme = Nothing
            Else
                mdecMaxLoanAmountDefined = 0
                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                cboLoanCalcType.SelectedValue = 0
                cboInterestCalcType.SelectedValue = 0
                cboLoanCalcType.Text = ""
                cboInterestCalcType.Text = ""
                txtInstallmentAmt.Decimal = 0
                objpnlLoanProjection.Enabled = False
                'S.SANDEEP [20-SEP-2017] -- END

                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                mdecEMI_NetPayPercentage = 0
                'Sohail (22 Sep 2017) -- End
                'Sohail (11 Apr 2018) -- Start
                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                'Hemant (25 Apr 2019) -- Start
                'Isssue#0003774(Sport Pesa):  net pay transhead is not showing on the list. Optional basic salary head not working. when you map other transaction head on "basic salary head (Optional)" (loan setting->configuration) the system gives error during application
                'mintTranheadUnkid = objLScheme._TranheadUnkid
                mintTranheadUnkid = ConfigParameter._Object._Advance_NetPayTranheadUnkid
                'Hemant (25 Apr 2019) -- End
                'Sohail (11 Apr 2018) -- End

            End If
            'S.SANDEEP [20-SEP-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.GotFocus
        Try
            With cboLoanScheme
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.Leave
        Try
            If CInt(cboLoanScheme.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub
    'Nilay (27-Oct-2016) -- End

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
    Private Sub cboLoanCalcType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanCalcType.SelectedIndexChanged, cboInterestCalcType.SelectedIndexChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) > 0 Then

                Select Case CInt(cboLoanCalcType.SelectedValue)

                    Case enLoanCalcId.Simple_Interest
                        nudDuration.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Simple_Interest

                        If menAction <> enAction.EDIT_ONE Then
                            Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)
                            txtInstallmentAmt.ReadOnly = True
                            If txtLoanRate.Enabled = False Then
                                txtLoanRate.Enabled = True
                            End If
                        End If

                    Case enLoanCalcId.Reducing_Amount
                        nudDuration.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Reducing_Amount
                        If menAction <> enAction.EDIT_ONE Then
                            Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)
                            txtInstallmentAmt.ReadOnly = True
                            If txtLoanRate.Enabled = False Then
                                txtLoanRate.Enabled = True
                            End If
                        End If
                    Case enLoanCalcId.No_Interest
                        RemoveHandler cboInterestCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
                        cboInterestCalcType.SelectedValue = 0
                        txtInstallmentAmt.ReadOnly = False
                        If menAction <> enAction.EDIT_ONE Then
                            nudDuration.Value = mintNoOfInstallment
                            'Sohail (09 Feb 2018) -- Start
                            'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
                            'nudDuration.Enabled = False
                            nudDuration.Enabled = True
                            'Sohail (09 Feb 2018) -- End
                            txtInstallmentAmt.Text = Format(mdecInstallmentAmount, GUI.fmtCurrency)
                            txtLoanRate.Decimal = 0
                            txtLoanRate.Enabled = False
                            'Gajanan (25-May-2018) -- Start
                            'Issue -When Amount Enter First And After That Loan Scheme Is Select It Does Not Call Loan Installment Method.
                            Call txtLoanAmt_TextChanged(New Object, New EventArgs)
                            'Gajanan (25-May-2018) -- End
                        End If
                        mintLastCalcTypeId = enLoanCalcId.No_Interest
                    Case enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI
                        nudDuration.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI
                        txtPrincipalAmt.ReadOnly = False
                        If menAction <> enAction.EDIT_ONE Then
                            Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)
                            txtInstallmentAmt.ReadOnly = True
                            If txtLoanRate.Enabled = False Then
                                txtLoanRate.Enabled = True
                            End If
                        End If
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    Case enLoanCalcId.No_Interest_With_Mapped_Head
                        RemoveHandler cboInterestCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
                        cboInterestCalcType.SelectedValue = 0
                        txtInstallmentAmt.ReadOnly = False
                        If menAction <> enAction.EDIT_ONE Then
                            nudDuration.Value = mintNoOfInstallment
                            nudDuration.Enabled = False
                            txtInstallmentAmt.Text = Format(mdecInstallmentAmount, GUI.fmtCurrency)
                            txtLoanRate.Decimal = 0
                            txtLoanRate.Enabled = False
                            Call txtLoanAmt_TextChanged(New Object, New EventArgs)
                        End If
                        mintLastCalcTypeId = enLoanCalcId.No_Interest_With_Mapped_Head
                        'Sohail (29 Apr 2019) -- End
                End Select
            End If
            If CInt(cboInterestCalcType.SelectedValue) <= 0 Then
                cboInterestCalcType.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanCalcType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [20-SEP-2017] -- END

    'Sohail (22 Sep 2017) -- Start
    'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExRate.Text = ""
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, mdtPeriodEnd, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (22 Sep 2017) -- End

#End Region

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
#Region " Numeric Up Down Event "

    Private Sub nudDuration_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudDuration.ValueChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                'Sohail (09 Feb 2018) -- Start
                'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                txtInstallmentAmt.Text = Format(txtLoanAmt.Decimal / CDec(nudDuration.Value), GUI.fmtCurrency)
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                txtPrincipalAmt.Text = Format(txtLoanAmt.Decimal / CDec(nudDuration.Value), GUI.fmtCurrency)
                AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                txtNetAmount.Text = Format(txtLoanAmt.Decimal, GUI.fmtCurrency)
                'Sohail (09 Feb 2018) -- End

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                'Sohail (09 Feb 2018) -- Start
                'SUMATRA issue # 0001895: inactive no. of installement field for no interest type loan in loan application in 70.1.
                txtPrincipalAmt.ReadOnly = False
                RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                txtPrincipalAmt.Text = Format(txtLoanAmt.Decimal / nudDuration.Value, GUI.fmtCurrency)
                AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                'Sohail (09 Feb 2018) -- End
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                txtInstallmentAmt.Text = Format(txtLoanAmt.Decimal / CDec(nudDuration.Value), GUI.fmtCurrency)
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                txtPrincipalAmt.Text = Format(txtLoanAmt.Decimal / CDec(nudDuration.Value), GUI.fmtCurrency)
                AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                txtNetAmount.Text = Format(txtLoanAmt.Decimal, GUI.fmtCurrency)
                'Sohail (29 Apr 2019) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudDuration_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP [20-SEP-2017] -- END

#Region " Button's Events "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If


            With objFrm
                .ValueMember = cboEmpName.ValueMember
                .DisplayMember = cboEmpName.DisplayMember
                .DataSource = CType(cboEmpName.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmpName.SelectedValue = objFrm.SelectedValue
                cboEmpName.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddLoanScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddLoanScheme.Click
        Dim frm As New frmLoanScheme_AddEdit
        Dim intLoan_SchemeId As Integer = -1
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(intLoan_SchemeId, enAction.ADD_ONE) Then
                Dim dsCombo As New DataSet
                Dim objLoan As New clsLoan_Scheme
                dsCombo = objLoan.getComboList(True, "LoanScheme")
                With cboLoanScheme
                    .ValueMember = "loanschemeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables("LoanScheme")
                    .SelectedValue = intLoan_SchemeId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddLoanScheme_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objLoanSchemeSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objLoanSchemeSearch.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboLoanScheme.ValueMember
                .DisplayMember = cboLoanScheme.DisplayMember
                .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                .CodeMember = "Code"
            End With
            If objFrm.DisplayDialog Then
                cboLoanScheme.SelectedValue = objFrm.SelectedValue
                cboLoanScheme.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objLoanSchemeSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchDeductionPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchDeductionPeriod.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboDeductionPeriod.ValueMember
                .DisplayMember = cboDeductionPeriod.DisplayMember
                .DataSource = CType(cboDeductionPeriod.DataSource, DataTable)
                'Nilay (23-Aug-2016) -- Start
                'Enhancement - Create New Loan Notification 
                .CodeMember = "code"
                'Nilay (23-Aug-2016) -- End
            End With
            If objFrm.DisplayDialog Then
                cboDeductionPeriod.SelectedValue = objFrm.SelectedValue
                cboDeductionPeriod.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchDeductionPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim btnFlag As Boolean = False
        Try
            If IsValidate() = False Then Exit Sub

            Call SetValue()

            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Me.Cursor = Cursors.WaitCursor
            'Shani (21-Jul-2016) -- End

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objProcesspendingloan._FormName = mstrModuleName
            objProcesspendingloan._Loginemployeeunkid = 0
            objProcesspendingloan._ClientIP = getIP()
            objProcesspendingloan._HostName = getHostName()
            objProcesspendingloan._FromWeb = False
            objProcesspendingloan._AuditUserId = User._Object._Userunkid
objProcesspendingloan._CompanyUnkid = Company._Object._Companyunkid
            objProcesspendingloan._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

         objEmailList = New List(Of clsEmailCollection) 'Hemant (30 Aug 2019)

            If menAction = enAction.EDIT_ONE Then
                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                'btnFlag = objProcesspendingloan.Update()
                btnFlag = objProcesspendingloan.Update(True)
                'Nilay (13-Sept-2016) -- End
            Else
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'btnFlag = objProcesspendingloan.Insert()
                btnFlag = objProcesspendingloan.Insert(FinancialYear._Object._DatabaseName, _
                                                       User._Object._Userunkid, _
                                                       FinancialYear._Object._YearUnkid, _
                                                       Company._Object._Companyunkid, _
                                                       ConfigParameter._Object._LoanApplicationNoType, _
                                                       ConfigParameter._Object._LoanApplicationPrifix)
                'Nilay (10-Oct-2015) -- End
            End If

            If btnFlag = False And objProcesspendingloan._Message <> "" Then
                eZeeMsgBox.Show(objProcesspendingloan._Message, enMsgBoxStyle.Information)
                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
            ElseIf btnFlag AndAlso menAction <> enAction.EDIT_ONE Then

                'Nilay (08-Dec-2016) -- Start
                'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
                'If radLoan.Checked Then
                '    objProcesspendingloan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                '                                                     CInt(cboLoanScheme.SelectedValue), _
                '                                                     CInt(cboEmpName.SelectedValue), -1, _
                '                                                     clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                '                                                     False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                '                                                     ConfigParameter._Object._ArutiSelfServiceURL, True, enLogin_Mode.DESKTOP, 0, 0)
                'Else
                '    objProcesspendingloan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                '                                                     CInt(cboLoanScheme.SelectedValue), _
                '                                                     CInt(cboEmpName.SelectedValue), -1, _
                '                                                     clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                '                                                     False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                '                                                     ConfigParameter._Object._ArutiSelfServiceURL, True, enLogin_Mode.DESKTOP, 0, 0)
                'End If

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                Dim oEmail As New List(Of clsEmailCollection) 'Hemant (30 Aug 2019)
                If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = True Then

                If radLoan.Checked Then

                        If objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED Then

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objProcesspendingloan._FormName = mstrModuleName
                            objProcesspendingloan._Loginemployeeunkid = 0
                            objProcesspendingloan._ClientIP = getIP()
                            objProcesspendingloan._HostName = getHostName()
                            objProcesspendingloan._FromWeb = False
                            objProcesspendingloan._AuditUserId = User._Object._Userunkid
objProcesspendingloan._CompanyUnkid = Company._Object._Companyunkid
                            objProcesspendingloan._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END


                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objProcesspendingloan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid, _
                            '                                                clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                            '                                                clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                            '                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                            '                                                 enLogin_Mode.DESKTOP, 0, 0, False) 'Nilay (27-Dec-2016) -- [False]
                            objProcesspendingloan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid, _
                                                                            clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                            clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , _
                                                                             enLogin_Mode.DESKTOP, 0, 0, False)
                            'Sohail (30 Nov 2017) -- End

                            objProcesspendingloan.Send_Notification_Assign(CInt(cboEmpName.SelectedValue), _
                                                                           FinancialYear._Object._YearUnkid, _
                                                                           ConfigParameter._Object._ArutiSelfServiceURL, _
                                                                           objProcesspendingloan._Processpendingloanunkid, _
                                                                           FinancialYear._Object._DatabaseName, _
                                                                           Company._Object._Companyunkid, _
                                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                           ConfigParameter._Object._UserAccessModeSetting, _
                                                                           ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                           False, enLogin_Mode.DESKTOP, 0, 0, , _
                                                                           ConfigParameter._Object._Notify_LoanAdvance_Users)
                            'Hemant (30 Aug 2019) -- [ConfigParameter._Object._Notify_LoanAdvance_Users]
                        Else

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objProcesspendingloan._FormName = mstrModuleName
                            objProcesspendingloan._Loginemployeeunkid = 0
                            objProcesspendingloan._ClientIP = getIP()
                            objProcesspendingloan._HostName = getHostName()
                            objProcesspendingloan._FromWeb = False
                            objProcesspendingloan._AuditUserId = User._Object._Userunkid
objProcesspendingloan._CompanyUnkid = Company._Object._Companyunkid
                            objProcesspendingloan._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END

                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objProcesspendingloan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                            '                                                         CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), _
                            '                                                         CInt(IIf(objProcesspendingloan._MinApprovedPriority > 0, objProcesspendingloan._MinApprovedPriority, -1)), _
                            '                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                            '                                                 False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                            '                                                 ConfigParameter._Object._ArutiSelfServiceURL, True, enLogin_Mode.DESKTOP, 0, 0)
                            'Hemant (30 Aug 2019) -- Start
                            'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                            'objProcesspendingloan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                            '                                                                                     CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), _
                            '                                                                                     CInt(IIf(objProcesspendingloan._MinApprovedPriority > 0, objProcesspendingloan._MinApprovedPriority, -1)), _
                            '                                                                             clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                            '                                                                             False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                            '                                                                                     ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, True, enLogin_Mode.DESKTOP, 0, 0)
                    objProcesspendingloan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                             CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), _
                                                                             CInt(IIf(objProcesspendingloan._MinApprovedPriority > 0, objProcesspendingloan._MinApprovedPriority, -1)), _
                                                                     clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                     False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                                                                             ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, True, enLogin_Mode.DESKTOP, 0, 0, _
                                                                             False, oEmail)
                            objEmailList.AddRange(oEmail)
                            'Hemant (30 Aug 2019) -- End
                            'Sohail (30 Nov 2017) -- End
                        End If

                    ElseIf radAdvance.Checked Then

                        If objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED Then

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objProcesspendingloan._FormName = mstrModuleName
                            objProcesspendingloan._Loginemployeeunkid = 0
                            objProcesspendingloan._ClientIP = getIP()
                            objProcesspendingloan._HostName = getHostName()
                            objProcesspendingloan._FromWeb = False
                            objProcesspendingloan._AuditUserId = User._Object._Userunkid
objProcesspendingloan._CompanyUnkid = Company._Object._Companyunkid
                            objProcesspendingloan._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END

                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objProcesspendingloan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid, _
                            '                                                 clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                            '                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                            '                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                            '                                                 enLogin_Mode.DESKTOP, 0, 0, False) 'Nilay (27-Dec-2016) -- [False]
                            objProcesspendingloan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid, _
                                                                             clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                             clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , _
                                                                             enLogin_Mode.DESKTOP, 0, 0, False)
                            'Sohail (30 Nov 2017) -- End

                            objProcesspendingloan.Send_Notification_Assign(CInt(cboEmpName.SelectedValue), _
                                                                           FinancialYear._Object._YearUnkid, _
                                                                           ConfigParameter._Object._ArutiSelfServiceURL, _
                                                                           objProcesspendingloan._Processpendingloanunkid, _
                                                                           FinancialYear._Object._DatabaseName, _
                                                                           Company._Object._Companyunkid, _
                                                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                           ConfigParameter._Object._UserAccessModeSetting, _
                                                                           ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                           False, enLogin_Mode.DESKTOP, 0, 0, , _
                                                                           ConfigParameter._Object._Notify_LoanAdvance_Users)
                                                                          'Hemant (30 Aug 2019) -- [ConfigParameter._Object._Notify_LoanAdvance_Users]
                Else

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objProcesspendingloan._FormName = mstrModuleName
                            objProcesspendingloan._Loginemployeeunkid = 0
                            objProcesspendingloan._ClientIP = getIP()
                            objProcesspendingloan._HostName = getHostName()
                            objProcesspendingloan._FromWeb = False
                            objProcesspendingloan._AuditUserId = User._Object._Userunkid
objProcesspendingloan._CompanyUnkid = Company._Object._Companyunkid
                            objProcesspendingloan._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END

                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objProcesspendingloan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                            '                                                         CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), _
                            '                                                         CInt(IIf(objProcesspendingloan._MinApprovedPriority > 0, objProcesspendingloan._MinApprovedPriority, -1)), _
                            '                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                            '                                                 False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                            '                                                 ConfigParameter._Object._ArutiSelfServiceURL, True, enLogin_Mode.DESKTOP, 0, 0)
                            'Hemant (30 Aug 2019) -- Start
                            'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                            'objProcesspendingloan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                            '                                                         CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), _
                            '                                                         CInt(IIf(objProcesspendingloan._MinApprovedPriority > 0, objProcesspendingloan._MinApprovedPriority, -1)), _
                            '                                                 clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                            '                                                 False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                            '                                                        ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, True, enLogin_Mode.DESKTOP, 0, 0)
                    objProcesspendingloan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                             CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), _
                                                                             CInt(IIf(objProcesspendingloan._MinApprovedPriority > 0, objProcesspendingloan._MinApprovedPriority, -1)), _
                                                                     clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                     False, objProcesspendingloan._Processpendingloanunkid.ToString, _
                                                                             ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, True, enLogin_Mode.DESKTOP, 0, 0, _
                                                                             False, oEmail)
                            objEmailList.AddRange(oEmail)
                            'Hemant (30 Aug 2019) -- End                            
                            'Sohail (30 Nov 2017) -- End
                End If
                    End If
                    'Nilay (08-Dec-2016) -- End
                'Shani (21-Jul-2016) -- End
            End If
            End If
                'Nilay (10-Dec-2016) -- End

            If btnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objProcesspendingloan = Nothing
                    objProcesspendingloan = New clsProcess_pending_loan
                    Call GetValue()
                    cboEmpName.Focus()
                Else
                    mintProcessPendingLoanUnkid = objProcesspendingloan._Processpendingloanunkid
                    Me.Close()
                End If
            End If

            'Hemant (30 Aug 2019) -- Start
            'ISSUE#0004110(ZURI) :  Error on global assigning loans..
            trd = New Thread(AddressOf Send_Notification)
            trd.IsBackground = True
            trd.Start()
            'Hemant (30 Aug 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
        Finally
            Me.Cursor = Cursors.Default
            'Shani (21-Jul-2016) -- End
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region


    


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            
            Call SetLanguage()
			
			Me.gbProcessPendingLoanInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbProcessPendingLoanInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbProcessPendingLoanInfo.Text = Language._Object.getCaption(Me.gbProcessPendingLoanInfo.Name, Me.gbProcessPendingLoanInfo.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.lblAmt.Text = Language._Object.getCaption(Me.lblAmt.Name, Me.lblAmt.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblApplicationDate.Text = Language._Object.getCaption(Me.lblApplicationDate.Name, Me.lblApplicationDate.Text)
			Me.lblApplicationNo.Text = Language._Object.getCaption(Me.lblApplicationNo.Name, Me.lblApplicationNo.Text)
			Me.chkExternalEntity.Text = Language._Object.getCaption(Me.chkExternalEntity.Name, Me.chkExternalEntity.Text)
			Me.lblEmpRemark.Text = Language._Object.getCaption(Me.lblEmpRemark.Name, Me.lblEmpRemark.Text)
			Me.lblDeductionPeriod.Text = Language._Object.getCaption(Me.lblDeductionPeriod.Name, Me.lblDeductionPeriod.Text)
            Me.lblEMIInstallments1.Text = Language._Object.getCaption(Me.lblEMIInstallments1.Name, Me.lblEMIInstallments1.Text)
            Me.lblEMIAmount1.Text = Language._Object.getCaption(Me.lblEMIAmount1.Name, Me.lblEMIAmount1.Text)
            Me.lblDuration.Text = Language._Object.getCaption(Me.lblDuration.Name, Me.lblDuration.Text)
			Me.elProjectedINSTLAmt.Text = Language._Object.getCaption(Me.elProjectedINSTLAmt.Name, Me.elProjectedINSTLAmt.Text)
			Me.lblIntAmt.Text = Language._Object.getCaption(Me.lblIntAmt.Name, Me.lblIntAmt.Text)
			Me.lblPrincipalAmt.Text = Language._Object.getCaption(Me.lblPrincipalAmt.Name, Me.lblPrincipalAmt.Text)
			Me.lblLoanInterest.Text = Language._Object.getCaption(Me.lblLoanInterest.Name, Me.lblLoanInterest.Text)
			Me.lblEMIInstallments.Text = Language._Object.getCaption(Me.lblEMIInstallments.Name, Me.lblEMIInstallments.Text)
			Me.lblEMIAmount.Text = Language._Object.getCaption(Me.lblEMIAmount.Name, Me.lblEMIAmount.Text)
			Me.lnProjectedAmount.Text = Language._Object.getCaption(Me.lnProjectedAmount.Name, Me.lnProjectedAmount.Text)
			Me.lblInterestAmt.Text = Language._Object.getCaption(Me.lblInterestAmt.Name, Me.lblInterestAmt.Text)
			Me.lblNetAmount.Text = Language._Object.getCaption(Me.lblNetAmount.Name, Me.lblNetAmount.Text)
			Me.lblInterestCalcType.Text = Language._Object.getCaption(Me.lblInterestCalcType.Name, Me.lblInterestCalcType.Text)
			Me.lblLoanCalcType.Text = Language._Object.getCaption(Me.lblLoanCalcType.Name, Me.lblLoanCalcType.Text)
            Me.radAdvance.Text = Language._Object.getCaption(Me.radAdvance.Name, Me.radAdvance.Text)
            Me.radLoan.Text = Language._Object.getCaption(Me.radLoan.Name, Me.radLoan.Text)
            Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.Name, Me.lblLoanAdvance.Text)
			Me.lblLoanAccNumber.Text = Language._Object.getCaption(Me.lblLoanAccNumber.Name, Me.lblLoanAccNumber.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue.")
			Language.setMessage(mstrModuleName, 3, "Loan/Advance Amount cannot be blank. Loan/Advance Amount is compulsory information.")
			Language.setMessage(mstrModuleName, 4, "Currency is compulsory information.Please select currency.")
			Language.setMessage(mstrModuleName, 5, "Application No cannot be blank. Application No is compulsory information.")
			Language.setMessage(mstrModuleName, 6, "External Entity cannot be blank. External Entity is compulsory information.")
			Language.setMessage(mstrModuleName, 7, "Application date should be between current financial year.")
			Language.setMessage(mstrModuleName, 9, "Deduction Period is compulsory information.Please select deduction period.")
			Language.setMessage(mstrModuleName, 10, "Installment Amount cannot be 0.Please define installment amount greater than 0.")
			Language.setMessage(mstrModuleName, 13, "Period Duration From :")
			Language.setMessage(mstrModuleName, 14, "To")
			Language.setMessage(mstrModuleName, 15, "Please Assign Approver(s) to this employee.")
			Language.setMessage(mstrModuleName, 16, "Please Map this Loan scheme(s) to this employee's Approver(s).")
            Language.setMessage(mstrModuleName, 17, "Installment Amount cannot be greater than Loan Amount.")
            Language.setMessage(mstrModuleName, 18, "Type to Search")
            Language.setMessage(mstrModuleName, 19, "Sorry, you can not apply for this loan scheme: Reason, amount applied is exceeding the max amount")
            Language.setMessage(mstrModuleName, 20, " set for selected scheme.")
            Language.setMessage(mstrModuleName, 21, "Sorry, you can not apply loan for the selected scheme: Reason, The selected loan scheme is already assigned to you and having (In-progress or On hold) status. Instead of applying new loan, You can add top-up amount from Loan Advance List-> Operation -> Other Loan Operations.")
			Language.setMessage(mstrModuleName, 22, "Sorry, you can not apply for this loan scheme: Reason, There is no previous salary history for this employee.")
			Language.setMessage(mstrModuleName, 23, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the")
			Language.setMessage(mstrModuleName, 24, "of previous net pay.")
            Language.setMessage(mstrModuleName, 28, "Principal Amount cannot be 0. Please enter Principal amount.")
			Language.setMessage(mstrModuleName, 29, "Sorry, you can not apply for this loan scheme: Reason, Either There is no previous salary history for this employee for the net pay head set on loan scheme master screen.")
			Language.setMessage(mstrModuleName, 30, "Sorry, you can not apply for this loan scheme: Reason, Monthly EMI amount applied is exceeding the")
			Language.setMessage(mstrModuleName, 31, "of previous net pay head")
			Language.setMessage(mstrModuleName, 32, "Installment months cannot be greater than")
			Language.setMessage(mstrModuleName, 33, "for")
			Language.setMessage(mstrModuleName, 34, "Scheme.")
			Language.setMessage(mstrModuleName, 35, "Sorry, you can not apply for this Advance: Reason, There is no previous salary history for this employee.")
			Language.setMessage(mstrModuleName, 36, "Sorry, you can not apply for this Advance: Reason, Either There is no previous salary history for this employee for the net pay head set on Configuration -> Option screen.")
			Language.setMessage(mstrModuleName, 37, "Sorry, you can not apply for this Advance: Reason, Advance amount applied is exceeding the")
			Language.setMessage(mstrModuleName, 38, "Sorry, you can not apply for this Advance: Reason, Advance amount applied is exceeding the")
			Language.setMessage(mstrModuleName, 39, "Sorry, you can not apply for Advance: Reason, The Advance is already assigned to you and having (In-progress or On hold) status.")
			Language.setMessage(mstrModuleName, 40, "Sorry, you can not apply for Advance: Reason, Days to Advance Has Been Exceeded.")
			Language.setMessage(mstrModuleName, 41, "Sorry, Loan tenure should not be greater than employee tenure month")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

    
End Class