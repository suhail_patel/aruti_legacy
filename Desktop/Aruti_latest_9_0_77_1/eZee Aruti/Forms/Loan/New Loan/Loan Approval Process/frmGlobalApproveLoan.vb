﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGlobalApproveLoan

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGlobalApproveLoan"
    Private objPendingLoan As clsProcess_pending_loan
    Private objLoanapproval As clsloanapproval_process_Tran
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private dtTab As DataTable
    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage
    Private mstrAdvanceFilter As String = ""
    Private mstrEmployeeIDs As String = ""
    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private mstrSearchText As String = String.Empty
    'Nilay (27-Oct-2016) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objAMaster As New clsLoanApprover_master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objMasterData As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim objLevel As New clslnapproverlevel_master
        Try

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objAMaster.GetEmployeeFromUser(User._Object._Userunkid, ConfigParameter._Object._EmployeeAsOnDate.ToString, ConfigParameter._Object._IsIncludeInactiveEmp)
            dsCombo = objAMaster.GetEmployeeFromUser(FinancialYear._Object._DatabaseName, _
                                                     User._Object._Userunkid, _
                                                     FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                     True, _
                                                     ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                     "List")
            'Nilay (10-Oct-2015) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables(0)
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboEmployee)
            'Nilay (27-Oct-2016) -- End

            Dim lstIDs As List(Of String) = (From p In dsCombo.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
            mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsCombo = objLoanScheme.getComboList(True, "LoanScheme")
            dsCombo = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("LoanScheme")
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboLoanScheme)
            'Nilay (27-Oct-2016) -- End

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                FinancialYear._Object._YearUnkid, _
                                                FinancialYear._Object._DatabaseName, _
                                                FinancialYear._Object._Database_Start_Date, _
                                                "Period", True, enStatusType.Open)
            'Nilay (10-Oct-2015) -- End

            With cboDeductionPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
            End With

            With dgcolhDeductionPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period").Copy
            End With

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsCombo = objMasterData.GetCondition(False, True)
            dsCombo = objMasterData.GetCondition(False, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            Dim dtCondition As DataTable = New DataView(dsCombo.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            With cboAmountCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dtCondition
            End With


            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            RemoveHandler cboApprover.SelectedIndexChanged, AddressOf cboApprover_SelectedIndexChanged
            'Nilay (21-Jul-2016) -- End
            dsCombo = objLevel.GetLevelFromUserLogin(User._Object._Userunkid)
            With cboApprover
                .ValueMember = "lnapproverunkid"
                .DisplayMember = "Approver"
                .DataSource = dsCombo.Tables(0)
            End With
            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            AddHandler cboApprover.SelectedIndexChanged, AddressOf cboApprover_SelectedIndexChanged
            'Nilay (21-Jul-2016) -- End

            dsCombo = objPendingLoan.GetLoan_Status("List", True)
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'Dim dtTemp() As DataRow = dsCombo.Tables("List").Select("Id NOT IN(2)")
            'For i As Integer = 0 To dtTemp.Length - 1
            '    dsCombo.Tables("List").Rows.Remove(dtTemp(i))
            'Next
            Dim xRow = From p In dsCombo.Tables("List") Where CInt(p.Item("Id")) = enLoanApplicationStatus.APPROVED OrElse CInt(p.Item("Id")) = enLoanApplicationStatus.REJECTED Select p
            Dim dtTable As DataTable = xRow.CopyToDataTable
            'Nilay (20-Sept-2016) -- End
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable 'Nilay (20-Sept-2016) -- [REMOVED: dsCombo.Tables("List")]
                .SelectedValue = 2
            End With

            '========START FOR ALIGMENT RIGHT IN DATAGRID NUMERIC FIELD.
            dgcolhAAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhAAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgcolhRAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhRAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            'Nilay (21-Oct-2015) -- Start
            'ENHANCEMENT : NEW LOAN Given By Rutta
            dgcolhBasicSal.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhBasicSal.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            'Nilay (21-Oct-2015) -- End

            dgcolhDuration.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhDuration.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgcolhInstallmentAmt.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhInstallmentAmt.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgcolhNoOfInstallments.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhNoOfInstallments.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            '========END FOR ALIGMENT RIGHT IN DATAGRID NUMERIC FIELD.


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objAMaster = Nothing : objLoanScheme = Nothing : objMasterData = Nothing : objPeriod = Nothing : objLevel = Nothing
        End Try
    End Sub

    Private Sub FillGrid()
        Dim intMode As Integer = 0
        Dim mstrSearch As String = ""
        Try

            If radAdvance.Checked Then
                intMode = 0
            ElseIf radLoan.Checked Then
                intMode = 1
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.deductionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue) & " "
            End If

            If txtLoanAmount.Text.Trim <> "" AndAlso txtLoanAmount.Decimal > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.loan_amount  " & cboAmountCondition.Text & " " & txtLoanAmount.Decimal & " "
            End If

            If mstrEmployeeIDs.Trim.Length > 0 Then
                mstrSearch &= "AND lnloan_process_pending_loan.employeeunkid IN (" & mstrEmployeeIDs & ") "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                mstrSearch &= "AND " & mstrAdvanceFilter
            End If

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Trim.Substring(3)
            End If

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dtTab = objPendingLoan.GetDataList(User._Object._Userunkid, CInt(cboApprover.SelectedValue), intMode, mstrSearch)
            dtTab = objPendingLoan.GetDataList(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, _
                                               True, _
                                               ConfigParameter._Object._IsIncludeInactiveEmp, _
                                               "List", _
                                               CInt(cboApprover.SelectedValue), intMode, mstrSearch)
            'Nilay (10-Oct-2015) -- End

            dgvData.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsChecked"
            dgcolhAAmount.DataPropertyName = "AppAmount"
            dgcolhAppNo.DataPropertyName = "AppNo"
            dgcolhEName.DataPropertyName = "EName"
            'Nilay (21-Oct-2015) -- Start
            'ENHANCEMENT : NEW LOAN Given By Rutta
            dgcolhBasicSal.DataPropertyName = "BasicSal"
            'Nilay (21-Oct-2015) -- End
            dgcolhDeductionPeriod.DataPropertyName = "DeductionPeriodID"
            dgcolhDuration.DataPropertyName = "duration"
            dgcolhInstallmentAmt.DataPropertyName = "installmentamt"
            dgcolhNoOfInstallments.DataPropertyName = "noofinstallment"
            dgcolhRAmount.DataPropertyName = "Amount"
            objcolhPendingUnkid.DataPropertyName = "PId"
            objdgcolhIsGrp.DataPropertyName = "IsGrp"
            objdgcolhGrpId.DataPropertyName = "GrpId"
            'Sohail (03 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            objdgcolhemployeeunkid.DataPropertyName = "employeeunkid"
            'Sohail (03 Apr 2018) -- End

            dgvData.DataSource = dtTab

            dgcolhRAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhAAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            dgcolhInstallmentAmt.DefaultCellStyle.Format = GUI.fmtCurrency
            'Nilay (21-Oct-2015) -- Start
            'ENHANCEMENT : NEW LOAN Given By Rutta
            dgcolhBasicSal.DefaultCellStyle.Format = GUI.fmtCurrency
            'Nilay (21-Oct-2015) -- End

            dgcolhDeductionPeriod.DefaultCellStyle.ForeColor = Color.Blue
            dgcolhDeductionPeriod.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
            dgcolhDeductionPeriod.DefaultCellStyle.SelectionBackColor = Color.White
            dgcolhDeductionPeriod.DefaultCellStyle.SelectionForeColor = Color.Red

            dgcolhDuration.DefaultCellStyle.ForeColor = Color.Blue
            dgcolhDuration.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
            dgcolhDuration.DefaultCellStyle.SelectionBackColor = Color.White
            dgcolhDuration.DefaultCellStyle.SelectionForeColor = Color.Red

            dgcolhInstallmentAmt.DefaultCellStyle.ForeColor = Color.Blue
            dgcolhInstallmentAmt.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
            dgcolhInstallmentAmt.DefaultCellStyle.SelectionBackColor = Color.White
            dgcolhInstallmentAmt.DefaultCellStyle.SelectionForeColor = Color.Red

            dgcolhNoOfInstallments.DefaultCellStyle.ForeColor = Color.Blue
            dgcolhNoOfInstallments.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
            dgcolhNoOfInstallments.DefaultCellStyle.SelectionBackColor = Color.White
            dgcolhNoOfInstallments.DefaultCellStyle.SelectionForeColor = Color.Red

            dgcolhAAmount.DefaultCellStyle.ForeColor = Color.Blue
            dgcolhAAmount.DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
            dgcolhAAmount.DefaultCellStyle.SelectionBackColor = Color.White
            dgcolhAAmount.DefaultCellStyle.SelectionForeColor = Color.Red

            'Sohail (15 May 2018) -- Start
            'CCK Enhancement - Ref # 179 : On loan approval screen on MSS, not all approvers should see the basic salary of the staff. in 72.1.
            dgcolhBasicSal.Visible = User._Object.Privilege._AllowTo_View_Scale
            'Sohail (15 May 2018) -- End

            Call SetGridColor()
            Call SetCollapseValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            Call objbtnSearch.ShowResult(CStr(dgvData.RowCount))
            'Nilay (20-Sept-2016) -- End
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If btnClose.Focused = False Then
                For i As Integer = 0 To dgvData.RowCount - 1

                    If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = False Then

                            'Nilay (21-Oct-2015) -- Start
                            'ENHANCEMENT : NEW LOAN Given By Rutta
                            'If radLoan.Checked AndAlso CInt(dgvData.Rows(i).Cells(dgcolhDuration.Index).Value) <= 0 Then
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Duration in Months cannot be 0.Please define duration in months greater than 0."), enMsgBoxStyle.Information)
                            'dgvData.Focus()
                            'dgvData.CurrentCell = dgvData.Rows(i).Cells(dgcolhDuration.Index)
                            'SendKeys.Send("{F2}")
                            'Return False
                            
                        'ElseIf CInt(dgvData.Rows(i).Cells(dgcolhDeductionPeriod.Index).Value) <= 0 Then
                         If CInt(dgvData.Rows(i).Cells(dgcolhDeductionPeriod.Index).Value) <= 0 Then
                            'Nilay (21-Oct-2015) -- End
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Deduction Period is compulsory information.Please select deduction period."), enMsgBoxStyle.Information)
                            dgvData.Focus()
                            dgvData.CurrentCell = dgvData.Rows(i).Cells(dgcolhDeductionPeriod.Index)
                            SendKeys.Send("{F4}")
                            Return False

                        ElseIf radLoan.Checked AndAlso CDec(dgvData.Rows(i).Cells(dgcolhInstallmentAmt.Index).Value) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Installment Amount cannot be 0.Please define installment amount greater than 0."), enMsgBoxStyle.Information)
                            dgvData.Focus()
                            dgvData.CurrentCell = dgvData.Rows(i).Cells(dgcolhInstallmentAmt.Index)
                            SendKeys.Send("{F2}")
                            Return False

                        ElseIf radLoan.Checked AndAlso CInt(dgvData.Rows(i).Cells(dgcolhNoOfInstallments.Index).Value) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "No of Installment cannot be 0.Please define No of Installment greater than 0."), enMsgBoxStyle.Information)
                            dgvData.Focus()
                            dgvData.CurrentCell = dgvData.Rows(i).Cells(dgcolhNoOfInstallments.Index)
                            SendKeys.Send("{F2}")
                            Return False

                        ElseIf radLoan.Checked AndAlso (Not IsDBNull(dgvData.Rows(i).Cells(dgcolhInstallmentAmt.Index).Value) AndAlso Not IsDBNull(dgvData.Rows(i).Cells(dgcolhAAmount.Index).Value)) AndAlso CDec(dgvData.Rows(i).Cells(dgcolhInstallmentAmt.Index).Value) > CDec(dgvData.Rows(i).Cells(dgcolhAAmount.Index).Value) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Installment Amount cannot be greater than approved loan amount."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            dgvData.Focus()
                            dgvData.CurrentCell = dgvData.Rows(i).Cells(dgcolhInstallmentAmt.Index)
                            SendKeys.Send("{F2}")
                            Return False

                            'Nilay (15-Dec-2015) -- Start
                            'ElseIf radLoan.Checked AndAlso Format(CDec(dgvData.Rows(i).Cells(dgcolhAAmount.Index).Value), GUI.fmtCurrency) <> Format((CInt(dgvData.Rows(i).Cells(dgcolhNoOfInstallments.Index).Value) * CDec(dgvData.Rows(i).Cells(dgcolhInstallmentAmt.Index).Value)), GUI.fmtCurrency) Then
                            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "You are changing approved loan amount.We recommanded you that you have to change No of installments or Installment amount."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            '    dgvData.Focus()
                            '    dgvData.CurrentCell = dgvData.Rows(i).Cells(dgcolhNoOfInstallments.Index)
                            '    SendKeys.Send("{F2}")
                            '    Return False
                            'Nilay (15-Dec-2015) -- End
                        End If
                        'Varsha (25 Nov 2017) -- Start
                        'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                        Dim objPendingLoan As New clsProcess_pending_loan
                        objPendingLoan._Processpendingloanunkid = CInt(dgvData.Rows(i).Cells(objcolhPendingUnkid.Index).Value)
                        Dim objLoanScheme As New clsLoan_Scheme
                        objLoanScheme._Loanschemeunkid = objPendingLoan._Loanschemeunkid
                        If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(dgvData.Rows(i).Cells(dgcolhNoOfInstallments.Index).Value) > objLoanScheme._MaxNoOfInstallment Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Language.getMessage(mstrModuleName, 14, " for ") & objLoanScheme._Name & Language.getMessage(mstrModuleName, 15, " Scheme , Application No : [ ") & objPendingLoan._Application_No & Language.getMessage(mstrModuleName, 16, " ] , Employee Code and Name is") & CStr(dgvData.Rows(i).Cells(dgcolhEName.Index).Value) & ".", enMsgBoxStyle.Information)
                            dgvData.Focus()
                            dgvData.CurrentCell = dgvData.Rows(i).Cells(dgcolhNoOfInstallments.Index)
                            SendKeys.Send("{F2}")
                            Return False
                        End If
                        'Varsha (25 Nov 2017) -- End
                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                        If CInt(cboStatus.SelectedValue) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select atleast one Status from the list to perform further operation.."), enMsgBoxStyle.Information)
                            cboStatus.Focus()
                            Return False
                        End If
                        'If CInt(cboStatus.SelectedValue) = 3 AndAlso txtRemarks.Text.Trim.Length <= 0 Then
                        If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED AndAlso txtRemarks.Text.Trim.Length <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Remark cannot be blank. Remark is compulsory information."), enMsgBoxStyle.Information)
                            txtRemarks.Focus()
                            Return False
                        End If
                        'Nilay (20-Sept-2016) -- End
                    End If

                Next

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvData.Rows(i).DefaultCellStyle = dgvcsHeader
                    dgvData.Rows(i).Cells(dgcolhAAmount.Index).ReadOnly = True
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvData.Font.FontFamily, 13, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            For i As Integer = 0 To dgvData.RowCount - 1
                If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                        dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                    End If
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
                Else
                    dgvData.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 12, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Nilay (27-Oct-2016) -- End

#End Region

#Region " Form's Events "

    Private Sub frmGlobalApproveLoan_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPendingLoan = New clsProcess_pending_loan
        objLoanapproval = New clsloanapproval_process_Tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalApproveLoan_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsProcess_pending_loan.SetMessages()
            objfrm._Other_ModuleNames = "clsProcess_pending_loan"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objFrm.DisplayDialog Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchScheme.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboLoanScheme.ValueMember
                .DisplayMember = cboLoanScheme.DisplayMember
                'Nilay (15-Dec-2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                .CodeMember = "Code"
                'Nilay (15-Dec-2015) -- End
                .DataSource = CType(cboLoanScheme.DataSource, DataTable)
            End With

            If objFrm.DisplayDialog Then
                cboLoanScheme.SelectedValue = objFrm.SelectedValue
                cboLoanScheme.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchScheme_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchDeductionPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchDeductionPeriod.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboDeductionPeriod.ValueMember
                .DisplayMember = cboDeductionPeriod.DisplayMember
                'Nilay (15-Dec-2015) -- Start
                .CodeMember = "code"
                'Nilay (15-Dec-2015) -- End
                .DataSource = CType(cboDeductionPeriod.DataSource, DataTable)
            End With

            If objFrm.DisplayDialog Then
                cboDeductionPeriod.SelectedValue = objFrm.SelectedValue
                cboDeductionPeriod.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchDeductionPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchApprover.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboApprover.ValueMember
                .DisplayMember = cboApprover.DisplayMember
                .DataSource = CType(cboApprover.DataSource, DataTable)
            End With

            If objFrm.DisplayDialog Then
                cboApprover.SelectedValue = objFrm.SelectedValue
                cboApprover.Focus()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If radLoan.Checked = False AndAlso radAdvance.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Select Loan/Advance to continue global approval process."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
            FillGrid()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            'Nilay (16-Nov-2016) -- Start
            cboEmployee.SelectedValue = 0
            cboLoanScheme.SelectedValue = 0
            Call SetDefaultSearchText(cboEmployee)
            Call SetDefaultSearchText(cboLoanScheme)
            lblEmployee.Focus()
            'Nilay (16-Nov-2016) -- End

            cboDeductionPeriod.SelectedValue = 0
            txtLoanAmount.Decimal = 0
            cboAmountCondition.SelectedIndex = 0
            RemoveHandler radLoan.CheckedChanged, AddressOf radLoan_CheckedChanged
            radLoan.Checked = False
            AddHandler radLoan.CheckedChanged, AddressOf radLoan_CheckedChanged
            RemoveHandler radAdvance.CheckedChanged, AddressOf radAdvance_CheckedChanged
            radAdvance.Checked = False
            AddHandler radAdvance.CheckedChanged, AddressOf radAdvance_CheckedChanged
            dgvData.DataSource = Nothing
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            mstrAdvanceFilter = ""
            'Nilay (01-Mar-2016) -- End

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            Call FillGrid()
            'Nilay (20-Sept-2016) -- End


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CInt(cboApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Approver is compulsory information. Please select Approver to continue."), enMsgBoxStyle.Information)
                cboApprover.Focus()
                Exit Sub
            End If

            If dtTab Is Nothing Then Exit Sub

            Dim dtTemp() As DataRow = Nothing
            dtTemp = dtTab.Select("IsChecked = 1 And IsGrp = false")
            If dtTemp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select atleast one information to Approve."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If IsValidate() Then
                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                Dim objProcessLoan As New clsProcess_pending_loan

                With objProcessLoan
                    ._FormName = mstrModuleName
                    ._Loginemployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With

                Dim intPriority As Integer = -1
                intPriority = CType(cboApprover.DataSource, DataTable).AsEnumerable().Where(Function(x) x.Field(Of Integer)("lnapproverunkid") = CInt(cboApprover.SelectedValue)).Select(Function(x) x.Field(Of Integer)("priority")).First
                
                Me.Cursor = Cursors.WaitCursor
                'Shani (21-Jul-2016) -- End
                For i As Integer = 0 To dtTemp.Length - 1
                    objLoanapproval._Pendingloantranunkid = CInt(dtTemp(i).Item("pendingloantranunkid"))
                    objLoanapproval._Processpendingloanunkid = CInt(dtTemp(i).Item("PId"))
                    objLoanapproval._Approvertranunkid = CInt(cboApprover.SelectedValue)
                    objLoanapproval._Approvaldate = ConfigParameter._Object._CurrentDateAndTime.Date
                    objLoanapproval._Statusunkid = CInt(cboStatus.SelectedValue)
                    objLoanapproval._Deductionperiodunkid = CInt(dtTemp(i).Item("DeductionPeriodID"))
                    'Nilay (21-Oct-2015) -- Start
                    'ENHANCEMENT : NEW LOAN Given By Rutta
                    'objLoanapproval._Duration = CInt(dtTemp(i).Item("duration"))
                    'Nilay (21-Oct-2015) -- End
                    objLoanapproval._Installmentamt = CDec(dtTemp(i).Item("installmentamt"))
                    objLoanapproval._Noofinstallment = CInt(dtTemp(i).Item("noofinstallment"))
                    objLoanapproval._Loan_Amount = CDec(dtTemp(i).Item("AppAmount"))
                    objLoanapproval._Userunkid = User._Object._Userunkid
                    'Nilay (21-Oct-2015) -- Start
                    'ENHANCEMENT : NEW LOAN Given By Rutta
                    objLoanapproval._Remark = txtRemarks.Text.ToString
                    'Nilay (21-Oct-2015) -- End

                    'S.SANDEEP [28-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objLoanapproval._FormName = mstrModuleName
                    objLoanapproval._LoginEmployeeunkid = 0
                    objLoanapproval._ClientIP = getIP()
                    objLoanapproval._HostName = getHostName()
                    objLoanapproval._FromWeb = False
                    objLoanapproval._AuditUserId = User._Object._Userunkid
objLoanapproval._CompanyUnkid = Company._Object._Companyunkid
                    objLoanapproval._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END

                    If objLoanapproval.Update(CBool(ConfigParameter._Object._IsLoanApprover_ForLoanScheme), CInt(dtTemp(i)("GrpId")), Nothing) = False Then
                        eZeeMsgBox.Show(objLoanapproval._Message, enMsgBoxStyle.Information)
                        dgvData.Rows(dtTab.Rows.IndexOf(dtTemp(i))).Cells(dgcolhAppNo.Index).Selected = True
                        Exit Sub
                    End If

                    With objProcessLoan
                        ._FormName = mstrModuleName
                        ._Loginemployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With

                    'Nilay (10-Dec-2016) -- Start
                    'Issue #26: Setting to be included on configuration for Loan flow Approval process
                    If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = True Then
                    'Shani (21-Jul-2016) -- Start
                    'Enhancement - Create New Loan Notification 

                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'If CInt(cboStatus.SelectedValue) = 2 Then 'Approve
                    If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.APPROVED Then
                        'Nilay (20-Sept-2016) -- End
                        If objLoanapproval.IsPendingLoanApplication(CInt(dtTemp(i).Item("PId")), True) = False Then
                            If radLoan.Checked Then
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                    '                                      CInt(cboLoanScheme.SelectedValue), _
                                    '                                      CInt(dtTemp(i).Item("employeeunkid")), _
                                    '                                      intPriority, _
                                    '                                      clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                    '                                      False, dtTemp(i).Item("PId").ToString, _
                                    '                                      ConfigParameter._Object._ArutiSelfServiceURL, True, _
                                    '                                      enLogin_Mode.DESKTOP, 0, 0)
                                objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                      CInt(cboLoanScheme.SelectedValue), _
                                                                      CInt(dtTemp(i).Item("employeeunkid")), _
                                                                      intPriority, _
                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                      False, dtTemp(i).Item("PId").ToString, _
                                                                          ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, True, _
                                                                      enLogin_Mode.DESKTOP, 0, 0)
                                    'Sohail (30 Nov 2017) -- End
                            Else
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                    '                                      CInt(cboLoanScheme.SelectedValue), _
                                    '                                      CInt(dtTemp(i).Item("employeeunkid")), _
                                    '                                      intPriority, _
                                    '                                      clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                    '                                      False, dtTemp(i).Item("PId").ToString, _
                                    '                                      ConfigParameter._Object._ArutiSelfServiceURL, True, _
                                    '                                      enLogin_Mode.DESKTOP, 0, 0)
                                objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                      CInt(cboLoanScheme.SelectedValue), _
                                                                      CInt(dtTemp(i).Item("employeeunkid")), _
                                                                      intPriority, _
                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                      False, dtTemp(i).Item("PId").ToString, _
                                                                          ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, True, _
                                                                      enLogin_Mode.DESKTOP, 0, 0)
                                    'Sohail (30 Nov 2017) -- End
                            End If
                            Else
                                'Nilay (08-Dec-2016) -- Start
                                'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
                                If radLoan.Checked Then
                            
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), CInt(dtTemp(i).Item("PId")), _
                                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                    '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                                    '                                          enLogin_Mode.DESKTOP, 0, 0, False) 'Nilay (27-Dec-2016) -- [False]
                                    objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), CInt(dtTemp(i).Item("PId")), _
                                                                              clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                              clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , _
                                                                              enLogin_Mode.DESKTOP, 0, 0, False)
                                    'Sohail (30 Nov 2017) -- End
                                Else
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), CInt(dtTemp(i).Item("PId")), _
                                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                    '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                                    '                                          enLogin_Mode.DESKTOP, 0, 0, False) 'Nilay (27-Dec-2016) -- [False]
                                    objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), CInt(dtTemp(i).Item("PId")), _
                                                                              clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                              clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , _
                                                                              enLogin_Mode.DESKTOP, 0, 0, False)
                                    'Sohail (30 Nov 2017) -- End
                                End If

                                objProcessLoan.Send_Notification_Assign(CInt(dtTemp(i).Item("employeeunkid")), _
                                                                    FinancialYear._Object._YearUnkid, _
                                                                    ConfigParameter._Object._ArutiSelfServiceURL, _
                                                                        CInt(dtTemp(i).Item("PId")), _
                                                                    FinancialYear._Object._DatabaseName, _
                                                                    Company._Object._Companyunkid, _
                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                                    ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                        False, enLogin_Mode.DESKTOP, 0, 0, , _
                                                                    ConfigParameter._Object._Notify_LoanAdvance_Users)
                                'Hemant (30 Aug 2019) -- [ConfigParameter._Object._Notify_LoanAdvance_Users]
                                'Nilay (08-Dec-2016) -- End

                        End If
                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                        'ElseIf CInt(cboStatus.SelectedValue) = 3 Then ' Reject
                    ElseIf CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED Then
                        'Nilay (20-Sept-2016) -- End
                        If radLoan.Checked Then
                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), CInt(dtTemp(i).Item("PId")), _
                                '                                          clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                '                                          txtRemarks.Text, enLogin_Mode.DESKTOP, 0, 0)
                                objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), CInt(dtTemp(i).Item("PId")), _
                                                                          clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, _
                                                                          txtRemarks.Text, enLogin_Mode.DESKTOP, 0, 0)
                                'Sohail (30 Nov 2017) -- End
                        Else
                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), CInt(dtTemp(i).Item("PId")), _
                                '                                          clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                '                                          txtRemarks.Text, enLogin_Mode.DESKTOP, 0, 0)
                                objProcessLoan.Send_Notification_Employee(CInt(dtTemp(i).Item("employeeunkid")), CInt(dtTemp(i).Item("PId")), _
                                                                          clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                                                          clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, _
                                                                          txtRemarks.Text, enLogin_Mode.DESKTOP, 0, 0)
                                'Sohail (30 Nov 2017) -- End
                        End If
                    End If
                    'Shani (21-Jul-2016) -- End
                    End If
                    'Nilay (10-Dec-2016) -- End
                Next

                Call FillGrid()
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                txtRemarks.Text = ""
                'Nilay (20-Sept-2016) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Me.Cursor = Cursors.Default
            'Shani (21-Jul-2016) -- End
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (03 Apr 2018) -- Start
    'CCK Enhancement : Ref. No. 184 - View employee diary on loan approval screen. This should help the management on decision to approve/reject the loan application in 71.1.
    Private Sub btnViewDiary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewDiary.Click
        Dim frm As New frmEmployeeDiary
        Try
            If CInt(dgvData.SelectedRows(0).Cells(objdgcolhemployeeunkid.Index).Value) <= 0 Then Exit Sub
            If dgvData.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please select Loan Application approver from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                dgvData.Select()
                Exit Sub
            End If
            frm.displayDialog(CInt(dgvData.SelectedRows(0).Cells(objdgcolhemployeeunkid.Index).Value))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnViewDiary_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sohail (03 Apr 2018) -- End

#End Region

#Region "DataGrid Event"

    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick, dgvData.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvData.IsCurrentCellDirty Then
                Me.dgvData.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgvData.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvData.Rows(i).Visible = False Then
                                    dgvData.Rows(i).Visible = True
                                Else
                                    dgvData.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                    Case 1
                        For i = e.RowIndex + 1 To dgvData.RowCount - 1
                            Dim blnFlg As Boolean = CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
                            If CInt(dgvData.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvData.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                dgvData.Rows(i).Cells(objdgcolhCheck.Index).Value = blnFlg
                            End If
                        Next
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellValueChanged
        Try
            If e.RowIndex = -1 Then Exit Sub

            If CBool(dgvData.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then Exit Sub

            RemoveHandler dgvData.CellValueChanged, AddressOf dgvData_CellValueChanged

            If IsDBNull(dgvData.Rows(e.RowIndex).Cells(dgcolhAAmount.Index).Value) Then
                dgvData.Rows(e.RowIndex).Cells(dgcolhAAmount.Index).Value = dgvData.Rows(e.RowIndex).Cells(dgcolhRAmount.Index).Value
            ElseIf CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhAAmount.Index).Value) <= 0 Then
                dgvData.Rows(e.RowIndex).Cells(dgcolhAAmount.Index).Value = CDec(dgvData.Rows(e.RowIndex).Cells(dgcolhRAmount.Index).Value)
            End If

            Dim dtTable As DataTable = CType(dgvData.DataSource, DataTable)

            If e.ColumnIndex = dgcolhInstallmentAmt.Index Then
                'Nilay (15-Dec-2015) -- Start
                'If CDec(dgvData.CurrentRow.Cells(dgcolhAAmount.Index).Value) > 0 AndAlso CInt(dgvData.CurrentRow.Cells(dgcolhInstallmentAmt.Index).Value) > 0 Then
                '    dgvData.CurrentRow.Cells(dgcolhNoOfInstallments.Index).Value = CInt(CDec(dgvData.CurrentRow.Cells(dgcolhAAmount.Index).Value) / CDec(dgvData.CurrentRow.Cells(dgcolhInstallmentAmt.Index).Value))
                'End If
                If CDec(dgvData.CurrentRow.Cells(dgcolhAAmount.Index).Value) > 0 AndAlso CInt(dgvData.CurrentRow.Cells(dgcolhInstallmentAmt.Index).Value) > 0 Then
                    dgvData.CurrentRow.Cells(dgcolhNoOfInstallments.Index).Value = CInt(Math.Ceiling(CDec(dgvData.CurrentRow.Cells(dgcolhAAmount.Index).Value) / CDec(dgvData.CurrentRow.Cells(dgcolhInstallmentAmt.Index).Value)))
                End If
                'Nilay (15-Dec-2015) -- End

            ElseIf e.ColumnIndex = dgcolhNoOfInstallments.Index Then

                If CDec(dgvData.CurrentRow.Cells(dgcolhAAmount.Index).Value) > 0 AndAlso CInt(dgvData.CurrentRow.Cells(dgcolhNoOfInstallments.Index).Value) > 0 Then
                    dgvData.CurrentRow.Cells(dgcolhInstallmentAmt.Index).Tag = CDec(CDec(dgvData.CurrentRow.Cells(dgcolhAAmount.Index).Value) / CInt(dgvData.CurrentRow.Cells(dgcolhNoOfInstallments.Index).Value))
                    dgvData.CurrentRow.Cells(dgcolhInstallmentAmt.Index).Value = CDec(CDec(dgvData.CurrentRow.Cells(dgcolhAAmount.Index).Value) / CInt(dgvData.CurrentRow.Cells(dgcolhNoOfInstallments.Index).Value))
                Else
                    dgvData.CurrentRow.Cells(dgcolhInstallmentAmt.Index).Tag = CDec(dgvData.CurrentRow.Cells(dgcolhAAmount.Index).Value)
                    dgvData.CurrentRow.Cells(dgcolhInstallmentAmt.Index).Value = CDec(dgvData.CurrentRow.Cells(dgcolhAAmount.Index).Value)
                End If
                'Nilay (15-Dec-2015) -- Start
            ElseIf e.ColumnIndex = dgcolhAAmount.Index Then

                If CDec(dgvData.CurrentRow.Cells(dgcolhAAmount.Index).Value) > 0 AndAlso CDec(dgvData.CurrentRow.Cells(dgcolhInstallmentAmt.Index).Value) > 0 Then
                    dgvData.CurrentRow.Cells(dgcolhNoOfInstallments.Index).Value = CInt(Math.Ceiling(CDec(dgvData.CurrentRow.Cells(dgcolhAAmount.Index).Value) / CDec(dgvData.CurrentRow.Cells(dgcolhInstallmentAmt.Index).Value)))
                    dgvData.CurrentRow.Cells(dgcolhInstallmentAmt.Index).Tag = CDec(CDec(dgvData.CurrentRow.Cells(dgcolhAAmount.Index).Value) / CInt(dgvData.CurrentRow.Cells(dgcolhNoOfInstallments.Index).Value))
                Else

                End If
                'Nilay (15-Dec-2015) -- End

                dtTable.Rows(e.RowIndex)("installmentamt") = CDec(dgvData.CurrentRow.Cells(dgcolhInstallmentAmt.Index).Tag)
            End If
            AddHandler dgvData.CellValueChanged, AddressOf dgvData_CellValueChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvData_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvData.EditingControlShowing
        Try
            'Nilay (28-Aug-2015) -- Start
            'If (Me.dgvData.CurrentCell.ColumnIndex = 6) And Not e.Control Is Nothing Then
            '    Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
            '    AddHandler txt.KeyPress, AddressOf tb_keypress
            'End If
            If (Me.dgvData.CurrentCell.ColumnIndex = dgcolhDuration.Index) And Not e.Control Is Nothing Then
                Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
                AddHandler txt.KeyPress, AddressOf tb_keypress
            End If
            'Nilay (28-Aug-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmpCashDenomination_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvData.DataBindingComplete
        Try
            If dgvData.DataSource IsNot Nothing AndAlso dgvData.RowCount > 0 Then
                Dim cboPeriod As DataGridViewComboBoxCell = Nothing
                For i As Integer = 0 To dgvData.RowCount - 1
                    If CBool(dgvData.Rows(i).Cells(objdgcolhIsGrp.Index).Value) Then
                        cboPeriod = CType(dgvData.Rows(i).Cells(dgcolhDeductionPeriod.Index), DataGridViewComboBoxCell)
                        cboPeriod.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                        cboPeriod.ReadOnly = True
                        'Nilay (18-Feb-2016) -- Start
                        'dgvData.Rows(i).ReadOnly = True
                        'Nilay (18-Feb-2016) -- End
                    End If
                Next
                cboPeriod = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_DataBindingComplete", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvData.DataError

    End Sub

    'Private Sub dgvData_RowValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.RowValidated
    '    Try
    '        If IsValidate() = False Then
    '            Exit Sub
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvData_RowValidated", mstrModuleName)
    '    End Try
    'End Sub

#End Region

#Region "Radio Button Event"

    Private Sub radAdvance_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radAdvance.CheckedChanged
        Try
            If radAdvance.Checked Then
                Call FillGrid()
                dgcolhDuration.Visible = False
                dgcolhInstallmentAmt.Visible = False
                dgcolhNoOfInstallments.Visible = False
                'Nilay (28-Aug-2015) -- Start
                dgcolhEName.Width = 235
                dgcolhBasicSal.Width = 125
                'Nilay (28-Aug-2015) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radAdvance_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radLoan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radLoan.CheckedChanged
        Try
            If radLoan.Checked Then
                'Nilay (21-Oct-2015) -- Start
                'ENHANCEMENT : NEW LOAN Given By Rutta
                'dgcolhDuration.Visible = True
                'Nilay (21-Oct-2015) -- End
                dgcolhInstallmentAmt.Visible = True
                dgcolhNoOfInstallments.Visible = True
                Call FillGrid()
                'Nilay (28-Aug-2015) -- Start
                dgcolhEName.Width = 200
                dgcolhBasicSal.Width = 100
                'Nilay (28-Aug-2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radLoan_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'mstrAdvanceFilter = frm._GetFilterString
                frm._Hr_EmployeeTable_Alias = "hremployee_master"
                mstrAdvanceFilter = frm._GetFilterString
                Call FillGrid()
                'Nilay (01-Mar-2016) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    'Shani (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification 
#Region " ComboBox's Events "

    Private Sub cboApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApprover.SelectedIndexChanged
        Try
            If CInt(cboApprover.SelectedValue) > 0 Then
                Call objbtnSearch_Click(objbtnSearch, New EventArgs())
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboApprover_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) < 0 Then Call SetDefaultSearchText(cboEmployee)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Try
            With cboEmployee
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboEmployee)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboLoanScheme.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboLoanScheme.ValueMember
                    .DisplayMember = cboLoanScheme.DisplayMember
                    .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                    .CodeMember = "Code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboLoanScheme.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboLoanScheme.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged
        Try
            If CInt(cboLoanScheme.SelectedValue) < 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.GotFocus
        Try
            With cboLoanScheme
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.Leave
        Try
            If CInt(cboLoanScheme.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_Leave", mstrModuleName)
        End Try
    End Sub
    'Nilay (27-Oct-2016) -- End

    'Private Sub cboApprover_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApprover.Validated
    '    Try
    '        If CInt(cboApprover.SelectedValue) > 0 Then
    '            Call objbtnSearch_Click(objbtnSearch, New EventArgs())
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboApprover_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboApprover_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboApprover.Validating
    '    Try
    '        If CInt(cboApprover.SelectedValue) > 0 Then
    '            Call objbtnSearch_Click(objbtnSearch, New EventArgs())
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboApprover_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

#End Region
    'Shani (21-Jul-2016) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
            Call SetLanguage()
			
			Me.gbPending.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbPending.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilter.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilter.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnViewDiary.GradientBackColor = GUI._ButttonBackColor 
			Me.btnViewDiary.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbPending.Text = Language._Object.getCaption(Me.gbPending.Name, Me.gbPending.Text)
			Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
			Me.radAdvance.Text = Language._Object.getCaption(Me.radAdvance.Name, Me.radAdvance.Text)
			Me.radLoan.Text = Language._Object.getCaption(Me.radLoan.Name, Me.radLoan.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.Name, Me.gbFilter.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.LblDeductionPeriod.Text = Language._Object.getCaption(Me.LblDeductionPeriod.Name, Me.LblDeductionPeriod.Text)
			Me.lblLoanAmount.Text = Language._Object.getCaption(Me.lblLoanAmount.Name, Me.lblLoanAmount.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
			Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
			Me.dgcolhBasicSal.HeaderText = Language._Object.getCaption(Me.dgcolhBasicSal.Name, Me.dgcolhBasicSal.HeaderText)
			Me.dgcolhAppNo.HeaderText = Language._Object.getCaption(Me.dgcolhAppNo.Name, Me.dgcolhAppNo.HeaderText)
			Me.dgcolhRAmount.HeaderText = Language._Object.getCaption(Me.dgcolhRAmount.Name, Me.dgcolhRAmount.HeaderText)
			Me.dgcolhDeductionPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhDeductionPeriod.Name, Me.dgcolhDeductionPeriod.HeaderText)
			Me.dgcolhDuration.HeaderText = Language._Object.getCaption(Me.dgcolhDuration.Name, Me.dgcolhDuration.HeaderText)
			Me.dgcolhInstallmentAmt.HeaderText = Language._Object.getCaption(Me.dgcolhInstallmentAmt.Name, Me.dgcolhInstallmentAmt.HeaderText)
			Me.dgcolhNoOfInstallments.HeaderText = Language._Object.getCaption(Me.dgcolhNoOfInstallments.Name, Me.dgcolhNoOfInstallments.HeaderText)
			Me.dgcolhAAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAAmount.Name, Me.dgcolhAAmount.HeaderText)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
	    Me.btnViewDiary.Text = Language._Object.getCaption(Me.btnViewDiary.Name, Me.btnViewDiary.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select atleast one information to Approve.")
			Language.setMessage(mstrModuleName, 2, "Approver is compulsory information. Please select Approver to continue.")
			Language.setMessage(mstrModuleName, 4, "Deduction Period is compulsory information.Please select deduction period.")
			Language.setMessage(mstrModuleName, 5, "Installment Amount cannot be 0.Please define installment amount greater than 0.")
			Language.setMessage(mstrModuleName, 6, "No of Installment cannot be 0.Please define No of Installment greater than 0.")
			Language.setMessage(mstrModuleName, 7, "Installment Amount cannot be greater than approved loan amount.")
			Language.setMessage(mstrModuleName, 9, "Please Select Loan/Advance to continue global approval process.")
                        Language.setMessage(mstrModuleName, 10, "Please select atleast one Status from the list to perform further operation..")
                        Language.setMessage(mstrModuleName, 11, "Remark cannot be blank. Remark is compulsory information.")
                        Language.setMessage(mstrModuleName, 12, "Type to Search")
			Language.setMessage(mstrModuleName, 13, "Installment months cannot be greater than")
			Language.setMessage(mstrModuleName, 14, " for")
                        Language.setMessage(mstrModuleName, 15, " Scheme , Application No : [ ")
			Language.setMessage(mstrModuleName, 16, " ] , Employee Code and Name is")
			Language.setMessage(mstrModuleName, 17, "Please select Loan Application approver from the list to perform further operation.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

   
End Class