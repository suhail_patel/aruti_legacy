﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanSchemeList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanSchemeList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboInterestCalcType = New System.Windows.Forms.ComboBox
        Me.lblInterestCalcType = New System.Windows.Forms.Label
        Me.cboLoanCalcType = New System.Windows.Forms.ComboBox
        Me.lblLoanCalcType = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.txtMaxLoanAmount = New eZee.TextBox.NumericTextBox
        Me.objbtnSearchLoanScheme = New eZee.Common.eZeeGradientButton
        Me.lblMaxLoanAmt = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.txtMinSalary = New eZee.TextBox.NumericTextBox
        Me.lblMinSalary = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvLoanScheme = New eZee.Common.eZeeListView(Me.components)
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhLoanName = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.colhMinSalary = New System.Windows.Forms.ColumnHeader
        Me.colhMaximumAmt = New System.Windows.Forms.ColumnHeader
        Me.colhLoanCalcType = New System.Windows.Forms.ColumnHeader
        Me.colhInterestCalcType = New System.Windows.Forms.ColumnHeader
        Me.colhLoanRate = New System.Windows.Forms.ColumnHeader
        Me.pnlMainInfo.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.lvLoanScheme)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(830, 488)
        Me.pnlMainInfo.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(830, 58)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "List of Loan Scheme"
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboInterestCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.lblInterestCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanCalcType)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.txtMaxLoanAmount)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.lblMaxLoanAmt)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.txtMinSalary)
        Me.gbFilterCriteria.Controls.Add(Me.lblMinSalary)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(806, 92)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInterestCalcType
        '
        Me.cboInterestCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterestCalcType.FormattingEnabled = True
        Me.cboInterestCalcType.Location = New System.Drawing.Point(128, 61)
        Me.cboInterestCalcType.Name = "cboInterestCalcType"
        Me.cboInterestCalcType.Size = New System.Drawing.Size(188, 21)
        Me.cboInterestCalcType.TabIndex = 337
        '
        'lblInterestCalcType
        '
        Me.lblInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestCalcType.Location = New System.Drawing.Point(10, 63)
        Me.lblInterestCalcType.Name = "lblInterestCalcType"
        Me.lblInterestCalcType.Size = New System.Drawing.Size(112, 16)
        Me.lblInterestCalcType.TabIndex = 336
        Me.lblInterestCalcType.Text = "Int. Calc. Type"
        Me.lblInterestCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanCalcType
        '
        Me.cboLoanCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanCalcType.DropDownWidth = 250
        Me.cboLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanCalcType.FormattingEnabled = True
        Me.cboLoanCalcType.Location = New System.Drawing.Point(128, 34)
        Me.cboLoanCalcType.Name = "cboLoanCalcType"
        Me.cboLoanCalcType.Size = New System.Drawing.Size(188, 21)
        Me.cboLoanCalcType.TabIndex = 335
        '
        'lblLoanCalcType
        '
        Me.lblLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanCalcType.Location = New System.Drawing.Point(10, 36)
        Me.lblLoanCalcType.Name = "lblLoanCalcType"
        Me.lblLoanCalcType.Size = New System.Drawing.Size(112, 16)
        Me.lblLoanCalcType.TabIndex = 334
        Me.lblLoanCalcType.Text = "Loan Calc. Type"
        Me.lblLoanCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboLoanScheme.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Location = New System.Drawing.Point(442, 34)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(324, 21)
        Me.cboLoanScheme.TabIndex = 0
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(324, 37)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(112, 16)
        Me.lblLoanScheme.TabIndex = 85
        Me.lblLoanScheme.Text = "Loan Scheme"
        '
        'txtMaxLoanAmount
        '
        Me.txtMaxLoanAmount.AllowNegative = True
        Me.txtMaxLoanAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMaxLoanAmount.DigitsInGroup = 0
        Me.txtMaxLoanAmount.Flags = 0
        Me.txtMaxLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxLoanAmount.Location = New System.Drawing.Point(661, 61)
        Me.txtMaxLoanAmount.MaxDecimalPlaces = 6
        Me.txtMaxLoanAmount.MaxWholeDigits = 21
        Me.txtMaxLoanAmount.Name = "txtMaxLoanAmount"
        Me.txtMaxLoanAmount.Prefix = ""
        Me.txtMaxLoanAmount.RangeMax = 1.7976931348623157E+308
        Me.txtMaxLoanAmount.RangeMin = -1.7976931348623157E+308
        Me.txtMaxLoanAmount.Size = New System.Drawing.Size(105, 21)
        Me.txtMaxLoanAmount.TabIndex = 243
        Me.txtMaxLoanAmount.Text = "0"
        Me.txtMaxLoanAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objbtnSearchLoanScheme
        '
        Me.objbtnSearchLoanScheme.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLoanScheme.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLoanScheme.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLoanScheme.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLoanScheme.BorderSelected = False
        Me.objbtnSearchLoanScheme.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLoanScheme.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLoanScheme.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLoanScheme.Location = New System.Drawing.Point(772, 34)
        Me.objbtnSearchLoanScheme.Name = "objbtnSearchLoanScheme"
        Me.objbtnSearchLoanScheme.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLoanScheme.TabIndex = 88
        '
        'lblMaxLoanAmt
        '
        Me.lblMaxLoanAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxLoanAmt.Location = New System.Drawing.Point(553, 63)
        Me.lblMaxLoanAmt.Name = "lblMaxLoanAmt"
        Me.lblMaxLoanAmt.Size = New System.Drawing.Size(102, 16)
        Me.lblMaxLoanAmt.TabIndex = 244
        Me.lblMaxLoanAmt.Text = "Max. Loan Amount"
        Me.lblMaxLoanAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(780, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(756, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'txtMinSalary
        '
        Me.txtMinSalary.AllowNegative = True
        Me.txtMinSalary.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMinSalary.DigitsInGroup = 0
        Me.txtMinSalary.Flags = 0
        Me.txtMinSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMinSalary.Location = New System.Drawing.Point(442, 61)
        Me.txtMinSalary.MaxDecimalPlaces = 6
        Me.txtMinSalary.MaxWholeDigits = 21
        Me.txtMinSalary.Name = "txtMinSalary"
        Me.txtMinSalary.Prefix = ""
        Me.txtMinSalary.RangeMax = 1.7976931348623157E+308
        Me.txtMinSalary.RangeMin = -1.7976931348623157E+308
        Me.txtMinSalary.Size = New System.Drawing.Size(105, 21)
        Me.txtMinSalary.TabIndex = 1
        Me.txtMinSalary.Text = "0"
        Me.txtMinSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMinSalary
        '
        Me.lblMinSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinSalary.Location = New System.Drawing.Point(325, 63)
        Me.lblMinSalary.Name = "lblMinSalary"
        Me.lblMinSalary.Size = New System.Drawing.Size(111, 16)
        Me.lblMinSalary.TabIndex = 241
        Me.lblMinSalary.Text = "Min. Monthly Salary"
        Me.lblMinSalary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 433)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(830, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(618, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(515, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 1
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(412, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 0
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(721, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lvLoanScheme
        '
        Me.lvLoanScheme.BackColorOnChecked = True
        Me.lvLoanScheme.ColumnHeaders = Nothing
        Me.lvLoanScheme.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCode, Me.colhLoanName, Me.colhDescription, Me.colhMinSalary, Me.colhMaximumAmt, Me.colhLoanCalcType, Me.colhInterestCalcType, Me.colhLoanRate})
        Me.lvLoanScheme.CompulsoryColumns = ""
        Me.lvLoanScheme.FullRowSelect = True
        Me.lvLoanScheme.GridLines = True
        Me.lvLoanScheme.GroupingColumn = Nothing
        Me.lvLoanScheme.HideSelection = False
        Me.lvLoanScheme.Location = New System.Drawing.Point(12, 162)
        Me.lvLoanScheme.MinColumnWidth = 50
        Me.lvLoanScheme.MultiSelect = False
        Me.lvLoanScheme.Name = "lvLoanScheme"
        Me.lvLoanScheme.OptionalColumns = ""
        Me.lvLoanScheme.ShowMoreItem = False
        Me.lvLoanScheme.ShowSaveItem = False
        Me.lvLoanScheme.ShowSelectAll = True
        Me.lvLoanScheme.ShowSizeAllColumnsToFit = True
        Me.lvLoanScheme.Size = New System.Drawing.Size(806, 266)
        Me.lvLoanScheme.Sortable = True
        Me.lvLoanScheme.TabIndex = 1
        Me.lvLoanScheme.UseCompatibleStateImageBehavior = False
        Me.lvLoanScheme.View = System.Windows.Forms.View.Details
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 70
        '
        'colhLoanName
        '
        Me.colhLoanName.Tag = "colhLoanName"
        Me.colhLoanName.Text = "Loan Scheme Name"
        Me.colhLoanName.Width = 130
        '
        'colhDescription
        '
        Me.colhDescription.Tag = "colhDescription"
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 0
        '
        'colhMinSalary
        '
        Me.colhMinSalary.Tag = "colhMinSalary"
        Me.colhMinSalary.Text = "Min. Net Monthly Salary"
        Me.colhMinSalary.Width = 135
        '
        'colhMaximumAmt
        '
        Me.colhMaximumAmt.Tag = "colhMaximumAmt"
        Me.colhMaximumAmt.Text = "Max. Loan Amount"
        Me.colhMaximumAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhMaximumAmt.Width = 130
        '
        'colhLoanCalcType
        '
        Me.colhLoanCalcType.Tag = "colhLoanCalcType"
        Me.colhLoanCalcType.Text = "Loan Calc. Type"
        Me.colhLoanCalcType.Width = 120
        '
        'colhInterestCalcType
        '
        Me.colhInterestCalcType.Tag = "colhInterestCalcType"
        Me.colhInterestCalcType.Text = "Interest Calc. Type"
        Me.colhInterestCalcType.Width = 110
        '
        'colhLoanRate
        '
        Me.colhLoanRate.Tag = "colhLoanRate"
        Me.colhLoanRate.Text = "Loan Rate"
        Me.colhLoanRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhLoanRate.Width = 70
        '
        'frmLoanSchemeList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(830, 488)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanSchemeList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "List of Loan Scheme"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchLoanScheme As eZee.Common.eZeeGradientButton
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvLoanScheme As eZee.Common.eZeeListView
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoanName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMinSalary As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtMinSalary As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMinSalary As System.Windows.Forms.Label
    Friend WithEvents colhMaximumAmt As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtMaxLoanAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMaxLoanAmt As System.Windows.Forms.Label
    Friend WithEvents cboLoanCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanCalcType As System.Windows.Forms.Label
    Friend WithEvents cboInterestCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblInterestCalcType As System.Windows.Forms.Label
    Friend WithEvents colhLoanCalcType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInterestCalcType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoanRate As System.Windows.Forms.ColumnHeader
End Class
