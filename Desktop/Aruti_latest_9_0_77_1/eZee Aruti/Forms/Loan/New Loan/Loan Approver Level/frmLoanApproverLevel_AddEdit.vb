﻿Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 2

Public Class frmLoanApproverLevel_AddEdit

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmLoanApproverLevel_AddEdit"
    Private mblnCancel As Boolean = True
    Private objlnApproverLevel As clslnapproverlevel_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintlnApproverLevelunkid As Integer = -1
    Dim wkMins As Double

#End Region

#Region "Display Dialog"

    Public Function DisplayDialog(ByVal intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintlnApproverLevelunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintlnApproverLevelunkid

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DisplayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmLoanApproverLevel_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objlnApproverLevel = New clslnapproverlevel_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)

            OtherSettings()

            SetColor()

            If menAction = enAction.EDIT_ONE Then
                objlnApproverLevel._lnLevelunkid = mintlnApproverLevelunkid
            End If

            GetValue()
            txtLevelName.Focus()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanApproverLevel_AddEdit_Load", mstrModuleName)
        End Try

    End Sub

    Private Sub frmLoanApproverLevel_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanApproverLevel_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanApproverLevel_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanApproverLevel_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanApproverLevel_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objlnApproverLevel = Nothing
    End Sub

    Private Sub frmLoanApproverLevel_AddEdit_LanguageClick(ByVal sender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clslnapproverlevel_master.SetMessages()

            objfrm._Other_ModuleNames = "clslnapproverlevel_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmLoanApproverLevel_AddEdit_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtLevelName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Loan Approver Level Name cannot be blank. Loan Approver Level Name is required information."), enMsgBoxStyle.Information)
                txtLevelName.Focus()
                Exit Sub
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objlnApproverLevel._FormName = mstrModuleName
            objlnApproverLevel._LoginEmployeeunkid = 0
            objlnApproverLevel._ClientIP = getIP()
            objlnApproverLevel._HostName = getHostName()
            objlnApproverLevel._FromWeb = False
            objlnApproverLevel._AuditUserId = User._Object._Userunkid
objlnApproverLevel._CompanyUnkid = Company._Object._Companyunkid
            objlnApproverLevel._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objlnApproverLevel.Update()
            Else
                blnFlag = objlnApproverLevel.Insert()
            End If

            If blnFlag = False And objlnApproverLevel._Message <> "" Then
                eZeeMsgBox.Show(objlnApproverLevel._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objlnApproverLevel = Nothing
                    objlnApproverLevel = New clslnapproverlevel_master
                    Call GetValue()
                    txtLevelName.Focus()
                Else
                    mintlnApproverLevelunkid = objlnApproverLevel._lnLevelunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            Call objFrm.displayDialog(txtLevelName.Text, objlnApproverLevel._lnLevelname1, objlnApproverLevel._lnLevelname2)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub SetColor()
        Try
            txtLevelName.BackColor = GUI.ColorComp
            nudPriority.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtLevelName.Text = objlnApproverLevel._lnLevelname
            nudPriority.Value = objlnApproverLevel._Priority
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objlnApproverLevel._lnLevelname = txtLevelName.Text
            objlnApproverLevel._Priority = CInt(nudPriority.Value)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

			Call SetLanguage()
			
			Me.gbApproverLevelInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbApproverLevelInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbApproverLevelInfo.Text = Language._Object.getCaption(Me.gbApproverLevelInfo.Name, Me.gbApproverLevelInfo.Text)
			Me.lblLevelName.Text = Language._Object.getCaption(Me.lblLevelName.Name, Me.lblLevelName.Text)
			Me.lblPriority.Text = Language._Object.getCaption(Me.lblPriority.Name, Me.lblPriority.Text)
			Me.lblPRemark.Text = Language._Object.getCaption(Me.lblPRemark.Name, Me.lblPRemark.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Loan Approver Level Name cannot be blank. Loan Approver Level Name is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class