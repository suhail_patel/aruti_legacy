﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmLoanApprover_Migration

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmLoanApprover_Migration"
    Private mblnCancel As Boolean = True
    Private mstrAdvanceFilter As String = ""
    Private menAction As enAction = enAction.ADD_ONE
    Private dvOldApproveEmp As DataView
    Private dvNewApprovAssignEmp As DataView
    Private dvNewApprovMigratedEmp As DataView
    Dim objApprover As clsLoanApprover_master


    'Pinkal (11-Dec-2017) -- Start
    'Bug - Solved Loan Approver Migration Issue .
    Dim dsCombo As DataSet = Nothing
    'Pinkal (11-Dec-2017) -- End


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function


#End Region

#Region "Form's Event"

    Private Sub frmLoanApprover_Migration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            objApprover = New clsLoanApprover_master
            FillCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanApprover_Migration_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods"

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try

            'Pinkal (11-Dec-2017) -- Start
            'Bug - Solved Loan Approver Migration Issue .
            'Dim dtTable As DataTable = New DataView(objApprover.getListForCombo("List", True, True).Tables(0), "", "", DataViewRowState.CurrentRows).ToTable()
            dsCombo = objApprover.getListForCombo("List", True, True, False)
            Dim dtTable As DataTable = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "lnempapproverunkid", "name", "isexternalapprover")
            'Pinkal (11-Dec-2017) -- End

            cboOldApprover.ValueMember = "lnempapproverunkid"
            cboOldApprover.DisplayMember = "name"
            cboOldApprover.DataSource = dtTable
            dtTable = Nothing

            dsFill = Nothing
            Dim objApproverLevel As New clslnapproverlevel_master
            dsFill = objApproverLevel.getListForCombo("List", True)
            cboOldLevel.ValueMember = "lnlevelunkid"
            cboOldLevel.DisplayMember = "name"
            cboOldLevel.DataSource = dsFill.Tables(0)

            cboNewLevel.ValueMember = "lnlevelunkid"
            cboNewLevel.DisplayMember = "name"
            cboNewLevel.DataSource = dsFill.Tables(0).Copy

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillOldApproverEmployeeList()
        Try
            If dvOldApproveEmp IsNot Nothing Then
                dgOldApproverEmp.AutoGenerateColumns = False
                dgOldApproverEmp.DataSource = dvOldApproveEmp
                objcolhdgSelect.DataPropertyName = "select"
                colhdgEmployeecode.DataPropertyName = "employeecode"
                colhdgEmployee.DataPropertyName = "employeename"
            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "FillOldApproverEmployeeList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillNewApproverAssignEmployeeList()
        Try

            If dvNewApprovAssignEmp IsNot Nothing Then
                dgNewApproverAssignEmp.AutoGenerateColumns = False
                dgNewApproverAssignEmp.DataSource = dvNewApprovAssignEmp
                colhdgAssignEmpCode.DataPropertyName = "employeecode"
                colhdgAssignEmployee.DataPropertyName = "employeename"
            End If
          
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "FillOldApproverEmployeeList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillNewApproverMigratedEmployeeList()
        Try
            If dvNewApprovMigratedEmp IsNot Nothing Then
                dgNewApproverMigratedEmp.AutoGenerateColumns = False
                dgNewApproverMigratedEmp.DataSource = dvNewApprovMigratedEmp
                objColhdgMigratedSelect.DataPropertyName = "select"
                colhdgMigratedEmpCode.DataPropertyName = "employeecode"
                colhdgMigratedEmp.DataPropertyName = "employeename"
            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "FillNewApproverMigratedEmployeeList", mstrModuleName)
        End Try
    End Sub

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    'Private Sub GetLoanApproverLevel(ByVal sender As Object, ByVal intApproverEmpID As Integer)
    Private Sub GetLoanApproverLevel(ByVal sender As Object, ByVal intApproverEmpID As Integer, Optional ByVal blnExtarnalApprover As Boolean = False)
        'Nilay (01-Mar-2016) -- End
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'Dim dsList As DataSet = objApprover.GetLevelFromLoanApprover(True, intApproverEmpID)
            Dim dsList As DataSet = objApprover.GetLevelFromLoanApprover(intApproverEmpID, blnExtarnalApprover, True)
            'Nilay (01-Mar-2016) -- End

            If CType(sender, ComboBox).Name.ToUpper = "CBONEWAPPROVER" Then
                cboNewLevel.DisplayMember = "name"
                cboNewLevel.ValueMember = "lnlevelunkid"
                cboNewLevel.DataSource = dsList.Tables(0)

            ElseIf CType(sender, ComboBox).Name.ToUpper = "CBOOLDAPPROVER" Then
                cboOldLevel.DisplayMember = "name"
                cboOldLevel.ValueMember = "lnlevelunkid"
                cboOldLevel.DataSource = dsList.Tables(0)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLoanApproverLevel", mstrModuleName)
        End Try
    End Sub

    Private Sub SetCheckBoxOldApproverValue()
        Try
            Dim drRow As DataRow() = dvOldApproveEmp.Table.Select("select = true")

            RemoveHandler objchkOldCheck.CheckedChanged, AddressOf objchkOldCheck_CheckedChanged

            If drRow.Length <= 0 Then
                objchkOldCheck.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dvOldApproveEmp.Table.Rows.Count Then
                objchkOldCheck.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dvOldApproveEmp.Table.Rows.Count Then
                objchkOldCheck.CheckState = CheckState.Checked
            End If

            AddHandler objchkOldCheck.CheckedChanged, AddressOf objchkOldCheck_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxOldApproverValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetCheckBoxNewApproverValue()
        Try
            Dim drRow As DataRow() = dvNewApprovMigratedEmp.ToTable.Select("select = true")

            RemoveHandler objchkNewCheck.CheckedChanged, AddressOf objchkNewCheck_CheckedChanged

            If drRow.Length <= 0 Then
                objchkNewCheck.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgNewApproverMigratedEmp.Rows.Count Then
                objchkNewCheck.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgNewApproverMigratedEmp.Rows.Count Then
                objchkNewCheck.CheckState = CheckState.Checked
            End If

            AddHandler objchkNewCheck.CheckedChanged, AddressOf objchkNewCheck_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxNewApproverValue", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnSearchOldApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOldApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboOldApprover.DataSource, DataTable)
            With cboOldApprover
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOldApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchNewApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchNewApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboNewApprover.DataSource, DataTable)
            With cboNewApprover
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchNewApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchNewLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchNewLevel.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboNewLevel.DataSource, DataTable)
            With cboNewLevel
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchNewLevel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchOldLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOldLevel.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboOldLevel.DataSource, DataTable)
            With cboOldLevel
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOldLevel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            txtOldSearchEmployee.Text = ""
            mstrAdvanceFilter = ""
            If dvOldApproveEmp IsNot Nothing Then dvOldApproveEmp.RowFilter = mstrAdvanceFilter
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
        Try
            If dvOldApproveEmp Is Nothing Then
                GoTo warning
            End If

            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), enMsgBoxStyle.Information)
                cboNewApprover.Select()
                Exit Sub
            ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Level to migrate employee(s)."), enMsgBoxStyle.Information)
                cboNewLevel.Select()
                Exit Sub
            End If

            Dim drRow() As DataRow = dvOldApproveEmp.Table.Select("select = true")

            If drRow.Length <= 0 Then
warning:        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please check atleast one employee to migrate."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            Dim blnFlag As Boolean = False
            Dim blnPendingFlag As Boolean = False
            If dgOldApproverEmp.RowCount <= 0 Then Exit Sub
            Dim dtMigratedEmployee As DataTable = Nothing

            If drRow.Length > 0 Then
                Dim dtGetExistData As DataTable = Nothing
                For i As Integer = 0 To drRow.Length - 1

                    If dvNewApprovAssignEmp IsNot Nothing AndAlso dvNewApprovAssignEmp.Table.Rows.Count > 0 Then
                        Dim dRow() As DataRow = dvNewApprovAssignEmp.Table.Select("employeeunkid=" & drRow(i)("employeeunkid").ToString())
                        If dRow.Length > 0 Then
                            blnFlag = True
                            If dtGetExistData Is Nothing Then
                                dtGetExistData = dvNewApprovAssignEmp.Table().Clone
                            End If
                            dtGetExistData.ImportRow(drRow(i))
                            Continue For
                        End If
                    End If

                    drRow(i)("select") = False

                    If CInt(drRow(i)("employeeunkid")) = CInt(cboNewApprover.SelectedValue) Then Continue For

                    If dvNewApprovMigratedEmp Is Nothing Then
                        dtMigratedEmployee = dvOldApproveEmp.Table().Clone
                        dtMigratedEmployee.ImportRow(drRow(i))
                        dvNewApprovMigratedEmp = dtMigratedEmployee.DefaultView
                    Else
                        dvNewApprovMigratedEmp.Table.ImportRow(drRow(i))
                    End If

                    dvOldApproveEmp.Table.Rows.Remove(drRow(i))
                Next


                objchkOldCheck.Checked = False

                If blnFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Some of the checked employees are already linked with the selected approver."), enMsgBoxStyle.Information)
                    dtGetExistData.Rows.Clear()
                    dtGetExistData = Nothing
                End If

                FillNewApproverMigratedEmployeeList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnUnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnUnAssign.Click
        Try

            If dvNewApprovMigratedEmp Is Nothing Then
                GoTo Warning
            End If

            Dim drRow() As DataRow = dvNewApprovMigratedEmp.Table.Select("select = true")

            If drRow.Length <= 0 Then
Warning:        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please check atleast one employee to unassign."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If dgNewApproverMigratedEmp.RowCount <= 0 Then Exit Sub
            Dim dtOldApproverEmployee As DataTable = Nothing
            If drRow.Length > 0 Then
                For i As Integer = 0 To drRow.Length - 1

                    drRow(i)("select") = False

                    If dvOldApproveEmp Is Nothing Then
                        dtOldApproverEmployee = dvNewApprovMigratedEmp.Table().Clone
                        dtOldApproverEmployee.ImportRow(drRow(i))
                        dvOldApproveEmp = dtOldApproverEmployee.DefaultView
                    Else
                        dvOldApproveEmp.Table.ImportRow(drRow(i))
                    End If

                    dvNewApprovMigratedEmp.Table.Rows.Remove(drRow(i))
                Next
                dvOldApproveEmp.Table.AcceptChanges()
                objchkNewCheck.Checked = False
                FillOldApproverEmployeeList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnUnAssign_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please Select From Approver to migrate employee(s)."), enMsgBoxStyle.Information)
                cboOldApprover.Select()
                Exit Sub

            ElseIf CInt(cboOldLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Level to migrate employee(s)."), enMsgBoxStyle.Information)
                cboOldLevel.Select()
                Exit Sub

            ElseIf CInt(cboNewApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select To Approver to migrate employee(s)."), enMsgBoxStyle.Information)
                cboNewApprover.Select()
                Exit Sub

            ElseIf CInt(cboNewLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Level to migrate employee(s)."), enMsgBoxStyle.Information)
                cboNewLevel.Select()
                Exit Sub

            ElseIf dgNewApproverMigratedEmp.RowCount = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "There is no employee(s) for migration."), enMsgBoxStyle.Information)
                dgNewApproverMigratedEmp.Focus()
                Exit Sub

            End If


            Dim objApproverTran As New clsLoanApprover_tran
            objApproverTran._UserId = User._Object._Userunkid

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objApproverTran._FormName = mstrModuleName
            objApproverTran._LoginEmployeeunkid = 0
            objApproverTran._ClientIP = getIP()
            objApproverTran._HostName = getHostName()
            objApproverTran._FromWeb = False
            objApproverTran._AuditUserId = User._Object._Userunkid
objApproverTran._CompanyUnkid = Company._Object._Companyunkid
            objApproverTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objApproverTran.Approver_Migration(dvNewApprovMigratedEmp.Table(), CInt(cboNewApprover.Tag), CInt(cboNewApprover.SelectedValue)) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Problem in Approver Migration."), enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Approver Migration done successfully."), enMsgBoxStyle.Information)
                cboOldApprover.SelectedIndex = 0
                cboOldLevel.SelectedIndex = 0
                cboNewApprover.SelectedIndex = 0
                cboNewLevel.SelectedIndex = 0
                cboOldApprover.Select()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "ComboBox Event"

    Private Sub cboOldApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOldApprover.SelectedIndexChanged
        Try
            Dim dtTable As DataTable = Nothing

            If CInt(cboOldApprover.SelectedValue) > 0 Then
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'dtTable = New DataView(CType(cboOldApprover.DataSource, DataTable), "lnempapproverunkid <> " & CInt(cboOldApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable(True, "lnempapproverunkid", "name")
                Dim strApproverUnkids As String = ""
                strApproverUnkids = objApprover.GetApproverUnkid(CInt(cboOldApprover.SelectedValue), CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isexternalapprover")))

                'Pinkal (11-Dec-2017) -- Start
                'Bug - Solved Loan Approver Migration Issue .
                'dtTable = New DataView(CType(cboOldApprover.DataSource, DataTable), "lnapproverunkid NOT IN(" & strApproverUnkids & ") ", "", DataViewRowState.CurrentRows).ToTable()
                dtTable = New DataView(dsCombo.Tables(0), "lnapproverunkid NOT IN(" & strApproverUnkids & ") ", "", DataViewRowState.CurrentRows).ToTable()
                'Pinkal (11-Dec-2017) -- End

                'Nilay (01-Mar-2016) -- End
            Else
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'dtTable = New DataView(CType(cboOldApprover.DataSource, DataTable), "", "", DataViewRowState.CurrentRows).ToTable(True, "lnempapproverunkid", "name")
                dtTable = New DataView(CType(cboOldApprover.DataSource, DataTable), "", "", DataViewRowState.CurrentRows).ToTable()
                'Nilay (01-Mar-2016) -- End
            End If


            'Pinkal (11-Dec-2017) -- Start
            'Bug - Solved Loan Approver Migration Issue .
            dtTable = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "lnempapproverunkid", "name", "isexternalapprover")
            'Pinkal (11-Dec-2017) -- End



            cboNewApprover.ValueMember = "lnempapproverunkid"
            cboNewApprover.DisplayMember = "name"
            cboNewApprover.DataSource = dtTable

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'GetLoanApproverLevel(sender, CInt(cboOldApprover.SelectedValue))
            GetLoanApproverLevel(sender, CInt(cboOldApprover.SelectedValue), CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
            'Nilay (01-Mar-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOldApprover_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNewApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNewApprover.SelectedIndexChanged
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'GetLoanApproverLevel(sender, CInt(cboNewApprover.SelectedValue))
            If CInt(cboNewApprover.SelectedValue) > 0 Then
                GetLoanApproverLevel(sender, CInt(cboNewApprover.SelectedValue), CBool(CType(cboNewApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
            End If

            'Nilay (01-Mar-2016) -- End

            'Dim dsNewApproverEmp As DataSet = objApprover.GetEmployeeFromLoanApprover(CInt(cboNewApprover.SelectedValue), -1)
            'dvNewApprovAssignEmp = dsNewApproverEmp.Tables(0).DefaultView
            'objchkNewCheck.Checked = False

            'If CInt(cboNewApprover.SelectedValue) <= 0 Then
            '    If dvNewApprovMigratedEmp IsNot Nothing Then dvNewApprovMigratedEmp.Table.Rows.Clear()
            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboNewApprover_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboOldLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOldLevel.SelectedIndexChanged
        Try
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsOldApproverEmp As DataSet = objApprover.GetEmployeeFromLoanApprover(CInt(cboOldApprover.SelectedValue), CInt(cboOldLevel.SelectedValue))

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'Dim dsOldApproverEmp As DataSet = objApprover.GetEmployeeFromLoanApprover(FinancialYear._Object._DatabaseName, _
            '                                                                          User._Object._Userunkid, _
            '                                                                          FinancialYear._Object._YearUnkid, _
            '                                                                          Company._Object._Companyunkid, _
            '                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                                          ConfigParameter._Object._UserAccessModeSetting, _
            '                                                                          True, _
            '                                                                          ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                                                          "List", _
            '                                                                          CInt(cboOldApprover.SelectedValue), _
            '                                                                          CInt(cboOldLevel.SelectedValue))

            Dim dsOldApproverEmp As DataSet = objApprover.GetEmployeeFromLoanApprover(FinancialYear._Object._DatabaseName, _
                                                                                      User._Object._Userunkid, _
                                                                                      FinancialYear._Object._YearUnkid, _
                                                                                      Company._Object._Companyunkid, _
                                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                      ConfigParameter._Object._UserAccessModeSetting, _
                                                                                      True, _
                                                                                      ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                                      "List", _
                                                                                      CInt(cboOldApprover.SelectedValue), _
                                                                                      CInt(cboOldLevel.SelectedValue), _
                                                                                      CBool(CType(cboOldApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
            'Nilay (01-Mar-2016) -- End

            'Nilay (10-Oct-2015) -- End

            dvOldApproveEmp = dsOldApproverEmp.Tables(0).DefaultView
            dvOldApproveEmp.RowFilter = mstrAdvanceFilter
            objchkOldCheck.Checked = False
            FillOldApproverEmployeeList()

            If dsOldApproverEmp IsNot Nothing AndAlso dsOldApproverEmp.Tables(0).Rows.Count > 0 Then
                cboOldApprover.Tag = CInt(dsOldApproverEmp.Tables(0).Rows(0)("lnapproverunkid"))
            Else
                cboOldApprover.Tag = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOldLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNewLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNewLevel.SelectedIndexChanged
        Try
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsNewApproverEmp As DataSet = objApprover.GetEmployeeFromLoanApprover(CInt(cboNewApprover.SelectedValue), CInt(cboNewLevel.SelectedValue))

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'Dim dsNewApproverEmp As DataSet = objApprover.GetEmployeeFromLoanApprover(FinancialYear._Object._DatabaseName, _
            '                                                                          User._Object._Userunkid, _
            '                                                                          FinancialYear._Object._YearUnkid, _
            '                                                                          Company._Object._Companyunkid, _
            '                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                                                                          ConfigParameter._Object._UserAccessModeSetting, _
            '                                                                          True, _
            '                                                                          ConfigParameter._Object._IsIncludeInactiveEmp, _
            '                                                                          "List", _
            '                                                                          CInt(cboNewApprover.SelectedValue), _
            '                                                                          CInt(cboNewLevel.SelectedValue))
            'If CInt(cboNewApprover.SelectedValue) <= 0 Then
            '    If dvNewApprovMigratedEmp IsNot Nothing Then dvNewApprovMigratedEmp.Table.Rows.Clear()
            'End If
            'dvNewApprovAssignEmp = dsNewApproverEmp.Tables(0).DefaultView
            'FillNewApproverAssignEmployeeList()

            'If dsNewApproverEmp IsNot Nothing AndAlso dsNewApproverEmp.Tables(0).Rows.Count > 0 Then
            '    cboNewApprover.Tag = CInt(dsNewApproverEmp.Tables(0).Rows(0)("lnapproverunkid"))
            'Else
            '    cboNewApprover.Tag = Nothing
            'End If

            If CInt(cboNewApprover.SelectedValue) <= 0 Then
                If dvNewApprovMigratedEmp IsNot Nothing Then dvNewApprovMigratedEmp.Table.Rows.Clear()
            Else
                Dim dsNewApproverEmp As DataSet = objApprover.GetEmployeeFromLoanApprover(FinancialYear._Object._DatabaseName, _
                                                                                          User._Object._Userunkid, _
                                                                                          FinancialYear._Object._YearUnkid, _
                                                                                          Company._Object._Companyunkid, _
                                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                          ConfigParameter._Object._UserAccessModeSetting, _
                                                                                          True, _
                                                                                          ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                                          "List", _
                                                                                          CInt(cboNewApprover.SelectedValue), _
                                                                                          CInt(cboNewLevel.SelectedValue), _
                                                                                          CBool(CType(cboNewApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
                dvNewApprovAssignEmp = dsNewApproverEmp.Tables(0).DefaultView
                FillNewApproverAssignEmployeeList()

                If dsNewApproverEmp IsNot Nothing AndAlso dsNewApproverEmp.Tables(0).Rows.Count > 0 Then
                    cboNewApprover.Tag = CInt(dsNewApproverEmp.Tables(0).Rows(0)("lnapproverunkid"))
                Else
                    cboNewApprover.Tag = Nothing

                End If
            End If
            'Nilay (01-Mar-2016) -- End
            'Nilay (10-Oct-2015) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboNewLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub objchkOldCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkOldCheck.CheckedChanged
        Try
            If dvOldApproveEmp.Table.Rows.Count > 0 Then

                'Pinkal (11-Dec-2017) -- Start
                'Bug - Solved Loan Approver Migration Issue .
                'For Each dr As DataRow In dvOldApproveEmp.Table.Rows
                '    RemoveHandler dgOldApproverEmp.CellContentClick, AddressOf dgOldApproverEmp_CellContentClick
                '    dr("select") = objchkOldCheck.Checked
                '    AddHandler dgOldApproverEmp.CellContentClick, AddressOf dgOldApproverEmp_CellContentClick
                'Next
                For Each dr As DataRowView In dvOldApproveEmp
                    RemoveHandler dgOldApproverEmp.CellContentClick, AddressOf dgOldApproverEmp_CellContentClick
                    dr("select") = objchkOldCheck.Checked
                    AddHandler dgOldApproverEmp.CellContentClick, AddressOf dgOldApproverEmp_CellContentClick
                Next
                'Pinkal (11-Dec-2017) -- End
                dvOldApproveEmp.Table.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkOldCheck_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkNewCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkNewCheck.CheckedChanged
        Try
            If dvNewApprovMigratedEmp.Table.Rows.Count > 0 Then
                For Each dr As DataRow In dvNewApprovMigratedEmp.Table.Rows
                    RemoveHandler dgNewApproverMigratedEmp.CellContentClick, AddressOf dgNewApproverMigratedEmp_CellContentClick
                    dr("select") = objchkNewCheck.Checked
                    AddHandler dgNewApproverMigratedEmp.CellContentClick, AddressOf dgNewApproverMigratedEmp_CellContentClick
                Next
                dvNewApprovMigratedEmp.Table.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkNewCheck_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGridView Event"

    Private Sub dgOldApproverEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgOldApproverEmp.CellContentClick, dgOldApproverEmp.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objcolhdgSelect.Index Then

                If dgOldApproverEmp.IsCurrentCellDirty Then
                    dgOldApproverEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                dvOldApproveEmp.Table.AcceptChanges()

                SetCheckBoxOldApproverValue()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgOldApproverEmp_CellContentClick", mstrModuleName)
        End Try

    End Sub

    Private Sub dgNewApproverMigratedEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgNewApproverMigratedEmp.CellContentClick, dgNewApproverMigratedEmp.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objColhdgMigratedSelect.Index Then

                If dgNewApproverMigratedEmp.IsCurrentCellDirty Then
                    dgNewApproverMigratedEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                SetCheckBoxNewApproverValue()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgNewApproverMigratedEmp_CellContentClick", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "TextBox Event"

    Private Sub txtOldSearchEmployee_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOldSearchEmployee.TextChanged
        Try
            If dvOldApproveEmp Is Nothing Then Exit Sub
            dvOldApproveEmp.RowFilter = "(employeecode like '%" & txtOldSearchEmployee.Text.Trim & "%' or employeename like '%" & txtOldSearchEmployee.Text.Trim & "%')"
            If mstrAdvanceFilter.Trim.Length > 0 Then
                dvOldApproveEmp.RowFilter &= " AND " & mstrAdvanceFilter.Trim
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtOldSearchEmployee_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtNewApproverAssignEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewApproverAssignEmp.TextChanged
        Try
            If dvNewApprovAssignEmp Is Nothing Then Exit Sub
            dvNewApprovAssignEmp.RowFilter = "employeecode like '%" & txtNewApproverAssignEmp.Text.Trim & "%' or employeename like '%" & txtNewApproverAssignEmp.Text.Trim & "%'"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtNewApproverAssignEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtNewApproverMigratedEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewApproverMigratedEmp.TextChanged
        Try
            If dvNewApprovMigratedEmp Is Nothing Then Exit Sub
            dvNewApprovMigratedEmp.RowFilter = "employeecode like '%" & txtNewApproverMigratedEmp.Text.Trim & "%' or employeename like '%" & txtNewApproverMigratedEmp.Text.Trim & "%'"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtNewApproverMigratedEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Linkbutton Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            If CInt(cboOldApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "From Approver is compulsory information.Please select From Approver."), enMsgBoxStyle.Information)
                cboOldApprover.Select()
                Exit Sub
            End If
            If CInt(cboOldLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Level is compulsory information.Please select Level."), enMsgBoxStyle.Information)
                cboOldLevel.Select()
                Exit Sub
            End If

            Dim frm As New frmAdvanceSearch

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then

                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'mstrAdvanceFilter = frm._GetFilterString
                frm._Hr_EmployeeTable_Alias = "hremployee_master"
                mstrAdvanceFilter = frm._GetFilterString
                If mstrAdvanceFilter.Trim.Length > 0 Then
                    mstrAdvanceFilter = mstrAdvanceFilter.Replace("ADF.", "")
                End If
                'Nilay (01-Mar-2016) -- End

            End If
            dvOldApproveEmp.RowFilter = mstrAdvanceFilter
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAssign.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnUnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnUnAssign.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.lblCaption1.Text = Language._Object.getCaption(Me.lblCaption1.Name, Me.lblCaption1.Text)
            Me.lblNewLevel.Text = Language._Object.getCaption(Me.lblNewLevel.Name, Me.lblNewLevel.Text)
            Me.lblNewApprover.Text = Language._Object.getCaption(Me.lblNewApprover.Name, Me.lblNewApprover.Text)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.colhdgEmployeecode.HeaderText = Language._Object.getCaption(Me.colhdgEmployeecode.Name, Me.colhdgEmployeecode.HeaderText)
            Me.colhdgEmployee.HeaderText = Language._Object.getCaption(Me.colhdgEmployee.Name, Me.colhdgEmployee.HeaderText)
            Me.tbpMigratedEmployee.Text = Language._Object.getCaption(Me.tbpMigratedEmployee.Name, Me.tbpMigratedEmployee.Text)
            Me.tbpAssignEmployee.Text = Language._Object.getCaption(Me.tbpAssignEmployee.Name, Me.tbpAssignEmployee.Text)
            Me.colhdgAssignEmpCode.HeaderText = Language._Object.getCaption(Me.colhdgAssignEmpCode.Name, Me.colhdgAssignEmpCode.HeaderText)
            Me.colhdgAssignEmployee.HeaderText = Language._Object.getCaption(Me.colhdgAssignEmployee.Name, Me.colhdgAssignEmployee.HeaderText)
            Me.colhdgMigratedEmpCode.HeaderText = Language._Object.getCaption(Me.colhdgMigratedEmpCode.Name, Me.colhdgMigratedEmpCode.HeaderText)
            Me.colhdgMigratedEmp.HeaderText = Language._Object.getCaption(Me.colhdgMigratedEmp.Name, Me.colhdgMigratedEmp.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Please Select To Approver to migrate employee(s).")
            Language.setMessage(mstrModuleName, 3, "Please Select Level to migrate employee(s).")
            Language.setMessage(mstrModuleName, 4, "Please check atleast one employee to migrate.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Some of the checked employees are already linked with the selected approver.")
            Language.setMessage(mstrModuleName, 6, "Please check atleast one employee to unassign.")
            Language.setMessage(mstrModuleName, 7, "Problem in Approver Migration.")
            Language.setMessage(mstrModuleName, 8, "Approver Migration done successfully.")
            Language.setMessage(mstrModuleName, 9, "Please Select From Approver to migrate employee(s).")
            Language.setMessage(mstrModuleName, 10, "There is no employee(s) for migration.")
            Language.setMessage(mstrModuleName, 11, "From Approver is compulsory information.Please select From Approver.")
            Language.setMessage(mstrModuleName, 12, "Level is compulsory information.Please select Level.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class