﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.clsPayment_tran

Public Class frmLoanStatus_AddEdit

#Region " Private Variable "
    Private ReadOnly mstrModuleName As String = "frmLoanStatus_AddEdit"
    Private mblnIsFromLoan As Boolean = False
    Private mintTranUnkid As Integer = -1
    Private mblnCancel As Boolean = True
    Private objLoan_Advance As clsLoan_Advance
    Private objEmployee As clsEmployee_Master
    Private objLoanScheme As clsLoan_Scheme
    Private objStatusTran As clsLoan_Status_tran
    Private mstrStatusData As String = String.Empty
    Private mintStatus As Integer
    'Nilay (25-Mar-2016) -- Start
    Private mdtStartDate As Date
    Private mdtEndDate As Date
    'Nilay (25-Mar-2016) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal blnIsLoan As Boolean, Optional ByVal intStatus As Integer = 0) As Boolean

        mintTranUnkid = intUnkId
        mblnIsFromLoan = blnIsLoan
        mintStatus = intStatus

        Me.ShowDialog()

        intUnkId = mintTranUnkid

        Return Not mblnCancel
    End Function
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            txtEmployeeName.BackColor = GUI.ColorComp
            txtLoanScheme.BackColor = GUI.ColorComp
            txtRemarks.BackColor = GUI.ColorOptional
            txtSettlementAmt.BackColor = GUI.ColorOptional
            cboStatus.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objMaster As New clsMasterData
        Try
            objLoan_Advance._Loanadvancetranunkid = mintTranUnkid 'Sohail (07 May 2015)
            'If mblnIsFromLoan = True Then
            dsCombos = objMaster.GetLoan_Saving_Status("Status")
            If objLoan_Advance._Balance_Amount > 0 Then
                dsCombos.Tables(0).Rows.RemoveAt(4)
            End If
            'End If
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Status")
                .SelectedValue = 0
            End With

            Dim objPeriod As New clscommom_period_Tran
            'SHANI (07 JUL 2015) -- Start
            Dim mdtTable As DataTable
            'SHANI (07 JUL 2015) -- End 

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            Dim dsList As DataSet = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              FinancialYear._Object._DatabaseName, _
                                                              FinancialYear._Object._Database_Start_Date, _
                                                              "Period", True, enStatusType.Open)
            'Nilay (10-Oct-2015) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open, FinancialYear._Object._YearUnkid)
            Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'S.SANDEEP [04 JUN 2015] -- END

            If intFirstPeriodId > 0 Then
                mdtTable = New DataView(dsList.Tables("Period"), "periodunkid = " & intFirstPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                mdtTable = New DataView(dsList.Tables("Period"), "1=2", "", DataViewRowState.CurrentRows).ToTable
            End If
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                'SHANI (07 JUL 2015) -- Start
                '.DataSource = dsList.Tables(0)
                '.SelectedValue = 0
                .DataSource = mdtTable
                .SelectedValue = intFirstPeriodId
                'SHANI (07 JUL 2015) -- End 

            End With
            objPeriod = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Info()
        Try
            txtEmployeeName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " "
            txtLoanScheme.Text = objLoanScheme._Name
            cboStatus.SelectedValue = objLoan_Advance._LoanStatus

            'SHANI (07 JUL 2015) -- Start
            'txtBalance.Text = Format(objLoan_Advance._Balance_Amount, GUI.fmtCurrency) 'LAstprojectBalnce

            Dim objPeriod As New clscommom_period_Tran
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Nilay (10-Oct-2015) -- End

            'Nilay (25-Mar-2016) -- Start
            'Dim dsTable As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo("List", objPeriod._Start_Date.Date, objLoan_Advance._Employeeunkid.ToString, mintTranUnkid, objPeriod._Start_Date, False, True)
            Dim dsTable As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo(FinancialYear._Object._DatabaseName, _
                                                                               User._Object._Userunkid, _
                                                                               FinancialYear._Object._YearUnkid, _
                                                                               Company._Object._Companyunkid, _
                                                                               objPeriod._Start_Date, _
                                                                               objPeriod._End_Date, _
                                                                               ConfigParameter._Object._UserAccessModeSetting, _
                                                                               True, "List", objPeriod._Start_Date.Date, _
                                                                               objLoan_Advance._Employeeunkid.ToString, mintTranUnkid, _
                                                                               objPeriod._Start_Date, False, True)
            'Nilay (25-Mar-2016) -- End

            If dsTable.Tables("List").Rows.Count > 0 Then
                txtBalance.Text = Format(dsTable.Tables(0).Rows(0)("LastProjectedBalance"), GUI.fmtCurrency)
            End If
            objPeriod = Nothing

            'SHANI (07 JUL 2015) -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Info", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'Nilay (25-Mar-2016) -- Start
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            mdtStartDate = objPeriod._Start_Date
            mdtEndDate = objPeriod._End_Date
            'Nilay (25-Mar-2016) -- End

            objStatusTran._Isvoid = False
            objStatusTran._Loanadvancetranunkid = objLoan_Advance._Loanadvancetranunkid
            objStatusTran._Remark = txtRemarks.Text
            If txtSettlementAmt.Text.Trim <> "" Then
                objStatusTran._Settle_Amount = txtSettlementAmt.Decimal
            Else
                objStatusTran._Settle_Amount = 0
            End If

            'Pinkal (04-Mar-2015) -- Start
            'Enhancement - REDESIGN LOAN MODULE.
            'objStatusTran._Staus_Date = dtpDate.Value
            objStatusTran._Userunkid = User._Object._Userunkid
            objStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
            objStatusTran._Staus_Date = ConfigParameter._Object._CurrentDateAndTime
            'Pinkal (04-Mar-2015) -- End
            objStatusTran._Voiddatetime = Nothing
            objStatusTran._Voiduserunkid = -1
            objStatusTran._Statusunkid = CInt(cboStatus.SelectedValue)
            'SHANI (07 JUL 2015) -- Start
            objStatusTran._IsCalculateInterest = chkCalInterest.Checked
            'SHANI (07 JUL 2015) -- End 

            'Please Add
            mstrStatusData = objStatusTran._Isvoid & "|" & _
                                       objStatusTran._Loanadvancetranunkid & "|" & _
                                       objStatusTran._Remark & "|" & _
                                       objStatusTran._Settle_Amount & "|" & _
                                       objStatusTran._Staus_Date & "|" & _
                                       objStatusTran._Voiddatetime & "|" & _
                                       objStatusTran._Voiduserunkid & "|" & _
                                       objStatusTran._Statusunkid & "|" & _
                                       objStatusTran._Periodunkid & "|" 'S.SANDEEP [15 JUN 2015] -- START  {objStatusTran._Periodunkid} -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Period is compulsory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False

            ElseIf CInt(cboStatus.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Status is compulsory information. Please select Status to continue."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False

            ElseIf CInt(cboStatus.SelectedValue) = mintStatus Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, This status is already set. Please select different status."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False
            
            End If

            Dim dtTable As DataTable = CType(dgvLoanAdvanceStatushistory.DataSource, DataTable)
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drPeriod As DataRowView = CType(cboPeriod.SelectedItem, DataRowView)
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                'If eZeeDate.convertDate(drPeriod("start_date").ToString()).Date < CDate(dtTable.Rows(0)("end_date")).Date Then
                If eZeeDate.convertDate(drPeriod("start_date").ToString()).Date < CDate(dtTable.Rows(0)("start_date")).Date Then
                    'Sohail (07 May 2015) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Loan/Advance status period should be greater than or equal to the Last Loan/Advance status period. Please set Loan/Advance status period greater than or equal to the the Last Loan/Advance status period."), enMsgBoxStyle.Information)
                    cboPeriod.Select()
                    Exit Function
                End If
            End If

            Select Case CInt(cboStatus.SelectedValue)
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'Case 3, 4
                Case enLoanStatus.WRITTEN_OFF, enLoanStatus.COMPLETED
                    'Nilay (20-Sept-2016) -- End
                    If txtRemarks.Text.Trim = "" Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Remark cannot be blank. Remark is compulsory information."), enMsgBoxStyle.Information)
                        txtRemarks.Focus()
                        Return False
                    End If
                    lnkAddSettlement.Enabled = False
            End Select

            'SHANI (07 JUL 2015) -- Start
            Dim objTnaLeaveTran As New clsTnALeaveTran
            Dim objPeriod As New clscommom_period_Tran

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Nilay (10-Oct-2015) -- End

            If objTnaLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), objLoan_Advance._Employeeunkid.ToString, objPeriod._End_Date) Then
                'Sohail (19 Apr 2019) -- Start
                'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot change loan status for this employee loan. Reason:Process Payroll is already done for this employee for last date of selected period."), enMsgBoxStyle.Information)
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot change loan status for this employee loan. Reason:Process Payroll is already done for this employee for last date of selected period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 22, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objFrm As New frmProcessPayroll
                    objFrm.ShowDialog()
                End If
                'Sohail (19 Apr 2019) -- End
                objTnaLeaveTran = Nothing : objPeriod = Nothing
                Return False
            End If

            objTnaLeaveTran = Nothing

            Dim ds As DataSet = objLoan_Advance.GetLastLoanBalance("List", mintTranUnkid)
            Dim strMsg As String = ""
            If ds.Tables("List").Rows.Count > 0 Then
                If ds.Tables("List").Rows.Count > 0 AndAlso ds.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(objPeriod._Start_Date.Date) Then
                    If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                        strMsg = Language.getMessage(mstrModuleName, 13, " Process Payroll.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
                        strMsg = Language.getMessage(mstrModuleName, 14, " Loan Payment List.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
                        strMsg = Language.getMessage(mstrModuleName, 15, " Receipt Payment List.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                        strMsg = Language.getMessage(mstrModuleName, 16, " Loan Installment Change.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                        strMsg = Language.getMessage(mstrModuleName, 17, " Loan Topup Added.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                        strMsg = Language.getMessage(mstrModuleName, 18, " Loan Rate Change.")
                    End If
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, You cannot change loan status. Please delete last transaction from the screen of") & strMsg, enMsgBoxStyle.Information)
                    objPeriod = Nothing
                    Return False
                End If
            End If
            objPeriod = Nothing
            'SHANI (07 JUL 2015) -- End 

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub FillHistory()
        Try
            Dim dsList As DataSet = objStatusTran.GetPeriodWiseLoanStatusHistory("List", True, "loanadvancetranunkid = " & mintTranUnkid)

            dgvLoanAdvanceStatushistory.AutoGenerateColumns = False
            objdgcolhIsGroup.DataPropertyName = "grp"
            dgcolhUser.DataPropertyName = "username"
            dgcolhTransactionDate.DataPropertyName = "status_date"
            dgcolhStatus.DataPropertyName = "status"
            objLoanAdvancestatustranunkid.DataPropertyName = "loanadvancetranunkid"
            objdgcolhPeriodStatusId.DataPropertyName = "statusid"
            'SHANI (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            dgcolhCalculateInterest.DataPropertyName = "iscalculateinterest"
            'SHANI (07 May 2015) -- End
            dgvLoanAdvanceStatushistory.DataSource = dsList.Tables(0)
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            If dsList.Tables(0).Rows.Count > 1 Then
                mintStatus = CInt(dsList.Tables(0).Rows(1).Item("statusunkid"))
            End If
            'Sohail (07 May 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillHistory", mstrModuleName)
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray
            dgvcsHeader.Font = New Font(Me.Font.Name, Me.Font.Size, FontStyle.Bold)

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.ForeColor = Color.Black
            dgvcsChild.BackColor = Color.White

            'SHANI (07 JUL 2015) -- Start
            Dim pCell As New clsMergeCell
            'SHANI (07 JUL 2015) -- End 

            For i As Integer = 0 To dgvLoanAdvanceStatushistory.RowCount - 1
                If CBool(dgvLoanAdvanceStatushistory.Rows(i).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    'SHANI (07 JUL 2015) -- Start
                    pCell.MakeMerge(dgvLoanAdvanceStatushistory, i, 0, 0, Color.Gray, Color.Gray, Nothing, "")
                    'SHANI (07 JUL 2015) -- End 

                    'SHANI (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    pCell.MakeMerge(dgvLoanAdvanceStatushistory, i, dgcolhCalculateInterest.Index, dgcolhCalculateInterest.Index, Color.Gray, Color.Gray, Nothing, "")
                    'SHANI (07 May 2015) -- End

                    dgvLoanAdvanceStatushistory.Rows(i).DefaultCellStyle = dgvcsHeader

                Else
                    If CInt(dgvLoanAdvanceStatushistory.Rows(i).Cells(objdgcolhPeriodStatusId.Index).Value) = enStatusType.Close Then
                        pCell.MakeMerge(dgvLoanAdvanceStatushistory, i, 0, 0, Color.White, Color.White, Nothing, "")
                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    Else
                        If i > 1 Then
                            pCell.MakeMerge(dgvLoanAdvanceStatushistory, i, 0, 0, Color.White, Color.White, Nothing, "")
                        End If
                        'Sohail (07 May 2015) -- End
                    End If
                    dgvLoanAdvanceStatushistory.Rows(i).DefaultCellStyle = dgvcsChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmLoanStatus_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLoan_Advance = New clsLoan_Advance
        objEmployee = New clsEmployee_Master
        objLoanScheme = New clsLoan_Scheme
        objStatusTran = New clsLoan_Status_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            SetColor()
            lnkAddSettlement.Enabled = False
            objLoan_Advance._Loanadvancetranunkid = mintTranUnkid

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = objLoan_Advance._Employeeunkid
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLoan_Advance._Employeeunkid
            'S.SANDEEP [04 JUN 2015] -- END

            objLoanScheme._Loanschemeunkid = objLoan_Advance._Loanschemeunkid
            Call Fill_Info()
            FillHistory()
            cboPeriod.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanStatus_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmStatus_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objLoan_Advance = Nothing
    End Sub

    Private Sub frmStatus_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmStatus_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsLoan_Advance.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Advance"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Try
            Dim objFrm As New frmCommonSearch
            With objFrm
                .ValueMember = cboPeriod.ValueMember
                .DisplayMember = cboPeriod.DisplayMember
                .DataSource = CType(cboPeriod.DataSource, DataTable)
            End With
            If objFrm.DisplayDialog Then
                cboPeriod.SelectedValue = objFrm.SelectedValue
                cboPeriod.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValid() = False Then Exit Sub

            Select Case CInt(cboStatus.SelectedValue)
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'Case 3, 4
                Case enLoanStatus.WRITTEN_OFF, enLoanStatus.COMPLETED
                    'Nilay (20-Sept-2016) -- End
                    Dim objPayment As New clsPayment_tran

                    Dim ePRef As enPaymentRefId
                    If mblnIsFromLoan = True Then
                        ePRef = enPaymentRefId.LOAN
                    Else
                        ePRef = enPaymentRefId.ADVANCE
                    End If

                    If objPayment.IsPaymentDone(enPayTypeId.PAYMENT, mintTranUnkid, ePRef) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, you cannot set the selected status for this transaction. Reason : Payment is not made to employee for this transaction."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    objPayment = Nothing
                    'S.SANDEEP [01 JUN 2015] -- START
                    'If txtSettlementAmt.Text.Trim = "" Then
                    If txtSettlementAmt.Decimal = 0 Then
                        'S.SANDEEP [01 JUN 2015] -- END

                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                        'If CInt(cboStatus.SelectedValue) = 3 Then
                        If CInt(cboStatus.SelectedValue) = enLoanStatus.WRITTEN_OFF Then
                            'Nilay (20-Sept-2016) -- End
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "You are about to ") & CStr(cboStatus.Text) & _
                                               Language.getMessage(mstrModuleName, 3, "selected transaction. Do you want to settle it?"), _
                                               CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                lnkAddSettlement.Focus()
                                Exit Sub
                            End If
                            'Nilay (20-Sept-2016) -- Start
                            'Enhancement : Cancel feature for approved but not assigned loan application
                            'ElseIf CInt(cboStatus.SelectedValue) = 4 Then
                        ElseIf CInt(cboStatus.SelectedValue) = enLoanStatus.COMPLETED Then
                            'Nilay (20-Sept-2016) -- End
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot set the selected status for this transaction. Reason : Loan/Advance is not settled."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
            End Select

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objStatusTran._FormName = mstrModuleName
            objStatusTran._LoginEmployeeunkid = 0
            objStatusTran._ClientIP = getIP()
            objStatusTran._HostName = getHostName()
            objStatusTran._FromWeb = False
            objStatusTran._AuditUserId = User._Object._Userunkid
objStatusTran._CompanyUnkid = Company._Object._Companyunkid
            objStatusTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'blnFlag = objStatusTran.Insert

            'Nilay (25-Mar-2016) -- Start
            'blnFlag = objStatusTran.Insert(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, , True)
            blnFlag = objStatusTran.Insert(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           mdtStartDate, _
                                           mdtEndDate, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, , True)
            'Nilay (25-Mar-2016) -- End

            'Sohail (15 Dec 2015) -- End

            If blnFlag Then
                objLoan_Advance._LoanStatus = CInt(cboStatus.SelectedValue)

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objLoan_Advance._FormName = mstrModuleName
                objLoan_Advance._LoginEmployeeUnkid = 0
                objLoan_Advance._ClientIP = getIP()
                objLoan_Advance._HostName = getHostName()
                objLoan_Advance._FromWeb = False
                objLoan_Advance._AuditUserId = User._Object._Userunkid
objLoan_Advance._CompanyUnkid = Company._Object._Companyunkid
                objLoan_Advance._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objLoan_Advance.Update()
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Linkbutton Events "

    Private Sub lnkAddSettlement_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAddSettlement.LinkClicked
        Dim frm As New frmPayment_AddEdit
        Dim mintPaymentTranId As Integer = -1
        Dim objPayment_Tran As New clsPayment_tran
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Call SetValue()
            frm._Loan_Status_data = mstrStatusData
            frm._IsFrom_Status = True
            If frm.displayDialog(mintPaymentTranId, enAction.ADD_ONE, CInt(IIf(mblnIsFromLoan = True, enPaymentRefId.LOAN, enPaymentRefId.ADVANCE)), mintTranUnkid, enPayTypeId.RECEIVED) Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAddSettlement_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextBox Events"

    Private Sub txtRemarks_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemarks.TextChanged
        Try
            If txtRemarks.Text.Trim = "" Then
                lnkAddSettlement.Enabled = False
            Else
                Select Case CInt(cboStatus.SelectedValue)
                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'Case 3, 4
                    '    lnkAddSettlement.Enabled = True
                    'Case 0, 1, 2
                    '    lnkAddSettlement.Enabled = False
                    Case enLoanStatus.WRITTEN_OFF, enLoanStatus.COMPLETED
                        lnkAddSettlement.Enabled = True
                    Case 0, enLoanStatus.IN_PROGRESS, enLoanStatus.ON_HOLD
                        lnkAddSettlement.Enabled = False
                        'Nilay (20-Sept-2016) -- End
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemarks_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGrid Event"

    Private Sub dgvLoanAdvanceStatushistory_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvLoanAdvanceStatushistory.DataBindingComplete
        Try
            Call SetGridColor()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvLoanAdvanceStatushistory_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub

    'SHANI (07 JUL 2015) -- Start
    Private Sub dgvLoanAdvanceStatushistory_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvLoanAdvanceStatushistory.DataError

    End Sub

    Private Sub dgvLoanAdvanceStatushistory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLoanAdvanceStatushistory.CellClick
        Try

            If e.ColumnIndex = objColDelete.Index Then
                If CBool(dgvLoanAdvanceStatushistory.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True Then Exit Sub

                If CInt(dgvLoanAdvanceStatushistory.Rows(e.RowIndex).Cells(objdgcolhPeriodStatusId.Index).Value) = enStatusType.Close Then Exit Sub
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                If CInt(dgvLoanAdvanceStatushistory.Rows(e.RowIndex).Cells(objdgcolhPeriodStatusId.Index).Value) = enStatusType.Open AndAlso e.RowIndex > 1 Then Exit Sub
                'Sohail (07 May 2015) -- End

                Dim dtRow As DataRow = CType(dgvLoanAdvanceStatushistory.DataSource, DataTable).Rows(e.RowIndex)
                If eZeeDate.convertDate(objLoan_Advance._Effective_Date) = eZeeDate.convertDate(CDate(dtRow("status_date"))) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot delete this loan status. Reason : This is system generated  loan status."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim objTnaLeaveTran As New clsTnALeaveTran
                If objTnaLeaveTran.IsPayrollProcessDone(CInt(dtRow("periodunkid")), objLoan_Advance._Employeeunkid.ToString, CDate(dtRow("end_date"))) Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot delete this loan status for this employee loan. Reason:Process payroll is already done for this employee for last date of selected period."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot delete this loan status for this employee loan. Reason:Process payroll is already done for this employee for last date of selected period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 22, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    objTnaLeaveTran = Nothing
                    Exit Sub
                End If
                objTnaLeaveTran = Nothing

                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Are you sure you want to delete this Loan Status?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                    Dim ds As DataSet = objLoan_Advance.GetLastLoanBalance("List", mintTranUnkid)
                    Dim strMsg As String = String.Empty
                    If ds.Tables("List").Rows.Count > 0 AndAlso ds.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(CDate(dtRow("start_date")).Date) Then
                        If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 13, " Process Payroll.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
                            strMsg = Language.getMessage(mstrModuleName, 14, " Loan Payment List.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
                            strMsg = Language.getMessage(mstrModuleName, 15, " Receipt Payment List.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 16, " Loan Installment Change.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 17, " Loan Topup Added.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 18, " Loan Rate Change.")
                        End If
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, You cannot delete this transaction. Please delete last transaction from the screen of") & strMsg, enMsgBoxStyle.Information)
                        Exit Sub
                    End If

                    objStatusTran._Isvoid = True
                    Dim frm As New frmReasonSelection
                    Dim mstrVoidReason As String = String.Empty
                    frm.displayDialog(enVoidCategoryType.LOAN, mstrVoidReason)
                    If mstrVoidReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objStatusTran._Voidreason = mstrVoidReason
                    End If
                    frm = Nothing
                    objStatusTran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objStatusTran._Voiduserunkid = User._Object._Userunkid
                    With objStatusTran
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    objStatusTran.DeleteByLoanStatusUnkId(CInt(dtRow("loanstatustranunkid")))

                    If objStatusTran._Message <> "" Then
                        eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
                    Else
                        Call FillHistory()

                        'Nilay (15-Dec-2015) -- Start
                        'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                        btnSave.Enabled = False
                        'Nilay (15-Dec-2015) -- End

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvLoanAdvanceStatushistory_CellClick", mstrModuleName)
        End Try
    End Sub

    'SHANI (07 JUL 2015) -- End 

#End Region

    'SHANI (07 JUL 2015) -- Start
    '
#Region "Combobox Event(S)"

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboStatus.SelectedValue) = enLoanStatus.ON_HOLD Then
                chkCalInterest.Enabled = True
                lnkAddSettlement.Enabled = False
                chkCalInterest.Checked = False
            ElseIf CInt(cboStatus.SelectedValue) = enLoanStatus.IN_PROGRESS Then
                chkCalInterest.Enabled = False
                lnkAddSettlement.Enabled = False
                chkCalInterest.Checked = False
            Else
                If txtRemarks.Text.Trim.Length > 0 Then lnkAddSettlement.Enabled = True
                chkCalInterest.Enabled = False
                chkCalInterest.Checked = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboStatus_SelectedIndexChanged : ", mstrModuleName)
        End Try
    End Sub

#End Region
    'SHANI (07 JUL 2015) -- End 


    
   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbLoanStatusInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLoanStatusInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbLoanStatusInfo.Text = Language._Object.getCaption(Me.gbLoanStatusInfo.Name, Me.gbLoanStatusInfo.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
			Me.lblSettlementAmt.Text = Language._Object.getCaption(Me.lblSettlementAmt.Name, Me.lblSettlementAmt.Text)
			Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.Name, Me.lblBalance.Text)
			Me.lnkAddSettlement.Text = Language._Object.getCaption(Me.lnkAddSettlement.Name, Me.lnkAddSettlement.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.chkCalInterest.Text = Language._Object.getCaption(Me.chkCalInterest.Name, Me.chkCalInterest.Text)
			Me.dgcolhUser.HeaderText = Language._Object.getCaption(Me.dgcolhUser.Name, Me.dgcolhUser.HeaderText)
			Me.dgcolhTransactionDate.HeaderText = Language._Object.getCaption(Me.dgcolhTransactionDate.Name, Me.dgcolhTransactionDate.HeaderText)
			Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
			Me.dgcolhCalculateInterest.HeaderText = Language._Object.getCaption(Me.dgcolhCalculateInterest.Name, Me.dgcolhCalculateInterest.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, you cannot set the selected status for this transaction. Reason : Payment is not made to employee for this transaction.")
			Language.setMessage(mstrModuleName, 2, "You are about to")
			Language.setMessage(mstrModuleName, 3, "selected transaction. Do you want to settle it?")
			Language.setMessage(mstrModuleName, 4, "Remark cannot be blank. Remark is compulsory information.")
			Language.setMessage(mstrModuleName, 5, "Status is compulsory information. Please select Status to continue.")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot set the selected status for this transaction. Reason : Loan/Advance is not settled.")
			Language.setMessage(mstrModuleName, 7, "Period is compulsory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 8, "Sorry, This status is already set. Please select different status.")
			Language.setMessage(mstrModuleName, 9, "Loan/Advance status period should be greater than or equal to the Last Loan/Advance status period. Please set Loan/Advance status period greater than or equal to the the Last Loan/Advance status period.")
			Language.setMessage(mstrModuleName, 10, "Sorry, you cannot delete this loan status. Reason : This is system generated  loan status.")
			Language.setMessage(mstrModuleName, 11, "Sorry, you cannot change loan status for this employee loan. Reason:Process Payroll is already done for this employee for last date of selected period.")
			Language.setMessage(mstrModuleName, 12, "Sorry, You cannot delete this transaction. Please delete last transaction from the screen of")
			Language.setMessage(mstrModuleName, 13, " Process Payroll.")
			Language.setMessage(mstrModuleName, 14, " Loan Payment List.")
			Language.setMessage(mstrModuleName, 15, " Receipt Payment List.")
			Language.setMessage(mstrModuleName, 16, " Loan Installment Change.")
			Language.setMessage(mstrModuleName, 17, " Loan Topup Added.")
			Language.setMessage(mstrModuleName, 18, " Loan Rate Change.")
			Language.setMessage(mstrModuleName, 19, "Sorry, You cannot change loan status. Please delete last transaction from the screen of")
			Language.setMessage(mstrModuleName, 20, "Are you sure you want to delete this Loan Status?")
			Language.setMessage(mstrModuleName, 21, "Sorry, you cannot delete this loan status for this employee loan. Reason:Process payroll is already done for this employee for last date of selected period.")
			Language.setMessage(mstrModuleName, 22, "Do you want to void Payroll?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class