﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanApproverAddEdit
    Inherits eZee.Common.eZeeForm
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanApproverAddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbAssignedEmployee = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAData = New System.Windows.Forms.Panel
        Me.tblpAssessor = New System.Windows.Forms.TableLayoutPanel
        Me.txtAssignedEmpSearch = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlAssessor = New System.Windows.Forms.Panel
        Me.objchkAssignEmp = New System.Windows.Forms.CheckBox
        Me.dgvAssignedEmp = New System.Windows.Forms.DataGridView
        Me.objdgcolhaCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhaCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhaEmp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhaDepartment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhaJob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhaEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAMasterId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhATranId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnDeleteA = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkExternalApprover = New System.Windows.Forms.CheckBox
        Me.lnkMapLoanScheme = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchLevel = New eZee.Common.eZeeGradientButton
        Me.cboLoanApproveLevel = New System.Windows.Forms.ComboBox
        Me.lblApproveLevel = New System.Windows.Forms.Label
        Me.btnAdd = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchEmp = New eZee.TextBox.AlphanumericTextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvAEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnSearchUser = New eZee.Common.eZeeGradientButton
        Me.objAlloacationReset = New eZee.Common.eZeeGradientButton
        Me.lblLoanApproverName = New System.Windows.Forms.Label
        Me.cboUser = New System.Windows.Forms.ComboBox
        Me.LblUser = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.gbAssignedEmployee.SuspendLayout()
        Me.pnlAData.SuspendLayout()
        Me.tblpAssessor.SuspendLayout()
        Me.objpnlAssessor.SuspendLayout()
        CType(Me.dgvAssignedEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbInfo.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbAssignedEmployee)
        Me.pnlMainInfo.Controls.Add(Me.gbInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Location = New System.Drawing.Point(1, 2)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(948, 528)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbAssignedEmployee
        '
        Me.gbAssignedEmployee.BorderColor = System.Drawing.Color.Black
        Me.gbAssignedEmployee.Checked = False
        Me.gbAssignedEmployee.CollapseAllExceptThis = False
        Me.gbAssignedEmployee.CollapsedHoverImage = Nothing
        Me.gbAssignedEmployee.CollapsedNormalImage = Nothing
        Me.gbAssignedEmployee.CollapsedPressedImage = Nothing
        Me.gbAssignedEmployee.CollapseOnLoad = False
        Me.gbAssignedEmployee.Controls.Add(Me.pnlAData)
        Me.gbAssignedEmployee.ExpandedHoverImage = Nothing
        Me.gbAssignedEmployee.ExpandedNormalImage = Nothing
        Me.gbAssignedEmployee.ExpandedPressedImage = Nothing
        Me.gbAssignedEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssignedEmployee.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssignedEmployee.HeaderHeight = 25
        Me.gbAssignedEmployee.HeaderMessage = ""
        Me.gbAssignedEmployee.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAssignedEmployee.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssignedEmployee.HeightOnCollapse = 0
        Me.gbAssignedEmployee.LeftTextSpace = 0
        Me.gbAssignedEmployee.Location = New System.Drawing.Point(342, 3)
        Me.gbAssignedEmployee.Name = "gbAssignedEmployee"
        Me.gbAssignedEmployee.OpenHeight = 300
        Me.gbAssignedEmployee.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssignedEmployee.ShowBorder = True
        Me.gbAssignedEmployee.ShowCheckBox = False
        Me.gbAssignedEmployee.ShowCollapseButton = False
        Me.gbAssignedEmployee.ShowDefaultBorderColor = True
        Me.gbAssignedEmployee.ShowDownButton = False
        Me.gbAssignedEmployee.ShowHeader = True
        Me.gbAssignedEmployee.Size = New System.Drawing.Size(604, 474)
        Me.gbAssignedEmployee.TabIndex = 8
        Me.gbAssignedEmployee.Temp = 0
        Me.gbAssignedEmployee.Text = "Assigned Employee"
        Me.gbAssignedEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAData
        '
        Me.pnlAData.Controls.Add(Me.tblpAssessor)
        Me.pnlAData.Controls.Add(Me.btnDeleteA)
        Me.pnlAData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlAData.Location = New System.Drawing.Point(2, 26)
        Me.pnlAData.Name = "pnlAData"
        Me.pnlAData.Size = New System.Drawing.Size(600, 446)
        Me.pnlAData.TabIndex = 306
        '
        'tblpAssessor
        '
        Me.tblpAssessor.ColumnCount = 1
        Me.tblpAssessor.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessor.Controls.Add(Me.txtAssignedEmpSearch, 0, 0)
        Me.tblpAssessor.Controls.Add(Me.objpnlAssessor, 0, 1)
        Me.tblpAssessor.Dock = System.Windows.Forms.DockStyle.Top
        Me.tblpAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessor.Location = New System.Drawing.Point(0, 0)
        Me.tblpAssessor.Name = "tblpAssessor"
        Me.tblpAssessor.RowCount = 2
        Me.tblpAssessor.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessor.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessor.Size = New System.Drawing.Size(600, 401)
        Me.tblpAssessor.TabIndex = 304
        '
        'txtAssignedEmpSearch
        '
        Me.txtAssignedEmpSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtAssignedEmpSearch.Flags = 0
        Me.txtAssignedEmpSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtAssignedEmpSearch.Location = New System.Drawing.Point(3, 3)
        Me.txtAssignedEmpSearch.Name = "txtAssignedEmpSearch"
        Me.txtAssignedEmpSearch.Size = New System.Drawing.Size(594, 21)
        Me.txtAssignedEmpSearch.TabIndex = 6
        '
        'objpnlAssessor
        '
        Me.objpnlAssessor.Controls.Add(Me.objchkAssignEmp)
        Me.objpnlAssessor.Controls.Add(Me.dgvAssignedEmp)
        Me.objpnlAssessor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlAssessor.Location = New System.Drawing.Point(3, 29)
        Me.objpnlAssessor.Name = "objpnlAssessor"
        Me.objpnlAssessor.Size = New System.Drawing.Size(594, 369)
        Me.objpnlAssessor.TabIndex = 107
        '
        'objchkAssignEmp
        '
        Me.objchkAssignEmp.AutoSize = True
        Me.objchkAssignEmp.Location = New System.Drawing.Point(7, 5)
        Me.objchkAssignEmp.Name = "objchkAssignEmp"
        Me.objchkAssignEmp.Size = New System.Drawing.Size(15, 14)
        Me.objchkAssignEmp.TabIndex = 104
        Me.objchkAssignEmp.UseVisualStyleBackColor = True
        '
        'dgvAssignedEmp
        '
        Me.dgvAssignedEmp.AllowUserToAddRows = False
        Me.dgvAssignedEmp.AllowUserToDeleteRows = False
        Me.dgvAssignedEmp.AllowUserToResizeColumns = False
        Me.dgvAssignedEmp.AllowUserToResizeRows = False
        Me.dgvAssignedEmp.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAssignedEmp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAssignedEmp.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAssignedEmp.ColumnHeadersHeight = 21
        Me.dgvAssignedEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAssignedEmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhaCheck, Me.dgcolhaCode, Me.dgcolhaEmp, Me.dgcolhaDepartment, Me.dgcolhaJob, Me.objdgcolhaEmpId, Me.objdgcolhAMasterId, Me.objdgcolhATranId})
        Me.dgvAssignedEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAssignedEmp.Location = New System.Drawing.Point(0, 0)
        Me.dgvAssignedEmp.MultiSelect = False
        Me.dgvAssignedEmp.Name = "dgvAssignedEmp"
        Me.dgvAssignedEmp.RowHeadersVisible = False
        Me.dgvAssignedEmp.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAssignedEmp.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAssignedEmp.Size = New System.Drawing.Size(594, 369)
        Me.dgvAssignedEmp.TabIndex = 105
        '
        'objdgcolhaCheck
        '
        Me.objdgcolhaCheck.HeaderText = ""
        Me.objdgcolhaCheck.Name = "objdgcolhaCheck"
        Me.objdgcolhaCheck.Width = 25
        '
        'dgcolhaCode
        '
        Me.dgcolhaCode.HeaderText = "Code"
        Me.dgcolhaCode.Name = "dgcolhaCode"
        Me.dgcolhaCode.ReadOnly = True
        Me.dgcolhaCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhaCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhaCode.Width = 70
        '
        'dgcolhaEmp
        '
        Me.dgcolhaEmp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhaEmp.HeaderText = "Employee"
        Me.dgcolhaEmp.Name = "dgcolhaEmp"
        Me.dgcolhaEmp.ReadOnly = True
        Me.dgcolhaEmp.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhaEmp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhaDepartment
        '
        Me.dgcolhaDepartment.HeaderText = "Department"
        Me.dgcolhaDepartment.Name = "dgcolhaDepartment"
        '
        'dgcolhaJob
        '
        Me.dgcolhaJob.HeaderText = "Job"
        Me.dgcolhaJob.Name = "dgcolhaJob"
        Me.dgcolhaJob.Width = 150
        '
        'objdgcolhaEmpId
        '
        Me.objdgcolhaEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhaEmpId.Name = "objdgcolhaEmpId"
        Me.objdgcolhaEmpId.Visible = False
        '
        'objdgcolhAMasterId
        '
        Me.objdgcolhAMasterId.HeaderText = "objdgcolhAMasterId"
        Me.objdgcolhAMasterId.Name = "objdgcolhAMasterId"
        Me.objdgcolhAMasterId.Visible = False
        '
        'objdgcolhATranId
        '
        Me.objdgcolhATranId.HeaderText = "objdgcolhATranId"
        Me.objdgcolhATranId.Name = "objdgcolhATranId"
        Me.objdgcolhATranId.Visible = False
        '
        'btnDeleteA
        '
        Me.btnDeleteA.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteA.BackColor = System.Drawing.Color.White
        Me.btnDeleteA.BackgroundImage = CType(resources.GetObject("btnDeleteA.BackgroundImage"), System.Drawing.Image)
        Me.btnDeleteA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeleteA.BorderColor = System.Drawing.Color.Empty
        Me.btnDeleteA.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDeleteA.FlatAppearance.BorderSize = 0
        Me.btnDeleteA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteA.ForeColor = System.Drawing.Color.Black
        Me.btnDeleteA.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeleteA.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteA.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteA.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteA.Location = New System.Drawing.Point(497, 411)
        Me.btnDeleteA.Name = "btnDeleteA"
        Me.btnDeleteA.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeleteA.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeleteA.Size = New System.Drawing.Size(97, 29)
        Me.btnDeleteA.TabIndex = 7
        Me.btnDeleteA.Text = "&Delete"
        Me.btnDeleteA.UseVisualStyleBackColor = True
        '
        'gbInfo
        '
        Me.gbInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInfo.Checked = False
        Me.gbInfo.CollapseAllExceptThis = False
        Me.gbInfo.CollapsedHoverImage = Nothing
        Me.gbInfo.CollapsedNormalImage = Nothing
        Me.gbInfo.CollapsedPressedImage = Nothing
        Me.gbInfo.CollapseOnLoad = False
        Me.gbInfo.Controls.Add(Me.chkExternalApprover)
        Me.gbInfo.Controls.Add(Me.lnkMapLoanScheme)
        Me.gbInfo.Controls.Add(Me.objbtnSearchLevel)
        Me.gbInfo.Controls.Add(Me.cboLoanApproveLevel)
        Me.gbInfo.Controls.Add(Me.lblApproveLevel)
        Me.gbInfo.Controls.Add(Me.btnAdd)
        Me.gbInfo.Controls.Add(Me.lnkAllocation)
        Me.gbInfo.Controls.Add(Me.tblpAssessorEmployee)
        Me.gbInfo.Controls.Add(Me.objbtnSearchUser)
        Me.gbInfo.Controls.Add(Me.objAlloacationReset)
        Me.gbInfo.Controls.Add(Me.lblLoanApproverName)
        Me.gbInfo.Controls.Add(Me.cboUser)
        Me.gbInfo.Controls.Add(Me.LblUser)
        Me.gbInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbInfo.Controls.Add(Me.txtName)
        Me.gbInfo.ExpandedHoverImage = Nothing
        Me.gbInfo.ExpandedNormalImage = Nothing
        Me.gbInfo.ExpandedPressedImage = Nothing
        Me.gbInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInfo.HeaderHeight = 25
        Me.gbInfo.HeaderMessage = ""
        Me.gbInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInfo.HeightOnCollapse = 0
        Me.gbInfo.LeftTextSpace = 0
        Me.gbInfo.Location = New System.Drawing.Point(3, 3)
        Me.gbInfo.Name = "gbInfo"
        Me.gbInfo.OpenHeight = 300
        Me.gbInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInfo.ShowBorder = True
        Me.gbInfo.ShowCheckBox = False
        Me.gbInfo.ShowCollapseButton = False
        Me.gbInfo.ShowDefaultBorderColor = True
        Me.gbInfo.ShowDownButton = False
        Me.gbInfo.ShowHeader = True
        Me.gbInfo.Size = New System.Drawing.Size(333, 474)
        Me.gbInfo.TabIndex = 7
        Me.gbInfo.Temp = 0
        Me.gbInfo.Text = "Loan Approver Information"
        Me.gbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkExternalApprover
        '
        Me.chkExternalApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkExternalApprover.Location = New System.Drawing.Point(102, 34)
        Me.chkExternalApprover.Name = "chkExternalApprover"
        Me.chkExternalApprover.Size = New System.Drawing.Size(221, 17)
        Me.chkExternalApprover.TabIndex = 311
        Me.chkExternalApprover.Text = "Make External Approver"
        Me.chkExternalApprover.UseVisualStyleBackColor = True
        '
        'lnkMapLoanScheme
        '
        Me.lnkMapLoanScheme.BackColor = System.Drawing.Color.Transparent
        Me.lnkMapLoanScheme.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkMapLoanScheme.Location = New System.Drawing.Point(9, 136)
        Me.lnkMapLoanScheme.Margin = New System.Windows.Forms.Padding(0)
        Me.lnkMapLoanScheme.Name = "lnkMapLoanScheme"
        Me.lnkMapLoanScheme.Size = New System.Drawing.Size(111, 15)
        Me.lnkMapLoanScheme.TabIndex = 309
        Me.lnkMapLoanScheme.TabStop = True
        Me.lnkMapLoanScheme.Text = "Map Loan Scheme"
        Me.lnkMapLoanScheme.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchLevel
        '
        Me.objbtnSearchLevel.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLevel.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLevel.BorderSelected = False
        Me.objbtnSearchLevel.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLevel.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLevel.Location = New System.Drawing.Point(302, 83)
        Me.objbtnSearchLevel.Name = "objbtnSearchLevel"
        Me.objbtnSearchLevel.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLevel.TabIndex = 307
        '
        'cboLoanApproveLevel
        '
        Me.cboLoanApproveLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanApproveLevel.DropDownWidth = 200
        Me.cboLoanApproveLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanApproveLevel.FormattingEnabled = True
        Me.cboLoanApproveLevel.Location = New System.Drawing.Point(102, 83)
        Me.cboLoanApproveLevel.Name = "cboLoanApproveLevel"
        Me.cboLoanApproveLevel.Size = New System.Drawing.Size(194, 21)
        Me.cboLoanApproveLevel.TabIndex = 2
        '
        'lblApproveLevel
        '
        Me.lblApproveLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproveLevel.Location = New System.Drawing.Point(11, 85)
        Me.lblApproveLevel.Name = "lblApproveLevel"
        Me.lblApproveLevel.Size = New System.Drawing.Size(85, 16)
        Me.lblApproveLevel.TabIndex = 306
        Me.lblApproveLevel.Text = "Level"
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.BackgroundImage = CType(resources.GetObject("btnAdd.BackgroundImage"), System.Drawing.Image)
        Me.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAdd.BorderColor = System.Drawing.Color.Empty
        Me.btnAdd.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.Black
        Me.btnAdd.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAdd.GradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Location = New System.Drawing.Point(232, 437)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAdd.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAdd.Size = New System.Drawing.Size(98, 29)
        Me.btnAdd.TabIndex = 5
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(218, 136)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(78, 15)
        Me.lnkAllocation.TabIndex = 236
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(7, 161)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(319, 269)
        Me.tblpAssessorEmployee.TabIndex = 303
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchEmp.Flags = 0
        Me.txtSearchEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(313, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.objchkEmployee)
        Me.objpnlEmp.Controls.Add(Me.dgvAEmployee)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(313, 237)
        Me.objpnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvAEmployee
        '
        Me.dgvAEmployee.AllowUserToAddRows = False
        Me.dgvAEmployee.AllowUserToDeleteRows = False
        Me.dgvAEmployee.AllowUserToResizeColumns = False
        Me.dgvAEmployee.AllowUserToResizeRows = False
        Me.dgvAEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAEmployee.ColumnHeadersHeight = 21
        Me.dgvAEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvAEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvAEmployee.MultiSelect = False
        Me.dgvAEmployee.Name = "dgvAEmployee"
        Me.dgvAEmployee.RowHeadersVisible = False
        Me.dgvAEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAEmployee.Size = New System.Drawing.Size(313, 237)
        Me.dgvAEmployee.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'objbtnSearchUser
        '
        Me.objbtnSearchUser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUser.BorderSelected = False
        Me.objbtnSearchUser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUser.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUser.Location = New System.Drawing.Point(302, 109)
        Me.objbtnSearchUser.Name = "objbtnSearchUser"
        Me.objbtnSearchUser.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUser.TabIndex = 302
        '
        'objAlloacationReset
        '
        Me.objAlloacationReset.BackColor = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor1 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BackColor2 = System.Drawing.Color.Transparent
        Me.objAlloacationReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objAlloacationReset.BorderSelected = False
        Me.objAlloacationReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objAlloacationReset.Image = Global.Aruti.Main.My.Resources.Resources.reset_20
        Me.objAlloacationReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objAlloacationReset.Location = New System.Drawing.Point(302, 134)
        Me.objAlloacationReset.Name = "objAlloacationReset"
        Me.objAlloacationReset.Size = New System.Drawing.Size(21, 21)
        Me.objAlloacationReset.TabIndex = 238
        '
        'lblLoanApproverName
        '
        Me.lblLoanApproverName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanApproverName.Location = New System.Drawing.Point(11, 59)
        Me.lblLoanApproverName.Name = "lblLoanApproverName"
        Me.lblLoanApproverName.Size = New System.Drawing.Size(85, 16)
        Me.lblLoanApproverName.TabIndex = 5
        Me.lblLoanApproverName.Text = "Loan Approver"
        Me.lblLoanApproverName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboUser
        '
        Me.cboUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUser.DropDownWidth = 200
        Me.cboUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUser.FormattingEnabled = True
        Me.cboUser.Location = New System.Drawing.Point(102, 109)
        Me.cboUser.Name = "cboUser"
        Me.cboUser.Size = New System.Drawing.Size(194, 21)
        Me.cboUser.TabIndex = 4
        '
        'LblUser
        '
        Me.LblUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUser.Location = New System.Drawing.Point(11, 111)
        Me.LblUser.Name = "LblUser"
        Me.LblUser.Size = New System.Drawing.Size(85, 16)
        Me.LblUser.TabIndex = 301
        Me.LblUser.Text = "User"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(302, 57)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 95
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(102, 57)
        Me.txtName.Name = "txtName"
        Me.txtName.ReadOnly = True
        Me.txtName.Size = New System.Drawing.Size(194, 21)
        Me.txtName.TabIndex = 1
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 478)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(948, 50)
        Me.objFooter.TabIndex = 6
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(736, 10)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 8
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(841, 10)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 9
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Department"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Job"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 150
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhAMasterId"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objdgcolhATranId"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Width = 70
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn9.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'frmLoanApproverAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(950, 531)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanApproverAddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Loan Approver"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbAssignedEmployee.ResumeLayout(False)
        Me.pnlAData.ResumeLayout(False)
        Me.tblpAssessor.ResumeLayout(False)
        Me.tblpAssessor.PerformLayout()
        Me.objpnlAssessor.ResumeLayout(False)
        Me.objpnlAssessor.PerformLayout()
        CType(Me.dgvAssignedEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbInfo.ResumeLayout(False)
        Me.gbInfo.PerformLayout()
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        Me.objpnlEmp.PerformLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchLevel As eZee.Common.eZeeGradientButton
    Friend WithEvents cboLoanApproveLevel As System.Windows.Forms.ComboBox
    Friend WithEvents lblApproveLevel As System.Windows.Forms.Label
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents btnAdd As eZee.Common.eZeeLightButton
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents objAlloacationReset As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchUser As eZee.Common.eZeeGradientButton
    Friend WithEvents lblLoanApproverName As System.Windows.Forms.Label
    Friend WithEvents cboUser As System.Windows.Forms.ComboBox
    Friend WithEvents LblUser As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSearchEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvAEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbAssignedEmployee As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAData As System.Windows.Forms.Panel
    Friend WithEvents tblpAssessor As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtAssignedEmpSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objpnlAssessor As System.Windows.Forms.Panel
    Friend WithEvents objchkAssignEmp As System.Windows.Forms.CheckBox
    Friend WithEvents dgvAssignedEmp As System.Windows.Forms.DataGridView
    Friend WithEvents btnDeleteA As eZee.Common.eZeeLightButton
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhaCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhaCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhaEmp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhaDepartment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhaJob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhaEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAMasterId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhATranId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lnkMapLoanScheme As System.Windows.Forms.LinkLabel
    Friend WithEvents chkExternalApprover As System.Windows.Forms.CheckBox
End Class
