﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewLoanAdvance_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNewLoanAdvance_AddEdit))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.tabcLoanInformation = New System.Windows.Forms.TabControl
        Me.tabpInterestInfo = New System.Windows.Forms.TabPage
        Me.dgvInterestHistory = New System.Windows.Forms.DataGridView
        Me.objdgcolhIAdd = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhIEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhIDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhInterestPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInterestEffectiveDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInterestRate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhlninteresttranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIPStatusId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tabpEMIInfo = New System.Windows.Forms.TabPage
        Me.dgvEMIHistory = New System.Windows.Forms.DataGridView
        Me.objdgcolhEAdd = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhEMIPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEMIeffectivedate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhloan_duration = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhInstallmentAmt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEMINoofInstallment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhlnemitranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEPstatusid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tabpTopupInfo = New System.Windows.Forms.TabPage
        Me.dgvTopupHistory = New System.Windows.Forms.DataGridView
        Me.objdgcolhTAdd = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhTEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhTDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhTopupPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhTopupEffectiveDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTopupAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhTopupInterest = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhlntopuptranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhTPstatusid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tabpLoanAdvHistory = New System.Windows.Forms.TabPage
        Me.pnlData = New System.Windows.Forms.Panel
        Me.dgvHistory = New System.Windows.Forms.DataGridView
        Me.dgcolhLoanScheme = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhVoucherNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNetAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRepayment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBalanceAmt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbLoanAdvanceInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboMappedHead = New System.Windows.Forms.ComboBox
        Me.lblMappedHead = New System.Windows.Forms.Label
        Me.cboInterestCalcType = New System.Windows.Forms.ComboBox
        Me.lblInterestCalcType = New System.Windows.Forms.Label
        Me.cboLoanCalcType = New System.Windows.Forms.ComboBox
        Me.lblLoanCalcType = New System.Windows.Forms.Label
        Me.elProjectedINSTLAmt = New eZee.Common.eZeeLine
        Me.pnlInformation = New System.Windows.Forms.Panel
        Me.lblIntAmt = New System.Windows.Forms.Label
        Me.lblPrincipalAmt = New System.Windows.Forms.Label
        Me.txtIntAmt = New eZee.TextBox.NumericTextBox
        Me.txtPrincipalAmt = New eZee.TextBox.NumericTextBox
        Me.txtLoanRate = New eZee.TextBox.NumericTextBox
        Me.lblLoanInterest = New System.Windows.Forms.Label
        Me.lblDuration = New System.Windows.Forms.Label
        Me.nudDuration = New System.Windows.Forms.NumericUpDown
        Me.lblEMIAmount = New System.Windows.Forms.Label
        Me.txtInstallmentAmt = New eZee.TextBox.NumericTextBox
        Me.lblPurpose = New System.Windows.Forms.Label
        Me.txtPurpose = New eZee.TextBox.AlphanumericTextBox
        Me.objlblExRate = New System.Windows.Forms.Label
        Me.lnProjectedAmount = New eZee.Common.eZeeLine
        Me.dtpDate = New System.Windows.Forms.DateTimePicker
        Me.lblEffectiveDate = New System.Windows.Forms.Label
        Me.lblDeductionPeriod = New System.Windows.Forms.Label
        Me.cboDeductionPeriod = New System.Windows.Forms.ComboBox
        Me.pnlProjectedAmt = New System.Windows.Forms.Panel
        Me.lblInterestAmt = New System.Windows.Forms.Label
        Me.txtInterestAmt = New eZee.TextBox.NumericTextBox
        Me.lblNetAmount = New System.Windows.Forms.Label
        Me.txtNetAmount = New eZee.TextBox.NumericTextBox
        Me.fpnlType = New System.Windows.Forms.FlowLayoutPanel
        Me.radLoan = New System.Windows.Forms.RadioButton
        Me.radAdvance = New System.Windows.Forms.RadioButton
        Me.objStLine1 = New eZee.Common.eZeeStraightLine
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.txtLoanAmt = New eZee.TextBox.NumericTextBox
        Me.txtAdvanceAmt = New eZee.TextBox.NumericTextBox
        Me.txtApprovedBy = New eZee.TextBox.AlphanumericTextBox
        Me.cboEmpName = New System.Windows.Forms.ComboBox
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblApprovedBy = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.txtVoucherNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblVoucherNo = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.lblLoanAmt = New System.Windows.Forms.Label
        Me.lblAdvanceAmt = New System.Windows.Forms.Label
        Me.pnlMainInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.tabcLoanInformation.SuspendLayout()
        Me.tabpInterestInfo.SuspendLayout()
        CType(Me.dgvInterestHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpEMIInfo.SuspendLayout()
        CType(Me.dgvEMIHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpTopupInfo.SuspendLayout()
        CType(Me.dgvTopupHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpLoanAdvHistory.SuspendLayout()
        Me.pnlData.SuspendLayout()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbLoanAdvanceInfo.SuspendLayout()
        Me.pnlInformation.SuspendLayout()
        CType(Me.nudDuration, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlProjectedAmt.SuspendLayout()
        Me.fpnlType.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.tabcLoanInformation)
        Me.pnlMainInfo.Controls.Add(Me.gbLoanAdvanceInfo)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(780, 530)
        Me.pnlMainInfo.TabIndex = 0
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 475)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(780, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(568, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(671, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'tabcLoanInformation
        '
        Me.tabcLoanInformation.Controls.Add(Me.tabpInterestInfo)
        Me.tabcLoanInformation.Controls.Add(Me.tabpEMIInfo)
        Me.tabcLoanInformation.Controls.Add(Me.tabpTopupInfo)
        Me.tabcLoanInformation.Controls.Add(Me.tabpLoanAdvHistory)
        Me.tabcLoanInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabcLoanInformation.Location = New System.Drawing.Point(2, 284)
        Me.tabcLoanInformation.Name = "tabcLoanInformation"
        Me.tabcLoanInformation.SelectedIndex = 0
        Me.tabcLoanInformation.Size = New System.Drawing.Size(776, 188)
        Me.tabcLoanInformation.TabIndex = 1
        '
        'tabpInterestInfo
        '
        Me.tabpInterestInfo.Controls.Add(Me.dgvInterestHistory)
        Me.tabpInterestInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabpInterestInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpInterestInfo.Name = "tabpInterestInfo"
        Me.tabpInterestInfo.Size = New System.Drawing.Size(768, 162)
        Me.tabpInterestInfo.TabIndex = 0
        Me.tabpInterestInfo.Tag = "tabpInterestInfo"
        Me.tabpInterestInfo.Text = "Loan Interest Information"
        Me.tabpInterestInfo.UseVisualStyleBackColor = True
        '
        'dgvInterestHistory
        '
        Me.dgvInterestHistory.AllowUserToAddRows = False
        Me.dgvInterestHistory.AllowUserToDeleteRows = False
        Me.dgvInterestHistory.AllowUserToResizeRows = False
        Me.dgvInterestHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvInterestHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvInterestHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvInterestHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhIAdd, Me.objdgcolhIEdit, Me.objdgcolhIDelete, Me.dgcolhInterestPeriod, Me.dgcolhInterestEffectiveDate, Me.dgcolhInterestRate, Me.objdgcolhlninteresttranunkid, Me.objdgcolhIGUID, Me.objdgcolhIPStatusId})
        Me.dgvInterestHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvInterestHistory.Location = New System.Drawing.Point(0, 0)
        Me.dgvInterestHistory.MultiSelect = False
        Me.dgvInterestHistory.Name = "dgvInterestHistory"
        Me.dgvInterestHistory.ReadOnly = True
        Me.dgvInterestHistory.RowHeadersVisible = False
        Me.dgvInterestHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvInterestHistory.Size = New System.Drawing.Size(768, 162)
        Me.dgvInterestHistory.TabIndex = 0
        '
        'objdgcolhIAdd
        '
        Me.objdgcolhIAdd.Frozen = True
        Me.objdgcolhIAdd.HeaderText = ""
        Me.objdgcolhIAdd.Name = "objdgcolhIAdd"
        Me.objdgcolhIAdd.ReadOnly = True
        Me.objdgcolhIAdd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhIAdd.Visible = False
        Me.objdgcolhIAdd.Width = 25
        '
        'objdgcolhIEdit
        '
        Me.objdgcolhIEdit.Frozen = True
        Me.objdgcolhIEdit.HeaderText = ""
        Me.objdgcolhIEdit.Name = "objdgcolhIEdit"
        Me.objdgcolhIEdit.ReadOnly = True
        Me.objdgcolhIEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhIEdit.Visible = False
        Me.objdgcolhIEdit.Width = 25
        '
        'objdgcolhIDelete
        '
        Me.objdgcolhIDelete.Frozen = True
        Me.objdgcolhIDelete.HeaderText = ""
        Me.objdgcolhIDelete.Name = "objdgcolhIDelete"
        Me.objdgcolhIDelete.ReadOnly = True
        Me.objdgcolhIDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhIDelete.Visible = False
        Me.objdgcolhIDelete.Width = 25
        '
        'dgcolhInterestPeriod
        '
        Me.dgcolhInterestPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhInterestPeriod.HeaderText = "Period"
        Me.dgcolhInterestPeriod.Name = "dgcolhInterestPeriod"
        Me.dgcolhInterestPeriod.ReadOnly = True
        Me.dgcolhInterestPeriod.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhInterestEffectiveDate
        '
        Me.dgcolhInterestEffectiveDate.HeaderText = "Effective Date"
        Me.dgcolhInterestEffectiveDate.Name = "dgcolhInterestEffectiveDate"
        Me.dgcolhInterestEffectiveDate.ReadOnly = True
        Me.dgcolhInterestEffectiveDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhInterestEffectiveDate.Width = 110
        '
        'dgcolhInterestRate
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhInterestRate.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhInterestRate.HeaderText = "Interest (%)"
        Me.dgcolhInterestRate.Name = "dgcolhInterestRate"
        Me.dgcolhInterestRate.ReadOnly = True
        Me.dgcolhInterestRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhlninteresttranunkid
        '
        Me.objdgcolhlninteresttranunkid.HeaderText = "objdgcolhlninteresttranunkid"
        Me.objdgcolhlninteresttranunkid.Name = "objdgcolhlninteresttranunkid"
        Me.objdgcolhlninteresttranunkid.ReadOnly = True
        Me.objdgcolhlninteresttranunkid.Visible = False
        '
        'objdgcolhIGUID
        '
        Me.objdgcolhIGUID.HeaderText = "objdgcolhIGUID"
        Me.objdgcolhIGUID.Name = "objdgcolhIGUID"
        Me.objdgcolhIGUID.ReadOnly = True
        Me.objdgcolhIGUID.Visible = False
        '
        'objdgcolhIPStatusId
        '
        Me.objdgcolhIPStatusId.HeaderText = "objdgcolhIPStatusId"
        Me.objdgcolhIPStatusId.Name = "objdgcolhIPStatusId"
        Me.objdgcolhIPStatusId.ReadOnly = True
        Me.objdgcolhIPStatusId.Visible = False
        '
        'tabpEMIInfo
        '
        Me.tabpEMIInfo.Controls.Add(Me.dgvEMIHistory)
        Me.tabpEMIInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpEMIInfo.Name = "tabpEMIInfo"
        Me.tabpEMIInfo.Size = New System.Drawing.Size(768, 162)
        Me.tabpEMIInfo.TabIndex = 1
        Me.tabpEMIInfo.Tag = "tabpEMIInfo"
        Me.tabpEMIInfo.Text = "Loan EMI Information"
        Me.tabpEMIInfo.UseVisualStyleBackColor = True
        '
        'dgvEMIHistory
        '
        Me.dgvEMIHistory.AllowUserToAddRows = False
        Me.dgvEMIHistory.AllowUserToDeleteRows = False
        Me.dgvEMIHistory.AllowUserToResizeRows = False
        Me.dgvEMIHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvEMIHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvEMIHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEMIHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhEAdd, Me.objdgcolhEEdit, Me.objdgcolhEDelete, Me.dgcolhEMIPeriod, Me.dgcolhEMIeffectivedate, Me.dgcolhloan_duration, Me.dgcolhInstallmentAmt, Me.dgcolhEMINoofInstallment, Me.objdgcolhlnemitranunkid, Me.objdgcolhEGUID, Me.objdgcolhEPstatusid})
        Me.dgvEMIHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvEMIHistory.Location = New System.Drawing.Point(0, 0)
        Me.dgvEMIHistory.MultiSelect = False
        Me.dgvEMIHistory.Name = "dgvEMIHistory"
        Me.dgvEMIHistory.ReadOnly = True
        Me.dgvEMIHistory.RowHeadersVisible = False
        Me.dgvEMIHistory.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvEMIHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEMIHistory.Size = New System.Drawing.Size(768, 162)
        Me.dgvEMIHistory.TabIndex = 326
        '
        'objdgcolhEAdd
        '
        Me.objdgcolhEAdd.Frozen = True
        Me.objdgcolhEAdd.HeaderText = ""
        Me.objdgcolhEAdd.Name = "objdgcolhEAdd"
        Me.objdgcolhEAdd.ReadOnly = True
        Me.objdgcolhEAdd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEAdd.Visible = False
        Me.objdgcolhEAdd.Width = 25
        '
        'objdgcolhEEdit
        '
        Me.objdgcolhEEdit.Frozen = True
        Me.objdgcolhEEdit.HeaderText = ""
        Me.objdgcolhEEdit.Name = "objdgcolhEEdit"
        Me.objdgcolhEEdit.ReadOnly = True
        Me.objdgcolhEEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEEdit.Visible = False
        Me.objdgcolhEEdit.Width = 25
        '
        'objdgcolhEDelete
        '
        Me.objdgcolhEDelete.Frozen = True
        Me.objdgcolhEDelete.HeaderText = ""
        Me.objdgcolhEDelete.Name = "objdgcolhEDelete"
        Me.objdgcolhEDelete.ReadOnly = True
        Me.objdgcolhEDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEDelete.Visible = False
        Me.objdgcolhEDelete.Width = 25
        '
        'dgcolhEMIPeriod
        '
        Me.dgcolhEMIPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhEMIPeriod.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhEMIPeriod.FillWeight = 369.5431!
        Me.dgcolhEMIPeriod.HeaderText = "Period"
        Me.dgcolhEMIPeriod.Name = "dgcolhEMIPeriod"
        Me.dgcolhEMIPeriod.ReadOnly = True
        Me.dgcolhEMIPeriod.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhEMIeffectivedate
        '
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhEMIeffectivedate.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhEMIeffectivedate.FillWeight = 17.73779!
        Me.dgcolhEMIeffectivedate.HeaderText = "Effective Date"
        Me.dgcolhEMIeffectivedate.Name = "dgcolhEMIeffectivedate"
        Me.dgcolhEMIeffectivedate.ReadOnly = True
        Me.dgcolhEMIeffectivedate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhloan_duration
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhloan_duration.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgcolhloan_duration.HeaderText = "Loan Duration"
        Me.dgcolhloan_duration.Name = "dgcolhloan_duration"
        Me.dgcolhloan_duration.ReadOnly = True
        Me.dgcolhloan_duration.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhloan_duration.Visible = False
        '
        'dgcolhInstallmentAmt
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "F0"
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhInstallmentAmt.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgcolhInstallmentAmt.FillWeight = 3.36817!
        Me.dgcolhInstallmentAmt.HeaderText = "Installment Amt."
        Me.dgcolhInstallmentAmt.Name = "dgcolhInstallmentAmt"
        Me.dgcolhInstallmentAmt.ReadOnly = True
        Me.dgcolhInstallmentAmt.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhInstallmentAmt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhInstallmentAmt.Width = 130
        '
        'dgcolhEMINoofInstallment
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhEMINoofInstallment.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgcolhEMINoofInstallment.FillWeight = 9.350899!
        Me.dgcolhEMINoofInstallment.HeaderText = "No. Of Installments"
        Me.dgcolhEMINoofInstallment.Name = "dgcolhEMINoofInstallment"
        Me.dgcolhEMINoofInstallment.ReadOnly = True
        Me.dgcolhEMINoofInstallment.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEMINoofInstallment.Width = 130
        '
        'objdgcolhlnemitranunkid
        '
        Me.objdgcolhlnemitranunkid.HeaderText = "objdgcolhlnemitranunkid"
        Me.objdgcolhlnemitranunkid.Name = "objdgcolhlnemitranunkid"
        Me.objdgcolhlnemitranunkid.ReadOnly = True
        Me.objdgcolhlnemitranunkid.Visible = False
        '
        'objdgcolhEGUID
        '
        Me.objdgcolhEGUID.HeaderText = "objdgcolhEGUID"
        Me.objdgcolhEGUID.Name = "objdgcolhEGUID"
        Me.objdgcolhEGUID.ReadOnly = True
        Me.objdgcolhEGUID.Visible = False
        '
        'objdgcolhEPstatusid
        '
        Me.objdgcolhEPstatusid.HeaderText = "dgcolhEPstatusid"
        Me.objdgcolhEPstatusid.Name = "objdgcolhEPstatusid"
        Me.objdgcolhEPstatusid.ReadOnly = True
        Me.objdgcolhEPstatusid.Visible = False
        '
        'tabpTopupInfo
        '
        Me.tabpTopupInfo.Controls.Add(Me.dgvTopupHistory)
        Me.tabpTopupInfo.Location = New System.Drawing.Point(4, 22)
        Me.tabpTopupInfo.Name = "tabpTopupInfo"
        Me.tabpTopupInfo.Size = New System.Drawing.Size(768, 162)
        Me.tabpTopupInfo.TabIndex = 2
        Me.tabpTopupInfo.Tag = "tabpTopupInfo"
        Me.tabpTopupInfo.Text = "Loan Topup Information"
        Me.tabpTopupInfo.UseVisualStyleBackColor = True
        '
        'dgvTopupHistory
        '
        Me.dgvTopupHistory.AllowUserToAddRows = False
        Me.dgvTopupHistory.AllowUserToDeleteRows = False
        Me.dgvTopupHistory.AllowUserToResizeRows = False
        Me.dgvTopupHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvTopupHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTopupHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvTopupHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhTAdd, Me.objdgcolhTEdit, Me.objdgcolhTDelete, Me.dgcolhTopupPeriod, Me.dgColhTopupEffectiveDate, Me.dgcolhTopupAmount, Me.dgcolhTopupInterest, Me.objdgcolhlntopuptranunkid, Me.objdgcolhTGUID, Me.objdgcolhTPstatusid})
        Me.dgvTopupHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvTopupHistory.Location = New System.Drawing.Point(0, 0)
        Me.dgvTopupHistory.MultiSelect = False
        Me.dgvTopupHistory.Name = "dgvTopupHistory"
        Me.dgvTopupHistory.ReadOnly = True
        Me.dgvTopupHistory.RowHeadersVisible = False
        Me.dgvTopupHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTopupHistory.Size = New System.Drawing.Size(768, 162)
        Me.dgvTopupHistory.TabIndex = 347
        '
        'objdgcolhTAdd
        '
        Me.objdgcolhTAdd.Frozen = True
        Me.objdgcolhTAdd.HeaderText = ""
        Me.objdgcolhTAdd.Name = "objdgcolhTAdd"
        Me.objdgcolhTAdd.ReadOnly = True
        Me.objdgcolhTAdd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhTAdd.Visible = False
        Me.objdgcolhTAdd.Width = 25
        '
        'objdgcolhTEdit
        '
        Me.objdgcolhTEdit.Frozen = True
        Me.objdgcolhTEdit.HeaderText = ""
        Me.objdgcolhTEdit.Name = "objdgcolhTEdit"
        Me.objdgcolhTEdit.ReadOnly = True
        Me.objdgcolhTEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhTEdit.Visible = False
        Me.objdgcolhTEdit.Width = 25
        '
        'objdgcolhTDelete
        '
        Me.objdgcolhTDelete.Frozen = True
        Me.objdgcolhTDelete.HeaderText = ""
        Me.objdgcolhTDelete.Name = "objdgcolhTDelete"
        Me.objdgcolhTDelete.ReadOnly = True
        Me.objdgcolhTDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhTDelete.Visible = False
        Me.objdgcolhTDelete.Width = 25
        '
        'dgcolhTopupPeriod
        '
        Me.dgcolhTopupPeriod.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.Format = "F0"
        Me.dgcolhTopupPeriod.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgcolhTopupPeriod.HeaderText = "Period"
        Me.dgcolhTopupPeriod.Name = "dgcolhTopupPeriod"
        Me.dgcolhTopupPeriod.ReadOnly = True
        Me.dgcolhTopupPeriod.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgcolhTopupPeriod.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgColhTopupEffectiveDate
        '
        Me.dgColhTopupEffectiveDate.HeaderText = "Effective Date"
        Me.dgColhTopupEffectiveDate.Name = "dgColhTopupEffectiveDate"
        Me.dgColhTopupEffectiveDate.ReadOnly = True
        Me.dgColhTopupEffectiveDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgColhTopupEffectiveDate.Width = 110
        '
        'dgcolhTopupAmount
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhTopupAmount.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgcolhTopupAmount.HeaderText = "Topup Amount"
        Me.dgcolhTopupAmount.Name = "dgcolhTopupAmount"
        Me.dgcolhTopupAmount.ReadOnly = True
        Me.dgcolhTopupAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTopupAmount.Width = 200
        '
        'dgcolhTopupInterest
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhTopupInterest.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgcolhTopupInterest.HeaderText = "Topup Interest"
        Me.dgcolhTopupInterest.Name = "dgcolhTopupInterest"
        Me.dgcolhTopupInterest.ReadOnly = True
        Me.dgcolhTopupInterest.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhTopupInterest.Visible = False
        Me.dgcolhTopupInterest.Width = 120
        '
        'objdgcolhlntopuptranunkid
        '
        Me.objdgcolhlntopuptranunkid.HeaderText = "objdgcolhlntopuptranunkid"
        Me.objdgcolhlntopuptranunkid.Name = "objdgcolhlntopuptranunkid"
        Me.objdgcolhlntopuptranunkid.ReadOnly = True
        Me.objdgcolhlntopuptranunkid.Visible = False
        '
        'objdgcolhTGUID
        '
        Me.objdgcolhTGUID.HeaderText = "objdgcolhTGUID"
        Me.objdgcolhTGUID.Name = "objdgcolhTGUID"
        Me.objdgcolhTGUID.ReadOnly = True
        Me.objdgcolhTGUID.Visible = False
        '
        'objdgcolhTPstatusid
        '
        Me.objdgcolhTPstatusid.HeaderText = "objdgcolhTPstatusid"
        Me.objdgcolhTPstatusid.Name = "objdgcolhTPstatusid"
        Me.objdgcolhTPstatusid.ReadOnly = True
        Me.objdgcolhTPstatusid.Visible = False
        '
        'tabpLoanAdvHistory
        '
        Me.tabpLoanAdvHistory.Controls.Add(Me.pnlData)
        Me.tabpLoanAdvHistory.Location = New System.Drawing.Point(4, 22)
        Me.tabpLoanAdvHistory.Name = "tabpLoanAdvHistory"
        Me.tabpLoanAdvHistory.Size = New System.Drawing.Size(768, 162)
        Me.tabpLoanAdvHistory.TabIndex = 3
        Me.tabpLoanAdvHistory.Tag = "tabpLoanAdvHistory"
        Me.tabpLoanAdvHistory.Text = "Loan/Advance History"
        Me.tabpLoanAdvHistory.UseVisualStyleBackColor = True
        '
        'pnlData
        '
        Me.pnlData.Controls.Add(Me.dgvHistory)
        Me.pnlData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(0, 0)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(768, 162)
        Me.pnlData.TabIndex = 16
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToResizeRows = False
        Me.dgvHistory.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhLoanScheme, Me.dgcolhVoucherNo, Me.dgcolhDate, Me.dgcolhAmount, Me.dgcolhNetAmount, Me.dgcolhRepayment, Me.dgcolhBalanceAmt, Me.dgcolhStatus})
        Me.dgvHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvHistory.Location = New System.Drawing.Point(0, 0)
        Me.dgvHistory.MultiSelect = False
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White
        Me.dgvHistory.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.dgvHistory.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.White
        Me.dgvHistory.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
        Me.dgvHistory.Size = New System.Drawing.Size(768, 162)
        Me.dgvHistory.TabIndex = 0
        '
        'dgcolhLoanScheme
        '
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black
        Me.dgcolhLoanScheme.DefaultCellStyle = DataGridViewCellStyle10
        Me.dgcolhLoanScheme.HeaderText = "Loan Scheme"
        Me.dgcolhLoanScheme.Name = "dgcolhLoanScheme"
        Me.dgcolhLoanScheme.ReadOnly = True
        '
        'dgcolhVoucherNo
        '
        Me.dgcolhVoucherNo.HeaderText = "Voc #"
        Me.dgcolhVoucherNo.Name = "dgcolhVoucherNo"
        Me.dgcolhVoucherNo.ReadOnly = True
        Me.dgcolhVoucherNo.Width = 80
        '
        'dgcolhDate
        '
        Me.dgcolhDate.HeaderText = "Date"
        Me.dgcolhDate.Name = "dgcolhDate"
        Me.dgcolhDate.ReadOnly = True
        Me.dgcolhDate.Width = 80
        '
        'dgcolhAmount
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhAmount.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgcolhAmount.HeaderText = "Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        Me.dgcolhAmount.Width = 105
        '
        'dgcolhNetAmount
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhNetAmount.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgcolhNetAmount.HeaderText = "Net Amount"
        Me.dgcolhNetAmount.Name = "dgcolhNetAmount"
        Me.dgcolhNetAmount.ReadOnly = True
        Me.dgcolhNetAmount.Width = 110
        '
        'dgcolhRepayment
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhRepayment.DefaultCellStyle = DataGridViewCellStyle13
        Me.dgcolhRepayment.HeaderText = "Repayment"
        Me.dgcolhRepayment.Name = "dgcolhRepayment"
        Me.dgcolhRepayment.ReadOnly = True
        '
        'dgcolhBalanceAmt
        '
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhBalanceAmt.DefaultCellStyle = DataGridViewCellStyle14
        Me.dgcolhBalanceAmt.HeaderText = "Balance Amount"
        Me.dgcolhBalanceAmt.Name = "dgcolhBalanceAmt"
        Me.dgcolhBalanceAmt.ReadOnly = True
        Me.dgcolhBalanceAmt.Width = 120
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        Me.dgcolhStatus.Width = 70
        '
        'gbLoanAdvanceInfo
        '
        Me.gbLoanAdvanceInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLoanAdvanceInfo.Checked = False
        Me.gbLoanAdvanceInfo.CollapseAllExceptThis = False
        Me.gbLoanAdvanceInfo.CollapsedHoverImage = Nothing
        Me.gbLoanAdvanceInfo.CollapsedNormalImage = Nothing
        Me.gbLoanAdvanceInfo.CollapsedPressedImage = Nothing
        Me.gbLoanAdvanceInfo.CollapseOnLoad = False
        Me.gbLoanAdvanceInfo.Controls.Add(Me.cboMappedHead)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblMappedHead)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.cboInterestCalcType)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblInterestCalcType)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.cboLoanCalcType)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblLoanCalcType)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.elProjectedINSTLAmt)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.pnlInformation)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblPurpose)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.txtPurpose)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.objlblExRate)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lnProjectedAmount)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.dtpDate)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblEffectiveDate)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblDeductionPeriod)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.cboDeductionPeriod)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.pnlProjectedAmt)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.fpnlType)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.objStLine1)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.cboCurrency)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.txtLoanAmt)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.txtAdvanceAmt)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.txtApprovedBy)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.cboEmpName)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.cboPayPeriod)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblApprovedBy)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblPeriod)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblEmpName)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.txtVoucherNo)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblVoucherNo)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.cboLoanScheme)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblLoanScheme)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblLoanAmt)
        Me.gbLoanAdvanceInfo.Controls.Add(Me.lblAdvanceAmt)
        Me.gbLoanAdvanceInfo.ExpandedHoverImage = Nothing
        Me.gbLoanAdvanceInfo.ExpandedNormalImage = Nothing
        Me.gbLoanAdvanceInfo.ExpandedPressedImage = Nothing
        Me.gbLoanAdvanceInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLoanAdvanceInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLoanAdvanceInfo.HeaderHeight = 25
        Me.gbLoanAdvanceInfo.HeaderMessage = ""
        Me.gbLoanAdvanceInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLoanAdvanceInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLoanAdvanceInfo.HeightOnCollapse = 0
        Me.gbLoanAdvanceInfo.LeftTextSpace = 0
        Me.gbLoanAdvanceInfo.Location = New System.Drawing.Point(2, 2)
        Me.gbLoanAdvanceInfo.Name = "gbLoanAdvanceInfo"
        Me.gbLoanAdvanceInfo.OpenHeight = 300
        Me.gbLoanAdvanceInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLoanAdvanceInfo.ShowBorder = True
        Me.gbLoanAdvanceInfo.ShowCheckBox = False
        Me.gbLoanAdvanceInfo.ShowCollapseButton = False
        Me.gbLoanAdvanceInfo.ShowDefaultBorderColor = True
        Me.gbLoanAdvanceInfo.ShowDownButton = False
        Me.gbLoanAdvanceInfo.ShowHeader = True
        Me.gbLoanAdvanceInfo.Size = New System.Drawing.Size(776, 278)
        Me.gbLoanAdvanceInfo.TabIndex = 0
        Me.gbLoanAdvanceInfo.Temp = 0
        Me.gbLoanAdvanceInfo.Text = "Loan / Advance General Info"
        Me.gbLoanAdvanceInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMappedHead
        '
        Me.cboMappedHead.DropDownWidth = 250
        Me.cboMappedHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMappedHead.FormattingEnabled = True
        Me.cboMappedHead.Location = New System.Drawing.Point(302, 249)
        Me.cboMappedHead.Name = "cboMappedHead"
        Me.cboMappedHead.Size = New System.Drawing.Size(99, 21)
        Me.cboMappedHead.TabIndex = 357
        '
        'lblMappedHead
        '
        Me.lblMappedHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMappedHead.Location = New System.Drawing.Point(302, 224)
        Me.lblMappedHead.Name = "lblMappedHead"
        Me.lblMappedHead.Size = New System.Drawing.Size(108, 15)
        Me.lblMappedHead.TabIndex = 356
        Me.lblMappedHead.Text = "Mapped Head"
        Me.lblMappedHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInterestCalcType
        '
        Me.cboInterestCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterestCalcType.FormattingEnabled = True
        Me.cboInterestCalcType.Location = New System.Drawing.Point(109, 249)
        Me.cboInterestCalcType.Name = "cboInterestCalcType"
        Me.cboInterestCalcType.Size = New System.Drawing.Size(187, 21)
        Me.cboInterestCalcType.TabIndex = 331
        '
        'lblInterestCalcType
        '
        Me.lblInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestCalcType.Location = New System.Drawing.Point(12, 252)
        Me.lblInterestCalcType.Name = "lblInterestCalcType"
        Me.lblInterestCalcType.Size = New System.Drawing.Size(91, 15)
        Me.lblInterestCalcType.TabIndex = 330
        Me.lblInterestCalcType.Text = "Int. Calc. Type"
        Me.lblInterestCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanCalcType
        '
        Me.cboLoanCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanCalcType.DropDownWidth = 250
        Me.cboLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanCalcType.FormattingEnabled = True
        Me.cboLoanCalcType.Location = New System.Drawing.Point(109, 222)
        Me.cboLoanCalcType.Name = "cboLoanCalcType"
        Me.cboLoanCalcType.Size = New System.Drawing.Size(187, 21)
        Me.cboLoanCalcType.TabIndex = 329
        '
        'lblLoanCalcType
        '
        Me.lblLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanCalcType.Location = New System.Drawing.Point(12, 225)
        Me.lblLoanCalcType.Name = "lblLoanCalcType"
        Me.lblLoanCalcType.Size = New System.Drawing.Size(91, 15)
        Me.lblLoanCalcType.TabIndex = 328
        Me.lblLoanCalcType.Text = "Loan Calc. Type"
        Me.lblLoanCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elProjectedINSTLAmt
        '
        Me.elProjectedINSTLAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elProjectedINSTLAmt.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elProjectedINSTLAmt.Location = New System.Drawing.Point(419, 116)
        Me.elProjectedINSTLAmt.Name = "elProjectedINSTLAmt"
        Me.elProjectedINSTLAmt.Size = New System.Drawing.Size(350, 17)
        Me.elProjectedINSTLAmt.TabIndex = 25
        Me.elProjectedINSTLAmt.Text = "Projected Installment Amount (First Installment)"
        '
        'pnlInformation
        '
        Me.pnlInformation.Controls.Add(Me.lblIntAmt)
        Me.pnlInformation.Controls.Add(Me.lblPrincipalAmt)
        Me.pnlInformation.Controls.Add(Me.txtIntAmt)
        Me.pnlInformation.Controls.Add(Me.txtPrincipalAmt)
        Me.pnlInformation.Controls.Add(Me.txtLoanRate)
        Me.pnlInformation.Controls.Add(Me.lblLoanInterest)
        Me.pnlInformation.Controls.Add(Me.lblDuration)
        Me.pnlInformation.Controls.Add(Me.nudDuration)
        Me.pnlInformation.Controls.Add(Me.lblEMIAmount)
        Me.pnlInformation.Controls.Add(Me.txtInstallmentAmt)
        Me.pnlInformation.Location = New System.Drawing.Point(432, 139)
        Me.pnlInformation.Name = "pnlInformation"
        Me.pnlInformation.Size = New System.Drawing.Size(341, 82)
        Me.pnlInformation.TabIndex = 25
        '
        'lblIntAmt
        '
        Me.lblIntAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIntAmt.Location = New System.Drawing.Point(190, 32)
        Me.lblIntAmt.Name = "lblIntAmt"
        Me.lblIntAmt.Size = New System.Drawing.Size(90, 15)
        Me.lblIntAmt.TabIndex = 28
        Me.lblIntAmt.Text = "Interest Amt."
        Me.lblIntAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPrincipalAmt
        '
        Me.lblPrincipalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrincipalAmt.Location = New System.Drawing.Point(3, 32)
        Me.lblPrincipalAmt.Name = "lblPrincipalAmt"
        Me.lblPrincipalAmt.Size = New System.Drawing.Size(90, 15)
        Me.lblPrincipalAmt.TabIndex = 27
        Me.lblPrincipalAmt.Text = "Principal Amt."
        Me.lblPrincipalAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIntAmt
        '
        Me.txtIntAmt.AllowNegative = True
        Me.txtIntAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtIntAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtIntAmt.DigitsInGroup = 3
        Me.txtIntAmt.Flags = 0
        Me.txtIntAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIntAmt.Location = New System.Drawing.Point(286, 29)
        Me.txtIntAmt.MaxDecimalPlaces = 6
        Me.txtIntAmt.MaxWholeDigits = 21
        Me.txtIntAmt.Name = "txtIntAmt"
        Me.txtIntAmt.Prefix = ""
        Me.txtIntAmt.RangeMax = 1.7976931348623157E+308
        Me.txtIntAmt.RangeMin = -1.7976931348623157E+308
        Me.txtIntAmt.ReadOnly = True
        Me.txtIntAmt.Size = New System.Drawing.Size(51, 21)
        Me.txtIntAmt.TabIndex = 26
        Me.txtIntAmt.Text = "0"
        Me.txtIntAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrincipalAmt
        '
        Me.txtPrincipalAmt.AllowNegative = True
        Me.txtPrincipalAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtPrincipalAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPrincipalAmt.DigitsInGroup = 3
        Me.txtPrincipalAmt.Flags = 0
        Me.txtPrincipalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrincipalAmt.Location = New System.Drawing.Point(99, 29)
        Me.txtPrincipalAmt.MaxDecimalPlaces = 6
        Me.txtPrincipalAmt.MaxWholeDigits = 21
        Me.txtPrincipalAmt.Name = "txtPrincipalAmt"
        Me.txtPrincipalAmt.Prefix = ""
        Me.txtPrincipalAmt.RangeMax = 1.7976931348623157E+308
        Me.txtPrincipalAmt.RangeMin = -1.7976931348623157E+308
        Me.txtPrincipalAmt.ReadOnly = True
        Me.txtPrincipalAmt.Size = New System.Drawing.Size(85, 21)
        Me.txtPrincipalAmt.TabIndex = 25
        Me.txtPrincipalAmt.Text = "0"
        Me.txtPrincipalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLoanRate
        '
        Me.txtLoanRate.AllowNegative = True
        Me.txtLoanRate.BackColor = System.Drawing.SystemColors.Window
        Me.txtLoanRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanRate.DigitsInGroup = 0
        Me.txtLoanRate.Flags = 0
        Me.txtLoanRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanRate.Location = New System.Drawing.Point(99, 2)
        Me.txtLoanRate.MaxDecimalPlaces = 6
        Me.txtLoanRate.MaxWholeDigits = 21
        Me.txtLoanRate.Name = "txtLoanRate"
        Me.txtLoanRate.Prefix = ""
        Me.txtLoanRate.RangeMax = 1.7976931348623157E+308
        Me.txtLoanRate.RangeMin = -1.7976931348623157E+308
        Me.txtLoanRate.Size = New System.Drawing.Size(48, 21)
        Me.txtLoanRate.TabIndex = 18
        Me.txtLoanRate.Text = "0"
        Me.txtLoanRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLoanInterest
        '
        Me.lblLoanInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanInterest.Location = New System.Drawing.Point(3, 5)
        Me.lblLoanInterest.Name = "lblLoanInterest"
        Me.lblLoanInterest.Size = New System.Drawing.Size(90, 15)
        Me.lblLoanInterest.TabIndex = 17
        Me.lblLoanInterest.Text = "Rate (%)"
        Me.lblLoanInterest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDuration
        '
        Me.lblDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDuration.Location = New System.Drawing.Point(188, 4)
        Me.lblDuration.Name = "lblDuration"
        Me.lblDuration.Size = New System.Drawing.Size(92, 15)
        Me.lblDuration.TabIndex = 19
        Me.lblDuration.Text = "INSTL(In Months)"
        '
        'nudDuration
        '
        Me.nudDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDuration.Location = New System.Drawing.Point(286, 2)
        Me.nudDuration.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.nudDuration.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudDuration.Name = "nudDuration"
        Me.nudDuration.Size = New System.Drawing.Size(51, 21)
        Me.nudDuration.TabIndex = 20
        Me.nudDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudDuration.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblEMIAmount
        '
        Me.lblEMIAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIAmount.Location = New System.Drawing.Point(3, 59)
        Me.lblEMIAmount.Name = "lblEMIAmount"
        Me.lblEMIAmount.Size = New System.Drawing.Size(90, 15)
        Me.lblEMIAmount.TabIndex = 23
        Me.lblEMIAmount.Text = "INSTL Amt."
        Me.lblEMIAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInstallmentAmt
        '
        Me.txtInstallmentAmt.AllowNegative = True
        Me.txtInstallmentAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtInstallmentAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInstallmentAmt.DigitsInGroup = 3
        Me.txtInstallmentAmt.Flags = 0
        Me.txtInstallmentAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInstallmentAmt.Location = New System.Drawing.Point(99, 56)
        Me.txtInstallmentAmt.MaxDecimalPlaces = 6
        Me.txtInstallmentAmt.MaxWholeDigits = 21
        Me.txtInstallmentAmt.Name = "txtInstallmentAmt"
        Me.txtInstallmentAmt.Prefix = ""
        Me.txtInstallmentAmt.RangeMax = 1.7976931348623157E+308
        Me.txtInstallmentAmt.RangeMin = -1.7976931348623157E+308
        Me.txtInstallmentAmt.Size = New System.Drawing.Size(238, 21)
        Me.txtInstallmentAmt.TabIndex = 24
        Me.txtInstallmentAmt.Text = "0"
        Me.txtInstallmentAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPurpose
        '
        Me.lblPurpose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPurpose.Location = New System.Drawing.Point(432, 231)
        Me.lblPurpose.Name = "lblPurpose"
        Me.lblPurpose.Size = New System.Drawing.Size(90, 15)
        Me.lblPurpose.TabIndex = 24
        Me.lblPurpose.Text = "Loan Purpose"
        '
        'txtPurpose
        '
        Me.txtPurpose.BackColor = System.Drawing.SystemColors.Window
        Me.txtPurpose.Flags = 0
        Me.txtPurpose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPurpose.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPurpose.Location = New System.Drawing.Point(528, 228)
        Me.txtPurpose.Multiline = True
        Me.txtPurpose.Name = "txtPurpose"
        Me.txtPurpose.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtPurpose.Size = New System.Drawing.Size(238, 42)
        Me.txtPurpose.TabIndex = 25
        '
        'objlblExRate
        '
        Me.objlblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExRate.Location = New System.Drawing.Point(232, 198)
        Me.objlblExRate.Name = "objlblExRate"
        Me.objlblExRate.Size = New System.Drawing.Size(181, 15)
        Me.objlblExRate.TabIndex = 326
        Me.objlblExRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnProjectedAmount
        '
        Me.lnProjectedAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnProjectedAmount.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnProjectedAmount.Location = New System.Drawing.Point(419, 36)
        Me.lnProjectedAmount.Name = "lnProjectedAmount"
        Me.lnProjectedAmount.Size = New System.Drawing.Size(350, 17)
        Me.lnProjectedAmount.TabIndex = 22
        Me.lnProjectedAmount.Text = "Projected Amount"
        '
        'dtpDate
        '
        Me.dtpDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Checked = False
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(109, 87)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(102, 21)
        Me.dtpDate.TabIndex = 7
        '
        'lblEffectiveDate
        '
        Me.lblEffectiveDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEffectiveDate.Location = New System.Drawing.Point(12, 90)
        Me.lblEffectiveDate.Name = "lblEffectiveDate"
        Me.lblEffectiveDate.Size = New System.Drawing.Size(91, 15)
        Me.lblEffectiveDate.TabIndex = 6
        Me.lblEffectiveDate.Text = "Effective Date"
        '
        'lblDeductionPeriod
        '
        Me.lblDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeductionPeriod.Location = New System.Drawing.Point(12, 63)
        Me.lblDeductionPeriod.Name = "lblDeductionPeriod"
        Me.lblDeductionPeriod.Size = New System.Drawing.Size(91, 15)
        Me.lblDeductionPeriod.TabIndex = 4
        Me.lblDeductionPeriod.Text = "Deduction Period"
        Me.lblDeductionPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboDeductionPeriod
        '
        Me.cboDeductionPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeductionPeriod.FormattingEnabled = True
        Me.cboDeductionPeriod.Location = New System.Drawing.Point(109, 60)
        Me.cboDeductionPeriod.Name = "cboDeductionPeriod"
        Me.cboDeductionPeriod.Size = New System.Drawing.Size(292, 21)
        Me.cboDeductionPeriod.TabIndex = 5
        '
        'pnlProjectedAmt
        '
        Me.pnlProjectedAmt.Controls.Add(Me.lblInterestAmt)
        Me.pnlProjectedAmt.Controls.Add(Me.txtInterestAmt)
        Me.pnlProjectedAmt.Controls.Add(Me.lblNetAmount)
        Me.pnlProjectedAmt.Controls.Add(Me.txtNetAmount)
        Me.pnlProjectedAmt.Location = New System.Drawing.Point(432, 57)
        Me.pnlProjectedAmt.Name = "pnlProjectedAmt"
        Me.pnlProjectedAmt.Size = New System.Drawing.Size(341, 53)
        Me.pnlProjectedAmt.TabIndex = 23
        '
        'lblInterestAmt
        '
        Me.lblInterestAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestAmt.Location = New System.Drawing.Point(3, 5)
        Me.lblInterestAmt.Name = "lblInterestAmt"
        Me.lblInterestAmt.Size = New System.Drawing.Size(90, 16)
        Me.lblInterestAmt.TabIndex = 0
        Me.lblInterestAmt.Text = "Interest Amount"
        Me.lblInterestAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInterestAmt
        '
        Me.txtInterestAmt.AllowNegative = True
        Me.txtInterestAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInterestAmt.DigitsInGroup = 0
        Me.txtInterestAmt.Flags = 0
        Me.txtInterestAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterestAmt.Location = New System.Drawing.Point(99, 3)
        Me.txtInterestAmt.MaxDecimalPlaces = 6
        Me.txtInterestAmt.MaxWholeDigits = 21
        Me.txtInterestAmt.Name = "txtInterestAmt"
        Me.txtInterestAmt.Prefix = ""
        Me.txtInterestAmt.RangeMax = 1.7976931348623157E+308
        Me.txtInterestAmt.RangeMin = -1.7976931348623157E+308
        Me.txtInterestAmt.ReadOnly = True
        Me.txtInterestAmt.Size = New System.Drawing.Size(237, 21)
        Me.txtInterestAmt.TabIndex = 1
        Me.txtInterestAmt.Text = "0"
        Me.txtInterestAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNetAmount
        '
        Me.lblNetAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetAmount.Location = New System.Drawing.Point(3, 32)
        Me.lblNetAmount.Name = "lblNetAmount"
        Me.lblNetAmount.Size = New System.Drawing.Size(90, 17)
        Me.lblNetAmount.TabIndex = 2
        Me.lblNetAmount.Text = "Net Amount"
        '
        'txtNetAmount
        '
        Me.txtNetAmount.AllowNegative = True
        Me.txtNetAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNetAmount.DigitsInGroup = 0
        Me.txtNetAmount.Flags = 0
        Me.txtNetAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNetAmount.Location = New System.Drawing.Point(99, 30)
        Me.txtNetAmount.MaxDecimalPlaces = 6
        Me.txtNetAmount.MaxWholeDigits = 21
        Me.txtNetAmount.Name = "txtNetAmount"
        Me.txtNetAmount.Prefix = ""
        Me.txtNetAmount.RangeMax = 1.7976931348623157E+308
        Me.txtNetAmount.RangeMin = -1.7976931348623157E+308
        Me.txtNetAmount.ReadOnly = True
        Me.txtNetAmount.Size = New System.Drawing.Size(237, 21)
        Me.txtNetAmount.TabIndex = 3
        Me.txtNetAmount.Text = "0"
        Me.txtNetAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'fpnlType
        '
        Me.fpnlType.BackColor = System.Drawing.Color.Transparent
        Me.fpnlType.Controls.Add(Me.radLoan)
        Me.fpnlType.Controls.Add(Me.radAdvance)
        Me.fpnlType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fpnlType.Location = New System.Drawing.Point(217, 87)
        Me.fpnlType.Name = "fpnlType"
        Me.fpnlType.Size = New System.Drawing.Size(184, 21)
        Me.fpnlType.TabIndex = 8
        '
        'radLoan
        '
        Me.radLoan.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radLoan.BackColor = System.Drawing.Color.Transparent
        Me.radLoan.Checked = True
        Me.radLoan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.radLoan.Location = New System.Drawing.Point(3, 3)
        Me.radLoan.Name = "radLoan"
        Me.radLoan.Size = New System.Drawing.Size(76, 17)
        Me.radLoan.TabIndex = 0
        Me.radLoan.TabStop = True
        Me.radLoan.Text = "Loan"
        Me.radLoan.UseVisualStyleBackColor = False
        '
        'radAdvance
        '
        Me.radAdvance.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radAdvance.BackColor = System.Drawing.Color.Transparent
        Me.radAdvance.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.radAdvance.Location = New System.Drawing.Point(85, 3)
        Me.radAdvance.Name = "radAdvance"
        Me.radAdvance.Size = New System.Drawing.Size(95, 17)
        Me.radAdvance.TabIndex = 1
        Me.radAdvance.Text = "Advance"
        Me.radAdvance.UseVisualStyleBackColor = False
        '
        'objStLine1
        '
        Me.objStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objStLine1.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objStLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine1.Location = New System.Drawing.Point(411, 24)
        Me.objStLine1.Name = "objStLine1"
        Me.objStLine1.Size = New System.Drawing.Size(6, 251)
        Me.objStLine1.TabIndex = 19
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(203, 195)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(28, 21)
        Me.cboCurrency.TabIndex = 18
        '
        'txtLoanAmt
        '
        Me.txtLoanAmt.AllowNegative = True
        Me.txtLoanAmt.BackColor = System.Drawing.Color.White
        Me.txtLoanAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanAmt.DigitsInGroup = 3
        Me.txtLoanAmt.Flags = 0
        Me.txtLoanAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanAmt.Location = New System.Drawing.Point(109, 195)
        Me.txtLoanAmt.MaxDecimalPlaces = 6
        Me.txtLoanAmt.MaxWholeDigits = 21
        Me.txtLoanAmt.Name = "txtLoanAmt"
        Me.txtLoanAmt.Prefix = ""
        Me.txtLoanAmt.RangeMax = 1.7976931348623157E+308
        Me.txtLoanAmt.RangeMin = -1.7976931348623157E+308
        Me.txtLoanAmt.ReadOnly = True
        Me.txtLoanAmt.Size = New System.Drawing.Size(93, 21)
        Me.txtLoanAmt.TabIndex = 16
        Me.txtLoanAmt.Text = "0"
        Me.txtLoanAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAdvanceAmt
        '
        Me.txtAdvanceAmt.AllowNegative = True
        Me.txtAdvanceAmt.BackColor = System.Drawing.Color.White
        Me.txtAdvanceAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAdvanceAmt.DigitsInGroup = 0
        Me.txtAdvanceAmt.Flags = 0
        Me.txtAdvanceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAdvanceAmt.Location = New System.Drawing.Point(109, 195)
        Me.txtAdvanceAmt.MaxDecimalPlaces = 6
        Me.txtAdvanceAmt.MaxWholeDigits = 21
        Me.txtAdvanceAmt.Name = "txtAdvanceAmt"
        Me.txtAdvanceAmt.Prefix = ""
        Me.txtAdvanceAmt.RangeMax = 1.7976931348623157E+308
        Me.txtAdvanceAmt.RangeMin = -1.7976931348623157E+308
        Me.txtAdvanceAmt.ReadOnly = True
        Me.txtAdvanceAmt.Size = New System.Drawing.Size(93, 21)
        Me.txtAdvanceAmt.TabIndex = 17
        Me.txtAdvanceAmt.Text = "0"
        Me.txtAdvanceAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtApprovedBy
        '
        Me.txtApprovedBy.Flags = 0
        Me.txtApprovedBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApprovedBy.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApprovedBy.Location = New System.Drawing.Point(109, 141)
        Me.txtApprovedBy.Name = "txtApprovedBy"
        Me.txtApprovedBy.ReadOnly = True
        Me.txtApprovedBy.Size = New System.Drawing.Size(292, 21)
        Me.txtApprovedBy.TabIndex = 12
        '
        'cboEmpName
        '
        Me.cboEmpName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpName.FormattingEnabled = True
        Me.cboEmpName.Items.AddRange(New Object() {"Select"})
        Me.cboEmpName.Location = New System.Drawing.Point(109, 114)
        Me.cboEmpName.Name = "cboEmpName"
        Me.cboEmpName.Size = New System.Drawing.Size(292, 21)
        Me.cboEmpName.TabIndex = 10
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(270, 33)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(131, 21)
        Me.cboPayPeriod.TabIndex = 3
        '
        'lblApprovedBy
        '
        Me.lblApprovedBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovedBy.Location = New System.Drawing.Point(12, 144)
        Me.lblApprovedBy.Name = "lblApprovedBy"
        Me.lblApprovedBy.Size = New System.Drawing.Size(91, 15)
        Me.lblApprovedBy.TabIndex = 11
        Me.lblApprovedBy.Text = "Approved By"
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(217, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(47, 15)
        Me.lblPeriod.TabIndex = 2
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmpName
        '
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(12, 117)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(91, 15)
        Me.lblEmpName.TabIndex = 9
        Me.lblEmpName.Text = "Employee"
        '
        'txtVoucherNo
        '
        Me.txtVoucherNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtVoucherNo.Flags = 0
        Me.txtVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVoucherNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtVoucherNo.Location = New System.Drawing.Point(109, 33)
        Me.txtVoucherNo.Name = "txtVoucherNo"
        Me.txtVoucherNo.Size = New System.Drawing.Size(102, 21)
        Me.txtVoucherNo.TabIndex = 1
        '
        'lblVoucherNo
        '
        Me.lblVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVoucherNo.Location = New System.Drawing.Point(12, 36)
        Me.lblVoucherNo.Name = "lblVoucherNo"
        Me.lblVoucherNo.Size = New System.Drawing.Size(91, 15)
        Me.lblVoucherNo.TabIndex = 0
        Me.lblVoucherNo.Text = "Voucher No."
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Items.AddRange(New Object() {"Select"})
        Me.cboLoanScheme.Location = New System.Drawing.Point(109, 168)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(292, 21)
        Me.cboLoanScheme.TabIndex = 14
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(12, 171)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(91, 15)
        Me.lblLoanScheme.TabIndex = 13
        Me.lblLoanScheme.Text = "Loan Scheme"
        '
        'lblLoanAmt
        '
        Me.lblLoanAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAmt.Location = New System.Drawing.Point(12, 198)
        Me.lblLoanAmt.Name = "lblLoanAmt"
        Me.lblLoanAmt.Size = New System.Drawing.Size(91, 15)
        Me.lblLoanAmt.TabIndex = 15
        Me.lblLoanAmt.Text = "Loan Amount"
        Me.lblLoanAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAdvanceAmt
        '
        Me.lblAdvanceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAdvanceAmt.Location = New System.Drawing.Point(12, 198)
        Me.lblAdvanceAmt.Name = "lblAdvanceAmt"
        Me.lblAdvanceAmt.Size = New System.Drawing.Size(91, 15)
        Me.lblAdvanceAmt.TabIndex = 324
        Me.lblAdvanceAmt.Text = "Adv. Amount"
        Me.lblAdvanceAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmNewLoanAdvance_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(780, 530)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNewLoanAdvance_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Loan And Advance"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.tabcLoanInformation.ResumeLayout(False)
        Me.tabpInterestInfo.ResumeLayout(False)
        CType(Me.dgvInterestHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpEMIInfo.ResumeLayout(False)
        CType(Me.dgvEMIHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpTopupInfo.ResumeLayout(False)
        CType(Me.dgvTopupHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpLoanAdvHistory.ResumeLayout(False)
        Me.pnlData.ResumeLayout(False)
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbLoanAdvanceInfo.ResumeLayout(False)
        Me.gbLoanAdvanceInfo.PerformLayout()
        Me.pnlInformation.ResumeLayout(False)
        Me.pnlInformation.PerformLayout()
        CType(Me.nudDuration, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlProjectedAmt.ResumeLayout(False)
        Me.pnlProjectedAmt.PerformLayout()
        Me.fpnlType.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbLoanAdvanceInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPurpose As System.Windows.Forms.Label
    Friend WithEvents txtPurpose As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboEmpName As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprovedBy As System.Windows.Forms.Label
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtVoucherNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEffectiveDate As System.Windows.Forms.Label
    Friend WithEvents lblVoucherNo As System.Windows.Forms.Label
    Friend WithEvents txtApprovedBy As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblLoanAmt As System.Windows.Forms.Label
    Friend WithEvents txtLoanAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents dgvEMIHistory As System.Windows.Forms.DataGridView
    Friend WithEvents cboDeductionPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblDeductionPeriod As System.Windows.Forms.Label
    Friend WithEvents tabcLoanInformation As System.Windows.Forms.TabControl
    Friend WithEvents tabpInterestInfo As System.Windows.Forms.TabPage
    Friend WithEvents tabpEMIInfo As System.Windows.Forms.TabPage
    Friend WithEvents tabpTopupInfo As System.Windows.Forms.TabPage
    Friend WithEvents dgvTopupHistory As System.Windows.Forms.DataGridView
    Friend WithEvents txtAdvanceAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAdvanceAmt As System.Windows.Forms.Label
    Friend WithEvents radAdvance As System.Windows.Forms.RadioButton
    Friend WithEvents radLoan As System.Windows.Forms.RadioButton
    Friend WithEvents dgvInterestHistory As System.Windows.Forms.DataGridView
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents txtNetAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtInterestAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblInterestAmt As System.Windows.Forms.Label
    Friend WithEvents lblNetAmount As System.Windows.Forms.Label
    Friend WithEvents tabpLoanAdvHistory As System.Windows.Forms.TabPage
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents objStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents fpnlType As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents lnProjectedAmount As eZee.Common.eZeeLine
    Friend WithEvents pnlProjectedAmt As System.Windows.Forms.Panel
    Friend WithEvents objlblExRate As System.Windows.Forms.Label
    Friend WithEvents lblDuration As System.Windows.Forms.Label
    Friend WithEvents lblLoanInterest As System.Windows.Forms.Label
    Friend WithEvents lblEMIAmount As System.Windows.Forms.Label
    Friend WithEvents txtInstallmentAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents nudDuration As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtLoanRate As eZee.TextBox.NumericTextBox
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents pnlInformation As System.Windows.Forms.Panel
    Friend WithEvents objdgcolhIAdd As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhIEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhIDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhInterestPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInterestEffectiveDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInterestRate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhlninteresttranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIPStatusId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTAdd As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhTEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhTDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhTopupPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhTopupEffectiveDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTopupAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhTopupInterest As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhlntopuptranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhTPstatusid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoanScheme As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhVoucherNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNetAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRepayment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBalanceAmt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents elProjectedINSTLAmt As eZee.Common.eZeeLine
    Friend WithEvents objdgcolhEAdd As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhEMIPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEMIeffectivedate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhloan_duration As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhInstallmentAmt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEMINoofInstallment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhlnemitranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEPstatusid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboInterestCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblInterestCalcType As System.Windows.Forms.Label
    Friend WithEvents cboLoanCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanCalcType As System.Windows.Forms.Label
    Friend WithEvents txtIntAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents txtPrincipalAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblPrincipalAmt As System.Windows.Forms.Label
    Friend WithEvents lblIntAmt As System.Windows.Forms.Label
    Friend WithEvents lblMappedHead As System.Windows.Forms.Label
    Friend WithEvents cboMappedHead As System.Windows.Forms.ComboBox
End Class
