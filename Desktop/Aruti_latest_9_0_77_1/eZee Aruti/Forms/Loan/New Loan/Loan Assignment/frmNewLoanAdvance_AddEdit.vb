﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Math

#End Region

Public Class frmNewLoanAdvance_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmNewLoanAdvance_AddEdit"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private mintLoanAdvanceUnkid As Integer = -1
    Private mintProcessPendingLoanUnkid As Integer = -1
    Private mintEmployeeSelectedId As Integer = -1
    Private mintApproverSeletedId As Integer = -1
    Private imgAdd As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.add_16)
    Private imgEdit As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.edit)
    Private imgDelete As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.remove)
    Private imgBlank As Drawing.Bitmap = New Drawing.Bitmap(1, 1).Clone

    Private objLoan_Advance As clsLoan_Advance
    Private objlnInterest As clslnloan_interest_tran
    Private objlnEMI As clslnloan_emitenure_tran
    Private objlnTopUp As clslnloan_topup_tran

    Private mblnIsFormLoad As Boolean = False

    Private mdtInterest As DataTable
    Private mdtEMI As DataTable
    Private mdtTopup As DataTable

    'Private mdvInterestView As DataView
    'Private mdvEMIView As DataView
    'Private mdvTopupView As DataView
    Private mintCurrOpenPeriodId As Integer = 0

    Private mblnIsExternalEntity As Boolean = False
    Private mintEmployeeid As Integer = -1
    Private mintPendingApprovalTranID As Integer = -1

    Private mintLastCalcTypeId As Integer = 0
    Private mblnIsValidDeductionPeriod As Boolean = False

    'Sohail (07 May 2015) -- Start
    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    Private mstrBaseCurrSign As String
    Private mintBaseCurrId As Integer
    Private mintPaidCurrId As Integer
    Private mdecBaseExRate As Decimal
    Private mdecPaidExRate As Decimal
    'Sohail (07 May 2015) -- End

    'Nilay (15-Dec-2015) -- Start
    Private mdecInstallmentAmount As Decimal = 0
    'Nilay (15-Dec-2015) -- End
    'Nilay (17-Dec-2015) -- Start
    Private mintNoOfInstallment As Integer = 0
    'Nilay (17-Dec-2015) -- End

    'Nilay (25-Mar-2016) -- Start
    Private mdtStartDate As Date
    Private mdtEndDate As Date
    'Nilay (25-Mar-2016) -- End
    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Private mstrSearchText As String = ""
    'Sohail (29 Apr 2019) -- End

    'Hemant (29 Jan 2022) -- Start            
    Private mdecExchangerate As Decimal
    'Hemant (29 Jan 2022) -- End

#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intPendingLoanUnkid As Integer = -1) As Boolean
        Try
            mintLoanAdvanceUnkid = intUnkId
            menAction = eAction
            mintProcessPendingLoanUnkid = intPendingLoanUnkid

            'THIS TAB IS REMOVED COZ ONLY EDITED LOAN HISTORY WAS DISPLAYED {ONLY ONE ROW}
            If tabcLoanInformation.TabPages.Contains(tabpLoanAdvHistory) Then
                tabcLoanInformation.TabPages.Remove(tabpLoanAdvHistory)
            End If

            If menAction <> enAction.EDIT_ONE Then
                tabcLoanInformation.TabPages.Remove(tabpTopupInfo)
            End If
            Me.ShowDialog()

            intUnkId = mintLoanAdvanceUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Properties "

    Public Property _PendingApprovalTranID() As Integer
        Get
            Return mintPendingApprovalTranID
        End Get
        Set(ByVal value As Integer)
            mintPendingApprovalTranID = value
        End Set
    End Property

    Public Property _Employeeid() As Integer
        Get
            Return mintEmployeeid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeid = value
        End Set
    End Property

    'Public Property _LoanSchemeId() As Integer
    '    Get
    '        Return mintLoanSchemeId
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintLoanSchemeId = value
    '    End Set
    'End Property

    'Public Property _IsItLoan() As Boolean
    '    Get
    '        Return mblnIsItLoan
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsItLoan = value
    '    End Set
    'End Property

    'Public Property _ApproverId() As Integer
    '    Get
    '        Return mintApproverId
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintApproverId = value
    '    End Set
    'End Property

    'Public Property _ApprovedAmount() As Decimal
    '    Get
    '        Return mdecApprovedAmount
    '    End Get
    '    Set(ByVal value As Decimal)
    '        mdecApprovedAmount = value
    '    End Set
    'End Property

    'Public Property _IsFromAssign() As Boolean
    '    Get
    '        Return mblnIsFromAssign
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsFromAssign = value
    '    End Set
    'End Property

    'Public Property _IsExternalEntity() As Boolean
    '    Get
    '        Return mblnIsExternalEntity
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsExternalEntity = value
    '    End Set
    'End Property

#End Region

#Region " Form's Events "

    Private Sub frmNewLoanAdvance_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        ' objLoan_Advance = Nothing
    End Sub

    Private Sub frmNewLoanAdvance_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmNewLoanAdvance_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmNewLoanAdvance_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLoan_Advance = New clsLoan_Advance : objlnInterest = New clslnloan_interest_tran
        objlnEMI = New clslnloan_emitenure_tran : objlnTopUp = New clslnloan_topup_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()
            Call SetProcessVisibility()
            Call FillCombo()
            Call SetImage()

            AddHandler cboEmpName.KeyDown, AddressOf ComboBox_KeyDown
            AddHandler cboEmpName.KeyPress, AddressOf ComboBox_KeyPress

            AddHandler cboLoanScheme.KeyDown, AddressOf ComboBox_KeyDown
            AddHandler cboLoanScheme.KeyPress, AddressOf ComboBox_KeyPress

            AddHandler cboCurrency.KeyDown, AddressOf ComboBox_KeyDown
            AddHandler cboCurrency.KeyPress, AddressOf ComboBox_KeyPress

            If menAction = enAction.EDIT_ONE Then
                objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceUnkid
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
                'fpnlCalcType.Enabled = False
                cboLoanCalcType.Enabled = False
                cboInterestCalcType.Enabled = False
                'Sohail (15 Dec 2015) -- End
                fpnlType.Enabled = False
                cboDeductionPeriod.Enabled = False
                cboPayPeriod.Enabled = False
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                cboMappedHead.Enabled = False
                'Sohail (29 Apr 2019) -- End
                Call ShowHideProjectedLoan()
                If objLoan_Advance._Isloan = False Then
                    tabcLoanInformation.TabPages.Remove(tabpTopupInfo)
                End If
            End If

            Dim objMstData As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintCurrOpenPeriodId = objMstData.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open)
            mintCurrOpenPeriodId = objMstData.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'S.SANDEEP [04 JUN 2015] -- END


            'objlnInterest._Loanadvancetranunkid = mintLoanAdvanceUnkid
            'mdtInterest = objlnInterest._DataTable.Copy

            'objlnEMI._Loanadvancetranunkid = mintLoanAdvanceUnkid
            'mdtEMI = objlnEMI._DataTable.Copy

            'objlnTopUp._Loanadvancetranunkid = mintLoanAdvanceUnkid
            'mdtTopup = objlnTopUp._DataTable.Copy

            Call GetValue()

            Call FillHistoryList()
            Call Fill_Transaction_Grids()

            txtVoucherNo.Focus()

            mblnIsFormLoad = True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmNewLoanAdvance_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoan_Advance.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Advance"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboDeductionPeriod.BackColor = GUI.ColorComp
            cboCurrency.BackColor = GUI.ColorComp
            cboEmpName.BackColor = GUI.ColorComp
            cboLoanScheme.BackColor = GUI.ColorComp
            cboPayPeriod.BackColor = GUI.ColorComp
            txtVoucherNo.BackColor = GUI.ColorComp
            txtApprovedBy.BackColor = GUI.ColorComp
            txtLoanAmt.BackColor = GUI.ColorComp
            txtNetAmount.BackColor = GUI.ColorComp
            txtPurpose.BackColor = GUI.ColorOptional
            txtLoanRate.BackColor = GUI.ColorComp
            txtInstallmentAmt.BackColor = GUI.ColorComp
            nudDuration.BackColor = GUI.ColorComp
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
            cboLoanCalcType.BackColor = GUI.ColorComp
            cboInterestCalcType.BackColor = GUI.ColorComp
            txtPrincipalAmt.BackColor = GUI.ColorOptional
            txtIntAmt.BackColor = GUI.ColorOptional
            'Sohail (15 Dec 2015) -- End
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            cboMappedHead.BackColor = GUI.ColorOptional
            'Sohail (29 Apr 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetProcessVisibility()
        Try
            If ConfigParameter._Object._LoanVocNoType = 1 Then
                txtVoucherNo.Enabled = False
            Else
                txtVoucherNo.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetProcessVisibility", mstrModuleName)
        End Try
    End Sub

    Public Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Dim objPeriod As New clscommom_period_Tran
        Dim objExRate As New clsExchangeRate
        Dim objMaster As New clsMasterData 'Sohail (15 Dec 2015)
        Dim objHead As New clsTransactionHead 'Sohail (29 Apr 2019)
        Try

            Dim objEmployee As New clsEmployee_Master
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("Employee", False, True, mintEmployeeid, , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("Employee", False, True, mintEmployeeid)
            'End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, False, _
                                                   "Employee", False, mintEmployeeid)

            'Nilay (10-Oct-2015) -- End

            With cboEmpName
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Employee")
            End With
            objEmployee = Nothing


            Dim objLoanScheme As New clsLoan_Scheme
            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(True, "LoanScheme")
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .SelectedValue = 0
            End With
            objLoanScheme = Nothing

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If menAction = enAction.EDIT_ONE Then
            '    dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "PeriodList", True)
            'Else
            '    dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "PeriodList", True, enStatusType.Open)
            'End If
            If menAction = enAction.EDIT_ONE Then
                dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, _
                                                   FinancialYear._Object._DatabaseName, _
                                                   FinancialYear._Object._Database_Start_Date, _
                                                   "PeriodList", True)
            Else
                dsList = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   FinancialYear._Object._DatabaseName, _
                                                   FinancialYear._Object._Database_Start_Date, _
                                                   "PeriodList", True, enStatusType.Open)
            End If
            'Nilay (10-Oct-2015) -- End

            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("PeriodList")
                .SelectedValue = 0
            End With

            With cboDeductionPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("PeriodList").Copy
                .SelectedValue = 0
            End With

            dsList = objExRate.getComboList("ExRate", False)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsList.Tables("ExRate")
                .SelectedIndex = 0
            End With
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                End If
            End If
            'Sohail (07 May 2015) -- End

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
            dsList = objLoan_Advance.GetLoanCalculationTypeList("List", True)
            With cboLoanCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objLoan_Advance.GetLoan_Interest_Calculation_Type("List", True)
            With cboInterestCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'Sohail (15 Dec 2015) -- End

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False, "calctype_id <> " & CInt(enCalcType.NET_PAY) & " ")
            With cboMappedHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboMappedHead)
            End With
            'Sohail (29 Apr 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objHead = Nothing 'Sohail (29 Apr 2019)
        End Try
    End Sub

    Private Sub SetImage()
        Try
            objdgcolhIAdd.Image = imgAdd
            objdgcolhIEdit.Image = imgEdit
            objdgcolhIDelete.Image = imgDelete

            objdgcolhIAdd.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhIAdd.DefaultCellStyle.SelectionForeColor = Color.White
            objdgcolhIEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhIEdit.DefaultCellStyle.SelectionForeColor = Color.White
            objdgcolhIDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhIDelete.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhEAdd.Image = imgAdd
            objdgcolhEEdit.Image = imgEdit
            objdgcolhEDelete.Image = imgDelete

            objdgcolhEAdd.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEAdd.DefaultCellStyle.SelectionForeColor = Color.White
            objdgcolhEEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEEdit.DefaultCellStyle.SelectionForeColor = Color.White
            objdgcolhEDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEDelete.DefaultCellStyle.SelectionForeColor = Color.White


            objdgcolhTAdd.Image = imgAdd
            objdgcolhTEdit.Image = imgEdit
            objdgcolhTDelete.Image = imgDelete

            objdgcolhTAdd.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhTAdd.DefaultCellStyle.SelectionForeColor = Color.White
            objdgcolhTEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhTEdit.DefaultCellStyle.SelectionForeColor = Color.White
            objdgcolhTDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhTDelete.DefaultCellStyle.SelectionForeColor = Color.White

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetImage", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            'Dim objApprove As New clsEmployee_Master
            Dim objEmployee As New clsEmployee_Master
            'Nilay (21-Jul-2016) -- End

            If mintPendingApprovalTranID > 0 Then
                Dim objApprovalTran As New clsloanapproval_process_Tran
                Dim objLoanApplication As New clsProcess_pending_loan
                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                Dim objLScheme As New clsLoan_Scheme
                'S.SANDEEP [20-SEP-2017] -- END

                objApprovalTran._Pendingloantranunkid = mintPendingApprovalTranID
                objLoanApplication._Processpendingloanunkid = objApprovalTran._Processpendingloanunkid
                mintProcessPendingLoanUnkid = objLoanApplication._Processpendingloanunkid
                mblnIsExternalEntity = objLoanApplication._Isexternal_Entity
                txtPurpose.Text = objLoanApplication._Emp_Remark
                If objLoanApplication._Isloan Then
                    radLoan.Checked = True
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'fpnlCalcType.Enabled = True
                    cboLoanCalcType.Enabled = True
                    cboInterestCalcType.Enabled = True
                    'Sohail (15 Dec 2015) -- End
                    pnlInformation.Enabled = True
                    pnlProjectedAmt.Enabled = True
                    Me.Size = CType(New Point(786, 553), Drawing.Size)

                    'S.SANDEEP [20-SEP-2017] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 50
                    objLScheme._Loanschemeunkid = objLoanApplication._Loanschemeunkid
                    txtLoanRate.Decimal = objLScheme._InterestRate
                    cboLoanCalcType.SelectedValue = objLScheme._LoanCalcTypeId
                    cboInterestCalcType.SelectedValue = objLScheme._InterestCalctypeId
                    'S.SANDEEP [20-SEP-2017] -- END

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    cboMappedHead.Enabled = True
                    cboMappedHead.SelectedValue = objLScheme._Mapped_TranheadUnkid
                    'Sohail (29 Apr 2019) -- End

                Else
                    pnlInformation.Enabled = False
                    pnlProjectedAmt.Enabled = False
                    radAdvance.Checked = True
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'fpnlCalcType.Enabled = False
                    cboLoanCalcType.Enabled = False
                    cboInterestCalcType.Enabled = False
                    'Sohail (15 Dec 2015) -- End
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    cboMappedHead.Enabled = False
                    'Sohail (29 Apr 2019) -- End
                    tabcLoanInformation.TabPages.Remove(tabpInterestInfo)
                    tabcLoanInformation.TabPages.Remove(tabpEMIInfo)
                    tabcLoanInformation.TabPages.Remove(tabpTopupInfo)

                    lnProjectedAmount.Visible = False
                    pnlProjectedAmt.Visible = False
                    pnlInformation.Visible = False
                    elProjectedINSTLAmt.Visible = False
                    lblPurpose.Location = New Point(435, 36)
                    txtPurpose.Location = New Point(531, 36) : txtPurpose.Size = CType(New Point(238, 231), Drawing.Size)
                    Me.Size = CType(New Point(786, 362), Drawing.Size)
                End If

                fpnlType.Enabled = False
                cboLoanScheme.SelectedValue = objLoanApplication._Loanschemeunkid


                'Nilay (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                ''S.SANDEEP [04 JUN 2015] -- START
                ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                ''objEmployee._Employeeunkid = objApprovalTran._Approverempunkid
                'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objApprovalTran._Approverempunkid
                ''S.SANDEEP [04 JUN 2015] -- END
                'txtApprovedBy.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
                'txtApprovedBy.Tag = objApprovalTran._Approvertranunkid
                'objEmployee = Nothing
                Dim objLoanApprover As New clsLoanApprover_master
                Dim objLoanLevel As New clslnapproverlevel_master
                objLoanApprover._lnApproverunkid = objLoanApplication._Approverunkid
                objLoanLevel._lnLevelunkid = objLoanApprover._lnLevelunkid

                If objLoanApprover._IsExternalApprover = True Then
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = objLoanApprover._ApproverEmpunkid
                    Dim strAppName As String = objUser._Username & " - " & objLoanLevel._lnLevelname
                    If strAppName.Trim.Length > 0 Then
                        txtApprovedBy.Text = strAppName
                    Else
                        txtApprovedBy.Text = objUser._Username
                    End If
                    objUser = Nothing
                Else
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLoanApprover._ApproverEmpunkid
                    txtApprovedBy.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " - " & objLoanLevel._lnLevelname
                End If
                txtApprovedBy.Tag = objApprovalTran._Approvertranunkid
                'Nilay (21-Jul-2016) -- End

                cboCurrency.SelectedValue = objApprovalTran._Countryunkid
                'Nilay (15-Dec-2015) -- Start
                'cboDeductionPeriod.SelectedValue = objApprovalTran._Deductionperiodunkid
                ''Sohail (15 Dec 2015) -- Start
                ''Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
                'cboPayPeriod.SelectedValue = objApprovalTran._Deductionperiodunkid
                ''Sohail (15 Dec 2015) -- End

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                'If radLoan.Checked Then
                '    txtLoanAmt.Text = Format(objApprovalTran._Loan_Amount, GUI.fmtCurrency)
                'ElseIf radAdvance.Checked Then
                '    txtAdvanceAmt.Text = Format(objApprovalTran._Loan_Amount, GUI.fmtCurrency)
                'End If

                'RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                ''Nilay (18-Nov-2015) -- Start
                ''RemoveHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
                'RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                ''Nilay (18-Nov-2015) -- End

                'If objLoanApplication._Isloan Then
                '    'Nilay (15-Dec-2015) -- Start
                '    ' nudDuration.Value = objApprovalTran._Noofinstallment
                '    If CInt(objApprovalTran._Noofinstallment) <= 0 Then
                '        nudDuration.Value = 1
                '    Else
                    ' nudDuration.Value = objApprovalTran._Noofinstallment
                '    End If
                '    'Nilay (15-Dec-2015) -- End

                '    'Nilay (17-Dec-2015) -- Start
                '    mintNoOfInstallment = CInt(objApprovalTran._Noofinstallment)
                '    'Nilay (17-Dec-2015) -- End

                '    'Nilay (15-Dec-2015) -- Start
                '    mdecInstallmentAmount = objApprovalTran._Installmentamt
                '    'Nilay (15-Dec-2015) -- End
                '    txtInstallmentAmt.Tag = objApprovalTran._Installmentamt
                '    txtInstallmentAmt.Text = Format(CDec(objApprovalTran._Installmentamt), GUI.fmtCurrency)
                'End If

                ''Nilay (18-Nov-2015) -- Start
                ''AddHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
                'AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                ''Nilay (18-Nov-2015) -- End
                'AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                'S.SANDEEP [20-SEP-2017] -- END

                'If mdtEMI IsNot Nothing Then
                '    Dim objPeriod As New clscommom_period_Tran
                '    objPeriod._Periodunkid = objApprovalTran._Deductionperiodunkid
                '    Dim drRow As DataRow = mdtEMI.NewRow()
                '    drRow("lnemitranunkid") = -1
                '    drRow("loanadvancetranunkid") = -1
                '    drRow("periodunkid") = objApprovalTran._Deductionperiodunkid
                '    drRow("effectivedate") = objPeriod._Start_Date
                '    drRow("loan_duration") = objApprovalTran._Duration
                '    drRow("emi_tenure") = objApprovalTran._Noofinstallment
                '    drRow("emi_amount") = objApprovalTran._Installmentamt
                '    drRow("dperiod") = objPeriod._Period_Name
                '    drRow("ddate") = objPeriod._Start_Date.ToShortDateString
                '    drRow("pstatusid") = objPeriod._Statusid
                '    drRow("userunkid") = User._Object._Userunkid
                '    drRow("isvoid") = False
                '    drRow("voiduserunkid") = -1
                '    drRow("voiddatetime") = DBNull.Value
                '    drRow("AUD") = "A"
                '    drRow("GUID") = Guid.NewGuid
                '    mdtEMI.Rows.Add(drRow)
                '    objPeriod = Nothing
                'End If

                If radLoan.Checked Then
                    txtLoanAmt.Text = Format(objApprovalTran._Loan_Amount, GUI.fmtCurrency)
                ElseIf radAdvance.Checked Then
                    txtAdvanceAmt.Text = Format(objApprovalTran._Loan_Amount, GUI.fmtCurrency)
                End If

                RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                If objLoanApplication._Isloan Then
                    If CInt(objApprovalTran._Noofinstallment) <= 0 Then
                        nudDuration.Value = 1
                    Else
                        nudDuration.Value = objApprovalTran._Noofinstallment
                    End If
                    mintNoOfInstallment = CInt(objApprovalTran._Noofinstallment)
                    mdecInstallmentAmount = objApprovalTran._Installmentamt
                    txtInstallmentAmt.Tag = objApprovalTran._Installmentamt
                    txtInstallmentAmt.Text = Format(CDec(objApprovalTran._Installmentamt), GUI.fmtCurrency)

                    cboInterestCalcType.SelectedValue = objLScheme._InterestCalctypeId
                    cboLoanCalcType.SelectedValue = objLScheme._LoanCalcTypeId
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    cboMappedHead.SelectedValue = objLScheme._Mapped_TranheadUnkid
                    'Sohail (29 Apr 2019) -- End
                    'Hemant (12 Nov 2021) -- Start
                    'ISSUE(REA) : on changing No of installment of emi , emi amount should get changed too but it remained unchanged.
                    If CInt(cboLoanCalcType.SelectedValue) = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI) Then
                        txtPrincipalAmt.Tag = objApprovalTran._Installmentamt
                        txtPrincipalAmt.Text = Format(CDec(objApprovalTran._Installmentamt), GUI.fmtCurrency)
                    End If
                    'Hemant (12 Nov 2021) -- End
                End If
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged

                Dim objMaster As New clsMasterData
                Dim objPeriod As New clscommom_period_Tran
                Dim intFirstOpenPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, _
                                                                                 CInt(FinancialYear._Object._YearUnkid), _
                                                                                 StatusType.Open, _
                                                                                 False, True)

                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = objApprovalTran._Deductionperiodunkid
                If objPeriod._Statusid = StatusType.Close Then
                    cboPayPeriod.SelectedValue = intFirstOpenPeriodId
                    cboDeductionPeriod.SelectedValue = intFirstOpenPeriodId
                Else
                    cboPayPeriod.SelectedValue = objApprovalTran._Deductionperiodunkid
                    cboDeductionPeriod.SelectedValue = objApprovalTran._Deductionperiodunkid
                End If
                'S.SANDEEP [20-SEP-2017] -- END

                objLoanApplication = Nothing
                objApprovalTran = Nothing

                'cboDeductionPeriod.Enabled = False
            Else
                'Nilay (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                ''S.SANDEEP [04 JUN 2015] -- START
                ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                ''objEmployee._Employeeunkid = objLoan_Advance._Approverunkid
                'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLoan_Advance._Approverunkid
                ''S.SANDEEP [04 JUN 2015] -- END
                'txtApprovedBy.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
                'txtApprovedBy.Tag = objLoan_Advance._Approverunkid
                'objEmployee = Nothing
                Dim objLoanApprover As New clsLoanApprover_master
                Dim objLoanLevel As New clslnapproverlevel_master
                objLoanApprover._lnApproverunkid = objLoan_Advance._Approverunkid
                objLoanLevel._lnLevelunkid = objLoanApprover._lnLevelunkid

                If objLoanApprover._IsExternalApprover = True Then
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = objLoanApprover._ApproverEmpunkid
                    Dim strAppName As String = objUser._Username & " - " & objLoanLevel._lnLevelname
                    If strAppName.Trim.Length > 0 Then
                        txtApprovedBy.Text = strAppName
                    Else
                        txtApprovedBy.Text = objUser._Username
                    End If
                    objUser = Nothing
                Else
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLoanApprover._ApproverEmpunkid
                    txtApprovedBy.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " - " & objLoanLevel._lnLevelname
                End If
                txtApprovedBy.Tag = objLoan_Advance._Approverunkid
                'Nilay (21-Jul-2016) -- End

                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
                'Select Case objLoan_Advance._Calctype_Id
                '    Case 1
                '        radSimpleInterest.Checked = True
                '    Case 3
                '        radReduceBalInterest.Checked = True
                '    Case 4
                '        radNoInterest.Checked = True
                'End Select
                cboLoanCalcType.SelectedValue = objLoan_Advance._Calctype_Id
                cboInterestCalcType.SelectedValue = objLoan_Advance._Interest_Calctype_Id
                'Sohail (15 Dec 2015) -- End
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                cboMappedHead.SelectedValue = objLoan_Advance._Mapped_TranheadUnkid
                'Sohail (29 Apr 2019) -- End

                If objLoan_Advance._Isloan = True Then
                    radLoan.Checked = True
                    tabcLoanInformation.Visible = True
                    Me.Size = CType(New Point(786, 553), Drawing.Size)
                Else
                    radAdvance.Checked = True
                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
                    'fpnlCalcType.Enabled = False
                    cboLoanCalcType.Enabled = False
                    cboInterestCalcType.Enabled = False
                    'Sohail (15 Dec 2015) -- End
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    cboMappedHead.Enabled = False
                    'Sohail (29 Apr 2019) -- End
                    tabcLoanInformation.Visible = False
                    tabcLoanInformation.TabPages.Remove(tabpInterestInfo)
                    tabcLoanInformation.TabPages.Remove(tabpEMIInfo)
                    tabcLoanInformation.TabPages.Remove(tabpTopupInfo)
                    Me.Size = CType(New Point(786, 362), Drawing.Size)
                End If

                If Not (objLoan_Advance._Effective_Date = Nothing) Then
                    dtpDate.Value = objLoan_Advance._Effective_Date
                End If
                cboEmpName.SelectedValue = objLoan_Advance._Employeeunkid
                txtInterestAmt.Text = Format(objLoan_Advance._Interest_Amount, GUI.fmtCurrency)
                radLoan.Checked = objLoan_Advance._Isloan
                txtLoanAmt.Text = Format(objLoan_Advance._Loan_Amount, GUI.fmtCurrency)
                txtAdvanceAmt.Text = Format(objLoan_Advance._Advance_Amount, GUI.fmtCurrency)
                txtPurpose.Text = objLoan_Advance._Loan_Purpose
                cboLoanScheme.SelectedValue = objLoan_Advance._Loanschemeunkid
                txtVoucherNo.Text = objLoan_Advance._Loanvoucher_No
                txtNetAmount.Text = Format(objLoan_Advance._Net_Amount, GUI.fmtCurrency)
                cboPayPeriod.SelectedValue = objLoan_Advance._Periodunkid
                objLoan_Advance._Userunkid = objLoan_Advance._Userunkid
                objLoan_Advance._Voiduserunkid = -1
                objLoan_Advance._Voiddatetime = Nothing
                objLoan_Advance._LoanStatus = objLoan_Advance._LoanStatus
                cboCurrency.SelectedValue = objLoan_Advance._CountryUnkid
                cboDeductionPeriod.SelectedValue = objLoan_Advance._Deductionperiodunkid
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'Nilay (25-Mar-2016) -- Start
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            mdtStartDate = objPeriod._Start_Date
            mdtEndDate = objPeriod._End_Date
            'Nilay (25-Mar-2016) -- End

            objLoan_Advance._Effective_Date = dtpDate.Value
            objLoan_Advance._Employeeunkid = CInt(cboEmpName.SelectedValue)
            objLoan_Advance._Isbrought_Forward = False
            objLoan_Advance._Loan_Amount = txtLoanAmt.Decimal
            objLoan_Advance._Loan_Purpose = txtPurpose.Text
            objLoan_Advance._Loanvoucher_No = txtVoucherNo.Text
            objLoan_Advance._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            If mintLoanAdvanceUnkid = -1 Then
                objLoan_Advance._LoanStatus = 1
            Else
                objLoan_Advance._LoanStatus = objLoan_Advance._LoanStatus
            End If
            objLoan_Advance._Deductionperiodunkid = CInt(cboDeductionPeriod.SelectedValue)

            If mintLoanAdvanceUnkid <= 0 Then
                objLoan_Advance._Processpendingloanunkid = mintProcessPendingLoanUnkid
            End If
            objLoan_Advance._Isloan = radLoan.Checked
            objLoan_Advance._Advance_Amount = txtAdvanceAmt.Decimal
            objLoan_Advance._Approverunkid = CInt(txtApprovedBy.Tag)
            objLoan_Advance._CountryUnkid = CInt(cboCurrency.SelectedValue)

            If radLoan.Checked Then
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
                'If radSimpleInterest.Checked Then
                '    objLoan_Advance._Calctype_Id = enLoanCalcId.Simple_Interest '1
                'ElseIf radReduceBalInterest.Checked Then
                '    objLoan_Advance._Calctype_Id = enLoanCalcId.Reducing_Amount '3
                'ElseIf radNoInterest.Checked Then
                '    objLoan_Advance._Calctype_Id = enLoanCalcId.No_Interest '4
                'End If
                objLoan_Advance._Calctype_Id = CInt(cboLoanCalcType.SelectedValue)
                objLoan_Advance._Interest_Calctype_Id = CInt(cboInterestCalcType.SelectedValue)
                'Sohail (15 Dec 2015) -- End
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                objLoan_Advance._Mapped_TranheadUnkid = CInt(cboMappedHead.SelectedValue)
                'Sohail (29 Apr 2019) -- End
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                objLoan_Advance._Exchange_rate = mdecPaidExRate
                objLoan_Advance._Basecurrency_amount = txtLoanAmt.Decimal / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                objLoan_Advance._Opening_balance = txtLoanAmt.Decimal / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                objLoan_Advance._Opening_balancePaidCurrency = txtLoanAmt.Decimal
                'Sohail (07 May 2015) -- End
            Else
                objLoan_Advance._Calctype_Id = 0
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
                objLoan_Advance._Interest_Calctype_Id = 0
                'Sohail (15 Dec 2015) -- End
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                objLoan_Advance._Mapped_TranheadUnkid = 0
                'Sohail (29 Apr 2019) -- End
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                objLoan_Advance._Exchange_rate = mdecPaidExRate
                objLoan_Advance._Basecurrency_amount = txtAdvanceAmt.Decimal / mdecPaidExRate
                objLoan_Advance._Opening_balance = txtAdvanceAmt.Decimal / mdecPaidExRate
                objLoan_Advance._Opening_balancePaidCurrency = txtAdvanceAmt.Decimal
                'Sohail (07 May 2015) -- End
            End If

            If radLoan.Checked = True Then
                If menAction <> enAction.EDIT_ONE Then
                    If mblnIsExternalEntity = True Then
                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        'objLoan_Advance._Balance_Amount = txtLoanAmt.Decimal
                        objLoan_Advance._Balance_Amount = txtLoanAmt.Decimal / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                        objLoan_Advance._Balance_AmountPaidCurrency = txtLoanAmt.Decimal
                        'Sohail (07 May 2015) -- End
                    End If
                End If
                objLoan_Advance._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                If menAction <> enAction.EDIT_ONE Then
                    objLoan_Advance._Interest_Rate = txtLoanRate.Decimal
                    objLoan_Advance._Loan_Duration = CInt(nudDuration.Value)
                    objLoan_Advance._Interest_Amount = CDec(txtInterestAmt.Tag) / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                    objLoan_Advance._Net_Amount = CDec(txtNetAmount.Tag) / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                    objLoan_Advance._Emi_Tenure = CInt(nudDuration.Value)
                    objLoan_Advance._Emi_Amount = CDec(txtInstallmentAmt.Tag) / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                End If
            Else
                If menAction <> enAction.EDIT_ONE Then
                    If mblnIsExternalEntity = True Then
                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        objLoan_Advance._Balance_Amount = txtAdvanceAmt.Decimal / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                        objLoan_Advance._Balance_AmountPaidCurrency = txtAdvanceAmt.Decimal
                        'Sohail (07 May 2015) -- End
                    End If
                End If
                objLoan_Advance._Loanschemeunkid = 0
                objLoan_Advance._Net_Amount = 0
            End If

            If mintLoanAdvanceUnkid = -1 Then
                objLoan_Advance._Userunkid = User._Object._Userunkid
                objLoan_Advance._Voiduserunkid = -1
                objLoan_Advance._Voiddatetime = Nothing
                objLoan_Advance._Isvoid = False
            Else
                objLoan_Advance._Userunkid = objLoan_Advance._Userunkid
                objLoan_Advance._Voiduserunkid = objLoan_Advance._Voiduserunkid
                objLoan_Advance._Voiddatetime = objLoan_Advance._Voiddatetime
                objLoan_Advance._Isvoid = objLoan_Advance._Isvoid
            End If

            objLoan_Advance._RateOfInterest = txtLoanRate.Decimal
            objLoan_Advance._NoOfInstallments = CInt(nudDuration.Value)
            objLoan_Advance._InstallmentAmount = txtInstallmentAmt.Decimal

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            objLoan_Advance._EMI_Principal_amount = txtPrincipalAmt.Decimal
            objLoan_Advance._EMI_Interest_amount = txtIntAmt.Decimal
            'Sohail (15 Dec 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Common_Validation() As Boolean
        Try
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Pay Period is compulsory information. Please select Pay Period."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Return False
            End If

            If CInt(cboDeductionPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Deduction period is compulsory information. Please select Deduction period to continue."), enMsgBoxStyle.Information)
                cboDeductionPeriod.Focus()
                Return False
            End If

            Dim objPeriod As New clscommom_period_Tran
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                Dim objDPeriod As New clscommom_period_Tran
                'objDPeriod._Periodunkid = CInt(cboDeductionPeriod.SelectedValue)
                objDPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboDeductionPeriod.SelectedValue)
                'Nilay (10-Oct-2015) -- End
                If dtpDate.Value.Date > objDPeriod._End_Date Or dtpDate.Value.Date < objDPeriod._Start_Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Effective date should be between ") & _
                                    objDPeriod._Start_Date & Language.getMessage(mstrModuleName, 18, " And ") & _
                                    objDPeriod._End_Date, enMsgBoxStyle.Information)
                    dtpDate.Focus()
                    Return False
                End If

                If objDPeriod._Start_Date < objPeriod._Start_Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Deduction period cannot be less then the Loan Period."), enMsgBoxStyle.Information)
                    cboDeductionPeriod.Focus()
                    Return False
                End If

                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                Dim objPayment As New clsPayment_tran
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim ds As DataSet = objPayment.GetListByPeriod("List", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(cboDeductionPeriod.SelectedValue), clsPayment_tran.enPayTypeId.PAYMENT, cboEmpName.SelectedValue.ToString, " AND 1 = 1 ")
                Dim ds As DataSet = objPayment.GetListByPeriod(FinancialYear._Object._DatabaseName, _
                                                               User._Object._Userunkid, _
                                                               FinancialYear._Object._YearUnkid, _
                                                               Company._Object._Companyunkid, _
                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                               ConfigParameter._Object._UserAccessModeSetting, _
                                                               True, _
                                                               ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                               "List", _
                                                               clsPayment_tran.enPaymentRefId.PAYSLIP, _
                                                               CInt(cboDeductionPeriod.SelectedValue), _
                                                               clsPayment_tran.enPayTypeId.PAYMENT, _
                                                               cboEmpName.SelectedValue.ToString, False)
                'Nilay (10-Oct-2015) -- End


                If ds.Tables("List").Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, you cannot assign Loan/Advance. Reason: Payslip Payment is already done for selected period."), enMsgBoxStyle.Information)
                    Return False
                End If

                Dim objTnA As New clsTnALeaveTran
                If objTnA.IsPayrollProcessDone(CInt(cboDeductionPeriod.SelectedValue), cboEmpName.SelectedValue.ToString, objDPeriod._End_Date, enModuleReference.Payroll) = True Then
                    'Sohail (19 Apr 2019) -- Start
                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot assign Loan/Advance. Reason: Payroll process is already done for the last date of selected deduction period."), enMsgBoxStyle.Information)
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot assign Loan/Advance. Reason: Payroll process is already done for the last date of selected deduction period.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 31, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim objFrm As New frmProcessPayroll
                        objFrm.ShowDialog()
                    End If
                    'Sohail (19 Apr 2019) -- End
                    Return False
                End If
                'Sohail (07 May 2015) -- End

                objDPeriod = Nothing
            End If
            objPeriod = Nothing

            If radAdvance.Checked = False Then
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'If radSimpleInterest.Checked = False AndAlso _
                '    radReduceBalInterest.Checked = False AndAlso _
                '    radNoInterest.Checked = False Then
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, Interest calculation type is mandatory information. Please set interest calculation type to continue."), enMsgBoxStyle.Information)
                If CInt(cboLoanCalcType.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, Loan calculation type is mandatory information. Please select loan calculation type to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'If CInt(cboLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest AndAlso CInt(cboInterestCalcType.SelectedValue) <= 0 Then
                If Not (CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest OrElse CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head) AndAlso CInt(cboInterestCalcType.SelectedValue) <= 0 Then
                    'Sohail (29 Apr 2019) -- End
                    'Sohail (15 Dec 2015) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Interest calculation type is mandatory information. Please select interest calculation type to continue."), enMsgBoxStyle.Information)
                    Return False

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head AndAlso CInt(cboMappedHead.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Please select Mapped Head to continue."), enMsgBoxStyle.Information)
                    cboMappedHead.Focus()
                    Return False
                    'Sohail (29 Apr 2019) -- End

                End If

                'Sohail (17 Dec 2019) -- Start
                'Mukuba University # 0004115: Option for EOC to affect number of installments on loans.
                If CInt(nudDuration.Value) > 1 Then
                    Dim dtLoanEnd As Date = dtpDate.Value.Date.AddMonths(CInt(nudDuration.Value)).AddDays(-1)
                    Dim objEmpDates As New clsemployee_dates_tran
                    Dim ds As DataSet = objEmpDates.GetEmployeeEndDates(dtLoanEnd, cboEmpName.SelectedValue.ToString)
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, dtpDate.Value.Date, eZeeDate.convertDate(ds.Tables(0).Rows(0).Item("finalenddate").ToString).AddDays(1)))
                        If CInt(nudDuration.Value) > intEmpTenure Then
                            'Hemant (24 Nov 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-495 - Add an option that will allow users to import/add loan without considering the employee tenure(EOC).
                            If ConfigParameter._Object._SkipEOCValidationOnLoanTenure = False Then
                                'Hemant (24 Nov 2021) -- End
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Sorry, Loan tenure should not be greater than employee tenure month") & " [" & intEmpTenure & "].", enMsgBoxStyle.Information)
                            If nudDuration.Enabled = True Then nudDuration.Focus()
                            Return False
                            End If 'Hemant (24 Nov 2021)
                        End If
                    End If
                End If
                'Sohail (17 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Common_Validation", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    'Private Function OtherLoanValidation() As Boolean
    '    Try
    '        If radAdvance.Checked = False Then
    '            Dim xRow() As DataRow = Nothing
    '            mblnIsValidDeductionPeriod = False
    '            If radNoInterest.Checked = False Then
    '                If mdtInterest IsNot Nothing Then
    '                    xRow = mdtInterest.Select("periodunkid = '" & CInt(cboDeductionPeriod.SelectedValue) & "' AND AUD <> 'D' AND periodunkid > 0 ")
    '                    If xRow.Length <= 0 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Deduction period you set is not present in loan interest list. Please set the correct effective period to continue."), enMsgBoxStyle.Information)
    '                        mblnIsValidDeductionPeriod = True
    '                        Return False
    '                    End If
    '                End If
    '            End If

    '            If mdtEMI IsNot Nothing Then
    '                xRow = mdtEMI.Select("periodunkid = '" & CInt(cboDeductionPeriod.SelectedValue) & "' AND AUD <> 'D' AND periodunkid > 0 ")
    '                If xRow.Length <= 0 Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Deduction period you set is not present in loan EMI list. Please set the correct effective period to continue."), enMsgBoxStyle.Information)
    '                    mblnIsValidDeductionPeriod = True
    '                    Return False
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "OtherLoanValidation", mstrModuleName)
    '    Finally
    '    End Try
    '    Return True
    'End Function

    Private Function IsValidate() As Boolean
        Try
            If ConfigParameter._Object._LoanVocNoType = 0 Then
                If txtVoucherNo.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Voucher No. cannot be blank. Voucher No. is compulsory information."), enMsgBoxStyle.Information)
                    txtVoucherNo.Focus()
                    Return False
                End If
            End If

            If CInt(cboEmpName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmpName.Focus()
                Return False
            End If

            If radLoan.Checked = True Then
                If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), enMsgBoxStyle.Information)
                    cboLoanScheme.Focus()
                    Return False
                End If

                If txtLoanAmt.Decimal = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Loan Amount cannot be blank. Loan Amount is compulsory information."), enMsgBoxStyle.Information)
                    txtLoanAmt.Focus()
                    Return False
                End If

                'Sohail (21 Mar 2018) -- Start
                'TRA Issue - Support Issue Id # 0002126 : Duplicate loan transactions are coming at time of salary in 70.2.
                'Sohail (14 Jul 2018) -- Start
                'Issue : Audit Trail Issues in Loan module in 72.1.
                'If objLoan_Advance.GetExistLoan(CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), True) = True Then
                If menAction <> enAction.EDIT_ONE AndAlso objLoan_Advance.GetExistLoan(CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), True) = True Then
                    'Sohail (14 Jul 2018) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, you can not assign loan for the selected scheme: Reason, The selected loan scheme is already assigned to you and having (In-progress or On hold) status. Instead of applying new loan, You can add top-up amount from Loan Advance List-> Operation -> Other Loan Operations."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Sohail (21 Mar 2018) -- End

            ElseIf radAdvance.Checked = True Then
                If txtAdvanceAmt.Decimal = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Advance Amount cannot be blank. Advance Amount is compulsory information."), enMsgBoxStyle.Information)
                    txtAdvanceAmt.Focus()
                    Return False
                End If

                'Sohail (16 May 2018) -- Start
                'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
                'Sohail (14 Jul 2018) -- Start
                'Issue : Audit Trail Issues in Loan module in 72.1.
                'If ConfigParameter._Object._Advance_NetPayPercentage > 0 AndAlso objLoan_Advance.GetExistLoan(0, CInt(cboEmpName.SelectedValue), False) = True Then
                If ConfigParameter._Object._Advance_NetPayPercentage > 0 AndAlso menAction <> enAction.EDIT_ONE AndAlso objLoan_Advance.GetExistLoan(0, CInt(cboEmpName.SelectedValue), False) = True Then
                    'Sohail (14 Jul 2018) -- End
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry, you can not assign Advance: Reason, The Advance is already assigned to you and having (In-progress or On hold) status."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Sohail (16 May 2018) -- End

            End If

            If Common_Validation() = False Then Return False

            Dim dtemp() As DataRow = Nothing
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'If radSimpleInterest.Checked = True Or radReduceBalInterest.Checked = True Then
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest OrElse CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount OrElse CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                'Sohail (15 Dec 2015) -- End
                If txtLoanRate.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, Interest rate is mandatory information. Please enter interest rate."), enMsgBoxStyle.Information)
                    txtLoanRate.Focus()
                    Return False
                End If

                'If mdtInterest IsNot Nothing Then
                '    dtemp = mdtInterest.Select("AUD <> 'D' AND periodunkid > 0")
                '    If dtemp.Length <= 0 Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Sorry, Interest rate is mandatory information. Please add interest rate on the list."), enMsgBoxStyle.Information)
                '        Return False
                '    End If
                'End If
            End If

            'dtemp = mdtEMI.Select("AUD <> 'D' AND periodunkid > 0")
            'If dtemp.Length <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Sorry, EMI is mandatory information. Please add emi on the list."), enMsgBoxStyle.Information)
            '    Return False
            'End If

            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            Dim objLoanScheme As New clsLoan_Scheme
            objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(nudDuration.Value) > objLoanScheme._MaxNoOfInstallment Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Language.getMessage(mstrModuleName, 27, " for ") & objLoanScheme._Name & Language.getMessage(mstrModuleName, 28, " Scheme."), enMsgBoxStyle.Information)
                nudDuration.Focus()
                Exit Function
            End If
            'Varsha (25 Nov 2017) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Private Sub FillHistoryList()
        Dim dsHistory As New DataSet
        Try
            'dsHistory = objLoan_Advance.Calculate_LoanBalanceInfo("List", ConfigParameter._Object._CurrentDateAndTime.Date, CInt(cboEmpName.SelectedValue).ToString, mintLoanAdvanceUnkid, objLoan_Advance._Effective_Date)
            ''dsHistory = objLoan_Advance.GetLoanAdvance_History_List(CInt(cboEmpName.SelectedValue), ConfigParameter._Object._CurrentDateAndTime.Date)

            'dgvHistory.AutoGenerateColumns = False
            'dgcolhLoanScheme.DataPropertyName = "lnScheme"
            'dgcolhVoucherNo.DataPropertyName = "lnVocNo"
            'dgcolhDate.DataPropertyName = "lnvDate"
            'dgcolhAmount.DataPropertyName = "lnAmount"
            'dgcolhNetAmount.DataPropertyName = "lnNetAmount"
            'dgcolhRepayment.DataPropertyName = "lnPaid"
            'dgcolhBalanceAmt.DataPropertyName = "lnBalance"
            'dgcolhStatus.DataPropertyName = "lnStatus"
            'dgvHistory.DataSource = dsHistory.Tables(0)

            'dgcolhAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            'dgcolhNetAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            'dgcolhRepayment.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            'dgcolhBalanceAmt.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

            'dgcolhAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            'dgcolhNetAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            'dgcolhRepayment.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            'dgcolhBalanceAmt.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            'dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            'dgcolhNetAmount.DefaultCellStyle.Format = GUI.fmtCurrency
            'dgcolhRepayment.DefaultCellStyle.Format = GUI.fmtCurrency
            'dgcolhBalanceAmt.DefaultCellStyle.Format = GUI.fmtCurrency

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillHistoryList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_InterestRate_Grid()
        Try
            mdtInterest = objlnInterest.GetList("List", mintLoanAdvanceUnkid).Tables(0)

            dgvInterestHistory.AutoGenerateColumns = False

            dgcolhInterestPeriod.DataPropertyName = "dperiod"
            dgcolhInterestEffectiveDate.DataPropertyName = "ddate"
            dgcolhInterestRate.DataPropertyName = "interest_rate"
            objdgcolhlninteresttranunkid.DataPropertyName = "lninteresttranunkid"
            objdgcolhIGUID.DataPropertyName = "GUID"
            objdgcolhIPStatusId.DataPropertyName = "pstatusid"

            dgvInterestHistory.DataSource = mdtInterest

            dgcolhInterestRate.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_InterestRate_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_EMI_Transaction_Grid()
        Try
            mdtEMI = objlnEMI.GetList("List", mintLoanAdvanceUnkid).Tables(0)

            dgvEMIHistory.AutoGenerateColumns = False

            dgcolhEMIPeriod.DataPropertyName = "dperiod"
            dgcolhEMIeffectivedate.DataPropertyName = "ddate"
            dgcolhInstallmentAmt.DataPropertyName = "emi_amount"
            dgcolhEMINoofInstallment.DataPropertyName = "emi_tenure"
            objdgcolhlnemitranunkid.DataPropertyName = "lnemitranunkid"
            objdgcolhEGUID.DataPropertyName = "GUID"
            objdgcolhEPstatusid.DataPropertyName = "pstatusid"
            dgcolhloan_duration.DataPropertyName = "loan_duration"

            dgvEMIHistory.DataSource = mdtEMI

            dgcolhEMINoofInstallment.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhEMINoofInstallment.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

            dgcolhInstallmentAmt.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhInstallmentAmt.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhInstallmentAmt.DefaultCellStyle.Format = GUI.fmtCurrency

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_EMI_Transaction_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Topup_Transaction_Grid()
        Try
            mdtTopup = objlnTopUp.GetList("List", mintLoanAdvanceUnkid).Tables(0)

            dgvTopupHistory.AutoGenerateColumns = False

            dgcolhTopupPeriod.DataPropertyName = "dperiod"
            dgColhTopupEffectiveDate.DataPropertyName = "ddate"
            dgcolhTopupAmount.DataPropertyName = "topup_amount"
            dgcolhTopupInterest.DataPropertyName = "interest_amount"
            objdgcolhlntopuptranunkid.DataPropertyName = "lntopuptranunkid"
            objdgcolhTGUID.DataPropertyName = "GUID"
            objdgcolhTPstatusid.DataPropertyName = "pstatusid"

            dgvTopupHistory.DataSource = mdtTopup

            dgcolhTopupAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhTopupInterest.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight

            dgcolhTopupAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhTopupInterest.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgcolhTopupAmount.DefaultCellStyle.Format = GUI.fmtCurrency

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Topup_Transaction_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Transaction_Grids()
        Try
            '****************************************************** INTEREST DATA HISTORY GRID
            Call Fill_InterestRate_Grid()
            '****************************************************** EMI DATA HISTORY GRID
            Call Fill_EMI_Transaction_Grid()
            '****************************************************** TOPUP DATA HISTORY GRID
            Call Fill_Topup_Transaction_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Transaction_Grids", mstrModuleName)
        Finally
        End Try
    End Sub

    'Nilay (28-Aug-2015) -- Start
    'Private Sub Calculate_Projected_Loan_Amount()
    '    Try
    '        Dim xdecNetAmount, xdecInterest_Amount, xRate As Decimal
    '        xdecNetAmount = 0 : xdecInterest_Amount = 0 : xRate = 0
    '        Dim xDuration As Integer : xDuration = 0

    '        Dim xtmp() As DataRow = Nothing

    '        'If mdtInterest IsNot Nothing Then
    '        '    xtmp = mdtInterest.Select("periodunkid = '" & CInt(cboDeductionPeriod.SelectedValue) & "' AND AUD <> 'D'")
    '        '    If xtmp.Length > 0 Then
    '        '        xRate = CDec(xtmp(0).Item("interest_rate"))
    '        '    End If
    '        'End If
    '        xRate = txtLoanRate.Decimal

    '        'If mdtEMI IsNot Nothing Then
    '        '    xtmp = mdtEMI.Select("periodunkid = '" & CInt(cboDeductionPeriod.SelectedValue) & "' AND AUD <> 'D'")
    '        '    If xtmp.Length > 0 Then
    '        '        xDuration = CInt(xtmp(0).Item("loan_duration"))
    '        '    End If
    '        'End If
    '        xDuration = CInt(nudDuration.Value)

    '        If radSimpleInterest.Checked Then
    '            objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Simple_Interest, txtLoanAmt.Decimal, xDuration, xRate, xdecNetAmount, xdecInterest_Amount)
    '            txtInterestAmt.Decimal = CDec(Format(CDec(xdecInterest_Amount), GUI.fmtCurrency))
    '            txtNetAmount.Decimal = CDec(Format(CDec(xdecNetAmount), GUI.fmtCurrency))
    '        ElseIf radReduceBalInterest.Checked = True Then
    '            objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Reducing_Amount, txtLoanAmt.Decimal, xDuration, xRate, xdecNetAmount, xdecInterest_Amount)
    '            txtInterestAmt.Decimal = CDec(Format(CDec(xdecInterest_Amount), GUI.fmtCurrency))
    '            txtNetAmount.Decimal = CDec(Format(CDec(xdecNetAmount), GUI.fmtCurrency))
    '        ElseIf radNoInterest.Checked Then
    '            txtInterestAmt.Decimal = 0
    '            txtNetAmount.Decimal = 0
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "DisplayProjectedAmount", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'Nilay (28-Aug-2015) -- End

    Private Sub Calculate_Projected_Loan_Balance(ByVal eLnCalcType As enLoanCalcId _
                                                 , ByVal eIntRateCalcTypeID As enLoanInterestCalcType _
                                                 , Optional ByVal blnIsDurationChange As Boolean = False _
                                                 )
        'Sohail (15 Dec 2015) - [eIntRateCalcTypeID]
        Try
            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'If eLnCalcType <> enLoanCalcId.No_Interest Then
            'Sohail (15 Dec 2015) -- End

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'Dim mdecInstallmentAmount As Decimal = 0
            'Dim mdecInstrAmount As Decimal = 0
            Dim decInstallmentAmount As Decimal = 0
            Dim decInstrAmount As Decimal = 0
            'Nilay (15-Dec-2015) -- End

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            Dim decTotInstallmentAmount As Decimal = 0
            Dim decTotIntrstAmount As Decimal = 0
            'Sohail (15 Dec 2015) -- End

                Dim dtEndDate As Date = DateAdd(DateInterval.Month, nudDuration.Value, dtpDate.Value.Date)

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'objLoan_Advance.Calulate_Projected_Loan_Balance(txtLoanAmt.Decimal, CInt(DateDiff(DateInterval.Day, dtpDate.Value.Date, dtEndDate.Date)), txtLoanRate.Decimal, eLnCalcType, CInt(nudDuration.Value), mdecInstrAmount, mdecInstallmentAmount)
            Dim objApprovalTran As New clsloanapproval_process_Tran
            Dim objLoanApplication As New clsProcess_pending_loan
            objApprovalTran._Pendingloantranunkid = mintPendingApprovalTranID
            objLoanApplication._Processpendingloanunkid = objApprovalTran._Processpendingloanunkid
            Dim dtFPeriodStart As Date = dtEndDate
            Dim dtFPeriodEnd As Date = dtEndDate
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
                dtFPeriodStart = objPeriod._Start_Date
                dtFPeriodEnd = objPeriod._End_Date
            End If

            'Sohail (29 Jan 2016) -- Start
            'Enhancement - New Loan Status filter "In Progress with Loan Deducted" for Loan Report.
            'objLoan_Advance.Calulate_Projected_Loan_Balance(txtLoanAmt.Decimal _
            '                                                , CInt(DateDiff(DateInterval.Day, dtpDate.Value.Date, dtEndDate.Date.AddDays(1))) _
            '                                                , txtLoanRate.Decimal _
            '                                                , eLnCalcType _
            '                                                , eIntRateCalcTypeID _
            '                                                , CInt(nudDuration.Value) _
            '                                                , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
            '                                                , objApprovalTran._Installmentamt _
            '                                                , decInstrAmount _
            '                                                , decInstallmentAmount _
            '                                                , decTotIntrstAmount _
            '                                                , decTotInstallmentAmount _
            '                                                )
            objLoan_Advance.Calulate_Projected_Loan_Balance(txtLoanAmt.Decimal _
                                                            , CInt(DateDiff(DateInterval.Day, dtpDate.Value.Date, dtEndDate.Date)) _
                                                            , txtLoanRate.Decimal _
                                                            , eLnCalcType _
                                                            , eIntRateCalcTypeID _
                                                            , CInt(nudDuration.Value) _
                                                            , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
                                                            , Convert.ToDecimal(IIf(eLnCalcType = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, txtPrincipalAmt.Decimal, txtInstallmentAmt.Decimal)) _
                                                            , decInstrAmount _
                                                            , decInstallmentAmount _
                                                            , decTotIntrstAmount _
                                                            , decTotInstallmentAmount _
                                                            )
            'Hemant (12 Nov 2021) -- [objApprovalTran._Installmentamt --> Convert.ToDecimal(IIf(eLnCalcType = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, txtPrincipalAmt.Decimal, txtInstallmentAmt.Decimal))  ]
            'Sohail (29 Jan 2016) -- End
            'Sohail (15 Dec 2015) -- End

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'txtInterestAmt.Tag = mdecInstrAmount
            'txtInterestAmt.Decimal = CDec(Format(mdecInstrAmount, GUI.fmtCurrency))
            txtInterestAmt.Tag = decTotIntrstAmount
            txtInterestAmt.Decimal = CDec(Format(decTotIntrstAmount, GUI.fmtCurrency))
            'Sohail (15 Dec 2015) -- End

                'Nilay (18-Nov-2015) -- Start
                'RemoveHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                'Nilay (18-Nov-2015) -- End

            txtInstallmentAmt.Tag = decInstallmentAmount
            txtInstallmentAmt.Decimal = CDec(Format(decInstallmentAmount, GUI.fmtCurrency))
                'Nilay (18-Nov-2015) -- Start
                'AddHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                'Nilay (18-Nov-2015) -- End

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            txtPrincipalAmt.Text = Format(decInstallmentAmount - decInstrAmount, GUI.fmtCurrency)
            txtIntAmt.Text = Format(decInstrAmount, GUI.fmtCurrency)
            'Sohail (15 Dec 2015) -- End

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'txtNetAmount.Tag = CDec(txtLoanAmt.Decimal + mdecInstrAmount)
            'txtNetAmount.Decimal = CDec(Format(CDec(txtLoanAmt.Decimal + mdecInstrAmount), GUI.fmtCurrency))
            txtNetAmount.Tag = decTotInstallmentAmount + decTotIntrstAmount
            txtNetAmount.Decimal = CDec(Format(decTotInstallmentAmount + decTotIntrstAmount, GUI.fmtCurrency))
            'Else
            '    Dim intNoOfEMI As Integer = 1
            '    If blnIsDurationChange = False Then
            '        If txtInstallmentAmt.Decimal > 0 Then
            '            intNoOfEMI = CInt(IIf(txtInstallmentAmt.Decimal = 0, 0, txtLoanAmt.Decimal / txtInstallmentAmt.Decimal))
            '        Else
            '            intNoOfEMI = 1
            '        End If

            '        'If CInt(CDec(IIf(txtInstallmentAmt.Decimal = 0, 0, txtLoanAmt.Decimal / txtInstallmentAmt.Decimal))) > intNoOfEMI Then
            '        '    intNoOfEMI += 1
            '        'End If   'Sandeep (12 Dec 2015)

            '        RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
            '        nudDuration.Value = intNoOfEMI
            '        AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
            '    Else
            '        Dim decInstallmentAmt As Decimal
            '        If intNoOfEMI > 0 Then
            '            decInstallmentAmt = CDec(IIf(intNoOfEMI = 0, 0, txtLoanAmt.Decimal / nudDuration.Value))
            '        Else
            '            decInstallmentAmt = 0
            '        End If

            '        'Nilay (18-Nov-2015) -- Start
            '        'RemoveHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
            '        RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            '        'Nilay (18-Nov-2015) -- End

            '        txtInstallmentAmt.Tag = decInstallmentAmt
            '        txtInstallmentAmt.Decimal = CDec(Format(decInstallmentAmt, GUI.fmtCurrency))
            '        'Nilay (18-Nov-2015) -- Start
            '        'AddHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
            '        AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            '        'Nilay (18-Nov-2015) -- End

            '    End If
            'End If
            'Sohail (15 Dec 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Calculate_Projected_Loan_Balance", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ShowHideProjectedLoan()
        Try
            If menAction = enAction.EDIT_ONE Then
                lnProjectedAmount.Visible = False
                pnlProjectedAmt.Visible = False
                pnlInformation.Visible = False
                elProjectedINSTLAmt.Visible = False
                lblPurpose.Location = New Point(435, 36)
                txtPurpose.Location = New Point(531, 36) : txtPurpose.Size = CType(New Point(238, 231), Drawing.Size)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ShowHideProjectedLoan", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 7, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefauSetRegularFontltSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Apr 2019) -- End

#End Region

#Region " Radio button Event "

    Private Sub radLoan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radLoan.CheckedChanged, radAdvance.CheckedChanged
        Try
            If CType(sender, RadioButton).Name.ToUpper = "RADLOAN" Then
                lblLoanAmt.Visible = True
                txtLoanAmt.Visible = True
                lblAdvanceAmt.Visible = False
                txtAdvanceAmt.Visible = False
            ElseIf CType(sender, RadioButton).Name.ToUpper = "RADADVANCE" Then
                lblLoanAmt.Visible = False
                txtLoanAmt.Visible = False
                lblAdvanceAmt.Visible = True
                txtAdvanceAmt.Visible = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radLoan_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    'Private Sub radSimpleInterest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radSimpleInterest.CheckedChanged
    '    Try
    '        If radSimpleInterest.Checked = True Then
    '            'Nilay (15-Dec-2015) -- Start
    '            nudDuration.Enabled = True
    '            'Nilay (15-Dec-2015) -- End

    '            mintLastCalcTypeId = enLoanCalcId.Simple_Interest
    '            If menAction <> enAction.EDIT_ONE Then
    '                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest)
    '                Call ShowHideProjectedLoan()
    '                txtInstallmentAmt.ReadOnly = True
    '                'Nilay (28-Aug-2015) -- Start
    '                If txtLoanRate.Enabled = False Then
    '                    txtLoanRate.Enabled = True
    '                End If
    '                'Nilay (28-Aug-2015) -- End

    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "radSimpleInterest_CheckedChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub radReduceBalInterest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radReduceBalInterest.CheckedChanged
    '    Try
    '        If radReduceBalInterest.Checked = True Then
    '            'Nilay (15-Dec-2015) -- Start
    '            nudDuration.Enabled = True
    '            'Nilay (15-Dec-2015) -- End

    '            mintLastCalcTypeId = enLoanCalcId.Reducing_Amount
    '            If menAction <> enAction.EDIT_ONE Then
    '                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount)
    '                Call ShowHideProjectedLoan()
    '                txtInstallmentAmt.ReadOnly = True
    '                'Nilay (28-Aug-2015) -- Start
    '                If txtLoanRate.Enabled = False Then
    '                    txtLoanRate.Enabled = True
    '                End If
    '                'Nilay (28-Aug-2015) -- End
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "radReduceBalInterest_CheckedChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub radNoInterest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radNoInterest.CheckedChanged
    '    Try
    '        If radNoInterest.Checked = True Then
    '            'Nilay (17-Dec-2015) -- Start
    '            nudDuration.Value = mintNoOfInstallment
    '            'Nilay (17-Dec-2015) -- End

    '            'Nilay (15-Dec-2015) -- Start
    '            nudDuration.Enabled = False
    '            txtInstallmentAmt.Text = Format(mdecInstallmentAmount, GUI.fmtCurrency)
    '            'Nilay (15-Dec-2015) -- End

    '            If txtLoanRate.Decimal > 0 Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Sorry, you cannot change the loan calculation type. Reason : Interest rate is already define for this loan."), enMsgBoxStyle.Information)
    '                If mintLastCalcTypeId <> 0 Then
    '                    Select Case mintLastCalcTypeId
    '                        Case enLoanCalcId.Simple_Interest
    '                            radSimpleInterest.Checked = True
    '                        Case enLoanCalcId.Reducing_Amount
    '                            radReduceBalInterest.Checked = True
    '                    End Select
    '                End If
    '                Exit Sub
    '            End If
    '            If menAction <> enAction.EDIT_ONE Then
    '                Call ShowHideProjectedLoan()
    '                'Nilay (17-Dec-2015) -- Start
    '                'txtInstallmentAmt.ReadOnly = False
    '                txtInstallmentAmt.ReadOnly = True
    '                'Nilay (17-Dec-2015) -- End
    '                txtLoanRate.Decimal = 0
    '                txtLoanRate.Enabled = False
    '            End If
    '            mintLastCalcTypeId = enLoanCalcId.No_Interest
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "radNoInterest_CheckedChanged", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    'Sohail (15 Dec 2015) -- End

#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValidate() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objLoan_Advance._FormName = mstrModuleName
            objLoan_Advance._LoginEmployeeUnkid = 0
            objLoan_Advance._ClientIP = getIP()
            objLoan_Advance._HostName = getHostName()
            objLoan_Advance._FromWeb = False
            objLoan_Advance._AuditUserId = User._Object._Userunkid
objLoan_Advance._CompanyUnkid = Company._Object._Companyunkid
            objLoan_Advance._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Me.Cursor = Cursors.WaitCursor
            'Shani (21-Jul-2016) -- End
            If menAction = enAction.EDIT_ONE Then
                'Nilay (05-May-2016) -- Start
                'blnFlag = objLoan_Advance.Update(User._Object._Userunkid)
                blnFlag = objLoan_Advance.Update
                'Nilay (05-May-2016) -- End

            Else
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objLoan_Advance.Insert(User._Object._Userunkid, Company._Object._Companyunkid)

                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'blnFlag = objLoan_Advance.Insert(User._Object._Userunkid, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid)

                'Nilay (25-Mar-2016) -- Start
                'blnFlag = objLoan_Advance.Insert(User._Object._Userunkid, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName)
                blnFlag = objLoan_Advance.Insert(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 mdtStartDate, _
                                                 mdtEndDate, _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True)
                'Nilay (25-Mar-2016) -- End

                'Sohail (15 Dec 2015) -- End

                'S.SANDEEP [04 JUN 2015] -- END
            End If

            If blnFlag = False And objLoan_Advance._Message <> "" Then
                eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
            Else
                Dim objProcessLoan As New clsProcess_pending_loan

                With objProcessLoan
                    ._FormName = mstrModuleName
                    ._Loginemployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With

                'Nilay (10-Sept-2016) -- Start
                'Enhancement - Create New Loan Notification 
                'objProcessLoan.Send_Notification_After_Assign(FinancialYear._Object._DatabaseName, _
                '                                              User._Object._Userunkid, _
                '                                              FinancialYear._Object._YearUnkid, _
                '                                              Company._Object._Companyunkid, _
                '                                              ConfigParameter._Object._IsIncludeInactiveEmp, _
                '                                              mdtStartDate, mdtEndDate, _
                '                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                              ConfigParameter._Object._UserAccessModeSetting, _
                '                                              ConfigParameter._Object._ArutiSelfServiceURL, _
                '                                              objLoan_Advance._Loanadvancetranunkid, enLogin_Mode.DESKTOP)
                'objProcessLoan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), _
                '                                          objLoan_Advance._Loanadvancetranunkid, _
                '                                          clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
                '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                '                                          enLogin_Mode.DESKTOP, 0, 0)

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = True Then

                Dim enMailType As New clsProcess_pending_loan.enApproverEmailType

                If radLoan.Checked = True Then 'LOAN
                    enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
                ElseIf radAdvance.Checked = True Then 'ADVANCE
                    enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
                End If

                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objProcessLoan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), _
                    '                                          objLoan_Advance._Loanadvancetranunkid, _
                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
                    '                                          enMailType, _
                    '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                    '                                          enLogin_Mode.DESKTOP, 0, 0, False) 'Nilay (27-Dec-2016) -- [False]
                    objProcessLoan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), _
                                                              objLoan_Advance._Loanadvancetranunkid, _
                                                              clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
                                                              enMailType, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , _
                                                              enLogin_Mode.DESKTOP, 0, 0, False)
                    'Sohail (30 Nov 2017) -- End

                objProcessLoan.Send_Notification_After_Assign(FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                              mdtStartDate, mdtEndDate, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._UserAccessModeSetting, _
                                                              ConfigParameter._Object._ArutiSelfServiceURL, _
                                                              objLoan_Advance._Loanadvancetranunkid, _
                                                              enMailType, _
                                                              enLogin_Mode.DESKTOP, , , ConfigParameter._Object._Notify_LoanAdvance_Users)
                    'Hemant (30 Aug 2019) -- [ConfigParameter._Object._Notify_LoanAdvance_Users]

                End If
                'Nilay (10-Dec-2016) -- End

                'Nilay (10-Sept-2016) -- End

                'Shani (21-Jul-2016) -- End
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objLoan_Advance = Nothing
                    objLoan_Advance = New clsLoan_Advance
                    Call GetValue()
                    txtVoucherNo.Focus()
                Else
                    mintLoanAdvanceUnkid = objLoan_Advance._Loanadvancetranunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Me.Cursor = Cursors.Default
            'Shani (21-Jul-2016) -- End
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub ComboBox_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        Try
            e.Handled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ComboBox_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ComboBox_KeyDown(ByVal sender As Object, ByVal e As KeyEventArgs)
        Try
            If e.KeyCode = Keys.Right Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Home Or e.KeyCode = Keys.End Or e.KeyCode = Keys.Tab Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ComboBox_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboDeductionPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDeductionPeriod.SelectedIndexChanged
        'Sohail (07 May 2015) -- Start
        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        'Sohail (07 May 2015) -- End
        Try
            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objPrd._Periodunkid = CInt(cboDeductionPeriod.SelectedValue)
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboDeductionPeriod.SelectedValue)
                'Nilay (10-Oct-2015) -- End

                dtpDate.Value = objPrd._Start_Date
                dtpDate.Enabled = False
                objPrd = Nothing
            Else
                dtpDate.Enabled = True : dtpDate.Value = ConfigParameter._Object._CurrentDateAndTime
            End If

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            objlblExRate.Text = ""
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpDate.Value.Date, True)
            If dsList.Tables("ExRate").Rows.Count > 0 Then
                mdecBaseExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1"))
                'Hemant (29 Jan 2022) -- Start            
                'mdecPaidExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                mdecExchangerate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                mdecPaidExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate"))
                'Hemant (29 Jan 2022) -- End
                mintPaidCurrId = CInt(dsList.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
                'Hemant (29 Jan 2022) -- Start            
                'objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dsList.Tables("ExRate").Rows(0).Item("currency_sign").ToString & " "
                objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecExchangerate.ToString & " " & dsList.Tables("ExRate").Rows(0).Item("currency_sign").ToString & " "
                'Hemant (29 Jan 2022) -- End
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
            'Sohail (07 May 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDeductionPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
    Private Sub cboLoanCalcType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanCalcType.SelectedIndexChanged, cboInterestCalcType.SelectedIndexChanged, cboPayPeriod.SelectedIndexChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If

            'Nilay (23-Aug-2016) -- Start
            'If menAction <> enAction.EDIT_ONE Then
            '    cboInterestCalcType.Enabled = True
            'End If
            'Nilay (23-Aug-2016) -- END
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If CInt(cboLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                cboMappedHead.SelectedValue = 0
                cboMappedHead.Enabled = False
            End If
            'Sohail (29 Apr 2019) -- End

            If CInt(cboLoanCalcType.SelectedValue) > 0 Then

                Select Case CInt(cboLoanCalcType.SelectedValue)

                    Case enLoanCalcId.Simple_Interest

                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        If menAction <> enAction.EDIT_ONE Then
                            cboInterestCalcType.Enabled = True
                        End If
                        'Nilay (04-Nov-2016) -- End

                        nudDuration.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Simple_Interest

                        If menAction <> enAction.EDIT_ONE Then

                            Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)
                            Call ShowHideProjectedLoan()
                            txtInstallmentAmt.ReadOnly = True
                            If txtLoanRate.Enabled = False Then
                                txtLoanRate.Enabled = True
                            End If

                        End If

                    Case enLoanCalcId.Reducing_Amount

                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        If menAction <> enAction.EDIT_ONE Then
                            cboInterestCalcType.Enabled = True
                        End If
                        'Nilay (04-Nov-2016) -- End

                        nudDuration.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Reducing_Amount

                        If menAction <> enAction.EDIT_ONE Then

                            Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)
                            Call ShowHideProjectedLoan()
                            txtInstallmentAmt.ReadOnly = True
                            If txtLoanRate.Enabled = False Then
                                txtLoanRate.Enabled = True
                            End If

                        End If

                    Case enLoanCalcId.No_Interest

                        cboInterestCalcType.Enabled = False
                        RemoveHandler cboInterestCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
                        cboInterestCalcType.SelectedValue = 0

                        'Nilay (09-Nov-2016) -- Start
                        'If txtLoanRate.Decimal > 0 Then
                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot change the loan calculation type. Reason : Interest rate is already define for this loan."), enMsgBoxStyle.Information)
                        '    If mintLastCalcTypeId <> 0 Then
                        '        Select Case mintLastCalcTypeId
                        '            Case enLoanCalcId.Simple_Interest
                        '                cboLoanCalcType.SelectedValue = enLoanCalcId.Simple_Interest

                        '            Case enLoanCalcId.Reducing_Amount
                        '                cboLoanCalcType.SelectedValue = enLoanCalcId.Reducing_Amount
                        '        End Select
                        '    End If
                        '    Exit Sub
                        'End If
                        'Nilay (09-Nov-2016) -- End

                        If menAction <> enAction.EDIT_ONE Then
                            'Hemant (17 Oct 2020) -- Start
                            'Issue : AH1108-HYATT TZ-The Holded loan not deducting after changing status to in progress
                            Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest, enIntCalcType)
                            'Hemant (17 Oct 2020) -- End
                            'S.SANDEEP [20-SEP-2017] -- START
                            'ISSUE/ENHANCEMENT : REF-ID # 50
                            'nudDuration.Value = mintNoOfInstallment
                            nudDuration.Value = CDec(IIf(mintNoOfInstallment <= 0, 1, mintNoOfInstallment))
                            'S.SANDEEP [20-SEP-2017] -- END
                            nudDuration.Enabled = False
                            txtInstallmentAmt.Text = Format(mdecInstallmentAmount, GUI.fmtCurrency)
                            Call ShowHideProjectedLoan()
                            txtInstallmentAmt.ReadOnly = False
                            txtLoanRate.Decimal = 0
                            txtLoanRate.Enabled = False
                        End If

                        mintLastCalcTypeId = enLoanCalcId.No_Interest

                    Case enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI

                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        If menAction <> enAction.EDIT_ONE Then
                            cboInterestCalcType.Enabled = True
                        End If
                        'Nilay (04-Nov-2016) -- End

                        nudDuration.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI

                        If menAction <> enAction.EDIT_ONE Then

                            Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)
                            Call ShowHideProjectedLoan()
                            txtInstallmentAmt.ReadOnly = True
                            If txtLoanRate.Enabled = False Then
                                txtLoanRate.Enabled = True
                            End If

                        End If

                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    Case enLoanCalcId.No_Interest_With_Mapped_Head

                        cboInterestCalcType.Enabled = False
                        RemoveHandler cboInterestCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
                        cboInterestCalcType.SelectedValue = 0

                        If menAction <> enAction.EDIT_ONE Then
                            nudDuration.Value = CDec(IIf(mintNoOfInstallment <= 0, 1, mintNoOfInstallment))
                            nudDuration.Enabled = False
                            txtInstallmentAmt.Text = Format(mdecInstallmentAmount, GUI.fmtCurrency)
                            Call ShowHideProjectedLoan()
                            txtInstallmentAmt.ReadOnly = False
                            txtLoanRate.Decimal = 0
                            txtLoanRate.Enabled = False
                            cboMappedHead.Enabled = True
                        End If

                        mintLastCalcTypeId = enLoanCalcId.No_Interest_With_Mapped_Head
                        'Sohail (29 Apr 2019) -- End

                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanCalcType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (15 Dec 2015) -- End

    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Private Sub cboMappedHead_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboMappedHead.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    .CodeMember = "code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboMappedHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMappedHead.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboMappedHead_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMappedHead.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboMappedHead_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMappedHead.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (29 Apr 2019) -- End

#End Region

#Region " Numeric Up Down Event "

    Private Sub nudDuration_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudDuration.ValueChanged
        Try
            If mblnIsFormLoad = True Then
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
                'If radSimpleInterest.Checked = True Then
                '    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest)
                'ElseIf radReduceBalInterest.Checked = True Then
                '    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount)
                'ElseIf radNoInterest.Checked = True Then
                '    'Nilay (15-Dec-2015) -- Start
                '    'Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, True)
                '    'Nilay (15-Dec-2015) -- End
                'End If
                Dim enIntCalcType As enLoanInterestCalcType
                If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                    enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
                Else
                    enIntCalcType = enLoanInterestCalcType.MONTHLY
                End If
                If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                    'Nilay (15-Dec-2015) -- Start
                    'Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, True)
                    'Nilay (15-Dec-2015) -- End

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                    'Hemant (12 Nov 2021) -- Start
                    'ISSUE(REA) : on changing No of installment of emi , emi amount should get changed too but it remained unchanged.
                    txtPrincipalAmt.Text = Format(txtLoanAmt.Decimal / nudDuration.Value, GUI.fmtCurrency)
                    'Hemant (12 Nov 2021) -- End
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                    'Sohail (29 Apr 2019) -- End

                End If
                'Sohail (15 Dec 2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudDuration_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Events "

    Private Sub txtInstallmentAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            If mblnIsFormLoad = True Then
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
                'If radSimpleInterest.Checked = True Then
                '    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest)
                'ElseIf radReduceBalInterest.Checked = True Then
                '    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount)
                'ElseIf radNoInterest.Checked = True Then
                '    'Nilay (17-Dec-2015) -- Start
                '    'Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest)
                '    'Nilay (17-Dec-2015) -- End
                'End If
                Dim enIntCalcType As enLoanInterestCalcType
                If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                    enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
                Else
                    enIntCalcType = enLoanInterestCalcType.MONTHLY
                End If
                If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest_With_Mapped_Head, enIntCalcType)
                    'Sohail (29 Apr 2019) -- End

                End If
                'Sohail (15 Dec 2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtInstallmentAmt_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtLoanRate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanRate.TextChanged
        Try
            If mblnIsFormLoad = True Then
                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Pricipal EMI and new interest calculation Monthly apart from Daily for KBC.
                'If radSimpleInterest.Checked = True Then
                '    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest)
                'ElseIf radReduceBalInterest.Checked = True Then
                '    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount)
                'End If
                Dim enIntCalcType As enLoanInterestCalcType
                If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                    enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
                Else
                    enIntCalcType = enLoanInterestCalcType.MONTHLY
                End If
                If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                End If
                'Sohail (15 Dec 2015) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtLoanRate_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGridView Event(s) "

    'Private Sub dgvInterestHistory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvInterestHistory.CellClick
    '    Dim frm As New frmAddEditLoanParameters
    '    Try
    '        If User._Object._Isrighttoleft = True Then
    '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            frm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(frm)
    '        End If

    '        If e.RowIndex <= -1 Then Exit Sub
    '        Dim xCalTypeId As enLoanCalcId
    '        If radLoan.Checked Then
    '            If radSimpleInterest.Checked Then
    '                xCalTypeId = enLoanCalcId.Simple_Interest '1
    '            ElseIf radReduceBalInterest.Checked Then
    '                xCalTypeId = enLoanCalcId.Reducing_Amount '3
    '            ElseIf radNoInterest.Checked Then
    '                xCalTypeId = enLoanCalcId.No_Interest   '4
    '            End If
    '        End If

    '        Select Case e.ColumnIndex
    '            Case objdgcolhIAdd.Index
    '                If menAction <> enAction.EDIT_ONE Then
    '                    If mblnIsValidDeductionPeriod = True Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Please use edit operion from list and set correct deduction period to continue."), enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If
    '                End If
    '                If dgvInterestHistory.Rows(e.RowIndex).Cells(objdgcolhIAdd.Index).Value Is imgBlank Then Exit Sub
    '                If Common_Validation() = False Then
    '                    Exit Sub
    '                End If

    '                If radNoInterest.Checked = True Then
    '                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, You are not allowed to add interest rate for No Interest Loans."), enMsgBoxStyle.Information)
    '                    Exit Sub
    '                End If

    '                If menAction = enAction.EDIT_ONE Then
    '                    If radSimpleInterest.Checked = True Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, You are not allowed to add new interest rate for Simple Interest Loans."), enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If
    '                End If

    '                Dim xtmp() As DataRow = Nothing
    '                If radSimpleInterest.Checked = True Then
    '                    If mdtInterest IsNot Nothing AndAlso mdtInterest.Rows.Count > 0 Then
    '                        xtmp = mdtInterest.Select("periodunkid > 0 AND AUD <> 'D'")
    '                        If xtmp.Length > 0 Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Interest Rate is already present for simple interest. You are not allowed to add new interest rate for Simple Interest Loans."), enMsgBoxStyle.Information)
    '                            Exit Sub
    '                        End If
    '                    End If
    '                End If

    '                If menAction = enAction.EDIT_ONE Then
    '                    frm._PeriodUnkid = mintCurrOpenPeriodId
    '                Else
    '                    frm._PeriodUnkid = CInt(cboDeductionPeriod.SelectedValue)
    '                End If

    '                xtmp = mdtInterest.Select("periodunkid > 0 AND AUD <> 'D'")
    '                Dim blnAllowEdit As Boolean = True
    '                If xtmp.Length > 0 Then
    '                    blnAllowEdit = True
    '                Else
    '                    blnAllowEdit = False
    '                End If

    '                If frm.displayDialog(-1, enParameterMode.LN_RATE, mintLoanAdvanceUnkid, xCalTypeId, enAction.ADD_ONE, CBool(IIf(menAction = enAction.EDIT_ONE, True, blnAllowEdit))) Then
    '                    If menAction <> enAction.EDIT_ONE Then
    '                        xtmp = mdtInterest.Select("periodunkid = '" & CInt(cboDeductionPeriod.SelectedValue) & "' AND AUD <> 'D'")
    '                        If xtmp.Length <= 0 Then
    '                            If frm._PeriodUnkid <> CInt(cboDeductionPeriod.SelectedValue) Then
    '                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Sorry, Selected Period does not match with the deduction Period. Please select correct deduction period."), enMsgBoxStyle.Information)
    '                                Exit Sub
    '                            End If
    '                        End If
    '                    End If
    '                    xtmp = mdtInterest.Select("periodunkid = '" & frm._PeriodUnkid & "' AND AUD <> 'D'")
    '                    If xtmp.Length > 0 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Interest Rate is already present for the selected period."), enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If

    '                    If frm._PeriodUnkid > 0 Then
    '                        Dim objTnaLeaveTran As New clsTnALeaveTran
    '                        Dim objPeriod As New clscommom_period_Tran
    '                        objPeriod._Periodunkid = frm._PeriodUnkid

    '                        If objTnaLeaveTran.IsPayrollProcessDone(frm._PeriodUnkid, cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot add this Interest Rate Information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), enMsgBoxStyle.Information)
    '                            objPeriod = Nothing
    '                            objTnaLeaveTran = Nothing
    '                            Exit Sub
    '                        End If
    '                        objTnaLeaveTran = Nothing : objPeriod = Nothing
    '                    End If

    '                    Dim dRow As DataRow = mdtInterest.NewRow()
    '                    dRow.Item("lninteresttranunkid") = -1
    '                    dRow.Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
    '                    'dRow.Item("lntopuptranunkid") = -1
    '                    dRow.Item("periodunkid") = frm._PeriodUnkid
    '                    dRow.Item("effectivedate") = frm._EffectiveDate
    '                    dRow.Item("interest_rate") = frm._InterestRate
    '                    dRow.Item("userunkid") = User._Object._Userunkid
    '                    dRow.Item("isvoid") = False
    '                    dRow.Item("voiduserunkid") = -1
    '                    dRow.Item("voiddatetime") = DBNull.Value
    '                    dRow.Item("voidreason") = ""
    '                    dRow.Item("AUD") = "A"
    '                    dRow.Item("GUID") = Guid.NewGuid.ToString()
    '                    dRow.Item("dperiod") = frm._PeriodName
    '                    dRow.Item("ddate") = frm._EffectiveDate.Date.ToShortDateString
    '                    dRow.Item("pstatusid") = enStatusType.Open
    '                    mdtInterest.Rows.Add(dRow)
    '                    Call Fill_InterestRate_Grid()
    '                End If
    '            Case objdgcolhIEdit.Index
    '                If dgvInterestHistory.Rows(e.RowIndex).Cells(objdgcolhIEdit.Index).Value Is imgBlank Then Exit Sub
    '                Dim xtmp() As DataRow = Nothing
    '                If CInt(dgvInterestHistory.CurrentRow.Cells(objdgcolhlninteresttranunkid.Index).Value) > 0 Then
    '                    xtmp = mdtInterest.Select("lninteresttranunkid = '" & dgvInterestHistory.CurrentRow.Cells(objdgcolhlninteresttranunkid.Index).Value.ToString & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                Else
    '                    xtmp = mdtInterest.Select("GUID = '" & dgvInterestHistory.CurrentRow.Cells(objdgcolhIGUID.Index).Value.ToString & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                End If
    '                If xtmp.Length > 0 Then
    '                    If CInt(xtmp(0).Item("lninteresttranunkid").ToString) > 0 Then
    '                        Dim objTnaLeaveTran As New clsTnALeaveTran
    '                        Dim objPeriod As New clscommom_period_Tran
    '                        objPeriod._Periodunkid = CInt(xtmp(0).Item("periodunkid"))

    '                        If objTnaLeaveTran.IsPayrollProcessDone(CInt(xtmp(0).Item("periodunkid")), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot edit assiged Interest Information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), enMsgBoxStyle.Information)
    '                            objPeriod = Nothing
    '                            objTnaLeaveTran = Nothing
    '                            Exit Sub
    '                        End If
    '                        objTnaLeaveTran = Nothing : objPeriod = Nothing
    '                    End If

    '                    frm._PeriodUnkid = CInt(xtmp(0).Item("periodunkid"))
    '                    frm._EffectiveDate = CDate(xtmp(0).Item("effectivedate"))
    '                    frm._InterestRate = CDec(xtmp(0).Item("interest_rate"))
    '                    If frm.displayDialog(CInt(xtmp(0).Item("lninteresttranunkid")), _
    '                                         enParameterMode.LN_RATE, _
    '                                         mintLoanAdvanceUnkid, _
    '                                         xCalTypeId, _
    '                                         enAction.EDIT_ONE, _
    '                                         CBool(IIf(CInt(cboDeductionPeriod.SelectedValue) = CInt(xtmp(0).Item("periodunkid")), False, True))) Then


    '                        If menAction <> enAction.EDIT_ONE Then
    '                            If frm._PeriodUnkid <> CInt(cboDeductionPeriod.SelectedValue) Then
    '                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Sorry, Selected Period does not match with the deduction Period. Please select correct deduction period."), enMsgBoxStyle.Information)
    '                                Exit Sub
    '                            End If
    '                        End If

    '                        Dim xFRow() As DataRow = mdtInterest.Select("periodunkid = '" & frm._PeriodUnkid & "' AND AUD <> 'D' AND periodunkid <> '" & CInt(xtmp(0).Item("periodunkid")) & "'")

    '                        If xFRow.Length > 0 Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Interest Rate is already present for the selected period."), enMsgBoxStyle.Information)
    '                            Exit Sub
    '                        End If

    '                        xtmp(0).Item("lninteresttranunkid") = xtmp(0).Item("lninteresttranunkid")
    '                        xtmp(0).Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
    '                        'xtmp(0).Item("lntopuptranunkid") = xtmp(0).Item("lntopuptranunkid")
    '                        xtmp(0).Item("periodunkid") = frm._PeriodUnkid
    '                        xtmp(0).Item("effectivedate") = frm._EffectiveDate
    '                        xtmp(0).Item("interest_rate") = frm._InterestRate
    '                        xtmp(0).Item("userunkid") = User._Object._Userunkid
    '                        xtmp(0).Item("isvoid") = False
    '                        xtmp(0).Item("voiduserunkid") = -1
    '                        xtmp(0).Item("voiddatetime") = DBNull.Value
    '                        xtmp(0).Item("voidreason") = ""
    '                        If xtmp(0).Item("AUD").ToString = "" Then
    '                            xtmp(0).Item("AUD") = "U"
    '                        End If
    '                        xtmp(0).Item("GUID") = xtmp(0).Item("GUID")
    '                        xtmp(0).Item("dperiod") = frm._PeriodName
    '                        xtmp(0).Item("ddate") = frm._EffectiveDate.Date.ToShortDateString
    '                        xtmp(0).Item("pstatusid") = xtmp(0).Item("pstatusid")
    '                        xtmp(0).AcceptChanges()
    '                        mdtInterest.AcceptChanges()
    '                        Call Fill_InterestRate_Grid()
    '                    End If
    '                End If
    '            Case objdgcolhIDelete.Index

    '                If menAction <> enAction.EDIT_ONE Then
    '                    If mblnIsValidDeductionPeriod = True Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Please use edit operion from list and set correct deduction period to continue."), enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If
    '                End If

    '                If dgvInterestHistory.Rows(e.RowIndex).Cells(objdgcolhIDelete.Index).Value Is imgBlank Then Exit Sub
    '                Dim xtmp() As DataRow = Nothing
    '                If CInt(dgvInterestHistory.CurrentRow.Cells(objdgcolhlninteresttranunkid.Index).Value) > 0 Then
    '                    xtmp = mdtInterest.Select("lninteresttranunkid = '" & dgvInterestHistory.CurrentRow.Cells(objdgcolhlninteresttranunkid.Index).Value.ToString & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                Else
    '                    xtmp = mdtInterest.Select("GUID = '" & dgvInterestHistory.CurrentRow.Cells(objdgcolhIGUID.Index).Value.ToString & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                End If
    '                If xtmp.Length > 0 Then

    '                    If CInt(cboDeductionPeriod.SelectedValue) = CInt(xtmp(0).Item("periodunkid")) Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, you cannot delete this entry. Reason : It is linked with deduction period."), enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If

    '                    If CInt(xtmp(0).Item("lninteresttranunkid").ToString) > 0 Then

    '                        If CInt(xtmp(0).Item("lninteresttranunkid").ToString) > 0 Then
    '                            Dim objTnaLeaveTran As New clsTnALeaveTran
    '                            Dim objPeriod As New clscommom_period_Tran
    '                            objPeriod._Periodunkid = CInt(xtmp(0).Item("periodunkid"))

    '                            If objTnaLeaveTran.IsPayrollProcessDone(CInt(xtmp(0).Item("periodunkid")), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot remove assiged Interest information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), enMsgBoxStyle.Information)
    '                                objPeriod = Nothing
    '                                objTnaLeaveTran = Nothing
    '                                Exit Sub
    '                            End If
    '                            objTnaLeaveTran = Nothing : objPeriod = Nothing
    '                        End If


    '                        Dim xUsedMessage As String = String.Empty
    '                        xUsedMessage = objLoan_Advance.Check_Transaction_Linking("lninteresttranunkid", CInt(xtmp(0).Item("lninteresttranunkid")), mintLoanAdvanceUnkid)

    '                        If xUsedMessage.Trim.Length > 0 Then
    '                            eZeeMsgBox.Show(xUsedMessage, enMsgBoxStyle.Information)
    '                            Exit Sub
    '                        End If

    '                        Dim xStrVoidReason As String = String.Empty
    '                        Dim rfrm As New frmReasonSelection
    '                        If User._Object._Isrighttoleft = True Then
    '                            rfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                            rfrm.RightToLeftLayout = True
    '                            Call Language.ctlRightToLeftlayOut(rfrm)
    '                        End If
    '                        rfrm.displayDialog(enVoidCategoryType.LOAN, xStrVoidReason)
    '                        If xStrVoidReason.Trim.Length <= 0 Then Exit Sub
    '                        xtmp(0).Item("isvoid") = True
    '                        xtmp(0).Item("voiduserunkid") = User._Object._Userunkid
    '                        xtmp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
    '                        xtmp(0).Item("voidreason") = xStrVoidReason
    '                        xtmp(0).Item("AUD") = "D"
    '                    Else
    '                        xtmp(0).Item("AUD") = "D"
    '                    End If
    '                    xtmp(0).AcceptChanges()
    '                    mdtInterest.AcceptChanges()
    '                    Call Fill_InterestRate_Grid()
    '                End If
    '        End Select
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvInterestHistory_CellClick", mstrModuleName)
    '    Finally
    '        If frm IsNot Nothing Then frm.Dispose()
    '    End Try
    'End Sub

    'Private Sub dgvEMIHistory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEMIHistory.CellClick
    '    Dim frm As New frmAddEditLoanParameters
    '    Try
    '        If User._Object._Isrighttoleft = True Then
    '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            frm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(frm)
    '        End If

    '        If e.RowIndex <= -1 Then Exit Sub
    '        Dim xCalTypeId As enLoanCalcId
    '        If radLoan.Checked Then
    '            If radSimpleInterest.Checked Then
    '                xCalTypeId = enLoanCalcId.Simple_Interest '1
    '            ElseIf radReduceBalInterest.Checked Then
    '                xCalTypeId = enLoanCalcId.Reducing_Amount '3
    '            ElseIf radNoInterest.Checked Then
    '                xCalTypeId = enLoanCalcId.No_Interest   '4
    '            End If
    '        End If

    '        Select Case e.ColumnIndex
    '            Case objdgcolhEAdd.Index

    '                If menAction <> enAction.EDIT_ONE Then
    '                    If mblnIsValidDeductionPeriod = True Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Please use edit operion from list and set correct deduction period to continue."), enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If
    '                End If

    '                If dgvInterestHistory.Rows(e.RowIndex).Cells(objdgcolhEAdd.Index).Value Is imgBlank Then Exit Sub
    '                If Common_Validation() = False Then
    '                    Exit Sub
    '                End If

    '                If menAction = enAction.EDIT_ONE Then
    '                    frm._PeriodUnkid = mintCurrOpenPeriodId
    '                Else
    '                    frm._PeriodUnkid = CInt(cboDeductionPeriod.SelectedValue)
    '                End If

    '                Dim xtmp() As DataRow = Nothing

    '                If frm.displayDialog(-1, enParameterMode.LN_EMI, mintLoanAdvanceUnkid, xCalTypeId, enAction.ADD_ONE) Then

    '                    If menAction <> enAction.EDIT_ONE Then
    '                        xtmp = mdtEMI.Select("periodunkid = '" & CInt(cboDeductionPeriod.SelectedValue) & "' AND AUD <> 'D'")
    '                        If xtmp.Length <= 0 Then
    '                            If frm._PeriodUnkid <> CInt(cboDeductionPeriod.SelectedValue) Then
    '                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Sorry, Selected Period does not match with the deduction Period. Please select correct deduction period."), enMsgBoxStyle.Information)
    '                                Exit Sub
    '                            End If
    '                        End If
    '                    End If

    '                    xtmp = mdtEMI.Select("periodunkid = '" & frm._PeriodUnkid & "' AND AUD <> 'D'")
    '                    If xtmp.Length > 0 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry, EMI is already present for the selected period."), enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If

    '                    If frm._PeriodUnkid > 0 Then
    '                        Dim objTnaLeaveTran As New clsTnALeaveTran
    '                        Dim objPeriod As New clscommom_period_Tran
    '                        objPeriod._Periodunkid = frm._PeriodUnkid

    '                        If objTnaLeaveTran.IsPayrollProcessDone(frm._PeriodUnkid, cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, you cannot add this EMI information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), enMsgBoxStyle.Information)
    '                            objPeriod = Nothing
    '                            objTnaLeaveTran = Nothing
    '                            Exit Sub
    '                        End If
    '                        objTnaLeaveTran = Nothing : objPeriod = Nothing
    '                    End If

    '                    Dim dRow As DataRow = mdtEMI.NewRow()
    '                    dRow.Item("lnemitranunkid") = -1
    '                    dRow.Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
    '                    dRow.Item("periodunkid") = frm._PeriodUnkid
    '                    dRow.Item("effectivedate") = frm._EffectiveDate
    '                    dRow.Item("emi_tenure") = frm._NoOfInstallment
    '                    dRow.Item("emi_amount") = frm._InstallmentAmount
    '                    dRow.Item("loan_duration") = frm._Duration
    '                    dRow.Item("userunkid") = User._Object._Userunkid
    '                    dRow.Item("isvoid") = False
    '                    dRow.Item("voiduserunkid") = -1
    '                    dRow.Item("voiddatetime") = DBNull.Value
    '                    dRow.Item("voidreason") = ""
    '                    dRow.Item("AUD") = "A"
    '                    dRow.Item("GUID") = Guid.NewGuid.ToString()
    '                    dRow.Item("dperiod") = frm._PeriodName
    '                    dRow.Item("ddate") = frm._EffectiveDate.Date.ToShortDateString
    '                    dRow.Item("pstatusid") = enStatusType.Open
    '                    mdtEMI.Rows.Add(dRow)
    '                    Call Fill_EMI_Transaction_Grid()
    '                End If
    '            Case objdgcolhEEdit.Index
    '                If dgvEMIHistory.Rows(e.RowIndex).Cells(objdgcolhEEdit.Index).Value Is imgBlank Then Exit Sub
    '                Dim xtmp() As DataRow = Nothing
    '                If CInt(dgvEMIHistory.CurrentRow.Cells(objdgcolhlnemitranunkid.Index).Value) > 0 Then
    '                    xtmp = mdtEMI.Select("lnemitranunkid = '" & dgvEMIHistory.CurrentRow.Cells(objdgcolhlnemitranunkid.Index).Value.ToString & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                Else
    '                    xtmp = mdtEMI.Select("GUID = '" & dgvEMIHistory.CurrentRow.Cells(objdgcolhEGUID.Index).Value.ToString & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                End If
    '                If xtmp.Length > 0 Then
    '                    If CInt(xtmp(0).Item("lnemitranunkid").ToString) > 0 Then
    '                        Dim objTnaLeaveTran As New clsTnALeaveTran
    '                        Dim objPeriod As New clscommom_period_Tran
    '                        objPeriod._Periodunkid = CInt(xtmp(0).Item("periodunkid"))

    '                        If objTnaLeaveTran.IsPayrollProcessDone(CInt(xtmp(0).Item("periodunkid")), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot edit assiged EMI Information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), enMsgBoxStyle.Information)
    '                            objPeriod = Nothing
    '                            objTnaLeaveTran = Nothing
    '                            Exit Sub
    '                        End If
    '                        objTnaLeaveTran = Nothing : objPeriod = Nothing
    '                    End If

    '                    frm._PeriodUnkid = CInt(xtmp(0).Item("periodunkid"))
    '                    frm._EffectiveDate = CDate(xtmp(0).Item("effectivedate"))
    '                    frm._NoOfInstallment = CInt(xtmp(0).Item("emi_tenure"))
    '                    frm._InstallmentAmount = CDec(xtmp(0).Item("emi_amount"))
    '                    frm._Duration = CInt(xtmp(0).Item("loan_duration"))
    '                    If frm.displayDialog(CInt(xtmp(0).Item("lnemitranunkid")), _
    '                                         enParameterMode.LN_EMI, _
    '                                         mintLoanAdvanceUnkid, _
    '                                         xCalTypeId, _
    '                                         enAction.EDIT_ONE, _
    '                                         CBool(IIf(CInt(cboDeductionPeriod.SelectedValue) = CInt(xtmp(0).Item("periodunkid")), False, True))) Then

    '                        If menAction <> enAction.EDIT_ONE Then
    '                            If frm._PeriodUnkid <> CInt(cboDeductionPeriod.SelectedValue) Then
    '                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Sorry, Selected Period does not match with the deduction Period. Please select correct deduction period."), enMsgBoxStyle.Information)
    '                                Exit Sub
    '                            End If
    '                        End If

    '                        Dim xFRow() As DataRow = mdtEMI.Select("periodunkid = '" & frm._PeriodUnkid & "' AND AUD <> 'D' AND periodunkid <> '" & CInt(xtmp(0).Item("periodunkid")) & "'")

    '                        If xFRow.Length > 0 Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry, EMI is already present for the selected period."), enMsgBoxStyle.Information)
    '                            Exit Sub
    '                        End If

    '                        xtmp(0).Item("lnemitranunkid") = xtmp(0).Item("lnemitranunkid")
    '                        xtmp(0).Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
    '                        xtmp(0).Item("periodunkid") = frm._PeriodUnkid
    '                        xtmp(0).Item("effectivedate") = frm._EffectiveDate
    '                        xtmp(0).Item("emi_tenure") = frm._NoOfInstallment
    '                        xtmp(0).Item("emi_amount") = frm._InstallmentAmount
    '                        xtmp(0).Item("loan_duration") = frm._Duration
    '                        xtmp(0).Item("userunkid") = User._Object._Userunkid
    '                        xtmp(0).Item("isvoid") = False
    '                        xtmp(0).Item("voiduserunkid") = -1
    '                        xtmp(0).Item("voiddatetime") = DBNull.Value
    '                        xtmp(0).Item("voidreason") = ""
    '                        If xtmp(0).Item("AUD").ToString = "" Then
    '                            xtmp(0).Item("AUD") = "U"
    '                        End If
    '                        xtmp(0).Item("GUID") = xtmp(0).Item("GUID")
    '                        xtmp(0).Item("dperiod") = frm._PeriodName
    '                        xtmp(0).Item("ddate") = frm._EffectiveDate.Date.ToShortDateString
    '                        xtmp(0).Item("pstatusid") = enStatusType.Open
    '                        xtmp(0).AcceptChanges()
    '                        mdtEMI.AcceptChanges()
    '                        Call Fill_EMI_Transaction_Grid()
    '                    End If
    '                End If
    '            Case objdgcolhEDelete.Index

    '                If menAction <> enAction.EDIT_ONE Then
    '                    If mblnIsValidDeductionPeriod = True Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Please use edit operion from list and set correct deduction period to continue."), enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If
    '                End If

    '                If dgvEMIHistory.Rows(e.RowIndex).Cells(objdgcolhEDelete.Index).Value Is imgBlank Then Exit Sub
    '                Dim xtmp() As DataRow = Nothing
    '                If CInt(dgvEMIHistory.CurrentRow.Cells(objdgcolhlnemitranunkid.Index).Value) > 0 Then
    '                    xtmp = mdtEMI.Select("lnemitranunkid = '" & dgvEMIHistory.CurrentRow.Cells(objdgcolhlnemitranunkid.Index).Value.ToString & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                Else
    '                    xtmp = mdtEMI.Select("GUID = '" & dgvEMIHistory.CurrentRow.Cells(objdgcolhEGUID.Index).Value.ToString & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                End If
    '                If xtmp.Length > 0 Then
    '                    If CInt(cboDeductionPeriod.SelectedValue) = CInt(xtmp(0).Item("periodunkid")) Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, you cannot delete this entry. Reason : It is linked with deduction period."), enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If
    '                    If CInt(xtmp(0).Item("lnemitranunkid").ToString) > 0 Then

    '                        Dim objTnaLeaveTran As New clsTnALeaveTran
    '                        Dim objPeriod As New clscommom_period_Tran
    '                        objPeriod._Periodunkid = CInt(xtmp(0).Item("periodunkid"))

    '                        If objTnaLeaveTran.IsPayrollProcessDone(CInt(xtmp(0).Item("periodunkid")), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot remove assiged EMI Information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), enMsgBoxStyle.Information)
    '                            objPeriod = Nothing
    '                            objTnaLeaveTran = Nothing
    '                            Exit Sub
    '                        End If
    '                        objTnaLeaveTran = Nothing : objPeriod = Nothing

    '                        Dim xUsedMessage As String = String.Empty
    '                        xUsedMessage = objLoan_Advance.Check_Transaction_Linking("lnemitranunkid", CInt(xtmp(0).Item("lnemitranunkid")), mintLoanAdvanceUnkid)

    '                        If xUsedMessage.Trim.Length > 0 Then
    '                            eZeeMsgBox.Show(xUsedMessage, enMsgBoxStyle.Information)
    '                            Exit Sub
    '                        End If

    '                        Dim xStrVoidReason As String = String.Empty
    '                        Dim rfrm As New frmReasonSelection
    '                        If User._Object._Isrighttoleft = True Then
    '                            rfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                            rfrm.RightToLeftLayout = True
    '                            Call Language.ctlRightToLeftlayOut(rfrm)
    '                        End If
    '                        rfrm.displayDialog(enVoidCategoryType.LOAN, xStrVoidReason)
    '                        If xStrVoidReason.Trim.Length <= 0 Then Exit Sub
    '                        xtmp(0).Item("isvoid") = True
    '                        xtmp(0).Item("voiduserunkid") = User._Object._Userunkid
    '                        xtmp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
    '                        xtmp(0).Item("voidreason") = xStrVoidReason
    '                        xtmp(0).Item("AUD") = "D"
    '                    Else
    '                        xtmp(0).Item("AUD") = "D"
    '                    End If
    '                    xtmp(0).AcceptChanges()
    '                    mdtEMI.AcceptChanges()
    '                    Call Fill_EMI_Transaction_Grid()
    '                End If
    '        End Select
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvEMIHistory_CellClick", mstrModuleName)
    '    Finally
    '        If frm IsNot Nothing Then frm.Dispose()
    '    End Try
    'End Sub

    'Private Sub dgvTopupHistory_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTopupHistory.CellClick
    '    Dim frm As New frmAddEditLoanParameters
    '    Try
    '        If User._Object._Isrighttoleft = True Then
    '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            frm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(frm)
    '        End If

    '        If e.RowIndex <= -1 Then Exit Sub
    '        Dim xCalTypeId As enLoanCalcId
    '        If radLoan.Checked Then
    '            If radSimpleInterest.Checked Then
    '                xCalTypeId = enLoanCalcId.Simple_Interest '1
    '            ElseIf radReduceBalInterest.Checked Then
    '                xCalTypeId = enLoanCalcId.Reducing_Amount '3
    '            ElseIf radNoInterest.Checked Then
    '                xCalTypeId = enLoanCalcId.No_Interest   '4
    '            End If
    '        Else
    '            objLoan_Advance._Calctype_Id = 0
    '        End If

    '        Select Case e.ColumnIndex
    '            Case objdgcolhTAdd.Index

    '                If Common_Validation() = False Then
    '                    Exit Sub
    '                End If

    '                frm._PeriodUnkid = mintCurrOpenPeriodId
    '                If frm.displayDialog(-1, enParameterMode.LN_TOPUP, mintLoanAdvanceUnkid, xCalTypeId, enAction.ADD_ONE) Then

    '                    Dim oDeductionPeriod As New clscommom_period_Tran
    '                    oDeductionPeriod._Periodunkid = CInt(cboDeductionPeriod.SelectedValue)

    '                    Dim xtmp() As DataRow = mdtTopup.Select("periodunkid = '" & frm._PeriodUnkid & "' AND AUD <> 'D'")
    '                    If xtmp.Length > 0 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, Topup is already present for the selected period."), enMsgBoxStyle.Information)
    '                        Exit Sub
    '                    End If

    '                    If frm._PeriodUnkid > 0 Then
    '                        Dim objTnaLeaveTran As New clsTnALeaveTran
    '                        Dim objPeriod As New clscommom_period_Tran
    '                        objPeriod._Periodunkid = frm._PeriodUnkid

    '                        If objPeriod._End_Date.Date < oDeductionPeriod._End_Date.Date Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Sorry, you can give topup based on deduction period set for this loan."), enMsgBoxStyle.Information)
    '                            Exit Sub
    '                        End If

    '                        If objTnaLeaveTran.IsPayrollProcessDone(frm._PeriodUnkid, cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry, you cannot add Topup information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), enMsgBoxStyle.Information)
    '                            objPeriod = Nothing
    '                            objTnaLeaveTran = Nothing
    '                            Exit Sub
    '                        End If
    '                        objTnaLeaveTran = Nothing : objPeriod = Nothing
    '                    End If

    '                    Dim dRow As DataRow = mdtTopup.NewRow()
    '                    dRow.Item("lntopuptranunkid") = -1
    '                    dRow.Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
    '                    dRow.Item("periodunkid") = frm._PeriodUnkid
    '                    dRow.Item("effectivedate") = frm._EffectiveDate
    '                    dRow.Item("topup_amount") = frm._TopupAmount
    '                    If xCalTypeId = enLoanCalcId.Simple_Interest Then
    '                        dRow.Item("interest_amount") = 0
    '                    Else
    '                        dRow.Item("interest_amount") = 0
    '                    End If
    '                    dRow.Item("userunkid") = User._Object._Userunkid
    '                    dRow.Item("isvoid") = False
    '                    dRow.Item("voiduserunkid") = -1
    '                    dRow.Item("voiddatetime") = DBNull.Value
    '                    dRow.Item("voidreason") = ""
    '                    dRow.Item("AUD") = "A"
    '                    dRow.Item("GUID") = Guid.NewGuid.ToString()
    '                    dRow.Item("dperiod") = frm._PeriodName
    '                    dRow.Item("ddate") = frm._EffectiveDate.Date.ToShortDateString
    '                    dRow.Item("pstatusid") = enStatusType.Open
    '                    mdtTopup.Rows.Add(dRow)
    '                    Call Fill_Topup_Transaction_Grid()

    '                    oDeductionPeriod = Nothing

    '                End If
    '            Case objdgcolhTEdit.Index
    '                If dgvTopupHistory.Rows(e.RowIndex).Cells(objdgcolhTEdit.Index).Value Is imgBlank Then Exit Sub
    '                Dim xtmp() As DataRow = Nothing
    '                If CInt(dgvTopupHistory.CurrentRow.Cells(objdgcolhlntopuptranunkid.Index).Value) > 0 Then
    '                    xtmp = mdtTopup.Select("lntopuptranunkid = '" & dgvTopupHistory.CurrentRow.Cells(objdgcolhlntopuptranunkid.Index).Value.ToString & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                Else
    '                    xtmp = mdtTopup.Select("GUID = '" & dgvTopupHistory.CurrentRow.Cells(objdgcolhTGUID.Index).Value.ToString & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                End If
    '                If xtmp.Length > 0 Then
    '                    If CInt(xtmp(0).Item("lntopuptranunkid").ToString) > 0 Then
    '                        Dim objTnaLeaveTran As New clsTnALeaveTran
    '                        Dim objPeriod As New clscommom_period_Tran
    '                        objPeriod._Periodunkid = CInt(xtmp(0).Item("periodunkid"))
    '                        If objTnaLeaveTran.IsPayrollProcessDone(CInt(xtmp(0).Item("periodunkid")), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, you cannot edit assiged Topup Information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), enMsgBoxStyle.Information)
    '                            objPeriod = Nothing
    '                            objTnaLeaveTran = Nothing
    '                            Exit Sub
    '                        End If
    '                        objTnaLeaveTran = Nothing : objPeriod = Nothing
    '                    End If
    '                    frm._PeriodUnkid = CInt(xtmp(0).Item("periodunkid"))
    '                    frm._EffectiveDate = CDate(xtmp(0).Item("effectivedate"))
    '                    frm._TopupAmount = CInt(xtmp(0).Item("topup_amount"))
    '                    If frm.displayDialog(CInt(xtmp(0).Item("lntopuptranunkid")), enParameterMode.LN_TOPUP, mintLoanAdvanceUnkid, xCalTypeId, enAction.EDIT_ONE, False) Then

    '                        Dim xFRow() As DataRow = mdtTopup.Select("periodunkid = '" & frm._PeriodUnkid & "' AND AUD <> 'D' AND periodunkid <> '" & CInt(xtmp(0).Item("periodunkid")) & "'")
    '                        If xFRow.Length > 0 Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, Topup is already present for the selected period."), enMsgBoxStyle.Information)
    '                            Exit Sub
    '                        End If

    '                        xtmp(0).Item("lntopuptranunkid") = xtmp(0).Item("lntopuptranunkid")
    '                        xtmp(0).Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
    '                        xtmp(0).Item("periodunkid") = frm._PeriodUnkid
    '                        xtmp(0).Item("effectivedate") = frm._EffectiveDate
    '                        xtmp(0).Item("topup_amount") = frm._TopupAmount
    '                        If xCalTypeId = enLoanCalcId.Simple_Interest Then
    '                            xtmp(0).Item("interest_amount") = 0
    '                        Else
    '                            xtmp(0).Item("interest_amount") = 0
    '                        End If
    '                        xtmp(0).Item("userunkid") = User._Object._Userunkid
    '                        xtmp(0).Item("isvoid") = False
    '                        xtmp(0).Item("voiduserunkid") = -1
    '                        xtmp(0).Item("voiddatetime") = DBNull.Value
    '                        xtmp(0).Item("voidreason") = ""
    '                        If xtmp(0).Item("AUD").ToString = "" Then
    '                            xtmp(0).Item("AUD") = "U"
    '                        End If
    '                        xtmp(0).Item("GUID") = xtmp(0).Item("GUID")
    '                        xtmp(0).Item("dperiod") = frm._PeriodName
    '                        xtmp(0).Item("ddate") = frm._EffectiveDate.Date.ToShortDateString
    '                        xtmp(0).Item("pstatusid") = enStatusType.Open
    '                        xtmp(0).AcceptChanges()
    '                        mdtTopup.AcceptChanges()
    '                        Fill_Topup_Transaction_Grid()
    '                    End If
    '                End If
    '            Case objdgcolhTDelete.Index
    '                If dgvTopupHistory.Rows(e.RowIndex).Cells(objdgcolhTDelete.Index).Value Is imgBlank Then Exit Sub
    '                Dim xtmp() As DataRow = Nothing
    '                If CInt(dgvTopupHistory.CurrentRow.Cells(objdgcolhlntopuptranunkid.Index).Value) > 0 Then
    '                    xtmp = mdtTopup.Select("lntopuptranunkid = '" & dgvTopupHistory.CurrentRow.Cells(objdgcolhlntopuptranunkid.Index).Value.ToString & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                Else
    '                    xtmp = mdtTopup.Select("GUID = '" & dgvTopupHistory.CurrentRow.Cells(objdgcolhTGUID.Index).Value.ToString & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                End If
    '                If xtmp.Length > 0 Then
    '                    If CInt(xtmp(0).Item("lntopuptranunkid").ToString) > 0 Then
    '                        Dim objTnaLeaveTran As New clsTnALeaveTran
    '                        Dim objPeriod As New clscommom_period_Tran
    '                        objPeriod._Periodunkid = CInt(xtmp(0).Item("periodunkid"))
    '                        If objTnaLeaveTran.IsPayrollProcessDone(CInt(xtmp(0).Item("periodunkid")), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot delete assiged Topup Information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), enMsgBoxStyle.Information)
    '                            objPeriod = Nothing
    '                            objTnaLeaveTran = Nothing
    '                            Exit Sub
    '                        End If
    '                        objTnaLeaveTran = Nothing : objPeriod = Nothing
    '                        Dim xUsedMessage As String = String.Empty
    '                        xUsedMessage = objLoan_Advance.Check_Transaction_Linking("lntopuptranunkid", CInt(xtmp(0).Item("lntopuptranunkid")), mintLoanAdvanceUnkid)

    '                        If xUsedMessage.Trim.Length > 0 Then
    '                            eZeeMsgBox.Show(xUsedMessage, enMsgBoxStyle.Information)
    '                            Exit Sub
    '                        End If
    '                        Dim xStrVoidReason As String = String.Empty
    '                        Dim rfrm As New frmReasonSelection
    '                        If User._Object._Isrighttoleft = True Then
    '                            rfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '                            rfrm.RightToLeftLayout = True
    '                            Call Language.ctlRightToLeftlayOut(rfrm)
    '                        End If
    '                        rfrm.displayDialog(enVoidCategoryType.LOAN, xStrVoidReason)
    '                        If xStrVoidReason.Trim.Length <= 0 Then Exit Sub
    '                        xtmp(0).Item("isvoid") = True
    '                        xtmp(0).Item("voiduserunkid") = User._Object._Userunkid
    '                        xtmp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
    '                        xtmp(0).Item("voidreason") = xStrVoidReason
    '                        xtmp(0).Item("AUD") = "D"
    '                    Else
    '                        xtmp(0).Item("AUD") = "D"
    '                    End If
    '                    mdtTopup.AcceptChanges()
    '                    Call Fill_Topup_Transaction_Grid()
    '                End If
    '        End Select
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvTopupHistory_CellClick", mstrModuleName)
    '    Finally
    '        If frm IsNot Nothing Then frm.Dispose()
    '    End Try
    'End Sub

    'Private Sub dgvInterestHistory_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvInterestHistory.DataBindingComplete
    '    Try
    '        For Each xdgvr As DataGridViewRow In dgvInterestHistory.Rows
    '            If CInt(xdgvr.Cells(objdgcolhIPStatusId.Index).Value) = enStatusType.Close Then
    '                xdgvr.Cells(objdgcolhIDelete.Index).Value = imgBlank
    '                xdgvr.Cells(objdgcolhIEdit.Index).Value = imgBlank
    '            End If
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvInterestHistory_DataBindingComplete", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub dgvEMIHistory_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvEMIHistory.DataBindingComplete
    '    Try
    '        For Each xdgvr As DataGridViewRow In dgvEMIHistory.Rows
    '            If CInt(xdgvr.Cells(objdgcolhEPstatusid.Index).Value) = enStatusType.Close Then
    '                xdgvr.Cells(objdgcolhEDelete.Index).Value = imgBlank
    '                xdgvr.Cells(objdgcolhEEdit.Index).Value = imgBlank
    '            End If
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvEMIHistory_DataBindingComplete", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub dgvTopupHistory_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvTopupHistory.DataBindingComplete
    '    Try
    '        For Each xdgvr As DataGridViewRow In dgvTopupHistory.Rows
    '            If CInt(xdgvr.Cells(objdgcolhTPstatusid.Index).Value) = enStatusType.Close Then
    '                xdgvr.Cells(objdgcolhEDelete.Index).Value = imgBlank
    '                xdgvr.Cells(objdgcolhEEdit.Index).Value = imgBlank
    '            End If
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "dgvTopupHistory_DataBindingComplete", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub dgvInterestHistory_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvInterestHistory.DataError

    'End Sub

    'Private Sub dgvEMIHistory_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvEMIHistory.DataError

    'End Sub

    'Private Sub dgvTopupHistory_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvTopupHistory.DataError

    'End Sub

    'If mblnIsExternalEntity = True Then
    '    If mblnIsItLoan = True Then
    '        objLoan_Advance._Balance_Amount = txtNetAmount.Decimal
    '    Else
    '        objLoan_Advance._Balance_Amount = txtAdvanceAmt.Decimal
    '    End If
    'Else
    '    objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount
    'End If
    'If menAction <> enAction.EDIT_ONE Then
    '    objLoan_Advance._LoanBF_Amount = txtNetAmount.Decimal
    'End If
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

			Call SetLanguage()

            Me.gbLoanAdvanceInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLoanAdvanceInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub


	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbLoanAdvanceInfo.Text = Language._Object.getCaption(Me.gbLoanAdvanceInfo.Name, Me.gbLoanAdvanceInfo.Text)
			Me.lblPurpose.Text = Language._Object.getCaption(Me.lblPurpose.Name, Me.lblPurpose.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblApprovedBy.Text = Language._Object.getCaption(Me.lblApprovedBy.Name, Me.lblApprovedBy.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
			Me.lblVoucherNo.Text = Language._Object.getCaption(Me.lblVoucherNo.Name, Me.lblVoucherNo.Text)
			Me.lblLoanAmt.Text = Language._Object.getCaption(Me.lblLoanAmt.Name, Me.lblLoanAmt.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.lblDeductionPeriod.Text = Language._Object.getCaption(Me.lblDeductionPeriod.Name, Me.lblDeductionPeriod.Text)
			Me.tabpInterestInfo.Text = Language._Object.getCaption(Me.tabpInterestInfo.Name, Me.tabpInterestInfo.Text)
			Me.tabpEMIInfo.Text = Language._Object.getCaption(Me.tabpEMIInfo.Name, Me.tabpEMIInfo.Text)
			Me.tabpTopupInfo.Text = Language._Object.getCaption(Me.tabpTopupInfo.Name, Me.tabpTopupInfo.Text)
			Me.lblAdvanceAmt.Text = Language._Object.getCaption(Me.lblAdvanceAmt.Name, Me.lblAdvanceAmt.Text)
			Me.radAdvance.Text = Language._Object.getCaption(Me.radAdvance.Name, Me.radAdvance.Text)
			Me.radLoan.Text = Language._Object.getCaption(Me.radLoan.Name, Me.radLoan.Text)
			Me.lblInterestAmt.Text = Language._Object.getCaption(Me.lblInterestAmt.Name, Me.lblInterestAmt.Text)
			Me.lblNetAmount.Text = Language._Object.getCaption(Me.lblNetAmount.Name, Me.lblNetAmount.Text)
			Me.tabpLoanAdvHistory.Text = Language._Object.getCaption(Me.tabpLoanAdvHistory.Name, Me.tabpLoanAdvHistory.Text)
			Me.lnProjectedAmount.Text = Language._Object.getCaption(Me.lnProjectedAmount.Name, Me.lnProjectedAmount.Text)
			Me.lblDuration.Text = Language._Object.getCaption(Me.lblDuration.Name, Me.lblDuration.Text)
			Me.lblLoanInterest.Text = Language._Object.getCaption(Me.lblLoanInterest.Name, Me.lblLoanInterest.Text)
			Me.lblEMIAmount.Text = Language._Object.getCaption(Me.lblEMIAmount.Name, Me.lblEMIAmount.Text)
			Me.dgcolhInterestPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhInterestPeriod.Name, Me.dgcolhInterestPeriod.HeaderText)
			Me.dgcolhInterestEffectiveDate.HeaderText = Language._Object.getCaption(Me.dgcolhInterestEffectiveDate.Name, Me.dgcolhInterestEffectiveDate.HeaderText)
			Me.dgcolhInterestRate.HeaderText = Language._Object.getCaption(Me.dgcolhInterestRate.Name, Me.dgcolhInterestRate.HeaderText)
			Me.dgcolhTopupPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhTopupPeriod.Name, Me.dgcolhTopupPeriod.HeaderText)
			Me.dgColhTopupEffectiveDate.HeaderText = Language._Object.getCaption(Me.dgColhTopupEffectiveDate.Name, Me.dgColhTopupEffectiveDate.HeaderText)
			Me.dgcolhTopupAmount.HeaderText = Language._Object.getCaption(Me.dgcolhTopupAmount.Name, Me.dgcolhTopupAmount.HeaderText)
			Me.dgcolhTopupInterest.HeaderText = Language._Object.getCaption(Me.dgcolhTopupInterest.Name, Me.dgcolhTopupInterest.HeaderText)
			Me.dgcolhLoanScheme.HeaderText = Language._Object.getCaption(Me.dgcolhLoanScheme.Name, Me.dgcolhLoanScheme.HeaderText)
			Me.dgcolhVoucherNo.HeaderText = Language._Object.getCaption(Me.dgcolhVoucherNo.Name, Me.dgcolhVoucherNo.HeaderText)
			Me.dgcolhDate.HeaderText = Language._Object.getCaption(Me.dgcolhDate.Name, Me.dgcolhDate.HeaderText)
			Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
			Me.dgcolhNetAmount.HeaderText = Language._Object.getCaption(Me.dgcolhNetAmount.Name, Me.dgcolhNetAmount.HeaderText)
			Me.dgcolhRepayment.HeaderText = Language._Object.getCaption(Me.dgcolhRepayment.Name, Me.dgcolhRepayment.HeaderText)
			Me.dgcolhBalanceAmt.HeaderText = Language._Object.getCaption(Me.dgcolhBalanceAmt.Name, Me.dgcolhBalanceAmt.HeaderText)
			Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
			Me.elProjectedINSTLAmt.Text = Language._Object.getCaption(Me.elProjectedINSTLAmt.Name, Me.elProjectedINSTLAmt.Text)
			Me.dgcolhEMIPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhEMIPeriod.Name, Me.dgcolhEMIPeriod.HeaderText)
			Me.dgcolhEMIeffectivedate.HeaderText = Language._Object.getCaption(Me.dgcolhEMIeffectivedate.Name, Me.dgcolhEMIeffectivedate.HeaderText)
			Me.dgcolhloan_duration.HeaderText = Language._Object.getCaption(Me.dgcolhloan_duration.Name, Me.dgcolhloan_duration.HeaderText)
			Me.dgcolhInstallmentAmt.HeaderText = Language._Object.getCaption(Me.dgcolhInstallmentAmt.Name, Me.dgcolhInstallmentAmt.HeaderText)
			Me.dgcolhEMINoofInstallment.HeaderText = Language._Object.getCaption(Me.dgcolhEMINoofInstallment.Name, Me.dgcolhEMINoofInstallment.HeaderText)
			Me.lblInterestCalcType.Text = Language._Object.getCaption(Me.lblInterestCalcType.Name, Me.lblInterestCalcType.Text)
			Me.lblLoanCalcType.Text = Language._Object.getCaption(Me.lblLoanCalcType.Name, Me.lblLoanCalcType.Text)
			Me.lblPrincipalAmt.Text = Language._Object.getCaption(Me.lblPrincipalAmt.Name, Me.lblPrincipalAmt.Text)
			Me.lblIntAmt.Text = Language._Object.getCaption(Me.lblIntAmt.Name, Me.lblIntAmt.Text)
			Me.lblMappedHead.Text = Language._Object.getCaption(Me.lblMappedHead.Name, Me.lblMappedHead.Text)

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub


	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 7, "Type to Search")
			Language.setMessage(mstrModuleName, 10, "Pay Period is compulsory information. Please select Pay Period.")
			Language.setMessage(mstrModuleName, 11, "Loan Scheme is compulsory information. Please select Loan Scheme to continue.")
			Language.setMessage(mstrModuleName, 12, "Loan Amount cannot be blank. Loan Amount is compulsory information.")
			Language.setMessage(mstrModuleName, 13, "Advance Amount cannot be blank. Advance Amount is compulsory information.")
			Language.setMessage(mstrModuleName, 14, "Effective date should be between")
			Language.setMessage(mstrModuleName, 15, "Deduction period is compulsory information. Please select Deduction period to continue.")
			Language.setMessage(mstrModuleName, 16, "Deduction period cannot be less then the Loan Period.")
			Language.setMessage(mstrModuleName, 17, "Sorry, Loan calculation type is mandatory information. Please select loan calculation type to continue.")
			Language.setMessage(mstrModuleName, 18, " And")
			Language.setMessage(mstrModuleName, 19, "Sorry, Interest calculation type is mandatory information. Please select interest calculation type to continue.")
			Language.setMessage(mstrModuleName, 20, "Sorry, you cannot assign Loan/Advance. Reason: Payslip Payment is already done for selected period.")
			Language.setMessage(mstrModuleName, 21, "Sorry, you cannot assign Loan/Advance. Reason: Payroll process is already done for the last date of selected deduction period.")
			Language.setMessage(mstrModuleName, 22, "Voucher No. cannot be blank. Voucher No. is compulsory information.")
			Language.setMessage(mstrModuleName, 23, "Employee is compulsory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 25, "Sorry, Interest rate is mandatory information. Please enter interest rate.")
			Language.setMessage(mstrModuleName, 26, "Installment months cannot be greater than")
			Language.setMessage(mstrModuleName, 27, " for")
			Language.setMessage(mstrModuleName, 28, " Scheme.")
			Language.setMessage(mstrModuleName, 29, "Sorry, you can not assign loan for the selected scheme: Reason, The selected loan scheme is already assigned to you and having (In-progress or On hold) status. Instead of applying new loan, You can add top-up amount from Loan Advance List-> Operation -> Other Loan Operations.")
			Language.setMessage(mstrModuleName, 30, "Sorry, you can not assign Advance: Reason, The Advance is already assigned to you and having (In-progress or On hold) status.")
			Language.setMessage(mstrModuleName, 31, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 32, "Please select Mapped Head to continue.")
			Language.setMessage(mstrModuleName, 33, "Sorry, Loan tenure should not be greater than employee tenure month")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

