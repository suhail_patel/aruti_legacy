﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.clsPayment_tran

#End Region

Public Class frmNewLoanAdvanceList

#Region " Private Varaibles "

    Private objLoan_Advance As clsLoan_Advance
    Private ReadOnly mstrModuleName As String = "frmNewLoanAdvanceList"
    Private mintSeletedValue As Integer = 0
    Private mstrFilterTitle As String = ""
    Private mintSelectedRowIndex As Integer = -1
    Private mdtLoanBalance As DataTable 'SHANI (11 JUL 2015) -- Start
    Private mstrBaseCurrSign As String = "" 'Shani(07 May 2015)
    Private mintBaseCurrId As Integer = 0 'Sohail (07 May 2015)
    Private mstrSearch As String = "" 'Shani (07 May 2015)
    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private mstrSearchText As String = String.Empty
    'Nilay (27-Oct-2016) -- End

#End Region

    'Sohail (17 Feb 2021) -- Start
    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
#Region " Properties "

    Private mblnIsFromMail As Boolean = False
    Public WriteOnly Property _IsFromMail() As Boolean
        Set(ByVal value As Boolean)
            mblnIsFromMail = value
        End Set
    End Property

    Private dsEmailListData As New DataSet
    Public Property _Email_Data() As DataSet
        Get
            Return dsEmailListData
        End Get
        Set(ByVal value As DataSet)
            dsEmailListData = value
        End Set
    End Property

    Private mstrLoanAdvanceTranUnkids As String = ""
    Public Property _LoanAdvanceTranUnkids() As String
        Get
            Return mstrLoanAdvanceTranUnkids
        End Get
        Set(ByVal value As String)
            mstrLoanAdvanceTranUnkids = value
        End Set
    End Property
#End Region
    'Sohail (17 Feb 2021) -- End

#Region " Private Function "

    Private Sub FillList()
        Dim dsLoanAdvance As New DataSet
        Dim dtTable As New DataTable
        Dim StrSearching As String = String.Empty
        Dim objExchangeRate As New clsExchangeRate
        mstrFilterTitle = ""
        'Hemant (16 Jan 2019) -- Start
        'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
        Dim strExtraReportFilterString As String = String.Empty
        'Hemant (16 Jan 2019) -- End
        Try
            RemoveHandler dgvLoanAdvanceList.SelectionChanged, AddressOf dgvLoanAdvanceList_SelectionChanged

            If User._Object.Privilege._AllowToViewLoanAdvanceList = True Then

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrSearching &= "AND LN.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                    mstrFilterTitle &= ", " & lblEmployee.Text & " : " & cboEmployee.Text
                End If

                If CInt(cboLoanScheme.SelectedValue) > 0 Then
                    StrSearching &= "AND LN.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
                    mstrFilterTitle &= ", " & lblLoanScheme.Text & " : " & cboLoanScheme.Text
                End If

                If CInt(cboStatus.SelectedValue) > 0 Then
                    StrSearching &= "AND LN.loan_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
                    mstrFilterTitle &= ", " & lblStatus.Text & " : " & cboStatus.Text
                End If

                If CInt(cboAssignPeriod.SelectedValue) > 0 Then
                    StrSearching &= "AND LN.periodunkid = " & CInt(cboAssignPeriod.SelectedValue) & " "
                    mstrFilterTitle &= ", " & lblPayPeriod.Text & " : " & cboAssignPeriod.Text
                    'Hemant (16 Jan 2019) -- Start
                    'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
                    strExtraReportFilterString &= "AND lnloan_advance_tran.periodunkid = " & CInt(cboAssignPeriod.SelectedValue) & " "
                    'Hemant (16 Jan 2019) -- End
                End If

                If CInt(cboLoanAdvance.SelectedIndex) > 0 Then
                    StrSearching &= "AND LN.Loan_AdvanceUnkid = " & CInt(cboLoanAdvance.SelectedIndex) & " "
                    mstrFilterTitle &= ", " & lblLoanAdvance.Text & " : " & cboLoanAdvance.Text
                End If

                If txtAmount.Text.Trim <> "" Then
                    If txtAmount.Decimal > 0 Then
                        StrSearching &= "AND LN.Amount " & cboCondition.Text.Trim & txtAmount.Decimal & " "
                        mstrFilterTitle &= ", " & lblAmount.Text & " : " & txtAmount.Text
                    End If
                End If

                If txtVocNo.Text.Trim <> "" Then
                    StrSearching &= "AND LN.VocNo LIKE '%" & CStr(txtVocNo.Text) & "%'" & " "
                    mstrFilterTitle &= ", " & lblVocNo.Text & " : " & txtVocNo.Text
                    'Hemant (16 Jan 2019) -- Start
                    'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
                    strExtraReportFilterString &= "AND lnloan_advance_tran.loanvoucher_no LIKE '%" & CStr(txtVocNo.Text) & "%'" & " "
                    'Hemant (16 Jan 2019) -- End
                End If

                'Nilay (02-Jul-2016) -- Start
                If CInt(cboOtherOpStatus.SelectedValue) > 0 Then
                    StrSearching &= "AND LN.opstatus_id = " & CInt(cboOtherOpStatus.SelectedValue) & " "
                End If
                'Nilay (02-Jul-2016) -- End

                'Nilay (04-Nov-2016) -- Start
                'Enhancements: Global Change Status feature requested by {Rutta}
                If CInt(cboCalcType.SelectedValue) > 0 Then
                    StrSearching &= "AND LN.calctype_id = " & CInt(cboCalcType.SelectedValue) & " "
                    'Hemant (16 Jan 2019) -- Start
                    'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
                    strExtraReportFilterString &= "AND lnloan_advance_tran.calctype_id = " & CInt(cboCalcType.SelectedValue) & " "
                    'Hemant (16 Jan 2019) -- End
                End If
                'Nilay (04-Nov-2016) -- End

                'SHANI (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    StrSearching &= "AND LN.countryunkid = '" & cboCurrency.SelectedValue.ToString & "' "
                    mstrFilterTitle &= ", " & lblCurrency.Text & " : " & cboCurrency.Text
                    'Hemant (16 Jan 2019) -- Start
                    'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
                    strExtraReportFilterString &= "AND lnloan_advance_tran.countryunkid = '" & cboCurrency.SelectedValue.ToString & "' "
                    'Hemant (16 Jan 2019) -- End
                Else
                    mstrFilterTitle &= ", " & lblCurrency.Text & " : ALL"
                End If
                'SHANI (07 May 2015) -- End 

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                    mstrFilterTitle = mstrFilterTitle.Substring(2)
                    'SHANI (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                Else
                    mstrFilterTitle = mstrFilterTitle.Substring(2)
                    'SHANI (07 May 2015) -- End 
                End If

                'Nilay (15-Dec-2015) -- Start
                Me.Cursor = Cursors.WaitCursor
                'Nilay (15-Dec-2015) -- End

                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsLoanAdvance = objLoan_Advance.GetList("Loan", , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , StrSearching)
                'Else
                '    dsLoanAdvance = objLoan_Advance.GetList("Loan", , , , , , , StrSearching)
                'End If

                'SHANI (11 JUL 2015) -- Start
                'Enhancement : Replacing all List view with Grid view on Close Year Wizard
                'dsLoanAdvance = objLoan_Advance.GetList(FinancialYear._Object._DatabaseName, _
                '                                     User._Object._Userunkid, _
                '                                     FinancialYear._Object._YearUnkid, _
                '                                     Company._Object._Companyunkid, _
                '                                     ConfigParameter._Object._IsIncludeInactiveEmp, _
                '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                     UserAccessLevel._AccessLevelFilterString, _
                '                                     StrSearching)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid, , True)
                Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1, , True)
                'S.SANDEEP [04 JUN 2015] -- END

                Dim objPeriod As New clscommom_period_Tran
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objPeriod._Periodunkid = intFirstOpenPeriodID
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intFirstOpenPeriodID
                'Nilay (10-Oct-2015) -- End

                'lblBalanceAsOnDate.Text = Language.getMessage(mstrModuleName, 13, "* Current Balance as on date") & " " & Format(objPeriod._End_Date, "dd-MMM-yyyy")

                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsLoanAdvance = objLoan_Advance.GetList(FinancialYear._Object._DatabaseName, _
                '                                        User._Object._Userunkid, _
                '                                        FinancialYear._Object._YearUnkid, _
                '                                        Company._Object._Companyunkid, _
                '                                        ConfigParameter._Object._IsIncludeInactiveEmp, _
                '                                        objPeriod._Start_Date, _
                '                                        objPeriod._End_Date, _
                '                                        objPeriod._End_Date, _
                '                                        UserAccessLevel._AccessLevelFilterString, _
                '                                        StrSearching)

                'Shani(26-Nov-2015) -- Start
                'ENHANCEMENT : Add Loan Import Form


                'dsLoanAdvance = objLoan_Advance.GetList(FinancialYear._Object._DatabaseName, _
                '                                        User._Object._Userunkid, _
                '                                        FinancialYear._Object._YearUnkid, _
                '                                        Company._Object._Companyunkid, _
                '                                        ConfigParameter._Object._IsIncludeInactiveEmp, _
                '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                        ConfigParameter._Object._UserAccessModeSetting, _
                '                                        "Loan", StrSearching)

                dsLoanAdvance = objLoan_Advance.GetList(FinancialYear._Object._DatabaseName, _
                                                        User._Object._Userunkid, _
                                                        FinancialYear._Object._YearUnkid, _
                                                        Company._Object._Companyunkid, _
                                                        ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                        objPeriod._Start_Date, _
                                                        objPeriod._End_Date, _
                                                        objPeriod._End_Date, _
                                                        ConfigParameter._Object._UserAccessModeSetting, _
                                                        "Loan", StrSearching, CInt(cboEmployee.SelectedValue))
                'Hemant (26 Mar 2019) - [CInt(cboEmployee.SelectedValue)]
                'Shani(26-Nov-2015) -- End

                'Nilay (10-Oct-2015) -- End

                'Nilay (25-Mar-2016) -- Start
                'Dim dsLoanBalance As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo("Balance", objPeriod._End_Date.AddDays(1), IIf(CInt(cboEmployee.SelectedValue) <> 0, cboEmployee.SelectedValue.ToString, "").ToString, 0, objPeriod._Start_Date, True, True, , True, True, ConfigParameter._Object._FirstNamethenSurname, True, True)
                'Hemant (16 Jan 2019) -- Start
                'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
                'Dim dsLoanBalance As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo(FinancialYear._Object._DatabaseName, _
                '                                                                         User._Object._Userunkid, _
                '                                                                         FinancialYear._Object._YearUnkid, _
                '                                                                         Company._Object._Companyunkid, _
                '                                                                         objPeriod._Start_Date, _
                '                                                                         objPeriod._End_Date, _
                '                                                                         ConfigParameter._Object._UserAccessModeSetting, _
                '                                                                         True, "Balance", _
                '                                                                         objPeriod._End_Date.AddDays(1), _
                '                                                                         CInt(cboEmployee.SelectedValue).ToString, 0, _
                '                                                                         objPeriod._Start_Date, True, True, , True, True, _
                '                                                                         ConfigParameter._Object._FirstNamethenSurname, True, True)
                Dim dsLoanBalance As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo(FinancialYear._Object._DatabaseName, _
                                                                                         User._Object._Userunkid, _
                                                                                         FinancialYear._Object._YearUnkid, _
                                                                                         Company._Object._Companyunkid, _
                                                                                         objPeriod._Start_Date, _
                                                                                         objPeriod._End_Date, _
                                                                                         ConfigParameter._Object._UserAccessModeSetting, _
                                                                                         True, "Balance", _
                                                                                         objPeriod._End_Date.AddDays(1), _
                                                                                         CInt(cboEmployee.SelectedValue).ToString, 0, _
                                                                                         objPeriod._Start_Date, True, True, cboLoanScheme.SelectedValue.ToString, True, True, _
                                                                                         ConfigParameter._Object._FirstNamethenSurname, True, True, strExtraReportFilterString)
                'Hemant (16 Jan 2019) -- End
                'Nilay (02-Jan-2017) -- Start
                'REMOVED - IIf(CInt(cboEmployee.SelectedValue) <> 0, cboEmployee.SelectedValue.ToString, "").ToString
                'ADDED - CInt(cboEmployee.SelectedValue).ToString
                'Nilay (02-Jan-2017) -- End

                'Nilay (16-Nov-2016) -- Start
                'REMOVED -- [IIf(CInt(cboEmployee.SelectedValue) <> 0, cboEmployee.SelectedValue.ToString, "").ToString]
                'ADDED -- [IIf(CInt(cboEmployee.SelectedValue) <> 0, CInt(cboEmployee.SelectedValue).ToString, "").ToString]
                'Nilay (16-Nov-2016) -- End

                'Nilay (25-Mar-2016) -- End

                mdtLoanBalance = dsLoanBalance.Tables("Balance")
                'Dim decTotalAmt As Decimal = 0
                Dim decTotalBalance As Decimal = 0
                Dim decTotalPaid As Decimal = 0

                dsLoanAdvance.Tables(0).Columns.Add("loanbalance", Type.GetType("System.String"))
                dsLoanAdvance.Tables(0).Columns.Add("paidlaon", Type.GetType("System.String"))
                'Nilay (27-Oct-2016) -- Start
                'dsLoanAdvance.Tables(0).Columns.Add("TotalLoanAmount", Type.GetType("System.Decimal"))
                'Nilay (27-Oct-2016) -- End

                'Varsha Rana (12-Sept-2017) -- Start
                'Enhancement - Loan Topup in ESS
                Dim objPaymentTran As New clsPayment_tran
                ' Varsha Rana (12-Sept-2017) -- End


                For Each dtrow As DataRow In dsLoanAdvance.Tables(0).Rows
                    Dim intUnkID As Integer = CInt(dtrow.Item("loanadvancetranunkid"))
                    Dim dr_Row As List(Of DataRow) = (From p In dsLoanBalance.Tables("Balance") Where (CInt(p.Item("loanadvancetranunkid")) = intUnkID) Select (p)).ToList

                    If dr_Row.Count > 0 Then

                        'Varsha Rana (12-Sept-2017) -- Start
                        'Enhancement - Loan Topup in ESS
                        Dim ePRef As enPaymentRefId
                        If CBool(dtrow.Item("isloan")) = True Then
                            ePRef = enPaymentRefId.LOAN
                        Else
                            ePRef = enPaymentRefId.ADVANCE
                        End If
                        ' Varsha Rana (12-Sept-2017) -- End

                        'SHANI (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        If CInt(cboCurrency.SelectedValue) > 0 Then

                            'Varsha Rana (12-Sept-2017) -- Start
                            'Enhancement - Loan Topup in ESS
                            If CBool(dtrow("isexternal_entity")) = True OrElse objPaymentTran.IsPaymentDone(enPayTypeId.PAYMENT, CInt(dtrow.Item("loanadvancetranunkid")), ePRef) = True Then
                                ' Varsha Rana (12-Sept-2017) -- End

                                'Hemant (09 Jan 2019) -- Start
                                'Support Issue Id # 3312 : employees have negative current principal balance in 76.1.
                                'dtrow("loanbalance") = Format(CDec(dr_Row(0).Item("balance_amountPaidCurrency").ToString), GUI.fmtCurrency)

                                '    'Nilay (27-Oct-2016) -- Start
                                '    'dtrow("paidlaon") = Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")), GUI.fmtCurrency)
                                '    dtrow("paidlaon") = Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency)
                                ''Nilay (27-Oct-2016) -- End
                                'decTotalBalance += CDec(Format(CDec(dr_Row(0).Item("balance_amountPaidCurrency").ToString), GUI.fmtCurrency))
                                ''Nilay (27-Oct-2016) -- Start
                                ''decTotalPaid += CDec(Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")), GUI.fmtCurrency))
                                'decTotalPaid += CDec(Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency))
                                ''Nilay (27-Oct-2016) -- End

                                ''Nilay (08-Nov-2016) -- Start
                                'dtrow("Amount") = Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency)
                                ''Nilay (08-Nov-2016) -- End
                                dtrow("loanbalance") = Format(CDec(dr_Row(0).Item("LastProjectedBalancePaidCurrency").ToString), GUI.fmtCurrency)
                                dtrow("paidlaon") = Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) - CDec(dr_Row(0).Item("LastProjectedBalancePaidCurrency")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency)
                                decTotalBalance += CDec(Format(CDec(dr_Row(0).Item("LastProjectedBalancePaidCurrency").ToString), GUI.fmtCurrency))
                                decTotalPaid += CDec(Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) - CDec(dr_Row(0).Item("LastProjectedBalancePaidCurrency")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency))
                            dtrow("Amount") = Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency)
                                'Hemant (09 Jan 2019) -- End

                                'Varsha Rana (12-Sept-2017) -- Start
                                'Enhancement - Loan Topup in ESS
                            Else
                                dtrow("loanbalance") = Format(0, GUI.fmtCurrency)
                                dtrow("paidlaon") = Format(0, GUI.fmtCurrency)
                                decTotalBalance += 0
                                decTotalPaid += 0

                            End If
                            dtrow("Amount") = Format(CDec(dr_Row(0).Item("loan_advance_amountPaidCurrency")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency)

                            ' Varsha Rana (12-Sept-2017) -- End


                        Else

                            'Varsha Rana (12-Sept-2017) -- Start
                            'Enhancement - Loan Topup in ESS
                            If CBool(dtrow("isexternal_entity")) = True OrElse objPaymentTran.IsPaymentDone(enPayTypeId.PAYMENT, CInt(dtrow.Item("loanadvancetranunkid")), ePRef) = True Then
                                ' Varsha Rana (12-Sept-2017) -- End

                                'Hemant (09 Jan 2019) -- Start
                                'Support Issue Id # 3312 : employees have negative current principal balance in 76.1.
                                'dtrow("loanbalance") = Format(CDec(dr_Row(0).Item("balance_amountPaidCurrency").ToString), GUI.fmtCurrency)

                                ''Nilay (08-Nov-2016) -- Start
                                ''dtrow("paidlaon") = Format(CDec(dr_Row(0).Item("loan_amount")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")), GUI.fmtCurrency)
                                'dtrow("paidlaon") = Format(CDec(dr_Row(0).Item("loan_amount")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency)
                                ''Nilay (08-Nov-2016) -- End
                                'dtrow("cSign") = mstrBaseCurrSign
                                'decTotalBalance += CDec(Format(CDec(dr_Row(0).Item("balance_amountPaidCurrency").ToString), GUI.fmtCurrency))
                                ''Nilay (08-Nov-2016) -- Start
                                ''decTotalPaid += CDec(Format(CDec(dr_Row(0).Item("loan_amount")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")), GUI.fmtCurrency))
                                'decTotalPaid += CDec(Format(CDec(dr_Row(0).Item("loan_amount")) - CDec(dr_Row(0).Item("balance_amountPaidCurrency")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency))
                                'dtrow("Amount") = Format(CDec(dr_Row(0).Item("loan_amount")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency)
                                ''Nilay (08-Nov-2016) -- End
                                dtrow("loanbalance") = Format(CDec(dr_Row(0).Item("LastProjectedBalancePaidCurrency").ToString), GUI.fmtCurrency)
                                dtrow("paidlaon") = Format(CDec(dr_Row(0).Item("loan_amount")) - CDec(dr_Row(0).Item("LastProjectedBalancePaidCurrency")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency)
                            dtrow("cSign") = mstrBaseCurrSign
                                decTotalBalance += CDec(Format(CDec(dr_Row(0).Item("LastProjectedBalancePaidCurrency").ToString), GUI.fmtCurrency))
                                decTotalPaid += CDec(Format(CDec(dr_Row(0).Item("loan_amount")) - CDec(dr_Row(0).Item("LastProjectedBalancePaidCurrency")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency))
                            dtrow("Amount") = Format(CDec(dr_Row(0).Item("loan_amount")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency)
                                'Hemant (09 Jan 2019) -- End

                                'Varsha Rana (12-Sept-2017) -- Start
                                'Enhancement - Loan Topup in ESS
                            Else
                                dtrow("loanbalance") = Format(0, GUI.fmtCurrency)
                                dtrow("paidlaon") = Format(0, GUI.fmtCurrency)
                                decTotalBalance += 0
                                decTotalPaid += 0

                            End If
                            dtrow("Amount") = Format(CDec(dr_Row(0).Item("loan_amount")) + CDec(dtrow.Item("total_topup")), GUI.fmtCurrency)

                            ' Varsha Rana (12-Sept-2017) -- End
                        End If
                        'SHANI (07 May 2015) -- End
                    Else
                        dtrow("loanbalance") = Format(0, GUI.fmtCurrency)
                        dtrow("paidlaon") = Format(0, GUI.fmtCurrency)

                        decTotalBalance += CDec(Format(0, GUI.fmtCurrency))
                        decTotalPaid += CDec(Format(0, GUI.fmtCurrency))
                    End If
                Next

                If dsLoanAdvance.Tables(0).Rows.Count > 0 AndAlso mblnIsFromMail = False Then
                    'Sohail (17 Feb 2021) - [AndAlso mblnIsFromMail = False]
                    Dim xrow As DataRow = dsLoanAdvance.Tables(0).NewRow
                    xrow("Employee") = "GRAND TOTAL :"
                    xrow("loanbalance") = decTotalBalance.ToString
                    xrow("paidlaon") = decTotalPaid.ToString
                    xrow("Amount") = (From p In dsLoanAdvance.Tables(0) Select (CDec(p.Item("Amount")))).DefaultIfEmpty.Sum()
                    xrow("basecurrency_amount") = (From p In dsLoanAdvance.Tables(0) Select (CDec(p.Item("basecurrency_amount")))).DefaultIfEmpty.Sum()
                    dsLoanAdvance.Tables(0).Rows.Add(xrow)
                End If
                'SHANI (11 JUL 2015) -- End 

                dgvLoanAdvanceList.AutoGenerateColumns = False
                dgcolhVocNo.DataPropertyName = "VocNo"
                dgcolhEmployee.DataPropertyName = "Employee"
                dgcolhLoanScheme.DataPropertyName = "LoanScheme"
                dgcolhLoanAdvance.DataPropertyName = "Loan_Advance"
                'Nilay (15-Dec-2015) -- Start
                dgcolhLoanCalcType.DataPropertyName = "LoanCalcType"
                dgcolhInterestCalcType.DataPropertyName = "InterestCalcType"
                'Nilay (15-Dec-2015) -- End
                dgvLoanAdvanceList.Columns(dgcolhAmount.Index).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                dgcolhAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency

                'SHANI (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    dgcolhAmount.DataPropertyName = "Amount"
                Else
                    dgcolhAmount.DataPropertyName = "basecurrency_amount"
                End If
                'SHANI (07 May 2015) -- End 

                dgvLoanAdvanceList.Columns(dgcolhInstallments.Index).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                dgcolhInstallments.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgcolhInstallments.DataPropertyName = "installments" 'Nilay (15-Dec-2015) -- [Intallments] Replaced by [installments]

                dgcolhPeriod.DataPropertyName = "PeriodName"
                'Nilay (15-Dec-2015) -- Start
                dgcolhDeductionPeriod.DataPropertyName = "DeductionPeriodName"
                'Nilay (15-Dec-2015) -- End
                dgcolhStatus.DataPropertyName = "loan_status"
                objdgcolhIsLoan.DataPropertyName = "isloan"
                objdgcolhemployeeunkid.DataPropertyName = "employeeunkid"
                objdgcolhstatusunkid.DataPropertyName = "loan_statusunkid"
                objdgcolhLoanAdvanceunkid.DataPropertyName = "loanadvancetranunkid"
                dgcolhCurrency.DataPropertyName = "cSign"
                objdgcolhhisbrought_forward.DataPropertyName = "isbrought_forward"

                'SHANI (11 JUL 2015) -- Start
                'Enhancement : Replacing all List view with Grid view on Close Year Wizard
                dgvLoanAdvanceList.Columns(dgcolhBalance.Index).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                dgcolhBalance.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgcolhBalance.DefaultCellStyle.Format = GUI.fmtCurrency
                dgcolhBalance.DataPropertyName = "loanbalance"

                dgvLoanAdvanceList.Columns(dgcolhPaidLoan.Index).HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                dgcolhPaidLoan.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgcolhPaidLoan.DefaultCellStyle.Format = GUI.fmtCurrency
                dgcolhPaidLoan.DataPropertyName = "paidlaon"
                'SHANI (11 JUL 2015) -- End 

                'Shani(26-Nov-2015) -- Start
                'ENHANCEMENT : Add Loan Import Form
                objdgcolhVisibleStatus.DataPropertyName = "visiblestatus"
                'Shani(26-Nov-2015) -- End

                'Nilay (02-Jul-2016) -- Start
                dgcolhOtherOpApproval.DataPropertyName = "otherop_approval"
                objdgcolhOpStatusId.DataPropertyName = "opstatus_id"
                'Nilay (02-Jul-2016) -- End

                'Nilay (23-Aug-2016) -- Start
                'Enhancement - Create New Loan Notification 
                objdgcolhProcessPendingUnkid.DataPropertyName = "processpendingloanunkid"
                'Nilay (23-Aug-2016) -- End

                'Hemant (12 Nov 2021) -- Start
                'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
                objdgcolhLoanSchemeUnkid.DataPropertyName = "loanschemeunkid"
                'Hemant (12 Nov 2021) -- End

                'Sohail (17 Feb 2021) -- Start
                'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                objdgcolhCheck.DataPropertyName = "ischecked"
                objdgcolhCheck.Visible = mblnIsFromMail
                objChkAll.Visible = mblnIsFromMail
                'Sohail (17 Feb 2021) -- End

                dgvLoanAdvanceList.DataSource = dsLoanAdvance.Tables(0)

                'SHANI (11 JUL 2015) -- Start
                'Enhancement : Replacing all List view with Grid view on Close Year Wizard
                If dgvLoanAdvanceList.Rows.Count > 0 AndAlso mblnIsFromMail = False Then
                    'Sohail (17 Feb 2021) - [AndAlso mblnIsFromMail = False]
                    dgvLoanAdvanceList.Rows(dgvLoanAdvanceList.Rows.Count - 1).DefaultCellStyle.BackColor = Color.Blue
                    dgvLoanAdvanceList.Rows(dgvLoanAdvanceList.Rows.Count - 1).DefaultCellStyle.ForeColor = Color.White
                    dgvLoanAdvanceList.Rows(dgvLoanAdvanceList.Rows.Count - 1).DefaultCellStyle.Font = New Font(Me.Font, FontStyle.Bold)
                End If
                'SHANI (11 JUL 2015) -- End

                For Each dgvRow As DataGridViewRow In dgvLoanAdvanceList.Rows

                    'SHANI (11 JUL 2015) -- Start
                    'Enhancement : Replacing all List view with Grid view on Close Year Wizard
                    'If CBool(dgvRow.Cells(objdgcolhhisbrought_forward.Index).Value) = True Then
                    If CBool(IIf(IsDBNull(dgvRow.Cells(objdgcolhhisbrought_forward.Index).Value) = True, False, dgvRow.Cells(objdgcolhhisbrought_forward.Index).Value)) = True Then
                        'SHANI (11 JUL 2015) -- End
                        dgvRow.DefaultCellStyle.ForeColor = Color.Gray
                    End If
                Next

                'Shani (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                If StrSearching.Trim = mstrSearch.Trim Then
                    'SHANI (11 JUL 2015) -- Start
                    'Enhancement : Replacing all List view with Grid view on Close Year Wizard
                    If dgvLoanAdvanceList.Rows.Count > 0 AndAlso dgvLoanAdvanceList.Rows.Count > mintSelectedRowIndex Then
                        'SHANI (11 JUL 2015) -- End
                        If mintSelectedRowIndex >= 0 Then
                            dgvLoanAdvanceList.Rows(mintSelectedRowIndex).Selected = True
                            Call dgvLoanAdvanceList_SelectionChanged(dgvLoanAdvanceList, New System.EventArgs) 'Sohail (07 May 2015)
                            dgvLoanAdvanceList.FirstDisplayedScrollingRowIndex = dgvLoanAdvanceList.SelectedRows(0).Index
                        End If
                    End If
                Else
                    mstrSearch = StrSearching
                    If dgvLoanAdvanceList.RowCount > 0 Then
                        dgvLoanAdvanceList.Rows(0).Selected = True
                        Call dgvLoanAdvanceList_SelectionChanged(dgvLoanAdvanceList, New System.EventArgs)
                    End If
                End If
                'Shani (07 May 2015) -- End

                'SHANI (11 JUL 2015) -- Start
                'Enhancement : Replacing all List view with Grid view on Close Year Wizard
            End If
            'SHANI (11 JUL 2015) -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            'Nilay (15-Dec-2015) -- Start
            Me.Cursor = Cursors.Default
            'Nilay (15-Dec-2015) -- End
            AddHandler dgvLoanAdvanceList.SelectionChanged, AddressOf dgvLoanAdvanceList_SelectionChanged
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        'Nilay (02-Jul-2016) -- Start
        Dim objOtherOpApproval As New clsloanotherop_approval_tran
        'Nilay (02-Jul-2016) -- End

        Try

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Employee", True, True)
            'End If

            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, _
                                                   False, _
                                                   "Employee", True)

            'Nilay (10-Oct-2015) -- End



            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Employee")
                .SelectedValue = 0
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboEmployee)
            'Nilay (27-Oct-2016) -- End
            objEmployee = Nothing

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsCombos = objLoanScheme.getComboList(True, "LoanScheme")
            dsCombos = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("LoanScheme")
                .SelectedValue = 0
            End With
            'Nilay (27-Oct-2016) -- Start
            'Enhancements: Searchable Dropdown
            Call SetDefaultSearchText(cboLoanScheme)
            'Nilay (27-Oct-2016) -- End
            objLoanScheme = Nothing

            dsCombos = objMaster.GetLoan_Saving_Status("List", False)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsCombos = objMaster.GetCondition(False, True)
            dsCombos = objMaster.GetCondition(False, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            Dim dtCondition As DataTable = New DataView(dsCombos.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            With cboCondition
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dtCondition
                .SelectedIndex = 0
            End With
            dtCondition = Nothing
            objMaster = Nothing

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 FinancialYear._Object._DatabaseName, _
                                                 FinancialYear._Object._Database_Start_Date, _
                                                 "Period", True)
            'Nilay (10-Oct-2015) -- End

            With cboAssignPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = 0
            End With
            objPeriod = Nothing

            cboLoanAdvance.Items.Clear()
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 4, "Select"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 5, "Loan"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 6, "Advance"))
            cboLoanAdvance.SelectedIndex = 0

            'SHANI (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency", False)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombos.Tables("Currency")
            End With

            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("countryunkid")) 'Sohail (07 May 2015)
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0 'Sohail (07 May 2015)
            End If
            'SHANI (07 May 2015) -- End 
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            If mintBaseCurrId > 0 Then
                cboCurrency.SelectedValue = mintBaseCurrId
            Else
                If cboCurrency.Items.Count > 0 Then cboCurrency.SelectedIndex = 0
            End If
            'Sohail (07 May 2015) -- End

            'Nilay (02-Jul-2016) -- Start
            dsCombos = objOtherOpApproval.getComboOtherOpStatus("List", True)
            With cboOtherOpStatus
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            'Nilay (02-Jul-2016) -- End

            'Nilay (04-Nov-2016) -- Start
            'Enhancements: Global Change Status feature requested by {Rutta}
            dsCombos = objLoan_Advance.GetLoanCalculationTypeList("List", True)
            With cboCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
            End With
            'Nilay (04-Nov-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'Nilay (04-Nov-2016) -- Start
    'Enhancements: Global Change Status feature requested by {Rutta}
    Private Sub SetVisibilityOperationMenu(ByVal blnIsEnable As Boolean)
        Try
            mnuPayment.Enabled = blnIsEnable
            mnuReceived.Enabled = blnIsEnable
            mnuViewApprovalForm.Enabled = blnIsEnable
            mnuLoanOperation.Enabled = blnIsEnable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibilityOperationMenu", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnChangeStatus.Enabled = CBool(User._Object.Privilege._AllowChangeLoanAvanceStatus)
            mnuGlobalChangeStatus.Enabled = CBool(User._Object.Privilege._AllowChangeLoanAvanceStatus)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'Nilay (04-Nov-2016) -- End

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 17, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub
    'Nilay (27-Oct-2016) -- End


#End Region

#Region " Form's Events "

    Private Sub frmNewLoanAdvanceList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objLoan_Advance = New clsLoan_Advance
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()

            'Nilay (05-Feb-2016) -- Start
            Dim intOpenPeriodId As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, _
                                                                                  enStatusType.Open, , True)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intOpenPeriodId
            lblCurrOpenPeriod.Text = Language.getMessage(mstrModuleName, 14, "* Balance As On:") & " " & objPeriod._Period_Name.ToString
            'Nilay (05-Feb-2016) -- End

            'Sohail (17 Feb 2021) -- Start
            'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
            dgvLoanAdvanceList.ReadOnly = Not mblnIsFromMail
            If mblnIsFromMail = True Then
                objChkAll.BringToFront()
                For Each col As DataGridViewColumn In dgvLoanAdvanceList.Columns
                    If col.Name <> objdgcolhCheck.Name Then
                        col.ReadOnly = True
                    End If
                Next
            End If

            objdgcolhCheck.Visible = mblnIsFromMail
            objChkAll.Visible = mblnIsFromMail
            'Sohail (17 Feb 2021) -- End

            'lblBalanceAsOnDate.Text = "" 'Sohail (07 May 2015)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmNewLoanAdvanceList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmNewLoanAdvanceList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmNewLoanAdvanceList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objLoan_Advance = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoan_Advance.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Advance"
            objfrm.displayDialog(Me)


        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedIndex = 0
            cboLoanScheme.SelectedIndex = 0
            'Nilay (16-Nov-2016) -- Start
            Call SetDefaultSearchText(cboEmployee)
            Call SetDefaultSearchText(cboLoanScheme)
            lblEmployee.Focus()
            'Nilay (16-Nov-2016) -- End
            cboLoanAdvance.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            cboAssignPeriod.SelectedIndex = 0
            cboCondition.SelectedIndex = 0
            txtVocNo.Text = ""
            txtAmount.Decimal = 0
            'Nilay (02-Jul-2016) -- Start
            cboOtherOpStatus.SelectedValue = 0
            'Nilay (02-Jul-2016) -- End

            'Nilay (04-Nov-2016) -- Start
            'Enhancements: Global Change Status feature requested by {Rutta}
            cboCalcType.SelectedValue = 0
            'Nilay (04-Nov-2016) -- End

            dgvLoanAdvanceList.DataSource = Nothing
            Call objbtnReset.ShowResult(CStr(dgvLoanAdvanceList.RowCount))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            objChkAll.Checked = False 'Sohail (17 Feb 2021)
            FillList()
            If dgvLoanAdvanceList.RowCount > 0 Then
                'Sohail (17 Feb 2021) -- Start
                'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                'Call objbtnSearch.ShowResult(CStr(dgvLoanAdvanceList.RowCount - 1))
                If mblnIsFromMail = False Then
                Call objbtnSearch.ShowResult(CStr(dgvLoanAdvanceList.RowCount - 1))
            Else
                Call objbtnSearch.ShowResult(CStr(dgvLoanAdvanceList.RowCount))
            End If
                'Sohail (17 Feb 2021) -- End
            Else
                Call objbtnSearch.ShowResult(CStr(dgvLoanAdvanceList.RowCount))
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If dgvLoanAdvanceList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                dgvLoanAdvanceList.Select()
                Exit Sub
            End If

            'SHANI (11 JUL 2015) -- Start
            'Enhancement : Replacing all List view with Grid view on Close Year Wizard
            If dgvLoanAdvanceList.SelectedRows(0).Cells(dgcolhEmployee.Index).Value.ToString.Contains("GRAND TOTAL") Then Exit Sub
            'SHANI (11 JUL 2015) -- End 

            If CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhhisbrought_forward.Index).Value) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot delete this Loan/Advance. Reason : Loan/Advance is brought forward."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Hemant (16 Jan 2019) -- Start
            'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
            'objLoan_Advance._Loanadvancetranunkid = CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value)

            'Select Case objLoan_Advance._LoanStatus
            Select Case CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhstatusunkid.Index).Value)
                'Hemant (16 Jan 2019) -- End
                Case 3, 4
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot Edit/Delete Loan/Advance. Reason : Loan/Advance status is writtenoff or completed."), enMsgBoxStyle.Information)
                    Exit Sub
            End Select

            If objLoan_Advance._Isloan = True Then
                Dim objPaymentTran As New clsPayment_tran
                If objPaymentTran.IsPaymentDone(enPayTypeId.PAYMENT, CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), enPaymentRefId.LOAN) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot delete the transaction. Reason : Payment is already done for this transaction."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If objPaymentTran.IsPaymentDone(enPayTypeId.RECEIVED, CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), enPaymentRefId.LOAN) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot delete the transaction. Reason : Repayment is already done for this transaction."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                objPaymentTran = Nothing
            Else
                Dim objPaymentTran As New clsPayment_tran
                If objPaymentTran.IsPaymentDone(enPayTypeId.PAYMENT, CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), enPaymentRefId.ADVANCE) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot delete the transaction. Reason : Payment is already done for this transaction."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If objPaymentTran.IsPaymentDone(enPayTypeId.RECEIVED, CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), enPaymentRefId.ADVANCE) = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot delete the transaction. Reason : Repayment is already done for this transaction."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                objPaymentTran = Nothing
            End If

            Dim dsList As New DataSet
            dsList = objLoan_Advance.GetLastLoanBalance("List", CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value))
            If dsList.Tables(0).Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot delete the transaction. Reason : Deduction or Repayment is already done for this transaction."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            Dim objlnOtherOpapprovalTran As New clsloanotherop_approval_tran
            Dim blnFlag As Boolean
            blnFlag = objlnOtherOpapprovalTran.IsAllowedLnAdvChangeStatusOrDelete(CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), _
                                                                          CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhemployeeunkid.Index).Value))
            If blnFlag = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Some of Operations either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process. In order to Change Status or Delete, please either Approve or Reject pending operations."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Nilay (01-Apr-2016) -- End


            'Nilay (23-Aug-2016) -- Start
            'Enhancement - Create New Loan Notification 
            'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Loan?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
            '    objLoan_Advance._Isvoid = True
            '    Dim frm As New frmReasonSelection
            '    Dim mstrVoidReason As String = String.Empty
            '    frm.displayDialog(enVoidCategoryType.LOAN, mstrVoidReason)
            '    If mstrVoidReason.Length <= 0 Then
            '        Exit Sub
            '    Else
            '        objLoan_Advance._Voidreason = mstrVoidReason
            '    End If
            '    frm = Nothing
            '    objLoan_Advance._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            '    objLoan_Advance._Voiduserunkid = User._Object._Userunkid
            '    'Nilay (25-Mar-2016) -- Start
            '    'objLoan_Advance.Delete(CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value))
            '    objLoan_Advance.Delete(FinancialYear._Object._DatabaseName, _
            '                           User._Object._Userunkid, _
            '                           FinancialYear._Object._YearUnkid, _
            '                           Company._Object._Companyunkid, _
            '                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                           ConfigParameter._Object._UserAccessModeSetting, _
            '                           True, _
            '                           CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value))
            '    'Nilay (25-Mar-2016) -- End
            '    If objLoan_Advance._Message <> "" Then
            '        eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
            '    Else
            '        mintSelectedRowIndex = -1
            '        Call FillList()
            '    End If
            'End If

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Loan?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
            '    gbFilterCriteria.Enabled = False
            '    dgvLoanAdvanceList.Enabled = False
            '    objFooter.Enabled = False
            '    txtAssignerRemarks.Text = ""
            '    gbAssignerRemark.Visible = True
            '    gbAssignerRemark.BringToFront()
            'Else
            '    gbFilterCriteria.Enabled = True
            '    dgvLoanAdvanceList.Enabled = True
            '    objFooter.Enabled = True
            '    txtAssignerRemarks.Text = ""
            '    gbAssignerRemark.Visible = False
            'End If
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Loan?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Sub
            End If
            Dim frm As New frmRemark
            Dim strRemarks As String = ""

            frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 16, "Assigner's Remarks")

            If frm.displayDialog(strRemarks, enArutiApplicatinType.Aruti_Payroll) = False Then
                Exit Sub
            End If

            objLoan_Advance._Isvoid = True
            objLoan_Advance._Voidreason = strRemarks
            objLoan_Advance._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objLoan_Advance._Voiduserunkid = User._Object._Userunkid


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objLoan_Advance._FormName = mstrModuleName
            objLoan_Advance._LoginEmployeeUnkid = 0
            objLoan_Advance._ClientIP = getIP()
            objLoan_Advance._HostName = getHostName()
            objLoan_Advance._FromWeb = False
            objLoan_Advance._AuditUserId = User._Object._Userunkid
objLoan_Advance._CompanyUnkid = Company._Object._Companyunkid
            objLoan_Advance._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            objLoan_Advance.Delete(FinancialYear._Object._DatabaseName, _
                                   User._Object._Userunkid, _
                                   FinancialYear._Object._YearUnkid, _
                                   Company._Object._Companyunkid, _
                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                   ConfigParameter._Object._UserAccessModeSetting, _
                                   True, _
                                   CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), _
                                   True)

            If objLoan_Advance._Message <> "" Then
                eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
                Exit Sub
            Else
                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = True Then

                    Dim objProcessLoan As New clsProcess_pending_loan


                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                    objProcessLoan._FormName = mstrModuleName
                    objProcessLoan._Loginemployeeunkid = 0
                    objProcessLoan._ClientIP = getIP()
                    objProcessLoan._HostName = getHostName()
                    objProcessLoan._FromWeb = False
                    objProcessLoan._AuditUserId = User._Object._Userunkid
objProcessLoan._CompanyUnkid = Company._Object._Companyunkid
                    objProcessLoan._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    'S.SANDEEP [28-May-2018] -- END
                    Dim enEmailType As clsProcess_pending_loan.enApproverEmailType
                    If CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value) = True Then
                        enEmailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
                    Else
                        enEmailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
                    End If

                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objProcessLoan.Send_Notification_Employee(CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhemployeeunkid.Index).Value), _
                    '                                          CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), _
                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.DELETE_ASSIGNED, _
                    '                                          enEmailType, _
                    '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                    '                                              strRemarks, , , , False) 'Nilay (27-Dec-2016) -- [False]
                    objProcessLoan.Send_Notification_Employee(CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhemployeeunkid.Index).Value), _
                                                              CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), _
                                                              clsProcess_pending_loan.enNoticationLoanStatus.DELETE_ASSIGNED, _
                                                              enEmailType, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, _
                                                                  strRemarks, , , , False)
                    'Sohail (30 Nov 2017) -- End

                    objProcessLoan.Send_Notification_Assign(CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhemployeeunkid.Index).Value), _
                                                            FinancialYear._Object._YearUnkid, _
                                                            ConfigParameter._Object._ArutiSelfServiceURL, _
                                                            CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhProcessPendingUnkid.Index).Value), _
                                                            FinancialYear._Object._DatabaseName, _
                                                            Company._Object._Companyunkid, _
                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                            ConfigParameter._Object._UserAccessModeSetting, _
                                                            ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                            True, enLogin_Mode.DESKTOP, 0, 0, , _
                                                            ConfigParameter._Object._Notify_LoanAdvance_Users)
                    'Hemant (30 Aug 2019) -- [ConfigParameter._Object._Notify_LoanAdvance_Users]
                End If
                'Nilay (10-Dec-2016) -- End

                mintSelectedRowIndex = -1
                gbFilterCriteria.Enabled = True
                dgvLoanAdvanceList.Enabled = True
                objFooter.Enabled = True
                Call FillList()
            End If
            'Nilay (20-Sept-2016) -- End
            'Nilay (23-Aug-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmNewLoanAdvance_AddEdit
        Try
            'Nilay (27-Oct-2016) -- Start
            'If dgvLoanAdvanceList.SelectedRows.Count <= 0 Then Exit Sub
            If dgvLoanAdvanceList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                dgvLoanAdvanceList.Select()
                Exit Sub
            End If
            'Nilay (27-Oct-2016) -- End

            'SHANI (11 JUL 2015) -- Start
            'Enhancement : Replacing all List view with Grid view on Close Year Wizard
            If dgvLoanAdvanceList.SelectedRows(0).Cells(dgcolhEmployee.Index).Value.ToString.Contains("GRAND TOTAL") Then Exit Sub
            'SHANI (11 JUL 2015) -- End 
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Select Case CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhstatusunkid.Index).Value)
                Case 3, 4
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot Edit/Delete Loan/Advance. Reason : Loan/Advance status is writtenoff or completed."), enMsgBoxStyle.Information)
                    Exit Sub
            End Select
            If frm.displayDialog(CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), enAction.EDIT_ONE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click, objLoanSchemeSearch.Click
        Dim objFrm As New frmCommonSearch
        Try
            With objFrm
                If CType(sender, eZee.Common.eZeeGradientButton).Name = "objbtnSearchEmployee" Then
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .CodeMember = "employeecode"
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name = "objLoanSchemeSearch" Then
                    .ValueMember = cboLoanScheme.ValueMember
                    .DisplayMember = cboLoanScheme.DisplayMember
                    .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                    'Nilay (10-Dec-2015) -- Start
                    .CodeMember = "code"
                    'Nilay (10-Dec-2015) -- End

                End If
            End With

            If objFrm.DisplayDialog Then
                If CType(sender, eZee.Common.eZeeGradientButton).Name = "objbtnSearchEmployee" Then
                    cboEmployee.SelectedValue = objFrm.SelectedValue
                    cboEmployee.Focus()
                ElseIf CType(sender, eZee.Common.eZeeGradientButton).Name = "objLoanSchemeSearch" Then
                    cboLoanScheme.SelectedValue = objFrm.SelectedValue
                    cboLoanScheme.Focus()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Try
            Dim mdtLoanAdvanceList As DataTable = Nothing

            mdtLoanAdvanceList = CType(dgvLoanAdvanceList.DataSource, DataTable)

            If mdtLoanAdvanceList Is Nothing OrElse mdtLoanAdvanceList.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please generate list to Preview report."), enMsgBoxStyle.Information)
                dgvLoanAdvanceList.Select()
                Exit Sub
            End If

            'SHANI (11 JUL 2015) -- Start
            'Enhancement : Replacing all List view with Grid view on Close Year Wizard
            Dim dtTable As DataTable = mdtLoanAdvanceList.Copy
            dtTable.Rows.RemoveAt(dtTable.Rows.Count - 1)
            'SHANI (11 JUL 2015) -- End 

            Dim objDic As New Dictionary(Of Integer, String)

            'Nilay (28-Aug-2015) -- Start
            'To preview Future Loan
            Dim dsList As DataSet
            Dim objMaster As New clsMasterData
            dsList = objMaster.GetLoan_Saving_Status("Status", False)
            Dim dRow As DataRow = dsList.Tables("Status").NewRow
            dRow.Item("Id") = "-999"
            dRow.Item("NAME") = Language.getMessage(mstrModuleName, 13, "Future Loan")
            dsList.Tables("Status").Rows.Add(dRow)
            dsList.Tables("Status").AcceptChanges()
            'Nilay (28-Aug-2015) -- End

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            'For Each dsRow As DataRow In CType(cboStatus.DataSource, DataTable).Rows
            '    objDic.Add(CInt(dsRow.Item("Id")), dsRow.Item("Name").ToString)
            'Next

            'Nilay (28-Aug-2015) -- Start
            'objDic = (From p In CType(cboStatus.DataSource, DataTable) Select (p)).ToDictionary(Function(x) CInt(x.Item("Id")), Function(y) y.Item("Name").ToString)
            objDic = (From p In dsList.Tables("Status") Select (p)).ToDictionary(Function(x) CInt(x.Item("Id")), Function(y) y.Item("Name").ToString)
            'Nilay (28-Aug-2015) -- End

            'Sohail (07 May 2015) -- End

            Dim objRptLoanAdvance As New ArutiReports.clsLoanAdvanceReport

            'SHANI (11 JUL 2015) -- Start
            'Enhancement : Replacing all List view with Grid view on Close Year Wizard
            'objRptLoanAdvance._Data_LoanAdvance = mdtLoanAdvanceList
            objRptLoanAdvance._Data_LoanAdvance = dtTable
            'SHANI (11 JUL 2015) -- End
            objRptLoanAdvance._Status_Dic = objDic
            objRptLoanAdvance._FilterTitle = mstrFilterTitle
            objRptLoanAdvance._Data_LoanBalance = mdtLoanBalance
            objRptLoanAdvance._CountryUnkid = CInt(cboCurrency.SelectedValue)
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objRptLoanAdvance.generateReport(0, enPrintAction.Preview, enExportAction.None)
            objRptLoanAdvance.generateReportNew(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                True, _
                                                ConfigParameter._Object._ExportReportPath, _
                                                ConfigParameter._Object._OpenAfterExport, _
                                                0, enPrintAction.Preview, enExportAction.None)
            'Nilay (10-Oct-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPreview_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnChangeStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
        Dim frm As New frmLoanStatus_AddEdit
        Try
            'Nilay (01-Apr-2016) -- Start
            If dgvLoanAdvanceList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation."), enMsgBoxStyle.Information)
                dgvLoanAdvanceList.Select()
                Exit Sub
            End If
            'Nilay (01-Apr-2016) -- End

            If dgvLoanAdvanceList.SelectedRows.Count > 0 Then
                'SHANI (11 JUL 2015) -- Start
                'Enhancement : Replacing all List view with Grid view on Close Year Wizard
                If dgvLoanAdvanceList.SelectedRows(0).Cells(dgcolhEmployee.Index).Value.ToString.Contains("GRAND TOTAL") Then Exit Sub
                'SHANI (11 JUL 2015) -- End 
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                'Hemant (16 Jan 2019) -- Start
                'Support Issue Id # 3365 (TANAPA) : Even the "Change status" button gives a message that the loans are complete hence they cant be Edited in 76.1.
                'objLoan_Advance._Loanadvancetranunkid = CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value)

                'Select Case objLoan_Advance._LoanStatus
                Select Case CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhstatusunkid.Index).Value)
                    'Hemant (16 Jan 2019) -- End
                    Case 3, 4
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot Edit/Delete Loan/Advance. Reason : Loan/Advance status is writtenoff or completed."), enMsgBoxStyle.Information)
                        Exit Sub
                End Select

                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                Dim objlnOtherOpapprovalTran As New clsloanotherop_approval_tran
                Dim blnFlag As Boolean
                blnFlag = objlnOtherOpapprovalTran.IsAllowedLnAdvChangeStatusOrDelete(CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), _
                                                                              CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhemployeeunkid.Index).Value))
                If blnFlag = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, Some of Operations either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process. In order to Change Status or Delete, please either Approve or Reject pending operations."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
                'Nilay (01-Apr-2016) -- End

                frm.displayDialog(CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value), CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhstatusunkid.Index).Value))

                Call FillList()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnChangeStatus_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'Sohail (17 Feb 2021) -- Start
    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
    Private Sub btnEmailClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEmailClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEmailOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailOk.Click
        If dgvLoanAdvanceList.RowCount <= 0 Then Exit Sub

        Dim objFields As New clsLetterFields
        Dim blnFlag As Boolean = False
        Dim objPeriod As New clscommom_period_Tran
        Dim dtTable As DataTable = CType(dgvLoanAdvanceList.DataSource, DataTable)

        If dtTable.Select("ischecked = 1 ").Length <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Please check atleast one employee to send email."), enMsgBoxStyle.Information)
            Exit Sub
        End If


        Try
            mstrLoanAdvanceTranUnkids = String.Join(",", (From p In dtTable Where (CBool(p.Item("ischecked")) = True) Select (p.Item("loanadvancetranunkid").ToString)).ToArray)

            If mstrLoanAdvanceTranUnkids.Length > 0 Then

                Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1, , True)

                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intFirstOpenPeriodID

                dsEmailListData = objFields.GetEmployeeData(mstrLoanAdvanceTranUnkids, enImg_Email_RefId.Loan_Balance, objPeriod._End_Date, Company._Object._Companyunkid, FinancialYear._Object._DatabaseName, "DataTable", dtTable.Select("IsChecked = 1 ").CopyToDataTable)

                If dsEmailListData.Tables(0).Rows.Count > 0 Then
                    Dim dtRow() As DataRow = dsEmailListData.Tables(0).Select("Email = ''")
                    If dtRow.Length > 0 Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Some of the employees email addresses are blank. Do you want to continue? "), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        Else
                            For i As Integer = 0 To dtRow.Length - 1
                                dsEmailListData.Tables(0).Rows.Remove(dtRow(i))
                            Next
                        End If
                    End If
                End If
            End If

            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEmailOk_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (17 Feb 2021) -- End

    'Nilay (23-Aug-2016) -- Start
    'Enhancement - Create New Loan Notification 

    'Nilay (20-Sept-2016) -- Start
    'Enhancement : Cancel feature for approved but not assigned loan application
    'Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
    '    Try
    '        If txtAssignerRemarks.Text.Trim = "" Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, Assigner remark cannot be blank. Please enter reason to delete Loan Assignment."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        objLoan_Advance._Isvoid = True
    '        objLoan_Advance._Voidreason = txtAssignerRemarks.Text
    '        objLoan_Advance._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '        objLoan_Advance._Voiduserunkid = User._Object._Userunkid

    '        objLoan_Advance.Delete(FinancialYear._Object._DatabaseName, _
    '                               User._Object._Userunkid, _
    '                               FinancialYear._Object._YearUnkid, _
    '                               Company._Object._Companyunkid, _
    '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                               ConfigParameter._Object._UserAccessModeSetting, _
    '                               True, _
    '                               CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value))

    '        'Nilay (10-Sept-2016) -- Start
    '        'Enhancement - Create New Loan Notification 
    '        'Dim objProcessLoan As New clsProcess_pending_loan

    '        'Dim enEmailType As clsProcess_pending_loan.enApproverEmailType
    '        'If CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value) = True Then
    '        '    enEmailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
    '        'Else
    '        '    enEmailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
    '        'End If

    '        'objProcessLoan.Send_Notification_Employee(CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhemployeeunkid.Index).Value), _
    '        '                                          CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), _
    '        '                                          clsProcess_pending_loan.enNoticationLoanStatus.DELETE_ASSIGNED, _
    '        '                                          enEmailType, _
    '        '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), txtAssignerRemarks.Text)

    '        'objProcessLoan.Send_Notification_Assign(CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhemployeeunkid.Index).Value), _
    '        '                                        FinancialYear._Object._YearUnkid, _
    '        '                                        ConfigParameter._Object._ArutiSelfServiceURL, _
    '        '                                        CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhProcessPendingUnkid.Index).Value), _
    '        '                                        FinancialYear._Object._DatabaseName, _
    '        '                                        Company._Object._Companyunkid, _
    '        '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '        '                                        ConfigParameter._Object._UserAccessModeSetting, _
    '        '                                        ConfigParameter._Object._IsIncludeInactiveEmp, _
    '        '                                        True, enLogin_Mode.DESKTOP, 0, 0)
    '        'Nilay (10-Sept-2016) -- End

    '        If objLoan_Advance._Message <> "" Then
    '            eZeeMsgBox.Show(objLoan_Advance._Message, enMsgBoxStyle.Information)
    '            Exit Sub
    '        Else
    '            'Nilay (10-Sept-2016) -- Start
    '            'Enhancement - Create New Loan Notification 
    'Dim objProcessLoan As New clsProcess_pending_loan

    'Dim enEmailType As clsProcess_pending_loan.enApproverEmailType
    'If CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value) = True Then
    '    enEmailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
    'Else
    '    enEmailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
    'End If

    'objProcessLoan.Send_Notification_Employee(CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhemployeeunkid.Index).Value), _
    '                                          CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), _
    '                                          clsProcess_pending_loan.enNoticationLoanStatus.DELETE_ASSIGNED, _
    '                                          enEmailType, _
    '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), txtAssignerRemarks.Text)

    'objProcessLoan.Send_Notification_Assign(CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhemployeeunkid.Index).Value), _
    '                                        FinancialYear._Object._YearUnkid, _
    '                                        ConfigParameter._Object._ArutiSelfServiceURL, _
    '                                        CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhProcessPendingUnkid.Index).Value), _
    '                                        FinancialYear._Object._DatabaseName, _
    '                                        Company._Object._Companyunkid, _
    '                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                        ConfigParameter._Object._UserAccessModeSetting, _
    '                                        ConfigParameter._Object._IsIncludeInactiveEmp, _
    '                                        True, enLogin_Mode.DESKTOP, 0, 0)
    '            'Nilay (10-Sept-2016) -- End
    '            mintSelectedRowIndex = -1
    '            gbFilterCriteria.Enabled = True
    '            dgvLoanAdvanceList.Enabled = True
    '            objFooter.Enabled = True
    '            gbAssignerRemark.Visible = False
    '            Call FillList()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
    '    Finally
    '        Me.Cursor = Cursors.Default
    '    End Try
    'End Sub
    'Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        gbFilterCriteria.Enabled = True
    '        dgvLoanAdvanceList.Enabled = True
    '        objFooter.Enabled = True
    '        txtAssignerRemarks.Text = ""
    '        gbAssignerRemark.Visible = False
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
    '    End Try
    'End Sub
    'Nilay (20-Sept-2016) -- End




    'Nilay (23-Aug-2016) -- End

#End Region

#Region " Context Menu Events "

    Private Sub mnuPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPayment.Click
        Dim frm As New frmPaymentList
        Try
            If dgvLoanAdvanceList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation."), enMsgBoxStyle.Information)
                dgvLoanAdvanceList.Select()
                Exit Sub
            End If

            'SHANI (11 JUL 2015) -- Start
            'Enhancement : Replacing all List view with Grid view on Close Year Wizard
            If dgvLoanAdvanceList.SelectedRows(0).Cells(dgcolhEmployee.Index).Value.ToString.Contains("GRAND TOTAL") Then Exit Sub
            'SHANI (11 JUL 2015) -- End 

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim objPendingLoan As New clsProcess_pending_loan
            objLoan_Advance._Loanadvancetranunkid = CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value)
            objPendingLoan._Processpendingloanunkid = objLoan_Advance._Processpendingloanunkid
            If objPendingLoan._Isexternal_Entity = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry you cannot make payment operation on Loan/Advance.Reason : This Loan/Advance is from external Enity."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            objPendingLoan = Nothing

            If CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value) = True Then
                frm._Reference_Id = enPaymentRefId.LOAN
            Else
                frm._Reference_Id = enPaymentRefId.ADVANCE
            End If
            frm._Transaction_Id = CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value)
            frm._PaymentType_Id = enPayTypeId.PAYMENT
            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            frm._LoanSavingEffectiveDate = eZeeDate.convertDate(DirectCast(dgvLoanAdvanceList.SelectedRows(0).DataBoundItem, DataRowView).Item("effective_date").ToString)
            'Sohail (12 Dec 2015) -- End

            frm.ShowDialog()

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPayment_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuReceived_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReceived.Click
        Dim frm As New frmPaymentList
        Try
            If dgvLoanAdvanceList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation."), enMsgBoxStyle.Information)
                dgvLoanAdvanceList.Select()
                Exit Sub
            End If

            'SHANI (11 JUL 2015) -- Start
            'Enhancement : Replacing all List view with Grid view on Close Year Wizard
            If dgvLoanAdvanceList.SelectedRows(0).Cells(dgcolhEmployee.Index).Value.ToString.Contains("GRAND TOTAL") Then Exit Sub
            'SHANI (11 JUL 2015) -- End 

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim objPendingLoan As New clsProcess_pending_loan

            objLoan_Advance._Loanadvancetranunkid = CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value)
            objPendingLoan._Processpendingloanunkid = objLoan_Advance._Processpendingloanunkid

            If objPendingLoan._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry you cannot make payment operation on Loan/Advance.Reason : This Loan/Advance is from external Enity."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            objPendingLoan = Nothing

            If CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value) = True Then
                frm._Reference_Id = enPaymentRefId.LOAN
            Else
                frm._Reference_Id = enPaymentRefId.ADVANCE
            End If
            frm._Transaction_Id = CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value)
            frm._PaymentType_Id = enPayTypeId.RECEIVED
            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            frm._LoanSavingEffectiveDate = eZeeDate.convertDate(DirectCast(dgvLoanAdvanceList.SelectedRows(0).DataBoundItem, DataRowView).Item("effective_date").ToString)
            'Sohail (12 Dec 2015) -- End

            frm.ShowDialog()

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuReceived_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuViewApprovalForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewApprovalForm.Click
        Try
            If dgvLoanAdvanceList.SelectedRows.Count > 0 Then

                'SHANI (11 JUL 2015) -- Start
                'Enhancement : Replacing all List view with Grid view on Close Year Wizard
                If dgvLoanAdvanceList.SelectedRows(0).Cells(dgcolhEmployee.Index).Value.ToString.Contains("GRAND TOTAL") Then Exit Sub
                'SHANI (11 JUL 2015) -- End 

                Dim frm As New ArutiReports.frmBBL_Loan_Report
                frm.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
                frm.StartPosition = FormStartPosition.CenterParent
                frm.WindowState = FormWindowState.Normal
                frm._EmpId = CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhemployeeunkid.Index).Value)
                'Sohail (29 Apr 2020) -- Start
                'Ifakara Enhancement # 0004668 : Advance Approve Form.
                frm._IsLoan = CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value)
                'Hemant (12 Nov 2021) -- Start
                'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
                frm._LoanSchemeId = CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanSchemeUnkid.Index).Value)
                frm._ProcessPendingLoanunkId = CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhProcessPendingUnkid.Index).Value)
                frm._IsFromLoanAdvanceListScreen = True
                'Hemant (12 Nov 2021) -- End
                'Sohail (29 Apr 2020) -- End
                frm.ShowDialog()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuViewApprovalForm_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuChangeStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuChangeStatus.Click
        Dim frm As New frmGlobalLoanStatus_Change
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm.ShowDialog()
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuChangeStatus_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuLoanOperation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuLoanOperation.Click
        Dim frm As New frmLoanAdvanceOperationList
        Try
            If dgvLoanAdvanceList.SelectedRows.Count > 0 Then

                'SHANI (11 JUL 2015) -- Start
                'Enhancement : Replacing all List view with Grid view on Close Year Wizard
                If dgvLoanAdvanceList.SelectedRows(0).Cells(dgcolhEmployee.Index).Value.ToString.Contains("GRAND TOTAL") Then Exit Sub
                'SHANI (11 JUL 2015) -- End 

                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                If frm.displayDialog(dgvLoanAdvanceList.SelectedRows(0).Cells(dgcolhEmployee.Index).Value.ToString, _
                                     dgvLoanAdvanceList.SelectedRows(0).Cells(dgcolhLoanScheme.Index).Value.ToString, _
                                     dgvLoanAdvanceList.SelectedRows(0).Cells(dgcolhVocNo.Index).Value.ToString, _
                                     CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value)) Then
                    Call FillList()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuLoanOperation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Nilay (04-Nov-2016) -- Start
    'Enhancements: Global Change Status feature requested by {Rutta}
    Private Sub mnuGlobalChangeStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuGlobalChangeStatus.Click
        Try
            Dim frm As New frmGlobalChangeStatus
            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalChangeStatus_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (04-Nov-2016) -- End

#End Region

#Region " Data Grid Events "

    Private Sub dgvLoanAdvanceList_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLoanAdvanceList.SelectionChanged
        Try
            If dgvLoanAdvanceList.SelectedRows.Count > 0 Then

                'Sohail (05 Jul 2018) -- Start
                'Internal Issue - Conversion from DBNull to type Boolean is not valid in 72.1.
                'S.SANDEEP [04-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
                'If CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value) = True Then
                '    mnuViewApprovalForm.Enabled = True
                'End If
                'S.SANDEEP [04-May-2018] -- END
                'Sohail (05 Jul 2018) -- End

                'SHANI (11 JUL 2015) -- Start
                'Enhancement : Replacing all List view with Grid view on Close Year Wizard
                If dgvLoanAdvanceList.SelectedRows(0).Cells(dgcolhEmployee.Index).Value.ToString.Contains("GRAND TOTAL") Then
                    'Nilay (04-Nov-2016) -- Start
                    'Enhancements: Global Change Status feature requested by {Rutta}
                    'btnOperations.Enabled = False
                    Call SetVisibilityOperationMenu(False)
                    'Nilay (04-Nov-2016) -- End
                    btnChangeStatus.Enabled = False
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                    Exit Sub
                Else
                    'Nilay (04-Nov-2016) -- Start
                    'Enhancements: Global Change Status feature requested by {Rutta}
                    'btnOperations.Enabled = True
                    Call SetVisibilityOperationMenu(True)
                    'Nilay (04-Nov-2016) -- End
                    btnChangeStatus.Enabled = True
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True

                    'Sohail (05 Jul 2018) -- Start
                    'Internal Issue - Conversion from DBNull to type Boolean is not valid in 72.1.
                    If CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value) = True Then
                        mnuViewApprovalForm.Enabled = True
                    End If
                    'Sohail (05 Jul 2018) -- End

                End If
                'SHANI (11 JUL 2015) -- End 

                mintSelectedRowIndex = dgvLoanAdvanceList.SelectedRows(0).Index

                'Nilay (04-Nov-2016) -- Start
                'Enhancements: Global Change Status feature requested by {Rutta}
                'Call SetVisibility()
                'Nilay (04-Nov-2016) -- End

                Select Case CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhstatusunkid.Index).Value)
                    Case 2
                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        'btnOperations.Enabled = False
                        Call SetVisibilityOperationMenu(False)
                        'Nilay (04-Nov-2016) -- End
                        btnChangeStatus.Enabled = True
                    Case 3, 4
                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        'btnOperations.Enabled = False
                        Call SetVisibilityOperationMenu(False)
                        'Nilay (04-Nov-2016) -- End
                        btnChangeStatus.Enabled = False

                        'Nilay (28-Aug-2015) -- Start
                    Case -999
                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        'btnOperations.Enabled = False
                        Call SetVisibilityOperationMenu(False)
                        'Nilay (04-Nov-2016) -- End
                        btnChangeStatus.Enabled = False
                        'Nilay (28-Aug-2015) -- End
                    Case Else
                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        'btnOperations.Enabled = True
                        Call SetVisibilityOperationMenu(True)
                        'Nilay (04-Nov-2016) -- End
                        btnChangeStatus.Enabled = True
                End Select

                Dim objPaymentTran As New clsPayment_tran
                Dim ePRef As enPaymentRefId
                If CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value) = True Then
                    ePRef = enPaymentRefId.LOAN
                Else
                    ePRef = enPaymentRefId.ADVANCE
                    'S.SANDEEP [04-May-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
                    'Sohail (29 Apr 2020) -- Start
                    'Ifakara Enhancement # 0004668 : Advance Approve Form.
                    'mnuViewApprovalForm.Enabled = False
                    'Sohail (29 Apr 2020) -- End
                    'S.SANDEEP [04-May-2018] -- END
                End If

                If objPaymentTran.IsPaymentDone(enPayTypeId.PAYMENT, CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value), ePRef) = False Then

                    mnuReceived.Enabled = False
                    'Nilay (15-Dec-2015) -- Start
                    mnuLoanOperation.Enabled = False
                    'Nilay (15-Dec-2015) -- End

                    Dim objPendingLoan As New clsProcess_pending_loan
                    objLoan_Advance._Loanadvancetranunkid = CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhLoanAdvanceunkid.Index).Value)
                    objPendingLoan._Processpendingloanunkid = objLoan_Advance._Processpendingloanunkid
                    'Nilay (04-Nov-2016) -- Start
                    'Enhancements: Global Change Status feature requested by {Rutta}
                    'If objPendingLoan._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount > 0 Then
                    'Sohail (19 Dec 2018) -- Start
                    'Telematics Issue : Unable to do other loan operation due to loan status in advance_tran table is 'completed' but loan is 'in progress' in loan_status_tran table in 76.1.
                    'If objPendingLoan._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount > 0 _
                    '    AndAlso objLoan_Advance._LoanStatus = 1 Then
                    If objPendingLoan._Isexternal_Entity = True AndAlso objLoan_Advance._Balance_Amount > 0 _
                        AndAlso CInt(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhstatusunkid.Index).Value) = 1 Then
                        'Sohail (19 Dec 2018) -- End
                        'Nilay (04-Nov-2016) -- End
                        mnuReceived.Enabled = True
                        'Nilay (15-Dec-2015) -- Start

                        'Nilay (27-Oct-2016) -- Start
                        'mnuLoanOperation.Enabled = True
                        If ePRef = enPaymentRefId.LOAN Then
                            mnuLoanOperation.Enabled = True
                        ElseIf ePRef = enPaymentRefId.ADVANCE Then
                            mnuLoanOperation.Enabled = False
                        End If
                        'Nilay (27-Oct-2016) -- End

                        'Nilay (15-Dec-2015) -- End
                    End If
                Else
                    'Nilay (04-Nov-2016) -- Start
                    'Enhancements: Global Change Status feature requested by {Rutta}
                    'mnuReceived.Enabled = True
                    ''Nilay (15-Dec-2015) -- Start
                    'mnuLoanOperation.Enabled = True
                    ''Nilay (15-Dec-2015) -- End
                    'Nilay (04-Nov-2016) -- End
                End If
                objPaymentTran = Nothing

                If CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value) = False Then
                    mnuLoanOperation.Visible = False
                Else
                    mnuLoanOperation.Visible = True
                End If

                'Shani(26-Nov-2015) -- Start
                'ENHANCEMENT : Add Loan Import Form
                mnuLoanOperation.Visible = CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhVisibleStatus.Index).Value)
                btnChangeStatus.Enabled = CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhVisibleStatus.Index).Value)
                'Shani(26-Nov-2015) -- End

                'Pinkal (01-May-2018) - Start
                'Enhancement  [Ref # 204] Loan Approvers list should be shown in the loan approval form..
                'Sohail (29 Apr 2020) -- Start
                'Ifakara Enhancement # 0004668 : Advance Approve Form.
                'mnuViewApprovalForm.Enabled = CBool(dgvLoanAdvanceList.SelectedRows(0).Cells(objdgcolhIsLoan.Index).Value)
                'Sohail (29 Apr 2020) -- End
                'Pinkal (01-May-2018) - End
               

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvLoanAdvanceList_SelectionChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (17 Feb 2021) -- Start
    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
    Private Sub dgvLoanAdvanceList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLoanAdvanceList.CellContentClick, dgvLoanAdvanceList.CellContentDoubleClick
        Try
            If dgvLoanAdvanceList.RowCount <= 0 Then Exit Sub
            If e.ColumnIndex = objdgcolhCheck.Index Then
                Dim dtTable As DataTable = CType(dgvLoanAdvanceList.DataSource, DataTable)

                If Me.dgvLoanAdvanceList.IsCurrentCellDirty Then
                    Me.dgvLoanAdvanceList.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
                Dim dtmp() As DataRow = dtTable.Select("isChecked = True")
                If dtmp.Length > 0 Then
                    If dtmp.Length < dtTable.Rows.Count Then
                        objChkAll.CheckState = CheckState.Indeterminate
                    ElseIf dtmp.Length = dtTable.Rows.Count Then
                        objChkAll.CheckState = CheckState.Checked
                    End If
                Else
                    objChkAll.CheckState = CheckState.Unchecked
                End If
                AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvApplicantList_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvLoanAdvanceList_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvLoanAdvanceList.DataError

    End Sub
    'Sohail (17 Feb 2021) -- End

#End Region

    'Nilay (27-Oct-2016) -- Start
    'Enhancements: Searchable Dropdown
#Region " ComboBox's Events "
    Private Sub cboEmployee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmployee.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmployee.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) < 0 Then Call SetDefaultSearchText(cboEmployee)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.GotFocus
        Try
            With cboEmployee
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmployee_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.Leave
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cboEmployee)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboLoanScheme.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboLoanScheme.ValueMember
                    .DisplayMember = cboLoanScheme.DisplayMember
                    .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                    .CodeMember = "Code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboLoanScheme.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboLoanScheme.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged
        Try
            If CInt(cboLoanScheme.SelectedValue) < 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.GotFocus
        Try
            With cboLoanScheme
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.Leave
        Try
            If CInt(cboLoanScheme.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_Leave", mstrModuleName)
        End Try
    End Sub
#End Region
    'Nilay (27-Oct-2016) -- End

    'Sohail (17 Feb 2021) -- Start
    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
#Region " Checkbox Event(s) "

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            If dgvLoanAdvanceList.RowCount <= 0 Then Exit Sub
            Dim dtTable As DataTable = CType(dgvLoanAdvanceList.DataSource, DataTable)
            RemoveHandler dgvLoanAdvanceList.CellContentClick, AddressOf dgvLoanAdvanceList_CellContentClick
            For Each row As DataRow In dtTable.Rows
                row.Item("isChecked") = objChkAll.Checked
            Next
            dtTable.AcceptChanges()
            AddHandler dgvLoanAdvanceList.CellContentClick, AddressOf dgvLoanAdvanceList_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'Sohail (17 Feb 2021) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


			
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnChangeStatus.GradientBackColor = GUI._ButttonBackColor
            Me.btnChangeStatus.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperations.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperations.GradientForeColor = GUI._ButttonFontColor

            Me.btnPreview.GradientBackColor = GUI._ButttonBackColor
            Me.btnPreview.GradientForeColor = GUI._ButttonFontColor

			Me.btnEmailClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEmailClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnEmailOk.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEmailOk.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.Name, Me.lblLoanAdvance.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnChangeStatus.Text = Language._Object.getCaption(Me.btnChangeStatus.Name, Me.btnChangeStatus.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblVocNo.Text = Language._Object.getCaption(Me.lblVocNo.Name, Me.lblVocNo.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.Name, Me.btnOperations.Text)
            Me.mnuPayment.Text = Language._Object.getCaption(Me.mnuPayment.Name, Me.mnuPayment.Text)
            Me.mnuReceived.Text = Language._Object.getCaption(Me.mnuReceived.Name, Me.mnuReceived.Text)
            Me.btnPreview.Text = Language._Object.getCaption(Me.btnPreview.Name, Me.btnPreview.Text)
            Me.mnuViewApprovalForm.Text = Language._Object.getCaption(Me.mnuViewApprovalForm.Name, Me.mnuViewApprovalForm.Text)
            Me.mnuChangeStatus.Text = Language._Object.getCaption(Me.mnuChangeStatus.Name, Me.mnuChangeStatus.Text)
            Me.mnuLoanOperation.Text = Language._Object.getCaption(Me.mnuLoanOperation.Name, Me.mnuLoanOperation.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.lblCurrOpenPeriod.Text = Language._Object.getCaption(Me.lblCurrOpenPeriod.Name, Me.lblCurrOpenPeriod.Text)
            Me.lblOtherOpStatus.Text = Language._Object.getCaption(Me.lblOtherOpStatus.Name, Me.lblOtherOpStatus.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
            Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
            Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
            Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
            Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
            Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
            Me.DataGridViewTextBoxColumn22.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn22.Name, Me.DataGridViewTextBoxColumn22.HeaderText)
			Me.DataGridViewTextBoxColumn23.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn23.Name, Me.DataGridViewTextBoxColumn23.HeaderText)
			Me.mnuGlobalChangeStatus.Text = Language._Object.getCaption(Me.mnuGlobalChangeStatus.Name, Me.mnuGlobalChangeStatus.Text)
			Me.lblCalcType.Text = Language._Object.getCaption(Me.lblCalcType.Name, Me.lblCalcType.Text)
			Me.btnEmailClose.Text = Language._Object.getCaption(Me.btnEmailClose.Name, Me.btnEmailClose.Text)
			Me.btnEmailOk.Text = Language._Object.getCaption(Me.btnEmailOk.Name, Me.btnEmailOk.Text)
            Me.dgcolhVocNo.HeaderText = Language._Object.getCaption(Me.dgcolhVocNo.Name, Me.dgcolhVocNo.HeaderText)
            Me.dgcolhPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhPeriod.Name, Me.dgcolhPeriod.HeaderText)
            Me.dgcolhDeductionPeriod.HeaderText = Language._Object.getCaption(Me.dgcolhDeductionPeriod.Name, Me.dgcolhDeductionPeriod.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhLoanScheme.HeaderText = Language._Object.getCaption(Me.dgcolhLoanScheme.Name, Me.dgcolhLoanScheme.HeaderText)
            Me.dgcolhLoanAdvance.HeaderText = Language._Object.getCaption(Me.dgcolhLoanAdvance.Name, Me.dgcolhLoanAdvance.HeaderText)
            Me.dgcolhLoanCalcType.HeaderText = Language._Object.getCaption(Me.dgcolhLoanCalcType.Name, Me.dgcolhLoanCalcType.HeaderText)
            Me.dgcolhInterestCalcType.HeaderText = Language._Object.getCaption(Me.dgcolhInterestCalcType.Name, Me.dgcolhInterestCalcType.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)
            Me.dgcolhCurrency.HeaderText = Language._Object.getCaption(Me.dgcolhCurrency.Name, Me.dgcolhCurrency.HeaderText)
            Me.dgcolhInstallments.HeaderText = Language._Object.getCaption(Me.dgcolhInstallments.Name, Me.dgcolhInstallments.HeaderText)
            Me.dgcolhBalance.HeaderText = Language._Object.getCaption(Me.dgcolhBalance.Name, Me.dgcolhBalance.HeaderText)
            Me.dgcolhPaidLoan.HeaderText = Language._Object.getCaption(Me.dgcolhPaidLoan.Name, Me.dgcolhPaidLoan.HeaderText)
            Me.dgcolhStatus.HeaderText = Language._Object.getCaption(Me.dgcolhStatus.Name, Me.dgcolhStatus.HeaderText)
            Me.dgcolhOtherOpApproval.HeaderText = Language._Object.getCaption(Me.dgcolhOtherOpApproval.Name, Me.dgcolhOtherOpApproval.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Loan/Advance from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot delete this Loan/Advance. Reason : Loan/Advance is brought forward.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Loan?")
            Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "Loan")
            Language.setMessage(mstrModuleName, 6, "Advance")
            Language.setMessage(mstrModuleName, 7, "Sorry, you cannot Edit/Delete Loan/Advance. Reason : Loan/Advance status is writtenoff or completed.")
            Language.setMessage(mstrModuleName, 8, "Sorry, you cannot delete the transaction. Reason : Payment is already done for this transaction.")
            Language.setMessage(mstrModuleName, 9, "Sorry, you cannot delete the transaction. Reason : Repayment is already done for this transaction.")
            Language.setMessage(mstrModuleName, 10, "Sorry, you cannot delete the transaction. Reason : Deduction or Repayment is already done for this transaction.")
            Language.setMessage(mstrModuleName, 11, "Please generate list to Preview report.")
            Language.setMessage(mstrModuleName, 12, "Sorry you cannot make payment operation on Loan/Advance.Reason : This Loan/Advance is from external Enity.")
            Language.setMessage(mstrModuleName, 13, "Future Loan")
            Language.setMessage(mstrModuleName, 14, "* Balance As On:")
            Language.setMessage(mstrModuleName, 15, "Sorry, Some of Operations either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process. In order to Change Status or Delete, please either Approve or Reject pending operations.")
            Language.setMessage(mstrModuleName, 16, "Assigner's Remarks")
            Language.setMessage(mstrModuleName, 17, "Type to Search")
			Language.setMessage(mstrModuleName, 18, "Some of the employees email addresses are blank. Do you want to continue?")
			Language.setMessage(mstrModuleName, 19, "Please check atleast one employee to send email.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class