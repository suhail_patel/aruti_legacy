﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportLoan_AdvanceWizard
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportLoan_AdvanceWizard))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.eZeeWizImportLoan = New eZee.Common.eZeeWizard
        Me.WizPageMapping = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbFieldMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlLoanMapping = New System.Windows.Forms.Panel
        Me.cboLoanBalance = New System.Windows.Forms.ComboBox
        Me.lblLoanBalance = New System.Windows.Forms.Label
        Me.objlblSign12 = New System.Windows.Forms.Label
        Me.cboExternalEntity = New System.Windows.Forms.ComboBox
        Me.lblExternalEntity = New System.Windows.Forms.Label
        Me.objlblSign11 = New System.Windows.Forms.Label
        Me.cboSchduleBy = New System.Windows.Forms.ComboBox
        Me.lblScheduleBy = New System.Windows.Forms.Label
        Me.objlblSign10 = New System.Windows.Forms.Label
        Me.cboLoanSchedule = New System.Windows.Forms.ComboBox
        Me.lblLoanSchedule = New System.Windows.Forms.Label
        Me.objlblSign9 = New System.Windows.Forms.Label
        Me.cboLoanDuration = New System.Windows.Forms.ComboBox
        Me.lblLoanDuration = New System.Windows.Forms.Label
        Me.objlblSign8 = New System.Windows.Forms.Label
        Me.lblInterestRate = New System.Windows.Forms.Label
        Me.cboInterestRate = New System.Windows.Forms.ComboBox
        Me.cboLoanAmount = New System.Windows.Forms.ComboBox
        Me.lblLoanAmount = New System.Windows.Forms.Label
        Me.objlblSign7 = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.objlblSign6 = New System.Windows.Forms.Label
        Me.cboLoanDeductionPeriod = New System.Windows.Forms.ComboBox
        Me.lblDeductionPeriod = New System.Windows.Forms.Label
        Me.objlblSign5 = New System.Windows.Forms.Label
        Me.cboLoanAssignedPeriod = New System.Windows.Forms.ComboBox
        Me.lblAssignedPeriod = New System.Windows.Forms.Label
        Me.objlblSign4 = New System.Windows.Forms.Label
        Me.cboLoanAssignedDate = New System.Windows.Forms.ComboBox
        Me.lblLoanDate = New System.Windows.Forms.Label
        Me.objlblSign3 = New System.Windows.Forms.Label
        Me.cboLoanVocNo = New System.Windows.Forms.ComboBox
        Me.lblVocNo = New System.Windows.Forms.Label
        Me.objlblSign2 = New System.Windows.Forms.Label
        Me.cboEmployeeCode = New System.Windows.Forms.ComboBox
        Me.cboLastname = New System.Windows.Forms.ComboBox
        Me.lblFirstName = New System.Windows.Forms.Label
        Me.cboFirstname = New System.Windows.Forms.ComboBox
        Me.lblEmployeeCode = New System.Windows.Forms.Label
        Me.lblLastName = New System.Windows.Forms.Label
        Me.objlblSign1 = New System.Windows.Forms.Label
        Me.pnlAdvance = New System.Windows.Forms.Panel
        Me.cboAdvaceBalance = New System.Windows.Forms.ComboBox
        Me.lblABalance = New System.Windows.Forms.Label
        Me.objlblASign8 = New System.Windows.Forms.Label
        Me.cboAExEntity = New System.Windows.Forms.ComboBox
        Me.lblAExEntity = New System.Windows.Forms.Label
        Me.objlblASign7 = New System.Windows.Forms.Label
        Me.cboAdvanceAmt = New System.Windows.Forms.ComboBox
        Me.lblAAmount = New System.Windows.Forms.Label
        Me.objlblASign6 = New System.Windows.Forms.Label
        Me.cboADeductPeriod = New System.Windows.Forms.ComboBox
        Me.lblADeductionPeriod = New System.Windows.Forms.Label
        Me.objlblASign5 = New System.Windows.Forms.Label
        Me.cboAAssignPeriod = New System.Windows.Forms.ComboBox
        Me.lblAAssignedPeriod = New System.Windows.Forms.Label
        Me.objlblASign4 = New System.Windows.Forms.Label
        Me.cboAAssignDate = New System.Windows.Forms.ComboBox
        Me.lblAAssignDate = New System.Windows.Forms.Label
        Me.objlblASign3 = New System.Windows.Forms.Label
        Me.cboAdvanceVocNo = New System.Windows.Forms.ComboBox
        Me.lblAVocNo = New System.Windows.Forms.Label
        Me.objlblASign2 = New System.Windows.Forms.Label
        Me.cboAEmployeeCode = New System.Windows.Forms.ComboBox
        Me.cboASurname = New System.Windows.Forms.ComboBox
        Me.lblAFirstname = New System.Windows.Forms.Label
        Me.cboAFirstname = New System.Windows.Forms.ComboBox
        Me.lblAEmpCode = New System.Windows.Forms.Label
        Me.lblASurname = New System.Windows.Forms.Label
        Me.objlblASign1 = New System.Windows.Forms.Label
        Me.lblCaption = New System.Windows.Forms.Label
        Me.WizPageApprover = New eZee.Common.eZeeWizardPage(Me.components)
        Me.gbImportInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lblImportation = New System.Windows.Forms.Label
        Me.radAdvance = New System.Windows.Forms.RadioButton
        Me.radLoan = New System.Windows.Forms.RadioButton
        Me.objbtnSearchLoanApprover = New eZee.Common.eZeeGradientButton
        Me.cboApprovedBy = New System.Windows.Forms.ComboBox
        Me.lblApprover = New System.Windows.Forms.Label
        Me.elImportationType = New eZee.Common.eZeeLine
        Me.elApproverInfo = New eZee.Common.eZeeLine
        Me.WizPageSelectFile = New eZee.Common.eZeeWizardPage(Me.components)
        Me.lblMessage = New System.Windows.Forms.Label
        Me.lblTitle = New System.Windows.Forms.Label
        Me.btnOpenFile = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtFilePath = New eZee.TextBox.AlphanumericTextBox
        Me.lblSelectfile = New System.Windows.Forms.Label
        Me.WizPageImporting = New eZee.Common.eZeeWizardPage(Me.components)
        Me.btnFilter = New eZee.Common.eZeeSplitButton
        Me.cmsFilter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmShowAll = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmSuccessful = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowWarning = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmShowError = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmExportError = New System.Windows.Forms.ToolStripMenuItem
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhlogindate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhstatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlInfo = New System.Windows.Forms.Panel
        Me.ezWait = New eZee.Common.eZeeWait
        Me.objError = New System.Windows.Forms.Label
        Me.objWarning = New System.Windows.Forms.Label
        Me.objSuccess = New System.Windows.Forms.Label
        Me.lblWarning = New System.Windows.Forms.Label
        Me.lblError = New System.Windows.Forms.Label
        Me.objTotal = New System.Windows.Forms.Label
        Me.lblSuccess = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.objbuttonBack = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbuttonNext = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.eZeeWizImportLoan.SuspendLayout()
        Me.WizPageMapping.SuspendLayout()
        Me.gbFieldMapping.SuspendLayout()
        Me.pnlLoanMapping.SuspendLayout()
        Me.pnlAdvance.SuspendLayout()
        Me.WizPageApprover.SuspendLayout()
        Me.gbImportInformation.SuspendLayout()
        Me.WizPageSelectFile.SuspendLayout()
        Me.WizPageImporting.SuspendLayout()
        Me.cmsFilter.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.eZeeWizImportLoan)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(775, 433)
        Me.pnlMainInfo.TabIndex = 0
        '
        'eZeeWizImportLoan
        '
        Me.eZeeWizImportLoan.Controls.Add(Me.WizPageMapping)
        Me.eZeeWizImportLoan.Controls.Add(Me.WizPageApprover)
        Me.eZeeWizImportLoan.Controls.Add(Me.WizPageSelectFile)
        Me.eZeeWizImportLoan.Controls.Add(Me.WizPageImporting)
        Me.eZeeWizImportLoan.Dock = System.Windows.Forms.DockStyle.Fill
        Me.eZeeWizImportLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeWizImportLoan.HeaderImage = Global.Aruti.Main.My.Resources.Resources.importdata
        Me.eZeeWizImportLoan.Location = New System.Drawing.Point(0, 0)
        Me.eZeeWizImportLoan.Name = "eZeeWizImportLoan"
        Me.eZeeWizImportLoan.Pages.AddRange(New eZee.Common.eZeeWizardPage() {Me.WizPageSelectFile, Me.WizPageApprover, Me.WizPageMapping, Me.WizPageImporting})
        Me.eZeeWizImportLoan.Size = New System.Drawing.Size(775, 433)
        Me.eZeeWizImportLoan.TabIndex = 3
        Me.eZeeWizImportLoan.WelcomeImage = Nothing
        '
        'WizPageMapping
        '
        Me.WizPageMapping.Controls.Add(Me.gbFieldMapping)
        Me.WizPageMapping.Location = New System.Drawing.Point(0, 0)
        Me.WizPageMapping.Name = "WizPageMapping"
        Me.WizPageMapping.Size = New System.Drawing.Size(775, 385)
        Me.WizPageMapping.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageMapping.TabIndex = 8
        '
        'gbFieldMapping
        '
        Me.gbFieldMapping.BorderColor = System.Drawing.Color.Black
        Me.gbFieldMapping.Checked = False
        Me.gbFieldMapping.CollapseAllExceptThis = False
        Me.gbFieldMapping.CollapsedHoverImage = Nothing
        Me.gbFieldMapping.CollapsedNormalImage = Nothing
        Me.gbFieldMapping.CollapsedPressedImage = Nothing
        Me.gbFieldMapping.CollapseOnLoad = False
        Me.gbFieldMapping.Controls.Add(Me.pnlLoanMapping)
        Me.gbFieldMapping.Controls.Add(Me.pnlAdvance)
        Me.gbFieldMapping.Controls.Add(Me.lblCaption)
        Me.gbFieldMapping.ExpandedHoverImage = Nothing
        Me.gbFieldMapping.ExpandedNormalImage = Nothing
        Me.gbFieldMapping.ExpandedPressedImage = Nothing
        Me.gbFieldMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFieldMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFieldMapping.HeaderHeight = 25
        Me.gbFieldMapping.HeaderMessage = ""
        Me.gbFieldMapping.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFieldMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFieldMapping.HeightOnCollapse = 0
        Me.gbFieldMapping.LeftTextSpace = 0
        Me.gbFieldMapping.Location = New System.Drawing.Point(165, 0)
        Me.gbFieldMapping.Name = "gbFieldMapping"
        Me.gbFieldMapping.OpenHeight = 300
        Me.gbFieldMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFieldMapping.ShowBorder = True
        Me.gbFieldMapping.ShowCheckBox = False
        Me.gbFieldMapping.ShowCollapseButton = False
        Me.gbFieldMapping.ShowDefaultBorderColor = True
        Me.gbFieldMapping.ShowDownButton = False
        Me.gbFieldMapping.ShowHeader = True
        Me.gbFieldMapping.Size = New System.Drawing.Size(609, 384)
        Me.gbFieldMapping.TabIndex = 1
        Me.gbFieldMapping.Temp = 0
        Me.gbFieldMapping.Text = "Field Mapping"
        Me.gbFieldMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLoanMapping
        '
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanBalance)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanBalance)
        Me.pnlLoanMapping.Controls.Add(Me.objlblSign12)
        Me.pnlLoanMapping.Controls.Add(Me.cboExternalEntity)
        Me.pnlLoanMapping.Controls.Add(Me.lblExternalEntity)
        Me.pnlLoanMapping.Controls.Add(Me.objlblSign11)
        Me.pnlLoanMapping.Controls.Add(Me.cboSchduleBy)
        Me.pnlLoanMapping.Controls.Add(Me.lblScheduleBy)
        Me.pnlLoanMapping.Controls.Add(Me.objlblSign10)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanSchedule)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanSchedule)
        Me.pnlLoanMapping.Controls.Add(Me.objlblSign9)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanDuration)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanDuration)
        Me.pnlLoanMapping.Controls.Add(Me.objlblSign8)
        Me.pnlLoanMapping.Controls.Add(Me.lblInterestRate)
        Me.pnlLoanMapping.Controls.Add(Me.cboInterestRate)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanAmount)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanAmount)
        Me.pnlLoanMapping.Controls.Add(Me.objlblSign7)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanScheme)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanScheme)
        Me.pnlLoanMapping.Controls.Add(Me.objlblSign6)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanDeductionPeriod)
        Me.pnlLoanMapping.Controls.Add(Me.lblDeductionPeriod)
        Me.pnlLoanMapping.Controls.Add(Me.objlblSign5)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanAssignedPeriod)
        Me.pnlLoanMapping.Controls.Add(Me.lblAssignedPeriod)
        Me.pnlLoanMapping.Controls.Add(Me.objlblSign4)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanAssignedDate)
        Me.pnlLoanMapping.Controls.Add(Me.lblLoanDate)
        Me.pnlLoanMapping.Controls.Add(Me.objlblSign3)
        Me.pnlLoanMapping.Controls.Add(Me.cboLoanVocNo)
        Me.pnlLoanMapping.Controls.Add(Me.lblVocNo)
        Me.pnlLoanMapping.Controls.Add(Me.objlblSign2)
        Me.pnlLoanMapping.Controls.Add(Me.cboEmployeeCode)
        Me.pnlLoanMapping.Controls.Add(Me.cboLastname)
        Me.pnlLoanMapping.Controls.Add(Me.lblFirstName)
        Me.pnlLoanMapping.Controls.Add(Me.cboFirstname)
        Me.pnlLoanMapping.Controls.Add(Me.lblEmployeeCode)
        Me.pnlLoanMapping.Controls.Add(Me.lblLastName)
        Me.pnlLoanMapping.Controls.Add(Me.objlblSign1)
        Me.pnlLoanMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlLoanMapping.Location = New System.Drawing.Point(560, 351)
        Me.pnlLoanMapping.Name = "pnlLoanMapping"
        Me.pnlLoanMapping.Size = New System.Drawing.Size(47, 16)
        Me.pnlLoanMapping.TabIndex = 324
        '
        'cboLoanBalance
        '
        Me.cboLoanBalance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanBalance.DropDownWidth = 190
        Me.cboLoanBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanBalance.FormattingEnabled = True
        Me.cboLoanBalance.Location = New System.Drawing.Point(447, 98)
        Me.cboLoanBalance.Name = "cboLoanBalance"
        Me.cboLoanBalance.Size = New System.Drawing.Size(146, 21)
        Me.cboLoanBalance.TabIndex = 48
        '
        'lblLoanBalance
        '
        Me.lblLoanBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanBalance.Location = New System.Drawing.Point(315, 100)
        Me.lblLoanBalance.Name = "lblLoanBalance"
        Me.lblLoanBalance.Size = New System.Drawing.Size(125, 17)
        Me.lblLoanBalance.TabIndex = 47
        Me.lblLoanBalance.Text = "Loan Balance"
        Me.lblLoanBalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign12
        '
        Me.objlblSign12.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign12.ForeColor = System.Drawing.Color.Red
        Me.objlblSign12.Location = New System.Drawing.Point(299, 100)
        Me.objlblSign12.Name = "objlblSign12"
        Me.objlblSign12.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign12.TabIndex = 46
        Me.objlblSign12.Text = "*"
        Me.objlblSign12.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboExternalEntity
        '
        Me.cboExternalEntity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExternalEntity.DropDownWidth = 190
        Me.cboExternalEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExternalEntity.FormattingEnabled = True
        Me.cboExternalEntity.Location = New System.Drawing.Point(447, 71)
        Me.cboExternalEntity.Name = "cboExternalEntity"
        Me.cboExternalEntity.Size = New System.Drawing.Size(146, 21)
        Me.cboExternalEntity.TabIndex = 45
        '
        'lblExternalEntity
        '
        Me.lblExternalEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExternalEntity.Location = New System.Drawing.Point(315, 73)
        Me.lblExternalEntity.Name = "lblExternalEntity"
        Me.lblExternalEntity.Size = New System.Drawing.Size(125, 17)
        Me.lblExternalEntity.TabIndex = 44
        Me.lblExternalEntity.Text = "External Entity"
        Me.lblExternalEntity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign11
        '
        Me.objlblSign11.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign11.ForeColor = System.Drawing.Color.Red
        Me.objlblSign11.Location = New System.Drawing.Point(299, 73)
        Me.objlblSign11.Name = "objlblSign11"
        Me.objlblSign11.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign11.TabIndex = 43
        Me.objlblSign11.Text = "*"
        Me.objlblSign11.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboSchduleBy
        '
        Me.cboSchduleBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSchduleBy.DropDownWidth = 190
        Me.cboSchduleBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSchduleBy.FormattingEnabled = True
        Me.cboSchduleBy.Location = New System.Drawing.Point(447, 44)
        Me.cboSchduleBy.Name = "cboSchduleBy"
        Me.cboSchduleBy.Size = New System.Drawing.Size(146, 21)
        Me.cboSchduleBy.TabIndex = 42
        '
        'lblScheduleBy
        '
        Me.lblScheduleBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScheduleBy.Location = New System.Drawing.Point(315, 46)
        Me.lblScheduleBy.Name = "lblScheduleBy"
        Me.lblScheduleBy.Size = New System.Drawing.Size(125, 17)
        Me.lblScheduleBy.TabIndex = 41
        Me.lblScheduleBy.Text = "Schdule(Amount/Period)"
        Me.lblScheduleBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign10
        '
        Me.objlblSign10.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign10.ForeColor = System.Drawing.Color.Red
        Me.objlblSign10.Location = New System.Drawing.Point(299, 46)
        Me.objlblSign10.Name = "objlblSign10"
        Me.objlblSign10.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign10.TabIndex = 40
        Me.objlblSign10.Text = "*"
        Me.objlblSign10.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboLoanSchedule
        '
        Me.cboLoanSchedule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanSchedule.DropDownWidth = 190
        Me.cboLoanSchedule.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanSchedule.FormattingEnabled = True
        Me.cboLoanSchedule.Location = New System.Drawing.Point(447, 17)
        Me.cboLoanSchedule.Name = "cboLoanSchedule"
        Me.cboLoanSchedule.Size = New System.Drawing.Size(146, 21)
        Me.cboLoanSchedule.TabIndex = 39
        '
        'lblLoanSchedule
        '
        Me.lblLoanSchedule.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanSchedule.Location = New System.Drawing.Point(315, 19)
        Me.lblLoanSchedule.Name = "lblLoanSchedule"
        Me.lblLoanSchedule.Size = New System.Drawing.Size(125, 17)
        Me.lblLoanSchedule.TabIndex = 38
        Me.lblLoanSchedule.Text = "Loan Schedule"
        Me.lblLoanSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign9
        '
        Me.objlblSign9.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign9.ForeColor = System.Drawing.Color.Red
        Me.objlblSign9.Location = New System.Drawing.Point(299, 19)
        Me.objlblSign9.Name = "objlblSign9"
        Me.objlblSign9.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign9.TabIndex = 37
        Me.objlblSign9.Text = "*"
        Me.objlblSign9.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboLoanDuration
        '
        Me.cboLoanDuration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanDuration.DropDownWidth = 190
        Me.cboLoanDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanDuration.FormattingEnabled = True
        Me.cboLoanDuration.Location = New System.Drawing.Point(143, 287)
        Me.cboLoanDuration.Name = "cboLoanDuration"
        Me.cboLoanDuration.Size = New System.Drawing.Size(146, 21)
        Me.cboLoanDuration.TabIndex = 36
        '
        'lblLoanDuration
        '
        Me.lblLoanDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanDuration.Location = New System.Drawing.Point(27, 289)
        Me.lblLoanDuration.Name = "lblLoanDuration"
        Me.lblLoanDuration.Size = New System.Drawing.Size(110, 17)
        Me.lblLoanDuration.TabIndex = 35
        Me.lblLoanDuration.Text = "Duration (Months)"
        Me.lblLoanDuration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign8
        '
        Me.objlblSign8.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign8.ForeColor = System.Drawing.Color.Red
        Me.objlblSign8.Location = New System.Drawing.Point(11, 289)
        Me.objlblSign8.Name = "objlblSign8"
        Me.objlblSign8.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign8.TabIndex = 34
        Me.objlblSign8.Text = "*"
        Me.objlblSign8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblInterestRate
        '
        Me.lblInterestRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestRate.Location = New System.Drawing.Point(27, 262)
        Me.lblInterestRate.Name = "lblInterestRate"
        Me.lblInterestRate.Size = New System.Drawing.Size(110, 17)
        Me.lblInterestRate.TabIndex = 32
        Me.lblInterestRate.Text = "Interest Rate"
        Me.lblInterestRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInterestRate
        '
        Me.cboInterestRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterestRate.DropDownWidth = 190
        Me.cboInterestRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterestRate.FormattingEnabled = True
        Me.cboInterestRate.Location = New System.Drawing.Point(143, 260)
        Me.cboInterestRate.Name = "cboInterestRate"
        Me.cboInterestRate.Size = New System.Drawing.Size(146, 21)
        Me.cboInterestRate.TabIndex = 33
        '
        'cboLoanAmount
        '
        Me.cboLoanAmount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanAmount.DropDownWidth = 190
        Me.cboLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanAmount.FormattingEnabled = True
        Me.cboLoanAmount.Location = New System.Drawing.Point(143, 233)
        Me.cboLoanAmount.Name = "cboLoanAmount"
        Me.cboLoanAmount.Size = New System.Drawing.Size(146, 21)
        Me.cboLoanAmount.TabIndex = 31
        '
        'lblLoanAmount
        '
        Me.lblLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAmount.Location = New System.Drawing.Point(27, 235)
        Me.lblLoanAmount.Name = "lblLoanAmount"
        Me.lblLoanAmount.Size = New System.Drawing.Size(110, 17)
        Me.lblLoanAmount.TabIndex = 30
        Me.lblLoanAmount.Text = "Loan Amount"
        Me.lblLoanAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign7
        '
        Me.objlblSign7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign7.ForeColor = System.Drawing.Color.Red
        Me.objlblSign7.Location = New System.Drawing.Point(11, 235)
        Me.objlblSign7.Name = "objlblSign7"
        Me.objlblSign7.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign7.TabIndex = 29
        Me.objlblSign7.Text = "*"
        Me.objlblSign7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanScheme.DropDownWidth = 190
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Location = New System.Drawing.Point(143, 206)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(146, 21)
        Me.cboLoanScheme.TabIndex = 28
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(27, 208)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(110, 17)
        Me.lblLoanScheme.TabIndex = 27
        Me.lblLoanScheme.Text = "Loan Scheme"
        Me.lblLoanScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign6
        '
        Me.objlblSign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign6.ForeColor = System.Drawing.Color.Red
        Me.objlblSign6.Location = New System.Drawing.Point(11, 208)
        Me.objlblSign6.Name = "objlblSign6"
        Me.objlblSign6.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign6.TabIndex = 26
        Me.objlblSign6.Text = "*"
        Me.objlblSign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboLoanDeductionPeriod
        '
        Me.cboLoanDeductionPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanDeductionPeriod.DropDownWidth = 190
        Me.cboLoanDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanDeductionPeriod.FormattingEnabled = True
        Me.cboLoanDeductionPeriod.Location = New System.Drawing.Point(143, 179)
        Me.cboLoanDeductionPeriod.Name = "cboLoanDeductionPeriod"
        Me.cboLoanDeductionPeriod.Size = New System.Drawing.Size(146, 21)
        Me.cboLoanDeductionPeriod.TabIndex = 25
        '
        'lblDeductionPeriod
        '
        Me.lblDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeductionPeriod.Location = New System.Drawing.Point(27, 181)
        Me.lblDeductionPeriod.Name = "lblDeductionPeriod"
        Me.lblDeductionPeriod.Size = New System.Drawing.Size(110, 17)
        Me.lblDeductionPeriod.TabIndex = 24
        Me.lblDeductionPeriod.Text = "Deduction Period"
        Me.lblDeductionPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign5
        '
        Me.objlblSign5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign5.ForeColor = System.Drawing.Color.Red
        Me.objlblSign5.Location = New System.Drawing.Point(11, 181)
        Me.objlblSign5.Name = "objlblSign5"
        Me.objlblSign5.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign5.TabIndex = 23
        Me.objlblSign5.Text = "*"
        Me.objlblSign5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboLoanAssignedPeriod
        '
        Me.cboLoanAssignedPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanAssignedPeriod.DropDownWidth = 190
        Me.cboLoanAssignedPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanAssignedPeriod.FormattingEnabled = True
        Me.cboLoanAssignedPeriod.Location = New System.Drawing.Point(143, 152)
        Me.cboLoanAssignedPeriod.Name = "cboLoanAssignedPeriod"
        Me.cboLoanAssignedPeriod.Size = New System.Drawing.Size(146, 21)
        Me.cboLoanAssignedPeriod.TabIndex = 22
        '
        'lblAssignedPeriod
        '
        Me.lblAssignedPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssignedPeriod.Location = New System.Drawing.Point(27, 154)
        Me.lblAssignedPeriod.Name = "lblAssignedPeriod"
        Me.lblAssignedPeriod.Size = New System.Drawing.Size(110, 17)
        Me.lblAssignedPeriod.TabIndex = 21
        Me.lblAssignedPeriod.Text = "Assigned Period"
        Me.lblAssignedPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign4
        '
        Me.objlblSign4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign4.ForeColor = System.Drawing.Color.Red
        Me.objlblSign4.Location = New System.Drawing.Point(11, 154)
        Me.objlblSign4.Name = "objlblSign4"
        Me.objlblSign4.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign4.TabIndex = 20
        Me.objlblSign4.Text = "*"
        Me.objlblSign4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboLoanAssignedDate
        '
        Me.cboLoanAssignedDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanAssignedDate.DropDownWidth = 190
        Me.cboLoanAssignedDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanAssignedDate.FormattingEnabled = True
        Me.cboLoanAssignedDate.Location = New System.Drawing.Point(143, 125)
        Me.cboLoanAssignedDate.Name = "cboLoanAssignedDate"
        Me.cboLoanAssignedDate.Size = New System.Drawing.Size(146, 21)
        Me.cboLoanAssignedDate.TabIndex = 19
        '
        'lblLoanDate
        '
        Me.lblLoanDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanDate.Location = New System.Drawing.Point(27, 127)
        Me.lblLoanDate.Name = "lblLoanDate"
        Me.lblLoanDate.Size = New System.Drawing.Size(110, 17)
        Me.lblLoanDate.TabIndex = 18
        Me.lblLoanDate.Text = "Loan Date"
        Me.lblLoanDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign3
        '
        Me.objlblSign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign3.ForeColor = System.Drawing.Color.Red
        Me.objlblSign3.Location = New System.Drawing.Point(11, 127)
        Me.objlblSign3.Name = "objlblSign3"
        Me.objlblSign3.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign3.TabIndex = 17
        Me.objlblSign3.Text = "*"
        Me.objlblSign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboLoanVocNo
        '
        Me.cboLoanVocNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanVocNo.DropDownWidth = 190
        Me.cboLoanVocNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanVocNo.FormattingEnabled = True
        Me.cboLoanVocNo.Location = New System.Drawing.Point(143, 98)
        Me.cboLoanVocNo.Name = "cboLoanVocNo"
        Me.cboLoanVocNo.Size = New System.Drawing.Size(146, 21)
        Me.cboLoanVocNo.TabIndex = 16
        '
        'lblVocNo
        '
        Me.lblVocNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVocNo.Location = New System.Drawing.Point(27, 100)
        Me.lblVocNo.Name = "lblVocNo"
        Me.lblVocNo.Size = New System.Drawing.Size(110, 17)
        Me.lblVocNo.TabIndex = 15
        Me.lblVocNo.Text = "Loan Voc. No"
        Me.lblVocNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign2
        '
        Me.objlblSign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign2.ForeColor = System.Drawing.Color.Red
        Me.objlblSign2.Location = New System.Drawing.Point(11, 100)
        Me.objlblSign2.Name = "objlblSign2"
        Me.objlblSign2.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign2.TabIndex = 14
        Me.objlblSign2.Text = "*"
        Me.objlblSign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboEmployeeCode
        '
        Me.cboEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeCode.DropDownWidth = 190
        Me.cboEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeCode.FormattingEnabled = True
        Me.cboEmployeeCode.Location = New System.Drawing.Point(143, 17)
        Me.cboEmployeeCode.Name = "cboEmployeeCode"
        Me.cboEmployeeCode.Size = New System.Drawing.Size(146, 21)
        Me.cboEmployeeCode.TabIndex = 9
        '
        'cboLastname
        '
        Me.cboLastname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLastname.DropDownWidth = 190
        Me.cboLastname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLastname.FormattingEnabled = True
        Me.cboLastname.Location = New System.Drawing.Point(143, 71)
        Me.cboLastname.Name = "cboLastname"
        Me.cboLastname.Size = New System.Drawing.Size(146, 21)
        Me.cboLastname.TabIndex = 13
        '
        'lblFirstName
        '
        Me.lblFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstName.Location = New System.Drawing.Point(27, 46)
        Me.lblFirstName.Name = "lblFirstName"
        Me.lblFirstName.Size = New System.Drawing.Size(110, 17)
        Me.lblFirstName.TabIndex = 10
        Me.lblFirstName.Text = "Firstname"
        Me.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFirstname
        '
        Me.cboFirstname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFirstname.DropDownWidth = 190
        Me.cboFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFirstname.FormattingEnabled = True
        Me.cboFirstname.Location = New System.Drawing.Point(143, 44)
        Me.cboFirstname.Name = "cboFirstname"
        Me.cboFirstname.Size = New System.Drawing.Size(146, 21)
        Me.cboFirstname.TabIndex = 11
        '
        'lblEmployeeCode
        '
        Me.lblEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeCode.Location = New System.Drawing.Point(27, 19)
        Me.lblEmployeeCode.Name = "lblEmployeeCode"
        Me.lblEmployeeCode.Size = New System.Drawing.Size(110, 17)
        Me.lblEmployeeCode.TabIndex = 8
        Me.lblEmployeeCode.Text = "Employee Code"
        Me.lblEmployeeCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLastName
        '
        Me.lblLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastName.Location = New System.Drawing.Point(27, 73)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(110, 17)
        Me.lblLastName.TabIndex = 12
        Me.lblLastName.Text = "Surname"
        Me.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblSign1
        '
        Me.objlblSign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblSign1.ForeColor = System.Drawing.Color.Red
        Me.objlblSign1.Location = New System.Drawing.Point(11, 19)
        Me.objlblSign1.Name = "objlblSign1"
        Me.objlblSign1.Size = New System.Drawing.Size(10, 17)
        Me.objlblSign1.TabIndex = 2
        Me.objlblSign1.Text = "*"
        Me.objlblSign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'pnlAdvance
        '
        Me.pnlAdvance.Controls.Add(Me.cboAdvaceBalance)
        Me.pnlAdvance.Controls.Add(Me.lblABalance)
        Me.pnlAdvance.Controls.Add(Me.objlblASign8)
        Me.pnlAdvance.Controls.Add(Me.cboAExEntity)
        Me.pnlAdvance.Controls.Add(Me.lblAExEntity)
        Me.pnlAdvance.Controls.Add(Me.objlblASign7)
        Me.pnlAdvance.Controls.Add(Me.cboAdvanceAmt)
        Me.pnlAdvance.Controls.Add(Me.lblAAmount)
        Me.pnlAdvance.Controls.Add(Me.objlblASign6)
        Me.pnlAdvance.Controls.Add(Me.cboADeductPeriod)
        Me.pnlAdvance.Controls.Add(Me.lblADeductionPeriod)
        Me.pnlAdvance.Controls.Add(Me.objlblASign5)
        Me.pnlAdvance.Controls.Add(Me.cboAAssignPeriod)
        Me.pnlAdvance.Controls.Add(Me.lblAAssignedPeriod)
        Me.pnlAdvance.Controls.Add(Me.objlblASign4)
        Me.pnlAdvance.Controls.Add(Me.cboAAssignDate)
        Me.pnlAdvance.Controls.Add(Me.lblAAssignDate)
        Me.pnlAdvance.Controls.Add(Me.objlblASign3)
        Me.pnlAdvance.Controls.Add(Me.cboAdvanceVocNo)
        Me.pnlAdvance.Controls.Add(Me.lblAVocNo)
        Me.pnlAdvance.Controls.Add(Me.objlblASign2)
        Me.pnlAdvance.Controls.Add(Me.cboAEmployeeCode)
        Me.pnlAdvance.Controls.Add(Me.cboASurname)
        Me.pnlAdvance.Controls.Add(Me.lblAFirstname)
        Me.pnlAdvance.Controls.Add(Me.cboAFirstname)
        Me.pnlAdvance.Controls.Add(Me.lblAEmpCode)
        Me.pnlAdvance.Controls.Add(Me.lblASurname)
        Me.pnlAdvance.Controls.Add(Me.objlblASign1)
        Me.pnlAdvance.Location = New System.Drawing.Point(3, 25)
        Me.pnlAdvance.Name = "pnlAdvance"
        Me.pnlAdvance.Size = New System.Drawing.Size(604, 320)
        Me.pnlAdvance.TabIndex = 325
        '
        'cboAdvaceBalance
        '
        Me.cboAdvaceBalance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAdvaceBalance.DropDownWidth = 190
        Me.cboAdvaceBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAdvaceBalance.FormattingEnabled = True
        Me.cboAdvaceBalance.Location = New System.Drawing.Point(266, 283)
        Me.cboAdvaceBalance.Name = "cboAdvaceBalance"
        Me.cboAdvaceBalance.Size = New System.Drawing.Size(166, 21)
        Me.cboAdvaceBalance.TabIndex = 90
        '
        'lblABalance
        '
        Me.lblABalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblABalance.Location = New System.Drawing.Point(137, 285)
        Me.lblABalance.Name = "lblABalance"
        Me.lblABalance.Size = New System.Drawing.Size(123, 17)
        Me.lblABalance.TabIndex = 89
        Me.lblABalance.Text = "Advance Balance"
        Me.lblABalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign8
        '
        Me.objlblASign8.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign8.ForeColor = System.Drawing.Color.Red
        Me.objlblASign8.Location = New System.Drawing.Point(121, 285)
        Me.objlblASign8.Name = "objlblASign8"
        Me.objlblASign8.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign8.TabIndex = 88
        Me.objlblASign8.Text = "*"
        Me.objlblASign8.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboAExEntity
        '
        Me.cboAExEntity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAExEntity.DropDownWidth = 190
        Me.cboAExEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAExEntity.FormattingEnabled = True
        Me.cboAExEntity.Location = New System.Drawing.Point(266, 256)
        Me.cboAExEntity.Name = "cboAExEntity"
        Me.cboAExEntity.Size = New System.Drawing.Size(166, 21)
        Me.cboAExEntity.TabIndex = 87
        '
        'lblAExEntity
        '
        Me.lblAExEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAExEntity.Location = New System.Drawing.Point(137, 258)
        Me.lblAExEntity.Name = "lblAExEntity"
        Me.lblAExEntity.Size = New System.Drawing.Size(123, 17)
        Me.lblAExEntity.TabIndex = 86
        Me.lblAExEntity.Text = "External Entity"
        Me.lblAExEntity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign7
        '
        Me.objlblASign7.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign7.ForeColor = System.Drawing.Color.Red
        Me.objlblASign7.Location = New System.Drawing.Point(121, 258)
        Me.objlblASign7.Name = "objlblASign7"
        Me.objlblASign7.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign7.TabIndex = 85
        Me.objlblASign7.Text = "*"
        Me.objlblASign7.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboAdvanceAmt
        '
        Me.cboAdvanceAmt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAdvanceAmt.DropDownWidth = 190
        Me.cboAdvanceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAdvanceAmt.FormattingEnabled = True
        Me.cboAdvanceAmt.Location = New System.Drawing.Point(266, 229)
        Me.cboAdvanceAmt.Name = "cboAdvanceAmt"
        Me.cboAdvanceAmt.Size = New System.Drawing.Size(166, 21)
        Me.cboAdvanceAmt.TabIndex = 73
        '
        'lblAAmount
        '
        Me.lblAAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAAmount.Location = New System.Drawing.Point(137, 231)
        Me.lblAAmount.Name = "lblAAmount"
        Me.lblAAmount.Size = New System.Drawing.Size(123, 17)
        Me.lblAAmount.TabIndex = 72
        Me.lblAAmount.Text = "Advance Amount"
        Me.lblAAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign6
        '
        Me.objlblASign6.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign6.ForeColor = System.Drawing.Color.Red
        Me.objlblASign6.Location = New System.Drawing.Point(121, 231)
        Me.objlblASign6.Name = "objlblASign6"
        Me.objlblASign6.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign6.TabIndex = 71
        Me.objlblASign6.Text = "*"
        Me.objlblASign6.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboADeductPeriod
        '
        Me.cboADeductPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboADeductPeriod.DropDownWidth = 190
        Me.cboADeductPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboADeductPeriod.FormattingEnabled = True
        Me.cboADeductPeriod.Location = New System.Drawing.Point(266, 202)
        Me.cboADeductPeriod.Name = "cboADeductPeriod"
        Me.cboADeductPeriod.Size = New System.Drawing.Size(166, 21)
        Me.cboADeductPeriod.TabIndex = 67
        '
        'lblADeductionPeriod
        '
        Me.lblADeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblADeductionPeriod.Location = New System.Drawing.Point(137, 204)
        Me.lblADeductionPeriod.Name = "lblADeductionPeriod"
        Me.lblADeductionPeriod.Size = New System.Drawing.Size(123, 17)
        Me.lblADeductionPeriod.TabIndex = 66
        Me.lblADeductionPeriod.Text = "Deduction Period"
        Me.lblADeductionPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign5
        '
        Me.objlblASign5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign5.ForeColor = System.Drawing.Color.Red
        Me.objlblASign5.Location = New System.Drawing.Point(121, 204)
        Me.objlblASign5.Name = "objlblASign5"
        Me.objlblASign5.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign5.TabIndex = 65
        Me.objlblASign5.Text = "*"
        Me.objlblASign5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboAAssignPeriod
        '
        Me.cboAAssignPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAAssignPeriod.DropDownWidth = 190
        Me.cboAAssignPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAAssignPeriod.FormattingEnabled = True
        Me.cboAAssignPeriod.Location = New System.Drawing.Point(266, 175)
        Me.cboAAssignPeriod.Name = "cboAAssignPeriod"
        Me.cboAAssignPeriod.Size = New System.Drawing.Size(166, 21)
        Me.cboAAssignPeriod.TabIndex = 64
        '
        'lblAAssignedPeriod
        '
        Me.lblAAssignedPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAAssignedPeriod.Location = New System.Drawing.Point(137, 177)
        Me.lblAAssignedPeriod.Name = "lblAAssignedPeriod"
        Me.lblAAssignedPeriod.Size = New System.Drawing.Size(123, 17)
        Me.lblAAssignedPeriod.TabIndex = 63
        Me.lblAAssignedPeriod.Text = "Assigned Period"
        Me.lblAAssignedPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign4
        '
        Me.objlblASign4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign4.ForeColor = System.Drawing.Color.Red
        Me.objlblASign4.Location = New System.Drawing.Point(121, 177)
        Me.objlblASign4.Name = "objlblASign4"
        Me.objlblASign4.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign4.TabIndex = 62
        Me.objlblASign4.Text = "*"
        Me.objlblASign4.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboAAssignDate
        '
        Me.cboAAssignDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAAssignDate.DropDownWidth = 190
        Me.cboAAssignDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAAssignDate.FormattingEnabled = True
        Me.cboAAssignDate.Location = New System.Drawing.Point(266, 148)
        Me.cboAAssignDate.Name = "cboAAssignDate"
        Me.cboAAssignDate.Size = New System.Drawing.Size(166, 21)
        Me.cboAAssignDate.TabIndex = 61
        '
        'lblAAssignDate
        '
        Me.lblAAssignDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAAssignDate.Location = New System.Drawing.Point(137, 150)
        Me.lblAAssignDate.Name = "lblAAssignDate"
        Me.lblAAssignDate.Size = New System.Drawing.Size(123, 17)
        Me.lblAAssignDate.TabIndex = 60
        Me.lblAAssignDate.Text = "Advance  Date"
        Me.lblAAssignDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign3
        '
        Me.objlblASign3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign3.ForeColor = System.Drawing.Color.Red
        Me.objlblASign3.Location = New System.Drawing.Point(121, 150)
        Me.objlblASign3.Name = "objlblASign3"
        Me.objlblASign3.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign3.TabIndex = 59
        Me.objlblASign3.Text = "*"
        Me.objlblASign3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboAdvanceVocNo
        '
        Me.cboAdvanceVocNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAdvanceVocNo.DropDownWidth = 190
        Me.cboAdvanceVocNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAdvanceVocNo.FormattingEnabled = True
        Me.cboAdvanceVocNo.Location = New System.Drawing.Point(266, 121)
        Me.cboAdvanceVocNo.Name = "cboAdvanceVocNo"
        Me.cboAdvanceVocNo.Size = New System.Drawing.Size(166, 21)
        Me.cboAdvanceVocNo.TabIndex = 58
        '
        'lblAVocNo
        '
        Me.lblAVocNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAVocNo.Location = New System.Drawing.Point(137, 123)
        Me.lblAVocNo.Name = "lblAVocNo"
        Me.lblAVocNo.Size = New System.Drawing.Size(123, 17)
        Me.lblAVocNo.TabIndex = 57
        Me.lblAVocNo.Text = "Advance Voc. No"
        Me.lblAVocNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign2
        '
        Me.objlblASign2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign2.ForeColor = System.Drawing.Color.Red
        Me.objlblASign2.Location = New System.Drawing.Point(121, 123)
        Me.objlblASign2.Name = "objlblASign2"
        Me.objlblASign2.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign2.TabIndex = 56
        Me.objlblASign2.Text = "*"
        Me.objlblASign2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'cboAEmployeeCode
        '
        Me.cboAEmployeeCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAEmployeeCode.DropDownWidth = 190
        Me.cboAEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAEmployeeCode.FormattingEnabled = True
        Me.cboAEmployeeCode.Location = New System.Drawing.Point(266, 40)
        Me.cboAEmployeeCode.Name = "cboAEmployeeCode"
        Me.cboAEmployeeCode.Size = New System.Drawing.Size(166, 21)
        Me.cboAEmployeeCode.TabIndex = 51
        '
        'cboASurname
        '
        Me.cboASurname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboASurname.DropDownWidth = 190
        Me.cboASurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboASurname.FormattingEnabled = True
        Me.cboASurname.Location = New System.Drawing.Point(266, 94)
        Me.cboASurname.Name = "cboASurname"
        Me.cboASurname.Size = New System.Drawing.Size(166, 21)
        Me.cboASurname.TabIndex = 55
        '
        'lblAFirstname
        '
        Me.lblAFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAFirstname.Location = New System.Drawing.Point(137, 69)
        Me.lblAFirstname.Name = "lblAFirstname"
        Me.lblAFirstname.Size = New System.Drawing.Size(123, 17)
        Me.lblAFirstname.TabIndex = 52
        Me.lblAFirstname.Text = "Firstname"
        Me.lblAFirstname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAFirstname
        '
        Me.cboAFirstname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAFirstname.DropDownWidth = 190
        Me.cboAFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAFirstname.FormattingEnabled = True
        Me.cboAFirstname.Location = New System.Drawing.Point(266, 67)
        Me.cboAFirstname.Name = "cboAFirstname"
        Me.cboAFirstname.Size = New System.Drawing.Size(166, 21)
        Me.cboAFirstname.TabIndex = 53
        '
        'lblAEmpCode
        '
        Me.lblAEmpCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAEmpCode.Location = New System.Drawing.Point(137, 42)
        Me.lblAEmpCode.Name = "lblAEmpCode"
        Me.lblAEmpCode.Size = New System.Drawing.Size(123, 17)
        Me.lblAEmpCode.TabIndex = 50
        Me.lblAEmpCode.Text = "Employee Code"
        Me.lblAEmpCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblASurname
        '
        Me.lblASurname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblASurname.Location = New System.Drawing.Point(137, 96)
        Me.lblASurname.Name = "lblASurname"
        Me.lblASurname.Size = New System.Drawing.Size(123, 17)
        Me.lblASurname.TabIndex = 54
        Me.lblASurname.Text = "Surname"
        Me.lblASurname.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblASign1
        '
        Me.objlblASign1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblASign1.ForeColor = System.Drawing.Color.Red
        Me.objlblASign1.Location = New System.Drawing.Point(121, 42)
        Me.objlblASign1.Name = "objlblASign1"
        Me.objlblASign1.Size = New System.Drawing.Size(10, 17)
        Me.objlblASign1.TabIndex = 49
        Me.objlblASign1.Text = "*"
        Me.objlblASign1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'lblCaption
        '
        Me.lblCaption.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCaption.BackColor = System.Drawing.Color.Transparent
        Me.lblCaption.ForeColor = System.Drawing.Color.Red
        Me.lblCaption.Location = New System.Drawing.Point(321, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(284, 19)
        Me.lblCaption.TabIndex = 323
        Me.lblCaption.Text = "'*' are Mandatory Fields"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'WizPageApprover
        '
        Me.WizPageApprover.Controls.Add(Me.gbImportInformation)
        Me.WizPageApprover.Location = New System.Drawing.Point(0, 0)
        Me.WizPageApprover.Name = "WizPageApprover"
        Me.WizPageApprover.Size = New System.Drawing.Size(775, 385)
        Me.WizPageApprover.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageApprover.TabIndex = 10
        '
        'gbImportInformation
        '
        Me.gbImportInformation.BorderColor = System.Drawing.Color.Black
        Me.gbImportInformation.Checked = False
        Me.gbImportInformation.CollapseAllExceptThis = False
        Me.gbImportInformation.CollapsedHoverImage = Nothing
        Me.gbImportInformation.CollapsedNormalImage = Nothing
        Me.gbImportInformation.CollapsedPressedImage = Nothing
        Me.gbImportInformation.CollapseOnLoad = False
        Me.gbImportInformation.Controls.Add(Me.lblImportation)
        Me.gbImportInformation.Controls.Add(Me.radAdvance)
        Me.gbImportInformation.Controls.Add(Me.radLoan)
        Me.gbImportInformation.Controls.Add(Me.objbtnSearchLoanApprover)
        Me.gbImportInformation.Controls.Add(Me.cboApprovedBy)
        Me.gbImportInformation.Controls.Add(Me.lblApprover)
        Me.gbImportInformation.Controls.Add(Me.elImportationType)
        Me.gbImportInformation.Controls.Add(Me.elApproverInfo)
        Me.gbImportInformation.ExpandedHoverImage = Nothing
        Me.gbImportInformation.ExpandedNormalImage = Nothing
        Me.gbImportInformation.ExpandedPressedImage = Nothing
        Me.gbImportInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbImportInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbImportInformation.HeaderHeight = 25
        Me.gbImportInformation.HeaderMessage = ""
        Me.gbImportInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbImportInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbImportInformation.HeightOnCollapse = 0
        Me.gbImportInformation.LeftTextSpace = 0
        Me.gbImportInformation.Location = New System.Drawing.Point(165, 0)
        Me.gbImportInformation.Name = "gbImportInformation"
        Me.gbImportInformation.OpenHeight = 300
        Me.gbImportInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbImportInformation.ShowBorder = True
        Me.gbImportInformation.ShowCheckBox = False
        Me.gbImportInformation.ShowCollapseButton = False
        Me.gbImportInformation.ShowDefaultBorderColor = True
        Me.gbImportInformation.ShowDownButton = False
        Me.gbImportInformation.ShowHeader = True
        Me.gbImportInformation.Size = New System.Drawing.Size(609, 384)
        Me.gbImportInformation.TabIndex = 0
        Me.gbImportInformation.Temp = 0
        Me.gbImportInformation.Text = "Import Information"
        Me.gbImportInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblImportation
        '
        Me.lblImportation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImportation.Location = New System.Drawing.Point(26, 164)
        Me.lblImportation.Name = "lblImportation"
        Me.lblImportation.Size = New System.Drawing.Size(158, 15)
        Me.lblImportation.TabIndex = 321
        Me.lblImportation.Text = "Select Importation Type"
        Me.lblImportation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radAdvance
        '
        Me.radAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAdvance.Location = New System.Drawing.Point(315, 163)
        Me.radAdvance.Name = "radAdvance"
        Me.radAdvance.Size = New System.Drawing.Size(118, 17)
        Me.radAdvance.TabIndex = 320
        Me.radAdvance.Text = "Advance"
        Me.radAdvance.UseVisualStyleBackColor = True
        '
        'radLoan
        '
        Me.radLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLoan.Location = New System.Drawing.Point(190, 163)
        Me.radLoan.Name = "radLoan"
        Me.radLoan.Size = New System.Drawing.Size(118, 17)
        Me.radLoan.TabIndex = 320
        Me.radLoan.Text = "Loan"
        Me.radLoan.UseVisualStyleBackColor = True
        '
        'objbtnSearchLoanApprover
        '
        Me.objbtnSearchLoanApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLoanApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLoanApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLoanApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLoanApprover.BorderSelected = False
        Me.objbtnSearchLoanApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLoanApprover.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLoanApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLoanApprover.Location = New System.Drawing.Point(577, 75)
        Me.objbtnSearchLoanApprover.Name = "objbtnSearchLoanApprover"
        Me.objbtnSearchLoanApprover.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLoanApprover.TabIndex = 319
        '
        'cboApprovedBy
        '
        Me.cboApprovedBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprovedBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprovedBy.FormattingEnabled = True
        Me.cboApprovedBy.Location = New System.Drawing.Point(190, 75)
        Me.cboApprovedBy.Name = "cboApprovedBy"
        Me.cboApprovedBy.Size = New System.Drawing.Size(381, 21)
        Me.cboApprovedBy.TabIndex = 318
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(26, 78)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(158, 15)
        Me.lblApprover.TabIndex = 4
        Me.lblApprover.Text = "Select Loan/Advace Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elImportationType
        '
        Me.elImportationType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elImportationType.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elImportationType.Location = New System.Drawing.Point(9, 125)
        Me.elImportationType.Name = "elImportationType"
        Me.elImportationType.Size = New System.Drawing.Size(589, 17)
        Me.elImportationType.TabIndex = 3
        Me.elImportationType.Text = "Importation Type"
        Me.elImportationType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elApproverInfo
        '
        Me.elApproverInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elApproverInfo.Location = New System.Drawing.Point(9, 45)
        Me.elApproverInfo.Name = "elApproverInfo"
        Me.elApproverInfo.Size = New System.Drawing.Size(589, 17)
        Me.elApproverInfo.TabIndex = 2
        Me.elApproverInfo.Text = "Approver Information " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.elApproverInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'WizPageSelectFile
        '
        Me.WizPageSelectFile.Controls.Add(Me.lblMessage)
        Me.WizPageSelectFile.Controls.Add(Me.lblTitle)
        Me.WizPageSelectFile.Controls.Add(Me.btnOpenFile)
        Me.WizPageSelectFile.Controls.Add(Me.txtFilePath)
        Me.WizPageSelectFile.Controls.Add(Me.lblSelectfile)
        Me.WizPageSelectFile.Location = New System.Drawing.Point(0, 0)
        Me.WizPageSelectFile.Name = "WizPageSelectFile"
        Me.WizPageSelectFile.Size = New System.Drawing.Size(775, 385)
        Me.WizPageSelectFile.Style = eZee.Common.eZeeWizardPageStyle.eZeeStyle
        Me.WizPageSelectFile.TabIndex = 7
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.Transparent
        Me.lblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(189, 49)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(426, 34)
        Me.lblMessage.TabIndex = 18
        Me.lblMessage.Text = "This wizard will import Employee 'Loan/Advance' records made from other system."
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(188, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(427, 23)
        Me.lblTitle.TabIndex = 17
        Me.lblTitle.Text = "Employee Loan/Advance Import Wizard"
        '
        'btnOpenFile
        '
        Me.btnOpenFile.BackColor = System.Drawing.Color.White
        Me.btnOpenFile.BackgroundImage = CType(resources.GetObject("btnOpenFile.BackgroundImage"), System.Drawing.Image)
        Me.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnOpenFile.BorderColor = System.Drawing.Color.Empty
        Me.btnOpenFile.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnOpenFile.FlatAppearance.BorderSize = 0
        Me.btnOpenFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOpenFile.ForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOpenFile.GradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Location = New System.Drawing.Point(732, 202)
        Me.btnOpenFile.Name = "btnOpenFile"
        Me.btnOpenFile.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnOpenFile.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnOpenFile.Size = New System.Drawing.Size(23, 20)
        Me.btnOpenFile.TabIndex = 16
        Me.btnOpenFile.Text = "..."
        Me.btnOpenFile.UseVisualStyleBackColor = False
        '
        'txtFilePath
        '
        Me.txtFilePath.BackColor = System.Drawing.Color.White
        Me.txtFilePath.Flags = 0
        Me.txtFilePath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.InvalidChars = New Char(-1) {}
        Me.txtFilePath.Location = New System.Drawing.Point(189, 202)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.ReadOnly = True
        Me.txtFilePath.Size = New System.Drawing.Size(537, 21)
        Me.txtFilePath.TabIndex = 15
        '
        'lblSelectfile
        '
        Me.lblSelectfile.BackColor = System.Drawing.Color.Transparent
        Me.lblSelectfile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectfile.Location = New System.Drawing.Point(189, 173)
        Me.lblSelectfile.Name = "lblSelectfile"
        Me.lblSelectfile.Size = New System.Drawing.Size(228, 20)
        Me.lblSelectfile.TabIndex = 14
        Me.lblSelectfile.Text = "Select File ..."
        Me.lblSelectfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'WizPageImporting
        '
        Me.WizPageImporting.Controls.Add(Me.btnFilter)
        Me.WizPageImporting.Controls.Add(Me.dgData)
        Me.WizPageImporting.Controls.Add(Me.pnlInfo)
        Me.WizPageImporting.Controls.Add(Me.objbuttonBack)
        Me.WizPageImporting.Controls.Add(Me.objbuttonCancel)
        Me.WizPageImporting.Controls.Add(Me.objbuttonNext)
        Me.WizPageImporting.Location = New System.Drawing.Point(0, 0)
        Me.WizPageImporting.Name = "WizPageImporting"
        Me.WizPageImporting.Size = New System.Drawing.Size(775, 385)
        Me.WizPageImporting.Style = eZee.Common.eZeeWizardPageStyle.Custom
        Me.WizPageImporting.TabIndex = 9
        '
        'btnFilter
        '
        Me.btnFilter.BorderColor = System.Drawing.Color.Black
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnFilter.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnFilter.Location = New System.Drawing.Point(12, 349)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.ShowDefaultBorderColor = True
        Me.btnFilter.Size = New System.Drawing.Size(107, 30)
        Me.btnFilter.SplitButtonMenu = Me.cmsFilter
        Me.btnFilter.TabIndex = 23
        Me.btnFilter.Text = "Filter"
        '
        'cmsFilter
        '
        Me.cmsFilter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmShowAll, Me.tsmSuccessful, Me.tsmShowWarning, Me.tsmShowError, Me.tsmExportError})
        Me.cmsFilter.Name = "cmsReport"
        Me.cmsFilter.Size = New System.Drawing.Size(198, 114)
        '
        'tsmShowAll
        '
        Me.tsmShowAll.Name = "tsmShowAll"
        Me.tsmShowAll.Size = New System.Drawing.Size(197, 22)
        Me.tsmShowAll.Text = "Show All Actions"
        '
        'tsmSuccessful
        '
        Me.tsmSuccessful.Name = "tsmSuccessful"
        Me.tsmSuccessful.Size = New System.Drawing.Size(197, 22)
        Me.tsmSuccessful.Text = "Show Successful Action"
        '
        'tsmShowWarning
        '
        Me.tsmShowWarning.Name = "tsmShowWarning"
        Me.tsmShowWarning.Size = New System.Drawing.Size(197, 22)
        Me.tsmShowWarning.Text = "Show Warnings"
        '
        'tsmShowError
        '
        Me.tsmShowError.Name = "tsmShowError"
        Me.tsmShowError.Size = New System.Drawing.Size(197, 22)
        Me.tsmShowError.Text = "Show Error"
        '
        'tsmExportError
        '
        Me.tsmExportError.Name = "tsmExportError"
        Me.tsmExportError.Size = New System.Drawing.Size(197, 22)
        Me.tsmExportError.Text = "Export Error(s)."
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.colhEmployee, Me.colhlogindate, Me.colhStatus, Me.colhMessage, Me.objcolhstatus, Me.objcolhDate})
        Me.dgData.Location = New System.Drawing.Point(12, 69)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(751, 274)
        Me.dgData.TabIndex = 19
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Width = 30
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhlogindate
        '
        Me.colhlogindate.HeaderText = "Date"
        Me.colhlogindate.Name = "colhlogindate"
        Me.colhlogindate.ReadOnly = True
        Me.colhlogindate.Visible = False
        Me.colhlogindate.Width = 80
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        '
        'colhMessage
        '
        Me.colhMessage.HeaderText = "Message"
        Me.colhMessage.Name = "colhMessage"
        Me.colhMessage.ReadOnly = True
        Me.colhMessage.Width = 250
        '
        'objcolhstatus
        '
        Me.objcolhstatus.HeaderText = "objcolhstatus"
        Me.objcolhstatus.Name = "objcolhstatus"
        Me.objcolhstatus.ReadOnly = True
        Me.objcolhstatus.Visible = False
        '
        'objcolhDate
        '
        Me.objcolhDate.HeaderText = "objcolhDate"
        Me.objcolhDate.Name = "objcolhDate"
        Me.objcolhDate.ReadOnly = True
        Me.objcolhDate.Visible = False
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.White
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ezWait)
        Me.pnlInfo.Controls.Add(Me.objError)
        Me.pnlInfo.Controls.Add(Me.objWarning)
        Me.pnlInfo.Controls.Add(Me.objSuccess)
        Me.pnlInfo.Controls.Add(Me.lblWarning)
        Me.pnlInfo.Controls.Add(Me.lblError)
        Me.pnlInfo.Controls.Add(Me.objTotal)
        Me.pnlInfo.Controls.Add(Me.lblSuccess)
        Me.pnlInfo.Controls.Add(Me.lblTotal)
        Me.pnlInfo.Location = New System.Drawing.Point(12, 12)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(751, 51)
        Me.pnlInfo.TabIndex = 2
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(5, 2)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 1
        '
        'objError
        '
        Me.objError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objError.Location = New System.Drawing.Point(629, 29)
        Me.objError.Name = "objError"
        Me.objError.Size = New System.Drawing.Size(39, 13)
        Me.objError.TabIndex = 15
        Me.objError.Text = "0"
        Me.objError.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objWarning
        '
        Me.objWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objWarning.Location = New System.Drawing.Point(515, 29)
        Me.objWarning.Name = "objWarning"
        Me.objWarning.Size = New System.Drawing.Size(39, 13)
        Me.objWarning.TabIndex = 14
        Me.objWarning.Text = "0"
        Me.objWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objSuccess
        '
        Me.objSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objSuccess.Location = New System.Drawing.Point(629, 7)
        Me.objSuccess.Name = "objSuccess"
        Me.objSuccess.Size = New System.Drawing.Size(39, 13)
        Me.objSuccess.TabIndex = 13
        Me.objSuccess.Text = "0"
        Me.objSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWarning
        '
        Me.lblWarning.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWarning.Location = New System.Drawing.Point(561, 29)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(67, 13)
        Me.lblWarning.TabIndex = 12
        Me.lblWarning.Text = "Warning"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblError
        '
        Me.lblError.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblError.Location = New System.Drawing.Point(674, 29)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(67, 13)
        Me.lblError.TabIndex = 11
        Me.lblError.Text = "Error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objTotal
        '
        Me.objTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objTotal.Location = New System.Drawing.Point(515, 7)
        Me.objTotal.Name = "objTotal"
        Me.objTotal.Size = New System.Drawing.Size(39, 13)
        Me.objTotal.TabIndex = 10
        Me.objTotal.Text = "0"
        Me.objTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSuccess
        '
        Me.lblSuccess.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSuccess.Location = New System.Drawing.Point(674, 7)
        Me.lblSuccess.Name = "lblSuccess"
        Me.lblSuccess.Size = New System.Drawing.Size(67, 13)
        Me.lblSuccess.TabIndex = 9
        Me.lblSuccess.Text = "Success"
        Me.lblSuccess.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(561, 7)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(67, 13)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbuttonBack
        '
        Me.objbuttonBack.BackColor = System.Drawing.Color.White
        Me.objbuttonBack.BackgroundImage = CType(resources.GetObject("objbuttonBack.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonBack.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonBack.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonBack.Enabled = False
        Me.objbuttonBack.FlatAppearance.BorderSize = 0
        Me.objbuttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonBack.ForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonBack.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Location = New System.Drawing.Point(246, 265)
        Me.objbuttonBack.Name = "objbuttonBack"
        Me.objbuttonBack.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonBack.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonBack.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonBack.TabIndex = 6
        Me.objbuttonBack.Text = "Back"
        Me.objbuttonBack.UseVisualStyleBackColor = False
        '
        'objbuttonCancel
        '
        Me.objbuttonCancel.BackColor = System.Drawing.Color.White
        Me.objbuttonCancel.BackgroundImage = CType(resources.GetObject("objbuttonCancel.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonCancel.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.objbuttonCancel.FlatAppearance.BorderSize = 0
        Me.objbuttonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonCancel.ForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonCancel.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Location = New System.Drawing.Point(414, 265)
        Me.objbuttonCancel.Name = "objbuttonCancel"
        Me.objbuttonCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonCancel.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonCancel.TabIndex = 5
        Me.objbuttonCancel.Text = "Cancel"
        Me.objbuttonCancel.UseVisualStyleBackColor = False
        '
        'objbuttonNext
        '
        Me.objbuttonNext.BackColor = System.Drawing.Color.White
        Me.objbuttonNext.BackgroundImage = CType(resources.GetObject("objbuttonNext.BackgroundImage"), System.Drawing.Image)
        Me.objbuttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbuttonNext.BorderColor = System.Drawing.Color.Empty
        Me.objbuttonNext.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbuttonNext.FlatAppearance.BorderSize = 0
        Me.objbuttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbuttonNext.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbuttonNext.ForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbuttonNext.GradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Location = New System.Drawing.Point(330, 265)
        Me.objbuttonNext.Name = "objbuttonNext"
        Me.objbuttonNext.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbuttonNext.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbuttonNext.Size = New System.Drawing.Size(77, 29)
        Me.objbuttonNext.TabIndex = 4
        Me.objbuttonNext.Text = "Next"
        Me.objbuttonNext.UseVisualStyleBackColor = False
        '
        'frmImportLoan_AdvanceWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(775, 433)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportLoan_AdvanceWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Import Loan/Advance Wizard"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.eZeeWizImportLoan.ResumeLayout(False)
        Me.WizPageMapping.ResumeLayout(False)
        Me.gbFieldMapping.ResumeLayout(False)
        Me.pnlLoanMapping.ResumeLayout(False)
        Me.pnlAdvance.ResumeLayout(False)
        Me.WizPageApprover.ResumeLayout(False)
        Me.gbImportInformation.ResumeLayout(False)
        Me.WizPageSelectFile.ResumeLayout(False)
        Me.WizPageSelectFile.PerformLayout()
        Me.WizPageImporting.ResumeLayout(False)
        Me.cmsFilter.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents eZeeWizImportLoan As eZee.Common.eZeeWizard
    Friend WithEvents WizPageSelectFile As eZee.Common.eZeeWizardPage
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents btnOpenFile As eZee.Common.eZeeLightButton
    Friend WithEvents txtFilePath As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSelectfile As System.Windows.Forms.Label
    Friend WithEvents WizPageMapping As eZee.Common.eZeeWizardPage
    Friend WithEvents WizPageImporting As eZee.Common.eZeeWizardPage
    Friend WithEvents btnFilter As eZee.Common.eZeeSplitButton
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents objError As System.Windows.Forms.Label
    Friend WithEvents objWarning As System.Windows.Forms.Label
    Friend WithEvents objSuccess As System.Windows.Forms.Label
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents objTotal As System.Windows.Forms.Label
    Friend WithEvents lblSuccess As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents objbuttonBack As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonCancel As eZee.Common.eZeeLightButton
    Friend WithEvents objbuttonNext As eZee.Common.eZeeLightButton
    Friend WithEvents WizPageApprover As eZee.Common.eZeeWizardPage
    Friend WithEvents cmsFilter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmShowAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmSuccessful As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowWarning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmShowError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmExportError As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gbImportInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents elImportationType As eZee.Common.eZeeLine
    Friend WithEvents elApproverInfo As eZee.Common.eZeeLine
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents lblImportation As System.Windows.Forms.Label
    Friend WithEvents radAdvance As System.Windows.Forms.RadioButton
    Friend WithEvents radLoan As System.Windows.Forms.RadioButton
    Friend WithEvents objbtnSearchLoanApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents cboApprovedBy As System.Windows.Forms.ComboBox
    Friend WithEvents gbFieldMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCaption As System.Windows.Forms.Label
    Friend WithEvents pnlLoanMapping As System.Windows.Forms.Panel
    Friend WithEvents objlblSign1 As System.Windows.Forms.Label
    Friend WithEvents cboLoanDeductionPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblDeductionPeriod As System.Windows.Forms.Label
    Friend WithEvents objlblSign5 As System.Windows.Forms.Label
    Friend WithEvents cboLoanAssignedPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblAssignedPeriod As System.Windows.Forms.Label
    Friend WithEvents objlblSign4 As System.Windows.Forms.Label
    Friend WithEvents cboLoanAssignedDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanDate As System.Windows.Forms.Label
    Friend WithEvents objlblSign3 As System.Windows.Forms.Label
    Friend WithEvents cboLoanVocNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblVocNo As System.Windows.Forms.Label
    Friend WithEvents objlblSign2 As System.Windows.Forms.Label
    Friend WithEvents cboEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboLastname As System.Windows.Forms.ComboBox
    Friend WithEvents lblFirstName As System.Windows.Forms.Label
    Friend WithEvents cboFirstname As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeCode As System.Windows.Forms.Label
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents cboLoanAmount As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanAmount As System.Windows.Forms.Label
    Friend WithEvents objlblSign7 As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents objlblSign6 As System.Windows.Forms.Label
    Friend WithEvents cboLoanDuration As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanDuration As System.Windows.Forms.Label
    Friend WithEvents objlblSign8 As System.Windows.Forms.Label
    Friend WithEvents lblInterestRate As System.Windows.Forms.Label
    Friend WithEvents cboInterestRate As System.Windows.Forms.ComboBox
    Friend WithEvents cboLoanSchedule As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanSchedule As System.Windows.Forms.Label
    Friend WithEvents objlblSign9 As System.Windows.Forms.Label
    Friend WithEvents cboSchduleBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblScheduleBy As System.Windows.Forms.Label
    Friend WithEvents objlblSign10 As System.Windows.Forms.Label
    Friend WithEvents cboExternalEntity As System.Windows.Forms.ComboBox
    Friend WithEvents lblExternalEntity As System.Windows.Forms.Label
    Friend WithEvents objlblSign11 As System.Windows.Forms.Label
    Friend WithEvents cboLoanBalance As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanBalance As System.Windows.Forms.Label
    Friend WithEvents objlblSign12 As System.Windows.Forms.Label
    Friend WithEvents pnlAdvance As System.Windows.Forms.Panel
    Friend WithEvents cboAdvaceBalance As System.Windows.Forms.ComboBox
    Friend WithEvents lblABalance As System.Windows.Forms.Label
    Friend WithEvents objlblASign8 As System.Windows.Forms.Label
    Friend WithEvents cboAExEntity As System.Windows.Forms.ComboBox
    Friend WithEvents lblAExEntity As System.Windows.Forms.Label
    Friend WithEvents objlblASign7 As System.Windows.Forms.Label
    Friend WithEvents cboAdvanceAmt As System.Windows.Forms.ComboBox
    Friend WithEvents lblAAmount As System.Windows.Forms.Label
    Friend WithEvents objlblASign6 As System.Windows.Forms.Label
    Friend WithEvents cboADeductPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblADeductionPeriod As System.Windows.Forms.Label
    Friend WithEvents objlblASign5 As System.Windows.Forms.Label
    Friend WithEvents cboAAssignPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblAAssignedPeriod As System.Windows.Forms.Label
    Friend WithEvents objlblASign4 As System.Windows.Forms.Label
    Friend WithEvents cboAAssignDate As System.Windows.Forms.ComboBox
    Friend WithEvents lblAAssignDate As System.Windows.Forms.Label
    Friend WithEvents objlblASign3 As System.Windows.Forms.Label
    Friend WithEvents cboAdvanceVocNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblAVocNo As System.Windows.Forms.Label
    Friend WithEvents objlblASign2 As System.Windows.Forms.Label
    Friend WithEvents cboAEmployeeCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboASurname As System.Windows.Forms.ComboBox
    Friend WithEvents lblAFirstname As System.Windows.Forms.Label
    Friend WithEvents cboAFirstname As System.Windows.Forms.ComboBox
    Friend WithEvents lblAEmpCode As System.Windows.Forms.Label
    Friend WithEvents lblASurname As System.Windows.Forms.Label
    Friend WithEvents objlblASign1 As System.Windows.Forms.Label
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhlogindate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhstatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
