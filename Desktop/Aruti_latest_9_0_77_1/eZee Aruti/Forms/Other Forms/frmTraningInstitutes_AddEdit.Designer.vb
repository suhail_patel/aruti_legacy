﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTraningInstitutes_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTraningInstitutes_AddEdit))
        Me.pnlTraningInstitues = New System.Windows.Forms.Panel
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbTaraningInstitutes = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkLocalProvider = New System.Windows.Forms.CheckBox
        Me.chkSyncwithRecruitment = New System.Windows.Forms.CheckBox
        Me.chkIsFormRequire = New System.Windows.Forms.CheckBox
        Me.cboCity = New System.Windows.Forms.ComboBox
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.lblCountry = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblCity = New System.Windows.Forms.Label
        Me.lblState = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmail = New eZee.TextBox.AlphanumericTextBox
        Me.txtContactPerson = New eZee.TextBox.AlphanumericTextBox
        Me.txtFax = New eZee.TextBox.AlphanumericTextBox
        Me.txtTelNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtAddress = New eZee.TextBox.AlphanumericTextBox
        Me.txtInstituteName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblEmail = New System.Windows.Forms.Label
        Me.lblContactPerson = New System.Windows.Forms.Label
        Me.lblFax = New System.Windows.Forms.Label
        Me.lblTelephone = New System.Windows.Forms.Label
        Me.lblInstiuteAddress = New System.Windows.Forms.Label
        Me.lblInstituteName = New System.Windows.Forms.Label
        Me.lblCodes = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlTraningInstitues.SuspendLayout()
        Me.gbTaraningInstitutes.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlTraningInstitues
        '
        Me.pnlTraningInstitues.Controls.Add(Me.eZeeHeader)
        Me.pnlTraningInstitues.Controls.Add(Me.gbTaraningInstitutes)
        Me.pnlTraningInstitues.Controls.Add(Me.objFooter)
        Me.pnlTraningInstitues.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTraningInstitues.Location = New System.Drawing.Point(0, 0)
        Me.pnlTraningInstitues.Name = "pnlTraningInstitues"
        Me.pnlTraningInstitues.Size = New System.Drawing.Size(681, 352)
        Me.pnlTraningInstitues.TabIndex = 0
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(681, 60)
        Me.eZeeHeader.TabIndex = 3
        Me.eZeeHeader.Title = "Institutes"
        '
        'gbTaraningInstitutes
        '
        Me.gbTaraningInstitutes.BorderColor = System.Drawing.Color.Black
        Me.gbTaraningInstitutes.Checked = False
        Me.gbTaraningInstitutes.CollapseAllExceptThis = False
        Me.gbTaraningInstitutes.CollapsedHoverImage = Nothing
        Me.gbTaraningInstitutes.CollapsedNormalImage = Nothing
        Me.gbTaraningInstitutes.CollapsedPressedImage = Nothing
        Me.gbTaraningInstitutes.CollapseOnLoad = False
        Me.gbTaraningInstitutes.Controls.Add(Me.chkLocalProvider)
        Me.gbTaraningInstitutes.Controls.Add(Me.chkSyncwithRecruitment)
        Me.gbTaraningInstitutes.Controls.Add(Me.chkIsFormRequire)
        Me.gbTaraningInstitutes.Controls.Add(Me.cboCity)
        Me.gbTaraningInstitutes.Controls.Add(Me.cboState)
        Me.gbTaraningInstitutes.Controls.Add(Me.EZeeStraightLine2)
        Me.gbTaraningInstitutes.Controls.Add(Me.lblCountry)
        Me.gbTaraningInstitutes.Controls.Add(Me.cboCountry)
        Me.gbTaraningInstitutes.Controls.Add(Me.lblCity)
        Me.gbTaraningInstitutes.Controls.Add(Me.lblState)
        Me.gbTaraningInstitutes.Controls.Add(Me.txtDescription)
        Me.gbTaraningInstitutes.Controls.Add(Me.txtEmail)
        Me.gbTaraningInstitutes.Controls.Add(Me.txtContactPerson)
        Me.gbTaraningInstitutes.Controls.Add(Me.txtFax)
        Me.gbTaraningInstitutes.Controls.Add(Me.txtTelNo)
        Me.gbTaraningInstitutes.Controls.Add(Me.txtAddress)
        Me.gbTaraningInstitutes.Controls.Add(Me.txtInstituteName)
        Me.gbTaraningInstitutes.Controls.Add(Me.txtCode)
        Me.gbTaraningInstitutes.Controls.Add(Me.lblDescription)
        Me.gbTaraningInstitutes.Controls.Add(Me.lblEmail)
        Me.gbTaraningInstitutes.Controls.Add(Me.lblContactPerson)
        Me.gbTaraningInstitutes.Controls.Add(Me.lblFax)
        Me.gbTaraningInstitutes.Controls.Add(Me.lblTelephone)
        Me.gbTaraningInstitutes.Controls.Add(Me.lblInstiuteAddress)
        Me.gbTaraningInstitutes.Controls.Add(Me.lblInstituteName)
        Me.gbTaraningInstitutes.Controls.Add(Me.lblCodes)
        Me.gbTaraningInstitutes.ExpandedHoverImage = Nothing
        Me.gbTaraningInstitutes.ExpandedNormalImage = Nothing
        Me.gbTaraningInstitutes.ExpandedPressedImage = Nothing
        Me.gbTaraningInstitutes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbTaraningInstitutes.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbTaraningInstitutes.HeaderHeight = 25
        Me.gbTaraningInstitutes.HeaderMessage = ""
        Me.gbTaraningInstitutes.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbTaraningInstitutes.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbTaraningInstitutes.HeightOnCollapse = 0
        Me.gbTaraningInstitutes.LeftTextSpace = 0
        Me.gbTaraningInstitutes.Location = New System.Drawing.Point(9, 66)
        Me.gbTaraningInstitutes.Name = "gbTaraningInstitutes"
        Me.gbTaraningInstitutes.OpenHeight = 217
        Me.gbTaraningInstitutes.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbTaraningInstitutes.ShowBorder = True
        Me.gbTaraningInstitutes.ShowCheckBox = False
        Me.gbTaraningInstitutes.ShowCollapseButton = False
        Me.gbTaraningInstitutes.ShowDefaultBorderColor = True
        Me.gbTaraningInstitutes.ShowDownButton = False
        Me.gbTaraningInstitutes.ShowHeader = True
        Me.gbTaraningInstitutes.Size = New System.Drawing.Size(662, 225)
        Me.gbTaraningInstitutes.TabIndex = 2
        Me.gbTaraningInstitutes.Temp = 0
        Me.gbTaraningInstitutes.Text = "Institutes"
        Me.gbTaraningInstitutes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkLocalProvider
        '
        Me.chkLocalProvider.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLocalProvider.Location = New System.Drawing.Point(418, 63)
        Me.chkLocalProvider.Name = "chkLocalProvider"
        Me.chkLocalProvider.Size = New System.Drawing.Size(232, 17)
        Me.chkLocalProvider.TabIndex = 12
        Me.chkLocalProvider.Text = "Local Provider"
        Me.chkLocalProvider.UseVisualStyleBackColor = True
        Me.chkLocalProvider.Visible = False
        '
        'chkSyncwithRecruitment
        '
        Me.chkSyncwithRecruitment.Checked = True
        Me.chkSyncwithRecruitment.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSyncwithRecruitment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSyncwithRecruitment.Location = New System.Drawing.Point(201, 35)
        Me.chkSyncwithRecruitment.Name = "chkSyncwithRecruitment"
        Me.chkSyncwithRecruitment.Size = New System.Drawing.Size(193, 17)
        Me.chkSyncwithRecruitment.TabIndex = 102
        Me.chkSyncwithRecruitment.Text = "Sync with Recruitment"
        Me.chkSyncwithRecruitment.UseVisualStyleBackColor = True
        Me.chkSyncwithRecruitment.Visible = False
        '
        'chkIsFormRequire
        '
        Me.chkIsFormRequire.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsFormRequire.Location = New System.Drawing.Point(418, 36)
        Me.chkIsFormRequire.Name = "chkIsFormRequire"
        Me.chkIsFormRequire.Size = New System.Drawing.Size(232, 17)
        Me.chkIsFormRequire.TabIndex = 11
        Me.chkIsFormRequire.Text = "Sick Sheet Required"
        Me.chkIsFormRequire.UseVisualStyleBackColor = True
        Me.chkIsFormRequire.Visible = False
        '
        'cboCity
        '
        Me.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCity.FormattingEnabled = True
        Me.cboCity.Location = New System.Drawing.Point(92, 142)
        Me.cboCity.Name = "cboCity"
        Me.cboCity.Size = New System.Drawing.Size(103, 21)
        Me.cboCity.TabIndex = 6
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(291, 115)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(103, 21)
        Me.cboState.TabIndex = 5
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(400, 61)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(12, 156)
        Me.EZeeStraightLine2.TabIndex = 29
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(8, 117)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(78, 16)
        Me.lblCountry.TabIndex = 100
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(92, 115)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(103, 21)
        Me.cboCountry.TabIndex = 4
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(8, 144)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(78, 16)
        Me.lblCity.TabIndex = 98
        Me.lblCity.Text = "City"
        Me.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(201, 117)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(84, 16)
        Me.lblState.TabIndex = 95
        Me.lblState.Text = "State"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(418, 115)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(232, 102)
        Me.txtDescription.TabIndex = 13
        '
        'txtEmail
        '
        Me.txtEmail.Flags = 0
        Me.txtEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmail.Location = New System.Drawing.Point(92, 196)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(302, 21)
        Me.txtEmail.TabIndex = 10
        '
        'txtContactPerson
        '
        Me.txtContactPerson.Flags = 0
        Me.txtContactPerson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContactPerson.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtContactPerson.Location = New System.Drawing.Point(291, 142)
        Me.txtContactPerson.Name = "txtContactPerson"
        Me.txtContactPerson.Size = New System.Drawing.Size(103, 21)
        Me.txtContactPerson.TabIndex = 7
        '
        'txtFax
        '
        Me.txtFax.Flags = 0
        Me.txtFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFax.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFax.Location = New System.Drawing.Point(92, 169)
        Me.txtFax.Name = "txtFax"
        Me.txtFax.Size = New System.Drawing.Size(103, 21)
        Me.txtFax.TabIndex = 8
        '
        'txtTelNo
        '
        Me.txtTelNo.Flags = 0
        Me.txtTelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTelNo.Location = New System.Drawing.Point(291, 169)
        Me.txtTelNo.Name = "txtTelNo"
        Me.txtTelNo.Size = New System.Drawing.Size(103, 21)
        Me.txtTelNo.TabIndex = 9
        '
        'txtAddress
        '
        Me.txtAddress.Flags = 0
        Me.txtAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress.Location = New System.Drawing.Point(92, 88)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(302, 21)
        Me.txtAddress.TabIndex = 3
        '
        'txtInstituteName
        '
        Me.txtInstituteName.Flags = 0
        Me.txtInstituteName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInstituteName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtInstituteName.Location = New System.Drawing.Point(92, 61)
        Me.txtInstituteName.Name = "txtInstituteName"
        Me.txtInstituteName.Size = New System.Drawing.Size(302, 21)
        Me.txtInstituteName.TabIndex = 2
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(92, 34)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(103, 21)
        Me.txtCode.TabIndex = 1
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(418, 88)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(78, 16)
        Me.lblDescription.TabIndex = 8
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(8, 198)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(78, 16)
        Me.lblEmail.TabIndex = 7
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblContactPerson
        '
        Me.lblContactPerson.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContactPerson.Location = New System.Drawing.Point(201, 142)
        Me.lblContactPerson.Name = "lblContactPerson"
        Me.lblContactPerson.Size = New System.Drawing.Size(84, 16)
        Me.lblContactPerson.TabIndex = 6
        Me.lblContactPerson.Text = "Contact Person"
        Me.lblContactPerson.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFax
        '
        Me.lblFax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFax.Location = New System.Drawing.Point(8, 171)
        Me.lblFax.Name = "lblFax"
        Me.lblFax.Size = New System.Drawing.Size(78, 16)
        Me.lblFax.TabIndex = 5
        Me.lblFax.Text = "Fax"
        Me.lblFax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTelephone
        '
        Me.lblTelephone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTelephone.Location = New System.Drawing.Point(201, 169)
        Me.lblTelephone.Name = "lblTelephone"
        Me.lblTelephone.Size = New System.Drawing.Size(84, 16)
        Me.lblTelephone.TabIndex = 4
        Me.lblTelephone.Text = "Tel. No"
        Me.lblTelephone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInstiuteAddress
        '
        Me.lblInstiuteAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstiuteAddress.Location = New System.Drawing.Point(8, 90)
        Me.lblInstiuteAddress.Name = "lblInstiuteAddress"
        Me.lblInstiuteAddress.Size = New System.Drawing.Size(78, 16)
        Me.lblInstiuteAddress.TabIndex = 3
        Me.lblInstiuteAddress.Text = "Address"
        Me.lblInstiuteAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInstituteName
        '
        Me.lblInstituteName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstituteName.Location = New System.Drawing.Point(8, 63)
        Me.lblInstituteName.Name = "lblInstituteName"
        Me.lblInstituteName.Size = New System.Drawing.Size(78, 16)
        Me.lblInstituteName.TabIndex = 2
        Me.lblInstituteName.Text = "Institute Name"
        Me.lblInstituteName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCodes
        '
        Me.lblCodes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCodes.Location = New System.Drawing.Point(8, 36)
        Me.lblCodes.Name = "lblCodes"
        Me.lblCodes.Size = New System.Drawing.Size(78, 16)
        Me.lblCodes.TabIndex = 1
        Me.lblCodes.Text = "Code"
        Me.lblCodes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 297)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(681, 55)
        Me.objFooter.TabIndex = 15
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(469, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 13
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(572, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 14
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmTraningInstitutes_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(681, 352)
        Me.Controls.Add(Me.pnlTraningInstitues)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTraningInstitutes_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Institutes"
        Me.pnlTraningInstitues.ResumeLayout(False)
        Me.gbTaraningInstitutes.ResumeLayout(False)
        Me.gbTaraningInstitutes.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTraningInstitues As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbTaraningInstitutes As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents lblContactPerson As System.Windows.Forms.Label
    Friend WithEvents lblFax As System.Windows.Forms.Label
    Friend WithEvents lblTelephone As System.Windows.Forms.Label
    Friend WithEvents lblInstiuteAddress As System.Windows.Forms.Label
    Friend WithEvents lblInstituteName As System.Windows.Forms.Label
    Friend WithEvents lblCodes As System.Windows.Forms.Label
    Friend WithEvents txtInstituteName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtContactPerson As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFax As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtTelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAddress As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents cboCity As System.Windows.Forms.ComboBox
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents chkIsFormRequire As System.Windows.Forms.CheckBox
    Friend WithEvents chkSyncwithRecruitment As System.Windows.Forms.CheckBox
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents chkLocalProvider As System.Windows.Forms.CheckBox
End Class
