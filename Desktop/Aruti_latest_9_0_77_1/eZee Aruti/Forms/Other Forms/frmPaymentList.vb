﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.clsPayment_tran

Public Class frmPaymentList

#Region " Private Varaibles "
    Private objPaymentTran As New clsPayment_tran
    Private ReadOnly mstrModuleName As String = "frmPaymentList"
    Private mintTransactonId As Integer = -1
    Private mintReferenceId As Integer = -1
    Private mintPaymentTypeId As Integer = -1

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mdtPeriod_Startdate As Date
    Private mdtPeriod_Enddate As Date
    'Sohail (21 Aug 2015) -- End

    'Sohail (12 Dec 2015) -- Start
    'Enhancement - Provide Deposit feaure in Employee Saving.
    Private mdtLoanSavingEffectiveDate As Date
    'Sohail (12 Dec 2015) -- End
#End Region

#Region " Properties "
    Public WriteOnly Property _Transaction_Id() As Integer
        Set(ByVal value As Integer)
            mintTransactonId = value
        End Set
    End Property

    Public WriteOnly Property _Reference_Id() As Integer
        Set(ByVal value As Integer)
            mintReferenceId = value
        End Set
    End Property

    Public WriteOnly Property _PaymentType_Id() As Integer
        Set(ByVal value As Integer)
            mintPaymentTypeId = value
        End Set
    End Property

    'Sohail (12 Dec 2015) -- Start
    'Enhancement - Provide Deposit feaure in Employee Saving.
    Public WriteOnly Property _LoanSavingEffectiveDate() As Date
        Set(ByVal value As Date)
            mdtLoanSavingEffectiveDate = value
        End Set
    End Property
    'Sohail (12 Dec 2015) -- End
#End Region

#Region " Private Function "

    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorOptional
            cboPayYear.BackColor = GUI.ColorOptional
            cboPayPeriod.BackColor = GUI.ColorOptional
            txtVoucherno.BackColor = GUI.ColorOptional
            txtPaidAmount.BackColor = GUI.ColorOptional
            txtPaidAmountTo.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- End

    Private Sub FillList()
        Dim dsPayment As New DataSet
        Dim dtTable As New DataTable
        Dim StrSearching As String = String.Empty
        Dim lvItem As ListViewItem
        Dim objExrate As clsExchangeRate 'Sohail (08 Oct 2011)

        Try

            If User._Object.Privilege._AllowToViewPaymentList = True Then                'Pinkal (02-Jul-2012) -- Start

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsPayment = objPaymentTran.GetList("Payment", mintReferenceId, mintTransactonId, mintPaymentTypeId)
                dsPayment = objPaymentTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, mdtPeriod_Startdate, mdtPeriod_Enddate, _
                                                   ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                   "Payment", mintReferenceId, mintTransactonId, mintPaymentTypeId, True)
                'Sohail (21 Aug 2015) -- End

            lvPayment.Items.Clear()

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " "
            End If

            If CInt(cboPayYear.SelectedValue) > 0 Then
                StrSearching &= "AND yearunkid = " & CInt(cboPayYear.SelectedValue) & " "
            End If

            If txtPaidAmount.Decimal <> 0 AndAlso txtPaidAmountTo.Decimal <> 0 Then 'Sohail (11 May 2011)
                StrSearching &= "AND Amount >= " & txtPaidAmount.Decimal & " AND Amount <= " & txtPaidAmountTo.Decimal & " " 'Sohail (11 May 2011)
            End If

            If txtVoucherno.Text.Trim <> "" Then
                StrSearching &= "AND Voc LIKE '%" & CStr(txtVoucherno.Text) & "%'" & " "
            End If

            If dtpPaymentDate.Checked = True Then
                StrSearching &= "AND Date = '" & eZeeDate.convertDate(dtpPaymentDate.Value) & "'" & "  "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsPayment.Tables("Payment"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsPayment.Tables("Payment")
            End If

            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem

                lvItem.Text = eZeeDate.convertDate(dtRow.Item("Date").ToString).ToShortDateString
                lvItem.SubItems.Add(dtRow.Item("Voc").ToString)
                'Sohail (28 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                lvItem.SubItems.Add(dtRow.Item("employeecode").ToString)
                'Sohail (28 Jan 2012) -- End
                lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)
                lvItem.SubItems.Add(dtRow.Item("YearName").ToString)
                lvItem.SubItems.Add(dtRow.Item("PeriodName").ToString)
                    lvItem.SubItems(colhPayPeriod.Index).Tag = CInt(dtRow.Item("periodunkid"))
                'Sohail (04 Mar 2011) -- Start
                'lvItem.SubItems.Add(dtRow.Item("Amount").ToString)
                'Sohail (08 Oct 2011) -- Start
                'lvItem.SubItems.Add(Format(CDec(dtRow.Item("Amount").ToString), GUI.fmtCurrency)) 'Sohail (11 May 2011)
                lvItem.SubItems.Add(Format(CDec(dtRow.Item("expaidamt").ToString), GUI.fmtCurrency))
                objExrate = New clsExchangeRate
                objExrate._ExchangeRateunkid = CInt(dtRow.Item("paidcurrencyid"))
                lvItem.SubItems.Add(objExrate._Currency_Sign)
                'Sohail (08 Oct 2011) -- End
                'Sohail (04 Mar 2011) -- End

                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    lvItem.SubItems.Add(dtRow.Item("paymentdate_periodunkid").ToString)
                    lvItem.SubItems(colhEmployee.Index).Tag = CInt(dtRow.Item("employeeunkid"))
                    'Sohail (07 May 2015) -- End


                lvItem.Tag = dtRow.Item("paymenttranunkid")

                lvPayment.Items.Add(lvItem)

                lvItem = Nothing
            Next

            If lvPayment.Items.Count > 14 Then
                colhEmployee.Width = 230 - 18
            Else
                colhEmployee.Width = 230
            End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombos As New DataSet
        'Sandeep [ 21 Aug 2010 ] -- Start
        Dim objMasterData As New clsMasterData
        'Sandeep [ 21 Aug 2010 ] -- End 
        Try
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmployee.GetEmployeeList("Emp", True, True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True)
            'End If

            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            

            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                .SelectedValue = 0
            End With

            'Sandeep [ 21 Aug 2010 ] -- Start
            'dsCombos = objMasterData.getListForYearCombo("Yeat", True)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objMasterData.getComboListPAYYEAR("Yeat", True)
            dsCombos = objMasterData.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Yeat", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sandeep [ 21 Aug 2010 ] -- End
            With cboPayYear
                'Sandeep [ 21 Aug 2010 ] -- Start
                '.ValueMember = "yearunkid"
                .ValueMember = "Id"
                'Sandeep [ 21 Aug 2010 ] -- End
                .DisplayMember = "name"

                'Sandeep [ 09 Oct 2010 ] -- Start
                'Issues Reported by Vimal
                '.DataSource = dsCombos.Tables("Year")
                .DataSource = dsCombos.Tables("Yeat")
                'Sandeep [ 09 Oct 2010 ] -- End 
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            'btnGlobalVoidPayment.Visible = False 'Sohail (12 Oct 2011)
            If mintReferenceId = enPaymentRefId.LOAN Or mintReferenceId = enPaymentRefId.ADVANCE Then
                If mintPaymentTypeId = enPayTypeId.PAYMENT Then
                    btnNew.Enabled = User._Object.Privilege._AddLoanAdvancePayment
                    'Sohail (25 Mar 2015) -- Start
                    'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                    'btnEdit.Enabled = User._Object.Privilege._EditLoanAdvancePayment
                    'Sohail (25 Mar 2015) -- End
                    btnDelete.Enabled = User._Object.Privilege._DeleteLoanAdvancePayment
                ElseIf mintPaymentTypeId = enPayTypeId.RECEIVED Then
                    btnNew.Enabled = User._Object.Privilege._AddLoanAdvanceReceived
                    'Sohail (25 Mar 2015) -- Start
                    'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                    'btnEdit.Enabled = User._Object.Privilege._EditLoanAdvanceReceived
                    'Sohail (25 Mar 2015) -- End
                    btnDelete.Enabled = User._Object.Privilege._DeleteLoanAdvanceReceived
                    Me.Text = Language.getMessage(mstrModuleName, 10, "List of Receipts")
                    EZeeHeader.Title = Language.getMessage(mstrModuleName, 11, "Receipt List")
                End If
            ElseIf mintReferenceId = enPaymentRefId.SAVINGS Then
                'Sohail (21 Nov 2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                'btnNew.Enabled = User._Object.Privilege._AddSavingsPayment
                'btnDelete.Enabled = User._Object.Privilege._DeleteSavingsPayment
                If mintPaymentTypeId = enPayTypeId.DEPOSIT Then
                    btnNew.Enabled = User._Object.Privilege._AddSavingsDeposit
                    btnDelete.Enabled = User._Object.Privilege._DeleteSavingsDeposit
                Else
                btnNew.Enabled = User._Object.Privilege._AddSavingsPayment
                    btnDelete.Enabled = User._Object.Privilege._DeleteSavingsPayment
                End If
                'Sohail (21 Nov 2015) -- End
                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                'btnEdit.Enabled = User._Object.Privilege._EditSavingsPayment
                'Sohail (25 Mar 2015) -- End
            Else
                btnNew.Enabled = User._Object.Privilege._AddPayment
                'Sohail (04 Mar 2011) -- Start
                'Issue : Editing payment was closed before and part payment also. on privilege it was opened so closed now 
                'btnEdit.Enabled = User._Object.Privilege._EditPayment 'Sohail (04 Mar 2011)
                 btnDelete.Enabled = User._Object.Privilege._DeletePayment
                 'Sohail (04 Mar 2011) -- End                  
                'Sohail (12 Oct 2011) -- Start
                'btnGlobalVoidPayment.Visible = True
                'btnGlobalVoidPayment.Enabled = User._Object.Privilege._DeletePayment
                'Sohail (12 Oct 2011) -- End
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmPaymentList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPaymentTran = New clsPayment_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            Call OtherSettings()

            Call SetColor() 'Sohail (10 Jul 2014)

            Call FillCombo()
            Call FillList()

            'Sohail (25 Mar 2015) -- Start
            'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
            'If mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP Then btnEdit.Enabled = False
            btnEdit.Enabled = False
            'Sohail (25 Mar 2015) -- End
            If lvPayment.Items.Count > 0 Then lvPayment.Items(0).Selected = True
            lvPayment.Select()
            Call SetVisibility()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanAdvanceList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaymentList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvPayment.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaymentList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPaymentList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objPaymentTran = Nothing
    End Sub

    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayment_authorize_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsPayment_authorize_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- End
#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Sohail (17 Oct 2012) -- Start
        'TRA - ENHANCEMENT
        Dim dsList As DataSet
        Dim strFilter As String = ""
        'Sohail (17 Oct 2012) -- End
        Dim dtActAdj As New DataTable 'Sohail (23 May 2017) 
        Try
            If lvPayment.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Payment from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvPayment.Select()
                Exit Sub
            End If

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvPayment.SelectedItems(0).Index

            'If lvPayment.isUsed(CInt(CInt(lvPayment.SelectedItems(0).Tag)) Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Payment. Reason: This Loan is in use."), enMsgBoxStyle.Information) '?2
            '    lvPayment.Select()
            '    Exit Sub
            'End If

            'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Payment?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then 'Sohail (04 Mar 2011)

                objPaymentTran._Paymenttranunkid = CInt(lvPayment.SelectedItems(0).Tag)

            'Sohail (04 Mar 2011) -- Start
            If objPaymentTran._Paymentdate.Date < FinancialYear._Object._Database_Start_Date.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Payment. Reason: This Payment is brought forwarded."), enMsgBoxStyle.Information) '?1
                lvPayment.Select()
                Exit Sub
            End If
            'Sohail (04 Mar 2011) -- End

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            Dim objTranPeriod As New clscommom_period_Tran
            objTranPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvPayment.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag)
            'Sohail (23 May 2017) -- End

            'Sohail (17 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            If objPaymentTran._Paymentrefid = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                strFilter = " prpayment_tran.periodunkid = " & CInt(lvPayment.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag) & " AND isauthorized = 1 AND prpayment_tran.paymenttranunkid = " & CInt(lvPayment.SelectedItems(0).Tag) & " "
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPaymentTran.GetList("Authorized", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, strFilter)
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'dsList = objPaymentTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                '                                Company._Object._Companyunkid, mdtPeriod_Startdate, mdtPeriod_Enddate, _
                '                                ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                '                                "Authorized", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, _
                '                                False, strFilter)
                dsList = objPaymentTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, objTranPeriod._Start_Date, objTranPeriod._End_Date, _
                                                ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                "Authorized", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, _
                                                False, strFilter)
                'Sohail (23 May 2017) -- End
                'Sohail (21 Aug 2015) -- End
                If dsList.Tables("Authorized").Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry! This payments is already Authorized. Please Void Authorized payment for selected employee."), enMsgBoxStyle.Information) '?1
                    lvPayment.Select()
                    Exit Try
                End If
            End If
            'Sohail (17 Oct 2012) -- End

            'Sohail (13 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            If ConfigParameter._Object._SetPayslipPaymentApproval = True Then
                If objPaymentTran._Isapproved = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete this Payment. Reason: This Payment is Final Approved."), enMsgBoxStyle.Information) '?1
                    lvPayment.Select()
                    Exit Sub
                End If

                Dim objPaymentApproval As New clsPayment_approval_tran
                dsList = objPaymentApproval.GetList("Approved", True, lvPayment.SelectedItems(0).Tag.ToString, , , "levelunkid <> -1 AND priority <> -1 ")
                If dsList.Tables("Approved").Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot delete this Payment. Reason: This Payment is Approved by some Approvers."), enMsgBoxStyle.Information) '?1
                    lvPayment.Select()
                    Exit Sub
                End If
                objPaymentApproval = Nothing
            End If
            'Sohail (13 Feb 2013) -- End

                'Sandeep [ 17 DEC 2010 ] -- Start
                'Select Case mintPaymentTypeId
                '    Case clsPayment_tran.enPayTypeId.PAYMENT
                '        Dim ObjLoan_Advance As New clsLoan_Advance
                '        ObjLoan_Advance._Loanadvancetranunkid = objPaymentTran._Payreftranunkid
                '        If ObjLoan_Advance._Isloan = True Then
                '            If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Net_Amount Then
                '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), enMsgBoxStyle.Information)
                '                Exit Sub
                '            End If
                '        Else
                '            If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Advance_Amount Then
                '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), enMsgBoxStyle.Information)
                '                Exit Sub
                '            End If
                '        End If
                '    Case clsPayment_tran.enPayTypeId.REPAYMENT
                '        Dim objEmployeeSaving As New clsSaving_Tran
                '        objEmployeeSaving._Savingtranunkid = objPaymentTran._Payreftranunkid
                '        If objEmployeeSaving._Savingstatus = clsPayment_tran.enPayTypeId.REPAYMENT Then
                '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot delete this Payment. Reason : This Saving Scheme is in complete mode."), enMsgBoxStyle.Information)
                '            Exit Sub
                '        End If
                'End Select

                Select Case objPaymentTran._Paymentrefid
                    Case clsPayment_tran.enPaymentRefId.LOAN
                        Select Case objPaymentTran._PaymentTypeId
                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        'Case clsPayment_tran.enPayTypeId.PAYMENT, clsPayment_tran.enPayTypeId.RECEIVED
                        '    Dim ObjLoan_Advance As New clsLoan_Advance
                        '    ObjLoan_Advance._Loanadvancetranunkid = objPaymentTran._Payreftranunkid
                        '    If ObjLoan_Advance._Isloan = True Then
                        '        If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Net_Amount Then
                        '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot delete this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), enMsgBoxStyle.Information)
                        '            Exit Sub
                        '        End If
                        '    End If
                        Case clsPayment_tran.enPayTypeId.PAYMENT
                        Dim ObjLoan_Advance As New clsLoan_Advance
                        ObjLoan_Advance._Loanadvancetranunkid = objPaymentTran._Payreftranunkid
                        If ObjLoan_Advance._Isloan = True Then
                                If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Loan_Amount Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot edit this Loan/Advance payment. Reason : Deduction or Repayment is already done."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                                End If

                        Case clsPayment_tran.enPayTypeId.RECEIVED
                            Dim objPeriod As New clscommom_period_Tran
                            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvPayment.SelectedItems(0).SubItems(colhPaymentDatePeriod.Index).Text)
                            If objPeriod._Statusid = enStatusType.Close Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot delete this Loan/Advance payment. Reason : Payment date period is already closed."), enMsgBoxStyle.Information)
                                Exit Sub
                            Else
                                Dim objTnA As New clsTnALeaveTran
                                If objTnA.IsPayrollProcessDone(CInt(lvPayment.SelectedItems(0).SubItems(colhPaymentDatePeriod.Index).Text), lvPayment.SelectedItems(0).SubItems(colhEmployee.Index).Tag.ToString, objPeriod._End_Date, enModuleReference.Payroll) = True Then
                                    'Sohail (19 Apr 2019) -- Start
                                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot delete this Loan/Advance payment. Reason : Process Payroll is already done for the last date of selected period for the selected employee."), enMsgBoxStyle.Information)
                                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot delete this Loan/Advance payment. Reason : Process Payroll is already done for the last date of selected period for the selected employee.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 20, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                        Dim objFrm As New frmProcessPayroll
                                        objFrm.ShowDialog()
                                    End If
                                    'Sohail (19 Apr 2019) -- End
                                    Exit Sub
                                End If
                            End If
                            'Sohail (07 May 2015) -- End
                        End Select
                    Case clsPayment_tran.enPaymentRefId.ADVANCE
                        Select Case objPaymentTran._PaymentTypeId
                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        'Case clsPayment_tran.enPayTypeId.PAYMENT, clsPayment_tran.enPayTypeId.RECEIVED
                        '    Dim ObjLoan_Advance As New clsLoan_Advance
                        '    ObjLoan_Advance._Loanadvancetranunkid = objPaymentTran._Payreftranunkid
                        '    If ObjLoan_Advance._Isloan = False Then
                        '        If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Advance_Amount Then
                        '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot delete this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), enMsgBoxStyle.Information)
                        '            Exit Sub
                        '        End If
                        '    End If
                        Case clsPayment_tran.enPayTypeId.PAYMENT
                                Dim ObjLoan_Advance As New clsLoan_Advance
                                ObjLoan_Advance._Loanadvancetranunkid = objPaymentTran._Payreftranunkid
                                If ObjLoan_Advance._Isloan = False Then
                            If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Advance_Amount Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot edit this Loan/Advance payment. Reason : Deduction or Repayment is already done."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If

                        Case clsPayment_tran.enPayTypeId.RECEIVED
                            Dim objPeriod As New clscommom_period_Tran
                            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvPayment.SelectedItems(0).SubItems(colhPaymentDatePeriod.Index).Text)
                            If objPeriod._Statusid = enStatusType.Close Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot delete this Loan/Advance payment. Reason : Payment date period is already closed."), enMsgBoxStyle.Information)
                                Exit Sub
                            Else
                                Dim objTnA As New clsTnALeaveTran
                                If objTnA.IsPayrollProcessDone(CInt(lvPayment.SelectedItems(0).SubItems(colhPaymentDatePeriod.Index).Text), lvPayment.SelectedItems(0).SubItems(colhEmployee.Index).Tag.ToString, objPeriod._End_Date, enModuleReference.Payroll) = True Then
                                    'Sohail (19 Apr 2019) -- Start
                                    'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot delete this Loan/Advance payment. Reason : Process Payroll is already done for the last date of selected period for the selected employee."), enMsgBoxStyle.Information)
                                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot delete this Loan/Advance payment. Reason : Process Payroll is already done for the last date of selected period for the selected employee.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 20, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                                        Dim objFrm As New frmProcessPayroll
                                        objFrm.ShowDialog()
                                    End If
                                    'Sohail (19 Apr 2019) -- End
                                    Exit Sub
                                End If
                                'Sohail (27 Feb 2018) -- Start
                                'Internal Issue : Dont allow to delete loan / advance one it is completed in 70.1.
                                Dim objLoan As New clsLoan_Advance
                                Dim ds As DataSet = objLoan.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, True, objPeriod._Start_Date, objPeriod._End_Date, objPeriod._End_Date, "", "List", "LN.loanadvancetranunkid = " & objPaymentTran._Payreftranunkid & " ", 0, False, "")
                                If ds.Tables(0).Rows.Count > 0 Then
                                    If CInt(ds.Tables(0).Rows(0).Item("loan_statusunkid")) = CInt(enLoanStatus.WRITTEN_OFF) OrElse CInt(ds.Tables(0).Rows(0).Item("loan_statusunkid")) = CInt(enLoanStatus.COMPLETED) Then
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, You cannot delete this Loan/Advance payment. Reason : Loan/Advance is already completed."), enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                End If
                                'Sohail (27 Feb 2018) -- End
                            End If
                            'Sohail (07 May 2015) -- End
                        End Select
                    Case clsPayment_tran.enPaymentRefId.PAYSLIP

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    Dim objBudget As New clsBudget_MasterNew
                    Dim intBudgetId As Integer = 0
                    dsList = objBudget.GetComboList("List", False, True, )
                    If dsList.Tables(0).Rows.Count > 0 Then
                        intBudgetId = CInt(dsList.Tables(0).Rows(0).Item("budgetunkid"))
                    End If
                    If intBudgetId > 0 Then
                        Dim mstrAnalysis_Fields As String = ""
                        Dim mstrAnalysis_Join As String = ""
                        Dim mstrAnalysis_OrderBy As String = "hremployee_master.employeeunkid"
                        Dim mstrReport_GroupName As String = ""
                        Dim mstrAnalysis_TableName As String = ""
                        Dim mstrAnalysis_CodeField As String = ""
                        objBudget._Budgetunkid = intBudgetId
                        Dim strEmpList As String = lvPayment.SelectedItems(0).SubItems(colhEmployee.Index).Tag.ToString()
                        Dim objBudgetCodes As New clsBudgetcodes_master
                        Dim dsEmp As DataSet = Nothing
                        If objBudget._Viewbyid = enBudgetViewBy.Allocation Then
                            Dim objEmp As New clsEmployee_Master
                            Dim objBudget_Tran As New clsBudget_TranNew
                            Dim mstrAllocationTranUnkIDs As String = objBudget_Tran.GetAllocationTranUnkIDs(intBudgetId)

                            frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", objBudget._Allocationbyid, mstrAllocationTranUnkIDs, objBudget._Budget_date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)

                            dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(lvPayment.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), intBudgetId, 0, 0, mstrAllocationTranUnkIDs, 1)
                            dsEmp = objEmp.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objTranPeriod._Start_Date, objTranPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", , , , "hremployee_master.employeeunkid IN (" & strEmpList & ")")
                        Else
                            dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(lvPayment.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag), intBudgetId, 0, 0, strEmpList, 1)
                        End If

                        If dsList.Tables(0).Rows.Count > 0 Then
                            dtActAdj.Columns.Add("fundactivityunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                            dtActAdj.Columns.Add("transactiondate", System.Type.GetType("System.String")).DefaultValue = ""
                            dtActAdj.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
                            dtActAdj.Columns.Add("isexeed", System.Type.GetType("System.Boolean")).DefaultValue = False
                            dtActAdj.Columns.Add("Activity Code", System.Type.GetType("System.String")).DefaultValue = ""
                            dtActAdj.Columns.Add("Current Balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
                            dtActAdj.Columns.Add("Actual Salary", System.Type.GetType("System.Decimal")).DefaultValue = 0

                            Dim mdicActivity As New Dictionary(Of Integer, Decimal)
                            Dim mdicActivityCode As New Dictionary(Of Integer, String)
                            Dim objActivity As New clsfundactivity_Tran
                            Dim dsAct As DataSet = objActivity.GetList("Activity")
                            Dim objActAdjust As New clsFundActivityAdjustment_Tran
                            Dim dsActAdj As DataSet = objActAdjust.GetLastCurrentBalance("List", 0, objTranPeriod._End_Date)
                            mdicActivity = (From p In dsActAdj.Tables("List") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Total = CDec(p.Item("newbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                            mdicActivityCode = (From p In dsAct.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activity_code").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                            Dim objPayment As New clsPayment_tran
                            Dim dsBalance As DataSet = objPayment.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objTranPeriod._Start_Date, objTranPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, True, " prpayment_tran.paymenttranunkid = " & lvPayment.SelectedItems(0).Tag.ToString() & " ", Nothing)
                            If objBudget._Viewbyid = enBudgetViewBy.Allocation AndAlso dsEmp IsNot Nothing Then
                                dsBalance.Tables(0).PrimaryKey = New DataColumn() {dsBalance.Tables(0).Columns("employeeunkid")}
                                dsBalance.Tables(0).Merge(dsEmp.Tables(0))
                            End If
                            Dim intActivityId As Integer
                            Dim decTotal As Decimal = 0
                            Dim decActBal As Decimal = 0
                            Dim blnShowValidationList As Boolean = False
                            Dim blnIsExeed As Boolean = False
                            For Each pair In mdicActivityCode
                                intActivityId = pair.Key
                                decTotal = 0
                                decActBal = 0
                                blnIsExeed = False

                                If mdicActivity.ContainsKey(intActivityId) = True Then
                                    decActBal = mdicActivity.Item(intActivityId)
                                End If

                                Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundactivityunkid")) = intActivityId AndAlso CDec(p.Item("percentage")) > 0) Select (p)).ToList
                                If lstRow.Count > 0 Then
                                    For Each dRow As DataRow In lstRow
                                        Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                                        Dim decNetPay As Decimal = (From p In dsBalance.Tables(0) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso IsDBNull(p.Item("Amount")) = False) Select (CDec(p.Item("Amount")))).Sum
                                        decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                                    Next

                                    'If decTotal > decActBal Then
                                    '    blnShowValidationList = True
                                    '    blnIsExeed = True
                                    'End If
                                    Dim dr As DataRow = dtActAdj.NewRow
                                    dr.Item("fundactivityunkid") = intActivityId
                                    dr.Item("transactiondate") = eZeeDate.convertDate(objTranPeriod._End_Date).ToString()
                                    dr.Item("remark") = Language.getMessage(mstrModuleName, 9, "Salary payments Voided for the period of") & " " & cboPayPeriod.Text
                                    dr.Item("isexeed") = blnIsExeed
                                    dr.Item("Activity Code") = pair.Value
                                    dr.Item("Current Balance") = Format(decActBal, GUI.fmtCurrency)
                                    dr.Item("Actual Salary") = Format(decTotal, GUI.fmtCurrency)
                                    dtActAdj.Rows.Add(dr)
                                End If
                            Next

                            If blnShowValidationList = True Then
                                Dim dr() As DataRow = dtActAdj.Select("isexeed = 0 ")
                                For Each r In dr
                                    dtActAdj.Rows.Remove(r)
                                Next
                                dtActAdj.AcceptChanges()
                                dtActAdj.Columns.Remove("fundactivityunkid")
                                dtActAdj.Columns.Remove("transactiondate")
                                dtActAdj.Columns.Remove("remark")
                                dtActAdj.Columns.Remove("isexeed")
                                Dim objValid As New frmCommonValidationList
                                objValid.displayDialog(False, Language.getMessage(mstrModuleName, 17, "Sorry, Actual Salary Payment will be exceeding to the Activity Current Balance."), dtActAdj)
                                Exit Try
                            End If
                        End If
                    End If
                    'Sohail (23 May 2017) -- End

                    Case clsPayment_tran.enPaymentRefId.SAVINGS
                        Select Case objPaymentTran._PaymentTypeId
                    Case clsPayment_tran.enPayTypeId.REPAYMENT
                            'Sohail (04 Mar 2011) -- Start
                            'Dim objEmployeeSaving As New clsSaving_Tran
                            'objEmployeeSaving._Savingtranunkid = objPaymentTran._Payreftranunkid
                            'If objEmployeeSaving._Savingstatus = clsPayment_tran.enPayTypeId.REPAYMENT Then
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot delete this Payment. Reason : This Saving Scheme is in complete mode."), enMsgBoxStyle.Information)
                            'End If
                            'Exit Sub
                            'Sohail (04 Mar 2011) -- End
                End Select
                End Select
                'Sandeep [ 17 DEC 2010 ] -- End 

            'Sohail (04 Mar 2011) -- Start
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure you want to delete this Payment?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Try
            End If
            'Sohail (04 Mar 2011) -- End

                objPaymentTran._Isvoid = True
                objPaymentTran._Voiduserunkid = User._Object._Userunkid

                'Sandeep [ 16 Oct 2010 ] -- Start
                'objPaymentTran._Voidreason = "TESTING"
                'objPaymentTran._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss"))
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objPaymentTran._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objPaymentTran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End 
            With objPaymentTran
                ._FormName = mstrModuleName
                ._LoginEmployeeUnkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPaymentTran.Delete(CInt(lvPayment.SelectedItems(0).Tag))
            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            'If objPaymentTran.Delete(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_Startdate, mdtPeriod_Enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(lvPayment.SelectedItems(0).Tag), ConfigParameter._Object._CurrentDateAndTime, True) = False Then
            If objPaymentTran.Delete(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objTranPeriod._Start_Date, objTranPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, CInt(lvPayment.SelectedItems(0).Tag), ConfigParameter._Object._CurrentDateAndTime, True, , dtActAdj) = False Then
                'Sohail (23 May 2017) -- End
                eZeeMsgBox.Show(objPaymentTran._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Sohail (21 Aug 2015) -- End
                lvPayment.SelectedItems(0).Remove()


                If lvPayment.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvPayment.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvPayment.Items.Count - 1
                    lvPayment.Items(intSelectedIndex).Selected = True
                    lvPayment.EnsureVisible(intSelectedIndex)
                ElseIf lvPayment.Items.Count <> 0 Then
                    lvPayment.Items(intSelectedIndex).Selected = True
                    lvPayment.EnsureVisible(intSelectedIndex)
                End If
            'End If 'Sohail (04 Mar 2011)
            lvPayment.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmPayment_AddEdit
        'Sohail (13 Feb 2013) -- Start
        'TRA - ENHANCEMENT
        Dim strFilter As String = ""
        Dim dsList As DataSet
        'Sohail (13 Feb 2013) -- End

        Try
            If lvPayment.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Payment from the list to perform further operation."), enMsgBoxStyle.Information)
                lvPayment.Select()
                Exit Sub
            End If

            'Sohail (04 Mar 2011) -- Start
            objPaymentTran._Paymenttranunkid = CInt(lvPayment.SelectedItems(0).Tag)
            If objPaymentTran._Paymentdate.Date < FinancialYear._Object._Database_Start_Date.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, You cannot edit this Payment. Reason: This Payment is brought forwarded."), enMsgBoxStyle.Information) '?1
                lvPayment.Select()
                Exit Sub
            End If
            'Sohail (04 Mar 2011) -- End

            'Sohail (13 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            If objPaymentTran._Paymentrefid = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                strFilter = " prpayment_tran.periodunkid = " & CInt(lvPayment.SelectedItems(0).SubItems(colhPayPeriod.Index).Tag) & " AND isauthorized = 1 AND prpayment_tran.paymenttranunkid = " & CInt(lvPayment.SelectedItems(0).Tag) & " "
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPaymentTran.GetList("Authorized", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, strFilter)
                dsList = objPaymentTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_Startdate, mdtPeriod_Enddate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Authorized", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, False, strFilter)
                'Sohail (21 Aug 2015) -- End
                If dsList.Tables("Authorized").Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry! This payments is already Authorized. Please Void Authorized payment for selected employee."), enMsgBoxStyle.Information) '?1
                    lvPayment.Select()
                    Exit Try
                End If
            End If

            If ConfigParameter._Object._SetPayslipPaymentApproval = True Then
                If objPaymentTran._Isapproved = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, You cannot edit this Payment. Reason: This Payment is Final Approved."), enMsgBoxStyle.Information) '?1
                    lvPayment.Select()
                    Exit Sub
                End If

                Dim objPaymentApproval As New clsPayment_approval_tran
                dsList = objPaymentApproval.GetList("Approved", True, lvPayment.SelectedItems(0).Tag.ToString, , , "levelunkid <> -1 AND priority <> -1 ")
                If dsList.Tables("Approved").Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot edit this Payment. Reason: This Payment is Approved by some Approvers."), enMsgBoxStyle.Information) '?1
                    lvPayment.Select()
                    Exit Sub
                End If
                objPaymentApproval = Nothing
            End If
            'Sohail (13 Feb 2013) -- End

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            Select Case objPaymentTran._Paymentrefid
                Case clsPayment_tran.enPaymentRefId.LOAN
                    Select Case objPaymentTran._PaymentTypeId
                        Case clsPayment_tran.enPayTypeId.PAYMENT, clsPayment_tran.enPayTypeId.RECEIVED
                            Dim ObjLoan_Advance As New clsLoan_Advance
                            ObjLoan_Advance._Loanadvancetranunkid = objPaymentTran._Payreftranunkid
                            If ObjLoan_Advance._Isloan = True Then
                                If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Net_Amount Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot edit this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                    End Select
                Case clsPayment_tran.enPaymentRefId.ADVANCE
                    Select Case objPaymentTran._PaymentTypeId
                        Case clsPayment_tran.enPayTypeId.PAYMENT, clsPayment_tran.enPayTypeId.RECEIVED
                            Dim ObjLoan_Advance As New clsLoan_Advance
                            ObjLoan_Advance._Loanadvancetranunkid = objPaymentTran._Payreftranunkid
                            If ObjLoan_Advance._Isloan = False Then
                                If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Advance_Amount Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot edit this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), enMsgBoxStyle.Information)
                                    Exit Sub
                                End If
                            End If
                    End Select
                Case clsPayment_tran.enPaymentRefId.PAYSLIP

                Case clsPayment_tran.enPaymentRefId.SAVINGS
                    Select Case objPaymentTran._PaymentTypeId
                        Case clsPayment_tran.enPayTypeId.REPAYMENT
                    End Select
            End Select
            'S.SANDEEP [ 29 May 2013 ] -- END

            

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvPayment.SelectedItems(0).Index

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            frm._IsRefSelectionId = CInt(lvPayment.SelectedItems(0).Index)
            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            frm._LoanSavingEffectiveDate = mdtLoanSavingEffectiveDate
            'Sohail (12 Dec 2015) -- End

            If frm.displayDialog(CInt(lvPayment.SelectedItems(0).Tag), _
                                 enAction.EDIT_ONE, _
                                 mintReferenceId, _
                                 mintTransactonId, _
                                 mintPaymentTypeId) Then
                Call FillList()
            End If

            frm = Nothing

            lvPayment.Items(intSelectedIndex).Selected = True
            lvPayment.EnsureVisible(intSelectedIndex)
            lvPayment.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmPayment_AddEdit
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            'Sohail (04 Mar 2011) -- Start
            If mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP AndAlso lvPayment.Items.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, You can not make new payment. Reason: Payment is already done in this period."), enMsgBoxStyle.Information)
                lvPayment.Select()
                Exit Try
            End If
            'Sohail (04 Mar 2011) -- End

            frm._PaymentListCount = CInt(lvPayment.Items.Count)
            'Sohail (12 Dec 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            frm._LoanSavingEffectiveDate = mdtLoanSavingEffectiveDate
            'Sohail (12 Dec 2015) -- End
            If frm.displayDialog(-1, enAction.ADD_ONE, mintReferenceId, mintTransactonId, mintPaymentTypeId) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboPayPeriod.SelectedValue = 0
            cboPayYear.SelectedValue = 0
            txtPaidAmount.Text = ""
            txtPaidAmountTo.Text = ""
            txtVoucherno.Text = ""
            dtpPaymentDate.Checked = False
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (12 Oct 2011) -- Start
    'Private Sub btnGlobalVoidPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGlobalVoidPayment.Click
    '    Dim frm As New frmGlobalVoidPayment
    '    Try
    '        'If User._Object._RightToLeft = True Then
    '        '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '        '    frm.RightToLeftLayout = True
    '        '    Call Language.ctlRightToLeftlayOut(frm)
    '        'End If

    '        If frm.displayDialog(mintReferenceId, mintPaymentTypeId) Then
    '            Call FillList()
            'End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnGlobalVoidPayment_Click", mstrModuleName)
    '    Finally
    '        If frm IsNot Nothing Then frm.Dispose()
    '    End Try
    'End Sub
    'Sohail (12 Oct 2011) -- End

#End Region

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
#Region " Combobox Events "
    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Try
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                mdtPeriod_Startdate = eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
                mdtPeriod_Enddate = eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
            Else
                mdtPeriod_Startdate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriod_Enddate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (21 Aug 2015) -- End

    Private Sub objbtnSearchTrnHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTrnHead.Click
        Dim objfrm As New frmCommonSearch
        Try
            With objfrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objfrm.DisplayDialog Then
                cboEmployee.SelectedValue = objfrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTrnHead_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lvPayment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvPayment.Click, lvPayment.SelectedIndexChanged
        Dim objPaymentTran As New clsPayment_tran
        Dim intMaxId As Integer = -1
        Try
            'Sohail (25 Mar 2015) -- Start
            'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
            'If mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP Then Exit Sub
            If mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP OrElse mintReferenceId = clsPayment_tran.enPaymentRefId.LOAN OrElse mintReferenceId = clsPayment_tran.enPaymentRefId.ADVANCE OrElse mintReferenceId = clsPayment_tran.enPaymentRefId.SAVINGS Then Exit Sub
            'Sohail (25 Mar 2015) -- End
            If lvPayment.SelectedItems.Count > 0 Then
                objPaymentTran.Get_Max_PaymentId(mintTransactonId, mintReferenceId, mintPaymentTypeId, intMaxId)
                If CInt(lvPayment.SelectedItems(0).Tag) = intMaxId Then
                    btnEdit.Enabled = True
                Else
                    btnEdit.Enabled = False
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPayment_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.EZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.EZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.EZeeHeader.Title = Language._Object.getCaption(Me.EZeeHeader.Name & "_Title" , Me.EZeeHeader.Title)
			Me.EZeeHeader.Message = Language._Object.getCaption(Me.EZeeHeader.Name & "_Message" , Me.EZeeHeader.Message)
			Me.colhDate.Text = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.Text)
			Me.colhVoucher.Text = Language._Object.getCaption(CStr(Me.colhVoucher.Tag), Me.colhVoucher.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhPayYear.Text = Language._Object.getCaption(CStr(Me.colhPayYear.Tag), Me.colhPayYear.Text)
			Me.colhPayPeriod.Text = Language._Object.getCaption(CStr(Me.colhPayPeriod.Tag), Me.colhPayPeriod.Text)
			Me.colhPaidAmount.Text = Language._Object.getCaption(CStr(Me.colhPaidAmount.Tag), Me.colhPaidAmount.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
			Me.lblPaidAmount.Text = Language._Object.getCaption(Me.lblPaidAmount.Name, Me.lblPaidAmount.Text)
			Me.lblPaymentDate.Text = Language._Object.getCaption(Me.lblPaymentDate.Name, Me.lblPaymentDate.Text)
			Me.lblVoucherno.Text = Language._Object.getCaption(Me.lblVoucherno.Name, Me.lblVoucherno.Text)
			Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
			Me.lblPayYear.Text = Language._Object.getCaption(Me.lblPayYear.Name, Me.lblPayYear.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.colhCurrency.Text = Language._Object.getCaption(CStr(Me.colhCurrency.Tag), Me.colhCurrency.Text)
			Me.colhEmpCode.Text = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.Text)
			Me.colhPaymentDatePeriod.Text = Language._Object.getCaption(CStr(Me.colhPaymentDatePeriod.Tag), Me.colhPaymentDatePeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Payment from the list to perform further operation.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Payment. Reason: This Payment is brought forwarded.")
			Language.setMessage(mstrModuleName, 3, "Sorry! This payments is already Authorized. Please Void Authorized payment for selected employee.")
			Language.setMessage(mstrModuleName, 4, "Sorry, You cannot delete this Payment. Reason: This Payment is Final Approved.")
			Language.setMessage(mstrModuleName, 5, "Are you sure you want to delete this Payment?")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot edit this Loan/Advance payment. Reason : Deduction or Repayment is already done.")
			Language.setMessage(mstrModuleName, 7, "Sorry, You cannot delete this Payment. Reason: This Payment is Approved by some Approvers.")
			Language.setMessage(mstrModuleName, 8, "Sorry, You can not make new payment. Reason: Payment is already done in this period.")
			Language.setMessage(mstrModuleName, 9, "Salary payments Voided for the period of")
			Language.setMessage(mstrModuleName, 10, "List of Receipts")
			Language.setMessage(mstrModuleName, 11, "Receipt List")
			Language.setMessage(mstrModuleName, 12, "Sorry, You cannot edit this Payment. Reason: This Payment is Final Approved.")
			Language.setMessage(mstrModuleName, 13, "Sorry, You cannot edit this Payment. Reason: This Payment is Approved by some Approvers.")
			Language.setMessage(mstrModuleName, 14, "Sorry, you cannot edit this Loan/Advance payment. Reason : Deduction or Repayment is already done against this.")
			Language.setMessage(mstrModuleName, 15, "Sorry, you cannot delete this Loan/Advance payment. Reason : Payment date period is already closed.")
			Language.setMessage(mstrModuleName, 16, "Sorry, you cannot delete this Loan/Advance payment. Reason : Process Payroll is already done for the last date of selected period for the selected employee.")
			Language.setMessage(mstrModuleName, 17, "Sorry, Actual Salary Payment will be exceeding to the Activity Current Balance.")
			Language.setMessage(mstrModuleName, 18, "Sorry, You cannot edit this Payment. Reason: This Payment is brought forwarded.")
			Language.setMessage(mstrModuleName, 19, "Sorry, You cannot delete this Loan/Advance payment. Reason : Loan/Advance is already completed.")
			Language.setMessage(mstrModuleName, 20, "Do you want to void Payroll?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class