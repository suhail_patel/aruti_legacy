﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 3

Public Class frmTraningInstitutes_AddEdit

#Region "Private Variable"
    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private mstrModuleName As String = ""
    'S.SANDEEP [ 20 AUG 2011 ] -- END 
    Private mblnCancel As Boolean = True
    Private objInstitutemaster As clsinstitute_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintInstituteMasterUnkid As Integer = -1
    Private mbitIshospital As Boolean = False
    'S.SANDEEP [ 11 MAR 2014 ] -- START
    'Dim strExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    'S.SANDEEP [ 11 MAR 2014 ] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal IsHospital As Boolean) As Boolean
        Try
            mintInstituteMasterUnkid = intUnkId
            mbitIshospital = IsHospital
            menAction = eAction

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Select Case mbitIshospital
                Case True
                    Me.Name = "frmServiceProvider"
                    Me.Text = "Add/Edit Service Provider"
                    Me.gbTaraningInstitutes.Text = "Service Provider"
                    Me.eZeeHeader.Title = "Service Provider"
                Case False
                    Me.Name = "frmTrainingProvider"
                    Me.Text = "Add/Edit Training Provider"
                    Me.gbTaraningInstitutes.Text = "Training Provider"
                    Me.eZeeHeader.Title = "Training Provider"
            End Select
            mstrModuleName = Me.Name
            'S.SANDEEP [ 20 AUG 2011 ] -- END 


            Me.ShowDialog()

            intUnkId = mintInstituteMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmTraningInstitutes_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objInstitutemaster = New clsinstitute_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            If mbitIshospital Then
                chkIsFormRequire.Visible = True
                'S.SANDEEP [ 03 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                chkSyncwithRecruitment.Visible = False
                chkSyncwithRecruitment.Checked = False
                'S.SANDEEP [ 03 MAY 2012 ] -- END


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
                chkLocalProvider.Visible = True
                chkLocalProvider.Checked = True
                'Pinkal (18-Dec-2012) -- End

            Else
                chkIsFormRequire.Visible = False
                'S.SANDEEP [ 03 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                chkSyncwithRecruitment.Visible = True
                chkSyncwithRecruitment.Checked = False
                'S.SANDEEP [ 03 MAY 2012 ] -- END

                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
                chkLocalProvider.Visible = False
                chkLocalProvider.Checked = False
                'Pinkal (18-Dec-2012) -- End

            End If
            'Pinkal (12-Oct-2011) -- End

            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 15 April 2013 ] -- END

            setColor()
            If menAction = enAction.EDIT_ONE Then
                objInstitutemaster._Instituteunkid = mintInstituteMasterUnkid
            End If
            FillCombo()
            GetValue()
            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTraningInstitutes_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTraningInstitutes_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTraningInstitutes_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTraningInstitutes_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTraningInstitutes_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmTraningInstitutes_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objInstitutemaster = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsinstitute_master.SetMessages()
            objfrm._Other_ModuleNames = "clsinstitute_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Dropdown's Event"

    Private Sub cboCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            FillState()
            'FillCity()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged
        Try

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            If CInt(cboState.SelectedValue) > 0 Then
                FillCity()
            End If

            'Pinkal (12-Oct-2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCountry_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False

        'S.SANDEEP [ 11 MAR 2014 ] -- START
        'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
        Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
        'S.SANDEEP [ 11 MAR 2014 ] -- END

        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Institute Code is compulsory information.Please Select Institute Code."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            ElseIf Trim(txtInstituteName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Institute Name cannot be blank. Institute Name is required information."), enMsgBoxStyle.Information)
                txtInstituteName.Focus()
                Exit Sub
                'ElseIf CInt(cboCountry.SelectedValue) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Country is compulsory information.Please Select Country."), enMsgBoxStyle.Information)
                '    cboCountry.Select()
                '    Exit Sub
                'ElseIf CInt(cboState.SelectedValue) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "State is compulsory information.Please Select State."), enMsgBoxStyle.Information)
                '    cboState.Select()
                '    Exit Sub
                'ElseIf CInt(cboCity.SelectedValue) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "City is compulsory information.Please Select City."), enMsgBoxStyle.Information)
                '    cboCity.Select()
                '    Exit Sub
            ElseIf txtEmail.Text.Trim.Length > 0 Then
                If Expression.IsMatch(txtEmail.Text.Trim) = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Invalid Email.Please Enter Valid Email"), enMsgBoxStyle.Information)
                    txtEmail.Focus()
                    Exit Sub
                End If
            End If
            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objInstitutemaster._FormName = mstrModuleName
            objInstitutemaster._LoginEmployeeunkid = 0
            objInstitutemaster._ClientIP = getIP()
            objInstitutemaster._HostName = getHostName()
            objInstitutemaster._FromWeb = False
            objInstitutemaster._AuditUserId = User._Object._Userunkid
objInstitutemaster._CompanyUnkid = Company._Object._Companyunkid
            objInstitutemaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objInstitutemaster.Update()
            Else
                blnFlag = objInstitutemaster.Insert()
            End If

            If blnFlag = False And objInstitutemaster._Message <> "" Then
                eZeeMsgBox.Show(objInstitutemaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objInstitutemaster = Nothing
                    objInstitutemaster = New clsinstitute_master
                    Call GetValue()
                    txtCode.Focus()
                Else
                    mintInstituteMasterUnkid = objInstitutemaster._Instituteunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtInstituteName.BackColor = GUI.ColorComp
            txtAddress.BackColor = GUI.ColorOptional
            cboCountry.BackColor = GUI.ColorComp
            cboState.BackColor = GUI.ColorComp
            cboCity.BackColor = GUI.ColorComp
            txtContactPerson.BackColor = GUI.ColorOptional
            txtEmail.BackColor = GUI.ColorOptional
            txtFax.BackColor = GUI.ColorOptional
            txtTelNo.BackColor = GUI.ColorOptional
            txtDescription.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objInstitutemaster._Institute_Code
            txtInstituteName.Text = objInstitutemaster._Institute_Name
            txtAddress.Text = objInstitutemaster._Institute_Address
            cboCountry.SelectedValue = objInstitutemaster._Countryunkid
            cboState.SelectedValue = objInstitutemaster._Stateunkid
            cboCity.SelectedValue = objInstitutemaster._Cityunkid
            txtContactPerson.Text = objInstitutemaster._Contact_Person
            txtEmail.Text = objInstitutemaster._Institute_Email
            txtFax.Text = objInstitutemaster._Institute_Fax
            txtTelNo.Text = objInstitutemaster._Telephoneno
            txtDescription.Text = objInstitutemaster._Description

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            If mbitIshospital Then
                chkIsFormRequire.Checked = objInstitutemaster._IsFormRequire

                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
                chkLocalProvider.Checked = objInstitutemaster._IsLocalProvider
                'Pinkal (18-Dec-2012) -- End

            End If
            'Pinkal (12-Oct-2011) -- End

            'S.SANDEEP [ 03 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mbitIshospital = False Then
                If menAction = enAction.EDIT_ONE Then
                    chkSyncwithRecruitment.Checked = objInstitutemaster._Issync_Recruit
                End If

            Else
                chkSyncwithRecruitment.Checked = False
            End If
            'S.SANDEEP [ 03 MAY 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objInstitutemaster._Institute_Code = txtCode.Text.Trim
            objInstitutemaster._Institute_Name = txtInstituteName.Text.Trim
            objInstitutemaster._Institute_Address = txtAddress.Text.Trim
            objInstitutemaster._Countryunkid = CInt(cboCountry.SelectedValue)
            objInstitutemaster._Stateunkid = CInt(cboState.SelectedValue)
            objInstitutemaster._Cityunkid = CInt(cboCity.SelectedValue)
            objInstitutemaster._Contact_Person = txtContactPerson.Text.Trim
            objInstitutemaster._Institute_Email = txtEmail.Text.Trim
            objInstitutemaster._Telephoneno = txtTelNo.Text.Trim
            objInstitutemaster._Institute_Fax = txtFax.Text.Trim
            objInstitutemaster._Description = txtDescription.Text.Trim
            objInstitutemaster._Ishospital = mbitIshospital


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            If mbitIshospital Then
                objInstitutemaster._IsFormRequire = chkIsFormRequire.Checked
                'S.SANDEEP [ 03 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                objInstitutemaster._Issync_Recruit = False
                'S.SANDEEP [ 03 MAY 2012 ] -- END


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
                objInstitutemaster._IsLocalProvider = chkLocalProvider.Checked
                'Pinkal (18-Dec-2012) -- End

            End If
            'Pinkal (12-Oct-2011) -- End

            'S.SANDEEP [ 03 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mbitIshospital = False Then
                objInstitutemaster._Issync_Recruit = chkSyncwithRecruitment.Checked
            End If
            'S.SANDEEP [ 03 MAY 2012 ] -- END




        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objMasterData As New clsMasterData
            Dim dsCountry As DataSet = objMasterData.getCountryList("Country", True)
            cboCountry.ValueMember = "countryunkid"
            cboCountry.DisplayMember = "country_name"
            cboCountry.DataSource = dsCountry.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillState()
        Dim dsState As DataSet = Nothing
        Try
            Dim objStatemaster As New clsstate_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsState = objStatemaster.GetList("State", True, False, CInt(cboCountry.SelectedValue))
            Else
                dsState = objStatemaster.GetList("State", True, True)
            End If
            cboState.ValueMember = "stateunkid"
            cboState.DisplayMember = "name"
            cboState.DataSource = dsState.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillState", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCity()
        Dim dsCity As DataSet = Nothing
        Try
            Dim objCitymaster As New clscity_master
            If CInt(cboCountry.SelectedValue) > 0 Then
                dsCity = objCitymaster.GetList("City", True, False, CInt(cboState.SelectedValue))
            Else
                dsCity = objCitymaster.GetList("City", True, True)
            End If
            cboCity.ValueMember = "cityunkid"
            cboCity.DisplayMember = "name"
            cboCity.DataSource = dsCity.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCity", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 15 April 2013 ] -- START
    'ENHANCEMENT : LICENSE CHANGES
    Private Sub SetVisibility()
        Try
            If mbitIshospital Then
                chkIsFormRequire.Visible = True
                chkSyncwithRecruitment.Visible = False
                chkSyncwithRecruitment.Checked = False
                chkLocalProvider.Visible = True
                chkLocalProvider.Checked = True
            Else
                chkIsFormRequire.Visible = False
                chkSyncwithRecruitment.Visible = True
                chkSyncwithRecruitment.Checked = False
                chkLocalProvider.Visible = False
                chkLocalProvider.Checked = False
            End If
            If ConfigParameter._Object._IsArutiDemo = False Then
                chkSyncwithRecruitment.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management)
                chkSyncwithRecruitment.Checked = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management)

                chkLocalProvider.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management)
                chkLocalProvider.Checked = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management)

                chkIsFormRequire.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management)
                chkIsFormRequire.Checked = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 15 April 2013 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbTaraningInstitutes.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbTaraningInstitutes.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbTaraningInstitutes.Text = Language._Object.getCaption(Me.gbTaraningInstitutes.Name, Me.gbTaraningInstitutes.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.lblContactPerson.Text = Language._Object.getCaption(Me.lblContactPerson.Name, Me.lblContactPerson.Text)
			Me.lblFax.Text = Language._Object.getCaption(Me.lblFax.Name, Me.lblFax.Text)
			Me.lblTelephone.Text = Language._Object.getCaption(Me.lblTelephone.Name, Me.lblTelephone.Text)
			Me.lblInstiuteAddress.Text = Language._Object.getCaption(Me.lblInstiuteAddress.Name, Me.lblInstiuteAddress.Text)
			Me.lblInstituteName.Text = Language._Object.getCaption(Me.lblInstituteName.Name, Me.lblInstituteName.Text)
			Me.lblCodes.Text = Language._Object.getCaption(Me.lblCodes.Name, Me.lblCodes.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)
			Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.chkIsFormRequire.Text = Language._Object.getCaption(Me.chkIsFormRequire.Name, Me.chkIsFormRequire.Text)
			Me.chkSyncwithRecruitment.Text = Language._Object.getCaption(Me.chkSyncwithRecruitment.Name, Me.chkSyncwithRecruitment.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.chkLocalProvider.Text = Language._Object.getCaption(Me.chkLocalProvider.Name, Me.chkLocalProvider.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Institute Code is compulsory information.Please Select Institute Code.")
			Language.setMessage(mstrModuleName, 2, "Institute Name cannot be blank. Institute Name is required information.")
			Language.setMessage(mstrModuleName, 3, "Invalid Email.Please Enter Valid Email")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class