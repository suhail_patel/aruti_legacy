﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization


Public Class frmCommonMaster
    Private ReadOnly mstrModuleName As String = "frmCommonMaster"
    Dim objCommonMaster As clsCommon_Master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintMasterUnkid As Integer = -1
    Private mintMasterTypeId As Integer = -1
    Private mblnCancel As Boolean = True
    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Private mdtOldAllocation As DataTable = Nothing
    Private mdtAllocation As DataTable = Nothing
    'Sohail (14 Nov 2019) -- End

#Region " Private Methods "
    Private Sub setColor()
        Try
            'txtCode.BackColor = GUI.ColorOptional
            'txtName.BackColor = GUI.ColorComp
            '' txtDescription.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            mdtOldAllocation = Nothing
            mdtAllocation = Nothing
            'Sohail (14 Nov 2019) -- End
            txtCode.Text = objCommonMaster._Code
            txtAlias.Text = objCommonMaster._Alias
            txtName.Text = objCommonMaster._Name
            'S.SANDEEP [21 NOV 2015] -- START
            txtDescription.Text = objCommonMaster._Description
            'S.SANDEEP [21 NOV 2015] -- END


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            If objCommonMaster._QualificationGrp_Level.ToString() <> "0" Then
                nudLevel.Value = objCommonMaster._QualificationGrp_Level
            Else
                nudLevel.Value = 0
            End If

            'Pinkal (12-Oct-2011) -- End


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            If mintMasterTypeId = clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER Then
                If objCommonMaster._CouseTypeId = enCourseType.Job_Capability Then
                    rdJobCapability.Checked = True
                ElseIf objCommonMaster._CouseTypeId = enCourseType.Career_Development Then
                    rdCareerDevelopment.Checked = True
                End If

            End If

            'Pinkal (06-Feb-2012) -- End

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintMasterTypeId = clsCommon_Master.enCommonMaster.RELATIONS Then
                chkAllowedinReferee.Checked = objCommonMaster._IsRefreeAllowed
            End If
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            'Sohail (22 Apr 2015) -- Start
            'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            If mintMasterTypeId = clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES Then
                chkSyncWithRecruitment.Checked = objCommonMaster._IsSyncWithrecruitment
                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                chkMandatoryForNewEmp.Checked = objCommonMaster._IsMandatoryForNewEmp
                'Nilay (03-Dec-2015) -- End

            End If
            'Sohail (22 Apr 2015) -- End

            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
            If mintMasterTypeId = clsCommon_Master.enCommonMaster.VACANCY_SOURCE Then
                chkSyncWithRecruitment.Checked = objCommonMaster._IsSyncWithrecruitment
            End If
            'Sohail (27 Sep 2019) -- End

            'Hemant (06 Oct 2018) -- Start
            'Enhancement : Implementing New Module of Asset Declaration Template 2
            If mintMasterTypeId = clsCommon_Master.enCommonMaster.RELATIONS Then
                chkUseInAssetDeclaration.Checked = objCommonMaster._IsUseInAssetDeclaration
            End If
            'Hemant (06 Oct 2018) -- End

           'S.SANDEEP [09-OCT-2018] -- START
            If mintMasterTypeId = clsCommon_Master.enCommonMaster.TRAINING_RESOURCES Then
                chkTrainingRequisition.Checked = objCommonMaster._IsForTrainingRequisition
            End If
            'S.SANDEEP [09-OCT-2018] -- END

            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            If mintMasterTypeId = clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER AndAlso mintMasterUnkid > 0 Then
                Dim objCommonAllocation As New clsCommonmaster_allocation
                Dim ds As DataSet = objCommonAllocation.GetList("List", mintMasterUnkid, True)
                mdtOldAllocation = ds.Tables(0).Copy
                mdtAllocation = ds.Tables(0)
            End If
            'Sohail (14 Nov 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try


    End Sub

    Private Sub SetValue()
        Try
            objCommonMaster._Code = txtCode.Text
            objCommonMaster._Alias = txtAlias.Text
            objCommonMaster._Name = txtName.Text
            objCommonMaster._Mastertype = mintMasterTypeId
            'S.SANDEEP [21 NOV 2015] -- START
            objCommonMaster._Description = txtDescription.Text
            'S.SANDEEP [21 NOV 2015] -- END

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            objCommonMaster._QualificationGrp_Level = CInt(nudLevel.Value)
            'Pinkal (12-Oct-2011) -- End


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            If mintMasterTypeId = clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER Then
                If rdJobCapability.Checked Then
                    objCommonMaster._CouseTypeId = enCourseType.Job_Capability
                ElseIf rdCareerDevelopment.Checked Then
                    objCommonMaster._CouseTypeId = enCourseType.Career_Development
                End If
            End If

            'Pinkal (06-Feb-2012) -- End

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintMasterTypeId = clsCommon_Master.enCommonMaster.RELATIONS Then
                objCommonMaster._IsRefreeAllowed = chkAllowedinReferee.Checked
            End If
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            'Sohail (22 Apr 2015) -- Start
            'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
            If mintMasterTypeId = clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES Then
                objCommonMaster._IsSyncWithrecruitment = chkSyncWithRecruitment.Checked
                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                objCommonMaster._IsMandatoryForNewEmp = chkMandatoryForNewEmp.Checked
                'Nilay (03-Dec-2015) -- End
            End If
            'Sohail (22 Apr 2015) -- End

            'Sohail (27 Sep 2019) -- Start
            'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
            If mintMasterTypeId = clsCommon_Master.enCommonMaster.VACANCY_SOURCE Then
                objCommonMaster._IsSyncWithrecruitment = chkSyncWithRecruitment.Checked
            End If
            'Sohail (27 Sep 2019) -- End

            'Hemant (06 Oct 2018) -- Start
            'Enhancement : Implementing New Module of Asset Declaration Template 2
            If mintMasterTypeId = clsCommon_Master.enCommonMaster.RELATIONS Then
                objCommonMaster._IsUseInAssetDeclaration = chkUseInAssetDeclaration.Checked
            End If
            'Hemant (06 Oct 2018) -- End
             'S.SANDEEP [09-OCT-2018] -- START
            If mintMasterTypeId = clsCommon_Master.enCommonMaster.TRAINING_RESOURCES Then
                objCommonMaster._IsForTrainingRequisition = chkTrainingRequisition.Checked
            End If
            'S.SANDEEP [09-OCT-2018] -- END
            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            objCommonMaster._Userunkid = User._Object._Userunkid
            'Sohail (14 Nov 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try

    End Sub

    Private Sub MasterFillList()
        objCommonMaster = New clsCommon_Master
        Dim objMasterData As New clsMasterData
        Dim lvItem As ListViewItem
        Dim dsList As New DataSet

        Try

            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            If User._Object.Privilege._AllowToViewCommonMasterList = False Then Exit Sub
            'Anjan (25 Oct 2012)-End 


            dsList = objMasterData.getComboList_Common_Master("List")

            For Each drRow As DataRow In dsList.Tables(0).Rows
                If mintMasterTypeId > 0 Then
                    If mintMasterTypeId = CInt(drRow.Item("Id")) Then
                        lvItem = New ListViewItem
                        lvItem.Text = drRow.Item("NAME").ToString
                        lvItem.Tag = CInt(drRow.Item("Id"))
                        lvMasterList.Items.Add(lvItem)
                        Exit For
                    End If
                Else
                    lvItem = New ListViewItem
                    lvItem.Text = drRow.Item("NAME").ToString
                    lvItem.Tag = CInt(drRow.Item("Id"))
                    lvMasterList.Items.Add(lvItem)
                End If
            Next

            If lvMasterList.Items.Count > 16 Then
                colhMasterList.Width = 232 - 18
            Else
                colhMasterList.Width = 232
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "MasterFillList", mstrModuleName)
        Finally
            dsList.Dispose()
            lvItem = Nothing
        End Try
    End Sub

    Private Sub MasterInfoFillList()
        Dim dsMasterListInfo As New DataSet
        Dim lvItem As ListViewItem
        Try
            dsMasterListInfo = objCommonMaster.GetList(mintMasterTypeId)

            lvMasterListInfo.Items.Clear()

            For Each drRow As DataRow In dsMasterListInfo.Tables(0).Rows
                lvItem = New ListViewItem

                If IsDBNull(drRow("alias")) Then
                    lvItem.SubItems.Add("")
                Else

                    lvItem.Text = drRow("alias").ToString
                End If
                If IsDBNull(drRow("code")) Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(drRow("code").ToString)
                End If
                If IsDBNull(drRow("name")) Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(drRow("name").ToString)
                End If


                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes

                If IsDBNull(drRow("qlevel")) Or drRow("qlevel").ToString() = "0" Then
                    lvItem.SubItems.Add("")
                Else
                    lvItem.SubItems.Add(drRow("qlevel").ToString)
                End If

                    'Pinkal (12-Oct-2011) -- End


                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                lvItem.SubItems.Add(drRow("description").ToString)
                'Nilay (03-Dec-2015) -- End

                lvItem.Tag = drRow("masterunkid")
                lvMasterListInfo.Items.Add(lvItem)
                lvItem = Nothing
            Next

            'S.SANDEEP [21 NOV 2015] -- START
            'If lvMasterListInfo.Items.Count > 14 Then
            '    lvMasterListInfo.Columns(colhName.Index).Width = 245 - 18
            'Else
            '    lvMasterListInfo.Columns(colhName.Index).Width = 245
            'End If

            If lvMasterListInfo.Items.Count > 8 Then
                lvMasterListInfo.Columns(colhDecription.Index).Width = 140 - 18
            Else
                lvMasterListInfo.Columns(colhDecription.Index).Width = 140
            End If
            'S.SANDEEP [21 NOV 2015] -- END
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "MasterInfoFillList", mstrModuleName)
        Finally
            'objCommonMaster = Nothing
            If dsMasterListInfo IsNot Nothing Then
                dsMasterListInfo.Dispose()
            End If
            dsMasterListInfo = Nothing
            lvItem = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnSave.Enabled = User._Object.Privilege._AddCommonMasters
            btnDelete.Enabled = User._Object.Privilege._DeleteCommonMaster

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmCommonMaster_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objCommonMaster = Nothing
    End Sub

    Private Sub frmCommonMaster_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmCommonMaster_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmCommonMaster_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvMasterListInfo.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmCommonMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            objCommonMaster = New clsCommon_Master

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call SetVisibility()

            Call setColor()

            Call MasterFillList()

            If lvMasterList.Items.Count > 0 Then
                lvMasterList.Items(0).Selected = True
                'If mintMasterTypeId > 0 Then
                '    lvMasterList.Items(mintMasterTypeId - 1).Selected = True
                'Else
                '    lvMasterList.Items(0).Selected = True
                'End If
            End If
            lvMasterList.Select()

            'If lvMasterListInfo.Items.Count > 0 Then
            '    lvMasterListInfo.Enabled = True
            'Else
            '    lvMasterListInfo.Enabled = False
            'End If


            If menAction = enAction.EDIT_ONE Then
                objCommonMaster._Masterunkid = mintMasterUnkid
            Else
                objCommonMaster._Mastertype = mintMasterTypeId
            End If
            txtAlias.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCommonMaster_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCommon_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsCommon_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If
            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Name cannot be blank. Name is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If


            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes
            If lvMasterList.SelectedItems.Count > 0 Then
                If CInt(lvMasterList.SelectedItems(0).Tag) = clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER And rdJobCapability.Checked = False And rdCareerDevelopment.Checked = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select atleast one course type."), enMsgBoxStyle.Information) '?1
                    Exit Sub
                End If
            End If
            'Pinkal (06-Feb-2012) -- End


            Call SetValue()

           'Sohail (14 Nov 2019) -- Start
           'NMB UAT Enhancement # : Bind Allocation to training course.
            With objCommonMaster
                ._FormName = mstrModuleName
                ._LoginEmployeeunkid = 0
                ._ClientIP = getIP()
                ._HostName = getHostName()
                ._FromWeb = False
                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End With
                'Sohail (14 Nov 2019) -- End

            If menAction = enAction.EDIT_ONE Then
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'blnFlag = objCommonMaster.Update()
                If mdtAllocation IsNot Nothing Then
                    Dim dtCol() As DataColumn = {mdtAllocation.Columns("advancefilterallocationid"), mdtAllocation.Columns("advancefilterallocationtranunkid")}
                    mdtAllocation.PrimaryKey = dtCol

                    Dim dr As IEnumerable(Of DataRow) = Nothing
                    Dim dtDeleted As DataTable = Nothing
                    Dim dtAdded As DataTable = Nothing
                    dr = mdtOldAllocation.AsEnumerable.Except(mdtAllocation.AsEnumerable, DataRowComparer.Default)
                    If dr.Count > 0 Then
                        dtDeleted = dr.CopyToDataTable()
                    End If
                    dr = mdtAllocation.AsEnumerable.Except(mdtOldAllocation.AsEnumerable, DataRowComparer.Default)
                    If dr.Count > 0 Then
                        dtAdded = dr.CopyToDataTable
                    End If
                    If dtDeleted IsNot Nothing Then
                        For Each r As DataRow In dtDeleted.Rows
                            r.Item("AUD") = "D"
                        Next
                        dtDeleted.AcceptChanges()
                        mdtAllocation.Merge(dtDeleted.Copy)
                    End If
                    If dtAdded IsNot Nothing Then
                        For Each r As DataRow In dtAdded.Rows
                            r.Item("AUD") = "A"
                        Next
                        dtAdded.AcceptChanges()
                        mdtAllocation.Merge(dtAdded.Copy)
                    End If
                End If

                blnFlag = objCommonMaster.Update(mdtAllocation)
                'Sohail (14 Nov 2019) -- End
            Else
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'blnFlag = objCommonMaster.Insert()
                If mdtAllocation IsNot Nothing Then
                    For Each r As DataRow In mdtAllocation.Rows
                        r.Item("AUD") = "A"
                    Next
                    mdtAllocation.AcceptChanges()
                End If

                blnFlag = objCommonMaster.Insert(mdtAllocation)
                'Sohail (14 Nov 2019) -- End
            End If

            If blnFlag = False And objCommonMaster._Message <> "" Then
                eZeeMsgBox.Show(objCommonMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then

                If menAction = enAction.ADD_CONTINUE Then
                    objCommonMaster = Nothing
                    objCommonMaster = New clsCommon_Master
                    objCommonMaster._Mastertype = mintMasterTypeId
                    Call GetValue()
                    txtAlias.Focus()
                Else
                    mintMasterUnkid = objCommonMaster._Masterunkid
                    'Me.Close()
                End If
            End If
            Call MasterInfoFillList()

            If blnFlag = True Then
                txtAlias.Text = ""
                txtCode.Text = ""
                txtName.Text = ""
                'S.SANDEEP [21 NOV 2015] -- START
                txtDescription.Text = ""
                'S.SANDEEP [21 NOV 2015] -- END
                txtAlias.Focus()

                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes
                nudLevel.Value = 0
                'Pinkal (12-Oct-2011) -- End


                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes
                rdJobCapability.Checked = False
                rdCareerDevelopment.Checked = False
                'Pinkal (06-Feb-2012) -- End


                'S.SANDEEP [ 28 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                chkAllowedinReferee.Checked = False
                'S.SANDEEP [ 28 FEB 2012 ] -- END

                'Sohail (22 Apr 2015) -- Start
                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
                chkSyncWithRecruitment.Checked = False
                'Sohail (22 Apr 2015) -- End

                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                chkMandatoryForNewEmp.Checked = False
                'Nilay (03-Dec-2015) -- End

                'Hemant (06 Oct 2018) -- Start
                'Enhancement : Implementing New Module of Asset Declaration Template 2
                chkUseInAssetDeclaration.Checked = False
                'Hemant (06 Oct 2018) -- End
               
               'S.SANDEEP [09-OCT-2018] -- START
                chkTrainingRequisition.Checked = False
                'S.SANDEEP [09-OCT-2018] -- END
                menAction = enAction.ADD_CONTINUE
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        'If objCommonMaster.isUsed(mintMasterTypeId) = True And objCommonMaster._Message <> "" Then
        '    eZeeMsgBox.Show(objCommonMaster._Message, enMsgBoxStyle.Information)
        '    lvMasterListInfo.Select()
        '    Exit Sub
        'End If
        Try

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If lvMasterListInfo.SelectedItems.Count <= 0 Then
                Exit Sub
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END 

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Record?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                With objCommonMaster
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With

                objCommonMaster.Delete(mintMasterUnkid)
                If objCommonMaster._Message <> "" Then
                    eZeeMsgBox.Show(objCommonMaster._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    Call MasterInfoFillList()
                End If

            End If
            txtAlias.Text = ""
            txtCode.Text = ""
            txtName.Text = ""

            lvMasterListInfo.Select()

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnOtherLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOtherLanguage.Click
        Dim objFrm As New NameLanguagePopup_Form
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END


            Call objFrm.displayDialog(txtName.Text, objCommonMaster._Name1, objCommonMaster._Name2)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnOtherLanguage_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control Events "
    Private Sub lvMasterList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvMasterList.SelectedIndexChanged

        Try

            'Sohail (13 Nov 2019) -- Start
            'NMB Issue # : Sync with recruitment option not visible for scan attachment.
            chkSyncWithRecruitment.Visible = False
            'Sohail (13 Nov 2019) -- End
            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            lnkAdvanceFilter.Visible = False
            mdtOldAllocation = Nothing
            mdtAllocation = Nothing
            'Sohail (14 Nov 2019) -- End

            If lvMasterList.SelectedItems.Count > 0 Then
                gbMasterListInfo.Text = lvMasterList.SelectedItems(0).Text
                mintMasterTypeId = CInt(lvMasterList.SelectedItems(0).Tag)
                Call MasterInfoFillList()
                txtAlias.Text = ""
                txtCode.Text = ""
                txtName.Text = ""
                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                txtDescription.Text = ""
                'Nilay (03-Dec-2015) -- End

                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes
                nudLevel.Value = 0
                If CInt(lvMasterList.SelectedItems(0).Tag) = clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP Then
                    lblLevel.Visible = True
                    nudLevel.Visible = True
                    lblLevelDesc.Visible = True
                Else
                    lblLevel.Visible = False
                    nudLevel.Visible = False
                    lblLevelDesc.Visible = False
                End If

                'Pinkal (12-Oct-2011) -- End


                'Pinkal (06-Feb-2012) -- Start
                'Enhancement : TRA Changes
                If CInt(lvMasterList.SelectedItems(0).Tag) = clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER Then
                    rdCareerDevelopment.Visible = True
                    rdJobCapability.Visible = True
                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    lnkAdvanceFilter.Visible = True
                    'Sohail (14 Nov 2019) -- End
                Else
                    rdJobCapability.Checked = False
                    rdCareerDevelopment.Checked = False
                    rdCareerDevelopment.Visible = False
                    rdJobCapability.Visible = False
                End If
                'Pinkal (06-Feb-2012) -- End


                'S.SANDEEP [ 28 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                chkAllowedinReferee.Checked = False
                If CInt(lvMasterList.SelectedItems(0).Tag) = clsCommon_Master.enCommonMaster.RELATIONS Then
                    chkAllowedinReferee.Visible = True
                Else
                    chkAllowedinReferee.Visible = False
                End If
                'S.SANDEEP [ 28 FEB 2012 ] -- END

                'Sohail (22 Apr 2015) -- Start
                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
                chkSyncWithRecruitment.Checked = False
                'Nilay (03-Dec-2015) -- Start
                'ENHANCEMENT for CCBRT & SL - Requested by Andrew Muga.
                'If CInt(lvMasterList.SelectedItems(0).Tag) = clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES Then
                '    chkSyncWithRecruitment.Size = chkAllowedinReferee.Size
                '    chkSyncWithRecruitment.Location = chkAllowedinReferee.Location
                '    chkSyncWithRecruitment.Visible = True
                'Else
                '    chkSyncWithRecruitment.Visible = False
                'End If
                chkMandatoryForNewEmp.Checked = False
                If CInt(lvMasterList.SelectedItems(0).Tag) = clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES Then
                    chkSyncWithRecruitment.Size = chkAllowedinReferee.Size
                    chkSyncWithRecruitment.Location = chkAllowedinReferee.Location
                    chkSyncWithRecruitment.Visible = True
                    chkMandatoryForNewEmp.Visible = True
                Else
                    'Sohail (13 Nov 2019) -- Start
                    'NMB Issue # : Sync with recruitment option not visible for scan attachment.
                    'chkSyncWithRecruitment.Visible = False
                    'Sohail (13 Nov 2019) -- End
                    chkMandatoryForNewEmp.Visible = False
                End If
                'Nilay (03-Dec-2015) -- End

                
                'Sohail (22 Apr 2015) -- End

                'Sohail (27 Sep 2019) -- Start
                'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
                If CInt(lvMasterList.SelectedItems(0).Tag) = clsCommon_Master.enCommonMaster.VACANCY_SOURCE Then
                    chkSyncWithRecruitment.Size = chkAllowedinReferee.Size
                    chkSyncWithRecruitment.Location = chkAllowedinReferee.Location
                    chkSyncWithRecruitment.Visible = True
                Else
                    'Sohail (13 Nov 2019) -- Start
                    'NMB Issue # : Sync with recruitment option not visible for scan attachment.
                    'chkSyncWithRecruitment.Visible = False
                    'Sohail (13 Nov 2019) -- End
                End If
                'Sohail (27 Sep 2019) -- End

                'S.SANDEEP [09-OCT-2018] -- START
                If CInt(lvMasterList.SelectedItems(0).Tag) = clsCommon_Master.enCommonMaster.TRAINING_RESOURCES Then
                    chkTrainingRequisition.Visible = True
                Else
                    chkTrainingRequisition.Visible = False
                End If
                'S.SANDEEP [09-OCT-2018] -- END

                'If lvMasterListInfo.Items.Count > 0 Then
                '    lvMasterListInfo.Enabled = True
                '    txtAlias.Enabled = True
                '    txtCode.Enabled = True
                '    txtName.Enabled = True
                'Else
                '    lvMasterListInfo.Enabled = False
                '    txtAlias.Enabled = False
                '    txtCode.Enabled = False
                '    txtName.Enabled = False
                'End If

                'Hemant (06 Oct 2018) -- Start
                'Enhancement : Implementing New Module of Asset Declaration Template 2
                chkUseInAssetDeclaration.Checked = False
                If CInt(lvMasterList.SelectedItems(0).Tag) = clsCommon_Master.enCommonMaster.RELATIONS Then
                    chkUseInAssetDeclaration.Visible = True
                Else
                    chkUseInAssetDeclaration.Visible = False
                End If
                'Hemant (06 Oct 2018) -- End

                'Gajanan [13-Nov-2020] -- Start
                'Final Recruitment UAT v1 Changes.
                If CInt(lvMasterList.SelectedItems(0).Tag) = clsCommon_Master.enCommonMaster.VACANCY_MASTER AndAlso ConfigParameter._Object._AllowToSyncJobWithVacancyMaster Then
                    btnSave.Enabled = False
                    btnDelete.Enabled = False
                Else
                    btnSave.Enabled = True
                    btnDelete.Enabled = True
                End If
                'Gajanan [13-Nov-2020] -- END

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMasterList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvMasterListInfo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvMasterListInfo.SelectedIndexChanged
        Try
            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            mdtOldAllocation = Nothing
            mdtAllocation = Nothing
            'Sohail (14 Nov 2019) -- End
            If lvMasterListInfo.SelectedItems.Count > 0 Then
                lvMasterListInfo.Enabled = True
                menAction = enAction.EDIT_ONE
                mintMasterUnkid = CInt(lvMasterListInfo.SelectedItems(0).Tag)
                objCommonMaster._Masterunkid = mintMasterUnkid
                Call GetValue()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMasterListInfo_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Private Sub lnkAdvanceFilter_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAdvanceFilter.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            If mdtAllocation IsNot Nothing Then
                If mdtAllocation.Columns.Contains("masterunkid") = True Then
                    mdtAllocation.Columns("masterunkid").ColumnName = "UnkId"
                End If
                If mdtAllocation.Columns.Contains("advancefilterallocationid") = True Then
                    mdtAllocation.Columns("advancefilterallocationid").ColumnName = "GroupId"
                End If
                If mdtAllocation.Columns.Contains("advancefilterallocationname") = True Then
                    mdtAllocation.Columns("advancefilterallocationname").ColumnName = "GroupName"
                End If
                If mdtAllocation.Columns.Contains("advancefilterallocationtranunkid") = True Then
                    mdtAllocation.Columns("advancefilterallocationtranunkid").ColumnName = "Id"
                End If
                If mdtAllocation.Columns.Contains("advancefilterallocationtranname") = True Then
                    mdtAllocation.Columns("advancefilterallocationtranname").ColumnName = "Name"
                End If
                frm._DataTable = mdtAllocation
            End If
            frm._ShowSkill = True
            frm.ShowDialog()
            Dim mstrAdvanceFilter As String = frm._GetFilterString
            mdtAllocation = frm._DataTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvanceFilter_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (14 Nov 2019) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal intMasterTypeId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintMasterUnkid = intUnkId
            mintMasterTypeId = intMasterTypeId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbMasterList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMasterList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbMasterListInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMasterListInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbMasterList.Text = Language._Object.getCaption(Me.gbMasterList.Name, Me.gbMasterList.Text)
            Me.gbMasterListInfo.Text = Language._Object.getCaption(Me.gbMasterListInfo.Name, Me.gbMasterListInfo.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
            Me.lblAlias.Text = Language._Object.getCaption(Me.lblAlias.Name, Me.lblAlias.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhAlias.Text = Language._Object.getCaption(CStr(Me.colhAlias.Tag), Me.colhAlias.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhMasterList.Text = Language._Object.getCaption(CStr(Me.colhMasterList.Tag), Me.colhMasterList.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
	    Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.Name, Me.lblLevel.Text)
	    Me.colhQlevel.Text = Language._Object.getCaption(CStr(Me.colhQlevel.Tag), Me.colhQlevel.Text)
 	    Me.lblLevelDesc.Text = Language._Object.getCaption(Me.lblLevelDesc.Name, Me.lblLevelDesc.Text)
	    Me.rdCareerDevelopment.Text = Language._Object.getCaption(Me.rdCareerDevelopment.Name, Me.rdCareerDevelopment.Text)
	    Me.rdJobCapability.Text = Language._Object.getCaption(Me.rdJobCapability.Name, Me.rdJobCapability.Text)
	    Me.chkAllowedinReferee.Text = Language._Object.getCaption(Me.chkAllowedinReferee.Name, Me.chkAllowedinReferee.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Code cannot be blank. Code is required information.")
            Language.setMessage(mstrModuleName, 2, "Name cannot be blank. Name is required information.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Record?")
	    Language.setMessage(mstrModuleName, 4, "Please Select atleast one course type.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class