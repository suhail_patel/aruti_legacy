﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmCourseAttendance_AddEdit
#Region "Private Variable"
    Private ReadOnly mstrModuleName As String = "frmCourseAttendance_AddEdit"
    Private objTrainingAttendanceList As clsTraining_Attendance_Master
    Private objTrainingAttendanceTran As clsTraining_Attendance_Tran
    Private objTrainingSchedule As clsTraining_Scheduling
    Private objEnrollment As clsTraining_Enrollment_Tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mblnCancel As Boolean = True
    Private mdtTran As DataTable

    Private m_DataSource As DataTable
    Dim m_Dataview As New DataView

    Private mintAttendanceUnkid As Integer = -1
    Private mintEnrollmentUnkid As Integer = -1
    
#End Region

#Region "Display Dialog"
    Public Function displayDialog(ByRef intUnkid As Integer, ByVal eAction As enAction, Optional ByVal intEnrollmentUnkid As Integer = -1) As Boolean
        Try
            mintAttendanceUnkid = intUnkid
            menAction = eAction
            mintEnrollmentUnkid = intEnrollmentUnkid

            Me.ShowDialog()
            intUnkid = mintAttendanceUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Private Methods"

    Private Sub SetColor()
        Try
            txtCourseTitle.BackColor = GUI.ColorOptional
            txtStartDateTime.BackColor = GUI.ColorOptional
            txtEndDateTime.BackColor = GUI.ColorOptional
            txtRemark.BackColor = GUI.ColorOptional
            With dgvAttendanceSheet
                .Columns(colEmployeeName.Index).ReadOnly = True
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            If menAction = enAction.EDIT_ONE Then
                objTrainingAttendanceList._trainingschedulingunkid = objTrainingAttendanceList._trainingschedulingunkid
            Else
                objTrainingAttendanceList._trainingschedulingunkid = objEnrollment._Trainingschedulingunkid
            End If
            objTrainingAttendanceList._Attendance_Date = dtpAttendanceDate.Value
            objTrainingAttendanceList._Remark = txtRemark.Text
            If mintAttendanceUnkid = -1 Then
                objTrainingAttendanceList._Userunkid = 1
                objTrainingAttendanceList._Voiduserunkid = -1
                objTrainingAttendanceList._Voiddatetime = Nothing
                objTrainingAttendanceList._Isvoid = False
            Else
                objTrainingAttendanceList._Userunkid = objTrainingAttendanceList._Userunkid
                objTrainingAttendanceList._Voiduserunkid = objTrainingAttendanceList._Voiduserunkid
                objTrainingAttendanceList._Voiddatetime = objTrainingAttendanceList._Voiddatetime
                objTrainingAttendanceList._Isvoid = objTrainingAttendanceList._Isvoid
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            objTrainingAttendanceList._Attendanceunkid = mintAttendanceUnkid
            objTrainingSchedule._Trainingschedulingunkid = objTrainingAttendanceList._trainingschedulingunkid
            If Not (objTrainingAttendanceList._Attendance_Date = Nothing) Then
                dtpAttendanceDate.Value = objTrainingAttendanceList._Attendance_Date
            End If
            txtCourseTitle.Text = objTrainingSchedule._Course_Title
            txtStartDateTime.Text = CStr(objTrainingSchedule._Start_Date.ToShortDateString)
            txtEndDateTime.Text = CStr(objTrainingSchedule._End_Date.ToShortDateString)
            txtRemark.Text = objTrainingAttendanceList._Remark

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try

    End Sub

    Private Sub FillInfo()
        Dim dsList As New DataSet
        objTrainingAttendanceList = New clsTraining_Attendance_Master
        Try
            txtCourseTitle.Text = objTrainingSchedule._Course_Title
            txtStartDateTime.Text = CStr(objTrainingSchedule._Start_Date.ToShortDateString)
            txtEndDateTime.Text = CStr(objTrainingSchedule._End_Date.ToShortDateString)
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objTrainingAttendanceList.Get_Employee_List(CInt(objEnrollment._Trainingschedulingunkid), "EmployeeList")

            dsList = objTrainingAttendanceList.Get_Employee_List(FinancialYear._Object._DatabaseName, _
                                                                 User._Object._Userunkid, _
                                                                 FinancialYear._Object._YearUnkid, _
                                                                 Company._Object._Companyunkid, _
                                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                 ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                 ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                 CInt(objEnrollment._Trainingschedulingunkid), "EmployeeList")
            'S.SANDEEP [04 JUN 2015] -- END
            m_Dataview = New DataView(dsList.Tables("EmployeeList"))

            dgvAttendanceSheet.AutoGenerateColumns = False
            dgvAttendanceSheet.DataSource = m_Dataview

            objEmployeeUnkid.DataPropertyName = "employeeunkid"
            colEmployeeName.DataPropertyName = "EmpName"

            dgvAttendanceSheet.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillInfo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillTran()
        Dim dsList As DataSet = Nothing
        Dim dtRow As DataRow = Nothing
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objTrainingAttendanceList.GetEmpAttendanceList(CInt(objTrainingAttendanceList._Attendanceunkid), "EmpAttendanceList")
            dsList = objTrainingAttendanceList.GetEmpAttendanceList(FinancialYear._Object._DatabaseName, _
                                                                    User._Object._Userunkid, _
                                                                    FinancialYear._Object._YearUnkid, _
                                                                    Company._Object._Companyunkid, _
                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                    ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                    ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                    CInt(objTrainingAttendanceList._Attendanceunkid), "EmpAttendanceList")
            'S.SANDEEP [04 JUN 2015] -- END


            Dim dsOtherEmp As New DataSet

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsOtherEmp = objTrainingAttendanceList.Get_Employee_List_For_Edit(CInt(objTrainingAttendanceList._trainingschedulingunkid), CInt(objTrainingAttendanceList._Attendanceunkid), "Emp")

            dsOtherEmp = objTrainingAttendanceList.Get_Employee_List_For_Edit(FinancialYear._Object._DatabaseName, _
                                                                              User._Object._Userunkid, _
                                                                              FinancialYear._Object._YearUnkid, _
                                                                              Company._Object._Companyunkid, _
                                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                              ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                              CInt(objTrainingAttendanceList._trainingschedulingunkid), CInt(objTrainingAttendanceList._Attendanceunkid), "Emp")
            'S.SANDEEP [04 JUN 2015] -- END

            If dsOtherEmp.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsOtherEmp.Tables(0).Rows
                    dtRow = dsList.Tables(0).NewRow
                    With dtRow
                        .Item("attendancetranunkid") = -1
                        .Item("EmpName") = dRow.Item("EmpName")
                        .Item("employeeunkid") = dRow.Item("employeeunkid")
                        .Item("ispresent") = 0
                        .Item("isabsent") = 0
                        .Item("remark") = ""
                    End With
                    dsList.Tables(0).Rows.Add(dtRow)
                Next
            End If

            m_Dataview = New DataView(dsList.Tables("EmpAttendanceList"))

            dgvAttendanceSheet.AutoGenerateColumns = False
            dgvAttendanceSheet.DataSource = m_Dataview

            objAttendanceTranunkid.DataPropertyName = "attendancetranunkid"
            objAttendanceUnkid.DataPropertyName = "attendanceunkid"
            objEmployeeUnkid.DataPropertyName = "employeeunkid"
            colEmployeeName.DataPropertyName = "EmpName"
            colPresent.DataPropertyName = "ispresent"
            colAbsent.DataPropertyName = "isabsent"
            colRemark.DataPropertyName = "remark"

            dgvAttendanceSheet.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillTran", mstrModuleName)
        End Try
    End Sub

    Private Sub FillDatatable()
        Dim dtRow As DataRow = Nothing
        Dim dsList As DataSet = Nothing

        Try
            mdtTran.Rows.Clear()
            For i = 0 To dgvAttendanceSheet.Rows.Count - 1
                dtRow = mdtTran.NewRow
                With dgvAttendanceSheet.Rows(i)
                    If menAction = enAction.EDIT_ONE Then
                        dtRow.Item("attendancetranunkid") = CInt(.Cells(objAttendanceTranunkid.Index).Value)
                        dtRow.Item("attendanceunkid") = mintAttendanceUnkid
                    Else
                        dtRow.Item("attendancetranunkid") = -1
                        dtRow.Item("attendanceunkid") = -1
                    End If
                    dtRow.Item("employeeunkid") = CInt(.Cells(objEmployeeUnkid.Index).Value)
                    dtRow.Item("ispresent") = CBool(.Cells(colPresent.Index).Value)
                    dtRow.Item("isabsent") = CBool(.Cells(colAbsent.Index).Value)
                    dtRow.Item("remark") = CStr(.Cells(colRemark.Index).Value)
                End With
                mdtTran.Rows.Add(dtRow)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDataTable", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If dtpAttendanceDate.Value.Date > objTrainingSchedule._End_Date.Date Then 'Sohail (01 Dec 2010)
                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Attendance Date must less than End Date.", enMsgBoxStyle.Information))
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Attendance Date must less than End Date."), enMsgBoxStyle.Information)
                'S.SANDEEP [ 20 AUG 2011 ] -- END 
                dtpAttendanceDate.Focus()
                Return False
            End If

            If dtpAttendanceDate.Value.Date < objTrainingSchedule._Start_Date.Date Then 'Sohail (01 Dec 2010)
                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Attendance Date must greater than Start Date.", enMsgBoxStyle.Information)) 'Sohail (01 Dec 2010)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Attendance Date must greater than Start Date."), enMsgBoxStyle.Information)
                'S.SANDEEP [ 20 AUG 2011 ] -- END 
                dtpAttendanceDate.Focus()
                Return False
            End If

            'Sohail (01 Dec 2010) -- Start
            Dim blnChecked As Boolean = False
            For i As Integer = 0 To dgvAttendanceSheet.Rows.Count - 1
                If CBool(dgvAttendanceSheet.Rows(i).Cells(colPresent.Index).Value) = True Then
                    blnChecked = True
                    Exit For
                ElseIf CBool(dgvAttendanceSheet.Rows(i).Cells(colAbsent.Index).Value) = True Then
                    blnChecked = True
                    Exit For
                End If
            Next
            If blnChecked = False Then
                'S.SANDEEP [ 20 AUG 2011 ] -- START
                'ENHANCEMENT : LANGUAGES IMPLEMENTATION
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check Present or Absent for atleast one Employee.", enMsgBoxStyle.Information)) 'Sohail (01 Dec 2010)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check Present or Absent for atleast one Employee."), enMsgBoxStyle.Information)
                'S.SANDEEP [ 20 AUG 2011 ] -- END
                dtpAttendanceDate.Focus()
                Return False
            End If
            'Sohail (01 Dec 2010) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try

        Return True
    End Function

    'Sandeep [ 23 Oct 2010 ] -- Start
    Private Sub GlobalOperation(ByVal blnIsPresent As Boolean, ByVal blnOperation As Boolean)
        Try
            If blnIsPresent = True Then
                For i As Integer = 0 To dgvAttendanceSheet.RowCount - 1
                    dgvAttendanceSheet.Rows(i).Cells(colPresent.Index).Value = blnOperation
                Next
            Else
                For i As Integer = 0 To dgvAttendanceSheet.RowCount - 1
                    dgvAttendanceSheet.Rows(i).Cells(colAbsent.Index).Value = blnOperation
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GlobalOperation", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 23 Oct 2010 ] -- End 

#End Region

#Region "Form's Events"

    Private Sub frmCourseAttendance_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objTrainingSchedule = New clsTraining_Scheduling
        objEnrollment = New clsTraining_Enrollment_Tran
        objTrainingAttendanceTran = New clsTraining_Attendance_Tran
        objTrainingAttendanceList = New clsTraining_Attendance_Master

        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            objEnrollment._Trainingenrolltranunkid = mintEnrollmentUnkid
            objTrainingSchedule._Trainingschedulingunkid = objEnrollment._Trainingschedulingunkid
            Call SetColor()

            Call FillInfo()

            If menAction = enAction.EDIT_ONE Then
                objTrainingAttendanceList._Attendanceunkid = mintAttendanceUnkid
                Call FillTran()
            End If

            objTrainingAttendanceTran._AttendanceUnkid = mintAttendanceUnkid
            mdtTran = objTrainingAttendanceTran._DataList

            Call GetValue()

            dtpAttendanceDate.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCourseAttendance_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCourseAttendance_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.Control = True And e.KeyCode = Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmCourseAttendance_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTraining_Attendance_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsTraining_Attendance_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region "Buttons"
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValid() = False Then Exit Sub

            Call SetValue()

            Call FillDatatable()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objTrainingAttendanceList._FormName = mstrModuleName
            objTrainingAttendanceList._LoginEmployeeunkid = 0
            objTrainingAttendanceList._ClientIP = getIP()
            objTrainingAttendanceList._HostName = getHostName()
            objTrainingAttendanceList._FromWeb = False
            objTrainingAttendanceList._AuditUserId = User._Object._Userunkid
objTrainingAttendanceList._CompanyUnkid = Company._Object._Companyunkid
            objTrainingAttendanceList._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objTrainingAttendanceList.Update(mdtTran)
            Else
                blnFlag = objTrainingAttendanceList.Insert(mdtTran)
            End If

            If blnFlag = False And objTrainingAttendanceList._Message <> "" Then
                eZeeMsgBox.Show(objTrainingAttendanceList._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objTrainingAttendanceList = Nothing
                    objTrainingAttendanceList = New clsTraining_Attendance_Master
                    Call GetValue()
                Else
                    mintAttendanceUnkid = objTrainingAttendanceList._Attendanceunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Controls"

    Private Sub dgvAttendanceSheet_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAttendanceSheet.CellClick
        
    End Sub

    Private Sub dgvAttendanceSheet_CellMouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvAttendanceSheet.CellMouseClick

    End Sub

    Private Sub dgvAttendanceSheet_CellMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvAttendanceSheet.CellMouseUp
        Try
            Dim i As Integer
            i = e.RowIndex
            
            If e.RowIndex < 0 Then Exit Sub
            If e.ColumnIndex = colAbsent.Index Or e.ColumnIndex = colPresent.Index Then

                If dgvAttendanceSheet.Rows(i).Cells(colPresent.Index).Selected = True Then
                    dgvAttendanceSheet.Rows(i).Cells(colPresent.Index).Value = True
                    dgvAttendanceSheet.Rows(i).Cells(colAbsent.Index).Value = 0
                    dgvAttendanceSheet.Rows(i).Cells(colAbsent.Index).Selected = True
                    dgvAttendanceSheet.Rows(i).Cells(colPresent.Index).Selected = True
                ElseIf dgvAttendanceSheet.Rows(i).Cells(colAbsent.Index).Selected = True Then
                    dgvAttendanceSheet.Rows(i).Cells(colAbsent.Index).Value = True
                    dgvAttendanceSheet.Rows(i).Cells(colPresent.Index).Value = 0
                    dgvAttendanceSheet.Rows(i).Cells(colPresent.Index).Selected = True
                    dgvAttendanceSheet.Rows(i).Cells(colAbsent.Index).Selected = True
               
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAttendanceSheet_CellMouseUp", mstrModuleName)
        End Try
    End Sub

#End Region

    'Sandeep [ 23 Oct 2010 ] -- Start
    Private Sub chkMarkAllAbsent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMarkAllAbsent.CheckedChanged
        Try
            RemoveHandler chkMarkAllPresent.CheckedChanged, AddressOf chkMarkAllPresent_CheckedChanged
            chkMarkAllPresent.CheckState = CheckState.Unchecked
            Call GlobalOperation(True, CBool(chkMarkAllPresent.CheckState))
            AddHandler chkMarkAllPresent.CheckedChanged, AddressOf chkMarkAllPresent_CheckedChanged

            Call GlobalOperation(False, CBool(chkMarkAllAbsent.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkMarkAllAbsent_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkMarkAllPresent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMarkAllPresent.CheckedChanged
        Try

            RemoveHandler chkMarkAllAbsent.CheckedChanged, AddressOf chkMarkAllAbsent_CheckedChanged
            chkMarkAllAbsent.CheckState = CheckState.Unchecked
            Call GlobalOperation(False, CBool(chkMarkAllAbsent.CheckState))
            AddHandler chkMarkAllAbsent.CheckedChanged, AddressOf chkMarkAllAbsent_CheckedChanged

            Call GlobalOperation(True, CBool(chkMarkAllPresent.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkMarkAllPresent_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 23 Oct 2010 ] -- End 

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbAttendanceInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAttendanceInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbCourseInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbCourseInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbAttendanceInfo.Text = Language._Object.getCaption(Me.gbAttendanceInfo.Name, Me.gbAttendanceInfo.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbCourseInfo.Text = Language._Object.getCaption(Me.gbCourseInfo.Name, Me.gbCourseInfo.Text)
			Me.lblAttendanceDate.Text = Language._Object.getCaption(Me.lblAttendanceDate.Name, Me.lblAttendanceDate.Text)
			Me.lblTraningCourse.Text = Language._Object.getCaption(Me.lblTraningCourse.Name, Me.lblTraningCourse.Text)
			Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
			Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.colEmployeeName.HeaderText = Language._Object.getCaption(Me.colEmployeeName.Name, Me.colEmployeeName.HeaderText)
			Me.colPresent.HeaderText = Language._Object.getCaption(Me.colPresent.Name, Me.colPresent.HeaderText)
			Me.colAbsent.HeaderText = Language._Object.getCaption(Me.colAbsent.Name, Me.colAbsent.HeaderText)
			Me.colRemark.HeaderText = Language._Object.getCaption(Me.colRemark.Name, Me.colRemark.HeaderText)
			Me.chkMarkAllAbsent.Text = Language._Object.getCaption(Me.chkMarkAllAbsent.Name, Me.chkMarkAllAbsent.Text)
			Me.chkMarkAllPresent.Text = Language._Object.getCaption(Me.chkMarkAllPresent.Name, Me.chkMarkAllPresent.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Attendance Date must less than End Date.")
			Language.setMessage(mstrModuleName, 2, "Attendance Date must greater than Start Date.")
			Language.setMessage(mstrModuleName, 3, "Please check Present or Absent for atleast one Employee.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class