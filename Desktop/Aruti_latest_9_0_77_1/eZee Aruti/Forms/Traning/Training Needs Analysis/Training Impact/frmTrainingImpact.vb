﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

#End Region

Public Class frmTrainingImpact

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmTrainingImpact"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objImpactMst As clshrtnatraining_impact_master
    Private objImpactKRA As clshrtnatraining_impact_kra
    Private objImpactDevelopment As clshrtnatraining_impact_development
    Private objImpactObjective As clshrtnatraining_impact_objective
    Private objImpactFeedBack As clshrtnatraining_impact_feedback
    Private mdtItemAKPI As DataSet
    Private mdtItemADevelopmentGAP As DataSet
    Private mdtItemBObjective As DataSet
    Private mdtImpactFeedBack As DataSet
    Private mintImpactUnkId As Integer
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintImpactUnkId = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintImpactUnkId

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Function(s) & Method(s) "

    Public Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try

            Dim objCourse As New clsTraining_Scheduling
            dsList = objCourse.getComboList("List", True, -1, True)
            cboCourse.DisplayMember = "Name"
            cboCourse.ValueMember = "Id"
            cboCourse.DataSource = dsList.Tables(0)

            dsList = Nothing
            Dim objEmployee As New clsEmployee_Master
            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (23 Nov 2012) -- End
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = dsList.Tables(0)

            cboLineManager.DisplayMember = "employeename"
            cboLineManager.ValueMember = "employeeunkid"
            cboLineManager.DataSource = dsList.Tables(0).Copy()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub FillQuestion()
        Dim dsList As DataSet = Nothing
        Try
            Dim objFeedBackItem As New clshrtnafdbk_item_master
            dsList = objFeedBackItem.GetList("Question", True, True)
            cboQuestion.DisplayMember = "name"
            cboQuestion.ValueMember = "fdbkitemunkid"

            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("fdbkitemunkid") = 0
            drRow("name") = Language.getMessage(mstrModuleName, 7, "Select")
            drRow("resultgroupunkid") = 0
            dsList.Tables(0).Rows.InsertAt(drRow, 0)

            cboQuestion.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillQuestion", mstrModuleName)
        End Try
    End Sub

    Public Sub SetPartACaption()
        Try
            If txtStartDate.Tag Is Nothing Then Exit Sub

            pnlPartA.Visible = True
            If CInt(txtStartDate.Tag) = enCourseType.Job_Capability Then

                LblItemA.Text = Language.getMessage(mstrModuleName, 1, "Item 1(a):")
                LblItemACaption.Text = Language.getMessage(mstrModuleName, 2, "Job Capability or Training Related Performance Gap identified during Continuous Performance Assessment or Appraisal Process.")

                lblItemAKPI.Text = Language.getMessage(mstrModuleName, 3, "Key Performance Indicator or Key Results Area")
                lblItemAJobCapability.Text = Language.getMessage(mstrModuleName, 4, "Identified Job Capability or Training Related Performance Deficiency/Gap")
                colhItemAKPI.Text = Language.getMessage(mstrModuleName, 3, "Key Performance Indicator or Key Results Area")
                colhItemAJobCapability.Text = Language.getMessage(mstrModuleName, 4, "Identified Job Capability or Training Related Performance Deficiency/Gap")

                LblItemB.Text = Language.getMessage(mstrModuleName, 5, "Item 1(b):")
                LblItemBCaption.Text = Language.getMessage(mstrModuleName, 6, "Training/Programme Objective(s) or Expected Outcomes after attending the training: This should address Job Capability or Training Related Performance Gap Identified on Item 1(a). (Your expectation on what the staff will be able to do at workplace after attending the training/programme).")

                lblItemBKPI.Text = Language.getMessage(mstrModuleName, 3, "Key Performance Indicator or Key Results Area")
                lblItemBTrainingObjective.Text = Language.getMessage(mstrModuleName, 8, "Training Objective")
                colhItemBTrainingObjective.Text = Language.getMessage(mstrModuleName, 8, "Training Objective")
                colhPartAItemBKPI.Text = Language.getMessage(mstrModuleName, 3, "Key Performance Indicator or Key Results Area")


            ElseIf CInt(txtStartDate.Tag) = enCourseType.Career_Development Then

                LblItemA.Text = Language.getMessage(mstrModuleName, 9, "Item 2(a):")
                LblItemACaption.Text = Language.getMessage(mstrModuleName, 10, "Career Development Need Identified during Continuous Performance Assessment or Appraisal Process.")

                lblItemAKPI.Text = Language.getMessage(mstrModuleName, 11, "Key Performance Indicator or Key Results Area for senior position.")
                lblItemAJobCapability.Text = Language.getMessage(mstrModuleName, 12, "Identified Career Development Gap")
                colhItemAKPI.Text = Language.getMessage(mstrModuleName, 11, "Key Performance Indicator or Key Results Area for senior position.")
                colhItemAJobCapability.Text = Language.getMessage(mstrModuleName, 12, "Identified Career Development Gap")



                LblItemB.Text = Language.getMessage(mstrModuleName, 13, "Item 2(b):")
                LblItemBCaption.Text = Language.getMessage(mstrModuleName, 14, "Training/Programme Objective(s) or Expected Outcome after attending the training: This should address the identified Career Development Gap Identified on Item 2(a). (Your expectation on what the staff needs to have for preparation for immediate higher responsibilities or senior position).")

                lblItemBKPI.Text = Language.getMessage(mstrModuleName, 3, "Key Performance Indicator or Key Results Area")
                lblItemBTrainingObjective.Text = Language.getMessage(mstrModuleName, 15, "Training/Programme Objective")
                colhItemBTrainingObjective.Text = Language.getMessage(mstrModuleName, 15, "Training/Programme Objective")
                colhPartAItemBKPI.Text = Language.getMessage(mstrModuleName, 3, "Key Performance Indicator or Key Results Area")

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetPartACaption", mstrModuleName)
        End Try
    End Sub

    Public Sub SetPartBCaption()
        Try
            pnlPartB.Visible = True
            lblPartBDesc.Text = Language.getMessage(mstrModuleName, 27, "Six (6) Months ago you attended the above mentioned training/programme. As part of level three training impact evaluation process you are requested to provide Self Assessment on Competence enhancement/Improvement in relation to the explanations provided on Part A item 1(b).")
            lblPartBJobCapabilities.Text = Language.getMessage(mstrModuleName, 28, "Job capabilities enhanced/Improved after attending the training/programme.")
            lblNametask.Text = Language.getMessage(mstrModuleName, 29, "Name or Describe tasks that you are able to perform competently to the required standards after attending the Training in relation to Items 1(a).")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetPartBCaption", mstrModuleName)
        End Try
    End Sub

    Public Sub SetPartCCaption()
        Try
            pnlPactC.Visible = True
            lblPartCDesc.Text = Language.getMessage(mstrModuleName, 30, "Six (6) Months ago a member of your staff attended the above training programme. It is expected that following his/her return you have been giving him/her support, coaching and guidance to facilitate the transfer of the learning into the workplace for enhanced competences in the prior identified Job capability/training related performance gap and career Development Needs. To that end as part of Level Three Training Impact Evaluation Process you are kindly requested to briefly answer the following questions and return the form to us.")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetPartCCaption", mstrModuleName)
        End Try
    End Sub

    Public Sub FillPartAItemAKPIList()
        Try

            lvItemAKPIList.Items.Clear()
            lvItemBKPIList.Items.Clear()
            If mdtItemAKPI IsNot Nothing AndAlso mdtItemAKPI.Tables(0).Rows.Count > 0 Then

                Dim lvItemA As ListViewItem
                Dim lvItemB As ListViewItem
                For Each dr As DataRow In mdtItemAKPI.Tables(0).Rows
                    If dr("AUD").ToString() = "D" Then Continue For

                    lvItemA = New ListViewItem
                    lvItemA.Text = dr("kra").ToString()
                    lvItemA.SubItems.Add(dr("GUID").ToString())
                    lvItemAKPIList.Items.Add(lvItemA)

                    lvItemB = New ListViewItem
                    lvItemB.Text = dr("kra").ToString()
                    lvItemB.SubItems.Add(dr("GUID").ToString())
                    lvItemBKPIList.Items.Add(lvItemB)

                Next

                If lvItemAKPIList.Items.Count > 7 Then
                    colhItemAKPI.Width = 342 - 18
                Else
                    colhItemAKPI.Width = 342
                End If

                If lvItemBKPIList.Items.Count > 7 Then
                    colhPartAItemBKPI.Width = 342 - 18
                Else
                    colhPartAItemBKPI.Width = 342
                End If


            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPartAItemAKPIList", mstrModuleName)
        End Try
    End Sub

    Public Sub FillPartBKPIList()
        Try

            lvPartBKRA.Items.Clear()
            If mdtItemAKPI IsNot Nothing AndAlso mdtItemAKPI.Tables(0).Rows.Count > 0 Then

                Dim lvItem As ListViewItem
                For Each dr As DataRow In mdtItemAKPI.Tables(0).Rows
                    If dr("AUD").ToString() = "D" Then Continue For
                    lvItem = New ListViewItem
                    lvItem.Text = dr("kra").ToString()
                    lvPartBKRA.Items.Add(lvItem)
                Next

                If lvPartBKRA.Items.Count > 18 Then
                    colhPartBKRA.Width = 250 - 18
                Else
                    colhPartBKRA.Width = 250
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPartBKPIList", mstrModuleName)
        End Try
    End Sub

    Public Sub FillPartAItemA_DeveoplmentGAPList()
        Try

            lvItemAJobCapability.Items.Clear()
            If mdtItemADevelopmentGAP IsNot Nothing AndAlso mdtItemADevelopmentGAP.Tables(0).Rows.Count > 0 Then

                Dim lvItem As ListViewItem
                For Each dr As DataRow In mdtItemADevelopmentGAP.Tables(0).Rows
                    If dr("AUD").ToString() = "D" Then Continue For
                    lvItem = New ListViewItem
                    lvItem.Text = dr("development_area").ToString()
                    lvItem.SubItems.Add(dr("GUID").ToString())
                    lvItemAJobCapability.Items.Add(lvItem)
                Next

                If lvItemAJobCapability.Items.Count > 7 Then
                    colhItemAJobCapability.Width = 375 - 18
                Else
                    colhItemAJobCapability.Width = 375
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPartAItemA_DeveoplmentGAPList", mstrModuleName)
        End Try
    End Sub

    Public Sub FillPartAItemB_ObjectiveList()
        Try
            lvItemBList.Items.Clear()
            If mdtItemBObjective IsNot Nothing AndAlso mdtItemBObjective.Tables(0).Rows.Count > 0 Then

                Dim lvItem As ListViewItem
                For Each dr As DataRow In mdtItemBObjective.Tables(0).Rows
                    If dr("AUD").ToString() = "D" Then Continue For
                    lvItem = New ListViewItem
                    lvItem.Text = dr("training_objective").ToString()
                    lvItem.SubItems.Add(dr("GUID").ToString())
                    lvItemBList.Items.Add(lvItem)
                Next

                If lvItemBList.Items.Count > 7 Then
                    colhItemBTrainingObjective.Width = 370 - 18
                Else
                    colhItemBTrainingObjective.Width = 370
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPartAItemB_ObjectiveList", mstrModuleName)
        End Try
    End Sub

    Public Sub FillFeedBackList()
        Try
            lvFeedBackList.Items.Clear()
            If mdtImpactFeedBack IsNot Nothing AndAlso mdtImpactFeedBack.Tables(0).Rows.Count > 0 Then

                Dim lvItem As ListViewItem
                For Each dr As DataRow In mdtImpactFeedBack.Tables(0).Rows
                    If dr("AUD").ToString() = "D" Then Continue For
                    lvItem = New ListViewItem
                    lvItem.Text = dr("question").ToString()
                    lvItem.SubItems.Add(dr("answer").ToString())
                    lvItem.SubItems.Add(dr("manager_remark").ToString())
                    lvItem.SubItems.Add(dr("questionunkid").ToString())
                    lvItem.SubItems.Add(dr("answerunkid").ToString())
                    lvItem.SubItems.Add(dr("GUID").ToString())
                    lvFeedBackList.Items.Add(lvItem)
                Next

                If lvFeedBackList.Items.Count > 7 Then
                    colhFeedBackComment.Width = 225 - 18
                Else
                    colhFeedBackComment.Width = 225
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillFeedBackList", mstrModuleName)
        End Try
    End Sub

    Public Sub GetValue()
        Try
            cboCourse.SelectedValue = objImpactMst._Schedulingid
            cboEmployee.SelectedValue = objImpactMst._Employeeunkid
            cboLineManager.SelectedValue = objImpactMst._Managerunkid
            txtPartBNameTask.Text = objImpactMst._Self_Remark
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Public Sub SetValue()
        Try
            objImpactMst._Schedulingid = CInt(cboCourse.SelectedValue)
            objImpactMst._Courseunkid = CInt(txtEnddate.Tag)
            objImpactMst._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objImpactMst._Managerunkid = CInt(cboLineManager.SelectedValue)
            objImpactMst._Self_Remark = txtPartBNameTask.Text.Trim
            objImpactMst._Userunkid = User._Object._Userunkid


            '======= START FOR KRA============

            If mdtItemAKPI.Tables(0).Rows.Count > 0 Then
                objImpactKRA._KRAList.Tables(0).Rows.Clear()
                For i As Integer = 0 To mdtItemAKPI.Tables(0).Rows.Count - 1

                    Dim drRow As DataRow = objImpactKRA._KRAList.Tables(0).NewRow
                    drRow("kra") = mdtItemAKPI.Tables(0).Rows(i)("kra").ToString()

                    If menAction <> enAction.EDIT_ONE Then
                        If mdtItemAKPI.Tables(0).Rows(i)("AUD").ToString() = "" Then
                            drRow("AUD") = "A"
                        Else
                            drRow("AUD") = mdtItemAKPI.Tables(0).Rows(i)("AUD").ToString()
                        End If
                        drRow("kraunkid") = -1
                        drRow("impactunkid") = -1
                    Else
                        drRow("kraunkid") = IIf(mdtItemAKPI.Tables(0).Rows(i)("kraunkid").ToString() = "", -1, mdtItemAKPI.Tables(0).Rows(i)("kraunkid"))
                        drRow("impactunkid") = IIf(mdtItemAKPI.Tables(0).Rows(i)("impactunkid").ToString() = "", -1, mdtItemAKPI.Tables(0).Rows(i)("impactunkid"))
                        drRow("AUD") = mdtItemAKPI.Tables(0).Rows(i)("AUD").ToString()
                        drRow("isvoid") = IIf(mdtItemAKPI.Tables(0).Rows(i)("isvoid").ToString() = "", False, mdtItemAKPI.Tables(0).Rows(i)("isvoid"))
                        drRow("voiduserunkid") = IIf(mdtItemAKPI.Tables(0).Rows(i)("voiduserunkid").ToString() = "", -1, mdtItemAKPI.Tables(0).Rows(i)("voiduserunkid"))
                        drRow("voiddatetime") = IIf(mdtItemAKPI.Tables(0).Rows(i)("voiddatetime").ToString() = "", DBNull.Value, mdtItemAKPI.Tables(0).Rows(i)("voiddatetime"))
                        drRow("voidreason") = IIf(mdtItemAKPI.Tables(0).Rows(i)("voidreason").ToString() = "", "", mdtItemAKPI.Tables(0).Rows(i)("voidreason"))
                    End If
                    drRow("GUID") = mdtItemAKPI.Tables(0).Rows(i)("GUID").ToString()
                    objImpactKRA._KRAList.Tables(0).Rows.Add(drRow)
                Next


            End If
            objImpactMst._dsKRAList = objImpactKRA._KRAList

            '======= END FOR KRA============


            '======= START FOR DEVELOPMENT============

            If mdtItemADevelopmentGAP.Tables(0).Rows.Count > 0 Then
                objImpactDevelopment._DevelopmentList.Tables(0).Rows.Clear()

                For i As Integer = 0 To mdtItemADevelopmentGAP.Tables(0).Rows.Count - 1

                    Dim drRow As DataRow = objImpactDevelopment._DevelopmentList.Tables(0).NewRow
                    drRow("development_area") = mdtItemADevelopmentGAP.Tables(0).Rows(i)("development_area").ToString()

                    If menAction <> enAction.EDIT_ONE Then
                        drRow("developmentunkid") = -1
                        drRow("impactunkid") = -1
                        If mdtItemADevelopmentGAP.Tables(0).Rows(i)("AUD").ToString() = "" Then
                            drRow("AUD") = "A"
                        Else
                            drRow("AUD") = mdtItemADevelopmentGAP.Tables(0).Rows(i)("AUD").ToString()
                        End If
                    Else
                        drRow("developmentunkid") = IIf(mdtItemADevelopmentGAP.Tables(0).Rows(i)("developmentunkid").ToString() = "", -1, mdtItemADevelopmentGAP.Tables(0).Rows(i)("developmentunkid"))
                        drRow("impactunkid") = IIf(mdtItemADevelopmentGAP.Tables(0).Rows(i)("impactunkid").ToString() = "", -1, mdtItemADevelopmentGAP.Tables(0).Rows(i)("impactunkid"))
                        drRow("AUD") = mdtItemADevelopmentGAP.Tables(0).Rows(i)("AUD").ToString()
                        drRow("isvoid") = IIf(mdtItemADevelopmentGAP.Tables(0).Rows(i)("isvoid").ToString() = "", False, mdtItemADevelopmentGAP.Tables(0).Rows(i)("isvoid"))
                        drRow("voiduserunkid") = IIf(mdtItemADevelopmentGAP.Tables(0).Rows(i)("voiduserunkid").ToString() = "", -1, mdtItemADevelopmentGAP.Tables(0).Rows(i)("voiduserunkid"))
                        drRow("voiddatetime") = IIf(mdtItemADevelopmentGAP.Tables(0).Rows(i)("voiddatetime").ToString() = "", DBNull.Value, mdtItemADevelopmentGAP.Tables(0).Rows(i)("voiddatetime"))
                        drRow("voidreason") = IIf(mdtItemADevelopmentGAP.Tables(0).Rows(i)("voidreason").ToString() = "", "", mdtItemADevelopmentGAP.Tables(0).Rows(i)("voidreason"))
                    End If
                    drRow("GUID") = mdtItemADevelopmentGAP.Tables(0).Rows(i)("GUID").ToString()
                    objImpactDevelopment._DevelopmentList.Tables(0).Rows.Add(drRow)
                Next


            End If
            objImpactMst._dsDevelopmentList = objImpactDevelopment._DevelopmentList

            '======= END FOR DEVELOPMENT============

            '======= START FOR OBJECTIVE============

            If mdtItemBObjective.Tables(0).Rows.Count > 0 Then
                objImpactObjective._ObjectiveList.Tables(0).Rows.Clear()

                For i As Integer = 0 To mdtItemBObjective.Tables(0).Rows.Count - 1

                    Dim drRow As DataRow = objImpactObjective._ObjectiveList.Tables(0).NewRow
                    drRow("training_objective") = mdtItemBObjective.Tables(0).Rows(i)("training_objective").ToString()

                    If menAction <> enAction.EDIT_ONE Then
                        drRow("objectiveunkid") = -1
                        drRow("impactunkid") = -1
                        If mdtItemBObjective.Tables(0).Rows(i)("AUD").ToString() = "" Then
                            drRow("AUD") = "A"
                        Else
                            drRow("AUD") = mdtItemBObjective.Tables(0).Rows(i)("AUD").ToString()
                        End If
                    Else
                        drRow("objectiveunkid") = IIf(mdtItemBObjective.Tables(0).Rows(i)("objectiveunkid").ToString() = "", -1, mdtItemBObjective.Tables(0).Rows(i)("objectiveunkid"))
                        drRow("impactunkid") = IIf(mdtItemBObjective.Tables(0).Rows(i)("impactunkid").ToString() = "", -1, mdtItemBObjective.Tables(0).Rows(i)("impactunkid"))
                        drRow("AUD") = mdtItemBObjective.Tables(0).Rows(i)("AUD").ToString()
                        drRow("isvoid") = IIf(mdtItemBObjective.Tables(0).Rows(i)("isvoid").ToString() = "", False, mdtItemBObjective.Tables(0).Rows(i)("isvoid"))
                        drRow("voiduserunkid") = IIf(mdtItemBObjective.Tables(0).Rows(i)("voiduserunkid").ToString() = "", -1, mdtItemBObjective.Tables(0).Rows(i)("voiduserunkid"))
                        drRow("voiddatetime") = IIf(mdtItemBObjective.Tables(0).Rows(i)("voiddatetime").ToString() = "", DBNull.Value, mdtItemBObjective.Tables(0).Rows(i)("voiddatetime"))
                        drRow("voidreason") = IIf(mdtItemBObjective.Tables(0).Rows(i)("voidreason").ToString() = "", "", mdtItemBObjective.Tables(0).Rows(i)("voidreason"))
                    End If
                    drRow("GUID") = mdtItemBObjective.Tables(0).Rows(i)("GUID").ToString()
                    objImpactObjective._ObjectiveList.Tables(0).Rows.Add(drRow)
                Next



            End If
            objImpactMst._dsObjectiveList = objImpactObjective._ObjectiveList

            '======= END FOR OBJECTIVE============


            '======= START FOR FEEDBACK============


            For i As Integer = 0 To mdtImpactFeedBack.Tables(0).Rows.Count - 1
                mdtImpactFeedBack.Tables(0).Rows(i)("feedbackunkid") = IIf(mdtImpactFeedBack.Tables(0).Rows(i)("feedbackunkid").ToString() = "", -1, mdtImpactFeedBack.Tables(0).Rows(i)("feedbackunkid"))
                mdtImpactFeedBack.Tables(0).Rows(i)("impactunkid") = IIf(mdtImpactFeedBack.Tables(0).Rows(i)("impactunkid").ToString() = "", -1, mdtImpactFeedBack.Tables(0).Rows(i)("impactunkid"))
                mdtImpactFeedBack.Tables(0).Rows(i)("AUD") = mdtImpactFeedBack.Tables(0).Rows(i)("AUD").ToString()
                mdtImpactFeedBack.Tables(0).Rows(i)("isvoid") = IIf(mdtImpactFeedBack.Tables(0).Rows(i)("isvoid").ToString() = "", False, mdtImpactFeedBack.Tables(0).Rows(i)("isvoid"))
                mdtImpactFeedBack.Tables(0).Rows(i)("voiduserunkid") = IIf(mdtImpactFeedBack.Tables(0).Rows(i)("voiduserunkid").ToString() = "", -1, mdtImpactFeedBack.Tables(0).Rows(i)("voiduserunkid"))
                mdtImpactFeedBack.Tables(0).Rows(i)("voiddatetime") = IIf(mdtImpactFeedBack.Tables(0).Rows(i)("voiddatetime").ToString() = "", DBNull.Value, mdtImpactFeedBack.Tables(0).Rows(i)("voiddatetime"))
                mdtImpactFeedBack.Tables(0).Rows(i)("voidreason") = IIf(mdtImpactFeedBack.Tables(0).Rows(i)("voidreason").ToString() = "", "", mdtImpactFeedBack.Tables(0).Rows(i)("voidreason"))
            Next

            objImpactMst._dsFeedBackList = mdtImpactFeedBack

            '======= END FOR FEEDBACK============

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Public Function Validation() As Boolean
        Try

            If CInt(cboCourse.SelectedValue) <= 0 Then

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Course is compulsory information.Please Select Course."))
                cboCourse.Select()
                Return False

            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Employee is compulsory information.Please Select Employee."))
                cboEmployee.Select()
                Return False

            ElseIf CInt(cboLineManager.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Line Manager/Supervisor is compulsory information.Please Select Line Manager/Supervisor."))
                cboLineManager.Select()
                Return False

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Form's Event(s) "

    Private Sub frmTrainingImpact_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            objImpactMst = New clshrtnatraining_impact_master
            objImpactKRA = New clshrtnatraining_impact_kra
            objImpactDevelopment = New clshrtnatraining_impact_development
            objImpactObjective = New clshrtnatraining_impact_objective
            objImpactFeedBack = New clshrtnatraining_impact_feedback
            FillCombo()

            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            btnFinalSave.Enabled = User._Object.Privilege._AllowToSave_CompleteLevelIIIEvaluation
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            If menAction = enAction.EDIT_ONE Then
                objImpactMst._Impactunkid = mintImpactUnkId
                mdtItemAKPI = objImpactMst._dsKRAList
                mdtItemADevelopmentGAP = objImpactMst._dsDevelopmentList
                mdtItemBObjective = objImpactMst._dsObjectiveList
                mdtImpactFeedBack = objImpactMst._dsFeedBackList

                cboCourse.Enabled = False
                objbtnSearchCourse.Enabled = False
                cboEmployee.Enabled = False
                objBtnSearchEmployee.Enabled = False
            Else
                mdtImpactFeedBack = objImpactFeedBack._FeedBackList
            End If

            GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTrainingImpact_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clshrtnatraining_impact_master.SetMessages()
            'clshrtnatraining_impact_kra.SetMessages()
            'clshrtnatraining_impact_development.SetMessages()
            'clshrtnatraining_impact_objective.SetMessages()
            'clshrtnatraining_impact_feedback.SetMessages()
            ',clshrtnatraining_impact_kra,clshrtnatraining_impact_development,clshrtnatraining_impact_objective,clshrtnatraining_impact_feedback

            objfrm._Other_ModuleNames = "clshrtnatraining_impact_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnSearchCourse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCourse.Click
        Dim frm As New frmCommonSearch
        Try
            If cboCourse.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboCourse.ValueMember
                    .DisplayMember = cboCourse.DisplayMember
                    .DataSource = CType(cboCourse.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cboCourse.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objBtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objBtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If cboEmployee.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboEmployee.ValueMember
                    .DisplayMember = cboEmployee.DisplayMember
                    .CodeMember = "employeecode"
                    .DataSource = CType(cboEmployee.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objBtnLineManager_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objBtnLineManager.Click
        Dim frm As New frmCommonSearch
        Try
            If cboLineManager.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboLineManager.ValueMember
                    .DisplayMember = cboLineManager.DisplayMember
                    .CodeMember = "employeecode"
                    .DataSource = CType(cboLineManager.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cboLineManager.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnLineManager_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try

    End Sub

    Private Sub objBtnSearchQuestion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objBtnSearchQuestion.Click
        Dim frm As New frmCommonSearch
        Try
            If cboQuestion.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboQuestion.ValueMember
                    .DisplayMember = cboQuestion.DisplayMember
                    .DataSource = CType(cboQuestion.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cboQuestion.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnSearchQuestion_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objBtnSearchAnswer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objBtnSearchAnswer.Click
        Dim frm As New frmCommonSearch
        Try
            If cboAnswer.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboAnswer.ValueMember
                    .DisplayMember = cboAnswer.DisplayMember
                    .DataSource = CType(cboAnswer.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cboAnswer.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objBtnSearchAnswer_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnAddItemAKPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddItemAKPI.Click
        Try
            If txtItemAKPI.Text.Length > 0 Then

                Dim dRow As DataRow() = mdtItemAKPI.Tables(0).Select("kra='" & txtItemAKPI.Text.Trim & "' AND AUD <> 'D'")
                If dRow.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "This KPI/KRA already exists in the list.Please define new KPI/KRA."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    txtItemAKPI.Select()
                    Exit Sub
                End If

                Dim drRow As DataRow = mdtItemAKPI.Tables(0).NewRow
                drRow("kra") = txtItemAKPI.Text.Trim
                drRow("AUD") = "A"
                drRow("GUID") = Guid.NewGuid.ToString()
                mdtItemAKPI.Tables(0).Rows.Add(drRow)

                FillPartAItemAKPIList()
                FillPartAItemB_ObjectiveList()
                txtItemAKPI.Clear()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddItemAKPI_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEditItemAKPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditItemAKPI.Click
        Try
            If lvItemAKPIList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If txtItemAKPI.Text.Trim.Length <= 0 Then
                Dim StrMessage As String = String.Empty
                StrMessage = lblItemAKPI.Text.ToString & Language.getMessage(mstrModuleName, 19, " cannot be blank. ") & lblItemAKPI.Text & Language.getMessage(mstrModuleName, 20, " is required information.")
                eZeeMsgBox.Show(StrMessage, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtItemAKPI.Select()
                Exit Sub
            End If

            Dim dRow As DataRow() = Nothing
            If lvItemAKPIList.SelectedItems(0).SubItems(objcolhItemAKPIGUID.Index).Text = "" Then
                dRow = mdtItemAKPI.Tables(0).Select("kra='" & txtItemAKPI.Text.Trim & "' AND AUD <> 'D' AND kra <> '" & lvItemAKPIList.SelectedItems(0).Text & "'")

            ElseIf lvItemAKPIList.SelectedItems(0).SubItems(objcolhItemAKPIGUID.Index).Text <> "" Then
                dRow = mdtItemAKPI.Tables(0).Select("kra='" & txtItemAKPI.Text.Trim & "' AND AUD <> 'D' AND GUID <> '" & lvItemAKPIList.SelectedItems(0).SubItems(objcolhItemAKPIGUID.Index).Text & "'")
            End If

            If dRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "This KPI/KRA already exists in the list.Please define new KPI/KRA."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtItemAKPI.Select()
                Exit Sub
            End If

            Dim drRow As DataRow() = mdtItemAKPI.Tables(0).Select("kra='" & lvItemAKPIList.SelectedItems(0).Text & "'")
            If drRow.Length > 0 Then
                drRow(0)("kra") = txtItemAKPI.Text.Trim
                drRow(0)("AUD") = "U"
                drRow(0).AcceptChanges()
            End If

            FillPartAItemAKPIList()
            txtItemAKPI.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditItemAKPI_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDeleteItemAKPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteItemAKPI.Click
        Try
            If lvItemAKPIList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Are you sure you want to delete this Item ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim drRow As DataRow() = mdtItemAKPI.Tables(0).Select("kra='" & lvItemAKPIList.SelectedItems(0).Text & "'")
                If drRow.Length > 0 Then

                    If menAction = enAction.EDIT_ONE Then
                        Dim frm As New frmReasonSelection
                        Dim mstrVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.TRAINING, mstrVoidReason)
                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        Else
                            drRow(0)("AUD") = "D"
                            drRow(0)("isvoid") = True
                            drRow(0)("voiduserunkid") = User._Object._Userunkid
                            drRow(0)("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drRow(0)("voidreason") = mstrVoidReason
                        End If
                        frm = Nothing
                    Else
                        drRow(0)("AUD") = "D"
                    End If
                    drRow(0).AcceptChanges()
                End If
                FillPartAItemAKPIList()
                txtItemAKPI.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteItemAKPI_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAddItemAGAP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddItemAGAP.Click
        Try
            If txtItemAJobCapability.Text.Length > 0 Then

                Dim dRow As DataRow() = mdtItemADevelopmentGAP.Tables(0).Select("development_area='" & txtItemAJobCapability.Text.Trim & "' AND AUD <> 'D'")
                If dRow.Length > 0 Then
                    Dim StrMessage As String = String.Empty
                    StrMessage = lblItemAJobCapability.Text.ToString & Language.getMessage(mstrModuleName, 22, "  already exists in the list.Please define new ") & lblItemAJobCapability.Text & "."
                    eZeeMsgBox.Show(StrMessage, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    txtItemAJobCapability.Select()
                    Exit Sub
                End If

                Dim drRow As DataRow = mdtItemADevelopmentGAP.Tables(0).NewRow
                drRow("development_area") = txtItemAJobCapability.Text.Trim
                drRow("AUD") = "A"
                drRow("GUID") = Guid.NewGuid.ToString()
                mdtItemADevelopmentGAP.Tables(0).Rows.Add(drRow)

                FillPartAItemA_DeveoplmentGAPList()
                txtItemAJobCapability.Clear()

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddItemAGAP_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEditItemAGAP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditItemAGAP.Click
        Try
            If lvItemAJobCapability.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If txtItemAJobCapability.Text.Trim.Length <= 0 Then
                Dim StrMessage As String = String.Empty
                StrMessage = lblItemAJobCapability.Text.ToString & Language.getMessage(mstrModuleName, 19, " cannot be blank. ") & lblItemAJobCapability.Text & Language.getMessage(mstrModuleName, 20, " is required information.")
                eZeeMsgBox.Show(StrMessage, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtItemAJobCapability.Select()
                Exit Sub
            End If

            Dim dRow As DataRow() = Nothing
            If lvItemAJobCapability.SelectedItems(0).SubItems(objcolhItemAJobCapGUID.Index).Text = "" Then
                dRow = mdtItemADevelopmentGAP.Tables(0).Select("development_area='" & txtItemAJobCapability.Text.Trim & "' AND AUD <> 'D' AND development_area <> '" & lvItemAJobCapability.SelectedItems(0).Text & "'")

            ElseIf lvItemAJobCapability.SelectedItems(0).SubItems(objcolhItemAJobCapGUID.Index).Text <> "" Then
                dRow = mdtItemADevelopmentGAP.Tables(0).Select("development_area='" & txtItemAJobCapability.Text.Trim & "' AND AUD <> 'D' AND GUID <> '" & lvItemAJobCapability.SelectedItems(0).SubItems(objcolhItemAJobCapGUID.Index).Text & "'")
            End If

            If dRow.Length > 0 Then
                Dim StrMessage As String = String.Empty
                StrMessage = lblItemAJobCapability.Text.ToString & Language.getMessage(mstrModuleName, 22, "  already exists in the list.Please define new ") & lblItemAJobCapability.Text & "."
                eZeeMsgBox.Show(StrMessage, CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtItemAJobCapability.Select()
                Exit Sub
            End If


            Dim drRow As DataRow() = mdtItemADevelopmentGAP.Tables(0).Select("development_area='" & lvItemAJobCapability.SelectedItems(0).Text & "'")
            If drRow.Length > 0 Then
                drRow(0)("development_area") = txtItemAJobCapability.Text.Trim
                drRow(0)("AUD") = "U"
                drRow(0).AcceptChanges()
            End If

            FillPartAItemA_DeveoplmentGAPList()
            txtItemAJobCapability.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditItemAGAP_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDeleteItemAGAP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteItemAGAP.Click
        Try
            If lvItemAJobCapability.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Are you sure you want to delete this Item ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim drRow As DataRow() = mdtItemADevelopmentGAP.Tables(0).Select("development_area='" & lvItemAJobCapability.SelectedItems(0).Text & "'")
                If drRow.Length > 0 Then
                    If menAction = enAction.EDIT_ONE Then
                        Dim frm As New frmReasonSelection
                        Dim mstrVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.TRAINING, mstrVoidReason)
                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        Else
                            drRow(0)("AUD") = "D"
                            drRow(0)("isvoid") = True
                            drRow(0)("voiduserunkid") = User._Object._Userunkid
                            drRow(0)("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drRow(0)("voidreason") = mstrVoidReason
                        End If
                        frm = Nothing
                    Else
                        drRow(0)("AUD") = "D"
                    End If
                    drRow(0).AcceptChanges()
                End If
                FillPartAItemA_DeveoplmentGAPList()
                txtItemAJobCapability.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteItemAGAP_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAddItemB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddItemB.Click
        Try

            If txtItemBKPI.Text.Trim.Length > 0 Or txtItemBTrainingObjective.Text.Trim.Length > 0 Then

                Dim dRow As DataRow()
                'Dim dRow As DataRow() = mdtItemBObjective.Tables(0).Select("kra='" & txtItemBKPI.Text.Trim & "' AND AUD <> 'D'")
                'If dRow.Length > 0 Then
                '    Dim StrMessage As String = String.Empty
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "This KPI/KRA is already exist in the list.Please define new KPI/KRA."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                '    txtItemBKPI.Select()
                '    Exit Sub
                'End If

                dRow = mdtItemBObjective.Tables(0).Select("training_objective='" & txtItemBTrainingObjective.Text.Trim & "' AND AUD <> 'D'")
                If dRow.Length > 0 Then
                    Dim StrMessage As String = String.Empty
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "This Training Objective already exists in the list.Please define new Training Objective."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    txtItemBTrainingObjective.Select()
                    Exit Sub
                End If

                Dim drRow As DataRow = mdtItemBObjective.Tables(0).NewRow
                drRow("training_objective") = txtItemBTrainingObjective.Text.Trim
                drRow("AUD") = "A"
                drRow("GUID") = Guid.NewGuid.ToString()
                mdtItemBObjective.Tables(0).Rows.Add(drRow)

                FillPartAItemB_ObjectiveList()
                txtItemBKPI.Clear()
                txtItemBTrainingObjective.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddItemB_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEditItemB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditItemB.Click
        Try
            If lvItemBList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If txtItemBTrainingObjective.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Training Objective cannot be blank.Training Objective are required information."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            Dim dRow As DataRow() = Nothing
            'If lvItemBList.SelectedItems(0).SubItems(objColhItemBGUID.Index).Text = "" Then
            '    dRow = mdtItemBObjective.Tables(0).Select("kra='" & txtItemBKPI.Text.Trim & "' AND AUD <> 'D' AND kra <> '" & lvItemBList.SelectedItems(0).Text & "'")

            'ElseIf lvItemBList.SelectedItems(0).SubItems(objColhItemBGUID.Index).Text <> "" Then
            '    dRow = mdtItemBObjective.Tables(0).Select("kra='" & txtItemBKPI.Text.Trim & "' AND AUD <> 'D' AND GUID <> '" & lvItemBList.SelectedItems(0).SubItems(objColhItemBGUID.Index).Text & "'")
            'End If

            'If dRow.Length > 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "This KPI/KRA is already exist in the list.Please define new KPI/KRA."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
            '    txtItemBKPI.Select()
            '    Exit Sub
            'End If

            dRow = Nothing
            If lvItemBList.SelectedItems(0).SubItems(objColhItemBGUID.Index).Text = "" Then
                dRow = mdtItemBObjective.Tables(0).Select("training_objective='" & txtItemBTrainingObjective.Text.Trim & "' AND AUD <> 'D' AND training_objective <> '" & lvItemBList.SelectedItems(0).SubItems(colhItemBTrainingObjective.Index).Text & "'")

            ElseIf lvItemBList.SelectedItems(0).SubItems(objColhItemBGUID.Index).Text <> "" Then
                dRow = mdtItemBObjective.Tables(0).Select("training_objective='" & txtItemBTrainingObjective.Text.Trim & "' AND AUD <> 'D' AND GUID <> '" & lvItemBList.SelectedItems(0).SubItems(objColhItemBGUID.Index).Text & "'")
            End If

            If dRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "This Training Objective already exists in the list.Please define new Training Objective."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtItemBTrainingObjective.Select()
                Exit Sub
            End If

            'Dim drRow As DataRow() = mdtItemBObjective.Tables(0).Select("kra='" & lvItemBList.SelectedItems(0).Text & "' AND training_objective='" & lvItemBList.SelectedItems(0).SubItems(colhItemBTrainingObjective.Index).Text & "'")
            Dim drRow As DataRow() = mdtItemBObjective.Tables(0).Select("training_objective='" & lvItemBList.SelectedItems(0).SubItems(colhItemBTrainingObjective.Index).Text & "' AND AUD <> 'D'")
            If drRow.Length > 0 Then
                'drRow(0)("kra") = txtItemBKPI.Text.Trim
                drRow(0)("training_objective") = txtItemBTrainingObjective.Text.Trim
                drRow(0)("AUD") = "U"
                drRow(0).AcceptChanges()
            End If
            FillPartAItemB_ObjectiveList()
            txtItemBKPI.Clear()
            txtItemBTrainingObjective.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditItemB_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDeleteItemB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteItemB.Click
        Try
            If lvItemBList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Are you sure you want to delete this Item ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'Dim drRow As DataRow() = mdtItemBObjective.Tables(0).Select("kra='" & lvItemBList.SelectedItems(0).Text & "' AND training_objective='" & lvItemBList.SelectedItems(0).SubItems(colhItemBTrainingObjective.Index).Text & "'")
                Dim drRow As DataRow() = mdtItemBObjective.Tables(0).Select("training_objective='" & lvItemBList.SelectedItems(0).SubItems(colhItemBTrainingObjective.Index).Text & "' AND AUD <> 'D'")
                If drRow.Length > 0 Then

                    If menAction = enAction.EDIT_ONE Then
                        Dim frm As New frmReasonSelection
                        Dim mstrVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.TRAINING, mstrVoidReason)
                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        Else
                            drRow(0)("AUD") = "D"
                            drRow(0)("isvoid") = True
                            drRow(0)("voiduserunkid") = User._Object._Userunkid
                            drRow(0)("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drRow(0)("voidreason") = mstrVoidReason
                        End If
                        frm = Nothing
                    Else
                        drRow(0)("AUD") = "D"
                    End If
                    drRow(0).AcceptChanges()
                End If
                FillPartAItemB_ObjectiveList()
                txtItemBKPI.Clear()
                txtItemBTrainingObjective.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteItemB_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAddFeedback_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddFeedback.Click
        Try
            If CInt(cboQuestion.SelectedValue) > 0 Then

                If CInt(cboAnswer.SelectedValue) <= 0 And cboAnswer.Visible = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Feedback Answer is compuslory information.Please Select Feedback Answer."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    cboAnswer.Select()
                    Exit Sub
                End If

                Dim dRow As DataRow() = mdtImpactFeedBack.Tables(0).Select("questionunkid=" & CInt(cboQuestion.SelectedValue) & " AND AUD <> 'D'")
                If dRow.Length > 0 Then
                    Dim StrMessage As String = String.Empty
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "This Feedback Question already exists in the list.Please define new Feedback Question."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    cboQuestion.Select()
                    Exit Sub
                End If

                Dim drRow As DataRow = mdtImpactFeedBack.Tables(0).NewRow
                drRow("questionunkid") = CInt(cboQuestion.SelectedValue)
                drRow("question") = cboQuestion.Text
                If cboAnswer.Visible Then
                    drRow("answerunkid") = CInt(cboAnswer.SelectedValue)
                    drRow("answer") = cboAnswer.Text
                Else
                    drRow("answerunkid") = -1
                    drRow("answer") = ""
                End If
                drRow("manager_remark") = txtFeedbackComment.Text.Trim
                drRow("AUD") = "A"
                drRow("GUID") = Guid.NewGuid.ToString()
                mdtImpactFeedBack.Tables(0).Rows.Add(drRow)

                FillFeedBackList()
                cboQuestion.SelectedIndex = 0
                cboAnswer.SelectedIndex = 0
                txtFeedbackComment.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddFeedback_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEditFeedBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditFeedBack.Click
        Try
            If lvFeedBackList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If

            If CInt(cboQuestion.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Feedback Question is Compulsory information.Please Select Feedback Question."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboQuestion.Select()
                Exit Sub
            End If

            If CInt(cboAnswer.SelectedValue) <= 0 And cboAnswer.Visible = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Feedback Answer is Compulsory information.Please Select Feedback Answer."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboAnswer.Select()
                Exit Sub
            End If

            Dim dRow As DataRow() = Nothing
            If lvFeedBackList.SelectedItems(0).SubItems(objcolhPartCGUID.Index).Text = "" Then
                dRow = mdtImpactFeedBack.Tables(0).Select("questionunkid=" & CInt(cboQuestion.SelectedValue) & " AND AUD <> 'D' AND questionunkid <> " & CInt(lvFeedBackList.SelectedItems(0).SubItems(objColhQuestionunkid.Index).Text))

            ElseIf lvFeedBackList.SelectedItems(0).SubItems(objcolhPartCGUID.Index).Text <> "" Then
                dRow = mdtImpactFeedBack.Tables(0).Select("questionunkid=" & CInt(cboQuestion.SelectedValue) & " AND AUD <> 'D' AND GUID <> '" & lvFeedBackList.SelectedItems(0).SubItems(objcolhPartCGUID.Index).Text & "'")
            End If

            If dRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "This Feedback Question already exists in the list.Please define new Feedback Question."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtItemBTrainingObjective.Select()
                Exit Sub
            End If

            Dim drRow As DataRow() = mdtImpactFeedBack.Tables(0).Select("questionunkid=" & CInt(lvFeedBackList.SelectedItems(0).SubItems(objColhQuestionunkid.Index).Text) & " AND AUD <> 'D'")
            If drRow.Length > 0 Then
                drRow(0)("questionunkid") = CInt(cboQuestion.SelectedValue)
                drRow(0)("question") = cboQuestion.Text.Trim
                If cboAnswer.Visible Then
                    drRow(0)("answerunkid") = CInt(cboAnswer.SelectedValue)
                    drRow(0)("answer") = cboAnswer.Text
                Else
                    drRow(0)("answerunkid") = -1
                    drRow(0)("answer") = ""
                End If
                drRow(0)("manager_remark") = txtFeedbackComment.Text.Trim
                drRow(0)("AUD") = "U"
                drRow(0).AcceptChanges()
            End If
            FillFeedBackList()
            cboQuestion.SelectedIndex = 0
            cboAnswer.SelectedIndex = 0
            txtFeedbackComment.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEditFeedBack_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDeleteFeedBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteFeedBack.Click
        Try
            If lvFeedBackList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Exit Sub
            End If
            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Are you sure you want to delete this Item ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim drRow As DataRow() = mdtImpactFeedBack.Tables(0).Select("question='" & lvFeedBackList.SelectedItems(0).SubItems(colhQuestion.Index).Text & "'")
                If drRow.Length > 0 Then
                    If menAction = enAction.EDIT_ONE Then
                        Dim frm As New frmReasonSelection
                        Dim mstrVoidReason As String = String.Empty
                        frm.displayDialog(enVoidCategoryType.TRAINING, mstrVoidReason)
                        If mstrVoidReason.Length <= 0 Then
                            Exit Sub
                        Else
                            drRow(0)("AUD") = "D"
                            drRow(0)("isvoid") = True
                            drRow(0)("voiduserunkid") = User._Object._Userunkid
                            drRow(0)("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drRow(0)("voidreason") = mstrVoidReason
                        End If
                        frm = Nothing
                    Else
                        drRow(0)("AUD") = "D"
                    End If
                    drRow(0).AcceptChanges()
                End If
                cboQuestion.SelectedIndex = 0
                cboAnswer.SelectedIndex = 0
                txtFeedbackComment.Clear()
                FillFeedBackList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDeleteFeedBack_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Validation() Then

                SetValue()
                objImpactMst._Iscomplete = False

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objImpactMst._FormName = mstrModuleName
                objImpactMst._LoginEmployeeunkid = 0
                objImpactMst._ClientIP = getIP()
                objImpactMst._HostName = getHostName()
                objImpactMst._FromWeb = False
                objImpactMst._AuditUserId = User._Object._Userunkid
objImpactMst._CompanyUnkid = Company._Object._Companyunkid
                objImpactMst._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If menAction = enAction.EDIT_ONE Then
                    blnFlag = objImpactMst.Update()
                Else
                    blnFlag = objImpactMst.Insert()
                End If

                If blnFlag = False And objImpactMst._Message <> "" Then
                    eZeeMsgBox.Show(objImpactMst._Message, enMsgBoxStyle.Information)
                End If

                If blnFlag Then
                    mblnCancel = False
                    If menAction <> enAction.EDIT_ONE Then
                        objImpactMst = Nothing
                        objImpactMst = New clshrtnatraining_impact_master
                        mdtItemAKPI.Tables(0).Rows.Clear()
                        mdtItemADevelopmentGAP.Tables(0).Rows.Clear()
                        mdtItemBObjective.Tables(0).Rows.Clear()
                        mdtImpactFeedBack.Tables(0).Rows.Clear()
                        Call GetValue()
                        cboCourse.Select()
                    Else
                        mintImpactUnkId = objImpactMst._Impactunkid
                        Me.Close()
                    End If
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinalSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Validation() Then

                SetValue()
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Are you sure you want to Finally save this Imapct feedback.After this you won't be allowed to EDIT this Impact feedback.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    objImpactMst._Iscomplete = False
                Else
                    objImpactMst._Iscomplete = True
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objImpactMst._FormName = mstrModuleName
                objImpactMst._LoginEmployeeunkid = 0
                objImpactMst._ClientIP = getIP()
                objImpactMst._HostName = getHostName()
                objImpactMst._FromWeb = False
                objImpactMst._AuditUserId = User._Object._Userunkid
objImpactMst._CompanyUnkid = Company._Object._Companyunkid
                objImpactMst._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END


                If menAction = enAction.EDIT_ONE Then
                    blnFlag = objImpactMst.Update()
                Else
                    blnFlag = objImpactMst.Insert()
                End If

                'S.SANDEEP [06-MAR-2017] -- START
                'ISSUE/ENHANCEMENT : Training Module Notification
                If ConfigParameter._Object._Ntf_TrainingLevelIII_EvalUserIds.Trim.Length > 0 Then
                    If blnFlag Then
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objImpactMst.SendNotification(CInt(cboCourse.SelectedValue), CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), enLogin_Mode.DESKTOP, User._Object._Userunkid, ConfigParameter._Object._Ntf_TrainingLevelIII_EvalUserIds, 0, mstrModuleName)
                        objImpactMst.SendNotification(CInt(cboCourse.SelectedValue), CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), enLogin_Mode.DESKTOP, User._Object._Userunkid, ConfigParameter._Object._Ntf_TrainingLevelIII_EvalUserIds, 0, mstrModuleName, Company._Object._Companyunkid)
                        'Sohail (30 Nov 2017) -- End
                    End If
                End If
                'S.SANDEEP [06-MAR-2017] -- END


                If blnFlag = False And objImpactMst._Message <> "" Then
                    eZeeMsgBox.Show(objImpactMst._Message, enMsgBoxStyle.Information)
                End If

                If blnFlag Then
                    mblnCancel = False
                    mintImpactUnkId = objImpactMst._Impactunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnFinalSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combo's Event(s) "

    Private Sub cboCourse_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCourse.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboCourse.SelectedValue) > 0 Then

                Dim drRow As DataRowView = CType(cboCourse.SelectedItem, DataRowView)

                If Not IsDBNull(drRow("start_date")) Then
                    txtStartDate.Text = CDate(drRow("start_date")).ToShortDateString
                End If

                If Not IsDBNull(drRow("end_date")) Then
                    txtEnddate.Text = CDate(drRow("end_date")).ToShortDateString
                End If

                If Not IsDBNull(drRow("coursetypeid")) Then
                    txtStartDate.Tag = CInt(drRow("coursetypeid"))
                End If

                If Not IsDBNull(drRow("courseunkid")) Then
                    txtEnddate.Tag = CInt(drRow("courseunkid"))
                End If


                SetPartACaption()
                SetPartBCaption()
                SetPartCCaption()

            Else
                txtEnddate.Tag = 0
                txtStartDate.Clear()
                txtEnddate.Clear()
                txtFeedbackComment.Clear()
                txtPartBNameTask.Clear()

                pnlPartA.Visible = False
                pnlPartB.Visible = False
                pnlPactC.Visible = False
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then

                If menAction <> enAction.EDIT_ONE Then

                    '============ START FOR PART A ITEM 1(A) KPI/KRA LIST
                    mdtItemAKPI = objImpactMst.GetKRAFromTrainingPriority(CInt(txtEnddate.Tag), CInt(cboEmployee.SelectedValue))
                    FillPartAItemAKPIList()
                    '============ END FOR PART A ITEM 1(A) KPI/KRA LIST

                    '============ START FOR PART A ITEM 1(A) OR ITEM 2(A) DEVELOPMENT GAP LIST
                    mdtItemADevelopmentGAP = objImpactMst.GetDevelopmentGAPFromAssessment(CInt(txtEnddate.Tag), CInt(cboEmployee.SelectedValue))
                    FillPartAItemA_DeveoplmentGAPList()
                    '============ END FOR PART A ITEM 1(A) OR ITEM 2(A)  DEVELOPMENT GAP LIST

                    '============ START FOR PART B ITEM 1(B) OR ITEM 2(B) OBJECTIVE LIST
                    mdtItemBObjective = mdtItemAKPI.Copy
                    FillPartAItemB_ObjectiveList()
                    '============ END FOR PART B ITEM 1(B) OR ITEM 2(B) OBJECTIVE LIST

                Else
                    FillPartAItemAKPIList()
                    FillPartAItemA_DeveoplmentGAPList()
                    FillPartAItemB_ObjectiveList()
                    FillFeedBackList()
                End If


                If cboEmployee.DataSource IsNot Nothing Then
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        Dim dtList As DataTable = CType(cboEmployee.DataSource, DataTable)
                        cboLineManager.DisplayMember = "employeename"
                        cboLineManager.ValueMember = "employeeunkid"
                        cboLineManager.DataSource = New DataView(dtList, "employeeunkid <> " & CInt(cboEmployee.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
                    End If
                End If

                If menAction <> enAction.EDIT_ONE Then
                    cboLineManager.Enabled = True
                    objBtnLineManager.Enabled = True
                Else
                    cboLineManager.Enabled = False
                    objBtnLineManager.Enabled = False
                End If

            Else
                cboLineManager.Enabled = False
                objBtnLineManager.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCourse_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboQuestion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQuestion.SelectedIndexChanged
        Dim dsList As DataSet
        Try
            Dim drRow As DataRowView = CType(cboQuestion.SelectedItem, DataRowView)

            If CInt(drRow("resultgroupunkid")) >= 0 Then
                Dim objResult As New clsresult_master
                dsList = objResult.getComboList("Result", True, CInt(drRow("resultgroupunkid")))
                cboAnswer.DisplayMember = "name"
                cboAnswer.ValueMember = "Id"
                cboAnswer.DataSource = dsList.Tables(0)
                lblAnswer.Visible = True
                cboAnswer.Visible = True
                objBtnSearchAnswer.Visible = True
            Else
                lblAnswer.Visible = False
                cboAnswer.Visible = False
                objBtnSearchAnswer.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboQuestion_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvItemAKPIList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvItemAKPIList.SelectedIndexChanged
        Try
            If lvItemAKPIList.SelectedItems.Count = 0 Then Exit Sub

            txtItemAKPI.Text = lvItemAKPIList.SelectedItems(0).Text

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvItemAKPIList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvItemAJobCapability_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvItemAJobCapability.SelectedIndexChanged
        Try
            If lvItemAJobCapability.SelectedItems.Count = 0 Then Exit Sub

            txtItemAJobCapability.Text = lvItemAJobCapability.SelectedItems(0).Text

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvItemAJobCapability_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvItemBList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvItemBList.SelectedIndexChanged
        Try
            If lvItemBList.SelectedItems.Count = 0 Then Exit Sub

            txtItemBKPI.Text = lvItemBList.SelectedItems(0).Text
            txtItemBTrainingObjective.Text = lvItemBList.SelectedItems(0).SubItems(colhItemBTrainingObjective.Index).Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvItemBList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvFeedBackList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvFeedBackList.SelectedIndexChanged
        Try
            If lvFeedBackList.SelectedItems.Count = 0 Then Exit Sub

            cboQuestion.SelectedValue = CInt(lvFeedBackList.SelectedItems(0).SubItems(objColhQuestionunkid.Index).Text)
            cboAnswer.SelectedValue = CInt(lvFeedBackList.SelectedItems(0).SubItems(objColhAnswerunkid.Index).Text)
            txtFeedbackComment.Text = lvFeedBackList.SelectedItems(0).SubItems(colhFeedBackComment.Index).Text.Trim
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvFeedBackList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Tab Control event"

    Private Sub tbImpactEvaluation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbImpactEvaluation.SelectedIndexChanged
        Try
            If tbImpactEvaluation.SelectedTab.Name = tbppartB.Name Then
                FillPartBKPIList()

            ElseIf tbImpactEvaluation.SelectedTab.Name = tbppartC.Name Then
                FillQuestion()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tbImpactEvaluation_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteItemAGAP.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteItemAGAP.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddItemAGAP.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddItemAGAP.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteItemB.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteItemB.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditItemB.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditItemB.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditItemAGAP.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditItemAGAP.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddItemB.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddItemB.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditItemAKPI.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditItemAKPI.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteItemAKPI.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteItemAKPI.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddItemAKPI.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddItemAKPI.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditFeedBack.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditFeedBack.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteFeedBack.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteFeedBack.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddFeedback.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddFeedback.GradientForeColor = GUI._ButttonFontColor

            Me.btnFinalSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnFinalSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.tbppartA.Text = Language._Object.getCaption(Me.tbppartA.Name, Me.tbppartA.Text)
            Me.tbppartB.Text = Language._Object.getCaption(Me.tbppartB.Name, Me.tbppartB.Text)
            Me.tbppartC.Text = Language._Object.getCaption(Me.tbppartC.Name, Me.tbppartC.Text)
            Me.lblCourse.Text = Language._Object.getCaption(Me.lblCourse.Name, Me.lblCourse.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.LblItemACaption.Text = Language._Object.getCaption(Me.LblItemACaption.Name, Me.LblItemACaption.Text)
            Me.LblItemA.Text = Language._Object.getCaption(Me.LblItemA.Name, Me.LblItemA.Text)
            Me.lblItemAKPI.Text = Language._Object.getCaption(Me.lblItemAKPI.Name, Me.lblItemAKPI.Text)
            Me.lblItemAJobCapability.Text = Language._Object.getCaption(Me.lblItemAJobCapability.Name, Me.lblItemAJobCapability.Text)
            Me.btnDeleteItemAGAP.Text = Language._Object.getCaption(Me.btnDeleteItemAGAP.Name, Me.btnDeleteItemAGAP.Text)
            Me.btnAddItemAGAP.Text = Language._Object.getCaption(Me.btnAddItemAGAP.Name, Me.btnAddItemAGAP.Text)
            Me.colhItemAKPI.Text = Language._Object.getCaption(CStr(Me.colhItemAKPI.Tag), Me.colhItemAKPI.Text)
            Me.LblItemB.Text = Language._Object.getCaption(Me.LblItemB.Name, Me.LblItemB.Text)
            Me.lblItemBTrainingObjective.Text = Language._Object.getCaption(Me.lblItemBTrainingObjective.Name, Me.lblItemBTrainingObjective.Text)
            Me.LblItemBCaption.Text = Language._Object.getCaption(Me.LblItemBCaption.Name, Me.LblItemBCaption.Text)
            Me.btnDeleteItemB.Text = Language._Object.getCaption(Me.btnDeleteItemB.Name, Me.btnDeleteItemB.Text)
            Me.colhItemBTrainingObjective.Text = Language._Object.getCaption(CStr(Me.colhItemBTrainingObjective.Tag), Me.colhItemBTrainingObjective.Text)
            Me.lblPartBDesc.Text = Language._Object.getCaption(Me.lblPartBDesc.Name, Me.lblPartBDesc.Text)
            Me.lblPartBJobCapabilities.Text = Language._Object.getCaption(Me.lblPartBJobCapabilities.Name, Me.lblPartBJobCapabilities.Text)
            Me.lblNametask.Text = Language._Object.getCaption(Me.lblNametask.Name, Me.lblNametask.Text)
            Me.colhPartBKRA.Text = Language._Object.getCaption(CStr(Me.colhPartBKRA.Tag), Me.colhPartBKRA.Text)
            Me.ColumnHeader1.Text = Language._Object.getCaption(CStr(Me.ColumnHeader1.Tag), Me.ColumnHeader1.Text)
            Me.ColumnHeader2.Text = Language._Object.getCaption(CStr(Me.ColumnHeader2.Tag), Me.ColumnHeader2.Text)
            Me.btnEditItemB.Text = Language._Object.getCaption(Me.btnEditItemB.Name, Me.btnEditItemB.Text)
            Me.btnEditItemAGAP.Text = Language._Object.getCaption(Me.btnEditItemAGAP.Name, Me.btnEditItemAGAP.Text)
            Me.btnAddItemB.Text = Language._Object.getCaption(Me.btnAddItemB.Name, Me.btnAddItemB.Text)
            Me.colhItemAJobCapability.Text = Language._Object.getCaption(CStr(Me.colhItemAJobCapability.Tag), Me.colhItemAJobCapability.Text)
            Me.btnEditItemAKPI.Text = Language._Object.getCaption(Me.btnEditItemAKPI.Name, Me.btnEditItemAKPI.Text)
            Me.btnDeleteItemAKPI.Text = Language._Object.getCaption(Me.btnDeleteItemAKPI.Name, Me.btnDeleteItemAKPI.Text)
            Me.btnAddItemAKPI.Text = Language._Object.getCaption(Me.btnAddItemAKPI.Name, Me.btnAddItemAKPI.Text)
            Me.lblLineManager.Text = Language._Object.getCaption(Me.lblLineManager.Name, Me.lblLineManager.Text)
            Me.btnEditFeedBack.Text = Language._Object.getCaption(Me.btnEditFeedBack.Name, Me.btnEditFeedBack.Text)
            Me.btnDeleteFeedBack.Text = Language._Object.getCaption(Me.btnDeleteFeedBack.Name, Me.btnDeleteFeedBack.Text)
            Me.btnAddFeedback.Text = Language._Object.getCaption(Me.btnAddFeedback.Name, Me.btnAddFeedback.Text)
            Me.colhQuestion.Text = Language._Object.getCaption(CStr(Me.colhQuestion.Tag), Me.colhQuestion.Text)
            Me.lblItemBKPI.Text = Language._Object.getCaption(Me.lblItemBKPI.Name, Me.lblItemBKPI.Text)
            Me.colhPartAItemBKPI.Text = Language._Object.getCaption(CStr(Me.colhPartAItemBKPI.Tag), Me.colhPartAItemBKPI.Text)
            Me.lblQuestion.Text = Language._Object.getCaption(Me.lblQuestion.Name, Me.lblQuestion.Text)
            Me.lblPartCDesc.Text = Language._Object.getCaption(Me.lblPartCDesc.Name, Me.lblPartCDesc.Text)
            Me.lblAnswer.Text = Language._Object.getCaption(Me.lblAnswer.Name, Me.lblAnswer.Text)
            Me.lblComments.Text = Language._Object.getCaption(Me.lblComments.Name, Me.lblComments.Text)
            Me.colhAnswer.Text = Language._Object.getCaption(CStr(Me.colhAnswer.Tag), Me.colhAnswer.Text)
            Me.colhFeedBackComment.Text = Language._Object.getCaption(CStr(Me.colhFeedBackComment.Tag), Me.colhFeedBackComment.Text)
            Me.btnFinalSave.Text = Language._Object.getCaption(Me.btnFinalSave.Name, Me.btnFinalSave.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Item 1(a):")
            Language.setMessage(mstrModuleName, 2, "Job Capability or Training Related Performance Gap identified during Continuous Performance Assessment or Appraisal Process.")
            Language.setMessage(mstrModuleName, 3, "Key Performance Indicator or Key Results Area")
            Language.setMessage(mstrModuleName, 4, "Identified Job Capability or Training Related Performance Deficiency/Gap")
            Language.setMessage(mstrModuleName, 5, "Item 1(b):")
            Language.setMessage(mstrModuleName, 6, "Training/Programme Objective(s) or Expected Outcomes after attending the training: This should address Job Capability or Training Related Performance Gap Identified on Item 1(a). (Your expectation on what the staff will be able to do at workplace after attending the training/programme).")
            Language.setMessage(mstrModuleName, 7, "Select")
            Language.setMessage(mstrModuleName, 8, "Training Objective")
            Language.setMessage(mstrModuleName, 9, "Item 2(a):")
            Language.setMessage(mstrModuleName, 10, "Career Development Need Identified during Continuous Performance Assessment or Appraisal Process.")
            Language.setMessage(mstrModuleName, 11, "Key Performance Indicator or Key Results Area for senior position.")
            Language.setMessage(mstrModuleName, 12, "Identified Career Development Gap")
            Language.setMessage(mstrModuleName, 13, "Item 2(b):")
            Language.setMessage(mstrModuleName, 14, "Training/Programme Objective(s) or Expected Outcome after attending the training: This should address the identified Career Development Gap Identified on Item 2(a). (Your expectation on what the staff needs to have for preparation for immediate higher responsibilities or senior position).")
            Language.setMessage(mstrModuleName, 15, "Training/Programme Objective")
            Language.setMessage(mstrModuleName, 16, "Are you sure you want to delete this Item ?")
            Language.setMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation.")
            Language.setMessage(mstrModuleName, 18, "Training Objective cannot be blank.Training Objective are required information.")
            Language.setMessage(mstrModuleName, 19, " cannot be blank.")
            Language.setMessage(mstrModuleName, 20, " is required information.")
            Language.setMessage(mstrModuleName, 21, "This KPI/KRA already exists in the list.Please define new KPI/KRA.")
            Language.setMessage(mstrModuleName, 22, "  already exists in the list.Please define new")
            Language.setMessage(mstrModuleName, 23, "This Training Objective already exists in the list.Please define new Training Objective.")
            Language.setMessage(mstrModuleName, 24, "Course is compulsory information.Please Select Course.")
            Language.setMessage(mstrModuleName, 25, "Employee is compulsory information.Please Select Employee.")
            Language.setMessage(mstrModuleName, 26, "Line Manager/Supervisor is compulsory information.Please Select Line Manager/Supervisor.")
            Language.setMessage(mstrModuleName, 27, "Six (6) Months ago you attended the above mentioned training/programme. As part of level three training impact evaluation process you are requested to provide Self Assessment on Competence enhancement/Improvement in relation to the explanations provided on Part A item 1(b).")
            Language.setMessage(mstrModuleName, 28, "Job capabilities enhanced/Improved after attending the training/programme.")
            Language.setMessage(mstrModuleName, 29, "Name or Describe tasks that you are able to perform competently to the required standards after attending the Training in relation to Items 1(a).")
            Language.setMessage(mstrModuleName, 30, "Six (6) Months ago a member of your staff attended the above training programme. It is expected that following his/her return you have been giving him/her support, coaching and guidance to facilitate the transfer of the learning into the workplace for enhanced competences in the prior identified Job capability/training related performance gap and career Development Needs. To that end as part of Level Three Training Impact Evaluation Process you are kindly requested to briefly answer the following questions and return the form to us.")
            Language.setMessage(mstrModuleName, 31, "This Feedback Question is already exist in the list.Please define new Feedback Question.")
            Language.setMessage(mstrModuleName, 32, "Feedback Question is Compulsory information.Please Select Feedback Question.")
            Language.setMessage(mstrModuleName, 33, "Feedback Answer is Compulsory information.Please Select Feedback Answer.")
            Language.setMessage(mstrModuleName, 34, "Are you sure you want to Finally save this Imapct feedback.After this you won't be allowed to EDIT this Impact feedback.Do you want to continue?")
            Language.setMessage(mstrModuleName, 35, "Feedback Answer is compuslory information.Please Select Feedback Answer.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class