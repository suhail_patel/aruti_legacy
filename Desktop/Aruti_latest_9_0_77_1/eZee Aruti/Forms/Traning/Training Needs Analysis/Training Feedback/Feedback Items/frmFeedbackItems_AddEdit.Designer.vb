﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFeedbackItems_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFeedbackItems_AddEdit))
        Me.pnlAssessmentItems = New System.Windows.Forms.Panel
        Me.gbFeedbackItems = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddResultGroup = New eZee.Common.eZeeGradientButton
        Me.cboResultGroup = New System.Windows.Forms.ComboBox
        Me.lblResultGroup = New System.Windows.Forms.Label
        Me.objbtnAddGroup = New eZee.Common.eZeeGradientButton
        Me.objbtnOtherLanguage = New eZee.Common.eZeeGradientButton
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.cboGroup = New System.Windows.Forms.ComboBox
        Me.lblAssessmentDescription = New System.Windows.Forms.Label
        Me.lblAssessmentItem = New System.Windows.Forms.Label
        Me.lblAssessmentItemCode = New System.Windows.Forms.Label
        Me.lblGroupCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.pnlAssessmentItems.SuspendLayout()
        Me.gbFeedbackItems.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlAssessmentItems
        '
        Me.pnlAssessmentItems.Controls.Add(Me.gbFeedbackItems)
        Me.pnlAssessmentItems.Controls.Add(Me.objFooter)
        Me.pnlAssessmentItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlAssessmentItems.Location = New System.Drawing.Point(0, 0)
        Me.pnlAssessmentItems.Name = "pnlAssessmentItems"
        Me.pnlAssessmentItems.Size = New System.Drawing.Size(385, 312)
        Me.pnlAssessmentItems.TabIndex = 0
        '
        'gbFeedbackItems
        '
        Me.gbFeedbackItems.BorderColor = System.Drawing.Color.Black
        Me.gbFeedbackItems.Checked = False
        Me.gbFeedbackItems.CollapseAllExceptThis = False
        Me.gbFeedbackItems.CollapsedHoverImage = Nothing
        Me.gbFeedbackItems.CollapsedNormalImage = Nothing
        Me.gbFeedbackItems.CollapsedPressedImage = Nothing
        Me.gbFeedbackItems.CollapseOnLoad = False
        Me.gbFeedbackItems.Controls.Add(Me.objlblCaption)
        Me.gbFeedbackItems.Controls.Add(Me.objbtnAddResultGroup)
        Me.gbFeedbackItems.Controls.Add(Me.cboResultGroup)
        Me.gbFeedbackItems.Controls.Add(Me.lblResultGroup)
        Me.gbFeedbackItems.Controls.Add(Me.objbtnAddGroup)
        Me.gbFeedbackItems.Controls.Add(Me.objbtnOtherLanguage)
        Me.gbFeedbackItems.Controls.Add(Me.txtDescription)
        Me.gbFeedbackItems.Controls.Add(Me.txtName)
        Me.gbFeedbackItems.Controls.Add(Me.txtCode)
        Me.gbFeedbackItems.Controls.Add(Me.cboGroup)
        Me.gbFeedbackItems.Controls.Add(Me.lblAssessmentDescription)
        Me.gbFeedbackItems.Controls.Add(Me.lblAssessmentItem)
        Me.gbFeedbackItems.Controls.Add(Me.lblAssessmentItemCode)
        Me.gbFeedbackItems.Controls.Add(Me.lblGroupCode)
        Me.gbFeedbackItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFeedbackItems.ExpandedHoverImage = Nothing
        Me.gbFeedbackItems.ExpandedNormalImage = Nothing
        Me.gbFeedbackItems.ExpandedPressedImage = Nothing
        Me.gbFeedbackItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFeedbackItems.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFeedbackItems.HeaderHeight = 25
        Me.gbFeedbackItems.HeaderMessage = ""
        Me.gbFeedbackItems.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFeedbackItems.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFeedbackItems.HeightOnCollapse = 0
        Me.gbFeedbackItems.LeftTextSpace = 0
        Me.gbFeedbackItems.Location = New System.Drawing.Point(0, 0)
        Me.gbFeedbackItems.Name = "gbFeedbackItems"
        Me.gbFeedbackItems.OpenHeight = 119
        Me.gbFeedbackItems.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFeedbackItems.ShowBorder = True
        Me.gbFeedbackItems.ShowCheckBox = False
        Me.gbFeedbackItems.ShowCollapseButton = False
        Me.gbFeedbackItems.ShowDefaultBorderColor = True
        Me.gbFeedbackItems.ShowDownButton = False
        Me.gbFeedbackItems.ShowHeader = True
        Me.gbFeedbackItems.Size = New System.Drawing.Size(385, 257)
        Me.gbFeedbackItems.TabIndex = 0
        Me.gbFeedbackItems.Temp = 0
        Me.gbFeedbackItems.Text = "Feedback Items"
        Me.gbFeedbackItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddResultGroup
        '
        Me.objbtnAddResultGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddResultGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddResultGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddResultGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddResultGroup.BorderSelected = False
        Me.objbtnAddResultGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddResultGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddResultGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddResultGroup.Location = New System.Drawing.Point(356, 59)
        Me.objbtnAddResultGroup.Name = "objbtnAddResultGroup"
        Me.objbtnAddResultGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddResultGroup.TabIndex = 5
        '
        'cboResultGroup
        '
        Me.cboResultGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboResultGroup.FormattingEnabled = True
        Me.cboResultGroup.Items.AddRange(New Object() {"Outputs", "Skills/Attributes", "Knowledge", "Other Activites"})
        Me.cboResultGroup.Location = New System.Drawing.Point(91, 59)
        Me.cboResultGroup.Name = "cboResultGroup"
        Me.cboResultGroup.Size = New System.Drawing.Size(259, 21)
        Me.cboResultGroup.TabIndex = 4
        '
        'lblResultGroup
        '
        Me.lblResultGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResultGroup.Location = New System.Drawing.Point(8, 61)
        Me.lblResultGroup.Name = "lblResultGroup"
        Me.lblResultGroup.Size = New System.Drawing.Size(77, 15)
        Me.lblResultGroup.TabIndex = 3
        Me.lblResultGroup.Text = "Result Group"
        Me.lblResultGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddGroup
        '
        Me.objbtnAddGroup.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddGroup.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddGroup.BorderSelected = False
        Me.objbtnAddGroup.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddGroup.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddGroup.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddGroup.Location = New System.Drawing.Point(356, 33)
        Me.objbtnAddGroup.Name = "objbtnAddGroup"
        Me.objbtnAddGroup.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddGroup.TabIndex = 2
        '
        'objbtnOtherLanguage
        '
        Me.objbtnOtherLanguage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnOtherLanguage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnOtherLanguage.BorderSelected = False
        Me.objbtnOtherLanguage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnOtherLanguage.Image = Global.Aruti.Main.My.Resources.Resources.OtherLanguage_16
        Me.objbtnOtherLanguage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnOtherLanguage.Location = New System.Drawing.Point(356, 111)
        Me.objbtnOtherLanguage.Name = "objbtnOtherLanguage"
        Me.objbtnOtherLanguage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnOtherLanguage.TabIndex = 10
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(91, 138)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(259, 57)
        Me.txtDescription.TabIndex = 15
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(91, 111)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(259, 21)
        Me.txtName.TabIndex = 9
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(91, 85)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(133, 21)
        Me.txtCode.TabIndex = 7
        '
        'cboGroup
        '
        Me.cboGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGroup.FormattingEnabled = True
        Me.cboGroup.Items.AddRange(New Object() {"Outputs", "Skills/Attributes", "Knowledge", "Other Activites"})
        Me.cboGroup.Location = New System.Drawing.Point(91, 33)
        Me.cboGroup.Name = "cboGroup"
        Me.cboGroup.Size = New System.Drawing.Size(259, 21)
        Me.cboGroup.TabIndex = 1
        '
        'lblAssessmentDescription
        '
        Me.lblAssessmentDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentDescription.Location = New System.Drawing.Point(8, 142)
        Me.lblAssessmentDescription.Name = "lblAssessmentDescription"
        Me.lblAssessmentDescription.Size = New System.Drawing.Size(77, 15)
        Me.lblAssessmentDescription.TabIndex = 14
        Me.lblAssessmentDescription.Text = "Description"
        Me.lblAssessmentDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAssessmentItem
        '
        Me.lblAssessmentItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentItem.Location = New System.Drawing.Point(8, 111)
        Me.lblAssessmentItem.Name = "lblAssessmentItem"
        Me.lblAssessmentItem.Size = New System.Drawing.Size(77, 15)
        Me.lblAssessmentItem.TabIndex = 8
        Me.lblAssessmentItem.Text = "Name"
        Me.lblAssessmentItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAssessmentItemCode
        '
        Me.lblAssessmentItemCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessmentItemCode.Location = New System.Drawing.Point(8, 86)
        Me.lblAssessmentItemCode.Name = "lblAssessmentItemCode"
        Me.lblAssessmentItemCode.Size = New System.Drawing.Size(77, 15)
        Me.lblAssessmentItemCode.TabIndex = 6
        Me.lblAssessmentItemCode.Text = "Code"
        Me.lblAssessmentItemCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGroupCode
        '
        Me.lblGroupCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroupCode.Location = New System.Drawing.Point(8, 36)
        Me.lblGroupCode.Name = "lblGroupCode"
        Me.lblGroupCode.Size = New System.Drawing.Size(77, 15)
        Me.lblGroupCode.TabIndex = 0
        Me.lblGroupCode.Text = "Group"
        Me.lblGroupCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 257)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(385, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(173, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(276, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objlblCaption
        '
        Me.objlblCaption.ForeColor = System.Drawing.SystemColors.ControlText
        Me.objlblCaption.Location = New System.Drawing.Point(11, 202)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(339, 49)
        Me.objlblCaption.TabIndex = 17
        Me.objlblCaption.Text = "##"
        '
        'frmFeedbackItems_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(385, 312)
        Me.Controls.Add(Me.pnlAssessmentItems)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFeedbackItems_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Feedback Items"
        Me.pnlAssessmentItems.ResumeLayout(False)
        Me.gbFeedbackItems.ResumeLayout(False)
        Me.gbFeedbackItems.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlAssessmentItems As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents gbFeedbackItems As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblAssessmentDescription As System.Windows.Forms.Label
    Friend WithEvents lblAssessmentItem As System.Windows.Forms.Label
    Friend WithEvents lblAssessmentItemCode As System.Windows.Forms.Label
    Friend WithEvents lblGroupCode As System.Windows.Forms.Label
    Friend WithEvents cboGroup As System.Windows.Forms.ComboBox
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnOtherLanguage As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddResultGroup As eZee.Common.eZeeGradientButton
    Friend WithEvents cboResultGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblResultGroup As System.Windows.Forms.Label
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
End Class
