﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text
Imports System.IO

#End Region

Public Class frmTraningPriorityList
    Private ReadOnly mstrModuleName As String = "frmTraningPriorityList"

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Dim objPriorityMaster As New clshrtnaprirority_master
        Dim objPeriod As New clscommom_period_Tran
        Try
            dsList = objPriorityMaster.getListForCombo("List", True, False)
            With cboRefrenceNo
                .ValueMember = "prioritymasterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [23 APR 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            'S.SANDEEP [23 APR 2015] -- END
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Dim strSearching As String = ""
        Dim dtList As DataTable = Nothing
        Dim objPriorityMaster As New clshrtnaprirority_master
        Try

            If User._Object.Privilege._AllowToViewTrainingNeedsAnalysisList = True Then                'Pinkal (02-Jul-2012) -- Start

                Dim dsList As New DataSet
                dsList = objPriorityMaster.GetList("List", True)

                If CInt(cboRefrenceNo.SelectedValue) > 0 Then
                    strSearching &= "AND prioritymasterunkid = '" & CInt(cboRefrenceNo.SelectedValue) & "' "
                End If

                If CInt(cboPeriod.SelectedValue) > 0 Then
                    strSearching &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtList = New DataView(dsList.Tables(0), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtList = dsList.Tables(0)
                End If

                lvTrainingPriorityList.Items.Clear()
                Dim lvItem As ListViewItem = Nothing

                For Each dr As DataRow In dtList.Rows
                    lvItem = New ListViewItem

                    lvItem.Text = dr.Item("PName").ToString
                    lvItem.SubItems.Add(dr.Item("referenceno").ToString)
                    lvItem.SubItems.Add(dr.Item("remark").ToString)
                    lvItem.SubItems.Add(dr.Item("Status").ToString)
                    lvItem.SubItems.Add(dr.Item("periodunkid").ToString)

                    If CBool(dr.Item("isfinal")) = True Then
                        lvItem.ForeColor = Color.Blue
                    End If

                    lvItem.Tag = dr.Item("prioritymasterunkid")

                    lvTrainingPriorityList.Items.Add(lvItem)
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveExcelfile", mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable, ByVal blnWithNames As Boolean) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 COLSPAN ='12' ALIGN='MIDDLE'><FONT SIZE=3><B>" & Language.getMessage(mstrModuleName, 2, "Training Priorities") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            If lvTrainingPriorityList.SelectedItems.Count > 0 Then
                If lvTrainingPriorityList.SelectedItems(0).Text.Trim.Length > 0 Then
                    strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
                    strBuilder.Append(" <TD BORDER=1 COLSPAN ='12' ALIGN='LEFT'><FONT SIZE=3><B>" & Language.getMessage(mstrModuleName, 3, "Period : ") & " " & cboPeriod.Text.ToString & "</B></FONT></TD>" & vbCrLf)
                    strBuilder.Append(" </TR> " & vbCrLf)
                End If
            End If

            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B>")
            strBuilder.Append(" Reference No : " & lvTrainingPriorityList.SelectedItems(0).SubItems(colhRefrenceNo.Index).Text.ToString & vbCrLf)
            strBuilder.Append(" </FONT></B>")
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)

            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To objDataReader.Columns.Count - 1
                If objDataReader.Columns(j).Caption = "" Then Continue For
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B> &nbsp;" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            For i As Integer = 0 To objDataReader.Rows.Count - 1
                If objDataReader.Rows(i)("AUD").ToString() = "D" Then Continue For
                If CBool(objDataReader.Rows(i)("IsGrp")) = True Then
                    strBuilder.Append(" <TR> " & vbCrLf)
                    For k As Integer = 0 To objDataReader.Columns.Count - 1
                        If objDataReader.Columns(k).Caption = "" Then Continue For

                        If objDataReader.Columns(k).DataType Is Type.GetType("System.DateTime") Then
                            If objDataReader.Rows(i)(k).ToString() <> "" Then
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & CDate(objDataReader.Rows(i)(k)).ToShortDateString() & "</FONT></TD>" & vbCrLf)
                            Else
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> </FONT></TD>" & vbCrLf)
                            End If
                        Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & objDataReader.Rows(i)(k).ToString() & "<B></FONT></TD>" & vbCrLf)
                        End If
                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)
                Else
                    If blnWithNames = True Then
                        strBuilder.Append(" <TR> " & vbCrLf)
                        For k As Integer = 0 To objDataReader.Columns.Count - 1
                            If objDataReader.Columns(k).Caption = "" Then Continue For

                            If objDataReader.Columns(k).DataType Is Type.GetType("System.DateTime") Then
                                If objDataReader.Rows(i)(k).ToString() <> "" Then
                                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & CDate(objDataReader.Rows(i)(k)).ToShortDateString() & "</FONT></TD>" & vbCrLf)
                                Else
                                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> </FONT></TD>" & vbCrLf)
                                End If
                            Else
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataReader.Rows(i)(k).ToString() & "</FONT></TD>" & vbCrLf)
                            End If
                        Next
                        strBuilder.Append(" </TR> " & vbCrLf)
                    End If
                End If
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)

            strBuilder.Append(" </HTML> " & vbCrLf)

            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If

            blnFlag = SaveExcelfile(SavePath & "\" & flFileName, strBuilder)

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_to_Excel", mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddTrainingPriority
            btnEdit.Enabled = User._Object.Privilege._AllowToEditTrainingPriority
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteTrainingPriority
            mnuExportWithName.Enabled = User._Object.Privilege._AllowToExportWithEmployee
            mnuExportWithOutName.Enabled = User._Object.Privilege._AllowToExportWithoutEmployee
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 16 MAY 2012 ] -- END

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboRefrenceNo.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            If cboPeriod.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboPeriod.ValueMember
                    .DisplayMember = cboPeriod.DisplayMember
                    .DataSource = CType(cboPeriod.DataSource, DataTable)
                End With
                If frm.DisplayDialog Then
                    cboPeriod.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchRefNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRefNo.Click
        Dim frm As New frmCommonSearch
        Try
            If cboRefrenceNo.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboRefrenceNo.ValueMember
                    .DisplayMember = cboRefrenceNo.DisplayMember
                    .DataSource = CType(cboRefrenceNo.DataSource, DataTable)
                End With
                If frm.DisplayDialog Then
                    cboRefrenceNo.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim frm As New frmTraining_Priority
            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvTrainingPriorityList.SelectedItems.Count > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Are you sure you want to delete selected reference no. with its course and employee?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    Dim objPriorityMaster As New clshrtnaprirority_master
                    Dim frm As New frmReasonSelection
                    Dim mstrVoidReason As String = String.Empty
                    frm.displayDialog(enVoidCategoryType.TRAINING, mstrVoidReason)
                    If mstrVoidReason.Length <= 0 Then
                        Exit Sub
                    Else
                        objPriorityMaster._Voidreason = mstrVoidReason
                    End If
                    objPriorityMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objPriorityMaster._Voiduserunkid = User._Object._Userunkid
                    objPriorityMaster._Isvoid = True
                    frm = Nothing
                    With objPriorityMaster
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
                        ._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    objPriorityMaster.Delete(CInt(lvTrainingPriorityList.SelectedItems(0).Tag))

                    Call FillList()
                    objPriorityMaster = Nothing
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvTrainingPriorityList.SelectedItems.Count > 0 Then
                Dim frm As New frmTraining_Priority
                If frm.displayDialog(CInt(lvTrainingPriorityList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call FillList()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Private Sub frmTraningPriorityList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            Call OtherSettings()
            FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAppraisalRefrenceList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clshrtnaprirority_master.SetMessages()
            objfrm._Other_ModuleNames = "clshrtnaprirority_master"
            objfrm.displayDialog(Me)


            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub mnuExportWithName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportWithName.Click
        Try
            If lvTrainingPriorityList.SelectedItems.Count > 0 Then
                Dim dtTable As DataTable = Nothing
                Dim objPriorityTran As New clshrtnacoursepriority_tran
                objPriorityTran._RefrenceNo = lvTrainingPriorityList.SelectedItems(0).SubItems(colhRefrenceNo.Index).Text.ToString
                dtTable = objPriorityTran._DataTable

                Dim objFile As New SaveFileDialog
                objFile.OverwritePrompt = True
                objFile.Filter = "Xls Files (*.xls) |*.xls"

                If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim obj As New FileInfo(objFile.FileName)
                    Dim mstrFilePath As String = objFile.FileName.Substring(0, objFile.FileName.LastIndexOf(CChar("\")))
                    If Export_to_Excel(obj.Name, mstrFilePath, dtTable, True) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, mstrFilePath)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportWithName_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuExportWithOutName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportWithOutName.Click
        Try
            If lvTrainingPriorityList.SelectedItems.Count > 0 Then
                Dim dtTable As DataTable = Nothing
                Dim objPriorityTran As New clshrtnacoursepriority_tran
                objPriorityTran._RefrenceNo = lvTrainingPriorityList.SelectedItems(0).SubItems(colhRefrenceNo.Index).Text.ToString
                dtTable = objPriorityTran._DataTable

                Dim objFile As New SaveFileDialog
                objFile.OverwritePrompt = True
                objFile.Filter = "Xls Files (*.xls) |*.xls"

                If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim obj As New FileInfo(objFile.FileName)
                    Dim mstrFilePath As String = objFile.FileName.Substring(0, objFile.FileName.LastIndexOf(CChar("\")))
                    If Export_to_Excel(obj.Name, mstrFilePath, dtTable, False) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, mstrFilePath)
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportWithOutName_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvTrainingPriorityList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvTrainingPriorityList.SelectedIndexChanged
        Try
            If lvTrainingPriorityList.SelectedItems.Count > 0 Then
                If lvTrainingPriorityList.SelectedItems(0).ForeColor = Color.Blue Then
                    btnDelete.Enabled = False : btnOperation.Enabled = False : btnEdit.Enabled = False
                Else
                    btnDelete.Enabled = True : btnOperation.Enabled = True : btnEdit.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvTrainingPriorityList_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbEmployeeInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbEmployeeInfo.Text = Language._Object.getCaption(Me.gbEmployeeInfo.Name, Me.gbEmployeeInfo.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblRefrenceNo.Text = Language._Object.getCaption(Me.lblRefrenceNo.Name, Me.lblRefrenceNo.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.colhRefrenceNo.Text = Language._Object.getCaption(CStr(Me.colhRefrenceNo.Tag), Me.colhRefrenceNo.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuExportWithName.Text = Language._Object.getCaption(Me.mnuExportWithName.Name, Me.mnuExportWithName.Text)
            Me.mnuExportWithOutName.Text = Language._Object.getCaption(Me.mnuExportWithOutName.Name, Me.mnuExportWithOutName.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Are you sure you want to delete selected reference no. with its course and employee?")
            Language.setMessage(mstrModuleName, 2, "Training Priorities")
            Language.setMessage(mstrModuleName, 3, "Period :")
            Language.setMessage(mstrModuleName, 4, "Data Exported successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class