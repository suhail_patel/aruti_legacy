﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text

#End Region

Public Class frmTraining_Priority

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmTraining_Priority"
    Private mblnCancel As Boolean = True
    Private mdtTraining As DataTable
    Private dgView As New DataView
    Private objMTnAPriority As clshrtnaprirority_master
    Private objTTnAPriority As clshrtnacoursepriority_tran
    Private mdtEmp As DataTable
    Private mintSelectedCombo As Integer = 0
    Private mblnIsFinalizeTraining As Boolean = True
    Private mdtDelete As DataTable = Nothing
    Private menAction As enAction
    Private mstrReferenceNumber As String = String.Empty
    Private mintPeriodUnkid As Integer = -1
    Private mintPriorityUnkid As Integer = -1
    Private mblnIsExportAction As Boolean = False

#End Region

#Region "Property"

    Public Property _IsFinalizeTraining() As Boolean
        Get
            Return mblnIsFinalizeTraining
        End Get
        Set(ByVal value As Boolean)
            mblnIsFinalizeTraining = value
        End Set
    End Property

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intUnkid As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintPriorityUnkid = intUnkid
            menAction = eAction

            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try

            Dim objPeriod As New clscommom_period_Tran
            'S.SANDEEP [19 FEB 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, FinancialYear._Object._YearUnkid, "List", True, 1)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, 1)
            'Sohail (21 Aug 2015) -- End
            'S.SANDEEP [19 FEB 2015] -- END
            cboPeriod.DisplayMember = "name"
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DataSource = dsList.Tables(0)

            Dim objCourse As New clsCommon_Master
            dsList = objCourse.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")

            cboCourse.DisplayMember = "name"
            cboCourse.ValueMember = "masterunkid"
            cboCourse.DataSource = dsList.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployeeList()
        Try
            Dim objEmployee As New clsEmployee_Master
            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim dsList As DataSet = objEmployee.GetEmployeeList("List", False, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString()).Date, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString()).Date)
            Dim dsList As DataSet

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("List", False, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString()).Date, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString()).Date)
            'Else
            '    dsList = objEmployee.GetEmployeeList("List", False, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", False)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (23 Nov 2012) -- End

            If CBool(dsList.Tables(0).Columns.Contains("IsCheck")) = False Then
                dsList.Tables(0).Columns.Add("IsCheck", Type.GetType("System.Boolean")).DefaultValue = False
            End If

            mdtEmp = dsList.Tables(0)

            dgvEmployee.AutoGenerateColumns = False
            dgView = mdtEmp.DefaultView
            dgvEmployee.DataSource = dgView

            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "employeename"
            objdgcolhEmpId.DataPropertyName = "employeename"
            objdgcolhECheck.DataPropertyName = "IsCheck"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployeeList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDataSource()
        Try
            Dim dsCombo As New DataSet

            dgvTrainingPriority.AutoGenerateColumns = False

            objdgcolhCheck.DataPropertyName = "IsCheck"
            dgcolhTrainingCourse.DataPropertyName = "training_need"

            Dim objMData As New clsMasterData
            dsCombo = objMData.GetPriority(True)
            dgcolhGAP.ValueMember = "id"
            dgcolhGAP.DisplayMember = "Name"
            dgcolhGAP.DataSource = dsCombo.Tables(0).Copy
            dgcolhGAP.DropDownWidth = 120
            dgcolhGAP.DataPropertyName = "gappriority"
            objMData = Nothing

            dgcolhBusinessPriority.ValueMember = "id"
            dgcolhBusinessPriority.DisplayMember = "Name"
            dgcolhBusinessPriority.DataSource = dsCombo.Tables(0).Copy
            dgcolhBusinessPriority.DropDownWidth = 120
            dgcolhBusinessPriority.DataPropertyName = "bussinesspriority"


            dgcolhKRA.DataPropertyName = "kra"
            dgcolhRanking.DataPropertyName = "priority_ranking"

            Dim objInstitute As New clsinstitute_master
            dsCombo = objInstitute.getListForCombo(False, "List", True, 1)
            dgcolhProvider.ValueMember = "instituteunkid"
            dgcolhProvider.DisplayMember = "name"
            dgcolhProvider.DataSource = dsCombo.Tables("List")
            dgcolhProvider.DropDownWidth = 350
            dgcolhProvider.DataPropertyName = "providerunkid"
            objInstitute = Nothing


            dgcolhtrainingVenue.DataPropertyName = "training_venue"

            Dim objCMaster As New clsCommon_Master
            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.STRATEGIC_GOAL, True, "List")
            dgcolhGoal.ValueMember = "masterunkid"
            dgcolhGoal.DisplayMember = "name"
            dgcolhGoal.DataSource = dsCombo.Tables("List")
            dgcolhGoal.DropDownWidth = 350
            dgcolhGoal.DataPropertyName = "goalunkid"
            objCMaster = Nothing

            objdgcolhIsGrp.DataPropertyName = "IsGrp"
            objdgcolhGrpId.DataPropertyName = "GrpId"
            objdgcolhemployeeunkid.DataPropertyName = "employeeunkid"
            objdgcolhTrainingCourseId.DataPropertyName = "courseunkid"

            objdgcolhSkill_Gap.DataPropertyName = "skill_gap"
            objdgcolhbusiness.DataPropertyName = "business_impact"
            objdgcolhProvider.DataPropertyName = "pref_provider"
            objdgcolhStartegicGoal.DataPropertyName = "strategic_goal"

            dgcolhTotEmp.DataPropertyName = "total_emp"
            dgcolhTraningSol.DataPropertyName = "training_sol"
            dgcolhTraningObjective.DataPropertyName = "training_objective"

            dgvTrainingPriority.DataSource = mdtTraining

            Call SetGridColor()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataSource", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetGridColor()
        Try
            Dim pCell As New clsMergeCell
            For Each dgvRow As DataGridViewRow In dgvTrainingPriority.Rows
                If CBool(dgvRow.Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If dgvRow.Cells(objdgcolhECollapse.Index).Value Is Nothing Then
                        dgvRow.Cells(objdgcolhECollapse.Index).Value = "+"
                    ElseIf dgvRow.Cells(objdgcolhECollapse.Index).Value Is "-" Then
                        dgvRow.Cells(objdgcolhECollapse.Index).Value = "+"
                    End If
                    dgvRow.DefaultCellStyle.BackColor = GUI.ColorComp
                Else
                    pCell.MakeMerge(dgvTrainingPriority, dgvRow.Index, 3, dgvRow.Cells.Count - 1, Color.White, Color.White, "", "", picStayView.Image)
                    dgvRow.Cells(objdgcolhECollapse.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhTrainingCourse.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhGAP.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhBusinessPriority.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhKRA.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhRanking.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhProvider.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhtrainingVenue.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhGoal.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhTraningSol.Index).ReadOnly = True
                    dgvRow.Cells(dgcolhTraningObjective.Index).ReadOnly = True
                    dgvTrainingPriority.CurrentCell = Nothing
                    dgvRow.Visible = False
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue(Optional ByVal blnIsFinal As Boolean = False)
        Try
            Select Case menAction
                Case enAction.ADD_ONE
                    objMTnAPriority._Referenceno = Now.Millisecond.ToString & ConfigParameter._Object._CurrentDateAndTime.Year.ToString & ConfigParameter._Object._CurrentDateAndTime.ToString("hhmmss") & ConfigParameter._Object._CurrentDateAndTime.Date.Day.ToString("0#") & ConfigParameter._Object._CurrentDateAndTime.Date.Month.ToString("0#")
                    objMTnAPriority._Isfinal = False
                Case enAction.EDIT_ONE
                    objMTnAPriority._Referenceno = txtReferenceNo.Text
                    objMTnAPriority._Prioritymasterunkid = mintPriorityUnkid
                    objMTnAPriority._Isfinal = blnIsFinal
            End Select
            objMTnAPriority._Periodunkid = CInt(cboPeriod.SelectedValue)
            objMTnAPriority._Remark = txtRemark.Text
            objMTnAPriority._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveExcelfile", mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

    Private Function Export_to_Excel(ByVal BlnIsSave As Boolean, ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable, ByVal blnWithNames As Boolean) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try

            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            strBuilder.Append(" <TD BORDER=1 COLSPAN ='12' ALIGN='MIDDLE'><FONT SIZE=3><B>" & Language.getMessage(mstrModuleName, 10, "Training Priorities") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)

            If CInt(cboPeriod.SelectedValue) > 0 Then
                strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 COLSPAN ='12' ALIGN='LEFT'><FONT SIZE=3><B>" & Language.getMessage(mstrModuleName, 12, "Period : ") & " " & cboPeriod.Text.ToString & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If

            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)

            If BlnIsSave Then
                strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)
                strBuilder.Append(" <TR> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
                strBuilder.Append(" <FONT SIZE=3><B>")
                strBuilder.Append(" Reference No : " & objMTnAPriority._Referenceno.ToString & vbCrLf)
                strBuilder.Append(" </FONT></B>")
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
                strBuilder.Append(" </TABLE> " & vbCrLf)
                strBuilder.Append(" <BR> " & vbCrLf)
            End If


            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)
            For j As Integer = 0 To objDataReader.Columns.Count - 1
                If objDataReader.Columns(j).Caption = "" Then Continue For
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B> &nbsp;" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next
            strBuilder.Append(" </TR> " & vbCrLf)

            Dim intEmpId As Integer = 0

            For i As Integer = 0 To objDataReader.Rows.Count - 1
                If objDataReader.Rows(i)("AUD").ToString() = "D" Then Continue For

                If CBool(objDataReader.Rows(i)("IsGrp")) = True Then
                    strBuilder.Append(" <TR> " & vbCrLf)
                    For k As Integer = 0 To objDataReader.Columns.Count - 1
                        If objDataReader.Columns(k).Caption = "" Then Continue For

                        If objDataReader.Columns(k).DataType Is Type.GetType("System.DateTime") Then
                            If objDataReader.Rows(i)(k).ToString() <> "" Then
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & CDate(objDataReader.Rows(i)(k)).ToShortDateString() & "</FONT></TD>" & vbCrLf)
                            Else
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> </FONT></TD>" & vbCrLf)
                            End If
                        Else
                            strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & objDataReader.Rows(i)(k).ToString() & "<B></FONT></TD>" & vbCrLf)
                        End If
                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)
                Else
                    If blnWithNames = True Then
                        strBuilder.Append(" <TR> " & vbCrLf)
                        For k As Integer = 0 To objDataReader.Columns.Count - 1
                            If objDataReader.Columns(k).Caption = "" Then Continue For

                            If objDataReader.Columns(k).DataType Is Type.GetType("System.DateTime") Then
                                If objDataReader.Rows(i)(k).ToString() <> "" Then
                                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & CDate(objDataReader.Rows(i)(k)).ToShortDateString() & "</FONT></TD>" & vbCrLf)
                                Else
                                    strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2> </FONT></TD>" & vbCrLf)
                                End If
                            Else
                                strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataReader.Rows(i)(k).ToString() & "</FONT></TD>" & vbCrLf)
                            End If
                        Next
                        strBuilder.Append(" </TR> " & vbCrLf)
                    End If
                End If
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)

            strBuilder.Append(" </HTML> " & vbCrLf)

            If SavePath.LastIndexOf("\") = SavePath.Length - 1 Then
                SavePath = SavePath.Remove(SavePath.LastIndexOf("\"))
            End If

            blnFlag = SaveExcelfile(SavePath & "\" & flFileName, strBuilder)

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Export_to_Excel", mstrModuleName)
        Finally
        End Try
    End Function

    Private Function Validation() As Boolean
        Try
            Dim drRow As DataRow() = Nothing

            '====== FOR GAP PRIORITY

            drRow = mdtTraining.Select("gappriority <= 0 AND IsGrp = 1 AND AUD <> 'D'")
            If drRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Skill GAP Priority is compulsory information.Please Select Skill GAP Priority from the list."))
                dgvTrainingPriority.Select()
                Return False
            End If

            '====== FOR BUSINESS PRIORITY
            drRow = mdtTraining.Select("bussinesspriority <= 0 AND IsGrp = 1 AND AUD <> 'D'")
            If drRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Business Priority is compulsory information.Please Select Business Priority from the list."))
                dgvTrainingPriority.Select()
                Return False
            End If

            '====== FOR KRA
            drRow = mdtTraining.Select("KRA is null AND IsGrp = 1 AND AUD <> 'D'")
            If drRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "KRA cannot be blank. KRA is required information."))
                dgvTrainingPriority.Select()
                Return False
            End If

            '====== FOR PRIORITY RANKING
            drRow = mdtTraining.Select("(priority_ranking is null OR priority_ranking ='0') AND IsGrp = 1 AND AUD <> 'D'")
            If drRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Priority Ranking cannot be blank. Priority Ranking is required information."))
                dgvTrainingPriority.Select()
                Return False
            End If

            '====== FOR PREFERED PROVIDER
            drRow = mdtTraining.Select("providerunkid <= 0 AND IsGrp = 1 AND AUD <> 'D'")
            If drRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Prefered Provider is compulsory information.Please Select Prefered Provider from the list."))
                dgvTrainingPriority.Select()
                Return False
            End If

            '====== FOR TRAINING VENUE
            drRow = mdtTraining.Select("training_venue is null AND IsGrp = 1 AND AUD <> 'D'")
            If drRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Training Venue cannot be blank.Training Venue is required information."))
                dgvTrainingPriority.Select()
                Return False
            End If

            '====== FOR TRAINING SOLUTION
            drRow = mdtTraining.Select("training_sol is null AND IsGrp = 1 AND AUD <> 'D'")
            If drRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Training Solution cannot be blank.Training Solution is required information."))
                dgvTrainingPriority.Select()
                Return False
            End If

            '====== FOR TRAINING VENUE
            drRow = mdtTraining.Select("training_objective is null AND IsGrp = 1 AND AUD <> 'D'")
            If drRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Training Objective cannot be blank.Training Objective is required information."))
                dgvTrainingPriority.Select()
                Return False
            End If


            '====== FOR STRATERGIC GOAL
            drRow = mdtTraining.Select("goalunkid <= 0 AND IsGrp = 1 AND AUD <> 'D'")
            If drRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Strategic Goal is compulsory information.Please Select Strategic Goal from the list."))
                dgvTrainingPriority.Select()
                Return False
            End If


            '===========FOR CHECKING DUPLICATE PRIORITY RANKING
            drRow = mdtTraining.Select("IsGrp = 1 AND AUD <> 'D'")
            If drRow.Length > 0 Then

                For i As Integer = 0 To drRow.Length - 1

                    Dim dr As DataRow() = mdtTraining.Select("IsGrp = 1 AND AUD <> 'D' AND priority_ranking ='" & drRow(i)("priority_ranking").ToString() & "' AND courseunkid <>" & CInt(drRow(i)("courseunkid")))
                    If dr.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Priority Ranking cannot be same.Please define unique priority ranking."))
                        dgvTrainingPriority.Select()
                        Return False
                    End If

                Next

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Form's Event(s) "

    Private Sub frmTraining_Priority_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objMTnAPriority = New clshrtnaprirority_master
        objTTnAPriority = New clshrtnacoursepriority_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()



            'S.SANDEEP [ 15 April 2013 ] -- START
            'ENHANCEMENT : LICENSE CHANGES
            If ConfigParameter._Object._IsArutiDemo = False Then
                chkPerformanceEvalutation.Visible = ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)
            End If
            'S.SANDEEP [ 15 April 2013 ] -- END

            FillCombo()
            txtSearch.Enabled = False

            Select Case menAction
                Case enAction.ADD_ONE

                    btnExport.Visible = True
                    btnSaveExport.Visible = True
                    btnFinalSave.Visible = False
                    btnSave.Visible = False
                    pnlRefCombo.Visible = False
                    pnlOtherDetail.Location = New Point(10, 8)
                    chkPerformanceEvalutation.CheckState = CheckState.Checked
                    mdtTraining = objTTnAPriority._DataTable
                    mdtDelete = objTTnAPriority._DataTable.Clone

                Case enAction.EDIT_ONE

                    objMTnAPriority._Prioritymasterunkid = mintPriorityUnkid

                    btnExport.Visible = False
                    btnSaveExport.Visible = False
                    btnFinalSave.Visible = True
                    btnSave.Visible = True
                    mdtDelete = objTTnAPriority._DataTable.Clone
                    pnlRefCombo.Visible = True
                    pnlOtherDetail.Location = New Point(10, 34)

                    objTTnAPriority._RefrenceNo = objMTnAPriority._Referenceno.Trim
                    mstrReferenceNumber = objMTnAPriority._Referenceno.Trim
                    txtReferenceNo.Text = mstrReferenceNumber
                    RemoveHandler cboPeriod.SelectedIndexChanged, AddressOf cboPeriod_SelectedIndexChanged
                    mintPeriodUnkid = objMTnAPriority._Periodunkid

                    If mintPeriodUnkid > 0 Then
                        chkPerformanceEvalutation.CheckState = CheckState.Checked
                    Else
                        chkPerformanceEvalutation.CheckState = CheckState.Unchecked
                    End If

                    txtRemark.Text = objMTnAPriority._Remark

                    cboPeriod.SelectedValue = mintPeriodUnkid
                    cboPeriod.Enabled = False : chkPerformanceEvalutation.Enabled = False : objbtnSearchPeriod.Enabled = False
                    mdtTraining = objTTnAPriority._DataTable
                    Call SetDataSource()
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTraining_Priority_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clshrtnacoursepriority_tran.SetMessages()
            clshrtnaprirority_master.SetMessages()
            objfrm._Other_ModuleNames = "clshrtnacoursepriority_tran,clshrtnaprirority_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnHide_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnHide.Click
        Try
            SplitContainer1.Panel1Collapsed = True
            objbtnHide.Visible = False
            objbtnShow.Visible = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnHide_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnShow.Click
        Try
            SplitContainer1.Panel1Collapsed = False
            objbtnShow.Visible = False
            objbtnHide.Visible = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnShow_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillEmployeeList()
            txtSearch.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            dgvEmployee.DataSource = Nothing _
          : txtSearch.Enabled = False _
          : txtSearch.Text = "" _
          : objchkEmployee.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If CInt(cboCourse.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Training Course is mandatory information. Please select Training Course to continue."), enMsgBoxStyle.Information)
                cboCourse.Focus()
                Exit Sub
            End If

            If mdtTraining IsNot Nothing Then
                Dim dTemp() As DataRow = mdtEmp.Select("IsCheck = true")
                If dTemp.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Employee is mandatory information. Please select atleast one employee to continue."), enMsgBoxStyle.Information)
                    dgvEmployee.Focus()
                    Exit Sub
                End If

                Dim dCourse() As DataRow = mdtTraining.Select("courseunkid = '" & CInt(cboCourse.SelectedValue) & "' AND IsGrp = true AND AUD <> 'D'")
                If dCourse.Length > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "This Course is already present in the list. You can continue adding selected employee(s) to this Course Group." & vbCrLf & "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim dRow As DataRow = Nothing

                        Dim dPresent() As DataRow = Nothing
                        Dim blnIsShown As Boolean = False

                        For i As Integer = 0 To dTemp.Length - 1
                            dPresent = mdtTraining.Select("courseunkid = '" & CInt(cboCourse.SelectedValue) & "' AND employeeunkid = '" & dTemp(i)("employeeunkid").ToString & "' AND AUD <>'D' ")
                            If dPresent.Length > 0 Then
                                If blnIsShown = False Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Some of the employee(s) are already assigned to this particular course group and will not be added again."), enMsgBoxStyle.Information)
                                    blnIsShown = True
                                End If
                                Continue For
                            End If
                            dRow = mdtTraining.NewRow
                            dRow.Item("courseunkid") = CInt(cboCourse.SelectedValue)
                            dRow.Item("training_need") = Space(5) & dTemp(i)("employeecode").ToString & " - " & dTemp(i)("employeename").ToString
                            dRow.Item("IsGrp") = False
                            dRow.Item("GrpId") = CInt(cboCourse.SelectedValue)
                            dRow.Item("employeeunkid") = dTemp(i)("employeeunkid").ToString
                            dRow.Item("total_emp") = ""
                            dRow.Item("AUD") = "A"
                            dRow.Item("GUID") = Guid.NewGuid.ToString
                            mdtTraining.Rows.Add(dRow)
                            dTemp(i)("IsCheck") = False
                        Next

                        Dim dEmp() As DataRow = mdtTraining.Select("courseunkid = '" & CInt(cboCourse.SelectedValue) & "' AND IsGrp = false AND AUD <> 'D'")
                        dCourse(0)("total_emp") = dEmp.Length

                    End If
                Else
                    Dim dRow As DataRow = mdtTraining.NewRow
                    dRow.Item("courseunkid") = CInt(cboCourse.SelectedValue)
                    dRow.Item("training_need") = cboCourse.Text
                    dRow.Item("IsGrp") = True
                    dRow.Item("GrpId") = CInt(cboCourse.SelectedValue)
                    dRow.Item("employeeunkid") = -1
                    dRow.Item("total_emp") = dTemp.Length
                    dRow.Item("AUD") = "A"
                    dRow.Item("GUID") = Guid.NewGuid.ToString
                    mdtTraining.Rows.Add(dRow)

                    For i As Integer = 0 To dTemp.Length - 1
                        dRow = mdtTraining.NewRow
                        dRow.Item("courseunkid") = CInt(cboCourse.SelectedValue)
                        dRow.Item("training_need") = Space(5) & dTemp(i)("employeecode").ToString & " - " & dTemp(i)("employeename").ToString
                        dRow.Item("IsGrp") = False
                        dRow.Item("GrpId") = CInt(cboCourse.SelectedValue)
                        dRow.Item("employeeunkid") = dTemp(i)("employeeunkid").ToString
                        dRow.Item("total_emp") = ""
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtTraining.Rows.Add(dRow)
                        dTemp(i)("IsCheck") = False
                    Next
                End If
                Dim dView As DataView = mdtTraining.DefaultView
                dView.Sort = "courseunkid"
                mdtTraining = dView.ToTable
                Call SetDataSource()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try

            If mdtTraining Is Nothing Then Exit Sub

            Dim drRow As DataRow() = mdtTraining.Select("ischeck = true AND AUD <> 'D'")
            If drRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select atleast one employee or training course to remove from the list."), enMsgBoxStyle.Information)
                Exit Sub
            Else
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Are you sure you want to remove selected information from the list?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                    If drRow.Length > 0 Then
                        Dim mstrVoidReason As String = String.Empty
                        If mblnIsFinalizeTraining = True Then
                            Dim frm As New frmReasonSelection
                            frm.displayDialog(enVoidCategoryType.TRAINING, mstrVoidReason)
                            If mstrVoidReason.Length <= 0 Then
                                Exit Sub
                            End If
                        End If

                        For Each drow As DataRow In drRow
                            drow("AUD") = "D"

                            If mblnIsFinalizeTraining = True Then
                                drow.Item("isvoid") = True
                                drow.Item("voiduserunkid") = User._Object._Userunkid
                                drow.Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                                drow.Item("voidreason") = mstrVoidReason
                            End If

                            Dim dtRow As DataRow() = mdtTraining.Select("courseunkid = " & CInt(drow("courseunkid")) & " AND IsGrp = 1 AND AUD <> 'D'")
                            If dtRow.Length > 0 Then
                                dtRow(0)("total_emp") = CInt(dtRow(0)("total_emp")) - 1
                                dtRow(0).AcceptChanges()
                            End If

                            drow.AcceptChanges()

                            If mblnIsFinalizeTraining AndAlso mdtDelete IsNot Nothing Then
                                mdtDelete.ImportRow(drow)
                            End If

                        Next

                    End If

                    Dim dView As DataView = mdtTraining.DefaultView
                    dView.Sort = "courseunkid" : dView.RowFilter = "AUD <> 'D'"
                    mdtTraining = dView.ToTable
                    Call SetDataSource()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If dgvTrainingPriority.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, there is no data in the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim drRow() As DataRow = mdtTraining.Select("IsGrp=true AND total_emp = '0' AND AUD <>'D' ")

            If drRow.Length > 0 Then
                If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "There are some Course Groups that do not contain employee. Do you wish to Save?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                    Exit Sub
                Else
                    GoTo Save
                End If
            End If

Save:       Call SetValue(False)

            If mdtDelete IsNot Nothing AndAlso mdtDelete.Rows.Count > 0 Then
                mdtTraining.Merge(mdtDelete, True)
            End If


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objMTnAPriority._FormName = mstrModuleName
            objMTnAPriority._LoginEmployeeunkid = 0
            objMTnAPriority._ClientIP = getIP()
            objMTnAPriority._HostName = getHostName()
            objMTnAPriority._FromWeb = False
            objMTnAPriority._AuditUserId = User._Object._Userunkid
objMTnAPriority._CompanyUnkid = Company._Object._Companyunkid
            objMTnAPriority._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            blnFlag = objMTnAPriority.Update(mdtTraining)

            If blnFlag = False And objMTnAPriority._Message <> "" Then
                eZeeMsgBox.Show(objMTnAPriority._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                Me.Close()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnFinalSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinalSave.Click
        Dim blnFlag As Boolean = False
        Try
            If dgvTrainingPriority.Rows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, there is no data in the list."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'If Validation() Then

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "You are about to save this list as final list. After this you will not be able to edit this reference list." & vbCrLf & "Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim drRow() As DataRow = mdtTraining.Select("IsGrp = true AND total_emp = '0' AND AUD <> 'D'")

                If drRow.Length > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "There are some Course Groups that do not contain employee. Do you wish to Save?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        GoTo FSave
                    Else
                        Exit Sub
                    End If

                End If


FSave:          Call SetValue(True)

                If mdtDelete IsNot Nothing AndAlso mdtDelete.Rows.Count > 0 Then
                    mdtTraining.Merge(mdtDelete, True)
                End If


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objMTnAPriority._FormName = mstrModuleName
                objMTnAPriority._LoginEmployeeunkid = 0
                objMTnAPriority._ClientIP = getIP()
                objMTnAPriority._HostName = getHostName()
                objMTnAPriority._FromWeb = False
                objMTnAPriority._AuditUserId = User._Object._Userunkid
objMTnAPriority._CompanyUnkid = Company._Object._Companyunkid
                objMTnAPriority._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                blnFlag = objMTnAPriority.Update(mdtTraining)

                If blnFlag = False And objMTnAPriority._Message <> "" Then
                    eZeeMsgBox.Show(objMTnAPriority._Message, enMsgBoxStyle.Information)
                End If

                If blnFlag Then
                    mblnCancel = False
                    Me.Close()
                End If
            End If
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnFinalSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_SplitButtonClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles btnExport.SplitButtonClick
        Try
            mblnIsExportAction = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_SplitButtonClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSaveExport_SplitButtonClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles btnSaveExport.SplitButtonClick
        Try
            mblnIsExportAction = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveExport_SplitButtonClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " CheckBox Event(s) "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            If dgvEmployee.Rows.Count > 0 Then
                For Each dr As DataRow In mdtEmp.Rows
                    RemoveHandler dgvEmployee.CellContentClick, AddressOf dgvEmployee_CellContentClick
                    dr("IsCheck") = objchkEmployee.Checked
                    AddHandler dgvEmployee.CellContentClick, AddressOf dgvEmployee_CellContentClick
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkPerformanceEvalutation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPerformanceEvalutation.CheckedChanged
        Try
            If chkPerformanceEvalutation.CheckState = CheckState.Checked Then
                cboPeriod.Enabled = True : objbtnSearchPeriod.Enabled = True
            Else
                cboPeriod.SelectedValue = 0
                cboPeriod.Enabled = False : objbtnSearchPeriod.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkPerformanceEvalutation_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub mnuExportWithName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportWithName.Click
        Dim blnflag As Boolean = False
        Try
            If mdtTraining IsNot Nothing Then
                If mdtTraining.Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, there is no data to export."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If Validation() = False Then Exit Sub

                If txtRemark.Text.Trim.Length <= 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "You have not specified the remark.") & vbCrLf & Language.getMessage(mstrModuleName, 25, " Do you wish to export the list without remark?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                End If

                Dim drRow() As DataRow = mdtTraining.Select("IsGrp=true AND total_emp = '0' AND AUD <>'D' ")
                If drRow.Length > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "There are some Course Groups that do not contain employee. Do you wish to export?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                        If mblnIsExportAction = False Then
                            Call SetValue()

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objMTnAPriority._FormName = mstrModuleName
                            objMTnAPriority._LoginEmployeeunkid = 0
                            objMTnAPriority._ClientIP = getIP()
                            objMTnAPriority._HostName = getHostName()
                            objMTnAPriority._FromWeb = False
                            objMTnAPriority._AuditUserId = User._Object._Userunkid
objMTnAPriority._CompanyUnkid = Company._Object._Companyunkid
                            objMTnAPriority._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END

                            blnflag = objMTnAPriority.Insert(mdtTraining)
                            If blnflag = False And objMTnAPriority._Message <> "" Then
                                eZeeMsgBox.Show(objMTnAPriority._Message, enMsgBoxStyle.Information)
                            End If
                        End If

                        Dim objFile As New SaveFileDialog
                        objFile.OverwritePrompt = True
                        objFile.Filter = "Xls Files (*.xls) |*.xls"
                        If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                            Dim obj As New FileInfo(objFile.FileName)
                            Dim mstrFilePath As String = objFile.FileName.Substring(0, objFile.FileName.LastIndexOf(CChar("\")))
                            If Export_to_Excel(Not mblnIsExportAction, obj.Name, mstrFilePath, mdtTraining, True) Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, mstrFilePath)
                            End If
                        End If
                    End If
                Else
                    If mblnIsExportAction = False Then
                        Call SetValue()

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objMTnAPriority._FormName = mstrModuleName
                        objMTnAPriority._LoginEmployeeunkid = 0
                        objMTnAPriority._ClientIP = getIP()
                        objMTnAPriority._HostName = getHostName()
                        objMTnAPriority._FromWeb = False
                        objMTnAPriority._AuditUserId = User._Object._Userunkid
objMTnAPriority._CompanyUnkid = Company._Object._Companyunkid
                        objMTnAPriority._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        blnflag = objMTnAPriority.Insert(mdtTraining)

                        If blnflag = False And objMTnAPriority._Message <> "" Then
                            eZeeMsgBox.Show(objMTnAPriority._Message, enMsgBoxStyle.Information)
                        End If
                    End If

                    Dim objFile As New SaveFileDialog
                    objFile.OverwritePrompt = True
                    objFile.Filter = "Xls Files (*.xls) |*.xls"
                    If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        Dim obj As New FileInfo(objFile.FileName)
                        Dim mstrFilePath As String = objFile.FileName.Substring(0, objFile.FileName.LastIndexOf(CChar("\")))
                        If Export_to_Excel(Not mblnIsExportAction, obj.Name, mstrFilePath, mdtTraining, True) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, mstrFilePath)
                        End If
                    End If
                End If
                If mblnIsExportAction = False Then
                    If blnflag Then
                        mblnCancel = False
                        Me.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportWithName_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuExportWithOutName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportWithOutName.Click
        Dim blnflag As Boolean = False
        Try
            If mdtTraining IsNot Nothing Then
                If mdtTraining.Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, there is no data to export."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                If Validation() = False Then Exit Sub

                If txtRemark.Text.Trim.Length <= 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "You have not specified the remark.") & vbCrLf & Language.getMessage(mstrModuleName, 25, " Do you wish to export the list without remark?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                End If

                Dim drRow() As DataRow = mdtTraining.Select("IsGrp=true AND total_emp = '0' AND AUD <>'D' ")
                If drRow.Length > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "There are some Course Groups that do not contain employee. Do you wish to export?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        If mblnIsExportAction = False Then
                            Call SetValue()

                            'S.SANDEEP [28-May-2018] -- START
                            'ISSUE/ENHANCEMENT : {Audit Trails} 
                            objMTnAPriority._FormName = mstrModuleName
                            objMTnAPriority._LoginEmployeeunkid = 0
                            objMTnAPriority._ClientIP = getIP()
                            objMTnAPriority._HostName = getHostName()
                            objMTnAPriority._FromWeb = False
                            objMTnAPriority._AuditUserId = User._Object._Userunkid
objMTnAPriority._CompanyUnkid = Company._Object._Companyunkid
                            objMTnAPriority._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            'S.SANDEEP [28-May-2018] -- END

                            blnflag = objMTnAPriority.Insert(mdtTraining)
                            If blnflag = False And objMTnAPriority._Message <> "" Then
                                eZeeMsgBox.Show(objMTnAPriority._Message, enMsgBoxStyle.Information)
                            End If
                        End If


                        Dim objFile As New SaveFileDialog
                        objFile.OverwritePrompt = True
                        objFile.Filter = "Xls Files (*.xls) |*.xls"
                        If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                            Dim obj As New FileInfo(objFile.FileName)
                            Dim mstrFilePath As String = objFile.FileName.Substring(0, objFile.FileName.LastIndexOf(CChar("\")))
                            If Export_to_Excel(Not mblnIsExportAction, obj.Name, mstrFilePath, mdtTraining, False) Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, mstrFilePath)
                            End If
                        End If
                    End If
                Else
                    If mblnIsExportAction = False Then
                        Call SetValue()

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objMTnAPriority._FormName = mstrModuleName
                        objMTnAPriority._LoginEmployeeunkid = 0
                        objMTnAPriority._ClientIP = getIP()
                        objMTnAPriority._HostName = getHostName()
                        objMTnAPriority._FromWeb = False
                        objMTnAPriority._AuditUserId = User._Object._Userunkid
objMTnAPriority._CompanyUnkid = Company._Object._Companyunkid
                        objMTnAPriority._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        blnflag = objMTnAPriority.Insert(mdtTraining)

                        If blnflag = False And objMTnAPriority._Message <> "" Then
                            eZeeMsgBox.Show(objMTnAPriority._Message, enMsgBoxStyle.Information)
                        End If
                    End If

                    Dim objFile As New SaveFileDialog
                    objFile.OverwritePrompt = True
                    objFile.Filter = "Xls Files (*.xls) |*.xls"
                    If objFile.ShowDialog() = Windows.Forms.DialogResult.OK Then
                        Dim obj As New FileInfo(objFile.FileName)
                        Dim mstrFilePath As String = objFile.FileName.Substring(0, objFile.FileName.LastIndexOf(CChar("\")))
                        If Export_to_Excel(Not mblnIsExportAction, obj.Name, mstrFilePath, mdtTraining, False) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Data Exported successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                            Call ReportFunction.Open_ExportedFile(ConfigParameter._Object._OpenAfterExport, mstrFilePath)
                        End If
                    End If
                End If
                If mblnIsExportAction = False Then
                    If blnflag Then
                        mblnCancel = False
                        Me.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuExportWithOutName_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvEmployee.Rows.Count > 0 Then
                        If dgvEmployee.SelectedRows(0).Index = dgvEmployee.Rows(dgvEmployee.RowCount - 1).Index Then Exit Sub
                        dgvEmployee.Rows(dgvEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvEmployee.Rows.Count > 0 Then
                        If dgvEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvEmployee.Rows(dgvEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim strSearch As String = ""
        Try
            If txtSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhEName.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' OR " & _
                            dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearch.Text & "%'"
                objchkEmployee.Checked = False
                objchkEmployee.Enabled = False
            Else
                objchkEmployee.Enabled = True
            End If
            dgView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtAssessorEmp_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchCourse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCourse.Click
        Dim frm As New frmCommonSearch
        Try
            If cboCourse.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboCourse.ValueMember
                    .DisplayMember = cboCourse.DisplayMember
                    .DataSource = CType(cboCourse.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cboCourse.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCourse_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            If cboPeriod.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboPeriod.ValueMember
                    .DisplayMember = cboPeriod.DisplayMember
                    .DataSource = CType(cboPeriod.DataSource, DataTable)
                End With

                If frm.DisplayDialog Then
                    cboPeriod.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub dgvTrainingPriority_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTrainingPriority.CellContentClick, dgvTrainingPriority.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If dgvTrainingPriority.IsCurrentCellDirty Then
                dgvTrainingPriority.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvTrainingPriority.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        If dgvTrainingPriority.Rows(e.RowIndex).Cells(objdgcolhECollapse.Index).Value Is "-" Then
                            dgvTrainingPriority.Rows(e.RowIndex).Cells(objdgcolhECollapse.Index).Value = "+"
                        Else
                            dgvTrainingPriority.Rows(e.RowIndex).Cells(objdgcolhECollapse.Index).Value = "-"
                        End If
                        For i = e.RowIndex + 1 To dgvTrainingPriority.RowCount - 1
                            If CInt(dgvTrainingPriority.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvTrainingPriority.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvTrainingPriority.Rows(i).Visible = False Then
                                    dgvTrainingPriority.Rows(i).Visible = True
                                Else
                                    dgvTrainingPriority.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                    Case 1
                        Dim blnFlag As Boolean = CBool(dgvTrainingPriority.Rows(e.RowIndex).Cells(objdgcolhCheck.Index).Value)
                        For i = e.RowIndex + 1 To dgvTrainingPriority.RowCount - 1
                            If CInt(dgvTrainingPriority.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvTrainingPriority.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                dgvTrainingPriority.Rows(i).Cells(objdgcolhCheck.Index).Value = blnFlag
                            End If
                        Next
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTrainingPriority_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvTrainingPriority_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvTrainingPriority.DataError

    End Sub

    Private Sub dgvTrainingPriority_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTrainingPriority.CellEnter
        Try
            If e.ColumnIndex = dgcolhBusinessPriority.Index Or _
               e.ColumnIndex = dgcolhGAP.Index Or _
               e.ColumnIndex = dgcolhProvider.Index Or _
               e.ColumnIndex = dgcolhGoal.Index Or _
               e.ColumnIndex = dgcolhKRA.Index Or _
               e.ColumnIndex = dgcolhRanking.Index Or _
               e.ColumnIndex = dgcolhTraningSol.Index Or _
               e.ColumnIndex = dgcolhTraningObjective.Index Or _
               e.ColumnIndex = dgcolhtrainingVenue.Index Then
                SendKeys.Send("{F2}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTrainingPriority_CellEnter", mstrModuleName)
        End Try
    End Sub

    Private Sub RateKeyPress(ByVal sender As Object, ByVal e As Windows.Forms.KeyPressEventArgs)
        Try
            If dgvTrainingPriority.CurrentCell.ColumnIndex = dgcolhRanking.Index Then
                If Not (Asc(e.KeyChar) > 47 And Asc(e.KeyChar) < 58) Then
                    If Asc(e.KeyChar) = 8 Then Exit Sub
                    e.KeyChar = CChar("")
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "RateKeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub dgvTrainingPriority_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvTrainingPriority.EditingControlShowing
        Try
            If dgvTrainingPriority.CurrentCell.ColumnIndex = dgcolhRanking.Index And Not e.Control Is Nothing Then
                Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
                AddHandler txt.KeyPress, AddressOf RateKeyPress
            ElseIf dgvTrainingPriority.CurrentCell.ColumnIndex = dgcolhGAP.Index And Not e.Control Is Nothing Then
                mintSelectedCombo = 1
                Dim cbo As ComboBox = TryCast(e.Control, ComboBox)
                AddHandler cbo.Validating, AddressOf Combo_Validating
            ElseIf dgvTrainingPriority.CurrentCell.ColumnIndex = dgcolhBusinessPriority.Index And Not e.Control Is Nothing Then
                mintSelectedCombo = 2
                Dim cbo As ComboBox = TryCast(e.Control, ComboBox)
                AddHandler cbo.Validating, AddressOf Combo_Validating
            ElseIf dgvTrainingPriority.CurrentCell.ColumnIndex = dgcolhProvider.Index And Not e.Control Is Nothing Then
                mintSelectedCombo = 3
                Dim cbo As ComboBox = TryCast(e.Control, ComboBox)
                AddHandler cbo.Validating, AddressOf Combo_Validating
            ElseIf dgvTrainingPriority.CurrentCell.ColumnIndex = dgcolhGoal.Index And Not e.Control Is Nothing Then
                mintSelectedCombo = 4
                Dim cbo As ComboBox = TryCast(e.Control, ComboBox)
                AddHandler cbo.Validating, AddressOf Combo_Validating
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvTrainingPriority_EditingControlShowing", mstrModuleName)
        End Try
    End Sub

    Private Sub Combo_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Try
            Dim cbo As ComboBox = TryCast(sender, ComboBox)
            If cbo IsNot Nothing Then
                If CInt(cbo.SelectedValue) > 0 Then

                    'Pinkal (12 Oct 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS
                    dgvTrainingPriority.CurrentCell.Value = cbo.SelectedValue
                    'Pinkal (12 Oct 2012)-End

                    Select Case mintSelectedCombo
                        Case 1  'SKILL GAP
                            dgvTrainingPriority.Rows(dgvTrainingPriority.CurrentRow.Index).Cells(objdgcolhSkill_Gap.Index).Value = cbo.Text
                        Case 2  'BUSINESS IMPACT
                            dgvTrainingPriority.Rows(dgvTrainingPriority.CurrentRow.Index).Cells(objdgcolhbusiness.Index).Value = cbo.Text
                        Case 3  'PROVIDER
                            dgvTrainingPriority.Rows(dgvTrainingPriority.CurrentRow.Index).Cells(objdgcolhProvider.Index).Value = cbo.Text
                        Case 4  'STRATEGIC GOALS
                            dgvTrainingPriority.Rows(dgvTrainingPriority.CurrentRow.Index).Cells(objdgcolhStartegicGoal.Index).Value = cbo.Text
                    End Select
                    mintSelectedCombo = 0
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combo_Validating", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmployee.CellContentClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If dgvEmployee.IsCurrentCellDirty Then
                    dgvEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = mdtEmp.Select("IsCheck = true")

                RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

                If drRow.Length <= 0 Then
                    objchkEmployee.CheckState = CheckState.Unchecked
                ElseIf drRow.Length < dgvEmployee.Rows.Count Then
                    objchkEmployee.CheckState = CheckState.Indeterminate
                ElseIf drRow.Length = dgvEmployee.Rows.Count Then
                    objchkEmployee.CheckState = CheckState.Checked
                End If

                AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Me.Cursor = Cursors.WaitCursor
                If mdtTraining IsNot Nothing Then
                    mdtTraining.Rows.Clear()
                End If
                objTTnAPriority._PeriodUnkid = CInt(cboPeriod.SelectedValue)
                mdtTraining = objTTnAPriority._DataTable
                Call SetDataSource()
            Else
                dgvTrainingPriority.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

            Me.gbInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbTrainingPriority.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbTrainingPriority.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAdd.GradientBackColor = GUI._ButttonBackColor
            Me.btnAdd.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnSaveExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnFinalSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnFinalSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblCourse.Text = Language._Object.getCaption(Me.lblCourse.Name, Me.lblCourse.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.Name, Me.gbInfo.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.Name, Me.btnAdd.Text)
            Me.gbTrainingPriority.Text = Language._Object.getCaption(Me.gbTrainingPriority.Name, Me.gbTrainingPriority.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.btnSaveExport.Text = Language._Object.getCaption(Me.btnSaveExport.Name, Me.btnSaveExport.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
            Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
            Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
            Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
            Me.lblReferenceNo.Text = Language._Object.getCaption(Me.lblReferenceNo.Name, Me.lblReferenceNo.Text)
            Me.btnFinalSave.Text = Language._Object.getCaption(Me.btnFinalSave.Name, Me.btnFinalSave.Text)
            Me.chkPerformanceEvalutation.Text = Language._Object.getCaption(Me.chkPerformanceEvalutation.Name, Me.chkPerformanceEvalutation.Text)
            Me.mnuExportWithName.Text = Language._Object.getCaption(Me.mnuExportWithName.Name, Me.mnuExportWithName.Text)
            Me.mnuExportWithOutName.Text = Language._Object.getCaption(Me.mnuExportWithOutName.Name, Me.mnuExportWithOutName.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.dgcolhTrainingCourse.HeaderText = Language._Object.getCaption(Me.dgcolhTrainingCourse.Name, Me.dgcolhTrainingCourse.HeaderText)
            Me.dgcolhTotEmp.HeaderText = Language._Object.getCaption(Me.dgcolhTotEmp.Name, Me.dgcolhTotEmp.HeaderText)
            Me.dgcolhGAP.HeaderText = Language._Object.getCaption(Me.dgcolhGAP.Name, Me.dgcolhGAP.HeaderText)
            Me.dgcolhBusinessPriority.HeaderText = Language._Object.getCaption(Me.dgcolhBusinessPriority.Name, Me.dgcolhBusinessPriority.HeaderText)
            Me.dgcolhKRA.HeaderText = Language._Object.getCaption(Me.dgcolhKRA.Name, Me.dgcolhKRA.HeaderText)
            Me.dgcolhRanking.HeaderText = Language._Object.getCaption(Me.dgcolhRanking.Name, Me.dgcolhRanking.HeaderText)
            Me.dgcolhTraningSol.HeaderText = Language._Object.getCaption(Me.dgcolhTraningSol.Name, Me.dgcolhTraningSol.HeaderText)
            Me.dgcolhTraningObjective.HeaderText = Language._Object.getCaption(Me.dgcolhTraningObjective.Name, Me.dgcolhTraningObjective.HeaderText)
            Me.dgcolhProvider.HeaderText = Language._Object.getCaption(Me.dgcolhProvider.Name, Me.dgcolhProvider.HeaderText)
            Me.dgcolhtrainingVenue.HeaderText = Language._Object.getCaption(Me.dgcolhtrainingVenue.Name, Me.dgcolhtrainingVenue.HeaderText)
            Me.dgcolhGoal.HeaderText = Language._Object.getCaption(Me.dgcolhGoal.Name, Me.dgcolhGoal.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Training Course is mandatory information. Please select Training Course to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Employee is mandatory information. Please select atleast one employee to continue.")
            Language.setMessage(mstrModuleName, 3, "This Course is already present in the list. You can continue adding selected employee(s) to this Course Group." & vbCrLf & "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 4, "Please select atleast one employee or training course to remove from the list.")
            Language.setMessage(mstrModuleName, 5, "Are you sure you want to remove selected information from the list?")
            Language.setMessage(mstrModuleName, 6, "Sorry, there is no data to export.")
            Language.setMessage(mstrModuleName, 7, "Sorry, there is no data to export.")
            Language.setMessage(mstrModuleName, 8, "There are some Course Groups that do not contain employee. Do you wish to export?")
            Language.setMessage(mstrModuleName, 9, "Data Exported successfully.")
            Language.setMessage(mstrModuleName, 10, "Training Priorities")
            Language.setMessage(mstrModuleName, 11, "Sorry, there is no data in the list.")
            Language.setMessage(mstrModuleName, 12, "Period :")
            Language.setMessage(mstrModuleName, 13, "Business Priority is compulsory information.Please Select Business Priority from the list.")
            Language.setMessage(mstrModuleName, 14, "KRA cannot be blank. KRA is required information.")
            Language.setMessage(mstrModuleName, 15, "Priority Ranking cannot be blank. Priority Ranking is required information.")
            Language.setMessage(mstrModuleName, 16, "Prefered Provider is compulsory information.Please Select Prefered Provider from the list.")
            Language.setMessage(mstrModuleName, 17, "Training Venue cannot be blank.Training Venue is required information.")
            Language.setMessage(mstrModuleName, 18, "Training Solution cannot be blank.Training Solution is required information.")
            Language.setMessage(mstrModuleName, 19, "Training Objective cannot be blank.Training Objective is required information.")
            Language.setMessage(mstrModuleName, 20, "Strategic Goal is compulsory information.Please Select Strategic Goal from the list.")
            Language.setMessage(mstrModuleName, 21, "Priority Ranking cannot be same.Please define unique priority ranking.")
            Language.setMessage(mstrModuleName, 22, "Skill GAP Priority is compulsory information.Please Select Skill GAP Priority from the list.")
            Language.setMessage(mstrModuleName, 23, "There are some Course Groups that do not contain employee. Do you wish to Save?")
            Language.setMessage(mstrModuleName, 24, "You have not specified the remark.")
            Language.setMessage(mstrModuleName, 25, " Do you wish to export the list without remark?")
            Language.setMessage(mstrModuleName, 26, "You are about to save this list as final list. After this you will not be able to edit this reference list." & vbCrLf & "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 27, "Some of the employee(s) are already assigned to this particular course group and will not be added again.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class