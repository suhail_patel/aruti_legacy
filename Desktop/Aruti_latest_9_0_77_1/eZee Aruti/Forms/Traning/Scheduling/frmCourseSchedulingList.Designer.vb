﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCourseSchedulingList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCourseSchedulingList))
        Me.pnlCourseScheduling = New System.Windows.Forms.Panel
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchInstitute = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchCourse = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchQualification = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchQGrp = New eZee.Common.eZeeGradientButton
        Me.radActive = New System.Windows.Forms.RadioButton
        Me.lblAmountTo = New System.Windows.Forms.Label
        Me.radCancel = New System.Windows.Forms.RadioButton
        Me.txtAmountTo = New eZee.TextBox.NumericTextBox
        Me.radVoid = New System.Windows.Forms.RadioButton
        Me.radAll = New System.Windows.Forms.RadioButton
        Me.txtAmountFrom = New eZee.TextBox.NumericTextBox
        Me.cboCourseMaster = New System.Windows.Forms.ComboBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblTrainingTitle = New System.Windows.Forms.Label
        Me.cboQualification = New System.Windows.Forms.ComboBox
        Me.lblQualification = New System.Windows.Forms.Label
        Me.cboQualificationGroup = New System.Windows.Forms.ComboBox
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.lblTraningCourse = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.lblTraningInstitute = New System.Windows.Forms.Label
        Me.cboInstitute = New System.Windows.Forms.ComboBox
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lvCourseScheduling = New System.Windows.Forms.ListView
        Me.colhCourse = New System.Windows.Forms.ColumnHeader
        Me.colhStartdateTime = New System.Windows.Forms.ColumnHeader
        Me.colhEndDateTime = New System.Windows.Forms.ColumnHeader
        Me.colhInstitute = New System.Windows.Forms.ColumnHeader
        Me.colhVenue = New System.Windows.Forms.ColumnHeader
        Me.colhContactPersonAndNo = New System.Windows.Forms.ColumnHeader
        Me.colhTotalCost = New System.Windows.Forms.ColumnHeader
        Me.colhTitle = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnPrint = New eZee.Common.eZeeSplitButton
        Me.mnuPrint = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuActiveEmp = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuInActiveEmp = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrintCourse = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuActiveEmpPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuInActiveEmpPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.lblAllocType = New System.Windows.Forms.Label
        Me.cboAllocType = New System.Windows.Forms.ComboBox
        Me.cboAllocation = New System.Windows.Forms.ComboBox
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.objbtnSearchAllocation = New eZee.Common.eZeeGradientButton
        Me.lblTrainingStatus = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.colhAllocation = New System.Windows.Forms.ColumnHeader
        Me.pnlCourseScheduling.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.mnuPrint.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlCourseScheduling
        '
        Me.pnlCourseScheduling.Controls.Add(Me.gbFilterCriteria)
        Me.pnlCourseScheduling.Controls.Add(Me.lvCourseScheduling)
        Me.pnlCourseScheduling.Controls.Add(Me.objFooter)
        Me.pnlCourseScheduling.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlCourseScheduling.Location = New System.Drawing.Point(0, 0)
        Me.pnlCourseScheduling.Name = "pnlCourseScheduling"
        Me.pnlCourseScheduling.Size = New System.Drawing.Size(957, 547)
        Me.pnlCourseScheduling.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lblTrainingStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objlblCaption)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.dtpEndDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblAllocType)
        Me.gbFilterCriteria.Controls.Add(Me.lblEndDate)
        Me.gbFilterCriteria.Controls.Add(Me.cboAllocType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchInstitute)
        Me.gbFilterCriteria.Controls.Add(Me.lblStartDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchCourse)
        Me.gbFilterCriteria.Controls.Add(Me.dtpStartDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblAmount)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchQualification)
        Me.gbFilterCriteria.Controls.Add(Me.txtAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchQGrp)
        Me.gbFilterCriteria.Controls.Add(Me.cboCourseMaster)
        Me.gbFilterCriteria.Controls.Add(Me.lblTrainingTitle)
        Me.gbFilterCriteria.Controls.Add(Me.txtAmountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.cboQualification)
        Me.gbFilterCriteria.Controls.Add(Me.lblQualification)
        Me.gbFilterCriteria.Controls.Add(Me.cboQualificationGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblTraningCourse)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lblTraningInstitute)
        Me.gbFilterCriteria.Controls.Add(Me.cboInstitute)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 64)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(933, 115)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchInstitute
        '
        Me.objbtnSearchInstitute.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchInstitute.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchInstitute.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchInstitute.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchInstitute.BorderSelected = False
        Me.objbtnSearchInstitute.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchInstitute.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchInstitute.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchInstitute.Location = New System.Drawing.Point(903, 33)
        Me.objbtnSearchInstitute.Name = "objbtnSearchInstitute"
        Me.objbtnSearchInstitute.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchInstitute.TabIndex = 27
        '
        'objbtnSearchCourse
        '
        Me.objbtnSearchCourse.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCourse.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCourse.BorderSelected = False
        Me.objbtnSearchCourse.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCourse.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCourse.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCourse.Location = New System.Drawing.Point(281, 33)
        Me.objbtnSearchCourse.Name = "objbtnSearchCourse"
        Me.objbtnSearchCourse.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCourse.TabIndex = 24
        '
        'objbtnSearchQualification
        '
        Me.objbtnSearchQualification.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchQualification.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchQualification.BorderSelected = False
        Me.objbtnSearchQualification.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchQualification.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchQualification.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchQualification.Location = New System.Drawing.Point(281, 86)
        Me.objbtnSearchQualification.Name = "objbtnSearchQualification"
        Me.objbtnSearchQualification.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchQualification.TabIndex = 26
        '
        'objbtnSearchQGrp
        '
        Me.objbtnSearchQGrp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchQGrp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchQGrp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchQGrp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchQGrp.BorderSelected = False
        Me.objbtnSearchQGrp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchQGrp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchQGrp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchQGrp.Location = New System.Drawing.Point(281, 59)
        Me.objbtnSearchQGrp.Name = "objbtnSearchQGrp"
        Me.objbtnSearchQGrp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchQGrp.TabIndex = 25
        '
        'radActive
        '
        Me.radActive.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radActive.Location = New System.Drawing.Point(257, 10)
        Me.radActive.Name = "radActive"
        Me.radActive.Size = New System.Drawing.Size(81, 17)
        Me.radActive.TabIndex = 21
        Me.radActive.TabStop = True
        Me.radActive.Text = "Active"
        Me.radActive.UseVisualStyleBackColor = True
        Me.radActive.Visible = False
        '
        'lblAmountTo
        '
        Me.lblAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmountTo.Location = New System.Drawing.Point(769, 89)
        Me.lblAmountTo.Name = "lblAmountTo"
        Me.lblAmountTo.Size = New System.Drawing.Size(48, 15)
        Me.lblAmountTo.TabIndex = 18
        Me.lblAmountTo.Text = "To"
        Me.lblAmountTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radCancel
        '
        Me.radCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radCancel.Location = New System.Drawing.Point(344, 10)
        Me.radCancel.Name = "radCancel"
        Me.radCancel.Size = New System.Drawing.Size(81, 17)
        Me.radCancel.TabIndex = 23
        Me.radCancel.TabStop = True
        Me.radCancel.Text = "Cancel"
        Me.radCancel.UseVisualStyleBackColor = True
        Me.radCancel.Visible = False
        '
        'txtAmountTo
        '
        Me.txtAmountTo.AllowNegative = False
        Me.txtAmountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmountTo.DigitsInGroup = 0
        Me.txtAmountTo.Flags = 65536
        Me.txtAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountTo.Location = New System.Drawing.Point(824, 86)
        Me.txtAmountTo.MaxDecimalPlaces = 6
        Me.txtAmountTo.MaxWholeDigits = 21
        Me.txtAmountTo.Name = "txtAmountTo"
        Me.txtAmountTo.Prefix = ""
        Me.txtAmountTo.RangeMax = 1.7976931348623157E+308
        Me.txtAmountTo.RangeMin = -1.7976931348623157E+308
        Me.txtAmountTo.Size = New System.Drawing.Size(73, 21)
        Me.txtAmountTo.TabIndex = 19
        Me.txtAmountTo.Text = "0"
        Me.txtAmountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'radVoid
        '
        Me.radVoid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radVoid.Location = New System.Drawing.Point(257, 35)
        Me.radVoid.Name = "radVoid"
        Me.radVoid.Size = New System.Drawing.Size(81, 17)
        Me.radVoid.TabIndex = 22
        Me.radVoid.TabStop = True
        Me.radVoid.Text = "Void"
        Me.radVoid.UseVisualStyleBackColor = True
        Me.radVoid.Visible = False
        '
        'radAll
        '
        Me.radAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAll.Location = New System.Drawing.Point(344, 35)
        Me.radAll.Name = "radAll"
        Me.radAll.Size = New System.Drawing.Size(81, 17)
        Me.radAll.TabIndex = 24
        Me.radAll.TabStop = True
        Me.radAll.Text = "All"
        Me.radAll.UseVisualStyleBackColor = True
        Me.radAll.Visible = False
        '
        'txtAmountFrom
        '
        Me.txtAmountFrom.AllowNegative = False
        Me.txtAmountFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmountFrom.DigitsInGroup = 0
        Me.txtAmountFrom.Flags = 65536
        Me.txtAmountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountFrom.Location = New System.Drawing.Point(824, 59)
        Me.txtAmountFrom.MaxDecimalPlaces = 6
        Me.txtAmountFrom.MaxWholeDigits = 21
        Me.txtAmountFrom.Name = "txtAmountFrom"
        Me.txtAmountFrom.Prefix = ""
        Me.txtAmountFrom.RangeMax = 1.7976931348623157E+308
        Me.txtAmountFrom.RangeMin = -1.7976931348623157E+308
        Me.txtAmountFrom.Size = New System.Drawing.Size(73, 21)
        Me.txtAmountFrom.TabIndex = 17
        Me.txtAmountFrom.Text = "0"
        Me.txtAmountFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboCourseMaster
        '
        Me.cboCourseMaster.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCourseMaster.DropDownWidth = 350
        Me.cboCourseMaster.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCourseMaster.FormattingEnabled = True
        Me.cboCourseMaster.Location = New System.Drawing.Point(115, 33)
        Me.cboCourseMaster.Name = "cboCourseMaster"
        Me.cboCourseMaster.Size = New System.Drawing.Size(160, 21)
        Me.cboCourseMaster.TabIndex = 26
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(769, 62)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(48, 15)
        Me.lblAmount.TabIndex = 16
        Me.lblAmount.Text = "Amount"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(659, 59)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpStartDate.TabIndex = 3
        '
        'lblTrainingTitle
        '
        Me.lblTrainingTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainingTitle.Location = New System.Drawing.Point(8, 36)
        Me.lblTrainingTitle.Name = "lblTrainingTitle"
        Me.lblTrainingTitle.Size = New System.Drawing.Size(101, 15)
        Me.lblTrainingTitle.TabIndex = 0
        Me.lblTrainingTitle.Text = "Course"
        Me.lblTrainingTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboQualification
        '
        Me.cboQualification.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualification.DropDownWidth = 350
        Me.cboQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualification.FormattingEnabled = True
        Me.cboQualification.Location = New System.Drawing.Point(115, 86)
        Me.cboQualification.Name = "cboQualification"
        Me.cboQualification.Size = New System.Drawing.Size(160, 21)
        Me.cboQualification.TabIndex = 12
        '
        'lblQualification
        '
        Me.lblQualification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQualification.Location = New System.Drawing.Point(8, 89)
        Me.lblQualification.Name = "lblQualification"
        Me.lblQualification.Size = New System.Drawing.Size(101, 15)
        Me.lblQualification.TabIndex = 11
        Me.lblQualification.Text = "Qualification"
        Me.lblQualification.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboQualificationGroup
        '
        Me.cboQualificationGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboQualificationGroup.DropDownWidth = 350
        Me.cboQualificationGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboQualificationGroup.FormattingEnabled = True
        Me.cboQualificationGroup.Location = New System.Drawing.Point(115, 59)
        Me.cboQualificationGroup.Name = "cboQualificationGroup"
        Me.cboQualificationGroup.Size = New System.Drawing.Size(160, 21)
        Me.cboQualificationGroup.TabIndex = 10
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(659, 86)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpEndDate.TabIndex = 5
        '
        'lblTraningCourse
        '
        Me.lblTraningCourse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTraningCourse.Location = New System.Drawing.Point(8, 62)
        Me.lblTraningCourse.Name = "lblTraningCourse"
        Me.lblTraningCourse.Size = New System.Drawing.Size(101, 15)
        Me.lblTraningCourse.TabIndex = 9
        Me.lblTraningCourse.Text = "Qualification Group"
        Me.lblTraningCourse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(908, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(884, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(592, 62)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(61, 15)
        Me.lblStartDate.TabIndex = 2
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTraningInstitute
        '
        Me.lblTraningInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTraningInstitute.Location = New System.Drawing.Point(592, 36)
        Me.lblTraningInstitute.Name = "lblTraningInstitute"
        Me.lblTraningInstitute.Size = New System.Drawing.Size(61, 15)
        Me.lblTraningInstitute.TabIndex = 13
        Me.lblTraningInstitute.Text = "Institute"
        Me.lblTraningInstitute.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboInstitute
        '
        Me.cboInstitute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInstitute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInstitute.FormattingEnabled = True
        Me.cboInstitute.Location = New System.Drawing.Point(659, 33)
        Me.cboInstitute.Name = "cboInstitute"
        Me.cboInstitute.Size = New System.Drawing.Size(238, 21)
        Me.cboInstitute.TabIndex = 14
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(592, 89)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(61, 15)
        Me.lblEndDate.TabIndex = 4
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvCourseScheduling
        '
        Me.lvCourseScheduling.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCourse, Me.colhStartdateTime, Me.colhEndDateTime, Me.colhInstitute, Me.colhVenue, Me.colhContactPersonAndNo, Me.colhTotalCost, Me.colhTitle, Me.colhAllocation})
        Me.lvCourseScheduling.FullRowSelect = True
        Me.lvCourseScheduling.GridLines = True
        Me.lvCourseScheduling.Location = New System.Drawing.Point(12, 185)
        Me.lvCourseScheduling.Name = "lvCourseScheduling"
        Me.lvCourseScheduling.Size = New System.Drawing.Size(932, 300)
        Me.lvCourseScheduling.TabIndex = 0
        Me.lvCourseScheduling.UseCompatibleStateImageBehavior = False
        Me.lvCourseScheduling.View = System.Windows.Forms.View.Details
        '
        'colhCourse
        '
        Me.colhCourse.DisplayIndex = 1
        Me.colhCourse.Tag = "colhCourse"
        Me.colhCourse.Text = "Course"
        Me.colhCourse.Width = 115
        '
        'colhStartdateTime
        '
        Me.colhStartdateTime.DisplayIndex = 2
        Me.colhStartdateTime.Tag = "colhStartdateTime"
        Me.colhStartdateTime.Text = "Start Date & Time"
        Me.colhStartdateTime.Width = 100
        '
        'colhEndDateTime
        '
        Me.colhEndDateTime.DisplayIndex = 3
        Me.colhEndDateTime.Tag = "colhEndDateTime"
        Me.colhEndDateTime.Text = "End Date & Time"
        Me.colhEndDateTime.Width = 95
        '
        'colhInstitute
        '
        Me.colhInstitute.DisplayIndex = 4
        Me.colhInstitute.Tag = "colhInstitute"
        Me.colhInstitute.Text = "Institute"
        Me.colhInstitute.Width = 105
        '
        'colhVenue
        '
        Me.colhVenue.DisplayIndex = 5
        Me.colhVenue.Tag = "colhVenue"
        Me.colhVenue.Text = "Venue"
        Me.colhVenue.Width = 110
        '
        'colhContactPersonAndNo
        '
        Me.colhContactPersonAndNo.DisplayIndex = 6
        Me.colhContactPersonAndNo.Tag = "colhContactPersonAndNo"
        Me.colhContactPersonAndNo.Text = "Contact Person & No."
        Me.colhContactPersonAndNo.Width = 118
        '
        'colhTotalCost
        '
        Me.colhTotalCost.DisplayIndex = 8
        Me.colhTotalCost.Tag = "colhTotalCost"
        Me.colhTotalCost.Text = "Total Cost"
        Me.colhTotalCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhTotalCost.Width = 90
        '
        'colhTitle
        '
        Me.colhTitle.DisplayIndex = 0
        Me.colhTitle.Tag = "colhTitle"
        Me.colhTitle.Text = "Training Title"
        Me.colhTitle.Width = 126
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnPrint)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.radActive)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.radCancel)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.radVoid)
        Me.objFooter.Controls.Add(Me.radAll)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 492)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(957, 55)
        Me.objFooter.TabIndex = 4
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(745, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 151
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.BorderColor = System.Drawing.Color.Black
        Me.btnPrint.ContextMenuStrip = Me.mnuPrint
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPrint.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnPrint.Location = New System.Drawing.Point(12, 13)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.ShowDefaultBorderColor = True
        Me.btnPrint.Size = New System.Drawing.Size(97, 30)
        Me.btnPrint.SplitButtonMenu = Me.mnuPrint
        Me.btnPrint.TabIndex = 128
        Me.btnPrint.Text = "&Print"
        '
        'mnuPrint
        '
        Me.mnuPrint.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPreview, Me.mnuPrintCourse})
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(116, 48)
        '
        'mnuPreview
        '
        Me.mnuPreview.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuActiveEmp, Me.mnuInActiveEmp})
        Me.mnuPreview.Name = "mnuPreview"
        Me.mnuPreview.Size = New System.Drawing.Size(115, 22)
        Me.mnuPreview.Text = "P&review"
        '
        'mnuActiveEmp
        '
        Me.mnuActiveEmp.Name = "mnuActiveEmp"
        Me.mnuActiveEmp.Size = New System.Drawing.Size(253, 22)
        Me.mnuActiveEmp.Tag = "With Active Employee"
        Me.mnuActiveEmp.Text = "With Active Employee"
        '
        'mnuInActiveEmp
        '
        Me.mnuInActiveEmp.Name = "mnuInActiveEmp"
        Me.mnuInActiveEmp.Size = New System.Drawing.Size(253, 22)
        Me.mnuInActiveEmp.Text = "With Including InActive Employee"
        '
        'mnuPrintCourse
        '
        Me.mnuPrintCourse.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuActiveEmpPrint, Me.mnuInActiveEmpPrint})
        Me.mnuPrintCourse.Name = "mnuPrintCourse"
        Me.mnuPrintCourse.Size = New System.Drawing.Size(115, 22)
        Me.mnuPrintCourse.Text = "&Print"
        '
        'mnuActiveEmpPrint
        '
        Me.mnuActiveEmpPrint.Name = "mnuActiveEmpPrint"
        Me.mnuActiveEmpPrint.Size = New System.Drawing.Size(253, 22)
        Me.mnuActiveEmpPrint.Text = "With Active Employee"
        '
        'mnuInActiveEmpPrint
        '
        Me.mnuInActiveEmpPrint.Name = "mnuInActiveEmpPrint"
        Me.mnuInActiveEmpPrint.Size = New System.Drawing.Size(253, 22)
        Me.mnuInActiveEmpPrint.Text = "With Including InActive Employee"
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(642, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 150
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(539, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 149
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(848, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 125
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(957, 58)
        Me.eZeeHeader.TabIndex = 1
        Me.eZeeHeader.Title = "Course Scheduling List"
        '
        'lblAllocType
        '
        Me.lblAllocType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocType.Location = New System.Drawing.Point(308, 62)
        Me.lblAllocType.Name = "lblAllocType"
        Me.lblAllocType.Size = New System.Drawing.Size(87, 15)
        Me.lblAllocType.TabIndex = 66
        Me.lblAllocType.Text = "Alloc. Type"
        Me.lblAllocType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocType
        '
        Me.cboAllocType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocType.DropDownWidth = 145
        Me.cboAllocType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocType.FormattingEnabled = True
        Me.cboAllocType.Location = New System.Drawing.Point(398, 59)
        Me.cboAllocType.Name = "cboAllocType"
        Me.cboAllocType.Size = New System.Drawing.Size(161, 21)
        Me.cboAllocType.TabIndex = 65
        '
        'cboAllocation
        '
        Me.cboAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation.DropDownWidth = 300
        Me.cboAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation.FormattingEnabled = True
        Me.cboAllocation.Location = New System.Drawing.Point(398, 86)
        Me.cboAllocation.Name = "cboAllocation"
        Me.cboAllocation.Size = New System.Drawing.Size(161, 21)
        Me.cboAllocation.TabIndex = 67
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(308, 89)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(87, 15)
        Me.objlblCaption.TabIndex = 68
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchAllocation
        '
        Me.objbtnSearchAllocation.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAllocation.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAllocation.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAllocation.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAllocation.BorderSelected = False
        Me.objbtnSearchAllocation.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAllocation.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAllocation.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAllocation.Location = New System.Drawing.Point(565, 86)
        Me.objbtnSearchAllocation.Name = "objbtnSearchAllocation"
        Me.objbtnSearchAllocation.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAllocation.TabIndex = 69
        '
        'lblTrainingStatus
        '
        Me.lblTrainingStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrainingStatus.Location = New System.Drawing.Point(308, 36)
        Me.lblTrainingStatus.Name = "lblTrainingStatus"
        Me.lblTrainingStatus.Size = New System.Drawing.Size(87, 15)
        Me.lblTrainingStatus.TabIndex = 68
        Me.lblTrainingStatus.Text = "Status"
        Me.lblTrainingStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 145
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(398, 33)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(161, 21)
        Me.cboStatus.TabIndex = 67
        '
        'colhAllocation
        '
        Me.colhAllocation.DisplayIndex = 7
        Me.colhAllocation.Text = "Binded Allocation"
        Me.colhAllocation.Width = 150
        '
        'frmCourseSchedulingList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(957, 547)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.pnlCourseScheduling)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCourseSchedulingList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Course Scheduling List"
        Me.pnlCourseScheduling.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.mnuPrint.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlCourseScheduling As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents lvCourseScheduling As System.Windows.Forms.ListView
    Friend WithEvents colhCourse As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhStartdateTime As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEndDateTime As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhInstitute As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhVenue As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhContactPersonAndNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhTotalCost As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboQualificationGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cboInstitute As System.Windows.Forms.ComboBox
    Friend WithEvents lblTraningCourse As System.Windows.Forms.Label
    Friend WithEvents lblTraningInstitute As System.Windows.Forms.Label
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboQualification As System.Windows.Forms.ComboBox
    Friend WithEvents lblQualification As System.Windows.Forms.Label
    Friend WithEvents txtAmountFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents lblAmountTo As System.Windows.Forms.Label
    Friend WithEvents txtAmountTo As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTrainingTitle As System.Windows.Forms.Label
    Friend WithEvents colhTitle As System.Windows.Forms.ColumnHeader
    Friend WithEvents radAll As System.Windows.Forms.RadioButton
    Friend WithEvents radCancel As System.Windows.Forms.RadioButton
    Friend WithEvents radVoid As System.Windows.Forms.RadioButton
    Friend WithEvents radActive As System.Windows.Forms.RadioButton
    Friend WithEvents btnPrint As eZee.Common.eZeeSplitButton
    Friend WithEvents mnuPrint As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuPrintCourse As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuActiveEmp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuInActiveEmp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuActiveEmpPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuInActiveEmpPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboCourseMaster As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchInstitute As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchCourse As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchQualification As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchQGrp As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents cboAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllocType As System.Windows.Forms.Label
    Friend WithEvents cboAllocType As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchAllocation As eZee.Common.eZeeGradientButton
    Friend WithEvents lblTrainingStatus As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents colhAllocation As System.Windows.Forms.ColumnHeader
End Class
