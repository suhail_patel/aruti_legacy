﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language
'Sandeep [ 07 FEB 2011 ] -- START
Imports ArutiReports
'Sandeep [ 07 FEB 2011 ] -- END 


Public Class frmCourseSchedulingList

#Region " Private Varaibles "
    Private objTraining As clsTraining_Scheduling
    Private ReadOnly mstrModuleName As String = "frmCourseSchedulingList"
    Private mblnFormLoad As Boolean = False
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objYear As New clsMasterData
        Dim objCommon As New clsCommon_Master
        Dim objInstitute As New clsinstitute_master
        Dim objQualification As New clsqualification_master
        Try
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "Course")
            With cboQualificationGroup
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Course")
                .SelectedValue = 0
            End With



            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.RESULT_GROUP, True, "ResultGroup")
            'With cboQualification
            '    .ValueMember = "masterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("ResultGroup")
            '    .SelectedValue = 0
            'End With
            dsList = objQualification.GetComboList("List", True)
            With cboQualification
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboCourseMaster
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [ 18 FEB 2012 ] -- END

            dsList = objInstitute.getListForCombo(False, "Institute", True)
            With cboInstitute
                .ValueMember = "instituteunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Institute")
                .SelectedValue = 0
            End With

            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            With cboStatus
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 7, "All"))
                .Items.Add(Language.getMessage(mstrModuleName, 8, "Active"))
                .Items.Add(Language.getMessage(mstrModuleName, 9, "Cancel"))
                .Items.Add(Language.getMessage(mstrModuleName, 10, "Void"))
                .SelectedIndex = 0
            End With

            Dim objMData As New clsMasterData
            dsList = objMData.GetEAllocation_Notification("List")
            Dim drow As DataRow = dsList.Tables("List").NewRow
            With drow
                .Item("Id") = 0
                .Item("Name") = Language.getMessage(mstrModuleName, 11, "Select")
                dsList.Tables("List").Rows.InsertAt(drow, 0)
            End With
            With cboAllocType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'S.SANDEEP [08-FEB-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsTraining As New DataSet
        Dim lvItem As ListViewItem
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Try

            If User._Object.Privilege._AllowToViewTrainingSchedulingList = True Then                'Pinkal (02-Jul-2012) -- Start

                dsTraining = objTraining.GetList("Training")

                'Anjan (10 Feb 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                'If txtTitle.Text.Trim <> "" Then
                '    StrSearching &= "AND course_title LIKE '%" & txtTitle.Text & "%'" & " "
                'End If

                'Anjan (10 Feb 2012)-End


                'S.SANDEEP [ 18 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If txtVenue.Text.Trim <> "" Then
                '    StrSearching &= "AND training_venue LIKE '%" & txtVenue.Text & "%'" & " "
                'End If
                'S.SANDEEP [ 18 FEB 2012 ] -- END


                'S.SANDEEP [ 15 SEP 2011 ] -- START
                'ENHANCEMENT : CODE OPTIMIZATION
                'If txtAmountFrom.Text.Trim <> "" AndAlso txtAmountTo.Text.Trim <> "" Then
                '    StrSearching &= "AND TotalCost >= " & txtAmountFrom.Decimal & " AND TotalCost <= " & _
                '                    txtAmountTo.Decimal & " "
                'End If
                If txtAmountFrom.Decimal > 0 AndAlso txtAmountTo.Decimal > 0 Then
                    StrSearching &= "AND TotalCost >= " & txtAmountFrom.Decimal & " AND TotalCost <= " & _
                                    txtAmountTo.Decimal & " "
                End If
                'S.SANDEEP [ 15 SEP 2011 ] -- END 

                If CInt(cboQualificationGroup.SelectedValue) > 0 Then
                    StrSearching &= "AND qualificationgroupunkid = " & CInt(cboQualificationGroup.SelectedValue) & " "
                End If

                If CInt(cboInstitute.SelectedValue) > 0 Then
                    StrSearching &= "AND traininginstituteunkid = " & CInt(cboInstitute.SelectedValue) & " "
                End If

                'S.SANDEEP [ 24 FEB 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES [<TRAINING>]
                'If CInt(cboQualification.SelectedValue) > 0 Then
                '    StrSearching &= "AND resultgroupunkid = " & CInt(cboQualification.SelectedValue) & " "
                'End If

                If CInt(cboQualification.SelectedValue) > 0 Then
                    StrSearching &= "AND qualificationunkid = " & CInt(cboQualification.SelectedValue) & " "
                End If

                If CInt(cboCourseMaster.SelectedValue) > 0 Then
                    StrSearching &= "AND CourseId = " & CInt(cboCourseMaster.SelectedValue) & " "
                End If
                'S.SANDEEP [ 24 FEB 2012 ] -- END 


                If dtpStartDate.Checked = True And dtpEndDate.Checked = True Then
                    'Sandeep [ 09 Oct 2010 ] -- Start
                    'Issues Reported by Vimal
                    'StrSearching &= "AND StDate >= '" & eZeeDate.convertDate(dtpStartDate.Value) & "' AND EdDate <= '" & _
                    '                 eZeeDate.convertDate(dtpEndDate.Value)  & " "

                    StrSearching &= "AND StDate >= '" & eZeeDate.convertDate(dtpStartDate.Value) & "' AND EdDate <= '" & _
                                     eZeeDate.convertDate(dtpEndDate.Value) & "'" & " "
                    'Sandeep [ 09 Oct 2010 ] -- End 
                End If

                'S.SANDEEP [08-FEB-2017] -- START
                'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
                'If radActive.Checked Then
                '    StrSearching &= "AND isvoid = 0 AND iscancel = 0 "
                'ElseIf radVoid.Checked Then
                '    StrSearching &= "AND isvoid = 1 "
                'ElseIf radCancel.Checked Then
                '    StrSearching &= "AND iscancel = 1 "
                'End If
                Select Case cboStatus.SelectedIndex
                    Case 1  'ACTIVE
                        StrSearching &= "AND isvoid = 0 AND iscancel = 0 "
                    Case 2  'CANCEL
                        StrSearching &= "AND iscancel = 1 "
                    Case 3  'VOID
                        StrSearching &= "AND isvoid = 1 "
                End Select

                If CInt(cboAllocType.SelectedValue) > 0 Then
                    StrSearching &= "AND allocationtypeunkid = '" & CInt(cboAllocType.SelectedValue) & "' "
                End If
                'S.SANDEEP [08-FEB-2017] -- END


                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                    dtTable = New DataView(dsTraining.Tables("Training"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsTraining.Tables("Training"), "", "", DataViewRowState.CurrentRows).ToTable
                End If


                lvCourseScheduling.Items.Clear()
                For Each drRow As DataRow In dtTable.Rows

                    'S.SANDEEP [08-FEB-2017] -- START
                    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
                    If CInt(cboAllocation.SelectedValue) > 0 Then
                        If Array.IndexOf(drRow.Item("allocationids").ToString.Split(CChar(",")), CStr(cboAllocation.SelectedValue)) = -1 Then
                            Continue For
                        End If
                    End If
                    'S.SANDEEP [08-FEB-2017] -- END

                    lvItem = New ListViewItem

                    If CBool(drRow.Item("isvoid")) = True Then
                        lvItem.ForeColor = Color.Red
                    End If

                    If CBool(drRow.Item("iscancel")) = True Then
                        lvItem.ForeColor = Color.Purple
                    End If


                    lvItem.Text = drRow.Item("Course").ToString
                    lvItem.SubItems.Add(eZeeDate.convertDate(drRow.Item("StDate").ToString).ToShortDateString & "  " & _
                                        Format(CDate(drRow.Item("StTime")), "HH:mm"))
                    lvItem.SubItems.Add(eZeeDate.convertDate(drRow.Item("EdDate").ToString).ToShortDateString & "  " & _
                                        Format(CDate(drRow.Item("EdTime")), "HH:mm"))
                    lvItem.SubItems.Add(drRow.Item("Institute").ToString)
                    lvItem.SubItems.Add(drRow.Item("training_venue").ToString)
                    lvItem.SubItems.Add(drRow.Item("contact_person").ToString & "  " & drRow.Item("contact_no").ToString)
                    lvItem.SubItems.Add(Format(drRow.Item("TotalCost"), GUI.fmtCurrency))
                    lvItem.SubItems.Add(drRow.Item("course_title").ToString)

                    'S.SANDEEP [08-FEB-2017] -- START
                    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
                    lvItem.SubItems.Add(drRow.Item("allocation_used").ToString)
                    lvItem.SubItems(colhAllocation.Index).Tag = drRow.Item("allocationids").ToString
                    'S.SANDEEP [08-FEB-2017] -- END

                    lvItem.Tag = drRow.Item("trainingschedulingunkid")

                    lvCourseScheduling.Items.Add(lvItem)


                    lvItem = Nothing
                Next

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsTraining.Dispose()
        End Try
    End Sub

    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub


    Private Sub SetVisibility()

        Try
            btnNew.Enabled = User._Object.Privilege._AddCourseScheduling
            btnEdit.Enabled = User._Object.Privilege._EditCourseScheduling
            btnDelete.Enabled = User._Object.Privilege._DeleteCourseScheduling
            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            mnuPreview.Enabled = User._Object.Privilege._AllowToPreviewScheduledTraining
            mnuPrint.Enabled = User._Object.Privilege._AllowToPrintScheduledTraining
            'S.SANDEEP [ 16 MAY 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmCourseSchedulingList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If e.KeyCode = Keys.Delete And lvCourseScheduling.Focused = True Then
            Call btnDelete.PerformClick()
        End If
    End Sub

    Private Sub frmCourseSchedulingList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmCourseSchedulingList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objTraining = New clsTraining_Scheduling
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()

            Call FillCombo()
            radActive.Checked = True
            Call fillList()

            mblnFormLoad = True

            If lvCourseScheduling.Items.Count > 0 Then lvCourseScheduling.Items(0).Selected = True
            lvCourseScheduling.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCourseSchedulingList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCourseSchedulingList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objTraining = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsTraining_Scheduling.SetMessages()
            objfrm._Other_ModuleNames = "clsTraining_Scheduling"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Buttons "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvCourseScheduling.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Training Schedule from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvCourseScheduling.Select()
            Exit Sub
        End If
        If objTraining.isUsed(CInt(lvCourseScheduling.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Training Schedule. Reason: This Training Schedule is in use."), enMsgBoxStyle.Information) '?2
            lvCourseScheduling.Select()
            Exit Sub
        End If


        If lvCourseScheduling.SelectedItems(0).ForeColor = Color.Red Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, You cannot Void already voided transaction."), enMsgBoxStyle.Information) '?1
            lvCourseScheduling.Select()
            Exit Sub
        End If

        If lvCourseScheduling.SelectedItems(0).ForeColor = Color.Purple Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot Void cancelled transaction."), enMsgBoxStyle.Information) '?1
            lvCourseScheduling.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvCourseScheduling.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Training Schedule?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objTraining._Isvoid = True
                'Sandeep [ 16 Oct 2010 ] -- Start
                'objTraining._Voidreason = "TESTING"
                'objTraining._Voiddatetime = CDate(Now.Date & " " & Format(Now, "hh:mm:ss tt"))
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.TRAINING, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objTraining._Voidreason = mstrVoidReason
                End If
                frm = Nothing
                objTraining._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sandeep [ 16 Oct 2010 ] -- End 

                objTraining._Voiduserunkid = User._Object._Userunkid


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objTraining._FormName = mstrModuleName
                objTraining._LoginEmployeeunkid = 0
                objTraining._ClientIP = getIP()
                objTraining._HostName = getHostName()
                objTraining._FromWeb = False
                objTraining._AuditUserId = User._Object._Userunkid
objTraining._CompanyUnkid = Company._Object._Companyunkid
                objTraining._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objTraining.Delete(CInt(lvCourseScheduling.SelectedItems(0).Tag))
                lvCourseScheduling.SelectedItems(0).Remove()

                If lvCourseScheduling.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvCourseScheduling.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvCourseScheduling.Items.Count - 1
                    lvCourseScheduling.Items(intSelectedIndex).Selected = True
                    lvCourseScheduling.EnsureVisible(intSelectedIndex)
                ElseIf lvCourseScheduling.Items.Count <> 0 Then
                    lvCourseScheduling.Items(intSelectedIndex).Selected = True
                    lvCourseScheduling.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvCourseScheduling.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvCourseScheduling.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Training Schedule from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvCourseScheduling.Select()
            Exit Sub
        End If

        If lvCourseScheduling.SelectedItems(0).ForeColor = Color.Red Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot Edit Void transaction."), enMsgBoxStyle.Information) '?1
            lvCourseScheduling.Select()
            Exit Sub
        End If

        Dim frm As New frmCourseScheduling
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvCourseScheduling.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            If frm.displayDialog(CInt(lvCourseScheduling.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frm = Nothing

            'Sandeep [ 09 Oct 2010 ] -- Start
            'Issues Reported by Vimal
            'If lvCourseScheduling.Items.Count > 0 Then
            '    lvCourseScheduling.Items(intSelectedIndex).Selected = True
            '    lvCourseScheduling.EnsureVisible(intSelectedIndex)
            '    lvCourseScheduling.Select()
            'End If
            'Sandeep [ 09 Oct 2010 ] -- End 
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmCourseScheduling
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call fillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            txtAmountFrom.Text = ""
            txtAmountTo.Text = ""
            'txtTitle.Text = ""
            cboQualificationGroup.SelectedValue = 0
            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'txtVenue.Text = ""
            'S.SANDEEP [ 18 FEB 2012 ] -- END
            cboQualificationGroup.SelectedValue = 0
            cboInstitute.SelectedValue = 0
            cboQualification.SelectedValue = 0
            dtpEndDate.Checked = False
            dtpStartDate.Checked = False
            radActive.Checked = True
            'S.SANDEEP [08-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
            cboAllocType.SelectedValue = 0
            cboAllocation.SelectedValue = 0
            'S.SANDEEP [08-FEB-2017] -- END

            'S.SANDEEP [15-SEP-2017] -- START
            cboCourseMaster.SelectedValue = 0
            cboStatus.SelectedIndex = 0
            'S.SANDEEP [15-SEP-2017] -- END

            Call fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub radActive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radActive.CheckedChanged, radAll.CheckedChanged, radCancel.CheckedChanged, radVoid.CheckedChanged
        Try
            If mblnFormLoad = True Then
                Call fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radActive_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 24 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES [<TRAINING>]
    Private Sub objbtnSearchCourse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCourse.Click, _
                                                                                                             objbtnSearchQGrp.Click, _
                                                                                                             objbtnSearchQualification.Click, _
                                                                                                             objbtnSearchInstitute.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .DataSource = Nothing
                Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                    Case "OBJBTNSEARCHCOURSE"
                        .ValueMember = cboCourseMaster.ValueMember
                        .DisplayMember = cboCourseMaster.DisplayMember
                        .DataSource = CType(cboCourseMaster.DataSource, DataTable)
                    Case "OBJBTNSEARCHQGRP"
                        .ValueMember = cboQualificationGroup.ValueMember
                        .DisplayMember = cboQualificationGroup.DisplayMember
                        .DataSource = CType(cboQualification.DataSource, DataTable)
                    Case "OBJBTNSEARCHQUALIFICATION"
                        If cboQualification.DataSource Is Nothing Then Exit Sub
                        .ValueMember = cboQualification.ValueMember
                        .DisplayMember = cboQualification.DisplayMember
                        .DataSource = CType(cboQualification.DataSource, DataTable)
                    Case "OBJBTNSEARCHINSTITUTE"
                        .ValueMember = cboInstitute.ValueMember
                        .DisplayMember = cboInstitute.DisplayMember
                        .DataSource = CType(cboInstitute.DataSource, DataTable)
                End Select
            End With
            If frm.DisplayDialog Then
                Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                    Case "OBJBTNSEARCHCOURSE"
                        cboCourseMaster.SelectedValue = frm.SelectedValue
                        cboCourseMaster.Focus()
                    Case "OBJBTNSEARCHQGRP"
                        cboQualificationGroup.SelectedValue = frm.SelectedValue
                        cboQualificationGroup.Focus()
                    Case "OBJBTNSEARCHQUALIFICATION"
                        cboQualification.SelectedValue = frm.SelectedValue
                        cboQualification.Focus()
                    Case "OBJBTNSEARCHINSTITUTE"
                        cboInstitute.SelectedValue = frm.SelectedValue
                        cboInstitute.Focus()
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCourse_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [ 24 FEB 2012 ] -- END 

    Private Sub mnupreActiveEmp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuActiveEmp.Click, mnuActiveEmpPrint.Click
        If lvCourseScheduling.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Training Schedule from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvCourseScheduling.Select()
            Exit Sub
        End If
        Try
            Dim objTrainingReport As New clsTrainingDetailReport(User._Object._Languageunkid, Company._Object._Companyunkid)

            If CType(sender, ToolStripMenuItem).Name = "mnuActiveEmp" Then
                objTrainingReport.Generate_Training_Info(CInt(lvCourseScheduling.SelectedItems(0).Tag), enPrintAction.Preview, False)
            ElseIf CType(sender, ToolStripMenuItem).Name = "mnuActiveEmpPrint" Then
                objTrainingReport.Generate_Training_Info(CInt(lvCourseScheduling.SelectedItems(0).Tag), enPrintAction.Print, False)
            End If

            objTrainingReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPreview_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub mnuInActiveEmp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuInActiveEmp.Click, mnuInActiveEmpPrint.Click
        If lvCourseScheduling.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Training Schedule from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvCourseScheduling.Select()
            Exit Sub
        End If
        Try
            Dim objTrainingReport As New clsTrainingDetailReport(User._Object._Languageunkid, Company._Object._Companyunkid)

            If CType(sender, ToolStripMenuItem).Name = "mnuInActiveEmp" Then
                objTrainingReport.Generate_Training_Info(CInt(lvCourseScheduling.SelectedItems(0).Tag), enPrintAction.Preview, True)
            ElseIf CType(sender, ToolStripMenuItem).Name = "mnuInActiveEmpPrint" Then
                objTrainingReport.Generate_Training_Info(CInt(lvCourseScheduling.SelectedItems(0).Tag), enPrintAction.Print, True)
            End If

            objTrainingReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPreview_Click", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [08-FEB-2017] -- START
    'ISSUE/ENHANCEMENT : Binding Allocation with Training {TRA By Andrew}
    Private Sub cboAllocType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocType.SelectedIndexChanged
        Try
            If CInt(cboAllocType.SelectedValue) > 0 Then
                objlblCaption.Text = cboAllocType.Text
                cboAllocation.DataSource = Nothing
                Dim dsList As New DataSet
                Select Case CInt(cboAllocType.SelectedValue)
                    Case enAllocation.BRANCH
                        Dim objBranch As New clsStation
                        dsList = objBranch.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "stationunkid"
                            .DisplayMember = "name"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objBranch = Nothing
                    Case enAllocation.DEPARTMENT_GROUP
                        Dim objDeptGrp As New clsDepartmentGroup
                        dsList = objDeptGrp.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "deptgroupunkid"
                            .DisplayMember = "name"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objDeptGrp = Nothing
                    Case enAllocation.DEPARTMENT
                        Dim objDept As New clsDepartment
                        dsList = objDept.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "departmentunkid"
                            .DisplayMember = "name"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objDept = Nothing
                    Case enAllocation.SECTION_GROUP
                        Dim objSectionGrp As New clsSectionGroup
                        dsList = objSectionGrp.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "sectiongroupunkid"
                            .DisplayMember = "name"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objSectionGrp = Nothing
                    Case enAllocation.SECTION
                        Dim objSection As New clsSections
                        dsList = objSection.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "sectionunkid"
                            .DisplayMember = "name"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objSection = Nothing
                    Case enAllocation.UNIT_GROUP
                        Dim objUnitGrp As New clsUnitGroup
                        dsList = objUnitGrp.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "unitgroupunkid"
                            .DisplayMember = "name"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objUnitGrp = Nothing
                    Case enAllocation.UNIT
                        Dim objUnit As New clsUnits
                        dsList = objUnit.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "unitunkid"
                            .DisplayMember = "name"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objUnit = Nothing
                    Case enAllocation.TEAM
                        Dim objTeam As New clsTeams
                        dsList = objTeam.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "teamunkid"
                            .DisplayMember = "name"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objTeam = Nothing
                    Case enAllocation.JOB_GROUP
                        Dim objJobGrp As New clsJobGroup
                        dsList = objJobGrp.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "jobgroupunkid"
                            .DisplayMember = "name"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objJobGrp = Nothing
                    Case enAllocation.JOBS
                        Dim objJob As New clsJobs
                        dsList = objJob.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "jobunkid"
                            .DisplayMember = "name"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objJob = Nothing
                    Case enAllocation.CLASS_GROUP
                        Dim objClassGrp As New clsClassGroup
                        dsList = objClassGrp.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "classgroupunkid"
                            .DisplayMember = "name"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objClassGrp = Nothing
                    Case enAllocation.CLASSES
                        Dim objClass As New clsClass
                        dsList = objClass.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "classesunkid"
                            .DisplayMember = "name"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objClass = Nothing
                    Case enAllocation.COST_CENTER
                        Dim objCC As New clscostcenter_master
                        dsList = objCC.getComboList("List", True)
                        With cboAllocation
                            .ValueMember = "costcenterunkid"
                            .DisplayMember = "costcentername"
                            .DataSource = dsList.Tables(0)
                            .SelectedValue = 0
                        End With
                        objCC = Nothing
                End Select
                cboAllocation.Enabled = True : objbtnSearchAllocation.Enabled = True
            Else
                objlblCaption.Text = "" : cboAllocation.Enabled = False : objbtnSearchAllocation.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    Private Sub objbtnSearchAllocation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAllocation.Click
        Dim frm As New frmCommonSearch
        Try
            With frm
                .ValueMember = cboAllocation.ValueMember
                .DisplayMember = cboAllocation.DisplayMember
                .DataSource = CType(cboAllocation.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboAllocation.SelectedValue = frm.SelectedValue
                cboAllocation.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAllocation_Click", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [08-FEB-2017] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnPrint.GradientBackColor = GUI._ButttonBackColor
            Me.btnPrint.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.colhCourse.Text = Language._Object.getCaption(CStr(Me.colhCourse.Tag), Me.colhCourse.Text)
            Me.colhStartdateTime.Text = Language._Object.getCaption(CStr(Me.colhStartdateTime.Tag), Me.colhStartdateTime.Text)
            Me.colhEndDateTime.Text = Language._Object.getCaption(CStr(Me.colhEndDateTime.Tag), Me.colhEndDateTime.Text)
            Me.colhInstitute.Text = Language._Object.getCaption(CStr(Me.colhInstitute.Tag), Me.colhInstitute.Text)
            Me.colhVenue.Text = Language._Object.getCaption(CStr(Me.colhVenue.Tag), Me.colhVenue.Text)
            Me.colhContactPersonAndNo.Text = Language._Object.getCaption(CStr(Me.colhContactPersonAndNo.Tag), Me.colhContactPersonAndNo.Text)
            Me.colhTotalCost.Text = Language._Object.getCaption(CStr(Me.colhTotalCost.Tag), Me.colhTotalCost.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblTraningCourse.Text = Language._Object.getCaption(Me.lblTraningCourse.Name, Me.lblTraningCourse.Text)
            Me.lblTraningInstitute.Text = Language._Object.getCaption(Me.lblTraningInstitute.Name, Me.lblTraningInstitute.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
            Me.lblQualification.Text = Language._Object.getCaption(Me.lblQualification.Name, Me.lblQualification.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblAmountTo.Text = Language._Object.getCaption(Me.lblAmountTo.Name, Me.lblAmountTo.Text)
            Me.lblTrainingTitle.Text = Language._Object.getCaption(Me.lblTrainingTitle.Name, Me.lblTrainingTitle.Text)
            Me.colhTitle.Text = Language._Object.getCaption(CStr(Me.colhTitle.Tag), Me.colhTitle.Text)
            Me.radAll.Text = Language._Object.getCaption(Me.radAll.Name, Me.radAll.Text)
            Me.radCancel.Text = Language._Object.getCaption(Me.radCancel.Name, Me.radCancel.Text)
            Me.radVoid.Text = Language._Object.getCaption(Me.radVoid.Name, Me.radVoid.Text)
            Me.radActive.Text = Language._Object.getCaption(Me.radActive.Name, Me.radActive.Text)
            Me.btnPrint.Text = Language._Object.getCaption(Me.btnPrint.Name, Me.btnPrint.Text)
            Me.mnuPrintCourse.Text = Language._Object.getCaption(Me.mnuPrintCourse.Name, Me.mnuPrintCourse.Text)
            Me.mnuPreview.Text = Language._Object.getCaption(Me.mnuPreview.Name, Me.mnuPreview.Text)
            Me.mnuActiveEmp.Text = Language._Object.getCaption(Me.mnuActiveEmp.Name, Me.mnuActiveEmp.Text)
            Me.mnuInActiveEmp.Text = Language._Object.getCaption(Me.mnuInActiveEmp.Name, Me.mnuInActiveEmp.Text)
            Me.mnuActiveEmpPrint.Text = Language._Object.getCaption(Me.mnuActiveEmpPrint.Name, Me.mnuActiveEmpPrint.Text)
            Me.mnuInActiveEmpPrint.Text = Language._Object.getCaption(Me.mnuInActiveEmpPrint.Name, Me.mnuInActiveEmpPrint.Text)
            Me.lblAllocType.Text = Language._Object.getCaption(Me.lblAllocType.Name, Me.lblAllocType.Text)
            Me.lblTrainingStatus.Text = Language._Object.getCaption(Me.lblTrainingStatus.Name, Me.lblTrainingStatus.Text)
            Me.colhAllocation.Text = Language._Object.getCaption(CStr(Me.colhAllocation.Tag), Me.colhAllocation.Text)



        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Training Schedule from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Training Schedule. Reason: This Training Schedule is in use.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Training Schedule?")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot Edit Void transaction.")
            Language.setMessage(mstrModuleName, 5, "Sorry, You cannot Void already voided transaction.")
            Language.setMessage(mstrModuleName, 6, "Sorry, You cannot Void cancelled transaction.")
            Language.setMessage(mstrModuleName, 7, "All")
            Language.setMessage(mstrModuleName, 8, "Active")
            Language.setMessage(mstrModuleName, 9, "Cancel")
            Language.setMessage(mstrModuleName, 10, "Void")
            Language.setMessage(mstrModuleName, 11, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class