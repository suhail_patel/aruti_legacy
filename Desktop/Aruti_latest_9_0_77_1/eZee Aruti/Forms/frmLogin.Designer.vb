<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogin
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub
    Friend WithEvents UsernameLabel As System.Windows.Forms.Label
    Friend WithEvents PasswordLabel As System.Windows.Forms.Label

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLogin))
        Me.UsernameLabel = New System.Windows.Forms.Label
        Me.PasswordLabel = New System.Windows.Forms.Label
        Me.txtPassword = New eZee.TextBox.AlphanumericTextBox
        Me.txtUserName = New eZee.TextBox.AlphanumericTextBox
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objgbLogin = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnLogin = New eZee.Common.eZeeLightButton(Me.components)
        Me.LogoPictureBox = New System.Windows.Forms.PictureBox
        Me.pnlMain.SuspendLayout()
        Me.objgbLogin.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UsernameLabel
        '
        Me.UsernameLabel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UsernameLabel.Location = New System.Drawing.Point(9, 5)
        Me.UsernameLabel.Name = "UsernameLabel"
        Me.UsernameLabel.Size = New System.Drawing.Size(201, 13)
        Me.UsernameLabel.TabIndex = 0
        Me.UsernameLabel.Text = "&Username"
        Me.UsernameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PasswordLabel
        '
        Me.PasswordLabel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PasswordLabel.Location = New System.Drawing.Point(9, 70)
        Me.PasswordLabel.Name = "PasswordLabel"
        Me.PasswordLabel.Size = New System.Drawing.Size(201, 13)
        Me.PasswordLabel.TabIndex = 2
        Me.PasswordLabel.Text = "&Password"
        Me.PasswordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPassword
        '
        Me.txtPassword.Flags = 0
        Me.txtPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.HideSelection = False
        Me.txtPassword.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0)}
        Me.txtPassword.Location = New System.Drawing.Point(9, 86)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(201, 21)
        Me.txtPassword.TabIndex = 3
        '
        'txtUserName
        '
        Me.txtUserName.Flags = 0
        Me.txtUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUserName.HideSelection = False
        Me.txtUserName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0), Global.Microsoft.VisualBasic.ChrW(0)}
        Me.txtUserName.Location = New System.Drawing.Point(9, 25)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(201, 21)
        Me.txtUserName.TabIndex = 1
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objgbLogin)
        Me.pnlMain.Controls.Add(Me.objefFormFooter)
        Me.pnlMain.Controls.Add(Me.LogoPictureBox)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(405, 203)
        Me.pnlMain.TabIndex = 0
        '
        'objgbLogin
        '
        Me.objgbLogin.BorderColor = System.Drawing.Color.Black
        Me.objgbLogin.Checked = False
        Me.objgbLogin.CollapseAllExceptThis = False
        Me.objgbLogin.CollapsedHoverImage = Nothing
        Me.objgbLogin.CollapsedNormalImage = Nothing
        Me.objgbLogin.CollapsedPressedImage = Nothing
        Me.objgbLogin.CollapseOnLoad = False
        Me.objgbLogin.Controls.Add(Me.UsernameLabel)
        Me.objgbLogin.Controls.Add(Me.PasswordLabel)
        Me.objgbLogin.Controls.Add(Me.txtPassword)
        Me.objgbLogin.Controls.Add(Me.txtUserName)
        Me.objgbLogin.ExpandedHoverImage = Nothing
        Me.objgbLogin.ExpandedNormalImage = Nothing
        Me.objgbLogin.ExpandedPressedImage = Nothing
        Me.objgbLogin.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbLogin.HeaderHeight = 25
        Me.objgbLogin.HeaderMessage = ""
        Me.objgbLogin.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.objgbLogin.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbLogin.HeightOnCollapse = 0
        Me.objgbLogin.LeftTextSpace = 0
        Me.objgbLogin.Location = New System.Drawing.Point(175, 9)
        Me.objgbLogin.Name = "objgbLogin"
        Me.objgbLogin.OpenHeight = 300
        Me.objgbLogin.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbLogin.ShowBorder = True
        Me.objgbLogin.ShowCheckBox = False
        Me.objgbLogin.ShowCollapseButton = False
        Me.objgbLogin.ShowDefaultBorderColor = True
        Me.objgbLogin.ShowDownButton = False
        Me.objgbLogin.ShowHeader = False
        Me.objgbLogin.Size = New System.Drawing.Size(221, 129)
        Me.objgbLogin.TabIndex = 0
        Me.objgbLogin.Temp = 0
        Me.objgbLogin.Text = "EZeeCollapsibleContainer1"
        Me.objgbLogin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Controls.Add(Me.btnLogin)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(169, 148)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(236, 55)
        Me.objefFormFooter.TabIndex = 1
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(137, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnLogin
        '
        Me.btnLogin.BackColor = System.Drawing.Color.White
        Me.btnLogin.BackgroundImage = CType(resources.GetObject("btnLogin.BackgroundImage"), System.Drawing.Image)
        Me.btnLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnLogin.BorderColor = System.Drawing.Color.Empty
        Me.btnLogin.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnLogin.FlatAppearance.BorderSize = 0
        Me.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogin.ForeColor = System.Drawing.Color.Black
        Me.btnLogin.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnLogin.GradientForeColor = System.Drawing.Color.Black
        Me.btnLogin.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLogin.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnLogin.Location = New System.Drawing.Point(41, 13)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLogin.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnLogin.Size = New System.Drawing.Size(90, 30)
        Me.btnLogin.TabIndex = 0
        Me.btnLogin.Text = "&Login"
        Me.btnLogin.UseVisualStyleBackColor = False
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.BackgroundImage = CType(resources.GetObject("LogoPictureBox.BackgroundImage"), System.Drawing.Image)
        Me.LogoPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.LogoPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LogoPictureBox.Dock = System.Windows.Forms.DockStyle.Left
        Me.LogoPictureBox.Location = New System.Drawing.Point(0, 0)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.LogoPictureBox.Size = New System.Drawing.Size(169, 203)
        Me.LogoPictureBox.TabIndex = 224
        Me.LogoPictureBox.TabStop = False
        '
        'frmLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(405, 203)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Aruti Login"
        Me.pnlMain.ResumeLayout(False)
        Me.objgbLogin.ResumeLayout(False)
        Me.objgbLogin.PerformLayout()
        Me.objefFormFooter.ResumeLayout(False)
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtPassword As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtUserName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents LogoPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnLogin As eZee.Common.eZeeLightButton
    Friend WithEvents objgbLogin As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter

End Class
