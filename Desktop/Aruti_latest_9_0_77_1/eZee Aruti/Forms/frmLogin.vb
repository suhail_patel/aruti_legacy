Option Strict On
Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language


Public Class frmLogin

    Private objUser As clsUserAddEdit
    Private mintUserUnkid As Integer = 0
    Private mblnCancel As Boolean = True
    Private ReadOnly mstrModuleName As String = "frmLogin"

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    'Private Sub fillList()
    '    Dim dsUser As New DataSet
    '    Dim lvItem As ListViewItem
    '    Try
    '        dsUser = objUser.GetList("list", True)

    '        lvUser.Items.Clear()
    '        Dim i As Integer = 0
    '        For Each drRow As DataRow In dsUser.Tables("list").Rows
    '            If CBool(drRow("isactive")) = True Then
    '                lvItem = New ListViewItem

    '                lvItem.Text = CStr(i + 1)
    '                lvItem.Tag = drRow("userunkid")
    '                lvItem.SubItems.Add(drRow("username").ToString)

    '                lvUser.Items.Add(lvItem)
    '                lvItem = Nothing
    '                i += 1
    '            End If
    '        Next

    '        If lvUser.Items.Count > 5 Then
    '            lvUser.Columns(colhUser.Index).Width = 162 - 17
    '        Else
    '            lvUser.Columns(colhUser.Index).Width = 162
    '        End If

    '        If lvUser.Items.Count > 0 Then lvUser.Items(0).Selected = True

    '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        Dim objAppSettings As New clsApplicationSettings
    '        For iCnt As Integer = 0 To lvUser.Items.Count - 1
    '            If CInt(lvUser.Items(iCnt).Tag) = objAppSettings._LastUserId Then lvUser.Items(iCnt).Selected = True
    '        Next
    '        objAppSettings = Nothing
    '        'S.SANDEEP [ 08 June 2011 ] -- START

    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
    '    Finally
    '        If dsUser IsNot Nothing Then dsUser.Dispose()
    '    End Try
    'End Sub

    Public Sub UnlockUser()
        Dim dsUser As New DataSet
        Try

            'S.SANDEEP [24 SEP 2015] -- START

            ''S.SANDEEP [ 01 FEB 2013 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            'Dim objOption As New clsPassowdOptions
            'If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
            '    If objOption._IsEmployeeAsUser Then
            '        dsUser = objUser.GetList("list", True, False, True)
            '    Else
            '        dsUser = objUser.GetList("list", True)
            '    End If
            'ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Then
            '    dsUser = objUser.GetList("list", True, True)
            'ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
            '    dsUser = objUser.GetList("list", True, True)
            'End If
            ''S.SANDEEP [ 01 FEB 2013 ] -- END

            dsUser = objUser.GetList("list")

            'S.SANDEEP [24 SEP 2015] -- END

            If dsUser.Tables(0).Rows.Count <= 0 Then Exit Sub



            'Pinkal (28-Feb-2014) -- Start
            'Enhancement : TRA Changes

            Dim dRow() As DataRow = dsUser.Tables(0).Select("lockedduration IS NOT NULL")
            If dRow.Length <= 0 Then Exit Sub

            'Pinkal (28-Feb-2014) -- End


            For Each drRow As DataRow In dRow
                objUser._Userunkid = CInt(drRow("userunkid"))
                If objUser.PWDOptions._IsUnlockedByAdmin = False And objUser._LockedDuration <> Nothing Then
                    If ConfigParameter._Object._CurrentDateAndTime.ToShortTimeString >= objUser._LockedDuration.ToShortTimeString Then
                        objUser._IsAcc_Locked = False
                        objUser._Locked_Time = Nothing
                        objUser._LockedDuration = Nothing


'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objUser._FormName = mstrModuleName
                        objUser._LoginEmployeeunkid = 0
                        objUser._ClientIP = getIP()
                        objUser._HostName = getHostName()
                        objUser._FromWeb = False
                        objUser._AuditUserId = User._Object._Userunkid
objUser._CompanyUnkid = Company._Object._Companyunkid
                        objUser._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        'S.SANDEEP [ 24 JUNE 2011 ] -- START
                        'ISSUE : DELETION OF PRIVILEGE WHILE UNLOCKING USER
                        'objUser.Update()
                        objUser.Update(True)
                        'S.SANDEEP [ 24 JUNE 2011 ] -- END 

                        Dim objUAL As New clsUser_Acc_Lock

                        objUAL._Userunkid = CInt(drRow("userunkid"))

                        objUAL._Isunlocked = True
                        With objUAL
                            ._FormName = mstrModuleName
                            ._LoginEmployeeunkid = 0
                            ._ClientIP = getIP()
                            ._HostName = getHostName()
                            ._FromWeb = False
                            ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        End With
                        objUAL.Update()

                    End If
                End If
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : UnlockUser ; Module Name : " & mstrModuleName)
        Finally
            dsUser.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 20 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function Passwd_Change(ByVal intUId As Integer, ByVal objU As clsUserAddEdit, ByRef intVal As Integer) As Boolean
        Try
            objU._Userunkid = intUId
            If objU._Password <> txtPassword.Text Then
                eZeeMsgBox.Show(Language.getMessage("clsUserAddEdit", 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login."), enMsgBoxStyle.Information)
                Return False
            Else
                intVal = 1
            End If

            Dim objPwdOpt As New clsPassowdOptions
            If objPwdOpt._IsPasswordLenghtSet Then
                If txtPassword.Text.Length < objPwdOpt._PasswordLength Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Password does not match with policy enforced. Please change your password now."), enMsgBoxStyle.Information)
                    Return True
                End If
            End If

            If objPwdOpt._IsPasswdPolicySet Then
                Dim mRegx As System.Text.RegularExpressions.Regex
                If objPwdOpt._IsUpperCase_Mandatory Then
                    mRegx = New System.Text.RegularExpressions.Regex("[A-Z]")
                    If mRegx.IsMatch(txtPassword.Text) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Password does not match with policy enforced. Please change your password now."), enMsgBoxStyle.Information)
                        Return True
                    End If
                End If
                If objPwdOpt._IsLowerCase_Mandatory Then
                    mRegx = New System.Text.RegularExpressions.Regex("[a-z]")
                    If mRegx.IsMatch(txtPassword.Text) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Password does not match with policy enforced. Please change your password now."), enMsgBoxStyle.Information)
                        Return True
                    End If
                End If
                If objPwdOpt._IsNumeric_Mandatory Then
                    mRegx = New System.Text.RegularExpressions.Regex("[0-9]")
                    If mRegx.IsMatch(txtPassword.Text) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Password does not match with policy enforced. Please change your password now."), enMsgBoxStyle.Information)
                        Return True
                    End If
                End If
                If objPwdOpt._IsSpecalChars_Mandatory Then
                    'S.SANDEEP [ 07 MAY 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'mRegx = New System.Text.RegularExpressions.Regex("^.*[\[\]^$.|?*+()\\~`!@#%&\-_+={}'&quot;&lt;&gt;:;,\ ].*$")
                    mRegx = New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]")
                    'S.SANDEEP [ 07 MAY 2013 ] -- END
                    If mRegx.IsMatch(txtPassword.Text) = False Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Password does not match with policy enforced. Please change your password now."), enMsgBoxStyle.Information)
                        Return True
                    End If
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Passwd_Change", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 20 NOV 2012 ] -- END

#End Region

#Region " Form's Events "

    Private Sub frmLogin_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objUser = Nothing
    End Sub

    Private Sub frmLogin_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Call btnLogin.PerformClick()
        ElseIf Asc(e.KeyChar) = 27 Then
            Me.Close()
        End If
    End Sub

    Private Sub frmLogin_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim frm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            Call SetMessages() 'Sohail (11 Aug 2021)


            frm.displayDialog(Me)
            Call SetLanguage() 'Sohail (11 Aug 2021)

            Language.Refresh()
            Call Language.setLanguage(Me)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objLanguage_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub frmLogin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            objUser = New clsUserAddEdit
            Call SetLanguage()
            Call OtherSettings() 'Sohail (11 Aug 2021)


            Call setColor()
            'Call fillList()
            Call UnlockUser()

            'Me.Text = ""

            'Dim objMachine As New clsMachineSettings(enApplication.eZeeFD)
            'Dim i As Integer
            'For i = 0 To lvUser.Items.Count - 1
            '    If CInt(lvUser.Items(i).Tag) = objMachine._LastUserLoggedIn Then
            '        lvUser.Items(i).Selected = True
            '        lvUser.TopItem = lvUser.Items(i)
            '    End If
            'Next
            'Sohail (11 Aug 2021) -- Start
            'NMB Enhancement :  : Don't show company code option when there is a connection or any other issue on login in self service.
            Dim sTemp As String = ""
            sTemp = Language.getMessage(mstrModuleName, 8, "Sorry, We are not able to connect aruti database server. Please check if database server is reachable or try again after some time or contact aruti support team.")
            'Sohail (11 Aug 2021) -- End

            txtUserName.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmLogin_Load", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Try

            'eZeeMsgBox.Show(Language.getMessage("frmLogin", 14, "Sorry, You cannot login from this machine.Reason you are already logged in from another machine."), enMsgBoxStyle.Information)

            If txtUserName.Text.Trim.Length <= 0 Then
                'S.SANDEEP [21-NOV-2018] -- START
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Username cannot be blank. Username is mandatory information."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please enter a Valid Username and Password."), enMsgBoxStyle.Information)
                'S.SANDEEP [21-NOV-2018] -- END
                txtUserName.Focus()
                Exit Sub
            End If

            'S.SANDEEP [ 01 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim mRegx As System.Text.RegularExpressions.Regex

            'Anjan (21 Dec 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            ' mRegx = New System.Text.RegularExpressions.Regex("^[a-zA-Z0-9 ]*$")
            'If mRegx.IsMatch(txtUserName.Text.Trim) = False Then
            'Issue: This has been removed now due IDAMS (Oracle), so it has to be allowed now-> andrew muga.
            'mRegx = New System.Text.RegularExpressions.Regex("^.*[\%'*'+?\\<>:].*$")
            'If mRegx.IsMatch(txtUserName.Text.Trim) = True Then
            '    'Anjan (21 Dec 2012)-End 
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Special characters not allowed in login."), enMsgBoxStyle.Information)
            '    txtUserName.Focus()
            '    Exit Sub
            'End If
            'S.SANDEEP [ 20 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            ''Dim objPwdOpt As New clsPassowdOptions
            ''If objPwdOpt._IsUsrNameLengthSet = True Then
            ''    If txtUserName.Text.Trim.Length < objPwdOpt._UsrNameLength Then
            ''        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Login name cannot be less then ") & objPwdOpt._UsrNameLength & Language.getMessage(mstrModuleName, 14, " character(s)."), enMsgBoxStyle.Information)
            ''        txtUserName.Focus()
            ''        Exit Sub
            ''    End If
            ''End If
            Dim iUsrId As Integer = 0 : Dim objUsr As New clsUserAddEdit : Dim iValue As Integer = -1
            iUsrId = objUsr.Return_UserId(txtUserName.Text, "hrmsConfiguration", enLoginMode.USER)
            If iUsrId > 0 Then
                Dim objPwdOpt As New clsPassowdOptions

                'Pinkal (03-Apr-2017) -- Start
                'Enhancement - Working On Active directory Changes for PACRA.

                Dim drLoginOption As DataRow() = objPwdOpt._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

                If drLoginOption.Length > 0 Then

                    If objPwdOpt._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then

                If objPwdOpt._IsUsrNameLengthSet = True Then
                    If txtUserName.Text.Trim.Length < objPwdOpt._UsrNameLength Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Username does not match with the policy set for username. Please change your username."), enMsgBoxStyle.Information)
                        Dim frm As New frmChangeUsername
                        If frm.displayDialog() = True Then
                            Exit Sub
                        Else
                            Exit Sub
                        End If
                    End If
                End If
                If objPwdOpt._IsPasswdPolicySet = True Then
                    If Passwd_Change(iUsrId, objUsr, iValue) = True Then
                        Dim frm As New frmChangePassword
                        If frm.displayDialog(True, False, txtUserName.Text, iUsrId) = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Password changed successfully. Please login with new password."), enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                            Exit Sub
                        End If
                    Else
                        If iValue <= 0 Then
                            Exit Sub
                        End If
                    End If
                End If

                    ElseIf (objPwdOpt._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION OrElse objPwdOpt._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION) AndAlso iUsrId <> 1 Then

                        Dim objConfig As New clsConfigOptions
                        Dim mstrIPAddress As String = ""
                        Dim mstrDomain As String = ""
                        objConfig.IsValue_Changed("ADIPAddress", "-999", mstrIPAddress)
                        objConfig.IsValue_Changed("ADDomain", "-999", mstrDomain)
                        objConfig = Nothing


                        If IsAuthenticated(mstrIPAddress.Trim, mstrDomain.Trim, txtUserName.Text.Trim, txtPassword.Text.Trim) = False Then
                eZeeMsgBox.Show(Language.getMessage("clsUserAddEdit", 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login."), enMsgBoxStyle.Information)
                Exit Sub
            End If

                    End If


                End If

                'Pinkal (03-Apr-2017) -- End
            Else
                eZeeMsgBox.Show(Language.getMessage("clsUserAddEdit", 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            'Pinkal (27-Nov-2020) -- Start
            'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
            Dim objConfigOption As New clsConfigOptions
            Dim mstrAllowEmpSystemAccessOnActualEOCRetirementDate As String = ""
            Dim mblnAllowEmpSystemAccessOnActualEOCRetirementDate As Boolean = False
            objConfigOption.IsValue_Changed("AllowEmpSystemAccessOnActualEOCRetirementDate", "-999", mstrAllowEmpSystemAccessOnActualEOCRetirementDate)
            mblnAllowEmpSystemAccessOnActualEOCRetirementDate = CBool(IIf(mstrAllowEmpSystemAccessOnActualEOCRetirementDate.Length <= 0, False, mstrAllowEmpSystemAccessOnActualEOCRetirementDate))
            objConfigOption = Nothing
            'Pinkal (27-Nov-2020) -- End

            Dim objTempUser As New clsUserAddEdit
            Dim objOption As New clsPassowdOptions
            Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

            If drOption.Length > 0 Then
                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
                    'mintUserUnkid = objTempUser.IsValidLogin(txtUserName.Text, txtPassword.Text, enLoginMode.USER, "hrmsConfiguration")
                    mintUserUnkid = objTempUser.IsValidLogin(txtUserName.Text, txtPassword.Text, enLoginMode.USER, "hrmsConfiguration", mblnAllowEmpSystemAccessOnActualEOCRetirementDate)

                ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Then
                    'mintUserUnkid = objTempUser.IsValidLogin(txtUserName.Text, txtPassword.Text, enLoginMode.USER, "hrmsConfiguration", True)
                    mintUserUnkid = objTempUser.IsValidLogin(txtUserName.Text, txtPassword.Text, enLoginMode.USER, "hrmsConfiguration", mblnAllowEmpSystemAccessOnActualEOCRetirementDate, True)

                ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                    'mintUserUnkid = objTempUser.IsValidLogin(txtUserName.Text, txtPassword.Text, enLoginMode.USER, "hrmsConfiguration", True)
                    mintUserUnkid = objTempUser.IsValidLogin(txtUserName.Text, txtPassword.Text, enLoginMode.USER, "hrmsConfiguration", mblnAllowEmpSystemAccessOnActualEOCRetirementDate, True)

                    'Pinkal (27-Nov-2020) -- End

                End If

            End If

            If mintUserUnkid <= 0 AndAlso objTempUser._Message <> "" Then

                If objTempUser._ShowChangePasswdfrm = True AndAlso objTempUser._RetUserId > 0 Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot login to system. Reason : your password has been expired." & vbCrLf & _
                                                           "Do you want to change your password now?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                        Dim frm As New frmChangePassword
                        If frm.displayDialog(True, True, txtUserName.Text, objTempUser._RetUserId) = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Password changed successfully. Please login with new password."), enMsgBoxStyle.Information)
                            Exit Sub
                        Else
                            Exit Sub
                        End If
                    Else
                        Exit Sub
                    End If
                Else
                    eZeeMsgBox.Show(objTempUser._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            objUsr = Nothing

            User.Refresh()
            User._Object._Userunkid = mintUserUnkid
            mblnCancel = False
            frmSplash.Hide()
            Me.Close()

        Catch ex As Exception

            'S.SANDEEP [21-NOV-2018] -- START
            If ex.Message.Contains("The server is not operational.") Then
                eZeeMsgBox.Show(Language.getMessage("clsUserAddEdit", 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login."), enMsgBoxStyle.Information)
                Exit Sub
            Else
                DisplayError.Show("-1", ex.Message, "btnLogin_Click", mstrModuleName)
            End If
            'S.SANDEEP [21-NOV-2018] -- END

            'Pinkal (27-APR-2018) -- Start
            'Bug - 0002194 Unfriendly message box pops up when user enters incorrect username and password.
            If ex.Message.ToString().Trim().Contains("The user name or password is incorrect.") Then
                eZeeMsgBox.Show(Language.getMessage("clsUserAddEdit", 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login."), enMsgBoxStyle.Information)
            Else
            DisplayError.Show("-1", ex.Message, "btnLogin_Click", mstrModuleName)
            End If
            'Pinkal (27-APR-2018) -- End
        Finally
        End Try
    End Sub

    'Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
    '    If lvUser.SelectedItems.Count <= 0 Then
    '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select a User from the list to login."), enMsgBoxStyle.Information) '?3
            '    Exit Sub
            'End If
    '    Dim objTempUser As New clsUserAddEdit
    '    Try

    '        mintUserUnkid = CInt(lvUser.SelectedItems(0).Tag)

    '        objTempUser._Userunkid = mintUserUnkid


    '        'Sandeep [ 10 FEB 2011 ] -- Start
    '        Dim intDaysDiff As Integer = 0
    '        If objTempUser._Exp_Days.ToString.Length = 8 Then
    '            Dim dtDate As Date = CDate(eZeeDate.convertDate(objTempUser._Exp_Days.ToString).ToShortDateString)
    '            intDaysDiff = CInt(DateDiff(DateInterval.Day, dtDate, ConfigParameter._Object._CurrentDateAndTime.AddDays(-1)))

    '            'Sandeep [ 01 MARCH 2011 ] -- Start
    '            'If intDaysDiff = 0 Then
    '            If intDaysDiff >= 0 Then
    '                'Sandeep [ 01 MARCH 2011 ] -- End 
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot login. Reason : your password is expired. Please contact Administrator."), enMsgBoxStyle.Information)
    '                Exit Sub
    '            End If
    '        End If
    '        'Sandeep [ 10 FEB 2011 ] -- End 

    '        'If objTempUser.FD._AllowConfigProperty = False Then
    '        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, You can not log in to Configuration. You do not have rights to access Configuration, please contact your administrator."), enMsgBoxStyle.Information) '?1
    '        '    Exit Sub
    '        'End If

            'If mintUserUnkid > 0 Then
            '    objUser._Userunkid = mintUserUnkid

            '    If objUser._Userunkid <> 1 Then
            '        'S.SANDEEP [ 30 May 2011 ] -- START
            '        'ISSUE : FINCA REQ.
            '        If objUser._IsAcc_Locked = True Then
            '            Dim StrMsg As String = ""
            '            If objUser.PWDOptions._IsUnlockedByAdmin = False Then
    '                        StrMsg = Language.getMessage(mstrModuleName, 9, "You can relogin after ") & objUser.PWDOptions._AlloUserRetry & Language.getMessage(mstrModuleName, 10, " Minute(s).")
            '                StrMsg &= Language.getMessage(mstrModuleName, 11, " Or Please contact Adminstrator.")
            '            End If
            '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, You can not log in, Your account is locked.") & " " & StrMsg, enMsgBoxStyle.Information)
            '            Exit Sub
            '        End If
            '        'S.SANDEEP [ 30 May 2011 ] -- END 

            '        If objUser._Password <> txtPassword.Text Then
            '            'S.SANDEEP [ 30 May 2011 ] -- START
            '            'ISSUE : FINCA REQ.
            '            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You can not log in, The password is invalid. Enter correct password to log in."), enMsgBoxStyle.Information) '?2
            '            'txtPassword.Focus()
            '            'txtPassword.Text = txtPassword.SelectedText

            '            Dim StrMessage As String = String.Empty

            '            If objUser.PWDOptions._IsAccLockSet = True Then
            '                intTry = objUser.PWDOptions._LockUserAfter

            '                objUAL = New clsUser_Acc_Lock

            '                objUAL._Userunkid = mintUserUnkid

            '                If objUser.PWDOptions._LockUserAfter = objUAL._Attempts Then
            '                    Dim StrMsg As String = Language.getMessage(mstrModuleName, 8, "Sorry, You can not log in, Your account is locked.")
            '                    If objUser.PWDOptions._IsUnlockedByAdmin = False Then
    '                                StrMsg &= Language.getMessage(mstrModuleName, 9, "You can relogin after ") & objUser.PWDOptions._AlloUserRetry & Language.getMessage(mstrModuleName, 10, " Minute(s).")
            '                    End If
            '                    StrMsg &= Language.getMessage(mstrModuleName, 11, " Or Please contact Adminstrator.")
            '                    eZeeMsgBox.Show(StrMsg, enMsgBoxStyle.Information)
            '                    Exit Sub
            '                End If

            '                If objUAL._Attempts <> 0 Then
            '                    intTry = objUser.PWDOptions._LockUserAfter - objUAL._Attempts
            '                End If
            '                StrMessage = Language.getMessage(mstrModuleName, 5, "As per password policy settings your account will be locked after ") & objUser.PWDOptions._LockUserAfter & Language.getMessage(mstrModuleName, 6, " attempts. And you have ") & intTry - 1 & Language.getMessage(mstrModuleName, 7, " attempts left.")
            '            End If

            '            If StrMessage.Trim.Length > 0 Then
            '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You can not log in, The password is invalid. Enter correct password to log in.") & StrMessage, enMsgBoxStyle.Information) '?2
            '                StrMessage = String.Empty

            '                objUAL._Userunkid = mintUserUnkid
            '                objUAL._Ip = getIP()
            '                objUAL._Machine_Name = getHostName()
            '                objUAL._Unlockuserunkid = 0
            '                objUAL._Isunlocked = objUAL._Isunlocked

            '                Dim dtTime As DateTime = ConfigParameter._Object._CurrentDateAndTime

            '                If objUAL._Attempts <> 0 Then
            '                    objUAL._Attempts = objUAL._Attempts + 1
            '                    If objUAL._Attempts = objUser.PWDOptions._LockUserAfter Then
            '                        objUser._IsAcc_Locked = True
            '                        objUser._Locked_Time = ConfigParameter._Object._CurrentDateAndTime
            '                        If objUser.PWDOptions._IsUnlockedByAdmin = False Then
            '                            If objUser._Userunkid = 1 Then
            '                                objUser._LockedDuration = dtTime.AddMinutes(5)
            '                            Else
            '                                objUser._LockedDuration = dtTime.AddMinutes(objUser.PWDOptions._AlloUserRetry)
            '                            End If
            '                        Else
            '                            If objUser._Userunkid = 1 Then
            '                                objUser._LockedDuration = dtTime.AddMinutes(5)
            '                            Else
            '                                objUser._LockedDuration = Nothing
            '                            End If
            '                        End If
            '                        'S.SANDEEP [ 24 JUNE 2011 ] -- START
            '                        'ISSUE : DELETION OF PRIVILEGE WHILE UNLOCKING USER
            '                        'objUser.Update()
            '                        objUser.Update(True)
            '                        'S.SANDEEP [ 24 JUNE 2011 ] -- END 
            '                        objUAL._Lockedtime = ConfigParameter._Object._CurrentDateAndTime
            '                    End If
            '                    Call objUAL.Update()
            '                Else
            '                    objUAL._Attempts = 1
            '                    objUAL._Lockedtime = Nothing
            '                    Call objUAL.Insert()
            '                End If
    '                        txtPassword.Focus()
    '                        Exit Sub
            '                'S.SANDEEP [ 24 JUNE 2011 ] -- START
            '                'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            '            Else
            '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot login, The password is invalid. Enter correct password to login.") & StrMessage, enMsgBoxStyle.Information) '?2
            '                'S.SANDEEP [ 24 JUNE 2011 ] -- END 
    '                        txtPassword.Focus()
    '                        Exit Sub
    '                    End If
            '            End If
            '        Else
            '            If objUser._Password <> txtPassword.Text Then
            '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot login, The password is invalid. Enter correct password to login."), enMsgBoxStyle.Information) '?2
            '        txtPassword.Focus()
            '        Exit Sub
            '        End If
            '    End If

            '    If objUser._Password = txtPassword.Text Then
            '        User.Refresh()
            '        User._Object._Userunkid = mintUserUnkid
            '        'S.SANDEEP [ 08 June 2011 ] -- START
            '        Dim objAppSetting As New clsApplicationSettings
            '        objAppSetting._LastUserId = mintUserUnkid
            '        objAppSetting = Nothing
            '        'S.SANDEEP [ 08 June 2011 ] -- START
            '    End If
            '    'Dim objMain As New clsMain
            '    'objMain.makeUserObject(mintUserUnkid)
            '    'objMain = Nothing

            '    'Dim objMachine As New clsMachineSettings(enApplication.eZeeFD)
            '    'objMachine._LastUserLoggedIn = mintUserUnkid

            '    mblnCancel = False
            '    Me.Close()
            '    'frmSplash.Hide()
            '    'frmMDI.Show()
            'End If



    '        'If mintUserUnkid > 0 Then
    '        '    objUser._Userunkid = mintUserUnkid

    '        '    If objUser._Userunkid <> 1 Then
    '        '        'S.SANDEEP [ 30 May 2011 ] -- START
    '        '        'ISSUE : FINCA REQ.
    '        '        If objUser._IsAcc_Locked = True Then
    '        '            Dim StrMsg As String = ""
    '        '            If objUser.PWDOptions._IsUnlockedByAdmin = False Then
    '        '                StrMsg = Language.getMessage(mstrModuleName, 9, "You can relogin after ") & objUser.PWDOptions._AlloUserRetry & Language.getMessage(mstrModuleName, 10, " Minite(s).")
    '        '                StrMsg &= Language.getMessage(mstrModuleName, 11, " Or Please contact Adminstrator.")
    '        '            End If
    '        '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, You can not log in, Your account is locked.") & " " & StrMsg, enMsgBoxStyle.Information)
    '        '            Exit Sub
    '        '        End If
    '        '        'S.SANDEEP [ 30 May 2011 ] -- END 

    '        '        If objUser._Password <> txtPassword.Text Then
    '        '            'S.SANDEEP [ 30 May 2011 ] -- START
    '        '            'ISSUE : FINCA REQ.
    '        '            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You can not log in, The password is invalid. Enter correct password to log in."), enMsgBoxStyle.Information) '?2
    '        '            'txtPassword.Focus()
    '        '            'txtPassword.Text = txtPassword.SelectedText

    '        '            Dim StrMessage As String = String.Empty

    '        '            If objUser.PWDOptions._IsAccLockSet = True Then
    '        '                intTry = objUser.PWDOptions._LockUserAfter

    '        '                objUAL = New clsUser_Acc_Lock

    '        '                objUAL._Userunkid = mintUserUnkid

    '        '                If objUser.PWDOptions._LockUserAfter = objUAL._Attempts Then
    '        '                    Dim StrMsg As String = Language.getMessage(mstrModuleName, 8, "Sorry, You can not log in, Your account is locked.")
    '        '                    If objUser.PWDOptions._IsUnlockedByAdmin = False Then
    '        '                        StrMsg &= Language.getMessage(mstrModuleName, 9, "You can relogin after ") & objUser.PWDOptions._AlloUserRetry & Language.getMessage(mstrModuleName, 10, " Minite(s).")
    '        '                    End If
    '        '                    StrMsg &= Language.getMessage(mstrModuleName, 11, " Or Please contact Adminstrator.")
    '        '                    eZeeMsgBox.Show(StrMsg, enMsgBoxStyle.Information)
    '        '                    Exit Sub
    '        '                End If

    '        '                If objUAL._Attempts <> 0 Then
    '        '                    intTry = objUser.PWDOptions._LockUserAfter - objUAL._Attempts
    '        '                End If
    '        '                StrMessage = Language.getMessage(mstrModuleName, 5, "As per password policy settings your account will be locked after ") & objUser.PWDOptions._LockUserAfter & Language.getMessage(mstrModuleName, 6, " attempts. And you have ") & intTry - 1 & Language.getMessage(mstrModuleName, 7, " attempts left.")
    '        '            End If

    '        '            If StrMessage.Trim.Length > 0 Then
    '        '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You can not log in, The password is invalid. Enter correct password to log in.") & StrMessage, enMsgBoxStyle.Information) '?2
    '        '                StrMessage = String.Empty

    '        '                objUAL._Userunkid = mintUserUnkid
    '        '                objUAL._Ip = getIP()
    '        '                objUAL._Machine_Name = getHostName()
    '        '                objUAL._Unlockuserunkid = 0
    '        '                objUAL._Isunlocked = objUAL._Isunlocked

    '        '                Dim dtTime As DateTime = ConfigParameter._Object._CurrentDateAndTime

    '        '                If objUAL._Attempts <> 0 Then
    '        '                    objUAL._Attempts = objUAL._Attempts + 1
    '        '                    If objUAL._Attempts = objUser.PWDOptions._LockUserAfter Then
    '        '                        objUser._IsAcc_Locked = True
    '        '                        objUser._Locked_Time = ConfigParameter._Object._CurrentDateAndTime
    '        '                        If objUser.PWDOptions._IsUnlockedByAdmin = False Then
    '        '                            If objUser._Userunkid = 1 Then
    '        '                                objUser._LockedDuration = dtTime.AddMinutes(5)
    '        '                            Else
    '        '                                objUser._LockedDuration = dtTime.AddMinutes(objUser.PWDOptions._AlloUserRetry)
    '        '                            End If
    '        '                        Else
    '        '                            If objUser._Userunkid = 1 Then
    '        '                                objUser._LockedDuration = dtTime.AddMinutes(5)
    '        '                            Else
    '        '                                objUser._LockedDuration = Nothing
    '        '                            End If
    '        '                        End If
    '        '                        'S.SANDEEP [ 24 JUNE 2011 ] -- START
    '        '                        'ISSUE : DELETION OF PRIVILEGE WHILE UNLOCKING USER
    '        '                        'objUser.Update()
    '        '                        objUser.Update(True)
    '        '                        'S.SANDEEP [ 24 JUNE 2011 ] -- END 
    '        '                        objUAL._Lockedtime = ConfigParameter._Object._CurrentDateAndTime
    '        '                    End If
    '        '                    Call objUAL.Update()
    '        '                Else
    '        '                    objUAL._Attempts = 1
    '        '                    objUAL._Lockedtime = Nothing
    '        '                    Call objUAL.Insert()
    '        '                End If

    '        '                'S.SANDEEP [ 24 JUNE 2011 ] -- START
    '        '                'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        '            Else
    '        '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot login, The password is invalid. Enter correct password to login.") & StrMessage, enMsgBoxStyle.Information) '?2
    '        '                'S.SANDEEP [ 24 JUNE 2011 ] -- END 
    '        '            End If
    '        '        Else
    '        '            If objUser._Password <> txtPassword.Text Then
    '        '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot login, The password is invalid. Enter correct password to login."), enMsgBoxStyle.Information) '?2
    '        '            End If
    '        '        End If
    '        '        txtPassword.Focus()
    '        '        txtPassword.Text = txtPassword.SelectedText
    '        '        'S.SANDEEP [ 30 May 2011 ] -- END 
    '        '        Exit Sub
    '        '    Else
    '        '        If objUser._Password <> txtPassword.Text Then
    '        '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot login, The password is invalid. Enter correct password to login."), enMsgBoxStyle.Information) '?2
    '        '        End If
    '        '        txtPassword.Focus()
    '        '        txtPassword.Text = txtPassword.SelectedText
    '        '        Exit Sub
    '        '    End If
    '        '    If objUser._Password = txtPassword.Text Then
    '        '        User.Refresh()
    '        '        User._Object._Userunkid = mintUserUnkid
    '        '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        '        Dim objAppSetting As New clsApplicationSettings
    '        '        objAppSetting._LastUserId = mintUserUnkid
    '        '        objAppSetting = Nothing
    '        '        'S.SANDEEP [ 08 June 2011 ] -- START
    '        '    End If
    '        '    'Dim objMain As New clsMain
    '        '    'objMain.makeUserObject(mintUserUnkid)
    '        '    'objMain = Nothing

    '        '    'Dim objMachine As New clsMachineSettings(enApplication.eZeeFD)
    '        '    'objMachine._LastUserLoggedIn = mintUserUnkid

    '        '    mblnCancel = False
    '        '    Me.Close()
    '        '    'frmSplash.Hide()
    '        '    'frmMDI.Show()
    '        'End If

    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "btnLogin_Click", mstrModuleName)
    '    Finally
    '        objTempUser = Nothing
    '    End Try
    'End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " UI Settings "

    Private Sub setColor()
        Try
            txtUserName.BackColor = GUI.ColorComp
            txtPassword.BackColor = GUI.ColorOptional
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

#End Region

    'S.SANDEEP [ 28 JULY 2011 ] -- START
    'ENHANCEMENT : CHANGE IN LOGIN FLOW. 
    Private Sub txtUserName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUserName.TextChanged
        Try
            txtPassword.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtUserName_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 28 JULY 2011 ] -- END 

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.objgbLogin.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.objgbLogin.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnCancel.GradientBackColor = GUI._ButttonBackColor 
			Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

			Me.btnLogin.GradientBackColor = GUI._ButttonBackColor 
			Me.btnLogin.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.UsernameLabel.Text = Language._Object.getCaption(Me.UsernameLabel.Name, Me.UsernameLabel.Text)
			Me.PasswordLabel.Text = Language._Object.getCaption(Me.PasswordLabel.Name, Me.PasswordLabel.Text)
			Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
			Me.btnLogin.Text = Language._Object.getCaption(Me.btnLogin.Name, Me.btnLogin.Text)

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 3, "Username does not match with the policy set for username. Please change your username.")
            Language.setMessage("clsUserAddEdit", 4, "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login.")
			Language.setMessage(mstrModuleName, 3, "Username does not match with the policy set for username. Please change your username.")
            Language.setMessage(mstrModuleName, 4, "Password changed successfully. Please login with new password.")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot login to system. Reason : your password has been expired." & vbCrLf & _
                                                           "Do you want to change your password now?")
			Language.setMessage(mstrModuleName, 6, "Password does not match with policy enforced. Please change your password now.")
            Language.setMessage(mstrModuleName, 7, "Please enter a Valid Username and Password.")
			Language.setMessage(mstrModuleName, 8, "Sorry, We are not able to connect aruti database server. Please check if database server is reachable or try again after some time or contact aruti support team.")
			Language.setMessage(mstrModuleName, 4, "Password changed successfully. Please login with new password.")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
