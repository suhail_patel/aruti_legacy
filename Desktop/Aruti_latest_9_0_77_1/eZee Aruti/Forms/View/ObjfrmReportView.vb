﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports

#End Region

Public Class ObjfrmReportView

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "ObjfrmReportView"

#End Region

#Region " Form's Events "

    Private Sub ObjfrmReportView_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call FillReports()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ObjfrmReportView_Load", mstrModuleName)
        End Try
    End Sub

    'Nilay (18-Nov-2015) -- Start
    'Enhancement: Searching Reports
    'Private Sub ObjfrmReportView_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
    '    Try
    '        If e.KeyChar = CChar(ChrW(Windows.Forms.Keys.Enter)) Then
    '            Call objbtnSearch.PerformClick()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ObjfrmReportView_KeyPress", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub txtKeyWord_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtKeyWord.KeyUp
        Try
            If e.KeyCode = Keys.Enter Then
                Call objbtnSearch.PerformClick()
            ElseIf e.KeyCode = Keys.Back Then
                Call objbtnSearch.PerformClick()
            ElseIf e.KeyCode = Keys.Delete Then
                If txtKeyWord.SelectedText.Trim.Length = txtKeyWord.Text.Trim.Length Then
                    txtKeyWord.Text = ""
                End If
                Call objbtnSearch.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtKeyWord_KeyUp", mstrModuleName)
        End Try
    End Sub

    'Nilay (18-Nov-2015) -- End
#End Region

#Region " Button's Events "

    Private Sub objbtnSearchReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillReports()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            txtKeyWord.Text = ""
            Call FillReports()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillReports()
        Try
            Dim dsReport As New DataSet
            Dim objReport As New clsArutiReportClass

            'Nilay (17-Mar-2015) -- Start
            'Enhancement- For searching full string
            'dsReport = objReport.getList(txtKeyWord.Text, False)

            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'dsReport = objReport.getList(txtKeyWord.Text, True)
            dsReport = objReport.getList("hrmsConfiguration", User._Object._Userunkid, Company._Object._Companyunkid, True, txtKeyWord.Text, True, User._Object._Languageunkid)
            'S.SANDEEP [10 AUG 2015] -- END


            'Nilay (17-Mar-2015) -- End

            Dim intLastId As Integer = 0
            Dim nGroupNode As New TreeNode
            Dim nCurrentNode As New TreeNode

            'Sandeep [ 29 NOV 2010 ] -- Start
            Dim nFavoriteGroupNode As New TreeNode(Language.getMessage(mstrModuleName, 122, "My Favorite Report"))
            tvReports.Nodes.Clear()

            For i As Integer = 0 To dsReport.Tables(0).Rows.Count - 1
                If dsReport.Tables(0).Rows(i).Item("Assign") = 1 Then
                    If dsReport.Tables(0).Rows(i).Item("isfavorite") = True Then
                        nCurrentNode = nFavoriteGroupNode.Nodes.Add(1001 + dsReport.Tables(0).Rows(i).Item("reportunkid"), dsReport.Tables(0).Rows(i).Item("DisplayName"))
                    End If
                End If
            Next

            If nFavoriteGroupNode.Nodes.Count > 0 Then
                tvReports.Nodes.Add(nFavoriteGroupNode)
            End If
            'Sandeep [ 29 NOV 2010 ] -- End 

            For i As Integer = 0 To dsReport.Tables(0).Rows.Count - 1
                'Sandeep [ 29 NOV 2010 ] -- Start
                'If intLastId <> dsReport.Tables(0).Rows(i).Item("reportcategoryunkid") Then
                '    nGroupNode = New TreeNode
                '    nGroupNode = tvReports.Nodes.Add(dsReport.Tables(0).Rows(i).Item("reportcategoryunkid"), dsReport.Tables(0).Rows(i).Item("CategoryName"))
                '    intLastId = dsReport.Tables(0).Rows(i).Item("reportcategoryunkid")
                '    nGroupNode.Tag = -1
                'End If
                'nCurrentNode = nGroupNode.Nodes.Add(dsReport.Tables(0).Rows(i).Item("reportunkid"), dsReport.Tables(0).Rows(i).Item("DisplayName"))
                'nCurrentNode.Tag = dsReport.Tables(0).Rows(i).Item("reportunkid")
                If dsReport.Tables(0).Rows(i).Item("Assign") = 1 Then
                    'If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Employee Assessment Form" OrElse dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Employee Assessment Report" OrElse dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Discipline Analysis Report" Then Continue For

                   
                    If ConfigParameter._Object._AccountingSoftWare <> enIntegration.Orbit Then
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Orbit" Then Continue For
                    End If

                    'Sohail (01 Oct 2016) -- Start
                    'Enhancement - 63.1 - Sun Account Project JV Report as New Separate Report in Payroll Reports if Sun Account Project JV selected in Accounting Software in Integration tab on Configuration.
                    If ConfigParameter._Object._AccountingSoftWare <> enIntegration.SUN_ACCOUNT_PROJECT_JV Then
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "SUN Account (Project JV) Report" Then Continue For
                    End If
                    'Sohail (01 Oct 2016) -- End

                    'Pinkal (21-Oct-2013) -- Start
                    'Enhancement : Oman Changes
                    If ConfigParameter._Object._PolicyManagementTNA = False Then
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Attendance Summary Report" Then Continue For
                    End If
                    'Pinkal (21-Oct-2013) -- End



                    'Pinkal (06-Aug-2015) -- Start
                    'Enhancement - WORKING ON CITIONE TNA CHANGES REQUIRED BY RUTTA.
                    If ConfigParameter._Object._FirstCheckInLastCheckOut = True Then
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Employee Break Timesheet" Then Continue For
                    End If
                    'Pinkal (06-Aug-2015) -- End




                    'Pinkal (01-Oct-2014) -- Start
                    'Enhancement -  This Report is for DRC Country
                    If Company._Object._Countryunkid <> 51 AndAlso Company._Object._Countryunkid <> 52 Then  'CONGO & Congo Democractic Republic of the
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Staff Movement Report" Then Continue For
                    End If
                    'Pinkal (01-Oct-2014) -- End



                    'Pinkal (23-Feb-2018) -- Start
                    'Enhancement - Working on B5 Customize Medical Reports.
                    If ArtLic._Object.HotelName.Trim().ToUpper() <> "B5 PLUS LTD" Then
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Advance Statement Report" Then Continue For
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Payment Voucher Report" Then Continue For
                    End If
                    'Pinkal (23-Feb-2018) -- End


                    'Pinkal (12-May-2018) -- Start
                    'Enhancement - Ref # 205 Claim Request Form For Abood Group.
                    'If ArtLic._Object.HotelName.Trim().ToUpper() <> "ABOOD GROUP OF COMPANIES" Then
                    '    If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Employee Claim Form" Then Continue For
                    'End If

                    'Pinkal (08-Aug-2019) -- Starts
                    'Enhancement [Good Neighbours] -Working on adding Total count of Early/Arrilval Report.
                    'If Company._Object._Name.Trim().ToUpper() <> "ABOOD GROUP OF COMPANIES." Then
                    '    If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Employee Claim Form" Then Continue For
                    'End If

                    'Pinkal (05-May-2020) -- Start
                    'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
                    'If Company._Object._Name.Trim().ToUpper() <> "ABOOD GROUP OF COMPANIES." AndAlso Company._Object._Name.Trim().ToUpper() <> "GOOD NEIGHBORS TANZANIA" Then
                    '    If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Employee Claim Form" Then Continue For
                    'End If
                    'Pinkal (05-May-2020) -- End

                   
                    'Pinkal (08-Aug-2019) -- End

                    'Pinkal (12-May-2018) -- End

                    'Gajanan (06 Oct 2018) -- Start
                    'Enhancement : Implementing New Module of Asset Declaration Template 2
                    '11-asset Declaration report Category
                    If ConfigParameter._Object._AssetDeclarationTemplate = enAsset_Declaration_Template.Template2 Then
                        If dsReport.Tables(0).Rows(i).Item("reportcategoryunkid").ToString.Trim = enArutiReportCategory.Asset_Declaration_Reports Then Continue For
                    End If
                    'Gajanan(06 Oct 2018) -- End

                    'Sohail (16 Dec 2021) -- Start
                    'Issue : OLD-531 : NMB - Error on selecting Confidentiality Acknowledgement & Non Disclosure Reports when Asset Declaration Template 1 is selected.
                    If CInt(dsReport.Tables(0).Rows(i).Item("reportunkid")) = CInt(enArutiReport.Asset_Declaration_Status_Report) Then Continue For
                    If CInt(dsReport.Tables(0).Rows(i).Item("reportunkid")) = CInt(enArutiReport.Confidentiality_Acknowledgement_Report) Then Continue For
                    If CInt(dsReport.Tables(0).Rows(i).Item("reportunkid")) = CInt(enArutiReport.Employee_NonDisclosure_Declaration_Report) Then Continue For
                    'Sohail (16 Dec 2021) -- End

                    'Pinkal (07-Jun-2019) -- Start
                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                    If ArtLic._Object.HotelName.Trim().ToUpper() <> "NMB PLC" OrElse Company._Object._Name.ToUpper() <> "NMB BANK PLC" Then
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Employee Expense Report" Then Continue For
                    End If
                    'Pinkal (07-Jun-2019) -- End



                    'Pinkal (29-Jan-2020) -- Start
                    'Enhancement - Changes related To OT NMB Testing.
                    If ConfigParameter._Object._SetOTRequisitionMandatory = False Then
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "OT Requisition Report" OrElse dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "OT Requisition Detail Report" Then Continue For
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "OT Requisition Summary Report" Then Continue For
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Employee OT Requisition Report" Then Continue For
                    End If
                    'Pinkal (29-Jan-2020) -- End


                    'Pinkal (16-May-2020) -- Start
                    'Enhancement NMB Leave Report Changes -   Working on Leave Reports required by NMB.
                    If ArtLic._Object.HotelName.Trim().ToUpper() <> "NMB PLC" OrElse Company._Object._Name.ToUpper() <> "NMB BANK PLC" Then
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Leave Absence Detailed Report" OrElse dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Leave Absence Summary Report" Then Continue For
                    End If
                    'Pinkal (16-May-2020) -- End


                    'Pinkal (20-Nov-2020) -- Start
                    'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.
                    If Company._Object._Name.ToUpper() <> "KADCO" Then
                        If dsReport.Tables(0).Rows(i).Item("displayname").ToString.Trim = "Fuel Consumption Report" Then Continue For
                    End If
                    'Pinkal (20-Nov-2020) -- End



                    If intLastId <> dsReport.Tables(0).Rows(i).Item("reportcategoryunkid") Then
                        nGroupNode = New TreeNode
                        nGroupNode = tvReports.Nodes.Add(dsReport.Tables(0).Rows(i).Item("reportcategoryunkid"), dsReport.Tables(0).Rows(i).Item("CategoryName"))
                        intLastId = dsReport.Tables(0).Rows(i).Item("reportcategoryunkid")
                        nGroupNode.Tag = -1
                    End If
                    nCurrentNode = nGroupNode.Nodes.Add(dsReport.Tables(0).Rows(i).Item("reportunkid"), dsReport.Tables(0).Rows(i).Item("DisplayName"))
                    nCurrentNode.Tag = dsReport.Tables(0).Rows(i).Item("reportunkid")
                End If
                'Sandeep [ 29 NOV 2010 ] -- End
            Next

            tvReports.CollapseAll()
            If tvReports.Nodes.Count > 0 Then
                tvReports.TopNode = tvReports.Nodes(0)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillReports", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub tvReports_NodeMouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvReports.NodeMouseClick
        If e.Button = Windows.Forms.MouseButtons.Right Then
            RemoveHandler tvReports.AfterSelect, AddressOf tvReports_AfterSelect
            tvReports.SelectedNode = e.Node
            AddHandler tvReports.AfterSelect, AddressOf tvReports_AfterSelect
        End If
    End Sub

    Private Sub tvReports_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvReports.AfterSelect
        Dim objfrm As Object = Nothing
        Try

            'If e.Node.Nodes.Count <> 0 Then Exit Sub

            If e.Node.Parent Is Nothing Then
                If pnlReportSummary.Controls.Count > 0 Then
                    pnlReportSummary.Controls.RemoveAt(0)
                End If
                Exit Sub
            End If

            'Dim intReportId As enArutiReport = IIf(Val(e.Node.Name) > 1000, Val(e.Node.Name) - 1001, Val(e.Node.Name))
            Dim intReportId As enArutiReport
            If Val(e.Node.Name) > 1000 AndAlso Val(e.Node.Name) < 9000 Then
                intReportId = Val(e.Node.Name) - 1001
            Else
                intReportId = Val(e.Node.Name)
            End If


            Select Case intReportId
                'Payroll
                Case enArutiReport.PayrollReport
                    objfrm = New frmPayrollReport

                Case enArutiReport.BankPaymentList
                    objfrm = New frmBankPaymentList
                Case enArutiReport.BankSummary
                    objfrm = New frmBankSummaryReport
                Case enArutiReport.EDSpreadSheet
                    objfrm = New frmEDSpreadSheetReport
                Case enArutiReport.EmployeeBankRegisterList
                    objfrm = New frmEmployeeBankRegisterList
                Case enArutiReport.EmployeeHeadCount
                    objfrm = New frmEmployeeHeadCount
                Case enArutiReport.PayrollReport
                    objfrm = New frmPayrollReport
                Case enArutiReport.PayrollSummary
                    objfrm = New frmPayrollSummary
                Case enArutiReport.PayrollTotalVariance
                    objfrm = New frmPayrollTotalVariance
                Case enArutiReport.PayrollVariance
                    objfrm = New frmPayrollVariance
                Case enArutiReport.PaySlip
                    objfrm = New frmPaySlip
                Case enArutiReport.LoanAdvanceSaving
                    objfrm = New frmLoanAdvanceSaving
                Case enArutiReport.CoinageAnalysisReport
                    objfrm = New frmCoinAgeAnalysis
                Case enArutiReport.PensionFundReport
                    objfrm = New frmPensionFundReport
                Case enArutiReport.IncomeTaxDeductionP9
                    objfrm = New frmIncomeTaxDeductionP9
                Case enArutiReport.IncomeTaxFormP10
                    objfrm = New frmIncomeTaxFormP10
                Case enArutiReport.PPFContribution
                    objfrm = New frmPPFContribution(enArutiReport.PPFContribution, User._Object._Languageunkid, Company._Object._Companyunkid)
                Case enArutiReport.JournalVoucherLedger
                    objfrm = New frmJVLedgerReport
                    'Anjan (28 Dec 2010)-End
                    'Leave Reports
                Case enArutiReport.EmployeeLeaveAbsenceSummary
                    objfrm = New frmEmployeeLeaveAbsenceSummary
                Case enArutiReport.EmployeeLeaveBalanceList
                    objfrm = New frmEmployeeLeaveBalanceList

                    'HR Reports
                    'Anjan (27 Dec 2010)-Start
                Case enArutiReport.TrainingCostReport
                    objfrm = New frmTrainingCostReport
                    'Anjan (27 Dec 2010)-End
                  Case enArutiReport.Employee_Skills
                    objfrm = New frmEmployeeSkillsReport
                Case enArutiReport.Employee_Beneficairy_Report
                    objfrm = New frmEmployeeBeneficiaryReport
                Case enArutiReport.Employee_Qualification_Report
                    objfrm = New frmEmployeeQualificationReport
                Case enArutiReport.Employee_Referee_Report
                    objfrm = New frmEmployeeRefereeReport
                Case enArutiReport.DailyTimeSheet 'Vimal (21 Dec 2010) -- Start 
                    objfrm = New frmDailyTimeSheet
                Case enArutiReport.TrainingListReport
                    objfrm = New frmTrainingListReport
                Case enArutiReport.TrainingBudgetReport
                    objfrm = New frmTrainingBudgetReport
                Case enArutiReport.EmployeeAgeAnalysis
                    objfrm = New frmEmployeeAgeAnalysisReport
                Case enArutiReport.EmployeeDistributionReport
                    objfrm = New frmEmployeeDistributionReport
                Case enArutiReport.EmployeeMonthlyPhysicalReport
                    objfrm = New frmEmpMonthlyPhysicalReport
                    'Case enArutiReport.EmployeeAssessmentListReport
                    '    objfrm = New frmAssessmentListReport
                Case enArutiReport.EmployeeAssessmentFormReport
                    objfrm = New frmAssessmentFormReport
                Case enArutiReport.EmployeeBiodataReport
                    objfrm = New frmEmployeeBiodata
                Case enArutiReport.EmployeeRelationReport
                    objfrm = New frmEmployeeRelationReport
                Case enArutiReport.TrainingAnalysisReport
                    objfrm = New frmTrainingAnalysisReport
                Case enArutiReport.Training_Void_CancelReport
                    objfrm = New frmTrainingVoidCancelReport
                Case enArutiReport.TrainingEnroll_Void_CancelReport
                    objfrm = New frmTrainingEnrollVCReport

                Case enArutiReport.JobEmploymentHistory
                    objfrm = New frmJobEmploymentHistory
                Case enArutiReport.CashPaymentList
                    objfrm = New frmCashPaymentList

                Case enArutiReport.EmployeeTimeSheet
                    objfrm = New frmEmployeeTimeSheetReport
                Case enArutiReport.EmployeeShortTime_OverTime
                    objfrm = New frmEmployeeShort_OT_Report

                Case enArutiReport.LeaveIssue_AT_Report
                    objfrm = New frmLeaveIssue_AT_Report

                Case enArutiReport.LeaveBalance_AT_Report
                    objfrm = New frmLeaveBalance_AT_Report

                Case enArutiReport.Timesheet_AT_Report
                    objfrm = New frmTimesheet_AT_Report

                    'Case enArutiReport.LeaveApprover_AT_Report
                    '    objfrm = New frmLeaveApprover_AT_Report

Case enArutiReport.ATProcessPendingLAReport   'Sandeep [ 01 MARCH 2011 ] -- Start -- End
                    objfrm = New frmATProcessPendingLA
                Case enArutiReport.ATLoanAdvanceReport   'Sandeep [ 01 MARCH 2011 ] -- Start -- End
                    objfrm = New frmATLoanAdvanceReport
                Case enArutiReport.NSSF_FORM_5 'Sandeep [ 01 MARCH 2011 ] -- Start -- End
                    objfrm = New frmNSSFForm5Report(enArutiReport.NSSF_FORM_5, User._Object._Languageunkid, Company._Object._Companyunkid)
                Case enArutiReport.ATPaymentTranReport 'Sandeep [ 01 MARCH 2011 ] -- Start -- End
                    objfrm = New frmATPaymentTranReport

                Case enArutiReport.SDL_Statutory_Report
                    objfrm = New frmSDLReport

                Case enArutiReport.ATSavingTranReport
                    objfrm = New frmATSavingTranReport
                    'Pinkal (20-Apr-2011) -- End

                    'Anjan (21 May 2011)-Start
                Case enArutiReport.EmployeeAssetsRegister
                    objfrm = New frmEmployeeAssetRegister
                    'Anjan (21 May 2011)-End 

                    'Sandeep [ 02 JUNE 2011 ] -- Start
                Case enArutiReport.Vacancy_Report
                    objfrm = New frmVacancyReport
                Case enArutiReport.Batch_Schedule_Report
                    objfrm = New frmBatchScheduleReport
                Case enArutiReport.Interview_Analysis_Report
                    objfrm = New frmInterviewAnalysisReport
                Case enArutiReport.Exempt_Head_Report
                    objfrm = New frmExemptHeadReport
                    'Sandeep [ 02 JUNE 2011 ] -- End
 'Pinkal (15-Jun-2011) -- Start

 Case enArutiReport.Employee_Movement_Report
                    objfrm = New frmEmployeeMovementReport

                    'Case enArutiReport.DisciplineAnalysisReport
                    '    objfrm = New frmDiscipline_AnalysisReport

                Case enArutiReport.Employee_Listing
                    objfrm = New frmEmployee_Listing
                    'Pinkal (15-Jun-2011) -- End


                    'Pinkal (07-Jan-2012) -- Start
                    'Enhancement : TRA Changes
                Case enArutiReport.TRAMedical_Claim_Report
                    objfrm = New frmMedicalClaimReport
                    'Pinkal (07-Jan-2012) -- End


                    'Pinkal (20-Jan-2012) -- Start
                    'Enhancement : TRA Changes
                Case enArutiReport.EDDetail_Report
                    objfrm = New frmEDDetailReport
                    'Pinkal (20-Jan-2012) -- End

                Case enArutiReport.EmployeeAssessSoreRatingReport
                    objfrm = New frmAssessmentSoreRatingReport

                    'S.SANDEEP [30 JAN 2016] -- START
                    'Case enArutiReport.AppraisalSummaryReport
                    '    objfrm = New frmAppraisalSummReport
                Case enArutiReport.AppraisalSummaryReport
                    If Company._Object._Countryunkid <> 162 Then    ' OMAN
                    objfrm = New frmAppraisalSummReport
                    Else
                        objfrm = New frmOmanAppraisalSummaryReport
                    End If
                    'S.SANDEEP [30 JAN 2016] -- END


                    'Sohail (15 Feb 2012) -- Start
                    'TRA - ENHANCEMENT
                Case enArutiReport.TrainingNeedsPriorityForm
                    objfrm = New frmTrainingNeedsPriorityForm
                    'Sohail (15 Feb 2012) -- End

                    'S.SANDEEP [ 28 FEB 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.EmployeeRetirementReport
                    objfrm = New frmEmployeeRetirementReport
                    'S.SANDEEP [ 28 FEB 2012 ] -- END


                    'Pinkal (22-Mar-2012) -- Start
                    'Enhancement : TRA Changes

                Case enArutiReport.EmployeeWithNoDOB
                    objfrm = New frmEmployeeWithNoDOB

                Case enArutiReport.NewRecruitedEmployee
                    objfrm = New frmNewHiredEmployee

                Case enArutiReport.TerminatedEmployeeWithReason
                    objfrm = New frmTerminatedEmpWithReason

                    'Pinkal (22-Mar-2012) -- End

             'S.SANDEEP [ 24 MARCH 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.EmployeePersonalParticular
                    objfrm = New frmEmp_PersonalParticulars_Report
                Case enArutiReport.EmployeeCVReport
                    objfrm = New frmEmployeeCVReport
                Case enArutiReport.EmployeeStaffList
                    objfrm = New frmEmployeeStaffList
                    'S.SANDEEP [ 24 MARCH 2012 ] -- END

                 Case enArutiReport.ApplicantQualification
                    objfrm = New frmApplicantQualification

                    'Pinkal (22-Mar-2012) -- End
 
               'S.SANDEEP [ 02 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.Performance_AppraisalReport
                    objfrm = New frmPerformance_AppraisalReport
                Case enArutiReport.Training_Planning_Report
                    objfrm = New frmTrainingPlanReport
                    'S.SANDEEP [ 28 APRIL 2012 ] -- END
                    'Sohail (02 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                Case enArutiReport.LAPFContribution
                    objfrm = New frmLAPFContribution
                    'Sohail (02 Apr 2012) -- End

           'Anjan (20 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                Case enArutiReport.PSPFContribution
                    objfrm = New frmPPFContribution(enArutiReport.PSPFContribution, User._Object._Languageunkid, Company._Object._Companyunkid)
                    'Anjan (20 Mar 2012)-End 

'Pinkal (03-APR-2012) -- Start
                    'Enhancement : TRA Changes
                Case enArutiReport.EmployeeWorkingStation
                    objfrm = New frmEmployeeWorkingStation
                    'Pinkal (03-APR-2012) -- End

                    'S.SANDEEP [ 04 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.OverDeduction_NetPay_Report
                    objfrm = New frmOverDeduction_NetPay_Report
                Case enArutiReport.ManningLevel_Report
                    objfrm = New frmManningLevel_Report
                    'S.SANDEEP [ 04 APRIL 2012 ] -- END
                    'S.SANDEEP [ 03 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.Current_SalaryGradesReport
                    objfrm = New frmCurrSalaryGrade_Report
                    'S.SANDEEP [ 03 APRIL 2012 ] -- END
                    'Anjan (20 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                Case enArutiReport.GEPFContribution
                    objfrm = New frmPPFContribution(enArutiReport.GEPFContribution, User._Object._Languageunkid, Company._Object._Companyunkid)
                    'Anjan (20 Mar 2012)-End 
               'S.SANDEEP [ 06 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.Employee_without_Memebership
                    objfrm = New frmEmp_WithoutMembership_Report
                Case enArutiReport.Employee_ListLocation_Report
                    objfrm = New frmEmployee_List_Location_Report
                Case enArutiReport.Applicant_Other_Criteria_Report
                    objfrm = New frmApplicantOtherCriteria_Report
                Case enArutiReport.Discipline_Status_Summary_Report
                    objfrm = New frmDiscipline_Status_Summary_Report
                    'S.SANDEEP [ 06 APRIL 2012 ] -- END
                     'Sohail (06 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                Case enArutiReport.EmployeeWithoutED
                    objfrm = New frmEmployeeWithoutED
                Case enArutiReport.EmployeeDispositionReport
                    objfrm = New frmEmployeeDisposition_Report
                    'Sohail (06 Apr 2012) -- End

                    'Anjan (09 Apr 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                Case enArutiReport.EmployeeWithoutBank
                    objfrm = New frmEmp_WithoutBank_Report
                    'Anjan (09 Apr 2012)-End 

                    'S.SANDEEP [ 20 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.External_Discipline_Cases
                    objfrm = New frmExternalDisciplineReport
                    'S.SANDEEP [ 20 APRIL 2012 ] -- END

 'S.SANDEEP [ 07 MAY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.BBL_Loan_Report
                    objfrm = New frmBBL_Loan_Report
                Case enArutiReport.ApplicantCVReport
                    objfrm = New frmApplicantCVReport
                    'S.SANDEEP [ 07 MAY 2012 ] -- END

                    'Pinkal (11-MAY-2012) -- Start
                    'Enhancement : TRA Changes
                Case enArutiReport.ShortListed_Applicant
                    objfrm = New frmShortListedApplicantReport
                    'Pinkal (11-MAY-2012) -- End
 'Pinkal (15-MAY-2012) -- Start
                    'Enhancement : TRA Changes
                Case enArutiReport.EmployeeLeaveForm
                    objfrm = New frmEmployeeLeaveForm
                    'Pinkal (15-MAY-2012) -- End


                    'Pinkal (28-MAY-2012) -- Start
                    'Enhancement : TRA Changes
                Case enArutiReport.EOCEmployee
                    objfrm = New frmEOCEmployee
                    'Pinkal (28-MAY-2012) -- End



                    'Pinkal (03-Jun-2012) -- Start
                    'Enhancement : TRA Changes
                Case enArutiReport.DisciplinePendingCasesDetail
                    objfrm = New frmDisciplinePendingCasesDetail

                Case enArutiReport.EmployeeWithoutEmail
                    objfrm = New frmEmployee_Without_Email_Report

                    'Pinkal (03-Jun-2012) -- End

                    'S.SANDEEP [ 04 JULY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.ApplicantVacancyReport
                    objfrm = New frmApplicantVacancyReport
                    'S.SANDEEP [ 04 JULY 2012 ] -- END

                    'S.SANDEEP [ 12 JULY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.Employee_Salary_Change_Report
                    objfrm = New frmEmployee_Salary_Change_Report
                    'S.SANDEEP [ 12 JULY 2012 ] -- END

                    'S.SANDEEP [ 17 JULY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.Employee_WithSameName
                    objfrm = New frmEmp_With_Same_Names_Report
                    'S.SANDEEP [ 17 JULY 2012 ] -- END

                    'Pinkal (21-Jul-2012) -- Start
                    'Enhancement : TRA Changes

                Case enArutiReport.Employee_SameBankAccount
                    objfrm = New frmEmpSameBankAccReport

                    'Pinkal (21-Jul-2012) -- End


                    'Anjan (25 Jul 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Rutta's Request
                Case enArutiReport.ZSSF_FORM
                    objfrm = New frmNSSFForm5Report(enArutiReport.ZSSF_FORM, User._Object._Languageunkid, Company._Object._Companyunkid)
                    'Anjan (25 jul 2012)-End 



                    'Pinkal (10-Sep-2012) -- Start
                    'Enhancement : TRA Changes

                Case enArutiReport.EmployeeWithoutLeaveApprover
                    objfrm = New frmEmpListWithoutApprover
                    'Pinkal (10-Sep-2012) -- End

                    'Sohail (08 Nov 2012) -- Start
                    'TRA - ENHANCEMENT
                Case enArutiReport.PayrollHeadCount
                    objfrm = New frmPayrollHeadCount
                    'Sohail (08 Nov 2012) -- End


                    'Pinkal (10-Dec-2012) -- Start
                    'Enhancement : TRA Changes
                Case enArutiReport.EmployeeDetail_WithED
                    objfrm = New frmEmployeeDetail_withED
                    'Pinkal (10-Dec-2012) -- End
                    'Pinkal (14-Dec-2012) -- Start
                    'Enhancement : TRA Changes

                Case enArutiReport.MedicalBillSummaryReport
                    objfrm = New frmMedicalBillsummary

                    'Pinkal (14-Dec-2012) -- End


                    'Pinkal (18-Dec-2012) -- Start
                    'Enhancement : TRA Changes
                Case enArutiReport.LeaveApproverReport
                    objfrm = New frmLeaveApproverReport
                    'Pinkal (18-Dec-2012) -- End
                    'Sohail (19 Dec 2012) -- Start
                    'TRA - ENHANCEMENT
                Case enArutiReport.Orbit
                    objfrm = New frmOrbitReport
                    'Sohail (19 Dec 2012) -- End

                    'Anjan (25 Dec 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Rutta's Request
                Case enArutiReport.EmployeewithMultipleMembership
                    objfrm = New frmEmployeeWith_MultipleMembership_Report

                    'Anjan (25 Dec 2012)-End 


                    'Pinkal (18-Jan-2013) -- Start
                    'Enhancement : TRA Changes

                Case enArutiReport.LeaveStatementReport
                    objfrm = New frmLeaveStatement_Report

                    'Pinkal (18-Jan-2013) -- End

                    'S.SANDEEP [ 25 JAN 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.LoanAdvanceListReport
                    objfrm = New frmLoanAdvanceListReport
                    'S.SANDEEP [ 25 JAN 2013 ] -- END

                'Pinkal (24-Jan-2013) -- Start
                    'Enhancement : TRA Changes

                Case enArutiReport.EmployeeDurationReport
                    objfrm = New frmEmployeeDurationReport

                    'Pinkal (24-Jan-2013) -- End

                    'Pinkal (28-Jan-2013) -- Start
                    'Enhancement : TRA Changes

                Case enArutiReport.EndofProbation
                    objfrm = New frmEmpEndOfProbationReport

                    'Pinkal (28-Jan-2013) -- End

                    'Sohail (07 Feb 2013) -- Start
                    'TRA - ENHANCEMENT
                Case enArutiReport.Asset_Declaration_Report
                    objfrm = New frmEmp_AD_Report

                Case enArutiReport.Asset_Declaration_Category_Wise
                    objfrm = New frmAssetDeclarationCategoryWise
                    'Sohail (07 Feb 2013) -- End

                    'S.SANDEEP [ 09 MAR 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.Discipline_CaseStatus_Report
                    objfrm = New frmDiscipline_CaseStatus_Report
                    'S.SANDEEP [ 09 MAR 2013 ] -- END


                    'Pinkal (24-Apr-2013) -- Start
                    'Enhancement : TRA Changes

                Case enArutiReport.EmployeeWithOutImage
                    objfrm = New frmEmployee_Without_Image_Report

                Case enArutiReport.EmployeeWithOutGender
                    objfrm = New frmEmployee_Without_Gender_Report

                    'Pinkal (24-Apr-2013) -- End

                'Sohail (22 Apr 2013) -- Start
                Case enArutiReport.BSC_Planning_Report
                    objfrm = New frmBSC_Planning_Report
                    'Sohail (22 Apr 2013) -- End

                    'Sohail (18 May 2013) -- Start
                    'TRA - ENHANCEMENT
                Case enArutiReport.Tax_Report
                    objfrm = New frmTaxReport
                    'Sohail (18 May 2013) -- End

                    'Sohail (12 Jul 2013) -- Start
                    'TRA - ENHANCEMENT
                Case enArutiReport.ActivityDoneReport
                    objfrm = New frmActivityDone_Report
                    'Sohail (12 Jul 2013) -- End

                    'S.SANDEEP [ 25 JULY 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.ActivityTimesheetReport
                    objfrm = New frmAct_TimesheetReport
                    'S.SANDEEP [ 25 JULY 2013 ] -- END

                    'Pinkal (3-Aug-2013) -- Start
                    'Enhancement : TRA Changes

                Case enArutiReport.EmployeeIdentityReport
                    objfrm = New frmEmployeeIdentitiesReport
                    'Pinkal (3-Aug-2013) -- End

                    'Sohail (03 Aug 2013) -- Start
                    'TRA - ENHANCEMENT
                Case enArutiReport.EmployeeMembershipReport
                    objfrm = New frmEmployeeMembershipReport
                    'Sohail (03 Aug 2013) -- End

                    'Sohail (07 Aug 2013) -- Start
                    'TRA - ENHANCEMENT
                Case enArutiReport.JobReport
                    objfrm = New frmJobReport
                    'Sohail (07 Aug 2013) -- End

                    'Sohail (12 Aug 2013) -- Start
                    'TRA - ENHANCEMENT
                Case enArutiReport.P9A_Report
                    objfrm = New frmP9AReport 'Sohail (30 Jan 2014)
                Case enArutiReport.P10_Report
                    objfrm = New frmP10Report 'Sohail (13 Mar 2014)
                Case enArutiReport.PAYE_P11_Report
                    objfrm = New frmP11Report
                    'Sohail (12 Aug 2013) -- End

                    'S.SANDEEP [ 09 JULY 2013 ] -- START
                    'ENHANCEMENT : OTHER CHANGES
                Case enArutiReport.Za_ITF_P16_Report
                    objfrm = New frmZa_IFT_P16_Report
                Case enArutiReport.Za_Monthly_AMD_Report
                    objfrm = New frmZa_Monthly_Return_AMD
                Case enArutiReport.Za_AnnualPayeReturn
                    objfrm = New frmAnnualPayeReturn
                Case enArutiReport.Za_RemittanceForm
                    objfrm = New frmRemittancForm
                    'S.SANDEEP [ 09 JULY 2013 ] -- END

                    'Pinkal (18-Aug-2013) -- Start
                    'Enhancement : TRA Changes
                Case enArutiReport.KN_NHIFReport
                    objfrm = New frmNHIFReport
                    'Pinkal (18-Aug-2013) -- End

                    'S.SANDEEP [ 14 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.Performance_Evaluation_Report
                    objfrm = New frmPerformanceEvaluationReport
                    'S.SANDEEP [ 14 AUG 2013 ] -- END

                    'S.SANDEEP [ 26 SEPT 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                Case enArutiReport.Not_Asssessd_Employee_Report
                    objfrm = New frmNotAssessedEmployeeReport
                    'S.SANDEEP [ 26 SEPT 2013 ] -- END


                    'Pinkal (21-Oct-2013) -- Start
                    'Enhancement : Oman Changes

                Case enArutiReport.Attendance_Summary_Report
                    objfrm = New frmAttendanceSummaryReport

                    'Pinkal (21-Oct-2013) -- End


                    'Pinkal (15-Nov-2013) -- Start
                    'Enhancement : Oman Changes
                Case enArutiReport.Holiday_Attendance_Report
                    objfrm = New frmHolidayAttendanceReport
                    'Pinkal (15-Nov-2013) -- End

                    'Sohail (21 Nov 2013) -- Start
                    'ENHANCEMENT - OMAN
                Case enArutiReport.Gratuity_Report
                    objfrm = New frmGratuityReport
                    'Sohail (21 Nov 2013) -- End

                    'Pinkal (30-Nov-2013) -- Start
                    'Enhancement : Oman Changes

                Case enArutiReport.LeavePlanReport
                    objfrm = New frmLeavePlanReport
                    'Pinkal (30-Nov-2013) -- End

                    'S.SANDEEP [ 26 NOV 2013 ] -- START
                Case enArutiReport.Medical_Monthly_Payment
                    objfrm = New frmMAP_Malawi_Report
                    'S.SANDEEP [ 26 NOV 2013 ] -- END

                    'Sohail (03 Dec 2013) -- Start
                    'Enhancement - TBC
                Case enArutiReport.EDDetail_PeriodWise_Report
                    objfrm = New frmEDDetailPeriodWiseReport
                    'Sohail (03 Dec 2013) -- End

                    'Sohail (10 Dec 2013) -- Start
                    'Enhancement - End of Service report - OMAN
                Case enArutiReport.End_Of_Service_Report
                    objfrm = New frmEndOfServiceReport
                    'Sohail (10 Dec 2013) -- End

                    'S.SANDEEP [ 26 DEC 2013 ] -- START
                Case enArutiReport.Movement_Report
                    objfrm = New frmMovementReport
                    'S.SANDEEP [ 26 DEC 2013 ] -- END

                    'S.SANDEEP [ 09 JAN 2014 ] -- START
                Case enArutiReport.Employee_Schedule_Report
                    objfrm = New frmEmpScheduleReport
                    'S.SANDEEP [ 09 JAN 2014 ] -- END


                    'Pinkal (06-Mar-2014) -- Start
                    'Enhancement : TRA Changes
                Case enArutiReport.Pay_Per_Activity_Report
                    objfrm = New frmPPAReport
                    'Pinkal (06-Mar-2014) -- End

                    'Sohail (15 Mar 2014) -- Start
                    'Enhancement - Cost Center Report Branch Wise & Department Wise.
                Case enArutiReport.CostCenter_BranchWise_Report
                    objfrm = New frmCCReportBranchWise

                Case enArutiReport.CostCenter_DepartmentWise_Report
                    objfrm = New frmCCReportDepartmentWise

                Case enArutiReport.DepartmentGrp_Branchwise_Report
                    objfrm = New frmDeptGroupReportBranchWise

                Case enArutiReport.DepartmentGrp_General_Report
                    objfrm = New frmDeptGroupReportGeneral
                    'Sohail (15 Mar 2014) -- End

                    'Sohail (21 Mar 2014) -- Start
                    'Enhancement - Payment Journal Voucher Report.
                Case enArutiReport.Payment_Journal_Voucher_Report
                    objfrm = New frmPaymentJVReport
                    'Sohail (21 Mar 2014) -- End

                    'Pinkal (25-Apr-2014) -- Start
                    'Enhancement : TRA Changes

                Case enArutiReport.Leave_Form_Approval_Status_Report
                    objfrm = New frmLvForm_ApprovalStatus_Report

                Case enArutiReport.Issued_Leave_Form_Detail_Report
                    objfrm = New frmIssued_LvFormDetail_Report

                    'Pinkal (25-Apr-2014) -- End

                    'Sohail (23 May 2014) -- Start
                    'Enhancement - Loan Service Award Report for TANAPA 
                Case enArutiReport.Long_Service_Award_Report
                    objfrm = New frmLongServiceAwardReport
                    'Sohail (23 May 2014) -- End


                    'Sohail (10 May 2014) -- Start
                    'Enhancement - EPF Contribution Report for Sri Lanka 
                Case enArutiReport.EPF_Contribution_Report
                    objfrm = New frmEPFContributionReport
                    'Sohail (10 May 2014) -- End

                
                    'S.SANDEEP [ 01 JUL 2014 ] -- START
                Case enArutiReport.Employee_With_Out_Report
                    objfrm = New frmEmployeeWithoutReport
                    'S.SANDEEP [ 01 JUL 2014 ] -- END

                    'Sohail (26 Jul 2014) -- Start
                    'Enhancement - New iTax Form B Report for Kenya.
                Case enArutiReport.ITax_Form_B_Report
                    'Nilay (11 Apr 2017) -- Start
                    'Enhancements: New statutory report iTax Form C for CCK
                    'objfrm = New frmITaxFormBReport
                    objfrm = New frmITaxFormBReport(enArutiReport.ITax_Form_B_Report, User._Object._Languageunkid, Company._Object._Companyunkid)
                    'Nilay (11 Apr 2017) -- End

                    'Sohail (26 Jul 2014) -- End

                    'Pinkal (04-Aug-2014) -- Start
                    'Enhancement - NEW Leave Liability Report FOR Zanzibar Residence
                Case enArutiReport.Leave_Liability_Report
                    objfrm = New frmLeaveLiability_Report
                    'Pinkal (04-Aug-2014) -- End

                   'Sohail (12 Aug 2014) -- Start
                    'Enhancement - New EPF PEriod Wise Report.
                Case enArutiReport.EPF_PeriodWise_Report
                    objfrm = New frmEPFPeriodWiseReport
                    'Sohail (12 Aug 2014) -- End

                    'Sohail (28 Aug 2014) -- Start
                    'Enhancement - New Statutory Report Monthly Tax Income Report.
                Case enArutiReport.Monthly_Tax_Income_Report
                    objfrm = New frmMonthlyTaxIncomeReport
                    'Sohail (28 Aug 2014) -- End

                    'Pinkal (19-Sep-2014) -- Start
                    'Enhancement -  DEVEPLOING NEW MEDICAL REPORTS FOR FINCA CONGO(FDRC)
                Case enArutiReport.Employee_Medical_Cover_Summary
                    objfrm = New frmEmpMedicalCover_Summary

                  Case enArutiReport.Staff_Movement_Report
                    objfrm = New frmStaffMovement_Report

                    'Pinkal (19-Sep-2014) -- End

     'S.SANDEEP [ 06 OCT 2014 ] -- START
                Case enArutiReport.Employee_Medical_Cover_Detail
                    objfrm = New frmEmpMedicalCover_Detail_Report
                    'S.SANDEEP [ 06 OCT 2014 ] -- END

                    'Sohail (07 Oct 2014) -- Start
                    'Enhancement - New Report Salary Reconciliation Report for FINCA DRC.
                Case enArutiReport.Salary_Reconciliation_Report
                    objfrm = New frmSalaryReconciliationReport
                    'Sohail (07 Oct 2014) -- End

                    'Sohail (20 Oct 2014) -- Start
                    'Enhancement - New Report Termination Package Report for FINCA DRC.
                Case enArutiReport.Termination_Package_Report
                    objfrm = New frmTerminationPackageReport
                    'Sohail (20 Oct 2014) -- End

                    'Sohail (03 Dec 2014) -- Start
                    'AKF Enhancement - New Report E&D Detailed Report with Bank Account.
                Case enArutiReport.EDDetail_Bank_Account_Report
                    objfrm = New frmEDDetailBankAccountReport
                    'Sohail (03 Dec 2014) -- End

                    'Sohail (05 Dec 2014) -- Start
                    'Marie Stopes Enhancement - New Report Employee Salary On Hold Report.
                Case enArutiReport.Employee_Salary_On_Hold_Report
                    objfrm = New frmEmployeeSalaryOnHoldReport
                    'Sohail (05 Dec 2014) -- End
                 'Pinkal (16-Sep-2014) -- Start
                    'Enhancement - TRANSFER EMPLOYEE SCHEDULE REPORT FROM HR CATEGORY TO TNA CATEGORY [ AS PER VOLTAMP MR.PRABHAKAR REQUEST]
                Case enArutiReport.Air_Passage_Report
                    objfrm = New frmAirPassageReport
                    'Pinkal (16-Sep-2014) -- End

                    'Sohail (06 Apr 2015) -- Start
                    'Enhancement - New statutory report HELB report for Kenya.
                Case enArutiReport.HELB_REPORT
                    objfrm = New frmHELBReport
                    'Sohail (06 Apr 2015) -- End

                    'Sohail (09 Apr 2015) -- Start
                    'CCK Enhancement - New report Detailed Salary Breakdown report.
                Case enArutiReport.DETAILED_SALARY_BREAKDOWN_REPORT
                    objfrm = New frmDetailedSalaryBreakdownReport
                    'Sohail (09 Apr 2015) -- End

                    'Shani (03 JUN 2015) -- Start
                    'Enhancement - New Statutory Report Monthly Tax Deducation Schedule Report.
                Case enArutiReport.MONTHLY_TAX_DEDUCATION_SCHEDULE_REPORT
                    objfrm = New frmMonthlyTaxDeductionSchedule
                    'Shani (03 JUN 2015) -- End

                    'Sohail (05 Jun 2015) -- Start
                    'Enhancement - New Statutory Report Income Tax Division P12 for Malawi.
                Case enArutiReport.INCOME_TAX_DIVISION_P12_REPORT
                    objfrm = New frmIncomeTaxDivisionP12
                    'Sohail (05 Jun 2015) -- End


                    'Pinkal (21 Jul 2015) -- Start
                    'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
                Case enArutiReport.WCF_REPORT
                    objfrm = New frmWCFFormReport
                    'Pinkal (21 Jul 2015) -- End

                    'Pinkal (06-Aug-2015) -- Start
                    'Enhancement - WORKING ON CITIONE TNA CHANGES REQUIRED BY RUTTA.
                Case enArutiReport.Employee_Breaktime_Timesheet
                    objfrm = New frmEmpBreakTimesheetReport
                    'Pinkal (06-Aug-2015) -- End

                    'Shani(08-Aug-2015) -- Start
                    'Enhancement - C&R Enhancement Given by glory(CR Revised.docx)
                Case enArutiReport.CR_Summary_Report
                    objfrm = New frmCRSummaryReport
                    'Shani(08-Aug-2015) -- End


                    'Pinkal (14-Oct-2015) -- Start
                    'Enhancement - WORKING ON Daily Attendance Checking Report For Hyatt.
                Case enArutiReport.Daily_Attendance_Checking_Report
                    objfrm = New frmDailyAttendance_Report
                    'Pinkal (14-Oct-2015) -- End

                    'Sohail (30 Oct 2015) -- Start
                    'Enhancement - New Statutory Report I-TAX Form J Report and option for Prescribed Quarter Interest Rate on Period Master for Kenya.
                Case enArutiReport.ITax_Form_J_Report
                    objfrm = New frmITaxFormJReport
                    'Sohail (30 Oct 2015) -- End

                 'Sohail (07 Dec 2015) -- Start
                    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
                Case enArutiReport.P10A_P10D_Report
                    objfrm = New frmP10AP10DReport

                'Sohail (07 Dec 2015) -- End

                    'Shani(02-Dec-2015) -- Start
                    'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
                Case enArutiReport.Employee_Status_Report
                    objfrm = New frmEmployeeStatus
                    'Shani(02-Dec-2015) -- End

                    'Shani(01-APR-2016) -- Start
                    'Enhancement : New Seniority Level Report For TNP/KBC
                Case enArutiReport.Employee_Seniority_Report
                    objfrm = New frmEmployeeSeniorityReport
                    'Shani(01-APR-2016) -- End 
                    
                    'Pinkal (07-Jun-2016) --  Start
                    'Enhancement - IMPLEMENTING EMPLOYEE CLAIM BALANCE LIST REPORT  FOR CCK
                Case enArutiReport.Employee_Claim_Balance_Report
                    objfrm = New frmEmployeeClaimBalanceList
                    'Pinkal (07-Jun-2016) --  End


                    'Pinkal (06-Jun-2016) -- Start
                    'Enhancement - IMPLEMENTING LATE ARRIVAL AND EARLY GOING REPORT FOR ASP.
                Case enArutiReport.Late_Arrival_Report
                    objfrm = New frmLateArrial_EarlyGoingReport
                    'Pinkal (06-Jun-2016) -- End

                    'Sohail (07 Jun 2016) -- Start
                    'Enhancement -  62.1 - New Budget Redesign for Marie Stopes (Fund Sources, Funds Adjustments, Budget Formula and new Budget screen Allocation/Employee wise, Trans./Summary wise).
                Case enArutiReport.Salary_Budget_Breakdown_Report
                    objfrm = New frmSalaryBudgetBreakdown
                    'Sohail (07 Jun 2016) -- End

                    'Sohail (02 Sep 2016) -- Start
                    'Enhancement -  63.1 - New Budget Redesign for Marie Stopes (Period Wise budget codes).
                Case enArutiReport.Salary_Budget_Breakdown_By_Period_Report
                    objfrm = New frmSalaryBudgetBreakdownByPeriod
                    'Sohail (02 Sep 2016) -- End

                    'Sohail (01 Oct 2016) -- Start
                    'Enhancement - 63.1 - Sun Account Project JV Report as New Separate Report in Payroll Reports if Sun Account Project JV selected in Accounting Software in Integration tab on Configuration.
                Case enArutiReport.SUN_Account_Project_JV_Report
                    objfrm = New frmSunAccountProjectJVReport
                    'Sohail (01 Oct 2016) -- End

                    'Shani (24-Oct-2016) -- Start
                    'Enhancement - Add New Employee List Assessor Reviewer Report in PA 
                Case enArutiReport.Employee_List_Assessor_Reviewer_Report
                    objfrm = New frmEmployee_List_Asr_Rer
                    'Shani (24-Oct-2016) -- End

                    'Anjan [27 January 2017] -- Start
                    'ENHANCEMENT : New FTZ Auditors Report.
                Case enArutiReport.UserDetail_Auditors_Report
                    objfrm = New frmUserDetailsAuditorsReport
                    'Anjan [27 January 2017] -- End

                    'S.SANDEEP [13-FEB-2017] -- START
                    'ISSUE/ENHANCEMENT : Training Reports Changes & Enhancement(s)
                Case enArutiReport.Training_BasedOn_Report
                    objfrm = New frmTrainingBasedOnReport
                    'S.SANDEEP [13-FEB-2017] -- END

                    'Shani(14-FEB-2017) -- Start
                    'Enhancement - New Report requested by cck.
                Case enArutiReport.Custom_Item_Value_Report
                    objfrm = New frmCustomItemValueReport
                    'Shani(14-FEB-2017) -- End

                    'Sohail (20 Feb 2017) -- Start
                    'CCBRT Enhancement - 65.1 - New Report 'Detailed Salary Breakdown Report by Cost Center Group'.
                Case enArutiReport.DETAILED_SALARY_BREAKDOWN_REPORT_BY_COST_CENTER_GROUP
                    objfrm = New frmDetailedSalaryBreakdownByCostCenterGroupReport
                    'Sohail (20 Feb 2017) -- End


                    'Pinkal (16-Feb-2017) -- Start
                    'Enhancement - Working on Employee Budget Timesheet Report for Aga Khan.
                Case enArutiReport.Emplyoee_Project_Code_Timesheet_Report
                    objfrm = New frmEmpBudgetTimesheetReport
                    'Pinkal (16-Feb-2017) -- End


                    'S.SANDEEP [28-FEB-2017] -- START
                    'ISSUE/ENHANCEMENT : CCBRT, MONTHLY PAYROLL REPORT
                Case enArutiReport.EmployeeMonthlyPayrollReport
                    objfrm = New frmMonthlyPayrollReport
                    'S.SANDEEP [28-FEB-2017] -- END

                    'S.SANDEEP [22-MAR-2017] -- START
                    'ISSUE/ENHANCEMENT : Tanapa PE Forms Statutory Report
                Case enArutiReport.PE_Form_Report
                    objfrm = New frmPE_FormsReports
                    'S.SANDEEP [22-MAR-2017] -- END

                    'Nilay (11 Apr 2017) -- Start
                    'Enhancements: New statutory report iTax Form C for CCK
                Case enArutiReport.ITax_Form_C_Report
                    objfrm = New frmITaxFormBReport(enArutiReport.ITax_Form_C_Report, User._Object._Languageunkid, Company._Object._Companyunkid)
                    'Nilay (11 Apr 2017) -- End


                    'Pinkal (22-Apr-2017) -- Start
                    'Enhancement - Creating Timesheet Project Summary Report For PSI & AGA Khan .
                Case enArutiReport.Employee_Timesheet_Project_Summary_Report
                    objfrm = New frmEmpTimesheetProjectSummaryReport
                    'Pinkal (22-Apr-2017) -- End


                    'Pinkal (31-Jul-2017) -- Start
                    'Enhancement -Create Timesheet Submission Report for THPS .
                Case enArutiReport.Employee_Timesheet_Submission_Report
                    objfrm = New frmEmpBdgtTimeSheetSubmissionReport
                    'Pinkal (31-Jul-2017) -- End

                    'Sohail (02 Aug 2017) -- Start
                    'Enhancement - 69.1 - New Report 'Employee's cost Budget plan Vs Actual expenditure'.
                Case enArutiReport.Monthly_Employee_Budget_Variance_Analysis_Report
                    objfrm = New frmMonthlyEmployeeBudgetVarianceAnalysis
                    'Sohail (02 Aug 2017) -- End

                    'S.SANDEEP [05-OCT-2017] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 80
                Case enArutiReport.AssessmentDoneReport
                    objfrm = New frmAssessmentDoneReport
                    'S.SANDEEP [05-OCT-2017] -- END

                    'S.SANDEEP [11-OCT-2017] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 78
                Case enArutiReport.StaffRequisitionFormReport
                    objfrm = New frmStaffRequisitionFormReport
                    'S.SANDEEP [11-OCT-2017] -- END

                    'S.SANDEEP [13-OCT-2017] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 81
                Case enArutiReport.Mauritius_PAYE_Report
                    objfrm = New frmMauritius_PAYEReport
                    'S.SANDEEP [13-OCT-2017] -- END

                    'S.SANDEEP [13-OCT-2017] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 82
                Case enArutiReport.Mauritius_NPF_Report
                    objfrm = New frmMauritius_NPF_Report
                    'S.SANDEEP [13-OCT-2017] -- END

                    'Sohail (10 Nov 2017) -- Start
                    'Enhancement - 70.1 - Ref. No. 128 - report to describe the budget plan, which will split the budget to amounts depending to project codes %s. this report is like the one we already have "Monthly employee Budget Variance report". the only difference is it isn't having the actual column and the variance column and it is distributing budget not the actual amount..
                Case enArutiReport.Monthly_Finance_Plan_Report
                    objfrm = New frmMonthlyFinancePlanReport
                    'Sohail (10 Nov 2017) -- End

                    'Sohail (20 Dec 2017) -- Start
                    'Ref. # 138 - Changes in PSPF and NHIF report make available for Tanzania with some changes as per format given in email in 70.1.
                Case enArutiReport.NHIF_Report_TZ
                    objfrm = New frmNHIFReportTZ
                    'Sohail (20 Dec 2017) -- End

                    'S.SANDEEP [29-NOV-2017] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 102
                Case enArutiReport.Assessment_Acknowledgment_Report
                    objfrm = New frmAssessmentAckReport
                    'S.SANDEEP [29-NOV-2017] -- END



                    'Pinkal (23-Feb-2018) -- Start
                    'Enhancement - Working on B5 Customize Medical Reports.
                Case enArutiReport.B5_Advance_Statement_Report
                    objfrm = New frmAdvanceStatementReport

                Case enArutiReport.B5_Payment_Voucher_Report
                    objfrm = New frmPayementVoucherReport

                    'Pinkal (23-Feb-2018) -- End

                    'Sohail (29 Mar 2018) -- Start
                    'MARE ANGUILLES FARMS LTD Mauritius Enhancement : Ref. No. 186 - Combined Mauritius Statutory -PAYE_NPF Report in 71.1.
                Case enArutiReport.PAYE_NPF_NSF_Report
                    objfrm = New frmPAYE_NPF_NSF_Report
                    'Sohail (29 Mar 2018) -- End

                    'Sohail (18 Apr 2018) -- Start
                    'Malawi Enhancement - Ref # 211 : New statutory report for Malawi - Form P.9 shows annual gross incomes and taxable income for employee in 72.1.
                Case enArutiReport.PAYE_DEDUCTION_FORM_P9_Report
                    objfrm = New frmPayeDeductionsFormP9Report
                    'Sohail (18 Apr 2018) -- End

                    'Gajanan (05 May 2018) -- Start
                    'Malawi Enhancement - Ref # 212 : New statutory report for Malawi - TEVET Levy Report shows period wise gross emoluments for all staff and the corresponding levy applied on them for employee in 72.1.
                Case enArutiReport.TEVET_LEVY_REPORT
                    objfrm = New frmTevetLevyReport
                    'Gajanan (05 May 2018) -- End


                    'Pinkal (12-May-2018) -- Start
                    'Enhancement - Ref # 213 Certificate of Employee Leaving to show gross emoluments and tax paid by employee for the period of employment during the year.
                Case enArutiReport.Employee_Leaving_Certificate
                    objfrm = New frmEmpLeavingCertificate
                    'Pinkal (12-May-2018) -- End

                    'Pinkal (12-May-2018) -- Start
                    'Enhancement - Ref # 205 Claim Request Form For Abood Group.
                Case enArutiReport.Employee_Claim_Form
                    objfrm = New frmEmployeeClaimForm
                    'Pinkal (12-May-2018) -- End


                    'Gajanan [13-AUG-2018] -- Start
                    'Enhancement - Implementing Grievance Module.
                Case enArutiReport.Grievance_Detail_Report
                    objfrm = New frmGrievanceDetailReport
                    'Gajanan(13-AUG-2018) -- End

                    'Hemant (21 Nov 2018) -- Start
                    'Enhancement : New Statutory Report for (Public Service Social Security Fund) PSSSF for Tanazania in 75.1.
                Case enArutiReport.PSSSF_Report
                    objfrm = New frmPSSSFReport(enArutiReport.PSSSF_Report, User._Object._Languageunkid, Company._Object._Companyunkid)
                    'Hemant (21 Nov 2018) -- End

                    'S.SANDEEP |12-APR-2019| -- START
                    'ENHANCEMENT : ASSESSMENT AUDIT REPORT
                Case enArutiReport.Assessment_Audit_Report
                    objfrm = New frmAssessmentAuditReport
                    'S.SANDEEP |12-APR-2019| -- END


                    'Pinkal (31-May-2019) -- Start
                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                Case enArutiReport.Employee_Expense_Report
                    objfrm = New frmEmployeeExpenseReport
                    'Pinkal (31-May-2019) -- End


                    'Pinkal (13-Jun-2019) -- Start
                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                Case enArutiReport.Leave_Application_Report
                    objfrm = New frmLeaveApplication_Report
                    'Pinkal (13-Jun-2019) -- End

'S.SANDEEP |25-OCT-2019| -- START
'ISSUE/ENHANCEMENT : Calibration Issues
Case enArutiReport.Assessment_Calibration_Report
                    objfrm = New frmAssessmentCalibrationReport
'S.SANDEEP |25-OCT-2019| -- END


                    'Gajanan [24-OCT-2019] -- Start   
                    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                Case enArutiReport.Grievance_Summary_Reports
                    objfrm = New frmGrievanceStatusAndNatureReport
                    'Gajanan [24-OCT-2019] -- End

                    'Pinkal (15-Jan-2020) -- Start
                    'Enhancements -  Working on OT Requisistion Reports for NMB.

                Case enArutiReport.OTRequisition_Report
                    objfrm = New frmOTRequisition_Report

                    'Pinkal (15-Jan-2020) -- End

                    'Pinkal (29-Jan-2020) -- Start
                    'Enhancement - Changes related To OT NMB Testing.

                Case enArutiReport.OTRequisition_Summary_Report
                    objfrm = New frmOTRequisition_SummaryReport

                Case enArutiReport.Employee_OTRequisition_Report
                    objfrm = New frmEmpOTRequisition_Report

                    'Pinkal (29-Jan-2020) -- End

                    'Pinkal (07-Mar-2020) -- Start
                    'Enhancement - Changes Related to Payroll UAT for NMB.
                Case enArutiReport.Justification_Report
                    objfrm = New frmJustification_Report
                    'Pinkal (07-Mar-2020) -- End

                    'S.SANDEEP |25-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : NEW DISCIPLINE REPORT 
                Case enArutiReport.DisciplineCaseDetailReport
                    objfrm = New frmDisciplineCaseDetailsReport
                Case enArutiReport.DisciplineOutcomeReport
                    objfrm = New frmDisciplineOutcomeReport
                Case enArutiReport.DisciplineAppealReport
                    objfrm = New frmDisciplineAppealReport
                    'S.SANDEEP |25-MAR-2020| -- END

                    'Gajanan [18-May-2020] -- Start
                    'Enhancement:Discipline Module Enhancement NMB
                Case enArutiReport.DisciplineSuspensionReport
                    objfrm = New frmDisciplinarySuspensionReport
                    'Gajanan [18-May-2020] -- End

 'Pinkal (16-May-2020) -- Start
                    'Enhancement NMB Leave Report Changes -   Working on Leave Reports required by NMB.
                Case enArutiReport.Leave_Absence_DetailedReport
                    objfrm = New frmLeaveAbsence_Detailed_Report

                Case enArutiReport.Leave_Absence_SummaryReport
                    objfrm = New frmLeaveAbsence_Summary_Report
                    'Pinkal (16-May-2020) -- End

                    'Hemant (03 Jun 2020) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 3 : A Interview Score report showing interview candidate(s) and the scores of each interviewer to be developed)
                Case enArutiReport.Interview_Score_Report
                    objfrm = New frmInterviewScoreReport
                    'Hemant (03 Jun 2020) -- End

                    'Gajanan [27-June-2020] -- Start
                    'Enhancement:Create AT-Non Disclosure Declaration Report
                Case enArutiReport.ATNonDisclosureDeclaration_Report
                    objfrm = New frmATNonDisclouserDeclarationReport
                    'Gajanan [27-June-2020] -- End

 'Pinkal (27-Jun-2020) -- Start
                    'Enhancement NMB -   Working on Employee Signature Report.
                Case enArutiReport.Employee_Signature_Report
                    objfrm = New frmEmpSignature_Report
                    'Pinkal (27-Jun-2020) -- End

                    'Pinkal (26-Jun-2020) -- Start
                    'Ifakara Enhancement [3011] - Creating Employee Project Description Report in budget timesheet.
                Case enArutiReport.Employee_Project_Description_Report
                    objfrm = New frmEmpProjectDescriptionReport
                    'Pinkal (26-Jun-2020) -- End

                    'Pinkal (16-Jul-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
                Case enArutiReport.Employee_Calibrator_Approver_Report
                    objfrm = New frmEmp_Calibrator_Approver_Report
                    'Pinkal (16-Jul-2020) -- End


                    'Hemant (28 Aug 2020) -- Start
                    'Enhancement : New Statutory Report for Skills and Development Levy Monthly Return & Statement and Payment of Tax Withheld for Employees for Tanazania
                Case enArutiReport.Skills_And_Development_Levy_Monthly_Return
                    objfrm = New frmSkillsAndDevelopmentLevyMonthlyReturn(enArutiReport.Skills_And_Development_Levy_Monthly_Return)

                Case enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees
                    objfrm = New frmStatementAndPaymentTaxReport(enArutiReport.Statement_And_Payment_Of_Tax_Withheld_For_Employees)
                    'Hemant (28 Aug 2020) -- End


                    'Pinkal (20-Nov-2020) -- Start
                    'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.
                Case enArutiReport.FuelConsumption_Report
                    objfrm = New frmFuelConsumptionReport
                    'Pinkal (20-Nov-2020) -- End

                    'S.SANDEEP |23-JAN-2021| -- START
                    'ISSUE/ENHANCEMENT : DISCIPLINARY CHANGES
                Case enArutiReport.Disciplinary_Penalty_Report
                    objfrm = New frmDisciplinaryPenaltyReport
                    'S.SANDEEP |23-JAN-2021| -- END

                    'Sohail (23 Jan 2021) -- Start
                    'Netis Gabon Enhancement : - New statutory report CFP Report.
                Case enArutiReport.CFP_Report
                    objfrm = New frmCFPReport
                    'Sohail (23 Jan 2021) -- End

                    'Sohail (01 Feb 2021) -- Start
                    'Netis Gabon Enhancement : OLD-269 : New statutory report IRPP FNH CFP Report.
                Case enArutiReport.IRPP_FNH_CFP_Report
                    objfrm = New frmIRPPFNHCFPReport
                    'Sohail (01 Feb 2021) -- End


                    'Pinkal (10-Feb-2021) -- Start
                    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                Case enArutiReport.Imprest_Balance_Report
                    objfrm = New frmImprestBalanceReport
                    'Pinkal (10-Feb-2021) -- End

                    'Hemant (15 Mar 2021) -- Start
                    'ISSUE/ENHANCEMENT : Talent & Succession Report 
                Case enArutiReport.Talent_Screening_Status_Report
                    objfrm = New frmTalentScreenerStatusReport

                Case enArutiReport.Succession_Screening_Status_Report
                    objfrm = New frmSuccessionScreenerStatusReport
                    'Hemant (15 Mar 2021) -- End

                    'Hemant (22 Mar 2021) -- Start
                    'ISSUE/ENHANCEMENT : Talent & Succession Reports
                Case enArutiReport.Talent_Active_Approved_with_Screener_Report
                    objfrm = New frmTalentActiveApprovedScreenerReport

                Case enArutiReport.Talent_Ratio_Of_Active_Staff_Report
                    objfrm = New frmTalentRatioOfActiveStaffReport

                Case enArutiReport.Talent_Approved_Rejected_Report
                    objfrm = New frmTalentApprovedRejectedReport

                Case enArutiReport.Talent_Exits_Report
                    objfrm = New frmTalentExitsReport

                Case enArutiReport.Succession_Cross_Functional_Nominations
                    objfrm = New frmCrossFunctionalNominations

                Case enArutiReport.Succession_Ratios_Report
                    objfrm = New frmSuccessionRatioReport

                Case enArutiReport.Succession_List_Report
                    objfrm = New frmSuccessionListReport

                Case enArutiReport.Succession_Exits_Report
                    objfrm = New frmSuccessionExitsReport
                    'Hemant (22 Mar 2021) -- End

 'Pinkal (30-Mar-2021)-- Start
                    'NMB Enhancement  -  Working on Employee Recategorization history Report.
                Case enArutiReport.Emp_RecategorizationHistory_Report
                    objfrm = New frmEmp_Recategorize_History_Report

                Case enArutiReport.Emp_TransferHistory_Report
                    objfrm = New frmEmpTransfer_History_Report
                    'Pinkal (30-Mar-2021) -- End

                    'Gajanan [18-May-2021] -- Start
                Case enArutiReport.PDP_Status_Report
                    objfrm = New frmPDPStatusReport
                Case enArutiReport.PDP_Development_Goals_Report
                    objfrm = New frmPDPDevelopmentGoalsReport
                Case enArutiReport.PDP_Preferred_Move_Report
                    objfrm = New frmPDPPreferredMoveReport
                Case enArutiReport.PDP_Development_Areas_Report
                    objfrm = New frmPDPDevelopmentAreasReport
                Case enArutiReport.PDP_Action_Plan_Progress_Status_Report
                    objfrm = New frmPDPActionPlanProgressStatus
                    'Gajanan [18-May-2021] -- End

 'Sohail (20 May 2021) -- Start
                    'NMB Enhancement : : New Report "Training Priority Report" in training module.".
                Case enArutiReport.Training_Priority_Report
                    objfrm = New frmTrainingPriorityReport
                    'Sohail (20 May 2021) -- End

                    'Pinkal (21-May-2021) -- Start
                    'Enhancement On Training Reports -   Working on  NMB Training Reports.
                Case enArutiReport.Planned_Training_Report
                    objfrm = New frmPlannedTrainingReport
                    'Pinkal (21-May-2021) -- End

                    'Hemant (18 May 2021) -- Start
                    'ISSUE/ENHANCEMENT : NMB Training Reports
                Case enArutiReport.Executed_Trainings_Report
                    objfrm = New frmExecutedTrainingsReport

                Case enArutiReport.Executed_Trainings_Detailed_Report
                    objfrm = New frmExecutedTrainingsDetailedReport

                Case enArutiReport.Training_Calendar_Report
                    objfrm = New frmTrainingCalendarReport

                Case enArutiReport.Trainings_Covered_Proportion_Report
                    objfrm = New frmTrainingsCoveredProportionReport

                Case enArutiReport.Staff_Trainings_Attended_Report
                    objfrm = New frmStaffTrainingsAttendedReport
                    'Hemant (18 May 2021) -- End

                    'Sohail (30 Jun 2021) -- Start
                    'Zambia Enhancement : OLD - 415 : New statutory Report: National Health Insurance (NHI) Report.
                Case enArutiReport.National_Health_Insurance_Report
                    objfrm = New frmNationalHealthInsurance
                    'Sohail (30 Jun 2021) -- End

'S.SANDEEP |01-JUL-2021| -- START
                Case enArutiReport.Assessment_Progress_Update
                    objfrm = New frmProgressUpdateReport
                    'S.SANDEEP |01-JUL-2021| -- END

                    'Pinkal (25-Aug-2021)-- Start
                    'KBC Enhancement : Creating Approved Claim Summary Report in Desktop and MSS.
                Case enArutiReport.Approved_CR_Summary_Report
                    objfrm = New frmApprovedCRSummaryReport
                    'Pinkal (25-Aug-2021) -- End

                    'Hemant (04 Sep 2021) -- Start
                    'ENHANCEMENT : OLD-445 - New Report - Training Feedback Status Report.
                Case enArutiReport.Training_Feedback_Status_Report
                    objfrm = New frmTrainingFeedbackStatusReport
                    'Hemant (04 Sep 2021) -- End

                    'Hemant (13 Sep 2021) -- Start
                    'ENHANCEMENT : OLD-446 - New Report - Training Feedback Form.
                Case enArutiReport.Training_Feedback_Form_Report
                    objfrm = New frmTrainingFeedbackFormReport
                    'Hemant (13 Sep 2021) -- End

                    'Hemant (21 Sep 2021) -- Start
                    'ENHANCEMENT : OLD-467 - Training Status Summary Report.
                Case enArutiReport.Training_Status_Summary_Report
                    objfrm = New frmTrainingStatusSummaryReport
                    'Hemant (21 Sep 2021) -- End

'Gajanan [16-Sep-2021] -- Start
                    'ENHANCEMENT: OLD-476
                Case enArutiReport.Disciplinary_Charges_Detailed_Report
                    objfrm = New frmDisciplinaryChargesDetailedReport
                    'Gajanan [16-Sep-2021] -- End

                    'Pinkal (04-Oct-2021)-- Start
                    'Leave Planner Repor Enhancement : Creating Planned Leave Report. 
                Case enArutiReport.Planned_Leave_Summary_Report
                    objfrm = New frmPlannedLeave_SummaryReport
                    'Pinkal (04-Oct-2021) -- End

                    'Sohail (27 Oct 2021) -- Start
                    'Gabon Enhancement : OLD-474 : New statutory Rapport Trimestriel CNAMGS report for Gabon.
                Case enArutiReport.Rapport_Trimestriel_CNAMGS
                    objfrm = New frmRapportTrimestrielCNAMGS
                    'Sohail (27 Oct 2021) -- End

                    'Hemant (22 Nov 2021) -- Start
                    'ENHANCEMENT : OLD-490(Finca Uganda) - New Report: Training Budget Report.
                Case enArutiReport.Training_Budget_Spreadsheet
                    objfrm = New frmTrainingBudgetReportNew
                    'Hemant (22 Nov 2021) -- End

                    'Sohail (29 Nov 2021) -- Start
                    'Enhancement : OLD-478 : Kenya - New statutory report - NITA for Kenya only.
                Case enArutiReport.NITA_Report
                    objfrm = New frmNITAReport
                    'Sohail (29 Nov 2021) -- End

                    'Pinkal (29-Nov-2021)-- Start
                    'Netis Gabon Statutory Report.
                Case enArutiReport.Gabon_Declaration_Trimestrielle_Salaires
                    objfrm = New frmQuarterlyWages
                    'Pinkal (29-Nov-2021) -- End

                    'Sohail (03 Dec 2021) -- Start
                    'Enhancement : OLD-522 : Netis Gabon - New Statutory Report ID20 ETAT DE LA MASSE SALARIALE for Gabon.
                Case enArutiReport.ID20_ETAT_DE_LA_MASSE_SALARIALE_Report
                    objfrm = New frmID20ETATReport
                    'Sohail (03 Dec 2021) -- End

                    'Sohail (09 Dec 2021) -- Start
                    'Enhancement : OLD-524 : Netis Gabon - New Statutory Report ID21 DECLARATION ET TRAITEMENT DES SALAIRES for Gabon.
                Case enArutiReport.ID21_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report
                    objfrm = New frmID21DECLARATIONReport
                    'Sohail (09 Dec 2021) -- End

                    'Pinkal (10-Dec-2021)-- Start
                    'Netis Gabon Statutory Report.
                Case enArutiReport.ID22_DECLARATION_ET_TRAITEMENT_DES_SALAIRES_Report
                    objfrm = New frmID22DECLARATIONReport
                    'Pinkal (10-Dec-2021) -- End

            End Select

            If pnlReportSummary.Controls.Count > 0 Then pnlReportSummary.Controls.RemoveAt(0)
            objfrm.TopLevel = False
            objfrm.Size = pnlReportSummary.Size
            pnlReportSummary.Controls.Add(objfrm)
            objfrm.Show()
            objfrm.BringToFront()


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetCustomDateFormat(CType(objfrm, Form))
            'Pinkal (16-Apr-2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tvReport_AfterSelect", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub cmsFavoriteReport_Opening(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmsFavoriteReport.Opening
        Try
            If tvReports.SelectedNode.Nodes.Count <> 0 Then e.Cancel = True

            tsmAddToFavorite.Visible = True
            tsmRemoveFromFavorite.Visible = True

            If tvReports.SelectedNode.Name.Trim = "" OrElse CInt(tvReports.SelectedNode.Name) > 1000 Then
                tsmAddToFavorite.Visible = False
            Else
                tsmRemoveFromFavorite.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cmsFavoriteReport_Opening", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmAddToFavorite_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsmAddToFavorite.Click
        Try
            Dim objReport As New clsArutiReportClass
            Call objReport.AddToFavorite(User._Object._Userunkid, tvReports.SelectedNode.Name, Company._Object._Companyunkid)
            Call FillReports()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmAddToFavorite_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmRemoveFromFavorite_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsmRemoveFromFavorite.Click
        Try
            Dim objReport As New clsArutiReportClass
            Call objReport.RemoveFromFavorite(User._Object._Userunkid, Val(tvReports.SelectedNode.Name) - 1001, Company._Object._Companyunkid)
            Call FillReports()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmRemoveFromFavorite_Click", mstrModuleName)
        End Try
    End Sub

#End Region

End Class