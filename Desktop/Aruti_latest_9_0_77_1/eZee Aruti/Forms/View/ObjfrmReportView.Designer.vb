﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ObjfrmReportView
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ObjfrmReportView))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.tblReportLayout = New System.Windows.Forms.TableLayoutPanel
        Me.objspc1 = New System.Windows.Forms.SplitContainer
        Me.gbSearch = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtKeyWord = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lblSearch = New System.Windows.Forms.Label
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.tvReports = New System.Windows.Forms.TreeView
        Me.pnlReportSummary = New System.Windows.Forms.Panel
        Me.cmsFavoriteReport = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmAddToFavorite = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmRemoveFromFavorite = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlMainInfo.SuspendLayout()
        Me.tblReportLayout.SuspendLayout()
        Me.objspc1.Panel1.SuspendLayout()
        Me.objspc1.Panel2.SuspendLayout()
        Me.objspc1.SuspendLayout()
        Me.gbSearch.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsFavoriteReport.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.tblReportLayout)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(750, 504)
        Me.pnlMainInfo.TabIndex = 0
        '
        'tblReportLayout
        '
        Me.tblReportLayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.tblReportLayout.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset
        Me.tblReportLayout.ColumnCount = 2
        Me.tblReportLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 226.0!))
        Me.tblReportLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblReportLayout.Controls.Add(Me.objspc1, 0, 0)
        Me.tblReportLayout.Controls.Add(Me.pnlReportSummary, 1, 0)
        Me.tblReportLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblReportLayout.Location = New System.Drawing.Point(0, 0)
        Me.tblReportLayout.Name = "tblReportLayout"
        Me.tblReportLayout.RowCount = 1
        Me.tblReportLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblReportLayout.Size = New System.Drawing.Size(750, 504)
        Me.tblReportLayout.TabIndex = 2
        '
        'objspc1
        '
        Me.objspc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objspc1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objspc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objspc1.IsSplitterFixed = True
        Me.objspc1.Location = New System.Drawing.Point(5, 5)
        Me.objspc1.Name = "objspc1"
        Me.objspc1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objspc1.Panel1
        '
        Me.objspc1.Panel1.Controls.Add(Me.gbSearch)
        '
        'objspc1.Panel2
        '
        Me.objspc1.Panel2.Controls.Add(Me.tvReports)
        Me.objspc1.Size = New System.Drawing.Size(220, 494)
        Me.objspc1.SplitterDistance = 63
        Me.objspc1.TabIndex = 18
        '
        'gbSearch
        '
        Me.gbSearch.BorderColor = System.Drawing.Color.Black
        Me.gbSearch.Checked = False
        Me.gbSearch.CollapseAllExceptThis = False
        Me.gbSearch.CollapsedHoverImage = Nothing
        Me.gbSearch.CollapsedNormalImage = Nothing
        Me.gbSearch.CollapsedPressedImage = Nothing
        Me.gbSearch.CollapseOnLoad = False
        Me.gbSearch.Controls.Add(Me.txtKeyWord)
        Me.gbSearch.Controls.Add(Me.objbtnReset)
        Me.gbSearch.Controls.Add(Me.lblSearch)
        Me.gbSearch.Controls.Add(Me.objbtnSearch)
        Me.gbSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbSearch.ExpandedHoverImage = Nothing
        Me.gbSearch.ExpandedNormalImage = Nothing
        Me.gbSearch.ExpandedPressedImage = Nothing
        Me.gbSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSearch.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSearch.HeaderHeight = 25
        Me.gbSearch.HeightOnCollapse = 0
        Me.gbSearch.LeftTextSpace = 0
        Me.gbSearch.Location = New System.Drawing.Point(0, 0)
        Me.gbSearch.Name = "gbSearch"
        Me.gbSearch.OpenHeight = 300
        Me.gbSearch.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSearch.ShowBorder = True
        Me.gbSearch.ShowCheckBox = False
        Me.gbSearch.ShowCollapseButton = False
        Me.gbSearch.ShowDefaultBorderColor = True
        Me.gbSearch.ShowDownButton = False
        Me.gbSearch.ShowHeader = True
        Me.gbSearch.Size = New System.Drawing.Size(218, 61)
        Me.gbSearch.TabIndex = 0
        Me.gbSearch.Temp = 0
        Me.gbSearch.Text = "Find"
        Me.gbSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtKeyWord
        '
        Me.txtKeyWord.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtKeyWord.Flags = 0
        Me.txtKeyWord.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtKeyWord.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtKeyWord.Location = New System.Drawing.Point(64, 33)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(142, 21)
        Me.txtKeyWord.TabIndex = 2
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(191, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 2
        Me.objbtnReset.TabStop = False
        '
        'lblSearch
        '
        Me.lblSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSearch.Location = New System.Drawing.Point(8, 36)
        Me.lblSearch.Name = "lblSearch"
        Me.lblSearch.Size = New System.Drawing.Size(50, 15)
        Me.lblSearch.TabIndex = 1
        Me.lblSearch.Text = "Find"
        Me.lblSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(166, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 1
        Me.objbtnSearch.TabStop = False
        '
        'tvReports
        '
        Me.tvReports.ContextMenuStrip = Me.cmsFavoriteReport
        Me.tvReports.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvReports.Location = New System.Drawing.Point(0, 0)
        Me.tvReports.Name = "tvReports"
        Me.tvReports.Size = New System.Drawing.Size(218, 425)
        Me.tvReports.TabIndex = 0
        '
        'pnlReportSummary
        '
        Me.pnlReportSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlReportSummary.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlReportSummary.Location = New System.Drawing.Point(233, 5)
        Me.pnlReportSummary.Name = "pnlReportSummary"
        Me.pnlReportSummary.Size = New System.Drawing.Size(512, 494)
        Me.pnlReportSummary.TabIndex = 1
        '
        'cmsFavoriteReport
        '
        Me.cmsFavoriteReport.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmAddToFavorite, Me.tsmRemoveFromFavorite})
        Me.cmsFavoriteReport.Name = "cmsFavorite"
        Me.cmsFavoriteReport.Size = New System.Drawing.Size(184, 48)
        '
        'tsmAddToFavorite
        '
        Me.tsmAddToFavorite.Name = "tsmAddToFavorite"
        Me.tsmAddToFavorite.Size = New System.Drawing.Size(183, 22)
        Me.tsmAddToFavorite.Text = "Add To Favorite"
        '
        'tsmRemoveFromFavorite
        '
        Me.tsmRemoveFromFavorite.Name = "tsmRemoveFromFavorite"
        Me.tsmRemoveFromFavorite.Size = New System.Drawing.Size(183, 22)
        Me.tsmRemoveFromFavorite.Text = "Remove From Favorite"
        '
        'ObjfrmReportView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(750, 504)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ObjfrmReportView"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.pnlMainInfo.ResumeLayout(False)
        Me.tblReportLayout.ResumeLayout(False)
        Me.objspc1.Panel1.ResumeLayout(False)
        Me.objspc1.Panel2.ResumeLayout(False)
        Me.objspc1.ResumeLayout(False)
        Me.gbSearch.ResumeLayout(False)
        Me.gbSearch.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsFavoriteReport.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents tblReportLayout As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents objspc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents pnlReportSummary As System.Windows.Forms.Panel
    Friend WithEvents tvReports As System.Windows.Forms.TreeView
    Friend WithEvents gbSearch As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblSearch As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cmsFavoriteReport As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmAddToFavorite As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmRemoveFromFavorite As System.Windows.Forms.ToolStripMenuItem
End Class
