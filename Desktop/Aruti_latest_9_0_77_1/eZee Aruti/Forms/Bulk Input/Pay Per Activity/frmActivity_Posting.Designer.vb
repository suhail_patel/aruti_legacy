﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActivity_Posting
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmActivity_Posting))
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbActivityPosting = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.LblToDate = New System.Windows.Forms.Label
        Me.cboActivitiy = New System.Windows.Forms.ComboBox
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.LblFromDate = New System.Windows.Forms.Label
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnSearchAcitivity = New eZee.Common.eZeeGradientButton
        Me.LblActivitiy = New System.Windows.Forms.Label
        Me.objpnlData = New System.Windows.Forms.Panel
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhActivity = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhUoM = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhActValue = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboViewBy = New System.Windows.Forms.ComboBox
        Me.objbtnSearchMeasures = New eZee.Common.eZeeGradientButton
        Me.lblViewBy = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnVoidUnpostedActivity = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnPostActivity = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnVoidPosting = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.gbActivityPosting.SuspendLayout()
        Me.objpnlData.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbActivityPosting)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(699, 452)
        Me.pnlMain.TabIndex = 0
        '
        'gbActivityPosting
        '
        Me.gbActivityPosting.BorderColor = System.Drawing.Color.Black
        Me.gbActivityPosting.Checked = False
        Me.gbActivityPosting.CollapseAllExceptThis = False
        Me.gbActivityPosting.CollapsedHoverImage = Nothing
        Me.gbActivityPosting.CollapsedNormalImage = Nothing
        Me.gbActivityPosting.CollapsedPressedImage = Nothing
        Me.gbActivityPosting.CollapseOnLoad = False
        Me.gbActivityPosting.Controls.Add(Me.dtpToDate)
        Me.gbActivityPosting.Controls.Add(Me.LblToDate)
        Me.gbActivityPosting.Controls.Add(Me.cboActivitiy)
        Me.gbActivityPosting.Controls.Add(Me.dtpFromDate)
        Me.gbActivityPosting.Controls.Add(Me.LblFromDate)
        Me.gbActivityPosting.Controls.Add(Me.txtSearch)
        Me.gbActivityPosting.Controls.Add(Me.objbtnSearchAcitivity)
        Me.gbActivityPosting.Controls.Add(Me.LblActivitiy)
        Me.gbActivityPosting.Controls.Add(Me.objpnlData)
        Me.gbActivityPosting.Controls.Add(Me.cboPeriod)
        Me.gbActivityPosting.Controls.Add(Me.cboViewBy)
        Me.gbActivityPosting.Controls.Add(Me.objbtnSearchMeasures)
        Me.gbActivityPosting.Controls.Add(Me.lblViewBy)
        Me.gbActivityPosting.Controls.Add(Me.lblPeriod)
        Me.gbActivityPosting.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbActivityPosting.ExpandedHoverImage = Nothing
        Me.gbActivityPosting.ExpandedNormalImage = Nothing
        Me.gbActivityPosting.ExpandedPressedImage = Nothing
        Me.gbActivityPosting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbActivityPosting.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbActivityPosting.HeaderHeight = 25
        Me.gbActivityPosting.HeaderMessage = ""
        Me.gbActivityPosting.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbActivityPosting.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbActivityPosting.HeightOnCollapse = 0
        Me.gbActivityPosting.LeftTextSpace = 0
        Me.gbActivityPosting.Location = New System.Drawing.Point(0, 0)
        Me.gbActivityPosting.Name = "gbActivityPosting"
        Me.gbActivityPosting.OpenHeight = 300
        Me.gbActivityPosting.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbActivityPosting.ShowBorder = True
        Me.gbActivityPosting.ShowCheckBox = False
        Me.gbActivityPosting.ShowCollapseButton = False
        Me.gbActivityPosting.ShowDefaultBorderColor = True
        Me.gbActivityPosting.ShowDownButton = False
        Me.gbActivityPosting.ShowHeader = True
        Me.gbActivityPosting.Size = New System.Drawing.Size(699, 452)
        Me.gbActivityPosting.TabIndex = 0
        Me.gbActivityPosting.Temp = 0
        Me.gbActivityPosting.Text = "Activity Posting to Payroll"
        Me.gbActivityPosting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpToDate
        '
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(592, 33)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(102, 21)
        Me.dtpToDate.TabIndex = 115
        '
        'LblToDate
        '
        Me.LblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToDate.Location = New System.Drawing.Point(513, 36)
        Me.LblToDate.Name = "LblToDate"
        Me.LblToDate.Size = New System.Drawing.Size(71, 15)
        Me.LblToDate.TabIndex = 114
        Me.LblToDate.Text = "To Date"
        '
        'cboActivitiy
        '
        Me.cboActivitiy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboActivitiy.DropDownWidth = 350
        Me.cboActivitiy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboActivitiy.FormattingEnabled = True
        Me.cboActivitiy.Location = New System.Drawing.Point(71, 61)
        Me.cboActivitiy.Name = "cboActivitiy"
        Me.cboActivitiy.Size = New System.Drawing.Size(176, 21)
        Me.cboActivitiy.TabIndex = 110
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(391, 33)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(102, 21)
        Me.dtpFromDate.TabIndex = 113
        '
        'LblFromDate
        '
        Me.LblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFromDate.Location = New System.Drawing.Point(314, 36)
        Me.LblFromDate.Name = "LblFromDate"
        Me.LblFromDate.Size = New System.Drawing.Size(71, 15)
        Me.LblFromDate.TabIndex = 112
        Me.LblFromDate.Text = "From Date"
        '
        'txtSearch
        '
        Me.txtSearch.Flags = 0
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch.Location = New System.Drawing.Point(2, 95)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(693, 21)
        Me.txtSearch.TabIndex = 107
        '
        'objbtnSearchAcitivity
        '
        Me.objbtnSearchAcitivity.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchAcitivity.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchAcitivity.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchAcitivity.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchAcitivity.BorderSelected = False
        Me.objbtnSearchAcitivity.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchAcitivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchAcitivity.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchAcitivity.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchAcitivity.Location = New System.Drawing.Point(253, 61)
        Me.objbtnSearchAcitivity.Name = "objbtnSearchAcitivity"
        Me.objbtnSearchAcitivity.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchAcitivity.TabIndex = 111
        '
        'LblActivitiy
        '
        Me.LblActivitiy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblActivitiy.Location = New System.Drawing.Point(7, 64)
        Me.LblActivitiy.Name = "LblActivitiy"
        Me.LblActivitiy.Size = New System.Drawing.Size(58, 15)
        Me.LblActivitiy.TabIndex = 109
        Me.LblActivitiy.Text = "Activity"
        '
        'objpnlData
        '
        Me.objpnlData.Controls.Add(Me.chkSelectAll)
        Me.objpnlData.Controls.Add(Me.dgvData)
        Me.objpnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlData.Location = New System.Drawing.Point(2, 121)
        Me.objpnlData.Name = "objpnlData"
        Me.objpnlData.Size = New System.Drawing.Size(693, 267)
        Me.objpnlData.TabIndex = 13
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Location = New System.Drawing.Point(10, 5)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAll.TabIndex = 75
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.White
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.ColumnHeadersHeight = 21
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhSelect, Me.dgcolhDate, Me.dgcolhEmployee, Me.dgcolhActivity, Me.dgcolhUoM, Me.dgcolhActValue, Me.dgcolhRate, Me.dgcolhAmount})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(693, 267)
        Me.dgvData.TabIndex = 13
        '
        'objdgcolhSelect
        '
        Me.objdgcolhSelect.Frozen = True
        Me.objdgcolhSelect.HeaderText = ""
        Me.objdgcolhSelect.Name = "objdgcolhSelect"
        Me.objdgcolhSelect.Width = 30
        '
        'dgcolhDate
        '
        Me.dgcolhDate.Frozen = True
        Me.dgcolhDate.HeaderText = "Date"
        Me.dgcolhDate.Name = "dgcolhDate"
        Me.dgcolhDate.ReadOnly = True
        Me.dgcolhDate.Width = 80
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        '
        'dgcolhActivity
        '
        Me.dgcolhActivity.HeaderText = "Activity"
        Me.dgcolhActivity.Name = "dgcolhActivity"
        Me.dgcolhActivity.ReadOnly = True
        '
        'dgcolhUoM
        '
        Me.dgcolhUoM.HeaderText = "UoM"
        Me.dgcolhUoM.Name = "dgcolhUoM"
        Me.dgcolhUoM.ReadOnly = True
        Me.dgcolhUoM.Width = 50
        '
        'dgcolhActValue
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhActValue.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgcolhActValue.HeaderText = "Value"
        Me.dgcolhActValue.Name = "dgcolhActValue"
        Me.dgcolhActValue.ReadOnly = True
        '
        'dgcolhRate
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhRate.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgcolhRate.HeaderText = "Rate"
        Me.dgcolhRate.Name = "dgcolhRate"
        Me.dgcolhRate.ReadOnly = True
        '
        'dgcolhAmount
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhAmount.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgcolhAmount.HeaderText = "Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 350
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(71, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(176, 21)
        Me.cboPeriod.TabIndex = 3
        '
        'cboViewBy
        '
        Me.cboViewBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboViewBy.DropDownWidth = 350
        Me.cboViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboViewBy.FormattingEnabled = True
        Me.cboViewBy.Location = New System.Drawing.Point(391, 61)
        Me.cboViewBy.Name = "cboViewBy"
        Me.cboViewBy.Size = New System.Drawing.Size(303, 21)
        Me.cboViewBy.TabIndex = 15
        '
        'objbtnSearchMeasures
        '
        Me.objbtnSearchMeasures.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchMeasures.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchMeasures.BorderSelected = False
        Me.objbtnSearchMeasures.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchMeasures.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.objbtnSearchMeasures.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchMeasures.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchMeasures.Location = New System.Drawing.Point(253, 33)
        Me.objbtnSearchMeasures.Name = "objbtnSearchMeasures"
        Me.objbtnSearchMeasures.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchMeasures.TabIndex = 8
        '
        'lblViewBy
        '
        Me.lblViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblViewBy.Location = New System.Drawing.Point(314, 64)
        Me.lblViewBy.Name = "lblViewBy"
        Me.lblViewBy.Size = New System.Drawing.Size(71, 15)
        Me.lblViewBy.TabIndex = 14
        Me.lblViewBy.Text = "View Type"
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(7, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(58, 15)
        Me.lblPeriod.TabIndex = 2
        Me.lblPeriod.Text = "Period"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnVoidUnpostedActivity)
        Me.objFooter.Controls.Add(Me.btnPostActivity)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnVoidPosting)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 397)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(699, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnVoidUnpostedActivity
        '
        Me.btnVoidUnpostedActivity.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVoidUnpostedActivity.BackColor = System.Drawing.Color.White
        Me.btnVoidUnpostedActivity.BackgroundImage = CType(resources.GetObject("btnVoidUnpostedActivity.BackgroundImage"), System.Drawing.Image)
        Me.btnVoidUnpostedActivity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVoidUnpostedActivity.BorderColor = System.Drawing.Color.Empty
        Me.btnVoidUnpostedActivity.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnVoidUnpostedActivity.FlatAppearance.BorderSize = 0
        Me.btnVoidUnpostedActivity.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVoidUnpostedActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoidUnpostedActivity.ForeColor = System.Drawing.Color.Black
        Me.btnVoidUnpostedActivity.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVoidUnpostedActivity.GradientForeColor = System.Drawing.Color.Black
        Me.btnVoidUnpostedActivity.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoidUnpostedActivity.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnVoidUnpostedActivity.Location = New System.Drawing.Point(12, 13)
        Me.btnVoidUnpostedActivity.Name = "btnVoidUnpostedActivity"
        Me.btnVoidUnpostedActivity.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoidUnpostedActivity.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnVoidUnpostedActivity.Size = New System.Drawing.Size(149, 30)
        Me.btnVoidUnpostedActivity.TabIndex = 4
        Me.btnVoidUnpostedActivity.Text = "Void Unposted Activity"
        Me.btnVoidUnpostedActivity.UseVisualStyleBackColor = True
        '
        'btnPostActivity
        '
        Me.btnPostActivity.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPostActivity.BackColor = System.Drawing.Color.White
        Me.btnPostActivity.BackgroundImage = CType(resources.GetObject("btnPostActivity.BackgroundImage"), System.Drawing.Image)
        Me.btnPostActivity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPostActivity.BorderColor = System.Drawing.Color.Empty
        Me.btnPostActivity.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPostActivity.FlatAppearance.BorderSize = 0
        Me.btnPostActivity.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPostActivity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPostActivity.ForeColor = System.Drawing.Color.Black
        Me.btnPostActivity.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPostActivity.GradientForeColor = System.Drawing.Color.Black
        Me.btnPostActivity.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPostActivity.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPostActivity.Location = New System.Drawing.Point(473, 13)
        Me.btnPostActivity.Name = "btnPostActivity"
        Me.btnPostActivity.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPostActivity.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPostActivity.Size = New System.Drawing.Size(111, 30)
        Me.btnPostActivity.TabIndex = 0
        Me.btnPostActivity.Text = "&Post To Payroll"
        Me.btnPostActivity.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(590, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnVoidPosting
        '
        Me.btnVoidPosting.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVoidPosting.BackColor = System.Drawing.Color.White
        Me.btnVoidPosting.BackgroundImage = CType(resources.GetObject("btnVoidPosting.BackgroundImage"), System.Drawing.Image)
        Me.btnVoidPosting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVoidPosting.BorderColor = System.Drawing.Color.Empty
        Me.btnVoidPosting.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnVoidPosting.FlatAppearance.BorderSize = 0
        Me.btnVoidPosting.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVoidPosting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoidPosting.ForeColor = System.Drawing.Color.Black
        Me.btnVoidPosting.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVoidPosting.GradientForeColor = System.Drawing.Color.Black
        Me.btnVoidPosting.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoidPosting.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnVoidPosting.Location = New System.Drawing.Point(473, 13)
        Me.btnVoidPosting.Name = "btnVoidPosting"
        Me.btnVoidPosting.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoidPosting.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnVoidPosting.Size = New System.Drawing.Size(111, 30)
        Me.btnVoidPosting.TabIndex = 2
        Me.btnVoidPosting.Text = "&Void Posting"
        Me.btnVoidPosting.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Activity"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "UoM"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 50
        '
        'DataGridViewTextBoxColumn5
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle10
        Me.DataGridViewTextBoxColumn5.HeaderText = "Value"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle11
        Me.DataGridViewTextBoxColumn6.HeaderText = "Rate"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle12
        Me.DataGridViewTextBoxColumn7.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'frmActivity_Posting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(699, 452)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmActivity_Posting"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Post to Payroll"
        Me.pnlMain.ResumeLayout(False)
        Me.gbActivityPosting.ResumeLayout(False)
        Me.gbActivityPosting.PerformLayout()
        Me.objpnlData.ResumeLayout(False)
        Me.objpnlData.PerformLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnPostActivity As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbActivityPosting As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchMeasures As eZee.Common.eZeeGradientButton
    Friend WithEvents objpnlData As System.Windows.Forms.Panel
    Friend WithEvents cboViewBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblViewBy As System.Windows.Forms.Label
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents btnVoidPosting As eZee.Common.eZeeLightButton
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents objdgcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhActivity As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhUoM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhActValue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnVoidUnpostedActivity As eZee.Common.eZeeLightButton
    Friend WithEvents LblFromDate As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchAcitivity As eZee.Common.eZeeGradientButton
    Friend WithEvents cboActivitiy As System.Windows.Forms.ComboBox
    Friend WithEvents LblActivitiy As System.Windows.Forms.Label
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblToDate As System.Windows.Forms.Label
End Class
