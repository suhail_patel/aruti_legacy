﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImport_Activity_Master

#Region " Private Variables "

    Private Const mstrModuleName As String = "frmImport_Activity_Master"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Private dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Private mDicWeekName As Dictionary(Of String, Integer)

#End Region

#Region " Form's Events "

    Private Sub frmImport_Activity_Master_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImport_Activity_Master_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizActivityMst_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizActivityMst.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizActivityMst.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizActivityMst_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizActivityMst_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizActivityMst.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizActivityMst.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                    Call SetDataCombo()
                Case eZeeWizActivityMst.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFieldMapping.Controls
                            If TypeOf ctrl Is ComboBox Then
                                If CType(ctrl, ComboBox).Text = "" Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please the select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    e.Cancel = True
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                    If CInt(cboHeadType.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Head Type is mandatory information. Please select Head Type."), enMsgBoxStyle.Information)
                        e.Cancel = True
                    End If
                Case eZeeWizActivityMst.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizActivityMst_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetColor()
        Try
            cboActivityCode.BackColor = GUI.ColorComp
            cboActivityName.BackColor = GUI.ColorComp
            cboMeasures.BackColor = GUI.ColorComp
            cboHeadType.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMMaster As New clsMasterData
        Try
            Dim dsList As DataSet = objMMaster.getComboListForHeadType("List")
            Dim dTab As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enTranHeadType.EmployeesStatutoryDeductions & "," & enTranHeadType.EmployersStatutoryContributions & ")", "Id", DataViewRowState.CurrentRows).ToTable
            With cboHeadType
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dTab
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox AndAlso ctrl.Name.ToUpper <> "CBOHEADTYPE" Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboActivityCode.Items.Add(dtColumns.ColumnName)
                cboActivityName.Items.Add(dtColumns.ColumnName)
                cboMeasures.Items.Add(dtColumns.ColumnName)
            Next

            FillCombo()
            SetColor()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Try
            Dim blnIsNotThrown As Boolean = True
            ezWait.Active = True

            mdt_ImportData_Others.Columns.Add("ActivityCode", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("ActivityName", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("measure", System.Type.GetType("System.String")).DefaultValue = ""

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("Message", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("Status", System.Type.GetType("System.String")).DefaultValue = ""
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String")).DefaultValue = ""

            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboActivityCode.Text & " IS NULL OR " & cboActivityName.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ActivityCode") = dtRow.Item(cboActivityCode.Text).ToString.Trim
                drNewRow.Item("ActivityName") = dtRow.Item(cboActivityName.Text).ToString.Trim
                drNewRow.Item("measure") = dtRow.Item(cboMeasures.Text).ToString.Trim
                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If blnIsNotThrown = True Then
                colhActivityCode.DataPropertyName = "ActivityCode"
                colhActivityName.DataPropertyName = "ActivityName"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            eZeeWizActivityMst.BackEnabled = False
            eZeeWizActivityMst.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try

            With dtRow
                If .Item(cboActivityCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Activity Code is mandatory information. Please select Activity Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboActivityName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Activity Name is mandatory information. Please select Activity Name to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboMeasures.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Measure is mandatory information. Please select Measure to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

            End With

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then
                Exit Sub
            End If

            Dim objAMaster As New clsActivity_Master
            Dim objMeasure As New clsUnitMeasure_Master
            Dim iMeasureID As Integer = 0

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                '------------------------------ START CHECKING IF MEASURE PRESENT.
                iMeasureID = 0
                If dtRow.Item("measure").ToString.Trim.Length > 0 Then
                    'Sohail (30 Jun 2017) -- Start
                    'Issue - not able to edit measurement.
                    'objMeasure.isExist("", dtRow.Item("measure").ToString.Trim)
                    objMeasure.isExist("", dtRow.Item("measure").ToString.Trim, , iMeasureID)
                    'Sohail (30 Jun 2017) -- End
                    iMeasureID = objMeasure._Measureunkid
                    If iMeasureID <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 8, "Measure Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 9, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If
                '------------------------------ END CHECKING IF MEASURE PRESENT.

                objAMaster._Code = dtRow.Item("ActivityCode").ToString.Trim
                objAMaster._Name = dtRow.Item("ActivityName").ToString.Trim
                objAMaster._TranheadTypeId = CInt(cboHeadType.SelectedValue)
                objAMaster._Measureunkid = iMeasureID
                objAMaster._Is_Ot_Activity = chkOTActivity.Checked
                objAMaster._Isappearonpayslip = chkAppearOnPayslip.Checked
                objAMaster._IsappearOTonpayslip = chkAppearOTOnPayslip.Checked
                objAMaster._IsappearPHonpayslip = chkAppearPHOnPayslip.Checked
                objAMaster._Istaxable = chkTaxable.Checked
                objAMaster._IstaxableOT = chkTaxableOT.Checked
                objAMaster._IstaxablePH = chkTaxablePH.Checked

                With objAMaster
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
                    ._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With

                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objAMaster.Insert() = True Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objAMaster.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime) = True Then
                If objAMaster.Insert(FinancialYear._Object._DatabaseName, ConfigParameter._Object._CurrentDateAndTime, User._Object._Userunkid) = True Then
                    'Shani(24-Aug-2015) -- End

                    'Sohail (21 Aug 2015) -- End
                    dtRow.Item("image") = imgAccept
                    dtRow.Item("message") = ""
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 10, "Success")
                    dtRow.Item("objStatus") = 1
                    objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                Else
                    dtRow.Item("image") = imgError
                    If objAMaster._Message.Trim.Length > 0 Then
                        dtRow.Item("message") = objAMaster._Message.Trim
                    Else
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 11, "Insert Data Failed.")
                    End If
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 9, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Context Menu Events "

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Activity Master Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Events"

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" AndAlso cr.Name.ToUpper <> "CBOHEADTYPE" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboHeadType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboHeadType.SelectedIndexChanged
        Try
            If CInt(cboHeadType.SelectedValue) = enTranHeadType.EarningForEmployees Then
                chkTaxable.Enabled = True
                chkTaxableOT.Enabled = True
                chkTaxablePH.Enabled = True

                chkTaxable.Checked = True
                chkTaxableOT.Checked = True
                chkTaxablePH.Checked = True
            Else
                chkTaxable.Checked = False
                chkTaxable.Enabled = False

                chkTaxableOT.Checked = False
                chkTaxablePH.Enabled = False

                chkTaxablePH.Checked = False
                chkTaxablePH.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboHeadType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

			Me.btnFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

			Me.objbuttonSave.GradientBackColor = GUI._ButttonBackColor 
			Me.objbuttonSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeWizActivityMst.CancelText = Language._Object.getCaption(Me.eZeeWizActivityMst.Name & "_CancelText" , Me.eZeeWizActivityMst.CancelText)
			Me.eZeeWizActivityMst.NextText = Language._Object.getCaption(Me.eZeeWizActivityMst.Name & "_NextText" , Me.eZeeWizActivityMst.NextText)
			Me.eZeeWizActivityMst.BackText = Language._Object.getCaption(Me.eZeeWizActivityMst.Name & "_BackText" , Me.eZeeWizActivityMst.BackText)
			Me.eZeeWizActivityMst.FinishText = Language._Object.getCaption(Me.eZeeWizActivityMst.Name & "_FinishText" , Me.eZeeWizActivityMst.FinishText)
			Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
			Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
			Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
			Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
			Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
			Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
			Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
			Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
			Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
			Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
			Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
			Me.lblActivitycode.Text = Language._Object.getCaption(Me.lblActivitycode.Name, Me.lblActivitycode.Text)
			Me.lblActivityName.Text = Language._Object.getCaption(Me.lblActivityName.Name, Me.lblActivityName.Text)
			Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
			Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
			Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
			Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
			Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
			Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
			Me.lblTransactionHead.Text = Language._Object.getCaption(Me.lblTransactionHead.Name, Me.lblTransactionHead.Text)
			Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
			Me.chkTaxablePH.Text = Language._Object.getCaption(Me.chkTaxablePH.Name, Me.chkTaxablePH.Text)
			Me.chkAppearPHOnPayslip.Text = Language._Object.getCaption(Me.chkAppearPHOnPayslip.Name, Me.chkAppearPHOnPayslip.Text)
			Me.chkTaxableOT.Text = Language._Object.getCaption(Me.chkTaxableOT.Name, Me.chkTaxableOT.Text)
			Me.chkAppearOTOnPayslip.Text = Language._Object.getCaption(Me.chkAppearOTOnPayslip.Name, Me.chkAppearOTOnPayslip.Text)
			Me.chkTaxable.Text = Language._Object.getCaption(Me.chkTaxable.Name, Me.chkTaxable.Text)
			Me.chkAppearOnPayslip.Text = Language._Object.getCaption(Me.chkAppearOnPayslip.Name, Me.chkAppearOnPayslip.Text)
			Me.chkOTActivity.Text = Language._Object.getCaption(Me.chkOTActivity.Name, Me.chkOTActivity.Text)
			Me.Label2.Text = Language._Object.getCaption(Me.Label2.Name, Me.Label2.Text)
			Me.lblMeasures.Text = Language._Object.getCaption(Me.lblMeasures.Name, Me.lblMeasures.Text)
			Me.colhActivityCode.HeaderText = Language._Object.getCaption(Me.colhActivityCode.Name, Me.colhActivityCode.HeaderText)
			Me.colhActivityName.HeaderText = Language._Object.getCaption(Me.colhActivityName.Name, Me.colhActivityName.HeaderText)
			Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
			Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please the select proper file to Import Data from.")
			Language.setMessage(mstrModuleName, 2, "Please the select proper field to Import Data.")
			Language.setMessage(mstrModuleName, 3, "Activity Code is mandatory information. Please select Activity Code to continue.")
			Language.setMessage(mstrModuleName, 4, "Activity Name is mandatory information. Please select Activity Name to continue.")
			Language.setMessage(mstrModuleName, 5, "Measure is mandatory information. Please select Measure to continue.")
			Language.setMessage(mstrModuleName, 6, "Head Type is mandatory information. Please select Head Type.")
			Language.setMessage(mstrModuleName, 7, "This field is already selected.Please Select New field.")
			Language.setMessage(mstrModuleName, 8, "Measure Not Found.")
			Language.setMessage(mstrModuleName, 9, "Fail")
			Language.setMessage(mstrModuleName, 10, "Success")
			Language.setMessage(mstrModuleName, 11, "Insert Data Failed.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class