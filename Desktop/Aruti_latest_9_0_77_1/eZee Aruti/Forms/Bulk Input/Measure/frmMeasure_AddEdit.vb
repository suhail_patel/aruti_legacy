﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMeasure_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmMeasure_AddEdit"
    Private mblnCancel As Boolean = True
    Private objUoM As clsUnitMeasure_Master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintMeasureUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintMeasureUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintMeasureUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtCode.Text = objUoM._Code
            txtName.Text = objUoM._Name
            txtDescription.Text = objUoM._Description
            If objUoM._Isformula_Based = True Then
                radFB.Checked = True
            Else
                radNF.Checked = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objUoM._Code = txtCode.Text
            objUoM._Name = txtName.Text
            objUoM._Description = txtDescription.Text
            objUoM._Isformula_Based = radFB.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Function isValid_Data() As Boolean
        Try
            If txtCode.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Measure Code is mandatory information. Please provide Measure Code."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Return False
            End If

            If txtName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Measure Name is mandatory information. Please provide Measure Name."), enMsgBoxStyle.Information)
                txtName.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isValid_Data", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmMeasure_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objUoM = Nothing
    End Sub

    Private Sub frmMeasure_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMeasure_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMeasure_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMeasure_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMeasure_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objUoM = New clsUnitMeasure_Master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetVisibility()
            Call setColor()

            If menAction = enAction.EDIT_ONE Then
                objUoM._Measureunkid = mintMeasureUnkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMeasure_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsUnitMeasure_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsUnitMeasure_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If isValid_Data() = False Then Exit Sub

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objUoM._FormName = mstrModuleName
            objUoM._LoginEmployeeunkid = 0
            objUoM._ClientIP = getIP()
            objUoM._HostName = getHostName()
            objUoM._FromWeb = False
            objUoM._AuditUserId = User._Object._Userunkid
objUoM._CompanyUnkid = Company._Object._Companyunkid
            objUoM._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objUoM.Update()
            Else
                blnFlag = objUoM.Insert()
            End If

            If blnFlag = False And objUoM._Message <> "" Then
                eZeeMsgBox.Show(objUoM._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objUoM = Nothing
                    objUoM = New clsUnitMeasure_Master
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintMeasureUnkid = objUoM._Measureunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbUoM.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbUoM.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbUoM.Text = Language._Object.getCaption(Me.gbUoM.Name, Me.gbUoM.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
			Me.lblType.Text = Language._Object.getCaption(Me.lblType.Name, Me.lblType.Text)
			Me.radFB.Text = Language._Object.getCaption(Me.radFB.Name, Me.radFB.Text)
			Me.radNF.Text = Language._Object.getCaption(Me.radNF.Name, Me.radNF.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Measure Code is mandatory information. Please provide Measure Code.")
			Language.setMessage(mstrModuleName, 2, "Measure Name is mandatory information. Please provide Measure Name.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class