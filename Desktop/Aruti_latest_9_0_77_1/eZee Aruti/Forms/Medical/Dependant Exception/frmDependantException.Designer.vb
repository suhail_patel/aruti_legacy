﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDependantException
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDependantException))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.dgDependants = New System.Windows.Forms.DataGridView
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchRelation = New eZee.Common.eZeeGradientButton
        Me.cboRelation = New System.Windows.Forms.ComboBox
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.LblRelation = New System.Windows.Forms.Label
        Me.objbtnSearchDependants = New eZee.Common.eZeeGradientButton
        Me.cboDependents = New System.Windows.Forms.ComboBox
        Me.lbldependants = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCollaps = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhIschecked = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhParticulars = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBirthdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRelation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhIsMedical = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhMdStopdate = New eZee.Common.DataGridViewMaskTextBoxColumn
        Me.objdgcolhMdImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.objcolhIsLeave = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhLvStopdate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhLvImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhIsGroup = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgDependants, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.chkSelectAll)
        Me.pnlMainInfo.Controls.Add(Me.dgDependants)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(817, 508)
        Me.pnlMainInfo.TabIndex = 0
        '
        'chkSelectAll
        '
        Me.chkSelectAll.Location = New System.Drawing.Point(42, 114)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(13, 15)
        Me.chkSelectAll.TabIndex = 13
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'dgDependants
        '
        Me.dgDependants.AllowUserToAddRows = False
        Me.dgDependants.AllowUserToDeleteRows = False
        Me.dgDependants.BackgroundColor = System.Drawing.Color.White
        Me.dgDependants.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgDependants.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollaps, Me.objcolhIschecked, Me.dgcolhParticulars, Me.dgcolhBirthdate, Me.dgcolhRelation, Me.objcolhIsMedical, Me.dgcolhMdStopdate, Me.objdgcolhMdImage, Me.objcolhIsLeave, Me.dgcolhLvStopdate, Me.objdgcolhLvImage, Me.objdgcolhIsGroup, Me.objdgcolhEmpId})
        Me.dgDependants.Location = New System.Drawing.Point(10, 108)
        Me.dgDependants.Name = "dgDependants"
        Me.dgDependants.RowHeadersVisible = False
        Me.dgDependants.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgDependants.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgDependants.Size = New System.Drawing.Size(797, 339)
        Me.dgDependants.TabIndex = 12
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchRelation)
        Me.gbFilterCriteria.Controls.Add(Me.cboRelation)
        Me.gbFilterCriteria.Controls.Add(Me.EZeeStraightLine2)
        Me.gbFilterCriteria.Controls.Add(Me.LblRelation)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchDependants)
        Me.gbFilterCriteria.Controls.Add(Me.cboDependents)
        Me.gbFilterCriteria.Controls.Add(Me.lbldependants)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(10, 9)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(797, 92)
        Me.gbFilterCriteria.TabIndex = 11
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(656, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(86, 13)
        Me.lnkAllocation.TabIndex = 104
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocations"
        '
        'objbtnSearchRelation
        '
        Me.objbtnSearchRelation.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchRelation.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchRelation.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchRelation.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchRelation.BorderSelected = False
        Me.objbtnSearchRelation.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchRelation.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchRelation.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchRelation.Location = New System.Drawing.Point(758, 33)
        Me.objbtnSearchRelation.Name = "objbtnSearchRelation"
        Me.objbtnSearchRelation.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchRelation.TabIndex = 220
        '
        'cboRelation
        '
        Me.cboRelation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRelation.DropDownWidth = 150
        Me.cboRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRelation.FormattingEnabled = True
        Me.cboRelation.Location = New System.Drawing.Point(527, 33)
        Me.cboRelation.Name = "cboRelation"
        Me.cboRelation.Size = New System.Drawing.Size(225, 21)
        Me.cboRelation.TabIndex = 219
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(415, 29)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(10, 59)
        Me.EZeeStraightLine2.TabIndex = 218
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'LblRelation
        '
        Me.LblRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRelation.Location = New System.Drawing.Point(439, 36)
        Me.LblRelation.Name = "LblRelation"
        Me.LblRelation.Size = New System.Drawing.Size(74, 15)
        Me.LblRelation.TabIndex = 217
        Me.LblRelation.Text = "Relation"
        '
        'objbtnSearchDependants
        '
        Me.objbtnSearchDependants.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDependants.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDependants.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDependants.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDependants.BorderSelected = False
        Me.objbtnSearchDependants.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDependants.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDependants.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDependants.Location = New System.Drawing.Point(367, 60)
        Me.objbtnSearchDependants.Name = "objbtnSearchDependants"
        Me.objbtnSearchDependants.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDependants.TabIndex = 216
        '
        'cboDependents
        '
        Me.cboDependents.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDependents.DropDownWidth = 300
        Me.cboDependents.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDependents.FormattingEnabled = True
        Me.cboDependents.Location = New System.Drawing.Point(102, 60)
        Me.cboDependents.Name = "cboDependents"
        Me.cboDependents.Size = New System.Drawing.Size(259, 21)
        Me.cboDependents.TabIndex = 2
        '
        'lbldependants
        '
        Me.lbldependants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldependants.Location = New System.Drawing.Point(8, 63)
        Me.lbldependants.Name = "lbldependants"
        Me.lbldependants.Size = New System.Drawing.Size(74, 15)
        Me.lbldependants.TabIndex = 214
        Me.lbldependants.Text = "Dependants"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(770, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(367, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 86
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(747, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(102, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(259, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(74, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 453)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(817, 55)
        Me.objFooter.TabIndex = 10
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(712, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(616, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 13
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = ""
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 25
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Particulars"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Relation"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 190
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "EmpID"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "EmpID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'objdgcolhCollaps
        '
        Me.objdgcolhCollaps.HeaderText = ""
        Me.objdgcolhCollaps.Name = "objdgcolhCollaps"
        Me.objdgcolhCollaps.ReadOnly = True
        Me.objdgcolhCollaps.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCollaps.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhCollaps.Width = 25
        '
        'objcolhIschecked
        '
        Me.objcolhIschecked.HeaderText = ""
        Me.objcolhIschecked.Name = "objcolhIschecked"
        Me.objcolhIschecked.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhIschecked.Width = 25
        '
        'dgcolhParticulars
        '
        Me.dgcolhParticulars.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhParticulars.HeaderText = "Particulars"
        Me.dgcolhParticulars.Name = "dgcolhParticulars"
        Me.dgcolhParticulars.ReadOnly = True
        Me.dgcolhParticulars.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhParticulars.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhBirthdate
        '
        Me.dgcolhBirthdate.HeaderText = "Birth Date"
        Me.dgcolhBirthdate.Name = "dgcolhBirthdate"
        Me.dgcolhBirthdate.ReadOnly = True
        Me.dgcolhBirthdate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhBirthdate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhBirthdate.Width = 75
        '
        'dgcolhRelation
        '
        Me.dgcolhRelation.HeaderText = "Relation"
        Me.dgcolhRelation.Name = "dgcolhRelation"
        Me.dgcolhRelation.ReadOnly = True
        Me.dgcolhRelation.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhRelation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objcolhIsMedical
        '
        Me.objcolhIsMedical.HeaderText = "Medical"
        Me.objcolhIsMedical.Name = "objcolhIsMedical"
        Me.objcolhIsMedical.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhIsMedical.Width = 50
        '
        'dgcolhMdStopdate
        '
        DataGridViewCellStyle1.NullValue = Nothing
        Me.dgcolhMdStopdate.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhMdStopdate.HeaderText = "Medical Stop Date"
        Me.dgcolhMdStopdate.Mask = ""
        Me.dgcolhMdStopdate.MinimumWidth = 10
        Me.dgcolhMdStopdate.Name = "dgcolhMdStopdate"
        Me.dgcolhMdStopdate.ReadOnly = True
        Me.dgcolhMdStopdate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'objdgcolhMdImage
        '
        Me.objdgcolhMdImage.HeaderText = ""
        Me.objdgcolhMdImage.Name = "objdgcolhMdImage"
        Me.objdgcolhMdImage.Width = 30
        '
        'objcolhIsLeave
        '
        Me.objcolhIsLeave.HeaderText = "Leave"
        Me.objcolhIsLeave.Name = "objcolhIsLeave"
        Me.objcolhIsLeave.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhIsLeave.Width = 50
        '
        'dgcolhLvStopdate
        '
        Me.dgcolhLvStopdate.HeaderText = "Leave Stop Date"
        Me.dgcolhLvStopdate.Name = "dgcolhLvStopdate"
        Me.dgcolhLvStopdate.ReadOnly = True
        Me.dgcolhLvStopdate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhLvStopdate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhLvImage
        '
        Me.objdgcolhLvImage.HeaderText = ""
        Me.objdgcolhLvImage.Image = Global.Aruti.Main.My.Resources.Resources.NextReservation_16
        Me.objdgcolhLvImage.Name = "objdgcolhLvImage"
        Me.objdgcolhLvImage.ReadOnly = True
        Me.objdgcolhLvImage.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhLvImage.Width = 30
        '
        'objdgcolhIsGroup
        '
        Me.objdgcolhIsGroup.HeaderText = "IsGroup"
        Me.objdgcolhIsGroup.Name = "objdgcolhIsGroup"
        Me.objdgcolhIsGroup.ReadOnly = True
        Me.objdgcolhIsGroup.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhIsGroup.Visible = False
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "EmpID"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.ReadOnly = True
        Me.objdgcolhEmpId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEmpId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpId.Visible = False
        '
        'frmDependantException
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(817, 508)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDependantException"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Medical Dependant Exception"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.dgDependants, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboDependents As System.Windows.Forms.ComboBox
    Friend WithEvents lbldependants As System.Windows.Forms.Label
    Friend WithEvents LblRelation As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchDependants As eZee.Common.eZeeGradientButton
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objbtnSearchRelation As eZee.Common.eZeeGradientButton
    Friend WithEvents cboRelation As System.Windows.Forms.ComboBox
    Friend WithEvents dgDependants As System.Windows.Forms.DataGridView
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCollaps As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhIschecked As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhParticulars As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBirthdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRelation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhIsMedical As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhMdStopdate As eZee.Common.DataGridViewMaskTextBoxColumn
    Friend WithEvents objdgcolhMdImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objcolhIsLeave As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhLvStopdate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhLvImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhIsGroup As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
