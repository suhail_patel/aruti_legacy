﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMedicalCategoryTranList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMedicalCategoryTranList))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.lblTo = New System.Windows.Forms.Label
        Me.txtDependantsTo = New eZee.TextBox.NumericTextBox
        Me.txtDependantsFrom = New eZee.TextBox.NumericTextBox
        Me.lblDependants = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblCategory = New System.Windows.Forms.Label
        Me.lblCoverAmountTo = New System.Windows.Forms.Label
        Me.colhMaxDependants = New System.Windows.Forms.ColumnHeader
        Me.colhName = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.lvMedicalCategory = New eZee.Common.eZeeListView(Me.components)
        Me.colhCoverAmount = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboCategory = New System.Windows.Forms.ComboBox
        Me.lblCoverAmountFrom = New System.Windows.Forms.Label
        Me.txtAmountTo = New eZee.TextBox.NumericTextBox
        Me.txtAmountFrom = New eZee.TextBox.NumericTextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.btnMapProvider = New eZee.Common.eZeeLightButton(Me.components)
        Me.objFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(846, 473)
        Me.pnlMainInfo.TabIndex = 0
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(185, 63)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(44, 15)
        Me.lblTo.TabIndex = 216
        Me.lblTo.Text = "To"
        '
        'txtDependantsTo
        '
        Me.txtDependantsTo.AllowNegative = True
        Me.txtDependantsTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDependantsTo.DigitsInGroup = 0
        Me.txtDependantsTo.Flags = 0
        Me.txtDependantsTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDependantsTo.Location = New System.Drawing.Point(232, 60)
        Me.txtDependantsTo.MaxDecimalPlaces = 6
        Me.txtDependantsTo.MaxWholeDigits = 21
        Me.txtDependantsTo.Name = "txtDependantsTo"
        Me.txtDependantsTo.Prefix = ""
        Me.txtDependantsTo.RangeMax = 1.7976931348623157E+308
        Me.txtDependantsTo.RangeMin = -1.7976931348623157E+308
        Me.txtDependantsTo.Size = New System.Drawing.Size(55, 21)
        Me.txtDependantsTo.TabIndex = 3
        Me.txtDependantsTo.Text = "0"
        Me.txtDependantsTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDependantsFrom
        '
        Me.txtDependantsFrom.AllowNegative = True
        Me.txtDependantsFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDependantsFrom.DigitsInGroup = 0
        Me.txtDependantsFrom.Flags = 0
        Me.txtDependantsFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDependantsFrom.Location = New System.Drawing.Point(124, 60)
        Me.txtDependantsFrom.MaxDecimalPlaces = 6
        Me.txtDependantsFrom.MaxWholeDigits = 21
        Me.txtDependantsFrom.Name = "txtDependantsFrom"
        Me.txtDependantsFrom.Prefix = ""
        Me.txtDependantsFrom.RangeMax = 1.7976931348623157E+308
        Me.txtDependantsFrom.RangeMin = -1.7976931348623157E+308
        Me.txtDependantsFrom.Size = New System.Drawing.Size(55, 21)
        Me.txtDependantsFrom.TabIndex = 2
        Me.txtDependantsFrom.Text = "0"
        Me.txtDependantsFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDependants
        '
        Me.lblDependants.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDependants.Location = New System.Drawing.Point(8, 63)
        Me.lblDependants.Name = "lblDependants"
        Me.lblDependants.Size = New System.Drawing.Size(110, 15)
        Me.lblDependants.TabIndex = 213
        Me.lblDependants.Text = "Dependants"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnMapProvider)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 418)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(846, 55)
        Me.objFooter.TabIndex = 7
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(645, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 76
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(741, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 73
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(548, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 75
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(452, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 74
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'lblCategory
        '
        Me.lblCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategory.Location = New System.Drawing.Point(8, 34)
        Me.lblCategory.Name = "lblCategory"
        Me.lblCategory.Size = New System.Drawing.Size(110, 15)
        Me.lblCategory.TabIndex = 211
        Me.lblCategory.Text = "Medical Category"
        '
        'lblCoverAmountTo
        '
        Me.lblCoverAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCoverAmountTo.Location = New System.Drawing.Point(303, 63)
        Me.lblCoverAmountTo.Name = "lblCoverAmountTo"
        Me.lblCoverAmountTo.Size = New System.Drawing.Size(110, 15)
        Me.lblCoverAmountTo.TabIndex = 210
        Me.lblCoverAmountTo.Text = "Cover Amount To"
        '
        'colhMaxDependants
        '
        Me.colhMaxDependants.Tag = "colhMaxDependants"
        Me.colhMaxDependants.Text = "Dependants"
        Me.colhMaxDependants.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.colhMaxDependants.Width = 90
        '
        'colhName
        '
        Me.colhName.Tag = "colhName"
        Me.colhName.Text = "Name"
        Me.colhName.Width = 160
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 100
        '
        'lvMedicalCategory
        '
        Me.lvMedicalCategory.BackColorOnChecked = True
        Me.lvMedicalCategory.ColumnHeaders = Nothing
        Me.lvMedicalCategory.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCode, Me.colhName, Me.colhMaxDependants, Me.colhCoverAmount, Me.colhDescription})
        Me.lvMedicalCategory.CompulsoryColumns = ""
        Me.lvMedicalCategory.FullRowSelect = True
        Me.lvMedicalCategory.GridLines = True
        Me.lvMedicalCategory.GroupingColumn = Nothing
        Me.lvMedicalCategory.HideSelection = False
        Me.lvMedicalCategory.Location = New System.Drawing.Point(12, 164)
        Me.lvMedicalCategory.MinColumnWidth = 50
        Me.lvMedicalCategory.MultiSelect = False
        Me.lvMedicalCategory.Name = "lvMedicalCategory"
        Me.lvMedicalCategory.OptionalColumns = ""
        Me.lvMedicalCategory.ShowMoreItem = False
        Me.lvMedicalCategory.ShowSaveItem = False
        Me.lvMedicalCategory.ShowSelectAll = True
        Me.lvMedicalCategory.ShowSizeAllColumnsToFit = True
        Me.lvMedicalCategory.Size = New System.Drawing.Size(822, 248)
        Me.lvMedicalCategory.Sortable = True
        Me.lvMedicalCategory.TabIndex = 10
        Me.lvMedicalCategory.UseCompatibleStateImageBehavior = False
        Me.lvMedicalCategory.View = System.Windows.Forms.View.Details
        '
        'colhCoverAmount
        '
        Me.colhCoverAmount.Tag = "colhCoverAmount"
        Me.colhCoverAmount.Text = "Cover Amount"
        Me.colhCoverAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhCoverAmount.Width = 110
        '
        'colhDescription
        '
        Me.colhDescription.Tag = "colhDescription"
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 355
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtDependantsTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtDependantsFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblDependants)
        Me.gbFilterCriteria.Controls.Add(Me.lblCategory)
        Me.gbFilterCriteria.Controls.Add(Me.lblCoverAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblCoverAmountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtAmountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblDescription)
        Me.gbFilterCriteria.Controls.Add(Me.txtDescription)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 91
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(822, 91)
        Me.gbFilterCriteria.TabIndex = 9
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCategory
        '
        Me.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCategory.DropDownWidth = 150
        Me.cboCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCategory.FormattingEnabled = True
        Me.cboCategory.Location = New System.Drawing.Point(124, 31)
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(163, 21)
        Me.cboCategory.TabIndex = 1
        '
        'lblCoverAmountFrom
        '
        Me.lblCoverAmountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCoverAmountFrom.Location = New System.Drawing.Point(303, 34)
        Me.lblCoverAmountFrom.Name = "lblCoverAmountFrom"
        Me.lblCoverAmountFrom.Size = New System.Drawing.Size(110, 15)
        Me.lblCoverAmountFrom.TabIndex = 209
        Me.lblCoverAmountFrom.Text = "Cover Amount From"
        '
        'txtAmountTo
        '
        Me.txtAmountTo.AllowNegative = True
        Me.txtAmountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmountTo.DigitsInGroup = 0
        Me.txtAmountTo.Flags = 0
        Me.txtAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountTo.Location = New System.Drawing.Point(419, 60)
        Me.txtAmountTo.MaxDecimalPlaces = 6
        Me.txtAmountTo.MaxWholeDigits = 21
        Me.txtAmountTo.Name = "txtAmountTo"
        Me.txtAmountTo.Prefix = ""
        Me.txtAmountTo.RangeMax = 1.7976931348623157E+308
        Me.txtAmountTo.RangeMin = -1.7976931348623157E+308
        Me.txtAmountTo.Size = New System.Drawing.Size(109, 21)
        Me.txtAmountTo.TabIndex = 5
        Me.txtAmountTo.Text = "0"
        Me.txtAmountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAmountFrom
        '
        Me.txtAmountFrom.AllowNegative = True
        Me.txtAmountFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmountFrom.DigitsInGroup = 0
        Me.txtAmountFrom.Flags = 0
        Me.txtAmountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountFrom.Location = New System.Drawing.Point(419, 31)
        Me.txtAmountFrom.MaxDecimalPlaces = 6
        Me.txtAmountFrom.MaxWholeDigits = 21
        Me.txtAmountFrom.Name = "txtAmountFrom"
        Me.txtAmountFrom.Prefix = ""
        Me.txtAmountFrom.RangeMax = 1.7976931348623157E+308
        Me.txtAmountFrom.RangeMin = -1.7976931348623157E+308
        Me.txtAmountFrom.Size = New System.Drawing.Size(109, 21)
        Me.txtAmountFrom.TabIndex = 4
        Me.txtAmountFrom.Text = "0"
        Me.txtAmountFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(534, 34)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(76, 15)
        Me.lblDescription.TabIndex = 206
        Me.lblDescription.Text = "Description"
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(616, 33)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(193, 48)
        Me.txtDescription.TabIndex = 6
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(795, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(772, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(846, 60)
        Me.eZeeHeader.TabIndex = 8
        Me.eZeeHeader.Title = "Medical Category List"
        '
        'btnMapProvider
        '
        Me.btnMapProvider.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMapProvider.BackColor = System.Drawing.Color.White
        Me.btnMapProvider.BackgroundImage = CType(resources.GetObject("btnMapProvider.BackgroundImage"), System.Drawing.Image)
        Me.btnMapProvider.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnMapProvider.BorderColor = System.Drawing.Color.Empty
        Me.btnMapProvider.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnMapProvider.FlatAppearance.BorderSize = 0
        Me.btnMapProvider.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMapProvider.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMapProvider.ForeColor = System.Drawing.Color.Black
        Me.btnMapProvider.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnMapProvider.GradientForeColor = System.Drawing.Color.Black
        Me.btnMapProvider.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMapProvider.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnMapProvider.Location = New System.Drawing.Point(12, 13)
        Me.btnMapProvider.Name = "btnMapProvider"
        Me.btnMapProvider.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnMapProvider.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnMapProvider.Size = New System.Drawing.Size(106, 30)
        Me.btnMapProvider.TabIndex = 77
        Me.btnMapProvider.Text = "Map Provider"
        Me.btnMapProvider.UseVisualStyleBackColor = True
        '
        'frmMedicalCategoryTranList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 473)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.lvMedicalCategory)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle

        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMedicalCategoryTranList"

        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Medical Category List"
        Me.objFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents txtDependantsTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtDependantsFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents lblDependants As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents lblCategory As System.Windows.Forms.Label
    Friend WithEvents lblCoverAmountTo As System.Windows.Forms.Label
    Friend WithEvents colhMaxDependants As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvMedicalCategory As eZee.Common.eZeeListView
    Friend WithEvents colhCoverAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblCoverAmountFrom As System.Windows.Forms.Label
    Friend WithEvents txtAmountTo As eZee.TextBox.NumericTextBox
    Friend WithEvents txtAmountFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
    Friend WithEvents btnMapProvider As eZee.Common.eZeeLightButton
End Class
