﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmProvider_UserMapping

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmProvider_UserMapping"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objProviderMapping As clsprovider_mapping
    Private mintMappingUnkid As Integer


    'Pinkal (5-MAY-2012) -- Start
    'Enhancement : TRA Changes
    Private mdvProvider As DataView = Nothing
    'Pinkal (5-MAY-2012) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction
            Me.ShowDialog()

            intUnkId = mintMappingUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboUser.BackColor = GUI.ColorComp


            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes
            cboCountry.BackColor = GUI.ColorOptional
            cboState.BackColor = GUI.ColorOptional
            cboCity.BackColor = GUI.ColorOptional
            'Pinkal (5-MAY-2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objProviderMapping._Userunkid = CInt(cboUser.SelectedValue)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillProvider()
        Dim dsFill As DataSet = Nothing
        Try
            Dim objProvider As New clsinstitute_master


            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes
            'dsFill = objProvider.getListForCombo(True, "List", False, 1)
            dsFill = objProvider.GetList("Institute", True, False, 1)
            'Pinkal (5-MAY-2012) -- End



            For Each dr As DataRow In dsFill.Tables(0).Rows
                Dim drRow As DataRow = objProviderMapping._dtProvider.NewRow
                drRow("mapproviderunkid") = -1
                drRow("ischecked") = False


                'Pinkal (5-MAY-2012) -- Start
                'Enhancement : TRA Changes
                'drRow("instituteunkid") = dr("instituteunkid").ToString()
                'drRow("institute_name") = dr("name").ToString()
                drRow("instituteunkid") = dr("instituteunkid").ToString()
                drRow("institute_name") = dr("institute_name").ToString()
                drRow("countryunkid") = dr("countryunkid").ToString()
                drRow("stateunkid") = dr("stateunkid").ToString()
                drRow("cityunkid") = dr("cityunkid").ToString()
                'Pinkal (5-MAY-2012) -- End

                
                drRow("userunkid") = -1
                objProviderMapping._dtProvider.Rows.Add(drRow)
            Next


            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes
            mdvProvider = objProviderMapping._dtProvider.DefaultView
            'Pinkal (5-MAY-2012) -- End



            dgProvider.AutoGenerateColumns = False


            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes
            'dgProvider.DataSource = objProviderMapping._dtProvider
            dgProvider.DataSource = mdvProvider
            'Pinkal (5-MAY-2012) -- End


            objdgcolhSelect.DataPropertyName = "ischecked"
            dgcolhProvider.DataPropertyName = "institute_name"
            objdgcolhproviderId.DataPropertyName = "instituteunkid"
            objdgcolhProviderMappingId.DataPropertyName = "instituteunkid"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillProvider", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objUser As New clsUserAddEdit
            'S.SANDEEP [ 11 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim dsList As DataSet = objUser.getComboList("User", True, False)
            Dim dsList As New DataSet
            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : Aruti SaaS Changes
            'Dim objPswd As New clsPassowdOptions
            'If objPswd._IsEmployeeAsUser Then
            '    dsList = objUser.getComboList("User", True, False, True)
            'Else
            '    dsList = objUser.getComboList("User", True, False)
            'End If
            'objPswd = Nothing

            'Nilay (01-Mar-2016) -- Start
            'dsList = objUser.getNewComboList("User", , True)
            dsList = objUser.getNewComboList("User", , True, , , , True)
            'Nilay (01-Mar-2016) -- End

            'S.SANDEEP [10 AUG 2015] -- END

            With cboUser
                .DisplayMember = "name"
                .ValueMember = "userunkid"
                .DataSource = dsList.Tables(0)
            End With
            'S.SANDEEP [ 11 DEC 2012 ] -- END

            


            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes

            dsList = Nothing
            Dim objMasterData As New clsMasterData
            dsList = objMasterData.getCountryList("Country", True)
            cboCountry.ValueMember = "countryunkid"
            cboCountry.DisplayMember = "country_name"
            cboCountry.DataSource = dsList.Tables(0)


            dsList = Nothing
            Dim objState As New clsstate_master
            dsList = objState.GetList("State", True, True, 0)
            cboState.ValueMember = "stateunkid"
            cboState.DisplayMember = "name"
            cboState.DataSource = dsList.Tables(0)

            dsList = Nothing
            Dim objCity As New clscity_master
            dsList = objCity.GetList("City", True, True, 0)
            cboCity.ValueMember = "cityunkid"
            cboCity.DisplayMember = "name"
            cboCity.DataSource = dsList.Tables(0)

            'Pinkal (5-MAY-2012) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmProvider_UserMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objProviderMapping = New clsprovider_mapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
            FillProvider()
            SetColor()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_UserMapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProvider_UserMapping_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_UserMapping_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProvider_UserMapping_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_UserMapping_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProvider_UserMapping_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objProviderMapping = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Try
            Dim objfrm As New frmCommonSearch
            With objfrm
                .ValueMember = cboUser.ValueMember
                .DisplayMember = cboUser.DisplayMember
                .DataSource = CType(cboUser.DataSource, DataTable)
            End With

            If objfrm.DisplayDialog Then
                cboUser.SelectedValue = objfrm.SelectedValue
                cboUser.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (5-MAY-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchCountry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCountry.Click
        Try
            Dim objfrm As New frmCommonSearch
            With objfrm
                .ValueMember = cboCountry.ValueMember
                .DisplayMember = cboCountry.DisplayMember
                .DataSource = CType(cboCountry.DataSource, DataTable)
            End With

            If objfrm.DisplayDialog Then
                cboCountry.SelectedValue = objfrm.SelectedValue
                cboCountry.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCountry_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchState_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchState.Click
        Try
            Dim objfrm As New frmCommonSearch
            With objfrm
                .ValueMember = cboState.ValueMember
                .DisplayMember = cboState.DisplayMember
                .DataSource = CType(cboState.DataSource, DataTable)
            End With

            If objfrm.DisplayDialog Then
                cboState.SelectedValue = objfrm.SelectedValue
                cboState.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchState_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchCity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchCity.Click
        Try
            Dim objfrm As New frmCommonSearch
            With objfrm
                .ValueMember = cboCity.ValueMember
                .DisplayMember = cboCity.DisplayMember
                .DataSource = CType(cboCity.DataSource, DataTable)
            End With

            If objfrm.DisplayDialog Then
                cboCity.SelectedValue = objfrm.SelectedValue
                cboCity.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchCity_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Dim strSearching As String = ""
        Try

            If CInt(cboCountry.SelectedValue) > 0 Then
                strSearching &= "AND countryunkid = " & CInt(cboCountry.SelectedValue) & " "
            End If

            If CInt(cboState.SelectedValue) > 0 Then
                strSearching &= "AND stateunkid = " & CInt(cboState.SelectedValue) & " "
            End If

            If CInt(cboCity.SelectedValue) > 0 Then
                strSearching &= "AND cityunkid = " & CInt(cboCity.SelectedValue) & " "
            End If

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If

            mdvProvider.RowFilter = strSearching
            dgProvider_CellContentClick(New Object(), New DataGridViewCellEventArgs(objdgcolhSelect.Index, 0))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboCountry.SelectedIndex = 0
            cboState.SelectedIndex = 0
            cboCity.SelectedIndex = 0
            cboUser_SelectedIndexChanged(sender, e)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (5-MAY-2012) -- End


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboUser.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "User is compulsory information.Please Select User."), enMsgBoxStyle.Information)
                cboUser.Select()
                Exit Sub
            End If

            If objProviderMapping._dtProvider IsNot Nothing Then
                Dim drRow As DataRow() = objProviderMapping._dtProvider.Select("ischecked=true")

                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Atleast One Provider."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objProviderMapping._FormName = mstrModuleName
            objProviderMapping._LoginEmployeeunkid = 0
            objProviderMapping._ClientIP = getIP()
            objProviderMapping._HostName = getHostName()
            objProviderMapping._FromWeb = False
            objProviderMapping._AuditUserId = User._Object._Userunkid
objProviderMapping._CompanyUnkid = Company._Object._Companyunkid
            objProviderMapping._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            blnFlag = objProviderMapping.Insert()

            If blnFlag = False And objProviderMapping._Message <> "" Then
                eZeeMsgBox.Show(objProviderMapping._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboUser_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUser.SelectedIndexChanged
        Try
            chkSelect.Checked = False
            If CInt(cboUser.SelectedValue) > 0 Then
                Dim dsUser As DataSet = objProviderMapping.GetList("List", -1, CInt(cboUser.SelectedValue))
                If dsUser IsNot Nothing AndAlso dsUser.Tables(0).Rows.Count > 0 Then
                    Dim i As Integer = 0
                    For Each dr As DataRow In dsUser.Tables(0).Rows

                        'Pinkal (5-MAY-2012) -- Start
                        'Enhancement : TRA Changes

                        'Dim drRow As DataRow() = CType(dgProvider.DataSource, DataTable).Select("instituteunkid = " & dr("providerunkid").ToString())
                        'If drRow.Length > 0 Then
                        '    drRow(0)("ischecked") = True
                        '    dgProvider_CellContentClick(New Object(), New DataGridViewCellEventArgs(objdgcolhSelect.Index, i))
                        '    drRow(0)("mapproviderunkid") = dr("mapproviderunkid")
                        '    drRow(0).AcceptChanges()
                        'End If

                        Dim drRow As DataRow() = mdvProvider.Table().Select("instituteunkid = " & dr("providerunkid").ToString())
                        If drRow.Length > 0 Then
                            drRow(0)("ischecked") = True
                            dgProvider_CellContentClick(New Object(), New DataGridViewCellEventArgs(objdgcolhSelect.Index, i))
                            drRow(0)("mapproviderunkid") = dr("mapproviderunkid")
                            drRow(0).AcceptChanges()
                        End If

                        'Pinkal (5-MAY-2012) -- End

                        
                        i += 1
                    Next
                Else

                    'Pinkal (5-MAY-2012) -- Start
                    'Enhancement : TRA Changes
                    'objProviderMapping._dtProvider.Rows.Clear()
                    'FillProvider()
                    'Pinkal (5-MAY-2012) -- End

                    
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboUser_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkSelect_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelect.CheckedChanged
        Try

            If objProviderMapping._dtProvider.Rows.Count > 0 Then
                For Each dr As DataRow In objProviderMapping._dtProvider.Rows
                    RemoveHandler dgProvider.CellContentClick, AddressOf dgProvider_CellContentClick
                    dr("ischecked") = chkSelect.Checked
                    AddHandler dgProvider.CellContentClick, AddressOf dgProvider_CellContentClick
                Next
                objProviderMapping._dtProvider.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelect_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Datagrid Event"

    Private Sub dgProvider_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgProvider.CellContentClick, dgProvider.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhSelect.Index Then

                If dgProvider.IsCurrentCellDirty Then
                    dgProvider.CommitEdit(DataGridViewDataErrorContexts.Commit)


                    'Pinkal (5-MAY-2012) -- Start
                    'Enhancement : TRA Changes
                    'objProviderMapping._dtProvider.AcceptChanges()
                    mdvProvider.Table().AcceptChanges()
                    'Pinkal (5-MAY-2012) -- End


                End If


                'Pinkal (5-MAY-2012) -- Start
                'Enhancement : TRA Changes
                'Dim drRow As DataRow() = objProviderMapping._dtProvider.Select("ischecked = true")
                Dim drRow As DataRow() = mdvProvider.ToTable.Select("ischecked = true")
                'Pinkal (5-MAY-2012) -- End


                RemoveHandler chkSelect.CheckedChanged, AddressOf chkSelect_CheckedChanged

                If drRow.Length <= 0 Then
                    chkSelect.CheckState = CheckState.Unchecked
                ElseIf drRow.Length < dgProvider.Rows.Count Then
                    chkSelect.CheckState = CheckState.Indeterminate
                ElseIf drRow.Length = dgProvider.Rows.Count Then
                    chkSelect.CheckState = CheckState.Checked
                End If

                AddHandler chkSelect.CheckedChanged, AddressOf chkSelect_CheckedChanged

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgProvider_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.

#Region " Language & UI Settings "

    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbProviderdetail.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbProviderdetail.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbProviderdetail.Text = Language._Object.getCaption(Me.gbProviderdetail.Name, Me.gbProviderdetail.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.Name, Me.lblUser.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.chkSelect.Text = Language._Object.getCaption(Me.chkSelect.Name, Me.chkSelect.Text)
            Me.dgcolhProvider.HeaderText = Language._Object.getCaption(Me.dgcolhProvider.Name, Me.dgcolhProvider.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.lblState.Text = Language._Object.getCaption(Me.lblState.Name, Me.lblState.Text)
			Me.lblCountry.Text = Language._Object.getCaption(Me.lblCountry.Name, Me.lblCountry.Text)
			Me.lblCity.Text = Language._Object.getCaption(Me.lblCity.Name, Me.lblCity.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub

    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "User is compulsory information.Please Select User.")
            Language.setMessage(mstrModuleName, 2, "Please Select Atleast One Provider.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub

#End Region 'Language & UI Settings

	'</Language>


End Class
