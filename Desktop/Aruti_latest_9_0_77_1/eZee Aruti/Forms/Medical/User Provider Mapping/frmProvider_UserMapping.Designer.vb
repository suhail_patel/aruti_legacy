﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProvider_UserMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProvider_UserMapping))
        Me.gbProviderdetail = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchCity = New eZee.Common.eZeeGradientButton
        Me.cboCity = New System.Windows.Forms.ComboBox
        Me.lblCity = New System.Windows.Forms.Label
        Me.objbtnSearchState = New eZee.Common.eZeeGradientButton
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.lblState = New System.Windows.Forms.Label
        Me.objbtnSearchCountry = New eZee.Common.eZeeGradientButton
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblCountry = New System.Windows.Forms.Label
        Me.objbtnSearchUser = New eZee.Common.eZeeGradientButton
        Me.pnlProvider = New System.Windows.Forms.Panel
        Me.chkSelect = New System.Windows.Forms.CheckBox
        Me.dgProvider = New System.Windows.Forms.DataGridView
        Me.objdgcolhSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhProvider = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhproviderId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhProviderMappingId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboUser = New System.Windows.Forms.ComboBox
        Me.lblUser = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbProviderdetail.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlProvider.SuspendLayout()
        CType(Me.dgProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbProviderdetail
        '
        Me.gbProviderdetail.BorderColor = System.Drawing.Color.Black
        Me.gbProviderdetail.Checked = False
        Me.gbProviderdetail.CollapseAllExceptThis = False
        Me.gbProviderdetail.CollapsedHoverImage = Nothing
        Me.gbProviderdetail.CollapsedNormalImage = Nothing
        Me.gbProviderdetail.CollapsedPressedImage = Nothing
        Me.gbProviderdetail.CollapseOnLoad = False
        Me.gbProviderdetail.Controls.Add(Me.objbtnReset)
        Me.gbProviderdetail.Controls.Add(Me.objbtnSearch)
        Me.gbProviderdetail.Controls.Add(Me.objbtnSearchCity)
        Me.gbProviderdetail.Controls.Add(Me.cboCity)
        Me.gbProviderdetail.Controls.Add(Me.lblCity)
        Me.gbProviderdetail.Controls.Add(Me.objbtnSearchState)
        Me.gbProviderdetail.Controls.Add(Me.cboState)
        Me.gbProviderdetail.Controls.Add(Me.lblState)
        Me.gbProviderdetail.Controls.Add(Me.objbtnSearchCountry)
        Me.gbProviderdetail.Controls.Add(Me.cboCountry)
        Me.gbProviderdetail.Controls.Add(Me.lblCountry)
        Me.gbProviderdetail.Controls.Add(Me.objbtnSearchUser)
        Me.gbProviderdetail.Controls.Add(Me.pnlProvider)
        Me.gbProviderdetail.Controls.Add(Me.cboUser)
        Me.gbProviderdetail.Controls.Add(Me.lblUser)
        Me.gbProviderdetail.ExpandedHoverImage = Nothing
        Me.gbProviderdetail.ExpandedNormalImage = Nothing
        Me.gbProviderdetail.ExpandedPressedImage = Nothing
        Me.gbProviderdetail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbProviderdetail.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbProviderdetail.HeaderHeight = 25
        Me.gbProviderdetail.HeaderMessage = ""
        Me.gbProviderdetail.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbProviderdetail.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbProviderdetail.HeightOnCollapse = 0
        Me.gbProviderdetail.LeftTextSpace = 0
        Me.gbProviderdetail.Location = New System.Drawing.Point(10, 4)
        Me.gbProviderdetail.Name = "gbProviderdetail"
        Me.gbProviderdetail.OpenHeight = 300
        Me.gbProviderdetail.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbProviderdetail.ShowBorder = True
        Me.gbProviderdetail.ShowCheckBox = False
        Me.gbProviderdetail.ShowCollapseButton = False
        Me.gbProviderdetail.ShowDefaultBorderColor = True
        Me.gbProviderdetail.ShowDownButton = False
        Me.gbProviderdetail.ShowHeader = True
        Me.gbProviderdetail.Size = New System.Drawing.Size(589, 307)
        Me.gbProviderdetail.TabIndex = 8
        Me.gbProviderdetail.Temp = 0
        Me.gbProviderdetail.Text = "Service Provider "
        Me.gbProviderdetail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(564, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(541, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objbtnSearchCity
        '
        Me.objbtnSearchCity.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCity.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCity.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCity.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCity.BorderSelected = False
        Me.objbtnSearchCity.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCity.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCity.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCity.Location = New System.Drawing.Point(564, 30)
        Me.objbtnSearchCity.Name = "objbtnSearchCity"
        Me.objbtnSearchCity.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCity.TabIndex = 313
        '
        'cboCity
        '
        Me.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCity.FormattingEnabled = True
        Me.cboCity.Location = New System.Drawing.Point(364, 30)
        Me.cboCity.Name = "cboCity"
        Me.cboCity.Size = New System.Drawing.Size(194, 21)
        Me.cboCity.TabIndex = 311
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(302, 33)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(56, 15)
        Me.lblCity.TabIndex = 312
        Me.lblCity.Text = "City"
        Me.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchState
        '
        Me.objbtnSearchState.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchState.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchState.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchState.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchState.BorderSelected = False
        Me.objbtnSearchState.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchState.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchState.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchState.Location = New System.Drawing.Point(274, 59)
        Me.objbtnSearchState.Name = "objbtnSearchState"
        Me.objbtnSearchState.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchState.TabIndex = 310
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(74, 59)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(194, 21)
        Me.cboState.TabIndex = 308
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(5, 62)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(63, 15)
        Me.lblState.TabIndex = 309
        Me.lblState.Text = "State"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCountry
        '
        Me.objbtnSearchCountry.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCountry.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCountry.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCountry.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCountry.BorderSelected = False
        Me.objbtnSearchCountry.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCountry.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCountry.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCountry.Location = New System.Drawing.Point(274, 30)
        Me.objbtnSearchCountry.Name = "objbtnSearchCountry"
        Me.objbtnSearchCountry.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCountry.TabIndex = 307
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(74, 30)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(194, 21)
        Me.cboCountry.TabIndex = 305
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(5, 33)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(63, 15)
        Me.lblCountry.TabIndex = 306
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchUser
        '
        Me.objbtnSearchUser.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchUser.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchUser.BorderSelected = False
        Me.objbtnSearchUser.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchUser.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchUser.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchUser.Location = New System.Drawing.Point(564, 59)
        Me.objbtnSearchUser.Name = "objbtnSearchUser"
        Me.objbtnSearchUser.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchUser.TabIndex = 298
        '
        'pnlProvider
        '
        Me.pnlProvider.Controls.Add(Me.chkSelect)
        Me.pnlProvider.Controls.Add(Me.dgProvider)
        Me.pnlProvider.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlProvider.Location = New System.Drawing.Point(3, 88)
        Me.pnlProvider.Name = "pnlProvider"
        Me.pnlProvider.Size = New System.Drawing.Size(582, 216)
        Me.pnlProvider.TabIndex = 303
        '
        'chkSelect
        '
        Me.chkSelect.AutoSize = True
        Me.chkSelect.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelect.Location = New System.Drawing.Point(7, 5)
        Me.chkSelect.Name = "chkSelect"
        Me.chkSelect.Size = New System.Drawing.Size(15, 14)
        Me.chkSelect.TabIndex = 301
        Me.chkSelect.UseVisualStyleBackColor = True
        '
        'dgProvider
        '
        Me.dgProvider.AllowUserToAddRows = False
        Me.dgProvider.AllowUserToDeleteRows = False
        Me.dgProvider.AllowUserToResizeRows = False
        Me.dgProvider.BackgroundColor = System.Drawing.Color.White
        Me.dgProvider.ColumnHeadersHeight = 21
        Me.dgProvider.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgProvider.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhSelect, Me.dgcolhProvider, Me.objdgcolhproviderId, Me.objdgcolhProviderMappingId})
        Me.dgProvider.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgProvider.Location = New System.Drawing.Point(0, 0)
        Me.dgProvider.Name = "dgProvider"
        Me.dgProvider.RowHeadersVisible = False
        Me.dgProvider.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgProvider.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgProvider.Size = New System.Drawing.Size(582, 216)
        Me.dgProvider.TabIndex = 299
        '
        'objdgcolhSelect
        '
        Me.objdgcolhSelect.HeaderText = ""
        Me.objdgcolhSelect.Name = "objdgcolhSelect"
        Me.objdgcolhSelect.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhSelect.Width = 25
        '
        'dgcolhProvider
        '
        Me.dgcolhProvider.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhProvider.HeaderText = "Service Provider"
        Me.dgcolhProvider.Name = "dgcolhProvider"
        Me.dgcolhProvider.ReadOnly = True
        Me.dgcolhProvider.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhproviderId
        '
        Me.objdgcolhproviderId.HeaderText = "providerId"
        Me.objdgcolhproviderId.Name = "objdgcolhproviderId"
        Me.objdgcolhproviderId.ReadOnly = True
        Me.objdgcolhproviderId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhproviderId.Visible = False
        '
        'objdgcolhProviderMappingId
        '
        Me.objdgcolhProviderMappingId.HeaderText = "ProviderMappingId"
        Me.objdgcolhProviderMappingId.Name = "objdgcolhProviderMappingId"
        Me.objdgcolhProviderMappingId.ReadOnly = True
        Me.objdgcolhProviderMappingId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhProviderMappingId.Visible = False
        '
        'cboUser
        '
        Me.cboUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUser.FormattingEnabled = True
        Me.cboUser.Location = New System.Drawing.Point(364, 59)
        Me.cboUser.Name = "cboUser"
        Me.cboUser.Size = New System.Drawing.Size(194, 21)
        Me.cboUser.TabIndex = 1
        '
        'lblUser
        '
        Me.lblUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.Location = New System.Drawing.Point(302, 62)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(56, 15)
        Me.lblUser.TabIndex = 291
        Me.lblUser.Text = "User"
        Me.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 314)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(611, 55)
        Me.objFooter.TabIndex = 9
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(400, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(503, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "Service Provider"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "providerId"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Visible = False
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "ProviderMappingId"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'frmProvider_UserMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(611, 369)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbProviderdetail)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProvider_UserMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "User Mapping "
        Me.gbProviderdetail.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlProvider.ResumeLayout(False)
        Me.pnlProvider.PerformLayout()
        CType(Me.dgProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbProviderdetail As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents cboUser As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchUser As eZee.Common.eZeeGradientButton
    Friend WithEvents dgProvider As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkSelect As System.Windows.Forms.CheckBox
    Friend WithEvents objdgcolhSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhProvider As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhproviderId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhProviderMappingId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlProvider As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchState As eZee.Common.eZeeGradientButton
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCountry As eZee.Common.eZeeGradientButton
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchCity As eZee.Common.eZeeGradientButton
    Friend WithEvents cboCity As System.Windows.Forms.ComboBox
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
End Class
