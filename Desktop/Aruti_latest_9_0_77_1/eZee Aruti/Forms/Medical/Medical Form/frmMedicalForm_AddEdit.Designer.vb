﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMedicalForm_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMedicalForm_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbMedicalInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnAddService = New eZee.Common.eZeeGradientButton
        Me.dtpClaimDate = New System.Windows.Forms.DateTimePicker
        Me.lblClaimDate = New System.Windows.Forms.Label
        Me.txtClaimNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblClaimNo = New System.Windows.Forms.Label
        Me.lblService = New System.Windows.Forms.Label
        Me.cboService = New System.Windows.Forms.ComboBox
        Me.txtServiceNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblServiceNo = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.txtaction = New eZee.TextBox.AlphanumericTextBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblDoctors = New System.Windows.Forms.Label
        Me.txtIllness_Accident = New eZee.TextBox.AlphanumericTextBox
        Me.lblNatureOfIllness = New System.Windows.Forms.Label
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.lblMedicalYear = New System.Windows.Forms.Label
        Me.cboMedicalYear = New System.Windows.Forms.ComboBox
        Me.cboMedicalPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnAddPeriod = New eZee.Common.eZeeGradientButton
        Me.lblMedicalPeriod = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.gbMedicalInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbMedicalInfo)
        Me.pnlMainInfo.Controls.Add(Me.eZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.objbtnAddService)
        Me.pnlMainInfo.Controls.Add(Me.lblService)
        Me.pnlMainInfo.Controls.Add(Me.cboService)
        Me.pnlMainInfo.Controls.Add(Me.txtServiceNo)
        Me.pnlMainInfo.Controls.Add(Me.lblServiceNo)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(680, 353)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbMedicalInfo
        '
        Me.gbMedicalInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMedicalInfo.Checked = False
        Me.gbMedicalInfo.CollapseAllExceptThis = False
        Me.gbMedicalInfo.CollapsedHoverImage = Nothing
        Me.gbMedicalInfo.CollapsedNormalImage = Nothing
        Me.gbMedicalInfo.CollapsedPressedImage = Nothing
        Me.gbMedicalInfo.CollapseOnLoad = False
        Me.gbMedicalInfo.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbMedicalInfo.Controls.Add(Me.dtpClaimDate)
        Me.gbMedicalInfo.Controls.Add(Me.lblClaimDate)
        Me.gbMedicalInfo.Controls.Add(Me.txtClaimNo)
        Me.gbMedicalInfo.Controls.Add(Me.lblClaimNo)
        Me.gbMedicalInfo.Controls.Add(Me.lblEmployee)
        Me.gbMedicalInfo.Controls.Add(Me.txtaction)
        Me.gbMedicalInfo.Controls.Add(Me.cboEmployee)
        Me.gbMedicalInfo.Controls.Add(Me.lblDoctors)
        Me.gbMedicalInfo.Controls.Add(Me.txtIllness_Accident)
        Me.gbMedicalInfo.Controls.Add(Me.lblNatureOfIllness)
        Me.gbMedicalInfo.Controls.Add(Me.EZeeStraightLine2)
        Me.gbMedicalInfo.Controls.Add(Me.lblMedicalYear)
        Me.gbMedicalInfo.Controls.Add(Me.cboMedicalYear)
        Me.gbMedicalInfo.Controls.Add(Me.cboMedicalPeriod)
        Me.gbMedicalInfo.Controls.Add(Me.objbtnAddPeriod)
        Me.gbMedicalInfo.Controls.Add(Me.lblMedicalPeriod)
        Me.gbMedicalInfo.ExpandedHoverImage = Nothing
        Me.gbMedicalInfo.ExpandedNormalImage = Nothing
        Me.gbMedicalInfo.ExpandedPressedImage = Nothing
        Me.gbMedicalInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMedicalInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMedicalInfo.HeaderHeight = 25
        Me.gbMedicalInfo.HeightOnCollapse = 0
        Me.gbMedicalInfo.LeftTextSpace = 0
        Me.gbMedicalInfo.Location = New System.Drawing.Point(10, 66)
        Me.gbMedicalInfo.Name = "gbMedicalInfo"
        Me.gbMedicalInfo.OpenHeight = 300
        Me.gbMedicalInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMedicalInfo.ShowBorder = True
        Me.gbMedicalInfo.ShowCheckBox = False
        Me.gbMedicalInfo.ShowCollapseButton = False
        Me.gbMedicalInfo.ShowDefaultBorderColor = True
        Me.gbMedicalInfo.ShowDownButton = False
        Me.gbMedicalInfo.ShowHeader = True
        Me.gbMedicalInfo.Size = New System.Drawing.Size(656, 226)
        Me.gbMedicalInfo.TabIndex = 20
        Me.gbMedicalInfo.Temp = 0
        Me.gbMedicalInfo.Text = "Employee Injuries Info"
        Me.gbMedicalInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(246, 61)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 87
        '
        'objbtnAddService
        '
        Me.objbtnAddService.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddService.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddService.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddService.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddService.BorderSelected = False
        Me.objbtnAddService.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddService.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddService.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddService.Location = New System.Drawing.Point(245, 148)
        Me.objbtnAddService.Name = "objbtnAddService"
        Me.objbtnAddService.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddService.TabIndex = 215
        '
        'dtpClaimDate
        '
        Me.dtpClaimDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpClaimDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpClaimDate.Location = New System.Drawing.Point(107, 114)
        Me.dtpClaimDate.Name = "dtpClaimDate"
        Me.dtpClaimDate.Size = New System.Drawing.Size(100, 21)
        Me.dtpClaimDate.TabIndex = 7
        '
        'lblClaimDate
        '
        Me.lblClaimDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimDate.Location = New System.Drawing.Point(8, 117)
        Me.lblClaimDate.Name = "lblClaimDate"
        Me.lblClaimDate.Size = New System.Drawing.Size(93, 15)
        Me.lblClaimDate.TabIndex = 106
        Me.lblClaimDate.Text = "Claim Date"
        '
        'txtClaimNo
        '
        Me.txtClaimNo.Flags = 0
        Me.txtClaimNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClaimNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtClaimNo.Location = New System.Drawing.Point(107, 87)
        Me.txtClaimNo.Name = "txtClaimNo"
        Me.txtClaimNo.Size = New System.Drawing.Size(133, 21)
        Me.txtClaimNo.TabIndex = 6
        '
        'lblClaimNo
        '
        Me.lblClaimNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClaimNo.Location = New System.Drawing.Point(8, 90)
        Me.lblClaimNo.Name = "lblClaimNo"
        Me.lblClaimNo.Size = New System.Drawing.Size(93, 15)
        Me.lblClaimNo.TabIndex = 104
        Me.lblClaimNo.Text = "Reference No"
        '
        'lblService
        '
        Me.lblService.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblService.Location = New System.Drawing.Point(7, 151)
        Me.lblService.Name = "lblService"
        Me.lblService.Size = New System.Drawing.Size(93, 15)
        Me.lblService.TabIndex = 102
        Me.lblService.Text = "Service"
        '
        'cboService
        '
        Me.cboService.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboService.DropDownWidth = 150
        Me.cboService.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboService.FormattingEnabled = True
        Me.cboService.Location = New System.Drawing.Point(106, 148)
        Me.cboService.Name = "cboService"
        Me.cboService.Size = New System.Drawing.Size(133, 21)
        Me.cboService.TabIndex = 4
        '
        'txtServiceNo
        '
        Me.txtServiceNo.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtServiceNo.Flags = 0
        Me.txtServiceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServiceNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtServiceNo.Location = New System.Drawing.Point(106, 175)
        Me.txtServiceNo.Name = "txtServiceNo"
        Me.txtServiceNo.ReadOnly = True
        Me.txtServiceNo.Size = New System.Drawing.Size(133, 21)
        Me.txtServiceNo.TabIndex = 5
        '
        'lblServiceNo
        '
        Me.lblServiceNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServiceNo.Location = New System.Drawing.Point(7, 178)
        Me.lblServiceNo.Name = "lblServiceNo"
        Me.lblServiceNo.Size = New System.Drawing.Size(93, 15)
        Me.lblServiceNo.TabIndex = 99
        Me.lblServiceNo.Text = "Provider No"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 64)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(93, 15)
        Me.lblEmployee.TabIndex = 4
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtaction
        '
        Me.txtaction.Flags = 0
        Me.txtaction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtaction.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtaction.Location = New System.Drawing.Point(292, 147)
        Me.txtaction.Multiline = True
        Me.txtaction.Name = "txtaction"
        Me.txtaction.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtaction.Size = New System.Drawing.Size(349, 69)
        Me.txtaction.TabIndex = 9
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(107, 60)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(133, 21)
        Me.cboEmployee.TabIndex = 3
        '
        'lblDoctors
        '
        Me.lblDoctors.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDoctors.Location = New System.Drawing.Point(292, 129)
        Me.lblDoctors.Name = "lblDoctors"
        Me.lblDoctors.Size = New System.Drawing.Size(148, 15)
        Me.lblDoctors.TabIndex = 110
        Me.lblDoctors.Text = "Action / Remark"
        Me.lblDoctors.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIllness_Accident
        '
        Me.txtIllness_Accident.Flags = 0
        Me.txtIllness_Accident.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIllness_Accident.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtIllness_Accident.Location = New System.Drawing.Point(292, 54)
        Me.txtIllness_Accident.Multiline = True
        Me.txtIllness_Accident.Name = "txtIllness_Accident"
        Me.txtIllness_Accident.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtIllness_Accident.Size = New System.Drawing.Size(349, 72)
        Me.txtIllness_Accident.TabIndex = 8
        '
        'lblNatureOfIllness
        '
        Me.lblNatureOfIllness.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNatureOfIllness.Location = New System.Drawing.Point(292, 36)
        Me.lblNatureOfIllness.Name = "lblNatureOfIllness"
        Me.lblNatureOfIllness.Size = New System.Drawing.Size(148, 15)
        Me.lblNatureOfIllness.TabIndex = 108
        Me.lblNatureOfIllness.Text = "Nature of illness / Accident"
        Me.lblNatureOfIllness.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(273, 24)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(13, 202)
        Me.EZeeStraightLine2.TabIndex = 90
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'lblMedicalYear
        '
        Me.lblMedicalYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMedicalYear.Location = New System.Drawing.Point(8, 36)
        Me.lblMedicalYear.Name = "lblMedicalYear"
        Me.lblMedicalYear.Size = New System.Drawing.Size(93, 15)
        Me.lblMedicalYear.TabIndex = 1
        Me.lblMedicalYear.Text = "Medical Year"
        Me.lblMedicalYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMedicalYear
        '
        Me.cboMedicalYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMedicalYear.DropDownWidth = 150
        Me.cboMedicalYear.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMedicalYear.FormattingEnabled = True
        Me.cboMedicalYear.Location = New System.Drawing.Point(107, 33)
        Me.cboMedicalYear.Name = "cboMedicalYear"
        Me.cboMedicalYear.Size = New System.Drawing.Size(133, 21)
        Me.cboMedicalYear.TabIndex = 1
        '
        'cboMedicalPeriod
        '
        Me.cboMedicalPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMedicalPeriod.DropDownWidth = 150
        Me.cboMedicalPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMedicalPeriod.FormattingEnabled = True
        Me.cboMedicalPeriod.Location = New System.Drawing.Point(107, 60)
        Me.cboMedicalPeriod.Name = "cboMedicalPeriod"
        Me.cboMedicalPeriod.Size = New System.Drawing.Size(133, 21)
        Me.cboMedicalPeriod.TabIndex = 2
        Me.cboMedicalPeriod.Visible = False
        '
        'objbtnAddPeriod
        '
        Me.objbtnAddPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddPeriod.BorderSelected = False
        Me.objbtnAddPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddPeriod.Location = New System.Drawing.Point(246, 63)
        Me.objbtnAddPeriod.Name = "objbtnAddPeriod"
        Me.objbtnAddPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddPeriod.TabIndex = 218
        Me.objbtnAddPeriod.Visible = False
        '
        'lblMedicalPeriod
        '
        Me.lblMedicalPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMedicalPeriod.Location = New System.Drawing.Point(8, 63)
        Me.lblMedicalPeriod.Name = "lblMedicalPeriod"
        Me.lblMedicalPeriod.Size = New System.Drawing.Size(93, 15)
        Me.lblMedicalPeriod.TabIndex = 115
        Me.lblMedicalPeriod.Text = "Medical Period"
        Me.lblMedicalPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblMedicalPeriod.Visible = False
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(680, 60)
        Me.eZeeHeader.TabIndex = 21
        Me.eZeeHeader.Title = "Employee Injuries"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 298)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(680, 55)
        Me.objFooter.TabIndex = 15
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(571, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 11
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(468, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frmMedicalForm_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(680, 353)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMedicalForm_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Injuries"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        Me.gbMedicalInfo.ResumeLayout(False)
        Me.gbMedicalInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbMedicalInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblMedicalYear As System.Windows.Forms.Label
    Friend WithEvents cboMedicalYear As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblService As System.Windows.Forms.Label
    Friend WithEvents cboService As System.Windows.Forms.ComboBox
    Friend WithEvents txtServiceNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblServiceNo As System.Windows.Forms.Label
    Friend WithEvents dtpClaimDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblClaimDate As System.Windows.Forms.Label
    Friend WithEvents txtClaimNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblClaimNo As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblNatureOfIllness As System.Windows.Forms.Label
    Friend WithEvents txtaction As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDoctors As System.Windows.Forms.Label
    Friend WithEvents txtIllness_Accident As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboMedicalPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblMedicalPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnAddService As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
End Class
