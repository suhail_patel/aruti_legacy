﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 6

Public Class frmMedicalForm_AddEdit


#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmMedicalForm_AddEdit"
    Private mblnCancel As Boolean = True
    Private objInjurymaster As clsmedical_injury_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintInjuryMasterUnkid As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintInjuryMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintInjuryMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Form's Event"

    Private Sub frmMedicalForm_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objInjurymaster = New clsmedical_injury_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Call SetVisibility()
            setColor()
            If menAction = enAction.EDIT_ONE Then
                objInjurymaster._Injuryunkid = mintInjuryMasterUnkid
            End If

            'Pinkal (03-Jan-2011) -- Start

            dtpClaimDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
            dtpClaimDate.MaxDate = FinancialYear._Object._Database_End_Date.Date

            'Pinkal (03-Jan-2011) -- End


            FillCombo()
            FillEmployee()
            GetValue()
            cboMedicalYear.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalForm_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalForm_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalForm_AddEdit_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub frmMedicalForm_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalForm_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalForm_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objInjurymaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsmedical_injury_master.SetMessages()
            objfrm._Other_ModuleNames = "clsmedical_injury_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboMedicalYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMedicalYear.SelectedIndexChanged
        Try
            FillMedicalPeriod()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMedicalYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboService_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboService.SelectedIndexChanged
        Try
            If CInt(cboService.SelectedValue) < 0 Then Exit Sub
            Dim objMaster As New clsmedical_master
            objMaster._Mdmasterunkid = CInt(cboService.SelectedValue)
            txtServiceNo.Text = objMaster._MdServiceno
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboService_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If CInt(cboMedicalYear.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Medical Year is compulsory information.Please Select Medical Year."), enMsgBoxStyle.Information)
                cboMedicalYear.Select()
                Exit Sub
                'ElseIf CInt(cboMedicalPeriod.SelectedValue) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Medical Period is compulsory information.Please Select Medical Period."), enMsgBoxStyle.Information)
                '    cboMedicalPeriod.Select()
                '    Exit Sub
            ElseIf CInt(cboEmployee.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Exit Sub


                'Pinkal (07-Jan-2012) -- Start
                'Enhancement : TRA Changes

                'ElseIf CInt(cboService.SelectedValue) = 0 Then
                '    'Sandeep [ 09 Oct 2010 ] -- Start
                '    'Issues Reported by Vimal
                '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Membership is compulsory information.Please Select Membership."), enMsgBoxStyle.Information)
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Service is compulsory information.Please Select Service."), enMsgBoxStyle.Information)
                '    'Sandeep [ 09 Oct 2010 ] -- End 
                '    cboService.Select()
                '    Exit Sub
                'ElseIf Trim(txtServiceNo.Text) = "" Then
                '    'Sandeep [ 09 Oct 2010 ] -- Start
                '    'Issues Reported by Vimal
                '    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Membership No cannot be blank. Membership No is required information."), enMsgBoxStyle.Information)
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Provider No cannot be blank. Provider No is required information."), enMsgBoxStyle.Information)
                '    'Sandeep [ 09 Oct 2010 ] -- End 
                '    txtServiceNo.Focus()
                '    Exit Sub

                'Pinkal (07-Jan-2012) -- End

            ElseIf Trim(txtClaimNo.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Claim No cannot be blank. Claim No is required information."), enMsgBoxStyle.Information)
                txtClaimNo.Focus()
                Exit Sub
            End If

            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objInjurymaster._FormName = mstrModuleName
            objInjurymaster._LoginEmployeeunkid = 0
            objInjurymaster._ClientIP = getIP()
            objInjurymaster._HostName = getHostName()
            objInjurymaster._FromWeb = False
            objInjurymaster._AuditUserId = User._Object._Userunkid
objInjurymaster._CompanyUnkid = Company._Object._Companyunkid
            objInjurymaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objInjurymaster.Update()
            Else
                blnFlag = objInjurymaster.Insert()
            End If

            If blnFlag = False And objInjurymaster._Message <> "" Then
                eZeeMsgBox.Show(objInjurymaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objInjurymaster = Nothing
                    objInjurymaster = New clsmedical_injury_master
                    Call GetValue()
                    cboMedicalYear.Select()
                Else
                    mintInjuryMasterUnkid = objInjurymaster._Injuryunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAddService_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddService.Click
        Try
            'Dim objfrmMembership_AddEdit As New frmMembership_AddEdit
            'If objfrmMembership_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
            '    cboService.Select()
            'End If
            Dim objfrmMedicalMaster_AddEdit As New frmMedicalMaster_AddEdit
            Dim intRefId As Integer = -1
            Dim dsCombos As New DataSet
            Dim objMedical As New clsmedical_master
            If objfrmMedicalMaster_AddEdit.displayDialog(intRefId, enAction.ADD_ONE, enMedicalMasterType.Service) Then
                dsCombos = objMedical.getListForCombo(enMedicalMasterType.Service, "List", True)
                With cboService
                    .ValueMember = "mdmasterunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("List")
                End With
                cboService.SelectedValue = intRefId
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddService_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (07-Jan-2012) -- Start
    'Enhancement : TRA Changes


    'Private Sub objbtnAddEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim objfrmEmployeeMaster As New frmEmployeeMaster
    '        If objfrmEmployeeMaster.displayDialog(-1, enAction.ADD_CONTINUE) Then
    '            FillEmployee()
    '            cboEmployee.Select()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "objbtnAddEmployee_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim objFrm As New frmCommonSearch
            objFrm.DataSource = CType(cboEmployee.DataSource, DataTable)
            objFrm.DataSource = CType(cboEmployee.DataSource, DataTable)
            objFrm.DisplayMember = cboEmployee.DisplayMember
            objFrm.ValueMember = cboEmployee.ValueMember
            objFrm.CodeMember = "employeecode"
            If objFrm.DisplayDialog() Then
                cboEmployee.SelectedValue = objFrm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (07-Jan-2012) -- End

    Private Sub objbtnAddPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddPeriod.Click
        Try
            Dim objfrmperiod As New frmPeriod_AddEdit
            If objfrmperiod.displayDialog(-1, enAction.ADD_CONTINUE, CInt(enModuleReference.Medical)) Then
                FillMedicalPeriod()
                cboMedicalPeriod.Select()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Private Methods"

    Private Sub setColor()
        Try
            cboMedicalYear.BackColor = GUI.ColorComp
            cboMedicalPeriod.BackColor = GUI.ColorComp
            cboEmployee.BackColor = GUI.ColorComp
            cboService.BackColor = GUI.ColorComp
            txtServiceNo.BackColor = GUI.ColorComp
            txtClaimNo.BackColor = GUI.ColorComp
            txtaction.BackColor = GUI.ColorOptional
            txtIllness_Accident.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboMedicalYear.SelectedValue = CInt(objInjurymaster._Yearunkid)
            cboMedicalPeriod.SelectedValue = CInt(objInjurymaster._Periodunkid)
            If mintInjuryMasterUnkid > 0 Then
                cboEmployee.Enabled = False

                'Pinkal (07-Jan-2012) -- Start
                'Enhancement : TRA Changes
                'objbtnAddEmployee.Enabled = False
                'Pinkal (07-Jan-2012) -- End

            ElseIf mintInjuryMasterUnkid <= 0 Then
                cboEmployee.Enabled = True

                'Pinkal (07-Jan-2012) -- Start
                'Enhancement : TRA Changes
                'objbtnAddEmployee.Enabled = True
                'Pinkal (07-Jan-2012) -- End
            End If
            cboEmployee.SelectedValue = CInt(objInjurymaster._Employeeunkid)
            cboService.SelectedValue = CInt(objInjurymaster._Serviceunkid)
            txtClaimNo.Text = objInjurymaster._Claim_No
            If objInjurymaster._Injuryunkid > 0 Then
                dtpClaimDate.Value = objInjurymaster._Claim_Date

                'Pinkal (03-Jan-2011) -- Start

                'Else
                '    dtpClaimDate.Value = Now.Date

                'Pinkal (03-Jan-2011) -- End

            End If
            txtIllness_Accident.Text = objInjurymaster._Illness_Remark
            txtaction.Text = objInjurymaster._Remark
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objInjurymaster._Yearunkid = CInt(cboMedicalYear.SelectedValue)
            objInjurymaster._Periodunkid = CInt(cboMedicalPeriod.SelectedValue)
            objInjurymaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objInjurymaster._Serviceunkid = CInt(cboService.SelectedValue)
            '           objInjurymaster._Membershipno = txtServiceNo.Text.Trim
            objInjurymaster._Claim_No = txtClaimNo.Text.Trim
            objInjurymaster._Claim_Date = dtpClaimDate.Value
            objInjurymaster._Illness_Remark = txtIllness_Accident.Text.Trim
            objInjurymaster._Remark = txtaction.Text.Trim
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try

            Dim objMasterdata As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsFill = objMasterdata.getComboListPAYYEAR("Year", True)
            dsFill = objMasterdata.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            cboMedicalYear.DisplayMember = "name"
            cboMedicalYear.ValueMember = "Id"
            cboMedicalYear.DataSource = dsFill.Tables("Year")

            Dim objMaster As New clsmedical_master
            dsFill = objMaster.getListForCombo(enMedicalMasterType.Service, "Service", True)
            cboService.DisplayMember = "name"
            cboService.ValueMember = "mdmasterunkid"
            cboService.DataSource = dsFill.Tables(0)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployee()
        Try
            Dim objEmployee As New clsEmployee_Master

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : CHECK FOR ACTIVE EMPLOYEE
            'Dim dsEmployee As DataSet = objEmployee.GetEmployeeList("Employee", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim dsEmployee As DataSet = objEmployee.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim dsEmployee As DataSet
            'If menAction = enAction.EDIT_ONE Then
            '    dsEmployee = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsEmployee = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'End If
            dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            'Pinkal (24-Jun-2011) -- End

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsEmployee.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub FillMedicalPeriod()
        Try
            Dim objPeriod As New clscommom_period_Tran
            Dim dsPeriod As DataSet = Nothing
            If CInt(cboMedicalYear.SelectedValue) > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsPeriod = objPeriod.getListForCombo(CInt(enModuleReference.Medical), CInt(cboMedicalYear.SelectedValue), "Period", False)
                dsPeriod = objPeriod.getListForCombo(CInt(enModuleReference.Medical), CInt(cboMedicalYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsPeriod = objPeriod.getListForCombo(CInt(enModuleReference.Medical), CInt(cboMedicalYear.SelectedValue), "Period", True)
                dsPeriod = objPeriod.getListForCombo(CInt(enModuleReference.Medical), CInt(cboMedicalYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
                'Sohail (21 Aug 2015) -- End
            End If
            cboMedicalPeriod.ValueMember = "periodunkid"
            cboMedicalPeriod.DisplayMember = "name"
            cboMedicalPeriod.DataSource = dsPeriod.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMedicalPeriod", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try

            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes
            'objbtnAddEmployee.Enabled = User._Object.Privilege._AddEmployee
            'Pinkal (07-Jan-2012) -- End
            objbtnAddService.Enabled = User._Object.Privilege._AddMedicalMasters
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub


#End Region


    'Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
    '    Dim dsFill As DataSet = Nothing
    '    Try
    '        'FOR MEMBERSHIP
    '        Dim objEmployee As New clsEmployee_Master
    '        dsFill = objEmployee.GetEmployee_Membership(CInt(cboEmployee.SelectedValue), True, "Membership")
    '        cboMembership.ValueMember = "Mem_Id"
    '        cboMembership.DisplayMember = "Mem_Name"
    '        cboMembership.DataSource = dsFill.Tables("Membership")
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub cboMemership_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMembership.SelectedIndexChanged
    '    Dim dsFill As DataSet = Nothing
    '    Dim dtFill As DataTable = Nothing
    '    Try
    '        'FOR MEMBERSHIP
    '        Dim objEmployee As New clsEmployee_Master
    '        dsFill = objEmployee.GetEmployee_Membership(CInt(cboEmployee.SelectedValue), True, "Membership")
    '        dtFill = New DataView(dsFill.Tables("Membership"), "Mem_Id=" & CInt(cboMembership.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
    '        If dtFill.Rows.Count > 0 Then
    '            txtMembershipNo.Text = dtFill.Rows(0)("Mem_No").ToString
    '        Else
    '            txtMembershipNo.Text = ""
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboMemership_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

    
    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbMedicalInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMedicalInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbMedicalInfo.Text = Language._Object.getCaption(Me.gbMedicalInfo.Name, Me.gbMedicalInfo.Text)
			Me.lblMedicalYear.Text = Language._Object.getCaption(Me.lblMedicalYear.Name, Me.lblMedicalYear.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblService.Text = Language._Object.getCaption(Me.lblService.Name, Me.lblService.Text)
			Me.lblServiceNo.Text = Language._Object.getCaption(Me.lblServiceNo.Name, Me.lblServiceNo.Text)
			Me.lblClaimDate.Text = Language._Object.getCaption(Me.lblClaimDate.Name, Me.lblClaimDate.Text)
			Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.Name, Me.lblClaimNo.Text)
			Me.lblNatureOfIllness.Text = Language._Object.getCaption(Me.lblNatureOfIllness.Name, Me.lblNatureOfIllness.Text)
			Me.lblDoctors.Text = Language._Object.getCaption(Me.lblDoctors.Name, Me.lblDoctors.Text)
			Me.lblMedicalPeriod.Text = Language._Object.getCaption(Me.lblMedicalPeriod.Name, Me.lblMedicalPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Medical Year is compulsory information.Please Select Medical Year.")
			Language.setMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee.")
			Language.setMessage(mstrModuleName, 3, "Claim No cannot be blank. Claim No is required information.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class