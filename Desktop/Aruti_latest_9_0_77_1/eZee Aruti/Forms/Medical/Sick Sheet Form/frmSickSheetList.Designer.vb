﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSickSheetList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSickSheetList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objChkAll = New System.Windows.Forms.CheckBox
        Me.lvSickSheetList = New eZee.Common.eZeeListView(Me.components)
        Me.objColhSelect = New System.Windows.Forms.ColumnHeader
        Me.colhSickSheetNo = New System.Windows.Forms.ColumnHeader
        Me.colhSicksheetDate = New System.Windows.Forms.ColumnHeader
        Me.colhProvider = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.colhMedicalCover = New System.Windows.Forms.ColumnHeader
        Me.colhJob = New System.Windows.Forms.ColumnHeader
        Me.objColhEmployeeunkid = New System.Windows.Forms.ColumnHeader
        Me.objColhEmployeecode = New System.Windows.Forms.ColumnHeader
        Me.objEFooter = New eZee.Common.eZeeFooter
        Me.btnEClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEmailOk = New eZee.Common.eZeeLightButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuSickFormOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuPreviewSickSheet = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrintSickSheet = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.LblToSickSheetDate = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.LblSickSheetDate = New System.Windows.Forms.Label
        Me.objbtnSearchProvider = New eZee.Common.eZeeGradientButton
        Me.txtSickSheetNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblSickSheetNo = New System.Windows.Forms.Label
        Me.cboMedicalCover = New System.Windows.Forms.ComboBox
        Me.cboJob = New System.Windows.Forms.ComboBox
        Me.lblMedicalcover = New System.Windows.Forms.Label
        Me.lblJob = New System.Windows.Forms.Label
        Me.cboProvider = New System.Windows.Forms.ComboBox
        Me.EZeeStraightLine2 = New eZee.Common.eZeeStraightLine
        Me.lblProvider = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.pnlMain.SuspendLayout()
        Me.objEFooter.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.cmnuSickFormOperation.SuspendLayout()
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objChkAll)
        Me.pnlMain.Controls.Add(Me.lvSickSheetList)
        Me.pnlMain.Controls.Add(Me.objEFooter)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.pnlMain.Controls.Add(Me.eZeeHeader)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(661, 510)
        Me.pnlMain.TabIndex = 0
        '
        'objChkAll
        '
        Me.objChkAll.AutoSize = True
        Me.objChkAll.Location = New System.Drawing.Point(18, 195)
        Me.objChkAll.Name = "objChkAll"
        Me.objChkAll.Size = New System.Drawing.Size(15, 14)
        Me.objChkAll.TabIndex = 88
        Me.objChkAll.UseVisualStyleBackColor = True
        '
        'lvSickSheetList
        '
        Me.lvSickSheetList.BackColorOnChecked = True
        Me.lvSickSheetList.CheckBoxes = True
        Me.lvSickSheetList.ColumnHeaders = Nothing
        Me.lvSickSheetList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhSelect, Me.colhSickSheetNo, Me.colhSicksheetDate, Me.colhProvider, Me.colhEmployee, Me.colhMedicalCover, Me.colhJob, Me.objColhEmployeeunkid, Me.objColhEmployeecode})
        Me.lvSickSheetList.CompulsoryColumns = ""
        Me.lvSickSheetList.FullRowSelect = True
        Me.lvSickSheetList.GridLines = True
        Me.lvSickSheetList.GroupingColumn = Nothing
        Me.lvSickSheetList.HideSelection = False
        Me.lvSickSheetList.Location = New System.Drawing.Point(10, 191)
        Me.lvSickSheetList.MinColumnWidth = 50
        Me.lvSickSheetList.MultiSelect = False
        Me.lvSickSheetList.Name = "lvSickSheetList"
        Me.lvSickSheetList.OptionalColumns = ""
        Me.lvSickSheetList.ShowMoreItem = False
        Me.lvSickSheetList.ShowSaveItem = False
        Me.lvSickSheetList.ShowSelectAll = True
        Me.lvSickSheetList.ShowSizeAllColumnsToFit = True
        Me.lvSickSheetList.Size = New System.Drawing.Size(637, 260)
        Me.lvSickSheetList.Sortable = True
        Me.lvSickSheetList.TabIndex = 7
        Me.lvSickSheetList.UseCompatibleStateImageBehavior = False
        Me.lvSickSheetList.View = System.Windows.Forms.View.Details
        '
        'objColhSelect
        '
        Me.objColhSelect.Tag = "objColhSelect"
        Me.objColhSelect.Text = ""
        Me.objColhSelect.Width = 25
        '
        'colhSickSheetNo
        '
        Me.colhSickSheetNo.Tag = "colhSickSheetNo"
        Me.colhSickSheetNo.Text = "Sick Sheet No"
        Me.colhSickSheetNo.Width = 80
        '
        'colhSicksheetDate
        '
        Me.colhSicksheetDate.Tag = "colhSicksheetDate"
        Me.colhSicksheetDate.Text = "Sick Sheet Date"
        Me.colhSicksheetDate.Width = 110
        '
        'colhProvider
        '
        Me.colhProvider.Tag = "colhProvider"
        Me.colhProvider.Text = "Provider"
        Me.colhProvider.Width = 120
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 150
        '
        'colhMedicalCover
        '
        Me.colhMedicalCover.DisplayIndex = 6
        Me.colhMedicalCover.Tag = "colhMedicalCover"
        Me.colhMedicalCover.Text = "Medical cover"
        Me.colhMedicalCover.Width = 145
        '
        'colhJob
        '
        Me.colhJob.DisplayIndex = 5
        Me.colhJob.Tag = "colhJob"
        Me.colhJob.Text = "Job"
        Me.colhJob.Width = 0
        '
        'objColhEmployeeunkid
        '
        Me.objColhEmployeeunkid.Tag = "objColhEmployeeunkid"
        Me.objColhEmployeeunkid.Text = "Employeeunkid"
        Me.objColhEmployeeunkid.Width = 0
        '
        'objColhEmployeecode
        '
        Me.objColhEmployeecode.Tag = "objColhEmployeecode"
        Me.objColhEmployeecode.Text = "Employeecode"
        Me.objColhEmployeecode.Width = 0
        '
        'objEFooter
        '
        Me.objEFooter.BorderColor = System.Drawing.Color.Silver
        Me.objEFooter.Controls.Add(Me.btnEClose)
        Me.objEFooter.Controls.Add(Me.btnEmailOk)
        Me.objEFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objEFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objEFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objEFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objEFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objEFooter.Location = New System.Drawing.Point(0, 400)
        Me.objEFooter.Name = "objEFooter"
        Me.objEFooter.Size = New System.Drawing.Size(661, 55)
        Me.objEFooter.TabIndex = 89
        '
        'btnEClose
        '
        Me.btnEClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEClose.BackColor = System.Drawing.Color.White
        Me.btnEClose.BackgroundImage = CType(resources.GetObject("btnEClose.BackgroundImage"), System.Drawing.Image)
        Me.btnEClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEClose.BorderColor = System.Drawing.Color.Empty
        Me.btnEClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEClose.FlatAppearance.BorderSize = 0
        Me.btnEClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEClose.ForeColor = System.Drawing.Color.Black
        Me.btnEClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Location = New System.Drawing.Point(557, 14)
        Me.btnEClose.Name = "btnEClose"
        Me.btnEClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEClose.Size = New System.Drawing.Size(90, 30)
        Me.btnEClose.TabIndex = 79
        Me.btnEClose.Text = "Close"
        Me.btnEClose.UseVisualStyleBackColor = True
        '
        'btnEmailOk
        '
        Me.btnEmailOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEmailOk.BackColor = System.Drawing.Color.White
        Me.btnEmailOk.BackgroundImage = CType(resources.GetObject("btnEmailOk.BackgroundImage"), System.Drawing.Image)
        Me.btnEmailOk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmailOk.BorderColor = System.Drawing.Color.Empty
        Me.btnEmailOk.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEmailOk.FlatAppearance.BorderSize = 0
        Me.btnEmailOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmailOk.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmailOk.ForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmailOk.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailOk.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.Location = New System.Drawing.Point(458, 14)
        Me.btnEmailOk.Name = "btnEmailOk"
        Me.btnEmailOk.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailOk.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailOk.Size = New System.Drawing.Size(90, 30)
        Me.btnEmailOk.TabIndex = 78
        Me.btnEmailOk.Text = "&Ok"
        Me.btnEmailOk.UseVisualStyleBackColor = True
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 455)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(661, 55)
        Me.objFooter.TabIndex = 6
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.ContextMenuStrip = Me.cmnuSickFormOperation
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(10, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(90, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuSickFormOperation
        Me.btnOperation.TabIndex = 77
        Me.btnOperation.Text = "Operation"
        '
        'cmnuSickFormOperation
        '
        Me.cmnuSickFormOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPreviewSickSheet, Me.mnuPrintSickSheet})
        Me.cmnuSickFormOperation.Name = "cmnuSickFormOperation"
        Me.cmnuSickFormOperation.Size = New System.Drawing.Size(172, 48)
        '
        'mnuPreviewSickSheet
        '
        Me.mnuPreviewSickSheet.Name = "mnuPreviewSickSheet"
        Me.mnuPreviewSickSheet.Size = New System.Drawing.Size(171, 22)
        Me.mnuPreviewSickSheet.Text = "Pre&view Sick Sheet"
        '
        'mnuPrintSickSheet
        '
        Me.mnuPrintSickSheet.Name = "mnuPrintSickSheet"
        Me.mnuPrintSickSheet.Size = New System.Drawing.Size(171, 22)
        Me.mnuPrintSickSheet.Text = "&Print Sick Sheet"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(458, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(90, 30)
        Me.btnDelete.TabIndex = 76
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(361, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(90, 30)
        Me.btnEdit.TabIndex = 75
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(265, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(90, 30)
        Me.btnNew.TabIndex = 74
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(554, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 73
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.LblToSickSheetDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.dtpToDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.dtpFromDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.LblSickSheetDate)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchProvider)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.txtSickSheetNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblSickSheetNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboMedicalCover)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboJob)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblMedicalcover)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblJob)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboProvider)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.EZeeStraightLine2)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblProvider)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnReset)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearch)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmployee)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(10, 66)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 91
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(637, 116)
        Me.EZeeCollapsibleContainer1.TabIndex = 5
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Filter Criteria"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblToSickSheetDate
        '
        Me.LblToSickSheetDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblToSickSheetDate.Location = New System.Drawing.Point(208, 63)
        Me.LblToSickSheetDate.Name = "LblToSickSheetDate"
        Me.LblToSickSheetDate.Size = New System.Drawing.Size(20, 15)
        Me.LblToSickSheetDate.TabIndex = 216
        Me.LblToSickSheetDate.Text = "To"
        '
        'dtpToDate
        '
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(233, 60)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.ShowCheckBox = True
        Me.dtpToDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpToDate.TabIndex = 215
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(97, 60)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.ShowCheckBox = True
        Me.dtpFromDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpFromDate.TabIndex = 214
        '
        'LblSickSheetDate
        '
        Me.LblSickSheetDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSickSheetDate.Location = New System.Drawing.Point(6, 63)
        Me.LblSickSheetDate.Name = "LblSickSheetDate"
        Me.LblSickSheetDate.Size = New System.Drawing.Size(83, 15)
        Me.LblSickSheetDate.TabIndex = 213
        Me.LblSickSheetDate.Text = "Sick Sheet Date"
        '
        'objbtnSearchProvider
        '
        Me.objbtnSearchProvider.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchProvider.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchProvider.BorderSelected = False
        Me.objbtnSearchProvider.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchProvider.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchProvider.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchProvider.Location = New System.Drawing.Point(611, 33)
        Me.objbtnSearchProvider.Name = "objbtnSearchProvider"
        Me.objbtnSearchProvider.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchProvider.TabIndex = 211
        '
        'txtSickSheetNo
        '
        Me.txtSickSheetNo.Flags = 0
        Me.txtSickSheetNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSickSheetNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSickSheetNo.Location = New System.Drawing.Point(97, 33)
        Me.txtSickSheetNo.Name = "txtSickSheetNo"
        Me.txtSickSheetNo.Size = New System.Drawing.Size(240, 21)
        Me.txtSickSheetNo.TabIndex = 209
        '
        'lblSickSheetNo
        '
        Me.lblSickSheetNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSickSheetNo.Location = New System.Drawing.Point(6, 36)
        Me.lblSickSheetNo.Name = "lblSickSheetNo"
        Me.lblSickSheetNo.Size = New System.Drawing.Size(83, 15)
        Me.lblSickSheetNo.TabIndex = 208
        Me.lblSickSheetNo.Text = "Sick Sheet No"
        '
        'cboMedicalCover
        '
        Me.cboMedicalCover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMedicalCover.DropDownWidth = 150
        Me.cboMedicalCover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMedicalCover.FormattingEnabled = True
        Me.cboMedicalCover.Location = New System.Drawing.Point(435, 87)
        Me.cboMedicalCover.Name = "cboMedicalCover"
        Me.cboMedicalCover.Size = New System.Drawing.Size(172, 21)
        Me.cboMedicalCover.TabIndex = 206
        '
        'cboJob
        '
        Me.cboJob.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboJob.DropDownWidth = 150
        Me.cboJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboJob.FormattingEnabled = True
        Me.cboJob.Location = New System.Drawing.Point(435, 60)
        Me.cboJob.Name = "cboJob"
        Me.cboJob.Size = New System.Drawing.Size(172, 21)
        Me.cboJob.TabIndex = 205
        '
        'lblMedicalcover
        '
        Me.lblMedicalcover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMedicalcover.Location = New System.Drawing.Point(352, 90)
        Me.lblMedicalcover.Name = "lblMedicalcover"
        Me.lblMedicalcover.Size = New System.Drawing.Size(77, 15)
        Me.lblMedicalcover.TabIndex = 201
        Me.lblMedicalcover.Text = "Medical Cover"
        '
        'lblJob
        '
        Me.lblJob.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJob.Location = New System.Drawing.Point(352, 63)
        Me.lblJob.Name = "lblJob"
        Me.lblJob.Size = New System.Drawing.Size(77, 15)
        Me.lblJob.TabIndex = 200
        Me.lblJob.Text = "Job"
        '
        'cboProvider
        '
        Me.cboProvider.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProvider.DropDownWidth = 150
        Me.cboProvider.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboProvider.FormattingEnabled = True
        Me.cboProvider.Location = New System.Drawing.Point(435, 33)
        Me.cboProvider.Name = "cboProvider"
        Me.cboProvider.Size = New System.Drawing.Size(172, 21)
        Me.cboProvider.TabIndex = 2
        '
        'EZeeStraightLine2
        '
        Me.EZeeStraightLine2.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine2.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.EZeeStraightLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine2.Location = New System.Drawing.Point(345, 24)
        Me.EZeeStraightLine2.Name = "EZeeStraightLine2"
        Me.EZeeStraightLine2.Size = New System.Drawing.Size(4, 89)
        Me.EZeeStraightLine2.TabIndex = 88
        Me.EZeeStraightLine2.Text = "EZeeStraightLine2"
        '
        'lblProvider
        '
        Me.lblProvider.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProvider.Location = New System.Drawing.Point(352, 36)
        Me.lblProvider.Name = "lblProvider"
        Me.lblProvider.Size = New System.Drawing.Size(77, 15)
        Me.lblProvider.TabIndex = 88
        Me.lblProvider.Text = "Provider"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(610, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 67
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(321, 88)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 86
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(587, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 66
        Me.objbtnSearch.TabStop = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 150
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(97, 87)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(221, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(5, 90)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(83, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(661, 60)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Sick Sheet List"
        '
        'frmSickSheetList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(661, 510)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSickSheetList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Sick Sheet List"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.objEFooter.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.cmnuSickFormOperation.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.EZeeCollapsibleContainer1.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblMedicalcover As System.Windows.Forms.Label
    Friend WithEvents lblJob As System.Windows.Forms.Label
    Friend WithEvents cboProvider As System.Windows.Forms.ComboBox
    Friend WithEvents EZeeStraightLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents lblProvider As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents cboMedicalCover As System.Windows.Forms.ComboBox
    Friend WithEvents cboJob As System.Windows.Forms.ComboBox
    Friend WithEvents lvSickSheetList As eZee.Common.eZeeListView
    Friend WithEvents colhSickSheetNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhProvider As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhJob As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMedicalCover As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuSickFormOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuPrintSickSheet As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblSickSheetNo As System.Windows.Forms.Label
    Friend WithEvents objColhEmployeeunkid As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhEmployeecode As System.Windows.Forms.ColumnHeader
    Friend WithEvents objColhSelect As System.Windows.Forms.ColumnHeader
    Friend WithEvents objChkAll As System.Windows.Forms.CheckBox
    Friend WithEvents objEFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnEClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmailOk As eZee.Common.eZeeLightButton
    Friend WithEvents colhSicksheetDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objbtnSearchProvider As eZee.Common.eZeeGradientButton
    Friend WithEvents LblSickSheetDate As System.Windows.Forms.Label
    Friend WithEvents LblToSickSheetDate As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtSickSheetNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents mnuPreviewSickSheet As System.Windows.Forms.ToolStripMenuItem
End Class
