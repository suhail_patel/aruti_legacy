﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmSickSheetList

#Region " Private Variable(s) "

    Private objSickSheet As clsmedical_sicksheet
    Private ReadOnly mstrModuleName As String = "frmSickSheetList"


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Private mblnIsFromMail As Boolean = False
    Private mblnExport_Print As Boolean = False
    Private mstrSickSheetID As String = ""
    Private mstrEmployeeUnkids As String = ""
    Private dsEmailListData As New DataSet

    'Pinkal (20-Jan-2012) -- End

#End Region

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

#Region "Property"

    Public Property _EmployeeUnkids() As String
        Get
            Return mstrEmployeeUnkids
        End Get
        Set(ByVal value As String)
            mstrEmployeeUnkids = value
        End Set
    End Property

    Public WriteOnly Property _IsFromMail() As Boolean
        Set(ByVal value As Boolean)
            mblnIsFromMail = value
        End Set
    End Property

    Public WriteOnly Property IsExport_Print() As Boolean
        Set(ByVal value As Boolean)
            mblnExport_Print = value
        End Set
    End Property

    Public ReadOnly Property SickSheetIDs() As String
        Get
            Return mstrSickSheetID
        End Get
    End Property

    Public Property _Email_Data() As DataSet
        Get
            Return dsEmailListData
        End Get
        Set(ByVal value As DataSet)
            dsEmailListData = value
        End Set
    End Property

#End Region

    'Pinkal (20-Jan-2012) -- End

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            Dim objEmployee As New clsEmployee_Master

            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'dsFill = objEmployee.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)


            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim strUACF As String = "TRUE"
            If User._Object.Privilege._RevokeUserAccessOnSicksheet = True Then
                strUACF = "FALSE"
            End If


            'If User._Object.Privilege._RevokeUserAccessOnSicksheet = True Then
            '    If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '        dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , " AND 1=1")
            '    Else
            '        dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime, , , " AND 1=1 ")
            '    End If
            'Else

            '    If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '        dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            '    Else
            '        dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            '    End If
            'End If

            dsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True, , , , , , , , , , , , , , , , , CBool(strUACF))
            'S.SANDEEP [04 JUN 2015] -- END

            'Pinkal (12-Sep-2014) -- End

            'Anjan (17 Apr 2012)-End 


            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsFill.Tables(0)

            dsFill = Nothing
            'Pinkal (19-Nov-2012) -- Start
            'Enhancement : TRA Changes

            'Dim objprovider As New clsinstitute_master
            'dsFill = objprovider.getListForCombo(True, "Provider", True, 1, True)

            Dim objUserMapping As New clsprovider_mapping
            dsFill = objUserMapping.GetUserProvider("Provider", , User._Object._Userunkid, True)

            Dim drRow As DataRow = dsFill.Tables(0).NewRow
            drRow("providerunkid") = 0
            drRow("name") = Language.getMessage(mstrModuleName, 5, "Select")
            dsFill.Tables(0).Rows.InsertAt(drRow, 0)

            'Pinkal (19-Nov-2012) -- End


            cboProvider.DisplayMember = "name"
            cboProvider.ValueMember = "providerunkid"
            cboProvider.DataSource = dsFill.Tables("Provider")

            dsFill = Nothing
            Dim objjob As New clsJobs
            dsFill = objjob.getComboList("Job", True)
            cboJob.DataSource = dsFill.Tables("Job")
            cboJob.DisplayMember = "name"
            cboJob.ValueMember = "jobunkid"

            dsFill = Nothing
            Dim objMaster As New clsmedical_master
            dsFill = objMaster.getListForCombo(enMedicalMasterType.Cover, "Cover", True)
            cboMedicalCover.DisplayMember = "name"
            cboMedicalCover.ValueMember = "mdmasterunkid"
            cboMedicalCover.DataSource = dsFill.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim strSearching As String = ""
        Dim dtList As New DataTable
        Try

            If User._Object.Privilege._AllowToViewEmpSickSheetList = True Then   'Pinkal (02-Jul-2012) -- Start


                'Pinkal (12-Sep-2014) -- Start
                'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
                objSickSheet._RevokeUserAccessOnSickSheet = User._Object.Privilege._RevokeUserAccessOnSicksheet
                'Pinkal (12-Sep-2014) -- End


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objSickSheet.GetList("SickSheet")
                dsList = objSickSheet.GetList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                              "SickSheet")
                'Shani(24-Aug-2015) -- End


                If txtSickSheetNo.Text.Trim.Length > 0 Then
                    strSearching = "AND sicksheetNo like '%" & txtSickSheetNo.Text.Trim & "%'"
                End If

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    strSearching &= "AND employeeunkid =" & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboProvider.SelectedValue) > 0 Then
                    strSearching &= "AND instituteunkid =" & CInt(cboProvider.SelectedValue) & " "
                End If

                If CInt(cboJob.SelectedValue) > 0 Then
                    strSearching &= "AND jobunkid =" & CInt(cboJob.SelectedValue) & " "
                End If

                If CInt(cboMedicalCover.SelectedValue) > 0 Then
                    strSearching &= "AND medicalcoverunkid =" & CInt(cboMedicalCover.SelectedValue) & " "
                End If


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes

                If dtpFromDate.Checked Then
                    strSearching &= "AND sicksheetdate >= '" & eZeeDate.convertDate(dtpFromDate.Value.Date) & "' "
                End If

                If dtpToDate.Checked Then
                    strSearching &= "AND sicksheetdate <= '" & eZeeDate.convertDate(dtpToDate.Value.Date) & "' "
                End If

                'Pinkal (18-Dec-2012) -- End



                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtList = New DataView(dsList.Tables("SickSheet"), strSearching, "sicksheetdate desc ", DataViewRowState.CurrentRows).ToTable
                Else
                    dtList = dsList.Tables("SickSheet")
                End If


                Dim lvItem As ListViewItem


                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                RemoveHandler lvSickSheetList.ItemChecked, AddressOf lvSickSheetList_ItemChecked
                'Pinkal (21-Jul-2014) -- End


                lvSickSheetList.Items.Clear()
                For Each drRow As DataRow In dtList.Rows
                    lvItem = New ListViewItem
                    lvItem.Tag = drRow("sicksheetunkid")

                    'Pinkal (20-Jan-2012) -- Start
                    'Enhancement : TRA Changes
                    'lvItem.Text = drRow("sicksheetno").ToString

                    lvItem.Text = ""
                    lvItem.SubItems.Add(drRow("sicksheetno").ToString)
                    'Pinkal (20-Jan-2012) -- End


                    'Pinkal (18-Dec-2012) -- Start
                    'Enhancement : TRA Changes
                    lvItem.SubItems.Add(eZeeDate.convertDate(drRow("sicksheetdate").ToString()).ToShortDateString)
                    'Pinkal (18-Dec-2012) -- End


                    lvItem.SubItems.Add(drRow("institute_name").ToString)
                    lvItem.SubItems.Add(drRow("employeename").ToString)

                    lvItem.SubItems.Add(drRow("cover").ToString)




                    'Pinkal (18-Jul-2012) -- Start
                    'Enhancement : TRA Changes
                    lvItem.SubItems.Add(drRow("job").ToString)
                    'Pinkal (18-Jul-2012) -- End

                    'Pinkal (07-Jan-2012) -- Start
                    'Enhancement : TRA Changes
                    lvItem.SubItems.Add(drRow("employeeunkid").ToString)
                    lvItem.SubItems.Add(drRow("employeecode").ToString)
                    'Pinkal (07-Jan-2012) -- End

                    lvSickSheetList.Items.Add(lvItem)
                Next



                'Pinkal (18-Jul-2012) -- Start
                'Enhancement : TRA Changes

                'If lvSickSheetList.Items.Count > 16 Then
                '    colhMedicalCover.Width = 110 - 18
                'Else
                '    colhMedicalCover.Width = 110
                'End If

                If lvSickSheetList.Items.Count > 16 Then
                    colhMedicalCover.Width = 145 - 18
                Else
                    colhMedicalCover.Width = 145
                End If

                'Pinkal (18-Jul-2012) -- End

                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                AddHandler lvSickSheetList.ItemChecked, AddressOf lvSickSheetList_ItemChecked
                'Pinkal (21-Jul-2014) -- End

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            dtList.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddMedicalSickSheet
            btnEdit.Enabled = User._Object.Privilege._AllowToEditMedicalSickSheet
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteMedicalSickSheet
            mnuPrintSickSheet.Enabled = User._Object.Privilege._AllowToPrintMedicalSickSheet
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 16 MAY 2012 ] -- END

#End Region

#Region " Form's Event(s) "

    Private Sub frmSickSheetList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objSickSheet = New clsmedical_sicksheet
        Try
            Call Set_Logo(Me, gApplicationType)
            FillCombo()
            If mblnIsFromMail = True Then
                lvSickSheetList.CheckBoxes = True
                objColhSelect.Width = 25
                objChkAll.Visible = True
            Else
                objChkAll.Visible = False
                objColhSelect.Width = 0
                colhSickSheetNo.Width = colhSickSheetNo.Width + 25
            End If

            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 16 MAY 2012 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSickSheetList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSickSheetList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvSickSheetList.Focused = True Then
                btnDelete_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSickSheetList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmSickSheetList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmSickSheetList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalFormList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objSickSheet = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsmedical_sicksheet.SetMessages()
            ArutiReports.clsSickSheetReport.SetMessages()
            objfrm._Other_ModuleNames = "clsmedical_sicksheet,clsSickSheetReport"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmSicksheet_AddEdit
            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvSickSheetList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Sick Sheet from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvSickSheetList.Select()
            Exit Sub
        End If
        Dim objfrmsickSheet As New frmSicksheet_AddEdit
        Try


            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

            If objSickSheet.isUsed(CInt(lvSickSheetList.SelectedItems(0).Tag)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry,You cannot Edit/Delete this sicksheet.Reason:It is used in Medical claim."), enMsgBoxStyle.Information) '?1
                lvSickSheetList.Select()
                Exit Sub
            End If

            'Pinkal (12-Sep-2014) -- End

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvSickSheetList.SelectedItems(0).Index
            If objfrmsickSheet.displayDialog(CInt(lvSickSheetList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call FillList()
            End If
            objfrmsickSheet = Nothing
            If lvSickSheetList.Items.Count > 0 Then
                lvSickSheetList.Items(intSelectedIndex).Selected = True
                lvSickSheetList.EnsureVisible(intSelectedIndex)
            End If
            lvSickSheetList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmsickSheet IsNot Nothing Then objfrmsickSheet.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvSickSheetList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Sick Sheet from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvSickSheetList.Select()
            Exit Sub
        End If
        Try

            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

            If objSickSheet.isUsed(CInt(lvSickSheetList.SelectedItems(0).Tag)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry,You cannot Edit/Delete this sicksheet.Reason:It is used in Medical claim."), enMsgBoxStyle.Information) '?1
                lvSickSheetList.Select()
                Exit Sub
            End If

            'Pinkal (12-Sep-2014) -- End

            Dim intSelectedIndex As Integer
            intSelectedIndex = lvSickSheetList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Sick Sheet ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.MEDICAL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objSickSheet._Voidreason = mstrVoidReason
                End If
                frm = Nothing

                objSickSheet._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objSickSheet._Voiduserunkid = User._Object._Userunkid

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objSickSheet._FormName = mstrModuleName
                objSickSheet._LoginEmployeeunkid = 0
                objSickSheet._ClientIP = getIP()
                objSickSheet._HostName = getHostName()
                objSickSheet._FromWeb = False
                objSickSheet._AuditUserId = User._Object._Userunkid
objSickSheet._CompanyUnkid = Company._Object._Companyunkid
                objSickSheet._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objSickSheet.Delete(CInt(lvSickSheetList.SelectedItems(0).Tag))
                lvSickSheetList.SelectedItems(0).Remove()

                If lvSickSheetList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvSickSheetList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvSickSheetList.Items.Count - 1
                    lvSickSheetList.Items(intSelectedIndex).Selected = True
                    lvSickSheetList.EnsureVisible(intSelectedIndex)
                ElseIf lvSickSheetList.Items.Count <> 0 Then
                    lvSickSheetList.Items(intSelectedIndex).Selected = True
                    lvSickSheetList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvSickSheetList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim frm As New frmCommonSearch
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.ValueMember = cboEmployee.ValueMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog() Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            txtSickSheetNo.Text = ""
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedIndex = 0
            If cboProvider.Items.Count > 0 Then cboProvider.SelectedIndex = 0
            If cboMedicalCover.Items.Count > 0 Then cboMedicalCover.SelectedIndex = 0
            If cboJob.Items.Count > 0 Then cboJob.SelectedIndex = 0

            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            dtpFromDate.Checked = True
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Checked = True
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            'Pinkal (18-Dec-2012) -- End


            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub btnEmailOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmailOk.Click
        Dim objFields As New clsLetterFields
        Dim blnFlag As Boolean = False

        If lvSickSheetList.CheckedItems.Count <= 0 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check atleast one Sick Sheet."), enMsgBoxStyle.Information)
            Exit Sub
        End If

        Try
            For Each lvItem As ListViewItem In lvSickSheetList.CheckedItems
                If mstrSickSheetID.Contains(lvItem.Tag.ToString) = False Then
                    mstrSickSheetID &= "," & lvItem.Tag.ToString
                End If
            Next

            If mstrSickSheetID.Length > 0 Then
                mstrSickSheetID = Mid(mstrSickSheetID, 2)

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsEmailListData = objFields.GetEmployeeData(mstrSickSheetID, enImg_Email_RefId.Medical_Module, "DataTable")
                dsEmailListData = objFields.GetEmployeeData(mstrSickSheetID, enImg_Email_RefId.Medical_Module, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, "DataTable")
                'S.SANDEEP [04 JUN 2015] -- END

                If dsEmailListData.Tables("DataTable").Rows.Count > 0 Then
                    Dim dtRow() As DataRow = dsEmailListData.Tables("DataTable").Select("Email = ''")
                    If dtRow.Length > 0 Then
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Some of the employees email addresses are blank. Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                            Exit Sub
                        Else
                            For i As Integer = 0 To dtRow.Length - 1
                                dsEmailListData.Tables("DataTable").Rows.Remove(dtRow(i))
                            Next
                        End If
                    End If
                End If
            End If
            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEmailOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEClose_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- End


    'Pinkal (18-Jul-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchProvider_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchProvider.Click
        Try
            Dim frm As New frmCommonSearch
            frm.DataSource = CType(cboProvider.DataSource, DataTable)
            frm.DisplayMember = cboProvider.DisplayMember
            frm.ValueMember = cboProvider.ValueMember
            If frm.DisplayDialog() Then
                cboProvider.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchProvider_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (18-Jul-2012) -- End


#End Region

#Region "ContextMenu Event(s)"

    Private Sub mnuPrintSickSheet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrintSickSheet.Click
        Try
            If lvSickSheetList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Sick Sheet from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvSickSheetList.Select()
                Exit Sub
            End If

            Dim objSickSheetRpt As New ArutiReports.clsSickSheetReport
            objSickSheetRpt._SickSheetunkid = lvSickSheetList.SelectedItems(0).Tag.ToString()
            objSickSheetRpt._Employeeunkid = lvSickSheetList.SelectedItems(0).SubItems(objColhEmployeeunkid.Index).Text.Trim()
            objSickSheetRpt._SicksheetDate = CDate(lvSickSheetList.SelectedItems(0).SubItems(colhSicksheetDate.Index).Text.Trim()).Date
            objSickSheetRpt._SicksheetTemplate = ConfigParameter._Object._SickSheetTemplate

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            'objSickSheetRpt.generateReport(enPrintAction.Print)
            objSickSheetRpt.generateReport(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), enPrintAction.Print)
            'Pinkal (16-Apr-2016) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPrintSickSheet_Click", mstrModuleName)
        End Try
    End Sub




    Private Sub mnuPreviewSickSheet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPreviewSickSheet.Click
        Try
            If lvSickSheetList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Sick Sheet from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvSickSheetList.Select()
                Exit Sub
            End If

            Dim objSickSheetRpt As New ArutiReports.clsSickSheetReport
            objSickSheetRpt._SickSheetunkid = lvSickSheetList.SelectedItems(0).Tag.ToString()
            objSickSheetRpt._Employeeunkid = lvSickSheetList.SelectedItems(0).SubItems(objColhEmployeeunkid.Index).Text.Trim()
            objSickSheetRpt._SicksheetDate = CDate(lvSickSheetList.SelectedItems(0).SubItems(colhSicksheetDate.Index).Text.Trim()).Date
            objSickSheetRpt._SicksheetTemplate = ConfigParameter._Object._SickSheetTemplate

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            'objSickSheetRpt.generateReport(enPrintAction.Preview)
            objSickSheetRpt.generateReport(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), enPrintAction.Preview)
            'Pinkal (16-Apr-2016) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuPreviewSickSheet_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (05-Jan-2013) -- End


#End Region


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

#Region "CheckBox Event"

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            RemoveHandler lvSickSheetList.ItemChecked, AddressOf lvSickSheetList_ItemChecked
            For Each lvItem As ListViewItem In lvSickSheetList.Items
                lvItem.Checked = CBool(objChkAll.CheckState)
            Next
            AddHandler lvSickSheetList.ItemChecked, AddressOf lvSickSheetList_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvSickSheetList_ItemChecked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvSickSheetList.ItemChecked
        Try
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            If lvSickSheetList.CheckedItems.Count <= 0 Then
                objChkAll.CheckState = CheckState.Unchecked
            ElseIf lvSickSheetList.CheckedItems.Count < lvSickSheetList.Items.Count Then
                objChkAll.CheckState = CheckState.Indeterminate
            ElseIf lvSickSheetList.CheckedItems.Count = lvSickSheetList.Items.Count Then
                objChkAll.CheckState = CheckState.Checked
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvSickSheetList_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (20-Jan-2012) -- End




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor

            Me.btnEClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnEClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEmailOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnEmailOk.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.lblMedicalcover.Text = Language._Object.getCaption(Me.lblMedicalcover.Name, Me.lblMedicalcover.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblProvider.Text = Language._Object.getCaption(Me.lblProvider.Name, Me.lblProvider.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.colhSickSheetNo.Text = Language._Object.getCaption(CStr(Me.colhSickSheetNo.Tag), Me.colhSickSheetNo.Text)
            Me.colhProvider.Text = Language._Object.getCaption(CStr(Me.colhProvider.Tag), Me.colhProvider.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhJob.Text = Language._Object.getCaption(CStr(Me.colhJob.Tag), Me.colhJob.Text)
            Me.colhMedicalCover.Text = Language._Object.getCaption(CStr(Me.colhMedicalCover.Tag), Me.colhMedicalCover.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuPrintSickSheet.Text = Language._Object.getCaption(Me.mnuPrintSickSheet.Name, Me.mnuPrintSickSheet.Text)
            Me.lblSickSheetNo.Text = Language._Object.getCaption(Me.lblSickSheetNo.Name, Me.lblSickSheetNo.Text)
            Me.btnEClose.Text = Language._Object.getCaption(Me.btnEClose.Name, Me.btnEClose.Text)
            Me.btnEmailOk.Text = Language._Object.getCaption(Me.btnEmailOk.Name, Me.btnEmailOk.Text)
            Me.colhSicksheetDate.Text = Language._Object.getCaption(CStr(Me.colhSicksheetDate.Tag), Me.colhSicksheetDate.Text)
            Me.LblSickSheetDate.Text = Language._Object.getCaption(Me.LblSickSheetDate.Name, Me.LblSickSheetDate.Text)
            Me.LblToSickSheetDate.Text = Language._Object.getCaption(Me.LblToSickSheetDate.Name, Me.LblToSickSheetDate.Text)
            Me.mnuPreviewSickSheet.Text = Language._Object.getCaption(Me.mnuPreviewSickSheet.Name, Me.mnuPreviewSickSheet.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Sick Sheet from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Sick Sheet ?")
            Language.setMessage(mstrModuleName, 3, "Please check atleast one Sick Sheet.")
            Language.setMessage(mstrModuleName, 4, "Some of the employees email addresses are blank. Do you wish to continue?")
            Language.setMessage(mstrModuleName, 5, "Select")
            Language.setMessage(mstrModuleName, 6, "Sorry,You cannot Edit/Delete this sicksheet.Reason:It is used in Medical claim.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class