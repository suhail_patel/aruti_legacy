﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message index = 3 

Public Class frmMedical_Claim_List

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmMedical_Claim_List"
    Private objmedicalclaim As clsmedical_claim_master

#End Region

#Region " Private Methods "

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Dim lvItem As ListViewItem
        Try

            If User._Object.Privilege._AllowToViewMedicalClaimList = True Then


                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.

                If txtInvoiceNo.Text.Trim.Length > 0 Then
                    StrSearching &= "AND mdmedical_claim_master.invoiceno like '%" & txtInvoiceNo.Text.Trim & "%'" & " "
                End If

                If CInt(cboProvider.SelectedValue) > 0 Then
                    StrSearching &= "AND mdmedical_claim_master.instituteunkid=" & CInt(cboProvider.SelectedValue) & " "
                End If

                If CInt(cboPeriod.SelectedValue) > 0 Then
                    StrSearching &= "AND mdmedical_claim_master.periodunkid =" & CInt(cboPeriod.SelectedValue) & " "
                End If

                If CInt(cboStatus.SelectedValue) > 0 Then
                    StrSearching &= "AND mdmedical_claim_master.statusunkid =" & CInt(cboStatus.SelectedValue) & " "
                End If

                If txtFromTotInvoiceAmt.Decimal <> 0 Then
                    StrSearching &= "AND mdmedical_claim_master.invoice_amt >=" & CDec(Format(txtFromTotInvoiceAmt.Decimal, GUI.fmtCurrency)) & " "
                End If

                If txtToTotInvoiceAmt.Decimal <> 0 Then
                    StrSearching &= "AND mdmedical_claim_master.invoice_amt <=" & CDec(Format(txtFromTotInvoiceAmt.Decimal, GUI.fmtCurrency)) & " "
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                End If

                dsList = objmedicalclaim.GetList("List", True, , chkMyInvoiceOnly.Checked, StrSearching)

                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable

                'Pinkal (06-Jan-2016) -- End

                lvClaimList.Items.Clear()
                For Each dtRow As DataRow In dtTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = dtRow("invoiceno").ToString
                    lvItem.Tag = dtRow("claimunkid").ToString
                    lvItem.SubItems.Add(dtRow("period_name").ToString)
                    lvItem.SubItems.Add(dtRow("institute_name").ToString)
                    lvItem.SubItems.Add(Format(CDec(dtRow("invoice_amt")), GUI.fmtCurrency))
                    lvItem.SubItems.Add(dtRow("status_name").ToString)
                    lvItem.SubItems.Add(dtRow("statusunkid").ToString)
                    lvItem.SubItems.Add(dtRow("isfinal").ToString)
                    lvClaimList.Items.Add(lvItem)
                Next

                If lvClaimList.Items.Count > 18 Then
                    colhStatus.Width = 120 - 18
                Else
                    colhStatus.Width = 120
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try
            'FOR MEDICAL INSTITUTE
            dsFill = Nothing
            Dim objInstitute As New clsinstitute_master


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            'dsFill = objInstitute.getListForCombo(True, "Institute", True, 1)
            dsFill = objInstitute.getListForCombo(True, "Institute", True, 1, False, User._Object._Userunkid)
            'Pinkal (06-Jan-2016) -- End
            cboProvider.DisplayMember = "name"
            cboProvider.ValueMember = "instituteunkid"
            cboProvider.DataSource = dsFill.Tables("Institute")

            'FOR YEAR
            dsFill = Nothing
            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            cboPeriod.DisplayMember = "name"
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DataSource = dsFill.Tables("Period")

            'FOR Status
            dsFill = Nothing
            Dim objStatus As New clsMasterData
            dsFill = objStatus.getMedicalStatusList("Status", True)
            cboStatus.DisplayMember = "Name"
            cboStatus.ValueMember = "statusunkid"
            cboStatus.DataSource = dsFill.Tables("Status")


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AddMedicalClaim
            btnEdit.Enabled = User._Object.Privilege._EditMedicalClaim
            btnDelete.Enabled = User._Object.Privilege._DeleteMedicalClaim
            btnExportStatus.Enabled = User._Object.Privilege._AllowToExportMedicalClaim
            btnCancelExport.Enabled = User._Object.Privilege._AllowToCancelExportedMedicalClaim
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmMedical_Claim_List_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objmedicalclaim = New clsmedical_claim_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            OtherSettings()

            Call SetVisibility()
            FillCombo()

            txtFromTotInvoiceAmt.Text = Format(txtFromTotInvoiceAmt.Decimal, GUI.fmtCurrency)
            txtToTotInvoiceAmt.Text = Format(txtToTotInvoiceAmt.Decimal, GUI.fmtCurrency)

            If lvClaimList.Items.Count > 0 Then lvClaimList.Items(0).Selected = True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedical_Claim_List_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedical_Claim_List_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedical_Claim_List_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedical_Claim_List_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Delete And lvClaimList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedical_Claim_List_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedical_Claim_List_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objmedicalclaim = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsmedical_claim_master.SetMessages()
            objfrm._Other_ModuleNames = "clsmedical_claim_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmMedicalClaim_AddEdit As New frmMedicalClaim_AddEdit
            If objfrmMedicalClaim_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvClaimList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Invoice from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvClaimList.Select()
            Exit Sub
        End If
        Dim objfrmMedicalClaim_AddEdit As New frmMedicalClaim_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvClaimList.SelectedItems(0).Index
            If objfrmMedicalClaim_AddEdit.displayDialog(CInt(lvClaimList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                FillList()
            End If
            objfrmMedicalClaim_AddEdit = Nothing
            If lvClaimList.Items.Count > 0 Then
                lvClaimList.Items(intSelectedIndex).Selected = True
                lvClaimList.EnsureVisible(intSelectedIndex)
            End If
            lvClaimList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmMedicalClaim_AddEdit IsNot Nothing Then objfrmMedicalClaim_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objMedicalClaim_master As New clsmedical_claim_master
        If lvClaimList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Invoice from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvClaimList.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvClaimList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Medical Claim?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.MEDICAL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objMedicalClaim_master._Voidreason = mstrVoidReason
                End If

                frm = Nothing
                objMedicalClaim_master._Voiduserunkid = User._Object._Userunkid
                objMedicalClaim_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                With objMedicalClaim_master
                    ._FormName = mstrModuleName
                    ._LoginEmployeeunkid = 0
                    ._ClientIP = getIP()
                    ._HostName = getHostName()
                    ._FromWeb = False
                    ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                End With
                objMedicalClaim_master.Delete(CInt(lvClaimList.SelectedItems(0).Tag))
                lvClaimList.SelectedItems(0).Remove()


                If lvClaimList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvClaimList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvClaimList.Items.Count - 1
                    lvClaimList.Items(intSelectedIndex).Selected = True
                    lvClaimList.EnsureVisible(intSelectedIndex)
                ElseIf lvClaimList.Items.Count <> 0 Then
                    lvClaimList.Items(intSelectedIndex).Selected = True
                    lvClaimList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvClaimList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboProvider.Items.Count > 0 Then cboProvider.SelectedIndex = 0
            txtInvoiceNo.Text = ""
            txtFromTotInvoiceAmt.Text = Format("0", GUI.fmtCurrency)
            txtToTotInvoiceAmt.Text = Format("0", GUI.fmtCurrency)

            'Pinkal (12-Jun-2012) -- Start
            'Enhancement : TRA Changes
            chkMyInvoiceOnly.Checked = True
            'Pinkal (12-Jun-2012) -- End

            FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub


    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub btnExportStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportStatus.Click
        Try
            If lvClaimList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Invoice from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvClaimList.Select()
                Exit Sub
            End If

            objmedicalclaim._Claimunkid = CInt(lvClaimList.SelectedItems(0).Tag)
            objmedicalclaim._Statusunkid = 2 ' FOR EXPORT STATUS

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            Dim dstrandata As DataSet = (New clsmedical_claim_Tran).GetClaimTran(FinancialYear._Object._DatabaseName, _
                                                                                 User._Object._Userunkid, _
                                                                                 FinancialYear._Object._YearUnkid, _
                                                                                 Company._Object._Companyunkid, _
                                                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                                 objmedicalclaim._Claimunkid)
            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objmedicalclaim._FormName = mstrModuleName
            objmedicalclaim._LoginEmployeeunkid = 0
            objmedicalclaim._ClientIP = getIP()
            objmedicalclaim._HostName = getHostName()
            objmedicalclaim._FromWeb = False
            objmedicalclaim._AuditUserId = User._Object._Userunkid
objmedicalclaim._CompanyUnkid = Company._Object._Companyunkid
            objmedicalclaim._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objmedicalclaim.Update(objmedicalclaim._DsTranData.Tables(0), ) = False Then
            If objmedicalclaim.Update(dstrandata.Tables(0)) = False Then
                'Shani(24-Aug-2015) -- End
                eZeeMsgBox.Show(objmedicalclaim._Message, enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Medical Claim bill Exported Successfully."), enMsgBoxStyle.Information)
                FillList()
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExportStatus_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancelExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelExport.Click
        Try
            If lvClaimList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Invoice from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvClaimList.Select()
                Exit Sub
            End If

            objmedicalclaim._Claimunkid = CInt(lvClaimList.SelectedItems(0).Tag)

            objmedicalclaim._IsFinal = False
            objmedicalclaim._Statusunkid = 1 ' FOR PENDING STATUS


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            Dim dsTranData As DataSet = (New clsmedical_claim_Tran).GetClaimTran(FinancialYear._Object._DatabaseName, _
                                                                                 User._Object._Userunkid, _
                                                                                 FinancialYear._Object._YearUnkid, _
                                                                                 Company._Object._Companyunkid, _
                                                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                                 objmedicalclaim._Claimunkid)


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objmedicalclaim._FormName = mstrModuleName
            objmedicalclaim._LoginEmployeeunkid = 0
            objmedicalclaim._ClientIP = getIP()
            objmedicalclaim._HostName = getHostName()
            objmedicalclaim._FromWeb = False
            objmedicalclaim._AuditUserId = User._Object._Userunkid
objmedicalclaim._CompanyUnkid = Company._Object._Companyunkid
            objmedicalclaim._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            'If objmedicalclaim.Update(objmedicalclaim._DsTranData.Tables(0)) = False Then
            If objmedicalclaim.Update(dsTranData.Tables(0)) = False Then
                'Pinkal (06-Jan-2016) -- End

                eZeeMsgBox.Show(objmedicalclaim._Message, enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Medical Claim bill Cancel Exported Successfully."), enMsgBoxStyle.Information)
                FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancelExport_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (20-Jan-2012) -- End



    'Pinkal (19-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchProvider_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchProvider.Click
        Try
            Dim frm As New frmCommonSearch
            frm.DataSource = CType(cboProvider.DataSource, DataTable)
            frm.DisplayMember = cboProvider.DisplayMember
            frm.ValueMember = cboProvider.ValueMember
            If frm.DisplayDialog() Then
                cboProvider.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchProvider_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Try
            Dim frm As New frmCommonSearch
            frm.DataSource = CType(cboPeriod.DataSource, DataTable)
            frm.DisplayMember = cboPeriod.DisplayMember
            frm.ValueMember = cboPeriod.ValueMember
            If frm.DisplayDialog() Then
                cboPeriod.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        End Try
    End Sub

    'Pinkal (19-Nov-2012) -- End


#End Region

#Region "TextBox Event"

    Private Sub txtFromTotInvoiceAmt_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFromTotInvoiceAmt.Validated, txtToTotInvoiceAmt.Validated
        Try
            CType(sender, eZee.TextBox.NumericTextBox).Text = Format(CType(sender, eZee.TextBox.NumericTextBox).Decimal, GUI.fmtCurrency)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtFromTotInvoiceAmt_Validated", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ListView Event"

    Private Sub lvClaimList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvClaimList.SelectedIndexChanged
        Try

            If lvClaimList.SelectedItems.Count > 0 Then


                'Pinkal (20-Jan-2012) -- Start
                'Enhancement : TRA Changes

                'If CInt(lvClaimList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 1 Then
                '    btnEdit.Enabled = True
                '    btnDelete.Enabled = True
                'Else
                '    btnEdit.Enabled = False
                '    btnDelete.Enabled = False
                'End If

                If CBool(lvClaimList.SelectedItems(0).SubItems(objColhIsFinal.Index).Text) = False Then
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                    btnExportStatus.Enabled = False
                    btnCancelExport.Enabled = False
                ElseIf CBool(lvClaimList.SelectedItems(0).SubItems(objColhIsFinal.Index).Text) = True And CInt(lvClaimList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 1 Then
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                    btnExportStatus.Enabled = True
                    btnCancelExport.Enabled = False
                ElseIf CBool(lvClaimList.SelectedItems(0).SubItems(objColhIsFinal.Index).Text) = True And CInt(lvClaimList.SelectedItems(0).SubItems(objcolhStatusunkid.Index).Text) = 2 Then
                    btnEdit.Enabled = False
                    btnDelete.Enabled = False
                    btnExportStatus.Enabled = False
                    btnCancelExport.Enabled = True
                End If

                'Pinkal (20-Jan-2012) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvClaimList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.BtnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.BtnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblProvider.Text = Language._Object.getCaption(Me.lblProvider.Name, Me.lblProvider.Text)
            Me.lblTreatmentDate.Text = Language._Object.getCaption(Me.lblTreatmentDate.Name, Me.lblTreatmentDate.Text)
            Me.lblServiceNo.Text = Language._Object.getCaption(Me.lblServiceNo.Name, Me.lblServiceNo.Text)
            Me.lblService.Text = Language._Object.getCaption(Me.lblService.Name, Me.lblService.Text)
            Me.lblFromTotInvoiceAmt.Text = Language._Object.getCaption(Me.lblFromTotInvoiceAmt.Name, Me.lblFromTotInvoiceAmt.Text)
            Me.lblInvoiceNo.Text = Language._Object.getCaption(Me.lblInvoiceNo.Name, Me.lblInvoiceNo.Text)
            Me.lblToTotInvoiceAmt.Text = Language._Object.getCaption(Me.lblToTotInvoiceAmt.Name, Me.lblToTotInvoiceAmt.Text)
            Me.colhInvoiceNo.Text = Language._Object.getCaption(CStr(Me.colhInvoiceNo.Tag), Me.colhInvoiceNo.Text)
            Me.colhInstitute.Text = Language._Object.getCaption(CStr(Me.colhInstitute.Tag), Me.colhInstitute.Text)
            Me.colhInvoiceAmt.Text = Language._Object.getCaption(CStr(Me.colhInvoiceAmt.Tag), Me.colhInvoiceAmt.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.colhStatus.Text = Language._Object.getCaption(CStr(Me.colhStatus.Tag), Me.colhStatus.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.BtnOperation.Text = Language._Object.getCaption(Me.BtnOperation.Name, Me.BtnOperation.Text)
            Me.btnExportStatus.Text = Language._Object.getCaption(Me.btnExportStatus.Name, Me.btnExportStatus.Text)
            Me.btnCancelExport.Text = Language._Object.getCaption(Me.btnCancelExport.Name, Me.btnCancelExport.Text)
            Me.chkMyInvoiceOnly.Text = Language._Object.getCaption(Me.chkMyInvoiceOnly.Name, Me.chkMyInvoiceOnly.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Invoice from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Medical Claim?")
            Language.setMessage(mstrModuleName, 3, "Medical Claim bill Exported Successfully.")
            Language.setMessage(mstrModuleName, 4, "Medical Claim bill Cancel Exported Successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class frmMedical_Claim_List

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmMedical_Claim_List"
'    Private objmedicalclaim_Tran As clsmedical_claim_Tran
'    Dim objExchangeRate As clsExchangeRate

'#End Region

'#Region " Private Methods "

'    Private Sub FillList()
'        Dim dsList As New DataSet
'        Dim StrSearching As String = String.Empty
'        Dim dtTable As DataTable
'        Dim lvItem As ListViewItem
'        Try

'            dsList = objmedicalclaim_Tran.GetClaimTran()

'            If CInt(cboEmployee.SelectedValue) > 0 Then
'                StrSearching = "AND employeeunkid= " & CInt(cboEmployee.SelectedValue) & " "
'            End If

'            If CInt(cboMedicalYear.SelectedValue) > 0 Then
'                StrSearching &= "AND yearunkid=" & CInt(cboMedicalYear.SelectedValue) & " "
'            End If

'            If txtClaimNo.Text.Trim.Length > 0 Then
'                StrSearching &= "AND claimno like '%" & txtClaimNo.Text.Trim & "%'" & " "
'            End If

'            If dtpClaimDate.Checked Then
'                StrSearching &= "AND claimdate='" & dtpClaimDate.Value.Date & "'" & " "
'            End If

'            'Sohail (17 Jun 2011) -- Start
'            'If txtClaimAmount.Text.Trim.Length > 0 Then
'            '    StrSearching &= "AND amount = " & Format(CDec(txtClaimAmount.Text.Trim), objExchangeRate._fmtCurrency) & " "
'            'End If
'            If txtClaimAmount.Decimal <> 0 Then
'                StrSearching &= "AND amount = " & txtClaimAmount.Decimal & " "
'            End If
'            'Sohail (17 Jun 2011) -- End

'            If CInt(cboHospital.SelectedValue) > 0 Then
'                StrSearching &= "AND instituteunkid=" & CInt(cboHospital.SelectedValue) & " "
'            End If

'            If dtpTreatmentDate.Checked Then
'                StrSearching &= "AND treatmentdate='" & dtpTreatmentDate.Value.Date & "'" & " "
'            End If

'            If CInt(cboService.SelectedValue) > 0 Then
'                StrSearching &= "AND serviceunkid=" & CInt(cboService.SelectedValue) & " "
'            End If

'            If txtServiceNo.Text.Trim.Length > 0 Then
'                StrSearching &= "AND serviceno like '%" & txtServiceNo.Text.Trim & "%'" & " "
'            End If

'            If StrSearching.Length > 0 Then
'                StrSearching = StrSearching.Substring(3)
'                dtTable = New DataView(dsList.Tables(0), StrSearching, "", DataViewRowState.CurrentRows).ToTable
'            Else
'                dtTable = dsList.Tables(0)
'            End If
'            lvClaimList.Items.Clear()
'            For Each dtRow As DataRow In dtTable.Rows
'                lvItem = New ListViewItem
'                lvItem.Text = dtRow("employeename").ToString
'                lvItem.Tag = dtRow("claimunkid").ToString
'                lvItem.SubItems.Add(dtRow("year").ToString)
'                lvItem.SubItems.Add(dtRow("servicename").ToString)
'                lvItem.SubItems.Add(dtRow("serviceno").ToString)
'                lvItem.SubItems.Add(dtRow("claimno").ToString)
'                lvItem.SubItems.Add(CDate(dtRow("claimdate")).ToShortDateString)
'                lvItem.SubItems.Add(Format(CDec(dtRow("amount")), objExchangeRate._fmtCurrency))
'                lvItem.SubItems.Add(dtRow("Hospital").ToString)
'                lvItem.SubItems.Add(CDate(dtRow("treatmentdate")).ToShortDateString)
'                lvItem.SubItems.Add(dtRow("claimtranunkid").ToString)
'                lvClaimList.Items.Add(lvItem)
'            Next

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub FillCombo()
'    Dim dsFill As DataSet = Nothing
'    Try
'            'FOR EMPLOYEE 
'        Dim objEmployee As New clsEmployee_Master

'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : CHECK FOR ACTIVE EMPLOYEE
'            'dsFill = objEmployee.GetEmployeeList("Employee", True, True)
'            dsFill = objEmployee.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
'            'Pinkal (24-Jun-2011) -- End
'            cboEmployee.DisplayMember = "employeename"
'            cboEmployee.ValueMember = "employeeunkid"
'            cboEmployee.DataSource = dsFill.Tables("Employee")

'            'FOR YEAR
'            dsFill = Nothing
'            Dim objyear As New clsMasterData
'            dsFill = objyear.getComboListPAYYEAR("Year", True)
'            cboMedicalYear.DisplayMember = "name"
'            cboMedicalYear.ValueMember = "Id"
'            cboMedicalYear.DataSource = dsFill.Tables("Year")

'            'FOR MEDICAL INSTITUTE
'            dsFill = Nothing
'            Dim objInstitute As New clsinstitute_master
'            dsFill = objInstitute.getListForCombo(True, "Institute", True)
'            cboHospital.DisplayMember = "name"
'            cboHospital.ValueMember = "instituteunkid"
'            cboHospital.DataSource = dsFill.Tables("Institute")


'            'FOR MEDICAL SERVICE
'            dsFill = Nothing
'            Dim objMedical As New clsmedical_master
'            dsFill = objMedical.getListForCombo(enMedicalMasterType.Service, "Service", True)
'            cboService.DisplayMember = "name"
'            cboService.ValueMember = "mdmasterunkid"
'            cboService.DataSource = dsFill.Tables("Service")


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetVisibility()

'        Try

'            btnNew.Enabled = User._Object.Privilege._AddMedicalClaim
'            btnEdit.Enabled = User._Object.Privilege._EditMedicalClaim
'            btnDelete.Enabled = User._Object.Privilege._DeleteMedicalClaim

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        End Try

'    End Sub
'#End Region

'#Region " Form's Events "

'    Private Sub frmMedical_Claim_List_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objmedicalclaim_Tran = New clsmedical_claim_Tran
'        objExchangeRate = New clsExchangeRate
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            objExchangeRate._ExchangeRateunkid = 1
'            Language.setLanguage(Me.Name)

'            Call SetVisibility()

'            'Pinkal (03-Jan-2011) -- Start

'            dtpClaimDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
'            dtpClaimDate.MaxDate = FinancialYear._Object._Database_End_Date.Date

'            dtpTreatmentDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
'            dtpTreatmentDate.MaxDate = FinancialYear._Object._Database_End_Date.Date

'            'Pinkal (03-Jan-2011) -- End


'            FillCombo()
'            FillList()

'            If lvClaimList.Items.Count > 0 Then lvClaimList.Items(0).Selected = True
'            cboEmployee.Select()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmMedical_Claim_List_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmMedical_Claim_List_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
'        Try
'            If Asc(e.KeyChar) = 13 Then
'                SendKeys.Send("{TAB}")
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmMedical_Claim_List_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmMedical_Claim_List_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
'        Try
'            If e.KeyCode = Keys.Delete And lvClaimList.Focused = True Then
'                'Sohail (28 Jun 2011) -- Start
'                'Issue : Delete event fired even if there is no delete privilege
'                'btnDelete_Click(sender, e)
'                Call btnDelete.PerformClick()
'                'Sohail (28 Jun 2011) -- End
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmMedical_Claim_List_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmMedical_Claim_List_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
'        objmedicalclaim_Tran = Nothing
'    End Sub

'#End Region

'#Region " Button's Events "

'    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
'        Try
'            Dim objfrmMedicalClaim_AddEdit As New frmMedicalClaim_AddEdit
'            If objfrmMedicalClaim_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
'                FillList()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
'        If lvClaimList.SelectedItems.Count < 1 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Medical Claim from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            lvClaimList.Select()
'            Exit Sub
'        End If
'        Dim objfrmMedicalClaim_AddEdit As New frmMedicalClaim_AddEdit
'        Try
'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvClaimList.SelectedItems(0).Index
'            If objfrmMedicalClaim_AddEdit.displayDialog(CInt(lvClaimList.SelectedItems(0).Tag), enAction.EDIT_ONE, CInt(lvClaimList.SelectedItems(0).SubItems(colhClaimtranunkid.Index).Text)) Then
'                FillList()
'            End If
'            objfrmMedicalClaim_AddEdit = Nothing
'            If lvClaimList.Items.Count > 0 Then
'                If lvClaimList.Items.Count > 0 Then intSelectedIndex = lvClaimList.Items.Count - 1
'                lvClaimList.Items(intSelectedIndex).Selected = True
'                lvClaimList.EnsureVisible(intSelectedIndex)
'            End If
'            lvClaimList.Select()
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        Finally
'            If objfrmMedicalClaim_AddEdit IsNot Nothing Then objfrmMedicalClaim_AddEdit.Dispose()
'        End Try
'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        Dim objMedicalClaim_master As New clsmedical_claim_master
'        If lvClaimList.SelectedItems.Count < 1 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Medical Claim from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'            lvClaimList.Select()
'            Exit Sub
'        End If
'        'If objMedicalClaim_master.isUsed(CInt(lvClaimList.SelectedItems(0).Tag)) Then
'        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Medical Claim. Reason: This Medical Claim is in use."), enMsgBoxStyle.Information) '?2
'        '    lvClaimList.Select()
'        '    Exit Sub
'        'End If
'        Try
'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvClaimList.SelectedItems(0).Index

'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Medical Claim?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
'                'Sandeep [ 16 Oct 2010 ] -- Start
'                'objMedicalClaim_master._Voidreason = "VOID REASON"
'                Dim frm As New frmReasonSelection
'                Dim mstrVoidReason As String = String.Empty
'                frm.displayDialog(enVoidCategoryType.MEDICAL, mstrVoidReason)
'                If mstrVoidReason.Length <= 0 Then
'                    Exit Sub
'        Else
'                    objMedicalClaim_master._Voidreason = mstrVoidReason
'                End If
'                frm = Nothing
'                objMedicalClaim_master._Voiduserunkid = User._Object._Userunkid
'                'Sandeep [ 16 Oct 2010 ] -- End 

'                objMedicalClaim_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime

'                objMedicalClaim_master.Delete(CInt(lvClaimList.SelectedItems(0).Tag), CInt(lvClaimList.SelectedItems(0).SubItems(colhClaimtranunkid.Index).Text))
'                lvClaimList.SelectedItems(0).Remove()


'                If lvClaimList.Items.Count <= 0 Then
'                    Exit Try
'        End If

'                If lvClaimList.Items.Count = intSelectedIndex Then
'                    intSelectedIndex = lvClaimList.Items.Count - 1
'                    lvClaimList.Items(intSelectedIndex).Selected = True
'                    lvClaimList.EnsureVisible(intSelectedIndex)
'                ElseIf lvClaimList.Items.Count <> 0 Then
'                    lvClaimList.Items(intSelectedIndex).Selected = True
'                    lvClaimList.EnsureVisible(intSelectedIndex)
'                End If
'            End If
'            lvClaimList.Select()
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
'        Try
'            FillList()
'    Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
'    End Try
'End Sub

'    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
'    Try
'            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedIndex = 0
'            If cboMedicalYear.Items.Count > 0 Then cboMedicalYear.SelectedIndex = 0
'            txtClaimNo.Text = ""

'            'Pinkal (03-Jan-2011) --Start

'            If FinancialYear._Object._Database_End_Date.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
'                dtpClaimDate.Value = FinancialYear._Object._Database_End_Date.Date
'                dtpTreatmentDate.Value = FinancialYear._Object._Database_End_Date.Date

'            ElseIf FinancialYear._Object._Database_End_Date.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
'                dtpClaimDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
'                dtpTreatmentDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date

'            End If

'            'Pinkal (03-Jan-2011) --End


'            dtpClaimDate.Checked = False
'            dtpTreatmentDate.Checked = False

'            txtClaimAmount.Text = ""
'            If cboHospital.Items.Count > 0 Then cboHospital.SelectedIndex = 0
'            If cboService.Items.Count > 0 Then cboService.SelectedIndex = 0
'            txtServiceNo.Text = ""
'            FillList()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        Dim objFrmCommon As New frmCommonSearch
'        Try
'            objFrmCommon.DataSource = CType(cboEmployee.DataSource, DataTable)
'            objFrmCommon.DisplayMember = cboEmployee.DisplayMember
'            objFrmCommon.ValueMember = cboEmployee.ValueMember
'            objFrmCommon.CodeMember = "employeecode"
'            If objFrmCommon.DisplayDialog Then
'                cboEmployee.SelectedValue = objFrmCommon.SelectedValue
'        End If
'    Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'    End Try
'End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Me.Close()
'    End Sub

'#End Region

'#Region " Dropdown's Event "

'    'Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
'    '    Dim dsFill As DataSet = Nothing
'    '    Try
'    '        'FOR MEMBERSHIP
'    '        cboService.DataSource = Nothing
'    '        Dim objEmployee As New clsEmployee_Master
'    '        If CInt(cboEmployee.SelectedValue) <= 0 Then
'    '            Dim objMembership As New clsmembership_master
'    '            dsFill = objMembership.getListForCombo("Membership", True)
'    '            cboService.ValueMember = "membershipunkid"
'    '            cboService.DisplayMember = "name"
'    '        Else

'    '            dsFill = objEmployee.GetEmployee_Membership(CInt(cboEmployee.SelectedValue), True, "Membership")
'    '            cboService.ValueMember = "Mem_Id"
'    '            cboService.DisplayMember = "Mem_Name"
'    '        End If

'    '        cboService.DataSource = dsFill.Tables("Membership")
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
'    '    End Try
'    'End Sub

'    'Private Sub cboMembership_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboService.SelectedIndexChanged
'    '    Dim dsFill As DataSet = Nothing
'    '    Dim dtFill As DataTable = Nothing
'    '    Try
'    '        'FOR MEMBERSHIPNo
'    '        Dim objEmployee As New clsEmployee_Master
'    '        dsFill = objEmployee.GetEmployee_Membership(CInt(cboEmployee.SelectedValue), True, "Membership")
'    '        dtFill = New DataView(dsFill.Tables("Membership"), "Mem_Id=" & CInt(cboService.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
'    '        If dtFill.Rows.Count > 0 Then
'    '            txtServiceNo.Text = dtFill.Rows(0)("Mem_No").ToString
'    '        Else
'    '            txtServiceNo.Text = ""
'    '        End If
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "cboMembershipname_SelectedIndexChanged", mstrModuleName)
'    '    End Try
'    'End Sub

'#End Region

'End Class

