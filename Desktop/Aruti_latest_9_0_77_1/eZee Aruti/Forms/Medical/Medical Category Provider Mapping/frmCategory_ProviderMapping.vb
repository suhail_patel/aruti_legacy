﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmCategory_ProviderMapping

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCategory_ProviderMapping"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objProviderMapping As clscategory_providermapping
    Private mintCategoryID As Integer
    Private mintCategoryMstID As Integer
    Private mdvProvider As DataView = Nothing

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal intCategoryID As Integer) As Boolean
        Try
            menAction = eAction
            mintCategoryID = intCategoryID
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub


    Private Sub FillProvider()
        Dim dsFill As DataSet = Nothing
        Try
            Dim objProvider As New clsinstitute_master
            dsFill = objProvider.GetList("Institute", True, False, 1)

            For Each dr As DataRow In dsFill.Tables(0).Rows
                Dim drRow As DataRow = objProviderMapping._dtProvider.NewRow
                drRow("mdcateogryproviderunkid") = objProviderMapping.GetProviderMappingId(mintCategoryID, mintCategoryMstID, CInt(dr("instituteunkid")))
                drRow("medicalcategoryunkid") = mintCategoryID
                drRow("mdcategorymasterunkid") = mintCategoryMstID
                drRow("ischecked") = objProviderMapping.isExist(mintCategoryID, mintCategoryMstID, CInt(dr("instituteunkid")))
                drRow("instituteunkid") = dr("instituteunkid").ToString()
                drRow("institute_name") = dr("institute_name").ToString()
                objProviderMapping._dtProvider.Rows.Add(drRow)
            Next

            mdvProvider = objProviderMapping._dtProvider.DefaultView
            dgProvider.AutoGenerateColumns = False
            dgProvider.DataSource = mdvProvider
            objdgcolhSelect.DataPropertyName = "ischecked"
            dgcolhProvider.DataPropertyName = "institute_name"
            objdgcolhproviderId.DataPropertyName = "instituteunkid"
            objdgcolhProviderMappingId.DataPropertyName = "instituteunkid"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillProvider", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " Form's Events "

    Private Sub frmProvider_UserMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objProviderMapping = New clscategory_providermapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Dim objMdCategroyMst As New clsmedical_category_master
            objMdCategroyMst._Medicalcategoryunkid = mintCategoryID

            Dim objCategoryMst As New clsmedical_master
            objCategoryMst._Mdmasterunkid = objMdCategroyMst._Mdcategorymasterunkid
            mintCategoryMstID = objCategoryMst._Mdmasterunkid
            lblValueCategory.Text = objCategoryMst._Mdmastername

            FillProvider()
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_UserMapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProvider_UserMapping_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_UserMapping_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProvider_UserMapping_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_UserMapping_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmProvider_UserMapping_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objProviderMapping = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()


        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If objProviderMapping._dtProvider IsNot Nothing Then
                Dim drRow As DataRow() = objProviderMapping._dtProvider.Select("ischecked=true")

                If drRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Atleast One Provider."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            objProviderMapping._Userunkid = User._Object._Userunkid


            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objProviderMapping._FormName = mstrModuleName
            objProviderMapping._LoginEmployeeunkid = 0
            objProviderMapping._ClientIP = getIP()
            objProviderMapping._HostName = getHostName()
            objProviderMapping._FromWeb = False
            objProviderMapping._AuditUserId = User._Object._Userunkid
objProviderMapping._CompanyUnkid = Company._Object._Companyunkid
            objProviderMapping._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            blnFlag = objProviderMapping.Insert()

            If blnFlag = False And objProviderMapping._Message <> "" Then
                eZeeMsgBox.Show(objProviderMapping._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkSelect_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelect.CheckedChanged
        Try

            If objProviderMapping._dtProvider.Rows.Count > 0 Then
                For Each dr As DataRow In objProviderMapping._dtProvider.Rows
                    RemoveHandler dgProvider.CellContentClick, AddressOf dgProvider_CellContentClick
                    dr("ischecked") = chkSelect.Checked
                    AddHandler dgProvider.CellContentClick, AddressOf dgProvider_CellContentClick
                Next
                objProviderMapping._dtProvider.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelect_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Datagrid Event"

    Private Sub dgProvider_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgProvider.CellContentClick, dgProvider.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhSelect.Index Then

                If dgProvider.IsCurrentCellDirty Then
                    dgProvider.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    mdvProvider.Table().AcceptChanges()
                End If

                Dim drRow As DataRow() = mdvProvider.ToTable.Select("ischecked = true")

                RemoveHandler chkSelect.CheckedChanged, AddressOf chkSelect_CheckedChanged

                If drRow.Length <= 0 Then
                    chkSelect.CheckState = CheckState.Unchecked
                ElseIf drRow.Length < dgProvider.Rows.Count Then
                    chkSelect.CheckState = CheckState.Indeterminate
                ElseIf drRow.Length = dgProvider.Rows.Count Then
                    chkSelect.CheckState = CheckState.Checked
                End If

                AddHandler chkSelect.CheckedChanged, AddressOf chkSelect_CheckedChanged

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgProvider_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbProviderdetail.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbProviderdetail.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbProviderdetail.Text = Language._Object.getCaption(Me.gbProviderdetail.Name, Me.gbProviderdetail.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.chkSelect.Text = Language._Object.getCaption(Me.chkSelect.Name, Me.chkSelect.Text)
			Me.dgcolhProvider.HeaderText = Language._Object.getCaption(Me.dgcolhProvider.Name, Me.dgcolhProvider.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.lblCategory.Text = Language._Object.getCaption(Me.lblCategory.Name, Me.lblCategory.Text)
			Me.lblValueCategory.Text = Language._Object.getCaption(Me.lblValueCategory.Name, Me.lblValueCategory.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "Please Select Atleast One Provider.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
