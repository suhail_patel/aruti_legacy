﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message index = 17

Public Class frmImport_MedicalClaim

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmImport_MedicalClaim"
    Private objMdClaimtran As clsmedical_claim_Tran
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Dim mdsPayroll As DataSet = Nothing
    Dim mdtOther As DataTable = Nothing
    Dim dvGriddata As DataView = Nothing
    Dim mintFilter As Integer = 0
    Dim imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Dim imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Dim imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Dim mdtClaim As DataTable = Nothing
    Dim mblnIsError As Boolean = False


    'Pinkal (18-Dec-2012) -- Start
    'Enhancement : TRA Changes
    Private _ProviderId As Integer = 0
    'Pinkal (18-Dec-2012) -- End

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intProviderId As Integer, ByVal eAction As enAction) As Boolean
        Try
            menAction = eAction

            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            _ProviderId = intProviderId
            'Pinkal (18-Dec-2012) -- End

            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Property"

    Public Property _mdtClaim() As DataTable
        Get
            Return mdtClaim
        End Get
        Set(ByVal value As DataTable)
            mdtClaim = value
        End Set
    End Property


#End Region

#Region " Private Methods "

    Private Sub setStatus(ByVal dtPRow As DataRow, ByVal blnResult As Boolean)
        Try
            If blnResult Then
                'dtPRow.Item("Message") = ""
                dtPRow.Item("Status") = Language.getMessage(mstrModuleName, 2, "Success")
                dtPRow.Item("image") = imgAccept
                dtPRow.Item("objStatus") = 1
                objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
            Else
                ' dtPRow.Item("Message") = Language.getMessage(mstrModuleName, 4, "Error!")
                dtPRow.Item("Status") = Language.getMessage(mstrModuleName, 3, "Fail")
                dtPRow.Item("image") = imgError
                dtPRow.Item("objStatus") = 2
                objError.Text = CStr(Val(objError.Text) + 1)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setStatus", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGridData()
        Try

            Dim mstrCode As String = ""
            Dim mstrName As String = ""
            Dim mstrSickSheetno As String = ""
            Dim mstrDependants As String = ""

            mdsPayroll.Tables(0).Columns.Add("image", System.Type.GetType("System.Object"))
            mdsPayroll.Tables(0).Columns.Add("Message", System.Type.GetType("System.String"))
            mdsPayroll.Tables(0).Columns.Add("Status", System.Type.GetType("System.String"))
            mdsPayroll.Tables(0).Columns.Add("objStatus", System.Type.GetType("System.Int32"))

            If mdsPayroll Is Nothing Then Exit Sub

            For i As Integer = 0 To mdsPayroll.Tables(0).Rows.Count - 1
                mdsPayroll.Tables(0).Rows(i)("image") = New Drawing.Bitmap(1, 1)
                mdsPayroll.Tables(0).Rows(i)("Message") = ""
                mdsPayroll.Tables(0).Rows(i)("Status") = ""
                mdsPayroll.Tables(0).Rows(i)("objStatus") = 0
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next


            dgEmployeedata.AutoGenerateColumns = False

            'VISIBLE COLUMN
            colhImage.DataPropertyName = "image"
            colhEmployeecode.DataPropertyName = "employeecode"
            colhEmployee.DataPropertyName = "employeename"
            colhDepedants.DataPropertyName = "dependentsname"
            colhSickSheet.DataPropertyName = "sicksheetno"
            colhMessage.DataPropertyName = "Message"
            colhStatus.DataPropertyName = "Status"


            'INVISIBLE COLUMN
            objcolhstatus.DataPropertyName = "objStatus"

            dvGriddata = New DataView(mdsPayroll.Tables(0))
            dgEmployeedata.DataSource = dvGriddata

            ImportData()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGridData", mstrModuleName)
        End Try
    End Sub

    Private Sub ImportData()
        Dim blnResult As Boolean = True
        Dim objlogin As New clslogin_Tran
        Dim dsList As DataSet = Nothing
        Try
            objWarning.Text = "0"
            objError.Text = "0"
            objSuccess.Text = "0"

            wzImportData.CancelEnabled = False
            btnFilter.Enabled = False
            ezWait.Active = True

            Dim objEmployee As New clsEmployee_Master
            Dim objsickSheet As New clsmedical_sicksheet


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            Dim objDependant As New clsDependants_Beneficiary_tran
            Dim objInstitute As New clsinstitute_master
            objInstitute._Instituteunkid = _ProviderId

            'Pinkal (18-Dec-2012) -- End


            For Each dtRow As DataRow In mdsPayroll.Tables(0).Rows

                blnResult = True

                If dtRow.Item("objStatus").ToString <> "0" Then
                    Continue For
                End If

                If CInt(dtRow.Item("employeeunkid")) <= 0 Then
                    dtRow.Item("employeeunkid") = objEmployee.GetEmployeeUnkid("", dtRow.Item("employeecode").ToString())
                End If

                If CInt(dtRow.Item("employeeunkid")) <= 0 Then
                    dtRow.Item("Message") = Language.getMessage(mstrModuleName, 4, "Employee Not Found.")
                    dtRow.Item("Status") = Language.getMessage(mstrModuleName, 3, "Fail")
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("objStatus") = 2
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    blnResult = False
                    mblnIsError = True
                    Continue For
                End If


                If CInt(dtRow.Item("dependantsunkid")) <= 0 Then


                    'Pinkal (18-Dec-2012) -- Start
                    'Enhancement : TRA Changes
                    'Dim dtEmployee As DataSet = objEmployee.GetEmployee_Dependents(CInt(dtRow.Item("employeeunkid")), False)


                    'Pinkal (21-Jul-2014) -- Start
                    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                    'Dim dtEmployee As DataSet = objDependant.GetQualifiedDepedant(CInt(dtRow.Item("employeeunkid")),False)
                    'Sohail (18 May 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    'Dim dtEmployee As DataSet = objDependant.GetQualifiedDepedant(CInt(dtRow.Item("employeeunkid")), True, False, Nothing)
                    Dim dtEmployee As DataSet = objDependant.GetQualifiedDepedant(CInt(dtRow.Item("employeeunkid")), True, False, Nothing, dtAsOnDate:=Now)
                    'Sohail (18 May 2019) -- End
                    'Pinkal (21-Jul-2014) -- End

                    'Pinkal (18-Dec-2012) -- End



                    If dtEmployee.Tables(0).Rows.Count > 0 Then


                        'Pinkal (18-Dec-2012) -- Start
                        'Enhancement : TRA Changes

                        'Dim row As DataRow() = dtEmployee.Tables(0).Select("dependent_Name = '" & dtRow.Item("dependentsname").ToString() & "'")
                        Dim row As DataRow() = dtEmployee.Tables(0).Select("dependants = '" & dtRow.Item("dependentsname").ToString() & "'")

                        'Pinkal (18-Dec-2012) -- End

                        If row.Length > 0 Then
                            dtRow.Item("dependantsunkid") = row(0)("dependent_Id")
                        End If

                    End If

                End If

                If CInt(dtRow.Item("dependantsunkid")) <= 0 And dtRow.Item("dependentsname").ToString() <> "" Then
                    dtRow.Item("Message") = Language.getMessage(mstrModuleName, 12, "Dependants Not Found.")
                    dtRow.Item("Status") = Language.getMessage(mstrModuleName, 3, "Fail")
                    dtRow.Item("image") = imgWarring
                    dtRow.Item("objStatus") = 2
                    objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    blnResult = False
                    mblnIsError = True
                    Continue For
                End If

                If CInt(dtRow.Item("sicksheetunkid")) <= 0 Then

                    'Pinkal (18-Dec-2012) -- Start
                    'Enhancement : TRA Changes

                    If objInstitute._IsFormRequire Then

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'Dim dtEmployee As DataSet = objsickSheet.GetList("List", True, dtRow.Item("sicksheetno").ToString())
                        Dim dtEmployee As DataSet = objsickSheet.GetList(FinancialYear._Object._DatabaseName, _
                                                                         User._Object._Userunkid, _
                                                                         FinancialYear._Object._YearUnkid, _
                                                                         Company._Object._Companyunkid, _
                                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                         ConfigParameter._Object._UserAccessModeSetting, _
                                                                         True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                         "List", True, dtRow.Item("sicksheetno").ToString())
                        'Shani(24-Aug-2015) -- End

                    If dtEmployee.Tables(0).Rows.Count > 0 Then
                        dtRow.Item("sicksheetunkid") = dtEmployee.Tables(0).Rows(0)("sicksheetunkid").ToString()
                    Else
                        dtRow.Item("Message") = Language.getMessage(mstrModuleName, 13, "Invalid Sick Sheet No.")
                        dtRow.Item("Status") = Language.getMessage(mstrModuleName, 3, "Fail")
                        dtRow.Item("image") = imgWarring
                        dtRow.Item("objStatus") = 2
                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        blnResult = False
                        mblnIsError = True
                        Continue For
                    End If

                End If

                    'Pinkal (18-Dec-2012) -- End

                End If

                If CInt(dtRow.Item("sicksheetunkid")) > 0 And CInt(dtRow.Item("employeeunkid")) > 0 Then

                    If objInstitute._IsFormRequire Then

                    If objsickSheet.isExist(dtRow.Item("sicksheetno").ToString(), , CInt(dtRow.Item("employeeunkid"))) = False Then
                        dtRow.Item("Message") = Language.getMessage(mstrModuleName, 14, "This Sick Sheet No is not valid for this employee.")
                        dtRow.Item("Status") = Language.getMessage(mstrModuleName, 3, "Fail")
                        dtRow.Item("image") = imgWarring
                        dtRow.Item("objStatus") = 2
                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                        blnResult = False
                        mblnIsError = True
                        Continue For
                    End If

                End If

                End If

                If CStr(dtRow.Item("Claimno")).Trim.Length > 0 Then
                    Dim drClaimno As DataRow() = Nothing
                    drClaimno = mdsPayroll.Tables(0).Select("claimno = '" & CStr(dtRow.Item("Claimno")).Trim & "' AND employeeunkid <>" & CInt(dtRow.Item("employeeunkid")) & " AND employeeunkid <> -1 ")
                    If drClaimno.Length > 0 Then
                        dtRow.Item("Message") = Language.getMessage(mstrModuleName, 15, "This Claim No is already exist.")
                        dtRow.Item("Status") = Language.getMessage(mstrModuleName, 3, "Fail")
                        dtRow.Item("image") = imgWarring
                        dtRow.Item("objStatus") = 2
                        objWarning.Text = CStr(Val(objWarning.Text) + 1)

                        blnResult = False
                        mblnIsError = True
                        Continue For
                    End If
                End If

                setStatus(dtRow, blnResult)

            Next


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ImportData", mstrModuleName)
        Finally
            ezWait.Active = False
            wzImportData.CancelEnabled = True
            btnFilter.Enabled = True
        End Try
    End Sub

    Private Sub CreateDataForExcel()
        Try
            Dim dtClaim As DataTable = New DataTable("Claim")

            dtClaim.Columns.Add("claimtranunkid", Type.GetType("System.Int32"))
            dtClaim.Columns.Add("claimunkid", Type.GetType("System.Int32"))
            dtClaim.Columns.Add("employeeunkid", System.Type.GetType("System.Int32"))
            dtClaim.Columns.Add("dependantsunkid", System.Type.GetType("System.Int32"))
            dtClaim.Columns.Add("employeecode", System.Type.GetType("System.String"))
            dtClaim.Columns.Add("employeename", System.Type.GetType("System.String"))
            dtClaim.Columns.Add("dependentsname", System.Type.GetType("System.String"))
            dtClaim.Columns.Add("intstituteunkid", System.Type.GetType("System.Int32"))
            dtClaim.Columns.Add("institute_name", System.Type.GetType("System.String"))
            dtClaim.Columns.Add("sicksheetunkid", System.Type.GetType("System.Int32"))
            dtClaim.Columns.Add("sicksheetno", System.Type.GetType("System.String"))
            dtClaim.Columns.Add("claimno", System.Type.GetType("System.String"))
            dtClaim.Columns.Add("claimdate", System.Type.GetType("System.String"))
            dtClaim.Columns.Add("amount", System.Type.GetType("System.Decimal"))
            dtClaim.Columns.Add("remarks", System.Type.GetType("System.String"))
            dtClaim.Columns.Add("statusunkid", System.Type.GetType("System.Int32"))
            dtClaim.Columns.Add("isvoid", System.Type.GetType("System.Boolean"))
            dtClaim.Columns.Add("userunkid", System.Type.GetType("System.Int32"))
            dtClaim.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32"))
            dtClaim.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime"))
            dtClaim.Columns.Add("voidreason", System.Type.GetType("System.String"))
            dtClaim.Columns.Add("AUD", System.Type.GetType("System.String"))
            dtClaim.Columns.Add("GUID", System.Type.GetType("System.String"))


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            dtClaim.Columns.Add("isempexempted", System.Type.GetType("System.Boolean"))
            'Pinkal (18-Dec-2012) -- End


            For i As Integer = 0 To mdtOther.Rows.Count - 1

                Dim drRow As DataRow = dtClaim.NewRow

                drRow("claimtranunkid") = -1
                drRow("claimunkid") = -1
                drRow("employeeunkid") = -1
                drRow("dependantsunkid") = -1
                drRow("sicksheetunkid") = -1
                drRow("employeecode") = mdtOther.Rows(i)(cboEmployeeCode.Text)
                drRow("employeename") = mdtOther.Rows(i)(cboEmployee.Text)
                drRow("dependentsname") = mdtOther.Rows(i)(cboDepedant.Text)
                drRow("sicksheetno") = mdtOther.Rows(i)(cboSickSheet.Text)
                drRow("claimno") = mdtOther.Rows(i)(cboClaimNo.Text)
                drRow("claimdate") = mdtOther.Rows(i)(cboClaimdate.Text)
                If IsDBNull(mdtOther.Rows(i)(cboClaimAmt.Text)) Or mdtOther.Rows(i)(cboClaimAmt.Text).ToString.Trim = "" Then
                    drRow("amount") = 0
                Else
                    drRow("amount") = mdtOther.Rows(i)(cboClaimAmt.Text)
                End If

                If mdtOther.Columns.Contains("remark") Then
                    drRow("remarks") = mdtOther.Rows(i)("remark")
                Else
                    drRow("remark") = ""
                End If
                drRow("isvoid") = False
                drRow("userunkid") = 0
                drRow("voiduserunkid") = 0
                drRow("voiddatetime") = DBNull.Value
                drRow("voidreason") = ""
                drRow("AUD") = "A"
                drRow("GUID") = ""


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
                If mdtOther.Columns.Contains("isempexempted") Then
                    drRow("isempexempted") = mdtOther.Rows(i)("isempexempted")
                Else
                    drRow("isempexempted") = False
                End If
                'Pinkal (18-Dec-2012) -- End

                dtClaim.Rows.Add(drRow)

            Next

            mdsPayroll = New DataSet
            mdsPayroll.Tables.Add(dtClaim)

            Call FillGridData()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataForExcel", mstrModuleName)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If cboEmployeeCode.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employee Code is compulsory information.Please map employee code."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboEmployeeCode.Select()
                Return False

            ElseIf cboEmployee.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Employee is compulsory information.Please map employee."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboEmployee.Select()
                Return False

            ElseIf cboDepedant.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Dependant is compulsory information.Please map depedent."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboDepedant.Select()
                Return False

            ElseIf cboSickSheet.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sick Sheet is compulsory information.Please map sicksheet."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboSickSheet.Select()
                Exit Function

            ElseIf cboClaimNo.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Claim No is compulsory information.Please map claim no."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboClaimNo.Select()
                Return False

            ElseIf cboClaimdate.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Claim Date is compulsory information.Please claim date."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboClaimdate.Select()
                Return False

            ElseIf cboClaimAmt.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Claim Amount is compulsory information.Please map claim amount."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboClaimAmt.Select()
                Return False


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
            ElseIf cboClaimAmt.Text = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Exempt From Deduction is compulsory information.Please map claim Exempt From Deduction."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboClaimAmt.Select()
                Return False

                'Pinkal (18-Dec-2012) -- End


            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmImport_MedicalClaim_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            objMdClaimtran = New clsmedical_claim_Tran
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImport_MedicalClaim_Load", mstrModuleName)
        End Try
    End Sub
  
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileDialog As New OpenFileDialog
            objFileDialog.Filter = "Excel files(*.xlsx)|*.xlsx"
            objFileDialog.FilterIndex = 5
            If objFileDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileDialog.FileName
            End If
            objFileDialog = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Wizard's Event"

    Private Sub wzImportData_BeforeSwitchPages(ByVal sender As System.Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles wzImportData.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case wzImportData.Pages.IndexOf(wpFileSelection)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                    Dim ImpFile As New IO.FileInfo(txtFilePath.Text)
                    If ImpFile.Extension.ToLower = ".xls" Or ImpFile.Extension.ToLower = ".xlsx" Then

                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mdtOther = iExcelData.Import(txtFilePath.Text).Tables(0).Copy
                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mdtOther = OpenXML_Import(txtFilePath.Text).Tables(0).Copy
                        'S.SANDEEP [12-Jan-2018] -- END

                        cboEmployeeCode.Items.Clear()
                        cboEmployee.Items.Clear()
                        cboDepedant.Items.Clear()
                        cboSickSheet.Items.Clear()
                        cboClaimNo.Items.Clear()
                        cboClaimdate.Items.Clear()
                        cboClaimAmt.Items.Clear()

                        'Pinkal (18-Dec-2012) -- Start
                        'Enhancement : TRA Changes
                        cboExemptDeduction.Items.Clear()
                        'Pinkal (18-Dec-2012) -- End


                        For Each dtColumns As DataColumn In mdtOther.Columns
                            cboEmployeeCode.Items.Add(dtColumns.ColumnName)
                            cboEmployee.Items.Add(dtColumns.ColumnName)
                            cboDepedant.Items.Add(dtColumns.ColumnName)
                            cboSickSheet.Items.Add(dtColumns.ColumnName)
                            cboClaimNo.Items.Add(dtColumns.ColumnName)
                            cboClaimdate.Items.Add(dtColumns.ColumnName)
                            cboClaimAmt.Items.Add(dtColumns.ColumnName)

                            'Pinkal (18-Dec-2012) -- Start
                            'Enhancement : TRA Changes
                            cboExemptDeduction.Items.Add(dtColumns.ColumnName)
                            'Pinkal (18-Dec-2012) -- End

                        Next

                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please the select proper file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If

                Case wzImportData.Pages.IndexOf(wpMapfield)
                    If Validation() = False Then e.Cancel = True


            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "wzImportData_BeforeSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub wzImportData_AfterSwitchPages(ByVal sender As System.Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles wzImportData.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case wzImportData.Pages.IndexOf(wpMapfield)
                    cboEmployeeCode.Select()
                Case wzImportData.Pages.IndexOf(wpImportData)
                    wzImportData.CancelText = Language.getMessage(mstrModuleName, 16, "Finish")
                    CreateDataForExcel()
                    mdtClaim = mdsPayroll.Tables(0)
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "wzImportData_AfterSwitchPages", mstrModuleName)
        End Try
    End Sub

    Private Sub wzImportData_Cancel(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles wzImportData.Cancel
        Try
            If wzImportData.CancelText = Language.getMessage(mstrModuleName, 16, "Finish") And mblnIsError = False Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
            ElseIf wzImportData.CancelText = Language.getMessage(mstrModuleName, 16, "Finish") And mblnIsError = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "You can not Import Medical Claim.Reason: Error in importation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Me.DialogResult = Windows.Forms.DialogResult.Cancel
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "wzImportData_Cancel", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "ContextMenu Event"

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try

            dvGriddata.RowFilter = "objStatus = 1"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.wzImportData.CancelText = Language._Object.getCaption(Me.wzImportData.Name & "_CancelText", Me.wzImportData.CancelText)
            Me.wzImportData.NextText = Language._Object.getCaption(Me.wzImportData.Name & "_NextText", Me.wzImportData.NextText)
            Me.wzImportData.BackText = Language._Object.getCaption(Me.wzImportData.Name & "_BackText", Me.wzImportData.BackText)
            Me.wzImportData.FinishText = Language._Object.getCaption(Me.wzImportData.Name & "_FinishText", Me.wzImportData.FinishText)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblClaimAmount.Text = Language._Object.getCaption(Me.lblClaimAmount.Name, Me.lblClaimAmount.Text)
            Me.lblClaimDate.Text = Language._Object.getCaption(Me.lblClaimDate.Name, Me.lblClaimDate.Text)
            Me.lblSickSheet.Text = Language._Object.getCaption(Me.lblSickSheet.Name, Me.lblSickSheet.Text)
            Me.lblDependant.Text = Language._Object.getCaption(Me.lblDependant.Name, Me.lblDependant.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblClaimno.Text = Language._Object.getCaption(Me.lblClaimno.Name, Me.lblClaimno.Text)
            Me.lblEmployeecode.Text = Language._Object.getCaption(Me.lblEmployeecode.Name, Me.lblEmployeecode.Text)
            Me.colhImage.HeaderText = Language._Object.getCaption(Me.colhImage.Name, Me.colhImage.HeaderText)
            Me.colhEmployeecode.HeaderText = Language._Object.getCaption(Me.colhEmployeecode.Name, Me.colhEmployeecode.HeaderText)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhDepedants.HeaderText = Language._Object.getCaption(Me.colhDepedants.Name, Me.colhDepedants.HeaderText)
            Me.colhSickSheet.HeaderText = Language._Object.getCaption(Me.colhSickSheet.Name, Me.colhSickSheet.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
			Me.LblEmpExempted.Text = Language._Object.getCaption(Me.LblEmpExempted.Name, Me.LblEmpExempted.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub

			
    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please the select proper file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Success")
            Language.setMessage(mstrModuleName, 3, "Fail")
            Language.setMessage(mstrModuleName, 4, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 5, "Employee Code is compulsory information.Please map employee code.")
            Language.setMessage(mstrModuleName, 6, "Employee is compulsory information.Please map employee.")
            Language.setMessage(mstrModuleName, 7, "Dependant is compulsory information.Please map depedent.")
            Language.setMessage(mstrModuleName, 8, "Sick Sheet is compulsory information.Please map sicksheet.")
            Language.setMessage(mstrModuleName, 9, "Claim No is compulsory information.Please map claim no.")
            Language.setMessage(mstrModuleName, 10, "Claim Date is compulsory information.Please claim date.")
            Language.setMessage(mstrModuleName, 11, "Claim Amount is compulsory information.Please map claim amount.")
            Language.setMessage(mstrModuleName, 12, "Dependants Not Found.")
            Language.setMessage(mstrModuleName, 13, "Invalid Sick Sheet No.")
            Language.setMessage(mstrModuleName, 14, "This Sick Sheet No is not valid for this employee.")
            Language.setMessage(mstrModuleName, 15, "This Claim No is already exist.")
            Language.setMessage(mstrModuleName, 16, "Finish")
            Language.setMessage(mstrModuleName, 17, "You can not Import Medical Claim.Reason: Error in importation.")
            Language.setMessage(mstrModuleName, 18, "Exempt From Deduction is compulsory information.Please map claim Exempt From Deduction.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub

#End Region 'Language & UI Settings

    '</Language>
End Class

