﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewPerformancePlanning
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewPerformancePlanning))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objSpc1 = New System.Windows.Forms.SplitContainer
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchEmp = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.tbcPlanningView = New System.Windows.Forms.TabControl
        Me.tabpBSC = New System.Windows.Forms.TabPage
        Me.dgvBSCData = New System.Windows.Forms.DataGridView
        Me.tabpCompetencies = New System.Windows.Forms.TabPage
        Me.lvAssignedCompetencies = New eZee.Common.eZeeListView(Me.components)
        Me.colhCompetencies = New System.Windows.Forms.ColumnHeader
        Me.colhWeight = New System.Windows.Forms.ColumnHeader
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuSubmitApproval = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuApproveSubmitted = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuFinalSave = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUnlockFinalSave = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep2 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuGlobalAssign = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUpdatePercentage = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGetFileFormat = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImportBSC_Planning = New System.Windows.Forms.ToolStripMenuItem
        Me.objSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuPrintBSCForm = New System.Windows.Forms.ToolStripMenuItem
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblTotalWeight = New System.Windows.Forms.Label
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.lblCaption1 = New System.Windows.Forms.Label
        Me.lblCaption2 = New System.Windows.Forms.Label
        Me.objpnl2 = New System.Windows.Forms.Panel
        Me.objpnl1 = New System.Windows.Forms.Panel
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objcolhccategory = New System.Windows.Forms.ColumnHeader
        Me.objSpc1.Panel1.SuspendLayout()
        Me.objSpc1.Panel2.SuspendLayout()
        Me.objSpc1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbcPlanningView.SuspendLayout()
        Me.tabpBSC.SuspendLayout()
        CType(Me.dgvBSCData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpCompetencies.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(845, 486)
        Me.pnlMain.TabIndex = 0
        '
        'objSpc1
        '
        Me.objSpc1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objSpc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objSpc1.IsSplitterFixed = True
        Me.objSpc1.Location = New System.Drawing.Point(0, 0)
        Me.objSpc1.Name = "objSpc1"
        Me.objSpc1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objSpc1.Panel1
        '
        Me.objSpc1.Panel1.Controls.Add(Me.gbFilterCriteria)
        '
        'objSpc1.Panel2
        '
        Me.objSpc1.Panel2.Controls.Add(Me.tbcPlanningView)
        Me.objSpc1.Size = New System.Drawing.Size(845, 486)
        Me.objSpc1.SplitterDistance = 66
        Me.objSpc1.SplitterWidth = 2
        Me.objSpc1.TabIndex = 1
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmp)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 71
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(845, 66)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmp
        '
        Me.objbtnSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmp.BorderSelected = False
        Me.objbtnSearchEmp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmp.Location = New System.Drawing.Point(520, 35)
        Me.objbtnSearchEmp.Name = "objbtnSearchEmp"
        Me.objbtnSearchEmp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmp.TabIndex = 2
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(12, 38)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(77, 15)
        Me.lblEmployee.TabIndex = 0
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(95, 35)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(419, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(547, 38)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(79, 15)
        Me.lblPeriod.TabIndex = 3
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(632, 35)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(199, 21)
        Me.cboPeriod.TabIndex = 4
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(818, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(795, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'tbcPlanningView
        '
        Me.tbcPlanningView.Controls.Add(Me.tabpBSC)
        Me.tbcPlanningView.Controls.Add(Me.tabpCompetencies)
        Me.tbcPlanningView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbcPlanningView.Location = New System.Drawing.Point(0, 0)
        Me.tbcPlanningView.Name = "tbcPlanningView"
        Me.tbcPlanningView.SelectedIndex = 0
        Me.tbcPlanningView.Size = New System.Drawing.Size(845, 418)
        Me.tbcPlanningView.TabIndex = 0
        '
        'tabpBSC
        '
        Me.tabpBSC.Controls.Add(Me.dgvBSCData)
        Me.tabpBSC.Location = New System.Drawing.Point(4, 22)
        Me.tabpBSC.Name = "tabpBSC"
        Me.tabpBSC.Size = New System.Drawing.Size(837, 392)
        Me.tabpBSC.TabIndex = 0
        Me.tabpBSC.Text = "Employee Goals"
        Me.tabpBSC.UseVisualStyleBackColor = True
        '
        'dgvBSCData
        '
        Me.dgvBSCData.AllowUserToAddRows = False
        Me.dgvBSCData.AllowUserToDeleteRows = False
        Me.dgvBSCData.AllowUserToResizeRows = False
        Me.dgvBSCData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvBSCData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvBSCData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvBSCData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvBSCData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvBSCData.Location = New System.Drawing.Point(0, 0)
        Me.dgvBSCData.MultiSelect = False
        Me.dgvBSCData.Name = "dgvBSCData"
        Me.dgvBSCData.RowHeadersVisible = False
        Me.dgvBSCData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBSCData.Size = New System.Drawing.Size(837, 392)
        Me.dgvBSCData.TabIndex = 0
        '
        'tabpCompetencies
        '
        Me.tabpCompetencies.Controls.Add(Me.lvAssignedCompetencies)
        Me.tabpCompetencies.Location = New System.Drawing.Point(4, 22)
        Me.tabpCompetencies.Name = "tabpCompetencies"
        Me.tabpCompetencies.Size = New System.Drawing.Size(837, 392)
        Me.tabpCompetencies.TabIndex = 1
        Me.tabpCompetencies.Text = "Employee Competencies"
        Me.tabpCompetencies.UseVisualStyleBackColor = True
        '
        'lvAssignedCompetencies
        '
        Me.lvAssignedCompetencies.BackColorOnChecked = False
        Me.lvAssignedCompetencies.ColumnHeaders = Nothing
        Me.lvAssignedCompetencies.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCompetencies, Me.colhWeight, Me.objcolhccategory})
        Me.lvAssignedCompetencies.CompulsoryColumns = ""
        Me.lvAssignedCompetencies.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvAssignedCompetencies.FullRowSelect = True
        Me.lvAssignedCompetencies.GridLines = True
        Me.lvAssignedCompetencies.GroupingColumn = Nothing
        Me.lvAssignedCompetencies.HideSelection = False
        Me.lvAssignedCompetencies.Location = New System.Drawing.Point(0, 0)
        Me.lvAssignedCompetencies.MinColumnWidth = 50
        Me.lvAssignedCompetencies.MultiSelect = False
        Me.lvAssignedCompetencies.Name = "lvAssignedCompetencies"
        Me.lvAssignedCompetencies.OptionalColumns = ""
        Me.lvAssignedCompetencies.ShowMoreItem = False
        Me.lvAssignedCompetencies.ShowSaveItem = False
        Me.lvAssignedCompetencies.ShowSelectAll = True
        Me.lvAssignedCompetencies.ShowSizeAllColumnsToFit = True
        Me.lvAssignedCompetencies.Size = New System.Drawing.Size(837, 392)
        Me.lvAssignedCompetencies.Sortable = True
        Me.lvAssignedCompetencies.TabIndex = 11
        Me.lvAssignedCompetencies.UseCompatibleStateImageBehavior = False
        Me.lvAssignedCompetencies.View = System.Windows.Forms.View.Details
        '
        'colhCompetencies
        '
        Me.colhCompetencies.Tag = "colhCompetencies"
        Me.colhCompetencies.Text = "Competencies"
        Me.colhCompetencies.Width = 760
        '
        'colhWeight
        '
        Me.colhWeight.Tag = "colhWeight"
        Me.colhWeight.Text = "Weight"
        Me.colhWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhWeight.Width = 70
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSubmitApproval, Me.mnuApproveSubmitted, Me.mnuFinalSave, Me.mnuUnlockFinalSave, Me.objSep2, Me.mnuGlobalAssign, Me.mnuUpdatePercentage, Me.mnuGetFileFormat, Me.mnuImportBSC_Planning, Me.objSep1, Me.mnuPrintBSCForm})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(237, 214)
        '
        'mnuSubmitApproval
        '
        Me.mnuSubmitApproval.Name = "mnuSubmitApproval"
        Me.mnuSubmitApproval.Size = New System.Drawing.Size(236, 22)
        Me.mnuSubmitApproval.Tag = "mnuSubmitApproval"
        Me.mnuSubmitApproval.Text = "S&ubmit for Approval"
        '
        'mnuApproveSubmitted
        '
        Me.mnuApproveSubmitted.Name = "mnuApproveSubmitted"
        Me.mnuApproveSubmitted.Size = New System.Drawing.Size(236, 22)
        Me.mnuApproveSubmitted.Tag = "mnuApproveSubmitted"
        Me.mnuApproveSubmitted.Text = "&Approve/Reject BSC Plan"
        '
        'mnuFinalSave
        '
        Me.mnuFinalSave.Name = "mnuFinalSave"
        Me.mnuFinalSave.Size = New System.Drawing.Size(236, 22)
        Me.mnuFinalSave.Tag = "mnuFinalSave"
        Me.mnuFinalSave.Text = "&Final Approved"
        Me.mnuFinalSave.Visible = False
        '
        'mnuUnlockFinalSave
        '
        Me.mnuUnlockFinalSave.Name = "mnuUnlockFinalSave"
        Me.mnuUnlockFinalSave.Size = New System.Drawing.Size(236, 22)
        Me.mnuUnlockFinalSave.Tag = "mnuUnlockFinalSave"
        Me.mnuUnlockFinalSave.Text = "&Unlock Final Approved"
        '
        'objSep2
        '
        Me.objSep2.Name = "objSep2"
        Me.objSep2.Size = New System.Drawing.Size(233, 6)
        Me.objSep2.Tag = "objSep2"
        Me.objSep2.Visible = False
        '
        'mnuGlobalAssign
        '
        Me.mnuGlobalAssign.Name = "mnuGlobalAssign"
        Me.mnuGlobalAssign.Size = New System.Drawing.Size(236, 22)
        Me.mnuGlobalAssign.Tag = "mnuGlobalAssign"
        Me.mnuGlobalAssign.Text = "Global Assign Goals"
        Me.mnuGlobalAssign.Visible = False
        '
        'mnuUpdatePercentage
        '
        Me.mnuUpdatePercentage.Name = "mnuUpdatePercentage"
        Me.mnuUpdatePercentage.Size = New System.Drawing.Size(236, 22)
        Me.mnuUpdatePercentage.Tag = "mnuUpdatePercentage"
        Me.mnuUpdatePercentage.Text = "Update Percentage Completed"
        Me.mnuUpdatePercentage.Visible = False
        '
        'mnuGetFileFormat
        '
        Me.mnuGetFileFormat.Name = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Size = New System.Drawing.Size(236, 22)
        Me.mnuGetFileFormat.Tag = "mnuGetFileFormat"
        Me.mnuGetFileFormat.Text = "Get &File Format"
        Me.mnuGetFileFormat.Visible = False
        '
        'mnuImportBSC_Planning
        '
        Me.mnuImportBSC_Planning.Name = "mnuImportBSC_Planning"
        Me.mnuImportBSC_Planning.Size = New System.Drawing.Size(236, 22)
        Me.mnuImportBSC_Planning.Tag = "mnuImportBSC_Planning"
        Me.mnuImportBSC_Planning.Text = "Import BSC Planning"
        Me.mnuImportBSC_Planning.Visible = False
        '
        'objSep1
        '
        Me.objSep1.Name = "objSep1"
        Me.objSep1.Size = New System.Drawing.Size(233, 6)
        Me.objSep1.Tag = "objSep1"
        Me.objSep1.Visible = False
        '
        'mnuPrintBSCForm
        '
        Me.mnuPrintBSCForm.Name = "mnuPrintBSCForm"
        Me.mnuPrintBSCForm.Size = New System.Drawing.Size(236, 22)
        Me.mnuPrintBSCForm.Tag = "mnuPrintBSCForm"
        Me.mnuPrintBSCForm.Text = "&Print BSC Form"
        Me.mnuPrintBSCForm.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblTotalWeight)
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.lblCaption1)
        Me.objFooter.Controls.Add(Me.lblCaption2)
        Me.objFooter.Controls.Add(Me.objpnl2)
        Me.objFooter.Controls.Add(Me.objpnl1)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 486)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(845, 55)
        Me.objFooter.TabIndex = 0
        '
        'objlblTotalWeight
        '
        Me.objlblTotalWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblTotalWeight.Location = New System.Drawing.Point(338, 22)
        Me.objlblTotalWeight.Name = "objlblTotalWeight"
        Me.objlblTotalWeight.Size = New System.Drawing.Size(392, 13)
        Me.objlblTotalWeight.TabIndex = 5
        Me.objlblTotalWeight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(97, 30)
        Me.btnOperations.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperations.TabIndex = 0
        Me.btnOperations.Text = "Operations"
        '
        'lblCaption1
        '
        Me.lblCaption1.Location = New System.Drawing.Point(166, 10)
        Me.lblCaption1.Name = "lblCaption1"
        Me.lblCaption1.Size = New System.Drawing.Size(166, 17)
        Me.lblCaption1.TabIndex = 3
        Me.lblCaption1.Text = "Submitted for Approval(s)."
        Me.lblCaption1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCaption2
        '
        Me.lblCaption2.Location = New System.Drawing.Point(166, 31)
        Me.lblCaption2.Name = "lblCaption2"
        Me.lblCaption2.Size = New System.Drawing.Size(166, 17)
        Me.lblCaption2.TabIndex = 4
        Me.lblCaption2.Text = "Finally Approved"
        Me.lblCaption2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnl2
        '
        Me.objpnl2.BackColor = System.Drawing.Color.Blue
        Me.objpnl2.Location = New System.Drawing.Point(125, 31)
        Me.objpnl2.Name = "objpnl2"
        Me.objpnl2.Size = New System.Drawing.Size(35, 17)
        Me.objpnl2.TabIndex = 2
        '
        'objpnl1
        '
        Me.objpnl1.BackColor = System.Drawing.Color.Green
        Me.objpnl1.Location = New System.Drawing.Point(125, 10)
        Me.objpnl1.Name = "objpnl1"
        Me.objpnl1.Size = New System.Drawing.Size(35, 17)
        Me.objpnl1.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(736, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objcolhccategory
        '
        Me.objcolhccategory.Tag = "objcolhccategory"
        Me.objcolhccategory.Text = ""
        Me.objcolhccategory.Width = 0
        '
        'frmViewPerformancePlanning
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(845, 541)
        Me.Controls.Add(Me.objSpc1)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.objFooter)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmViewPerformancePlanning"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Performance Planning"
        Me.objSpc1.Panel1.ResumeLayout(False)
        Me.objSpc1.Panel2.ResumeLayout(False)
        Me.objSpc1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbcPlanningView.ResumeLayout(False)
        Me.tabpBSC.ResumeLayout(False)
        CType(Me.dgvBSCData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpCompetencies.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objSpc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuSubmitApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuApproveSubmitted As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFinalSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUnlockFinalSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuGlobalAssign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUpdatePercentage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGetFileFormat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportBSC_Planning As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuPrintBSCForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents lblCaption1 As System.Windows.Forms.Label
    Friend WithEvents lblCaption2 As System.Windows.Forms.Label
    Friend WithEvents objpnl2 As System.Windows.Forms.Panel
    Friend WithEvents objpnl1 As System.Windows.Forms.Panel
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents tbcPlanningView As System.Windows.Forms.TabControl
    Friend WithEvents tabpBSC As System.Windows.Forms.TabPage
    Friend WithEvents tabpCompetencies As System.Windows.Forms.TabPage
    Friend WithEvents dgvBSCData As System.Windows.Forms.DataGridView
    Friend WithEvents objlblTotalWeight As System.Windows.Forms.Label
    Friend WithEvents lvAssignedCompetencies As eZee.Common.eZeeListView
    Friend WithEvents colhCompetencies As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhWeight As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhccategory As System.Windows.Forms.ColumnHeader
End Class
