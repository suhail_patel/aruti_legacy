﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMapScaleGroup_Fields
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMapScaleGroup_Fields))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbAssignementInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchScale = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchPerspective = New eZee.Common.eZeeGradientButton
        Me.cboPerspective = New System.Windows.Forms.ComboBox
        Me.objSCheckAll = New System.Windows.Forms.CheckBox
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.lvMapped = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhSCheck = New System.Windows.Forms.ColumnHeader
        Me.colhSPerspecive = New System.Windows.Forms.ColumnHeader
        Me.colhScaleGroup = New System.Windows.Forms.ColumnHeader
        Me.objcolhPeriod = New System.Windows.Forms.ColumnHeader
        Me.objcolhScaleMasterId = New System.Windows.Forms.ColumnHeader
        Me.objcolhPerspectiveId = New System.Windows.Forms.ColumnHeader
        Me.objstLine = New eZee.Common.eZeeStraightLine
        Me.lblPerspective = New System.Windows.Forms.Label
        Me.lvScaleList = New eZee.Common.eZeeListView(Me.components)
        Me.colhScale = New System.Windows.Forms.ColumnHeader
        Me.colhDescription = New System.Windows.Forms.ColumnHeader
        Me.cboScaleGroup = New System.Windows.Forms.ComboBox
        Me.lblScaleScoreGroup = New System.Windows.Forms.Label
        Me.lblMappedCaption = New System.Windows.Forms.Label
        Me.txtMappedCaption = New eZee.TextBox.AlphanumericTextBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbAssignementInfo.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbAssignementInfo)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(837, 417)
        Me.pnlMain.TabIndex = 0
        '
        'gbAssignementInfo
        '
        Me.gbAssignementInfo.BorderColor = System.Drawing.Color.Black
        Me.gbAssignementInfo.Checked = False
        Me.gbAssignementInfo.CollapseAllExceptThis = False
        Me.gbAssignementInfo.CollapsedHoverImage = Nothing
        Me.gbAssignementInfo.CollapsedNormalImage = Nothing
        Me.gbAssignementInfo.CollapsedPressedImage = Nothing
        Me.gbAssignementInfo.CollapseOnLoad = False
        Me.gbAssignementInfo.Controls.Add(Me.objbtnSearchScale)
        Me.gbAssignementInfo.Controls.Add(Me.objbtnSearchPerspective)
        Me.gbAssignementInfo.Controls.Add(Me.cboPerspective)
        Me.gbAssignementInfo.Controls.Add(Me.objSCheckAll)
        Me.gbAssignementInfo.Controls.Add(Me.btnDelete)
        Me.gbAssignementInfo.Controls.Add(Me.btnSave)
        Me.gbAssignementInfo.Controls.Add(Me.lvMapped)
        Me.gbAssignementInfo.Controls.Add(Me.objstLine)
        Me.gbAssignementInfo.Controls.Add(Me.lblPerspective)
        Me.gbAssignementInfo.Controls.Add(Me.lvScaleList)
        Me.gbAssignementInfo.Controls.Add(Me.cboScaleGroup)
        Me.gbAssignementInfo.Controls.Add(Me.lblScaleScoreGroup)
        Me.gbAssignementInfo.Controls.Add(Me.lblMappedCaption)
        Me.gbAssignementInfo.Controls.Add(Me.txtMappedCaption)
        Me.gbAssignementInfo.Controls.Add(Me.lblPeriod)
        Me.gbAssignementInfo.Controls.Add(Me.cboPeriod)
        Me.gbAssignementInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbAssignementInfo.ExpandedHoverImage = Nothing
        Me.gbAssignementInfo.ExpandedNormalImage = Nothing
        Me.gbAssignementInfo.ExpandedPressedImage = Nothing
        Me.gbAssignementInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAssignementInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAssignementInfo.HeaderHeight = 25
        Me.gbAssignementInfo.HeaderMessage = ""
        Me.gbAssignementInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAssignementInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAssignementInfo.HeightOnCollapse = 0
        Me.gbAssignementInfo.LeftTextSpace = 0
        Me.gbAssignementInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbAssignementInfo.Name = "gbAssignementInfo"
        Me.gbAssignementInfo.OpenHeight = 300
        Me.gbAssignementInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAssignementInfo.ShowBorder = True
        Me.gbAssignementInfo.ShowCheckBox = False
        Me.gbAssignementInfo.ShowCollapseButton = False
        Me.gbAssignementInfo.ShowDefaultBorderColor = True
        Me.gbAssignementInfo.ShowDownButton = False
        Me.gbAssignementInfo.ShowHeader = True
        Me.gbAssignementInfo.Size = New System.Drawing.Size(837, 362)
        Me.gbAssignementInfo.TabIndex = 1
        Me.gbAssignementInfo.Temp = 0
        Me.gbAssignementInfo.Text = "Assignement Information"
        Me.gbAssignementInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchScale
        '
        Me.objbtnSearchScale.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchScale.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchScale.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchScale.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchScale.BorderSelected = False
        Me.objbtnSearchScale.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchScale.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchScale.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchScale.Location = New System.Drawing.Point(409, 117)
        Me.objbtnSearchScale.Name = "objbtnSearchScale"
        Me.objbtnSearchScale.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchScale.TabIndex = 501
        '
        'objbtnSearchPerspective
        '
        Me.objbtnSearchPerspective.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchPerspective.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchPerspective.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchPerspective.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchPerspective.BorderSelected = False
        Me.objbtnSearchPerspective.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchPerspective.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchPerspective.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchPerspective.Location = New System.Drawing.Point(409, 90)
        Me.objbtnSearchPerspective.Name = "objbtnSearchPerspective"
        Me.objbtnSearchPerspective.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchPerspective.TabIndex = 500
        '
        'cboPerspective
        '
        Me.cboPerspective.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPerspective.DropDownWidth = 250
        Me.cboPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPerspective.FormattingEnabled = True
        Me.cboPerspective.Location = New System.Drawing.Point(125, 90)
        Me.cboPerspective.Name = "cboPerspective"
        Me.cboPerspective.Size = New System.Drawing.Size(278, 21)
        Me.cboPerspective.TabIndex = 499
        '
        'objSCheckAll
        '
        Me.objSCheckAll.AutoSize = True
        Me.objSCheckAll.Location = New System.Drawing.Point(456, 40)
        Me.objSCheckAll.Name = "objSCheckAll"
        Me.objSCheckAll.Size = New System.Drawing.Size(15, 14)
        Me.objSCheckAll.TabIndex = 497
        Me.objSCheckAll.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(724, 324)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(101, 30)
        Me.btnDelete.TabIndex = 2
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(327, 324)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(103, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lvMapped
        '
        Me.lvMapped.BackColorOnChecked = False
        Me.lvMapped.CheckBoxes = True
        Me.lvMapped.ColumnHeaders = Nothing
        Me.lvMapped.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhSCheck, Me.colhSPerspecive, Me.colhScaleGroup, Me.objcolhPeriod, Me.objcolhScaleMasterId, Me.objcolhPerspectiveId})
        Me.lvMapped.CompulsoryColumns = ""
        Me.lvMapped.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvMapped.FullRowSelect = True
        Me.lvMapped.GridLines = True
        Me.lvMapped.GroupingColumn = Nothing
        Me.lvMapped.HideSelection = False
        Me.lvMapped.Location = New System.Drawing.Point(449, 35)
        Me.lvMapped.MinColumnWidth = 50
        Me.lvMapped.MultiSelect = False
        Me.lvMapped.Name = "lvMapped"
        Me.lvMapped.OptionalColumns = ""
        Me.lvMapped.ShowMoreItem = False
        Me.lvMapped.ShowSaveItem = False
        Me.lvMapped.ShowSelectAll = True
        Me.lvMapped.ShowSizeAllColumnsToFit = True
        Me.lvMapped.Size = New System.Drawing.Size(377, 283)
        Me.lvMapped.Sortable = True
        Me.lvMapped.TabIndex = 493
        Me.lvMapped.UseCompatibleStateImageBehavior = False
        Me.lvMapped.View = System.Windows.Forms.View.Details
        '
        'objcolhSCheck
        '
        Me.objcolhSCheck.Tag = "objcolhSCheck"
        Me.objcolhSCheck.Text = ""
        Me.objcolhSCheck.Width = 25
        '
        'colhSPerspecive
        '
        Me.colhSPerspecive.Tag = "colhSPerspecive"
        Me.colhSPerspecive.Text = "Perspectives"
        Me.colhSPerspecive.Width = 150
        '
        'colhScaleGroup
        '
        Me.colhScaleGroup.Tag = "colhScaleGroup"
        Me.colhScaleGroup.Text = "Scale Group"
        Me.colhScaleGroup.Width = 180
        '
        'objcolhPeriod
        '
        Me.objcolhPeriod.Tag = "objcolhPeriod"
        Me.objcolhPeriod.Text = ""
        Me.objcolhPeriod.Width = 0
        '
        'objcolhScaleMasterId
        '
        Me.objcolhScaleMasterId.Tag = "objcolhScaleMasterId"
        Me.objcolhScaleMasterId.Text = ""
        Me.objcolhScaleMasterId.Width = 0
        '
        'objcolhPerspectiveId
        '
        Me.objcolhPerspectiveId.Tag = "objcolhPerspectiveId"
        Me.objcolhPerspectiveId.Text = ""
        Me.objcolhPerspectiveId.Width = 0
        '
        'objstLine
        '
        Me.objstLine.BackColor = System.Drawing.Color.Transparent
        Me.objstLine.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.objstLine.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objstLine.Location = New System.Drawing.Point(436, 24)
        Me.objstLine.Name = "objstLine"
        Me.objstLine.Size = New System.Drawing.Size(7, 339)
        Me.objstLine.TabIndex = 492
        '
        'lblPerspective
        '
        Me.lblPerspective.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPerspective.Location = New System.Drawing.Point(12, 93)
        Me.lblPerspective.Name = "lblPerspective"
        Me.lblPerspective.Size = New System.Drawing.Size(107, 16)
        Me.lblPerspective.TabIndex = 490
        Me.lblPerspective.Text = "Perspective"
        Me.lblPerspective.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lvScaleList
        '
        Me.lvScaleList.BackColorOnChecked = False
        Me.lvScaleList.ColumnHeaders = Nothing
        Me.lvScaleList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhScale, Me.colhDescription})
        Me.lvScaleList.CompulsoryColumns = ""
        Me.lvScaleList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvScaleList.FullRowSelect = True
        Me.lvScaleList.GridLines = True
        Me.lvScaleList.GroupingColumn = Nothing
        Me.lvScaleList.HideSelection = False
        Me.lvScaleList.Location = New System.Drawing.Point(125, 144)
        Me.lvScaleList.MinColumnWidth = 50
        Me.lvScaleList.MultiSelect = False
        Me.lvScaleList.Name = "lvScaleList"
        Me.lvScaleList.OptionalColumns = ""
        Me.lvScaleList.ShowMoreItem = False
        Me.lvScaleList.ShowSaveItem = False
        Me.lvScaleList.ShowSelectAll = True
        Me.lvScaleList.ShowSizeAllColumnsToFit = True
        Me.lvScaleList.Size = New System.Drawing.Size(305, 174)
        Me.lvScaleList.Sortable = True
        Me.lvScaleList.TabIndex = 487
        Me.lvScaleList.UseCompatibleStateImageBehavior = False
        Me.lvScaleList.View = System.Windows.Forms.View.Details
        '
        'colhScale
        '
        Me.colhScale.Tag = "colhScale"
        Me.colhScale.Text = "Scale"
        Me.colhScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.colhScale.Width = 70
        '
        'colhDescription
        '
        Me.colhDescription.Tag = "colhDescription"
        Me.colhDescription.Text = "Description"
        Me.colhDescription.Width = 230
        '
        'cboScaleGroup
        '
        Me.cboScaleGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboScaleGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboScaleGroup.FormattingEnabled = True
        Me.cboScaleGroup.Location = New System.Drawing.Point(125, 117)
        Me.cboScaleGroup.Name = "cboScaleGroup"
        Me.cboScaleGroup.Size = New System.Drawing.Size(278, 21)
        Me.cboScaleGroup.TabIndex = 485
        '
        'lblScaleScoreGroup
        '
        Me.lblScaleScoreGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScaleScoreGroup.Location = New System.Drawing.Point(12, 119)
        Me.lblScaleScoreGroup.Name = "lblScaleScoreGroup"
        Me.lblScaleScoreGroup.Size = New System.Drawing.Size(107, 16)
        Me.lblScaleScoreGroup.TabIndex = 484
        Me.lblScaleScoreGroup.Text = "Scale/Score Group"
        Me.lblScaleScoreGroup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMappedCaption
        '
        Me.lblMappedCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMappedCaption.Location = New System.Drawing.Point(12, 65)
        Me.lblMappedCaption.Name = "lblMappedCaption"
        Me.lblMappedCaption.Size = New System.Drawing.Size(107, 16)
        Me.lblMappedCaption.TabIndex = 483
        Me.lblMappedCaption.Text = "Mapped Field Name"
        Me.lblMappedCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMappedCaption
        '
        Me.txtMappedCaption.BackColor = System.Drawing.Color.White
        Me.txtMappedCaption.Flags = 0
        Me.txtMappedCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMappedCaption.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMappedCaption.Location = New System.Drawing.Point(125, 63)
        Me.txtMappedCaption.Name = "txtMappedCaption"
        Me.txtMappedCaption.ReadOnly = True
        Me.txtMappedCaption.Size = New System.Drawing.Size(305, 21)
        Me.txtMappedCaption.TabIndex = 482
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 37)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(107, 16)
        Me.lblPeriod.TabIndex = 480
        Me.lblPeriod.Text = "Assessment Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(125, 35)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(305, 21)
        Me.cboPeriod.TabIndex = 481
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 362)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(837, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(731, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmMapScaleGroup_Fields
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(837, 417)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMapScaleGroup_Fields"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Assign Scale Group"
        Me.pnlMain.ResumeLayout(False)
        Me.gbAssignementInfo.ResumeLayout(False)
        Me.gbAssignementInfo.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbAssignementInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents txtMappedCaption As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMappedCaption As System.Windows.Forms.Label
    Friend WithEvents cboScaleGroup As System.Windows.Forms.ComboBox
    Friend WithEvents lblScaleScoreGroup As System.Windows.Forms.Label
    Friend WithEvents lvScaleList As eZee.Common.eZeeListView
    Friend WithEvents colhScale As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents objstLine As eZee.Common.eZeeStraightLine
    Friend WithEvents lblPerspective As System.Windows.Forms.Label
    Friend WithEvents lvMapped As eZee.Common.eZeeListView
    Friend WithEvents objcolhSCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhSPerspecive As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhScaleGroup As System.Windows.Forms.ColumnHeader
    Friend WithEvents objSCheckAll As System.Windows.Forms.CheckBox
    Friend WithEvents objcolhPeriod As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhScaleMasterId As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhPerspectiveId As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboPerspective As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchScale As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchPerspective As eZee.Common.eZeeGradientButton
End Class
