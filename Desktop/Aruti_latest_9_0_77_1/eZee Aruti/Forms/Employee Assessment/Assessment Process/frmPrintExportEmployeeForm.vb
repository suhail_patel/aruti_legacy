﻿#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.IO

#End Region

Public Class frmPrintExportEmployeeForm

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPrintExportEmployeeForm"
    Private ObjAssesss_Form As clsAssessment_Form
    Private mDicPeriods As Dictionary(Of Integer, String)
    Private mdtEmployee As DataTable
    Private mvwEmployee As DataView
    Private mstrAdvanceFilter As String = ""
    Private mSzOriginalSize As Size = New Size(357, 117)
    Private mSzSinglePeriod As Size = New Size(357, 63)
    Private mSzMultiPrdSize As Size = New Size(357, 91)
    Private mLcOriginalLoct As Point = New Point(94, 60)
    Private mLcNewCbo1Locat As Point = New Point(94, 33)
    Private mLcNewCbo2Locat As Point = New Point(94, 60)
    Private mCbo1LocLabel As Point = New Point(8, 63)
    Private strFolderPath As String = String.Empty
    Private mDicReportName As Dictionary(Of Integer, String)
    Private eExportAction As enExportAction = enExportAction.None
    Private mintProgressCount As Integer = 0
    Private ePrintAction As enPrintAction = enPrintAction.None

#End Region

#Region " Form's Events "

    Private Sub frmPrintExportEmployeeForm_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        ObjAssesss_Form = Nothing
    End Sub

    Private Sub frmPrintExportEmployeeForm_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessment_Form.SetMessages()
            objfrm._Other_ModuleNames = "clsAssessment_Form"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPrintExportEmployeeForm_LanguageClick", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPrintExportEmployeeForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ObjAssesss_Form = New clsAssessment_Form(User._Object._Languageunkid, Company._Object._Companyunkid)
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call FillEmployee()
            If Company._Object._Code = "CCK" AndAlso ConfigParameter._Object._AssessmentReportTemplateId = enAssessmentReportTemplate.ART_TEMPLATE_13 Then
                chkProbationTemplate.Visible = True
                chkProbationTemplate.Checked = True
            Else
                chkProbationTemplate.Visible = False
                chkProbationTemplate.Checked = False
            End If
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End

            'If ConfigParameter._Object._AssessmentReportTemplateId = enAssessmentReportTemplate.ART_TEMPLATE_5 Then
            '    cboPeriod.Visible = False : lblPeriod.Visible = False
            '    lblFromPeriod.Location = lblPeriod.Location : cboFromPeriod.Location = cboPeriod.Location
            '    lblToPeriod.Location = mCbo1LocLabel : cboToPeriod.Location = mLcOriginalLoct
            '    gbMultiperiod.Size = mSzMultiPrdSize
            'Else
            '    cboPeriod.Visible = True : lblPeriod.Visible = True
            '    lblFromPeriod.Visible = False : cboFromPeriod.Visible = False
            '    lblToPeriod.Visible = False : cboToPeriod.Visible = False
            '    gbMultiperiod.Size = mSzSinglePeriod
            '    If Company._Object._Code = "CCK" AndAlso ConfigParameter._Object._AssessmentReportTemplateId = enAssessmentReportTemplate.ART_TEMPLATE_13 Then
            '        chkProbationTemplate.Visible = True
            '        chkProbationTemplate.Checked = True
            '    Else
            '        chkProbationTemplate.Visible = False
            '        chkProbationTemplate.Checked = False
            '    End If
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPrintExportEmployeeForm_Load", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objYear As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
            End With

            With cboFromPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
            End With

            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
            End With

            Dim iCboItems() As ComboBoxValue = {New ComboBoxValue(Language.getMessage(mstrModuleName, 100, "Select"), 0), _
                                                New ComboBoxValue(Language.getMessage(mstrModuleName, 101, "Excel (.xls)"), enExportAction.Excel), _
                                                New ComboBoxValue(Language.getMessage(mstrModuleName, 102, "Portable Document Format (.pdf)"), enExportAction.PDF), _
                                                New ComboBoxValue(Language.getMessage(mstrModuleName, 103, "HyperText Markup Language (.html)"), enExportAction.HTML), _
                                                New ComboBoxValue(Language.getMessage(mstrModuleName, 104, "Rich Text Format (.rtf)"), enExportAction.RichText), _
                                                New ComboBoxValue(Language.getMessage(mstrModuleName, 105, "Word Dcoument (.doc)"), enExportAction.Word) _
                                               }
            With cboExportFormat
                .Items.Clear()
                .Items.AddRange(iCboItems)
                .SelectedIndex = 0
                .SelectedValue = CType(cboExportFormat.SelectedItem, ComboBoxValue).Value
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombos.Dispose()
            ObjPeriod = Nothing
        End Try
    End Sub

    Private Sub FillEmployee()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsData As New DataSet
        Try
            dsData = ObjEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "List", , , , , , , , , , , , , , , , mstrAdvanceFilter)
            Dim dCol As New DataColumn
            dCol.DataType = GetType(System.Boolean)
            dCol.DefaultValue = False
            dCol.ColumnName = "ischeck"
            dsData.Tables(0).Columns.Add(dCol)

            mdtEmployee = dsData.Tables(0).Copy
            mvwEmployee = mdtEmployee.DefaultView

            dgvAEmployee.AutoGenerateColumns = False
            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEName.DataPropertyName = "EmpCodeName"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgvAEmployee.DataSource = mvwEmployee
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        Finally
            dsData.Dispose() : ObjEmp = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            ObjAssesss_Form.SetDefaultValue()

            If ConfigParameter._Object._AssessmentReportTemplateId <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Default Assessment Form template is not set in Aruti Configuration. Please go to following path to set default template." & vbCrLf & _
                                                    "Aruti Configuration -> Option -> Performance Management Tab"), enMsgBoxStyle.Information)
                Return False
            End If

            If CInt(cboExportFormat.SelectedIndex <= 0) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Export Type is compulsory information. Please select export type to continue."), enMsgBoxStyle.Information)
                cboExportFormat.Focus()
                Return False
            End If

            If cboPeriod.Visible = True AndAlso cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory information. Please select period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If cboFromPeriod.Visible = True AndAlso cboFromPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "From Period is compulsory information. Please select period to continue."), enMsgBoxStyle.Information)
                cboFromPeriod.Focus()
                Return False
            End If

            If cboToPeriod.Visible = True AndAlso cboToPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "To Period is compulsory information. Please select period to continue."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            End If

            mdtEmployee.AcceptChanges()
            If mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please check atleast one employee to continue."), enMsgBoxStyle.Information)
                dgvAEmployee.Focus()
                Return False
            End If

            If cboFromPeriod.Visible = True AndAlso cboToPeriod.Visible = True Then
                If cboToPeriod.SelectedIndex < cboPeriod.SelectedIndex Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, To Period cannot be less than From Period"), enMsgBoxStyle.Information)
                    cboToPeriod.Focus()
                    Exit Function
                End If

                mDicPeriods = New Dictionary(Of Integer, String)
                Dim objPrd As New clscommom_period_Tran : Dim dsList As New DataSet
                dsList = objPrd.GetList("List", enModuleReference.Assessment, FinancialYear._Object._YearUnkid, FinancialYear._Object._Database_Start_Date, True, 1, , True)
                Dim dview As DataView = Nothing
                If cboFromPeriod.SelectedIndex = cboToPeriod.SelectedIndex Then
                    dview = New DataView(dsList.Tables(0), "periodunkid = '" & CType(cboFromPeriod.SelectedItem, DataRowView).Item("periodunkid") & "'", "end_date", DataViewRowState.CurrentRows)
                Else
                    dview = New DataView(dsList.Tables(0), "start_date >= '" & CType(cboFromPeriod.SelectedItem, DataRowView).Item("start_date") & "' AND end_date <= '" & CType(cboToPeriod.SelectedItem, DataRowView).Item("end_date") & "'", "end_date", DataViewRowState.CurrentRows)
                End If
                mDicPeriods = dview.ToTable.AsEnumerable().ToDictionary(Of Integer, String)(Function(row) row.Field(Of Integer)("periodunkid"), Function(row) row.Field(Of String)("period_name"))
                objPrd = Nothing : dsList.Dispose()
            End If

            If cboFromPeriod.Visible = True AndAlso cboToPeriod.Visible = True Then
                ObjAssesss_Form._PeriodId = cboFromPeriod.SelectedValue
                ObjAssesss_Form._PeriodName = cboFromPeriod.Text
            Else
                ObjAssesss_Form._PeriodId = cboPeriod.SelectedValue
                ObjAssesss_Form._PeriodName = cboPeriod.Text
            End If
            ObjAssesss_Form._IsSelfAssignCompetencies = ConfigParameter._Object._Self_Assign_Competencies
            ObjAssesss_Form._IsPreviewDisplay = False
            ObjAssesss_Form._SelectedTemplateId = ConfigParameter._Object._AssessmentReportTemplateId
            ObjAssesss_Form._ScoringOptionId = ConfigParameter._Object._ScoringOptionId
            ObjAssesss_Form._EmployeeAsOnDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            ObjAssesss_Form._IsProbationTemplate = chkProbationTemplate.Checked


            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub EanbleDisableControls(ByVal blnFlag As Boolean)
        btnStopProcess.Enabled = Not blnFlag
        btnClose.Enabled = blnFlag : btnExport.Enabled = blnFlag : btnPrint.Enabled = blnFlag
        gbEmployee.Enabled = blnFlag
        gbFilterCriteria.Enabled = blnFlag
    End Sub

    Private Sub CommonOperation()
        Try
            If SetFilter() = False Then Exit Sub
            mDicReportName = mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).ToDictionary(Of Integer, String)(Function(row) row.Field(Of Integer)("employeeunkid"), Function(row) row.Field(Of String)("EmpCodeName"))
            pbProgress_Report.Maximum = mDicReportName.Keys.Count
            pbProgress_Report.Value = 0
            mintProgressCount = 1
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CommonOperation", mstrModuleName)
        Finally
        End Try
    End Sub


    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisibility()
        Try
            btnPrint.Enabled = User._Object.Privilege._AllowToPrintEmployeeScoreCard
            btnExport.Enabled = User._Object.Privilege._AllowToExportEmployeeScoreCard
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End

#End Region

#Region " Button's Events "

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            Call CommonOperation()
            eExportAction = enExportAction.None
            ePrintAction = enPrintAction.Print
            EanbleDisableControls(False)
            bgWorker.RunWorkerAsync()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnPrint_Click", mstrModuleName)
        Finally
            'EZeeFooter1.Enabled = True
            'pbProgress_Report.Value = 0
            'objlnkValue.Text = ""
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            Dim fdlg As New FolderBrowserDialog
            If fdlg.ShowDialog = Windows.Forms.DialogResult.OK Then
                strFolderPath = fdlg.SelectedPath
            End If
            Dim attr As FileAttributes = File.GetAttributes(strFolderPath)
            If Not (attr And FileAttributes.Directory) = FileAttributes.Directory Then
                Exit Sub
            End If
            Call CommonOperation()
            eExportAction = CType(cboExportFormat.SelectedItem, ComboBoxValue).Value
            ePrintAction = enPrintAction.None
            EanbleDisableControls(False)
            bgWorker.RunWorkerAsync()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objAlloacationReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objAlloacationReset.Click
        Try
            mstrAdvanceFilter = ""
            Call FillEmployee()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAlloacationReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnStopProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStopProcess.Click
        Try
            If bgWorker.IsBusy Then
                bgWorker.CancelAsync()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnStopProcess_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                Call FillEmployee()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = dgvAEmployee.Rows(dgvAEmployee.RowCount - 1).Index Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvAEmployee.Rows.Count > 0 Then
                        If dgvAEmployee.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvAEmployee.Rows(dgvAEmployee.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            mvwEmployee.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Data Grid Event(s) "

    Private Sub dgvAEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAEmployee.CellContentClick, dgvAEmployee.CellContentDoubleClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvAEmployee.IsCurrentCellDirty Then
                    Me.dgvAEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = mvwEmployee.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If mvwEmployee.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvAEmployee_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
            For Each dr As DataRowView In mvwEmployee
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
            Next
            dgvAEmployee.Refresh()
            AddHandler dgvAEmployee.CellContentClick, AddressOf dgvAEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Background Worker Event(s) "

    Private Sub bgWorker_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker.DoWork
        RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
        For Each ikey As Integer In mDicReportName.Keys
            If bgWorker.CancellationPending = True Then
                e.Cancel = True
                Call EanbleDisableControls(True)
                Exit For
            End If
            ObjAssesss_Form._ReportName = mDicReportName(ikey)
            ObjAssesss_Form._EmployeeId = ikey
            'Shani (13-Jan-2017) -- Start
            ObjAssesss_Form._FromExportScreen = True
            'Shani (13-Jan-2017) -- End
            ObjAssesss_Form.generateReportNew(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, True, strFolderPath, _
                                              False, 0, ePrintAction, eExportAction)
            bgWorker.ReportProgress(mintProgressCount)
            mintProgressCount = mintProgressCount + 1
        Next
        AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
    End Sub

    Private Sub bgWorker_RunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgWorker.RunWorkerCompleted
        Call EanbleDisableControls(True)
        pbProgress_Report.Value = 0
        objlnkValue.Text = ""
    End Sub

    Private Sub bgWorker_ProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgWorker.ProgressChanged
        pbProgress_Report.Value = e.ProgressPercentage
        objlnkValue.Text = Language.getMessage(mstrModuleName, 200, "Processed : ") & " " & pbProgress_Report.Value.ToString & " / " & pbProgress_Report.Maximum.ToString
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbMultiperiod.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMultiperiod.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployee.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployee.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnPrint.GradientBackColor = GUI._ButttonBackColor
            Me.btnPrint.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnStopProcess.GradientBackColor = GUI._ButttonBackColor
            Me.btnStopProcess.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.gbMultiperiod.Text = Language._Object.getCaption(Me.gbMultiperiod.Name, Me.gbMultiperiod.Text)
            Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.Name, Me.lblToPeriod.Text)
            Me.lblFromPeriod.Text = Language._Object.getCaption(Me.lblFromPeriod.Name, Me.lblFromPeriod.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.chkProbationTemplate.Text = Language._Object.getCaption(Me.chkProbationTemplate.Name, Me.chkProbationTemplate.Text)
            Me.gbEmployee.Text = Language._Object.getCaption(Me.gbEmployee.Name, Me.gbEmployee.Text)
            Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.pbProgress_Report.Text = Language._Object.getCaption(Me.pbProgress_Report.Name, Me.pbProgress_Report.Text)
            Me.btnPrint.Text = Language._Object.getCaption(Me.btnPrint.Name, Me.btnPrint.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnStopProcess.Text = Language._Object.getCaption(Me.btnStopProcess.Name, Me.btnStopProcess.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Default Assessment Form template is not set in Aruti Configuration. Please go to following path to set default template." & vbCrLf & _
                                                             "Aruti Configuration -> Option -> Performance Management Tab")
            Language.setMessage(mstrModuleName, 2, "Export Type is compulsory information. Please select export type to continue.")
            Language.setMessage(mstrModuleName, 3, "Period is compulsory information. Please select period to continue.")
            Language.setMessage(mstrModuleName, 4, "From Period is compulsory information. Please select period to continue.")
            Language.setMessage(mstrModuleName, 5, "To Period is compulsory information. Please select period to continue.")
            Language.setMessage(mstrModuleName, 6, "Please check atleast one employee to continue.")
            Language.setMessage(mstrModuleName, 7, "Sorry, To Period cannot be less than From Period")
            Language.setMessage(mstrModuleName, 100, "Select")
            Language.setMessage(mstrModuleName, 101, "Excel (.xls)")
            Language.setMessage(mstrModuleName, 102, "Portable Document Format (.pdf)")
            Language.setMessage(mstrModuleName, 103, "HyperText Markup Language (.html)")
            Language.setMessage(mstrModuleName, 104, "Rich Text Format (.rtf)")
            Language.setMessage(mstrModuleName, 105, "Word Dcoument (.doc)")
            Language.setMessage(mstrModuleName, 200, "Processed :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class