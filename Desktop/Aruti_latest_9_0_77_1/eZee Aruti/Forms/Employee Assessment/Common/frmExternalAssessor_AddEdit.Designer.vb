﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExternalAssessor_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExternalAssessor_AddEdit))
        Me.pnlAdvertiser = New System.Windows.Forms.Panel
        Me.gbExternalAssessor = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtDisplayName = New eZee.TextBox.AlphanumericTextBox
        Me.txtEmpCode = New eZee.TextBox.AlphanumericTextBox
        Me.txtAssessorName = New eZee.TextBox.AlphanumericTextBox
        Me.objbtnAddRelation = New eZee.Common.eZeeGradientButton
        Me.lblDisplayName = New System.Windows.Forms.Label
        Me.cboCity = New System.Windows.Forms.ComboBox
        Me.cboRelation = New System.Windows.Forms.ComboBox
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.txtEmail = New eZee.TextBox.AlphanumericTextBox
        Me.lblRelation = New System.Windows.Forms.Label
        Me.lblEmail = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.txtPhone = New eZee.TextBox.AlphanumericTextBox
        Me.lblPhone = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblCountry = New System.Windows.Forms.Label
        Me.lblCity = New System.Windows.Forms.Label
        Me.txtCompany = New eZee.TextBox.AlphanumericTextBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblAddress = New System.Windows.Forms.Label
        Me.lblAssessorName = New System.Windows.Forms.Label
        Me.lblstate = New System.Windows.Forms.Label
        Me.txtaddress = New eZee.TextBox.AlphanumericTextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlAdvertiser.SuspendLayout()
        Me.gbExternalAssessor.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlAdvertiser
        '
        Me.pnlAdvertiser.Controls.Add(Me.gbExternalAssessor)
        Me.pnlAdvertiser.Controls.Add(Me.objFooter)
        Me.pnlAdvertiser.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlAdvertiser.Location = New System.Drawing.Point(0, 0)
        Me.pnlAdvertiser.Name = "pnlAdvertiser"
        Me.pnlAdvertiser.Size = New System.Drawing.Size(464, 369)
        Me.pnlAdvertiser.TabIndex = 1
        '
        'gbExternalAssessor
        '
        Me.gbExternalAssessor.BorderColor = System.Drawing.Color.Black
        Me.gbExternalAssessor.Checked = False
        Me.gbExternalAssessor.CollapseAllExceptThis = False
        Me.gbExternalAssessor.CollapsedHoverImage = Nothing
        Me.gbExternalAssessor.CollapsedNormalImage = Nothing
        Me.gbExternalAssessor.CollapsedPressedImage = Nothing
        Me.gbExternalAssessor.CollapseOnLoad = False
        Me.gbExternalAssessor.Controls.Add(Me.txtDisplayName)
        Me.gbExternalAssessor.Controls.Add(Me.txtEmpCode)
        Me.gbExternalAssessor.Controls.Add(Me.txtAssessorName)
        Me.gbExternalAssessor.Controls.Add(Me.objbtnAddRelation)
        Me.gbExternalAssessor.Controls.Add(Me.lblDisplayName)
        Me.gbExternalAssessor.Controls.Add(Me.cboCity)
        Me.gbExternalAssessor.Controls.Add(Me.cboRelation)
        Me.gbExternalAssessor.Controls.Add(Me.cboState)
        Me.gbExternalAssessor.Controls.Add(Me.cboEmployee)
        Me.gbExternalAssessor.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbExternalAssessor.Controls.Add(Me.txtEmail)
        Me.gbExternalAssessor.Controls.Add(Me.lblRelation)
        Me.gbExternalAssessor.Controls.Add(Me.lblEmail)
        Me.gbExternalAssessor.Controls.Add(Me.lblEmployee)
        Me.gbExternalAssessor.Controls.Add(Me.txtPhone)
        Me.gbExternalAssessor.Controls.Add(Me.lblPhone)
        Me.gbExternalAssessor.Controls.Add(Me.cboCountry)
        Me.gbExternalAssessor.Controls.Add(Me.lblCountry)
        Me.gbExternalAssessor.Controls.Add(Me.lblCity)
        Me.gbExternalAssessor.Controls.Add(Me.txtCompany)
        Me.gbExternalAssessor.Controls.Add(Me.lblCompany)
        Me.gbExternalAssessor.Controls.Add(Me.lblAddress)
        Me.gbExternalAssessor.Controls.Add(Me.lblAssessorName)
        Me.gbExternalAssessor.Controls.Add(Me.lblstate)
        Me.gbExternalAssessor.Controls.Add(Me.txtaddress)
        Me.gbExternalAssessor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbExternalAssessor.ExpandedHoverImage = Nothing
        Me.gbExternalAssessor.ExpandedNormalImage = Nothing
        Me.gbExternalAssessor.ExpandedPressedImage = Nothing
        Me.gbExternalAssessor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbExternalAssessor.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbExternalAssessor.HeaderHeight = 25
        Me.gbExternalAssessor.HeaderMessage = ""
        Me.gbExternalAssessor.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbExternalAssessor.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbExternalAssessor.HeightOnCollapse = 0
        Me.gbExternalAssessor.LeftTextSpace = 0
        Me.gbExternalAssessor.Location = New System.Drawing.Point(0, 0)
        Me.gbExternalAssessor.Name = "gbExternalAssessor"
        Me.gbExternalAssessor.OpenHeight = 300
        Me.gbExternalAssessor.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbExternalAssessor.ShowBorder = True
        Me.gbExternalAssessor.ShowCheckBox = False
        Me.gbExternalAssessor.ShowCollapseButton = False
        Me.gbExternalAssessor.ShowDefaultBorderColor = True
        Me.gbExternalAssessor.ShowDownButton = False
        Me.gbExternalAssessor.ShowHeader = True
        Me.gbExternalAssessor.Size = New System.Drawing.Size(464, 314)
        Me.gbExternalAssessor.TabIndex = 2
        Me.gbExternalAssessor.Temp = 0
        Me.gbExternalAssessor.Text = "External Assessor"
        Me.gbExternalAssessor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDisplayName
        '
        Me.txtDisplayName.Flags = 0
        Me.txtDisplayName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDisplayName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDisplayName.Location = New System.Drawing.Point(202, 115)
        Me.txtDisplayName.Name = "txtDisplayName"
        Me.txtDisplayName.Size = New System.Drawing.Size(226, 21)
        Me.txtDisplayName.TabIndex = 3
        '
        'txtEmpCode
        '
        Me.txtEmpCode.BackColor = System.Drawing.Color.White
        Me.txtEmpCode.Flags = 0
        Me.txtEmpCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmpCode.Location = New System.Drawing.Point(100, 115)
        Me.txtEmpCode.Name = "txtEmpCode"
        Me.txtEmpCode.ReadOnly = True
        Me.txtEmpCode.Size = New System.Drawing.Size(96, 21)
        Me.txtEmpCode.TabIndex = 239
        Me.txtEmpCode.TabStop = False
        '
        'txtAssessorName
        '
        Me.txtAssessorName.Flags = 0
        Me.txtAssessorName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAssessorName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAssessorName.Location = New System.Drawing.Point(100, 88)
        Me.txtAssessorName.Name = "txtAssessorName"
        Me.txtAssessorName.Size = New System.Drawing.Size(328, 21)
        Me.txtAssessorName.TabIndex = 4
        '
        'objbtnAddRelation
        '
        Me.objbtnAddRelation.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddRelation.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddRelation.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddRelation.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddRelation.BorderSelected = False
        Me.objbtnAddRelation.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddRelation.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddRelation.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddRelation.Location = New System.Drawing.Point(434, 61)
        Me.objbtnAddRelation.Name = "objbtnAddRelation"
        Me.objbtnAddRelation.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddRelation.TabIndex = 236
        '
        'lblDisplayName
        '
        Me.lblDisplayName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisplayName.Location = New System.Drawing.Point(8, 117)
        Me.lblDisplayName.Name = "lblDisplayName"
        Me.lblDisplayName.Size = New System.Drawing.Size(89, 16)
        Me.lblDisplayName.TabIndex = 238
        Me.lblDisplayName.Text = "Display Name"
        Me.lblDisplayName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCity
        '
        Me.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCity.DropDownWidth = 300
        Me.cboCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCity.FormattingEnabled = True
        Me.cboCity.Location = New System.Drawing.Point(100, 231)
        Me.cboCity.Name = "cboCity"
        Me.cboCity.Size = New System.Drawing.Size(135, 21)
        Me.cboCity.TabIndex = 8
        '
        'cboRelation
        '
        Me.cboRelation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRelation.DropDownWidth = 300
        Me.cboRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRelation.FormattingEnabled = True
        Me.cboRelation.Location = New System.Drawing.Point(100, 61)
        Me.cboRelation.Name = "cboRelation"
        Me.cboRelation.Size = New System.Drawing.Size(328, 21)
        Me.cboRelation.TabIndex = 2
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.DropDownWidth = 200
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(305, 204)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(123, 21)
        Me.cboState.TabIndex = 7
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(100, 34)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(328, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(434, 34)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 235
        '
        'txtEmail
        '
        Me.txtEmail.Flags = 0
        Me.txtEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmail.Location = New System.Drawing.Point(100, 285)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(328, 21)
        Me.txtEmail.TabIndex = 10
        '
        'lblRelation
        '
        Me.lblRelation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRelation.Location = New System.Drawing.Point(8, 63)
        Me.lblRelation.Name = "lblRelation"
        Me.lblRelation.Size = New System.Drawing.Size(89, 16)
        Me.lblRelation.TabIndex = 116
        Me.lblRelation.Text = "Relation"
        Me.lblRelation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(8, 287)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(89, 16)
        Me.lblEmail.TabIndex = 136
        Me.lblEmail.Text = "Email"
        Me.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(89, 16)
        Me.lblEmployee.TabIndex = 118
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPhone
        '
        Me.txtPhone.Flags = 0
        Me.txtPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPhone.Location = New System.Drawing.Point(305, 231)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(123, 21)
        Me.txtPhone.TabIndex = 9
        '
        'lblPhone
        '
        Me.lblPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone.Location = New System.Drawing.Point(241, 233)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(58, 16)
        Me.lblPhone.TabIndex = 131
        Me.lblPhone.Text = "Phone"
        Me.lblPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.DropDownWidth = 300
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(100, 204)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(135, 21)
        Me.cboCountry.TabIndex = 6
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(8, 206)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(89, 16)
        Me.lblCountry.TabIndex = 129
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(8, 233)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(89, 16)
        Me.lblCity.TabIndex = 127
        Me.lblCity.Text = "City"
        Me.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCompany
        '
        Me.txtCompany.Flags = 0
        Me.txtCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompany.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCompany.Location = New System.Drawing.Point(100, 258)
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.Size = New System.Drawing.Size(328, 21)
        Me.txtCompany.TabIndex = 11
        '
        'lblCompany
        '
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(8, 260)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(89, 16)
        Me.lblCompany.TabIndex = 124
        Me.lblCompany.Text = "Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAddress
        '
        Me.lblAddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress.Location = New System.Drawing.Point(8, 146)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(89, 16)
        Me.lblAddress.TabIndex = 109
        Me.lblAddress.Text = "Res. Address"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAssessorName
        '
        Me.lblAssessorName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssessorName.Location = New System.Drawing.Point(8, 90)
        Me.lblAssessorName.Name = "lblAssessorName"
        Me.lblAssessorName.Size = New System.Drawing.Size(89, 16)
        Me.lblAssessorName.TabIndex = 120
        Me.lblAssessorName.Text = "Assessor Name"
        Me.lblAssessorName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblstate
        '
        Me.lblstate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblstate.Location = New System.Drawing.Point(241, 206)
        Me.lblstate.Name = "lblstate"
        Me.lblstate.Size = New System.Drawing.Size(58, 16)
        Me.lblstate.TabIndex = 112
        Me.lblstate.Text = "State"
        Me.lblstate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtaddress
        '
        Me.txtaddress.Flags = 0
        Me.txtaddress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtaddress.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtaddress.Location = New System.Drawing.Point(100, 142)
        Me.txtaddress.Multiline = True
        Me.txtaddress.Name = "txtaddress"
        Me.txtaddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtaddress.Size = New System.Drawing.Size(328, 56)
        Me.txtaddress.TabIndex = 5
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 314)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(464, 55)
        Me.objFooter.TabIndex = 20
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(358, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 13
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(258, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 12
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frmExternalAssessor_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(464, 369)
        Me.Controls.Add(Me.pnlAdvertiser)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExternalAssessor_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit External Assessor"
        Me.pnlAdvertiser.ResumeLayout(False)
        Me.gbExternalAssessor.ResumeLayout(False)
        Me.gbExternalAssessor.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlAdvertiser As System.Windows.Forms.Panel
    Friend WithEvents gbExternalAssessor As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents txtCompany As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents txtAssessorName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAssessorName As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents lblRelation As System.Windows.Forms.Label
    Friend WithEvents lblstate As System.Windows.Forms.Label
    Friend WithEvents txtaddress As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress As System.Windows.Forms.Label
    Friend WithEvents txtPhone As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents txtEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents cboCity As System.Windows.Forms.ComboBox
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents cboRelation As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddRelation As eZee.Common.eZeeGradientButton
    Friend WithEvents txtDisplayName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtEmpCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblDisplayName As System.Windows.Forms.Label
End Class
