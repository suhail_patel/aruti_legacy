﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAssessmentWeight

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentWeight"
    Private dView As DataView
    Private mdtValueTable As DataTable
    Private menAction As enAction = enAction.ADD_ONE
    Private mblnCancel As Boolean = True
    Private mintRatioUnkid As Integer = 0

    'S.SANDEEP [14 MAR 2016] -- START
    'Private objRatio As clsAssessment_Ratio
    Private objRatio As clsassess_ratio_master
    Private mstrRatioAllocationIds As String = String.Empty
    Private mstrPeriodRatioAssignedAllocIds As String = String.Empty
    'S.SANDEEP [14 MAR 2016] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintRatioUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintRatioUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "Period", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = 0
            End With

            'S.SANDEEP [14 MAR 2016] -- START
            Dim objMData As New clsMasterData
            dsCombo = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsCombo.Tables(0), "Id NOT IN(" & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 1
            End With
            'S.SANDEEP [14 MAR 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
        End Try
    End Sub


    'S.SANDEEP [14 MAR 2016] -- START
    Private Sub SetValue()
        'Private Sub SetValue(ByVal iJGrpId As Integer)
        'S.SANDEEP [14 MAR 2016] -- END

        Try

            'S.SANDEEP [14 MAR 2016] -- START
            'objRatio._Bsc_Weight = txtBSCWeight.Decimal
            'objRatio._Ge_Weight = txtGeneralWeight.Decimal
            'objRatio._Jobgroupunkid = iJGrpId
            'objRatio._Periodunkid = CInt(cboPeriod.SelectedValue)
            'objRatio._Isactive = True

            objRatio._Allocrefunkid = CInt(cboAllocations.SelectedValue)
            objRatio._Bsc_Weight = txtBSCWeight.Decimal
            objRatio._Employeeunkid = 0
            objRatio._Ge_Weight = txtGeneralWeight.Decimal
            objRatio._Isvoid = False
            objRatio._Periodunkid = CInt(cboPeriod.SelectedValue)
            objRatio._Userunkid = User._Object._Userunkid
            objRatio._Voiddatetime = Nothing
            objRatio._Voidreason = ""
            objRatio._Voiduserunkid = -1

            'S.SANDEEP [14 MAR 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [14 MAR 2016] -- START
    Private Sub Fill_Data()
        Try
            Dim dList As New DataSet
            Select Case CInt(cboAllocations.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_Grid(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_Grid(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_Grid(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_Grid(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_Grid(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_Grid(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_Grid(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_Grid(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_Grid(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_Grid(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_Grid(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_Grid(dList.Tables(0), "classesunkid", "name")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    'Private Sub Fill_Grid(ByVal iJobGrpId As Integer)
    '    Try
    '        Dim dtTable As DataTable
    '        Dim objJGrp As New clsJobGroup
    '        Dim dsList As New DataSet

    '        dsList = objJGrp.getComboList("List", False)
    '        If dsList.Tables(0).Rows.Count > 0 Then
    '            If iJobGrpId > 0 Then
    '                dtTable = New DataView(dsList.Tables(0), "jobgroupunkid = '" & iJobGrpId & "'", "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
    '            End If
    '        Else
    '            dtTable = dsList.Tables(0).Copy
    '        End If

    '        dtTable.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
    '        For i As Integer = 0 To dtTable.Rows.Count - 1
    '            dtTable.Rows(i).Item("ischeck") = IIf(iJobGrpId > 0, True, False)
    '        Next

    '        mdtJGTable = dtTable.Copy
    '        dJGView = mdtJGTable.DefaultView

    '        dgvJobGroup.AutoGenerateColumns = False
    '        objdgcolhJCheck.DataPropertyName = "ischeck"
    '        dgcolhJName.DataPropertyName = "name"
    '        objdgcolhJId.DataPropertyName = "jobgroupunkid"
    '        dgvJobGroup.DataSource = dJGView
    '        objJGrp = Nothing
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Private Sub Fill_Grid(ByVal dtTable As DataTable, ByVal strIdColName As String, ByVal strDisplayColName As String)
        Try
            dtTable.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
            Dim strFilterString As String = String.Empty

            If menAction <> enAction.EDIT_ONE Then
                If mstrPeriodRatioAssignedAllocIds.Trim.Length > 0 Then
                    strFilterString = strIdColName & " NOT IN(" & mstrPeriodRatioAssignedAllocIds & ")"
                End If
            Else
                If mstrRatioAllocationIds.Trim.Length > 0 Then
                    strFilterString = strIdColName & " IN(" & mstrRatioAllocationIds & ")"
                End If
            End If
            mdtValueTable = New DataView(dtTable, strFilterString, "", DataViewRowState.CurrentRows).ToTable.Copy
            dView = mdtValueTable.DefaultView
            dgvData.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "ischeck"
            dgcolhName.DataPropertyName = strDisplayColName
            objdgcolhId.DataPropertyName = strIdColName
            dgvData.DataSource = dView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [14 MAR 2016] -- END

    Private Sub GetValue()
        Try
            txtBSCWeight.Decimal = objRatio._Bsc_Weight
            txtGeneralWeight.Decimal = objRatio._Ge_Weight
            'S.SANDEEP [14 MAR 2016] -- START
            mstrRatioAllocationIds = objRatio._RatioAllocationIds
            'S.SANDEEP [14 MAR 2016] -- END

            cboPeriod.SelectedValue = objRatio._Periodunkid
            'S.SANDEEP [14 MAR 2016] -- START            

            Dim blnCall As Boolean = False
            If CInt(cboAllocations.SelectedValue) = CInt(IIf(objRatio._Allocrefunkid <= 0, 1, objRatio._Allocrefunkid)) Then
                blnCall = True
            End If
            cboAllocations.SelectedValue = IIf(objRatio._Allocrefunkid <= 0, 1, objRatio._Allocrefunkid)
            If blnCall Then
                Call cboAllocations_SelectedIndexChanged(New Object, New EventArgs)
            End If
            If mdtValueTable IsNot Nothing Then
                If mstrRatioAllocationIds.Trim.Length > 0 Then
                    For Each StrId As String In mstrRatioAllocationIds.Split(CChar(","))
                        Dim dtRow() As DataRow = mdtValueTable.Select(objdgcolhId.DataPropertyName & " = '" & StrId & "'")
                        If dtRow.Length >= 0 Then
                            dtRow(0)("ischeck") = True
                            dtRow(0).AcceptChanges()
                        End If
                    Next
                End If
            End If
            'S.SANDEEP [14 MAR 2016] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If txtGeneralWeight.Decimal <= 0 AndAlso txtBSCWeight.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Performance Evaluation percentages are mandatory information. Please set percentages."), enMsgBoxStyle.Information)
                txtGeneralWeight.Focus()
                Return False
            End If

            If txtGeneralWeight.Decimal > 0 AndAlso txtGeneralWeight.Decimal <> 100 Then
                If (txtGeneralWeight.Decimal + txtBSCWeight.Decimal) > 100 Or _
                   (txtGeneralWeight.Decimal + txtBSCWeight.Decimal) < 100 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, performance evaluation percentage can be between 0 and 100."), enMsgBoxStyle.Information)
                    txtGeneralWeight.Focus()
                    Return False
                End If
            ElseIf txtBSCWeight.Decimal > 0 AndAlso txtBSCWeight.Decimal <> 100 Then
                If (txtGeneralWeight.Decimal + txtBSCWeight.Decimal) > 100 Or _
                   (txtGeneralWeight.Decimal + txtBSCWeight.Decimal) < 100 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, performance evaluation percentage can be between 0 and 100."), enMsgBoxStyle.Information)
                    txtBSCWeight.Focus()
                    Return False
                End If
            End If

            Dim dtmp() As DataRow = Nothing
            mdtValueTable.AcceptChanges()
            dtmp = mdtValueTable.Select("ischeck = True")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Allocation is mandatory information. Please check atleast one Allocation."), enMsgBoxStyle.Information)
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmAssessmentWeight_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objRatio = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentWeight_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAssessmentWeight_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'S.SANDEEP [14 MAR 2016] -- START
        'objRatio = New clsAssessment_Ratio
        objRatio = New clsassess_ratio_master
        'S.SANDEEP [14 MAR 2016] -- END

        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call FillCombo()

            Call SetVisibility()

            If menAction = enAction.EDIT_ONE Then
                objRatio._Ratiounkid = mintRatioUnkid

                'S.SANDEEP [14 MAR 2016] -- START
                cboAllocations.Enabled = False
                'S.SANDEEP [14 MAR 2016] -- END

            End If

            Call GetValue()

            'S.SANDEEP [14 MAR 2016] -- START
            'Call Fill_Grid(objRatio._Jobgroupunkid)
            'S.SANDEEP [14 MAR 2016] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentWeight_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAssessmentWeight_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentWeight_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAssessmentWeight_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentWeight_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'S.SANDEEP [14 MAR 2016] -- START
            'clsAssessment_Ratio.SetMessages()
            'objfrm._Other_ModuleNames = "clsAssessment_Ratio"

            clsassess_ratio_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_ratio_master"
            'S.SANDEEP [14 MAR 2016] -- END

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            'S.SANDEEP [14 MAR 2016] -- START
            Dim blnFlag As Boolean = False
            If Is_Valid() = False Then Exit Sub
            Call SetValue()

            Dim strAllocationIds As String = ""
            Dim selectedTags As List(Of String) = (From p In mdtValueTable.AsEnumerable() Where p.IsNull("ischeck") = False AndAlso p.Field(Of Boolean)("ischeck") = True Select (p.Item(objdgcolhId.DataPropertyName).ToString)).ToList
            strAllocationIds = String.Join(", ", selectedTags.ToArray())
            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objRatio._FormName = mstrModuleName
            objRatio._LoginEmployeeunkid = 0
            objRatio._ClientIP = getIP()
            objRatio._HostName = getHostName()
            objRatio._FromWeb = False
            objRatio._AuditUserId = User._Object._Userunkid
objRatio._CompanyUnkid = Company._Object._Companyunkid
            objRatio._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objRatio.Update(strAllocationIds, ConfigParameter._Object._CurrentDateAndTime)
            Else
                blnFlag = objRatio.Insert(strAllocationIds, ConfigParameter._Object._CurrentDateAndTime)
            End If

            If blnFlag = False AndAlso objRatio._Message <> "" Then
                eZeeMsgBox.Show(objRatio._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If menAction = enAction.ADD_CONTINUE Then
                objRatio = Nothing
                objRatio = New clsassess_ratio_master
                Call GetValue()
                Call Fill_Data()
            Else
                Me.Close()
            End If

            'Dim blnFlag, blnIsDisplayed As Boolean
            'blnFlag = False : blnIsDisplayed = False
            'If Is_Valid() = False Then Exit Sub

            'Dim dGrp() As DataRow = Nothing
            'dGrp = mdtValueTable.Select("ischeck = True")

            'For i As Integer = 0 To dGrp.Length - 1
            '    Call SetValue(CInt(dGrp(i).Item("jobgroupunkid")))
            '    If mintRatioUnkid > 0 Then
            '        blnFlag = objRatio.Update()
            '    Else
            '        blnFlag = objRatio.Insert()
            '        If blnIsDisplayed = False Then
            '            If blnFlag = False AndAlso objRatio._Message <> "" Then
            '                If eZeeMsgBox.Show(objRatio._Message & Language.getMessage(mstrModuleName, 5, " Do you wish to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
            '                    Exit For
            '                Else
            '                    blnIsDisplayed = True
            '                End If
            '            End If
            '        End If
            '    End If
            'Next
            'If menAction = enAction.ADD_CONTINUE Then
            '    objRatio = Nothing
            '    objRatio = New clsassess_ratio_master            
            '    Call GetValue()            
            '    Call Fill_Grid(-1)            
            'Else
            '    Me.Close()
            'End If

            'S.SANDEEP [14 MAR 2016] -- END



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = dgvData.Rows(dgvData.RowCount - 1).Index Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvData.Rows.Count > 0 Then
                        If dgvData.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvData.Rows(dgvData.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim strSearch As String = ""
        Try
            If txtSearch.Text.Trim.Length > 0 Then
                strSearch = dgcolhName.DataPropertyName & " LIKE '%" & txtSearch.Text & "%' "
            End If

            dView.RowFilter = strSearch

        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkCheck.CheckedChanged
        Try
            For Each dr As DataRowView In dView
                dr.Item("ischeck") = CBool(objchkCheck.CheckState)
            Next
            dgvData.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkCheck_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [14 MAR 2016] -- START
    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            dgcolhName.HeaderText = cboAllocations.Text
            If CInt(cboPeriod.SelectedValue) > 0 Then Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If menAction <> enAction.EDIT_ONE Then
                If CInt(cboPeriod.SelectedValue) > 0 Then
                    Dim intAllocRefId As Integer = 0
                    intAllocRefId = objRatio.GetAllocReferenceId(CInt(cboPeriod.SelectedValue))
                    If intAllocRefId > 0 Then
                        cboAllocations.SelectedValue = intAllocRefId
                        cboAllocations.Enabled = False
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectionChangeCommitted
        Try
            mstrPeriodRatioAssignedAllocIds = objRatio.GetAllocationIdsByPeriod(CInt(cboPeriod.SelectedValue))
            Call cboAllocations_SelectedIndexChanged(sender, e)            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectionChangeCommitted", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [14 MAR 2016] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbRatio.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbRatio.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblGeneralEvaluation.Text = Language._Object.getCaption(Me.lblGeneralEvaluation.Name, Me.lblGeneralEvaluation.Text)
            Me.lblBalanceScoreCard.Text = Language._Object.getCaption(Me.lblBalanceScoreCard.Name, Me.lblBalanceScoreCard.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.gbRatio.Text = Language._Object.getCaption(Me.gbRatio.Name, Me.gbRatio.Text)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
            Me.dgcolhName.HeaderText = Language._Object.getCaption(Me.dgcolhName.Name, Me.dgcolhName.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Performance Evaluation percentages are mandatory information. Please set percentages.")
            Language.setMessage(mstrModuleName, 2, "Sorry, performance evaluation percentage can be in between 0 to 100.")
            Language.setMessage(mstrModuleName, 3, "Job Group is mandatory information. Please check atleast one Job Group.")
            Language.setMessage(mstrModuleName, 4, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 5, " Do you wish to continue?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class