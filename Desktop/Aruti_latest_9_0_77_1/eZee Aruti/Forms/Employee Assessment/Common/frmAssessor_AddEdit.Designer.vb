﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssessor_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAssessor_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.tabcAssessorReviewerInfo = New System.Windows.Forms.TabControl
        Me.tabpAssessor = New System.Windows.Forms.TabPage
        Me.tblpAssessor = New System.Windows.Forms.TableLayoutPanel
        Me.txtAssessor = New eZee.TextBox.AlphanumericTextBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.objchkAssessor = New System.Windows.Forms.CheckBox
        Me.dgvAssessor = New System.Windows.Forms.DataGridView
        Me.objdgcolhACheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhACode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtAssessorEmp = New eZee.TextBox.AlphanumericTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvAEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnAAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnAUnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.tabpReviewer = New System.Windows.Forms.TabPage
        Me.tblpReviewer = New System.Windows.Forms.TableLayoutPanel
        Me.txtReviewer = New eZee.TextBox.AlphanumericTextBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.chkReviewer = New System.Windows.Forms.CheckBox
        Me.dgvReviewer = New System.Windows.Forms.DataGridView
        Me.objdgcolhRCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhRCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhReviewer = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhRUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objbtnRAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnRUnAssign = New eZee.Common.eZeeLightButton(Me.components)
        Me.tblpReviewerEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtReviewerEmp = New eZee.TextBox.AlphanumericTextBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.chkREmployee = New System.Windows.Forms.CheckBox
        Me.dgvREmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhRECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhRECode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhREmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhREmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.tabcAssessorReviewerInfo.SuspendLayout()
        Me.tabpAssessor.SuspendLayout()
        Me.tblpAssessor.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvAssessor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabpReviewer.SuspendLayout()
        Me.tblpReviewer.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvReviewer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tblpReviewerEmployee.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvREmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.tabcAssessorReviewerInfo)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(594, 425)
        Me.pnlMainInfo.TabIndex = 0
        '
        'tabcAssessorReviewerInfo
        '
        Me.tabcAssessorReviewerInfo.Controls.Add(Me.tabpAssessor)
        Me.tabcAssessorReviewerInfo.Controls.Add(Me.tabpReviewer)
        Me.tabcAssessorReviewerInfo.Location = New System.Drawing.Point(2, 0)
        Me.tabcAssessorReviewerInfo.Name = "tabcAssessorReviewerInfo"
        Me.tabcAssessorReviewerInfo.SelectedIndex = 0
        Me.tabcAssessorReviewerInfo.Size = New System.Drawing.Size(591, 370)
        Me.tabcAssessorReviewerInfo.TabIndex = 105
        '
        'tabpAssessor
        '
        Me.tabpAssessor.Controls.Add(Me.tblpAssessor)
        Me.tabpAssessor.Controls.Add(Me.tblpAssessorEmployee)
        Me.tabpAssessor.Controls.Add(Me.objbtnAAssign)
        Me.tabpAssessor.Controls.Add(Me.objbtnAUnAssign)
        Me.tabpAssessor.Location = New System.Drawing.Point(4, 22)
        Me.tabpAssessor.Name = "tabpAssessor"
        Me.tabpAssessor.Size = New System.Drawing.Size(583, 344)
        Me.tabpAssessor.TabIndex = 0
        Me.tabpAssessor.Text = "Assessor"
        Me.tabpAssessor.UseVisualStyleBackColor = True
        '
        'tblpAssessor
        '
        Me.tblpAssessor.ColumnCount = 1
        Me.tblpAssessor.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessor.Controls.Add(Me.txtAssessor, 0, 0)
        Me.tblpAssessor.Controls.Add(Me.Panel2, 0, 1)
        Me.tblpAssessor.Location = New System.Drawing.Point(319, 3)
        Me.tblpAssessor.Name = "tblpAssessor"
        Me.tblpAssessor.RowCount = 2
        Me.tblpAssessor.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessor.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessor.Size = New System.Drawing.Size(261, 339)
        Me.tblpAssessor.TabIndex = 108
        '
        'txtAssessor
        '
        Me.txtAssessor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtAssessor.Flags = 0
        Me.txtAssessor.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtAssessor.Location = New System.Drawing.Point(3, 3)
        Me.txtAssessor.Name = "txtAssessor"
        Me.txtAssessor.Size = New System.Drawing.Size(255, 21)
        Me.txtAssessor.TabIndex = 106
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.objchkAssessor)
        Me.Panel2.Controls.Add(Me.dgvAssessor)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(3, 29)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(255, 307)
        Me.Panel2.TabIndex = 107
        '
        'objchkAssessor
        '
        Me.objchkAssessor.AutoSize = True
        Me.objchkAssessor.Location = New System.Drawing.Point(7, 5)
        Me.objchkAssessor.Name = "objchkAssessor"
        Me.objchkAssessor.Size = New System.Drawing.Size(15, 14)
        Me.objchkAssessor.TabIndex = 104
        Me.objchkAssessor.UseVisualStyleBackColor = True
        '
        'dgvAssessor
        '
        Me.dgvAssessor.AllowUserToAddRows = False
        Me.dgvAssessor.AllowUserToDeleteRows = False
        Me.dgvAssessor.AllowUserToResizeColumns = False
        Me.dgvAssessor.AllowUserToResizeRows = False
        Me.dgvAssessor.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAssessor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAssessor.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAssessor.ColumnHeadersHeight = 21
        Me.dgvAssessor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAssessor.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhACheck, Me.dgcolhACode, Me.dgcolhAName, Me.dgcolhAEmpId})
        Me.dgvAssessor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAssessor.Location = New System.Drawing.Point(0, 0)
        Me.dgvAssessor.MultiSelect = False
        Me.dgvAssessor.Name = "dgvAssessor"
        Me.dgvAssessor.RowHeadersVisible = False
        Me.dgvAssessor.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAssessor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAssessor.Size = New System.Drawing.Size(255, 307)
        Me.dgvAssessor.TabIndex = 105
        '
        'objdgcolhACheck
        '
        Me.objdgcolhACheck.HeaderText = ""
        Me.objdgcolhACheck.Name = "objdgcolhACheck"
        Me.objdgcolhACheck.Width = 25
        '
        'dgcolhACode
        '
        Me.dgcolhACode.HeaderText = "Code"
        Me.dgcolhACode.Name = "dgcolhACode"
        Me.dgcolhACode.ReadOnly = True
        Me.dgcolhACode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhACode.Width = 70
        '
        'dgcolhAName
        '
        Me.dgcolhAName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhAName.HeaderText = "Assessor"
        Me.dgcolhAName.Name = "dgcolhAName"
        Me.dgcolhAName.ReadOnly = True
        Me.dgcolhAName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAEmpId
        '
        Me.dgcolhAEmpId.HeaderText = "dgcolhAUnkid"
        Me.dgcolhAEmpId.Name = "dgcolhAEmpId"
        Me.dgcolhAEmpId.Visible = False
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtAssessorEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.Panel1, 0, 1)
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(3, 3)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(261, 339)
        Me.tblpAssessorEmployee.TabIndex = 107
        '
        'txtAssessorEmp
        '
        Me.txtAssessorEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtAssessorEmp.Flags = 0
        Me.txtAssessorEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtAssessorEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtAssessorEmp.Name = "txtAssessorEmp"
        Me.txtAssessorEmp.Size = New System.Drawing.Size(255, 21)
        Me.txtAssessorEmp.TabIndex = 106
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkEmployee)
        Me.Panel1.Controls.Add(Me.dgvAEmployee)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 29)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(255, 307)
        Me.Panel1.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvAEmployee
        '
        Me.dgvAEmployee.AllowUserToAddRows = False
        Me.dgvAEmployee.AllowUserToDeleteRows = False
        Me.dgvAEmployee.AllowUserToResizeColumns = False
        Me.dgvAEmployee.AllowUserToResizeRows = False
        Me.dgvAEmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvAEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvAEmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvAEmployee.ColumnHeadersHeight = 21
        Me.dgvAEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvAEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvAEmployee.MultiSelect = False
        Me.dgvAEmployee.Name = "dgvAEmployee"
        Me.dgvAEmployee.RowHeadersVisible = False
        Me.dgvAEmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvAEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAEmployee.Size = New System.Drawing.Size(255, 307)
        Me.dgvAEmployee.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'objbtnAAssign
        '
        Me.objbtnAAssign.BackColor = System.Drawing.Color.White
        Me.objbtnAAssign.BackgroundImage = CType(resources.GetObject("objbtnAAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAAssign.FlatAppearance.BorderSize = 0
        Me.objbtnAAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnAAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAAssign.Image = Global.Aruti.Main.My.Resources.Resources.Next_24x24
        Me.objbtnAAssign.Location = New System.Drawing.Point(270, 122)
        Me.objbtnAAssign.Name = "objbtnAAssign"
        Me.objbtnAAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnAAssign.TabIndex = 102
        Me.objbtnAAssign.UseVisualStyleBackColor = True
        '
        'objbtnAUnAssign
        '
        Me.objbtnAUnAssign.BackColor = System.Drawing.Color.White
        Me.objbtnAUnAssign.BackgroundImage = CType(resources.GetObject("objbtnAUnAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAUnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAUnAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAUnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAUnAssign.FlatAppearance.BorderSize = 0
        Me.objbtnAUnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAUnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAUnAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnAUnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAUnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAUnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAUnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAUnAssign.Image = Global.Aruti.Main.My.Resources.Resources.Previous_24x24
        Me.objbtnAUnAssign.Location = New System.Drawing.Point(270, 168)
        Me.objbtnAUnAssign.Name = "objbtnAUnAssign"
        Me.objbtnAUnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAUnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAUnAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnAUnAssign.TabIndex = 103
        Me.objbtnAUnAssign.UseVisualStyleBackColor = True
        '
        'tabpReviewer
        '
        Me.tabpReviewer.Controls.Add(Me.tblpReviewer)
        Me.tabpReviewer.Controls.Add(Me.objbtnRAssign)
        Me.tabpReviewer.Controls.Add(Me.objbtnRUnAssign)
        Me.tabpReviewer.Controls.Add(Me.tblpReviewerEmployee)
        Me.tabpReviewer.Location = New System.Drawing.Point(4, 22)
        Me.tabpReviewer.Name = "tabpReviewer"
        Me.tabpReviewer.Size = New System.Drawing.Size(583, 344)
        Me.tabpReviewer.TabIndex = 1
        Me.tabpReviewer.Text = "Reviewer"
        Me.tabpReviewer.UseVisualStyleBackColor = True
        '
        'tblpReviewer
        '
        Me.tblpReviewer.ColumnCount = 1
        Me.tblpReviewer.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpReviewer.Controls.Add(Me.txtReviewer, 0, 0)
        Me.tblpReviewer.Controls.Add(Me.Panel4, 0, 1)
        Me.tblpReviewer.Location = New System.Drawing.Point(319, 3)
        Me.tblpReviewer.Name = "tblpReviewer"
        Me.tblpReviewer.RowCount = 2
        Me.tblpReviewer.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpReviewer.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpReviewer.Size = New System.Drawing.Size(261, 339)
        Me.tblpReviewer.TabIndex = 111
        '
        'txtReviewer
        '
        Me.txtReviewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtReviewer.Flags = 0
        Me.txtReviewer.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtReviewer.Location = New System.Drawing.Point(3, 3)
        Me.txtReviewer.Name = "txtReviewer"
        Me.txtReviewer.Size = New System.Drawing.Size(255, 21)
        Me.txtReviewer.TabIndex = 106
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.chkReviewer)
        Me.Panel4.Controls.Add(Me.dgvReviewer)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(3, 29)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(255, 307)
        Me.Panel4.TabIndex = 107
        '
        'chkReviewer
        '
        Me.chkReviewer.AutoSize = True
        Me.chkReviewer.Location = New System.Drawing.Point(7, 5)
        Me.chkReviewer.Name = "chkReviewer"
        Me.chkReviewer.Size = New System.Drawing.Size(15, 14)
        Me.chkReviewer.TabIndex = 104
        Me.chkReviewer.UseVisualStyleBackColor = True
        '
        'dgvReviewer
        '
        Me.dgvReviewer.AllowUserToAddRows = False
        Me.dgvReviewer.AllowUserToDeleteRows = False
        Me.dgvReviewer.AllowUserToResizeColumns = False
        Me.dgvReviewer.AllowUserToResizeRows = False
        Me.dgvReviewer.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvReviewer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvReviewer.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvReviewer.ColumnHeadersHeight = 21
        Me.dgvReviewer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvReviewer.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhRCheck, Me.dgcolhRCode, Me.dgcolhReviewer, Me.objdgcolhRUnkid})
        Me.dgvReviewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvReviewer.Location = New System.Drawing.Point(0, 0)
        Me.dgvReviewer.MultiSelect = False
        Me.dgvReviewer.Name = "dgvReviewer"
        Me.dgvReviewer.RowHeadersVisible = False
        Me.dgvReviewer.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvReviewer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvReviewer.Size = New System.Drawing.Size(255, 307)
        Me.dgvReviewer.TabIndex = 105
        '
        'objdgcolhRCheck
        '
        Me.objdgcolhRCheck.HeaderText = ""
        Me.objdgcolhRCheck.Name = "objdgcolhRCheck"
        Me.objdgcolhRCheck.Width = 25
        '
        'dgcolhRCode
        '
        Me.dgcolhRCode.HeaderText = "Code"
        Me.dgcolhRCode.Name = "dgcolhRCode"
        Me.dgcolhRCode.ReadOnly = True
        Me.dgcolhRCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRCode.Width = 70
        '
        'dgcolhReviewer
        '
        Me.dgcolhReviewer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhReviewer.HeaderText = "Reviewer"
        Me.dgcolhReviewer.Name = "dgcolhReviewer"
        Me.dgcolhReviewer.ReadOnly = True
        Me.dgcolhReviewer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhRUnkid
        '
        Me.objdgcolhRUnkid.HeaderText = "dgcolhAUnkid"
        Me.objdgcolhRUnkid.Name = "objdgcolhRUnkid"
        Me.objdgcolhRUnkid.Visible = False
        '
        'objbtnRAssign
        '
        Me.objbtnRAssign.BackColor = System.Drawing.Color.White
        Me.objbtnRAssign.BackgroundImage = CType(resources.GetObject("objbtnRAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnRAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnRAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnRAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnRAssign.FlatAppearance.BorderSize = 0
        Me.objbtnRAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnRAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnRAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnRAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnRAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnRAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnRAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnRAssign.Image = Global.Aruti.Main.My.Resources.Resources.Next_24x24
        Me.objbtnRAssign.Location = New System.Drawing.Point(270, 122)
        Me.objbtnRAssign.Name = "objbtnRAssign"
        Me.objbtnRAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnRAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnRAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnRAssign.TabIndex = 109
        Me.objbtnRAssign.UseVisualStyleBackColor = True
        '
        'objbtnRUnAssign
        '
        Me.objbtnRUnAssign.BackColor = System.Drawing.Color.White
        Me.objbtnRUnAssign.BackgroundImage = CType(resources.GetObject("objbtnRUnAssign.BackgroundImage"), System.Drawing.Image)
        Me.objbtnRUnAssign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnRUnAssign.BorderColor = System.Drawing.Color.Empty
        Me.objbtnRUnAssign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnRUnAssign.FlatAppearance.BorderSize = 0
        Me.objbtnRUnAssign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnRUnAssign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnRUnAssign.ForeColor = System.Drawing.Color.Black
        Me.objbtnRUnAssign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnRUnAssign.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnRUnAssign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnRUnAssign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnRUnAssign.Image = Global.Aruti.Main.My.Resources.Resources.Previous_24x24
        Me.objbtnRUnAssign.Location = New System.Drawing.Point(270, 168)
        Me.objbtnRUnAssign.Name = "objbtnRUnAssign"
        Me.objbtnRUnAssign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnRUnAssign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnRUnAssign.Size = New System.Drawing.Size(42, 40)
        Me.objbtnRUnAssign.TabIndex = 110
        Me.objbtnRUnAssign.UseVisualStyleBackColor = True
        '
        'tblpReviewerEmployee
        '
        Me.tblpReviewerEmployee.ColumnCount = 1
        Me.tblpReviewerEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpReviewerEmployee.Controls.Add(Me.txtReviewerEmp, 0, 0)
        Me.tblpReviewerEmployee.Controls.Add(Me.Panel3, 0, 1)
        Me.tblpReviewerEmployee.Location = New System.Drawing.Point(3, 3)
        Me.tblpReviewerEmployee.Name = "tblpReviewerEmployee"
        Me.tblpReviewerEmployee.RowCount = 2
        Me.tblpReviewerEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpReviewerEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpReviewerEmployee.Size = New System.Drawing.Size(261, 339)
        Me.tblpReviewerEmployee.TabIndex = 108
        '
        'txtReviewerEmp
        '
        Me.txtReviewerEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtReviewerEmp.Flags = 0
        Me.txtReviewerEmp.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtReviewerEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtReviewerEmp.Name = "txtReviewerEmp"
        Me.txtReviewerEmp.Size = New System.Drawing.Size(255, 21)
        Me.txtReviewerEmp.TabIndex = 106
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.chkREmployee)
        Me.Panel3.Controls.Add(Me.dgvREmployee)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 29)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(255, 307)
        Me.Panel3.TabIndex = 107
        '
        'chkREmployee
        '
        Me.chkREmployee.AutoSize = True
        Me.chkREmployee.Location = New System.Drawing.Point(7, 5)
        Me.chkREmployee.Name = "chkREmployee"
        Me.chkREmployee.Size = New System.Drawing.Size(15, 14)
        Me.chkREmployee.TabIndex = 104
        Me.chkREmployee.UseVisualStyleBackColor = True
        '
        'dgvREmployee
        '
        Me.dgvREmployee.AllowUserToAddRows = False
        Me.dgvREmployee.AllowUserToDeleteRows = False
        Me.dgvREmployee.AllowUserToResizeColumns = False
        Me.dgvREmployee.AllowUserToResizeRows = False
        Me.dgvREmployee.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvREmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvREmployee.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvREmployee.ColumnHeadersHeight = 21
        Me.dgvREmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvREmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhRECheck, Me.dgcolhRECode, Me.dgcolhREmployee, Me.objdgcolhREmpId})
        Me.dgvREmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvREmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgvREmployee.MultiSelect = False
        Me.dgvREmployee.Name = "dgvREmployee"
        Me.dgvREmployee.RowHeadersVisible = False
        Me.dgvREmployee.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvREmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvREmployee.Size = New System.Drawing.Size(255, 307)
        Me.dgvREmployee.TabIndex = 105
        '
        'objdgcolhRECheck
        '
        Me.objdgcolhRECheck.HeaderText = ""
        Me.objdgcolhRECheck.Name = "objdgcolhRECheck"
        Me.objdgcolhRECheck.Width = 25
        '
        'dgcolhRECode
        '
        Me.dgcolhRECode.HeaderText = "Code"
        Me.dgcolhRECode.Name = "dgcolhRECode"
        Me.dgcolhRECode.ReadOnly = True
        Me.dgcolhRECode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhRECode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhRECode.Width = 70
        '
        'dgcolhREmployee
        '
        Me.dgcolhREmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhREmployee.HeaderText = "Employee"
        Me.dgcolhREmployee.Name = "dgcolhREmployee"
        Me.dgcolhREmployee.ReadOnly = True
        Me.dgcolhREmployee.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhREmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhREmpId
        '
        Me.objdgcolhREmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhREmpId.Name = "objdgcolhREmpId"
        Me.objdgcolhREmpId.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 370)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(594, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(382, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(485, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn2.HeaderText = "Emoloyee"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "dgcolhAUnkid"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 70
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 70
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn8.HeaderText = "Emoloyee"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "dgcolhAUnkid"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Width = 70
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn11.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "objdgcolhEmpId"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'frmAssessor_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(594, 425)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAssessor_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Assessor / Reviewer"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.tabcAssessorReviewerInfo.ResumeLayout(False)
        Me.tabpAssessor.ResumeLayout(False)
        Me.tblpAssessor.ResumeLayout(False)
        Me.tblpAssessor.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvAssessor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvAEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabpReviewer.ResumeLayout(False)
        Me.tblpReviewer.ResumeLayout(False)
        Me.tblpReviewer.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvReviewer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tblpReviewerEmployee.ResumeLayout(False)
        Me.tblpReviewerEmployee.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgvREmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnAUnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnAAssign As eZee.Common.eZeeLightButton
    Friend WithEvents objchkAssessor As System.Windows.Forms.CheckBox
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents tabcAssessorReviewerInfo As System.Windows.Forms.TabControl
    Friend WithEvents tabpAssessor As System.Windows.Forms.TabPage
    Friend WithEvents tabpReviewer As System.Windows.Forms.TabPage
    Friend WithEvents tblpAssessor As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtAssessor As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dgvAssessor As System.Windows.Forms.DataGridView
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtAssessorEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvAEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents tblpReviewerEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtReviewerEmp As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents chkREmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvREmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objbtnRAssign As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnRUnAssign As eZee.Common.eZeeLightButton
    Friend WithEvents tblpReviewer As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtReviewer As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents chkReviewer As System.Windows.Forms.CheckBox
    Friend WithEvents dgvReviewer As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhRECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhRECode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhREmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhREmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhACheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhACode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhRCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhRCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhReviewer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhRUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
