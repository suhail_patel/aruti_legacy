﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region "Imports  "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmReviewer_List

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmReviewer_List"
    Private objAssessor As clsAssessor
    Private mintUserMappunkid As Integer = -1

#End Region

#Region " Private Functions "

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim dTable As DataTable
        Dim StrSearch As String = String.Empty
        Try
            If User._Object.Privilege._AllowtoViewReviewerAccessList = True Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objAssessor.GetList("List", True, False)

                'If CInt(cboReviewer.SelectedValue) > 0 Then
                '    StrSearch &= "AND assessormasterunkid = '" & CInt(cboReviewer.SelectedValue) & "' "
                'End If
                'If CInt(cboDepartment.SelectedValue) > 0 Then
                '    StrSearch &= "AND DeptId = '" & CInt(cboDepartment.SelectedValue) & "' "
                'End If
                ''Shani [ 09 DEC 2014 ] -- START
                ''
                'If CInt(cboUser.SelectedValue) > 0 Then
                '    StrSearch &= "AND mapuserunkid = '" & CInt(cboUser.SelectedValue) & "' "
                'End If
                ''Shani [ 09 DEC 2014 ] -- END
                'If StrSearch.Trim.Length > 0 Then
                '    StrSearch = StrSearch.Substring(3)
                '    dTable = New DataView(dsList.Tables("List"), StrSearch, "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dTable = New DataView(dsList.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
                'End If

                If CInt(cboReviewer.SelectedValue) > 0 Then
                    StrSearch &= "AND hrassessor_master.assessormasterunkid = '" & CInt(cboReviewer.SelectedValue) & "' "
                End If

                'Shani(01-MAR-2016) -- Start
                'Enhancement:PA External Approver Flow
                'If CInt(cboDepartment.SelectedValue) > 0 Then
                '    StrSearch &= "AND hrdepartment_master.departmentunkid = '" & CInt(cboDepartment.SelectedValue) & "' "
                'End If
                'Shani(01-MAR-2016)-- End

                If CInt(cboUser.SelectedValue) > 0 Then
                    StrSearch &= "AND ISNULL(hrapprover_usermapping.userunkid,0) = '" & CInt(cboUser.SelectedValue) & "' "
                End If

                If StrSearch.Trim.Length > 0 Then
                    StrSearch = StrSearch.Substring(3)
                End If

                'S.SANDEEP [27 DEC 2016] -- START
                'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                Dim strVisibleTypeIds As Integer = clsAssessor.enARVisibilityTypeId.VISIBLE
                'S.SANDEEP [27 DEC 2016] -- END

                dsList = objAssessor.GetList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, strVisibleTypeIds.ToString, False, StrSearch)

                dTable = New DataView(dsList.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
                'S.SANDEEP [04 JUN 2015] -- END

                lvAssessorList.Items.Clear()
                Dim lvItem As New ListViewItem
                For Each dtRow As DataRow In dTable.Rows
                    lvItem = New ListViewItem

                    lvItem.Text = dtRow.Item("assessorname").ToString
                    lvItem.SubItems.Add(dtRow.Item("Department").ToString)
                    lvItem.SubItems.Add(dtRow.Item("usermapped").ToString)

                    lvItem.Tag = dtRow.Item("assessormasterunkid")
                    'Shani(01-MAR-2016) -- Start
                    'Enhancement:PA External Approver Flow
                    lvItem.SubItems.Add(dtRow.Item("IsExAR").ToString)
                    'Shani(01-MAR-2016)-- End
                    lvAssessorList.Items.Add(lvItem)
                Next
                If lvAssessorList.Items.Count > 10 Then
                    colhDepartment.Width = 220 - 18
                Else
                    colhDepartment.Width = 220
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'btnNew.Enabled = User._Object.Privilege._AddAssessorAccess
            'btnEdit.Enabled = User._Object.Privilege._EditAssessorAccess
            'btnDelete.Enabled = User._Object.Privilege._DeleteAssessorAccess
            'mnuMapUser.Enabled = User._Object.Privilege._AllowMapAssessorwithUser            

            btnNew.Enabled = User._Object.Privilege._AllowtoAddReviewerAccess
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditReviewerAccess
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteReviewerAccess
            mnuImportAssessormnuImportReviewer.Enabled = User._Object.Privilege._AllowtoImportReviewer
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objAssessor As New clsAssessor
        Dim objDept As New clsDepartment
        Dim objUsr As New clsUserAddEdit
        Dim objPswd As New clsPassowdOptions
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objAssessor.GetList("List", True, True)

            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            Dim strVisibleTypeIds As Integer = clsAssessor.enARVisibilityTypeId.VISIBLE
            'S.SANDEEP [27 DEC 2016] -- END
            dsCombo = objAssessor.GetList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._IsIncludeInactiveEmp, "List", True, strVisibleTypeIds.ToString, True)

            'S.SANDEEP [04 JUN 2015] -- END

            With cboReviewer
                .ValueMember = "assessormasterunkid"
                .DisplayMember = "assessorname"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            'Shani(01-MAR-2016) -- Start
            'Enhancement:PA External Approver Flow
            'dsCombo = objDept.getComboList("List", True)
            'With cboDepartment
            '    .ValueMember = "departmentunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("List")
            '    .SelectedValue = 0
            'End With
            'Shani(01-MAR-2016)-- End

            'Shani [ 09 DEC 2014 ] -- START
            '

            'S.SANDEEP [10 AUG 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objPswd._IsEmployeeAsUser Then
            '    dsCombo = objUsr.getComboList("User", True, False, True, Company._Object._Companyunkid, 506, FinancialYear._Object._YearUnkid)
            'Else
            '    dsCombo = objUsr.getComboList("User", True, False, False, , 506)
            'End If

            'S.SANDEEP [05 SEP 2016] -- START
            'ENHANCEMENT : NEW STATUS {BY ANDREW}
            'dsCombo = objUsr.getNewComboList("User", , True, Company._Object._Companyunkid, 506, FinancialYear._Object._YearUnkid)
            Dim dsFill As New DataSet

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'dsFill = objUsr.getNewComboList("User", , False, Company._Object._Companyunkid, 867, FinancialYear._Object._YearUnkid)
            'dsCombo = objUsr.getNewComboList("User", , True, Company._Object._Companyunkid, 868, FinancialYear._Object._YearUnkid)
            'Dim dRows As DataRow() = dsFill.Tables("User").Select("")
            'dRows.ToList.ForEach(Function(x) Add_DataRow(x, dsCombo))

            Dim strPrivilegeIds As String = "867,868"
            dsCombo = objUsr.getNewComboList("User", , True, Company._Object._Companyunkid, strPrivilegeIds, FinancialYear._Object._YearUnkid)
            'S.SANDEEP [20-JUN-2018] -- END

            'S.SANDEEP [05 SEP 2016] -- END

            'S.SANDEEP [01 AUG 2015] -- END


            With cboUser
                .ValueMember = "userunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("User")
                .SelectedValue = 0
            End With
            'Shani [ 09 DEC 2014 ] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [05 SEP 2016] -- START
    'ENHANCEMENT : NEW STATUS {BY ANDREW}
    Private Function Add_DataRow(ByVal dr As DataRow, ByVal dsData As DataSet) As Boolean
        Try
            Dim xRow As DataRow() = Nothing
            xRow = dsData.Tables(0).Select("userunkid = '" & dr.Item("userunkid").ToString & "' ")
            If xRow.Length <= 0 Then
                dsData.Tables(0).ImportRow(dr)
            End If
            dsData.AcceptChanges()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Add_DataRow", mstrModuleName)
        Finally
        End Try
        Return True
    End Function
    'S.SANDEEP [05 SEP 2016] -- END


#End Region

#Region " Form's Events "

    Private Sub frmReviewer_List_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objAssessor = New clsAssessor
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call FillCombo()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReviewer_List_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssessor.SetMessages()
            objfrm._Other_ModuleNames = "clsAssessor"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmAssessorReviewerAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(enAction.ADD_CONTINUE, False, -1) = True Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmAssessorReviewerAddEdit
        Try
            If lvAssessorList.SelectedItems.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Reviewer from the list to perform further operation."), enMsgBoxStyle.Information)
                lvAssessorList.Select()
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(enAction.EDIT_ONE, False, CInt(lvAssessorList.SelectedItems(0).Tag)) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvAssessorList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Reviewer from the list to perform further operation."), enMsgBoxStyle.Information)
                lvAssessorList.Select()
                Exit Sub
            End If

            If objAssessor.isUsed(CInt(lvAssessorList.SelectedItems(0).Tag)) = True Then
                eZeeMsgBox.Show(objAssessor._Message, enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Reviewer?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim sVoidReason As String = String.Empty
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                frm.displayDialog(enVoidCategoryType.ASSESSMENT, sVoidReason)
                If sVoidReason.Trim.Length <= 0 Then Exit Sub

                objAssessor._Isvoid = True
                objAssessor._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objAssessor._Voidreason = sVoidReason
                objAssessor._Voiduserunkid = User._Object._Userunkid


                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objAssessor._FormName = mstrModuleName
                objAssessor._LoginEmployeeunkid = 0
                objAssessor._ClientIP = getIP()
                objAssessor._HostName = getHostName()
                objAssessor._FromWeb = False
                objAssessor._AuditUserId = User._Object._Userunkid
objAssessor._CompanyUnkid = Company._Object._Companyunkid
                objAssessor._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objAssessor.Delete(CInt(lvAssessorList.SelectedItems(0).Tag))
                lvAssessorList.SelectedItems(0).Remove()
                Fill_List()
                If lvAssessorList.Items.Count <= 0 Then
                    Exit Try
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboReviewer.SelectedValue = 0
            'cboDepartment.SelectedValue = 0 'Shani(01-MAR-2016)
            cboUser.SelectedValue = 0
            lvAssessorList.Items.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchReviewer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchReviewer.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboReviewer.ValueMember
                .DisplayMember = cboReviewer.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboReviewer.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboReviewer.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReviewer_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Shani [ 09 DEC 2014 ] -- START
    '
    Private Sub objbtnSearchUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            With frm
                .ValueMember = cboUser.ValueMember
                .CodeMember = cboUser.DisplayMember
                .DisplayMember = "Display"
                .DataSource = CType(cboUser.DataSource, DataTable)
            End With
            If frm.DisplayDialog Then
                cboUser.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Shani [ 09 DEC 2014 ] -- END

#End Region

#Region " Control's Events "

    Private Sub mnuImportAssessormnuImportReviewer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportAssessormnuImportReviewer.Click
        Dim frm As New frmImportAssessorReviewer
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._IsReviewer = True
            frm.ShowDialog()
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuImportAssessormnuImportReviewer_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblReviewer.Text = Language._Object.getCaption(Me.lblReviewer.Name, Me.lblReviewer.Text)
            Me.colhAssessor.Text = Language._Object.getCaption(CStr(Me.colhAssessor.Tag), Me.colhAssessor.Text)
            Me.colhDepartment.Text = Language._Object.getCaption(CStr(Me.colhDepartment.Tag), Me.colhDepartment.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuMapUser.Text = Language._Object.getCaption(Me.mnuMapUser.Name, Me.mnuMapUser.Text)
            Me.mnuImportAssessormnuImportReviewer.Text = Language._Object.getCaption(Me.mnuImportAssessormnuImportReviewer.Name, Me.mnuImportAssessormnuImportReviewer.Text)
            Me.colhusermapped.Text = Language._Object.getCaption(CStr(Me.colhusermapped.Tag), Me.colhusermapped.Text)
            Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.Name, Me.lblUser.Text)
            Me.colhExternal.Text = Language._Object.getCaption(CStr(Me.colhExternal.Tag), Me.colhExternal.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Reviewer from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Reviewer?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class


'Public Class frmReviewer_List

'#Region " Private Variables "

'    Private ReadOnly mstrModuleName As String = "frmReviewer_List"
'    Private objAssessorTran As clsAssessor_tran
'    Private mintUserMappunkid As Integer = -1

'#End Region

'#Region " Private Functions "

'    Private Sub Fill_List()
'        Dim dsList As New DataSet
'        Dim lvItem As ListViewItem
'        Dim dTable As DataTable
'        Dim StrSearch As String = String.Empty
'        Try

'            If User._Object.Privilege._AllowToViewReviewerAccessList = True Then   'Pinkal (09-Jul-2012) -- Start

'                dsList = objAssessorTran.GetList("Assessor", True)

'                If CInt(cboReviewer.SelectedValue) > 0 Then
'                    StrSearch &= "AND assessormasterunkid = '" & CInt(cboReviewer.SelectedValue) & "' "
'                End If

'                If CInt(cboDepartment.SelectedValue) > 0 Then
'                    StrSearch &= "AND departmentunkid = '" & CInt(cboDepartment.SelectedValue) & "' "
'                End If

'                If StrSearch.Trim.Length > 0 Then
'                    StrSearch = StrSearch.Substring(3)
'                    dTable = New DataView(dsList.Tables("Assessor"), StrSearch, "", DataViewRowState.CurrentRows).ToTable
'                Else
'                    dTable = New DataView(dsList.Tables("Assessor"), "", "", DataViewRowState.CurrentRows).ToTable
'                End If

'                lvAssessorList.Items.Clear()

'                For Each dtRow As DataRow In dTable.Rows
'                    lvItem = New ListViewItem

'                    lvItem.Text = dtRow.Item("assessor").ToString
'                    lvItem.SubItems.Add(dtRow.Item("Department").ToString)

'                    'S.SANDEEP [ 25 JULY 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    lvItem.SubItems.Add(dtRow.Item("usermapped").ToString)
'                    'S.SANDEEP [ 25 JULY 2013 ] -- END

'                    lvItem.Tag = dtRow.Item("assessormasterunkid")
'                    lvAssessorList.Items.Add(lvItem)

'                    lvItem = Nothing
'                Next


'                'Pinkal (12-Jun-2012) -- Start
'                'Enhancement : TRA Changes

'                'If lvAssessorList.Items.Count > 10 Then
'                '    colhDepartment.Width = 595 - 18
'                'Else
'                '    colhDepartment.Width = 595
'                'End If

'                'lvAssessorList.GridLines = False
'                'lvAssessorList.GroupingColumn = colhAssessor
'                'lvAssessorList.DisplayGroups(True)

'                If lvAssessorList.Items.Count > 10 Then
'                    colhDepartment.Width = 220 - 18
'                Else
'                    colhDepartment.Width = 220
'                End If

'            End If

'            'Pinkal (12-Jun-2012) -- End

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message & " " & ex.StackTrace, "Fill_List", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub SetVisibility()

'        Try
'            'S.SANDEEP [ 16 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
'            'btnNew.Enabled = User._Object.Privilege._AddAssessorAccess
'            'btnEdit.Enabled = User._Object.Privilege._EditAssessorAccess
'            'btnDelete.Enabled = User._Object.Privilege._DeleteAssessorAccess
'            'btnMapUser.Enabled = User._Object.Privilege._AllowMapAssessorwithUser

'            btnNew.Enabled = User._Object.Privilege._Add_Reviewer_Access
'            btnEdit.Enabled = User._Object.Privilege._Edit_Reviewer_Access
'            btnDelete.Enabled = User._Object.Privilege._Delete_Reviewer_Access

'            'S.SANDEEP [ 17 JULY 2013 ] -- START
'            'ENHANCEMENT : OTHER CHANGES
'            'btnMapUser.Enabled = User._Object.Privilege._AllowToMapReviewerWithUser
'            mnuMapUser.Enabled = User._Object.Privilege._AllowToMapReviewerWithUser
'            'S.SANDEEP [ 17 JULY 2013 ] -- END



'            'S.SANDEEP [ 16 MAY 2012 ] -- END
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        End Try

'    End Sub

'    Private Sub FillCombo()
'        Dim dsCombo As New DataSet
'        Dim objAssessor As New clsAssessor
'        Dim objDept As New clsDepartment
'        Try
'            dsCombo = objAssessor.GetList("List", True, True)
'            With cboReviewer
'                .ValueMember = "assessormasterunkid"
'                .DisplayMember = "assessorname"
'                .DataSource = dsCombo.Tables("List")
'                .SelectedValue = 0
'            End With

'            dsCombo = objDept.getComboList("List", True)
'            With cboDepartment
'                .ValueMember = "departmentunkid"
'                .DisplayMember = "name"
'                .DataSource = dsCombo.Tables("List")
'                .SelectedValue = 0
'            End With

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'            dsCombo.Dispose() : objAssessor = Nothing
'        End Try
'    End Sub

'#End Region

'#Region " Form's Events "

'    Private Sub frmAssessor_List_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        objAssessorTran = New clsAssessor_tran
'        Try
'            Call Set_Logo(Me, gApplicationType)

'            Language.setLanguage(Me.Name)
'            Call OtherSettings()

'            Call SetVisibility()

'            Call FillCombo()

'            Call Fill_List()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmAssessor_List_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            'clsAssessor_tran.SetMessages()
'            objfrm._Other_ModuleNames = "clsAssessor_tran"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub

'#End Region

'#Region " Button's Events "

'    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
'        Try
'            Dim objfrm As New frmReviewer_AddEdit

'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            objfrm.displayDialog(enAction.ADD_CONTINUE, -1)

'            Fill_List()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

'        If lvAssessorList.SelectedItems.Count = 0 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assessor from the list to perform further operation on it."), enMsgBoxStyle.Information)
'            lvAssessorList.Select()
'            Exit Sub
'        End If

'        Dim objfrm As New frmReviewer_AddEdit
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvAssessorList.SelectedItems(0).Index

'            objfrm.displayDialog(enAction.EDIT_ONE, CInt(lvAssessorList.SelectedItems(0).Tag))

'            Fill_List()

'            objfrm = Nothing

'            lvAssessorList.Select()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        Dim objAssessor As New clsAssessor_tran
'        If lvAssessorList.SelectedItems.Count < 1 Then
'            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assessor from the list to perform further operation on it."), enMsgBoxStyle.Information)
'            lvAssessorList.Select()
'            Exit Sub
'        End If

'        Try
'            Dim intSelectedIndex As Integer
'            intSelectedIndex = lvAssessorList.SelectedItems(0).Index

'            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Assessment?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
'                objAssessor.Delete(CInt(lvAssessorList.SelectedItems(0).Tag))
'                lvAssessorList.SelectedItems(0).Remove()
'                Fill_List()

'                If lvAssessorList.Items.Count <= 0 Then
'                    Exit Try
'                End If
'            End If
'            lvAssessorList.Select()
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
'        Try
'            cboReviewer.SelectedValue = 0
'            cboDepartment.SelectedValue = 0
'            Call Fill_List()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
'        Try
'            Call Fill_List()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnSearchTrnHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTrnHead.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            With frm
'                .ValueMember = cboReviewer.ValueMember
'                .DisplayMember = cboReviewer.DisplayMember
'                .CodeMember = "employeecode"
'                .DataSource = CType(cboReviewer.DataSource, DataTable)
'            End With

'            If frm.DisplayDialog Then
'                cboReviewer.SelectedValue = frm.SelectedValue
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchTrnHead_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'#End Region

'#Region "ListView Event"

'    Private Sub lvAssessorList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvAssessorList.SelectedIndexChanged
'        Dim blnflag As Boolean
'        Try
'            If lvAssessorList.SelectedItems.Count = 0 Then Exit Sub
'            Dim objUserMapping As New clsapprover_Usermapping
'            blnflag = objUserMapping.isExist(enUserType.Assessor, CInt(lvAssessorList.SelectedItems(0).Tag))

'            If blnflag Then

'                Dim dsFill As DataSet = objUserMapping.GetList("List")
'                Dim dtFill As DataTable = New DataView(dsFill.Tables("List"), "approverunkid=" & CInt(lvAssessorList.SelectedItems(0).Tag), "", DataViewRowState.CurrentRows).ToTable
'                If dtFill.Rows.Count > 0 Then
'                    mintUserMappunkid = CInt(dtFill.Rows(0)("mappingunkid"))
'                Else
'                    mintUserMappunkid = 0
'                End If
'            Else
'                mintUserMappunkid = 0
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvApproverList_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Controls Events "

'    'S.SANDEEP [ 17 JULY 2013 ] -- START
'    'ENHANCEMENT : OTHER CHANGES
'    Private Sub mnuMapUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMapUser.Click
'        Try
'            If lvAssessorList.SelectedItems.Count = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Assessor from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
'                lvAssessorList.Select()
'                Exit Sub
'            End If
'            Dim objFrm As New frmAssessor_usermapping
'            'S.SANDEEP [ 20 AUG 2011 ] -- START
'            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
'            If User._Object._Isrighttoleft = True Then
'                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objFrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objFrm)
'            End If
'            'S.SANDEEP [ 20 AUG 2011 ] -- END

'            'S.SANDEEP [ 19 JULY 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If mintUserMappunkid > 0 Then
'            '    objFrm.displayDialog(mintUserMappunkid, enAction.EDIT_ONE, CInt(lvAssessorList.SelectedItems(0).Tag))
'            'Else
'            '    objFrm.displayDialog(mintUserMappunkid, enAction.ADD_ONE, CInt(lvAssessorList.SelectedItems(0).Tag))
'            'End If
'            If mintUserMappunkid > 0 Then
'                objFrm.displayDialog(mintUserMappunkid, enAction.EDIT_ONE, CInt(lvAssessorList.SelectedItems(0).Tag), True)
'            Else
'                objFrm.displayDialog(mintUserMappunkid, enAction.ADD_ONE, CInt(lvAssessorList.SelectedItems(0).Tag), True)
'            End If
'            'S.SANDEEP [ 19 JULY 2012 ] -- END
'            mintUserMappunkid = objFrm.mintMappingUnkid
'            Call Fill_List()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuMapUser_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub mnuImportAssessor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImportAssessormnuImportReviewer.Click
'        Try
'            Dim frm As New frmImportAssessorReviewer
'            frm._IsReviewer = True
'            frm.ShowDialog()
'            Call Fill_List()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "mnuImportAssessor_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [ 17 JULY 2013 ] -- END

'#End Region

'End Class