﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCloseAssessmentPeriod
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCloseAssessmentPeriod))
        Me.gbClosePeriodInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.EZeeStraightLine1 = New eZee.Common.eZeeStraightLine
        Me.lnlNoteNo3 = New System.Windows.Forms.Label
        Me.lnlNote3 = New System.Windows.Forms.Label
        Me.lvUnAssessedEmpList = New eZee.Common.eZeeListView(Me.components)
        Me.objcolhGrp = New System.Windows.Forms.ColumnHeader
        Me.colhCode = New System.Windows.Forms.ColumnHeader
        Me.colhEmployee = New System.Windows.Forms.ColumnHeader
        Me.lblNoteNo2 = New System.Windows.Forms.Label
        Me.lblNote2 = New System.Windows.Forms.Label
        Me.lblNote = New System.Windows.Forms.Label
        Me.lnEmployeeList = New eZee.Common.eZeeLine
        Me.lblPayPeriodOpen = New System.Windows.Forms.Label
        Me.cboPayPeriodOpen = New System.Windows.Forms.ComboBox
        Me.cboPayYearClose = New System.Windows.Forms.ComboBox
        Me.lblPayYearClose = New System.Windows.Forms.Label
        Me.lblPayPeriodClose = New System.Windows.Forms.Label
        Me.cboPayPeriodClose = New System.Windows.Forms.ComboBox
        Me.pbProgress = New System.Windows.Forms.ProgressBar
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClosePeriod = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objspc1 = New System.Windows.Forms.SplitContainer
        Me.gbCarryForwardThings = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkNoDataTransfer = New System.Windows.Forms.CheckBox
        Me.lvTranferList = New System.Windows.Forms.ListView
        Me.colhCFDetails = New System.Windows.Forms.ColumnHeader
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.gbInformation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblValue = New System.Windows.Forms.Label
        Me.gbClosePeriodInfo.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.pnlMainInfo.SuspendLayout()
        Me.objspc1.Panel1.SuspendLayout()
        Me.objspc1.Panel2.SuspendLayout()
        Me.objspc1.SuspendLayout()
        Me.gbCarryForwardThings.SuspendLayout()
        Me.gbInformation.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbClosePeriodInfo
        '
        Me.gbClosePeriodInfo.BorderColor = System.Drawing.Color.Black
        Me.gbClosePeriodInfo.Checked = False
        Me.gbClosePeriodInfo.CollapseAllExceptThis = False
        Me.gbClosePeriodInfo.CollapsedHoverImage = Nothing
        Me.gbClosePeriodInfo.CollapsedNormalImage = Nothing
        Me.gbClosePeriodInfo.CollapsedPressedImage = Nothing
        Me.gbClosePeriodInfo.CollapseOnLoad = False
        Me.gbClosePeriodInfo.Controls.Add(Me.EZeeStraightLine1)
        Me.gbClosePeriodInfo.ExpandedHoverImage = Nothing
        Me.gbClosePeriodInfo.ExpandedNormalImage = Nothing
        Me.gbClosePeriodInfo.ExpandedPressedImage = Nothing
        Me.gbClosePeriodInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbClosePeriodInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbClosePeriodInfo.HeaderHeight = 25
        Me.gbClosePeriodInfo.HeaderMessage = ""
        Me.gbClosePeriodInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbClosePeriodInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbClosePeriodInfo.HeightOnCollapse = 0
        Me.gbClosePeriodInfo.LeftTextSpace = 0
        Me.gbClosePeriodInfo.Location = New System.Drawing.Point(673, 23)
        Me.gbClosePeriodInfo.Name = "gbClosePeriodInfo"
        Me.gbClosePeriodInfo.OpenHeight = 300
        Me.gbClosePeriodInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbClosePeriodInfo.ShowBorder = True
        Me.gbClosePeriodInfo.ShowCheckBox = False
        Me.gbClosePeriodInfo.ShowCollapseButton = False
        Me.gbClosePeriodInfo.ShowDefaultBorderColor = True
        Me.gbClosePeriodInfo.ShowDownButton = False
        Me.gbClosePeriodInfo.ShowHeader = True
        Me.gbClosePeriodInfo.Size = New System.Drawing.Size(19, 13)
        Me.gbClosePeriodInfo.TabIndex = 169
        Me.gbClosePeriodInfo.Temp = 0
        Me.gbClosePeriodInfo.Text = "Period to be closed"
        Me.gbClosePeriodInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbClosePeriodInfo.Visible = False
        '
        'EZeeStraightLine1
        '
        Me.EZeeStraightLine1.BackColor = System.Drawing.Color.Transparent
        Me.EZeeStraightLine1.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeStraightLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.EZeeStraightLine1.Location = New System.Drawing.Point(363, 24)
        Me.EZeeStraightLine1.Name = "EZeeStraightLine1"
        Me.EZeeStraightLine1.Size = New System.Drawing.Size(10, 319)
        Me.EZeeStraightLine1.TabIndex = 176
        Me.EZeeStraightLine1.Text = "EZeeStraightLine1"
        '
        'lnlNoteNo3
        '
        Me.lnlNoteNo3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnlNoteNo3.ForeColor = System.Drawing.Color.DarkRed
        Me.lnlNoteNo3.Location = New System.Drawing.Point(20, 104)
        Me.lnlNoteNo3.Name = "lnlNoteNo3"
        Me.lnlNoteNo3.Size = New System.Drawing.Size(25, 20)
        Me.lnlNoteNo3.TabIndex = 190
        Me.lnlNoteNo3.Text = "2."
        '
        'lnlNote3
        '
        Me.lnlNote3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnlNote3.ForeColor = System.Drawing.Color.DarkRed
        Me.lnlNote3.Location = New System.Drawing.Point(47, 104)
        Me.lnlNote3.Name = "lnlNote3"
        Me.lnlNote3.Size = New System.Drawing.Size(538, 42)
        Me.lnlNote3.TabIndex = 189
        Me.lnlNote3.Text = "Transaction Data (Goals)  will be transfered with the last status and state from " & _
            "old period to new period e.g. (submit for approval, final save, open change etc." & _
            ")"
        '
        'lvUnAssessedEmpList
        '
        Me.lvUnAssessedEmpList.BackColorOnChecked = False
        Me.lvUnAssessedEmpList.ColumnHeaders = Nothing
        Me.lvUnAssessedEmpList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objcolhGrp, Me.colhCode, Me.colhEmployee})
        Me.lvUnAssessedEmpList.CompulsoryColumns = ""
        Me.lvUnAssessedEmpList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvUnAssessedEmpList.FullRowSelect = True
        Me.lvUnAssessedEmpList.GridLines = True
        Me.lvUnAssessedEmpList.GroupingColumn = Nothing
        Me.lvUnAssessedEmpList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvUnAssessedEmpList.HideSelection = False
        Me.lvUnAssessedEmpList.Location = New System.Drawing.Point(8, 166)
        Me.lvUnAssessedEmpList.MinColumnWidth = 50
        Me.lvUnAssessedEmpList.MultiSelect = False
        Me.lvUnAssessedEmpList.Name = "lvUnAssessedEmpList"
        Me.lvUnAssessedEmpList.OptionalColumns = ""
        Me.lvUnAssessedEmpList.ShowMoreItem = False
        Me.lvUnAssessedEmpList.ShowSaveItem = False
        Me.lvUnAssessedEmpList.ShowSelectAll = True
        Me.lvUnAssessedEmpList.ShowSizeAllColumnsToFit = True
        Me.lvUnAssessedEmpList.Size = New System.Drawing.Size(577, 247)
        Me.lvUnAssessedEmpList.Sortable = True
        Me.lvUnAssessedEmpList.TabIndex = 187
        Me.lvUnAssessedEmpList.UseCompatibleStateImageBehavior = False
        Me.lvUnAssessedEmpList.View = System.Windows.Forms.View.Details
        '
        'objcolhGrp
        '
        Me.objcolhGrp.Tag = "objcolhGrp"
        Me.objcolhGrp.Text = ""
        Me.objcolhGrp.Width = 0
        '
        'colhCode
        '
        Me.colhCode.Tag = "colhCode"
        Me.colhCode.Text = "Code"
        Me.colhCode.Width = 90
        '
        'colhEmployee
        '
        Me.colhEmployee.Tag = "colhEmployee"
        Me.colhEmployee.Text = "Employee"
        Me.colhEmployee.Width = 480
        '
        'lblNoteNo2
        '
        Me.lblNoteNo2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoteNo2.ForeColor = System.Drawing.Color.DarkRed
        Me.lblNoteNo2.Location = New System.Drawing.Point(20, 62)
        Me.lblNoteNo2.Name = "lblNoteNo2"
        Me.lblNoteNo2.Size = New System.Drawing.Size(25, 20)
        Me.lblNoteNo2.TabIndex = 184
        Me.lblNoteNo2.Text = "1."
        '
        'lblNote2
        '
        Me.lblNote2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNote2.ForeColor = System.Drawing.Color.DarkRed
        Me.lblNote2.Location = New System.Drawing.Point(47, 61)
        Me.lblNote2.Name = "lblNote2"
        Me.lblNote2.Size = New System.Drawing.Size(538, 43)
        Me.lblNote2.TabIndex = 183
        Me.lblNote2.Text = "Please note that once you close the period, you can not make or change any transa" & _
            "ction regarding employee's General/BSC Evaluation for current period."
        '
        'lblNote
        '
        Me.lblNote.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNote.ForeColor = System.Drawing.Color.DarkRed
        Me.lblNote.Location = New System.Drawing.Point(10, 31)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(258, 18)
        Me.lblNote.TabIndex = 180
        Me.lblNote.Text = "Note:"
        '
        'lnEmployeeList
        '
        Me.lnEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnEmployeeList.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEmployeeList.Location = New System.Drawing.Point(10, 146)
        Me.lnEmployeeList.Name = "lnEmployeeList"
        Me.lnEmployeeList.Size = New System.Drawing.Size(571, 17)
        Me.lnEmployeeList.TabIndex = 175
        Me.lnEmployeeList.Text = "Unassessed Employee List"
        '
        'lblPayPeriodOpen
        '
        Me.lblPayPeriodOpen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriodOpen.Location = New System.Drawing.Point(4, 89)
        Me.lblPayPeriodOpen.Name = "lblPayPeriodOpen"
        Me.lblPayPeriodOpen.Size = New System.Drawing.Size(58, 16)
        Me.lblPayPeriodOpen.TabIndex = 173
        Me.lblPayPeriodOpen.Text = "Open"
        Me.lblPayPeriodOpen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayPeriodOpen
        '
        Me.cboPayPeriodOpen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriodOpen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriodOpen.FormattingEnabled = True
        Me.cboPayPeriodOpen.Location = New System.Drawing.Point(68, 87)
        Me.cboPayPeriodOpen.Name = "cboPayPeriodOpen"
        Me.cboPayPeriodOpen.Size = New System.Drawing.Size(179, 21)
        Me.cboPayPeriodOpen.TabIndex = 172
        '
        'cboPayYearClose
        '
        Me.cboPayYearClose.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayYearClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayYearClose.FormattingEnabled = True
        Me.cboPayYearClose.Location = New System.Drawing.Point(68, 33)
        Me.cboPayYearClose.Name = "cboPayYearClose"
        Me.cboPayYearClose.Size = New System.Drawing.Size(179, 21)
        Me.cboPayYearClose.TabIndex = 168
        '
        'lblPayYearClose
        '
        Me.lblPayYearClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayYearClose.Location = New System.Drawing.Point(4, 35)
        Me.lblPayYearClose.Name = "lblPayYearClose"
        Me.lblPayYearClose.Size = New System.Drawing.Size(58, 16)
        Me.lblPayYearClose.TabIndex = 170
        Me.lblPayYearClose.Text = "Year"
        Me.lblPayYearClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPayPeriodClose
        '
        Me.lblPayPeriodClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriodClose.Location = New System.Drawing.Point(4, 62)
        Me.lblPayPeriodClose.Name = "lblPayPeriodClose"
        Me.lblPayPeriodClose.Size = New System.Drawing.Size(58, 16)
        Me.lblPayPeriodClose.TabIndex = 171
        Me.lblPayPeriodClose.Text = "Close"
        Me.lblPayPeriodClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayPeriodClose
        '
        Me.cboPayPeriodClose.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriodClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriodClose.FormattingEnabled = True
        Me.cboPayPeriodClose.Location = New System.Drawing.Point(68, 60)
        Me.cboPayPeriodClose.Name = "cboPayPeriodClose"
        Me.cboPayPeriodClose.Size = New System.Drawing.Size(179, 21)
        Me.cboPayPeriodClose.TabIndex = 169
        '
        'pbProgress
        '
        Me.pbProgress.Location = New System.Drawing.Point(673, 23)
        Me.pbProgress.Name = "pbProgress"
        Me.pbProgress.Size = New System.Drawing.Size(19, 13)
        Me.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pbProgress.TabIndex = 188
        Me.pbProgress.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(738, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnClosePeriod
        '
        Me.btnClosePeriod.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClosePeriod.BackColor = System.Drawing.Color.White
        Me.btnClosePeriod.BackgroundImage = CType(resources.GetObject("btnClosePeriod.BackgroundImage"), System.Drawing.Image)
        Me.btnClosePeriod.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClosePeriod.BorderColor = System.Drawing.Color.Empty
        Me.btnClosePeriod.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClosePeriod.FlatAppearance.BorderSize = 0
        Me.btnClosePeriod.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClosePeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClosePeriod.ForeColor = System.Drawing.Color.Black
        Me.btnClosePeriod.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClosePeriod.GradientForeColor = System.Drawing.Color.Black
        Me.btnClosePeriod.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClosePeriod.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClosePeriod.Location = New System.Drawing.Point(635, 13)
        Me.btnClosePeriod.Name = "btnClosePeriod"
        Me.btnClosePeriod.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClosePeriod.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClosePeriod.Size = New System.Drawing.Size(97, 30)
        Me.btnClosePeriod.TabIndex = 0
        Me.btnClosePeriod.Text = "&Close Period"
        Me.btnClosePeriod.UseVisualStyleBackColor = True
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.pnlMainInfo)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(847, 474)
        Me.pnlMain.TabIndex = 187
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objspc1)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(847, 474)
        Me.pnlMainInfo.TabIndex = 187
        '
        'objspc1
        '
        Me.objspc1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objspc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objspc1.Location = New System.Drawing.Point(0, 0)
        Me.objspc1.Name = "objspc1"
        '
        'objspc1.Panel1
        '
        Me.objspc1.Panel1.Controls.Add(Me.gbCarryForwardThings)
        '
        'objspc1.Panel2
        '
        Me.objspc1.Panel2.Controls.Add(Me.gbInformation)
        Me.objspc1.Size = New System.Drawing.Size(847, 419)
        Me.objspc1.SplitterDistance = 252
        Me.objspc1.SplitterWidth = 2
        Me.objspc1.TabIndex = 189
        '
        'gbCarryForwardThings
        '
        Me.gbCarryForwardThings.BorderColor = System.Drawing.Color.Black
        Me.gbCarryForwardThings.Checked = False
        Me.gbCarryForwardThings.CollapseAllExceptThis = False
        Me.gbCarryForwardThings.CollapsedHoverImage = Nothing
        Me.gbCarryForwardThings.CollapsedNormalImage = Nothing
        Me.gbCarryForwardThings.CollapsedPressedImage = Nothing
        Me.gbCarryForwardThings.CollapseOnLoad = False
        Me.gbCarryForwardThings.Controls.Add(Me.chkNoDataTransfer)
        Me.gbCarryForwardThings.Controls.Add(Me.lvTranferList)
        Me.gbCarryForwardThings.Controls.Add(Me.objLine1)
        Me.gbCarryForwardThings.Controls.Add(Me.cboPayPeriodOpen)
        Me.gbCarryForwardThings.Controls.Add(Me.lblPayPeriodOpen)
        Me.gbCarryForwardThings.Controls.Add(Me.cboPayYearClose)
        Me.gbCarryForwardThings.Controls.Add(Me.cboPayPeriodClose)
        Me.gbCarryForwardThings.Controls.Add(Me.lblPayYearClose)
        Me.gbCarryForwardThings.Controls.Add(Me.lblPayPeriodClose)
        Me.gbCarryForwardThings.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbCarryForwardThings.ExpandedHoverImage = Nothing
        Me.gbCarryForwardThings.ExpandedNormalImage = Nothing
        Me.gbCarryForwardThings.ExpandedPressedImage = Nothing
        Me.gbCarryForwardThings.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbCarryForwardThings.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbCarryForwardThings.HeaderHeight = 25
        Me.gbCarryForwardThings.HeaderMessage = ""
        Me.gbCarryForwardThings.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbCarryForwardThings.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbCarryForwardThings.HeightOnCollapse = 0
        Me.gbCarryForwardThings.LeftTextSpace = 0
        Me.gbCarryForwardThings.Location = New System.Drawing.Point(0, 0)
        Me.gbCarryForwardThings.Name = "gbCarryForwardThings"
        Me.gbCarryForwardThings.OpenHeight = 300
        Me.gbCarryForwardThings.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbCarryForwardThings.ShowBorder = True
        Me.gbCarryForwardThings.ShowCheckBox = False
        Me.gbCarryForwardThings.ShowCollapseButton = False
        Me.gbCarryForwardThings.ShowDefaultBorderColor = True
        Me.gbCarryForwardThings.ShowDownButton = False
        Me.gbCarryForwardThings.ShowHeader = True
        Me.gbCarryForwardThings.Size = New System.Drawing.Size(252, 419)
        Me.gbCarryForwardThings.TabIndex = 188
        Me.gbCarryForwardThings.Temp = 0
        Me.gbCarryForwardThings.Text = "Period/Transfer Selection"
        Me.gbCarryForwardThings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkNoDataTransfer
        '
        Me.chkNoDataTransfer.ForeColor = System.Drawing.Color.Red
        Me.chkNoDataTransfer.Location = New System.Drawing.Point(7, 395)
        Me.chkNoDataTransfer.Name = "chkNoDataTransfer"
        Me.chkNoDataTransfer.Size = New System.Drawing.Size(238, 17)
        Me.chkNoDataTransfer.TabIndex = 178
        Me.chkNoDataTransfer.Text = "No Data Transfer, Just Close Period"
        Me.chkNoDataTransfer.UseVisualStyleBackColor = True
        '
        'lvTranferList
        '
        Me.lvTranferList.CheckBoxes = True
        Me.lvTranferList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCFDetails})
        Me.lvTranferList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTranferList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvTranferList.Location = New System.Drawing.Point(5, 131)
        Me.lvTranferList.Name = "lvTranferList"
        Me.lvTranferList.Size = New System.Drawing.Size(243, 258)
        Me.lvTranferList.TabIndex = 4
        Me.lvTranferList.UseCompatibleStateImageBehavior = False
        Me.lvTranferList.View = System.Windows.Forms.View.Details
        '
        'colhCFDetails
        '
        Me.colhCFDetails.Tag = "colhCFDetails"
        Me.colhCFDetails.Text = "C/F Details"
        Me.colhCFDetails.Width = 235
        '
        'objLine1
        '
        Me.objLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(4, 111)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(244, 17)
        Me.objLine1.TabIndex = 176
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbInformation
        '
        Me.gbInformation.BorderColor = System.Drawing.Color.Black
        Me.gbInformation.Checked = False
        Me.gbInformation.CollapseAllExceptThis = False
        Me.gbInformation.CollapsedHoverImage = Nothing
        Me.gbInformation.CollapsedNormalImage = Nothing
        Me.gbInformation.CollapsedPressedImage = Nothing
        Me.gbInformation.CollapseOnLoad = False
        Me.gbInformation.Controls.Add(Me.lnlNoteNo3)
        Me.gbInformation.Controls.Add(Me.lnEmployeeList)
        Me.gbInformation.Controls.Add(Me.lnlNote3)
        Me.gbInformation.Controls.Add(Me.lvUnAssessedEmpList)
        Me.gbInformation.Controls.Add(Me.lblNote)
        Me.gbInformation.Controls.Add(Me.lblNoteNo2)
        Me.gbInformation.Controls.Add(Me.lblNote2)
        Me.gbInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbInformation.ExpandedHoverImage = Nothing
        Me.gbInformation.ExpandedNormalImage = Nothing
        Me.gbInformation.ExpandedPressedImage = Nothing
        Me.gbInformation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInformation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInformation.HeaderHeight = 25
        Me.gbInformation.HeaderMessage = ""
        Me.gbInformation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbInformation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInformation.HeightOnCollapse = 0
        Me.gbInformation.LeftTextSpace = 0
        Me.gbInformation.Location = New System.Drawing.Point(0, 0)
        Me.gbInformation.Name = "gbInformation"
        Me.gbInformation.OpenHeight = 300
        Me.gbInformation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInformation.ShowBorder = True
        Me.gbInformation.ShowCheckBox = False
        Me.gbInformation.ShowCollapseButton = False
        Me.gbInformation.ShowDefaultBorderColor = True
        Me.gbInformation.ShowDownButton = False
        Me.gbInformation.ShowHeader = True
        Me.gbInformation.Size = New System.Drawing.Size(593, 419)
        Me.gbInformation.TabIndex = 191
        Me.gbInformation.Temp = 0
        Me.gbInformation.Text = "Information"
        Me.gbInformation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblValue)
        Me.objFooter.Controls.Add(Me.btnClosePeriod)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.pbProgress)
        Me.objFooter.Controls.Add(Me.gbClosePeriodInfo)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 419)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(847, 55)
        Me.objFooter.TabIndex = 190
        '
        'objlblValue
        '
        Me.objlblValue.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblValue.Location = New System.Drawing.Point(5, 16)
        Me.objlblValue.Name = "objlblValue"
        Me.objlblValue.Size = New System.Drawing.Size(624, 25)
        Me.objlblValue.TabIndex = 189
        Me.objlblValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmCloseAssessmentPeriod
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(847, 474)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCloseAssessmentPeriod"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Close Assessment Period"
        Me.gbClosePeriodInfo.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMainInfo.ResumeLayout(False)
        Me.objspc1.Panel1.ResumeLayout(False)
        Me.objspc1.Panel2.ResumeLayout(False)
        Me.objspc1.ResumeLayout(False)
        Me.gbCarryForwardThings.ResumeLayout(False)
        Me.gbInformation.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbClosePeriodInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblNoteNo2 As System.Windows.Forms.Label
    Friend WithEvents lblNote2 As System.Windows.Forms.Label
    Friend WithEvents lblNote As System.Windows.Forms.Label
    Friend WithEvents EZeeStraightLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents lnEmployeeList As eZee.Common.eZeeLine
    Friend WithEvents lblPayPeriodOpen As System.Windows.Forms.Label
    Friend WithEvents cboPayPeriodOpen As System.Windows.Forms.ComboBox
    Friend WithEvents cboPayYearClose As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayYearClose As System.Windows.Forms.Label
    Friend WithEvents lblPayPeriodClose As System.Windows.Forms.Label
    Friend WithEvents cboPayPeriodClose As System.Windows.Forms.ComboBox
    Friend WithEvents btnClosePeriod As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lvUnAssessedEmpList As eZee.Common.eZeeListView
    Friend WithEvents objcolhGrp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEmployee As System.Windows.Forms.ColumnHeader
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents pbProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents lnlNoteNo3 As System.Windows.Forms.Label
    Friend WithEvents lnlNote3 As System.Windows.Forms.Label
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objspc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents gbCarryForwardThings As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents lvTranferList As System.Windows.Forms.ListView
    Friend WithEvents colhCFDetails As System.Windows.Forms.ColumnHeader
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents gbInformation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkNoDataTransfer As System.Windows.Forms.CheckBox
    Friend WithEvents objlblValue As System.Windows.Forms.Label
End Class
