﻿Option Strict On

#Region "Imports  "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Runtime.InteropServices
Imports System.Reflection

#End Region

Public Class frmAssessmentRatio_List

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentRatio_List"

#End Region


#Region " Private Functions "


    Private Sub Fill_List()

        'S.SANDEEP [14 MAR 2016] -- START
        'Dim objRatio As New clsAssessment_Ratio
        'Dim dsList As New DataSet
        'Dim StrSearch As String = String.Empty
        'Dim dtFilter As DataTable
        'Try
        '    dsList = objRatio.GetList("List")

        '    If CInt(cboJobGroup.SelectedValue) > 0 Then
        '        StrSearch &= "AND jobgroupunkid = '" & CInt(cboJobGroup.SelectedValue) & "' "
        '    End If

        '    If CInt(cboPeriod.SelectedValue) > 0 Then
        '        StrSearch &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
        '    End If

        '    If StrSearch.Trim.Length > 0 Then
        '        StrSearch = StrSearch.Substring(3)
        '        dtFilter = New DataView(dsList.Tables("List"), StrSearch, "", DataViewRowState.CurrentRows).ToTable
        '    Else
        '        dtFilter = New DataView(dsList.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
        '    End If

        '    lvRatioList.Items.Clear()
        '    Dim lvItem As ListViewItem

        '    For Each dRow As DataRow In dtFilter.Rows
        '        lvItem = New ListViewItem

        '        lvItem.Text = dRow.Item("Job_Group").ToString
        '        lvItem.SubItems.Add(dRow.Item("ge_weight").ToString)
        '        lvItem.SubItems.Add(dRow.Item("bsc_weight").ToString)
        '        lvItem.SubItems.Add(dRow.Item("Period").ToString)
        '        lvItem.Tag = dRow.Item("ratiounkid")

        '        If CInt(dRow.Item("statusid")) = enStatusType.Close Then
        '            lvItem.ForeColor = Color.Gray
        '        End If

        '        lvRatioList.Items.Add(lvItem)
        '    Next

        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message & " " & ex.StackTrace, "Fill_List", mstrModuleName)
        'Finally
        '    objRatio = Nothing
        'End Try

        Dim objRatio As New clsassess_ratio_master
        Dim dsList As New DataSet
        Dim StrSearch As String = String.Empty

        Try
            If CInt(cboAllocationData.SelectedValue) > 0 Then
                StrSearch &= "AND hrassess_ratio_tran.allocationid = '" & CInt(cboAllocationData.SelectedValue) & "' AND hrassess_ratio_master.allocrefunkid = '" & CInt(cboAllocations.SelectedValue) & "'"
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                StrSearch &= "AND hrassess_ratio_master.periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
            End If

            If chkShowClosePeriodData.Checked = False Then
                StrSearch &= "AND cfcommon_period_tran.statusid = 1 "
            End If

            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
            End If

            dsList = objRatio.GetList("List", StrSearch)
            lvRatioList.Items.Clear()
            Dim lvItem As ListViewItem

            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            RemoveHandler lvRatioList.ItemChecked, AddressOf lvRatioList_ItemChecked

            For Each dRow As DataRow In dsList.Tables(0).Rows
                lvItem = New ListViewItem

                lvItem.Text = ""

                lvItem.SubItems.Add(dRow.Item("allocation_name").ToString)
                lvItem.SubItems.Add(dRow.Item("period_name").ToString & " - [ " & dRow.Item("allocation_type").ToString & " ]")
                lvItem.SubItems.Add(dRow.Item("ge_weight").ToString)
                lvItem.SubItems.Add(dRow.Item("bsc_weight").ToString)
                lvItem.SubItems.Add(dRow.Item("ratiotranunkid").ToString)
                lvItem.Tag = dRow.Item("ratiounkid")

                If CInt(dRow.Item("statusid")) = enStatusType.Close Then
                    lvItem.ForeColor = Color.Gray
                End If

                lvRatioList.Items.Add(lvItem)
            Next

            lvRatioList.GroupingColumn = colhPeriod
            lvRatioList.DisplayGroups(True)

            If lvRatioList.Groups.Count > 2 Then
                colhAllocation.Width = 530 - 20
            Else
                colhAllocation.Width = 530
            End If


            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged
            AddHandler lvRatioList.ItemChecked, AddressOf lvRatioList_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
            objRatio = Nothing
        End Try

        'S.SANDEEP [14 MAR 2016] -- END

    End Sub

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objJobGrp As New clsJobGroup
        Dim objPeriod As New clscommom_period_Tran
        Try

            'S.SANDEEP [14 MAR 2016] -- START
            'dsCombo = objJobGrp.getComboList("JobGroup", True)
            'With cboAllocationData
            '    .ValueMember = "jobgroupunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombo.Tables("JobGroup")
            '    .SelectedValue = 0
            'End With
            Dim objMData As New clsMasterData
            dsCombo = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsCombo.Tables(0), "Id NOT IN(" & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = 1
            End With
            'S.SANDEEP [14 MAR 2016] -- END


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, -1, "Period", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, -1, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose()
        End Try
    End Sub

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddAssessmentRatios
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditAssessmentRatios
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteAssessmentRatios
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [28 MAY 2015] -- END


    'S.SANDEEP [14 MAR 2016] -- START
    Private Sub Fill_Data()
        Try
            cboAllocationData.DataSource = Nothing
            With cboAllocationData
                Select Case CInt(cboAllocations.SelectedValue)
                    Case enAllocation.BRANCH  'BRANCH
                        .DataSource = (New clsStation).getComboList(, True).Tables(0)
                        .ValueMember = "stationunkid"
                        .DisplayMember = "name"
                    Case enAllocation.DEPARTMENT_GROUP  'DEPARTMENT GROUP
                        .DataSource = (New clsDepartmentGroup).getComboList("list", True).Tables(0)
                        .ValueMember = "deptgroupunkid"
                        .DisplayMember = "name"
                    Case enAllocation.DEPARTMENT  'DEPARTMENT
                        .DataSource = (New clsDepartment).getComboList(, True).Tables(0)
                        .ValueMember = "departmentunkid"
                        .DisplayMember = "name"
                    Case enAllocation.SECTION_GROUP  'SECTION GROUP
                        .DataSource = (New clsSectionGroup).getComboList(, True).Tables(0)
                        .ValueMember = "sectiongroupunkid"
                        .DisplayMember = "name"
                    Case enAllocation.SECTION  'SECTION
                        .DataSource = (New clsSections).getComboList(, True).Tables(0)
                        .ValueMember = "sectionunkid"
                        .DisplayMember = "name"
                    Case enAllocation.UNIT_GROUP  'UNIT GROUP
                        .DataSource = (New clsUnitGroup).getComboList(, True).Tables(0)
                        .ValueMember = "unitgroupunkid"
                        .DisplayMember = "name"
                    Case enAllocation.UNIT  'UNIT
                        .DataSource = (New clsUnits).getComboList(, True).Tables(0)
                        .ValueMember = "unitunkid"
                        .DisplayMember = "name"
                    Case enAllocation.TEAM  'TEAM
                        .DataSource = (New clsTeams).getComboList(, True).Tables(0)
                        .ValueMember = "teamunkid"
                        .DisplayMember = "name"
                    Case enAllocation.JOB_GROUP  'JOB GROUP
                        .DataSource = (New clsJobGroup).getComboList(, True).Tables(0)
                        .ValueMember = "jobgroupunkid"
                        .DisplayMember = "name"
                    Case enAllocation.JOBS 'JOB
                        .DataSource = (New clsJobs).getComboList(, True).Tables(0)
                        .ValueMember = "jobunkid"
                        .DisplayMember = "name"
                    Case enAllocation.CLASS_GROUP 'CLASS GROUP
                        .DataSource = (New clsClassGroup).getComboList(, True).Tables(0)
                        .ValueMember = "classgroupunkid"
                        .DisplayMember = "name"
                    Case enAllocation.CLASSES 'CLASS
                        .DataSource = (New clsClass).getComboList(, True).Tables(0)
                        .ValueMember = "classesunkid"
                        .DisplayMember = "name"
                End Select
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [14 MAR 2016] -- END

#End Region

#Region " Form's Events "

    Private Sub frmAssessmentRatio_List_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call FillCombo()
            Call Fill_List()
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            Call SetVisibility()
            'S.SANDEEP [28 MAY 2015] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAssessmentRatio_List_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearchJobGrp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAllocation.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboAllocationData.ValueMember
                .DisplayMember = cboAllocationData.DisplayMember
                .DataSource = CType(cboAllocationData.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboAllocationData.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJobGrp_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPeriod.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboPeriod.ValueMember
                .DisplayMember = cboPeriod.DisplayMember
                .DataSource = CType(cboPeriod.DataSource, DataTable)
            End With

            If frm.DisplayDialog Then
                cboPeriod.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPeriod_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            'S.SANDEEP [14 MAR 2016] -- START
            cboAllocations.SelectedValue = enAllocation.BRANCH
            cboAllocationData.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            chkShowClosePeriodData.Checked = False
            'S.SANDEEP [14 MAR 2016] -- END
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmAssessmentWeight
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(-1, enAction.ADD_CONTINUE)

            Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmAssessmentWeight
        Try
            If lvRatioList.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select information from the list to perform further operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If lvRatioList.SelectedItems(0).ForeColor = Color.Gray Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot edit Assessment Ratio, because period is closed."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(CInt(lvRatioList.SelectedItems(0).Tag), enAction.EDIT_ONE)

            Fill_List()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            'S.SANDEEP [14 MAR 2016] -- START
            'If lvRatioList.SelectedItems.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select information from the list to perform further operation on it."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If

            'If lvRatioList.SelectedItems(0).ForeColor = Color.Gray Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete Assessment Ratio, because period is closed."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If


            'If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Assessment Ratio?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

            '    Dim objRatio As New clsAssessment_Ratio

            '    If objRatio.Delete(CInt(lvRatioList.SelectedItems(0).Tag)) = True Then
            '        lvRatioList.SelectedItems(0).Remove()
            '    End If
            '    objRatio = Nothing
            'End If

            If lvRatioList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please check information from the list to perform further operation on it."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Assessment Ratio?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                Dim objRatio As New clsassess_ratio_master


                Dim strVoidReason As String = String.Empty

                Dim frm As New frmReasonSelection

                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If

                frm.displayDialog(enVoidCategoryType.ASSESSMENT, strVoidReason)
                frm.Dispose()

                If strVoidReason.Trim.Length > 0 Then

                    objRatio._Isvoid = True
                    objRatio._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objRatio._Voidreason = strVoidReason
                    objRatio._Voiduserunkid = User._Object._Userunkid

                    With objRatio
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With

                    Dim mDictionary As New Dictionary(Of Integer, String)
                    Dim iCheckedTags As List(Of Integer) = lvRatioList.CheckedItems.Cast(Of ListViewItem)().Select(Function(x) CInt(x.Tag)).Distinct().ToList()
                    If iCheckedTags IsNot Nothing Then
                        For Each iTag As Integer In iCheckedTags
                            Dim xTag As Integer = iTag
                            Dim iCheckedTranIds As List(Of String)
                            iCheckedTranIds = (From r In lvRatioList.CheckedItems.Cast(Of ListViewItem)() Where CInt(r.Tag) = xTag Select r.SubItems(objcolhTranId.Index).Text).ToList()
                            mDictionary.Add(xTag, String.Join(",", iCheckedTranIds.ToArray()))
                        Next
                    End If

                    If objRatio.Delete(mDictionary) = True Then
                        Call Fill_List()
                    End If

                End If

                objRatio = Nothing
            End If
            'S.SANDEEP [14 MAR 2016] -- END



        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Controls Events "


    'S.SANDEEP [14 MAR 2016] -- START
    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            objlblAllocationCaption.Text = cboAllocations.Text
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvRatioList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvRatioList.ItemChecked
        Try
            RemoveHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged

            If lvRatioList.CheckedItems.Count > 0 Then
                If lvRatioList.CheckedItems(0).ForeColor = Color.Gray Then
                    e.Item.Checked = False
                End If
            End If

            If lvRatioList.CheckedItems.Count <= 0 Then
                objchkAll.CheckState = CheckState.Unchecked
            ElseIf lvRatioList.CheckedItems.Count < lvRatioList.Items.Count Then
                objchkAll.CheckState = CheckState.Indeterminate
            ElseIf lvRatioList.CheckedItems.Count = lvRatioList.Items.Count Then
                objchkAll.CheckState = CheckState.Checked
            End If

            AddHandler objchkAll.CheckedChanged, AddressOf objchkAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvRatioList_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAll.CheckedChanged
        Try
            RemoveHandler lvRatioList.ItemChecked, AddressOf lvRatioList_ItemChecked
            For Each item As ListViewItem In lvRatioList.Items
                If item.ForeColor = Color.Gray Then Continue For
                item.Checked = objchkAll.Checked
            Next
            AddHandler lvRatioList.ItemChecked, AddressOf lvRatioList_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [14 MAR 2016] -- END

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhAllocation.Text = Language._Object.getCaption(CStr(Me.colhAllocation.Tag), Me.colhAllocation.Text)
            Me.colhGeneralEvalution.Text = Language._Object.getCaption(CStr(Me.colhGeneralEvalution.Tag), Me.colhGeneralEvalution.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.colhBSC.Text = Language._Object.getCaption(CStr(Me.colhBSC.Tag), Me.colhBSC.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
            Me.chkShowClosePeriodData.Text = Language._Object.getCaption(Me.chkShowClosePeriodData.Name, Me.chkShowClosePeriodData.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select information from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot edit Assessment Ratio, because period is closed.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Assessment Ratio?")
            Language.setMessage(mstrModuleName, 6, "Please check information from the list to perform further operation on it.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class