﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmExternalAssessorList

#Region "Private Variable"

    Private objExternalAssessor As clsexternal_assessor_master
    Private ReadOnly mstrModuleName As String = "frmExternalAssessorList"

#End Region

#Region "Form's Event"

    Private Sub frmExternalAssessorList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objExternalAssessor = New clsexternal_assessor_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            'S.SANDEEP [ 16 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 16 MAY 2012 ] -- END

            FillEmployee()
            lvExternalAssessorList.GridLines = False
            If lvExternalAssessorList.Items.Count > 0 Then lvExternalAssessorList.Items(0).Selected = True
            lvExternalAssessorList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExternalAssessorList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExternalAssessorList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvExternalAssessorList.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExternalAssessorList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmExternalAssessorList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objExternalAssessor = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsexternal_assessor_master.SetMessages()
            objfrm._Other_ModuleNames = "clsexternal_assessor_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try

            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchdName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchdName.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboDisplayName.DataSource, DataTable)
            With cboDisplayName
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchdName_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedIndex = 0
            If cboDisplayName.Items.Count > 0 Then cboDisplayName.SelectedIndex = 0
            If cboRelation.Items.Count > 0 Then cboRelation.SelectedIndex = 0
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objFrmExternalAssessor_AddEdit As New frmExternalAssessor_AddEdit
            If User._Object._Isrighttoleft = True Then
                objFrmExternalAssessor_AddEdit.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrmExternalAssessor_AddEdit.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objExternalAssessor)
            End If
            If objFrmExternalAssessor_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                FillDisplayName()
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvExternalAssessorList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select External Assessor from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
                lvExternalAssessorList.Select()
                Exit Sub
            End If

            Dim objExternalAssessor As New frmExternalAssessor_AddEdit

            Try
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvExternalAssessorList.SelectedItems(0).Index
                If User._Object._Isrighttoleft = True Then
                    objExternalAssessor.RightToLeft = Windows.Forms.RightToLeft.Yes
                    objExternalAssessor.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(objExternalAssessor)
                End If

                If objExternalAssessor.displayDialog(CInt(lvExternalAssessorList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    fillList()
                End If
                objExternalAssessor = Nothing

                lvExternalAssessorList.Items(intSelectedIndex).Selected = True
                lvExternalAssessorList.EnsureVisible(intSelectedIndex)
                lvExternalAssessorList.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objExternalAssessor IsNot Nothing Then objExternalAssessor.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvExternalAssessorList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select External Assessor from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvExternalAssessorList.Select()
            Exit Sub
        End If
        If objExternalAssessor.isUsed(CInt(lvExternalAssessorList.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete this External Assessor. Reason: This External Assessor is in use."), enMsgBoxStyle.Information) '?2
            lvExternalAssessorList.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvExternalAssessorList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this External Assessor?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objExternalAssessor._FormName = mstrModuleName
                objExternalAssessor._LoginEmployeeunkid = 0
                objExternalAssessor._ClientIP = getIP()
                objExternalAssessor._HostName = getHostName()
                objExternalAssessor._FromWeb = False
                objExternalAssessor._AuditUserId = User._Object._Userunkid
objExternalAssessor._CompanyUnkid = Company._Object._Companyunkid
                objExternalAssessor._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                objExternalAssessor.Delete(CInt(lvExternalAssessorList.SelectedItems(0).Tag))
                lvExternalAssessorList.SelectedItems(0).Remove()
                FillDisplayName()

                If lvExternalAssessorList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvExternalAssessorList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvExternalAssessorList.Items.Count - 1
                    lvExternalAssessorList.Items(intSelectedIndex).Selected = True
                    lvExternalAssessorList.EnsureVisible(intSelectedIndex)
                ElseIf lvExternalAssessorList.Items.Count <> 0 Then
                    lvExternalAssessorList.Items(intSelectedIndex).Selected = True
                    lvExternalAssessorList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvExternalAssessorList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'If User._Object.Privilege._AllowToViewExternalAssessorList = True Then    'Pinkal (09-Jul-2012) -- Start
            'S.SANDEEP [28 MAY 2015] -- END




            dsList = objExternalAssessor.GetList("List")

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue)
            End If

            If CInt(cboRelation.SelectedValue) > 0 Then
                StrSearching &= "AND relationunkid = " & CInt(cboRelation.SelectedValue)
            End If

            If CInt(cboDisplayName.SelectedValue) > 0 Then
                StrSearching &= "AND ext_assessorunkid = " & CInt(cboDisplayName.SelectedValue)
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables(0), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            Dim lvItem As ListViewItem

            lvExternalAssessorList.Items.Clear()
            For Each drRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("employee").ToString
                lvItem.Tag = drRow("ext_assessorunkid")
                lvItem.SubItems.Add(drRow("assessorname").ToString)
                lvItem.SubItems.Add(drRow("displayname").ToString)
                lvItem.SubItems.Add(drRow("relation").ToString)
                lvItem.SubItems.Add(drRow("phone").ToString)
                lvExternalAssessorList.Items.Add(lvItem)
            Next

            lvExternalAssessorList.GroupingColumn = colhEmployee
            lvExternalAssessorList.DisplayGroups(True)

            If lvExternalAssessorList.Items.Count > 16 Then
                colhPhNo.Width = 120 - 18
            Else
                colhPhNo.Width = 120
            End If

            'End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillEmployee()
        Dim dsList As DataSet = Nothing
        Try
            Dim objEmployee As New clsEmployee_Master
            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'dsList = objEmployee.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If

            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            'S.SANDEEP [04 JUN 2015] -- END

            'Anjan (17 Apr 2012)-End 

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsList.Tables("Employee")

            dsList = Nothing
            Dim objRelation As New clsCommon_Master
            dsList = objRelation.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation")
            cboRelation.ValueMember = "masterunkid"
            cboRelation.DisplayMember = "name"
            cboRelation.DataSource = dsList.Tables(0)

            FillDisplayName()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillDisplayName()
        Dim dsList As DataSet = Nothing
        Try
            dsList = objExternalAssessor.GetDisplayNameComboList("List", True)
            cboDisplayName.ValueMember = "Id"
            cboDisplayName.DisplayMember = "Name"
            cboDisplayName.DataSource = dsList.Tables("List")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDisplayName", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'btnNew.Enabled = User._Object.Privilege._AllowToAddExternalAssessor
            'btnEdit.Enabled = User._Object.Privilege._AllowToEditExternalAssessor
            'btnDelete.Enabled = User._Object.Privilege._AllowToDeleteExternalAssessor
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 16 MAY 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbExternalAssessor.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbExternalAssessor.ForeColor = GUI._eZeeContainerHeaderForeColor 


            Me.EZeeHeader1.GradientColor1 = GUI._HeaderBackColor1
			Me.EZeeHeader1.GradientColor2 = GUI._HeaderBackColor2 
			Me.EZeeHeader1.BorderColor = GUI._HeaderBorderColor 
			Me.EZeeHeader1.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.EZeeHeader1.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.gbExternalAssessor.Text = Language._Object.getCaption(Me.gbExternalAssessor.Name, Me.gbExternalAssessor.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.EZeeHeader1.Title = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Title" , Me.EZeeHeader1.Title)
			Me.EZeeHeader1.Message = Language._Object.getCaption(Me.EZeeHeader1.Name & "_Message" , Me.EZeeHeader1.Message)
			Me.lblExternalAssessor.Text = Language._Object.getCaption(Me.lblExternalAssessor.Name, Me.lblExternalAssessor.Text)
			Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
			Me.colhAssessorName.Text = Language._Object.getCaption(CStr(Me.colhAssessorName.Tag), Me.colhAssessorName.Text)
			Me.colhDisplayName.Text = Language._Object.getCaption(CStr(Me.colhDisplayName.Tag), Me.colhDisplayName.Text)
			Me.colhPhNo.Text = Language._Object.getCaption(CStr(Me.colhPhNo.Tag), Me.colhPhNo.Text)
			Me.colhRelation.Text = Language._Object.getCaption(CStr(Me.colhRelation.Tag), Me.colhRelation.Text)
			Me.lblRelation.Text = Language._Object.getCaption(Me.lblRelation.Name, Me.lblRelation.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select External Assessor from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this External Assessor?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>


  
End Class