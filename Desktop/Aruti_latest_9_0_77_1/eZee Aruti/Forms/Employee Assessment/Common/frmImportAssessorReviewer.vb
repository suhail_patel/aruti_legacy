﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmImportAssessorReviewer

#Region " Private Variables "

    Private mstrModuleName As String = "frmImportAssessorReviewer"
    Private mds_ImportData As DataSet
    Private m_dsImportData_eZee As DataSet
    Private mdt_ImportData_Others As New DataTable
    Private dvGriddata As DataView = Nothing
    Private imgAccept As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Accept)
    Private imgWarring As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Warring)
    Private imgError As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.HR_Data_Error)
    Private blnIsReviewer As Boolean = False

#End Region

#Region " Property "

    Public WriteOnly Property _IsReviewer() As Boolean
        Set(ByVal value As Boolean)
            blnIsReviewer = value
        End Set
    End Property

#End Region

#Region " Form's Events "

    Private Sub frmImportAssessorReviewer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If blnIsReviewer = False Then 'ASSESSOR
                Me.Name = "frmImportAssessor"
                mstrModuleName = Me.Name

                Me.Text = Language.getMessage(mstrModuleName, 17, "Import Assessor Wizard")
                Me.lblTitle.Text = Language.getMessage(mstrModuleName, 18, "Import Assessor Wizard")
                Me.lblMessage.Text = Language.getMessage(mstrModuleName, 19, "This wizard will import 'Assessor' records made from other system.")
                Me.lblARCode.Text = Language.getMessage(mstrModuleName, 20, "Assessor Code")
                Me.lblARName.Text = Language.getMessage(mstrModuleName, 21, "Assessor Name")
                radAssessor.Checked = True
            ElseIf blnIsReviewer = True Then 'REVIEWER
                Me.Name = "frmImportReviewer"
                mstrModuleName = Me.Name

                Me.Text = Language.getMessage(mstrModuleName, 22, "Import Reviewer Wizard")
                Me.lblTitle.Text = Language.getMessage(mstrModuleName, 23, "Import Reviewer Wizard")
                Me.lblMessage.Text = Language.getMessage(mstrModuleName, 24, "This wizard will import 'Reviewer' records made from other system.")
                Me.lblARCode.Text = Language.getMessage(mstrModuleName, 25, "Reviewer Code")
                Me.lblARName.Text = Language.getMessage(mstrModuleName, 26, "Reviewer Name")
                radReviewer.Checked = True
            End If


            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            txtFilePath.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmImportAssessorReviewer_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " eZee Wizard "

    Private Sub eZeeWizAR_AfterSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.AfterSwitchPagesEventArgs) Handles eZeeWizAR.AfterSwitchPages
        Try
            Select Case e.NewIndex
                Case eZeeWizAR.Pages.IndexOf(WizPageImporting)
                    Call CreateDataTable()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizReportTo_AfterSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub eZeeWizAR_BeforeSwitchPages(ByVal sender As Object, ByVal e As eZee.Common.eZeeWizard.BeforeSwitchPagesEventArgs) Handles eZeeWizAR.BeforeSwitchPages
        Try
            Select Case e.OldIndex
                Case eZeeWizAR.Pages.IndexOf(WizPageSelectFile)
                    If Not System.IO.File.Exists(txtFilePath.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        e.Cancel = True
                        Exit Sub
                    End If
                    'If radAssessor.Checked = False AndAlso radReviewer.Checked = False Then
                    '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Importation Type to import data"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    '    e.Cancel = True
                    '    Exit Sub
                    'End If

                    Dim ImportFile As New IO.FileInfo(txtFilePath.Text)

                    If ImportFile.Extension.ToLower = ".txt" Or ImportFile.Extension.ToLower = ".csv" Then
                        mds_ImportData = New DataSet
                        Using iCSV As New clsCSVData
                            iCSV.LoadCSV(txtFilePath.Text, True)
                            mds_ImportData = iCSV.CSVDataSet.Copy
                        End Using
                    ElseIf ImportFile.Extension.ToLower = ".xls" Or ImportFile.Extension.ToLower = ".xlsx" Then
                        'S.SANDEEP [12-Jan-2018] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 0001843
                        'Dim iExcelData As New ExcelData
                        'Dim ds As DataSet = iExcelData.Import(txtFilePath.Text)
                        'mds_ImportData = iExcelData.Import(txtFilePath.Text)

                        Dim ds As DataSet = OpenXML_Import(txtFilePath.Text)
                        mds_ImportData = OpenXML_Import(txtFilePath.Text)
                        'S.SANDEEP [12-Jan-2018] -- END
                    ElseIf ImportFile.Extension.ToLower = ".xml" Then
                        mds_ImportData = New DataSet
                        mds_ImportData.ReadXml(txtFilePath.Text)
                    Else
                        e.Cancel = True
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select the correct file to Import Data from."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                        Exit Sub
                    End If
                    Call SetDataCombo()
                    'Shani(01-MAR-2016) -- Start
                    'Enhancement :PA External Approver Flow
                    Call EnableDisableMappingControls()
                    'Shani(01-MAR-2016) -- End
                Case eZeeWizAR.Pages.IndexOf(WizPageMapping)
                    If e.NewIndex > e.OldIndex Then
                        For Each ctrl As Control In gbFieldMapping.Controls
                            'Shani(01-MAR-2016) -- Start
                            'Enhancement :PA External Approver Flow
                            'If TypeOf ctrl Is ComboBox Then
                            If TypeOf ctrl Is ComboBox AndAlso ctrl.Enabled = True Then
                                'Shani(01-MAR-2016) -- End
                                If CType(ctrl, ComboBox).Text = "" Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select the correct field to Import Data."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                    e.Cancel = True
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Case eZeeWizAR.Pages.IndexOf(WizPageImporting)
                    Me.Close()
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "eZeeWizAR_BeforeSwitchPages", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub SetDataCombo()
        Try
            For Each ctrl As Control In gbFieldMapping.Controls
                If TypeOf ctrl Is ComboBox Then
                    Call ClearCombo(CType(ctrl, ComboBox))
                End If
            Next

            For Each dtColumns As DataColumn In mds_ImportData.Tables(0).Columns
                cboECode.Items.Add(dtColumns.ColumnName)
                cboEName.Items.Add(dtColumns.ColumnName)
                cboARCode.Items.Add(dtColumns.ColumnName)
                cboARName.Items.Add(dtColumns.ColumnName)
                'Shani(01-MAR-2016) -- Start
                'Enhancement :PA External Approver Flow
                cboUserName.Items.Add(dtColumns.ColumnName)
                'Shani(01-MAR-2016) -- End
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDataCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ClearCombo(ByVal cboComboBox As ComboBox)
        Try
            cboComboBox.Items.Clear()
            AddHandler cboComboBox.SelectedIndexChanged, AddressOf Combobox_SelectedIndexChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ClearCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub CreateDataTable()
        Dim blnIsNotThrown As Boolean = True
        ezWait.Active = True
        Try
            mdt_ImportData_Others.Columns.Add("ecode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("ename", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("arcode", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("arname", System.Type.GetType("System.String"))

            mdt_ImportData_Others.Columns.Add("image", System.Type.GetType("System.Object"))
            mdt_ImportData_Others.Columns.Add("Message", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("Status", System.Type.GetType("System.String"))
            mdt_ImportData_Others.Columns.Add("objStatus", System.Type.GetType("System.String"))


            Dim dtTemp() As DataRow = mds_ImportData.Tables(0).Select("" & cboECode.Text & " IS NULL")
            For i As Integer = 0 To dtTemp.Length - 1
                mds_ImportData.Tables(0).Rows.Remove(dtTemp(i))
            Next
            mds_ImportData.AcceptChanges()

            Dim drNewRow As DataRow

            For Each dtRow As DataRow In mds_ImportData.Tables(0).Rows
                blnIsNotThrown = CheckInvalidData(dtRow)
                If blnIsNotThrown = False Then Exit Sub
                drNewRow = mdt_ImportData_Others.NewRow

                drNewRow.Item("ecode") = dtRow.Item(cboECode.Text).ToString.Trim
                drNewRow.Item("ename") = dtRow.Item(cboEName.Text).ToString.Trim

                'Shani(01-MAR-2016) -- Start
                'Enhancement :PA External Approver Flow
                'drNewRow.Item("arcode") = dtRow.Item(cboARCode.Text).ToString.Trim
                'drNewRow.Item("arname") = dtRow.Item(cboARName.Text).ToString.Trim
                If chkIsExternalEntity.Checked = False Then
                    drNewRow.Item("arcode") = dtRow.Item(cboARCode.Text).ToString.Trim
                    drNewRow.Item("arname") = dtRow.Item(cboARName.Text).ToString.Trim
                Else
                    drNewRow.Item("arcode") = dtRow.Item(cboUserName.Text).ToString.Trim
                    drNewRow.Item("arname") = dtRow.Item(cboUserName.Text).ToString.Trim
                End If
                'Shani(01-MAR-2016) -- End

                drNewRow.Item("image") = New Drawing.Bitmap(1, 1).Clone
                drNewRow.Item("Message") = ""
                drNewRow.Item("Status") = ""
                drNewRow.Item("objStatus") = ""

                mdt_ImportData_Others.Rows.Add(drNewRow)
                objTotal.Text = CStr(Val(objTotal.Text) + 1)
            Next

            If radAssessor.Checked = True Then
                colhReportingTo.HeaderText = Language.getMessage(mstrModuleName, 2, "Assessor")
            ElseIf radReviewer.Checked = True Then
                colhReportingTo.HeaderText = Language.getMessage(mstrModuleName, 16, "Reviewer")
            End If

            If blnIsNotThrown = True Then
                colhEmployee.DataPropertyName = "ename"
                colhReportingTo.DataPropertyName = "arname"
                colhMessage.DataPropertyName = "message"
                objcolhImage.DataPropertyName = "image"
                colhStatus.DataPropertyName = "status"
                objcolhstatus.DataPropertyName = "objStatus"
                dgData.AutoGenerateColumns = False
                dvGriddata = New DataView(mdt_ImportData_Others)
                dgData.DataSource = dvGriddata
            End If

            Call Import_Data()

            ezWait.Active = False
            eZeeWizAR.BackEnabled = False
            eZeeWizAR.CancelText = "Finish"

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateDataTable", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function CheckInvalidData(ByVal dtRow As DataRow) As Boolean
        Try
            With dtRow
                If .Item(cboECode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Code is mandatory information. Please select Employee Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If .Item(cboEName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Name is mandatory information. Please select Employee Name to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                'Shani(01-MAR-2016) -- Start
                'Enhancement :PA External Approver Flow
                'If .Item(cboARCode.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Assessor/Reviewer Code is mandatory information. Please select Assessor/Reviewer Code to continue."), enMsgBoxStyle.Information)
                '    Return False
                'End If

                'If .Item(cboARName.Text).ToString.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Assessor/Reviewer Name is mandatory information. Please select Assessor/Reviewer Name to continue."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                If cboARCode.Enabled = True AndAlso .Item(cboARCode.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Assessor/Reviewer Code is mandatory information. Please select Assessor/Reviewer Code to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If cboARName.Enabled = True AndAlso .Item(cboARName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Assessor/Reviewer Name is mandatory information. Please select Assessor/Reviewer Name to continue."), enMsgBoxStyle.Information)
                    Return False
                End If

                If cboUserName.Enabled = True AndAlso .Item(cboUserName.Text).ToString.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "External Username cannot be blank. Please set External Username in order to import."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Shani(01-MAR-2016) -- End
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckInvalidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Import_Data()
        btnFilter.Enabled = False
        Try
            If mdt_ImportData_Others.Rows.Count <= 0 Then Exit Sub
            Dim iAccessEmpId, iAREmpId, iDepartmentId, iRetValue As Integer
            Dim objEmp As New clsEmployee_Master
            Dim objAssessTran As clsAssessor_tran

            For Each dtRow As DataRow In mdt_ImportData_Others.Rows
                Try
                    dgData.FirstDisplayedScrollingRowIndex = mdt_ImportData_Others.Rows.IndexOf(dtRow) - 5
                    Application.DoEvents()
                    ezWait.Refresh()
                Catch ex As Exception
                End Try

                iAccessEmpId = 0 : iAREmpId = 0 : iDepartmentId = 0 : iRetValue = 0
                '--------------- CHECK IF EMOPLOYEE PRESENT
                If dtRow.Item("ecode").ToString.Trim.Length > 0 Then
                    iAccessEmpId = objEmp.GetEmployeeUnkid("", dtRow.Item("ecode").ToString.Trim)
                    If iAccessEmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 10, "Employee Not Found.")
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    Else
                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objEmp._Employeeunkid = iAccessEmpId
                        objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = iAccessEmpId
                        'S.SANDEEP [04 JUN 2015] -- END
                        iDepartmentId = objEmp._Departmentunkid
                    End If
                End If
                '--------------- CHECK IF ASSESSOR/REVIEWER PRESENT
                If dtRow.Item("arcode").ToString.Trim.Length > 0 Then

                    'Shani(01-MAR-2016) -- Start
                    'Enhancement :PA External Approver Flow
                    'iAREmpId = objEmp.GetEmployeeUnkid("", dtRow.Item("arcode").ToString.Trim)
                    If chkIsExternalEntity.Checked Then
                        Dim objUserAddEdit As New clsUserAddEdit
                        iAREmpId = objUserAddEdit.Return_UserId(CStr(dtRow.Item("arcode")).Trim, "hrmsConfiguration", enLoginMode.USER)
                        objUserAddEdit._Userunkid = iAREmpId
                        If objUserAddEdit._EmployeeCompanyUnkid = Company._Object._Companyunkid Then
                            iAREmpId = -1
                        End If
                        objUserAddEdit = Nothing
                    Else
                        iAREmpId = objEmp.GetEmployeeUnkid("", dtRow.Item("arcode").ToString.Trim)
                    End If
                    'Shani(01-MAR-2016) -- End
                    If iAREmpId <= 0 Then
                        dtRow.Item("image") = imgError
                        If radAssessor.Checked = True Then
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 12, "Assessor Not Found.")
                        Else
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 13, "Reviewer Not Found.")
                        End If
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                        Continue For
                    End If
                End If

                objAssessTran = New clsAssessor_tran
                Dim iMessage As String = String.Empty

                'Shani(01-MAR-2016) -- Start
                'Enhancement :PA External Approver Flow
                'iRetValue = objAssessTran.Import_Assesor_Reviewe_Access(iAccessEmpId, iDepartmentId, iAREmpId, CBool(IIf(radAssessor.Checked = True, False, True)), iMessage)

                If dtRow.Item("ecode").ToString.Trim = dtRow.Item("arcode").ToString.Trim Then
                    dtRow.Item("image") = imgError
                    If blnIsReviewer Then
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 28, "Sorry, you cannot be the reviewer of yourself.")
                    Else
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 29, "Sorry, you cannot be the assessor of yourself.")
                    End If
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objAssessTran._FormName = mstrModuleName
                objAssessTran._LoginEmployeeunkid = 0
                objAssessTran._ClientIP = getIP()
                objAssessTran._HostName = getHostName()
                objAssessTran._FromWeb = False
                objAssessTran._AuditUserId = User._Object._Userunkid
objAssessTran._CompanyUnkid = Company._Object._Companyunkid
                objAssessTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objAssessTran.CheckEmployeeInsert(iAccessEmpId, blnIsReviewer) Then
                    dtRow.Item("image") = imgError
                    If blnIsReviewer Then
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 30, "Sorry, you cannot assign this employee to selected reviewer. Reason: This employee is already assigned to other reviewer")
                    Else
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 31, "Sorry, you cannot assign this employee to selected assessor. Reason: This employee is already assigned to other assessor")
                    End If
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If

                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
                If objAssessTran.CheckEmployeeInsert(iAccessEmpId, blnIsReviewer, iAREmpId) Then
                    dtRow.Item("image") = imgError
                    If blnIsReviewer Then
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 32, "Sorry, you cannot assign this employee to selected reviewer. Reason: This employee is already assigned to this reviewer as assessor")
                    Else
                        dtRow.Item("message") = Language.getMessage(mstrModuleName, 33, "Sorry, you cannot assign this employee to selected assessor. Reason: This employee is already assigned to this assessor as reviewer")
                    End If
                    dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                    dtRow.Item("objStatus") = 2
                    objError.Text = CStr(Val(objError.Text) + 1)
                    Continue For
                End If
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objAssessTran._FormName = mstrModuleName
                objAssessTran._LoginEmployeeunkid = 0
                objAssessTran._ClientIP = getIP()
                objAssessTran._HostName = getHostName()
                objAssessTran._FromWeb = False
                objAssessTran._AuditUserId = User._Object._Userunkid
objAssessTran._CompanyUnkid = Company._Object._Companyunkid
                objAssessTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                iRetValue = objAssessTran.Import_Assesor_Reviewe_Access(iAccessEmpId, iDepartmentId, iAREmpId, CBool(IIf(radAssessor.Checked = True, False, True)), iMessage, chkIsExternalEntity.Checked)
                'Shani(01-MAR-2016)-- End


                Select Case iRetValue
                    Case -1 'ERROR
                        dtRow.Item("image") = imgError
                        dtRow.Item("message") = iMessage
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 2
                        objError.Text = CStr(Val(objError.Text) + 1)
                    Case 0  'WARNING
                        dtRow.Item("image") = imgWarring

                        'Shani(01-MAR-2016) -- Start
                        'Enhancement :PA External Approver Flow
                        'dtRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Access Already Exists.")
                        If iMessage.Trim.Length > 0 Then
                            dtRow.Item("message") = iMessage
                        Else
                            dtRow.Item("message") = Language.getMessage(mstrModuleName, 14, "Access Already Exists.")
                        End If
                        'Shani(01-MAR-2016) -- End
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 11, "Fail")
                        dtRow.Item("objStatus") = 0
                        objWarning.Text = CStr(Val(objWarning.Text) + 1)
                    Case 1  'SUCCESS
                        dtRow.Item("image") = imgAccept
                        dtRow.Item("message") = ""
                        dtRow.Item("status") = Language.getMessage(mstrModuleName, 15, "Success")
                        dtRow.Item("objStatus") = 1
                        objSuccess.Text = CStr(Val(objSuccess.Text) + 1)
                End Select
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Data", mstrModuleName)
        Finally
            btnFilter.Enabled = True
        End Try
    End Sub

    'Shani(01-MAR-2016) -- Start
    'Enhancement :PA External Approver Flow
    Private Sub EnableDisableMappingControls()
        Try
            objlblSign6.Enabled = chkIsExternalEntity.Checked
            lblUsername.Enabled = chkIsExternalEntity.Checked
            cboUserName.Enabled = chkIsExternalEntity.Checked

            objlblSign2.Enabled = Not chkIsExternalEntity.Checked
            lblARCode.Enabled = Not chkIsExternalEntity.Checked
            cboARCode.Enabled = Not chkIsExternalEntity.Checked

            objlblSign3.Enabled = Not chkIsExternalEntity.Checked
            lblARName.Enabled = Not chkIsExternalEntity.Checked
            cboARName.Enabled = Not chkIsExternalEntity.Checked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableDisableMappingControls", mstrModuleName)
        Finally
        End Try
    End Sub
    'Shani(01-MAR-2016) -- End

#End Region

#Region " Controls Events "

    Private Sub Combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim cmb As ComboBox = CType(sender, ComboBox)

            If cmb.Text <> "" Then

                cmb.Tag = mds_ImportData.Tables(0).Columns(cmb.Text).DataType.ToString

                For Each cr As Control In gbFieldMapping.Controls
                    If cr.GetType.FullName.ToString = "System.Windows.Forms.ComboBox" Then

                        If cr.Name <> cmb.Name Then

                            If CType(cr, ComboBox).SelectedIndex = cmb.SelectedIndex Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "This field is already selected.Please Select New field."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                                cmb.SelectedIndex = -1
                                cmb.Select()
                                Exit Sub
                            End If

                        End If

                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combobox_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmExportError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmExportError.Click
        Try
            Dim savDialog As New SaveFileDialog
            dvGriddata.RowFilter = "objStatus = 2"
            Dim dtTable As DataTable = dvGriddata.ToTable
            If dtTable.Rows.Count > 0 Then
                savDialog.Filter = "Execl files(*.xlsx)|*.xlsx"
                If savDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    dtTable.Columns.Remove("image")
                    dtTable.Columns.Remove("objstatus")

                    If modGlobal.Export_ErrorList(savDialog.FileName, dtTable, "Import Qalification Wizard") = True Then
                        Process.Start(savDialog.FileName)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmExportError_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub tsmSuccessful_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmSuccessful.Click
        Try
            dvGriddata.RowFilter = "objStatus = 1"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmSuccessful_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowError.Click
        Try
            dvGriddata.RowFilter = "objStatus = 2"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowError_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowWaning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowWarning.Click
        Try
            dvGriddata.RowFilter = "objStatus = 0"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub tsmShowAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmShowAll.Click
        Try
            dvGriddata.RowFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tsmShowWaning_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOpenFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Try
            Dim objFileOpen As New OpenFileDialog
            objFileOpen.Filter = "Excel File(*.xlsx)|*.xlsx"

            If objFileOpen.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtFilePath.Text = objFileOpen.FileName
            End If

            objFileOpen = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOpenFile_Click", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFieldMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFieldMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOpenFile.GradientBackColor = GUI._ButttonBackColor
            Me.btnOpenFile.GradientForeColor = GUI._ButttonFontColor

            Me.btnFilter.GradientBackColor = GUI._ButttonBackColor
            Me.btnFilter.GradientForeColor = GUI._ButttonFontColor

            Me.objbuttonSave.GradientBackColor = GUI._ButttonBackColor
            Me.objbuttonSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.tsmShowAll.Text = Language._Object.getCaption(Me.tsmShowAll.Name, Me.tsmShowAll.Text)
            Me.tsmSuccessful.Text = Language._Object.getCaption(Me.tsmSuccessful.Name, Me.tsmSuccessful.Text)
            Me.tsmShowWarning.Text = Language._Object.getCaption(Me.tsmShowWarning.Name, Me.tsmShowWarning.Text)
            Me.tsmShowError.Text = Language._Object.getCaption(Me.tsmShowError.Name, Me.tsmShowError.Text)
            Me.tsmExportError.Text = Language._Object.getCaption(Me.tsmExportError.Name, Me.tsmExportError.Text)
            Me.eZeeWizAR.CancelText = Language._Object.getCaption(Me.eZeeWizAR.Name & "_CancelText", Me.eZeeWizAR.CancelText)
            Me.eZeeWizAR.NextText = Language._Object.getCaption(Me.eZeeWizAR.Name & "_NextText", Me.eZeeWizAR.NextText)
            Me.eZeeWizAR.BackText = Language._Object.getCaption(Me.eZeeWizAR.Name & "_BackText", Me.eZeeWizAR.BackText)
            Me.eZeeWizAR.FinishText = Language._Object.getCaption(Me.eZeeWizAR.Name & "_FinishText", Me.eZeeWizAR.FinishText)
            Me.gbFieldMapping.Text = Language._Object.getCaption(Me.gbFieldMapping.Name, Me.gbFieldMapping.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.Name, Me.lblCaption.Text)
            Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.Name, Me.lblTitle.Text)
            Me.btnOpenFile.Text = Language._Object.getCaption(Me.btnOpenFile.Name, Me.btnOpenFile.Text)
            Me.lblSelectfile.Text = Language._Object.getCaption(Me.lblSelectfile.Name, Me.lblSelectfile.Text)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(Me.colhEmployee.Name, Me.colhEmployee.HeaderText)
            Me.colhReportingTo.HeaderText = Language._Object.getCaption(Me.colhReportingTo.Name, Me.colhReportingTo.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)
            Me.colhMessage.HeaderText = Language._Object.getCaption(Me.colhMessage.Name, Me.colhMessage.HeaderText)
            Me.btnFilter.Text = Language._Object.getCaption(Me.btnFilter.Name, Me.btnFilter.Text)
            Me.ezWait.Text = Language._Object.getCaption(Me.ezWait.Name, Me.ezWait.Text)
            Me.lblWarning.Text = Language._Object.getCaption(Me.lblWarning.Name, Me.lblWarning.Text)
            Me.lblError.Text = Language._Object.getCaption(Me.lblError.Name, Me.lblError.Text)
            Me.lblSuccess.Text = Language._Object.getCaption(Me.lblSuccess.Name, Me.lblSuccess.Text)
            Me.lblTotal.Text = Language._Object.getCaption(Me.lblTotal.Name, Me.lblTotal.Text)
            Me.elImportType.Text = Language._Object.getCaption(Me.elImportType.Name, Me.elImportType.Text)
            Me.radReviewer.Text = Language._Object.getCaption(Me.radReviewer.Name, Me.radReviewer.Text)
            Me.radAssessor.Text = Language._Object.getCaption(Me.radAssessor.Name, Me.radAssessor.Text)
            Me.lblARName.Text = Language._Object.getCaption(Me.lblARName.Name, Me.lblARName.Text)
            Me.lblARCode.Text = Language._Object.getCaption(Me.lblARCode.Name, Me.lblARCode.Text)
            Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.Name, Me.lblEmployeeCode.Text)
            Me.chkIsExternalEntity.Text = Language._Object.getCaption(Me.chkIsExternalEntity.Name, Me.chkIsExternalEntity.Text)
            Me.lblUsername.Text = Language._Object.getCaption(Me.lblUsername.Name, Me.lblUsername.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select the correct file to Import Data from.")
            Language.setMessage(mstrModuleName, 2, "Assessor")
            Language.setMessage(mstrModuleName, 3, "Employee Code is mandatory information. Please select Employee Code to continue.")
            Language.setMessage(mstrModuleName, 4, "Employee Name is mandatory information. Please select Employee Name to continue.")
            Language.setMessage(mstrModuleName, 5, "Assessor/Reviewer Code is mandatory information. Please select Assessor/Reviewer Code to continue.")
            Language.setMessage(mstrModuleName, 6, "Assessor/Reviewer Name is mandatory information. Please select Assessor/Reviewer Name to continue.")
            Language.setMessage(mstrModuleName, 8, "Please select the correct field to Import Data.")
            Language.setMessage(mstrModuleName, 9, "This field is already selected.Please Select New field.")
            Language.setMessage(mstrModuleName, 10, "Employee Not Found.")
            Language.setMessage(mstrModuleName, 11, "Fail")
            Language.setMessage(mstrModuleName, 12, "Assessor Not Found.")
            Language.setMessage(mstrModuleName, 13, "Reviewer Not Found.")
            Language.setMessage(mstrModuleName, 14, "Access Already Exists.")
            Language.setMessage(mstrModuleName, 15, "Success")
            Language.setMessage(mstrModuleName, 16, "Reviewer")
            Language.setMessage(mstrModuleName, 17, "Import Assessor Wizard")
            Language.setMessage(mstrModuleName, 18, "Import Assessor Wizard")
            Language.setMessage(mstrModuleName, 19, "This wizard will import 'Assessor' records made from other system.")
            Language.setMessage(mstrModuleName, 20, "Assessor Code")
            Language.setMessage(mstrModuleName, 21, "Assessor Name")
            Language.setMessage(mstrModuleName, 22, "Import Reviewer Wizard")
            Language.setMessage(mstrModuleName, 23, "Import Reviewer Wizard")
            Language.setMessage(mstrModuleName, 24, "This wizard will import 'Reviewer' records made from other system.")
            Language.setMessage(mstrModuleName, 25, "Reviewer Code")
            Language.setMessage(mstrModuleName, 26, "Reviewer Name")
            Language.setMessage(mstrModuleName, 27, "External Username cannot be blank. Please set External Username in order to import.")
            Language.setMessage(mstrModuleName, 28, "Sorry, you cannot be the reviewer of yourself.")
            Language.setMessage(mstrModuleName, 29, "Sorry, you cannot be the assessor of yourself.")
            Language.setMessage(mstrModuleName, 30, "Sorry, you cannot assign this employee to selected reviewer. Reason: This employee is already assigned to other reviewer")
            Language.setMessage(mstrModuleName, 31, "Sorry, you cannot assign this employee to selected assessor. Reason: This employee is already assigned to other assessor")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class