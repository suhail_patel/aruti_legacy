﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOwrFieldsList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOwrFieldsList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objspc1 = New System.Windows.Forms.SplitContainer
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchOwner = New eZee.Common.eZeeGradientButton
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.cboFieldValue4 = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.objbtnSearchField3 = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchField4 = New eZee.Common.eZeeGradientButton
        Me.cboFieldValue3 = New System.Windows.Forms.ComboBox
        Me.objlblField4 = New System.Windows.Forms.Label
        Me.objlblField3 = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblOwner = New System.Windows.Forms.Label
        Me.cboOwner = New System.Windows.Forms.ComboBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.cboFieldValue5 = New System.Windows.Forms.ComboBox
        Me.objlblField5 = New System.Windows.Forms.Label
        Me.cboOwnerType = New System.Windows.Forms.ComboBox
        Me.lblOwnerType = New System.Windows.Forms.Label
        Me.objbtnSearchField5 = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchField2 = New eZee.Common.eZeeGradientButton
        Me.objlblField1 = New System.Windows.Forms.Label
        Me.cboFieldValue2 = New System.Windows.Forms.ComboBox
        Me.objlblField2 = New System.Windows.Forms.Label
        Me.objbtnSearchField1 = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.cboFieldValue1 = New System.Windows.Forms.ComboBox
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhAdd = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhEdit = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblTotalWeight = New System.Windows.Forms.Label
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnOperation = New eZee.Common.eZeeSplitButton
        Me.cmnuOperations = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuCommit = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUnlock = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalAssign = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUpdatePercentage = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuVoidOwnerGoals = New System.Windows.Forms.ToolStripMenuItem
        Me.objbgWorker = New System.ComponentModel.BackgroundWorker
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.mnuGlobalOperation = New System.Windows.Forms.ToolStripMenuItem
        Me.pnlMain.SuspendLayout()
        Me.objspc1.Panel1.SuspendLayout()
        Me.objspc1.Panel2.SuspendLayout()
        Me.objspc1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperations.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objspc1)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(919, 552)
        Me.pnlMain.TabIndex = 0
        '
        'objspc1
        '
        Me.objspc1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objspc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objspc1.Location = New System.Drawing.Point(0, 0)
        Me.objspc1.Margin = New System.Windows.Forms.Padding(0)
        Me.objspc1.Name = "objspc1"
        Me.objspc1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objspc1.Panel1
        '
        Me.objspc1.Panel1.Controls.Add(Me.gbFilterCriteria)
        '
        'objspc1.Panel2
        '
        Me.objspc1.Panel2.Controls.Add(Me.dgvData)
        Me.objspc1.Size = New System.Drawing.Size(919, 497)
        Me.objspc1.SplitterDistance = 168
        Me.objspc1.SplitterWidth = 2
        Me.objspc1.TabIndex = 7
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchOwner)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue4)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField3)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField4)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue3)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField4)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField3)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblOwner)
        Me.gbFilterCriteria.Controls.Add(Me.cboOwner)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue5)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField5)
        Me.gbFilterCriteria.Controls.Add(Me.cboOwnerType)
        Me.gbFilterCriteria.Controls.Add(Me.lblOwnerType)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField5)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField2)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField1)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue2)
        Me.gbFilterCriteria.Controls.Add(Me.objlblField2)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchField1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.cboFieldValue1)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 71
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(919, 168)
        Me.gbFilterCriteria.TabIndex = 5
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchOwner
        '
        Me.objbtnSearchOwner.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOwner.BorderSelected = False
        Me.objbtnSearchOwner.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOwner.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOwner.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOwner.Location = New System.Drawing.Point(263, 139)
        Me.objbtnSearchOwner.Name = "objbtnSearchOwner"
        Me.objbtnSearchOwner.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOwner.TabIndex = 481
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 250
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(596, 139)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(243, 21)
        Me.cboStatus.TabIndex = 71
        '
        'cboFieldValue4
        '
        Me.cboFieldValue4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue4.DropDownWidth = 250
        Me.cboFieldValue4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue4.FormattingEnabled = True
        Me.cboFieldValue4.Location = New System.Drawing.Point(596, 51)
        Me.cboFieldValue4.Name = "cboFieldValue4"
        Me.cboFieldValue4.Size = New System.Drawing.Size(243, 21)
        Me.cboFieldValue4.TabIndex = 464
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(598, 119)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(239, 17)
        Me.lblStatus.TabIndex = 72
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchField3
        '
        Me.objbtnSearchField3.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField3.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField3.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField3.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField3.BorderSelected = False
        Me.objbtnSearchField3.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField3.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField3.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField3.Location = New System.Drawing.Point(547, 139)
        Me.objbtnSearchField3.Name = "objbtnSearchField3"
        Me.objbtnSearchField3.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchField3.TabIndex = 466
        '
        'objbtnSearchField4
        '
        Me.objbtnSearchField4.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField4.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField4.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField4.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField4.BorderSelected = False
        Me.objbtnSearchField4.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField4.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField4.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField4.Location = New System.Drawing.Point(845, 51)
        Me.objbtnSearchField4.Name = "objbtnSearchField4"
        Me.objbtnSearchField4.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchField4.TabIndex = 466
        '
        'cboFieldValue3
        '
        Me.cboFieldValue3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue3.DropDownWidth = 250
        Me.cboFieldValue3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue3.FormattingEnabled = True
        Me.cboFieldValue3.Location = New System.Drawing.Point(298, 139)
        Me.cboFieldValue3.Name = "cboFieldValue3"
        Me.cboFieldValue3.Size = New System.Drawing.Size(243, 21)
        Me.cboFieldValue3.TabIndex = 464
        '
        'objlblField4
        '
        Me.objlblField4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField4.Location = New System.Drawing.Point(598, 31)
        Me.objlblField4.Name = "objlblField4"
        Me.objlblField4.Size = New System.Drawing.Size(239, 17)
        Me.objlblField4.TabIndex = 465
        Me.objlblField4.Text = "#Caption"
        Me.objlblField4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblField3
        '
        Me.objlblField3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField3.Location = New System.Drawing.Point(300, 119)
        Me.objlblField3.Name = "objlblField3"
        Me.objlblField3.Size = New System.Drawing.Size(236, 17)
        Me.objlblField3.TabIndex = 465
        Me.objlblField3.Text = "#Caption"
        Me.objlblField3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 31)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(189, 17)
        Me.lblPeriod.TabIndex = 476
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblOwner
        '
        Me.lblOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOwner.Location = New System.Drawing.Point(12, 119)
        Me.lblOwner.Name = "lblOwner"
        Me.lblOwner.Size = New System.Drawing.Size(189, 17)
        Me.lblOwner.TabIndex = 479
        Me.lblOwner.Text = "Owner"
        Me.lblOwner.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOwner
        '
        Me.cboOwner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOwner.DropDownWidth = 200
        Me.cboOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOwner.FormattingEnabled = True
        Me.cboOwner.Location = New System.Drawing.Point(12, 139)
        Me.cboOwner.Name = "cboOwner"
        Me.cboOwner.Size = New System.Drawing.Size(245, 21)
        Me.cboOwner.TabIndex = 480
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(12, 51)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(245, 21)
        Me.cboPeriod.TabIndex = 477
        '
        'cboFieldValue5
        '
        Me.cboFieldValue5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue5.DropDownWidth = 250
        Me.cboFieldValue5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue5.FormattingEnabled = True
        Me.cboFieldValue5.Location = New System.Drawing.Point(596, 95)
        Me.cboFieldValue5.Name = "cboFieldValue5"
        Me.cboFieldValue5.Size = New System.Drawing.Size(243, 21)
        Me.cboFieldValue5.TabIndex = 467
        '
        'objlblField5
        '
        Me.objlblField5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField5.Location = New System.Drawing.Point(598, 75)
        Me.objlblField5.Name = "objlblField5"
        Me.objlblField5.Size = New System.Drawing.Size(239, 17)
        Me.objlblField5.TabIndex = 468
        Me.objlblField5.Text = "#Caption"
        Me.objlblField5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOwnerType
        '
        Me.cboOwnerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOwnerType.DropDownWidth = 200
        Me.cboOwnerType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOwnerType.FormattingEnabled = True
        Me.cboOwnerType.Location = New System.Drawing.Point(12, 95)
        Me.cboOwnerType.Name = "cboOwnerType"
        Me.cboOwnerType.Size = New System.Drawing.Size(245, 21)
        Me.cboOwnerType.TabIndex = 475
        '
        'lblOwnerType
        '
        Me.lblOwnerType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOwnerType.Location = New System.Drawing.Point(12, 75)
        Me.lblOwnerType.Name = "lblOwnerType"
        Me.lblOwnerType.Size = New System.Drawing.Size(189, 17)
        Me.lblOwnerType.TabIndex = 474
        Me.lblOwnerType.Text = "Owner Type"
        Me.lblOwnerType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchField5
        '
        Me.objbtnSearchField5.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField5.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField5.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField5.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField5.BorderSelected = False
        Me.objbtnSearchField5.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField5.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField5.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField5.Location = New System.Drawing.Point(845, 95)
        Me.objbtnSearchField5.Name = "objbtnSearchField5"
        Me.objbtnSearchField5.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchField5.TabIndex = 469
        '
        'objbtnSearchField2
        '
        Me.objbtnSearchField2.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField2.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField2.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField2.BorderSelected = False
        Me.objbtnSearchField2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField2.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField2.Location = New System.Drawing.Point(547, 95)
        Me.objbtnSearchField2.Name = "objbtnSearchField2"
        Me.objbtnSearchField2.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchField2.TabIndex = 466
        '
        'objlblField1
        '
        Me.objlblField1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField1.Location = New System.Drawing.Point(300, 30)
        Me.objlblField1.Name = "objlblField1"
        Me.objlblField1.Size = New System.Drawing.Size(236, 17)
        Me.objlblField1.TabIndex = 465
        Me.objlblField1.Text = "#Caption"
        Me.objlblField1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFieldValue2
        '
        Me.cboFieldValue2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue2.DropDownWidth = 250
        Me.cboFieldValue2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue2.FormattingEnabled = True
        Me.cboFieldValue2.Location = New System.Drawing.Point(298, 95)
        Me.cboFieldValue2.Name = "cboFieldValue2"
        Me.cboFieldValue2.Size = New System.Drawing.Size(243, 21)
        Me.cboFieldValue2.TabIndex = 464
        '
        'objlblField2
        '
        Me.objlblField2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblField2.Location = New System.Drawing.Point(300, 75)
        Me.objlblField2.Name = "objlblField2"
        Me.objlblField2.Size = New System.Drawing.Size(236, 17)
        Me.objlblField2.TabIndex = 465
        Me.objlblField2.Text = "#Caption"
        Me.objlblField2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchField1
        '
        Me.objbtnSearchField1.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchField1.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchField1.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchField1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchField1.BorderSelected = False
        Me.objbtnSearchField1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchField1.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchField1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchField1.Location = New System.Drawing.Point(547, 51)
        Me.objbtnSearchField1.Name = "objbtnSearchField1"
        Me.objbtnSearchField1.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchField1.TabIndex = 466
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(892, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'cboFieldValue1
        '
        Me.cboFieldValue1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFieldValue1.DropDownWidth = 250
        Me.cboFieldValue1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFieldValue1.FormattingEnabled = True
        Me.cboFieldValue1.Location = New System.Drawing.Point(298, 51)
        Me.cboFieldValue1.Name = "cboFieldValue1"
        Me.cboFieldValue1.Size = New System.Drawing.Size(243, 21)
        Me.cboFieldValue1.TabIndex = 464
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(869, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhAdd, Me.objdgcolhEdit, Me.objdgcolhDelete})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(919, 327)
        Me.dgvData.TabIndex = 5
        '
        'objdgcolhAdd
        '
        Me.objdgcolhAdd.Frozen = True
        Me.objdgcolhAdd.HeaderText = ""
        Me.objdgcolhAdd.Image = Global.Aruti.Main.My.Resources.Resources.add_16
        Me.objdgcolhAdd.Name = "objdgcolhAdd"
        Me.objdgcolhAdd.ReadOnly = True
        Me.objdgcolhAdd.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhAdd.Width = 25
        '
        'objdgcolhEdit
        '
        Me.objdgcolhEdit.Frozen = True
        Me.objdgcolhEdit.HeaderText = ""
        Me.objdgcolhEdit.Image = Global.Aruti.Main.My.Resources.Resources.edit
        Me.objdgcolhEdit.Name = "objdgcolhEdit"
        Me.objdgcolhEdit.ReadOnly = True
        Me.objdgcolhEdit.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhEdit.Width = 25
        '
        'objdgcolhDelete
        '
        Me.objdgcolhDelete.Frozen = True
        Me.objdgcolhDelete.HeaderText = ""
        Me.objdgcolhDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objdgcolhDelete.Name = "objdgcolhDelete"
        Me.objdgcolhDelete.ReadOnly = True
        Me.objdgcolhDelete.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhDelete.Width = 25
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblTotalWeight)
        Me.objFooter.Controls.Add(Me.objlblProgress)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnOperation)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 497)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(919, 55)
        Me.objFooter.TabIndex = 5
        '
        'objlblTotalWeight
        '
        Me.objlblTotalWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblTotalWeight.Location = New System.Drawing.Point(591, 22)
        Me.objlblTotalWeight.Name = "objlblTotalWeight"
        Me.objlblTotalWeight.Size = New System.Drawing.Size(213, 13)
        Me.objlblTotalWeight.TabIndex = 14
        Me.objlblTotalWeight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objlblProgress
        '
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.Location = New System.Drawing.Point(121, 20)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(254, 17)
        Me.objlblProgress.TabIndex = 13
        Me.objlblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(810, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnOperation
        '
        Me.btnOperation.BorderColor = System.Drawing.Color.Black
        Me.btnOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperation.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperation.Location = New System.Drawing.Point(12, 13)
        Me.btnOperation.Name = "btnOperation"
        Me.btnOperation.ShowDefaultBorderColor = True
        Me.btnOperation.Size = New System.Drawing.Size(103, 30)
        Me.btnOperation.SplitButtonMenu = Me.cmnuOperations
        Me.btnOperation.TabIndex = 12
        Me.btnOperation.Text = "Operation"
        '
        'cmnuOperations
        '
        Me.cmnuOperations.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCommit, Me.mnuUnlock, Me.mnuGlobalAssign, Me.mnuUpdatePercentage, Me.mnuVoidOwnerGoals, Me.mnuGlobalOperation})
        Me.cmnuOperations.Name = "cmnuOperations"
        Me.cmnuOperations.Size = New System.Drawing.Size(237, 158)
        '
        'mnuCommit
        '
        Me.mnuCommit.Name = "mnuCommit"
        Me.mnuCommit.Size = New System.Drawing.Size(236, 22)
        Me.mnuCommit.Tag = "mnuCommit"
        Me.mnuCommit.Text = "C&ommit"
        '
        'mnuUnlock
        '
        Me.mnuUnlock.Name = "mnuUnlock"
        Me.mnuUnlock.Size = New System.Drawing.Size(236, 22)
        Me.mnuUnlock.Tag = "mnuUnlock"
        Me.mnuUnlock.Text = "&Unlock Committed"
        '
        'mnuGlobalAssign
        '
        Me.mnuGlobalAssign.Name = "mnuGlobalAssign"
        Me.mnuGlobalAssign.Size = New System.Drawing.Size(236, 22)
        Me.mnuGlobalAssign.Tag = "mnuGlobalAssign"
        Me.mnuGlobalAssign.Text = "Global Assign"
        '
        'mnuUpdatePercentage
        '
        Me.mnuUpdatePercentage.Name = "mnuUpdatePercentage"
        Me.mnuUpdatePercentage.Size = New System.Drawing.Size(236, 22)
        Me.mnuUpdatePercentage.Tag = "mnuUpdatePercentage"
        Me.mnuUpdatePercentage.Text = "Update Percentage Completed"
        Me.mnuUpdatePercentage.Visible = False
        '
        'mnuVoidOwnerGoals
        '
        Me.mnuVoidOwnerGoals.Name = "mnuVoidOwnerGoals"
        Me.mnuVoidOwnerGoals.Size = New System.Drawing.Size(236, 22)
        Me.mnuVoidOwnerGoals.Tag = "mnuVoidOwnerGoals"
        Me.mnuVoidOwnerGoals.Text = "Void Owner Goals"
        '
        'objbgWorker
        '
        Me.objbgWorker.WorkerReportsProgress = True
        Me.objbgWorker.WorkerSupportsCancellation = True
        '
        'mnuGlobalOperation
        '
        Me.mnuGlobalOperation.Name = "mnuGlobalOperation"
        Me.mnuGlobalOperation.Size = New System.Drawing.Size(236, 22)
        Me.mnuGlobalOperation.Text = "Gloabal Operation"
        '
        'frmOwrFieldsList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(919, 552)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOwrFieldsList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Owner Goals List"
        Me.pnlMain.ResumeLayout(False)
        Me.objspc1.Panel1.ResumeLayout(False)
        Me.objspc1.Panel2.ResumeLayout(False)
        Me.objspc1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperations.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objspc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboFieldValue5 As System.Windows.Forms.ComboBox
    Friend WithEvents objlblField5 As System.Windows.Forms.Label
    Friend WithEvents cboFieldValue4 As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchField5 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchField4 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchField3 As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblField4 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchField2 As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblField3 As System.Windows.Forms.Label
    Friend WithEvents objlblField1 As System.Windows.Forms.Label
    Friend WithEvents cboFieldValue3 As System.Windows.Forms.ComboBox
    Friend WithEvents cboFieldValue2 As System.Windows.Forms.ComboBox
    Friend WithEvents objlblField2 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchField1 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents cboFieldValue1 As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents objdgcolhAdd As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhEdit As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents lblOwnerType As System.Windows.Forms.Label
    Friend WithEvents cboOwnerType As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchOwner As eZee.Common.eZeeGradientButton
    Friend WithEvents lblOwner As System.Windows.Forms.Label
    Friend WithEvents cboOwner As System.Windows.Forms.ComboBox
    Friend WithEvents btnOperation As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperations As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuCommit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUnlock As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents objbgWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents objlblTotalWeight As System.Windows.Forms.Label
    Friend WithEvents mnuGlobalAssign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUpdatePercentage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuVoidOwnerGoals As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGlobalOperation As System.Windows.Forms.ToolStripMenuItem
End Class
