﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmOwrFieldsList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmOwrFieldsList"
    Private mdtFinal As DataTable = Nothing
    Private objOwrField1 As clsassess_owrfield1_master
    Private objOwrField2 As clsassess_owrfield2_master
    Private objOwrField3 As clsassess_owrfield3_master
    Private objOwrField4 As clsassess_owrfield4_master
    Private objOwrField5 As clsassess_owrfield5_master
    Private cmnuAdd, cmnuEdit, cmnuDelete As ContextMenuStrip
    Private iOwnerRefId As Integer = 0
    Private objFMaster As New clsAssess_Field_Master(True)
    Private mintTotalCount As Integer = 0
    Private mblnDropDownClosed As Boolean = False
    Private mintLinkedFieldId As Integer = 0

#End Region

#Region " Private Methods "

    Private Sub SetFormViewSetting()
        Try
            objlblField1.Text = objFMaster._Field1_Caption
            cboFieldValue1.Tag = objFMaster._Field1Unkid
            cboFieldValue1.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField1.Text = "" Then
                cboFieldValue1.Enabled = False : objbtnSearchField1.Enabled = False
            End If
            objlblField2.Text = objFMaster._Field2_Caption
            cboFieldValue2.Tag = objFMaster._Field2Unkid
            cboFieldValue2.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField2.Text = "" Then
                cboFieldValue2.Enabled = False : objbtnSearchField2.Enabled = False
            End If
            objlblField3.Text = objFMaster._Field3_Caption
            cboFieldValue3.Tag = objFMaster._Field3Unkid
            cboFieldValue3.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField3.Text = "" Then
                cboFieldValue3.Enabled = False : objbtnSearchField3.Enabled = False
            End If
            objlblField4.Text = objFMaster._Field4_Caption
            cboFieldValue4.Tag = objFMaster._Field4Unkid
            cboFieldValue4.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField4.Text = "" Then
                cboFieldValue4.Enabled = False : objbtnSearchField4.Enabled = False
            End If
            objlblField5.Text = objFMaster._Field5_Caption
            cboFieldValue5.Tag = objFMaster._Field5Unkid
            cboFieldValue5.DrawMode = DrawMode.OwnerDrawFixed
            If objlblField5.Text = "" Then
                cboFieldValue5.Enabled = False : objbtnSearchField5.Enabled = False
            End If
            mnuGlobalAssign.Visible = ConfigParameter._Object._FollowEmployeeHierarchy
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFormViewSetting", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dtFinal As DataTable
        Dim iSearch As String = String.Empty
        Try
            If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, No data defined for asseessment caption settings screen."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objMap As New clsAssess_Field_Mapping
            If objMap.isExist(CInt(cboPeriod.SelectedValue)) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry no field is mapped with the selected period."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            objMap = Nothing

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            If User._Object.Privilege._AllowtoViewAllocationGoalsList = False Then Exit Sub
            'S.SANDEEP [28 MAY 2015] -- END

            dtFinal = objOwrField1.GetDisplayList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), "List")

            If dtFinal.Rows.Count <= 0 Then
                Dim iRow As DataRow = dtFinal.NewRow
                dtFinal.Rows.Add(iRow)
            Else
                If CInt(cboOwner.SelectedValue) > 0 Then
                    iSearch &= "AND ownerunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                End If

                If CInt(cboPeriod.SelectedValue) > 0 Then
                    iSearch &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
                End If

                If cboFieldValue1.Enabled = True AndAlso CInt(cboFieldValue1.SelectedValue) > 0 Then
                    iSearch &= "AND owrfield1unkid = '" & CInt(cboFieldValue1.SelectedValue) & "' "
                End If

                If cboFieldValue2.Enabled = True AndAlso CInt(cboFieldValue2.SelectedValue) > 0 Then
                    iSearch &= "AND owrfield2unkid = '" & CInt(cboFieldValue2.SelectedValue) & "' "
                End If

                If cboFieldValue3.Enabled = True AndAlso CInt(cboFieldValue3.SelectedValue) > 0 Then
                    iSearch &= "AND owrfield3unkid = '" & CInt(cboFieldValue3.SelectedValue) & "' "
                End If

                If cboFieldValue4.Enabled = True AndAlso CInt(cboFieldValue4.SelectedValue) > 0 Then
                    iSearch &= "AND owrfield4unkid = '" & CInt(cboFieldValue4.SelectedValue) & "' "
                End If

                If cboFieldValue5.Enabled = True AndAlso CInt(cboFieldValue5.SelectedValue) > 0 Then
                    iSearch &= "AND owrfield5unkid = '" & CInt(cboFieldValue5.SelectedValue) & "' "
                End If

                If CInt(cboStatus.SelectedValue) > 0 Then
                    iSearch &= "AND CStatusId = '" & CInt(cboStatus.SelectedValue) & "' "
                End If
            End If

            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                mdtFinal = New DataView(dtFinal, iSearch, "Field1Id DESC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
            Else
                mdtFinal = New DataView(dtFinal, "", "Field1Id DESC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
            End If

            dgvData.AutoGenerateColumns = False
            Dim iColName As String = String.Empty

            Dim iPlan() As String = Nothing
            If ConfigParameter._Object._ViewTitles_InPlanning.Trim.Length > 0 Then
                iPlan = ConfigParameter._Object._ViewTitles_InPlanning.Split("|")
            End If

            'S.SANDEEP [16 JUN 2015] -- START
            Dim dgvCol As DataGridViewColumn = Nothing
            'S.SANDEEP [16 JUN 2015] -- END

            For Each dCol As DataColumn In mdtFinal.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                If dgvData.Columns.Contains(iColName) = True Then Continue For

                'S.SANDEEP [16 JUN 2015] -- START
                'Dim dgvCol As New DataGridViewTextBoxColumn()
                If dCol.ColumnName = "vuProgress" Then
                    dgvCol = New DataGridViewLinkColumn()
                    CType(dgvCol, DataGridViewLinkColumn).LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
                Else
                    dgvCol = New DataGridViewTextBoxColumn()
                End If
                'S.SANDEEP [16 JUN 2015] -- END

                dgvCol.Name = iColName
                dgvCol.Width = 110
                'S.SANDEEP [24 SEP 2015] -- START
                'dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dgvCol.SortMode = DataGridViewColumnSortMode.Automatic
                'S.SANDEEP [24 SEP 2015] -- END
                dgvCol.ReadOnly = True
                dgvCol.DataPropertyName = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                If dCol.Caption.Length <= 0 Then
                    dgvCol.Visible = False
                Else
                    If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                        dgvCol.Width = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                            End If
                        End If
                    End If
                End If

                'S.SANDEEP [02 JUL 2015] -- START
                If dCol.ColumnName.StartsWith("CoyField") Then
                    dgvCol.Visible = False
                End If
                'S.SANDEEP [02 JUL 2015] -- END

                dgvData.Columns.Add(dgvCol)
            Next
            dgvData.DataSource = mdtFinal

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            Call SetVisibility()
            'S.SANDEEP [28 MAY 2015] -- END

            'S.SANDEEP [01 JUL 2015] -- START
            If CInt(dgvData.Rows(0).Cells("objopstatusid").Value) <> enObjective_Status.FINAL_COMMITTED Then
                dgvData.Columns("objvuRemark").Visible = False
                dgvData.Columns("objvuProgress").Visible = False
            End If
            RemoveHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            'S.SANDEEP [01 JUL 2015] -- END

            objdgcolhAdd.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhAdd.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhEdit.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhEdit.DefaultCellStyle.SelectionForeColor = Color.White

            objdgcolhDelete.DefaultCellStyle.SelectionBackColor = Color.White
            objdgcolhDelete.DefaultCellStyle.SelectionForeColor = Color.White

            For Each dgvRow As DataGridViewRow In dgvData.Rows
                If CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.FINAL_COMMITTED Then
                    dgvData.Columns(objdgcolhAdd.Index).Visible = False
                    dgvData.Columns(objdgcolhEdit.Index).Visible = False
                    dgvData.Columns(objdgcolhDelete.Index).Visible = False
                    'Exit For

                    'S.SANDEEP [01 JUL 2015] -- START
                    If dgvData.Columns("objvuRemark").Visible = False Then dgvData.Columns("objvuRemark").Visible = True
                    If dgvData.Columns("objvuProgress").Visible = False Then dgvData.Columns("objvuProgress").Visible = True
                    'S.SANDEEP [01 JUL 2015] -- END

                Else
                    'S.SANDEEP [16 JUN 2015] -- START
                    'dgvData.Columns(objdgcolhAdd.Index).Visible = True
                    'dgvData.Columns(objdgcolhEdit.Index).Visible = True
                    'dgvData.Columns(objdgcolhDelete.Index).Visible = True

                    objdgcolhAdd.Visible = User._Object.Privilege._AllowtoAddAllocationGoals
                    objdgcolhEdit.Visible = User._Object.Privilege._AllowtoEditAllocationGoals
                    objdgcolhDelete.Visible = User._Object.Privilege._AllowtoDeleteAllocationGoals
                    'S.SANDEEP [16 JUN 2015] -- END

                    'Exit For
                End If

                'S.SANDEEP [16 JUN 2015] -- START
                dgvRow.Cells("objvuProgress").Style.SelectionForeColor = Color.White
                dgvRow.Cells("objvuProgress").Style.SelectionBackColor = Color.White
                'S.SANDEEP [16 JUN 2015] -- END

            Next

            'S.SANDEEP [01 JUL 2015] -- START
            If dgvData.Columns("objvuProgress").Visible = True Then
                AddHandler dgvData.CellContentClick, AddressOf dgvData_CellContentClick
            End If
            'S.SANDEEP [01 JUL 2015] -- END

            If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Then
                objdgcolhAdd.Visible = False : objdgcolhDelete.Visible = False
            End If

            Dim objFMapping As New clsAssess_Field_Mapping
            Dim xTotalWeight As Double = 0
            xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue), 0)
            If xTotalWeight > 0 Then
                objlblTotalWeight.Text = Language.getMessage(mstrModuleName, 32, "Total Weight Assigned :") & " " & xTotalWeight.ToString
            Else
                objlblTotalWeight.Text = ""
            End If
            objFMapping = Nothing

            

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Generate_MenuItems()
        Dim dsList As New DataSet
        Try
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            If dsList.Tables(0).Rows.Count > 0 Then
                cmnuAdd.Items.Clear() : cmnuEdit.Items.Clear() : cmnuDelete.Items.Clear()
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    Dim objbtnAdd As New ToolStripMenuItem : Dim objbtnEdt As New ToolStripMenuItem : Dim objbtnDel As New ToolStripMenuItem

                    objbtnAdd.Name = "objbtnAdd" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    objbtnAdd.Text = Language.getMessage(mstrModuleName, 29, "Add") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    objbtnAdd.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    cmnuAdd.Items.Add(objbtnAdd)
                    AddHandler objbtnAdd.Click, AddressOf objbtnAdd_Click

                    objbtnEdt.Name = "objbtnEdt" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    objbtnEdt.Text = Language.getMessage(mstrModuleName, 30, "Edit") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    objbtnEdt.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    cmnuEdit.Items.Add(objbtnEdt)
                    AddHandler objbtnEdt.Click, AddressOf objbtnEdt_Click

                    objbtnDel.Name = "objbtnDel" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    objbtnDel.Text = Language.getMessage(mstrModuleName, 31, "Delete") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    objbtnDel.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    cmnuDelete.Items.Add(objbtnDel)
                    AddHandler objbtnDel.Click, AddressOf objbtnDel_Click
                Next
            End If
            objFMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_MenuItems", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    Private Sub SetVisibility()
        Try
            objdgcolhAdd.Visible = User._Object.Privilege._AllowtoAddAllocationGoals
            objdgcolhEdit.Visible = User._Object.Privilege._AllowtoEditAllocationGoals
            objdgcolhDelete.Visible = User._Object.Privilege._AllowtoDeleteAllocationGoals
            mnuCommit.Enabled = User._Object.Privilege._AllowtoCommitAllocationGoals
            mnuUnlock.Enabled = User._Object.Privilege._AllowtoUnlockcommittedAllocationGoals
            mnuGlobalAssign.Enabled = User._Object.Privilege._AllowtoPerformGlobalAssignAllocationGoals
            'S.SANDEEP [16 JUN 2015] -- START
            'mnuUpdatePercentage.Enabled = User._Object.Privilege._AllowtoUpdatePercentCompletedAllocationGoals
            If dgvData.ColumnCount > 0 Then
                If dgvData.Columns.Contains("objvuProgress") Then
                    dgvData.Columns("objvuProgress").Visible = User._Object.Privilege._AllowtoUpdatePercentCompletedAllocationGoals
                End If
            End If
            'S.SANDEEP [16 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [28 MAY 2015] -- END

#End Region

#Region " Form's Event "

    Private Sub frmOwrFieldsList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objOwrField1 = New clsassess_owrfield1_master : objOwrField2 = New clsassess_owrfield2_master
        objOwrField3 = New clsassess_owrfield3_master : objOwrField4 = New clsassess_owrfield4_master
        objOwrField5 = New clsassess_owrfield5_master
        cmnuAdd = New ContextMenuStrip() : cmnuEdit = New ContextMenuStrip() : cmnuDelete = New ContextMenuStrip()
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call Generate_MenuItems()
            Call SetFormViewSetting()
            Call FillCombo()
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            Call SetVisibility()
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOwrFieldsList_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_owrfield1_master.SetMessages()
            clsassess_owrfield2_master.SetMessages()
            clsassess_owrfield3_master.SetMessages()
            clsassess_owrfield4_master.SetMessages()
            clsassess_owrfield5_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_owrfield1_master,clsassess_owrfield2_master,clsassess_owrfield3_master,clsassess_owrfield4_master,clsassess_owrfield5_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboOwnerType.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Owner Type, Owner and Period as mandatory information. Please select Owner Type, Owner and Period to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call Fill_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboOwner.SelectedValue = 0
            cboFieldValue1.SelectedValue = 0
            cboFieldValue2.SelectedValue = 0
            cboFieldValue3.SelectedValue = 0
            cboFieldValue4.SelectedValue = 0
            cboFieldValue5.SelectedValue = 0
            dgvData.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchField1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchField1.Click, _
                                                                                                             objbtnSearchField2.Click, _
                                                                                                             objbtnSearchField3.Click, _
                                                                                                             objbtnSearchField4.Click, _
                                                                                                             objbtnSearchField5.Click, _
                                                                                                             objbtnSearchOwner.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim iCbo As ComboBox = Nothing
            Select Case CType(sender, eZee.Common.eZeeGradientButton).Name.ToUpper
                Case objbtnSearchField1.Name.ToUpper
                    iCbo = cboFieldValue1
                Case objbtnSearchField2.Name.ToUpper
                    iCbo = cboFieldValue2
                Case objbtnSearchField3.Name.ToUpper
                    iCbo = cboFieldValue3
                Case objbtnSearchField4.Name.ToUpper
                    iCbo = cboFieldValue4
                Case objbtnSearchField5.Name.ToUpper
                    iCbo = cboFieldValue5
                Case objbtnSearchOwner.Name.ToUpper
                    iCbo = cboOwner
            End Select
            If iCbo.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = iCbo.ValueMember
                .DisplayMember = iCbo.DisplayMember
                .DataSource = iCbo.DataSource
                If .DisplayDialog = True Then
                    iCbo.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchField1_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub dgvData_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvData.CellFormatting
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case objdgcolhAdd.Index
                    dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).ToolTipText = Language.getMessage(mstrModuleName, 5, "Add")
                Case objdgcolhEdit.Index
                    dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).ToolTipText = Language.getMessage(mstrModuleName, 6, "Edit")
                Case objdgcolhDelete.Index
                    dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).ToolTipText = Language.getMessage(mstrModuleName, 7, "Delete")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellFormatting", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [16 JUN 2015] -- START
    Private Sub dgvData_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvData.CellContentClick
        Try
            If dgvData.SelectedRows.Count > 0 Then
                If dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex).ColumnIndex = dgvData.Rows(e.RowIndex).Cells("objvuProgress").ColumnIndex Then
                    Dim iLinkedFieldId As Integer = 0
                    Dim iColName As String = ""
                    Dim iExOrder As Integer = 0
                    Dim objFMapping As New clsAssess_Field_Mapping
                    iLinkedFieldId = objFMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
                    objFMapping = Nothing
                    If iLinkedFieldId <= 0 Then Exit Sub
                    Dim objFMaster As New clsAssess_Field_Master
                    iExOrder = objFMaster.Get_Field_ExOrder(iLinkedFieldId)
                    Select Case iExOrder
                        Case 1
                            iColName = "objowrfield1unkid"
                        Case 2
                            iColName = "objowrfield2unkid"
                        Case 3
                            iColName = "objowrfield3unkid"
                        Case 4
                            iColName = "objowrfield4unkid"
                        Case 5
                            iColName = "objowrfield5unkid"
                    End Select


                    If dgvData.Rows(e.RowIndex).Cells("objField" & iLinkedFieldId.ToString).Value = "" Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                        " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField" & iLinkedFieldId.ToString).ColumnIndex).HeaderText & " " & _
                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                        " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField" & iLinkedFieldId.ToString).ColumnIndex).HeaderText & " " & _
                        Language.getMessage(mstrModuleName, 30, "in order to perform update progress operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    Else
                        Dim frm As New frmUpdateFieldValue

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(iLinkedFieldId, _
                                             CInt(cboOwner.SelectedValue), _
                                             CInt(cboPeriod.SelectedValue), _
                                             cboOwner.Text, cboPeriod.Text, _
                                             CInt(dgvData.Rows(e.RowIndex).Cells(iColName).Value), _
                                             clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, _
                                             dgvData.Rows(e.RowIndex).Cells("objField" & iLinkedFieldId.ToString).Value, 0, 0, "") Then 'S.SANDEEP |18-JAN-2019| -- START {goaltypeid,goalvalue} -- END,'S.SANDEEP |12-FEB-2019| -- START {uomtypeid} -- END
                            Call Fill_Grid()
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [16 JUN 2015] -- END

    Private Sub dgvData_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvData.CellMouseClick
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If e.Button = Windows.Forms.MouseButtons.Left Then
                If e.ColumnIndex < 3 Then
                    Select Case e.ColumnIndex
                        Case dgvData.Rows(e.RowIndex).Cells(objdgcolhAdd.Index).ColumnIndex
                            dgvData.CurrentCell = dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex)
                            cmnuAdd.Show(dgvData, dgvData.PointToClient(Windows.Forms.Cursor.Position))
                        Case dgvData.Rows(e.RowIndex).Cells(objdgcolhEdit.Index).ColumnIndex
                            dgvData.CurrentCell = dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex)
                            cmnuEdit.Show(dgvData, dgvData.PointToClient(Windows.Forms.Cursor.Position))
                        Case dgvData.Rows(e.RowIndex).Cells(objdgcolhDelete.Index).ColumnIndex
                            dgvData.CurrentCell = dgvData.Rows(e.RowIndex).Cells(e.ColumnIndex)
                            cmnuDelete.Show(dgvData, dgvData.PointToClient(Windows.Forms.Cursor.Position))
                    End Select
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvData_CellMouseClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmReasonSelection
        Try
            Dim iVoidReason As String = String.Empty

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim iMsg As String = Language.getMessage(mstrModuleName, 26, "You are about to remove the information from owner level, this will delete all child linked to this parent") & vbCrLf & _
Language.getMessage(mstrModuleName, 27, "Are you sure you want to delete?")

            If eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

            Select Case CInt(sender.tag.ToString.Split("|")(1))
                Case 1
                    If CInt(dgvData.CurrentRow.Cells("objowrfield1unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objOwrField1._Isvoid = True
                        objOwrField1._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objOwrField1._Voidreason = iVoidReason
                        objOwrField1._Voiduserunkid = User._Object._Userunkid

                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objOwrField1._FormName = mstrModuleName
                        objOwrField1._LoginEmployeeunkid = 0
                        objOwrField1._ClientIP = getIP()
                        objOwrField1._HostName = getHostName()
                        objOwrField1._FromWeb = False
                        objOwrField1._AuditUserId = User._Object._Userunkid
objOwrField1._CompanyUnkid = Company._Object._Companyunkid
                        objOwrField1._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END

                        If objOwrField1.Delete(CInt(dgvData.CurrentRow.Cells("objowrfield1unkid").Value)) = True Then
                            Call Fill_Grid()
                        Else
                            If objOwrField1._Message <> "" Then
                                eZeeMsgBox.Show(objOwrField1._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField1").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField1").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 2
                    If CInt(dgvData.CurrentRow.Cells("objowrfield2unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objOwrField2._Isvoid = True
                        objOwrField2._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objOwrField2._Voidreason = iVoidReason
                        objOwrField2._Voiduserunkid = User._Object._Userunkid
                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objOwrField2._FormName = mstrModuleName
                        objOwrField2._LoginEmployeeunkid = 0
                        objOwrField2._ClientIP = getIP()
                        objOwrField2._HostName = getHostName()
                        objOwrField2._FromWeb = False
                        objOwrField2._AuditUserId = User._Object._Userunkid
objOwrField2._CompanyUnkid = Company._Object._Companyunkid
                        objOwrField2._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END
                        If objOwrField2.Delete(CInt(dgvData.CurrentRow.Cells("objowrfield2unkid").Value)) = True Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField2").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField2").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 3
                    If CInt(dgvData.CurrentRow.Cells("objowrfield3unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objOwrField3._Isvoid = True
                        objOwrField3._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objOwrField3._Voidreason = iVoidReason
                        objOwrField3._Voiduserunkid = User._Object._Userunkid
                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objOwrField3._FormName = mstrModuleName
                        objOwrField3._LoginEmployeeunkid = 0
                        objOwrField3._ClientIP = getIP()
                        objOwrField3._HostName = getHostName()
                        objOwrField3._FromWeb = False
                        objOwrField3._AuditUserId = User._Object._Userunkid
objOwrField3._CompanyUnkid = Company._Object._Companyunkid
                        objOwrField3._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END
                        If objOwrField3.Delete(CInt(dgvData.CurrentRow.Cells("objowrfield3unkid").Value)) = True Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField3").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField3").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 4
                    If CInt(dgvData.CurrentRow.Cells("objowrfield4unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objOwrField4._Isvoid = True
                        objOwrField4._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objOwrField4._Voidreason = iVoidReason
                        objOwrField4._Voiduserunkid = User._Object._Userunkid
                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objOwrField4._FormName = mstrModuleName
                        objOwrField4._LoginEmployeeunkid = 0
                        objOwrField4._ClientIP = getIP()
                        objOwrField4._HostName = getHostName()
                        objOwrField4._FromWeb = False
                        objOwrField4._AuditUserId = User._Object._Userunkid
objOwrField4._CompanyUnkid = Company._Object._Companyunkid
                        objOwrField4._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END
                        If objOwrField4.Delete(CInt(dgvData.CurrentRow.Cells("objowrfield4unkid").Value)) = True Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                        " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField4").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                        " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField4").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 5
                    If CInt(dgvData.CurrentRow.Cells("objowrfield5unkid").Value) > 0 Then
                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
                        If iVoidReason.Length <= 0 Then
                            Exit Sub
                        End If
                        objOwrField5._Isvoid = True
                        objOwrField5._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objOwrField5._Voidreason = iVoidReason
                        objOwrField5._Voiduserunkid = User._Object._Userunkid
                        'S.SANDEEP [28-May-2018] -- START
                        'ISSUE/ENHANCEMENT : {Audit Trails} 
                        objOwrField5._FormName = mstrModuleName
                        objOwrField5._LoginEmployeeunkid = 0
                        objOwrField5._ClientIP = getIP()
                        objOwrField5._HostName = getHostName()
                        objOwrField5._FromWeb = False
                        objOwrField5._AuditUserId = User._Object._Userunkid
objOwrField5._CompanyUnkid = Company._Object._Companyunkid
                        objOwrField5._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        'S.SANDEEP [28-May-2018] -- END
                        If objOwrField5.Delete(CInt(dgvData.CurrentRow.Cells("objowrfield5unkid").Value)) = True Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField5").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField5").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 4, "in order to perform delete operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDel_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnEdt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Select Case CInt(sender.tag.ToString.Split("|")(1))
                Case 1
                    If CInt(dgvData.CurrentRow.Cells("objowrfield1unkid").Value) > 0 Then

                        Dim frm As New objfrmAddEditOwrField1

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objowrfield1unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboOwnerType.SelectedValue), CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField1").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField1").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 2
                    If CInt(dgvData.CurrentRow.Cells("objowrfield2unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditOwrField2

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objowrfield2unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(dgvData.CurrentRow.Cells("objownerunkid").Value), CInt(dgvData.CurrentRow.Cells("objperiodunkid").Value), CInt(cboOwnerType.SelectedValue), CInt(dgvData.CurrentRow.Cells("objowrfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField2").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField2").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 3
                    If CInt(dgvData.CurrentRow.Cells("objowrfield3unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditOwrField3

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objowrfield3unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(dgvData.CurrentRow.Cells("objownerunkid").Value), CInt(dgvData.CurrentRow.Cells("objperiodunkid").Value), CInt(cboOwnerType.SelectedValue), CInt(dgvData.CurrentRow.Cells("objowrfield2unkid").Value), CInt(dgvData.CurrentRow.Cells("objowrfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField3").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField3").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 4
                    If CInt(dgvData.CurrentRow.Cells("objowrfield4unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditOwrField4

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objowrfield4unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(dgvData.CurrentRow.Cells("objownerunkid").Value), CInt(dgvData.CurrentRow.Cells("objperiodunkid").Value), CInt(cboOwnerType.SelectedValue), CInt(dgvData.CurrentRow.Cells("objowrfield3unkid").Value), CInt(dgvData.CurrentRow.Cells("objowrfield2unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField4").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField4").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case 5
                    If CInt(dgvData.CurrentRow.Cells("objowrfield5unkid").Value) > 0 Then
                        Dim frm As New objfrmAddEditOwrField5

                        If User._Object._Isrighttoleft = True Then
                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                            frm.RightToLeftLayout = True
                            Call Language.ctlRightToLeftlayOut(frm)
                        End If

                        If frm.displayDialog(CInt(dgvData.CurrentRow.Cells("objowrfield5unkid").Value), enAction.EDIT_ONE, CInt(sender.tag.ToString.Split("|")(0)), CInt(dgvData.CurrentRow.Cells("objownerunkid").Value), CInt(dgvData.CurrentRow.Cells("objperiodunkid").Value), CInt(cboOwnerType.SelectedValue), CInt(dgvData.CurrentRow.Cells("objowrfield4unkid").Value), CInt(dgvData.CurrentRow.Cells("objowrfield3unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField5").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & dgvData.Columns(dgvData.CurrentRow.Cells("objField5").ColumnIndex).HeaderText & " " & _
                                        Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnEdt_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If CInt(cboOwnerType.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Owner Type, Owner and Period as mandatory information. Please select Owner Type, Owner and Period to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Select Case CInt(sender.tag.ToString.Split("|")(1))
                Case 1
                    Dim frm As New objfrmAddEditOwrField1

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboOwnerType.SelectedValue), CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue)) Then
                        Call Fill_Grid()
                    End If
                Case 2
                    Dim frm As New objfrmAddEditOwrField2

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Or _
                       ConfigParameter._Object._CascadingTypeId = enPACascading.LOOSE_CASCADING Then
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboOwnerType.SelectedValue), CInt(dgvData.CurrentRow.Cells("objowrfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboOwnerType.SelectedValue), CInt(dgvData.CurrentRow.Cells("objowrfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    End If
                Case 3
                    Dim frm As New objfrmAddEditOwrField3

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Or _
                       ConfigParameter._Object._CascadingTypeId = enPACascading.LOOSE_CASCADING Then
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboOwnerType.SelectedValue), CInt(dgvData.CurrentRow.Cells("objowrfield2unkid").Value), CInt(dgvData.CurrentRow.Cells("objowrfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboOwnerType.SelectedValue), CInt(dgvData.CurrentRow.Cells("objowrfield2unkid").Value), CInt(dgvData.CurrentRow.Cells("objowrfield1unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    End If

                Case 4
                    Dim frm As New objfrmAddEditOwrField4

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Or _
                       ConfigParameter._Object._CascadingTypeId = enPACascading.LOOSE_CASCADING Then
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboOwnerType.SelectedValue), CInt(dgvData.CurrentRow.Cells("objowrfield3unkid").Value), CInt(dgvData.CurrentRow.Cells("objowrfield2unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboOwnerType.SelectedValue), CInt(dgvData.CurrentRow.Cells("objowrfield3unkid").Value), CInt(dgvData.CurrentRow.Cells("objowrfield2unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    End If

                Case 5
                    Dim frm As New objfrmAddEditOwrField5

                    If User._Object._Isrighttoleft = True Then
                        frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                        frm.RightToLeftLayout = True
                        Call Language.ctlRightToLeftlayOut(frm)
                    End If

                    If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Or _
                       ConfigParameter._Object._CascadingTypeId = enPACascading.LOOSE_CASCADING Then
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboOwnerType.SelectedValue), CInt(dgvData.CurrentRow.Cells("objowrfield4unkid").Value), CInt(dgvData.CurrentRow.Cells("objowrfield3unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    Else
                        If frm.displayDialog(-1, enAction.ADD_CONTINUE, CInt(sender.tag.ToString.Split("|")(0)), CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboOwnerType.SelectedValue), CInt(dgvData.CurrentRow.Cells("objowrfield4unkid").Value), CInt(dgvData.CurrentRow.Cells("objowrfield3unkid").Value)) Then
                            Call Fill_Grid()
                        End If
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdd_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboOwnerType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOwnerType.SelectedIndexChanged
        Try
            Dim dList As New DataSet
            cboOwner.DataSource = Nothing
            Select Case CInt(cboOwnerType.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "stationunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0).Copy
                        .SelectedValue = 0
                    End With
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "deptgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "departmentunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "sectiongroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "sectionunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "unitgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "unitunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "teamunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "jobgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "jobunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "classgroupunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.getComboList("List", True)
                    With cboOwner
                        .ValueMember = "classesunkid"
                        .DisplayMember = "name"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = 0
                    End With
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOwnerType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboOwner_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOwner.SelectedIndexChanged
        Try
            Dim dsList As New DataSet

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objOwrField1.getComboList("List", True, CInt(cboPeriod.SelectedValue), , CInt(cboOwner.SelectedValue))
            Dim dtDateAsOn As DateTime
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                dtDateAsOn = objPrd._End_Date
                objPrd = Nothing
            Else
                dtDateAsOn = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            dsList = objOwrField1.getComboList(dtDateAsOn, "List", True, CInt(cboPeriod.SelectedValue), , CInt(cboOwner.SelectedValue))
            'S.SANDEEP [04 JUN 2015] -- END

            


            With cboFieldValue1
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOwner_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuCommit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCommit.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            btnOperation.Enabled = False
            btnClose.Enabled = False
            If dgvData.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, there is no goal defined in order to commit."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            If CInt(cboOwnerType.SelectedValue) <= 0 Or _
               CInt(cboOwner.SelectedValue) <= 0 Or _
               CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry in order to do commit operation please select following things [Owner Type, Owner and Period]."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim iLast_StatusId As Integer = 0 : Dim iMsgText As String = String.Empty

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objOwrField1._FormName = mstrModuleName
            objOwrField1._LoginEmployeeunkid = 0
            objOwrField1._ClientIP = getIP()
            objOwrField1._HostName = getHostName()
            objOwrField1._FromWeb = False
            objOwrField1._AuditUserId = User._Object._Userunkid
objOwrField1._CompanyUnkid = Company._Object._Companyunkid
            objOwrField1._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            iLast_StatusId = objOwrField1.GetLastStatus(CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue))

            If iLast_StatusId = enObjective_Status.FINAL_COMMITTED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot set the same operational status again. Reason : Owner goals are already committed."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objFMapping As New clsAssess_Field_Mapping
            iMsgText = objFMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue), 0)
            If iMsgText.Trim.Length > 0 Then
                eZeeMsgBox.Show(iMsgText, enMsgBoxStyle.Information)
                objFMapping = Nothing
                Exit Sub
            End If
            If objFMapping IsNot Nothing Then objFMapping = Nothing

            mintTotalCount = dgvData.RowCount

            Select Case ConfigParameter._Object._CascadingTypeId
                Case enPACascading.STRICT_CASCADING, enPACascading.LOOSE_CASCADING
                    iMsgText = Language.getMessage(mstrModuleName, 14, "You are about to commit the owner goals for the selected period. This will tranfer all goals to employee goals.") & vbCrLf & _
                               Language.getMessage(mstrModuleName, 15, "Due to this you will not be allowed to do any operation(s) on owner goals.") & vbCrLf & _
                               Language.getMessage(mstrModuleName, 16, "Do you wish to continue?")

                    If eZeeMsgBox.Show(iMsgText, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

                    Me.SuspendLayout()
                    RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

                    objbgWorker.RunWorkerAsync()

                Case Else
                    iMsgText = Language.getMessage(mstrModuleName, 24, "You are about to commit the owner goals for the selected period.") & vbCrLf & _
                               Language.getMessage(mstrModuleName, 15, "Due to this you will not be allowed to do any operation(s) on owner goals.") & vbCrLf & _
                               Language.getMessage(mstrModuleName, 16, "Do you wish to continue?")

                    If eZeeMsgBox.Show(iMsgText, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub
                    With objOwrField1
                        ._FormName = mstrModuleName
                        ._LoginEmployeeunkid = 0
                        ._ClientIP = getIP()
                        ._HostName = getHostName()
                        ._FromWeb = False
                        ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    End With
                    If objOwrField1.Update_Status(enObjective_Status.FINAL_COMMITTED, "", User._Object._Userunkid, CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue)) Then
                        Call Fill_Grid()
                    End If
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuCommit_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
            btnOperation.Enabled = True
            btnClose.Enabled = True
        End Try
    End Sub

    Private Sub bgWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgWorker.DoWork
        Try
            Me.ControlBox = False
            Me.Enabled = False

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objOwrField1._FormName = mstrModuleName
            objOwrField1._LoginEmployeeunkid = 0
            objOwrField1._ClientIP = getIP()
            objOwrField1._HostName = getHostName()
            objOwrField1._FromWeb = False
            objOwrField1._AuditUserId = User._Object._Userunkid
objOwrField1._CompanyUnkid = Company._Object._Companyunkid
            objOwrField1._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If objOwrField1.Transfer_Goals_ToEmployee(FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, User._Object._Userunkid, True, objbgWorker) Then
                If objOwrField1.Update_Status(enObjective_Status.FINAL_COMMITTED, "", User._Object._Userunkid, CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue)) Then
                    Call Fill_Grid()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgWorker_DoWork", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub bgWorker_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgWorker.ProgressChanged
        Try
            objlblProgress.Text = "[ " & e.ProgressPercentage.ToString & " / " & mintTotalCount & " ]"
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgWorker_ProgressChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub bgWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgWorker.RunWorkerCompleted
        Try
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            Else
                If objOwrField1._Message <> "" Then
                    eZeeMsgBox.Show(objOwrField1._Message, enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Commit Operation completed successfully."), enMsgBoxStyle.Information)
                End If
                objlblProgress.Text = ""
            End If
            Me.ControlBox = True
            Me.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "bgWorker_RunWorkerCompleted", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuUnlock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUnlock.Click
        Try
            If dgvData.RowCount <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, there is no goal defined in order to unlock."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If CInt(cboOwnerType.SelectedValue) <= 0 Or _
               CInt(cboOwner.SelectedValue) <= 0 Or _
               CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry in order to do unlock operation please select following things [Owner Type, Owner and Period]."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Dim iLast_StatusId As Integer = 0 : Dim iMsgText As String = String.Empty

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objOwrField1._FormName = mstrModuleName
            objOwrField1._LoginEmployeeunkid = 0
            objOwrField1._ClientIP = getIP()
            objOwrField1._HostName = getHostName()
            objOwrField1._FromWeb = False
            objOwrField1._AuditUserId = User._Object._Userunkid
objOwrField1._CompanyUnkid = Company._Object._Companyunkid
            objOwrField1._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            iLast_StatusId = objOwrField1.GetLastStatus(CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue))

            If iLast_StatusId <> enObjective_Status.FINAL_COMMITTED Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry you cannot unlock company goals. Reson : Owner goals are not committed."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If iLast_StatusId = enObjective_Status.OPEN_CHANGES Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, you cannot set the same operational status again. Reason : Owner goals are already uncommitted."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            iMsgText = Language.getMessage(mstrModuleName, 21, "You are about to unlock the owner goals. This will result in opening of all operation on owner goals.") & vbCrLf & _
                       Language.getMessage(mstrModuleName, 16, "Do you wish to continue?")

            If eZeeMsgBox.Show(iMsgText, enMsgBoxStyle.Information + enMsgBoxStyle.YesNo) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim iUnlockComments As String = ""

            iUnlockComments = InputBox(Language.getMessage(mstrModuleName, 22, "Enter reason to unlock"), Me.Text)

            If iUnlockComments.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, Unlock reason is mandaroty information."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If objOwrField1.Update_Status(enObjective_Status.OPEN_CHANGES, iUnlockComments, User._Object._Userunkid, CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue)) Then
                Call Fill_Grid()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuUnlock_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue1.DropDown, cboFieldValue2.DropDown, cboFieldValue3.DropDown, cboFieldValue4.DropDown, cboFieldValue5.DropDown
        Try
            mblnDropDownClosed = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DropDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DropDownClosed(ByVal sender As Object, ByVal e As EventArgs) Handles cboFieldValue1.DropDownClosed, cboFieldValue2.DropDownClosed, cboFieldValue3.DropDownClosed, cboFieldValue4.DropDownClosed, cboFieldValue5.DropDownClosed
        Try
            Dim iCbo As ComboBox = Nothing
            Select Case CType(sender, ComboBox).Name.ToUpper
                Case cboFieldValue1.Name.ToUpper
                    iCbo = cboFieldValue1
                Case cboFieldValue2.Name.ToUpper
                    iCbo = cboFieldValue2
                Case cboFieldValue3.Name.ToUpper
                    iCbo = cboFieldValue3
                Case cboFieldValue4.Name.ToUpper
                    iCbo = cboFieldValue4
                Case cboFieldValue5.Name.ToUpper
                    iCbo = cboFieldValue5
            End Select
            ToolTip1.Hide(iCbo) : mblnDropDownClosed = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DropDownClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_DrawItem(ByVal sender As Object, ByVal e As DrawItemEventArgs) Handles cboFieldValue1.DrawItem, cboFieldValue2.DrawItem, cboFieldValue3.DrawItem, cboFieldValue4.DrawItem, cboFieldValue5.DrawItem
        Try
            Dim iCbo As ComboBox = Nothing
            Select Case CType(sender, ComboBox).Name.ToUpper
                Case cboFieldValue1.Name.ToUpper
                    iCbo = cboFieldValue1
                Case cboFieldValue2.Name.ToUpper
                    iCbo = cboFieldValue2
                Case cboFieldValue3.Name.ToUpper
                    iCbo = cboFieldValue3
                Case cboFieldValue4.Name.ToUpper
                    iCbo = cboFieldValue4
                Case cboFieldValue5.Name.ToUpper
                    iCbo = cboFieldValue5
            End Select
            If e.Index < 0 Then
                Return
            End If
            Dim text As String = iCbo.GetItemText(iCbo.Items(e.Index))
            e.DrawBackground()
            Using br As New SolidBrush(e.ForeColor)
                e.Graphics.DrawString(text, e.Font, br, e.Bounds)
            End Using
            If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                If mblnDropDownClosed = False Then
                    ToolTip1.Show(text, iCbo, e.Bounds.Right, e.Bounds.Bottom)
                End If
            End If
            e.DrawFocusRectangle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_DrawItem", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim iExecOrder As Integer = 0
                Dim objMapping As New clsAssess_Field_Mapping
                mintLinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
                objMapping = Nothing
                Dim dsFields As New DataSet
                dsFields = objFMaster.Get_Field_Mapping("List", , False)
                If dsFields.Tables(0).Rows.Count > 0 Then
                    Dim dtmp() As DataRow = dsFields.Tables(0).Select("fieldunkid = '" & mintLinkedFieldId & "'")
                    If dtmp.Length > 0 Then
                        Select Case dtmp(0).Item("ExOrder")
                            Case 1
                                Dim objCoyField1 As New clsassess_coyfield1_master
                                iOwnerRefId = objCoyField1.GetOwnerRefId
                                objCoyField1 = Nothing
                            Case 2
                                Dim objCoyField2 As New clsassess_coyfield2_master
                                iOwnerRefId = objCoyField2.GetOwnerRefId
                                objCoyField2 = Nothing
                            Case 3
                                Dim objCoyField3 As New clsassess_coyfield3_master
                                iOwnerRefId = objCoyField3.GetOwnerRefId
                                objCoyField3 = Nothing
                            Case 4
                                Dim objCoyField4 As New clsassess_coyfield4_master
                                iOwnerRefId = objCoyField4.GetOwnerRefId
                                objCoyField4 = Nothing
                            Case 5
                                Dim objCoyField5 As New clsassess_coyfield5_master
                                iOwnerRefId = objCoyField5.GetOwnerRefId
                                objCoyField5 = Nothing
                        End Select
                    End If
                    Dim dsList As New DataSet : Dim objMData As New clsMasterData
                    dsList = objMData.GetEAllocation_Notification("List")
                    Dim dtTable As DataTable = Nothing
                    If iOwnerRefId > 0 Then
                        dtTable = New DataView(dsList.Tables(0), "Id IN(0," & iOwnerRefId & ")", "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
                    End If
                    With cboOwnerType
                        .ValueMember = "Id"
                        .DisplayMember = "Name"
                        .DataSource = dtTable
                        .SelectedValue = IIf(iOwnerRefId <= 0, enAllocation.BRANCH, iOwnerRefId)
                    End With
                    objMData = Nothing
                End If
            Else
                cboOwnerType.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue1.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objOwrField2.getComboList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue1.SelectedValue), "List", True)
            With cboFieldValue2
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue1_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue2.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objOwrField3.getComboList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue2.SelectedValue), "List", True)
            With cboFieldValue3
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue2_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue3.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objOwrField4.getComboList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue3.SelectedValue), "List", True)
            With cboFieldValue4
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue3_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue4.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objOwrField5.getComboList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue4.SelectedValue), "List", True)
            With cboFieldValue5
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFieldValue4_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub mnuGlobalAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalAssign.Click
        Dim frm As New frmGlobalAssignGoals
        Try
            If CInt(cboOwnerType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Sorry, Owner Type is mandatory information. Please select Owner Type to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(CInt(cboOwnerType.SelectedValue), cboOwner, cboPeriod) Then
                If CInt(cboOwner.SelectedValue) > 0 Then
                    Call Fill_Grid()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalAssign_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuUpdatePercentage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUpdatePercentage.Click
        Dim frm As New frmOwrUpdatePercentage
        Try
            If CInt(cboOwnerType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Sorry, Owner Type is mandatory information. Please select Owner Type to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(CInt(cboOwnerType.SelectedValue), cboOwner, cboPeriod) Then
                If CInt(cboOwner.SelectedValue) > 0 Then
                    Call Fill_Grid()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalAssign_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'S.SANDEEP [03 JUN 2015] -- START
    Private Sub mnuVoidOwnerGoals_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuVoidOwnerGoals.Click
        Dim frm As New frmGlobalVoidOwnerGoals
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuVoidOwnerGoals_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'S.SANDEEP [03 JUN 2015] -- END

    'Shani (05-Sep-2016) -- Start
    'Enhancement - Assessment Close Period to Save Last status data given by sandra (FZ)
    Private Sub mnuGlobalOperation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGlobalOperation.Click
        Dim frm As New frmOwnerGlobalOperation
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuGlobalOperation_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Shani (05-Sep-2016) -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnOperation.GradientBackColor = GUI._ButttonBackColor 
			Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblOwnerType.Text = Language._Object.getCaption(Me.lblOwnerType.Name, Me.lblOwnerType.Text)
			Me.lblOwner.Text = Language._Object.getCaption(Me.lblOwner.Name, Me.lblOwner.Text)
			Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
			Me.mnuCommit.Text = Language._Object.getCaption(Me.mnuCommit.Name, Me.mnuCommit.Text)
			Me.mnuUnlock.Text = Language._Object.getCaption(Me.mnuUnlock.Name, Me.mnuUnlock.Text)
			Me.mnuGlobalAssign.Text = Language._Object.getCaption(Me.mnuGlobalAssign.Name, Me.mnuGlobalAssign.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, there is no")
			Language.setMessage(mstrModuleName, 2, "defined. Please define")
			Language.setMessage(mstrModuleName, 3, "in order to perform edit operation.")
			Language.setMessage(mstrModuleName, 4, "in order to perform delete operation.")
			Language.setMessage(mstrModuleName, 5, "Add")
			Language.setMessage(mstrModuleName, 6, "Edit")
			Language.setMessage(mstrModuleName, 7, "Delete")
			Language.setMessage(mstrModuleName, 8, "Sorry, No data defined for asseessment caption settings screen.")
			Language.setMessage(mstrModuleName, 9, "Sorry no field is mapped with the selected period.")
			Language.setMessage(mstrModuleName, 11, "Sorry, there is no goal defined in order to commit.")
			Language.setMessage(mstrModuleName, 12, "Sorry in order to do commit operation please select following things [Owner Type, Owner and Period].")
			Language.setMessage(mstrModuleName, 13, "Sorry, you cannot set the same operational status again. Reason : Owner goals are already committed.")
			Language.setMessage(mstrModuleName, 14, "You are about to commit the owner goals for the selected period. This will tranfer all goals to employee goals.")
			Language.setMessage(mstrModuleName, 15, "Due to this you will not be allowed to do any operation(s) on owner goals.")
			Language.setMessage(mstrModuleName, 16, "Do you wish to continue?")
			Language.setMessage(mstrModuleName, 17, "Sorry, there is no goal defined in order to unlock.")
			Language.setMessage(mstrModuleName, 18, "Sorry in order to do unlock operation please select following things [Owner Type, Owner and Period].")
			Language.setMessage(mstrModuleName, 19, "Sorry you cannot unlock company goals. Reson : Owner goals are not committed.")
			Language.setMessage(mstrModuleName, 20, "Sorry, you cannot set the same operational status again. Reason : Owner goals are already uncommitted.")
			Language.setMessage(mstrModuleName, 21, "You are about to unlock the owner goals. This will result in opening of all operation on owner goals.")
			Language.setMessage(mstrModuleName, 22, "Enter reason to unlock")
			Language.setMessage(mstrModuleName, 23, "Sorry, Unlock reason is mandaroty information.")
			Language.setMessage(mstrModuleName, 24, "You are about to commit the owner goals for the selected period.")
			Language.setMessage(mstrModuleName, 25, "Commit Operation completed successfully.")
			Language.setMessage(mstrModuleName, 26, "You are about to remove the information from owner level, this will delete all child linked to this parent")
			Language.setMessage(mstrModuleName, 27, "Are you sure you want to delete?")
			Language.setMessage(mstrModuleName, 28, "Owner Type, Owner and Period as mandatory information. Please select Owner Type, Owner and Period to continue.")
			Language.setMessage(mstrModuleName, 29, "Add")
			Language.setMessage(mstrModuleName, 30, "Edit")
			Language.setMessage(mstrModuleName, 31, "Delete")
			Language.setMessage(mstrModuleName, 32, "Total Weight Assigned :")
			Language.setMessage(mstrModuleName, 33, "Sorry, Owner Type is mandatory information. Please select Owner Type to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class