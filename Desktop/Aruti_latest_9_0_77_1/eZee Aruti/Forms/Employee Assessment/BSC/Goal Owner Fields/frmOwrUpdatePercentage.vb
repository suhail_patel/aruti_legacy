﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmOwrUpdatePercentage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmOwrUpdatePercentage"
    Private mblnCancel As Boolean = True
    Private mintExOrder As Integer = 0
    Private mintLinkedFld As Integer = 0
    Private dtFinalTab As DataTable = Nothing
    Private mintFldTypeId As Integer = 0
    Private mintOwnerTypeId As Integer = 0
    'S.SANDEEP [04 JUN 2015] -- START
    Private blnIspct_completeDisplayed As Boolean = True
    'S.SANDEEP [04 JUN 2015] -- END

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal iOwnerTypeId As Integer, _
                                  ByVal cboOwr As ComboBox, _
                                  ByVal cboPrd As ComboBox) As Boolean
        Try
            mintOwnerTypeId = iOwnerTypeId
            With cboOwr
                cboOwner.ValueMember = .ValueMember
                cboOwner.DisplayMember = .DisplayMember
                cboOwner.DataSource = .DataSource
            End With
            With cboPrd
                cboPeriod.ValueMember = .ValueMember
                cboPeriod.DisplayMember = .DisplayMember
                cboPeriod.DataSource = .DataSource
            End With
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Set_Caption()
        Try
            Dim objFMapping As New clsAssess_Field_Mapping
            Dim strLinkedFld As String = objFMapping.Get_Map_FieldName(CInt(cboPeriod.SelectedValue))
            mintLinkedFld = objFMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            Dim objFMaster As New clsAssess_Field_Master
            mintExOrder = objFMaster.Get_Field_ExOrder(mintLinkedFld)
            Select Case mintExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    mintFldTypeId = enWeight_Types.WEIGHT_FIELD1
                Case enWeight_Types.WEIGHT_FIELD2
                    mintFldTypeId = enWeight_Types.WEIGHT_FIELD2
                Case enWeight_Types.WEIGHT_FIELD3
                    mintFldTypeId = enWeight_Types.WEIGHT_FIELD3
                Case enWeight_Types.WEIGHT_FIELD4
                    mintFldTypeId = enWeight_Types.WEIGHT_FIELD4
                Case enWeight_Types.WEIGHT_FIELD5
                    mintFldTypeId = enWeight_Types.WEIGHT_FIELD5
            End Select
            objFMaster = Nothing : objFMapping = Nothing
            objgbOwnerItems.Text = strLinkedFld & " " & Language.getMessage(mstrModuleName, 1, "Information")
            'S.SANDEEP [04 JUN 2015] -- START
            'objdgcolhfname.HeaderText = strLinkedFld
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Caption", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Items_Grid()
        Try
            RemoveHandler dgvItems.CellContentClick, AddressOf dgvItems_CellContentClick
            RemoveHandler objchkItems.CheckedChanged, AddressOf objchkItems_CheckedChanged

            Dim objOwner As New clsassess_owrfield1_master
            dtFinalTab = objOwner.GetDisplayList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), "List")
            dtFinalTab.Columns.Add("fcheck", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtFinalTab.Columns.Add("fchange", System.Type.GetType("System.Boolean")).DefaultValue = False

            dgvItems.AutoGenerateColumns = False

            'S.SANDEEP [04 JUN 2015] -- START
            'Select Case mintExOrder
            '    Case enWeight_Types.WEIGHT_FIELD1
            '        objdgcolhfname.DataPropertyName = "Field1"
            '    Case enWeight_Types.WEIGHT_FIELD2
            '        objdgcolhfname.DataPropertyName = "Field2"
            '    Case enWeight_Types.WEIGHT_FIELD3
            '        objdgcolhfname.DataPropertyName = "Field3"
            '    Case enWeight_Types.WEIGHT_FIELD4
            '        objdgcolhfname.DataPropertyName = "Field4"
            '    Case enWeight_Types.WEIGHT_FIELD5
            '        objdgcolhfname.DataPropertyName = "Field5"
            'End Select
            'dgcolhfeddate.DataPropertyName = "Ed_Date"
            'dgcolhfpct_complete.DataPropertyName = "pct_complete"
            'dgcolhfstatus.DataPropertyName = "CStatus"
            'dgcolhfstdate.DataPropertyName = "St_Date"
            'dgcolhfweight.DataPropertyName = "Weight"
            objdgcolhfcheck.DataPropertyName = "fcheck"
            Dim iColName As String = String.Empty

            Dim iPlan() As String = Nothing
            If ConfigParameter._Object._ViewTitles_InPlanning.Trim.Length > 0 Then
                iPlan = ConfigParameter._Object._ViewTitles_InPlanning.Split(CChar("|"))
            End If
            Dim objFMaster As New clsAssess_Field_Master
            For Each dCol As DataColumn In dtFinalTab.Columns
                If dCol.ColumnName = "fcheck" Then Continue For
                If dCol.ColumnName = "fchange" Then Continue For
                iColName = "" : iColName = "obj" & dCol.ColumnName
                If dgvItems.Columns.Contains(iColName) = True Then Continue For
                Dim dgvCol As New DataGridViewTextBoxColumn()
                dgvCol.Name = iColName
                dgvCol.Width = 120
                dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dgvCol.ReadOnly = True
                dgvCol.DataPropertyName = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                If dCol.Caption.Length <= 0 Then
                    dgvCol.Visible = False
                Else
                    If dtFinalTab.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                        dgvCol.Width = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, CInt(dtFinalTab.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)))
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, dtFinalTab.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                                If dCol.ColumnName = "pct_complete" Then
                                    blnIspct_completeDisplayed = False
                                End If
                            End If
                        End If
                    End If
                End If
                If dCol.ColumnName = "Emp" Then
                    dgvCol.Visible = False
                End If
                If ConfigParameter._Object._CascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                    If dCol.ColumnName.StartsWith("Owr") Then
                        dgvCol.Visible = False
                    End If
                End If
                dgvItems.Columns.Add(dgvCol)
            Next
            objFMaster = Nothing
            If blnIspct_completeDisplayed = False Then
                gbOperation.Enabled = False
            Else
                gbOperation.Enabled = True
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            dgvItems.DataSource = dtFinalTab

            AddHandler dgvItems.CellContentClick, AddressOf dgvItems_CellContentClick
            AddHandler objchkItems.CheckedChanged, AddressOf objchkItems_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Items_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            'S.SANDEEP [05 SEP 2016] -- START
            'ENHANCEMENT : NEW STATUS {BY ANDREW}
            'If txtPercent.Decimal > 100 Or txtPercent.Decimal <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Percentage Completed should be between 1 and 100."), enMsgBoxStyle.Information)
            '    txtPercent.Focus()
            '    Return False
            'End If

            If txtPercent.Decimal > 100 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Percentage Completed cannot be greater than 100."), enMsgBoxStyle.Information)
                txtPercent.Focus()
                Return False
            End If
            'S.SANDEEP [05 SEP 2016] -- END


            If radApplyTochecked.Checked = False AndAlso radApplyToAll.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Please select atleast one operation type to set percentage."), enMsgBoxStyle.Information)
                Return False
            End If

            If radApplyTochecked.Checked = True Then
                dtFinalTab.AcceptChanges()
                Dim xRow() As DataRow = Nothing
                xRow = dtFinalTab.Select("fcheck = true")
                If xRow.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, please check atleast one of the") & " " & objgbOwnerItems.Text & " " & _
                                    Language.getMessage(mstrModuleName, 4, "goal(s) to assign Percentage."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmOwrUpdatePercentage_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOwrUpdatePercentage_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Me.Enabled = False
        Try
            If dtFinalTab IsNot Nothing Then
                Dim blnflag As Boolean = False
                dtFinalTab.AcceptChanges()
                Dim rows As IEnumerable(Of DataRow) = dtFinalTab.Rows.Cast(Of DataRow)().Where(Function(r) r("fchange").ToString() = "True")
                Dim mGoalIdx As Integer = 1
                For Each row As DataRow In rows
                    objlnkCaption.Text = Language.getMessage(mstrModuleName, 7, "Processing") & " " & (mGoalIdx).ToString & "/" & rows.Count.ToString
                    Application.DoEvents()
                    Select Case mintExOrder
                        Case enWeight_Types.WEIGHT_FIELD1
                            Dim objOField1 As New clsassess_owrfield1_master
                            objOField1._Owrfield1unkid = CInt(row("owrfield1unkid"))
                            objOField1._Pct_Completed = CDec(row("pct_complete"))
                            With objOField1
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            blnflag = objOField1.Update()
                            If blnflag = False AndAlso objOField1._Message <> "" Then
                                eZeeMsgBox.Show(objOField1._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                            objOField1 = Nothing

                        Case enWeight_Types.WEIGHT_FIELD2
                            Dim objOField2 As New clsassess_owrfield2_master
                            objOField2._Owrfield2unkid = CInt(row("owrfield2unkid"))
                            objOField2._Pct_Completed = CDec(row("pct_complete"))
                            With objOField2
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            blnflag = objOField2.Update()
                            If blnflag = False AndAlso objOField2._Message <> "" Then
                                eZeeMsgBox.Show(objOField2._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                            objOField2 = Nothing

                        Case enWeight_Types.WEIGHT_FIELD3
                            Dim objOField3 As New clsassess_owrfield3_master
                            objOField3._Owrfield3unkid = CInt(row("owrfield3unkid"))
                            objOField3._Pct_Completed = CDec(row("pct_complete"))
                            With objOField3
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            blnflag = objOField3.Update()
                            If blnflag = False AndAlso objOField3._Message <> "" Then
                                eZeeMsgBox.Show(objOField3._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                            objOField3 = Nothing

                        Case enWeight_Types.WEIGHT_FIELD4
                            Dim objOField4 As New clsassess_owrfield4_master
                            objOField4._Owrfield4unkid = CInt(row("owrfield4unkid"))
                            objOField4._Pct_Completed = CDec(row("pct_complete"))
                            With objOField4
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            blnflag = objOField4.Update()
                            If blnflag = False AndAlso objOField4._Message <> "" Then
                                eZeeMsgBox.Show(objOField4._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                            objOField4 = Nothing

                        Case enWeight_Types.WEIGHT_FIELD5
                            Dim objOField5 As New clsassess_owrfield5_master
                            objOField5._Owrfield5unkid = CInt(row("owrfield5unkid"))
                            objOField5._Pct_Completed = CDec(row("pct_complete"))
                            With objOField5
                                ._FormName = mstrModuleName
                                ._LoginEmployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            blnflag = objOField5.Update()
                            If blnflag = False AndAlso objOField5._Message <> "" Then
                                eZeeMsgBox.Show(objOField5._Message, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                            objOField5 = Nothing

                    End Select
                    mGoalIdx += 1
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            objlnkCaption.Text = "" : Me.Enabled = True
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        mblnCancel = False
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Please select Period and Owner to do global assign operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            cboOwner.Focus()
            Call Fill_Items_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchOwner_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOwner.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If cboOwner.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = cboOwner.ValueMember
                .DisplayMember = cboOwner.DisplayMember
                .DataSource = CType(cboOwner.DataSource, DataTable)
                If .DisplayDialog = True Then
                    cboOwner.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOwner_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " DataGrid Events "

    Private Sub dgvItems_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvItems.CellContentClick, dgvItems.CellContentDoubleClick
        Try
            RemoveHandler objchkItems.CheckedChanged, AddressOf objchkItems_CheckedChanged
            If e.ColumnIndex = objdgcolhfcheck.Index Then
                If Me.dgvItems.IsCurrentCellDirty Then
                    Me.dgvItems.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = dtFinalTab.Select("fcheck = true", "")
                If drRow.Length > 0 Then
                    If drRow.Length > 0 Then
                        If dtFinalTab.Rows.Count = drRow.Length Then
                            objchkItems.CheckState = CheckState.Checked
                        Else
                            objchkItems.CheckState = CheckState.Indeterminate
                        End If
                    End If
                Else
                    objchkItems.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objchkItems.CheckedChanged, AddressOf objchkItems_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvItems_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event "

    Private Sub objchkItems_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkItems.CheckedChanged
        Try
            If dtFinalTab IsNot Nothing Then
                RemoveHandler dgvItems.CellContentClick, AddressOf dgvItems_CellContentClick
                For Each dr As DataRow In dtFinalTab.Rows
                    dr.Item("fcheck") = CBool(objchkItems.CheckState)
                Next
                dgvItems.Refresh()
                AddHandler dgvItems.CellContentClick, AddressOf dgvItems_CellContentClick
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkItems_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboOwner.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                dgvItems.DataSource = Nothing
                If dtFinalTab IsNot Nothing Then dtFinalTab.Rows.Clear()
            ElseIf CInt(cboPeriod.SelectedValue) > 0 Then
                Call Set_Caption()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub lnkUpdate_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkUpdate.LinkClicked
        Try
            If IsValidData() = False Then Exit Sub
            Dim rows As IEnumerable(Of DataRow) = Nothing
            If radApplyToAll.Checked = True Then
                rows = dtFinalTab.Rows.Cast(Of DataRow)()
            ElseIf radApplyTochecked.Checked = True Then
                rows = dtFinalTab.Rows.Cast(Of DataRow)().Where(Function(r) r("fcheck").ToString() = "True")
            End If
            For Each row As DataRow In rows
                row("pct_complete") = txtPercent.Decimal
                row("fchange") = True
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkUpdate_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbDetails.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDetails.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbOperation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbOperation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.objgbOwnerItems.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.objgbOwnerItems.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbDetails.Text = Language._Object.getCaption(Me.gbDetails.Name, Me.gbDetails.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblOwner.Text = Language._Object.getCaption(Me.lblOwner.Name, Me.lblOwner.Text)
            Me.gbOperation.Text = Language._Object.getCaption(Me.gbOperation.Name, Me.gbOperation.Text)
            Me.lblPercentage.Text = Language._Object.getCaption(Me.lblPercentage.Name, Me.lblPercentage.Text)
            Me.radApplyToAll.Text = Language._Object.getCaption(Me.radApplyToAll.Name, Me.radApplyToAll.Text)
            Me.radApplyTochecked.Text = Language._Object.getCaption(Me.radApplyTochecked.Name, Me.radApplyTochecked.Text)
            Me.lnkUpdate.Text = Language._Object.getCaption(Me.lnkUpdate.Name, Me.lnkUpdate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Information")
            Language.setMessage(mstrModuleName, 3, "Sorry, please check atleast one of the")
            Language.setMessage(mstrModuleName, 4, "goal(s) to assign Percentage.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Please select Period and Owner to do global assign operation.")
            Language.setMessage(mstrModuleName, 6, "Sorry, Please select atleast one operation type to set percentage.")
            Language.setMessage(mstrModuleName, 7, "Processing")
            Language.setMessage(mstrModuleName, 8, "Sorry, Percentage Completed cannot be greater than 100.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class