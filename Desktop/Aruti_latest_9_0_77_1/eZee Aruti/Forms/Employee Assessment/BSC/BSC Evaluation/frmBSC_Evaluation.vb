﻿'ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

'S.SANDEEP [ 28 DEC 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Public Class frmBSC_Evaluation

#Region " Private Variables "

    Private mstrModuleName As String = "frmBSC_Evaluation"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objBSCEvaluation As clsBSC_Analysis_Master
    Private objBSCTran As clsBSC_analysis_tran
    Private mintAssessAnalysisUnkid As Integer = -1
    Private mintEmplId As Integer = 0
    Private mintYearId As Integer = 0
    Private mintPeriodId As Integer = 0
    Private mintAssessorId As Integer = 0
    Private menAssess As enAssessmentMode

    Private mdicGroupWiseTotal As New Dictionary(Of Integer, Decimal)
    Private mdicItemWeight As New Dictionary(Of Integer, Decimal)
    Dim iCnt As Integer = 0
    Dim mdecRemaininResult As Decimal = 0
    Private mdtEvaluation As DataTable
    Dim objWSetting As New clsWeight_Setting(True)
    Dim mdtFullBSC As DataTable

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkid As Integer, ByVal eAction As enAction, ByVal eAssess As enAssessmentMode) As Boolean
        Try
            mintAssessAnalysisUnkid = intUnkid
            menAction = eAction
            menAssess = eAssess
            Me.ShowDialog()
            intUnkid = mintAssessAnalysisUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetVisibility()
        Try

            If objWSetting._Weight_Optionid <> enWeight_Options.WEIGHT_EACH_ITEM Then
                Select Case objWSetting._Weight_Typeid
                    Case enWeight_Types.WEIGHT_FIELD1
                        cboKPI.Enabled = False : objbtnSearchKPI.Enabled = False
                        cboTraget.Enabled = False : objbtnSearchTraget.Enabled = False
                        cboInitiative.Enabled = False : objbtnSearchInitiative.Enabled = False
                    Case enWeight_Types.WEIGHT_FIELD2
                        cboTraget.Enabled = False : objbtnSearchTraget.Enabled = False
                        cboInitiative.Enabled = False : objbtnSearchInitiative.Enabled = False
                    Case enWeight_Types.WEIGHT_FIELD3
                        cboInitiative.Enabled = False : objbtnSearchInitiative.Enabled = False
                End Select
            End If

            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                cboAssessor.Visible = False
                lblAssessor.Visible = False
                objbtnSearchAssessor.Enabled = False
                pnlContainer.Location = New Point(11, 11)
                colhResult.Text = Language.getMessage(mstrModuleName, 10, "Self")
                lnkViewBSC.Visible = False
                radInternalAssessor.Checked = False : radExternalAssessor.Checked = False
                radInternalAssessor.Visible = False : radExternalAssessor.Visible = False
                lblReviewer.Visible = False
                cboReviewer.Visible = False
                objbtnSearchReviewer.Visible = False
            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                cboAssessor.Visible = True
                lblAssessor.Visible = True
                objbtnSearchAssessor.Enabled = True
                pnlContainer.Location = New Point(11, 35)
                colhResult.Text = Language.getMessage(mstrModuleName, 11, "Assessor's")
                lblReviewer.Visible = False
                cboReviewer.Visible = False
                objbtnSearchReviewer.Visible = False
                lnkViewBSC.Visible = False
                radInternalAssessor.Checked = True
                radInternalAssessor.Visible = True : radExternalAssessor.Visible = True
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                colhResult.Text = Language.getMessage(mstrModuleName, 12, "Reviewer's")
                cboAssessor.Visible = False
                lblAssessor.Visible = False
                objbtnSearchAssessor.Visible = False
                lblReviewer.Visible = True
                cboReviewer.Visible = True
                objbtnSearchReviewer.Visible = True
                lnkViewBSC.Visible = True
                radInternalAssessor.Checked = False : radExternalAssessor.Checked = False
                radInternalAssessor.Visible = False : radExternalAssessor.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            cboEmployee.BackColor = GUI.ColorComp
            cboYear.BackColor = GUI.ColorComp
            cboPeriod.BackColor = GUI.ColorComp
            cboObjective.BackColor = GUI.ColorComp
            txtRemark.BackColor = GUI.ColorOptional
            cboPerspective.BackColor = GUI.ColorComp
            cboAssessor.BackColor = GUI.ColorComp
            cboResult.BackColor = GUI.ColorComp
            cboReviewer.BackColor = GUI.ColorComp
            txtRemark.BackColor = GUI.ColorOptional
            cboTraget.BackColor = GUI.ColorComp
            cboKPI.BackColor = GUI.ColorComp
            cboInitiative.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
                'End If
                'With cboEmployee
                '    .ValueMember = "employeeunkid"
                '    .DisplayMember = "employeename"
                '    .DataSource = dsCombos.Tables(0)
                '    .SelectedValue = 0
                'End With
                'S.SANDEEP [04 JUN 2015] -- END
            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                dsCombos = objBSCEvaluation.getAssessorComboList("Assessor", True, True)
                With cboReviewer
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objMaster.getComboListPAYYEAR("Year", True, , , , True)
            dsCombos = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True, True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboYear
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Year")
            End With

            dsCombos = objMaster.Get_BSC_Perspective("List", True)
            With cboPerspective
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            Select Case menAssess
                Case enAssessmentMode.SELF_ASSESSMENT
                    cboEmployee.SelectedValue = objBSCEvaluation._Selfemployeeunkid
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    If objBSCEvaluation._Ext_Assessorunkid > 0 Then
                        radExternalAssessor.Checked = True
                        cboAssessor.SelectedValue = objBSCEvaluation._Ext_Assessorunkid
                    Else
                        radInternalAssessor.Checked = True
                        cboAssessor.SelectedValue = objBSCEvaluation._Assessormasterunkid
                    End If
                    cboEmployee.SelectedValue = objBSCEvaluation._Assessedemployeeunkid
                Case enAssessmentMode.REVIEWER_ASSESSMENT

                    cboReviewer.SelectedValue = objBSCEvaluation._Assessormasterunkid
                    cboEmployee.SelectedValue = objBSCEvaluation._Assessedemployeeunkid
            End Select
            cboYear.SelectedValue = objBSCEvaluation._Yearunkid
            cboPeriod.SelectedValue = objBSCEvaluation._Periodunkid
            If objBSCEvaluation._Assessmentdate <> Nothing Then
                dtpAssessdate.Value = objBSCEvaluation._Assessmentdate
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objBSCEvaluation._Yearunkid = CInt(cboYear.SelectedValue)
            objBSCEvaluation._Periodunkid = CInt(cboPeriod.SelectedValue)
            Select Case menAssess
                Case enAssessmentMode.SELF_ASSESSMENT
                    objBSCEvaluation._Selfemployeeunkid = CInt(cboEmployee.SelectedValue)
                    objBSCEvaluation._Assessedemployeeunkid = -1
                    objBSCEvaluation._Assessormasterunkid = -1
                    objBSCEvaluation._Assessoremployeeunkid = -1
                    objBSCEvaluation._Reviewerunkid = -1
                    objBSCEvaluation._Assessmodeid = enAssessmentMode.SELF_ASSESSMENT
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    objBSCEvaluation._Selfemployeeunkid = -1
                    objBSCEvaluation._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
                    If radInternalAssessor.Checked = True Then
                        objBSCEvaluation._Assessormasterunkid = CInt(cboAssessor.SelectedValue)
                        Dim intEmployeeId As Integer = -1
                        intEmployeeId = objBSCEvaluation.GetAssessorEmpId(CInt(cboAssessor.SelectedValue))
                        objBSCEvaluation._Assessoremployeeunkid = intEmployeeId
                    ElseIf radExternalAssessor.Checked = True Then
                        objBSCEvaluation._Ext_Assessorunkid = CInt(cboAssessor.SelectedValue)
                        objBSCEvaluation._Assessormasterunkid = -1
                        objBSCEvaluation._Assessoremployeeunkid = -1
                    End If
                    objBSCEvaluation._Reviewerunkid = -1
                    objBSCEvaluation._Assessmodeid = enAssessmentMode.APPRAISER_ASSESSMENT
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    objBSCEvaluation._Selfemployeeunkid = -1
                    objBSCEvaluation._Assessormasterunkid = -1
                    objBSCEvaluation._Assessoremployeeunkid = -1
                    objBSCEvaluation._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
                    objBSCEvaluation._Reviewerunkid = User._Object._Userunkid
                    objBSCEvaluation._Assessmodeid = enAssessmentMode.REVIEWER_ASSESSMENT
                    objBSCEvaluation._Assessormasterunkid = CInt(cboReviewer.SelectedValue)
                    Dim intEmployeeId As Integer = -1
                    intEmployeeId = objBSCEvaluation.GetAssessorEmpId(CInt(cboReviewer.SelectedValue))
                    objBSCEvaluation._Reviewerunkid = intEmployeeId
            End Select
            objBSCEvaluation._Assessmentdate = dtpAssessdate.Value
            objBSCEvaluation._Userunkid = User._Object._Userunkid
            'S.SANDEEP [ 14 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objBSCEvaluation._Committeddatetime <> Nothing Then
                objBSCEvaluation._Committeddatetime = objBSCEvaluation._Committeddatetime
            Else
                objBSCEvaluation._Committeddatetime = Nothing
            End If
            'S.SANDEEP [ 14 JUNE 2012 ] -- END
            Select Case menAction
                Case enAction.ADD_ONE, enAction.ADD_CONTINUE
                    objBSCEvaluation._Isvoid = False
                    objBSCEvaluation._Voiduserunkid = -1
                    objBSCEvaluation._Voiddatetime = Nothing
                    objBSCEvaluation._Voidreason = ""
                Case enAction.EDIT_ONE
                    objBSCEvaluation._Isvoid = objBSCEvaluation._Isvoid
                    objBSCEvaluation._Voiduserunkid = objBSCEvaluation._Voiduserunkid
                    objBSCEvaluation._Voiddatetime = objBSCEvaluation._Voiddatetime
                    objBSCEvaluation._Voidreason = objBSCEvaluation._Voidreason
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function isAll_Assessed(Optional ByVal blnFlag As Boolean = True) As Boolean
        Try
            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim dtView As DataView = mdtEvaluation.DefaultView

            'Dim mdic_Items As New Dictionary(Of Integer, Integer)
            'objBSCEvaluation.Get_Items_Count(CInt(cboPerspective.SelectedValue), CInt(cboEmployee.SelectedValue))
            'mdic_Items = objBSCEvaluation._AllItemCount

            'If mdic_Items.Keys.Count > 0 Then
            '    '/******************* OBJECTIVE *********************/
            '    If mdic_Items.ContainsKey(0) Then
            '        If mdic_Items(0) <> dtView.ToTable(True, "objectiveunkid").Rows.Count Then
            '            If blnFlag = True Then
            '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot do Save Commit Operation as you have not assessed all objective(s) in the selected Perspective."), enMsgBoxStyle.Information)
            '            End If
            '            cboObjective.Focus()
            '            Return False
            '        End If
            '    End If
            '    '/******************* OBJECTIVE *********************/
            'Else
            '    Return False
            'End If

            'Return True

            'S.SANDEEP [ 25 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mdtFullBSC = objBSCEvaluation.GetFullBSC(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
            'S.SANDEEP [ 25 JULY 2013 ] -- END

            Dim dView1 As DataView : Dim dView2 As DataView
            dView1 = mdtFullBSC.DefaultView : dView2 = mdtEvaluation.DefaultView

            If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
                If dView1.ToTable(True, "objectiveunkid").Rows.Count <> dView2.ToTable(True, "objectiveunkid").Rows.Count Then
                    If blnFlag = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot do Save Commit Operation as you have not assessed all objective(s) in the selected Perspective."), enMsgBoxStyle.Information)
                    End If
                    cboObjective.Focus()
                    Return False
                End If
            Else
                If dView1.ToTable(True, "objectiveunkid").Rows.Count <> dView2.ToTable(True, "objectiveunkid").Rows.Count Then
                    If blnFlag = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot do Save Commit Operation as you have not assessed all objective(s) in the selected Perspective."), enMsgBoxStyle.Information)
                    End If
                    cboObjective.Focus()
                    Return False
                End If
                Dim dItems1() As DataRow : Dim dItmes2() As DataRow
                dItems1 = Nothing : dItmes2 = Nothing
                For Each dRow As DataRow In dView2.ToTable(True, "objectiveunkid").Rows
                    Select Case objWSetting._Weight_Typeid
                        Case enWeight_Types.WEIGHT_FIELD2
                            dItems1 = mdtFullBSC.Select("objectiveunkid = '" & CInt(dRow.Item("objectiveunkid")) & "' AND kpiunkid > 0 ")
                        Case enWeight_Types.WEIGHT_FIELD3
                            dItems1 = mdtFullBSC.Select("objectiveunkid = '" & CInt(dRow.Item("objectiveunkid")) & "' AND targetunkid > 0  ")
                        Case enWeight_Types.WEIGHT_FIELD4
                            dItems1 = mdtFullBSC.Select("objectiveunkid = '" & CInt(dRow.Item("objectiveunkid")) & "' AND initiativeunkid > 0 ")
                        Case Else
                            dItems1 = mdtFullBSC.Select("objectiveunkid = '" & CInt(dRow.Item("objectiveunkid")) & "'")
                    End Select
                    dItmes2 = mdtEvaluation.Select("objectiveunkid = '" & CInt(dRow.Item("objectiveunkid")) & "' AND AUD <>'D' AND isGrp=False")
                    If dItems1.Length <> dItmes2.Length Then
                        If blnFlag = True Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), enMsgBoxStyle.Information)
                            Return False
                            Exit For
                        End If
                    End If
                Next
            End If

            Return True
            'S.SANDEEP [ 28 DEC 2012 ] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isAll_Assessed", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub DoOperation(ByVal blnFlag As Boolean)
        Try
            cboReviewer.Enabled = blnFlag
            objbtnSearchReviewer.Enabled = blnFlag
            cboAssessor.Enabled = blnFlag
            objbtnSearchAssessor.Enabled = blnFlag
            cboEmployee.Enabled = blnFlag
            objbtnSearchEmployee.Enabled = blnFlag
            cboPeriod.Enabled = blnFlag
            cboYear.Enabled = blnFlag
            dtpAssessdate.Enabled = blnFlag
            radExternalAssessor.Enabled = blnFlag
            radInternalAssessor.Enabled = blnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillEvaluationList()
        Try
            Dim blnFlag As Boolean = False
            lvEvaluation.Items.Clear()
            Dim lvItem As New ListViewItem
            Dim StrGrpName As String = ""

            RemoveHandler lvEvaluation.ItemSelectionChanged, AddressOf lvEvaluation_ItemSelectionChanged
            lvEvaluation.GridLines = False
            If mdtEvaluation.Rows.Count <= 0 Then Exit Sub
            Dim mdecTotal As Decimal = 0 : Dim medecGrpTot As Decimal = 0

            Dim mDicPerspective As New Dictionary(Of Integer, Integer)

            For Each dRow As DataRow In mdtEvaluation.Rows

                If mDicPerspective.ContainsKey(CInt(dRow.Item("perspectiveunkid"))) Then Continue For

                mDicPerspective.Add(CInt(dRow.Item("perspectiveunkid")), CInt(dRow.Item("perspectiveunkid")))

                Dim dtRow() As DataRow = mdtEvaluation.Select("perspectiveunkid = '" & CInt(dRow.Item("perspectiveunkid")) & "' AND AUD <> 'D'", "GrpId,objectiveunkid,kpiunkid,targetunkid,initiativeunkid")

                If dtRow.Length > 0 Then
                    medecGrpTot = 0
                    blnFlag = True
                    For i As Integer = 0 To dtRow.Length - 1
                        lvItem = New ListViewItem
                        If dtRow(i)("result").ToString.Trim.Length > 0 Then
                            mdecTotal += CDec(dtRow(i)("result"))
                            medecGrpTot += CDec(dtRow(i)("result"))
                        End If
                        lvItem.Text = dtRow(i).Item("group_name").ToString
                        lvItem.SubItems.Add(dtRow(i).Item("objective").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("kpi").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("target").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("initiative").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("result").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("remark").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("objectiveunkid").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("kpiunkid").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("targetunkid").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("initiativeunkid").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("resultunkid").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("GUID").ToString)
                        lvItem.SubItems.Add(dtRow(i).Item("IsGrp").ToString)
                        lvItem.SubItems.Add("1")

                        lvItem.Tag = dtRow(i).Item("analysistranunkid")

                        lvEvaluation.Items.Add(lvItem)

                        lvItem = Nothing

                        StrGrpName = dtRow(i).Item("group_name").ToString
                    Next
                    If mdicGroupWiseTotal.ContainsKey(CInt(dRow("objectiveunkid"))) Then
                        lvItem = New ListViewItem
                        lvItem.UseItemStyleForSubItems = False
                        lvItem.Text = StrGrpName
                        lvItem.SubItems.Add("TOTAL : ", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add(medecGrpTot.ToString, Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
                        lvItem.SubItems.Add("-1")
                        lvItem.SubItems.Add("-1")
                        lvItem.SubItems.Add("-1")
                        lvItem.SubItems.Add("-1")
                        lvItem.SubItems.Add("-1")
                        lvItem.SubItems.Add("-1")
                        lvItem.SubItems.Add("FALSE")
                        lvItem.SubItems.Add("2")
                        lvItem.Tag = "-1"

                        lvEvaluation.Items.Add(lvItem)
                    End If
                End If
            Next

            If blnFlag = True Then
                lvItem = New ListViewItem
                lvItem.UseItemStyleForSubItems = False
                lvItem.Text = ""
                lvItem.SubItems.Add("GRAND TOTAL : ", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
                lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
                lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
                lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
                lvItem.SubItems.Add(mdecTotal.ToString, Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
                lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
                lvItem.SubItems.Add("-1")
                lvItem.SubItems.Add("-1")
                lvItem.SubItems.Add("-1")
                lvItem.SubItems.Add("-1")
                lvItem.SubItems.Add("-1")
                lvItem.SubItems.Add("-1")
                lvItem.SubItems.Add("FALSE")
                lvItem.SubItems.Add("3")

                lvItem.Tag = "-1"
                lvEvaluation.Items.Add(lvItem)
            End If

            If lvEvaluation.Items.Count > 2 Then
                colhRemark.Width = 170 - 20
            Else
                colhRemark.Width = 170
            End If

            lvEvaluation.GroupingColumn = objcolhPerspective
            lvEvaluation.DisplayGroups(True)

            AddHandler lvEvaluation.ItemSelectionChanged, AddressOf lvEvaluation_ItemSelectionChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEvaluationList", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetEvaluation()
        Try
            If cboKPI.Enabled = True Then
                cboKPI.SelectedValue = 0
            End If
            If cboTraget.Enabled = True Then
                cboTraget.SelectedValue = 0
            End If
            If cboInitiative.Enabled = True Then
                cboInitiative.SelectedValue = 0
            End If
            cboResult.SelectedValue = 0
            txtRemark.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetEvaluation", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Emplolyee is compulsory information.Please Select Emplolyee."), enMsgBoxStyle.Information)
                cboEmployee.Select()
                Return False
            ElseIf CInt(cboYear.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Year is compulsory information.Please Select Year."), enMsgBoxStyle.Information)
                cboYear.Select()
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Return False
            ElseIf CInt(cboPerspective.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Perspective is compulsory information. Please Select Perspective."), enMsgBoxStyle.Information)
                cboPerspective.Focus()
                Return False
            ElseIf CInt(cboObjective.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Objective is compulsory information. Please Select Objective."), enMsgBoxStyle.Information)
                cboObjective.Focus()
                Return False
            ElseIf CInt(cboResult.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Result Code is compulsory information.Please Select Result Code."), enMsgBoxStyle.Information)
                cboResult.Select()
                Return False
            ElseIf CInt(cboAssessor.SelectedValue) = 0 And menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Assessors is compulsory information.Please Select Assessors."), enMsgBoxStyle.Information)
                cboAssessor.Select()
                Return False
            End If

            'S.SANDEEP [ 24 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim objPeriod As New clscommom_period_Tran
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            'If dtpAssessdate.Value.Date > objPeriod._End_Date Or dtpAssessdate.Value.Date < objPeriod._Start_Date Then
            '    Dim strMsg As String = Language.getMessage(mstrModuleName, 8, "Assessment date should be in between ") & objPeriod._Start_Date & _
            '                           Language.getMessage(mstrModuleName, 9, " And ") & objPeriod._End_Date
            '    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
            '    dtpAssessdate.Focus()
            '    Return False
            'End If
            'objPeriod = Nothing
            Dim dsYr As New DataSet : Dim oCompany As New clsCompany_Master
            dsYr = oCompany.GetFinancialYearList(Company._Object._Companyunkid, User._Object._Userunkid, "List", CInt(cboYear.SelectedValue))
            If dsYr.Tables("List").Rows.Count > 0 Then
                If dtpAssessdate.Value.Date > eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date Or _
                   dtpAssessdate.Value.Date < eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date Then
                    Dim strMsg As String = Language.getMessage(mstrModuleName, 8, "Assessment date should be in between ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date & _
                                           Language.getMessage(mstrModuleName, 9, " And ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    dtpAssessdate.Focus()
                    Return False
                End If
            Else
                If dtpAssessdate.Value.Date > FinancialYear._Object._Database_End_Date.Date Or _
                   dtpAssessdate.Value.Date < FinancialYear._Object._Database_Start_Date.Date Then
                    Dim strMsg As String = Language.getMessage(mstrModuleName, 8, "Assessment date should be in between ") & FinancialYear._Object._Database_Start_Date.Date & _
                                           Language.getMessage(mstrModuleName, 9, " And ") & FinancialYear._Object._Database_End_Date.Date
                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                dtpAssessdate.Focus()
                Return False
            End If
            End If
            'S.SANDEEP [ 24 JULY 2013 ] -- END

            If menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                If CInt(cboReviewer.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Reviewer is mandatory information. Please create Reviewer first. Assessment -> Setups -> Reviewer."), enMsgBoxStyle.Information)
                    cboReviewer.Focus()
                    Return False
                End If
            End If
            
            If IsNumeric(cboResult.Text) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, You cannot add this result as it is not a numeric number."), enMsgBoxStyle.Information)
                cboResult.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Enable_Disable(ByVal blnOperation As Boolean)
        Try
            cboPerspective.Enabled = blnOperation
            cboObjective.Enabled = blnOperation : objbtnSearchObjective.Enabled = blnOperation

            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                cboKPI.Enabled = blnOperation : objbtnSearchKPI.Enabled = blnOperation
                cboTraget.Enabled = blnOperation : objbtnSearchTraget.Enabled = blnOperation
                cboInitiative.Enabled = blnOperation : objbtnSearchInitiative.Enabled = blnOperation
            ElseIf objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                Select Case objWSetting._Weight_Typeid
                    Case enWeight_Types.WEIGHT_FIELD2
                        cboKPI.Enabled = blnOperation : objbtnSearchKPI.Enabled = blnOperation
                    Case enWeight_Types.WEIGHT_FIELD3
                        cboKPI.Enabled = blnOperation : objbtnSearchKPI.Enabled = blnOperation
                        cboTraget.Enabled = blnOperation : objbtnSearchTraget.Enabled = blnOperation
                    Case enWeight_Types.WEIGHT_FIELD4
                        cboKPI.Enabled = blnOperation : objbtnSearchKPI.Enabled = blnOperation
                        cboTraget.Enabled = blnOperation : objbtnSearchTraget.Enabled = blnOperation
                        cboInitiative.Enabled = blnOperation : objbtnSearchInitiative.Enabled = blnOperation
                End Select
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub Set_Data_EachItem()
        Try
            If IsNumeric(cboResult.Text) = True Then
                If CDbl(cboResult.Text) > CDbl(IIf(txtWeightage.Text = "", 0, txtWeightage.Text)) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            Dim dtTemp() As DataRow = mdtEvaluation.Select("initiativeunkid = '" & CInt(cboInitiative.SelectedValue) & "' AND AUD <> 'D' ")
            If dtTemp.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Selected item is already in the list. Please add another item."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim dRow As DataRow = Nothing
            Dim dTemp() As DataRow = mdtEvaluation.Select("objectiveunkid = '" & CInt(cboObjective.SelectedValue) & "' AND AUD <> 'D' ")
            If dTemp.Length <= 0 Then
                dRow = mdtEvaluation.NewRow
                dRow.Item("analysistranunkid") = -1
                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                dRow.Item("objectiveunkid") = CInt(cboObjective.SelectedValue)
                dRow.Item("resultunkid") = CInt(cboResult.SelectedValue)
                dRow.Item("remark") = ""
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("objective") = cboObjective.Text
                dRow.Item("result") = ""
                dRow.Item("group_name") = cboPerspective.Text
                dRow.Item("perspectiveunkid") = cboPerspective.SelectedValue
                dRow.Item("IsGrp") = True
                dRow.Item("GrpId") = cboPerspective.SelectedValue
                mdtEvaluation.Rows.Add(dRow)
                If mdicGroupWiseTotal.ContainsKey(CInt(cboObjective.SelectedValue)) = False Then
                    mdicGroupWiseTotal.Add(CInt(cboObjective.SelectedValue), CDec(cboResult.Text))
                End If
            End If
            dRow = mdtEvaluation.NewRow
            dRow.Item("analysistranunkid") = -1
            dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
            dRow.Item("objectiveunkid") = CInt(cboObjective.SelectedValue)
            dRow.Item("resultunkid") = CInt(cboResult.SelectedValue)
            dRow.Item("remark") = txtRemark.Text
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("objective") = ""
            dRow.Item("group_name") = cboPerspective.Text
            dRow.Item("perspectiveunkid") = cboPerspective.SelectedValue
            dRow.Item("IsGrp") = False
            dRow.Item("GrpId") = cboPerspective.SelectedValue
            If cboKPI.Enabled = True AndAlso CInt(cboKPI.SelectedValue) > 0 Then
                dRow.Item("kpiunkid") = cboKPI.SelectedValue
                dRow.Item("kpi") = cboKPI.Text
            End If

            If cboTraget.Enabled = True AndAlso CInt(cboTraget.SelectedValue) > 0 Then
                dRow.Item("targetunkid") = cboTraget.SelectedValue
                dRow.Item("target") = cboTraget.Text
            End If

            If cboInitiative.Enabled = True AndAlso CInt(cboInitiative.SelectedValue) > 0 Then
                dRow.Item("initiativeunkid") = cboInitiative.SelectedValue
                dRow.Item("initiative") = cboInitiative.Text
            End If
            dRow.Item("result") = cboResult.Text

            mdtEvaluation.Rows.Add(dRow)

            If mdicGroupWiseTotal.ContainsKey(CInt(cboObjective.SelectedValue)) = True Then
                mdicGroupWiseTotal(CInt(cboObjective.SelectedValue)) = mdicGroupWiseTotal(CInt(cboObjective.SelectedValue)) + CDec(cboResult.Text)
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Data_EachItem", mstrModuleName)
        End Try
    End Sub

    Private Sub Set_Data_BasedOn()
        Try
            Dim dtTemp() As DataRow = Nothing
            Select Case objWSetting._Weight_Typeid
                Case enWeight_Types.WEIGHT_FIELD1
                    If IsNumeric(cboResult.Text) = True Then
                        If CDbl(cboResult.Text) > CDbl(IIf(txtWeightage.Text = "", 0, txtWeightage.Text)) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item group."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    dtTemp = mdtEvaluation.Select("objectiveunkid = '" & CInt(cboObjective.SelectedValue) & "' AND AUD <> 'D' ")
                    If dtTemp.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Selected item is already in the list. Please add another item."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case enWeight_Types.WEIGHT_FIELD2
                    If IsNumeric(cboResult.Text) = True Then
                        If CDbl(cboResult.Text) > CDbl(IIf(txtWeightage.Text = "", 0, txtWeightage.Text)) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item group."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    dtTemp = mdtEvaluation.Select("kpiunkid = '" & CInt(cboKPI.SelectedValue) & "' AND AUD <> 'D' ")
                    If dtTemp.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Selected item is already in the list. Please add another item."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case enWeight_Types.WEIGHT_FIELD3
                    If IsNumeric(cboResult.Text) = True Then
                        If CDbl(cboResult.Text) > CDbl(IIf(txtWeightage.Text = "", 0, txtWeightage.Text)) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item group."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    dtTemp = mdtEvaluation.Select("targetunkid = '" & CInt(cboTraget.SelectedValue) & "' AND AUD <> 'D' ")
                    If dtTemp.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Selected item is already in the list. Please add another item."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case enWeight_Types.WEIGHT_FIELD4
                    If IsNumeric(cboResult.Text) = True Then
                        If CDbl(cboResult.Text) > CDbl(IIf(txtWeightage.Text = "", 0, txtWeightage.Text)) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item group."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    dtTemp = mdtEvaluation.Select("initiativeunkid = '" & CInt(cboInitiative.SelectedValue) & "' AND AUD <> 'D' ")
                    If dtTemp.Length > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, Selected item is already in the list. Please add another item."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
            End Select

            Dim dRow As DataRow = Nothing
            Dim dTemp() As DataRow = mdtEvaluation.Select("objectiveunkid = '" & CInt(cboObjective.SelectedValue) & "' AND AUD <> 'D' ")
            If dTemp.Length <= 0 Then
                dRow = mdtEvaluation.NewRow
                dRow.Item("analysistranunkid") = -1
                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                dRow.Item("objectiveunkid") = CInt(cboObjective.SelectedValue)
                dRow.Item("remark") = ""
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("objective") = cboObjective.Text
                If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
                    dRow.Item("result") = CDec(cboResult.Text)
                    dRow.Item("resultunkid") = CInt(cboResult.SelectedValue)
                Else
                    dRow.Item("result") = ""
                    dRow.Item("resultunkid") = 0
                End If
                dRow.Item("group_name") = cboPerspective.Text
                dRow.Item("perspectiveunkid") = cboPerspective.SelectedValue
                dRow.Item("IsGrp") = True
                dRow.Item("GrpId") = cboPerspective.SelectedValue
                mdtEvaluation.Rows.Add(dRow)
                If mdicGroupWiseTotal.ContainsKey(CInt(cboObjective.SelectedValue)) = False Then
                    mdicGroupWiseTotal.Add(CInt(cboObjective.SelectedValue), CDec(cboResult.Text))
                End If
            End If
            Dim mdTable As DataTable = Nothing
            Select Case objWSetting._Weight_Typeid
                Case enWeight_Types.WEIGHT_FIELD1
                    mdTable = objBSCEvaluation.GetBSCPlanning(CInt(cboObjective.SelectedValue))
                Case enWeight_Types.WEIGHT_FIELD2, enWeight_Types.WEIGHT_FIELD3
                    mdTable = mdtFullBSC
                Case enWeight_Types.WEIGHT_FIELD4
                    dRow = mdtEvaluation.NewRow
                    dRow.Item("analysistranunkid") = -1
                    dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                    dRow.Item("objectiveunkid") = CInt(cboObjective.SelectedValue)
                    dRow.Item("resultunkid") = CInt(cboResult.SelectedValue)
                    dRow.Item("remark") = txtRemark.Text
                    dRow.Item("AUD") = "A"
                    dRow.Item("GUID") = Guid.NewGuid.ToString
                    dRow.Item("isvoid") = False
                    dRow.Item("voiduserunkid") = -1
                    dRow.Item("voiddatetime") = DBNull.Value
                    dRow.Item("voidreason") = ""
                    dRow.Item("objective") = ""
                    dRow.Item("group_name") = cboPerspective.Text
                    dRow.Item("perspectiveunkid") = cboPerspective.SelectedValue
                    dRow.Item("IsGrp") = False
                    dRow.Item("GrpId") = cboPerspective.SelectedValue

                    If cboKPI.Enabled = True AndAlso CInt(cboKPI.SelectedValue) > 0 Then
                        dRow.Item("kpiunkid") = cboKPI.SelectedValue
                        dRow.Item("kpi") = cboKPI.Text
                    End If

                    If cboTraget.Enabled = True AndAlso CInt(cboTraget.SelectedValue) > 0 Then
                        dRow.Item("targetunkid") = cboTraget.SelectedValue
                        dRow.Item("target") = cboTraget.Text
                    End If

                    If cboInitiative.Enabled = True AndAlso CInt(cboInitiative.SelectedValue) > 0 Then
                        dRow.Item("initiativeunkid") = cboInitiative.SelectedValue
                        dRow.Item("initiative") = cboInitiative.Text
                    End If
                    dRow.Item("result") = cboResult.Text

                    mdtEvaluation.Rows.Add(dRow)
            End Select
            If mdTable IsNot Nothing Then
                Select Case objWSetting._Weight_Typeid
                    Case enWeight_Types.WEIGHT_FIELD1
                        mdTable = New DataView(mdTable, "", "", DataViewRowState.CurrentRows).ToTable
                    Case enWeight_Types.WEIGHT_FIELD2
                        mdTable = New DataView(mdTable, "KPIUnkid = '" & CInt(cboKPI.SelectedValue) & "'", "", DataViewRowState.CurrentRows).ToTable
                    Case enWeight_Types.WEIGHT_FIELD3
                        mdTable = New DataView(mdTable, "targetunkid = '" & CInt(cboTraget.SelectedValue) & "'", "", DataViewRowState.CurrentRows).ToTable
                End Select
                Dim i As Integer = 0
                For Each dMRow As DataRow In mdTable.Rows
                    dRow = mdtEvaluation.NewRow
                    dRow.Item("analysistranunkid") = -1
                    dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                    dRow.Item("objectiveunkid") = CInt(cboObjective.SelectedValue)
                    dRow.Item("AUD") = "A"
                    dRow.Item("GUID") = Guid.NewGuid.ToString
                    dRow.Item("isvoid") = False
                    dRow.Item("voiduserunkid") = -1
                    dRow.Item("voiddatetime") = DBNull.Value
                    dRow.Item("voidreason") = ""
                    dRow.Item("objective") = ""
                    dRow.Item("kpiunkid") = dMRow.Item("KPIUnkid")
                    dRow.Item("targetunkid") = dMRow.Item("TargetUnkid")
                    dRow.Item("initiativeunkid") = dMRow.Item("InitiativeUnkid")

                    Select Case objWSetting._Weight_Typeid
                        Case enWeight_Types.WEIGHT_FIELD1
                            dRow.Item("kpi") = dMRow.Item("KPI")
                            dRow.Item("target") = dMRow.Item("Target")
                            dRow.Item("initiative") = dMRow.Item("Initiative")
                        Case enWeight_Types.WEIGHT_FIELD2
                            If i = 0 Then
                                If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
                                    If CInt(dMRow.Item("KPIUnkid")) = CInt(cboKPI.SelectedValue) Then
                                        If CInt(cboResult.SelectedValue) > 0 Then
                                            dRow.Item("result") = cboResult.Text
                                            dRow.Item("remark") = txtRemark.Text
                                            dRow.Item("resultunkid") = CInt(cboResult.SelectedValue)
                                        Else
                                            dRow.Item("result") = ""
                                            dRow.Item("resultunkid") = 0
                                        End If
                                    End If
                                End If
                                dRow.Item("kpi") = dMRow.Item("KPI")
                            End If

                            If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
                                If CInt(dMRow.Item("targetunkid")) = CInt(cboTraget.SelectedValue) Then
                                    If CInt(cboResult.SelectedValue) > 0 Then
                                        dRow.Item("result") = cboResult.Text
                                        dRow.Item("remark") = txtRemark.Text
                                        dRow.Item("resultunkid") = CInt(cboResult.SelectedValue)
                                    Else
                                        dRow.Item("result") = ""
                                        dRow.Item("resultunkid") = 0
                                    End If
                                End If
                            End If
                            dRow.Item("target") = dMRow.Item("Target")

                            If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD4 Then
                                If CInt(dMRow.Item("initiativeunkid")) = CInt(cboInitiative.SelectedValue) Then
                                    If CInt(cboResult.SelectedValue) > 0 Then
                                        dRow.Item("result") = cboResult.Text
                                        dRow.Item("remark") = txtRemark.Text
                                    Else
                                        dRow.Item("result") = ""
                                    End If
                                End If
                            End If
                            dRow.Item("initiative") = dMRow.Item("Initiative")

                        Case enWeight_Types.WEIGHT_FIELD3
                            If i = 0 Then
                                If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
                                    If CInt(dMRow.Item("KPIUnkid")) = CInt(cboKPI.SelectedValue) Then
                                        If CInt(cboResult.SelectedValue) > 0 Then
                                            dRow.Item("result") = cboResult.Text
                                            dRow.Item("remark") = txtRemark.Text
                                            dRow.Item("resultunkid") = CInt(cboResult.SelectedValue)
                                        Else
                                            dRow.Item("result") = ""
                                            dRow.Item("resultunkid") = 0
                                        End If
                                    End If
                                End If
                                dRow.Item("kpi") = dMRow.Item("KPI")

                                If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
                                    If CInt(dMRow.Item("targetunkid")) = CInt(cboTraget.SelectedValue) Then
                                        If CInt(cboResult.SelectedValue) > 0 Then
                                            dRow.Item("result") = cboResult.Text
                                            dRow.Item("remark") = txtRemark.Text
                                        Else
                                            dRow.Item("result") = ""
                                        End If
                                    End If
                                End If
                                dRow.Item("target") = dMRow.Item("Target")
                            End If

                            If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD4 Then
                                If CInt(dMRow.Item("initiativeunkid")) = CInt(cboInitiative.SelectedValue) Then
                                    If CInt(cboResult.SelectedValue) > 0 Then
                                        dRow.Item("result") = cboResult.Text
                                        dRow.Item("remark") = txtRemark.Text
                                        dRow.Item("resultunkid") = CInt(cboResult.SelectedValue)
                                    Else
                                        dRow.Item("result") = ""
                                        dRow.Item("resultunkid") = 0
                                    End If
                                End If
                            End If
                            dRow.Item("initiative") = dMRow.Item("Initiative")

                    End Select

                    dRow.Item("group_name") = cboPerspective.Text
                    dRow.Item("perspectiveunkid") = cboPerspective.SelectedValue
                    dRow.Item("IsGrp") = False
                    dRow.Item("GrpId") = cboPerspective.SelectedValue
                    mdtEvaluation.Rows.Add(dRow)
                    i += 1
                Next
            End If

            If mdicGroupWiseTotal.ContainsKey(CInt(cboObjective.SelectedValue)) = True Then
                mdicGroupWiseTotal(CInt(cboObjective.SelectedValue)) = mdicGroupWiseTotal(CInt(cboObjective.SelectedValue)) + CDec(cboResult.Text)
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Set_Data_BasedOn", mstrModuleName)
        End Try
    End Sub

    Private Sub Del_Data_EachItem(ByVal drRow() As DataRow, ByVal lvItem As ListViewItem)
        Try
            If CInt(lvItem.Tag) > 0 Then
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    Dim dtTemp() As DataRow = Nothing
                    drRow(0).Item("AUD") = "D"
                    drRow(0).Item("isvoid") = True
                    drRow(0).Item("voiduserunkid") = User._Object._Userunkid
                    drRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    drRow(0).Item("voidreason") = mstrVoidReason
                    dtTemp = mdtEvaluation.Select("objectiveunkid = '" & drRow(0).Item("objectiveunkid").ToString & "' AND AUD <> 'D' AND IsGrp = False")
                    If dtTemp.Length <= 0 Then
                        dtTemp = mdtEvaluation.Select("objectiveunkid = '" & drRow(0).Item("objectiveunkid").ToString & "' AND AUD <> 'D' AND IsGrp = True")
                        dtTemp(0).Item("AUD") = "D"
                        dtTemp(0).Item("isvoid") = True
                        dtTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                        dtTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dtTemp(0).Item("voidreason") = mstrVoidReason
                    End If
                End If
            Else
                Dim dtTemp() As DataRow = Nothing
                drRow(0).Item("AUD") = "D"
                dtTemp = mdtEvaluation.Select("objectiveunkid = '" & drRow(0).Item("objectiveunkid").ToString & "' AND AUD <> 'D' AND IsGrp = False")
                If dtTemp.Length <= 0 Then
                    dtTemp = mdtEvaluation.Select("objectiveunkid = '" & drRow(0).Item("objectiveunkid").ToString & "' AND AUD <> 'D' AND IsGrp = True")
                    dtTemp(0).Item("AUD") = "D"                   
                End If
            End If
            If mdicGroupWiseTotal.ContainsKey(CInt(drRow(0)("objectiveunkid"))) Then
                mdicGroupWiseTotal(CInt(drRow(0)("objectiveunkid"))) = mdicGroupWiseTotal(CInt(drRow(0)("objectiveunkid"))) - CDec(drRow(0).Item("result"))
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Del_Data_EachItem", mstrModuleName)
        End Try
    End Sub

    Private Sub Del_Data_BasedOn(ByVal drRow() As DataRow, ByVal lvItem As ListViewItem)
        Try
            If CInt(lvItem.Tag) > 0 Then
                Dim frm As New frmReasonSelection
                If User._Object._Isrighttoleft = True Then
                    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                    frm.RightToLeftLayout = True
                    Call Language.ctlRightToLeftlayOut(frm)
                End If
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)

                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    Dim dtTemp() As DataRow = Nothing
                    Dim drTemp() As DataRow = Nothing

                    Select Case objWSetting._Weight_Typeid
                        Case enWeight_Types.WEIGHT_FIELD1
                            dtTemp = mdtEvaluation.Select("objectiveunkid = '" & drRow(0).Item("objectiveunkid").ToString & "' AND AUD <> 'D'")
                        Case enWeight_Types.WEIGHT_FIELD2
                            dtTemp = mdtEvaluation.Select("kpiunkid = '" & drRow(0).Item("kpiunkid").ToString & "' AND AUD <> 'D'")
                        Case enWeight_Types.WEIGHT_FIELD3
                            dtTemp = mdtEvaluation.Select("targetunkid = '" & drRow(0).Item("targetunkid").ToString & "' AND AUD <> 'D'")
                        Case enWeight_Types.WEIGHT_FIELD4
                            dtTemp = mdtEvaluation.Select("initiativeunkid = '" & drRow(0).Item("initiativeunkid").ToString & "' AND AUD <> 'D'")
                    End Select

                    For i As Integer = 0 To dtTemp.Length - 1
                        dtTemp(i).Item("AUD") = "D"
                        dtTemp(i).Item("isvoid") = True
                        dtTemp(i).Item("voiduserunkid") = User._Object._Userunkid
                        dtTemp(i).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dtTemp(i).Item("voidreason") = mstrVoidReason
                    Next

                    Select Case objWSetting._Weight_Typeid
                        Case enWeight_Types.WEIGHT_FIELD2
                            drTemp = mdtEvaluation.Select("kpiunkid <> '" & drRow(0).Item("kpiunkid").ToString & "' AND AUD <> 'D' AND isGrp=False")
                        Case enWeight_Types.WEIGHT_FIELD3
                            drTemp = mdtEvaluation.Select("targetunkid <> '" & drRow(0).Item("targetunkid").ToString & "' AND AUD <> 'D' AND isGrp=False")
                        Case enWeight_Types.WEIGHT_FIELD4
                            drTemp = mdtEvaluation.Select("initiativeunkid <> '" & drRow(0).Item("initiativeunkid").ToString & "' AND AUD <> 'D' AND isGrp=False")
                    End Select

                    If drTemp.Length <= 0 Then
                        drTemp = mdtEvaluation.Select("objectiveunkid = '" & drRow(0).Item("objectiveunkid").ToString & "' AND AUD <> 'D' AND isGrp=True")
                        If drTemp.Length > 0 Then
                            drTemp(0).Item("AUD") = "D"
                            drTemp(0).Item("isvoid") = True
                            drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
                            drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            drTemp(0).Item("voidreason") = mstrVoidReason
                        End If
                    End If
                End If
            Else
                Dim dtTemp() As DataRow = Nothing
                Dim drTemp() As DataRow = Nothing
                Select Case objWSetting._Weight_Typeid
                    Case enWeight_Types.WEIGHT_FIELD1
                        dtTemp = mdtEvaluation.Select("objectiveunkid = '" & drRow(0).Item("objectiveunkid").ToString & "' AND AUD <> 'D'")
                    Case enWeight_Types.WEIGHT_FIELD2
                        dtTemp = mdtEvaluation.Select("kpiunkid = '" & drRow(0).Item("kpiunkid").ToString & "' AND AUD <> 'D'")
                    Case enWeight_Types.WEIGHT_FIELD3
                        dtTemp = mdtEvaluation.Select("targetunkid = '" & drRow(0).Item("targetunkid").ToString & "' AND AUD <> 'D'")
                    Case enWeight_Types.WEIGHT_FIELD4
                        dtTemp = mdtEvaluation.Select("initiativeunkid = '" & drRow(0).Item("initiativeunkid").ToString & "' AND AUD <> 'D'")
                End Select
                For i As Integer = 0 To dtTemp.Length - 1
                    dtTemp(i).Item("AUD") = "D"
                Next

                Select Case objWSetting._Weight_Typeid
                    Case enWeight_Types.WEIGHT_FIELD2
                        drTemp = mdtEvaluation.Select("kpiunkid <> '" & drRow(0).Item("kpiunkid").ToString & "' AND AUD <> 'D' AND isGrp=False")
                    Case enWeight_Types.WEIGHT_FIELD3
                        drTemp = mdtEvaluation.Select("targetunkid <> '" & drRow(0).Item("targetunkid").ToString & "' AND AUD <> 'D' AND isGrp=False")
                    Case enWeight_Types.WEIGHT_FIELD4
                        drTemp = mdtEvaluation.Select("initiativeunkid <> '" & drRow(0).Item("initiativeunkid").ToString & "' AND AUD <> 'D' AND isGrp=False")
                End Select
                If drTemp.Length <= 0 Then
                    drTemp = mdtEvaluation.Select("objectiveunkid = '" & drRow(0).Item("objectiveunkid").ToString & "' AND AUD <> 'D' AND isGrp=True")
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                    End If
                End If
            End If
            If mdicGroupWiseTotal.ContainsKey(CInt(drRow(0)("objectiveunkid"))) Then
                mdicGroupWiseTotal(CInt(drRow(0).Item("objectiveunkid"))) = mdicGroupWiseTotal(CInt(drRow(0).Item("objectiveunkid"))) - CDec(drRow(0).Item("result"))
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Del_Data_BasedOn", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmBSC_Evaluation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objBSCEvaluation = New clsBSC_Analysis_Master
        objBSCTran = New clsBSC_analysis_tran
        Try

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            Call OtherSettings()

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objBSCEvaluation._Analysisunkid = mintAssessAnalysisUnkid
                cboEmployee.Enabled = False
                objbtnSearchEmployee.Enabled = False
                cboAssessor.Enabled = False
                objbtnSearchAssessor.Enabled = False
                'cboPerspective.Enabled = False
                cboYear.Enabled = False
                cboPeriod.Enabled = False
                radExternalAssessor.Enabled = False
                radInternalAssessor.Enabled = False
                cboReviewer.Enabled = False
                objbtnSearchReviewer.Enabled = False
            End If
            Call GetValue()

            objBSCTran._AnalysisUnkid = mintAssessAnalysisUnkid
            mdtEvaluation = objBSCTran._DataTable
            mdicGroupWiseTotal = objBSCTran._GrpWiseTotal
            mdicItemWeight = objBSCTran._ItemWeight
            iCnt = objBSCTran._Grp_Counter
            Call FillEvaluationList()

            'S.SANDEEP [ 25 JULY 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'mdtFullBSC = objBSCEvaluation.GetFullBSC()
            mdtFullBSC = objBSCEvaluation.GetFullBSC(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
            'S.SANDEEP [ 25 JULY 2013 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Evaluation_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_Evaluation_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Evaluation_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_Evaluation_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 AndAlso txtRemark.Focused = False Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBSC_Evaluation_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBSC_Evaluation_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objBSCEvaluation = Nothing : objBSCTran = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBSC_Analysis_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsBSC_Analysis_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSaveCommit.Click
        Dim blnFlag As Boolean = False
        Try
            If lvEvaluation.Items.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please add atleast one evaluation in order to save."), enMsgBoxStyle.Information)
                Exit Sub
            End If


            Call SetValue()

            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
                Case "BTNSAVECOMMIT"
                    If ConfigParameter._Object._IsAllowFinalSave = False Then
                        If isAll_Assessed() = False Then Exit Sub
                    ElseIf ConfigParameter._Object._IsAllowFinalSave = True Then
                        If isAll_Assessed(False) = False Then
                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If
                        End If
                    End If
                    objBSCEvaluation._Iscommitted = True
                    objBSCEvaluation._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime
            End Select

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objBSCEvaluation.Update(mdtEvaluation)
            Else
                blnFlag = objBSCEvaluation.Insert(mdtEvaluation)
            End If

            If blnFlag = False And objBSCEvaluation._Message <> "" Then
                eZeeMsgBox.Show(objBSCEvaluation._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                'S.SANDEEP [ 13 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper = "BTNSAVECOMMIT" AndAlso objBSCEvaluation._Iscommitted = True Then
                    Select Case menAssess
                        Case enAssessmentMode.SELF_ASSESSMENT
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objBSCEvaluation.Email_Notification(menAssess, _
                            '                                    CInt(cboEmployee.SelectedValue), _
                            '                                    CInt(cboPeriod.SelectedValue), _
                            '                                    CInt(cboYear.SelectedValue), _
                            '                                    ConfigParameter._Object._IsCompanyNeedReviewer, , , "", enLogin_Mode.DESKTOP, 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {enLogin_Mode.DESKTOP, 0} -- END
                            objBSCEvaluation.Email_Notification(menAssess, _
                                                                CInt(cboEmployee.SelectedValue), _
                                                                CInt(cboPeriod.SelectedValue), _
                                                                CInt(cboYear.SelectedValue), _
                                                                ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , "", enLogin_Mode.DESKTOP, 0)
                            'Sohail (21 Aug 2015) -- End
                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objBSCEvaluation.Email_Notification(menAssess, _
                            '                                    CInt(cboEmployee.SelectedValue), _
                            '                                    CInt(cboPeriod.SelectedValue), _
                            '                                    CInt(cboYear.SelectedValue), _
                            '                                    ConfigParameter._Object._IsCompanyNeedReviewer, , , cboAssessor.Text, enLogin_Mode.DESKTOP, 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {enLogin_Mode.DESKTOP, 0} -- END
                            objBSCEvaluation.Email_Notification(menAssess, _
                                                                CInt(cboEmployee.SelectedValue), _
                                                                CInt(cboPeriod.SelectedValue), _
                                                                CInt(cboYear.SelectedValue), _
                                                                ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , cboAssessor.Text, enLogin_Mode.DESKTOP, 0)
                            'Sohail (21 Aug 2015) -- End
                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objBSCEvaluation.Email_Notification(menAssess, _
                            '                                    CInt(cboEmployee.SelectedValue), _
                            '                                    CInt(cboPeriod.SelectedValue), _
                            '                                    CInt(cboYear.SelectedValue), _
                            '                                    ConfigParameter._Object._IsCompanyNeedReviewer, , , cboReviewer.Text, enLogin_Mode.DESKTOP, 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {enLogin_Mode.DESKTOP, 0} -- END
                            objBSCEvaluation.Email_Notification(menAssess, _
                                                                CInt(cboEmployee.SelectedValue), _
                                                                CInt(cboPeriod.SelectedValue), _
                                                                CInt(cboYear.SelectedValue), _
                                                                ConfigParameter._Object._IsCompanyNeedReviewer, FinancialYear._Object._DatabaseName, , , cboReviewer.Text, enLogin_Mode.DESKTOP, 0)
                            'Sohail (21 Aug 2015) -- End
                    End Select
                End If
                'S.SANDEEP [ 13 AUG 2013 ] -- END
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objBSCEvaluation = Nothing
                    objBSCEvaluation = New clsBSC_Analysis_Master
                    Call GetValue()
                    Call DoOperation(True)
                    mdtEvaluation.Rows.Clear()
                    Call FillEvaluationList()
                    mdicGroupWiseTotal.Clear()
                    mdicItemWeight.Clear()
                Else
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnShow.Click
        Try
            spc1.Panel1Collapsed = False
            objbtnShow.Visible = False
            objbtnHide.Visible = True
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnShow_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnHide_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnHide.Click
        Try
            spc1.Panel1Collapsed = True
            objbtnHide.Visible = False
            objbtnShow.Visible = True
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnHide_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchObjective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchObjective.Click
        Dim frm As New frmCommonSearch
        Try
            If cboObjective.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboObjective.ValueMember
                    .DisplayMember = cboObjective.DisplayMember
                    .DataSource = CType(cboObjective.DataSource, DataTable)
                End With
                If frm.DisplayDialog = True Then
                    cboObjective.SelectedValue = frm.SelectedValue
                    cboObjective.Focus()
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchObjective_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchKPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchKPI.Click
        Dim frm As New frmCommonSearch
        Try
            If cboKPI.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboKPI.ValueMember
                    .DisplayMember = cboKPI.DisplayMember
                    .DataSource = CType(cboKPI.DataSource, DataTable)
                End With
                If frm.DisplayDialog = True Then
                    cboKPI.SelectedValue = frm.SelectedValue
                    cboKPI.Focus()
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchObjective_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchTraget_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTraget.Click
        Dim frm As New frmCommonSearch
        Try
            If cboTraget.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboTraget.ValueMember
                    .DisplayMember = cboTraget.DisplayMember
                    .DataSource = CType(cboTraget.DataSource, DataTable)
                End With
                If frm.DisplayDialog = True Then
                    cboTraget.SelectedValue = frm.SelectedValue
                    cboTraget.Focus()
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchObjective_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchInitiative_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchInitiative.Click
        Dim frm As New frmCommonSearch
        Try
            If cboInitiative.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboInitiative.ValueMember
                    .DisplayMember = cboInitiative.DisplayMember
                    .DataSource = CType(cboInitiative.DataSource, DataTable)
                End With
                If frm.DisplayDialog = True Then
                    cboInitiative.SelectedValue = frm.SelectedValue
                    cboInitiative.Focus()
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearchObjective_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnAddEvaluation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddEvaluation.Click
        Try
            If Validation() = False Then Exit Sub
            Select Case menAssess
                Case enAssessmentMode.SELF_ASSESSMENT
                    If objBSCEvaluation.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), , , mintAssessAnalysisUnkid) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    'S.SANDEEP [ 09 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If ConfigParameter._Object._AllowAssessor_Before_Emp = False Then
                        If objBSCEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                    'S.SANDEEP [ 09 AUG 2013 ] -- END
                    If objBSCEvaluation.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessor.SelectedValue), , mintAssessAnalysisUnkid, radExternalAssessor.Checked) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    'S.SANDEEP [ 09 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If ConfigParameter._Object._AllowAssessor_Before_Emp = False Then
                        If objBSCEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If

                        'S.SANDEEP [ 10 SEPT 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        If objBSCEvaluation.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1) = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless assessor does assessment for the selected employee and period."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                        'S.SANDEEP [ 10 SEPT 2013 ] -- END

                    End If
                    'S.SANDEEP [ 09 AUG 2013 ] -- END
                    If objBSCEvaluation.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), , CInt(cboReviewer.SelectedValue), mintAssessAnalysisUnkid) = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
            End Select

            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                Call Set_Data_EachItem()
            ElseIf objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                Call Set_Data_BasedOn()
            End If

            Call FillEvaluationList()
            Call ResetEvaluation()
            Call DoOperation(False)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnAddEvaluation_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEditEvaluation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditEvaluation.Click
        Try
            If lvEvaluation.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvEvaluation.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtEvaluation.Select("GUID = '" & lvEvaluation.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtEvaluation.Select("analysistranunkid = '" & CInt(lvEvaluation.SelectedItems(0).Tag) & "'")
                End If
                If drTemp.Length > 0 Then
                    If IsNumeric(cboResult.Text) Then
                        If CDbl(cboResult.Text) > CDbl(txtWeightage.Text) Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry, you cannot set this result as it exceeds the weight assigned to particular item group."), enMsgBoxStyle.Information)
                            Exit Sub
                        End If
                    End If
                End If
                If Validation() = False Then Exit Sub
                If drTemp.Length > 0 Then
                    With drTemp(0)
                        .Item("analysistranunkid") = lvEvaluation.SelectedItems(0).Tag
                        .Item("analysisunkid") = mintAssessAnalysisUnkid
                        .Item("objectiveunkid") = CInt(cboObjective.SelectedValue)
                        .Item("resultunkid") = CInt(cboResult.SelectedValue)
                        .Item("remark") = txtRemark.Text
                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If
                        .Item("GUID") = Guid.NewGuid.ToString
                        .Item("isvoid") = False
                        .Item("voiduserunkid") = -1
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voidreason") = ""
                        .Item("objective") = .Item("objective")

                        If cboKPI.Enabled = True Then
                            .Item("kpi") = cboKPI.Text
                            .Item("kpiunkid") = CInt(cboKPI.SelectedValue)
                        End If

                        If cboTraget.Enabled = True Then
                            .Item("target") = cboTraget.Text
                            .Item("targetunkid") = CInt(cboTraget.SelectedValue)
                        End If
                        
                        If cboInitiative.Enabled = True Then
                            .Item("initiative") = cboInitiative.Text
                            .Item("initiativeunkid") = CInt(cboInitiative.SelectedValue)
                        End If

                        .Item("result") = cboResult.Text
                        .Item("group_name") = cboPerspective.Text
                        .Item("perspectiveunkid") = cboPerspective.SelectedValue
                        .AcceptChanges()
                    End With
                    Call FillEvaluationList()
                End If
                Call ResetEvaluation()
            End If
            Enable_Disable(True)
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEditEvaluation_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDeleteEvaluation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteEvaluation.Click
        Try
            If lvEvaluation.SelectedItems.Count > 0 Then
                Dim drTemp As DataRow()
                If CInt(lvEvaluation.SelectedItems(0).Tag) = -1 Then
                    drTemp = mdtEvaluation.Select("GUID = '" & lvEvaluation.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
                Else
                    drTemp = mdtEvaluation.Select("analysistranunkid = '" & CInt(lvEvaluation.SelectedItems(0).Tag) & "'")
                End If
                If drTemp.Length > 0 Then
                    Select Case objWSetting._Weight_Optionid
                        Case enWeight_Options.WEIGHT_EACH_ITEM
                            Call Del_Data_EachItem(drTemp, lvEvaluation.SelectedItems(0))
                        Case enWeight_Options.WEIGHT_BASED_ON
                            Call Del_Data_BasedOn(drTemp, lvEvaluation.SelectedItems(0))
                    End Select
                    mdtEvaluation.AcceptChanges()
                    Call FillEvaluationList()
                End If
                Call ResetEvaluation()
            End If
            Call Enable_Disable(True)
            If lvEvaluation.Items.Count <= 0 Then
                Call DoOperation(True)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDeleteEvaluation_Click", mstrModuleName)
        End Try
    End Sub
    
#End Region

#Region " Controls Events "

    Private Sub cboAssessor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
            If CInt(cboAssessor.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                If radInternalAssessor.Checked = True Then
                    dsList = objBSCEvaluation.getEmployeeBasedAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
                ElseIf radExternalAssessor.Checked = True Then
                    Dim objExtAssessor As New clsexternal_assessor_master
                    dsList = objExtAssessor.GetEmpBasedOnExtAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
                End If
                With cboEmployee
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("AEmp")
                    .SelectedValue = 0
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAssessor_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub radInternalAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radInternalAssessor.CheckedChanged
        Try
            If radInternalAssessor.Checked = True Then
                cboAssessor.DataSource = Nothing
                Dim dsCombos As DataSet = objBSCEvaluation.getAssessorComboList("Assessor", True)
                With cboAssessor
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radInternalAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radExternalAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radExternalAssessor.CheckedChanged
        Try
            If radExternalAssessor.Checked = True Then
                cboAssessor.DataSource = Nothing
                Dim objExtAssessor As New clsexternal_assessor_master
                Dim dsCombos As DataSet = objExtAssessor.GetDisplayNameComboList("Assessor", True)
                With cboAssessor
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .SelectedValue = 0
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radExternalAssessor_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Try
            If CInt(cboYear.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "APeriod", True, 1)
                dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "APeriod", True, 1)
                'Sohail (21 Aug 2015) -- End
                With cboPeriod
                    .ValueMember = "periodunkid"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("APeriod")
                    .SelectedValue = 0
                End With
            Else
                cboPeriod.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If cboEmployee.DataSource IsNot Nothing Then
                frm.ValueMember = cboEmployee.ValueMember
                frm.DisplayMember = cboEmployee.DisplayMember
                Select Case menAssess
                    Case enAssessmentMode.SELF_ASSESSMENT
                        frm.CodeMember = "employeecode"
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT
                        frm.CodeMember = "Code"
                End Select
                frm.DataSource = CType(cboEmployee.DataSource, DataTable)
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    cboEmployee.Focus()
                End If
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchAssessor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAssessor.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboAssessor.ValueMember
            frm.DisplayMember = cboAssessor.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboAssessor.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboAssessor.SelectedValue = frm.SelectedValue
                cboAssessor.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAssessor_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchResult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchResult.Click
        Dim frm As New frmCommonSearch
        Try
            If cboResult.DataSource IsNot Nothing Then
                With frm
                    .ValueMember = cboResult.ValueMember
                    .DisplayMember = cboResult.DisplayMember
                    .DataSource = CType(cboResult.DataSource, DataTable)
                End With
                If frm.DisplayDialog Then
                    cboResult.SelectedValue = frm.SelectedValue
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchResult_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub lnkBSCPlanning_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkBSCPlanning.LinkClicked
        Try
            Dim frm As New frmBSC_Planning

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog(CInt(cboObjective.SelectedValue), cboObjective.Text)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkBSCPlanning_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboReviewer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReviewer.SelectedIndexChanged
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
            If CInt(cboReviewer.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                dsList = objBSCEvaluation.getEmployeeBasedAssessor(CInt(cboReviewer.SelectedValue), "REmp", True)
                With cboEmployee
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("REmp")
                    .SelectedValue = 0
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReviewer_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchReviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchReviewer.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ValueMember = cboReviewer.ValueMember
            frm.DisplayMember = cboReviewer.DisplayMember
            frm.CodeMember = "Code"
            frm.DataSource = CType(cboReviewer.DataSource, DataTable)
            If frm.DisplayDialog Then
                cboReviewer.SelectedValue = frm.SelectedValue
                cboReviewer.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchReviewer_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboPerspective.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            Dim dsList As New DataSet : Dim objObjective As New clsObjective_Master
            dsList = objObjective.getComboList("List", True, CInt(cboPerspective.SelectedValue), CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
            With cboObjective
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboObjective_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboObjective.SelectedIndexChanged
        Try
            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                Dim dsList As New DataSet
                Dim objKPI As New clsKPI_Master
                dsList = objKPI.getComboList("List", True, CInt(cboObjective.SelectedValue), CInt(cboEmployee.SelectedValue))
                With cboKPI
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = 0
                End With
                objKPI = Nothing
            ElseIf objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                If cboKPI.Enabled = True Then
                    Dim dsList As New DataSet
                    Dim objKPI As New clsKPI_Master
                    dsList = objKPI.getComboList("List", True, CInt(cboObjective.SelectedValue), CInt(cboEmployee.SelectedValue))
                    With cboKPI
                        .ValueMember = "id"
                        .DisplayMember = "name"
                        .DataSource = dsList.Tables("List")
                        .SelectedValue = 0
                    End With
                    objKPI = Nothing
                End If
            End If
            If CInt(cboObjective.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objObjective As New clsObjective_Master
                'S.SANDEEP [ 10 APR 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                objObjective._Objectiveunkid = CInt(cboObjective.SelectedValue)
                'S.SANDEEP [ 10 APR 2013 ] -- END
                Dim objResult As New clsresult_master
                dsList = objResult.getComboList("List", True, objObjective._ResultGroupunkid)
                With cboResult
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("List")
                End With
                dsList.Dispose() : objResult = Nothing
                lnkBSCPlanning.Enabled = True
                If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                    If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
                        objObjective._Objectiveunkid = CInt(cboObjective.SelectedValue)
                        txtWeightage.Text = CStr(objObjective._Weight)
                    End If
                End If
                objObjective = Nothing
            Else
                If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                    If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
                        txtWeightage.Text = ""
                    End If
                End If
                cboResult.DataSource = Nothing
                lnkBSCPlanning.Enabled = False
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboObjective_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboKPI_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboKPI.SelectedIndexChanged
        Try
            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                Dim dsList As New DataSet
                Dim objTarget As New clstarget_master
                dsList = objTarget.getComboList("List", True, CInt(cboObjective.SelectedValue), CInt(cboKPI.SelectedValue), CInt(cboEmployee.SelectedValue))
                With cboTraget
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = 0
                End With
                objTarget = Nothing
            ElseIf objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                If cboTraget.Enabled = True Then
                    Dim dsList As New DataSet
                    Dim objTarget As New clstarget_master
                    dsList = objTarget.getComboList("List", True, CInt(cboObjective.SelectedValue), CInt(cboKPI.SelectedValue), CInt(cboEmployee.SelectedValue))
                    With cboTraget
                        .ValueMember = "id"
                        .DisplayMember = "name"
                        .DataSource = dsList.Tables("List")
                        .SelectedValue = 0
                    End With
                    objTarget = Nothing
                End If
            End If

            If CInt(cboKPI.SelectedValue) > 0 Then
                Dim objKPI As New clsKPI_Master
                objKPI._Kpiunkid = CInt(cboKPI.SelectedValue)
                If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                    If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
                        txtWeightage.Text = CStr(objKPI._Weight)
                    End If
                End If
                objKPI = Nothing
            Else
                If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                    If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
                        txtWeightage.Text = ""
                    End If
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboKPI_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboTraget_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTraget.SelectedIndexChanged
        Try
            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                Dim dsList As New DataSet
                Dim objInitiative As New clsinitiative_master
                dsList = objInitiative.getComboList("List", True, CInt(cboObjective.SelectedValue), CInt(cboTraget.SelectedValue))
                With cboInitiative
                    .ValueMember = "id"
                    .DisplayMember = "name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = 0
                End With
            ElseIf objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                If cboInitiative.Enabled = True Then
                    Dim dsList As New DataSet
                    Dim objInitiative As New clsinitiative_master
                    dsList = objInitiative.getComboList("List", True, CInt(cboObjective.SelectedValue), CInt(cboTraget.SelectedValue))
                    With cboInitiative
                        .ValueMember = "id"
                        .DisplayMember = "name"
                        .DataSource = dsList.Tables("List")
                        .SelectedValue = 0
                    End With
                    objInitiative = Nothing
                End If
            End If

            If CInt(cboTraget.SelectedValue) > 0 Then
                Dim objTarget As New clstarget_master
                objTarget._Targetunkid = CInt(cboTraget.SelectedValue)
                If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                    If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
                        txtWeightage.Text = CStr(objTarget._Weight)
                    End If
                End If
                objTarget = Nothing
            Else
                If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                    If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
                        txtWeightage.Text = ""
                    End If
                End If
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboTraget_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboInitiative_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboInitiative.SelectedIndexChanged
        Try
            If cboInitiative.Enabled = True Then
                If CInt(cboInitiative.SelectedValue) > 0 Then
                    Dim objInitiative As New clsinitiative_master
                    objInitiative._Initiativeunkid = CInt(cboInitiative.SelectedValue)
                    Select Case objWSetting._Weight_Optionid
                        Case enWeight_Options.WEIGHT_EACH_ITEM
                            txtWeightage.Text = CStr(objInitiative._Weight)
                        Case enWeight_Options.WEIGHT_BASED_ON
                            If objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD4 Then
                                txtWeightage.Text = CStr(objInitiative._Weight)
                            End If
                    End Select
                Else
                    txtWeightage.Text = ""
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboInitiative_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvEvaluation_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvEvaluation.ItemSelectionChanged
        Try
            If lvEvaluation.SelectedItems.Count > 0 Then
                If e.Item.SubItems(colhResult.Index).Text = "" Or _
                   CInt(e.Item.SubItems(objcolhSortId.Index).Text) > 1 Then
                    e.Item.Selected = False
                End If
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "lvEvaluation_ItemSelectionChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvEvaluation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvEvaluation.Click
        Try
            If lvEvaluation.SelectedItems.Count > 0 Then
                Call Enable_Disable(True) : cboPerspective.SelectedValue = 0
                cboPerspective.Text = lvEvaluation.SelectedItems(0).SubItems(objcolhPerspective.Index).Text
                cboObjective.SelectedValue = lvEvaluation.SelectedItems(0).SubItems(objcolhobjectiveId.Index).Text
                cboResult.SelectedValue = lvEvaluation.SelectedItems(0).SubItems(objcolhResultId.Index).Text
                txtRemark.Text = lvEvaluation.SelectedItems(0).SubItems(colhRemark.Index).Text
                Select Case objWSetting._Weight_Optionid
                    Case enWeight_Options.WEIGHT_EACH_ITEM
                        cboKPI.SelectedValue = CInt(lvEvaluation.SelectedItems(0).SubItems(objcolhKpiId.Index).Text)
                        cboTraget.SelectedValue = CInt(lvEvaluation.SelectedItems(0).SubItems(objcolhTargetId.Index).Text)
                        cboInitiative.SelectedValue = CInt(lvEvaluation.SelectedItems(0).SubItems(objcolhInitiativeId.Index).Text)
                    Case enWeight_Options.WEIGHT_BASED_ON
                        Select Case objWSetting._Weight_Typeid
                            Case enWeight_Types.WEIGHT_FIELD2
                                cboKPI.SelectedValue = CInt(lvEvaluation.SelectedItems(0).SubItems(objcolhKpiId.Index).Text)
                            Case enWeight_Types.WEIGHT_FIELD3
                                cboKPI.SelectedValue = CInt(lvEvaluation.SelectedItems(0).SubItems(objcolhKpiId.Index).Text)
                                cboTraget.SelectedValue = CInt(lvEvaluation.SelectedItems(0).SubItems(objcolhTargetId.Index).Text)
                            Case enWeight_Types.WEIGHT_FIELD4
                                cboKPI.SelectedValue = CInt(lvEvaluation.SelectedItems(0).SubItems(objcolhKpiId.Index).Text)
                                cboTraget.SelectedValue = CInt(lvEvaluation.SelectedItems(0).SubItems(objcolhTargetId.Index).Text)
                                cboInitiative.SelectedValue = CInt(lvEvaluation.SelectedItems(0).SubItems(objcolhInitiativeId.Index).Text)
                        End Select
                End Select
                
                Call Enable_Disable(False)
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "lvEvaluation_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkViewBSC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewBSC.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or _
               CInt(cboYear.SelectedValue) <= 0 Or _
               CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please set following information [Employee,Year,Period and Group] to view assessment."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim frm As New frmCommonBSC_View

            'S.SANDEEP [ 10 APR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'frm.displayDialog(cboEmployee.Text, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue))
            frm.displayDialog(cboEmployee.Text, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), True)
            'S.SANDEEP [ 10 APR 2013 ] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkViewBSC_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbBSCEvaluation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBSCEvaluation.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSaveCommit.GradientBackColor = GUI._ButttonBackColor
            Me.btnSaveCommit.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnDeleteEvaluation.GradientBackColor = GUI._ButttonBackColor
            Me.btnDeleteEvaluation.GradientForeColor = GUI._ButttonFontColor

            Me.btnEditEvaluation.GradientBackColor = GUI._ButttonBackColor
            Me.btnEditEvaluation.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddEvaluation.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddEvaluation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSaveCommit.Text = Language._Object.getCaption(Me.btnSaveCommit.Name, Me.btnSaveCommit.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbBSCEvaluation.Text = Language._Object.getCaption(Me.gbBSCEvaluation.Name, Me.gbBSCEvaluation.Text)
            Me.lblAssessDate.Text = Language._Object.getCaption(Me.lblAssessDate.Name, Me.lblAssessDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.Name, Me.lblYear.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblAssessor.Text = Language._Object.getCaption(Me.lblAssessor.Name, Me.lblAssessor.Text)
            Me.lblReviewer.Text = Language._Object.getCaption(Me.lblReviewer.Name, Me.lblReviewer.Text)
            Me.lblObjective.Text = Language._Object.getCaption(Me.lblObjective.Name, Me.lblObjective.Text)
            Me.lblResult.Text = Language._Object.getCaption(Me.lblResult.Name, Me.lblResult.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.btnDeleteEvaluation.Text = Language._Object.getCaption(Me.btnDeleteEvaluation.Name, Me.btnDeleteEvaluation.Text)
            Me.btnEditEvaluation.Text = Language._Object.getCaption(Me.btnEditEvaluation.Name, Me.btnEditEvaluation.Text)
            Me.btnAddEvaluation.Text = Language._Object.getCaption(Me.btnAddEvaluation.Name, Me.btnAddEvaluation.Text)
            Me.lnkViewBSC.Text = Language._Object.getCaption(Me.lnkViewBSC.Name, Me.lnkViewBSC.Text)
            Me.lblPerspective.Text = Language._Object.getCaption(Me.lblPerspective.Name, Me.lblPerspective.Text)
            Me.colhObjective.Text = Language._Object.getCaption(CStr(Me.colhObjective.Tag), Me.colhObjective.Text)
            Me.colhKPI.Text = Language._Object.getCaption(CStr(Me.colhKPI.Tag), Me.colhKPI.Text)
            Me.colhTargets.Text = Language._Object.getCaption(CStr(Me.colhTargets.Tag), Me.colhTargets.Text)
            Me.colhInitiative.Text = Language._Object.getCaption(CStr(Me.colhInitiative.Tag), Me.colhInitiative.Text)
            Me.colhResult.Text = Language._Object.getCaption(CStr(Me.colhResult.Tag), Me.colhResult.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
            Me.radExternalAssessor.Text = Language._Object.getCaption(Me.radExternalAssessor.Name, Me.radExternalAssessor.Text)
            Me.radInternalAssessor.Text = Language._Object.getCaption(Me.radInternalAssessor.Name, Me.radInternalAssessor.Text)
            Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
            Me.lnkBSCPlanning.Text = Language._Object.getCaption(Me.lnkBSCPlanning.Name, Me.lnkBSCPlanning.Text)
            Me.lblInitiative.Text = Language._Object.getCaption(Me.lblInitiative.Name, Me.lblInitiative.Text)
            Me.lblTarget.Text = Language._Object.getCaption(Me.lblTarget.Name, Me.lblTarget.Text)
            Me.lblKPI.Text = Language._Object.getCaption(Me.lblKPI.Name, Me.lblKPI.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Emplolyee is compulsory information.Please Select Emplolyee.")
            Language.setMessage(mstrModuleName, 2, "Year is compulsory information.Please Select Year.")
            Language.setMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period.")
            Language.setMessage(mstrModuleName, 4, "Perspective is compulsory information. Please Select Perspective.")
            Language.setMessage(mstrModuleName, 5, "Objective is compulsory information. Please Select Objective.")
            Language.setMessage(mstrModuleName, 6, "Result Code is compulsory information.Please Select Result Code.")
            Language.setMessage(mstrModuleName, 7, "Assessors is compulsory information.Please Select Assessors.")
            Language.setMessage(mstrModuleName, 8, "Assessment date should be in between")
            Language.setMessage(mstrModuleName, 9, " And")
            Language.setMessage(mstrModuleName, 10, "Self")
            Language.setMessage(mstrModuleName, 11, "Assessor's")
            Language.setMessage(mstrModuleName, 12, "Reviewer's")
            Language.setMessage(mstrModuleName, 13, "Please add atleast one evaluation in order to save.")
            Language.setMessage(mstrModuleName, 14, "Sorry, you cannot do Save Commit Operation as you have not assessed all objective(s) in the selected Perspective.")
            Language.setMessage(mstrModuleName, 15, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items.")
            Language.setMessage(mstrModuleName, 16, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
            Language.setMessage(mstrModuleName, 17, "Reviewer is mandatory information. Please create Reviewer first. Assessment -> Setups -> Reviewer.")
            Language.setMessage(mstrModuleName, 18, "Sorry, You cannot add this result as it is not a numeric number.")
            Language.setMessage(mstrModuleName, 19, "Sorry, Selected item is already in the list. Please add another item.")
            Language.setMessage(mstrModuleName, 20, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item.")
            Language.setMessage(mstrModuleName, 21, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item group.")
            Language.setMessage(mstrModuleName, 22, "Sorry, you cannot set this result as it exceeds the weight assigned to particular item group.")
            Language.setMessage(mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class

'Public Class frmBSC_Evaluation1

'#Region " Private Variables "

'    Private mstrModuleName As String = "frmEvaluation"
'    Private mblnCancel As Boolean = True
'    Private menAction As enAction = enAction.ADD_ONE
'    Private objBSCEvaluation As clsBSC_Analysis_Master
'    Private objBSCTran As clsBSC_analysis_tran
'    Private mintAssessAnalysisUnkid As Integer = -1
'    Private mintEmplId As Integer = 0
'    Private mintYearId As Integer = 0
'    Private mintPeriodId As Integer = 0
'    Private mintAssessorId As Integer = 0
'    Private menAssess As enAssessmentMode

'    Private mdicGroupWiseTotal As New Dictionary(Of Integer, Decimal)
'    Private mdicItemWeight As New Dictionary(Of Integer, Decimal)

'    Dim iCnt As Integer = 0
'    Dim mdecRemaininResult As Decimal = 0

'    Private mdtEvaluation As DataTable

'#End Region

'#Region " Display Dialog "

'    Public Function displayDialog(ByRef intUnkid As Integer, ByVal eAction As enAction, ByVal eAssess As enAssessmentMode) As Boolean
'        Try
'            mintAssessAnalysisUnkid = intUnkid
'            menAction = eAction
'            menAssess = eAssess
'            Me.ShowDialog()
'            intUnkid = mintAssessAnalysisUnkid
'            Return Not mblnCancel
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
'        Finally
'        End Try
'    End Function

'#End Region

'#Region " Private Methods "

'    Private Sub SetVisibility()
'        Try
'            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
'                cboAssessor.Visible = False
'                lblAssessor.Visible = False
'                objbtnSearchAssessor.Enabled = False
'                pnlContainer.Location = New Point(3, 32)
'                colhResult.Text = Language.getMessage(mstrModuleName, 10, "Self")
'                lnkViewBSC.Visible = False
'                radInternalAssessor.Checked = False : radExternalAssessor.Checked = False
'                radInternalAssessor.Visible = False : radExternalAssessor.Visible = False
'                lblReviewer.Visible = False
'                cboReviewer.Visible = False
'                objbtnSearchReviewer.Visible = False
'            ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'                cboAssessor.Visible = True
'                lblAssessor.Visible = True
'                objbtnSearchAssessor.Enabled = True
'                pnlContainer.Location = New Point(3, 56)
'                colhResult.Text = Language.getMessage(mstrModuleName, 11, "Assessor's")
'                lblReviewer.Visible = False
'                cboReviewer.Visible = False
'                objbtnSearchReviewer.Visible = False
'                lnkViewBSC.Visible = False
'                radInternalAssessor.Checked = True
'                radInternalAssessor.Visible = True : radExternalAssessor.Visible = True
'            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
'                colhResult.Text = Language.getMessage(mstrModuleName, 12, "Reviewer's")
'                cboAssessor.Visible = False
'                lblAssessor.Visible = False
'                objbtnSearchAssessor.Visible = False
'                lblReviewer.Visible = True
'                cboReviewer.Visible = True
'                objbtnSearchReviewer.Visible = True
'                lnkViewBSC.Visible = True
'                radInternalAssessor.Checked = False : radExternalAssessor.Checked = False
'                radInternalAssessor.Visible = False : radExternalAssessor.Visible = False
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub SetColor()
'        Try
'            cboEmployee.BackColor = GUI.ColorComp
'            cboYear.BackColor = GUI.ColorComp
'            cboPeriod.BackColor = GUI.ColorComp
'            cboObjective.BackColor = GUI.ColorComp
'            txtRemark.BackColor = GUI.ColorOptional
'            cboPerspective.BackColor = GUI.ColorComp
'            cboAssessor.BackColor = GUI.ColorComp
'            cboResult.BackColor = GUI.ColorComp
'            cboReviewer.BackColor = GUI.ColorComp
'            txtRemark.BackColor = GUI.ColorOptional
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub FillCombo()
'        Dim dsCombos As New DataSet
'        Dim objEmp As New clsEmployee_Master
'        Dim objMaster As New clsMasterData
'        Dim objObjective As New clsObjective_Master
'        Dim objKPI As New clsKPI_Master
'        Dim objTarget As New clstarget_master
'        Dim objIA As New clsinitiative_master
'        Try
'            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
'                'Anjan (17 Apr 2012)-Start
'                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
'                'dsCombos = objEmp.GetEmployeeList("Emp", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
'                If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
'                Else
'                    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
'                End If
'                'Anjan (17 Apr 2012)-End 

'                With cboEmployee
'                    .ValueMember = "employeeunkid"
'                    .DisplayMember = "employeename"
'                    .DataSource = dsCombos.Tables(0)
'                    .SelectedValue = 0
'                End With
'            ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
'                dsCombos = objBSCEvaluation.getAssessorComboList("Assessor", True, True)
'                With cboReviewer
'                    .ValueMember = "Id"
'                    .DisplayMember = "Name"
'                    .DataSource = dsCombos.Tables("Assessor")
'                    .SelectedValue = 0
'                End With
'            End If


'            dsCombos = objMaster.getComboListPAYYEAR("Year", True)
'            With cboYear
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsCombos.Tables("Year")
'            End With

'            dsCombos = objMaster.Get_BSC_Perspective("List", True)
'            With cboPerspective
'                .ValueMember = "Id"
'                .DisplayMember = "Name"
'                .DataSource = dsCombos.Tables("List")
'                .SelectedValue = 0
'            End With

'            dsCombos = objObjective.getComboList("List", True)
'            With cboObjective
'                .ValueMember = "id"
'                .DisplayMember = "name"
'                .DataSource = dsCombos.Tables("List")
'                .SelectedValue = 0
'            End With
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub GetValue()
'        Try
'            Select Case menAssess
'                Case enAssessmentMode.SELF_ASSESSMENT
'                    cboEmployee.SelectedValue = objBSCEvaluation._Selfemployeeunkid
'                Case enAssessmentMode.APPRAISER_ASSESSMENT
'                    If objBSCEvaluation._Ext_Assessorunkid > 0 Then
'                        radExternalAssessor.Checked = True
'                        cboAssessor.SelectedValue = objBSCEvaluation._Ext_Assessorunkid
'                    Else
'                        radInternalAssessor.Checked = True
'                        cboAssessor.SelectedValue = objBSCEvaluation._Assessormasterunkid
'                    End If
'                    cboEmployee.SelectedValue = objBSCEvaluation._Assessedemployeeunkid
'                Case enAssessmentMode.REVIEWER_ASSESSMENT

'                    cboReviewer.SelectedValue = objBSCEvaluation._Assessormasterunkid
'                    cboEmployee.SelectedValue = objBSCEvaluation._Assessedemployeeunkid
'            End Select
'            cboYear.SelectedValue = objBSCEvaluation._Yearunkid
'            cboPeriod.SelectedValue = objBSCEvaluation._Periodunkid
'            If objBSCEvaluation._Assessmentdate <> Nothing Then
'                dtpAssessdate.Value = objBSCEvaluation._Assessmentdate
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub SetValue()
'        Try
'            objBSCEvaluation._Yearunkid = CInt(cboYear.SelectedValue)
'            objBSCEvaluation._Periodunkid = CInt(cboPeriod.SelectedValue)
'            Select Case menAssess
'                Case enAssessmentMode.SELF_ASSESSMENT
'                    objBSCEvaluation._Selfemployeeunkid = CInt(cboEmployee.SelectedValue)
'                    objBSCEvaluation._Assessedemployeeunkid = -1
'                    objBSCEvaluation._Assessormasterunkid = -1
'                    objBSCEvaluation._Assessoremployeeunkid = -1
'                    objBSCEvaluation._Reviewerunkid = -1
'                    objBSCEvaluation._Assessmodeid = enAssessmentMode.SELF_ASSESSMENT
'                Case enAssessmentMode.APPRAISER_ASSESSMENT
'                    objBSCEvaluation._Selfemployeeunkid = -1
'                    objBSCEvaluation._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
'                    If radInternalAssessor.Checked = True Then
'                        objBSCEvaluation._Assessormasterunkid = CInt(cboAssessor.SelectedValue)
'                        Dim intEmployeeId As Integer = -1
'                        intEmployeeId = objBSCEvaluation.GetAssessorEmpId(CInt(cboAssessor.SelectedValue))
'                        objBSCEvaluation._Assessoremployeeunkid = intEmployeeId
'                    ElseIf radExternalAssessor.Checked = True Then
'                        objBSCEvaluation._Ext_Assessorunkid = CInt(cboAssessor.SelectedValue)
'                        objBSCEvaluation._Assessormasterunkid = -1
'                        objBSCEvaluation._Assessoremployeeunkid = -1
'                    End If
'                    objBSCEvaluation._Reviewerunkid = -1
'                    objBSCEvaluation._Assessmodeid = enAssessmentMode.APPRAISER_ASSESSMENT
'                Case enAssessmentMode.REVIEWER_ASSESSMENT
'                    objBSCEvaluation._Selfemployeeunkid = -1
'                    objBSCEvaluation._Assessormasterunkid = -1
'                    objBSCEvaluation._Assessoremployeeunkid = -1
'                    objBSCEvaluation._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
'                    objBSCEvaluation._Reviewerunkid = User._Object._Userunkid
'                    objBSCEvaluation._Assessmodeid = enAssessmentMode.REVIEWER_ASSESSMENT
'                    objBSCEvaluation._Assessormasterunkid = CInt(cboReviewer.SelectedValue)
'                    Dim intEmployeeId As Integer = -1
'                    intEmployeeId = objBSCEvaluation.GetAssessorEmpId(CInt(cboReviewer.SelectedValue))
'                    objBSCEvaluation._Reviewerunkid = intEmployeeId
'            End Select
'            objBSCEvaluation._Assessmentdate = dtpAssessdate.Value
'            objBSCEvaluation._Userunkid = User._Object._Userunkid
'            'S.SANDEEP [ 14 JUNE 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If objBSCEvaluation._Committeddatetime <> Nothing Then
'                objBSCEvaluation._Committeddatetime = objBSCEvaluation._Committeddatetime
'            Else
'                objBSCEvaluation._Committeddatetime = Nothing
'            End If
'            'S.SANDEEP [ 14 JUNE 2012 ] -- END
'            Select Case menAction
'                Case enAction.ADD_ONE, enAction.ADD_CONTINUE
'                    objBSCEvaluation._Isvoid = False
'                    objBSCEvaluation._Voiduserunkid = -1
'                    objBSCEvaluation._Voiddatetime = Nothing
'                    objBSCEvaluation._Voidreason = ""
'                Case enAction.EDIT_ONE
'                    objBSCEvaluation._Isvoid = objBSCEvaluation._Isvoid
'                    objBSCEvaluation._Voiduserunkid = objBSCEvaluation._Voiduserunkid
'                    objBSCEvaluation._Voiddatetime = objBSCEvaluation._Voiddatetime
'                    objBSCEvaluation._Voidreason = objBSCEvaluation._Voidreason
'            End Select
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Function isAll_Assessed(Optional ByVal blnFlag As Boolean = True) As Boolean
'        Try
'            Dim dtView As DataView = mdtEvaluation.DefaultView

'            Dim mdic_Items As New Dictionary(Of Integer, Integer)
'            objBSCEvaluation.Get_Items_Count(CInt(cboPerspective.SelectedValue), CInt(cboEmployee.SelectedValue))
'            mdic_Items = objBSCEvaluation._AllItemCount

'            If mdic_Items.Keys.Count > 0 Then
'                '/******************* OBJECTIVE *********************/
'                If mdic_Items.ContainsKey(0) Then
'                    If mdic_Items(0) <> dtView.ToTable(True, "objectiveunkid").Rows.Count Then
'                        If blnFlag = True Then
'                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Sorry, you cannot do Save Commit Operation as you have not assessed all objective(s) in the selected Perspective."), enMsgBoxStyle.Information)
'                        End If
'                        cboObjective.Focus()
'                        Return False
'                    End If
'                End If
'                '/******************* OBJECTIVE *********************/
'            Else
'                Return False
'            End If

'            Return True

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "isAll_Assessed", mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Private Sub DoOperation(ByVal blnFlag As Boolean)
'        Try
'            'cboPerspective.Enabled = blnFlag
'            cboAssessor.Enabled = blnFlag
'            cboEmployee.Enabled = blnFlag
'            cboPeriod.Enabled = blnFlag
'            cboYear.Enabled = blnFlag
'            dtpAssessdate.Enabled = blnFlag
'            radExternalAssessor.Enabled = blnFlag
'            radInternalAssessor.Enabled = blnFlag
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub FillEvaluationList()
'        Try
'            Dim blnFlag As Boolean = False
'            lvEvaluation.Items.Clear()
'            Dim lvItem As New ListViewItem
'            Dim StrGrpName As String = ""

'            RemoveHandler lvEvaluation.ItemSelectionChanged, AddressOf lvEvaluation_ItemSelectionChanged
'            lvEvaluation.GridLines = False
'            If mdtEvaluation.Rows.Count <= 0 Then Exit Sub
'            Dim mdecTotal As Decimal = 0 : Dim medecGrpTot As Decimal = 0

'            Dim mDicPerspective As New Dictionary(Of Integer, Integer)

'            For Each dRow As DataRow In mdtEvaluation.Rows

'                If mDicPerspective.ContainsKey(CInt(dRow.Item("perspectiveunkid"))) Then Continue For

'                mDicPerspective.Add(CInt(dRow.Item("perspectiveunkid")), CInt(dRow.Item("perspectiveunkid")))

'                Dim dtRow() As DataRow = mdtEvaluation.Select("perspectiveunkid = '" & CInt(dRow.Item("perspectiveunkid")) & "' AND AUD <> 'D'", "GrpId,objectiveunkid,kpiunkid,targetunkid,initiativeunkid")

'                If dtRow.Length > 0 Then
'                    medecGrpTot = 0
'                    blnFlag = True
'                    For i As Integer = 0 To dtRow.Length - 1
'                        lvItem = New ListViewItem
'                        If dtRow(i)("result").ToString.Trim.Length > 0 Then
'                            mdecTotal += CDec(dtRow(i)("result"))
'                            medecGrpTot += CDec(dtRow(i)("result"))
'                        End If
'                        lvItem.Text = dtRow(i).Item("group_name").ToString
'                        lvItem.SubItems.Add(dtRow(i).Item("objective").ToString)
'                        lvItem.SubItems.Add(dtRow(i).Item("kpi").ToString)
'                        lvItem.SubItems.Add(dtRow(i).Item("target").ToString)
'                        lvItem.SubItems.Add(dtRow(i).Item("initiative").ToString)
'                        lvItem.SubItems.Add(dtRow(i).Item("result").ToString)
'                        lvItem.SubItems.Add(dtRow(i).Item("remark").ToString)
'                        lvItem.SubItems.Add(dtRow(i).Item("objectiveunkid").ToString)
'                        lvItem.SubItems.Add(dtRow(i).Item("kpiunkid").ToString)
'                        lvItem.SubItems.Add(dtRow(i).Item("targetunkid").ToString)
'                        lvItem.SubItems.Add(dtRow(i).Item("initiativeunkid").ToString)
'                        lvItem.SubItems.Add(dtRow(i).Item("resultunkid").ToString)
'                        lvItem.SubItems.Add(dtRow(i).Item("GUID").ToString)
'                        lvItem.SubItems.Add(dtRow(i).Item("IsGrp").ToString)

'                        lvItem.Tag = dtRow(i).Item("analysistranunkid")

'                        lvEvaluation.Items.Add(lvItem)

'                        lvItem = Nothing

'                        StrGrpName = dtRow(i).Item("group_name").ToString
'                    Next
'                    If mdicGroupWiseTotal.ContainsKey(CInt(dRow("objectiveunkid"))) Then
'                        lvItem = New ListViewItem
'                        lvItem.UseItemStyleForSubItems = False
'                        lvItem.Text = StrGrpName
'                        lvItem.SubItems.Add("TOTAL : ", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
'                        lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
'                        lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
'                        lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
'                        lvItem.SubItems.Add(medecGrpTot.ToString, Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
'                        lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
'                        lvItem.SubItems.Add("-1")
'                        lvItem.SubItems.Add("-1")
'                        lvItem.SubItems.Add("-1")
'                        lvItem.SubItems.Add("-1")
'                        lvItem.SubItems.Add("-1")
'                        lvItem.SubItems.Add("-1")
'                        lvItem.SubItems.Add("FALSE")
'                        lvItem.Tag = "-1"

'                        lvEvaluation.Items.Add(lvItem)
'                    End If
'                End If
'            Next

'            If blnFlag = True Then
'                lvItem = New ListViewItem
'                lvItem.UseItemStyleForSubItems = False
'                lvItem.Text = ""
'                lvItem.SubItems.Add("GRAND TOTAL : ", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
'                lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
'                lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
'                lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
'                lvItem.SubItems.Add(mdecTotal.ToString, Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
'                lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
'                lvItem.SubItems.Add("-1")
'                lvItem.SubItems.Add("-1")
'                lvItem.SubItems.Add("-1")
'                lvItem.SubItems.Add("-1")
'                lvItem.SubItems.Add("-1")
'                lvItem.SubItems.Add("-1")
'                lvItem.SubItems.Add("FALSE")
'                lvItem.Tag = "-1"
'                lvEvaluation.Items.Add(lvItem)
'            End If


'            'For Each StrKey As Keys In mdicGroupWiseTotal.Keys
'            '    mdecTotal += CDec(mdicGroupWiseTotal(StrKey))
'            '    Dim dtRow() As DataRow = mdtEvaluation.Select("objectiveunkid = '" & StrKey & "' AND AUD <> 'D' ")
'            '    Dim StrGrpName As String = ""
'            '    If dtRow.Length > 0 Then
'            '        blnFlag = True
'            '        For i As Integer = 0 To dtRow.Length - 1
'            '            lvItem = New ListViewItem

'            '            lvItem.Text = dtRow(i).Item("group_name").ToString
'            '            lvItem.SubItems.Add(dtRow(i).Item("objective").ToString)
'            '            lvItem.SubItems.Add(dtRow(i).Item("kpi").ToString)
'            '            lvItem.SubItems.Add(dtRow(i).Item("target").ToString)
'            '            lvItem.SubItems.Add(dtRow(i).Item("initiative").ToString)
'            '            lvItem.SubItems.Add(dtRow(i).Item("result").ToString)
'            '            lvItem.SubItems.Add(dtRow(i).Item("remark").ToString)
'            '            lvItem.SubItems.Add(dtRow(i).Item("objectiveunkid").ToString)
'            '            lvItem.SubItems.Add(dtRow(i).Item("kpiunkid").ToString)
'            '            lvItem.SubItems.Add(dtRow(i).Item("targetunkid").ToString)
'            '            lvItem.SubItems.Add(dtRow(i).Item("initiativeunkid").ToString)
'            '            lvItem.SubItems.Add(dtRow(i).Item("resultunkid").ToString)
'            '            lvItem.SubItems.Add(dtRow(i).Item("GUID").ToString)

'            '            lvItem.Tag = dtRow(i).Item("analysistranunkid")

'            '            lvEvaluation.Items.Add(lvItem)

'            '            lvItem = Nothing

'            '            StrGrpName = dtRow(i).Item("group_name").ToString
'            '        Next
'            '        lvItem = New ListViewItem
'            '        lvItem.UseItemStyleForSubItems = False
'            '        lvItem.Text = StrGrpName
'            '        lvItem.SubItems.Add("TOTAL : ", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
'            '        lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
'            '        lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
'            '        lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
'            '        lvItem.SubItems.Add(mdicGroupWiseTotal(StrKey).ToString, Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
'            '        lvItem.SubItems.Add("", Color.White, Color.SteelBlue, New Font(Me.Font, FontStyle.Bold))
'            '        lvItem.SubItems.Add("-1")
'            '        lvItem.SubItems.Add("-1")
'            '        lvItem.SubItems.Add("-1")
'            '        lvItem.SubItems.Add("-1")
'            '        lvItem.SubItems.Add("-1")
'            '        lvItem.SubItems.Add("-1")
'            '        lvItem.Tag = "-1"

'            '        lvEvaluation.Items.Add(lvItem)
'            '    End If
'            'Next

'            'If blnFlag = True Then
'            '    lvItem = New ListViewItem
'            '    lvItem.UseItemStyleForSubItems = False
'            '    lvItem.Text = ""
'            '    lvItem.SubItems.Add("GRAND TOTAL : ", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
'            '    lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
'            '    lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
'            '    lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
'            '    lvItem.SubItems.Add(mdecTotal.ToString, Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
'            '    lvItem.SubItems.Add("", Color.White, Color.Maroon, New Font(Me.Font, FontStyle.Bold))
'            '    lvItem.SubItems.Add("-1")
'            '    lvItem.SubItems.Add("-1")
'            '    lvItem.SubItems.Add("-1")
'            '    lvItem.SubItems.Add("-1")
'            '    lvItem.SubItems.Add("-1")
'            '    lvItem.SubItems.Add("-1")
'            '    lvItem.Tag = "-1"
'            '    lvEvaluation.Items.Add(lvItem)
'            'End If

'            If lvEvaluation.Items.Count > 2 Then
'                colhRemark.Width = 170 - 20
'            Else
'                colhRemark.Width = 170
'            End If

'            lvEvaluation.GroupingColumn = objcolhPerspective
'            lvEvaluation.DisplayGroups(True)

'            AddHandler lvEvaluation.ItemSelectionChanged, AddressOf lvEvaluation_ItemSelectionChanged
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "FillEvaluationList", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub ResetEvaluation()
'        Try
'            cboObjective.SelectedValue = 0
'            txtRemark.Text = ""
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "ResetEvaluation", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Function Validation() As Boolean
'        Try
'            If CInt(cboEmployee.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Emplolyee is compulsory information.Please Select Emplolyee."), enMsgBoxStyle.Information)
'                cboEmployee.Select()
'                Return False
'            ElseIf CInt(cboYear.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Year is compulsory information.Please Select Year."), enMsgBoxStyle.Information)
'                cboYear.Select()
'                Return False
'            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), enMsgBoxStyle.Information)
'                cboPeriod.Select()
'                Return False
'            ElseIf CInt(cboPerspective.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Perspective is compulsory information. Please Select Perspective."), enMsgBoxStyle.Information)
'                cboPerspective.Focus()
'                Return False
'            ElseIf CInt(cboObjective.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Objective is compulsory information. Please Select Objective."), enMsgBoxStyle.Information)
'                cboObjective.Focus()
'                Return False
'            ElseIf CInt(cboResult.SelectedValue) = 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Result Code is compulsory information.Please Select Result Code."), enMsgBoxStyle.Information)
'                cboResult.Select()
'                Return False
'            ElseIf CInt(cboAssessor.SelectedValue) = 0 And menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Assessors is compulsory information.Please Select Assessors."), enMsgBoxStyle.Information)
'                cboAssessor.Select()
'                Return False
'            End If

'            Dim objPeriod As New clscommom_period_Tran
'            objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
'            If dtpAssessdate.Value.Date > objPeriod._End_Date Or dtpAssessdate.Value.Date < objPeriod._Start_Date Then
'                Dim strMsg As String = Language.getMessage(mstrModuleName, 8, "Assessment date should be in between ") & objPeriod._Start_Date & _
'                                       Language.getMessage(mstrModuleName, 9, " And ") & objPeriod._End_Date
'                eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
'                dtpAssessdate.Focus()
'                Return False
'            End If
'            objPeriod = Nothing

'            'S.SANDEEP [ 23 JAN 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
'            If menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
'                If CInt(cboReviewer.SelectedValue) <= 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Reviewer is mandatory information. Please create Reviewer first. Assessment -> Setups -> Reviewer."), enMsgBoxStyle.Information)
'                    cboReviewer.Focus()
'                    Return False
'                End If
'            End If
'            'S.SANDEEP [ 23 JAN 2012 ] -- END

'            'S.SANDEEP [ 04 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            If IsNumeric(cboResult.Text) = False Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, You cannot add this result as it is not a numeric number."), enMsgBoxStyle.Information)
'                cboResult.Focus()
'                Return False
'            End If
'            'S.SANDEEP [ 04 FEB 2012 ] -- END

'            Return True

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
'        Finally
'        End Try
'    End Function

'#End Region

'#Region " Form's Events "

'    Private Sub frmBSC_Evaluation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objBSCEvaluation = New clsBSC_Analysis_Master
'        objBSCTran = New clsBSC_analysis_tran
'        Try

'            Call Set_Logo(Me, gApplicationType)
'            Language.setLanguage(Me.Name)

'            Call OtherSettings()

'            Call SetVisibility()
'            Call SetColor()
'            Call FillCombo()
'            If menAction = enAction.EDIT_ONE Then
'                objBSCEvaluation._Analysisunkid = mintAssessAnalysisUnkid
'                cboEmployee.Enabled = False
'                objbtnSearchEmployee.Enabled = False
'                cboAssessor.Enabled = False
'                objbtnSearchAssessor.Enabled = False
'                cboPerspective.Enabled = False
'                radExternalAssessor.Enabled = False
'                radInternalAssessor.Enabled = False
'                cboReviewer.Enabled = False
'                objbtnSearchReviewer.Enabled = False
'            End If
'            Call GetValue()

'            objBSCTran._AnalysisUnkid = mintAssessAnalysisUnkid
'            mdtEvaluation = objBSCTran._DataTable
'            mdicGroupWiseTotal = objBSCTran._GrpWiseTotal
'            mdicItemWeight = objBSCTran._ItemWeight
'            iCnt = objBSCTran._Grp_Counter
'            Call FillEvaluationList()

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmBSC_Evaluation_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmBSC_Evaluation_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
'        Try
'            If e.Control = True And e.KeyCode = Keys.S Then
'                btnSave_Click(sender, e)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmBSC_Evaluation_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmBSC_Evaluation_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
'        Try
'            If Asc(e.KeyChar) = 13 AndAlso txtRemark.Focused = False Then
'                SendKeys.Send("{TAB}")
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmBSC_Evaluation_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmBSC_Evaluation_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
'        objBSCEvaluation = Nothing : objBSCTran = Nothing
'    End Sub

'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            clsBSC_Analysis_Master.SetMessages()
'            objfrm._Other_ModuleNames = "clsBSC_Analysis_Master"
'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub

'#End Region

'#Region " Button's Events "

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSaveCommit.Click
'        Dim blnFlag As Boolean = False
'        Try
'            If lvEvaluation.Items.Count <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please add atleast one evaluation in order to save."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If


'            Call SetValue()

'            Select Case CType(sender, eZee.Common.eZeeLightButton).Name.ToUpper
'                Case "BTNSAVECOMMIT"
'                    If ConfigParameter._Object._IsAllowFinalSave = False Then
'                        If isAll_Assessed() = False Then Exit Sub
'                    ElseIf ConfigParameter._Object._IsAllowFinalSave = True Then
'                        If isAll_Assessed(False) = False Then
'                            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
'                                Exit Sub
'                            End If
'                        End If
'                    End If
'                    objBSCEvaluation._Iscommitted = True
'                    'S.SANDEEP [ 14 JUNE 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    objBSCEvaluation._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime
'                    'S.SANDEEP [ 14 JUNE 2012 ] -- END
'            End Select

'            If menAction = enAction.EDIT_ONE Then
'                blnFlag = objBSCEvaluation.Update(mdtEvaluation)
'            Else
'                blnFlag = objBSCEvaluation.Insert(mdtEvaluation)
'            End If

'            If blnFlag = False And objBSCEvaluation._Message <> "" Then
'                eZeeMsgBox.Show(objBSCEvaluation._Message, enMsgBoxStyle.Information)
'            End If

'            If blnFlag Then
'                mblnCancel = False
'                If menAction = enAction.ADD_CONTINUE Then
'                    objBSCEvaluation = Nothing
'                    objBSCEvaluation = New clsBSC_Analysis_Master
'                    Call GetValue()
'                    Call DoOperation(True)
'                    mdtEvaluation.Rows.Clear()
'                    Call FillEvaluationList()
'                    mdicGroupWiseTotal.Clear()
'                    mdicItemWeight.Clear()
'                Else
'                    Me.Close()
'                End If
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub btnAddEvaluation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddEvaluation.Click
'        Try

'            If Validation() = False Then Exit Sub

'            Select Case menAssess
'                Case enAssessmentMode.SELF_ASSESSMENT
'                    If objBSCEvaluation.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), , , mintAssessAnalysisUnkid) = True Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    End If
'                Case enAssessmentMode.APPRAISER_ASSESSMENT
'                    If objBSCEvaluation.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessor.SelectedValue), , mintAssessAnalysisUnkid, radExternalAssessor.Checked) = True Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    End If
'                Case enAssessmentMode.REVIEWER_ASSESSMENT
'                    If objBSCEvaluation.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue), , CInt(cboReviewer.SelectedValue), mintAssessAnalysisUnkid) = True Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    End If
'            End Select

'            'S.SANDEEP [ 05 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'Dim dtTemp() As DataRow = mdtEvaluation.Select("objectiveunkid = '" & CInt(cboObjective.SelectedValue) & "'")
'            Dim dtTemp() As DataRow = mdtEvaluation.Select("objectiveunkid = '" & CInt(cboObjective.SelectedValue) & "' AND AUD <> 'D'")
'            'S.SANDEEP [ 05 MARCH 2012 ] -- END

'            If dtTemp.Length > 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot add same information again in the list."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            If mdicGroupWiseTotal.ContainsKey(CInt(cboObjective.SelectedValue)) Then
'                If IsNumeric(cboResult.Text) = True Then
'                    If (mdicGroupWiseTotal(CInt(cboObjective.SelectedValue)) + CDec(cboResult.Text)) > mdicItemWeight(CInt(cboObjective.SelectedValue)) Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item group."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    End If
'                    mdicGroupWiseTotal(CInt(cboObjective.SelectedValue)) = (mdicGroupWiseTotal(CInt(cboObjective.SelectedValue)) + CDec(cboResult.Text))
'                End If
'            Else
'                If IsNumeric(cboResult.Text) = True Then
'                    If CDec(cboResult.Text) > mdicItemWeight(CInt(cboObjective.SelectedValue)) Then
'                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot add this result as it exceeds the weight assigned to particular item group."), enMsgBoxStyle.Information)
'                        Exit Sub
'                    End If
'                    mdicGroupWiseTotal.Add(CInt(cboObjective.SelectedValue), CDec(cboResult.Text))
'                End If
'            End If



'            'S.SANDEEP [ 05 MARCH 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'Dim dTemp() As DataRow = mdtEvaluation.Select("perspectiveunkid = '" & CInt(cboPerspective.SelectedValue) & "' ")
'            Dim dTemp() As DataRow = mdtEvaluation.Select("perspectiveunkid = '" & CInt(cboPerspective.SelectedValue) & "' AND AUD <> 'D'")
'            'S.SANDEEP [ 05 MARCH 2012 ] -- END

'            If dTemp.Length <= 0 Then
'                iCnt += 1
'            End If

'            Dim dRow As DataRow = mdtEvaluation.NewRow

'            dRow.Item("analysistranunkid") = -1
'            dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
'            dRow.Item("objectiveunkid") = CInt(cboObjective.SelectedValue)
'            dRow.Item("resultunkid") = CInt(cboResult.SelectedValue)
'            dRow.Item("remark") = txtRemark.Text
'            dRow.Item("AUD") = "A"
'            dRow.Item("GUID") = Guid.NewGuid.ToString
'            dRow.Item("isvoid") = False
'            dRow.Item("voiduserunkid") = -1
'            dRow.Item("voiddatetime") = DBNull.Value
'            dRow.Item("voidreason") = ""
'            dRow.Item("objective") = cboObjective.Text & "[ " & txtWeightage.Text & " ]"

'            If CInt(cboResult.SelectedValue) > 0 Then
'                dRow.Item("result") = cboResult.Text
'            Else
'                dRow.Item("result") = ""
'            End If

'            dRow.Item("group_name") = cboPerspective.Text
'            dRow.Item("perspectiveunkid") = cboPerspective.SelectedValue
'            dRow.Item("IsGrp") = True
'            dRow.Item("GrpId") = cboPerspective.SelectedValue

'            mdtEvaluation.Rows.Add(dRow)

'            Dim mdTable As DataTable = objBSCEvaluation.GetBSCPlanning(CInt(cboObjective.SelectedValue))

'            For Each dMRow As DataRow In mdTable.Rows
'                dRow = mdtEvaluation.NewRow

'                dRow.Item("analysistranunkid") = -1
'                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
'                dRow.Item("objectiveunkid") = CInt(cboObjective.SelectedValue)
'                dRow.Item("resultunkid") = CInt(cboResult.SelectedValue)
'                dRow.Item("remark") = ""
'                dRow.Item("AUD") = "A"
'                dRow.Item("GUID") = Guid.NewGuid.ToString
'                dRow.Item("isvoid") = False
'                dRow.Item("voiduserunkid") = -1
'                dRow.Item("voiddatetime") = DBNull.Value
'                dRow.Item("voidreason") = ""
'                dRow.Item("objective") = ""
'                dRow.Item("kpiunkid") = dMRow.Item("KPIUnkid")
'                dRow.Item("targetunkid") = dMRow.Item("TargetUnkid")
'                dRow.Item("initiativeunkid") = dMRow.Item("InitiativeUnkid")
'                dRow.Item("kpi") = dMRow.Item("KPI")
'                dRow.Item("target") = dMRow.Item("Target")
'                dRow.Item("initiative") = dMRow.Item("Initiative")
'                dRow.Item("group_name") = cboPerspective.Text
'                dRow.Item("perspectiveunkid") = cboPerspective.SelectedValue
'                dRow.Item("IsGrp") = False
'                dRow.Item("GrpId") = cboPerspective.SelectedValue
'                mdtEvaluation.Rows.Add(dRow)
'            Next

'            Call FillEvaluationList()
'            Call ResetEvaluation()
'            Call DoOperation(False)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnAddEvaluation_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnEditEvaluation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditEvaluation.Click
'        Try
'            If lvEvaluation.SelectedItems.Count > 0 Then
'                Dim drTemp As DataRow()
'                If CInt(lvEvaluation.SelectedItems(0).Tag) = -1 Then
'                    drTemp = mdtEvaluation.Select("GUID = '" & lvEvaluation.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
'                Else
'                    drTemp = mdtEvaluation.Select("analysistranunkid = '" & CInt(lvEvaluation.SelectedItems(0).Tag) & "'")
'                End If
'                If drTemp.Length > 0 Then
'                    If IsNumeric(cboResult.Text) Then
'                        If mdecRemaininResult + CDec(cboResult.Text) > mdicItemWeight(CInt(drTemp(0)("objectiveunkid"))) Then
'                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot set this result as it exceeds the weight assigned to particular item group."), enMsgBoxStyle.Information)
'                            Exit Sub
'                        End If
'                        mdicGroupWiseTotal(CInt(cboObjective.SelectedValue)) = mdecRemaininResult + CDec(cboResult.Text)
'                    End If
'                End If



'                'S.SANDEEP [ 05 MARCH 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                'Dim dtTemp() As DataRow = mdtEvaluation.Select("objectiveunkid = '" & CInt(cboObjective.SelectedValue) & "' AND GUID <> '" & lvEvaluation.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "' AND isGrp = True")
'                Dim dtTemp() As DataRow = mdtEvaluation.Select("objectiveunkid = '" & CInt(cboObjective.SelectedValue) & "' AND GUID <> '" & lvEvaluation.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "' AND isGrp = True AND AUD <> 'D'")
'                'S.SANDEEP [ 05 MARCH 2012 ] -- END

'                If dtTemp.Length > 0 Then
'                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot add same item again in the list."), enMsgBoxStyle.Information)
'                    Exit Sub
'                End If

'                If Validation() = False Then Exit Sub

'                If drTemp.Length > 0 Then
'                    With drTemp(0)
'                        .Item("analysistranunkid") = lvEvaluation.SelectedItems(0).Tag
'                        .Item("analysisunkid") = mintAssessAnalysisUnkid
'                        .Item("objectiveunkid") = CInt(cboObjective.SelectedValue)
'                        '.Item("kpiunkid") = CInt(cboKPI.SelectedValue)
'                        '.Item("targetunkid") = CInt(cboTargets.SelectedValue)
'                        '.Item("initiativeunkid") = CInt(cboInitiative.SelectedValue)
'                        .Item("resultunkid") = CInt(cboResult.SelectedValue)
'                        .Item("remark") = txtRemark.Text
'                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
'                            .Item("AUD") = "U"
'                        End If
'                        .Item("GUID") = Guid.NewGuid.ToString
'                        .Item("isvoid") = False
'                        .Item("voiduserunkid") = -1
'                        .Item("voiddatetime") = DBNull.Value
'                        .Item("voidreason") = ""
'                        .Item("objective") = cboObjective.Text & "[ " & txtWeightage.Text & " ]"
'                        '.Item("kpi") = cboKPI.Text
'                        'If CInt(cboTargets.SelectedValue) > 0 Then
'                        '    .Item("target") = cboTargets.Text
'                        'Else
'                        '    .Item("target") = ""
'                        'End If

'                        'If CInt(cboInitiative.SelectedValue) > 0 Then
'                        '    .Item("initiative") = cboInitiative.Text
'                        'Else
'                        '    .Item("initiative") = ""
'                        'End If

'                        If CInt(cboResult.SelectedValue) > 0 Then
'                            .Item("result") = cboResult.Text
'                        Else
'                            .Item("result") = ""
'                        End If

'                        .Item("group_name") = cboPerspective.Text
'                        .Item("perspectiveunkid") = cboPerspective.SelectedValue
'                        .AcceptChanges()
'                    End With
'                    Call FillEvaluationList()
'                End If
'                Call ResetEvaluation()
'            End If
'            cboPerspective.Enabled = True : cboObjective.Enabled = True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnEditEvaluation_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnDeleteEvaluation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteEvaluation.Click
'        Try
'            If lvEvaluation.SelectedItems.Count > 0 Then
'                Dim drTemp As DataRow()
'                If CInt(lvEvaluation.SelectedItems(0).Tag) = -1 Then
'                    drTemp = mdtEvaluation.Select("GUID = '" & lvEvaluation.SelectedItems(0).SubItems(objcolhGUID.Index).Text & "'")
'                Else
'                    drTemp = mdtEvaluation.Select("analysistranunkid = '" & CInt(lvEvaluation.SelectedItems(0).Tag) & "'")
'                End If

'                If drTemp.Length > 0 Then
'                    If CInt(lvEvaluation.SelectedItems(0).Tag) > 0 Then
'                        Dim frm As New frmReasonSelection
'                        If User._Object._Isrighttoleft = True Then
'                            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                            frm.RightToLeftLayout = True
'                            Call Language.ctlRightToLeftlayOut(frm)
'                        End If
'                        Dim mstrVoidReason As String = String.Empty
'                        frm.displayDialog(enVoidCategoryType.ASSESSMENT, mstrVoidReason)
'                        If mstrVoidReason.Length <= 0 Then
'                            Exit Sub
'                        Else
'                            drTemp(0).Item("AUD") = "D"
'                            drTemp(0).Item("isvoid") = True
'                            drTemp(0).Item("voiduserunkid") = User._Object._Userunkid
'                            drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
'                            drTemp(0).Item("voidreason") = mstrVoidReason
'                        End If
'                    Else
'                        Dim dDelRow() As DataRow = mdtEvaluation.Select("objectiveunkid='" & CInt(drTemp(0)("objectiveunkid")) & "'")
'                        For i As Integer = 0 To dDelRow.Length - 1
'                            'drTemp(0).Item("AUD") = "D"
'                            dDelRow(i).Item("AUD") = "D"
'                        Next
'                    End If
'                    mdtEvaluation.AcceptChanges()
'                    If mdicGroupWiseTotal.ContainsKey(CInt(drTemp(0)("objectiveunkid"))) Then
'                        mdicGroupWiseTotal(CInt(drTemp(0)("objectiveunkid"))) = mdecRemaininResult
'                    End If
'                    Call FillEvaluationList()
'                End If
'                Call ResetEvaluation()
'            End If
'            If lvEvaluation.Items.Count <= 0 Then
'                Call DoOperation(True)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnDeleteEvaluation_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Controls Events "

'    Private Sub cboAssessor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
'        Try
'            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
'            If CInt(cboAssessor.SelectedValue) > 0 Then
'                Dim dsList As New DataSet
'                If radInternalAssessor.Checked = True Then
'                    dsList = objBSCEvaluation.getEmployeeBasedAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
'                ElseIf radExternalAssessor.Checked = True Then
'                    Dim objExtAssessor As New clsexternal_assessor_master
'                    dsList = objExtAssessor.GetEmpBasedOnExtAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
'                End If
'                With cboEmployee
'                    .ValueMember = "Id"
'                    .DisplayMember = "Name"
'                    .DataSource = dsList.Tables("AEmp")
'                    .SelectedValue = 0
'                End With
'            Else
'                cboEmployee.DataSource = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboAssessor_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub radInternalAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radInternalAssessor.CheckedChanged
'        Try
'            If radInternalAssessor.Checked = True Then
'                cboAssessor.DataSource = Nothing
'                Dim dsCombos As DataSet = objBSCEvaluation.getAssessorComboList("Assessor", True)
'                With cboAssessor
'                    .ValueMember = "Id"
'                    .DisplayMember = "Name"
'                    .DataSource = dsCombos.Tables("Assessor")
'                    .SelectedValue = 0
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "radInternalAssessor_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub radExternalAssessor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radExternalAssessor.CheckedChanged
'        Try
'            If radExternalAssessor.Checked = True Then
'                cboAssessor.DataSource = Nothing
'                Dim objExtAssessor As New clsexternal_assessor_master
'                Dim dsCombos As DataSet = objExtAssessor.GetDisplayNameComboList("Assessor", True)
'                With cboAssessor
'                    .ValueMember = "Id"
'                    .DisplayMember = "Name"
'                    .DataSource = dsCombos.Tables("Assessor")
'                    .SelectedValue = 0
'                End With
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "radExternalAssessor_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub cboYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
'        Try
'            If CInt(cboYear.SelectedValue) > 0 Then
'                Dim dsList As New DataSet
'                Dim objPeriod As New clscommom_period_Tran
'                dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), "APeriod", True, 1)
'                With cboPeriod
'                    .ValueMember = "periodunkid"
'                    .DisplayMember = "name"
'                    .DataSource = dsList.Tables("APeriod")
'                    .SelectedValue = 0
'                End With
'            Else
'                cboPeriod.DataSource = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            If cboEmployee.DataSource IsNot Nothing Then
'                frm.ValueMember = cboEmployee.ValueMember
'                frm.DisplayMember = cboEmployee.DisplayMember
'                Select Case menAssess
'                    Case enAssessmentMode.SELF_ASSESSMENT
'                        frm.CodeMember = "employeecode"
'                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT
'                        frm.CodeMember = "Code"
'                End Select
'                frm.DataSource = CType(cboEmployee.DataSource, DataTable)
'                If frm.DisplayDialog Then
'                    cboEmployee.SelectedValue = frm.SelectedValue
'                    cboEmployee.Focus()
'                End If
'            End If


'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnSearchAssessor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAssessor.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            frm.ValueMember = cboAssessor.ValueMember
'            frm.DisplayMember = cboAssessor.DisplayMember
'            frm.CodeMember = "Code"
'            frm.DataSource = CType(cboAssessor.DataSource, DataTable)
'            If frm.DisplayDialog Then
'                cboAssessor.SelectedValue = frm.SelectedValue
'                cboAssessor.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchAssessor_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub objbtnSearchResult_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchResult.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If cboResult.DataSource IsNot Nothing Then
'                With frm
'                    .ValueMember = cboResult.ValueMember
'                    .DisplayMember = cboResult.DisplayMember
'                    .DataSource = CType(cboResult.DataSource, DataTable)
'                End With
'                If frm.DisplayDialog Then
'                    cboResult.SelectedValue = frm.SelectedValue
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchResult_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub cboObjective_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboObjective.SelectedIndexChanged
'        Try
'            If CInt(cboObjective.SelectedValue) > 0 Then
'                Dim objObjective As New clsObjective_Master
'                Dim dsList As New DataSet
'                Dim objResult As New clsresult_master
'                objObjective._Objectiveunkid = CInt(cboObjective.SelectedValue)

'                If mdicItemWeight.ContainsKey(CInt(cboObjective.SelectedValue)) = False Then
'                    mdicItemWeight.Add(CInt(cboObjective.SelectedValue), CDec(objObjective._Weight))
'                End If

'                txtWeightage.Text = CStr(objObjective._Weight)

'                dsList = objResult.getComboList("List", True, objObjective._ResultGroupunkid)
'                With cboResult
'                    .ValueMember = "Id"
'                    .DisplayMember = "Name"
'                    .DataSource = dsList.Tables("List")
'                End With
'                objObjective = Nothing : dsList.Dispose() : objResult = Nothing
'                lnkBSCPlanning.Enabled = True
'            Else
'                cboResult.DataSource = Nothing : txtWeightage.Text = ""
'                lnkBSCPlanning.Enabled = False
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboObjective_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub cboPerspective_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPerspective.SelectedIndexChanged
'        Try
'            'If CInt(cboPerspective.SelectedValue) > 0 Then
'            Dim dsList As New DataSet : Dim objObjective As New clsObjective_Master
'            'S.SANDEEP [ 23 JAN 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'dsList = objObjective.getComboList("List", True, CInt(cboPerspective.SelectedValue))
'            dsList = objObjective.getComboList("List", True, CInt(cboPerspective.SelectedValue), CInt(cboEmployee.SelectedValue))
'            'S.SANDEEP [ 23 JAN 2012 ] -- END
'            With cboObjective
'                .ValueMember = "id"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("List")
'                .SelectedValue = 0
'            End With
'            'End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboPerspective_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnSearchObjective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchObjective.Click
'        Dim frm As New frmCommonSearch
'        Try
'            With frm
'                .ValueMember = cboObjective.ValueMember
'                .DisplayMember = cboObjective.DisplayMember
'                .DataSource = CType(cboObjective.DataSource, DataTable)
'            End With

'            If frm.DisplayDialog Then
'                cboObjective.SelectedValue = frm.SelectedValue
'                cboObjective.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchObjective_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub

'    Private Sub lvEvaluation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvEvaluation.Click
'        Try
'            If lvEvaluation.SelectedItems.Count > 0 Then
'                cboObjective.SelectedValue = 0
'                cboPerspective.Text = lvEvaluation.SelectedItems(0).SubItems(objcolhPerspective.Index).Text
'                cboObjective.SelectedValue = lvEvaluation.SelectedItems(0).SubItems(objcolhobjectiveId.Index).Text
'                cboResult.SelectedValue = lvEvaluation.SelectedItems(0).SubItems(objcolhResultId.Index).Text
'                txtRemark.Text = lvEvaluation.SelectedItems(0).SubItems(colhRemark.Index).Text
'                If mdicGroupWiseTotal.ContainsKey(CInt(cboObjective.SelectedValue)) Then
'                    If IsNumeric(cboResult.Text) Then
'                        mdecRemaininResult = CDec(mdicGroupWiseTotal(CInt(cboObjective.SelectedValue)) - CDec(cboResult.Text))
'                    End If
'                End If
'                cboPerspective.Enabled = False : cboObjective.Enabled = False
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvEvaluation_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub lvEvaluation_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lvEvaluation.ItemSelectionChanged
'        Try
'            If lvEvaluation.SelectedItems.Count > 0 Then
'                If e.Item.SubItems(objcolhIsGrp.Index).Text.ToUpper = "FALSE" Then
'                    e.Item.Selected = False
'                    cboPerspective.Enabled = True : cboObjective.Enabled = True
'                End If
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lvEvaluation_ItemSelectionChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub lnkViewBSC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewBSC.Click
'        Try
'            If CInt(cboEmployee.SelectedValue) <= 0 Or _
'               CInt(cboYear.SelectedValue) <= 0 Or _
'               CInt(cboPeriod.SelectedValue) <= 0 Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Please set following information [Employee,Year,Period and Group] to view assessment."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            Dim frm As New frmCommonBSC_View

'            frm.displayDialog(cboEmployee.Text, CInt(cboEmployee.SelectedValue), CInt(cboYear.SelectedValue), CInt(cboPeriod.SelectedValue))

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lnkViewBSC_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    'S.SANDEEP [ 23 JAN 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
'        Try
'            Dim dsList As New DataSet : Dim objObjective As New clsObjective_Master
'            dsList = objObjective.getComboList("List", True, CInt(cboPerspective.SelectedValue), CInt(cboEmployee.SelectedValue))
'            With cboObjective
'                .ValueMember = "id"
'                .DisplayMember = "name"
'                .DataSource = dsList.Tables("List")
'                .SelectedValue = 0
'            End With
'            cboPerspective.SelectedValue = 0
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub lnkBSCPlanning_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkBSCPlanning.LinkClicked
'        Try
'            Dim frm As New frmBSC_Planning

'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            frm.displayDialog(CInt(cboObjective.SelectedValue), cboObjective.Text)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "lnkBSCPlanning_LinkClicked", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub cboReviewer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReviewer.SelectedIndexChanged
'        Try
'            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
'            If CInt(cboReviewer.SelectedValue) > 0 Then
'                Dim dsList As New DataSet
'                dsList = objBSCEvaluation.getEmployeeBasedAssessor(CInt(cboReviewer.SelectedValue), "REmp", True)
'                With cboEmployee
'                    .ValueMember = "Id"
'                    .DisplayMember = "Name"
'                    .DataSource = dsList.Tables("REmp")
'                    .SelectedValue = 0
'                End With
'            Else
'                cboEmployee.DataSource = Nothing
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "cboReviewer_SelectedIndexChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub objbtnSearchReviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchReviewer.Click
'        Dim frm As New frmCommonSearch
'        Try
'            If User._Object._Isrighttoleft = True Then
'                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                frm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(frm)
'            End If

'            frm.ValueMember = cboReviewer.ValueMember
'            frm.DisplayMember = cboReviewer.DisplayMember
'            frm.CodeMember = "Code"
'            frm.DataSource = CType(cboReviewer.DataSource, DataTable)
'            If frm.DisplayDialog Then
'                cboReviewer.SelectedValue = frm.SelectedValue
'                cboReviewer.Focus()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objbtnSearchReviewer_Click", mstrModuleName)
'        Finally
'            If frm IsNot Nothing Then frm.Dispose()
'        End Try
'    End Sub
'    'S.SANDEEP [ 23 JAN 2012 ] -- END

'#End Region


'End Class
'S.SANDEEP [ 28 DEC 2012 ] -- END