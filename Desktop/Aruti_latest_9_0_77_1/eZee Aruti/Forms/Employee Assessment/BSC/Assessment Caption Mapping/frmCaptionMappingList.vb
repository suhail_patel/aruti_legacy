﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCaptionMappingList

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmCaptionMappingList"
    Private objMapping As clsAssess_Field_Mapping

#End Region

#Region " Private Methods "

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Try
            dsList = objMapping.GetList("List")
            lvMapping.Items.Clear()
            For Each dRow As DataRow In dsList.Tables(0).Rows
                Dim lvitem As New ListViewItem

                lvitem.Text = dRow.Item("fperiod").ToString
                lvitem.SubItems.Add(dRow.Item("fcaption").ToString)
                lvitem.SubItems.Add(dRow.Item("periodunkid").ToString)
                lvitem.SubItems.Add(dRow.Item("statusid").ToString)
                lvitem.SubItems.Add(dRow.Item("weight").ToString)

                If CInt(dRow.Item("statusid")) = enStatusType.Close Then
                    lvitem.ForeColor = Color.Gray
                End If

                lvitem.Tag = dRow.Item("mappingunkid")

                lvMapping.Items.Add(lvitem)
            Next

            If lvMapping.Items.Count >= 2 Then
                colhFieldText.Width = 410 - 20
            Else
                colhFieldText.Width = 410
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            btnNew.Enabled = User._Object.Privilege._AllowtoAddBSCTitlesMapping
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditBSCTitlesMapping
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteBSCTitlesMapping
            'S.SANDEEP [28 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmCaptionMappingList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objMapping = New clsAssess_Field_Mapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call Fill_List()
            If lvMapping.Items.Count > 0 Then lvMapping.Items(0).Selected = True
            lvMapping.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCaptionMappingList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCaptionMappingList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objMapping = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssess_Field_Mapping.SetMessages()
            objfrm._Other_ModuleNames = "clsAssess_Field_Mapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmCaptionMappingAddEdit
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmCaptionMappingAddEdit
        Try
            If lvMapping.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Mapping from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvMapping.Select()
                Exit Sub
            End If

            If objMapping.isUsed(CInt(lvMapping.SelectedItems(0).Tag), True) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot edit this mapping. Reason, It is already linked with some transaction."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If frm.displayDialog(CInt(lvMapping.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call Fill_List()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If lvMapping.SelectedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Mapping from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvMapping.Select()
                Exit Sub
            End If

            If objMapping.isUsed(CInt(lvMapping.SelectedItems(0).Tag)) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete this mapping. Reason, It is already linked with some transaction."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Mapping?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objMapping._FormName = mstrModuleName
                objMapping._LoginEmployeeunkid = 0
                objMapping._ClientIP = getIP()
                objMapping._HostName = getHostName()
                objMapping._FromWeb = False
                objMapping._AuditUserId = User._Object._Userunkid
objMapping._CompanyUnkid = Company._Object._Companyunkid
                objMapping._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                If objMapping.Delete(CInt(lvMapping.SelectedItems(0).Tag), User._Object._Userunkid) = True Then
                    lvMapping.SelectedItems(0).Remove()
                End If
            End If
            lvMapping.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub lvMapping_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvMapping.SelectedIndexChanged
        Try
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            Call SetVisibility()
            'Shani(15-Feb-2016) -- End

            If lvMapping.SelectedItems.Count > 0 Then
                If lvMapping.SelectedItems(0).ForeColor = Color.Gray Then
                    btnEdit.Enabled = False : btnDelete.Enabled = False
                    'Shani(15-Feb-2016) -- Start
                    'Report not showing Data in ESS due to Access Level Filter
                    'Else
                    'btnEdit.Enabled = True : btnDelete.Enabled = True
                    'Shani(15-Feb-2016) -- End
                End If
            End If

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            ''S.SANDEEP [28 MAY 2015] -- START
            ''ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'Call SetVisibility()
            ''S.SANDEEP [28 MAY 2015] -- END
            'Shani(15-Feb-2016) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMapping_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhPeriod.Text = Language._Object.getCaption(CStr(Me.colhPeriod.Tag), Me.colhPeriod.Text)
            Me.colhFieldText.Text = Language._Object.getCaption(CStr(Me.colhFieldText.Tag), Me.colhFieldText.Text)
            Me.colhWeight.Text = Language._Object.getCaption(CStr(Me.colhWeight.Tag), Me.colhWeight.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Mapping from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Mapping?")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot delete this mapping. Reason, It is already linked with some transaction.")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot edit this mapping. Reason, It is already linked with some transaction.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class