﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmCaptionMappingAddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCaptionMappingAddEdit"
    Private objMapping As clsAssess_Field_Mapping
    Private mintMappingUnkid As Integer = 0
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintMappingUnkid = intUnkId
            menAction = eAction
            Call Set_Caption()
            Me.ShowDialog()

            intUnkId = mintMappingUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboPeriod.BackColor = GUI.ColorComp
            txtWeight.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Set_Caption()
        Try
            Dim dsLink As New DataSet : Dim objFieldMaster As New clsAssess_Field_Master
            dsLink = objFieldMaster.Get_Field_Mapping("List", , True)
            If dsLink.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsLink.Tables(0).Rows.Count - 1
                    Select Case i + 1
                        Case 1
                            objradField1.Text = dsLink.Tables(0).Rows(i).Item("fieldcaption").ToString
                            If objradField1.Text.Trim.Length > 0 Then objradField1.Enabled = True
                            objradField1.Tag = CInt(dsLink.Tables(0).Rows(i).Item("fieldunkid"))
                        Case 2
                            objradField2.Text = dsLink.Tables(0).Rows(i).Item("fieldcaption").ToString
                            If objradField2.Text.Trim.Length > 0 Then objradField2.Enabled = True
                            objradField2.Tag = CInt(dsLink.Tables(0).Rows(i).Item("fieldunkid"))
                        Case 3
                            objradField3.Text = dsLink.Tables(0).Rows(i).Item("fieldcaption").ToString
                            If objradField3.Text.Trim.Length > 0 Then objradField3.Enabled = True
                            objradField3.Tag = CInt(dsLink.Tables(0).Rows(i).Item("fieldunkid"))
                        Case 4
                            objradField4.Text = dsLink.Tables(0).Rows(i).Item("fieldcaption").ToString
                            If objradField4.Text.Trim.Length > 0 Then objradField4.Enabled = True
                            objradField4.Tag = CInt(dsLink.Tables(0).Rows(i).Item("fieldunkid"))
                        Case 5
                            objradField5.Text = dsLink.Tables(0).Rows(i).Item("fieldcaption").ToString
                            If objradField5.Text.Trim.Length > 0 Then objradField5.Enabled = True
                            objradField5.Tag = CInt(dsLink.Tables(0).Rows(i).Item("fieldunkid"))
                    End Select
                Next
            End If
            objFieldMaster = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Caption", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'cboPeriod.SelectedValue = objMapping._Periodunkid
            'txtWeight.Decimal = objMapping._Weight
            'Select Case objMapping._Fieldunkid
            '    Case CInt(objradField1.Tag)
            '        objradField1.Checked = True
            '    Case CInt(objradField2.Tag)
            '        objradField2.Checked = True
            '    Case CInt(objradField3.Tag)
            '        objradField3.Checked = True
            '    Case CInt(objradField4.Tag)
            '        objradField4.Checked = True
            '    Case CInt(objradField5.Tag)
            '        objradField5.Checked = True
            'End Select
            If menAction = enAction.EDIT_ONE Then
            cboPeriod.SelectedValue = objMapping._Periodunkid
            txtWeight.Decimal = objMapping._Weight
            Select Case objMapping._Fieldunkid
                Case CInt(objradField1.Tag)
                    objradField1.Checked = True
                Case CInt(objradField2.Tag)
                    objradField2.Checked = True
                Case CInt(objradField3.Tag)
                    objradField3.Checked = True
                Case CInt(objradField4.Tag)
                    objradField4.Checked = True
                Case CInt(objradField5.Tag)
                    objradField5.Checked = True
            End Select
            End If
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objMapping._Periodunkid = CInt(cboPeriod.SelectedValue)
            objMapping._Weight = txtWeight.Decimal
            objMapping._Userunkid = User._Object._Userunkid
            If objradField1.Checked = True Then
                objMapping._Fieldunkid = CInt(objradField1.Tag)
            ElseIf objradField2.Checked = True Then
                objMapping._Fieldunkid = CInt(objradField2.Tag)
            ElseIf objradField3.Checked = True Then
                objMapping._Fieldunkid = CInt(objradField3.Tag)
            ElseIf objradField4.Checked = True Then
                objMapping._Fieldunkid = CInt(objradField4.Tag)
            ElseIf objradField5.Checked = True Then
                objMapping._Fieldunkid = CInt(objradField5.Tag)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If txtWeight.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Weight is mandatory informatiopn. Please provide weight to contine."), enMsgBoxStyle.Information)
                txtWeight.Focus()
                Return False
            End If

            If objradField1.Checked = False AndAlso objradField2.Checked = False AndAlso objradField3.Checked = False AndAlso objradField4.Checked = False AndAlso objradField5.Checked = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please check atleast one of the master to link the informational fields."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Fill_Combo()
        Dim dsCombo As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Combo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            objradField1.Checked = False
            objradField2.Checked = False
            objradField3.Checked = False
            objradField4.Checked = False
            objradField5.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Clear_Controls", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmCaptionMappingAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objMapping = New clsAssess_Field_Mapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            If menAction = enAction.EDIT_ONE Then
                objMapping._Mappingunkid = mintMappingUnkid
            End If
            Call Fill_Combo()
            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCaptionMappingAddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCaptionMappingAddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCaptionMappingAddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCaptionMappingAddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCaptionMappingAddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAssess_Field_Mapping.SetMessages()
            objfrm._Other_ModuleNames = "clsAssess_Field_Mapping"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub
            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objMapping._FormName = mstrModuleName
            objMapping._LoginEmployeeunkid = 0
            objMapping._ClientIP = getIP()
            objMapping._HostName = getHostName()
            objMapping._FromWeb = False
            objMapping._AuditUserId = User._Object._Userunkid
objMapping._CompanyUnkid = Company._Object._Companyunkid
            objMapping._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objMapping.Update()
            Else
                blnFlag = objMapping.Insert()
            End If

            If blnFlag = False AndAlso objMapping._Message <> "" Then
                eZeeMsgBox.Show(objMapping._Message, enMsgBoxStyle.Information)
            End If
            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objMapping = Nothing
                    objMapping = New clsAssess_Field_Mapping
                    cboPeriod.Tag = CInt(cboPeriod.SelectedValue)
                    Call GetValue()
                    cboPeriod.SelectedValue = CInt(cboPeriod.Tag)
                    cboPeriod.Select()
                    Call Clear_Controls()
                Else
                    mintMappingUnkid = objMapping._Mappingunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMapInformationalField.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMapInformationalField.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbMapInformationalField.Text = Language._Object.getCaption(Me.gbMapInformationalField.Name, Me.gbMapInformationalField.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 2, "Please check atleast one of the master to link the informational fields.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class

'Public Class frmCaptionMapping1

'#Region " Private Variable "

'    Private ReadOnly mstrModuleName As String = "frmCaptionMapping"
'    Private mblnCancel As Boolean = True
'    Private mintFieldUnkid As Integer = 0
'    Private mintNewFieldUnkid As Integer = 0
'    Private objFieldMaster As clsAssess_Field_Master
'    Private dsLink As New DataSet
'    Private mblnIsInfoPresent As Boolean = False

'#End Region

'#Region " Form's Event "

'    Private Sub frmCaptionMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        objFieldMaster = New clsAssess_Field_Master
'        Try
'            Call Set_Logo(Me, gApplicationType)
'            Language.setLanguage(Me.Name)
'            Call OtherSettings()
'            dsLink = objFieldMaster.Get_Field_Mapping("List")
'            Call GetValue()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmCaptionMapping_Load", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmCaptionMapping_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
'        Try
'            If Asc(e.KeyChar) = 13 Then
'                SendKeys.Send("{TAB}")
'                e.Handled = True
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmCaptionMapping_KeyPress", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub frmCaptionMapping_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
'        Try
'            If e.Control = True And e.KeyCode = Keys.S Then
'                btnSave_Click(sender, e)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "frmCaptionMapping_KeyDown", mstrModuleName)
'        End Try
'    End Sub

'    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
'        Dim objfrm As New frmLanguage
'        Try
'            If User._Object._Isrighttoleft = True Then
'                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
'                objfrm.RightToLeftLayout = True
'                Call Language.ctlRightToLeftlayOut(objfrm)
'            End If

'            Call SetMessages()

'            objfrm.displayDialog(Me)

'            Call SetLanguage()

'        Catch ex As System.Exception
'            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
'        Finally
'            objfrm.Dispose()
'            objfrm = Nothing
'        End Try
'    End Sub

'#End Region

'#Region " Buttons Events "

'    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Try
'            If mblnIsInfoPresent = False Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry you cannot map the master. Reason : No informational field is defined in assessment caption setup."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            If objradField1.Checked = False AndAlso objradField2.Checked = False AndAlso objradField3.Checked = False AndAlso objradField4.Checked = False AndAlso objradField5.Checked = False Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please check atleast one of the master to link the informational fields."), enMsgBoxStyle.Information)
'                Exit Sub
'            End If

'            'VALIDATION
'            'If mintFieldUnkid <> mintNewFieldUnkid Then

'            '    Exit Sub
'            'End If
'            If objFieldMaster.UpdateLinkedField(mintFieldUnkid, mintNewFieldUnkid, User._Object._Userunkid) = False Then
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Problem in saving Mapping."), enMsgBoxStyle.Information)
'                Exit Sub
'            Else
'                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Mapping saved successfully."), enMsgBoxStyle.Information)
'                Me.Close()
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Try
'            Me.Close()
'        Catch ex As Exception
'            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
'        End Try
'    End Sub

'#End Region

'#Region " Private Methods "

'    Private Sub GetValue()
'        Try
'            If dsLink.Tables(0).Rows.Count > 0 Then
'                Dim dtmp() As DataRow = dsLink.Tables(0).Select("linkedto > 0")
'                If dtmp.Length > 0 Then
'                    mintFieldUnkid = CInt(dtmp(0).Item("linkedto"))
'                End If
'                dtmp = dsLink.Tables(0).Select("isinformational=True")
'                If dtmp.Length > 0 Then
'                    mblnIsInfoPresent = True
'                End If
'                For i As Integer = 0 To dsLink.Tables(0).Rows.Count - 1
'                    Select Case i + 1
'                        Case 1
'                            objradField1.Text = dsLink.Tables(0).Rows(i).Item("fieldcaption").ToString
'                            If objradField1.Text.Trim.Length > 0 Then objradField1.Enabled = True
'                            objradField1.Tag = CInt(dsLink.Tables(0).Rows(i).Item("fieldunkid"))
'                            If mintFieldUnkid = CInt(objradField1.Tag) Then
'                                objradField1.Checked = True
'                            Else
'                                objradField1.Checked = False
'                            End If
'                        Case 2
'                            objradField2.Text = dsLink.Tables(0).Rows(i).Item("fieldcaption").ToString
'                            If objradField2.Text.Trim.Length > 0 Then objradField2.Enabled = True
'                            objradField2.Tag = CInt(dsLink.Tables(0).Rows(i).Item("fieldunkid"))
'                            If mintFieldUnkid = CInt(objradField2.Tag) Then
'                                objradField2.Checked = True
'                            Else
'                                objradField2.Checked = False
'                            End If
'                        Case 3
'                            objradField3.Text = dsLink.Tables(0).Rows(i).Item("fieldcaption").ToString
'                            If objradField3.Text.Trim.Length > 0 Then objradField3.Enabled = True
'                            objradField3.Tag = CInt(dsLink.Tables(0).Rows(i).Item("fieldunkid"))
'                            If mintFieldUnkid = CInt(objradField3.Tag) Then
'                                objradField3.Checked = True
'                            Else
'                                objradField3.Checked = False
'                            End If
'                        Case 4
'                            objradField4.Text = dsLink.Tables(0).Rows(i).Item("fieldcaption").ToString
'                            If objradField4.Text.Trim.Length > 0 Then objradField4.Enabled = True
'                            objradField4.Tag = CInt(dsLink.Tables(0).Rows(i).Item("fieldunkid"))
'                            If mintFieldUnkid = CInt(objradField4.Tag) Then
'                                objradField4.Checked = True
'                            Else
'                                objradField4.Checked = False
'                            End If
'                        Case 5
'                            objradField5.Text = dsLink.Tables(0).Rows(i).Item("fieldcaption").ToString
'                            If objradField5.Text.Trim.Length > 0 Then objradField5.Enabled = True
'                            objradField5.Tag = CInt(dsLink.Tables(0).Rows(i).Item("fieldunkid"))
'                            If mintFieldUnkid = CInt(objradField5.Tag) Then
'                                objradField5.Checked = True
'                            Else
'                                objradField5.Checked = False
'                            End If
'                    End Select
'                Next
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " RadioButton's Events "

'    Private Sub objradField1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objradField1.CheckedChanged, objradField2.CheckedChanged, _
'                                                                                                             objradField3.CheckedChanged, objradField4.CheckedChanged, objradField5.CheckedChanged
'        Try
'            If CType(sender, RadioButton).Checked = True Then
'                mintNewFieldUnkid = CInt(CType(sender, RadioButton).Tag)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "objradField1_CheckedChanged", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'End Class