﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGlobalAssignEmpGoals

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGlobalAssignEmpGoals"
    Private mblnCancel As Boolean = True
    Private mintSelectedPeriodId As Integer = 0
    Private mdtOwner As DataTable
    Private dtOwner As DataTable = Nothing
    Private dtOwnerView As DataView
    Private mstrAdvanceFilter As String = ""
    Private mintExOrder As Integer = 0
    Private mintLinkedFld As Integer = 0
    Private dtFinalTab As DataTable = Nothing
    Private dtFinalTabView As DataView
    Private objEmpOwner As clsassess_empowner_tran
    Private mintOwrFldTypeId As Integer = -1

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal cboEmp As ComboBox, _
                                  ByVal cboPrd As ComboBox) As Boolean
        Try
            With cboEmp
                cboOwner.ValueMember = .ValueMember
                cboOwner.DisplayMember = .DisplayMember
                cboOwner.DataSource = .DataSource
            End With
            With cboPrd
                cboPeriod.ValueMember = .ValueMember
                cboPeriod.DisplayMember = .DisplayMember
                cboPeriod.DataSource = .DataSource
            End With
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Set_Caption()
        Try
            Dim objFMapping As New clsAssess_Field_Mapping
            Dim strLinkedFld As String = objFMapping.Get_Map_FieldName(mintSelectedPeriodId)
            mintLinkedFld = objFMapping.Get_Map_FieldId(mintSelectedPeriodId)
            Dim objFMaster As New clsAssess_Field_Master
            mintExOrder = objFMaster.Get_Field_ExOrder(mintLinkedFld)
            Select Case mintExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD1
                Case enWeight_Types.WEIGHT_FIELD2
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD2
                Case enWeight_Types.WEIGHT_FIELD3
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD3
                Case enWeight_Types.WEIGHT_FIELD4
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD4
                Case enWeight_Types.WEIGHT_FIELD5
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD5
            End Select
            objFMaster = Nothing : objFMapping = Nothing
            objgbOwnerItems.Text = strLinkedFld & " " & Language.getMessage(mstrModuleName, 1, "Information")

            'S.SANDEEP [12 MAY 2015] -- START
            'objdgcolhfname.HeaderText = strLinkedFld
            'S.SANDEEP [12 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Caption", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data()
        Try
            RemoveHandler dgvOwner.CellContentClick, AddressOf dgvOwner_CellContentClick
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dList = objEmployee.GetEmployee_Access(CInt(cboOwner.SelectedValue), 0, CInt(cboPeriod.SelectedValue), mstrAdvanceFilter)
            dList = objEmployee.GetEmployee_Access(CInt(cboOwner.SelectedValue), 0, FinancialYear._Object._DatabaseName, CInt(cboPeriod.SelectedValue), mstrAdvanceFilter)
            'Sohail (21 Aug 2015) -- End

            'S.SANDEEP [27 Jan 2016] -- START
            'DUPLICATION OF EMPLOYEE {If Employee Reporting To & Assessor/Reviewer is Same Employee}
            Dim dView As DataView = dList.Tables(0).DefaultView.ToTable(True, "ischeck", "employeecode", "employeename", "employeeunkid").DefaultView
            dList.Tables(0).Rows.Clear()
            dList.Tables.RemoveAt(0)
            dList.Tables.Add(dView.ToTable)
            'S.SANDEEP [27 Jan 2016] -- END

            dtOwner = dList.Tables(0)
            dtOwnerView = dtOwner.DefaultView
            dgvOwner.AutoGenerateColumns = False
            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "employeename"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgvOwner.DataSource = dtOwnerView

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
            AddHandler dgvOwner.CellContentClick, AddressOf dgvOwner_CellContentClick

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Items_Grid()
        Try
            RemoveHandler dgvItems.CellContentClick, AddressOf dgvItems_CellContentClick
            RemoveHandler objchkItems.CheckedChanged, AddressOf objchkItems_CheckedChanged

            Dim objEmpFld1 As New clsassess_empfield1_master
            dtFinalTab = objEmpFld1.GetDisplayList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), "List")
            dtFinalTab.Columns.Add("fcheck", System.Type.GetType("System.Boolean")).DefaultValue = False
            dgvItems.AutoGenerateColumns = False

            'S.SANDEEP [12 MAY 2015] -- START
            'Select Case mintExOrder
            '    Case enWeight_Types.WEIGHT_FIELD1
            '        objdgcolhfname.DataPropertyName = "Field1"
            '    Case enWeight_Types.WEIGHT_FIELD2
            '        objdgcolhfname.DataPropertyName = "Field2"
            '    Case enWeight_Types.WEIGHT_FIELD3
            '        objdgcolhfname.DataPropertyName = "Field3"
            '    Case enWeight_Types.WEIGHT_FIELD4
            '        objdgcolhfname.DataPropertyName = "Field4"
            '    Case enWeight_Types.WEIGHT_FIELD5
            '        objdgcolhfname.DataPropertyName = "Field5"
            'End Select
            'objdgcolhfcheck.DataPropertyName = "fcheck"
            'dgcolhfeddate.DataPropertyName = "Ed_Date"
            'dgcolhfpct_complete.DataPropertyName = "pct_complete"
            'dgcolhfstatus.DataPropertyName = "CStatus"
            'dgcolhfstdate.DataPropertyName = "St_Date"
            'dgcolhfweight.DataPropertyName = "Weight"
            'dtFinalTabView = dtFinalTab.DefaultView
            'dgvItems.DataSource = dtFinalTabView
            objdgcolhfcheck.DataPropertyName = "fcheck"
            Dim iColName As String = String.Empty

            Dim iPlan() As String = Nothing
            If ConfigParameter._Object._ViewTitles_InPlanning.Trim.Length > 0 Then
                iPlan = ConfigParameter._Object._ViewTitles_InPlanning.Split(CChar("|"))
            End If
            Dim objFMaster As New clsAssess_Field_Master
            For Each dCol As DataColumn In dtFinalTab.Columns
                If dCol.ColumnName = "fcheck" Then Continue For
                iColName = "" : iColName = "obj" & dCol.ColumnName
                If dgvItems.Columns.Contains(iColName) = True Then Continue For
                Dim dgvCol As New DataGridViewTextBoxColumn()
                dgvCol.Name = iColName
                dgvCol.Width = 120
                dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                dgvCol.ReadOnly = True
                dgvCol.DataPropertyName = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                If dCol.Caption.Length <= 0 Then
                    dgvCol.Visible = False
                Else
                    If dtFinalTab.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                        dgvCol.Width = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, CInt(dtFinalTab.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)))
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, dtFinalTab.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                            End If
                        End If
                    End If
                End If
                If dCol.ColumnName = "Emp" Then
                    dgvCol.Visible = False
                End If
                If ConfigParameter._Object._CascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                    If dCol.ColumnName.StartsWith("Owr") Then
                        dgvCol.Visible = False
                    End If
                End If

                'S.SANDEEP [01 JUL 2015] -- START
                If dCol.ColumnName = "vuRemark" Or dCol.ColumnName = "vuProgress" Then
                    dgvCol.Visible = False
                End If
                'S.SANDEEP [01 JUL 2015] -- END

                dgvItems.Columns.Add(dgvCol)
            Next
            dtFinalTabView = dtFinalTab.DefaultView
            dgvItems.DataSource = dtFinalTabView
            objFMaster = Nothing
            dgvItems.ColumnHeadersHeight = 30
            'S.SANDEEP [12 MAY 2015] -- END

            AddHandler dgvItems.CellContentClick, AddressOf dgvItems_CellContentClick
            AddHandler objchkItems.CheckedChanged, AddressOf objchkItems_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Items_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidOwner() As Boolean
        Try
            If CInt(cboOwner.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Owner is mandatory information. Please select owner to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            dtFinalTab.AcceptChanges() : dtOwner.AcceptChanges()
            Dim xRow() As DataRow = Nothing
            xRow = dtFinalTab.Select("fcheck = true")
            If xRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, please check atleast one of the") & " " & objgbOwnerItems.Text & " " & _
                                Language.getMessage(mstrModuleName, 4, "goal to assign owner."), enMsgBoxStyle.Information)
                Return False
            End If

            xRow = dtOwner.Select("ischeck = true")
            If xRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, please check atleast one of the owners to assign goals to them."), enMsgBoxStyle.Information)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidOwner", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmGlobalAssignEmpGoals_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmpOwner = New clsassess_empowner_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            mdtOwner = objEmpOwner.Get_Data(-1, mintOwrFldTypeId)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalAssignEmpGoals_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Me.Enabled = False
        Try
            If dtFinalTab IsNot Nothing AndAlso dtOwner IsNot Nothing Then
                Dim iFlag As Boolean = False
                If IsValidOwner() = False Then Exit Sub
                Dim dGoals(), dOwner() As DataRow
                dGoals = dtFinalTab.Select("fcheck = true")
                If dGoals.Length > 0 Then
                    dOwner = dtOwner.Select("ischeck = true")
                    If dOwner.Length > 0 Then
                        Dim iOwnerFldId As Integer = 0
                        For mGoalIdx As Integer = 0 To dGoals.Length - 1
                            objlnkCaption.Text = Language.getMessage(mstrModuleName, 6, "Processing") & " " & (mGoalIdx + 1).ToString & "/" & dGoals.Length.ToString
                            iOwnerFldId = -1 : Application.DoEvents()
                            For mEmpIdx As Integer = 0 To dOwner.Length - 1
                                Dim dRow As DataRow = mdtOwner.NewRow
                                dRow.Item("ownertranunkid") = -1
                                Select Case mintExOrder
                                    Case enWeight_Types.WEIGHT_FIELD1
                                        dRow.Item("empfieldunkid") = dGoals(mGoalIdx).Item("empfield1unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("empfield1unkid"))
                                    Case enWeight_Types.WEIGHT_FIELD2
                                        dRow.Item("empfieldunkid") = dGoals(mGoalIdx).Item("empfield2unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("empfield2unkid"))
                                    Case enWeight_Types.WEIGHT_FIELD3
                                        dRow.Item("empfieldunkid") = dGoals(mGoalIdx).Item("empfield3unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("empfield3unkid"))
                                    Case enWeight_Types.WEIGHT_FIELD4
                                        dRow.Item("empfieldunkid") = dGoals(mGoalIdx).Item("empfield4unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("empfield4unkid"))
                                    Case enWeight_Types.WEIGHT_FIELD5
                                        dRow.Item("empfieldunkid") = dGoals(mGoalIdx).Item("empfield5unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("empfield5unkid"))
                                End Select
                                dRow.Item("employeeunkid") = dOwner(mEmpIdx).Item("employeeunkid")
                                dRow.Item("empfieldtypeid") = mintOwrFldTypeId
                                dRow.Item("AUD") = "A"
                                dRow.Item("GUID") = Guid.NewGuid.ToString
                                mdtOwner.Rows.Add(dRow)

                                If mdtOwner IsNot Nothing Then
                                    objEmpOwner = New clsassess_empowner_tran
                                    objEmpOwner._DatTable = mdtOwner.Copy

                                    'S.SANDEEP [28-May-2018] -- START
                                    'ISSUE/ENHANCEMENT : {Audit Trails} 
                                    objEmpOwner._FormName = mstrModuleName
                                    objEmpOwner._LoginEmployeeunkid = 0
                                    objEmpOwner._ClientIP = getIP()
                                    objEmpOwner._HostName = getHostName()
                                    objEmpOwner._FromWeb = False
                                    objEmpOwner._AuditUserId = User._Object._Userunkid
objEmpOwner._CompanyUnkid = Company._Object._Companyunkid
                                    objEmpOwner._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    'S.SANDEEP [28-May-2018] -- END

                                    If objEmpOwner.GlobalAssign_Owner(User._Object._Userunkid, iOwnerFldId, mintOwrFldTypeId) = False Then
                                        objEmpOwner = Nothing : iFlag = False
                                        Exit Sub
                                    Else
                                        iFlag = True
                                    End If
                                    objEmpOwner = Nothing
                                End If
                            Next
                        Next
                    End If
                    If iFlag = True Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Goals assigned successfully."), enMsgBoxStyle.Information)
                        objchkEmployee.Checked = False : objchkItems.Checked = False
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            objlnkCaption.Text = "" : Me.Enabled = True
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        mblnCancel = False
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Please select Period and Owner to do global assign operation."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            cboOwner.Focus()
            Call Fill_Data()
            Call Fill_Items_Grid()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            mstrAdvanceFilter = ""
            Call Fill_Data()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchOwner_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOwner.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            If cboOwner.DataSource Is Nothing Then Exit Sub
            With frm
                .ValueMember = cboOwner.ValueMember
                .DisplayMember = cboOwner.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboOwner.DataSource, DataTable)
                If .DisplayDialog = True Then
                    cboOwner.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOwner_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Textbox Events "

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvOwner.Rows.Count > 0 Then
                        If dgvOwner.SelectedRows(0).Index = dgvOwner.Rows(dgvOwner.RowCount - 1).Index Then Exit Sub
                        dgvOwner.Rows(dgvOwner.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvOwner.Rows.Count > 0 Then
                        If dgvOwner.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvOwner.Rows(dgvOwner.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            dtOwnerView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchItems_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchItems.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvItems.Rows.Count > 0 Then
                        If dgvItems.SelectedRows(0).Index = dgvItems.Rows(dgvItems.RowCount - 1).Index Then Exit Sub
                        dgvItems.Rows(dgvItems.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvItems.Rows.Count > 0 Then
                        If dgvItems.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvItems.Rows(dgvItems.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchItems_KeyDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchItems_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchItems.TextChanged
        Try
            'S.SANDEEP [12 MAY 2015] -- START
            'Dim strSearch As String = ""
            'If txtSearchItems.Text.Trim.Length > 0 Then
            '    strSearch = objdgcolhfname.DataPropertyName & " LIKE '%" & txtSearchItems.Text & "%' OR " & _
            '                dgcolhfeddate.DataPropertyName & " LIKE '%" & txtSearchItems.Text & "%' OR " & _
            '                dgcolhfstatus.DataPropertyName & " LIKE '%" & txtSearchItems.Text & "%' OR " & _
            '                dgcolhfstdate.DataPropertyName & " LIKE '%" & txtSearchItems.Text & "%' OR " & _
            '                dgcolhfweight.DataPropertyName & " LIKE '%" & txtSearchItems.Text & "%'"
            'End If
            'dtFinalTabView.RowFilter = strSearch
            'S.SANDEEP [12 MAY 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchItems_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event "

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            If dtOwnerView IsNot Nothing Then
                RemoveHandler dgvOwner.CellContentClick, AddressOf dgvOwner_CellContentClick
                For Each dr As DataRowView In dtOwnerView
                    dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
                Next
                dgvOwner.Refresh()
                AddHandler dgvOwner.CellContentClick, AddressOf dgvOwner_CellContentClick
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkItems_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkItems.CheckedChanged
        Try
            If dtFinalTabView IsNot Nothing Then
                RemoveHandler dgvItems.CellContentClick, AddressOf dgvItems_CellContentClick
                For Each dr As DataRowView In dtFinalTabView
                    dr.Item("fcheck") = CBool(objchkItems.CheckState)
                Next
                dgvItems.Refresh()
                AddHandler dgvItems.CellContentClick, AddressOf dgvItems_CellContentClick
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkItems_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid Events "

    Private Sub dgvItems_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvItems.CellContentClick, dgvItems.CellContentDoubleClick
        Try
            RemoveHandler objchkItems.CheckedChanged, AddressOf objchkItems_CheckedChanged
            If e.ColumnIndex = objdgcolhfcheck.Index Then
                If Me.dgvItems.IsCurrentCellDirty Then
                    Me.dgvItems.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = dtFinalTabView.ToTable.Select("fcheck = true", "")
                If drRow.Length > 0 Then
                    If drRow.Length > 0 Then
                        If dtFinalTabView.ToTable.Rows.Count = drRow.Length Then
                            objchkItems.CheckState = CheckState.Checked
                        Else
                            objchkItems.CheckState = CheckState.Indeterminate
                        End If
                    End If
                Else
                    objchkItems.CheckState = CheckState.Unchecked
                End If
            End If
            AddHandler objchkItems.CheckedChanged, AddressOf objchkItems_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvItems_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvOwner_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOwner.CellContentClick, dgvOwner.CellContentDoubleClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvOwner.IsCurrentCellDirty Then
                    Me.dgvOwner.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Dim drRow As DataRow() = dtOwnerView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dtOwnerView.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOwner_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvOwner_ColumnWidthChanged(ByVal sender As Object, ByVal e As DataGridViewColumnEventArgs) Handles dgvOwner.ColumnWidthChanged
        Dim rtHeader As Rectangle = Me.dgvOwner.DisplayRectangle
        rtHeader.Height = CInt(Me.dgvOwner.ColumnHeadersHeight / 2)
        Me.dgvOwner.Invalidate(rtHeader)
    End Sub

    Private Sub dgvOwner_Scroll(ByVal sender As Object, ByVal e As ScrollEventArgs) Handles dgvOwner.Scroll
        Dim rtHeader As Rectangle = Me.dgvOwner.DisplayRectangle
        rtHeader.Height = CInt(Me.dgvOwner.ColumnHeadersHeight / 2)
        Me.dgvOwner.Invalidate(rtHeader)
    End Sub

    Private Sub dgvOwner_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles dgvOwner.Paint
        Dim j As Integer = 1
        While j < dgvOwner.ColumnCount - 1
            Dim r1 As Rectangle = Me.dgvOwner.GetCellDisplayRectangle(j, -1, True)
            Dim w2 As Integer = Me.dgvOwner.GetCellDisplayRectangle(j + 1, -1, True).Width
            Dim w3 As Integer = Me.dgvOwner.GetCellDisplayRectangle(j + 1, -1, True).Height
            'r1.X += 1
            r1.Y += 1
            r1.Width = r1.Width + w2 - 2
            'r1.Height = CInt(r1.Height / 2 - 2)
            r1.Height = w3 - 5
            e.Graphics.FillRectangle(New SolidBrush(Me.dgvOwner.ColumnHeadersDefaultCellStyle.SelectionForeColor), r1)
            Dim format As New StringFormat()
            format.Alignment = StringAlignment.Center
            format.LineAlignment = StringAlignment.Center
            e.Graphics.DrawString(Language.getMessage(mstrModuleName, 2, "Assigned To"), Me.dgvOwner.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Me.dgvOwner.ColumnHeadersDefaultCellStyle.ForeColor), r1, format)
            j += 2
        End While
    End Sub

    Private Sub dgvOwner_CellPainting(ByVal sender As Object, ByVal e As DataGridViewCellPaintingEventArgs) Handles dgvOwner.CellPainting
        If e.RowIndex = -1 AndAlso e.ColumnIndex > -1 Then
            Dim r2 As Rectangle = e.CellBounds
            r2.Y = CInt(r2.Y + e.CellBounds.Height / 2)
            r2.Height = CInt(e.CellBounds.Height / 2)
            e.PaintBackground(r2, True)
            e.PaintContent(r2)
            e.Handled = True
        End If
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboOwner.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                dgvItems.DataSource = Nothing : dgvOwner.DataSource = Nothing
                If dtFinalTab IsNot Nothing Then dtFinalTab.Rows.Clear()
                If dtOwner IsNot Nothing Then dtOwner.Rows.Clear()
                If mdtOwner IsNot Nothing Then mdtOwner.Rows.Clear()
            Else
                mintSelectedPeriodId = CInt(cboPeriod.SelectedValue)
                Call Set_Caption()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event "

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                mstrAdvanceFilter = frm._GetFilterString
                Call Fill_Data()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.objgbOwnerItems.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.objgbOwnerItems.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbOwnerData.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbOwnerData.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbDetails.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbDetails.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbOwnerData.Text = Language._Object.getCaption(Me.gbOwnerData.Name, Me.gbOwnerData.Text)
			Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
			Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
			Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
			Me.gbDetails.Text = Language._Object.getCaption(Me.gbDetails.Name, Me.gbDetails.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblOwner.Text = Language._Object.getCaption(Me.lblOwner.Name, Me.lblOwner.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Information")
			Language.setMessage(mstrModuleName, 2, "Assigned To")
			Language.setMessage(mstrModuleName, 3, "Sorry, please check atleast one of the")
			Language.setMessage(mstrModuleName, 4, "goal to assign owner.")
			Language.setMessage(mstrModuleName, 5, "Sorry, please check atleast one of the owners to assign goals to them.")
			Language.setMessage(mstrModuleName, 6, "Processing")
			Language.setMessage(mstrModuleName, 7, "Sorry, Please select Period and Owner to do global assign operation.")
			Language.setMessage(mstrModuleName, 8, "Goals assigned successfully.")
			Language.setMessage(mstrModuleName, 9, "Sorry, Owner is mandatory information. Please select owner to continue.")
			Language.setMessage(mstrModuleName, 10, "Sorry, Period is mandatory information. Please select Period to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class