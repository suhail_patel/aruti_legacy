﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objfrmAddEditEmpField3
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(objfrmAddEditEmpField3))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objpnlData = New System.Windows.Forms.Panel
        Me.txtGoalValue = New eZee.TextBox.NumericTextBox
        Me.lblGoalValue = New System.Windows.Forms.Label
        Me.lblGoalType = New System.Windows.Forms.Label
        Me.cboGoalType = New System.Windows.Forms.ComboBox
        Me.tblpAssessorEmployee = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearchEmp = New System.Windows.Forms.TextBox
        Me.objpnlEmp = New System.Windows.Forms.Panel
        Me.objchkEmployee = New System.Windows.Forms.CheckBox
        Me.dgvOwner = New System.Windows.Forms.DataGridView
        Me.objdgcolhECheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtPctComplete = New eZee.TextBox.NumericTextBox
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.lblEndDate = New System.Windows.Forms.Label
        Me.lblStartDate = New System.Windows.Forms.Label
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.objtabcRemarks = New System.Windows.Forms.TabControl
        Me.objtabpRemark1 = New System.Windows.Forms.TabPage
        Me.txtRemark1 = New System.Windows.Forms.TextBox
        Me.objtabpRemark2 = New System.Windows.Forms.TabPage
        Me.txtRemark2 = New System.Windows.Forms.TextBox
        Me.objtabpRemark3 = New System.Windows.Forms.TabPage
        Me.txtRemark3 = New System.Windows.Forms.TextBox
        Me.txtWeight = New eZee.TextBox.NumericTextBox
        Me.lblWeight = New System.Windows.Forms.Label
        Me.txtPeriod = New System.Windows.Forms.TextBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblEmpField1 = New System.Windows.Forms.Label
        Me.objtxtEmpField1 = New System.Windows.Forms.TextBox
        Me.objlblOwrField3 = New System.Windows.Forms.Label
        Me.txtEmpField3 = New System.Windows.Forms.TextBox
        Me.objbtnEmpSearchField2 = New eZee.Common.eZeeGradientButton
        Me.objlblEmpField2 = New System.Windows.Forms.Label
        Me.cboEmpFieldValue2 = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.txtEmployeeName = New System.Windows.Forms.TextBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cboUoM = New System.Windows.Forms.ComboBox
        Me.pnlMain.SuspendLayout()
        Me.objpnlData.SuspendLayout()
        Me.tblpAssessorEmployee.SuspendLayout()
        Me.objpnlEmp.SuspendLayout()
        CType(Me.dgvOwner, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objtabcRemarks.SuspendLayout()
        Me.objtabpRemark1.SuspendLayout()
        Me.objtabpRemark2.SuspendLayout()
        Me.objtabpRemark3.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objpnlData)
        Me.pnlMain.Controls.Add(Me.txtPeriod)
        Me.pnlMain.Controls.Add(Me.lblPeriod)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.objlblEmpField1)
        Me.pnlMain.Controls.Add(Me.objtxtEmpField1)
        Me.pnlMain.Controls.Add(Me.objlblOwrField3)
        Me.pnlMain.Controls.Add(Me.txtEmpField3)
        Me.pnlMain.Controls.Add(Me.objbtnEmpSearchField2)
        Me.pnlMain.Controls.Add(Me.objlblEmpField2)
        Me.pnlMain.Controls.Add(Me.cboEmpFieldValue2)
        Me.pnlMain.Controls.Add(Me.lblEmployee)
        Me.pnlMain.Controls.Add(Me.txtEmployeeName)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(821, 382)
        Me.pnlMain.TabIndex = 2
        '
        'objpnlData
        '
        Me.objpnlData.Controls.Add(Me.cboUoM)
        Me.objpnlData.Controls.Add(Me.txtGoalValue)
        Me.objpnlData.Controls.Add(Me.lblGoalValue)
        Me.objpnlData.Controls.Add(Me.lblGoalType)
        Me.objpnlData.Controls.Add(Me.cboGoalType)
        Me.objpnlData.Controls.Add(Me.tblpAssessorEmployee)
        Me.objpnlData.Controls.Add(Me.txtPctComplete)
        Me.objpnlData.Controls.Add(Me.dtpStartDate)
        Me.objpnlData.Controls.Add(Me.lblPercentage)
        Me.objpnlData.Controls.Add(Me.lblEndDate)
        Me.objpnlData.Controls.Add(Me.lblStartDate)
        Me.objpnlData.Controls.Add(Me.dtpEndDate)
        Me.objpnlData.Controls.Add(Me.cboStatus)
        Me.objpnlData.Controls.Add(Me.lblStatus)
        Me.objpnlData.Controls.Add(Me.objtabcRemarks)
        Me.objpnlData.Controls.Add(Me.txtWeight)
        Me.objpnlData.Controls.Add(Me.lblWeight)
        Me.objpnlData.Location = New System.Drawing.Point(331, 3)
        Me.objpnlData.Name = "objpnlData"
        Me.objpnlData.Size = New System.Drawing.Size(480, 323)
        Me.objpnlData.TabIndex = 499
        '
        'txtGoalValue
        '
        Me.txtGoalValue.AllowNegative = False
        Me.txtGoalValue.BackColor = System.Drawing.SystemColors.Window
        Me.txtGoalValue.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtGoalValue.DigitsInGroup = 0
        Me.txtGoalValue.Flags = 65536
        Me.txtGoalValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGoalValue.Location = New System.Drawing.Point(384, 141)
        Me.txtGoalValue.MaxDecimalPlaces = 6
        Me.txtGoalValue.MaxWholeDigits = 21
        Me.txtGoalValue.Name = "txtGoalValue"
        Me.txtGoalValue.Prefix = ""
        Me.txtGoalValue.RangeMax = 1.7976931348623157E+308
        Me.txtGoalValue.RangeMin = -1.7976931348623157E+308
        Me.txtGoalValue.Size = New System.Drawing.Size(89, 21)
        Me.txtGoalValue.TabIndex = 502
        Me.txtGoalValue.Text = "0.00"
        Me.txtGoalValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblGoalValue
        '
        Me.lblGoalValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGoalValue.Location = New System.Drawing.Point(313, 143)
        Me.lblGoalValue.Name = "lblGoalValue"
        Me.lblGoalValue.Size = New System.Drawing.Size(65, 17)
        Me.lblGoalValue.TabIndex = 501
        Me.lblGoalValue.Text = "Goal Value"
        Me.lblGoalValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGoalType
        '
        Me.lblGoalType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGoalType.Location = New System.Drawing.Point(8, 143)
        Me.lblGoalType.Name = "lblGoalType"
        Me.lblGoalType.Size = New System.Drawing.Size(74, 17)
        Me.lblGoalType.TabIndex = 500
        Me.lblGoalType.Text = "Goal Type"
        Me.lblGoalType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGoalType
        '
        Me.cboGoalType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGoalType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGoalType.FormattingEnabled = True
        Me.cboGoalType.Location = New System.Drawing.Point(88, 141)
        Me.cboGoalType.Name = "cboGoalType"
        Me.cboGoalType.Size = New System.Drawing.Size(136, 21)
        Me.cboGoalType.TabIndex = 499
        '
        'tblpAssessorEmployee
        '
        Me.tblpAssessorEmployee.ColumnCount = 1
        Me.tblpAssessorEmployee.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Controls.Add(Me.txtSearchEmp, 0, 0)
        Me.tblpAssessorEmployee.Controls.Add(Me.objpnlEmp, 0, 1)
        Me.tblpAssessorEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpAssessorEmployee.Location = New System.Drawing.Point(3, 168)
        Me.tblpAssessorEmployee.Name = "tblpAssessorEmployee"
        Me.tblpAssessorEmployee.RowCount = 2
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.tblpAssessorEmployee.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpAssessorEmployee.Size = New System.Drawing.Size(474, 153)
        Me.tblpAssessorEmployee.TabIndex = 490
        '
        'txtSearchEmp
        '
        Me.txtSearchEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearchEmp.Location = New System.Drawing.Point(3, 3)
        Me.txtSearchEmp.Name = "txtSearchEmp"
        Me.txtSearchEmp.Size = New System.Drawing.Size(468, 21)
        Me.txtSearchEmp.TabIndex = 106
        '
        'objpnlEmp
        '
        Me.objpnlEmp.Controls.Add(Me.objchkEmployee)
        Me.objpnlEmp.Controls.Add(Me.dgvOwner)
        Me.objpnlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objpnlEmp.Location = New System.Drawing.Point(3, 29)
        Me.objpnlEmp.Name = "objpnlEmp"
        Me.objpnlEmp.Size = New System.Drawing.Size(468, 121)
        Me.objpnlEmp.TabIndex = 107
        '
        'objchkEmployee
        '
        Me.objchkEmployee.AutoSize = True
        Me.objchkEmployee.Location = New System.Drawing.Point(7, 5)
        Me.objchkEmployee.Name = "objchkEmployee"
        Me.objchkEmployee.Size = New System.Drawing.Size(15, 14)
        Me.objchkEmployee.TabIndex = 104
        Me.objchkEmployee.UseVisualStyleBackColor = True
        '
        'dgvOwner
        '
        Me.dgvOwner.AllowUserToAddRows = False
        Me.dgvOwner.AllowUserToDeleteRows = False
        Me.dgvOwner.AllowUserToResizeColumns = False
        Me.dgvOwner.AllowUserToResizeRows = False
        Me.dgvOwner.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvOwner.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvOwner.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgvOwner.ColumnHeadersHeight = 21
        Me.dgvOwner.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvOwner.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhECheck, Me.dgcolhEcode, Me.dgcolhEName, Me.objdgcolhEmpId})
        Me.dgvOwner.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvOwner.Location = New System.Drawing.Point(0, 0)
        Me.dgvOwner.MultiSelect = False
        Me.dgvOwner.Name = "dgvOwner"
        Me.dgvOwner.RowHeadersVisible = False
        Me.dgvOwner.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvOwner.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvOwner.Size = New System.Drawing.Size(468, 121)
        Me.dgvOwner.TabIndex = 105
        '
        'objdgcolhECheck
        '
        Me.objdgcolhECheck.Frozen = True
        Me.objdgcolhECheck.HeaderText = ""
        Me.objdgcolhECheck.Name = "objdgcolhECheck"
        Me.objdgcolhECheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhECheck.Width = 25
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEcode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEcode.Width = 70
        '
        'dgcolhEName
        '
        Me.dgcolhEName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.Visible = False
        '
        'txtPctComplete
        '
        Me.txtPctComplete.AllowNegative = False
        Me.txtPctComplete.BackColor = System.Drawing.SystemColors.Window
        Me.txtPctComplete.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtPctComplete.DigitsInGroup = 0
        Me.txtPctComplete.Flags = 65536
        Me.txtPctComplete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPctComplete.Location = New System.Drawing.Point(170, 114)
        Me.txtPctComplete.MaxDecimalPlaces = 2
        Me.txtPctComplete.MaxWholeDigits = 9
        Me.txtPctComplete.Name = "txtPctComplete"
        Me.txtPctComplete.Prefix = ""
        Me.txtPctComplete.RangeMax = 1.7976931348623157E+308
        Me.txtPctComplete.RangeMin = -1.7976931348623157E+308
        Me.txtPctComplete.ReadOnly = True
        Me.txtPctComplete.Size = New System.Drawing.Size(54, 21)
        Me.txtPctComplete.TabIndex = 488
        Me.txtPctComplete.Text = "0.00"
        Me.txtPctComplete.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Checked = False
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(6, 26)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.ShowCheckBox = True
        Me.dtpStartDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpStartDate.TabIndex = 481
        '
        'lblPercentage
        '
        Me.lblPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentage.Location = New System.Drawing.Point(130, 94)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(94, 17)
        Me.lblPercentage.TabIndex = 487
        Me.lblPercentage.Text = "% Completed"
        Me.lblPercentage.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblEndDate
        '
        Me.lblEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndDate.Location = New System.Drawing.Point(124, 5)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(89, 17)
        Me.lblEndDate.TabIndex = 484
        Me.lblEndDate.Text = "End Date"
        Me.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblStartDate
        '
        Me.lblStartDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartDate.Location = New System.Drawing.Point(6, 6)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(99, 17)
        Me.lblStartDate.TabIndex = 483
        Me.lblStartDate.Text = "Start Date"
        Me.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Checked = False
        Me.dtpEndDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(118, 26)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.ShowCheckBox = True
        Me.dtpEndDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpEndDate.TabIndex = 482
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboStatus.Enabled = False
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(6, 70)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(218, 21)
        Me.cboStatus.TabIndex = 479
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(6, 50)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(205, 17)
        Me.lblStatus.TabIndex = 480
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objtabcRemarks
        '
        Me.objtabcRemarks.Controls.Add(Me.objtabpRemark1)
        Me.objtabcRemarks.Controls.Add(Me.objtabpRemark2)
        Me.objtabcRemarks.Controls.Add(Me.objtabpRemark3)
        Me.objtabcRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objtabcRemarks.Location = New System.Drawing.Point(230, 6)
        Me.objtabcRemarks.Name = "objtabcRemarks"
        Me.objtabcRemarks.SelectedIndex = 0
        Me.objtabcRemarks.Size = New System.Drawing.Size(247, 129)
        Me.objtabcRemarks.TabIndex = 478
        '
        'objtabpRemark1
        '
        Me.objtabpRemark1.Controls.Add(Me.txtRemark1)
        Me.objtabpRemark1.Location = New System.Drawing.Point(4, 22)
        Me.objtabpRemark1.Name = "objtabpRemark1"
        Me.objtabpRemark1.Size = New System.Drawing.Size(239, 103)
        Me.objtabpRemark1.TabIndex = 0
        Me.objtabpRemark1.Tag = "objtabpRemark1"
        Me.objtabpRemark1.UseVisualStyleBackColor = True
        '
        'txtRemark1
        '
        Me.txtRemark1.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark1.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark1.Multiline = True
        Me.txtRemark1.Name = "txtRemark1"
        Me.txtRemark1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark1.Size = New System.Drawing.Size(239, 103)
        Me.txtRemark1.TabIndex = 443
        '
        'objtabpRemark2
        '
        Me.objtabpRemark2.Controls.Add(Me.txtRemark2)
        Me.objtabpRemark2.Location = New System.Drawing.Point(4, 22)
        Me.objtabpRemark2.Name = "objtabpRemark2"
        Me.objtabpRemark2.Size = New System.Drawing.Size(239, 103)
        Me.objtabpRemark2.TabIndex = 1
        Me.objtabpRemark2.Tag = "objtabpRemark2"
        Me.objtabpRemark2.UseVisualStyleBackColor = True
        '
        'txtRemark2
        '
        Me.txtRemark2.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark2.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark2.Multiline = True
        Me.txtRemark2.Name = "txtRemark2"
        Me.txtRemark2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark2.Size = New System.Drawing.Size(239, 103)
        Me.txtRemark2.TabIndex = 444
        '
        'objtabpRemark3
        '
        Me.objtabpRemark3.Controls.Add(Me.txtRemark3)
        Me.objtabpRemark3.Location = New System.Drawing.Point(4, 22)
        Me.objtabpRemark3.Name = "objtabpRemark3"
        Me.objtabpRemark3.Size = New System.Drawing.Size(239, 103)
        Me.objtabpRemark3.TabIndex = 2
        Me.objtabpRemark3.Tag = "objtabpRemark3"
        Me.objtabpRemark3.UseVisualStyleBackColor = True
        '
        'txtRemark3
        '
        Me.txtRemark3.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemark3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtRemark3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark3.Location = New System.Drawing.Point(0, 0)
        Me.txtRemark3.Multiline = True
        Me.txtRemark3.Name = "txtRemark3"
        Me.txtRemark3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark3.Size = New System.Drawing.Size(239, 103)
        Me.txtRemark3.TabIndex = 444
        '
        'txtWeight
        '
        Me.txtWeight.AllowNegative = False
        Me.txtWeight.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtWeight.DigitsInGroup = 0
        Me.txtWeight.Flags = 65536
        Me.txtWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeight.Location = New System.Drawing.Point(6, 114)
        Me.txtWeight.MaxDecimalPlaces = 2
        Me.txtWeight.MaxWholeDigits = 9
        Me.txtWeight.Name = "txtWeight"
        Me.txtWeight.Prefix = ""
        Me.txtWeight.RangeMax = 1.7976931348623157E+308
        Me.txtWeight.RangeMin = -1.7976931348623157E+308
        Me.txtWeight.Size = New System.Drawing.Size(57, 21)
        Me.txtWeight.TabIndex = 467
        Me.txtWeight.Text = "0.00"
        Me.txtWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblWeight
        '
        Me.lblWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeight.Location = New System.Drawing.Point(9, 94)
        Me.lblWeight.Name = "lblWeight"
        Me.lblWeight.Size = New System.Drawing.Size(54, 17)
        Me.lblWeight.TabIndex = 466
        Me.lblWeight.Text = "Weight"
        Me.lblWeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPeriod
        '
        Me.txtPeriod.BackColor = System.Drawing.Color.White
        Me.txtPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeriod.Location = New System.Drawing.Point(12, 29)
        Me.txtPeriod.Name = "txtPeriod"
        Me.txtPeriod.ReadOnly = True
        Me.txtPeriod.Size = New System.Drawing.Size(286, 21)
        Me.txtPeriod.TabIndex = 498
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(12, 9)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(286, 17)
        Me.lblPeriod.TabIndex = 497
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 327)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(821, 55)
        Me.objFooter.TabIndex = 482
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(615, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(715, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objlblEmpField1
        '
        Me.objlblEmpField1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmpField1.Location = New System.Drawing.Point(12, 98)
        Me.objlblEmpField1.Name = "objlblEmpField1"
        Me.objlblEmpField1.Size = New System.Drawing.Size(286, 17)
        Me.objlblEmpField1.TabIndex = 495
        Me.objlblEmpField1.Text = "#Caption"
        '
        'objtxtEmpField1
        '
        Me.objtxtEmpField1.BackColor = System.Drawing.Color.White
        Me.objtxtEmpField1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objtxtEmpField1.Location = New System.Drawing.Point(12, 118)
        Me.objtxtEmpField1.Name = "objtxtEmpField1"
        Me.objtxtEmpField1.ReadOnly = True
        Me.objtxtEmpField1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.objtxtEmpField1.Size = New System.Drawing.Size(286, 21)
        Me.objtxtEmpField1.TabIndex = 494
        '
        'objlblOwrField3
        '
        Me.objlblOwrField3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblOwrField3.Location = New System.Drawing.Point(12, 186)
        Me.objlblOwrField3.Name = "objlblOwrField3"
        Me.objlblOwrField3.Size = New System.Drawing.Size(286, 17)
        Me.objlblOwrField3.TabIndex = 493
        Me.objlblOwrField3.Text = "#Caption"
        '
        'txtEmpField3
        '
        Me.txtEmpField3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmpField3.Location = New System.Drawing.Point(12, 206)
        Me.txtEmpField3.Multiline = True
        Me.txtEmpField3.Name = "txtEmpField3"
        Me.txtEmpField3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtEmpField3.Size = New System.Drawing.Size(286, 115)
        Me.txtEmpField3.TabIndex = 492
        '
        'objbtnEmpSearchField2
        '
        Me.objbtnEmpSearchField2.BackColor = System.Drawing.Color.Transparent
        Me.objbtnEmpSearchField2.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnEmpSearchField2.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnEmpSearchField2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnEmpSearchField2.BorderSelected = False
        Me.objbtnEmpSearchField2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnEmpSearchField2.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnEmpSearchField2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnEmpSearchField2.Location = New System.Drawing.Point(304, 162)
        Me.objbtnEmpSearchField2.Name = "objbtnEmpSearchField2"
        Me.objbtnEmpSearchField2.Size = New System.Drawing.Size(21, 21)
        Me.objbtnEmpSearchField2.TabIndex = 491
        '
        'objlblEmpField2
        '
        Me.objlblEmpField2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblEmpField2.Location = New System.Drawing.Point(12, 142)
        Me.objlblEmpField2.Name = "objlblEmpField2"
        Me.objlblEmpField2.Size = New System.Drawing.Size(286, 17)
        Me.objlblEmpField2.TabIndex = 490
        Me.objlblEmpField2.Text = "#Caption"
        Me.objlblEmpField2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmpFieldValue2
        '
        Me.cboEmpFieldValue2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpFieldValue2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpFieldValue2.FormattingEnabled = True
        Me.cboEmpFieldValue2.Location = New System.Drawing.Point(12, 162)
        Me.cboEmpFieldValue2.Name = "cboEmpFieldValue2"
        Me.cboEmpFieldValue2.Size = New System.Drawing.Size(286, 21)
        Me.cboEmpFieldValue2.TabIndex = 489
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(12, 53)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(286, 17)
        Me.lblEmployee.TabIndex = 485
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployeeName
        '
        Me.txtEmployeeName.BackColor = System.Drawing.Color.White
        Me.txtEmployeeName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeName.Location = New System.Drawing.Point(12, 74)
        Me.txtEmployeeName.Name = "txtEmployeeName"
        Me.txtEmployeeName.ReadOnly = True
        Me.txtEmployeeName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtEmployeeName.Size = New System.Drawing.Size(286, 21)
        Me.txtEmployeeName.TabIndex = 484
        '
        'cboUoM
        '
        Me.cboUoM.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUoM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUoM.FormattingEnabled = True
        Me.cboUoM.Location = New System.Drawing.Point(230, 141)
        Me.cboUoM.Name = "cboUoM"
        Me.cboUoM.Size = New System.Drawing.Size(77, 21)
        Me.cboUoM.TabIndex = 513
        '
        'objfrmAddEditEmpField3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(821, 382)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "objfrmAddEditEmpField3"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.objpnlData.ResumeLayout(False)
        Me.objpnlData.PerformLayout()
        Me.tblpAssessorEmployee.ResumeLayout(False)
        Me.tblpAssessorEmployee.PerformLayout()
        Me.objpnlEmp.ResumeLayout(False)
        Me.objpnlEmp.PerformLayout()
        CType(Me.dgvOwner, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objtabcRemarks.ResumeLayout(False)
        Me.objtabpRemark1.ResumeLayout(False)
        Me.objtabpRemark1.PerformLayout()
        Me.objtabpRemark2.ResumeLayout(False)
        Me.objtabpRemark2.PerformLayout()
        Me.objtabpRemark3.ResumeLayout(False)
        Me.objtabpRemark3.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents txtEmployeeName As System.Windows.Forms.TextBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objlblOwrField3 As System.Windows.Forms.Label
    Friend WithEvents txtEmpField3 As System.Windows.Forms.TextBox
    Friend WithEvents objbtnEmpSearchField2 As eZee.Common.eZeeGradientButton
    Friend WithEvents objlblEmpField2 As System.Windows.Forms.Label
    Friend WithEvents cboEmpFieldValue2 As System.Windows.Forms.ComboBox
    Friend WithEvents objlblEmpField1 As System.Windows.Forms.Label
    Friend WithEvents objtxtEmpField1 As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtPeriod As System.Windows.Forms.TextBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents objpnlData As System.Windows.Forms.Panel
    Friend WithEvents tblpAssessorEmployee As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtSearchEmp As System.Windows.Forms.TextBox
    Friend WithEvents objpnlEmp As System.Windows.Forms.Panel
    Friend WithEvents objchkEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgvOwner As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhECheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtPctComplete As eZee.TextBox.NumericTextBox
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtpEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents objtabcRemarks As System.Windows.Forms.TabControl
    Friend WithEvents objtabpRemark1 As System.Windows.Forms.TabPage
    Friend WithEvents txtRemark1 As System.Windows.Forms.TextBox
    Friend WithEvents objtabpRemark2 As System.Windows.Forms.TabPage
    Friend WithEvents txtRemark2 As System.Windows.Forms.TextBox
    Friend WithEvents objtabpRemark3 As System.Windows.Forms.TabPage
    Friend WithEvents txtRemark3 As System.Windows.Forms.TextBox
    Friend WithEvents txtWeight As eZee.TextBox.NumericTextBox
    Friend WithEvents lblWeight As System.Windows.Forms.Label
    Friend WithEvents txtGoalValue As eZee.TextBox.NumericTextBox
    Friend WithEvents lblGoalValue As System.Windows.Forms.Label
    Friend WithEvents lblGoalType As System.Windows.Forms.Label
    Friend WithEvents cboGoalType As System.Windows.Forms.ComboBox
    Friend WithEvents cboUoM As System.Windows.Forms.ComboBox
End Class
