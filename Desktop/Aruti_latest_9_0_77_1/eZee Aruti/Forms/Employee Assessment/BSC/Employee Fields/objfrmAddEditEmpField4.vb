﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class objfrmAddEditEmpField4

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "objfrmAddEditEmpField4"
    Private mblnCancel As Boolean = True
    Private mintEmpField4Unkid As Integer = 0
    Private objEmpField4 As clsassess_empfield4_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintFieldUnkid As Integer
    Private objFieldMaster As New clsAssess_Field_Master(True)
    Private mdicFieldData As New Dictionary(Of Integer, String)
    Private objWSetting As New clsWeight_Setting(True)
    Private mintParentId As Integer = 0
    Private mintEmployeeId As Integer = 0
    Private mintPeriodId As Integer = 0
    Private mblnDropDownClosed As Boolean = False
    Private mintLinkedFieldId As Integer = 0
    Private objOwrOwner As clsassess_empowner_tran
    Private mdtOwner As DataTable
    Private dtOwnerView As DataView
    Private mintMainParentId As Integer = 0

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, _
                                  ByVal eAction As enAction, _
                                  ByVal intFieldId As Integer, _
                                  ByVal iEmployeeId As Integer, _
                                  ByVal iPeriodId As Integer, _
                                  Optional ByVal iParentId As Integer = 0, _
                                  Optional ByVal iMainParentId As Integer = 0) As Boolean
        Try
            mintEmpField4Unkid = intUnkId
            mintFieldUnkid = intFieldId
            mintParentId = iParentId
            mintEmployeeId = iEmployeeId
            mintPeriodId = iPeriodId
            mintMainParentId = iMainParentId

            menAction = eAction

            Dim objMapping As New clsAssess_Field_Mapping
            mintLinkedFieldId = objMapping.Get_Map_FieldId(mintPeriodId)
            objMapping = Nothing

            If mintLinkedFieldId <> mintFieldUnkid Then
                objtabcRemarks.Enabled = False : objpnlData.Enabled = False
            End If

            Dim objPrd As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPrd._Periodunkid = mintPeriodId
            objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = mintPeriodId
            'Sohail (21 Aug 2015) -- End
            txtPeriod.Text = objPrd._Period_Name
            objPrd = Nothing

            Call Set_Form_Information()

            Me.ShowDialog()

            intUnkId = mintEmpField4Unkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub Set_Form_Information()
        Try
            Me.Text = Language.getMessage(mstrModuleName, 1, "Add/Edit Employee") & " " & objFieldMaster._Field4_Caption & " " & _
                      Language.getMessage(mstrModuleName, 2, "Information")

            objlblEmpField1.Text = objFieldMaster._Field1_Caption
            objlblEmpField2.Text = objFieldMaster._Field2_Caption
            objlblEmpField3.Text = objFieldMaster._Field3_Caption
            cboEmpFieldValue3.Tag = objFieldMaster._Field3Unkid

            objlblEmpField4.Text = objFieldMaster._Field4_Caption
            txtEmpField4.Tag = objFieldMaster._Field4Unkid
            If mintFieldUnkid = mintLinkedFieldId Then
                If objFieldMaster._Field6_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark1)
                Else
                    objtabpRemark1.Text = objFieldMaster._Field6_Caption
                    txtRemark1.Tag = objFieldMaster._Field6Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark2)
                Else
                    objtabpRemark2.Text = objFieldMaster._Field7_Caption
                    txtRemark2.Tag = objFieldMaster._Field7Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    objtabcRemarks.TabPages.Remove(objtabpRemark3)
                Else
                    objtabpRemark3.Text = objFieldMaster._Field8_Caption
                    txtRemark3.Tag = objFieldMaster._Field8Unkid
                    If mdicFieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicFieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicFieldData.Keys.Count > 0 Then objtabcRemarks.Enabled = True
                tblpAssessorEmployee.Enabled = ConfigParameter._Object._FollowEmployeeHierarchy
            Else
                objtabcRemarks.Enabled = False : objpnlData.Enabled = False
            End If
            Dim objEmp As New clsEmployee_Master
            objEmp._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = mintEmployeeId
            txtEmployeeName.Tag = mintEmployeeId
            If ConfigParameter._Object._FirstNamethenSurname Then
                txtEmployeeName.Text = objEmp._Employeecode & " - " & objEmp._Firstname & " " & objEmp._Surname
            Else
                txtEmployeeName.Text = objEmp._Employeecode & " - " & objEmp._Surname & " " & objEmp._Firstname
            End If
            objEmp = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Form_Information", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim objEmpField3 As New clsassess_empfield3_master
        Dim dsList As New DataSet
        Try
            RemoveHandler cboEmpFieldValue3.SelectedIndexChanged, AddressOf cboEmpFieldValue3_SelectedIndexChanged
            dsList = objEmpField3.getComboList(mintEmployeeId, mintPeriodId, mintMainParentId, "List", True)
            With cboEmpFieldValue3
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            cboEmpFieldValue3.DrawMode = DrawMode.OwnerDrawFixed
            AddHandler cboEmpFieldValue3.DrawItem, AddressOf cboEmpFieldValue3_DrawItem
            AddHandler cboEmpFieldValue3.DropDownClosed, AddressOf cboEmpFieldValue3_DropDownClosed
            AddHandler cboEmpFieldValue3.SelectedIndexChanged, AddressOf cboEmpFieldValue3_SelectedIndexChanged

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboStatus
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                'S.SANDEEP [16 JUN 2015] -- START
                '.SelectedValue = enCompGoalStatus.ST_PENDING
                .SelectedValue = 0
                .Text = ""
                'S.SANDEEP [16 JUN 2015] -- END

            End With

            'S.SANDEEP |18-JAN-2019| -- START
            dsList = objMData.GetGoalTypeList(ConfigParameter._Object._GoalTypeInclusion, "List", False)
            With cboGoalType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
            End With
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            Dim objCMaster As New clsCommon_Master
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.GOAL_UNIT_OF_MEASURE, True, "List")
            With cboUoM
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objCMaster = Nothing
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            txtEmpField4.BackColor = GUI.ColorComp
            cboStatus.BackColor = GUI.ColorComp
            txtWeight.BackColor = GUI.ColorComp
            cboEmpFieldValue3.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objEmpField4._Empfield3unkid = CInt(cboEmpFieldValue3.SelectedValue)
            objEmpField4._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD4
            If dtpEndDate.Checked = True Then
                objEmpField4._Enddate = dtpEndDate.Value
            Else
                objEmpField4._Enddate = Nothing
            End If
            objEmpField4._Field_Data = txtEmpField4.Text
            objEmpField4._Fieldunkid = mintFieldUnkid
            objEmpField4._Isvoid = False
            objEmpField4._Pct_Completed = txtPCT_Complete.Decimal
            If dtpStartDate.Checked = True Then
                objEmpField4._Startdate = dtpStartDate.Value
            Else
                objEmpField4._Startdate = Nothing
            End If
            objEmpField4._Statusunkid = CInt(cboStatus.SelectedValue)
            objEmpField4._Userunkid = User._Object._Userunkid
            objEmpField4._Voiddatetime = Nothing
            objEmpField4._Voidreason = ""
            objEmpField4._Voiduserunkid = -1
            objEmpField4._Weight = txtWeight.Decimal
            objEmpField4._Employeeunkid = mintEmployeeId
            objEmpField4._Periodunkid = mintPeriodId
            'S.SANDEEP |18-JAN-2019| -- START
            objEmpField4._GoalTypeid = CInt(cboGoalType.SelectedValue)
            objEmpField4._GoalValue = txtGoalValue.Decimal
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            objEmpField4._UnitOfMeasure = CInt(cboUoM.SelectedValue)
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboEmpFieldValue3.SelectedValue = objEmpField4._Empfield3unkid
            If objEmpField4._Enddate <> Nothing Then
                dtpEndDate.Value = objEmpField4._Enddate
                dtpEndDate.Checked = True
            End If
            txtEmpField4.Text = objEmpField4._Field_Data
            txtPCT_Complete.Decimal = objEmpField4._Pct_Completed
            If objEmpField4._Startdate <> Nothing Then
                dtpStartDate.Value = objEmpField4._Startdate
                dtpStartDate.Checked = True
            End If
            'S.SANDEEP [16 JUN 2015] -- START
            'cboStatus.SelectedValue = IIf(objEmpField4._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objEmpField4._Statusunkid)
            If objEmpField4._Statusunkid <= 0 Then
                cboStatus.SelectedValue = 0 : cboStatus.Text = ""
            Else
                cboStatus.SelectedValue = objEmpField4._Statusunkid
            End If
            'S.SANDEEP [16 JUN 2015] -- END

            'S.SANDEEP |18-JAN-2019| -- START
            cboGoalType.SelectedValue = objEmpField4._GoalTypeid
            txtGoalValue.Decimal = CDec(objEmpField4._GoalValue)
            'S.SANDEEP |18-JAN-2019| -- END


            'S.SANDEEP |27-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
            If mintEmpField4Unkid <= 0 Then
                If CInt(ConfigParameter._Object._GoalTypeInclusion) > 0 Then
                    cboGoalType.SelectedValue = CInt(ConfigParameter._Object._GoalTypeInclusion)
                End If
            End If
            'S.SANDEEP |27-NOV-2020| -- END


            txtWeight.Decimal = CDec(objEmpField4._Weight)
            If menAction = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_empinfofield_tran
                mdicFieldData = objInfoField.Get_Data(mintEmpField4Unkid, enWeight_Types.WEIGHT_FIELD4)
                If mdicFieldData.Keys.Count > 0 Then
                    If mdicFieldData.ContainsKey(CInt(txtRemark1.Tag)) Then
                        txtRemark1.Text = mdicFieldData(CInt(txtRemark1.Tag))
                    End If
                    If mdicFieldData.ContainsKey(CInt(txtRemark2.Tag)) Then
                        txtRemark2.Text = mdicFieldData(CInt(txtRemark2.Tag))
                    End If
                    If mdicFieldData.ContainsKey(CInt(txtRemark3.Tag)) Then
                        txtRemark3.Text = mdicFieldData(CInt(txtRemark3.Tag))
                    End If
                End If
                objInfoField = Nothing
            End If

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            cboUoM.SelectedValue = objEmpField4._UnitOfMeasure
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            Dim iMsg As String = String.Empty
            'If ConfigParameter._Object._CascadingTypeId = enPACascading.STRICT_CASCADING Then
            If CInt(cboEmpFieldValue3.SelectedValue) <= 0 Then
                iMsg = Language.getMessage(mstrModuleName, 3, "Sorry, ") & objFieldMaster._Field3_Caption & _
                       Language.getMessage(mstrModuleName, 4, " is mandatory information. Please select ") & objFieldMaster._Field3_Caption & _
                       Language.getMessage(mstrModuleName, 5, " to continue.")

                eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                cboEmpFieldValue3.Focus()
                Return False
            End If
            'End If

            If txtEmpField4.Text.Trim.Length <= 0 Then
                iMsg = Language.getMessage(mstrModuleName, 3, "Sorry, ") & objFieldMaster._Field4_Caption & _
                       Language.getMessage(mstrModuleName, 6, " is mandatory information. Please provide ") & objFieldMaster._Field4_Caption & _
                       Language.getMessage(mstrModuleName, 5, " to continue.")

                eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                txtEmpField4.Focus()
                Return False
            End If

            If mintFieldUnkid = mintLinkedFieldId Then

                If txtWeight.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Sorry, Weight is mandatory information. Please provide Weight to continue."), enMsgBoxStyle.Information)
                    txtWeight.Focus()
                    Return False
                End If

                If txtWeight.Decimal > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, enWeight_Types.WEIGHT_FIELD4, txtWeight.Decimal, mintPeriodId, mintLinkedFieldId, 0, mintEmployeeId, menAction, mintEmpField4Unkid)
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        eZeeMsgBox.Show(iMsg, enMsgBoxStyle.Information)
                        txtWeight.Focus()
                        Return False
                    End If
                End If
                'If txtWeight.Decimal > 100 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Weight cannot exceed 100. Please provide Weight between 1 to 100."), enMsgBoxStyle.Information)
                '    txtWeight.Focus()
                '    Return False
                'End If

                'S.SANDEEP [16 JUN 2015] -- START
                'If txtPCT_Complete.Decimal > 100 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Sorry, Percentage Completed cannot be greater than 100."), enMsgBoxStyle.Information)
                '    txtPCT_Complete.Focus()
                '    Return False
                'End If

                'If CInt(cboStatus.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, Status is mandatory information. Please select Status to continue."), enMsgBoxStyle.Information)
                '    cboStatus.Focus()
                '    Return False
                'End If
                'S.SANDEEP [16 JUN 2015] -- END

                'S.SANDEEP |18-JAN-2019| -- START
                If CInt(cboGoalType.SelectedValue) = enGoalType.GT_QUANTITATIVE Then
                    If txtGoalValue.Decimal <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, Goal Value is mandatory information. Please set goal value to continue."), enMsgBoxStyle.Information)
                        txtGoalValue.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP |18-JAN-2019| -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}

                'S.SANDEEP |05-JUN-2019| -- START
                'If CInt(cboUoM.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue."), enMsgBoxStyle.Information)
                '    cboUoM.Focus()
                '    Return False
                'End If
                If CInt(cboGoalType.SelectedValue) = enGoalType.GT_QUANTITATIVE Then
                If CInt(cboUoM.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue."), enMsgBoxStyle.Information)
                    cboUoM.Focus()
                    Return False
                End If
                End If
                'S.SANDEEP |05-JUN-2019| -- END


                'S.SANDEEP |12-FEB-2019| -- END

            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub Fill_Data()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Try
            RemoveHandler dgvOwner.CellContentClick, AddressOf dgvOwner_CellContentClick
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dList = objEmployee.GetEmployee_Access(mintEmployeeId, 0, mintPeriodId)
            dList = objEmployee.GetEmployee_Access(mintEmployeeId, 0, FinancialYear._Object._DatabaseName, mintPeriodId)
            'Sohail (21 Aug 2015) -- End

'S.SANDEEP [27 Jan 2016] -- START
            'DUPLICATION OF EMPLOYEE {If Employee Reporting To & Assessor/Reviewer is Same Employee}
            Dim dView As DataView = dList.Tables(0).DefaultView.ToTable(True, "ischeck", "employeecode", "employeename", "employeeunkid").DefaultView
            dList.Tables(0).Rows.Clear()
            dList.Tables.RemoveAt(0)
            dList.Tables.Add(dView.ToTable)
            'S.SANDEEP [27 Jan 2016] -- END

            If dList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    If mdtOwner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtOwner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,employeename"

            dgvOwner.AutoGenerateColumns = False
            objdgcolhECheck.DataPropertyName = "ischeck"
            dgcolhEcode.DataPropertyName = "employeecode"
            dgcolhEName.DataPropertyName = "employeename"
            objdgcolhEmpId.DataPropertyName = "employeeunkid"
            dgvOwner.DataSource = dtOwnerView

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged
            AddHandler dgvOwner.CellContentClick, AddressOf dgvOwner_CellContentClick

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub GoalOwnerOperation(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            If mdtOwner IsNot Nothing Then
                Dim dtmp() As DataRow = mdtOwner.Select("employeeunkid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtOwner.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("empfieldunkid") = mintEmpField4Unkid
                        dRow.Item("employeeunkid") = iTagUnkid
                        dRow.Item("empfieldtypeid") = enWeight_Types.WEIGHT_FIELD4
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtOwner.Rows.Add(dRow)
                    End If
                    mdtOwner.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GoalOwnerOperation", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub objfrmAddEditEmpField4_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objEmpField4 = New clsassess_empfield4_master
        objOwrOwner = New clsassess_empowner_tran
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetVisibility()
            Call SetColor()
            If menAction = enAction.EDIT_ONE Then
                objEmpField4._Empfield4unkid = mintEmpField4Unkid
                'cboEmpFieldValue3.Enabled = False : objbtnEmpSearchField3.Enabled = False
            End If
            mdtOwner = objOwrOwner.Get_Data(mintEmpField4Unkid, enWeight_Types.WEIGHT_FIELD4)
            Call Fill_Data()
            Call FillCombo()
            Call GetValue()
            If mintParentId > 0 Then
                cboEmpFieldValue3.SelectedValue = mintParentId
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmAddEditOwrField4_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objfrmAddEditEmpField4_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpField4 = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objfrmAddEditEmpField4_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_empfield4_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_empfield4_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

            Me.Text = Language.getMessage(mstrModuleName, 1, "Add/Edit Employee") & " " & objFieldMaster._Field4_Caption & " " & _
                      Language.getMessage(mstrModuleName, 2, "Information")

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim iblnFlag As Boolean = False
        Try
            If IsValidData() = False Then Exit Sub
            Call SetValue()

            'S.SANDEEP [28-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Audit Trails} 
            objEmpField4._FormName = mstrModuleName
            objEmpField4._Loginemployeeunkid = 0
            objEmpField4._ClientIP = getIP()
            objEmpField4._HostName = getHostName()
            objEmpField4._FromWeb = False
            objEmpField4._AuditUserId = User._Object._Userunkid
            objEmpField4._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'S.SANDEEP [28-May-2018] -- END

            If menAction = enAction.EDIT_ONE Then
                iblnFlag = objEmpField4.Update(mdicFieldData, mdtOwner)
            Else
                iblnFlag = objEmpField4.Insert(mdicFieldData, mdtOwner)
            End If
            If iblnFlag = False Then
                If objEmpField4._Message <> "" Then
                    eZeeMsgBox.Show(objEmpField4._Message, enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, problem in saving Employee Goals."), enMsgBoxStyle.Information)
                End If
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Employee Goals are saved successfully."), enMsgBoxStyle.Information)
                If menAction = enAction.ADD_CONTINUE Then
                    objEmpField4 = New clsassess_empfield4_master
                    RemoveHandler cboEmpFieldValue3.SelectedIndexChanged, AddressOf cboEmpFieldValue3_SelectedIndexChanged
                    Call GetValue()
                    txtRemark1.Text = "" : txtRemark2.Text = "" : txtRemark3.Text = ""
                    If mintParentId > 0 Then
                        cboEmpFieldValue3.SelectedValue = mintParentId
                    End If
                    AddHandler cboEmpFieldValue3.SelectedIndexChanged, AddressOf cboEmpFieldValue3_SelectedIndexChanged
                Else
                    Call btnClose_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnEmpSearchField3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnEmpSearchField3.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmpFieldValue3.ValueMember
                .DisplayMember = cboEmpFieldValue3.DisplayMember
                .DataSource = CType(cboEmpFieldValue3.DataSource, DataTable)
                If .DisplayDialog Then
                    cboEmpFieldValue3.SelectedValue = .SelectedValue
                    cboEmpFieldValue3.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnEmpSearchField3_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub txtRemark1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark1.TextChanged
        Try
            mdicFieldData(CInt(txtRemark1.Tag)) = txtRemark1.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark1_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark2.TextChanged
        Try
            mdicFieldData(CInt(txtRemark2.Tag)) = txtRemark2.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark2_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtRemark3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRemark3.TextChanged
        Try
            mdicFieldData(CInt(txtRemark3.Tag)) = txtRemark3.Text
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtRemark3_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmpFieldValue3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmpFieldValue3.SelectedIndexChanged
        Try
            mintParentId = CInt(cboEmpFieldValue3.SelectedValue)
            If CInt(cboEmpFieldValue3.SelectedValue) > 0 Then
                Dim objField3 As New clsassess_empfield3_master
                Dim objField2 As New clsassess_empfield2_master
                Dim objField1 As New clsassess_empfield1_master
                objField3._Empfield3unkid = CInt(cboEmpFieldValue3.SelectedValue)
                objField2._Empfield2unkid = objField3._Empfield2unkid
                objField1._Empfield1unkid = objField2._Empfield1unkid
                txtEmpField2.Text = objField2._Field_Data
                txtEmpField1.Text = objField1._Field_Data
                'txtEmployeeName.Text = objField1._EmployeeName
                'txtEmployeeName.Tag = objField1._Employeeunkid
                objField3 = Nothing : objField2 = Nothing : objField1 = Nothing
            Else
                txtEmpField1.Text = ""
                txtEmpField2.Text = "" 'txtEmployeeName.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboOwrFieldValue3_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmpFieldValue3_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpFieldValue3.DropDown
        Try
            mblnDropDownClosed = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpFieldValue3_DropDown", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmpFieldValue3_DropDownClosed(ByVal sender As Object, ByVal e As EventArgs)
        Try
            ToolTip1.Hide(cboEmpFieldValue3) : mblnDropDownClosed = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpFieldValue3_DropDownClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboEmpFieldValue3_DrawItem(ByVal sender As Object, ByVal e As DrawItemEventArgs)
        Try
            If e.Index < 0 Then
                Return
            End If
            Dim text As String = cboEmpFieldValue3.GetItemText(cboEmpFieldValue3.Items(e.Index))
            e.DrawBackground()
            Using br As New SolidBrush(e.ForeColor)
                e.Graphics.DrawString(text, e.Font, br, e.Bounds)
            End Using
            If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                If mblnDropDownClosed = False Then
                    ToolTip1.Show(text, cboEmpFieldValue3, e.Bounds.Right, e.Bounds.Bottom)
                End If
            End If
            e.DrawFocusRectangle()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpFieldValue3_DrawItem", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtSearchEmp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearchEmp.KeyDown
        Try
            Select Case e.KeyCode
                Case Windows.Forms.Keys.Down
                    If dgvOwner.Rows.Count > 0 Then
                        If dgvOwner.SelectedRows(0).Index = dgvOwner.Rows(dgvOwner.RowCount - 1).Index Then Exit Sub
                        dgvOwner.Rows(dgvOwner.SelectedRows(0).Index + 1).Selected = True
                    End If
                Case Windows.Forms.Keys.Up
                    If dgvOwner.Rows.Count > 0 Then
                        If dgvOwner.SelectedRows(0).Index = 0 Then Exit Sub
                        dgvOwner.Rows(dgvOwner.SelectedRows(0).Index - 1).Selected = True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "txtSearchEmp_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearchEmp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = dgcolhEcode.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            dgcolhEName.DataPropertyName & " LIKE '%" & txtSearchEmp.Text & "%'"
            End If
            dtOwnerView.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchEmp_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvOwner_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOwner.CellContentClick
        Try
            RemoveHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhECheck.Index Then

                If Me.dgvOwner.IsCurrentCellDirty Then
                    Me.dgvOwner.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                Call GoalOwnerOperation(CInt(dgvOwner.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value), CBool(dgvOwner.Rows(e.RowIndex).Cells(objdgcolhECheck.Index).Value))

                Dim drRow As DataRow() = dtOwnerView.ToTable.Select("ischeck = true", "")
                If drRow.Length > 0 Then
                    If dtOwnerView.ToTable.Rows.Count = drRow.Length Then
                        objchkEmployee.CheckState = CheckState.Checked
                    Else
                        objchkEmployee.CheckState = CheckState.Indeterminate
                    End If
                Else
                    objchkEmployee.CheckState = CheckState.Unchecked
                End If
            End If

            AddHandler objchkEmployee.CheckedChanged, AddressOf objchkEmployee_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvOwner_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkEmployee.CheckedChanged
        Try
            RemoveHandler dgvOwner.CellContentClick, AddressOf dgvOwner_CellContentClick
            For Each dr As DataRowView In dtOwnerView
                dr.Item("ischeck") = CBool(objchkEmployee.CheckState)
                Call GoalOwnerOperation(CInt(dr.Item("employeeunkid")), CBool(objchkEmployee.CheckState))
            Next
            dgvOwner.Refresh()
            AddHandler dgvOwner.CellContentClick, AddressOf dgvOwner_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkEmployee_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvOwner_ColumnWidthChanged(ByVal sender As Object, ByVal e As DataGridViewColumnEventArgs) Handles dgvOwner.ColumnWidthChanged
        Dim rtHeader As Rectangle = Me.dgvOwner.DisplayRectangle
        rtHeader.Height = CInt(Me.dgvOwner.ColumnHeadersHeight / 2)
        Me.dgvOwner.Invalidate(rtHeader)
    End Sub

    Private Sub dgvOwner_Scroll(ByVal sender As Object, ByVal e As ScrollEventArgs) Handles dgvOwner.Scroll
        Dim rtHeader As Rectangle = Me.dgvOwner.DisplayRectangle
        rtHeader.Height = CInt(Me.dgvOwner.ColumnHeadersHeight / 2)
        Me.dgvOwner.Invalidate(rtHeader)
    End Sub

    Private Sub dgvOwner_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles dgvOwner.Paint
        Dim j As Integer = 1
        While j < dgvOwner.ColumnCount - 1
            Dim r1 As Rectangle = Me.dgvOwner.GetCellDisplayRectangle(j, -1, True)
            Dim w2 As Integer = Me.dgvOwner.GetCellDisplayRectangle(j + 1, -1, True).Width
            Dim w3 As Integer = Me.dgvOwner.GetCellDisplayRectangle(j + 1, -1, True).Height
            'r1.X += 1
            r1.Y += 1
            r1.Width = r1.Width + w2 - 2
            'r1.Height = CInt(r1.Height / 2 - 2)
            r1.Height = w3 - 5
            e.Graphics.FillRectangle(New SolidBrush(Me.dgvOwner.ColumnHeadersDefaultCellStyle.BackColor), r1)
            Dim format As New StringFormat()
            format.Alignment = StringAlignment.Center
            format.LineAlignment = StringAlignment.Center
            e.Graphics.DrawString(Language.getMessage(mstrModuleName, 12, "Assigned To"), Me.dgvOwner.ColumnHeadersDefaultCellStyle.Font, New SolidBrush(Me.dgvOwner.ColumnHeadersDefaultCellStyle.ForeColor), r1, format)
            j += 2
        End While
    End Sub

    Private Sub dgvOwner_CellPainting(ByVal sender As Object, ByVal e As DataGridViewCellPaintingEventArgs) Handles dgvOwner.CellPainting
        If e.RowIndex = -1 AndAlso e.ColumnIndex > -1 Then
            Dim r2 As Rectangle = e.CellBounds
            r2.Y = CInt(r2.Y + e.CellBounds.Height / 2)
            r2.Height = CInt(e.CellBounds.Height / 2)
            e.PaintBackground(r2, True)
            e.PaintContent(r2)
            e.Handled = True
        End If
    End Sub

    'S.SANDEEP |18-JAN-2019| -- START
    Private Sub cboGoalType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGoalType.SelectedIndexChanged
        Try
            If CInt(cboGoalType.SelectedValue) = enGoalType.GT_QUALITATIVE Then
                txtGoalValue.Enabled = False : txtGoalValue.Decimal = 0
            Else
                txtGoalValue.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboGoalType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |18-JAN-2019| -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
			Me.dgcolhEName.HeaderText = Language._Object.getCaption(Me.dgcolhEName.Name, Me.dgcolhEName.HeaderText)
			Me.lblPercentage.Text = Language._Object.getCaption(Me.lblPercentage.Name, Me.lblPercentage.Text)
			Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.Name, Me.lblEndDate.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblWeight.Text = Language._Object.getCaption(Me.lblWeight.Name, Me.lblWeight.Text)
            Me.lblGoalValue.Text = Language._Object.getCaption(Me.lblGoalValue.Name, Me.lblGoalValue.Text)
            Me.lblGoalType.Text = Language._Object.getCaption(Me.lblGoalType.Name, Me.lblGoalType.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Add/Edit Employee")
			Language.setMessage(mstrModuleName, 2, "Information")
			Language.setMessage(mstrModuleName, 3, "Sorry,")
			Language.setMessage(mstrModuleName, 4, " is mandatory information. Please select")
			Language.setMessage(mstrModuleName, 5, " to continue.")
			Language.setMessage(mstrModuleName, 6, " is mandatory information. Please provide")
			Language.setMessage(mstrModuleName, 7, "Sorry, Weight is mandatory information. Please provide Weight to continue.")
			Language.setMessage(mstrModuleName, 9, "Sorry, problem in saving Employee Goals.")
			Language.setMessage(mstrModuleName, 10, "Employee Goals are saved successfully.")
			Language.setMessage(mstrModuleName, 12, "Assigned To")
            Language.setMessage(mstrModuleName, 100, "Sorry, Goal Value is mandatory information. Please set goal value to continue.")
            Language.setMessage(mstrModuleName, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class