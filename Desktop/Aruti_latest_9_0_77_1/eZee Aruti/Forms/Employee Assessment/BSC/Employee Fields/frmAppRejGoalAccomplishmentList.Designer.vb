﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAppRejGoalAccomplishmentList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAppRejGoalAccomplishmentList))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchEmp = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.picStayView = New System.Windows.Forms.PictureBox
        Me.objlblTotalWeight = New System.Windows.Forms.Label
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnAccomplishedReject = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAccomplishedApprove = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblRemark = New System.Windows.Forms.Label
        Me.txtRemark = New System.Windows.Forms.TextBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.objChkAllSelect = New System.Windows.Forms.CheckBox
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhApprove = New System.Windows.Forms.DataGridViewLinkColumn
        Me.objdgcolhReject = New System.Windows.Forms.DataGridViewLinkColumn
        Me.objchkSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgvcolhFiledData = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvcolhComplatePer = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgvcolhProgressRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvcolhAccomplishedStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgvcolhfiledunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgvcolhfiledUpdatetranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgvcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgvcolhAccomplishmentStatusid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmp)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 71
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(786, 66)
        Me.gbFilterCriteria.TabIndex = 6
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmp
        '
        Me.objbtnSearchEmp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmp.BorderSelected = False
        Me.objbtnSearchEmp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmp.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmp.Location = New System.Drawing.Point(298, 31)
        Me.objbtnSearchEmp.Name = "objbtnSearchEmp"
        Me.objbtnSearchEmp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmp.TabIndex = 481
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(12, 34)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(84, 15)
        Me.lblEmployee.TabIndex = 479
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(102, 31)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(190, 21)
        Me.cboEmployee.TabIndex = 480
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(325, 34)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(59, 15)
        Me.lblPeriod.TabIndex = 476
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.DropDownWidth = 250
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(627, 31)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(146, 21)
        Me.cboStatus.TabIndex = 71
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 250
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(390, 31)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(153, 21)
        Me.cboPeriod.TabIndex = 477
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(549, 34)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(72, 15)
        Me.lblStatus.TabIndex = 72
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(759, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 8
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(736, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 7
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.picStayView)
        Me.objFooter.Controls.Add(Me.objlblTotalWeight)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 445)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(786, 55)
        Me.objFooter.TabIndex = 448
        '
        'picStayView
        '
        Me.picStayView.Location = New System.Drawing.Point(340, 20)
        Me.picStayView.Name = "picStayView"
        Me.picStayView.Size = New System.Drawing.Size(15, 15)
        Me.picStayView.TabIndex = 450
        Me.picStayView.TabStop = False
        Me.picStayView.Visible = False
        '
        'objlblTotalWeight
        '
        Me.objlblTotalWeight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblTotalWeight.Location = New System.Drawing.Point(461, 22)
        Me.objlblTotalWeight.Name = "objlblTotalWeight"
        Me.objlblTotalWeight.Size = New System.Drawing.Size(213, 13)
        Me.objlblTotalWeight.TabIndex = 136
        Me.objlblTotalWeight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(680, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnAccomplishedReject)
        Me.Panel1.Controls.Add(Me.btnAccomplishedApprove)
        Me.Panel1.Controls.Add(Me.lblRemark)
        Me.Panel1.Controls.Add(Me.txtRemark)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 66)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(786, 379)
        Me.Panel1.TabIndex = 449
        '
        'btnAccomplishedReject
        '
        Me.btnAccomplishedReject.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccomplishedReject.BackColor = System.Drawing.Color.White
        Me.btnAccomplishedReject.BackgroundImage = CType(resources.GetObject("btnAccomplishedReject.BackgroundImage"), System.Drawing.Image)
        Me.btnAccomplishedReject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAccomplishedReject.BorderColor = System.Drawing.Color.Empty
        Me.btnAccomplishedReject.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAccomplishedReject.FlatAppearance.BorderSize = 0
        Me.btnAccomplishedReject.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAccomplishedReject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccomplishedReject.ForeColor = System.Drawing.Color.Black
        Me.btnAccomplishedReject.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAccomplishedReject.GradientForeColor = System.Drawing.Color.Black
        Me.btnAccomplishedReject.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAccomplishedReject.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAccomplishedReject.Location = New System.Drawing.Point(680, 328)
        Me.btnAccomplishedReject.Name = "btnAccomplishedReject"
        Me.btnAccomplishedReject.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAccomplishedReject.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAccomplishedReject.Size = New System.Drawing.Size(94, 30)
        Me.btnAccomplishedReject.TabIndex = 484
        Me.btnAccomplishedReject.Text = "&Reject"
        Me.btnAccomplishedReject.UseVisualStyleBackColor = True
        '
        'btnAccomplishedApprove
        '
        Me.btnAccomplishedApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAccomplishedApprove.BackColor = System.Drawing.Color.White
        Me.btnAccomplishedApprove.BackgroundImage = CType(resources.GetObject("btnAccomplishedApprove.BackgroundImage"), System.Drawing.Image)
        Me.btnAccomplishedApprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAccomplishedApprove.BorderColor = System.Drawing.Color.Empty
        Me.btnAccomplishedApprove.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAccomplishedApprove.FlatAppearance.BorderSize = 0
        Me.btnAccomplishedApprove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAccomplishedApprove.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccomplishedApprove.ForeColor = System.Drawing.Color.Black
        Me.btnAccomplishedApprove.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAccomplishedApprove.GradientForeColor = System.Drawing.Color.Black
        Me.btnAccomplishedApprove.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAccomplishedApprove.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAccomplishedApprove.Location = New System.Drawing.Point(580, 328)
        Me.btnAccomplishedApprove.Name = "btnAccomplishedApprove"
        Me.btnAccomplishedApprove.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAccomplishedApprove.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAccomplishedApprove.Size = New System.Drawing.Size(94, 30)
        Me.btnAccomplishedApprove.TabIndex = 137
        Me.btnAccomplishedApprove.Text = "&Approve"
        Me.btnAccomplishedApprove.UseVisualStyleBackColor = True
        '
        'lblRemark
        '
        Me.lblRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemark.Location = New System.Drawing.Point(12, 323)
        Me.lblRemark.Name = "lblRemark"
        Me.lblRemark.Size = New System.Drawing.Size(84, 35)
        Me.lblRemark.TabIndex = 483
        Me.lblRemark.Text = "Approve/Reject Remark"
        Me.lblRemark.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtRemark
        '
        Me.txtRemark.Location = New System.Drawing.Point(102, 323)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.Size = New System.Drawing.Size(441, 50)
        Me.txtRemark.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.objChkAllSelect)
        Me.Panel2.Controls.Add(Me.dgvData)
        Me.Panel2.Location = New System.Drawing.Point(3, 2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(779, 315)
        Me.Panel2.TabIndex = 0
        '
        'objChkAllSelect
        '
        Me.objChkAllSelect.AutoSize = True
        Me.objChkAllSelect.Location = New System.Drawing.Point(5, 4)
        Me.objChkAllSelect.Name = "objChkAllSelect"
        Me.objChkAllSelect.Size = New System.Drawing.Size(15, 14)
        Me.objChkAllSelect.TabIndex = 7
        Me.objChkAllSelect.UseVisualStyleBackColor = True
        Me.objChkAllSelect.Visible = False
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhApprove, Me.objdgcolhReject, Me.objchkSelect, Me.dgvcolhFiledData, Me.dgvcolhStatus, Me.dgvcolhComplatePer, Me.dgvcolhProgressRemark, Me.dgvcolhAccomplishedStatus, Me.objdgvcolhfiledunkid, Me.objdgvcolhfiledUpdatetranunkid, Me.objdgvcolhIsGrp, Me.objdgvcolhAccomplishmentStatusid})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(779, 315)
        Me.dgvData.TabIndex = 6
        '
        'objdgcolhApprove
        '
        Me.objdgcolhApprove.ActiveLinkColor = System.Drawing.Color.Blue
        Me.objdgcolhApprove.Frozen = True
        Me.objdgcolhApprove.HeaderText = "Approve"
        Me.objdgcolhApprove.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objdgcolhApprove.Name = "objdgcolhApprove"
        Me.objdgcolhApprove.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhApprove.Text = "Approve"
        Me.objdgcolhApprove.UseColumnTextForLinkValue = True
        Me.objdgcolhApprove.Visible = False
        Me.objdgcolhApprove.VisitedLinkColor = System.Drawing.Color.Blue
        Me.objdgcolhApprove.Width = 60
        '
        'objdgcolhReject
        '
        Me.objdgcolhReject.ActiveLinkColor = System.Drawing.Color.Blue
        Me.objdgcolhReject.Frozen = True
        Me.objdgcolhReject.HeaderText = "Reject"
        Me.objdgcolhReject.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objdgcolhReject.Name = "objdgcolhReject"
        Me.objdgcolhReject.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhReject.Text = "Reject"
        Me.objdgcolhReject.UseColumnTextForLinkValue = True
        Me.objdgcolhReject.Visible = False
        Me.objdgcolhReject.VisitedLinkColor = System.Drawing.Color.Blue
        Me.objdgcolhReject.Width = 60
        '
        'objchkSelect
        '
        Me.objchkSelect.HeaderText = ""
        Me.objchkSelect.Name = "objchkSelect"
        Me.objchkSelect.Width = 25
        '
        'dgvcolhFiledData
        '
        Me.dgvcolhFiledData.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgvcolhFiledData.HeaderText = "Filed Data"
        Me.dgvcolhFiledData.Name = "dgvcolhFiledData"
        Me.dgvcolhFiledData.ReadOnly = True
        '
        'dgvcolhStatus
        '
        Me.dgvcolhStatus.HeaderText = "Status"
        Me.dgvcolhStatus.Name = "dgvcolhStatus"
        Me.dgvcolhStatus.ReadOnly = True
        '
        'dgvcolhComplatePer
        '
        Me.dgvcolhComplatePer.AllowNegative = False
        Me.dgvcolhComplatePer.DecimalLength = 2
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F2"
        Me.dgvcolhComplatePer.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvcolhComplatePer.HeaderText = "Complate %"
        Me.dgvcolhComplatePer.Name = "dgvcolhComplatePer"
        Me.dgvcolhComplatePer.ReadOnly = True
        Me.dgvcolhComplatePer.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvcolhComplatePer.ShowNullWhenZero = True
        Me.dgvcolhComplatePer.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgvcolhComplatePer.Width = 80
        '
        'dgvcolhProgressRemark
        '
        Me.dgvcolhProgressRemark.HeaderText = "Progress Remark"
        Me.dgvcolhProgressRemark.Name = "dgvcolhProgressRemark"
        Me.dgvcolhProgressRemark.ReadOnly = True
        Me.dgvcolhProgressRemark.Width = 150
        '
        'dgvcolhAccomplishedStatus
        '
        Me.dgvcolhAccomplishedStatus.HeaderText = "Goal Accomplishment Status"
        Me.dgvcolhAccomplishedStatus.Name = "dgvcolhAccomplishedStatus"
        Me.dgvcolhAccomplishedStatus.ReadOnly = True
        Me.dgvcolhAccomplishedStatus.Width = 150
        '
        'objdgvcolhfiledunkid
        '
        Me.objdgvcolhfiledunkid.HeaderText = "objdgvcolhfiledunkid"
        Me.objdgvcolhfiledunkid.Name = "objdgvcolhfiledunkid"
        Me.objdgvcolhfiledunkid.ReadOnly = True
        Me.objdgvcolhfiledunkid.Visible = False
        '
        'objdgvcolhfiledUpdatetranunkid
        '
        Me.objdgvcolhfiledUpdatetranunkid.HeaderText = "objdgvcolhfiledUpdatetranunkid"
        Me.objdgvcolhfiledUpdatetranunkid.Name = "objdgvcolhfiledUpdatetranunkid"
        Me.objdgvcolhfiledUpdatetranunkid.ReadOnly = True
        Me.objdgvcolhfiledUpdatetranunkid.Visible = False
        '
        'objdgvcolhIsGrp
        '
        Me.objdgvcolhIsGrp.HeaderText = "objdgvcolhIsGrp"
        Me.objdgvcolhIsGrp.Name = "objdgvcolhIsGrp"
        Me.objdgvcolhIsGrp.ReadOnly = True
        Me.objdgvcolhIsGrp.Visible = False
        '
        'objdgvcolhAccomplishmentStatusid
        '
        Me.objdgvcolhAccomplishmentStatusid.HeaderText = "objdgvcolhAccomplishmentStatusid"
        Me.objdgvcolhAccomplishmentStatusid.Name = "objdgvcolhAccomplishmentStatusid"
        Me.objdgvcolhAccomplishmentStatusid.ReadOnly = True
        Me.objdgvcolhAccomplishmentStatusid.Visible = False
        '
        'frmAppRejGoalAccomplishmentList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(786, 500)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAppRejGoalAccomplishmentList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Approve/Reject Goals Accomplishment  List"
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        CType(Me.picStayView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchEmp As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents objlblTotalWeight As System.Windows.Forms.Label
    Friend WithEvents objChkAllSelect As System.Windows.Forms.CheckBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblRemark As System.Windows.Forms.Label
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents btnAccomplishedApprove As eZee.Common.eZeeLightButton
    Friend WithEvents btnAccomplishedReject As eZee.Common.eZeeLightButton
    Friend WithEvents picStayView As System.Windows.Forms.PictureBox
    Friend WithEvents objdgcolhApprove As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents objdgcolhReject As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents objchkSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgvcolhFiledData As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvcolhComplatePer As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgvcolhProgressRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvcolhAccomplishedStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgvcolhfiledunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgvcolhfiledUpdatetranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgvcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgvcolhAccomplishmentStatusid As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
