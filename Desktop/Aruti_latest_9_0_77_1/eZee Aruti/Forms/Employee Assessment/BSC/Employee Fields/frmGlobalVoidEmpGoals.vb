﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGlobalVoidEmpGoals

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGlobalVoidEmpGoals"
    Private mblnIsCancel As Boolean = True
    Private mdtFinal As DataTable = Nothing
    Private objFMaster As New clsAssess_Field_Master(True)
    Private cmnuDelete As ContextMenuStrip

    'S.SANDEEP [07-OCT-2017] -- START
    'ISSUE/ENHANCEMENT : IMPORT GOALS
    Private mintPeriodId As Integer = 0
    Private mintEmployeeId As Integer = 0
    Private mblnIsPreview As Boolean = False
    'S.SANDEEP [07-OCT-2017] -- END

#End Region

    'S.SANDEEP [07-OCT-2017] -- START
    'ISSUE/ENHANCEMENT : IMPORT GOALS
#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property
    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property
    Public WriteOnly Property _IsPreview() As Boolean
        Set(ByVal value As Boolean)
            mblnIsPreview = value
        End Set
    End Property

#End Region
    'S.SANDEEP [07-OCT-2017] -- END

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, False, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dtFinal As DataTable
        Dim iSearch As String = String.Empty
        Try

            If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, No data defined for asseessment caption settings screen."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim objMap As New clsAssess_Field_Mapping
            If objMap.isExist(CInt(cboPeriod.SelectedValue)) = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry no field is mapped with the selected period."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            objMap = Nothing

            If User._Object.Privilege._AllowtoViewEmployeeGoalsList = False Then Exit Sub
            Dim objEmpField1 = New clsassess_empfield1_master
            dtFinal = objEmpField1.GetDisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List")
            objEmpField1 = Nothing
            mdtFinal = New DataView(dtFinal, "", "empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable

            mdtFinal.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
            mdtFinal.Columns("ischeck").SetOrdinal(0)

            dgvData.AutoGenerateColumns = False
            Dim iColName As String = String.Empty

            Dim iPlan() As String = Nothing
            If ConfigParameter._Object._ViewTitles_InPlanning.Trim.Length > 0 Then
                iPlan = ConfigParameter._Object._ViewTitles_InPlanning.Split(CChar("|"))
            End If

            For Each dCol As DataColumn In mdtFinal.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                If dgvData.Columns.Contains(iColName) = True Then Continue For
                If dCol.ColumnName = "ischeck" Then
                    Dim dgvCol As New DataGridViewCheckBoxColumn()
                    dgvCol.Name = iColName
                    dgvCol.Width = 25
                    dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dgvCol.Frozen = True
                    dgvCol.HeaderText = ""
                    dgvCol.DataPropertyName = dCol.ColumnName
                    dgvCol.Resizable = DataGridViewTriState.False
                    dgvData.Columns.Add(dgvCol)
                Else
                    Dim dgvCol As New DataGridViewTextBoxColumn()
                    dgvCol.Name = iColName
                    dgvCol.Width = 120
                    dgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
                    dgvCol.ReadOnly = True
                    dgvCol.DataPropertyName = dCol.ColumnName
                    dgvCol.HeaderText = dCol.Caption
                    If dCol.Caption.Length <= 0 Then
                        dgvCol.Visible = False
                    Else
                        If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                            dgvCol.Width = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, CInt(mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)))
                            If iPlan IsNot Nothing Then
                                If Array.IndexOf(iPlan, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                    dgvCol.Visible = False
                                End If
                            End If
                        End If
                    End If
                    If dCol.ColumnName = "Emp" Then
                        dgvCol.Visible = False
                    End If
                    If dCol.ColumnName = "OPeriod" Then
                        dgvCol.Visible = False
                    End If
                    If ConfigParameter._Object._CascadingTypeId = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                        If dCol.ColumnName.StartsWith("Owr") Then
                            dgvCol.Visible = False
                        End If
                    End If
                    'S.SANDEEP [01 JUL 2015] -- START
                    If dCol.ColumnName = "vuRemark" Or dCol.ColumnName = "vuProgress" Then
                        dgvCol.Visible = False
                    End If
                    'S.SANDEEP [01 JUL 2015] -- END
                    dgvData.Columns.Add(dgvCol)
                End If
            Next
            dgvData.DataSource = mdtFinal

            For Each dgvRow As DataGridViewRow In dgvData.Rows
                'S.SANDEEP |10-AUG-2021| -- START
                'If CBool(dgvRow.Cells("objisfinal").Value) = True Then
                '    btnVoid.Enabled = False
                '    dgvRow.DefaultCellStyle.ForeColor = Color.Blue
                '    dgvRow.DefaultCellStyle.SelectionBackColor = Color.White
                '    dgvRow.DefaultCellStyle.SelectionForeColor = Color.Blue
                '    objChkAll.Enabled = False
                '    dgvRow.Cells("objischeck").ReadOnly = True
                'ElseIf CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.SUBMIT_APPROVAL Then
                '    btnVoid.Enabled = False
                '    dgvRow.DefaultCellStyle.ForeColor = Color.Green
                '    dgvRow.DefaultCellStyle.SelectionBackColor = Color.White
                '    dgvRow.DefaultCellStyle.SelectionForeColor = Color.Green
                '    objChkAll.Enabled = False
                '    dgvRow.Cells("objischeck").ReadOnly = True
                'Else
                '    btnVoid.Enabled = True
                '    dgvRow.DefaultCellStyle.ForeColor = Color.Black
                '    dgvRow.DefaultCellStyle.SelectionBackColor = Color.White
                '    dgvRow.DefaultCellStyle.SelectionForeColor = Color.Black
                '    objChkAll.Enabled = True
                '    dgvRow.Cells("objischeck").ReadOnly = False
                'End If

                If CBool(dgvRow.Cells("objisfinal").Value) = True Then
                    btnVoid.Enabled = False
                    dgvRow.DefaultCellStyle.ForeColor = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.FINAL_SAVE), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.FINAL_SAVE)), Color.Blue)
                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.White
                    dgvRow.DefaultCellStyle.SelectionForeColor = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.FINAL_SAVE), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.FINAL_SAVE)), Color.Blue)
                    objChkAll.Enabled = False
                    dgvRow.Cells("objischeck").ReadOnly = True
                ElseIf CInt(dgvRow.Cells("objopstatusid").Value) = enObjective_Status.SUBMIT_APPROVAL Then
                    btnVoid.Enabled = False
                    dgvRow.DefaultCellStyle.ForeColor = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.SUBMIT_APPROVAL), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.SUBMIT_APPROVAL)), Color.Green)
                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.White
                    dgvRow.DefaultCellStyle.SelectionForeColor = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.SUBMIT_APPROVAL), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.SUBMIT_APPROVAL)), Color.Green)
                    objChkAll.Enabled = False
                    dgvRow.Cells("objischeck").ReadOnly = True
                Else
                    btnVoid.Enabled = True
                    Dim cLr As Color
                    Select Case CInt(dgvRow.Cells("objopstatusid").Value)
                        Case enObjective_Status.OPEN_CHANGES
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.OPEN_CHANGES), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.OPEN_CHANGES)), Color.Black)
                        Case enObjective_Status.NOT_SUBMIT
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.NOT_SUBMIT), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.NOT_SUBMIT)), Color.Red)
                        Case enObjective_Status.NOT_COMMITTED
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.NOT_COMMITTED), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.NOT_COMMITTED)), Color.Purple)
                        Case enObjective_Status.FINAL_COMMITTED
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.FINAL_COMMITTED), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.FINAL_COMMITTED)), Color.Blue)
                        Case enObjective_Status.PERIODIC_REVIEW
                            cLr = IIf(ConfigParameter._Object._BSC_StatusColors.ContainsKey(enObjective_Status.PERIODIC_REVIEW), Color.FromArgb(ConfigParameter._Object._BSC_StatusColors(enObjective_Status.PERIODIC_REVIEW)), Color.Brown)
                    End Select
                    dgvRow.DefaultCellStyle.ForeColor = cLr
                    dgvRow.DefaultCellStyle.SelectionBackColor = Color.White
                    dgvRow.DefaultCellStyle.SelectionForeColor = cLr
                    objChkAll.Enabled = True
                    dgvRow.Cells("objischeck").ReadOnly = False
                End If
                'S.SANDEEP |10-AUG-2021| -- END
            Next

            Dim objFMapping As New clsAssess_Field_Mapping
            Dim xTotalWeight As Double = 0
            xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
            If xTotalWeight > 0 Then
                objlblTotalWeight.Text = Language.getMessage(mstrModuleName, 3, "Total Weight Assigned :") & " " & xTotalWeight.ToString
            Else
                objlblTotalWeight.Text = ""
            End If
            objFMapping = Nothing

            'S.SANDEEP [07-OCT-2017] -- START
            'ISSUE/ENHANCEMENT : IMPORT GOALS
            If mblnIsPreview Then
                objChkAll.Visible = False
                dgvData.Columns(0).Visible = False
            End If
            'S.SANDEEP [07-OCT-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Grid", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Generate_MenuItems()
        Dim dsList As New DataSet
        Try
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            If dsList.Tables(0).Rows.Count > 0 Then
                cmnuDelete.Items.Clear()
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    Dim objbtnDel As New ToolStripMenuItem
                    objbtnDel.Name = "objbtnDel" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    objbtnDel.Text = Language.getMessage(mstrModuleName, 6, "Delete") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    objbtnDel.Tag = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    cmnuDelete.Items.Add(objbtnDel)
                    AddHandler objbtnDel.Click, AddressOf objbtnDel_Click
                Next
            End If
            btnVoid.SplitButtonMenu = cmnuDelete
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_MenuItems", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub VoidCheckedData(ByVal intFieldTypeId As Integer, ByVal strVoidReasion As String, ByVal drow() As DataRow)
        Try
            For iRdx As Integer = 0 To drow.Length - 1
                Select Case intFieldTypeId
                    Case 1  'EMP FIELD 1 UNKID

                        Dim objEmpField1 As New clsassess_empfield1_master
                        If CInt(drow(iRdx).Item("empfield1unkid")) > 0 Then
                            objEmpField1._Isvoid = True
                            objEmpField1._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField1._Voidreason = strVoidReasion
                            objEmpField1._Voiduserunkid = User._Object._Userunkid
                            With objEmpField1
                                ._FormName = mstrModuleName
                                ._Loginemployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            objEmpField1.Delete(CInt(drow(iRdx).Item("empfield1unkid")))
                        End If
                        objEmpField1 = Nothing

                    Case 2  'EMP FIELD 2 UNKID

                        Dim objEmpField2 As New clsassess_empfield2_master
                        If CInt(drow(iRdx).Item("empfield2unkid")) > 0 Then
                            objEmpField2._Isvoid = True
                            objEmpField2._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField2._Voidreason = strVoidReasion
                            objEmpField2._Voiduserunkid = User._Object._Userunkid
                            With objEmpField2
                                ._FormName = mstrModuleName
                                ._Loginemployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            objEmpField2.Delete(CInt(drow(iRdx).Item("empfield2unkid")))
                        End If
                        objEmpField2 = Nothing

                    Case 3  'EMP FIELD 3 UNKID

                        Dim objEmpField3 As New clsassess_empfield3_master
                        If CInt(drow(iRdx).Item("empfield3unkid")) > 0 Then
                            objEmpField3._Isvoid = True
                            objEmpField3._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField3._Voidreason = strVoidReasion
                            objEmpField3._Voiduserunkid = User._Object._Userunkid
                            With objEmpField3
                                ._FormName = mstrModuleName
                                ._Loginemployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            objEmpField3.Delete(CInt(drow(iRdx).Item("empfield3unkid")))
                        End If
                        objEmpField3 = Nothing

                    Case 4  'EMP FIELD 4 UNKID

                        Dim objEmpField4 As New clsassess_empfield4_master
                        If CInt(drow(iRdx).Item("empfield4unkid")) > 0 Then
                            objEmpField4._Isvoid = True
                            objEmpField4._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField4._Voidreason = strVoidReasion
                            objEmpField4._Voiduserunkid = User._Object._Userunkid
                            With objEmpField4
                                ._FormName = mstrModuleName
                                ._Loginemployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            objEmpField4.Delete(CInt(drow(iRdx).Item("empfield4unkid")))
                        End If
                        objEmpField4 = Nothing

                    Case 5  'EMP FIELD 5 UNKID

                        Dim objEmpField5 As New clsassess_empfield5_master
                        If CInt(drow(iRdx).Item("empfield5unkid")) > 0 Then
                            objEmpField5._Isvoid = True
                            objEmpField5._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField5._Voidreason = strVoidReasion
                            objEmpField5._Voiduserunkid = User._Object._Userunkid
                            With objEmpField5
                                ._FormName = mstrModuleName
                                ._Loginemployeeunkid = 0
                                ._ClientIP = getIP()
                                ._HostName = getHostName()
                                ._FromWeb = False
                                ._AuditUserId = User._Object._Userunkid
._CompanyUnkid = Company._Object._Companyunkid
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            End With
                            objEmpField5.Delete(CInt(drow(iRdx).Item("empfield5unkid")))
                        End If
                        objEmpField5 = Nothing

                End Select
            Next
            Call Fill_Grid()
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            objChkAll.CheckState = CheckState.Unchecked
            If dgvData.RowCount <= 0 Then
                objChkAll.Enabled = False
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "VoidCheckedData", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmGlobalVoidEmpGoals_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmnuDelete = New ContextMenuStrip()
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
            Call Generate_MenuItems()
            Call objbtnReset_Click(New Object, New EventArgs)
            'S.SANDEEP [07-OCT-2017] -- START
            'ISSUE/ENHANCEMENT : IMPORT GOALS
            If mblnIsPreview Then
                cboPeriod.SelectedValue = mintPeriodId
                cboEmployee.SelectedValue = mintEmployeeId
                Call objbtnSearch_Click(New Object, New EventArgs)
                gbFilterCriteria.Enabled = False
                btnVoid.Enabled = False
            End If
            'S.SANDEEP [07-OCT-2017] -- END

            If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.FINAL_SAVE)) Then
                    objpnl2.BackColor = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.FINAL_SAVE))))
                Else
                    objpnl2.BackColor = Color.Blue
                End If
            Else
                objpnl2.BackColor = Color.Blue
            End If

            If ConfigParameter._Object._BSC_StatusColors.Keys.Count > 0 Then
                If ConfigParameter._Object._BSC_StatusColors.ContainsKey(CInt(enObjective_Status.SUBMIT_APPROVAL)) Then
                    objpnl1.BackColor = Color.FromArgb(CInt(ConfigParameter._Object._BSC_StatusColors(CInt(enObjective_Status.SUBMIT_APPROVAL))))
                Else
                    objpnl1.BackColor = Color.Green
                End If
            Else
                objpnl1.BackColor = Color.Green
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalVoidEmpGoals_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsassess_empfield1_master.SetMessages()
            clsassess_empfield2_master.SetMessages()
            clsassess_empfield3_master.SetMessages()
            clsassess_empfield4_master.SetMessages()
            clsassess_empfield5_master.SetMessages()
            objfrm._Other_ModuleNames = "clsassess_empfield1_master,clsassess_empfield2_master,clsassess_empfield3_master,clsassess_empfield4_master,clsassess_empfield5_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or _
                   CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee and Period are mandatory information. Please select Employee and Period to continue."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            Call Fill_Grid()
            If dgvData.RowCount <= 0 Then
                objChkAll.Enabled = False
            End If
            objbtnSearch.ShowResult(dgvData.RowCount)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            dgvData.DataSource = Nothing
            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            objChkAll.CheckState = CheckState.Unchecked
            objChkAll.Enabled = False
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            objbtnReset.ShowResult(dgvData.RowCount)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmp.Click
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .CodeMember = "employeecode"
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                If .DisplayDialog = True Then
                    cboEmployee.SelectedValue = .SelectedValue
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchField1_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Context Menu Events "

    Private Sub objbtnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmReasonSelection
        Try
            Dim dtmp() As DataRow = mdtFinal.Select("ischeck=true")
            If dtmp.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Please tick atleast one employee goal to delete it."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim iVoidReason As String = String.Empty

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            Dim iMsg As String = Language.getMessage(mstrModuleName, 7, "You are about to remove the information from employee level, this will delete all child linked to this parent") & vbCrLf & _
                                 Language.getMessage(mstrModuleName, 8, "Are you sure you want to delete?")

            If eZeeMsgBox.Show(iMsg, CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then Exit Sub

            frm.displayDialog(enVoidCategoryType.ASSESSMENT, iVoidReason)
            If iVoidReason.Length <= 0 Then
                Exit Sub
            End If

            Call VoidCheckedData(CInt(sender.tag.ToString.Split("|")(1)), iVoidReason, dtmp)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnDel_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " CheckBox Events "

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            For Each row As DataRow In mdtFinal.Rows
                row.Item("ischeck") = CBool(objChkAll.CheckState)
                row.AcceptChanges()
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnVoid.GradientBackColor = GUI._ButttonBackColor
            Me.btnVoid.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnVoid.Text = Language._Object.getCaption(Me.btnVoid.Name, Me.btnVoid.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, No data defined for asseessment caption settings screen.")
            Language.setMessage(mstrModuleName, 2, "Sorry no field is mapped with the selected period.")
            Language.setMessage(mstrModuleName, 3, "Total Weight Assigned :")
            Language.setMessage(mstrModuleName, 4, "Employee and Period are mandatory information. Please select Employee and Period to continue.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class