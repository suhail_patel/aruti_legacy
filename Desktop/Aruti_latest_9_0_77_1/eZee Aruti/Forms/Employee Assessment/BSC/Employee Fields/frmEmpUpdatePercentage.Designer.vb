﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEmpUpdatePercentage
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEmpUpdatePercentage))
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlnkCaption = New System.Windows.Forms.LinkLabel
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objgbOwnerItems = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlItmes = New System.Windows.Forms.Panel
        Me.objchkItems = New System.Windows.Forms.CheckBox
        Me.dgvItems = New System.Windows.Forms.DataGridView
        Me.gbOperation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkUpdate = New System.Windows.Forms.LinkLabel
        Me.radApplyToAll = New System.Windows.Forms.RadioButton
        Me.objStLine1 = New eZee.Common.eZeeStraightLine
        Me.radApplyTochecked = New System.Windows.Forms.RadioButton
        Me.txtPercent = New eZee.TextBox.NumericTextBox
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.gbDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeGradientButton
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.objbtnSearchOwner = New eZee.Common.eZeeGradientButton
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboOwner = New System.Windows.Forms.ComboBox
        Me.objdgcolhfcheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objFooter.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.objgbOwnerItems.SuspendLayout()
        Me.pnlItmes.SuspendLayout()
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbOperation.SuspendLayout()
        Me.gbDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlnkCaption)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 467)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(894, 55)
        Me.objFooter.TabIndex = 452
        '
        'objlnkCaption
        '
        Me.objlnkCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnkCaption.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlnkCaption.Location = New System.Drawing.Point(479, 20)
        Me.objlnkCaption.Name = "objlnkCaption"
        Me.objlnkCaption.Size = New System.Drawing.Size(203, 16)
        Me.objlnkCaption.TabIndex = 7
        Me.objlnkCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(688, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(788, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objgbOwnerItems)
        Me.pnlMain.Controls.Add(Me.gbOperation)
        Me.pnlMain.Controls.Add(Me.gbDetails)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(894, 522)
        Me.pnlMain.TabIndex = 451
        '
        'objgbOwnerItems
        '
        Me.objgbOwnerItems.BorderColor = System.Drawing.Color.Black
        Me.objgbOwnerItems.Checked = False
        Me.objgbOwnerItems.CollapseAllExceptThis = False
        Me.objgbOwnerItems.CollapsedHoverImage = Nothing
        Me.objgbOwnerItems.CollapsedNormalImage = Nothing
        Me.objgbOwnerItems.CollapsedPressedImage = Nothing
        Me.objgbOwnerItems.CollapseOnLoad = False
        Me.objgbOwnerItems.Controls.Add(Me.pnlItmes)
        Me.objgbOwnerItems.ExpandedHoverImage = Nothing
        Me.objgbOwnerItems.ExpandedNormalImage = Nothing
        Me.objgbOwnerItems.ExpandedPressedImage = Nothing
        Me.objgbOwnerItems.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbOwnerItems.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbOwnerItems.HeaderHeight = 25
        Me.objgbOwnerItems.HeaderMessage = ""
        Me.objgbOwnerItems.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.objgbOwnerItems.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbOwnerItems.HeightOnCollapse = 0
        Me.objgbOwnerItems.LeftTextSpace = 0
        Me.objgbOwnerItems.Location = New System.Drawing.Point(3, 69)
        Me.objgbOwnerItems.Name = "objgbOwnerItems"
        Me.objgbOwnerItems.OpenHeight = 300
        Me.objgbOwnerItems.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbOwnerItems.ShowBorder = True
        Me.objgbOwnerItems.ShowCheckBox = False
        Me.objgbOwnerItems.ShowCollapseButton = False
        Me.objgbOwnerItems.ShowDefaultBorderColor = True
        Me.objgbOwnerItems.ShowDownButton = False
        Me.objgbOwnerItems.ShowHeader = True
        Me.objgbOwnerItems.Size = New System.Drawing.Size(888, 397)
        Me.objgbOwnerItems.TabIndex = 496
        Me.objgbOwnerItems.Temp = 0
        Me.objgbOwnerItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlItmes
        '
        Me.pnlItmes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlItmes.Controls.Add(Me.objchkItems)
        Me.pnlItmes.Controls.Add(Me.dgvItems)
        Me.pnlItmes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlItmes.Location = New System.Drawing.Point(2, 26)
        Me.pnlItmes.Name = "pnlItmes"
        Me.pnlItmes.Size = New System.Drawing.Size(884, 369)
        Me.pnlItmes.TabIndex = 1
        '
        'objchkItems
        '
        Me.objchkItems.AutoSize = True
        Me.objchkItems.Location = New System.Drawing.Point(7, 5)
        Me.objchkItems.Name = "objchkItems"
        Me.objchkItems.Size = New System.Drawing.Size(15, 14)
        Me.objchkItems.TabIndex = 107
        Me.objchkItems.UseVisualStyleBackColor = True
        '
        'dgvItems
        '
        Me.dgvItems.AllowUserToAddRows = False
        Me.dgvItems.AllowUserToDeleteRows = False
        Me.dgvItems.AllowUserToResizeRows = False
        Me.dgvItems.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvItems.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvItems.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvItems.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhfcheck})
        Me.dgvItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvItems.Location = New System.Drawing.Point(0, 0)
        Me.dgvItems.MultiSelect = False
        Me.dgvItems.Name = "dgvItems"
        Me.dgvItems.RowHeadersVisible = False
        Me.dgvItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvItems.Size = New System.Drawing.Size(884, 369)
        Me.dgvItems.TabIndex = 6
        '
        'gbOperation
        '
        Me.gbOperation.BorderColor = System.Drawing.Color.Black
        Me.gbOperation.Checked = False
        Me.gbOperation.CollapseAllExceptThis = False
        Me.gbOperation.CollapsedHoverImage = Nothing
        Me.gbOperation.CollapsedNormalImage = Nothing
        Me.gbOperation.CollapsedPressedImage = Nothing
        Me.gbOperation.CollapseOnLoad = False
        Me.gbOperation.Controls.Add(Me.lnkUpdate)
        Me.gbOperation.Controls.Add(Me.radApplyToAll)
        Me.gbOperation.Controls.Add(Me.objStLine1)
        Me.gbOperation.Controls.Add(Me.radApplyTochecked)
        Me.gbOperation.Controls.Add(Me.txtPercent)
        Me.gbOperation.Controls.Add(Me.lblPercentage)
        Me.gbOperation.ExpandedHoverImage = Nothing
        Me.gbOperation.ExpandedNormalImage = Nothing
        Me.gbOperation.ExpandedPressedImage = Nothing
        Me.gbOperation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOperation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOperation.HeaderHeight = 25
        Me.gbOperation.HeaderMessage = ""
        Me.gbOperation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbOperation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOperation.HeightOnCollapse = 0
        Me.gbOperation.LeftTextSpace = 0
        Me.gbOperation.Location = New System.Drawing.Point(477, 2)
        Me.gbOperation.Name = "gbOperation"
        Me.gbOperation.OpenHeight = 300
        Me.gbOperation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOperation.ShowBorder = True
        Me.gbOperation.ShowCheckBox = False
        Me.gbOperation.ShowCollapseButton = False
        Me.gbOperation.ShowDefaultBorderColor = True
        Me.gbOperation.ShowDownButton = False
        Me.gbOperation.ShowHeader = True
        Me.gbOperation.Size = New System.Drawing.Size(414, 64)
        Me.gbOperation.TabIndex = 495
        Me.gbOperation.Temp = 0
        Me.gbOperation.Text = "Set Percentage Information"
        Me.gbOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkUpdate
        '
        Me.lnkUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkUpdate.AutoSize = True
        Me.lnkUpdate.BackColor = System.Drawing.Color.Transparent
        Me.lnkUpdate.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkUpdate.Location = New System.Drawing.Point(307, 6)
        Me.lnkUpdate.Name = "lnkUpdate"
        Me.lnkUpdate.Size = New System.Drawing.Size(95, 13)
        Me.lnkUpdate.TabIndex = 8
        Me.lnkUpdate.TabStop = True
        Me.lnkUpdate.Text = "Update Percent"
        '
        'radApplyToAll
        '
        Me.radApplyToAll.Location = New System.Drawing.Point(300, 36)
        Me.radApplyToAll.Name = "radApplyToAll"
        Me.radApplyToAll.Size = New System.Drawing.Size(102, 17)
        Me.radApplyToAll.TabIndex = 9
        Me.radApplyToAll.TabStop = True
        Me.radApplyToAll.Text = "Apply To All"
        Me.radApplyToAll.UseVisualStyleBackColor = True
        '
        'objStLine1
        '
        Me.objStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objStLine1.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me.objStLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objStLine1.Location = New System.Drawing.Point(149, 24)
        Me.objStLine1.Name = "objStLine1"
        Me.objStLine1.Size = New System.Drawing.Size(3, 40)
        Me.objStLine1.TabIndex = 9
        '
        'radApplyTochecked
        '
        Me.radApplyTochecked.Location = New System.Drawing.Point(158, 36)
        Me.radApplyTochecked.Name = "radApplyTochecked"
        Me.radApplyTochecked.Size = New System.Drawing.Size(136, 17)
        Me.radApplyTochecked.TabIndex = 8
        Me.radApplyTochecked.TabStop = True
        Me.radApplyTochecked.Text = "Apply To Checked"
        Me.radApplyTochecked.UseVisualStyleBackColor = True
        '
        'txtPercent
        '
        Me.txtPercent.AllowNegative = False
        Me.txtPercent.Decimal = New Decimal(New Integer() {0, 0, 0, 131072})
        Me.txtPercent.DigitsInGroup = 0
        Me.txtPercent.Flags = 65536
        Me.txtPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercent.Location = New System.Drawing.Point(93, 34)
        Me.txtPercent.MaxDecimalPlaces = 2
        Me.txtPercent.MaxWholeDigits = 9
        Me.txtPercent.Name = "txtPercent"
        Me.txtPercent.Prefix = ""
        Me.txtPercent.RangeMax = 1.7976931348623157E+308
        Me.txtPercent.RangeMin = -1.7976931348623157E+308
        Me.txtPercent.Size = New System.Drawing.Size(49, 21)
        Me.txtPercent.TabIndex = 490
        Me.txtPercent.Text = "0.00"
        Me.txtPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPercentage
        '
        Me.lblPercentage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercentage.Location = New System.Drawing.Point(10, 36)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(77, 17)
        Me.lblPercentage.TabIndex = 489
        Me.lblPercentage.Text = "% Completed"
        Me.lblPercentage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbDetails
        '
        Me.gbDetails.BorderColor = System.Drawing.Color.Black
        Me.gbDetails.Checked = False
        Me.gbDetails.CollapseAllExceptThis = False
        Me.gbDetails.CollapsedHoverImage = Nothing
        Me.gbDetails.CollapsedNormalImage = Nothing
        Me.gbDetails.CollapsedPressedImage = Nothing
        Me.gbDetails.CollapseOnLoad = False
        Me.gbDetails.Controls.Add(Me.objbtnSearch)
        Me.gbDetails.Controls.Add(Me.cboPeriod)
        Me.gbDetails.Controls.Add(Me.objbtnSearchOwner)
        Me.gbDetails.Controls.Add(Me.lblPeriod)
        Me.gbDetails.Controls.Add(Me.lblEmployee)
        Me.gbDetails.Controls.Add(Me.cboOwner)
        Me.gbDetails.ExpandedHoverImage = Nothing
        Me.gbDetails.ExpandedNormalImage = Nothing
        Me.gbDetails.ExpandedPressedImage = Nothing
        Me.gbDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDetails.HeaderHeight = 25
        Me.gbDetails.HeaderMessage = ""
        Me.gbDetails.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDetails.HeightOnCollapse = 0
        Me.gbDetails.LeftTextSpace = 0
        Me.gbDetails.Location = New System.Drawing.Point(3, 2)
        Me.gbDetails.Name = "gbDetails"
        Me.gbDetails.OpenHeight = 300
        Me.gbDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDetails.ShowBorder = True
        Me.gbDetails.ShowCheckBox = False
        Me.gbDetails.ShowCollapseButton = False
        Me.gbDetails.ShowDefaultBorderColor = True
        Me.gbDetails.ShowDownButton = False
        Me.gbDetails.ShowHeader = True
        Me.gbDetails.Size = New System.Drawing.Size(471, 64)
        Me.gbDetails.TabIndex = 494
        Me.gbDetails.Temp = 0
        Me.gbDetails.Text = "Goal Owner And Period Detail"
        Me.gbDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch.BorderSelected = False
        Me.objbtnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSearch.Image = Global.Aruti.Main.My.Resources.Resources.search_20
        Me.objbtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch.Location = New System.Drawing.Point(447, 2)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch.TabIndex = 493
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 200
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(326, 34)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(133, 21)
        Me.cboPeriod.TabIndex = 478
        '
        'objbtnSearchOwner
        '
        Me.objbtnSearchOwner.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOwner.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOwner.BorderSelected = False
        Me.objbtnSearchOwner.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOwner.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOwner.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOwner.Location = New System.Drawing.Point(247, 34)
        Me.objbtnSearchOwner.Name = "objbtnSearchOwner"
        Me.objbtnSearchOwner.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOwner.TabIndex = 483
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(274, 36)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(46, 17)
        Me.lblPeriod.TabIndex = 482
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(58, 17)
        Me.lblEmployee.TabIndex = 480
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOwner
        '
        Me.cboOwner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOwner.DropDownWidth = 200
        Me.cboOwner.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOwner.FormattingEnabled = True
        Me.cboOwner.Location = New System.Drawing.Point(72, 34)
        Me.cboOwner.Name = "cboOwner"
        Me.cboOwner.Size = New System.Drawing.Size(169, 21)
        Me.cboOwner.TabIndex = 482
        '
        'objdgcolhfcheck
        '
        Me.objdgcolhfcheck.Frozen = True
        Me.objdgcolhfcheck.HeaderText = ""
        Me.objdgcolhfcheck.Name = "objdgcolhfcheck"
        Me.objdgcolhfcheck.Width = 25
        '
        'frmEmpUpdatePercentage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 522)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEmpUpdatePercentage"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Update Percentage Completed"
        Me.objFooter.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.objgbOwnerItems.ResumeLayout(False)
        Me.pnlItmes.ResumeLayout(False)
        Me.pnlItmes.PerformLayout()
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbOperation.ResumeLayout(False)
        Me.gbOperation.PerformLayout()
        Me.gbDetails.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents objlnkCaption As System.Windows.Forms.LinkLabel
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objgbOwnerItems As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlItmes As System.Windows.Forms.Panel
    Friend WithEvents objchkItems As System.Windows.Forms.CheckBox
    Friend WithEvents dgvItems As System.Windows.Forms.DataGridView
    Friend WithEvents gbOperation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkUpdate As System.Windows.Forms.LinkLabel
    Friend WithEvents radApplyToAll As System.Windows.Forms.RadioButton
    Friend WithEvents objStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents radApplyTochecked As System.Windows.Forms.RadioButton
    Friend WithEvents txtPercent As eZee.TextBox.NumericTextBox
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents gbDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchOwner As eZee.Common.eZeeGradientButton
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboOwner As System.Windows.Forms.ComboBox
    Friend WithEvents objdgcolhfcheck As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
